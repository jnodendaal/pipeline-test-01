package com.pilog.version;

/**
 * @author Gavin Boshoff
 */
public class EPICClientVersion
{
    public static final String VERSION = "13";
}
