package com.pilog.epic.ide;

/**
 *
 * @author Bouwer.duPreez
 */
public class EpicCodeTemplate
{
    private String templateName;
    private String templateDescription;
    private String templateCode;

    public EpicCodeTemplate(String name, String description, String code)
    {
        this.templateName = name;
        this.templateDescription = description;
        this.templateCode = code;
    }

    public String getTemplateCode()
    {
        return templateCode;
    }

    public void setTemplateCode(String templateCode)
    {
        this.templateCode = templateCode;
    }

    public String getTemplateDescription()
    {
        return templateDescription;
    }

    public void setTemplateDescription(String templateDescription)
    {
        this.templateDescription = templateDescription;
    }

    public String getTemplateName()
    {
        return templateName;
    }

    public void setTemplateName(String templateName)
    {
        this.templateName = templateName;
    }
}
