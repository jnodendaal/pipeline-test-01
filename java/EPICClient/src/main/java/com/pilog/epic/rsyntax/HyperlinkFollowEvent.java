/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.epic.rsyntax;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class HyperlinkFollowEvent
{
    private String hyperlink;

    public HyperlinkFollowEvent(String hyperlink)
    {
        this.hyperlink = hyperlink;
    }

    public HyperlinkFollowEvent()
    {
    }

    public String getHyperlink()
    {
        return hyperlink;
    }

    public void setHyperlink(String hyperlink)
    {
        this.hyperlink = hyperlink;
    }
}
