/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.epic.ide;

import com.pilog.epic.ParserContext;
import com.pilog.epic.rsyntax.EPICAutoComplete;
import com.pilog.epic.rsyntax.EPICParser;
import com.pilog.epic.rsyntax.HyperlinkFollowListener;
import com.pilog.epic.rsyntax.MethodParameterChoicesProvider;
import com.pilog.epic.rsyntax.ParameterChoicesProviderProxy;
import com.pilog.epic.rsyntax.RegexCompletionProvider;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import javax.swing.text.NumberFormatter;
import org.fife.rsta.ui.CollapsibleSectionPanel;
import org.fife.rsta.ui.GoToDialog;
import org.fife.rsta.ui.SizeGripIcon;
import org.fife.rsta.ui.search.FindDialog;
import org.fife.rsta.ui.search.FindToolBar;
import org.fife.rsta.ui.search.ReplaceDialog;
import org.fife.rsta.ui.search.ReplaceToolBar;
import org.fife.rsta.ui.search.SearchEvent;
import org.fife.rsta.ui.search.SearchListener;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.CompletionProviderBase;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.ErrorStrip;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.folding.CurlyFoldParser;
import org.fife.ui.rsyntaxtextarea.folding.FoldParserManager;
import org.fife.ui.rsyntaxtextarea.parser.TaskTagParser;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.fife.ui.rtextarea.SearchContext;
import org.fife.ui.rtextarea.SearchEngine;
import org.fife.ui.rtextarea.SearchResult;
import org.fife.ui.rtextarea.ToolTipSupplier;

import static javax.swing.Action.*;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class RSyntaxEpicEditor extends javax.swing.JPanel implements EpicScriptEditor
{
    private final String userPrefferedFont = "userPrefferedFont";
    private final String userPrefferedFontSize = "userPrefferedFontSize";

    private final Preferences preferences;
    private final RSyntaxTextArea textArea;
    private final CollapsibleSectionPanel csp;
    private final StatusBar statusBar;
    private final EpicSearchListener searchListener;
    private final ParserContext parserContext;
    private AutoCompletion autoCompletion;
    private FindDialog findDialog;
    private ReplaceDialog replaceDialog;
    private FindToolBar findToolBar;
    private ReplaceToolBar replaceToolBar;
    private JFormattedTextField jFormattedTextFieldFontSize;

    public RSyntaxEpicEditor(ParserContext parserContext)
    {
        this.parserContext = parserContext;
        this.preferences = Preferences.userNodeForPackage(RSyntaxEpicEditor.class);
        initComponents();

        AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
        atmf.putMapping("text/epic", "com.pilog.epic.rsyntax.EPICTokenMaker");

        FoldParserManager.get().addFoldParserMapping("text/epic", new CurlyFoldParser(true, true));

        csp = new CollapsibleSectionPanel();
        searchListener = new EpicSearchListener();

        textArea = new RSyntaxTextArea();
        RTextScrollPane rTextScrollPane = new RTextScrollPane(textArea);
        rTextScrollPane.setFoldIndicatorEnabled(true);
        rTextScrollPane.setIconRowHeaderEnabled(true);
        rTextScrollPane.setLineNumbersEnabled(true);

        rTextScrollPane.getGutter().setBookmarkingEnabled(true);

        this.add(csp, BorderLayout.CENTER);
        csp.add(rTextScrollPane);

        textArea.addParser(new EPICParser(parserContext));
        textArea.addParser(new TaskTagParser());
        textArea.setCodeFoldingEnabled(true);
        textArea.setAntiAliasingEnabled(true);
        textArea.setSyntaxEditingStyle("text/epic");
        textArea.setAnimateBracketMatching(true);
        textArea.setAutoIndentEnabled(true);
        textArea.setBracketMatchingEnabled(true);
        textArea.setFractionalFontMetricsEnabled(true);
        textArea.setMarkOccurrences(true);
        textArea.setWrapStyleWord(true);

        ErrorStrip errorStrip = new ErrorStrip(textArea);
        add(errorStrip, BorderLayout.LINE_END);

        statusBar = new StatusBar();
        add(statusBar, BorderLayout.SOUTH);
        add(createMenuBar() , BorderLayout.PAGE_START);

        initSearchDialogs();
    }

    @Override
    public void setMethodParameterChoicesProvider(MethodParameterChoicesProvider methodParameterChoicesProvider)
    {
        ((CompletionProviderBase) getAutoCompletion().getCompletionProvider()).setParameterChoicesProvider(new ParameterChoicesProviderProxy(methodParameterChoicesProvider, autoCompletion.getCompletionProvider()));
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        textArea.setEditable(enabled);
    }

    @Override
    public void addCodeCompletionHandler(CodeCompletionHandler handler)
    {
    }

    @Override
    public void clearCodeCompletionHandlers()
    {
    }

    @Override
    public void configureSyntax(Map<String, Color> syntaxColors, Map<String, String> syntaxHelp)
    {
    }

    @Override
    public String getSourceCode()
    {
        return textArea.getText();
    }

    @Override
    public void highlightLine(int lineNumber, Color color)
    {
    }

    @Override
    public void insertSourceCode(String sourceCode)
    {
        try
        {
            insertSourceCode(sourceCode, textArea.getCaretPosition());
        }
        catch (Exception e)
        {
            // This exception will never practically occur because we guarantee the use of a valid position.
            e.printStackTrace();
        }
    }

    @Override
    public void insertSourceCode(String sourceCode, int position) throws BadLocationException
    {
        textArea.getDocument().insertString(textArea.getCaretPosition(), sourceCode, null);
    }

    @Override
    public void loadFile(String sourceFilePath)
    {
        BufferedReader inStream = null;

        try
        {
            StringBuffer text;
            String lineData;

            inStream = new BufferedReader(new InputStreamReader(new FileInputStream(sourceFilePath)));

            text = new StringBuffer();
            lineData = inStream.readLine();
            while (lineData != null)
            {
                text.append(lineData);
                text.append("\n");
            }

            textArea.setText(text.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                inStream.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setSourceCode(String sourceCode)
    {
        textArea.setText(sourceCode);
    }

    @Override
    public JComponent getComponent()
    {
        return this;
    }

    @Override
    public void startEditor()
    {
        getAutoCompletion().uninstall();
        getAutoCompletion().install(textArea);
    }

    @Override
    public void stopEditor()
    {
        getAutoCompletion().uninstall();
        findDialog.dispose();
        replaceDialog.dispose();
        textArea.setLinkGenerator(null);
        textArea.setToolTipSupplier(null);
        EPICAutoComplete.getDefault().clearRegexCompletionProviders();
    }

    @Override
    public void setHyperLinkListener(HyperlinkFollowListener listener)
    {
        textArea.setHyperlinksEnabled(true);
        textArea.setLinkScanningMask(KeyEvent.CTRL_DOWN_MASK);
        textArea.setLinkGenerator(new com.pilog.epic.rsyntax.LinkGenerator(listener));
    }

    @Override
    public void setRegexCompletionProviders(List<RegexCompletionProvider> regexCompletionProviders)
    {
        for (RegexCompletionProvider regexCompletionProvider : regexCompletionProviders)
        {
            EPICAutoComplete.getDefault().addRegexCompletionProvider(regexCompletionProvider);
        }
    }

    @Override
    public void setTooltipSupplier(final ToolTipSupplier tooltipSupplier)
    {
        textArea.setToolTipSupplier(tooltipSupplier);
    }

    private AutoCompletion getAutoCompletion()
    {
        if (autoCompletion == null)
        {
            autoCompletion = new AutoCompletion(EPICAutoComplete.getDefault());
            autoCompletion.setParameterAssistanceEnabled(true);
            autoCompletion.setShowDescWindow(true);
            autoCompletion.setAutoActivationDelay(1000);
            autoCompletion.setAutoActivationEnabled(false);
            autoCompletion.setAutoCompleteSingleChoices(true);
            autoCompletion.setAutoCompleteEnabled(true);
        }
        return autoCompletion;
    }

    /**
     * Creates our Find and Replace dialogs.
     */
    public final void initSearchDialogs()
    {
        findDialog = new FindDialog(SwingUtilities.windowForComponent(this), searchListener);
        replaceDialog = new ReplaceDialog(SwingUtilities.windowForComponent(this), searchListener);

        // This ties the properties of the two dialogs together (match case,
        // regex, etc.).
        SearchContext context = findDialog.getSearchContext();
        replaceDialog.setSearchContext(context);

        // Create tool bars and tie their search contexts together also.
        findToolBar = new FindToolBar(searchListener);
        findToolBar.setSearchContext(context);
        replaceToolBar = new ReplaceToolBar(searchListener);
        replaceToolBar.setSearchContext(context);
    }

    private JComponent createMenuBar()
    {
        JMenuBar mb = new JMenuBar();
        JMenu menu = new JMenu("Search");
        menu.add(new JMenuItem(new ShowFindDialogAction()));
        menu.add(new JMenuItem(new ShowReplaceDialogAction()));
        menu.add(new JMenuItem(new GoToLineAction()));
        menu.addSeparator();

        int ctrl = getToolkit().getMenuShortcutKeyMask();
        int shift = InputEvent.SHIFT_MASK;
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_F, ctrl | shift);
        Action a = csp.addBottomComponent(ks, findToolBar);
        a.putValue(Action.NAME, "Show Find Search Bar");
        menu.add(new JMenuItem(a));
        ks = KeyStroke.getKeyStroke(KeyEvent.VK_H, ctrl | shift);
        a = csp.addBottomComponent(ks, replaceToolBar);
        a.putValue(Action.NAME, "Show Replace Search Bar");
        menu.add(new JMenuItem(a));

        mb.add(menu);

        JPanel fontSettingsPanel;
        JComboBox<String> jComboBoxFonts;
        DefaultComboBoxModel<String> fontModel;
        GraphicsEnvironment localGraphicsEnvironment;
        String[] availableFontFamilyNames;

        fontSettingsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jFormattedTextFieldFontSize = new JFormattedTextField(new NumberFormatter(NumberFormat.getIntegerInstance()));
        jFormattedTextFieldFontSize.setPreferredSize(new Dimension(25, 23));
        jFormattedTextFieldFontSize.setValue(preferences.getLong(userPrefferedFontSize, 12));
        jComboBoxFonts = new JComboBox<>();
        fontModel = new DefaultComboBoxModel<>();
        localGraphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        availableFontFamilyNames = localGraphicsEnvironment.getAvailableFontFamilyNames();
        for (String fontName : availableFontFamilyNames)
        {
            fontModel.addElement(fontName);
        }
        jComboBoxFonts.setModel(fontModel);
        jComboBoxFonts.addItemListener(new FontChangeListener());
        jComboBoxFonts.setSelectedItem(preferences.get(userPrefferedFont, textArea.getFont().getFontName()));

        fontSettingsPanel.add(mb);
        fontSettingsPanel.add(jComboBoxFonts);
        fontSettingsPanel.add(jFormattedTextFieldFontSize);

        return fontSettingsPanel;
    }

    private class EpicSearchListener implements SearchListener
    {
        @Override
        public void searchEvent(SearchEvent e)
        {
            SearchEvent.Type type = e.getType();
            SearchContext context = e.getSearchContext();
            SearchResult result = null;

            switch (type)
            {
                case MARK_ALL:
                    result = SearchEngine.markAll(textArea, context);
                    break;
                case FIND:
                    result = SearchEngine.find(textArea, context);
                    if (!result.wasFound())
                    {
                        UIManager.getLookAndFeel().provideErrorFeedback(textArea);
                    }
                    break;
                case REPLACE:
                    result = SearchEngine.replace(textArea, context);
                    if (!result.wasFound())
                    {
                        UIManager.getLookAndFeel().provideErrorFeedback(textArea);
                    }
                    break;
                case REPLACE_ALL:
                    result = SearchEngine.replaceAll(textArea, context);
                    JOptionPane.showMessageDialog(null, result.getCount() + " occurrences replaced.");
                    break;
            }

            String text = null;
            if (result.wasFound())
            {
                text = "Text found; occurrences marked: " + result.getMarkedCount();
            }
            else if (type == SearchEvent.Type.MARK_ALL)
            {
                if (result.getMarkedCount() > 0)
                {
                    text = "Occurrences marked: " + result.getMarkedCount();
                }
                else
                {
                    text = "";
                }
            }
            else
            {
                text = "Text not found";
            }
            statusBar.setLabel(text);
        }

        @Override
        public String getSelectedText()
        {
            return textArea.getSelectedText();
        }
    }

    private class FontChangeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if(e.getStateChange() == ItemEvent.SELECTED)
            {
                Long fontSize = (Long) jFormattedTextFieldFontSize.getValue();
                if(fontSize == null || fontSize < 6 || fontSize > 32) fontSize = 12l;
                textArea.setFont(new Font((String) e.getItem(), Font.PLAIN, fontSize.intValue()));
                preferences.put(userPrefferedFont, (String) e.getItem());
                preferences.putLong(userPrefferedFontSize, fontSize);
            }
        }
    }

    private class GoToLineAction extends AbstractAction
    {
        private GoToLineAction()
        {
            super("Go To Line...");
            int c = getToolkit().getMenuShortcutKeyMask();
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_L, c));
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (findDialog.isVisible())
            {
                findDialog.setVisible(false);
            }
            if (replaceDialog.isVisible())
            {
                replaceDialog.setVisible(false);
            }
            GoToDialog dialog = new GoToDialog(SwingUtilities.windowForComponent(RSyntaxEpicEditor.this));
            dialog.setMaxLineNumberAllowed(textArea.getLineCount());
            dialog.setVisible(true);
            int line = dialog.getLineNumber();
            if (line > 0)
            {
                try
                {
                    textArea.setCaretPosition(textArea.getLineStartOffset(line - 1));
                }
                catch (BadLocationException ble)
                { // Never happens
                    UIManager.getLookAndFeel().provideErrorFeedback(textArea);
                    ble.printStackTrace();
                }
            }
        }

    }

    private class ShowFindDialogAction extends AbstractAction
    {
        private ShowFindDialogAction()
        {
            super("Find...");
            int c = getToolkit().getMenuShortcutKeyMask();
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F, c));
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (replaceDialog.isVisible())
            {
                replaceDialog.setVisible(false);
            }
            findDialog.setVisible(true);
        }
    }

    private class ShowReplaceDialogAction extends AbstractAction
    {
        private ShowReplaceDialogAction()
        {
            super("Replace...");
            int c = getToolkit().getMenuShortcutKeyMask();
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_H, c));
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (findDialog.isVisible())
            {
                findDialog.setVisible(false);
            }
            replaceDialog.setVisible(true);
        }
    }

    private static class StatusBar extends JPanel
    {
        private final JLabel label;

        private StatusBar()
        {
            label = new JLabel("Ready");
            setLayout(new BorderLayout());
            add(label, BorderLayout.LINE_START);
            add(new JLabel(new SizeGripIcon()), BorderLayout.LINE_END);
        }

        public void setLabel(String label)
        {
            this.label.setText(label);
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
