package com.pilog.epic.ide;

import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface CodeCompletionHandler
{
    public List<CodeCompletionResolver> getResolvers();
}
