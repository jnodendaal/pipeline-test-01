/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.epic.rsyntax;

import java.util.ArrayList;
import java.util.List;
import javax.swing.text.JTextComponent;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.ParameterChoicesProvider;
import org.fife.ui.autocomplete.ParameterizedCompletion;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class ParameterChoicesProviderProxy implements ParameterChoicesProvider
{
    private MethodParameterChoicesProvider parameterChoicesProvider;
    private CompletionProvider completionProvider;

    public ParameterChoicesProviderProxy(
                                         MethodParameterChoicesProvider parameterChoicesProvider,
                                         CompletionProvider completionProvider)
    {
        this.parameterChoicesProvider = parameterChoicesProvider;
        this.completionProvider = completionProvider;
    }

    @Override
    public List getParameterChoices(JTextComponent tc,
                                    ParameterizedCompletion.Parameter param)
    {
        MethodParameter methodParameter = new MethodParameter(param.getTypeObject(), param.getName(), param.isEndParam());
        List<Completion> returnList = new ArrayList<Completion>();
        for (BasicEpicCompletion methodParameterChoice : parameterChoicesProvider.getParameterChoices(tc, methodParameter))
        {
            returnList.add(new BasicCompletion(completionProvider, methodParameterChoice.getReplacementText(), null, null));
        }
        return returnList;
    }

}
