/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.epic.rsyntax.autocomplete;

import com.pilog.epic.annotation.EPICClass;
import com.pilog.epic.annotation.EPICMethod;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.ShorthandCompletion;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class ClassCompletion
{
    private static final T8Logger logger = T8Log.getLogger(ClassCompletion.class.getName());

    private ShorthandCompletion completion;
    private boolean onlyShowDeclared;
    private boolean onlyShowAnnotated;
    private String preferredVariableName;

    private List<MethodCompletion> methodCompletions;

    public ClassCompletion(CompletionProvider completionProvider, Class<?> completionClass)
    {
        EPICClass annotation = completionClass.getAnnotation(EPICClass.class);
        if(annotation != null && annotation.PreferredVariableName() != null)
        {
            completion = new ShorthandCompletion(completionProvider, annotation.PreferredVariableName(), annotation.VariableCreationTemplate());
            completion.setShortDescription(annotation.Description());
            completion.setSummary(annotation.Description());
            onlyShowDeclared = annotation.onlyShowDeclaredMethods();
            onlyShowAnnotated = annotation.onlyShowAnnotatedMethods();
            preferredVariableName = annotation.PreferredVariableName();
        }
        else logger.log(T8Logger.Level.WARNING, "Annotation incomplete for " + completionClass + " - using annotation " + annotation);

        methodCompletions = new ArrayList<MethodCompletion>();
        for (Method method : (onlyShowDeclared ? completionClass.getDeclaredMethods() : completionClass.getMethods()))
        {
            EPICMethod methodAnnotation = method.getAnnotation(EPICMethod.class);

            if(onlyShowAnnotated)
            {
                if(methodAnnotation != null && !methodAnnotation.Hidden()) methodCompletions.add(new MethodCompletion(completionProvider, method));
            }
            else
            {
                if(methodAnnotation == null || !methodAnnotation.Hidden()) methodCompletions.add(new MethodCompletion(completionProvider, method));
            }
        }
    }

    public String getPreferredVariableName()
    {
        return this.preferredVariableName == null ? "" : preferredVariableName.toUpperCase();
    }

    public Completion getCompletion()
    {
        return completion;
    }

    public List<MethodCompletion> getMethodCompletions()
    {
        return methodCompletions;
    }
}
