package com.pilog.epic.ide;

import com.pilog.epic.rsyntax.HyperlinkFollowListener;
import com.pilog.epic.rsyntax.MethodParameterChoicesProvider;
import com.pilog.epic.rsyntax.RegexCompletionProvider;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.text.BadLocationException;
import org.fife.ui.rtextarea.ToolTipSupplier;

/**
 * @author Gavin Boshoff
 */
public interface EpicScriptEditor
{
    public static final Color KEYWORD_COLOR_1 = Color.BLUE;
    public static final Color KEYWORD_COLOR_2 = new Color(180, 100, 255);
    
    void startEditor();
    void stopEditor();

    void addCodeCompletionHandler(CodeCompletionHandler handler);

    void setHyperLinkListener(HyperlinkFollowListener listener);

    /**
     * Specifies the component which will be used to supply the tooltips when
     * hovering over certain code sections, or performing other tooltip relevant
     * actions.
     *
     * @param tooltipSupplier The {@code ToolTipSupplier} component
     */
    void setTooltipSupplier(ToolTipSupplier tooltipSupplier);

    void clearCodeCompletionHandlers();

    void configureSyntax(Map<String, Color> syntaxColors, Map<String, String> syntaxHelp);

    String getSourceCode();

    void highlightLine(int lineNumber, Color color);

    void insertSourceCode(String sourceCode);

    void insertSourceCode(String sourceCode, int position) throws BadLocationException;

    void loadFile(String sourceFilePath);

    void setSourceCode(String sourceCode);

    void setMethodParameterChoicesProvider(MethodParameterChoicesProvider methodParameterChoicesProvider);

    void setRegexCompletionProviders(List<RegexCompletionProvider> regexCompletionProviders);

    JComponent getComponent();
}
