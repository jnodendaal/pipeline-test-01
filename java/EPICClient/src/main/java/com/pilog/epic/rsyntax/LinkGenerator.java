/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.epic.rsyntax;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.BadLocationException;
import org.fife.ui.rsyntaxtextarea.LinkGeneratorResult;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class LinkGenerator implements org.fife.ui.rsyntaxtextarea.LinkGenerator
{
    private final Pattern pattern;
    private final HyperlinkFollowListener listener;

    public LinkGenerator(HyperlinkFollowListener listener)
    {
        this.pattern = Pattern.compile(listener.getPatterMatchString());
        this.listener = listener;
    }

    public HyperlinkFollowListener getListener()
    {
        return listener;
    }

    @Override
    public LinkGeneratorResult isLinkAtOffset(RSyntaxTextArea textArea, int offs)
    {
        try
        {
            int wordEnd = findEndingOffset(textArea, offs);
            int wordStart = findStartingOffset(textArea, offs);

            if(wordStart < 0 && wordEnd >= textArea.getText().length())
                return null; //No matches for " found

            String word = textArea.getText(wordStart, wordEnd - wordStart);
            Matcher matcher = pattern.matcher(word);

            if (matcher.find())
            {
                return new GeneratorResult(matcher.start(), null, new HyperlinkFollowEvent(matcher.group()));
            }
        }
        catch (BadLocationException ex)
        {
        }
        return null;
    }

    private int findStartingOffset(RSyntaxTextArea textArea, int offs) throws BadLocationException
    {
        int finalOffset = offs;

        while(finalOffset >= 0 && !textArea.getText(finalOffset, 1).equals("\"")) finalOffset -= 1;

        return finalOffset;
    }

    private int findEndingOffset(RSyntaxTextArea textArea, int offs) throws BadLocationException
    {
        int finalOffset = offs;

        while(finalOffset < textArea.getText().length() && !textArea.getText(finalOffset, 1).equals("\"")) finalOffset += 1;

        return finalOffset;
    }

    private class GeneratorResult implements LinkGeneratorResult
    {
        private final int sourceOffset;
        private final HyperlinkFollowEvent event;

        GeneratorResult(int sourceOffset,
                               HyperlinkFollowListener listener,
                               HyperlinkFollowEvent event)
        {
            this.sourceOffset = sourceOffset;
            this.event = event;
        }

        @Override
        public HyperlinkEvent execute()
        {
            listener.hyperlinkFollowed(event);
            return null;
        }

        @Override
        public int getSourceOffset()
        {
            return sourceOffset;
        }
    }
}
