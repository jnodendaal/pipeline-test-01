package com.pilog.epic.ide;

import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface CodeCompletionResolver
{
    public List<String> getMatchPatterns();
    public Map<String, String> getCodeCompletionSuggestions(final String matchedText);
}
