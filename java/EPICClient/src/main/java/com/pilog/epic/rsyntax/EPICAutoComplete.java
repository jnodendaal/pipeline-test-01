/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.epic.rsyntax;

import com.pilog.epic.annotation.EPICClass;
import com.pilog.epic.rsyntax.autocomplete.ClassCompletion;
import com.pilog.epic.rsyntax.autocomplete.MethodCompletion;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.ParameterizedCompletion;
import org.reflections.Reflections;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class EPICAutoComplete extends DefaultCompletionProvider
{
    private List<ClassCompletion> epicClassCompletions;
    private List<RegexCompletionProvider> regexCompletionProviders;
    private static EPICAutoComplete instance;

    private EPICAutoComplete()
    {
        this.regexCompletionProviders = new ArrayList<RegexCompletionProvider>();
        init();
        setAutoActivationRules(true, ".");
        setParameterizedCompletionParams('(', ", ", ')');
        initialize();
    }

    public static EPICAutoComplete getDefault()
    {
        if(instance == null)
            instance = new EPICAutoComplete();
        return instance;
    }

    private void initialize()
    {
        epicClassCompletions = new ArrayList<ClassCompletion>();

        Reflections reflections = new Reflections("com.pilog.t8");
        Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(EPICClass.class);

        for (Class<?> epicClass : typesAnnotatedWith)
        {
            epicClassCompletions.add(new ClassCompletion(this, epicClass));
        }
    }

    public void addRegexCompletionProvider(RegexCompletionProvider regexCompletionProvider)
    {
        regexCompletionProviders.add(regexCompletionProvider);
    }

    public void clearRegexCompletionProviders()
    {
        regexCompletionProviders.clear();
    }

    /**
     * Gets the possible completions for the text component at the current
     * caret position.
     *
     * @param comp The text component.
     * <p>
     * @return The list of {@link Completion}s. If no completions are
     *         available, this may be <code>null</code>.
     */
    @Override
    public List<Completion> getCompletions(JTextComponent comp)
    {
        String variableName;
        String methodName;

        clear();
        variableName = getVariableName(comp);
        methodName = variableName = getVariableName(comp);
        if (variableName.contains("."))
        {
            methodName = getMethodName(variableName);
        }

        //First check if any of the regex providers can provide us with completions
        for (RegexCompletionProvider regexCompletionProvider : regexCompletionProviders)
        {
            String regexStringFound = getRegexMatch(comp, regexCompletionProvider.getRegexPatternToMatch());
            if(regexStringFound.matches(regexCompletionProvider.getRegexPatternToMatch()))
            {
                for (BasicEpicCompletion basicEpicCompletion : regexCompletionProvider.getCompletionChoices(comp, regexStringFound))
                {
                    addCompletion(new BasicCompletion(this, basicEpicCompletion.getReplacementText(), basicEpicCompletion.getShortDescription(), basicEpicCompletion.getSummary()));
                }

                return super.getCompletions(comp);
            }
        }


        for (ClassCompletion classCompletion : epicClassCompletions)
        {
            /*
             If the text infront of the caret it empty, return all classes that have a variable name set,
             otherwise analyze the text infront of the character, try to match classes starting with the specified text, if there is a .
             then return the methods for the class matching the name of the variable
             */
            if (variableName.isEmpty())
            {
                if ("".equals(classCompletion.getPreferredVariableName()))
                {
                    for (MethodCompletion methodCompletion : classCompletion.getMethodCompletions())
                    {
                        addCompletion(methodCompletion.getCompletion());
                    }
                }
                else
                {
                    addCompletion(classCompletion.getCompletion());
                }
            }
            else if (!"".equals(classCompletion.getPreferredVariableName()))
            {
                if (classCompletion.getPreferredVariableName().startsWith(variableName))
                {
                    addCompletion(classCompletion.getCompletion());
                }


                String variableNameStripped = stripMethodName(variableName);
                if (variableNameStripped.startsWith(classCompletion.getPreferredVariableName()) || variableNameStripped.endsWith(classCompletion.getPreferredVariableName()))
                {
                    for (MethodCompletion methodCompletion : classCompletion.getMethodCompletions())
                    {
                        if (methodCompletion.getName().startsWith(methodName) || methodName.isEmpty() || methodName.equals("."))
                        {
                            addCompletion(methodCompletion.getCompletion());
                        }
                    }
                }
            }
            else if("".equals(classCompletion.getPreferredVariableName()) && !variableName.contains("."))
            {
                for (MethodCompletion methodCompletion : classCompletion.getMethodCompletions())
                {
                    if (methodCompletion.getName().startsWith(methodName))
                    {
                        addCompletion(methodCompletion.getCompletion());
                    }
                }
            }
        }

        return super.getCompletions(comp);
    }

    private String stripMethodName(String variableString)
    {
        if(variableString.isEmpty() || variableString.indexOf('.') == -1) return variableString;
        return variableString.substring(0, variableString.indexOf('.')).replaceAll("\\s", "").toUpperCase();
    }

    private String getMethodName(String variableString)
    {
        if(variableString.isEmpty() || variableString.indexOf('.') == -1) return variableString;
        return variableString.substring(variableString.indexOf('.')).toUpperCase().replace(".", "");
    }

    private String getVariableName(JTextComponent comp)
    {
        StringBuilder variableName;
        boolean endFound;

        variableName = new StringBuilder();
        endFound = false;

        for (int i = comp.getCaretPosition() - 1; !endFound; i--)
        {
            try
            {
                String character = comp.getDocument().getText(i, 1);
                //We loop through in reverse, so always insert at first position
                variableName.insert(0, character);

                //Whitespace indicates the end of variable name
                if (character.matches("\\s"))
                {
                    endFound = true;
                }
            }
            catch (BadLocationException ex)
            {
                Logger.getLogger(EPICAutoComplete.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return variableName.toString().toUpperCase().replaceAll("\\s", "");
    }

    private String getRegexMatch(JTextComponent comp, String regex)
    {
        StringBuilder variableName;
        boolean endFound;

        variableName = new StringBuilder();
        endFound = false;

        for (int i = comp.getCaretPosition() - 1; !endFound; i--)
        {
            try
            {
                String character = comp.getDocument().getText(i, 1);

                //Whitespace indicates the end of variable name
                if (character.matches("\\s") || variableName.toString().matches(regex))
                {
                    endFound = true;
                }
                else
                {
                    //We loop through in reverse, so always insert at first position
                    variableName.insert(0, character);
                }
            }
            catch (BadLocationException ex)
            {
                Logger.getLogger(EPICAutoComplete.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return variableName.toString().replaceAll("\\s", "");
    }

    /**
     * Returns the completions that have been entered at the specified visual
     * location. This can be used for tool tips when the user hovers the
     * mouse over completed text.
     * <p>
     * @param comp The text component.
     * @param p    The position, usually from a <tt>MouseEvent</tt>.
     * <p>
     * @return The completions, or an empty list if there are none.
     */
//    @Override
//    public List<Completion> getCompletionsAt(JTextComponent comp, Point p)
//    {
//    }
    /**
     * Returns the cell renderer for completions returned from this provider.
     *
     * @return The cell renderer, or <code>null</code> if the default should
     *         be used.
     * <p>
     * @see #setListCellRenderer(ListCellRenderer)
     */
//    @Override
//    public ListCellRenderer getListCellRenderer()
//    {
//    }
    /**
     * Returns an object that can return a list of completion choices for
     * parameters. This is used when a user code-completes a parameterized
     * completion, such as a function or method. For any parameter to the
     * function/method, this object can return possible completions.
     *
     * @return The parameter choices provider, or <code>null</code> if
     *         none is installed.
     */
//    @Override
//    public ParameterChoicesProvider getParameterChoicesProvider()
//    {
//    }
    /**
     * Returns a list of parameterized completions that have been entered
     * at the current caret position of a text component (and thus can have
     * their completion choices displayed).
     * <p>
     * @param tc The text component.
     * <p>
     * @return The list of {@link ParameterizedCompletion}s. If no completions
     *         are available, this may be <code>null</code>.
     */
    @Override
    public List<ParameterizedCompletion> getParameterizedCompletions(
            JTextComponent comp)
    {
        List<ParameterizedCompletion> returnCompletions;
        String variableName;
        String methodName = null;

        returnCompletions = new ArrayList<ParameterizedCompletion>();
        methodName = variableName = getVariableName(comp);
        if (variableName.contains("."))
        {
            methodName = getMethodName(variableName);
        }

        for (ClassCompletion classCompletion : epicClassCompletions)
        {
            /*
             If the text infront of the caret it empty, return all classes that have a variable name set,
             otherwise analyze the text infront of the character, try to match classes starting with the specified text, if there is a .
             then return the methods for the class matching the name of the variable
             */
            if (!"".equals(classCompletion.getPreferredVariableName()))
            {
                if (classCompletion.getPreferredVariableName().equals(stripMethodName(variableName)))
                {
                    for (MethodCompletion methodCompletion : classCompletion.getMethodCompletions())
                    {
                        if (methodCompletion.getName().startsWith(methodName))
                        {
                            returnCompletions.add(methodCompletion.getCompletion());
                        }
                    }
                }
            }
            else
            {
                for (MethodCompletion methodCompletion : classCompletion.getMethodCompletions())
                {
                    if (methodCompletion.getName().startsWith(methodName))
                    {
                        returnCompletions.add(methodCompletion.getCompletion());
                    }
                }
            }
        }

        return returnCompletions;
    }

    /**
     * Returns the text that marks the end of a list of parameters to a
     * function or method.
     *
     * @return The text for a parameter list end, for example,
     *         '<code>)</code>'.
     * <p>
     * @see #getParameterListStart()
     * @see #getParameterListSeparator()
     * @see #setParameterizedCompletionParams(char, String, char)
     */
//    @Override
//    public char getParameterListEnd()
//    {
//    }
    /**
     * Returns the text that separates parameters to a function or method.
     *
     * @return The text that separates parameters, for example,
     *         "<code>, </code>".
     * <p>
     * @see #getParameterListStart()
     * @see #getParameterListEnd()
     * @see #setParameterizedCompletionParams(char, String, char)
     */
//    @Override
//    public String getParameterListSeparator()
//    {
//    }
    /**
     * Returns the text that marks the start of a list of parameters to a
     * function or method.
     *
     * @return The text for a parameter list start, for example,
     *         "<code>(</code>".
     * <p>
     * @see #getParameterListEnd()
     * @see #getParameterListSeparator()
     * @see #setParameterizedCompletionParams(char, String, char)
     */
//    @Override
//    public char getParameterListStart()
//    {
//    }
    /**
     * Returns the parent completion provider.
     *
     * @return The parent completion provider.
     * <p>
     * @see #setParent(CompletionProvider)
     */
//    @Override
//    public CompletionProvider getParent()
//    {
//    }
    /**
     * This method is called if auto-activation is enabled in the parent
     * {@link AutoCompletion} after the user types a single character. This
     * provider should check the text at the current caret position of the
     * text component, and decide whether auto-activation would be appropriate
     * here. For example, a <code>CompletionProvider</code> for Java might
     * want to return <code>true</code> for this method only if the last
     * character typed was a '<code>.</code>'.
     *
     * @param tc The text component.
     * <p>
     * @return Whether auto-activation would be appropriate.
     */
//    @Override
//    public boolean isAutoActivateOkay(JTextComponent tc)
//    {
//    }
    /**
     * Sets the renderer to use when displaying completion choices.
     *
     * @param r The renderer to use.
     * <p>
     * @see #getListCellRenderer()
     */
//    @Override
//    public void setListCellRenderer(ListCellRenderer r)
//    {
//    }
    /**
     * Sets the values used to identify and insert "parameterized completions"
     * (e.g. functions or methods). If this method isn't called, functions
     * and methods will not have their parameters auto-completed.
     *
     * @param listStart The character that marks the beginning of a list of
     *                  parameters, such as '<tt>(</tt>' in C or Java.
     * @param separator Text that should separate parameters in a parameter
     *                  list when one is inserted. For example, "<tt>, </tt>".
     * @param listEnd   The character that marks the end of a list of parameters,
     *                  such as '<tt>)</tt>' in C or Java.
     * <p>
     * @throws IllegalArgumentException If either <tt>listStart</tt> or
     * <tt>listEnd</tt> is not printable ASCII, or if
     * <tt>separator</tt> is <code>null</code> or an empty string.
     * @see #clearParameterizedCompletionParams()
     */
//    @Override
//    public void setParameterizedCompletionParams(char listStart,
//                                                 String separator, char listEnd)
//    {
//    }
    /**
     * Sets the parent completion provider.
     *
     * @param parent The parent provider.  <code>null</code> means there will
     *               be no parent provider.
     * <p>
     * @see #getParent()
     */
//    @Override
//    public void setParent(CompletionProvider parent)
//    {
//    }
}
