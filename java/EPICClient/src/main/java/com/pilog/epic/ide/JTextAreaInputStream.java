package com.pilog.epic.ide;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * @author Bouwer du Preez
 */
public class JTextAreaInputStream extends InputStream
{
    private static final T8Logger logger = T8Log.getLogger(JTextAreaInputStream.class.getSimpleName());

    private final JTextArea textArea;
    private final LinkedList<Byte> inputBuffer;
    private final Lock byteLock;

    public JTextAreaInputStream(JTextArea textArea)
    {
        this.textArea = textArea;
        this.inputBuffer = new LinkedList<>();
        //this.textArea.addKeyListener(new TextAreaKeyListener());
        this.textArea.getDocument().addDocumentListener(new TextAreaDocumentListener());
        this.byteLock = new ReentrantLock();
    }

    @Override
    public int read() throws IOException
    {
        byte inputByte;
        inputByte = getInputByte();
        logger.log("" + inputByte);
        return inputByte;
    }

    private synchronized byte getInputByte()
    {
        Byte inputByte;

        inputByte = inputBuffer.pollFirst();
        while (inputByte == null)
        {
            try
            {
                logger.log("Sleeping");
                Thread.sleep(1000);
                inputByte = inputBuffer.pollFirst();
            }
            catch (InterruptedException e)
            {
                logger.log("Interrupted.", e);
            }
        }

        return inputByte;
    }

    private synchronized void addInputByte(byte inputByte)
    {
        logger.log("Adding input byte: " + inputByte);
        byteLock.lock();
        logger.log("Adding input byte2: " + inputByte);
        inputBuffer.add(inputByte);
        byteLock.unlock();
    }

    private class TextAreaDocumentListener implements DocumentListener
    {
        @Override
        public void insertUpdate(DocumentEvent event)
        {
            try
            {
                Document doc;
                String inputText;
                byte[] inputBytes;

                logger.log("Handling insert.");
                doc = event.getDocument();
                inputText = doc.getText(event.getOffset(), event.getLength());
                inputBytes = inputText.getBytes();
                for (int offset = 0; offset < inputBytes.length; offset++)
                {
                    logger.log("Insert: " + inputBytes[offset]);
                    addInputByte(inputBytes[offset]);
                }
            }
            catch (BadLocationException e)
            {
                logger.log("Error inserting.", e);
            }
        }

        @Override
        public void removeUpdate(DocumentEvent e) {}

        @Override
        public void changedUpdate(DocumentEvent e) {}
    }
}
