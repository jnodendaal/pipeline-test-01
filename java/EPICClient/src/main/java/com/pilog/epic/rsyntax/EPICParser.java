package com.pilog.epic.rsyntax;

import com.pilog.epic.EPIC;
import com.pilog.epic.ExternalExpressionParser;
import com.pilog.epic.ParserContext;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ConstantExpression;
import com.pilog.epic.expressions.Expression;
import java.io.UnsupportedEncodingException;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.parser.AbstractParser;
import org.fife.ui.rsyntaxtextarea.parser.DefaultParseResult;
import org.fife.ui.rsyntaxtextarea.parser.DefaultParserNotice;
import org.fife.ui.rsyntaxtextarea.parser.ParseResult;

/**
 * @author Hennie Brink
 */
public class EPICParser extends AbstractParser
{
    private final ParserContext parserContext;
    private final DefaultParseResult result;

    public EPICParser(ParserContext parserContext)
    {
        this.parserContext = parserContext;
        this.result = new DefaultParseResult(this);
    }

    @Override
    public ParseResult parse(RSyntaxDocument doc, String style)
    {
        result.clearNotices();

        if (doc.getLength() == 0)
        {
            return result;
        }
        Element root = doc.getDefaultRootElement();
	int lineCount = root.getElementCount();

        result.setParseTime(System.currentTimeMillis());
        result.setParsedLines(0, lineCount-1);
        try
        {
            EPIC.compileProgram(doc.getText(0, doc.getLength()), "UTF-8", parserContext);
        }
        catch (EPICSyntaxException e)
        {
            Element element = root.getElement(e.getLineNumber());
            result.addNotice(new DefaultParserNotice(this, e.getMessage(), e.getLineNumber(), element.getStartOffset(), element.getEndOffset()));
        }
        catch (UnsupportedEncodingException e)
        {
            result.addNotice(new DefaultParserNotice(this,e.getMessage(), 0));
        }
        catch (BadLocationException ex)
        {
            result.setError(ex);
            ex.printStackTrace();
        }

        return result;
    }

    private class SurrogateParser implements ExternalExpressionParser
    {
        @Override
        public boolean isApplicableTo(String expression)
        {
            return true;
        }

        @Override
        public Expression parse(String expression)
        {
            return new ConstantExpression(expression);
        }
    }
}
