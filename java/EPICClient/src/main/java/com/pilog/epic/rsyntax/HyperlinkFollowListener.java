/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.epic.rsyntax;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public interface HyperlinkFollowListener
{
    String getPatterMatchString();

    void hyperlinkFollowed(HyperlinkFollowEvent event);
}
