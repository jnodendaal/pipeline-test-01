package com.pilog.epic.ide;

import com.pilog.epic.EPIC;
import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.ParserContext;
import com.pilog.epic.Program;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.xml.XmlDocument;
import java.awt.Color;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JFileChooser;

/**
 * @author Bouwer du Preez
 */
public class IDE extends javax.swing.JFrame
{
    private JTextAreaOutputStream textAreaOutputStream;
    private PrintStream textAreaPrintStream;
    private ParserContext parserContext;

    public IDE()
    {
        initComponents();

        textAreaOutputStream = new JTextAreaOutputStream(jTextAreaOutput);
        textAreaPrintStream = new PrintStream(textAreaOutputStream);
        System.setOut(textAreaPrintStream);
        System.setErr(textAreaPrintStream);

        parserContext = createParserContext();

        setTitle("EPIC");
        setSize(800, 800);
        setLocationRelativeTo(null);
    }

    private ParserContext createParserContext()
    {
        return new ParserContext();
    }

    private void createNewDocument()
    {
        RSyntaxEpicEditor editor;

        editor = new RSyntaxEpicEditor(parserContext);
        editor.addCodeCompletionHandler(new TestCodeCompletionHandler());
        //jTabbedPaneMain.add(editor, "Untitled");

        jTabbedPaneMain.add(editor, "Untitled");
    }

    private void loadDocument()
    {
        JFileChooser fileChooser;
        int returnValue;

        fileChooser = new JFileChooser();
        fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                String filePath;

                filePath = fileChooser.getSelectedFile().getCanonicalPath();
                if (filePath != null)
                {
                    RSyntaxEpicEditor editor;

                    editor = new RSyntaxEpicEditor(parserContext);
                    editor.loadFile(filePath);
                    jTabbedPaneMain.add(editor, "Untitled");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private void saveDocument()
    {

    }

    private void clearOutput()
    {
        jTextAreaOutput.setText(null);
    }

    private void doCompilationTest()
    {
        final RSyntaxEpicEditor selectedEditor;

        selectedEditor = (RSyntaxEpicEditor)jTabbedPaneMain.getSelectedComponent();
        if (selectedEditor != null)
        {
            try
            {
                Program newProgram;
                String sourceCode;
                long totalCompileTime;
                int iterationCount;

                clearOutput();

                iterationCount = 20;
                totalCompileTime = 0;
                sourceCode = selectedEditor.getSourceCode();
                for (int iterations = 0; iterations < iterationCount; iterations++)
                {
                    long startTime = System.currentTimeMillis();
                    newProgram = EPIC.compileProgram(sourceCode, "UTF-8");
                    long compileTime = System.currentTimeMillis() - startTime;
                    totalCompileTime += compileTime;
                }

                System.out.println("Iterations Compiled: " + iterationCount);
                System.out.println("Compile Time: " + totalCompileTime + "ms");
                System.out.println("Compile Time Per Iteration: " + (totalCompileTime / iterationCount) + "ms");
            }
            catch (EPICSyntaxException e)
            {
                selectedEditor.highlightLine(e.getLineNumber(), Color.RED);
                System.out.println("Syntax Error on line: " + e.getLineNumber());
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private void runProgram()
    {
        final RSyntaxEpicEditor selectedEditor;

        selectedEditor = (RSyntaxEpicEditor)jTabbedPaneMain.getSelectedComponent();
        if (selectedEditor != null)
        {
            new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        ArrayList<HashMap<String, Object>> testList;
                        HashMap<String, Object> parameters;
                        ParserContext parserContext;
                        Program newProgram;
                        String sourceCode;
                        Object testObject;

                        parserContext = new ParserContext();
                        parserContext.addExternalExpressionParser(new TestExternalExpressionParser());

                        testList = new ArrayList<HashMap<String, Object>>();
                        testList.add(new HashMap<String, Object>());

                        testObject = new TestObject();
                        parameters = new HashMap<String, Object>();
                        parameters.put("testObject", testObject);
                        parameters.put("testList", testList);
                        parameters.put("testEnum", TestEnum.TEST_ENUM_2);

                        clearOutput();

                        sourceCode = selectedEditor.getSourceCode();
                        long startTime = System.currentTimeMillis();
                        newProgram = EPIC.compileProgram(sourceCode, "UTF-8", parserContext);
                        long compileTime = System.currentTimeMillis() - startTime;
                        startTime = System.currentTimeMillis();
                        newProgram.addImport("bla", testObject, testObject.getClass().getMethod("getTestString", String.class));
                        newProgram.addImport("test", IDE.this, IDE.class.getDeclaredMethod("test", String.class));
                        newProgram.addClassImport(XmlDocument.class.getSimpleName(), XmlDocument.class);
                        newProgram.execute(parameters);
                        long executionTime = System.currentTimeMillis() - startTime;
                        System.out.println("Compile Time: " + compileTime);
                        System.out.println("Execution Time: " + executionTime);
                        System.out.println("Total Time: " + (executionTime + compileTime));
                    }
                    catch (EPICSyntaxException e)
                    {
                        selectedEditor.highlightLine(e.getLineNumber(), Color.RED);
                        System.out.println("Syntax Error on line: " + e.getLineNumber());
                        e.printStackTrace();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    private void runExpression()
    {
        final RSyntaxEpicEditor selectedEditor;

        selectedEditor = (RSyntaxEpicEditor)jTabbedPaneMain.getSelectedComponent();
        if (selectedEditor != null)
        {
            new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        ExpressionEvaluator expressionEvaluator;
                        ParserContext parserContext;
                        String sourceCode;
                        Object contextObject;
                        Object result;
                        HashMap<String, Object> parameters;
                        ArrayList<HashMap<String, Object>> testList;

                        contextObject = new TestObject();
                        expressionEvaluator = new ExpressionEvaluator();
                        parserContext = new ParserContext();
                        parserContext.addVariableDeclaration("testList");
                        parserContext.addVariableDeclaration("testEnum");
                        parserContext.addMethodImport("bla", contextObject, contextObject.getClass().getMethod("getTestString", String.class));
                        testList = new ArrayList<HashMap<String, Object>>();
                        testList.add(new HashMap<String, Object>());

                        parameters = new HashMap<String, Object>();
                        parameters.put("testList", testList);
                        parameters.put("testEnum", TestEnum.TEST_ENUM_2);

                        clearOutput();

                        sourceCode = selectedEditor.getSourceCode();
                        long startTime = System.currentTimeMillis();
                        expressionEvaluator.compileExpression(sourceCode, parserContext);
                        long compileTime = System.currentTimeMillis() - startTime;
                        startTime = System.currentTimeMillis();
                        result = expressionEvaluator.evaluateExpression(parameters, "ABC");
                        long executionTime = System.currentTimeMillis() - startTime;
                        System.out.println("Result: " + result);
                        System.out.println("Compile Time: " + compileTime);
                        System.out.println("Execution Time: " + executionTime);
                        System.out.println("Total Time: " + (executionTime + compileTime));
                    }
                    catch (EPICSyntaxException e)
                    {
                        selectedEditor.highlightLine(e.getLineNumber(), Color.RED);
                        System.out.println("Syntax Error on line: " + e.getLineNumber());
                        e.printStackTrace();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    public final String test(String value)
    {
        System.out.println("Test method int IDE class: " + value);
        return value.toUpperCase();
    }

    public static final void refreshComponent(String componentID)
    {
        return;
    }

    public static final String testExtensionMethod(String text)
    {
        return "Method converted Text = " + text;
    }

    public static class TestObject
    {
        private int testValue;
        private String testString;

        public TestObject()
        {
            testValue = 12345;
            testString = "Hello from the TestObject!";
        }

        public TestObject(int value)
        {
            testValue = value;
            testString = "Hello from the TestObject!";
        }

        public void test(String input1, String input2)
        {
            System.out.println(testString + input1 + input2);
        }

        public String getTestString(String input)
        {
            return testString + input;
        }

        public void setTestString(String testString)
        {
            this.testString = testString;
        }

        public int getTestValue()
        {
            return testValue;
        }

        public void setTestValue(int testValue)
        {
            this.testValue = testValue;
        }

        public void doTestPrintout()
        {
            System.out.println("Test printout to System.out");
        }
    }

    public enum TestEnum {TEST_ENUM_1, TEST_ENUM_2, TEST_ENUM_3};

    private class TestCodeCompletionHandler implements CodeCompletionHandler
    {
        private List<CodeCompletionResolver> resolvers;

        public TestCodeCompletionHandler()
        {
            resolvers = new ArrayList<CodeCompletionResolver>();
            resolvers.add(new CodeCompletionResolver()
            {
                @Override
                public List<String> getMatchPatterns()
                {
                    List<String> patterns;

                    patterns = new ArrayList<String>();
                    patterns.add("println");
                    return patterns;
                }

                @Override
                public Map<String, String> getCodeCompletionSuggestions(String matchedText)
                {
                    Map<String, String> suggestions;

                    suggestions = new HashMap<String, String>();
                    suggestions.put("(\"Test\")", "This is a code completion test!");
                    return suggestions;
                }
            });
        }

        @Override
        public List<CodeCompletionResolver> getResolvers()
        {
            return resolvers;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jSplitPaneMain = new javax.swing.JSplitPane();
        jTabbedPaneMain = new javax.swing.JTabbedPane();
        jPanelOutput = new javax.swing.JPanel();
        jLabelOutput = new javax.swing.JLabel();
        jScrollPaneOutput = new javax.swing.JScrollPane();
        jTextAreaOutput = new javax.swing.JTextArea();
        jMenuBarMain = new javax.swing.JMenuBar();
        jMenuFile = new javax.swing.JMenu();
        jMenuItemNew = new javax.swing.JMenuItem();
        jMenuItemLoad = new javax.swing.JMenuItem();
        jMenuItemSave = new javax.swing.JMenuItem();
        jMenuEdit = new javax.swing.JMenu();
        jMenuRun = new javax.swing.JMenu();
        jMenuItemRunProgram = new javax.swing.JMenuItem();
        jMenuItemRunExpression = new javax.swing.JMenuItem();
        jMenuTest = new javax.swing.JMenu();
        jMenuItemCompilationTest = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelContent.setLayout(new java.awt.GridBagLayout());

        jSplitPaneMain.setDividerLocation(500);
        jSplitPaneMain.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPaneMain.setLeftComponent(jTabbedPaneMain);

        jPanelOutput.setLayout(new java.awt.GridBagLayout());

        jLabelOutput.setText("Output:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelOutput.add(jLabelOutput, gridBagConstraints);

        jTextAreaOutput.setColumns(20);
        jTextAreaOutput.setRows(5);
        jScrollPaneOutput.setViewportView(jTextAreaOutput);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelOutput.add(jScrollPaneOutput, gridBagConstraints);

        jSplitPaneMain.setRightComponent(jPanelOutput);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelContent.add(jSplitPaneMain, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        getContentPane().add(jPanelContent, gridBagConstraints);

        jMenuFile.setText("File");

        jMenuItemNew.setText("New");
        jMenuItemNew.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemNewActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuItemNew);

        jMenuItemLoad.setText("Load");
        jMenuItemLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemLoadActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuItemLoad);

        jMenuItemSave.setText("Save");
        jMenuItemSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSaveActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuItemSave);

        jMenuBarMain.add(jMenuFile);

        jMenuEdit.setText("Edit");
        jMenuBarMain.add(jMenuEdit);

        jMenuRun.setText("Run");

        jMenuItemRunProgram.setText("Run as Program");
        jMenuItemRunProgram.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemRunProgramActionPerformed(evt);
            }
        });
        jMenuRun.add(jMenuItemRunProgram);

        jMenuItemRunExpression.setText("Run as Expression");
        jMenuItemRunExpression.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemRunExpressionActionPerformed(evt);
            }
        });
        jMenuRun.add(jMenuItemRunExpression);

        jMenuBarMain.add(jMenuRun);

        jMenuTest.setText("Test");

        jMenuItemCompilationTest.setText("Compilation Test");
        jMenuItemCompilationTest.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemCompilationTestActionPerformed(evt);
            }
        });
        jMenuTest.add(jMenuItemCompilationTest);

        jMenuBarMain.add(jMenuTest);

        setJMenuBar(jMenuBarMain);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemNewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemNewActionPerformed
    {//GEN-HEADEREND:event_jMenuItemNewActionPerformed
        createNewDocument();
    }//GEN-LAST:event_jMenuItemNewActionPerformed

    private void jMenuItemLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemLoadActionPerformed
    {//GEN-HEADEREND:event_jMenuItemLoadActionPerformed
        loadDocument();
    }//GEN-LAST:event_jMenuItemLoadActionPerformed

    private void jMenuItemSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSaveActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSaveActionPerformed
        saveDocument();
    }//GEN-LAST:event_jMenuItemSaveActionPerformed

    private void jMenuItemRunProgramActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemRunProgramActionPerformed
    {//GEN-HEADEREND:event_jMenuItemRunProgramActionPerformed
        runProgram();
    }//GEN-LAST:event_jMenuItemRunProgramActionPerformed

    private void jMenuItemRunExpressionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemRunExpressionActionPerformed
    {//GEN-HEADEREND:event_jMenuItemRunExpressionActionPerformed
        runExpression();
    }//GEN-LAST:event_jMenuItemRunExpressionActionPerformed

    private void jMenuItemCompilationTestActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemCompilationTestActionPerformed
    {//GEN-HEADEREND:event_jMenuItemCompilationTestActionPerformed
        doCompilationTest();
    }//GEN-LAST:event_jMenuItemCompilationTestActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                new IDE().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelOutput;
    private javax.swing.JMenuBar jMenuBarMain;
    private javax.swing.JMenu jMenuEdit;
    private javax.swing.JMenu jMenuFile;
    private javax.swing.JMenuItem jMenuItemCompilationTest;
    private javax.swing.JMenuItem jMenuItemLoad;
    private javax.swing.JMenuItem jMenuItemNew;
    private javax.swing.JMenuItem jMenuItemRunExpression;
    private javax.swing.JMenuItem jMenuItemRunProgram;
    private javax.swing.JMenuItem jMenuItemSave;
    private javax.swing.JMenu jMenuRun;
    private javax.swing.JMenu jMenuTest;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelOutput;
    private javax.swing.JScrollPane jScrollPaneOutput;
    private javax.swing.JSplitPane jSplitPaneMain;
    private javax.swing.JTabbedPane jTabbedPaneMain;
    private javax.swing.JTextArea jTextAreaOutput;
    // End of variables declaration//GEN-END:variables
}
