/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.epic.rsyntax.autocomplete;

import com.pilog.epic.annotation.EPICMethod;
import com.pilog.epic.annotation.EPICParameter;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.FunctionCompletion;
import org.fife.ui.autocomplete.ParameterizedCompletion;
import org.fife.ui.autocomplete.ParameterizedCompletion.Parameter;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class MethodCompletion
{
    private final FunctionCompletion completion;
    private final String methodName;

    public MethodCompletion(CompletionProvider completionProvider, Method method)
    {
        EPICMethod annotation = method.getAnnotation(EPICMethod.class);
        this.methodName = (annotation == null || Strings.isNullOrEmpty(annotation.Name())) ? method.getName() : annotation.Name();
        this.completion = new FunctionCompletion(completionProvider, this.methodName, method.getReturnType().getSimpleName());
        if(annotation != null)
        {
            this.completion.setSummary(annotation.Description());
            this.completion.setShortDescription(annotation.Description());
        }
        Annotation[][] methodParameterAnnotations = method.getParameterAnnotations();
        Class<?>[] parameterTypes = method.getParameterTypes();

        List<Parameter> completionParameters = new ArrayList<>();
        for (int i = 0; i < parameterTypes.length; i++)
        {
            Class<?> typeVariable = parameterTypes[i];
            String paremeterName = typeVariable.getSimpleName();
            String parameterDesc = "";
            for (Annotation paremeterAnnotation : methodParameterAnnotations[i])
            {
                if(paremeterAnnotation instanceof EPICParameter)
                {
                    paremeterName = ((EPICParameter)paremeterAnnotation).VariableName();
                    parameterDesc = ((EPICParameter)paremeterAnnotation).Description();
                }
            }
            Parameter parameter = new Parameter(typeVariable.getSimpleName(), paremeterName);
            parameter.setDescription(parameterDesc);
            completionParameters.add(parameter);
        }

        this.completion.setParams(completionParameters);
    }

    public String getName()
    {
        return this.methodName.toUpperCase();
    }

    public ParameterizedCompletion getCompletion()
    {
        return this.completion;
    }
}
