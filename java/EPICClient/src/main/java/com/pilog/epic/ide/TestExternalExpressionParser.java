package com.pilog.epic.ide;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.ExternalExpressionParser;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class TestExternalExpressionParser implements ExternalExpressionParser
{
    @Override
    public boolean isApplicableTo(String expression)
    {
        return true;
    }

    @Override
    public Expression parse(String expression)
    {
        return new ExternalExpression(expression);
    }

    private static class ExternalExpression implements Expression
    {
        private final String expression;

        public ExternalExpression(String expression)
        {
            this.expression = expression;
        }

        @Override
        public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
        {
            return "##" + expression + "##";
        }

        @Override
        public String unparse()
        {
            return "`" + expression + "`";
        }
    }
}
