package com.pilog.version;

/**
 * @author  Bouwer du Preez
 */
public class T8DeveloperClientVersion
{
    public final static String VERSION = "906";
}
