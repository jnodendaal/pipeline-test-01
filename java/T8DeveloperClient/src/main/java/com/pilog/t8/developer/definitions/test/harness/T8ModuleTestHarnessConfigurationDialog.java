package com.pilog.t8.developer.definitions.test.harness;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleTestHarnessConfigurationDialog extends javax.swing.JDialog
{
    private final T8ModuleTestHarness testHarness;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;

    public T8ModuleTestHarnessConfigurationDialog(T8Context context, T8ModuleTestHarness testHarness)
    {
        super(context.getClientContext().getParentWindow(), ModalityType.MODELESS);
        this.clientContext = context.getClientContext();
        this.context = context;
        this.testHarness = testHarness;
        this.definitionManager = clientContext.getDefinitionManager();
        initComponents();
        initializeDialog();

        this.jPanelUserConfiguration.setBorder(new T8ContentHeaderBorder("Test User Configuration"));

        setSize(500, 220);
        setLocationRelativeTo(null);
    }

    private void initializeDialog()
    {
        try
        {
            List<String> userIds;

            // Add a null value.
            jComboBoxUserIdentifier.addItem(null);

            // Add all users to the combo box.
            userIds = definitionManager.getGroupDefinitionIdentifiers(null, T8UserDefinition.GROUP_IDENTIFIER);
            for (String userIdentifier : userIds)
            {
                jComboBoxUserIdentifier.addItem(userIdentifier);
                if(userIdentifier.equals(context.getUserId()))
                {
                    jComboBoxUserIdentifier.setSelectedItem(userIdentifier);
                    refreshUserProfiles();
                }

            }

            // Add an item listener.
            jComboBoxUserIdentifier.addItemListener(new UserItemListener());
        }
        catch (Exception e)
        {
            T8Log.log("Exception while initializing module test harness.", e);
            JOptionPane.showMessageDialog(rootPane, "An error prevented successful initialization of this module.", "Initialization Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void refreshUserProfiles()
    {
        String selectedUserIdentifier;

        jComboBoxUserProfileIdentifier.removeAllItems();
        selectedUserIdentifier = (String)jComboBoxUserIdentifier.getSelectedItem();
        if (selectedUserIdentifier != null)
        {
            try
            {
                T8UserDefinition userDefinition;

                userDefinition = (T8UserDefinition)definitionManager.getInitializedDefinition(context, null, selectedUserIdentifier, null);
                if (userDefinition != null)
                {
                    List<String> userProfileIdentifiers;

                    userProfileIdentifiers = userDefinition.getProfileIdentifiers();
                    if (userProfileIdentifiers != null)
                    {
                        for (String userProfileIdentifier : userProfileIdentifiers)
                        {
                            jComboBoxUserProfileIdentifier.addItem(userProfileIdentifier);
                        }
                    }
                }
                jComboBoxUserProfileIdentifier.setSelectedIndex(0);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving user definition: " + selectedUserIdentifier, e);
                JOptionPane.showMessageDialog(rootPane, "An error prevent retrieval of user definition.", "Definition Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void applyConfiguration()
    {
        testHarness.setUserIdentifier((String)jComboBoxUserIdentifier.getSelectedItem());
        testHarness.setUserProfileIdentifier((String)jComboBoxUserProfileIdentifier.getSelectedItem());
        testHarness.setUsername(jTextFieldUsername.getText());
        testHarness.setUserPassword(jPasswordFieldUserPassword.getPassword());
        this.dispose();
    }

    private void cancelConfiguration()
    {
        this.dispose();
    }

    public static void configureTestHarness(T8Context context, T8ModuleTestHarness testHarness)
    {
        T8ModuleTestHarnessConfigurationDialog dialog;

        dialog = new T8ModuleTestHarnessConfigurationDialog(context, testHarness);
        dialog.setVisible(true);
    }

    private class UserItemListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                refreshUserProfiles();
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelUserConfiguration = new javax.swing.JPanel();
        jLabelUser = new javax.swing.JLabel();
        jComboBoxUserIdentifier = new javax.swing.JComboBox();
        jLabelUserProfile = new javax.swing.JLabel();
        jComboBoxUserProfileIdentifier = new javax.swing.JComboBox();
        jLabelUsername = new javax.swing.JLabel();
        jTextFieldUsername = new javax.swing.JTextField();
        jLabelPassword = new javax.swing.JLabel();
        jPasswordFieldUserPassword = new javax.swing.JPasswordField();
        jPanelControls = new javax.swing.JPanel();
        jButtonCancel = new javax.swing.JButton();
        jButtonApply = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelUserConfiguration.setLayout(new java.awt.GridBagLayout());

        jLabelUser.setText("User:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelUserConfiguration.add(jLabelUser, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelUserConfiguration.add(jComboBoxUserIdentifier, gridBagConstraints);

        jLabelUserProfile.setText("User Profile:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelUserConfiguration.add(jLabelUserProfile, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelUserConfiguration.add(jComboBoxUserProfileIdentifier, gridBagConstraints);

        jLabelUsername.setText("Username:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelUserConfiguration.add(jLabelUsername, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelUserConfiguration.add(jTextFieldUsername, gridBagConstraints);

        jLabelPassword.setText("Password:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelUserConfiguration.add(jLabelPassword, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelUserConfiguration.add(jPasswordFieldUserPassword, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        getContentPane().add(jPanelUserConfiguration, gridBagConstraints);

        jPanelControls.setLayout(new java.awt.GridBagLayout());

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        jPanelControls.add(jButtonCancel, gridBagConstraints);

        jButtonApply.setText("Apply");
        jButtonApply.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonApplyActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.1;
        jPanelControls.add(jButtonApply, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        getContentPane().add(jPanelControls, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonApplyActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonApplyActionPerformed
    {//GEN-HEADEREND:event_jButtonApplyActionPerformed
        applyConfiguration();
    }//GEN-LAST:event_jButtonApplyActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCancelActionPerformed
    {//GEN-HEADEREND:event_jButtonCancelActionPerformed
        cancelConfiguration();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonApply;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JComboBox jComboBoxUserIdentifier;
    private javax.swing.JComboBox jComboBoxUserProfileIdentifier;
    private javax.swing.JLabel jLabelPassword;
    private javax.swing.JLabel jLabelUser;
    private javax.swing.JLabel jLabelUserProfile;
    private javax.swing.JLabel jLabelUsername;
    private javax.swing.JPanel jPanelControls;
    private javax.swing.JPanel jPanelUserConfiguration;
    private javax.swing.JPasswordField jPasswordFieldUserPassword;
    private javax.swing.JTextField jTextFieldUsername;
    // End of variables declaration//GEN-END:variables
}
