package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author Bouwer du Preez
 */
public class BooleanDatumEditor extends T8DefaultDefinitionDatumEditor
{
    public BooleanDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        jCheckBoxValue.addItemListener(new SelectionChangeListener());
    }
    
    @Override
    public void initializeComponent()
    {
    }
    
    @Override
    public void startComponent()
    {
        refreshEditor();
    }
    
    @Override
    public void stopComponent()
    {
    }
    
    @Override
    public void commitChanges()
    {
        setDefinitionDatum(jCheckBoxValue.isSelected());
    }
    
    @Override
    public void refreshEditor()
    {
        Object value;
        
        value = getDefinitionDatum();
        jCheckBoxValue.setSelected(value != null ? (Boolean)value : false);
    }
    
    @Override
    public void setEditable(boolean editable)
    {
        jCheckBoxValue.setEnabled(editable);
    }
    
    private class SelectionChangeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                commitChanges();
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBoxValue = new javax.swing.JCheckBox();

        setLayout(new java.awt.BorderLayout());
        add(jCheckBoxValue, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox jCheckBoxValue;
    // End of variables declaration//GEN-END:variables
}
