package com.pilog.t8.developer.flow.monitor;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.developer.flow.monitor.core.T8StateFlowNodePanel;
import com.pilog.t8.developer.flow.monitor.core.T8StateFlowPanel;
import com.pilog.t8.developer.flow.monitor.core.T8StateFlowTaskPanel;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8FlowManager.T8TaskListRefreshType;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.table.T8TableAPIHandler;
import com.pilog.t8.developer.flow.monitor.flow.T8StateFlowDetailPanel;
import com.pilog.t8.developer.flow.monitor.node.T8StateFlowNodeDetailPanel;
import com.pilog.t8.developer.flow.monitor.task.T8StateFlowTaskDetailPanel;
import com.pilog.t8.developer.view.T8DefaultDeveloperView;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 * @author Bouwer du Preez
 */
public class T8FlowMonitor extends T8DefaultDeveloperView implements T8DeveloperView
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8FlowMonitor.class);

    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DeveloperView parentView;
    private T8DeveloperViewFactory viewFactory;
    private final T8FlowManager flowManager;
    private final T8ComponentController mainComponentController;
    private final T8ComponentController controller;
    private final T8StateFlowPanel stateFlowPanel;
    private final T8StateFlowNodePanel stateFlowNodePanel;
    private final T8StateFlowTaskPanel stateFlowTaskPanel;
    private final T8StateFlowDetailPanel stateFlowDetailPanel;
    private final T8StateFlowNodeDetailPanel stateFlowNodeDetailPanel;
    private final T8StateFlowTaskDetailPanel stateFlowTaskDetailPanel;
    private final List<String> selectedFlowIids;

    {
        this.selectedFlowIids = new ArrayList<>();
    }

    public T8FlowMonitor(T8Context context, T8DeveloperView parentView) throws Exception
    {
        ComponentEventListener eventListener;
        initComponents();

        this.clientContext = context.getClientContext();
        this.context = context;
        this.parentView = parentView;

        eventListener = new ComponentEventListener();
        this.mainComponentController = new T8DefaultComponentController(context, "FLOW_MONITOR", false);
        this.mainComponentController.addComponentEventListener(new ComponentSelectedFlowEventListener());
        this.mainComponentController.addComponentEventListener(eventListener);

        this.controller = new T8DefaultComponentController(context, "FLOW_MONITOR", false);
        this.controller.addComponentEventListener(eventListener);

        this.viewFactory = parentView.getViewFactory();
        this.flowManager = clientContext.getFlowManager();

        this.stateFlowPanel = new T8StateFlowPanel(this.mainComponentController);
        this.stateFlowNodePanel = new T8StateFlowNodePanel(controller);
        this.stateFlowTaskPanel = new T8StateFlowTaskPanel(controller);

        this.stateFlowDetailPanel = new T8StateFlowDetailPanel(controller);
        this.stateFlowNodeDetailPanel = new T8StateFlowNodeDetailPanel(controller);
        this.stateFlowTaskDetailPanel = new T8StateFlowTaskDetailPanel(controller);

        this.jSplitPaneFlowNode.setLeftComponent(stateFlowPanel);
        this.jSplitPaneNodeTask.setLeftComponent(stateFlowNodePanel);
        this.jSplitPaneNodeTask.setRightComponent(stateFlowTaskPanel);

        jTabbedPaneStateFlowDetail.addTab(getTranslatedString("Flow Detail"), stateFlowDetailPanel);
        jTabbedPaneStateFlowDetail.addTab(getTranslatedString("Node Detail"), stateFlowNodeDetailPanel);
        jTabbedPaneStateFlowDetail.addTab(getTranslatedString("Task Detail"), stateFlowTaskDetailPanel);
    }

    @Override
    public String getHeader()
    {
        return "Flow Monitor";
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        parentView.addView(view);
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        parentView.removeView(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    private void refreshData()
    {
        stateFlowPanel.getTable().refreshData();
    }

    private void killFlows()
    {
        boolean allFlowsStopped;
        boolean noFlowsStopped;
        int selectedOption;

        allFlowsStopped = true;
        noFlowsStopped = true;

        if (this.selectedFlowIids.isEmpty())
        {
            JOptionPane.showMessageDialog(this, getTranslatedString("No flows selected to be stopped."), getTranslatedString("No Flows"), JOptionPane.WARNING_MESSAGE);
            return;
        }
        else
        {
            // We show an initial confirmation
            selectedOption = JOptionPane.showConfirmDialog(this, String.format(getTranslatedString("You are about to kill %d flows. Are you sure you want to continue?"), selectedFlowIids.size()), getTranslatedString("Kill Flows Confirm"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (selectedOption != JOptionPane.YES_OPTION) return;
        }

        // Now we can process the killing of the flows
        for (String selectedFlowIid : selectedFlowIids)
        {
            try
            {
                LOGGER.log("Killing Flow : " + selectedFlowIid);
                this.flowManager.killFlow(context, selectedFlowIid);
                noFlowsStopped = false;
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to stop flow (" + selectedFlowIid + ") ", ex);
                allFlowsStopped = false;
            }
        }

        if (noFlowsStopped) JOptionPane.showMessageDialog(this, getTranslatedString("None of the selected flows could be stopped correctly. Please contact your system administrator."), getTranslatedString("Flow Stop Error"), JOptionPane.ERROR_MESSAGE);
        else if (allFlowsStopped) JOptionPane.showMessageDialog(this, getTranslatedString("Kill signal successfully sent for all selected flows."), getTranslatedString("Success"), JOptionPane.INFORMATION_MESSAGE);
        else JOptionPane.showMessageDialog(this, getTranslatedString("Some selected flows were successfully stopped. Please contact the system administrator for those that could not be stopped."), getTranslatedString("Flow Stop"), JOptionPane.WARNING_MESSAGE);

        refreshData();
    }

    private void refreshTaskList()
    {
        if (this.selectedFlowIids.isEmpty())
        {
            JOptionPane.showMessageDialog(this, getTranslatedString("No flows selected for which to refresh the task list."), getTranslatedString("No Flows Selected"), JOptionPane.WARNING_MESSAGE);
        }
        else
        {
            try
            {
                flowManager.refreshTaskList(context, T8TaskListRefreshType.FLOW_INSTANCE, selectedFlowIids);
                Toast.show("Task List Refreshed Successfully", Toast.Style.SUCCESS);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while refreshing task list for flows:" + selectedFlowIids, e);
                Toast.show("Exception During Task List Refresh", Toast.Style.ERROR);
            }
        }
    }

    private void recacheTaskEscalationTriggers()
    {
        try
        {
            flowManager.recacheTaskEscalationTriggers(context);
            Toast.show("Escalation Triggers Re-cached Successfully", Toast.Style.SUCCESS);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while recaching task escalation triggers.", e);
            Toast.show("Exception during operation.", Toast.Style.ERROR);
        }
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    @Override
    public void setSelectedDefinition(T8Definition selectedDefinition)
    {
    }

    @Override
    public void addDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    @Override
    public void removeDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    private class ComponentSelectedFlowEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            if (!event.getEventDefinition().getIdentifier().equals(T8TableAPIHandler.EVENT_ROW_SELECTION_CHANGED)) return;
            Map<String, Object> eventParameters;
            List<T8DataEntity> dataEntities;

            eventParameters = event.getEventParameters();
            dataEntities = eventParameters != null ? (List<T8DataEntity>)eventParameters.get(T8FlowMonitorResources.P_DATA_ENTITY_LIST) : null;

            if (dataEntities != null)
            {
                selectedFlowIids.clear();
                for (T8DataEntity dataEntity : dataEntities)
                {
                    LOGGER.log("Setting Last Selected Flow IID : " + dataEntity.getFieldValue(dataEntity.getIdentifier() + T8FlowMonitorResources.EF_FLOW_IID));
                    selectedFlowIids.add((String)dataEntity.getFieldValue(dataEntity.getIdentifier() + T8FlowMonitorResources.EF_FLOW_IID));
                }
            }
        }
    }

    private class ComponentEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;
            List<T8DataEntity> dataEntities;
            T8DataFilter dataFilter = null;
            Map<String, Object> eventParameters;
            T8Component component;

            eventDefinition = event.getEventDefinition();
            component = event.getComponent();
            eventParameters = event.getEventParameters();

            // Get the selected entities from the event parameters.
            dataEntities = eventParameters != null ? (List<T8DataEntity>)eventParameters.get(T8FlowMonitorResources.P_DATA_ENTITY_LIST) : null;
            if (dataEntities != null && !dataEntities.isEmpty())
            {
                //We only care about the first selected entity if multiples are selected
                T8DataEntity dataEntity = dataEntities.get(0);

                dataFilter = new T8DataFilter(dataEntity.getIdentifier());
                dataFilter.addFilterCriteria(HashMaps.newHashMap(dataEntity.getIdentifier() + T8FlowMonitorResources.EF_FLOW_IID, dataEntity.getFieldValue(dataEntity.getIdentifier() + T8FlowMonitorResources.EF_FLOW_IID)));
                if (eventDefinition.getIdentifier().equals(T8TableAPIHandler.EVENT_ROW_SELECTION_CHANGED) && component.getComponentDefinition().getIdentifier().equals(T8StateFlowNodePanel.TABLE_DEF_ID))
                {
                    dataFilter.addFilterCriteria(HashMaps.newHashMap(stateFlowNodePanel.getDataEntityIdentifier() + T8FlowMonitorResources.EF_NODE_IID, dataEntity.getFieldValue(dataEntity.getIdentifier() + T8FlowMonitorResources.EF_NODE_IID)));
                }
                else if (eventDefinition.getIdentifier().equals(T8TableAPIHandler.EVENT_ROW_SELECTION_CHANGED) && component.getComponentDefinition().getIdentifier().equals(T8StateFlowTaskPanel.TABLE_DEF_ID))
                {
                    dataFilter.addFilterCriteria(HashMaps.newHashMap(stateFlowTaskPanel.getDataEntityIdentifier() + T8FlowMonitorResources.EF_TASK_IID, dataEntity.getFieldValue(dataEntity.getIdentifier() + T8FlowMonitorResources.EF_TASK_IID)));
                }
            }
            else if (stateFlowPanel != null && stateFlowPanel.getTable().getDataEntityCount() == 0)
            {
                if (eventDefinition.getIdentifier().equals(T8TableAPIHandler.EVENT_ROW_SELECTION_CHANGED))
                {
                    dataFilter = new T8DataFilter(stateFlowPanel.getDataEntityIdentifier());
                    dataFilter.addFilterCriteria(HashMaps.newHashMap(stateFlowPanel.getDataEntityIdentifier()+ T8FlowMonitorResources.EF_FLOW_IID, null));
                }
            }

            //State Flow Panel Selection Change event
            if (eventDefinition.getIdentifier().equals(T8TableAPIHandler.EVENT_ROW_SELECTION_CHANGED) && component.getComponentDefinition().getIdentifier().equals(T8StateFlowPanel.TABLE_DEF_ID))
            {
                if (dataFilter != null)
                {
                    stateFlowNodePanel.getTable().setUserDefinedFilter(dataFilter);
                    stateFlowNodePanel.getTable().refreshData();

                    stateFlowDetailPanel.getFlowInPanel().getTable().setUserDefinedFilter(dataFilter);
                    stateFlowDetailPanel.getFlowWaitKeyPanel().getTable().setUserDefinedFilter(dataFilter);
                    stateFlowDetailPanel.getFlowInPanel().getTable().refreshData();
                    stateFlowDetailPanel.getFlowWaitKeyPanel().getTable().refreshData();
                }
            }

            //State Flow Panel Node Selection Change event
            if (eventDefinition.getIdentifier().equals(T8TableAPIHandler.EVENT_ROW_SELECTION_CHANGED) && component.getComponentDefinition().getIdentifier().equals(T8StateFlowNodePanel.TABLE_DEF_ID))
            {
                if (dataFilter != null)
                {
                    stateFlowTaskPanel.getTable().setUserDefinedFilter(dataFilter);
                    stateFlowTaskPanel.getTable().refreshData();

                    stateFlowNodeDetailPanel.getFlowNodeInPanel().getTable().setUserDefinedFilter(dataFilter);
                    stateFlowNodeDetailPanel.getFlowNodeOutPanel().getTable().setUserDefinedFilter(dataFilter);
                    stateFlowNodeDetailPanel.getFlowNodeExecPanel().getTable().setUserDefinedFilter(dataFilter);
                    stateFlowNodeDetailPanel.getFlowNodeOutNodesPanel().getTable().setUserDefinedFilter(dataFilter);
                    stateFlowNodeDetailPanel.getFlowNodeInPanel().getTable().refreshData();
                    stateFlowNodeDetailPanel.getFlowNodeOutPanel().getTable().refreshData();
                    stateFlowNodeDetailPanel.getFlowNodeExecPanel().getTable().refreshData();
                    stateFlowNodeDetailPanel.getFlowNodeOutNodesPanel().getTable().refreshData();
                }
            }

            //State Flow Panel Task Selection Change event
            if (eventDefinition.getIdentifier().equals(T8TableAPIHandler.EVENT_ROW_SELECTION_CHANGED) && component.getComponentDefinition().getIdentifier().equals(T8StateFlowTaskPanel.TABLE_DEF_ID))
            {
                if(eventParameters.get(T8FlowMonitorResources.P_DATA_ENTITY_LIST) != null && !((List<T8DataEntity>) eventParameters.get(T8FlowMonitorResources.P_DATA_ENTITY_LIST)).isEmpty())
                {
                    stateFlowTaskDetailPanel.getFlowTaskInPanel().getTable().setUserDefinedFilter(dataFilter);
                    stateFlowTaskDetailPanel.getFlowTaskInPanel().getTable().refreshData();
                }
                else
                {
                    Map<String, Object> filterMap;

                    filterMap = HashMaps.create(stateFlowTaskDetailPanel.getFlowTaskInPanel().getDataEntityIdentifier() + T8FlowMonitorResources.EF_TASK_IID, null);
                    stateFlowTaskDetailPanel.getFlowTaskInPanel().getTable().setUserDefinedFilter(new T8DataFilter(stateFlowTaskDetailPanel.getFlowTaskInPanel().getDataEntityIdentifier(), filterMap));
                    stateFlowTaskDetailPanel.getFlowTaskInPanel().getTable().refreshData();
                }
            }
        }
    }


    private String getTranslatedString(String text)
    {
        return clientContext.getConfigurationManager().getUITranslation(this.context, text);
    }

    private void findActiveNodes() throws Exception
    {
        List<String> activeNodes;
        T8DataEntity dataEntityFlow;
        T8DataEntity dataEntityNode;

        dataEntityFlow = stateFlowPanel.getTable().getSelectedDataEntity();
        dataEntityNode = stateFlowNodePanel.getTable().getSelectedDataEntity();

        activeNodes = flowManager.findActiveInputNodeInstanceIdentifiers(context, (String) dataEntityFlow.getFieldValue(dataEntityFlow.getIdentifier() + T8FlowMonitorResources.EF_FLOW_IID), (String) dataEntityNode.getFieldValue(dataEntityNode.getIdentifier() + T8FlowMonitorResources.EF_NODE_IID));
        JOptionPane.showMessageDialog(this, activeNodes, "Active Input Node Instances", JOptionPane.INFORMATION_MESSAGE);
    }

    private void retryNodeExecution()
    {
        T8DataEntity dataEntityNode;

        dataEntityNode = stateFlowNodePanel.getTable().getSelectedDataEntity();
        if (dataEntityNode != null)
        {
            String nodeIID;
            String flowIID;

            flowIID = (String)dataEntityNode.getFieldValue(T8FlowMonitorResources.EF_FLOW_IID);
            nodeIID = (String)dataEntityNode.getFieldValue(T8FlowMonitorResources.EF_NODE_IID);
            flowManager.retryExecution(context, flowIID, nodeIID);
            JOptionPane.showMessageDialog(this, "Node Execution Retried.", "Operation Result", JOptionPane.INFORMATION_MESSAGE);
        }
        else JOptionPane.showMessageDialog(this, "Please select a valid node to retry.", "Invalid Selection", JOptionPane.ERROR_MESSAGE);
    }

    private void clearIdleCachedFlows()
    {
        Integer flowsCleared;

        flowsCleared = flowManager.clearFlowCache(context);

        if (flowsCleared > 0) JOptionPane.showMessageDialog(this, flowsCleared + " Idling Flows Cache Cleard.", "Success", JOptionPane.INFORMATION_MESSAGE);
        else JOptionPane.showMessageDialog(this, "Something went wrong while clearing the Idling Flows Cache.", "Failure", JOptionPane.ERROR_MESSAGE);
    }

    private void signalFlows()
    {
        T8FlowSignalFrame.showFlowSignalFrame(context);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jButtonKillFlow = new javax.swing.JButton();
        jButtonClearCachedFlows = new javax.swing.JButton();
        btnFindActiveNodes = new javax.swing.JButton();
        jButtonRetryExecution = new javax.swing.JButton();
        jButtonSignal = new javax.swing.JButton();
        jButtonRefreshTaskList = new javax.swing.JButton();
        jButtonRecacheTaskEscalationTriggers = new javax.swing.JButton();
        jSplitPaneMain = new javax.swing.JSplitPane();
        jSplitPaneFlowNode = new javax.swing.JSplitPane();
        jSplitPaneNodeTask = new javax.swing.JSplitPane();
        jTabbedPaneStateFlowDetail = new javax.swing.JTabbedPane();

        setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setToolTipText("Refresh Flow Monitor");
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        jButtonKillFlow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/skullIcon.png"))); // NOI18N
        jButtonKillFlow.setToolTipText("Kill Selected Flows");
        jButtonKillFlow.setFocusable(false);
        jButtonKillFlow.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonKillFlow.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonKillFlow.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonKillFlow.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonKillFlowActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonKillFlow);

        jButtonClearCachedFlows.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/broomIcon.png"))); // NOI18N
        jButtonClearCachedFlows.setToolTipText("Clear Idle Cached Flows");
        jButtonClearCachedFlows.setFocusable(false);
        jButtonClearCachedFlows.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonClearCachedFlows.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonClearCachedFlows.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonClearCachedFlowsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonClearCachedFlows);

        btnFindActiveNodes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/nodeSelectChildIcon.png"))); // NOI18N
        btnFindActiveNodes.setToolTipText("Find Active Nodes");
        btnFindActiveNodes.setFocusable(false);
        btnFindActiveNodes.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFindActiveNodes.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFindActiveNodes.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnFindActiveNodesActionPerformed(evt);
            }
        });
        jToolBarMain.add(btnFindActiveNodes);

        jButtonRetryExecution.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/retryIcon.png"))); // NOI18N
        jButtonRetryExecution.setToolTipText("Retry Node Execution");
        jButtonRetryExecution.setFocusable(false);
        jButtonRetryExecution.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRetryExecution.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRetryExecution.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRetryExecutionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRetryExecution);

        jButtonSignal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/signalFlowsIcon.png"))); // NOI18N
        jButtonSignal.setToolTipText("Send Flow Signal");
        jButtonSignal.setFocusable(false);
        jButtonSignal.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSignal.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSignal.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSignalActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSignal);

        jButtonRefreshTaskList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/taskListIcon.png"))); // NOI18N
        jButtonRefreshTaskList.setToolTipText("Refresh Selected Flow Task Lists");
        jButtonRefreshTaskList.setFocusable(false);
        jButtonRefreshTaskList.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefreshTaskList.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefreshTaskList.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshTaskListActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefreshTaskList);

        jButtonRecacheTaskEscalationTriggers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/clockPlusIcon.png"))); // NOI18N
        jButtonRecacheTaskEscalationTriggers.setToolTipText("Recache Task Escalation Triggers");
        jButtonRecacheTaskEscalationTriggers.setFocusable(false);
        jButtonRecacheTaskEscalationTriggers.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRecacheTaskEscalationTriggers.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRecacheTaskEscalationTriggers.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRecacheTaskEscalationTriggersActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRecacheTaskEscalationTriggers);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarMain, gridBagConstraints);

        jSplitPaneMain.setResizeWeight(0.5);
        jSplitPaneMain.setToolTipText("");

        jSplitPaneFlowNode.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jSplitPaneNodeTask.setDividerLocation(300);
        jSplitPaneNodeTask.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPaneFlowNode.setBottomComponent(jSplitPaneNodeTask);

        jSplitPaneMain.setLeftComponent(jSplitPaneFlowNode);
        jSplitPaneMain.setRightComponent(jTabbedPaneStateFlowDetail);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jSplitPaneMain, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        refreshData();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jButtonKillFlowActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonKillFlowActionPerformed
    {//GEN-HEADEREND:event_jButtonKillFlowActionPerformed
        killFlows();
    }//GEN-LAST:event_jButtonKillFlowActionPerformed

    private void btnFindActiveNodesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnFindActiveNodesActionPerformed
    {//GEN-HEADEREND:event_btnFindActiveNodesActionPerformed
        try
        {
            findActiveNodes();
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, getTranslatedString(ex.getMessage()), getTranslatedString("Select Active Node Exception"), JOptionPane.ERROR_MESSAGE);
            LOGGER.log("Error attempting to find active nodes", ex);
        }
    }//GEN-LAST:event_btnFindActiveNodesActionPerformed

    private void jButtonRetryExecutionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRetryExecutionActionPerformed
    {//GEN-HEADEREND:event_jButtonRetryExecutionActionPerformed
        retryNodeExecution();
    }//GEN-LAST:event_jButtonRetryExecutionActionPerformed

    private void jButtonClearCachedFlowsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonClearCachedFlowsActionPerformed
    {//GEN-HEADEREND:event_jButtonClearCachedFlowsActionPerformed
        clearIdleCachedFlows();
    }//GEN-LAST:event_jButtonClearCachedFlowsActionPerformed

    private void jButtonSignalActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSignalActionPerformed
    {//GEN-HEADEREND:event_jButtonSignalActionPerformed
        signalFlows();
    }//GEN-LAST:event_jButtonSignalActionPerformed

    private void jButtonRefreshTaskListActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshTaskListActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshTaskListActionPerformed
        refreshTaskList();
    }//GEN-LAST:event_jButtonRefreshTaskListActionPerformed

    private void jButtonRecacheTaskEscalationTriggersActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRecacheTaskEscalationTriggersActionPerformed
    {//GEN-HEADEREND:event_jButtonRecacheTaskEscalationTriggersActionPerformed
        recacheTaskEscalationTriggers();
    }//GEN-LAST:event_jButtonRecacheTaskEscalationTriggersActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFindActiveNodes;
    private javax.swing.JButton jButtonClearCachedFlows;
    private javax.swing.JButton jButtonKillFlow;
    private javax.swing.JButton jButtonRecacheTaskEscalationTriggers;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JButton jButtonRefreshTaskList;
    private javax.swing.JButton jButtonRetryExecution;
    private javax.swing.JButton jButtonSignal;
    private javax.swing.JSplitPane jSplitPaneFlowNode;
    private javax.swing.JSplitPane jSplitPaneMain;
    private javax.swing.JSplitPane jSplitPaneNodeTask;
    private javax.swing.JTabbedPane jTabbedPaneStateFlowDetail;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
