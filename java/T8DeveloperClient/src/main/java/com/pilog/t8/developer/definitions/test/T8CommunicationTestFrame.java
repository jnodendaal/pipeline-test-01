package com.pilog.t8.developer.definitions.test;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.definition.communication.T8CommunicationDefinition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.developer.definitions.test.harness.T8CommunicationTestHarness;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8UserLoginResponse;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Dimension;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class T8CommunicationTestFrame extends javax.swing.JFrame
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8Context testContext;
    private final T8SecurityManager securityManager;
    private final T8CommunicationDefinition communicationDefinition;
    private final Preferences preferences;
    private final T8DefaultComponentContainer container;
    private final boolean testSessionCreated;

    public T8CommunicationTestFrame(T8Context context, T8CommunicationDefinition communicationDefinition, T8CommunicationTestHarness testHarness) throws Exception
    {
        initComponents();
        this.clientContext = context.getClientContext();
        this.context = context;
        this.communicationDefinition = communicationDefinition;
        this.preferences = Preferences.userNodeForPackage(T8CommunicationTestFrame.class);
        this.securityManager = clientContext.getSecurityManager();

        // If a user is defined by the test harness, create a new session context for the test.
        if ((testHarness != null) && (testHarness.getUserIdentifier() != null))
        {
            T8UserLoginResponse loginResponse;

            this.testContext = new T8Context(clientContext, securityManager.createNewSessionContext());
            T8Log.log("Loggin in as test user: " + testHarness.getUserIdentifier());
            loginResponse = securityManager.login(testContext, T8SecurityManager.LoginLocation.DEVELOPER, testHarness.getUserIdentifier(), testHarness.getUserPassword(), false);
            if (loginResponse.getResponseType().isSessionAuthenticated())
            {
                this.securityManager.switchUserProfile(testContext, testHarness.getUserProfileIdentifier());
                this.testSessionCreated = true;
            }
            else
            {
                Toast.show("Failed to login with user " + testHarness.getUserIdentifier(), Toast.Style.ERROR);
                this.testSessionCreated = false;
            }
        }
        else
        {
            this.testContext = new T8Context(context);
            this.testSessionCreated = false;
        }

        // Set the project context for the test.
        testContext.setProjectId(communicationDefinition.getRootProjectId());

        if (clientContext.getParentWindow() != null)
        {
            Dimension parentDimension = clientContext.getParentWindow().getSize();
            parentDimension.height = (int)(parentDimension.height * 0.80);
            parentDimension.width = (int)(parentDimension.width * 0.80);
            setPreferredSize(new Dimension(parentDimension.width, parentDimension.height));
        }
        else
        {
            setPreferredSize(new Dimension(600, 600));
        }

        setTitle("Operation Definition Test: " + communicationDefinition.getIdentifier());
        pack();
        setLocationRelativeTo(clientContext.getParentWindow());

        // Set input parameters.
        setInputParameters(communicationDefinition.getInputParameters());

        container = new T8DefaultComponentContainer(jTabbedPaneContent);
        setContentPane(container);

        if(!communicationDefinition.isLocked()) Toast.show("Definition not saved", Toast.Style.NORMAL, 5000);
    }

    private void sendCommunication()
    {
        container.setLocked(true);
        try
        {
            Map<String, Object> inputParameters;

            inputParameters = evaluateInputParameters();
            clientContext.getCommunicationManager().sendCommunication(testContext, communicationDefinition.getIdentifier(), inputParameters);
            Toast.show("Communciation sent", Toast.Style.SUCCESS);
        }
        catch (Exception ex)
        {
            PrintWriter writer;
            StringWriter stringWriter;

            T8Log.log("Failed to send Communciation", ex);
            jEditorPane1.setText(null);
            stringWriter = new StringWriter();
            writer = new PrintWriter(stringWriter);
            ex.printStackTrace(writer);
            jEditorPane1.setText(stringWriter.toString());
            jEditorPane1.setCaretPosition(0);
            writer.close();
            jTabbedPaneContent.setSelectedIndex(2);
        }
        container.setLocked(false);
    }

    private Map<String, Object> evaluateInputParameters()
    {
        T8ClientExpressionEvaluator expressionEvaluator;
        Map<String, Object> inputParameters;
        Map<String, String> inputParameterExpressions;
        TableModel model;

        // Stop editing.
        if (jTableInputParameters.getCellEditor() != null) jTableInputParameters.getCellEditor().stopCellEditing();

        model = jTableInputParameters.getModel();
        expressionEvaluator = new T8ClientExpressionEvaluator(context);
        inputParameters = new HashMap<String, Object>();
        inputParameterExpressions = new HashMap<String, String>();
        for (int rowIndex = 0; rowIndex < model.getRowCount(); rowIndex++)
        {
            String parameterIdentifier;
            String valueExpression;
            Object value;

            parameterIdentifier = (String)model.getValueAt(rowIndex, 0);
            valueExpression = (String)model.getValueAt(rowIndex, 1);

            if (Strings.isNullOrEmpty(valueExpression))
            {
                value = null;
            }
            else
            {
                try
                {
                    value = expressionEvaluator.evaluateExpression(valueExpression, null, null);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while evaluating value expression for task input parameter: " + parameterIdentifier, e);
                    value = null;
                }
            }

            inputParameterExpressions.put(parameterIdentifier, valueExpression);
            inputParameters.put(communicationDefinition.getIdentifier() + parameterIdentifier, value);
        }

        // Update the preferences.
        setInputParameterPreferences(inputParameterExpressions);
        return inputParameters;
    }

    private void setInputParameterPreferences(Map<String, String> inputParameterExpressions)
    {
        StringBuffer parameterString;

        parameterString = new StringBuffer();
        if (inputParameterExpressions != null)
        {
            for (String parameterIdentifier : inputParameterExpressions.keySet())
            {
                String expression;

                expression = inputParameterExpressions.get(parameterIdentifier);

                parameterString.append(parameterIdentifier);
                parameterString.append("```");
                parameterString.append(Strings.isNullOrEmpty(expression) ? "" : expression);
                parameterString.append("```");
            }
        }

        preferences.put(communicationDefinition.getIdentifier(), parameterString.toString());
    }

    private Map<String, String> getInputParameterPreferences()
    {
        String parameterString;

        parameterString = preferences.get(communicationDefinition.getIdentifier(), null);
        if (!Strings.isNullOrEmpty(parameterString))
        {
            try
            {
                Map<String, String> parameterPreferences;
                String[] splits;

                splits = parameterString.split("```");
                parameterPreferences = new HashMap<String, String>();
                for (int index = 0; index < splits.length -1; index += 2)
                {
                    String parameterIdentifier;
                    String expression;

                    parameterIdentifier = splits[index];
                    expression = splits[index+1];
                    parameterPreferences.put(parameterIdentifier, expression);
                }

                return parameterPreferences;
            }
            catch (Exception e)
            {
                T8Log.log("Exception while parsing input parameter preferences.", e);
                return null;
            }
        }
        else return null;
    }

    private void setInputParameters(List<T8DataParameterDefinition> parameterDefinitions)
    {
        DefaultTableModel model;
        Map<String, String> inputParameterPreferences;

        // Get any previously set preferences.
        inputParameterPreferences = getInputParameterPreferences();

        // Get the model to update.
        model = (DefaultTableModel)jTableInputParameters.getModel();

        // Clear the table model.
        while (model.getRowCount() > 0)
        {
            model.removeRow(0);
        }

        // Add all input parameters to the model.
        for (T8DataParameterDefinition parameterDefinition :  parameterDefinitions)
        {
            Object[] rowData;

            rowData = new Object[3];
            rowData[0] = parameterDefinition.getIdentifier();
            rowData[1] = inputParameterPreferences != null ? inputParameterPreferences.get(parameterDefinition.getIdentifier()) : null;
            model.addRow(rowData);
        }
    }

    public static final void testOperationDefinition(T8Context context, T8CommunicationDefinition communicationDefinition, T8CommunicationTestHarness testHarness) throws Exception
    {
        T8CommunicationTestFrame testFrame;

        testFrame = new T8CommunicationTestFrame(context, communicationDefinition, testHarness);
        testFrame.setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jTabbedPaneContent = new javax.swing.JTabbedPane();
        jPanelOperationInput = new javax.swing.JPanel();
        jScrollPaneInputParameters = new javax.swing.JScrollPane();
        jTableInputParameters = new javax.swing.JTable();
        jPanelInputControls = new javax.swing.JPanel();
        jButtonSendCommunication = new javax.swing.JButton();
        jPanelExceptionDetails = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();

        jPanelOperationInput.setLayout(new java.awt.BorderLayout());

        jTableInputParameters.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Parameter Identifier", "Value Expression"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableInputParameters.setRowHeight(25);
        jScrollPaneInputParameters.setViewportView(jTableInputParameters);

        jPanelOperationInput.add(jScrollPaneInputParameters, java.awt.BorderLayout.CENTER);

        jPanelInputControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 5, 1));

        jButtonSendCommunication.setText("Send Communication");
        jButtonSendCommunication.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButtonSendCommunication.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSendCommunicationActionPerformed(evt);
            }
        });
        jPanelInputControls.add(jButtonSendCommunication);

        jPanelOperationInput.add(jPanelInputControls, java.awt.BorderLayout.PAGE_END);

        jTabbedPaneContent.addTab("Operation Input", null, jPanelOperationInput, "");

        jPanelExceptionDetails.setLayout(new java.awt.BorderLayout());

        jEditorPane1.setEditable(false);
        jScrollPane1.setViewportView(jEditorPane1);

        jPanelExceptionDetails.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jTabbedPaneContent.addTab("Exception Details", null, jPanelExceptionDetails, "");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("T8Developer Module Test");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentShown(java.awt.event.ComponentEvent evt)
            {
                formComponentShown(evt);
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentShown
    {//GEN-HEADEREND:event_formComponentShown
        // Start the module when it is shown for the first time.

    }//GEN-LAST:event_formComponentShown

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        // Stop the tested component when this window is closed.

    }//GEN-LAST:event_formWindowClosing

    private void jButtonSendCommunicationActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSendCommunicationActionPerformed
    {//GEN-HEADEREND:event_jButtonSendCommunicationActionPerformed
        sendCommunication();
    }//GEN-LAST:event_jButtonSendCommunicationActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonSendCommunication;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JPanel jPanelExceptionDetails;
    private javax.swing.JPanel jPanelInputControls;
    private javax.swing.JPanel jPanelOperationInput;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPaneInputParameters;
    private javax.swing.JTabbedPane jTabbedPaneContent;
    private javax.swing.JTable jTableInputParameters;
    // End of variables declaration//GEN-END:variables
}
