package com.pilog.t8.developer.definitions.graphview.layout;

/**
 * @author Bouwer du Preez
 */
public class BoxLayoutMatrix extends LayoutMatrix
{
    private Orientation orientation;
    
    public enum Orientation {VERTICAL, HORIZONTAL};
    
    public BoxLayoutMatrix(Orientation orientation)
    {
        this.orientation = orientation;
    }
    
    public void addElement(LayoutElement element)
    {
        if (orientation == Orientation.HORIZONTAL)
        {
            this.addElement(element, 0, getColumnCount());
        }
        else
        {
            this.addElement(element, getRowCount(), 0);
        }
    }
}
