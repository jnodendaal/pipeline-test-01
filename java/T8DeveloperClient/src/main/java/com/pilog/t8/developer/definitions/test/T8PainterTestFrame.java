package com.pilog.t8.developer.definitions.test;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Dimension;
import java.util.HashMap;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class T8PainterTestFrame extends javax.swing.JFrame
{
    private final T8ClientContext clientContext;
    private final JXPanel painterPanel;

    public T8PainterTestFrame(T8Context context, T8PainterDefinition painterDefinition, HashMap<String, Object> inputParameters)
    {
        T8PainterDefinition initializedDefinition;

        this.clientContext = context.getClientContext();
        initComponents();

        // Initialize the Painter Definition..
        try
        {
            T8Context testContext;

            testContext = new T8Context(context);
            testContext.setProjectId(painterDefinition.getRootProjectId());
            initializedDefinition = (T8PainterDefinition)clientContext.getDefinitionManager().initializeDefinition(testContext, painterDefinition, inputParameters);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            initializedDefinition = null;
        }

        painterPanel = new JXPanel();
        add(painterPanel, java.awt.BorderLayout.CENTER);
        validate();

        painterPanel.setBackgroundPainter(new T8PainterAdapter(initializedDefinition.getNewPainterInstance()));

        if(clientContext.getParentWindow() != null)
        {
            Dimension parentDimension = clientContext.getParentWindow().getSize();
            parentDimension.height = (int)(parentDimension.height * 0.80);
            parentDimension.width = (int)(parentDimension.width * 0.80);
            setPreferredSize(new Dimension(parentDimension.width, parentDimension.height));
        }
        else
        {
            setPreferredSize(new Dimension(600, 600));
        }
        pack();
        setLocationRelativeTo(clientContext.getParentWindow());
    }

    public static final void testPainterDefinition(T8Context context, T8PainterDefinition painterDefinition, HashMap<String, Object> inputParameters)
    {
        T8PainterTestFrame testFrame;

        testFrame = new T8PainterTestFrame(context, painterDefinition, inputParameters);
        testFrame.setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("T8Developer Module Test");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
