package com.pilog.t8.developer.definitions.actionhandlers.datarecord.access;

import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.definition.data.document.T8DocumentResources;
import static com.pilog.t8.definition.data.document.T8DocumentResources.DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class DataRecordAccessPrescribedValueNode extends DataRecordAccessNode
{
    private static final T8Logger LOGGER = T8Log.getLogger(DataRecordAccessPrescribedValueNode.class);

    private final T8OntologyConcept concept;
    private final PropertyRequirement parentProperty;
    private final FieldRequirement fieldRequirement;

    public DataRecordAccessPrescribedValueNode(T8OntologyConcept concept, PropertyRequirement parentProperty, FieldRequirement fieldRequirement, DataRecordAccessNode parentNode)
    {
        super(parentNode);
        this.concept = concept;
        this.parentProperty = parentProperty;
        this.fieldRequirement = fieldRequirement;
    }

    @Override
    public DataRequirementInstance getDataRequirementInstance()
    {
        return parentProperty.getParentDataRequirementInstance();
    }

    @Override
    public String getConceptID()
    {
        return concept.getID();
    }

    @Override
    public void loadChildren(T8Context context, T8OntologyClientApi ontApi, T8DataRequirementClientApi drqApi)
    {
        if (!isChildrenLoaded())
        {
            try
            {
                List<T8DataEntity> entities;
                T8DataFilter filter;

                filter = new T8DataFilter(DATA_REQUIREMENT_VALUE_DE_IDENTIFIER);
                filter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, DATA_REQUIREMENT_VALUE_DE_IDENTIFIER + T8DocumentResources.EF_ORG_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, context.getRootOrganizationId(), false);
                filter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, DATA_REQUIREMENT_VALUE_DE_IDENTIFIER + T8DocumentResources.EF_DR_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, parentProperty.getDataRequirementID(), false);
                filter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, DATA_REQUIREMENT_VALUE_DE_IDENTIFIER + T8DocumentResources.EF_PROPERTY_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, parentProperty.getConceptID(), false);
                if (fieldRequirement != null)
                {
                    filter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, DATA_REQUIREMENT_VALUE_DE_IDENTIFIER + T8DocumentResources.EF_FIELD_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, fieldRequirement.getFieldID(), false);
                }

                entities = T8DataManagerOperationHandler.selectDataEntities(context, DATA_REQUIREMENT_VALUE_DE_IDENTIFIER, filter);
                for (T8DataEntity entity : entities)
                {
                    String conceptId;
                    DataRequirementInstance drInstance;

                    conceptId = (String) entity.getFieldValue(entity.getIdentifier() + T8DocumentResources.EF_CONCEPT_ID);
                    drInstance = drqApi.retrieveDataRequirementInstance(conceptId, context.getLanguageId(), false, false);
                    addChild(new DataRecordAccessInstanceNode(drInstance, parentNode));
                }
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to load property node children " + concept.getID(), ex);
            }

            sortChildren();
            setChildrenLoaded(true);
        }
    }

    @Override
    public String getDocPathRepresentation(String suffix)
    {
        return "";
    }
}
