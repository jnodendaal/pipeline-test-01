package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class IdentifierExpressionMappingDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private final List<IdentifierExpressionMappingEditor> editorList;
    private final List<String> identifierOptions;
    private int maximumElements;
    private boolean editable;

    public IdentifierExpressionMappingDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        this.maximumElements = datumType.getMaximumElements();
        this.editorList = new ArrayList<IdentifierExpressionMappingEditor>();
        this.identifierOptions = getIdentifierOptions();
        if (maximumElements == -1) maximumElements = 10000; // A very large value, since -1 indicates unbounded element count.
    }

    private List<String> getIdentifierOptions()
    {
        try
        {
            ArrayList<T8DefinitionDatumOption> datumOptions;

            datumOptions = definition.getDatumOptions(definitionContext, datumType.getIdentifier());
            if ((datumOptions != null) && (datumOptions.size() > 0))
            {
                Map<String, List<String>> optionMap;

                optionMap = (Map<String, List<String>>)datumOptions.get(0).getValue();
                if (optionMap != null)
                {
                    return new ArrayList<>(optionMap.keySet());
                }
                else return new ArrayList<>();
            }
            else return new ArrayList<>();
        }
        catch (Exception e)
        {
            T8Log.log("Exception while loading datum '" + datumType.getIdentifier() + "' options.", e);
            return new ArrayList<>();
        }
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void commitChanges()
    {
        if (editorList != null)
        {
            Map<String, String> mapping;

            mapping = new LinkedHashMap<String, String>();
            for (IdentifierExpressionMappingEditor editor : editorList)
            {
                mapping.put(editor.getIdentifier(), editor.getExpression());
            }

            setDefinitionDatum(mapping);
        }
        else setDefinitionDatum(null);
    }

    @Override
    public void refreshEditor()
    {
        Map<String, String> mapping;

        removeAllEditors();
        mapping = (Map<String, String>)getDefinitionDatum();
        if ((mapping != null) && (mapping.size() > 0))
        {
            for (String identifier : mapping.keySet())
            {
                addEditor(identifier, mapping.get(identifier));
            }
        }
    }

    @Override
    public void setEditable(boolean editable)
    {
        this.editable = editable;
        jButtonAdd.setEnabled(editable);
    }

    private void removeAllEditors()
    {
        editorList.clear();
        editorList.clear();
        jPanelContent.removeAll();
        jPanelContent.revalidate();
        jPanelContent.repaint();
    }

    public void removeEditor(IdentifierExpressionMappingEditor editorToRemove)
    {
        int index;

        index = editorList.indexOf(editorToRemove);
        if (index > -1)
        {
            IdentifierExpressionMappingEditor editor;

            editor = editorList.remove(index);
            editorList.remove(editorToRemove);
            jPanelContent.remove(editor);
            jPanelContent.revalidate();
            jPanelContent.repaint();
        }
    }

    private void addEditor(String identifier, String expression)
    {
        IdentifierExpressionMappingEditor editor;

        editor = new IdentifierExpressionMappingEditor(this, identifierOptions, identifier, expression);
        editorList.add(editor);
        jPanelContent.add(editor);
        jPanelContent.revalidate();
        jPanelContent.repaint();
    }

    public void moveEditorUp(IdentifierExpressionMappingEditor editor)
    {
        int index;

        index = editorList.indexOf(editor);
        if (index > 0)
        {
            editor = editorList.remove(index);
            editorList.add(index-1, editor);
        }

        // Commit the updated sequence.
        commitChanges();

        // Refresh the editor.
        refreshEditor();
    }

    public void moveEditorDown(IdentifierExpressionMappingEditor editor)
    {
        int index;

        // Decrement all selected Indices.
        index = editorList.indexOf(editor);
        if (index < editorList.size() -1)
        {
            editor = editorList.remove(index);
            editorList.add(index+1, editor);
        }

        // Commit the updated sequence.
        commitChanges();

        // Refresh the editor.
        refreshEditor();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jPanelContent = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAdd);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);

        jPanelContent.setLayout(new javax.swing.BoxLayout(jPanelContent, javax.swing.BoxLayout.Y_AXIS));
        add(jPanelContent, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        addEditor(null, null);
    }//GEN-LAST:event_jButtonAddActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
