package com.pilog.t8.developer.utils;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.developer.definitions.dialog.T8InformationDialog;
import com.pilog.t8.utilities.collections.HashMaps;
import java.awt.Window;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

import static com.pilog.t8.definition.system.T8AdministrationResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Hennie Brink
 */
public class T8DefinitionValidationUtils
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefinitionValidationUtils.class);

    public static boolean validateDefinitions(Window parentWindow, T8DeveloperView developerView, T8Context context, List<String> definitionIdentifiers) throws Exception
    {
        return validateDefinitions(parentWindow, developerView, context, definitionIdentifiers, false);
    }

    public static boolean validateDefinitions(Window parentWindow, T8DeveloperView developerView, T8Context context, List<String> definitionIdentifiers, boolean ignoreSucceed) throws Exception
    {
        Map<String, Object> result;
        String validationResult;

        result = context.getClientContext().executeAsynchronousServerOperation(context, OPERATION_VALIDATE_DEFINITIONS, HashMaps.createSingular(OPERATION_VALIDATE_DEFINITIONS + PARAMETER_DEFINITION_IDS, definitionIdentifiers), "Validating Definitions...");
        validationResult = (String)result.get(OPERATION_VALIDATE_DEFINITIONS + PARAMETER_DEFINITION_VALIDATION_REPORT);

        if (validationResult != null)
        {
            T8InformationDialog.showInformation(parentWindow, developerView, validationResult);
            return false;
        }
        else if (!ignoreSucceed)
        {
            JOptionPane.showMessageDialog(parentWindow, "No validation errors found.", "All Clear", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }
        else
        {
            // Success without a message.
            return true;
        }
    }

    public static String createReport(List<T8DefinitionValidationError> validationErrors)
    {
        StringBuffer report;


        report = new StringBuffer();
        report.append("<html>");
        report.append("<h1>System Validation Report</h1>");
        report.append("</br>");

        report.append("</br>");
        report.append("<h2>Validation Errors</h2>");

        if ((validationErrors != null) && (validationErrors.size() > 0))
        {
            report.append("<table border=\"1\">");
            report.append("<tr><td><b>Error Type</b></td><td><b>Project</b></td><td><b>Definition</b></td><td><b>Datum</b></td><td><b>Message</b></td></tr>");

            for (T8DefinitionValidationError error : validationErrors)
            {
                report.append("<tr><td>" + error.getErrorType() + "</td><td>" + error.getProjectIdentifier() + "</td><td><a href=" + error.getDefinitionIdentifier() + ">" + error.getDefinitionIdentifier() + "</a></td><td>" + error.getDatumIdentifier() + "</td><td>" + error.getMessage() + "</td></tr>");
            }
        }

        // Close the table tag if errors were found, else just log a message to indicate that everything is ok.
        if ((validationErrors != null) && validationErrors.size() > 0)
        {
            report.append("</table>");
        }
        else
        {
            report.append("No validation errors found.");
        }

        // Close the html tag.
        report.append("</html>");

        return report.toString();
    }
}
