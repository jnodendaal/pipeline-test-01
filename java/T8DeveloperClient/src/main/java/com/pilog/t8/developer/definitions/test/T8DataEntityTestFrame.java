package com.pilog.t8.developer.definitions.test;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.developer.definitions.test.harness.T8DataEntityTestHarness;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import com.pilog.t8.ui.T8ClientController;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityTestFrame extends javax.swing.JFrame
{
    private final T8ClientController testController;
    private T8ModuleDefinition moduleDefinition;
    private T8DataEntityDefinition entityDefinition;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8Context testContext;

    public T8DataEntityTestFrame(T8Context context, T8DataEntityDefinition entityDefinition, T8DataEntityTestHarness testHarness)
    {
        this.context = context;
        this.testContext = new T8ProjectContext(context, entityDefinition.getRootProjectId());
        this.clientContext = context.getClientContext();
        this.entityDefinition = entityDefinition;
        initComponents();
        constructModuleDefinition();

        // Construct the test controller.
        testController = new T8ClientController(testContext);
        testController.initializeComponent(null);
        add(testController, java.awt.BorderLayout.CENTER);
        validate();

        if (clientContext.getParentWindow() != null)
        {
            Dimension parentDimension = clientContext.getParentWindow().getSize();
            parentDimension.height = (int)(parentDimension.height * 0.80);
            parentDimension.width = (int)(parentDimension.width * 0.80);
            setPreferredSize(new Dimension(parentDimension.width, parentDimension.height));
        }
        else
        {
            setPreferredSize(new Dimension(600, 600));
        }

        pack();
        setLocationRelativeTo(clientContext.getParentWindow());
    }

    private void constructModuleDefinition()
    {
        try
        {
            ArrayList<T8TableColumnDefinition> columnDefinitions;
            List<T8DataEntityFieldDefinition> fieldDefinitions;
            T8PanelSlotDefinition panelSlotDefinition;
            T8PanelDefinition panelDefinition;
            T8TableDefinition tableDefinition;

            tableDefinition = new T8TableDefinition("TEMP");
            tableDefinition.setDataEntityIdentifier(entityDefinition.getIdentifier());
            fieldDefinitions = entityDefinition.getFieldDefinitions();
            columnDefinitions = new ArrayList<T8TableColumnDefinition>(tableDefinition.getColumnDefinitions());

            for (T8DataFieldDefinition fieldDefinition : fieldDefinitions)
            {
                T8TableColumnDefinition newColumnDefinition;
                String publicFieldIdentifier;
                boolean found;

                found = false;
                publicFieldIdentifier = fieldDefinition.getPublicIdentifier();
                for (T8TableColumnDefinition columnDefinition : tableDefinition.getColumnDefinitions())
                {
                    if (columnDefinition.getFieldIdentifier().equals(publicFieldIdentifier))
                    {
                        found = true;
                        break;
                    }
                }

                // If field was not found add a new column for it.
                if (!found)
                {
                    String fieldId;

                    fieldId = fieldDefinition.getIdentifier();
                    newColumnDefinition = new T8TableColumnDefinition(fieldId);
                    newColumnDefinition.setColumnName(fieldDefinition.getSourceIdentifier());
                    newColumnDefinition.setFieldIdentifier(publicFieldIdentifier);
                    newColumnDefinition.setVisible(true);
                    newColumnDefinition.setEditable(true);
                    newColumnDefinition.setWidth(100);
                    newColumnDefinition.setRequired(fieldDefinition.isKey() || fieldDefinition.isRequired());
                    columnDefinitions.add(newColumnDefinition);
                }
            }

            // Set the new collection of column definitions.
            if (columnDefinitions == null)
            {
                columnDefinitions = new ArrayList<T8TableColumnDefinition>();
            }
            tableDefinition.setColumnDefinitions(columnDefinitions);

            moduleDefinition = new T8ModuleDefinition("TABLE_TEST_MODULE");
            panelDefinition = new T8PanelDefinition("TABLE_TEST_PANEL");
            panelSlotDefinition = new T8PanelSlotDefinition("TABLE_TEST_PANEL_SLOT");
            panelSlotDefinition.setSlotComponentDefinition(tableDefinition);
            panelSlotDefinition.setAnchor(T8PanelSlotDefinition.Anchor.CENTER);
            panelSlotDefinition.setFill(T8PanelSlotDefinition.Fill.BOTH);
            panelSlotDefinition.setGridHeight(1);
            panelSlotDefinition.setGridWidth(1);
            panelSlotDefinition.setGridX(0);
            panelSlotDefinition.setGridY(0);
            panelSlotDefinition.setWeightX(0.1);
            panelSlotDefinition.setWeightY(0.1);

            panelDefinition.setPanelSlotDefinitions(new ArrayList<T8PanelSlotDefinition>(Arrays.asList(panelSlotDefinition)));
            moduleDefinition.setRootComponentDefinition(panelDefinition);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.entityDefinition = null;
        }
    }

    public void start()
    {
        try
        {
            T8ModuleDefinition initializedModuleDefinition;

            // Get the initialized module and entity definitions.
            initializedModuleDefinition = (T8ModuleDefinition)clientContext.getDefinitionManager().initializeDefinition(testContext, moduleDefinition, null);

            // Load the module from the initialized definition.
            testController.startComponent();
            testController.stopTaskExecution();
            testController.loadModule(initializedModuleDefinition, null);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while starting main module.", e);
        }
    }

    public void stop()
    {
        try
        {
            testController.stopComponent();
        }
        catch (Exception e)
        {
            T8Log.log("Exception while trying to log out.", e);
        }
    }

    public static final void testModuleDefinition(T8Context context, T8DataEntityDefinition dataEntityDefinition, T8DataEntityTestHarness testHarness)
    {
        T8DataEntityTestFrame testFrame;

        testFrame = new T8DataEntityTestFrame(context, dataEntityDefinition, testHarness);
        testFrame.setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("T8Developer Module Test");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentShown(java.awt.event.ComponentEvent evt)
            {
                formComponentShown(evt);
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentShown
    {//GEN-HEADEREND:event_formComponentShown
        // Start the module when it is shown for the first time.
        start();
    }//GEN-LAST:event_formComponentShown

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        // Stop the tested component when this window is closed.
        stop();
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
