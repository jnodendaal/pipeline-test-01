package com.pilog.t8.developer.definitions.projectmanager;

import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.view.DefaultVertexRenderer;
import com.pilog.graph.view.Graph;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import javax.swing.ImageIcon;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectVertexRenderer extends DefaultVertexRenderer
{
    private final T8ProjectGraphConfiguration config;
    private final Insets vertexInsets = new Insets(30, 30, 30, 30);

    private static final ImageIcon COMPUTER_ICON = new javax.swing.ImageIcon(T8ProjectVertexRenderer.class.getResource("/com/pilog/t8/developer/icons/computerIcon.png"));

    public T8ProjectVertexRenderer(T8ProjectGraphConfiguration config)
    {
        this.config = config;
    }

    public T8ProjectGraphConfiguration getConfiguration()
    {
        return config;
    }

    @Override
    public Component getRendererComponent(Graph graph, GraphVertex vertex, boolean isSelected)
    {
        Dimension size;

        this.graph = graph;
        this.vertex = vertex;
        this.isSelected = isSelected;
        size = vertex.getVertexBounds().getSize();
        this.setSize(size);
        this.setPreferredSize(size);
        return this;
    }

    @Override
    public void paint(Graphics g)
    {
        Color highColor;
        Color lowColor;
        Shape vertexShape;
        Image activityIcon;
        int shapeWidth;
        int shapeHeight;
        Graphics2D g2;

        // Cast graphics context to the required type.
        g2 = (Graphics2D)g;

        // Set the selection color of the vertex shape.
        if (isSelected)
        {
            highColor = config.getSelectedHighColor();
            lowColor = config.getSelectedLowColor();
        }
        else
        {
            highColor = config.getActivityHighColor();
            lowColor = config.getActivityLowColor();
        }

        // Get the size of the shape from the renderer component size.
        shapeWidth = getWidth();
        shapeHeight = getHeight();

        // Create the shape of the vertex and paint it.
        vertexShape = new Rectangle2D.Float(0, 0, shapeWidth-1, shapeHeight-1);
        paintShape(g2, vertexShape, highColor, lowColor, true);

        // Paint the vertex label.
        paintLabel(g2, shapeWidth, shapeHeight,  vertex.getId());

        // Paint the activity icon.
        activityIcon = COMPUTER_ICON.getImage();
        if (activityIcon != null)
        {
            g2.drawImage(activityIcon, 5, 5, null);
        }
    }

    @Override
    public Dimension getVertexSize(Graph graph, GraphVertex vertex, boolean isSelected)
    {
        FontMetrics fontMetrics;
        Dimension size;

        fontMetrics = this.getFontMetrics(config.getTextFont());
        size = new Dimension();
        size.width = fontMetrics.stringWidth(vertex.getId());
        size.width += (vertexInsets.left + vertexInsets.right);
        size.height = fontMetrics.getHeight();
        size.height += (vertexInsets.top + vertexInsets.bottom);
        return size;
    }

    private void paintLabel(Graphics2D g2, int shapeWidth, int shapeHeight, String text)
    {
        Rectangle2D bounds;

        g2.setFont(config.getTextFont());
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(2));
        g2.setColor(Color.DARK_GRAY);

        bounds = getStringBounds(g2, text);
        g2.drawString(text, (float)((shapeWidth/2.0) - (bounds.getWidth()/2.0)), (float)((shapeHeight/2.0) + (bounds.getHeight()/4.0)));
    }

    private void paintShape(Graphics2D g2, Shape clipShape, Color highColor, Color lowColor, boolean fill)
    {
        int shapeWidth;
        int shapeHeight;

        // Create the clip image.
        shapeWidth = clipShape.getBounds().width;
        shapeHeight = clipShape.getBounds().height;

        // Fill the shape with a gradient.
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(new GradientPaint(0, 0, highColor, 0, shapeHeight, lowColor));
        if (fill) g2.fill(clipShape);

        // Apply the border.
        g2.setStroke(new BasicStroke(1));
        g2.setPaint(config.getNodeBorderColor());
        g2.draw(clipShape);
    }

    private Rectangle2D getStringBounds(Graphics2D g2, String string)
    {
        FontMetrics metrics;

        metrics = g2.getFontMetrics();
        return metrics.getStringBounds(string, 0, string.length(), g2);
    }
}
