package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.epic.EPIC;
import com.pilog.epic.ParserContext;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.ide.CodeCompletionHandler;
import com.pilog.epic.ide.CodeCompletionResolver;
import com.pilog.epic.ide.EpicScriptEditor;
import com.pilog.epic.ide.RSyntaxEpicEditor;
import com.pilog.epic.rsyntax.HyperlinkFollowEvent;
import com.pilog.epic.rsyntax.HyperlinkFollowListener;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.client.T8ScriptConfigurationHandler;
import com.pilog.t8.data.document.path.DocPathEvaluatorFactory;
import com.pilog.t8.data.document.path.EpicDocPathParser;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ScriptDefinition;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionHandler;
import com.pilog.t8.definition.script.completionhandler.T8ScriptCompletionResolver;
import com.pilog.t8.developer.document.path.T8DocPathGenerator;
import com.pilog.t8.developer.utils.GUIDTermTooltipSupplier;
import com.pilog.t8.developer.utils.T8ConceptLookupFrame;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public class ScriptEditorPanel extends JPanel
{
    private final T8ClientContext clientContext;
    private final T8DefinitionContext definitionContext;
    private final T8Context context;
    private final EpicScriptEditor epicEditor;
    private final ScriptDatumEditor parentEditor;
    private T8ScriptDefinition definition;
    private String originalScript;
    private T8DocPathGenerator docPathGenerator;
    private T8ConceptLookupFrame conceptLookupFrame;

    public ScriptEditorPanel(T8DefinitionContext definitionContext, ScriptDatumEditor parentEditor)
    {
        this.clientContext = definitionContext.getClientContext();
        this.definitionContext = definitionContext;
        this.context = definitionContext.getContext();
        this.parentEditor = parentEditor;
        initComponents();
        this.epicEditor = new RSyntaxEpicEditor(createParserContext());
        configureSyntax();
        this.jPanelEditor.add(epicEditor.getComponent(), java.awt.BorderLayout.CENTER);
        this.jListInputParameters.addMouseListener(new ListMouseListener());
        this.jListOutputParameters.addMouseListener(new ListMouseListener());

        this.jXTaskPaneScriptAPI.add(jPanelScriptAPI);
    }

    private ParserContext createParserContext()
    {
        T8ScriptConfigurationHandler configurationHandler;
        DocPathEvaluatorFactory docPathFactory;
        ParserContext parserContext;

        // Create a docPath factory.
        docPathFactory = DocPathEvaluatorFactory.getFactory(context);

        // Create a new parser context.
        parserContext = new ParserContext();
        parserContext.addExternalExpressionParser(new EpicDocPathParser(docPathFactory));

        // Configure the parser context.
        configurationHandler = new T8ScriptConfigurationHandler(context, docPathFactory);
        configurationHandler.configureParserContext(parserContext);

        // Return the final parser context.
        return parserContext;
    }

    public void setEditable(boolean editable)
    {
        jButtonDocPathGen.setEnabled(editable);
        jButtonOntologyLookup.setEnabled(editable);
        epicEditor.getComponent().setEnabled(editable);
    }

    public String getScript()
    {
        return epicEditor.getSourceCode();
    }

    public void startEditor()
    {
        epicEditor.startEditor();

        ScriptEditorLinkListener listener;

        listener = new ScriptEditorLinkListener();
        epicEditor.setHyperLinkListener(listener);
        epicEditor.setTooltipSupplier(new GUIDTermTooltipSupplier(context));
    }

    public void stopEditor()
    {
        epicEditor.stopEditor();

        if(docPathGenerator != null) docPathGenerator.dispose();
        if(conceptLookupFrame != null) conceptLookupFrame.dispose();
    }

    public boolean hasChanges()
    {
        return originalScript != null && !originalScript.equals(epicEditor.getSourceCode());
    }

    public void setScript(T8ScriptDefinition scriptDefinition, String script)
    {
        try
        {
            DefaultListModel namespaceListModel;
            DefaultListModel inputParameterListModel;
            DefaultListModel outputParameterListModel;
            List<T8DataParameterDefinition> inputParameterDefinitions;
            List<T8DataParameterDefinition> outputParameterDefinitions;
            String namespace;
            T8ScriptCompletionHandler completionHandler;

            originalScript = script;
            definition = scriptDefinition;
            namespace = scriptDefinition.getNamespace();
            inputParameterDefinitions = scriptDefinition.getInputParameterDefinitions(context);
            outputParameterDefinitions = scriptDefinition.getOutputParameterDefinitions(context);
            completionHandler = scriptDefinition.getScriptCompletionHandler(context);

            if (Strings.isNullOrEmpty(script))
            {
                StringBuilder stringBuffer = new StringBuilder("program script(var session");
                if (inputParameterDefinitions != null)
                {
                    Iterator<T8DataParameterDefinition> inputParameterIterator = inputParameterDefinitions.iterator();
                    if (inputParameterIterator.hasNext())
                    {
                        stringBuffer.append(", ");
                    }
                    while (inputParameterIterator.hasNext())
                    {
                        stringBuffer.append("var ");
                        stringBuffer.append(inputParameterIterator.next().getIdentifier());
                        if (inputParameterIterator.hasNext())
                        {
                            stringBuffer.append(", ");
                        }
                    }
                }
                stringBuffer.append(")\n{\n");
                if (outputParameterDefinitions != null)
                {
                    Iterator<T8DataParameterDefinition> outputParameterIterator = outputParameterDefinitions.iterator();
                    if (outputParameterIterator.hasNext())
                    {
                        stringBuffer.append("return new [");
                    }
                    while (outputParameterIterator.hasNext())
                    {
                        stringBuffer.append("\"");
                        stringBuffer.append(outputParameterIterator.next().getPublicIdentifier());
                        stringBuffer.append("\":");
                        if (outputParameterIterator.hasNext())
                        {
                            stringBuffer.append(", ");
                        }
                        else
                        {
                            stringBuffer.append("];");
                        }
                    }
                }
                stringBuffer.append("\n}");
                script = stringBuffer.toString();
            }
            epicEditor.setSourceCode(script);
            // Add the code completion handler if one if provided by the script definition.
            epicEditor.clearCodeCompletionHandlers();
            if (completionHandler != null)
            {
                epicEditor.addCodeCompletionHandler(new CodeCompletionHandlerAdaptor(completionHandler));
            }

            if (completionHandler.getMethodParameterChoicesProvider() != null)
            {
                epicEditor.setMethodParameterChoicesProvider(completionHandler.getMethodParameterChoicesProvider());
            }

            if (completionHandler.getRegexCompletionProviders()!= null)
            {
                epicEditor.setRegexCompletionProviders(completionHandler.getRegexCompletionProviders());
            }

            // Set the namespace list.
            namespaceListModel = new DefaultListModel();
            namespaceListModel.addElement(namespace);
            jListNamespaces.setModel(namespaceListModel);

            // Set the input parameter list.
            if (inputParameterDefinitions != null)
            {
                inputParameterListModel = new DefaultListModel();
                for (T8DataParameterDefinition parameterDefinition : inputParameterDefinitions)
                {
                    String identifier;

                    identifier = parameterDefinition.getPublicIdentifier();
                    if (T8IdentifierUtilities.isInNamespace(identifier, namespace))
                    {
                        inputParameterListModel.addElement(parameterDefinition.getIdentifier());
                    }
                    else
                    {
                        inputParameterListModel.addElement(parameterDefinition.getPublicIdentifier());
                    }
                }
                jListInputParameters.setModel(inputParameterListModel);
            }

            // Set the output parameter list.
            if (outputParameterDefinitions != null)
            {
                outputParameterListModel = new DefaultListModel();
                for (T8DataParameterDefinition parameterDefinition : outputParameterDefinitions)
                {
                    String identifier;

                    identifier = parameterDefinition.getPublicIdentifier();
                    if (T8IdentifierUtilities.isInNamespace(identifier, namespace))
                    {
                        outputParameterListModel.addElement(parameterDefinition.getIdentifier());
                    }
                    else
                    {
                        outputParameterListModel.addElement(parameterDefinition.getPublicIdentifier());
                    }
                }
                jListOutputParameters.setModel(outputParameterListModel);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while setting Script API '" + definition.getIdentifier() + "'.", e);
        }
    }

    private void testScript()
    {
        try
        {
            EPIC.compileProgram(epicEditor.getSourceCode(), "UTF-8");
        }
        catch (EPICSyntaxException e)
        {
            T8Log.log("Exception while testing Script '" + definition.getIdentifier() + "'.", e);
            JOptionPane.showMessageDialog(this, e.getMessage(), "Syntax Error", JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while testing Script '" + definition.getIdentifier() + "'.", e);
            JOptionPane.showMessageDialog(this, e.getMessage(), "Syntax Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void configureSyntax()
    {
        HashMap<String, Color> colors;
        HashMap<String, String> help;

        colors = new HashMap<String, Color>();
        colors.put("do", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("doGet", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("execServerScript", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("execModuleScript", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("execServerOp", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("execProgressiveServerOp", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("execBackgroundServerOp", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("execSynchServerOp", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("execAsynchServerOp", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("execClientOp", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("signal", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("showInfo", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("showError", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("getInput", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("setVar", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("getVar", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("startFlow", EpicScriptEditor.KEYWORD_COLOR_2);
        colors.put("exit", EpicScriptEditor.KEYWORD_COLOR_2);

        help = new HashMap<String, String>();
        help.put("do", "Executes a component operation within the context of a module and returns a Map<String, Object> containing the operation output parameters.");
        help.put("doGet", "Executes a component operation within the context of a module and returns the specified output parameter form the operation output collection.");
        help.put("execServerScript", "Executes a server script and returns a Map<String, Object> containing the operation output parameters.");
        help.put("execServerOp", "Executes a server operation synchronously and returns a Map<String, Object> containing the operation output parameters.");
        help.put("execProgressiveServerScript", "Executes a server operation that can report on its progress and returns a Map<String, Object> containing the operation output parameters.");
        help.put("execBackgroundServerScript", "Executes a server operation in the background and returns the operation instance ID of the operation.");
        help.put("execModuleScript", "Executes a module script and returns a Map<String, Object> containing the operation output parameters.");
        help.put("execSynchServerOp", "Executes a server operation synchronously and returns a Map<String, Object> containing the operation output parameters.");
        help.put("execAsynchServerOp", "Executes a server operation asynchronously (not available yet).");
        help.put("execSynchClientOp", "Executes a client operation synchronously and returns a Map<String, Object> containing the operation output parameters.");
        help.put("execAsynchClientOp", "Execues a client operation asynchronously (not available yet).");
        help.put("signal", "Signals the occurrence of a module event.  The module event parameters may also be supplied.");
        help.put("showInfo", "Shows an information message in a popup dialog.");
        help.put("showError", "Shows an error message in a popup dialog.");
        help.put("getInput", "Shows an input dialog which allows the user to enter a String value which is then returned.");
        help.put("setVar", "Sets a variable value wihtin the context of a module.  This variable is available to any script running in the same context.");
        help.put("getVar", "Returns a variable value stored in the module context.");
        help.put("startFlow", "Starts a specified flow using the supplied input parameters.");

        epicEditor.configureSyntax(colors, help);
    }

    private void showDocPathGenerator()
    {
        if(docPathGenerator == null)
        {
            docPathGenerator = new T8DocPathGenerator(context, epicEditor);
            docPathGenerator.setTitle("Doc Path Generator " + parentEditor.getDefinition().getIdentifier() + " - " + parentEditor.getDatumIdentifier());
            docPathGenerator.setLocationRelativeTo(clientContext.getParentWindow());
        }

        docPathGenerator.setVisible(true);
    }

    private void showConceptLookup()
    {
        if(conceptLookupFrame == null)
        {
            conceptLookupFrame = new T8ConceptLookupFrame(context);
            conceptLookupFrame.setTitle("Concept Lookup " + parentEditor.getDefinition().getIdentifier() + " - " + parentEditor.getDatumIdentifier());
            conceptLookupFrame.setLocationRelativeTo(clientContext.getParentWindow());
            conceptLookupFrame.addConceptSelectionListener(new T8ConceptLookupFrame.ConceptSelectionListener()
            {

                @Override
                public void conceptSelected(String conceptID)
                {
                    epicEditor.insertSourceCode(conceptID);
                }
            });
        }

        conceptLookupFrame.setVisible(true);
    }

    public void addToolBarButton(AbstractButton button)
    {
        jToolBarMain.add(button);
    }

    public void removeToolBarButton(AbstractButton button)
    {
        jToolBarMain.remove(button);
    }

    private class ListMouseListener implements MouseListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.getClickCount() == 2)
            {
                JList list;
                String selectedIdentifier;

                list = (JList) e.getSource();
                selectedIdentifier = (String) list.getSelectedValue();
                if (selectedIdentifier != null)
                {
                    epicEditor.insertSourceCode(selectedIdentifier);
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    private class CodeCompletionHandlerAdaptor implements CodeCompletionHandler
    {
        private final List<CodeCompletionResolver> resolvers;

        private CodeCompletionHandlerAdaptor(T8ScriptCompletionHandler scriptCompletionHandler)
        {
            resolvers = new ArrayList<>();
            for (T8ScriptCompletionResolver resolver : scriptCompletionHandler.getResolvers())
            {
                resolvers.add(new CodeCompletionResolverAdaptor(resolver));
            }
        }

        @Override
        public List<CodeCompletionResolver> getResolvers()
        {
            return resolvers;
        }
    }

    private class CodeCompletionResolverAdaptor implements CodeCompletionResolver
    {
        T8ScriptCompletionResolver resolver;

        private CodeCompletionResolverAdaptor(T8ScriptCompletionResolver resolver)
        {
            this.resolver = resolver;
        }

        @Override
        public List<String> getMatchPatterns()
        {
            return resolver.getMatchPatterns();
        }

        @Override
        public Map<String, String> getCodeCompletionSuggestions(String matchedText)
        {
            return resolver.getCodeCompletionSuggestions(matchedText);
        }
    }

    private class ScriptEditorLinkListener implements HyperlinkFollowListener
    {
        @Override
        public String getPatterMatchString()
        {
            return "[@$]+\\w+";
        }

        @Override
        public void hyperlinkFollowed(HyperlinkFollowEvent event)
        {
            parentEditor.fireDefinitionLinkActivatedEvent(definition.getRootProjectId(), event.getHyperlink());
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelScriptAPI = new javax.swing.JPanel();
        jLabelNamespace = new javax.swing.JLabel();
        jListNamespaces = new javax.swing.JList();
        jLabelInputParameters = new javax.swing.JLabel();
        jScrollPaneInputParameters = new javax.swing.JScrollPane();
        jListInputParameters = new javax.swing.JList();
        jLabelOutputParameters = new javax.swing.JLabel();
        jScrollPaneOutputParameters = new javax.swing.JScrollPane();
        jListOutputParameters = new javax.swing.JList();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonTest = new javax.swing.JButton();
        jButtonReset = new javax.swing.JButton();
        jButtonDocPathGen = new javax.swing.JButton();
        jButtonOntologyLookup = new javax.swing.JButton();
        jXTaskPaneScriptAPI = new org.jdesktop.swingx.JXTaskPane();
        jPanelEditor = new javax.swing.JPanel();

        jPanelScriptAPI.setOpaque(false);
        jPanelScriptAPI.setLayout(new java.awt.GridBagLayout());

        jLabelNamespace.setText("Namespace:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanelScriptAPI.add(jLabelNamespace, gridBagConstraints);

        jListNamespaces.setBackground(new java.awt.Color(240, 240, 240));
        jListNamespaces.setModel(new javax.swing.AbstractListModel()
        {
            String[] strings = { " " };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jListNamespaces.setOpaque(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanelScriptAPI.add(jListNamespaces, gridBagConstraints);

        jLabelInputParameters.setText("Input Parameters:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanelScriptAPI.add(jLabelInputParameters, gridBagConstraints);

        jScrollPaneInputParameters.setMinimumSize(new java.awt.Dimension(23, 80));

        jListInputParameters.setBackground(new java.awt.Color(240, 240, 240));
        jListInputParameters.setModel(new javax.swing.AbstractListModel()
        {
            String[] strings = { " " };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jListInputParameters.setOpaque(false);
        jListInputParameters.setVisibleRowCount(5);
        jScrollPaneInputParameters.setViewportView(jListInputParameters);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelScriptAPI.add(jScrollPaneInputParameters, gridBagConstraints);

        jLabelOutputParameters.setText("Output Parameters:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanelScriptAPI.add(jLabelOutputParameters, gridBagConstraints);

        jScrollPaneOutputParameters.setMinimumSize(new java.awt.Dimension(23, 80));

        jListOutputParameters.setBackground(new java.awt.Color(240, 240, 240));
        jListOutputParameters.setModel(new javax.swing.AbstractListModel()
        {
            String[] strings = { " " };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jListOutputParameters.setOpaque(false);
        jListOutputParameters.setVisibleRowCount(5);
        jScrollPaneOutputParameters.setViewportView(jListOutputParameters);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanelScriptAPI.add(jScrollPaneOutputParameters, gridBagConstraints);

        setMinimumSize(new java.awt.Dimension(162, 1500));
        setPreferredSize(new java.awt.Dimension(120, 1500));
        setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonTest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/testIcon.png"))); // NOI18N
        jButtonTest.setText("Test");
        jButtonTest.setFocusable(false);
        jButtonTest.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonTest.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonTestActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonTest);

        jButtonReset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonReset.setText("Reset");
        jButtonReset.setFocusable(false);
        jButtonReset.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBarMain.add(jButtonReset);

        jButtonDocPathGen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/scriptIcon.png"))); // NOI18N
        jButtonDocPathGen.setText("Doc Path");
        jButtonDocPathGen.setFocusable(false);
        jButtonDocPathGen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDocPathGen.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDocPathGenActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDocPathGen);

        jButtonOntologyLookup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/flashlight.png"))); // NOI18N
        jButtonOntologyLookup.setText("Concept Lookup");
        jButtonOntologyLookup.setFocusable(false);
        jButtonOntologyLookup.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonOntologyLookup.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonOntologyLookupActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonOntologyLookup);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarMain, gridBagConstraints);

        jXTaskPaneScriptAPI.setAnimated(false);
        jXTaskPaneScriptAPI.setCollapsed(true);
        jXTaskPaneScriptAPI.setTitle("Script API");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jXTaskPaneScriptAPI, gridBagConstraints);

        jPanelEditor.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelEditor, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonTestActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonTestActionPerformed
    {//GEN-HEADEREND:event_jButtonTestActionPerformed
        testScript();
    }//GEN-LAST:event_jButtonTestActionPerformed

    private void jButtonDocPathGenActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDocPathGenActionPerformed
    {//GEN-HEADEREND:event_jButtonDocPathGenActionPerformed
        showDocPathGenerator();
    }//GEN-LAST:event_jButtonDocPathGenActionPerformed

    private void jButtonOntologyLookupActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonOntologyLookupActionPerformed
    {//GEN-HEADEREND:event_jButtonOntologyLookupActionPerformed
        showConceptLookup();
    }//GEN-LAST:event_jButtonOntologyLookupActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDocPathGen;
    private javax.swing.JButton jButtonOntologyLookup;
    private javax.swing.JButton jButtonReset;
    private javax.swing.JButton jButtonTest;
    private javax.swing.JLabel jLabelInputParameters;
    private javax.swing.JLabel jLabelNamespace;
    private javax.swing.JLabel jLabelOutputParameters;
    private javax.swing.JList jListInputParameters;
    private javax.swing.JList jListNamespaces;
    private javax.swing.JList jListOutputParameters;
    private javax.swing.JPanel jPanelEditor;
    private javax.swing.JPanel jPanelScriptAPI;
    private javax.swing.JScrollPane jScrollPaneInputParameters;
    private javax.swing.JScrollPane jScrollPaneOutputParameters;
    private javax.swing.JToolBar jToolBarMain;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneScriptAPI;
    // End of variables declaration//GEN-END:variables
}
