package com.pilog.t8.developer.definitions.actionhandlers.ui.entityeditor.panel;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.entityeditor.conceptlabel.T8ConceptLabelDataEntityEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.concepttypelabel.T8ConceptTypeLabelDataEntityEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.datacombobox.T8DataComboBoxDataEntityEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.datalookupfield.T8DataLookupFieldDataEntityEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.textarea.T8TextAreaDataEntityEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.textfield.T8TextFieldDataEntityEditorDefinition;
import com.pilog.t8.definition.ui.label.T8LabelDefinition.HorizontalTextAlignment;
import com.pilog.t8.definition.ui.label.T8LabelDefinition.VerticalTextAlignment;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.utilities.components.cellrenderers.CellRendererUtilities;
import java.awt.Component;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;

/**
 * @author Bouwer du Preez
 */
public class T8PanelDataEntityEditorFieldSetupDialog extends javax.swing.JDialog
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final List<T8DataEntityFieldDefinition> fieldDefinitions;
    private boolean proceed;

    public T8PanelDataEntityEditorFieldSetupDialog(T8Context context, List<T8DataEntityFieldDefinition> fieldDefinitions)
    {
        super(context.getClientContext().getParentWindow(), ModalityType.DOCUMENT_MODAL);
        this.clientContext = context.getClientContext();
        this.context = context;
        this.fieldDefinitions = fieldDefinitions;
        initComponents();
        this.jTableFieldSetup.setRowHeight(25);
        this.proceed = false;
        this.jPanelLayoutSetup.setBorder(new T8ContentHeaderBorder("Layout Setup"));
        this.jPanelFieldSetup.setBorder(new T8ContentHeaderBorder("Field Setup"));
        populateComboBoxes();
        populateTable();
        setSize(600, 500);
        setLocationRelativeTo(null);
    }

    private void populateComboBoxes()
    {
        // Vertical label text alignment.
        for (VerticalTextAlignment alignment : VerticalTextAlignment.values())
        {
            jComboBoxLabelVerticalTextAlignment.addItem(alignment);
        }

        // Horizontal label text alignment.
        for (HorizontalTextAlignment alignment : HorizontalTextAlignment.values())
        {
            jComboBoxLabelHorizontalTextAlignment.addItem(alignment);
        }
    }

    private void populateTable()
    {
        DefaultTableModel tableModel;

        // Set the editor type combo box cell editor.
        jTableFieldSetup.getColumnModel().getColumn(jTableFieldSetup.convertRowIndexToView(3)).setCellEditor(new EditorTypeCellEditor());

        // Get the table model and add all of the entity fields as setup rows.
        tableModel = (DefaultTableModel)jTableFieldSetup.getModel();
        for (T8DataEntityFieldDefinition fieldDefinition : fieldDefinitions)
        {
            Object[] dataRow;
            String fieldIdentifier;

            fieldIdentifier = fieldDefinition.getIdentifier();

            dataRow = new Object[5];
            dataRow[0] = fieldIdentifier;
            dataRow[1] = T8Definition.getLocalIdPrefix() + "FIELD_EDITOR_" + fieldIdentifier.substring(fieldIdentifier.indexOf(T8Definition.getLocalIdPrefix()) + 1);
            dataRow[2] = true;
            dataRow[3] = T8TextFieldDataEntityEditorDefinition.TYPE_IDENTIFIER;
            dataRow[4] = fieldIdentifier;

            tableModel.addRow(dataRow);
        }

        CellRendererUtilities.autoSizeTableColumns(jTableFieldSetup, 100);
    }

    public T8PanelDataEntityEditorSetup getPanelSetup()
    {
        if (proceed)
        {
            T8PanelDataEntityEditorSetup setup;
            DefaultTableModel tableModel;
            List<T8PanelDataEntityEditorFieldSetup> fieldSetup;
            Insets insets;

            // Stop all editing.
            if (jTableFieldSetup.getCellEditor() != null) jTableFieldSetup.getCellEditor().stopCellEditing();

            // Create and popuplate the panel setup object.
            setup = new T8PanelDataEntityEditorSetup();
            fieldSetup = new ArrayList<T8PanelDataEntityEditorFieldSetup>();
            tableModel = (DefaultTableModel)jTableFieldSetup.getModel();
            for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
            {
                String fieldIdentifier;
                String editorIdentifier;
                boolean addEditor;
                String editorTypeIdentifier;
                String editorLabel;

                fieldIdentifier = (String)tableModel.getValueAt(rowIndex, 0);
                editorIdentifier = (String)tableModel.getValueAt(rowIndex, 1);
                addEditor = (Boolean)tableModel.getValueAt(rowIndex, 2);
                editorTypeIdentifier = (String)tableModel.getValueAt(rowIndex, 3);
                editorLabel = (String)tableModel.getValueAt(rowIndex, 4);

                if (addEditor)
                {
                    fieldSetup.add(new T8PanelDataEntityEditorFieldSetup(fieldIdentifier, editorIdentifier, editorTypeIdentifier, editorLabel));
                }
            }

            // Commit all formatted text field values.
            try
            {
                jFormattedTextFieldInsetLeft.commitEdit();
                jFormattedTextFieldInsetTop.commitEdit();
                jFormattedTextFieldInsetRight.commitEdit();
                jFormattedTextFieldInsetBottom.commitEdit();
                jFormattedTextFieldHorizontalSpacing.commitEdit();
                jFormattedTextFieldVerticalSpacing.commitEdit();
            }
            catch (Exception e)
            {
                T8Log.log("Exception while committing formatted text field value.", e);
            }

            setup.setFieldSetup(fieldSetup);
            insets = new Insets(0, 0, 0, 0);
            insets.left = ((Number)jFormattedTextFieldInsetLeft.getValue()).intValue();
            insets.top = ((Number)jFormattedTextFieldInsetTop.getValue()).intValue();
            insets.right = ((Number)jFormattedTextFieldInsetRight.getValue()).intValue();
            insets.bottom = ((Number)jFormattedTextFieldInsetBottom.getValue()).intValue();
            setup.setInsets(insets);
            setup.setAddLabels(jCheckBoxAddLabels.isSelected());
            setup.setHorizontalSpacing(((Number)jFormattedTextFieldHorizontalSpacing.getValue()).intValue());
            setup.setVerticalSpacing(((Number)jFormattedTextFieldVerticalSpacing.getValue()).intValue());
            setup.setLabelVerticalTextAlignment((VerticalTextAlignment)jComboBoxLabelVerticalTextAlignment.getSelectedItem());
            setup.setLabelHorizontalTextAlignment((HorizontalTextAlignment)jComboBoxLabelHorizontalTextAlignment.getSelectedItem());

            return setup;
        }
        else return null;
    }

    public static final T8PanelDataEntityEditorSetup getPanelSetup(T8Context context, List<T8DataEntityFieldDefinition> fieldDefinitions)
    {
        T8PanelDataEntityEditorFieldSetupDialog dialog;

        dialog = new T8PanelDataEntityEditorFieldSetupDialog(context, fieldDefinitions);
        dialog.setVisible(true);
        return dialog.getPanelSetup();
    }

    private class EditorTypeCellEditor extends AbstractCellEditor implements TableCellEditor
    {
        private JComboBox cellEditor;

        public EditorTypeCellEditor()
        {
            cellEditor = new JComboBox();
            cellEditor.addItem(T8TextFieldDataEntityEditorDefinition.TYPE_IDENTIFIER);
            cellEditor.addItem(T8TextAreaDataEntityEditorDefinition.TYPE_IDENTIFIER);
            cellEditor.addItem(T8DataComboBoxDataEntityEditorDefinition.TYPE_IDENTIFIER);
            cellEditor.addItem(T8DataLookupFieldDataEntityEditorDefinition.TYPE_IDENTIFIER);
            cellEditor.addItem(T8ConceptLabelDataEntityEditorDefinition.TYPE_IDENTIFIER);
            cellEditor.addItem(T8ConceptTypeLabelDataEntityEditorDefinition.TYPE_IDENTIFIER);
        }

        @Override
        public Object getCellEditorValue()
        {
            return cellEditor.getSelectedItem();
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
        {
            cellEditor.setSelectedItem(value);
            return cellEditor;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelLayoutSetup = new javax.swing.JPanel();
        jLabelAddLabels = new javax.swing.JLabel();
        jCheckBoxAddLabels = new javax.swing.JCheckBox();
        jLabelLabelHorizontalTextAlignment = new javax.swing.JLabel();
        jComboBoxLabelHorizontalTextAlignment = new javax.swing.JComboBox();
        jLabelInsetLeft = new javax.swing.JLabel();
        jFormattedTextFieldInsetLeft = new javax.swing.JFormattedTextField();
        jLabelInsetTop = new javax.swing.JLabel();
        jFormattedTextFieldInsetTop = new javax.swing.JFormattedTextField();
        jLabelInsetRight = new javax.swing.JLabel();
        jFormattedTextFieldInsetRight = new javax.swing.JFormattedTextField();
        jLabelInsetBottom = new javax.swing.JLabel();
        jFormattedTextFieldInsetBottom = new javax.swing.JFormattedTextField();
        jLabelVerticalSpacing = new javax.swing.JLabel();
        jFormattedTextFieldVerticalSpacing = new javax.swing.JFormattedTextField();
        jLabelHorizontalSpacing = new javax.swing.JLabel();
        jFormattedTextFieldHorizontalSpacing = new javax.swing.JFormattedTextField();
        jComboBoxLabelVerticalTextAlignment = new javax.swing.JComboBox();
        jLabelLabelVerticalTextAlignment = new javax.swing.JLabel();
        jPanelFieldSetup = new javax.swing.JPanel();
        jScrollPaneFieldSetupTable = new javax.swing.JScrollPane();
        jTableFieldSetup = new javax.swing.JTable();
        jPanelControls = new javax.swing.JPanel();
        jButtonCancel = new javax.swing.JButton();
        jButtonProceed = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelLayoutSetup.setLayout(new java.awt.GridBagLayout());

        jLabelAddLabels.setText("Add Labels:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 1, 0);
        jPanelLayoutSetup.add(jLabelAddLabels, gridBagConstraints);

        jCheckBoxAddLabels.setSelected(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jPanelLayoutSetup.add(jCheckBoxAddLabels, gridBagConstraints);

        jLabelLabelHorizontalTextAlignment.setText("Label Text Horizontal Alignment:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 10, 1, 0);
        jPanelLayoutSetup.add(jLabelLabelHorizontalTextAlignment, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutSetup.add(jComboBoxLabelHorizontalTextAlignment, gridBagConstraints);

        jLabelInsetLeft.setText("Inset Left:");
        jLabelInsetLeft.setMaximumSize(null);
        jLabelInsetLeft.setMinimumSize(null);
        jLabelInsetLeft.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 10, 1, 0);
        jPanelLayoutSetup.add(jLabelInsetLeft, gridBagConstraints);

        jFormattedTextFieldInsetLeft.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldInsetLeft.setText("10");
        jFormattedTextFieldInsetLeft.setMaximumSize(null);
        jFormattedTextFieldInsetLeft.setMinimumSize(null);
        jFormattedTextFieldInsetLeft.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutSetup.add(jFormattedTextFieldInsetLeft, gridBagConstraints);

        jLabelInsetTop.setText("Inset Top:");
        jLabelInsetTop.setMaximumSize(null);
        jLabelInsetTop.setMinimumSize(null);
        jLabelInsetTop.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 10, 1, 0);
        jPanelLayoutSetup.add(jLabelInsetTop, gridBagConstraints);

        jFormattedTextFieldInsetTop.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldInsetTop.setText("10");
        jFormattedTextFieldInsetTop.setMaximumSize(null);
        jFormattedTextFieldInsetTop.setMinimumSize(null);
        jFormattedTextFieldInsetTop.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutSetup.add(jFormattedTextFieldInsetTop, gridBagConstraints);

        jLabelInsetRight.setText("Inset Right:");
        jLabelInsetRight.setMaximumSize(null);
        jLabelInsetRight.setMinimumSize(null);
        jLabelInsetRight.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 10, 1, 0);
        jPanelLayoutSetup.add(jLabelInsetRight, gridBagConstraints);

        jFormattedTextFieldInsetRight.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldInsetRight.setText("10");
        jFormattedTextFieldInsetRight.setMaximumSize(null);
        jFormattedTextFieldInsetRight.setMinimumSize(null);
        jFormattedTextFieldInsetRight.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutSetup.add(jFormattedTextFieldInsetRight, gridBagConstraints);

        jLabelInsetBottom.setText("Inset Bottom:");
        jLabelInsetBottom.setMaximumSize(null);
        jLabelInsetBottom.setMinimumSize(null);
        jLabelInsetBottom.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 10, 1, 0);
        jPanelLayoutSetup.add(jLabelInsetBottom, gridBagConstraints);

        jFormattedTextFieldInsetBottom.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldInsetBottom.setText("10");
        jFormattedTextFieldInsetBottom.setMaximumSize(null);
        jFormattedTextFieldInsetBottom.setMinimumSize(null);
        jFormattedTextFieldInsetBottom.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutSetup.add(jFormattedTextFieldInsetBottom, gridBagConstraints);

        jLabelVerticalSpacing.setText("Vertical Spacing:");
        jLabelVerticalSpacing.setMaximumSize(null);
        jLabelVerticalSpacing.setMinimumSize(null);
        jLabelVerticalSpacing.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 10, 10, 0);
        jPanelLayoutSetup.add(jLabelVerticalSpacing, gridBagConstraints);

        jFormattedTextFieldVerticalSpacing.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldVerticalSpacing.setText("10");
        jFormattedTextFieldVerticalSpacing.setMaximumSize(null);
        jFormattedTextFieldVerticalSpacing.setMinimumSize(null);
        jFormattedTextFieldVerticalSpacing.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 10, 5);
        jPanelLayoutSetup.add(jFormattedTextFieldVerticalSpacing, gridBagConstraints);

        jLabelHorizontalSpacing.setText("Horizontal Spacing:");
        jLabelHorizontalSpacing.setMaximumSize(null);
        jLabelHorizontalSpacing.setMinimumSize(null);
        jLabelHorizontalSpacing.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 10, 1, 0);
        jPanelLayoutSetup.add(jLabelHorizontalSpacing, gridBagConstraints);

        jFormattedTextFieldHorizontalSpacing.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldHorizontalSpacing.setText("10");
        jFormattedTextFieldHorizontalSpacing.setMaximumSize(null);
        jFormattedTextFieldHorizontalSpacing.setMinimumSize(null);
        jFormattedTextFieldHorizontalSpacing.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutSetup.add(jFormattedTextFieldHorizontalSpacing, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutSetup.add(jComboBoxLabelVerticalTextAlignment, gridBagConstraints);

        jLabelLabelVerticalTextAlignment.setText("Label Text Vertical Alignment:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 10, 1, 0);
        jPanelLayoutSetup.add(jLabelLabelVerticalTextAlignment, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        getContentPane().add(jPanelLayoutSetup, gridBagConstraints);

        jPanelFieldSetup.setLayout(new java.awt.GridBagLayout());

        jTableFieldSetup.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Entity Field Identifier", "Editor Identifier", "Add Editor", "Editor Type", "Editor Label"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.Boolean.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true, true, true, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableFieldSetup.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPaneFieldSetupTable.setViewportView(jTableFieldSetup);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelFieldSetup.add(jScrollPaneFieldSetupTable, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jPanelFieldSetup, gridBagConstraints);

        jPanelControls.setLayout(new java.awt.GridBagLayout());

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        jPanelControls.add(jButtonCancel, gridBagConstraints);

        jButtonProceed.setText("Proceed");
        jButtonProceed.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonProceedActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.1;
        jPanelControls.add(jButtonProceed, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 10, 9);
        getContentPane().add(jPanelControls, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonProceedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonProceedActionPerformed
    {//GEN-HEADEREND:event_jButtonProceedActionPerformed
        proceed = true;
        dispose();
    }//GEN-LAST:event_jButtonProceedActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCancelActionPerformed
    {//GEN-HEADEREND:event_jButtonCancelActionPerformed
        proceed = false;
        dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonProceed;
    private javax.swing.JCheckBox jCheckBoxAddLabels;
    private javax.swing.JComboBox jComboBoxLabelHorizontalTextAlignment;
    private javax.swing.JComboBox jComboBoxLabelVerticalTextAlignment;
    private javax.swing.JFormattedTextField jFormattedTextFieldHorizontalSpacing;
    private javax.swing.JFormattedTextField jFormattedTextFieldInsetBottom;
    private javax.swing.JFormattedTextField jFormattedTextFieldInsetLeft;
    private javax.swing.JFormattedTextField jFormattedTextFieldInsetRight;
    private javax.swing.JFormattedTextField jFormattedTextFieldInsetTop;
    private javax.swing.JFormattedTextField jFormattedTextFieldVerticalSpacing;
    private javax.swing.JLabel jLabelAddLabels;
    private javax.swing.JLabel jLabelHorizontalSpacing;
    private javax.swing.JLabel jLabelInsetBottom;
    private javax.swing.JLabel jLabelInsetLeft;
    private javax.swing.JLabel jLabelInsetRight;
    private javax.swing.JLabel jLabelInsetTop;
    private javax.swing.JLabel jLabelLabelHorizontalTextAlignment;
    private javax.swing.JLabel jLabelLabelVerticalTextAlignment;
    private javax.swing.JLabel jLabelVerticalSpacing;
    private javax.swing.JPanel jPanelControls;
    private javax.swing.JPanel jPanelFieldSetup;
    private javax.swing.JPanel jPanelLayoutSetup;
    private javax.swing.JScrollPane jScrollPaneFieldSetupTable;
    private javax.swing.JTable jTableFieldSetup;
    // End of variables declaration//GEN-END:variables
}
