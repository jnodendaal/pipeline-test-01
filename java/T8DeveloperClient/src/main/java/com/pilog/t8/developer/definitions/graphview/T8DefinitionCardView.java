package com.pilog.t8.developer.definitions.graphview;

import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.view.GraphSelectionEvent;
import com.pilog.graph.view.GraphSelectionListener;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionSelectionEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.ui.event.T8ViewHeaderChangedEvent;
import com.pilog.t8.definition.graph.T8GraphDefinition;
import com.pilog.t8.developer.definitions.navigatorview.T8DefinitionNavigatorView;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import com.pilog.t8.developer.utils.T8DefinitionValidationUtils;
import com.pilog.t8.developer.view.T8DefaultDeveloperView;
import com.pilog.t8.developer.view.T8DefaultDeveloperViewFactory;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionCardView extends T8DefaultDeveloperView implements T8DeveloperView
{
    private final TreeViewSelectionListener treeViewListener;
    private final T8DefinitionContext definitionContext;
    private final T8DefinitionManager definitionManager;
    private final T8ClientContext clientContext;
    private final T8DeveloperView parentView;
    private final T8Context context;
    private final T8DefinitionGraph graphView;
    private final T8DefinitionNavigatorView treeView;
    private T8DeveloperViewFactory viewFactory;
    private T8Definition definition;
    private ViewType currentView;

    private enum ViewType {GRAPH, TREE};
    public enum DefinitionGraphToolBarOption {LOAD, SAVE, PRINT, COPY, RENAME, TEST, CONFIGURE_TEST, REFRESH, CLEANUP};

    public T8DefinitionCardView(T8DefinitionContext definitionContext, T8DeveloperView parentView)
    {
        initComponents();
        this.clientContext = definitionContext.getClientContext();
        this.definitionManager = clientContext.getDefinitionManager();
        this.context = definitionContext.getContext();
        this.definitionContext = definitionContext;
        this.parentView = parentView;
        this.treeViewListener = new TreeViewSelectionListener();
        this.graphView = new T8DefinitionGraph(definitionContext, null);
        this.graphView.addGraphSelectionListener(new DefinitionGraphSelectionListener());
        this.graphView.setGridXSize(20);
        this.graphView.setGridYSize(20);
        this.treeView = new T8DefinitionNavigatorView(definitionContext, parentView, definition);
        this.treeView.setBorderVisible(false);
        this.treeView.addDefinitionViewListener(treeViewListener);
        this.treeView.addToolbarButton(jButtonGraphView);
        this.viewFactory = parentView != null ? parentView.getViewFactory() : new T8DefaultDeveloperViewFactory(definitionContext);
    }

    @Override
    public String getHeader()
    {
        if (definition != null)
        {
            return definition.getTypeMetaData().getDisplayName() + ": " + definition.getIdentifier();
        }
        else return "Definition Editor";
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return definition;
    }

    @Override
    public void setSelectedDefinition(T8Definition selectedDefinition)
    {
        definition = selectedDefinition;
        if (selectedDefinition instanceof T8GraphDefinition)
        {
            setBorder(new T8ContentHeaderBorder(definition.getTypeMetaData().getDisplayName() + " Editor"));
            graphView.setGraphDefinition((T8GraphDefinition)definition);
            treeView.setRootDefinition(definition);
            jButtonGraphView.setVisible(true);
            switchToGraphView();
        }
        else if (selectedDefinition != null)
        {
            setBorder(new T8ContentHeaderBorder(definition.getTypeMetaData().getDisplayName() + " Editor"));
            graphView.setGraphDefinition(null);
            treeView.setRootDefinition(selectedDefinition);
            jButtonGraphView.setVisible(false);
            switchToTreeView();
        }
        else
        {
            setBorder(new T8ContentHeaderBorder("Definition Editor"));
            graphView.setGraphDefinition(null);
            treeView.setRootDefinition(null);
            jButtonGraphView.setVisible(false);
            switchToTreeView();
        }
    }

    public void setToolBarOptionsVisible(List<DefinitionGraphToolBarOption> options, boolean visible)
    {
        for (DefinitionGraphToolBarOption option : options)
        {
            switch (option)
            {
                case SAVE:
                    jButtonSaveDefinition.setVisible(visible);
                    break;
                case RENAME:
                    jButtonRenameDefinition.setVisible(visible);
                    break;
                case COPY:
                    jButtonCopyDefinition.setVisible(visible);
                    break;
                case CONFIGURE_TEST:
                    jButtonConfigureTestHarness.setVisible(visible);
                    break;
                case PRINT:
                    jButtonPrint.setVisible(visible);
                    break;
                case REFRESH:
                    jButtonRefreshGraph.setVisible(visible);
                    break;
                case CLEANUP:
                    jButtonCleanup.setVisible(visible);
                    break;
                default:
                    break;
            }
        }
    }

    public void rebuildGraph()
    {
        graphView.rebuildModel();
    }

    private void saveDefinition()
    {
        try
        {
            T8Definition definition;

            definition = graphView.getGraphDefinition();
            definitionManager.saveDefinition(context, definition, null);
            JOptionPane.showMessageDialog(null, "Definition '" + definition.getIdentifier() + "' has been successfully saved.", "Save Successful", JOptionPane.INFORMATION_MESSAGE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "An unexpected error prevented the successful storage of the Definition.", "Save Failure", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    private void validateDefinition()
    {
        try
        {
            T8DefinitionValidationUtils.validateDefinitions(SwingUtilities.windowForComponent(this), this.parentView, context, ArrayLists.typeSafeList(definition.getIdentifier()));
        }
        catch (Exception e)
        {
            T8Log.log("Exception while validating definitions: " + definition, e);
            JOptionPane.showMessageDialog(this, "An unexpected exception occurred.", "Unexpected Exception", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void addToolbarButton(JButton button)
    {
        jToolBarMain.add(button);
    }

    private void copyDefinition()
    {
        T8DefinitionMetaData metaData;

        metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this.getRootPane(), definition.getTypeMetaData(), definition.getParentDefinition());
        if (metaData != null)
        {
            try
            {
                T8Definition newDefinition;

                newDefinition = definitionManager.copyDefinition(context, definition, metaData.getId());
                newDefinition.setProjectIdentifier(metaData.getProjectId());
                parentView.addView(viewFactory.createDefinitionGraph(parentView, (T8GraphDefinition)newDefinition));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private void renameDefinition()
    {
        T8DefinitionMetaData metaData;

        metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this.getRootPane(), definition.getTypeMetaData(), definition.getParentDefinition());
        if (metaData != null)
        {
            try
            {
                definitionManager.renameDefinition(context, definition, metaData.getId());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private void cleanupDefinition()
    {
        if (definition instanceof T8GraphDefinition)
        {
            T8GraphDefinition graphDefinition;

            graphDefinition = (T8GraphDefinition)definition;
            graphDefinition.removeLooseEdges();
            graphDefinition.removeLooseNodes();
            rebuildGraph();
        }
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        parentView.addView(view);
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        parentView.removeView(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
        parentView.notifyDefinitionLinkActivated(event);
    }

    private void setGridSnapEnabled(boolean enabled)
    {
        graphView.setGridEnabled(enabled);
        graphView.setSnapToGridEnabled(enabled);
        graphView.repaint();
    }

    public void configureGraph()
    {
        Component configurationComponent;

        configurationComponent = graphView.getGraphManager().getConfigurationComponent();
        if (configurationComponent != null)
        {
            T8GraphConfigurationDialog.showConfigurationDialog(SwingUtilities.getWindowAncestor(this), configurationComponent);
        }
    }

    private void switchToTreeView()
    {
        currentView = ViewType.TREE;
        remove(graphView);
        add(treeView, BorderLayout.CENTER);
        jToolBarMain.setVisible(false);
        revalidate();
        repaint();
    }

    private void switchToGraphView()
    {
        currentView = ViewType.GRAPH;
        remove(treeView);
        add(graphView, BorderLayout.CENTER);
        jToolBarMain.setVisible(true);
        validate();
        repaint();
    }

    private class TreeViewSelectionListener implements T8DefinitionViewListener
    {

        @Override
        public void selectionChanged(T8DefinitionSelectionEvent event)
        {
            fireDefinitionSelectionChangedEvent(event.getNewSelection());
        }

        @Override
        public void definitionLinkActivated(T8DefinitionLinkEvent event)
        {
        }

        @Override
        public void headerChanged(T8ViewHeaderChangedEvent event)
        {
        }
    }

    private class DefinitionGraphSelectionListener implements GraphSelectionListener
    {
        @Override
        public void selectionChanged(GraphSelectionEvent gse)
        {
            List<GraphVertex> selectedVertices;

            selectedVertices = graphView.getSelectedVertices();
            if (selectedVertices.size() > 0)
            {
                Object selectedObject;

                selectedObject = selectedVertices.get(0).getVertexDataObject();
                if (selectedObject instanceof T8Definition)
                {
                    fireDefinitionSelectionChangedEvent((T8Definition)selectedObject);
                }
            }
            else
            {
                fireDefinitionSelectionChangedEvent((T8Definition)graphView.getGraphDefinition());
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jButtonGraphView = new javax.swing.JButton();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonPrint = new javax.swing.JButton();
        jButtonCopyDefinition = new javax.swing.JButton();
        jButtonRenameDefinition = new javax.swing.JButton();
        jButtonSaveDefinition = new javax.swing.JButton();
        jButtonCleanup = new javax.swing.JButton();
        jButtonRefreshGraph = new javax.swing.JButton();
        jButtonConfigureTestHarness = new javax.swing.JButton();
        jToggleButtonGridSnap = new javax.swing.JToggleButton();
        jButtonCenterGraph = new javax.swing.JButton();
        jButtonValidateDefinition = new javax.swing.JButton();
        jButtonTreeView = new javax.swing.JButton();

        jButtonGraphView.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/flowViewIcon.png"))); // NOI18N
        jButtonGraphView.setToolTipText("Flow View");
        jButtonGraphView.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonGraphView.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonGraphViewActionPerformed(evt);
            }
        });

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/printIcon.png"))); // NOI18N
        jButtonPrint.setToolTipText("Print Workflow");
        jButtonPrint.setFocusable(false);
        jButtonPrint.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonPrint.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonPrint.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPrint.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPrintActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonPrint);

        jButtonCopyDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/documentCopyIcon.png"))); // NOI18N
        jButtonCopyDefinition.setToolTipText("Copy Definition");
        jButtonCopyDefinition.setFocusable(false);
        jButtonCopyDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCopyDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCopyDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCopyDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCopyDefinition);

        jButtonRenameDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/renameDocumentIcon.png"))); // NOI18N
        jButtonRenameDefinition.setToolTipText("Rename Definition");
        jButtonRenameDefinition.setFocusable(false);
        jButtonRenameDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRenameDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRenameDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRenameDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRenameDefinition);

        jButtonSaveDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/saveChangesIcon.png"))); // NOI18N
        jButtonSaveDefinition.setToolTipText("Save Definition");
        jButtonSaveDefinition.setFocusable(false);
        jButtonSaveDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSaveDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSaveDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSaveDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSaveDefinition);

        jButtonCleanup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/broomIcon.png"))); // NOI18N
        jButtonCleanup.setToolTipText("Remove loose edges and nodes.");
        jButtonCleanup.setFocusable(false);
        jButtonCleanup.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCleanup.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonCleanup.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCleanup.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCleanupActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCleanup);

        jButtonRefreshGraph.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonRefreshGraph.setToolTipText("Refresh Graph");
        jButtonRefreshGraph.setFocusable(false);
        jButtonRefreshGraph.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefreshGraph.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefreshGraph.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshGraphActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefreshGraph);

        jButtonConfigureTestHarness.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/configurationIcon.png"))); // NOI18N
        jButtonConfigureTestHarness.setToolTipText("Configure Graph");
        jButtonConfigureTestHarness.setFocusable(false);
        jButtonConfigureTestHarness.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonConfigureTestHarness.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonConfigureTestHarness.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonConfigureTestHarnessActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonConfigureTestHarness);

        jToggleButtonGridSnap.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/gridSnapIcon.png"))); // NOI18N
        jToggleButtonGridSnap.setToolTipText("Toggle Grid-Snap");
        jToggleButtonGridSnap.setFocusable(false);
        jToggleButtonGridSnap.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jToggleButtonGridSnap.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButtonGridSnap.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jToggleButtonGridSnapActionPerformed(evt);
            }
        });
        jToolBarMain.add(jToggleButtonGridSnap);

        jButtonCenterGraph.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowIn.png"))); // NOI18N
        jButtonCenterGraph.setToolTipText("Center Graph");
        jButtonCenterGraph.setFocusable(false);
        jButtonCenterGraph.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCenterGraph.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCenterGraph.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCenterGraphActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCenterGraph);

        jButtonValidateDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/bugExclamationIcon.png"))); // NOI18N
        jButtonValidateDefinition.setToolTipText("Validate Definition");
        jButtonValidateDefinition.setFocusable(false);
        jButtonValidateDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonValidateDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonValidateDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonValidateDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonValidateDefinition);

        jButtonTreeView.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/nodeSelectChildIcon.png"))); // NOI18N
        jButtonTreeView.setToolTipText("Tree View");
        jButtonTreeView.setFocusable(false);
        jButtonTreeView.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonTreeView.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonTreeView.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonTreeViewActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonTreeView);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSaveDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSaveDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonSaveDefinitionActionPerformed
        saveDefinition();
    }//GEN-LAST:event_jButtonSaveDefinitionActionPerformed

    private void jButtonCopyDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCopyDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonCopyDefinitionActionPerformed
        copyDefinition();
    }//GEN-LAST:event_jButtonCopyDefinitionActionPerformed

    private void jButtonRenameDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRenameDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonRenameDefinitionActionPerformed
        renameDefinition();
    }//GEN-LAST:event_jButtonRenameDefinitionActionPerformed

    private void jButtonPrintActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPrintActionPerformed
    {//GEN-HEADEREND:event_jButtonPrintActionPerformed
        T8PrintPreviewDialog.printComponent(SwingUtilities.getWindowAncestor(this), graphView);
    }//GEN-LAST:event_jButtonPrintActionPerformed

    private void jButtonCleanupActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCleanupActionPerformed
    {//GEN-HEADEREND:event_jButtonCleanupActionPerformed
        cleanupDefinition();
    }//GEN-LAST:event_jButtonCleanupActionPerformed

    private void jButtonConfigureTestHarnessActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonConfigureTestHarnessActionPerformed
    {//GEN-HEADEREND:event_jButtonConfigureTestHarnessActionPerformed
        configureGraph();
    }//GEN-LAST:event_jButtonConfigureTestHarnessActionPerformed

    private void jButtonRefreshGraphActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshGraphActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshGraphActionPerformed
        rebuildGraph();
    }//GEN-LAST:event_jButtonRefreshGraphActionPerformed

    private void jToggleButtonGridSnapActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jToggleButtonGridSnapActionPerformed
    {//GEN-HEADEREND:event_jToggleButtonGridSnapActionPerformed
        setGridSnapEnabled(jToggleButtonGridSnap.isSelected());
    }//GEN-LAST:event_jToggleButtonGridSnapActionPerformed

    private void jButtonValidateDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonValidateDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonValidateDefinitionActionPerformed
        validateDefinition();
    }//GEN-LAST:event_jButtonValidateDefinitionActionPerformed

    private void jButtonTreeViewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonTreeViewActionPerformed
    {//GEN-HEADEREND:event_jButtonTreeViewActionPerformed
        switchToTreeView();
    }//GEN-LAST:event_jButtonTreeViewActionPerformed

    private void jButtonGraphViewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonGraphViewActionPerformed
    {//GEN-HEADEREND:event_jButtonGraphViewActionPerformed
        switchToGraphView();
    }//GEN-LAST:event_jButtonGraphViewActionPerformed

    private void jButtonCenterGraphActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCenterGraphActionPerformed
    {//GEN-HEADEREND:event_jButtonCenterGraphActionPerformed
        this.graphView.centerGraph();
    }//GEN-LAST:event_jButtonCenterGraphActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCenterGraph;
    private javax.swing.JButton jButtonCleanup;
    private javax.swing.JButton jButtonConfigureTestHarness;
    private javax.swing.JButton jButtonCopyDefinition;
    private javax.swing.JButton jButtonGraphView;
    private javax.swing.JButton jButtonPrint;
    private javax.swing.JButton jButtonRefreshGraph;
    private javax.swing.JButton jButtonRenameDefinition;
    private javax.swing.JButton jButtonSaveDefinition;
    private javax.swing.JButton jButtonTreeView;
    private javax.swing.JButton jButtonValidateDefinition;
    private javax.swing.JToggleButton jToggleButtonGridSnap;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
