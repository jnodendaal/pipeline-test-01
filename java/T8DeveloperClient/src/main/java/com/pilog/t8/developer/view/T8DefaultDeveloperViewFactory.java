package com.pilog.t8.developer.view;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.definition.graph.T8GraphDefinition;
import com.pilog.t8.developer.definitions.compositionview.T8DefinitionCompositionView;
import com.pilog.t8.developer.definitions.graphview.T8DefinitionCardView;
import com.pilog.t8.developer.definitions.graphview.T8DefinitionCardView.DefinitionGraphToolBarOption;
import com.pilog.t8.developer.definitions.navigatorview.T8DefinitionNavigatorView;
import com.pilog.t8.developer.definitions.navigatorview.T8DefinitionNavigatorView.NavigatorToolBarOption;
import com.pilog.t8.developer.definitions.projectmanager.T8ProjectManagerView;
import com.pilog.t8.developer.definitions.projecteditor.T8ProjectEditor;
import com.pilog.t8.developer.definitions.tableview.T8DefinitionSearchView;
import com.pilog.t8.developer.definitions.tableview.T8DefinitionTableView;
import com.pilog.t8.developer.definitions.tableview.T8DefinitionUsagesView;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Arrays;

/**
 * This class is used by the developer framework to construct developer views of
 * certain types.  By extending this class and overriding the methods it
 * contains, the layout and composition of the default developer views can be
 * altered to comply to more specific requirements.
 *
 * @author Bouwer du Preez
 */
public class T8DefaultDeveloperViewFactory implements T8DeveloperViewFactory
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultDeveloperViewFactory.class);

    private final T8DefinitionContext definitionContext;
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8Context context;

    private static final String MAIN_SPLIT_LAYOUT_STRING = "(ROW (LEAF name=left weight=0.25) (LEAF name=right weight=0.75))";
    private static final String EDITOR_SPLIT_LAYOUT_STRING = "(ROW (LEAF name=left weight=0.5) (LEAF name=right weight=0.5))";
    private static final String PROJECT_MANAGER_SPLIT_LAYOUT_STRING = "(ROW (LEAF name=left weight=0.60) (LEAF name=right weight=0.40))";

    public T8DefaultDeveloperViewFactory(T8DefinitionContext definitionContext)
    {
        this.definitionContext = definitionContext;
        this.context = definitionContext.getContext();
        this.clientContext = definitionContext.getClientContext();
        this.sessionContext = definitionContext.getSessionContext();
    }

    @Override
    public T8DeveloperView createDefinitionNavigator(T8DeveloperView parentView, T8Definition newDefinition)
    {
        T8DefinitionNavigatorView definitionNavigator;

        definitionNavigator = new T8DefinitionNavigatorView(definitionContext, parentView, newDefinition);
        return new T8MultiSplitDefinitionView(definitionContext, parentView, definitionNavigator);
    }

    @Override
    public T8DeveloperView createReadOnlyDefinitionNavigator(T8DeveloperView parentView, T8Definition newDefinition)
    {
        T8DefinitionNavigatorView definitionNavigator;

        definitionNavigator = new T8DefinitionNavigatorView(definitionContext, parentView, newDefinition);
        definitionNavigator.setToolBarOptionsVisible(Arrays.asList(NavigatorToolBarOption.EDIT, NavigatorToolBarOption.SAVE, NavigatorToolBarOption.RENAME, NavigatorToolBarOption.COPY), false);
        return new T8MultiSplitDefinitionView(definitionContext, parentView, definitionNavigator);
    }

    @Override
    public T8DeveloperView createDefinitionGraph(T8DeveloperView parentView, T8GraphDefinition newDefinition)
    {
        T8DefinitionCardView definitionGraphView;

        definitionGraphView = new T8DefinitionCardView(definitionContext, parentView);
        definitionGraphView.setSelectedDefinition(newDefinition);
        return new T8MultiSplitDefinitionView(definitionContext, parentView, definitionGraphView);
    }

    @Override
    public T8DeveloperView createDefinitionGraphManager(T8DeveloperView parentView, String groupIdentifier)
    {
        T8MultiSplitDefinitionView splitPaneView;
        T8DefinitionCardView graphView;
        T8DeveloperView leftView;
        T8DeveloperView rightView;

        graphView = new T8DefinitionCardView(definitionContext, parentView);
        graphView.setToolBarOptionsVisible(ArrayLists.typeSafeList(DefinitionGraphToolBarOption.COPY, DefinitionGraphToolBarOption.LOAD, DefinitionGraphToolBarOption.RENAME, DefinitionGraphToolBarOption.SAVE), false);

        leftView = new T8DefinitionTableView(definitionContext, parentView, groupIdentifier, false);
        rightView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", graphView, "right", new T8DefinitionEditorView(definitionContext, parentView)), EDITOR_SPLIT_LAYOUT_STRING, "left");
        splitPaneView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", leftView, "right", rightView), MAIN_SPLIT_LAYOUT_STRING, "left");
        return splitPaneView;
    }

    @Override
    public T8DeveloperView createDefinitionGroupManager(T8DeveloperView parentView, String groupIdentifier)
    {
        T8MultiSplitDefinitionView splitPaneView;
        T8DefinitionNavigatorView definitionNavigator;
        T8DeveloperView leftView;
        T8DeveloperView rightView;

        definitionNavigator = new T8DefinitionNavigatorView(definitionContext, parentView, null);
        definitionNavigator.setToolBarOptionsVisible(ArrayLists.newArrayList(NavigatorToolBarOption.COPY, NavigatorToolBarOption.SAVE, NavigatorToolBarOption.EDIT, NavigatorToolBarOption.RENAME), false);

        leftView = new T8DefinitionTableView(definitionContext, parentView, groupIdentifier, false);
        rightView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", definitionNavigator, "right", new T8DefinitionEditorView(definitionContext, parentView)), EDITOR_SPLIT_LAYOUT_STRING, "left");
        splitPaneView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", leftView, "right", rightView), MAIN_SPLIT_LAYOUT_STRING, "left");
        return splitPaneView;
    }

    @Override
    public T8DeveloperView createProjectEditor(T8DeveloperView parentView, String projectId)
    {
        T8MultiSplitDefinitionView splitPaneView;
        T8DefinitionCardView definitionView;
        T8DeveloperView leftView;
        T8DeveloperView rightView;

        definitionView = new T8DefinitionCardView(definitionContext, parentView);
        definitionView.setToolBarOptionsVisible(ArrayLists.newArrayList(DefinitionGraphToolBarOption.COPY, DefinitionGraphToolBarOption.SAVE, DefinitionGraphToolBarOption.RENAME), false);

        leftView = new T8ProjectEditor(definitionContext, parentView, projectId);
        rightView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", definitionView, "right", new T8DefinitionEditorView(definitionContext, parentView)), EDITOR_SPLIT_LAYOUT_STRING, "left");
        splitPaneView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", leftView, "right", rightView), MAIN_SPLIT_LAYOUT_STRING, "left");
        return splitPaneView;
    }

    @Override
    public T8DeveloperView createProjectManager(T8DeveloperView parentView)
    {
        T8MultiSplitDefinitionView splitPaneView;
        T8ProjectManagerView projectManagerView;
        T8ProjectInformationView projectInformationView;

        projectManagerView = new T8ProjectManagerView(definitionContext, parentView);
        projectInformationView = new T8ProjectInformationView(context, this);
        splitPaneView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", projectManagerView, "right", projectInformationView), PROJECT_MANAGER_SPLIT_LAYOUT_STRING, "left");
        return splitPaneView;
    }

    @Override
    public T8DeveloperView createDefinitionCompositionView(T8DeveloperView parentView, String projectId, String definitionId)
    {
        T8MultiSplitDefinitionView splitPaneView;
        T8DefinitionNavigatorView definitionNavigator;
        T8DeveloperView leftView;
        T8DeveloperView rightView;

        definitionNavigator = new T8DefinitionNavigatorView(definitionContext, parentView, null);
        definitionNavigator.setToolBarOptionsVisible(ArrayLists.newArrayList(NavigatorToolBarOption.COPY, NavigatorToolBarOption.SAVE, NavigatorToolBarOption.EDIT, NavigatorToolBarOption.RENAME), false);

        leftView = new T8DefinitionCompositionView(definitionContext, parentView, projectId, definitionId);
        rightView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", definitionNavigator, "right", new T8DefinitionEditorView(definitionContext, parentView)), EDITOR_SPLIT_LAYOUT_STRING, "left");
        splitPaneView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", leftView, "right", rightView), MAIN_SPLIT_LAYOUT_STRING, "left");
        return splitPaneView;
    }

    @Override
    public T8DeveloperView createDefinitionUsagesManager(T8DeveloperView parentView, T8Definition definition)
    {
        T8MultiSplitDefinitionView splitPaneView;
        T8DefinitionNavigatorView definitionNavigator;
        T8DeveloperView leftView;
        T8DeveloperView rightView;

        definitionNavigator = new T8DefinitionNavigatorView(definitionContext, parentView, null);
        definitionNavigator.setToolBarOptionsVisible(ArrayLists.newArrayList(NavigatorToolBarOption.COPY, NavigatorToolBarOption.SAVE, NavigatorToolBarOption.EDIT, NavigatorToolBarOption.RENAME), false);

        leftView = new T8DefinitionUsagesView(definitionContext, parentView, definition.getPublicIdentifier());
        rightView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", definitionNavigator, "right", new T8DefinitionEditorView(definitionContext, parentView)), EDITOR_SPLIT_LAYOUT_STRING, "left");
        splitPaneView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", leftView, "right", rightView), MAIN_SPLIT_LAYOUT_STRING, "left");
        return splitPaneView;
    }

    @Override
    public T8MultiSplitDefinitionView createDefinitionSearchManager(T8DeveloperView parentView)
    {
        T8MultiSplitDefinitionView splitPaneView;
        T8DefinitionNavigatorView definitionNavigator;
        T8DeveloperView leftView;
        T8DeveloperView rightView;

        definitionNavigator = new T8DefinitionNavigatorView(definitionContext, parentView, null);
        definitionNavigator.setToolBarOptionsVisible(ArrayLists.newArrayList(NavigatorToolBarOption.COPY, NavigatorToolBarOption.RENAME), false);

        leftView = new T8DefinitionSearchView(definitionContext, parentView);
        rightView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", definitionNavigator, "right", new T8DefinitionEditorView(definitionContext, parentView)), EDITOR_SPLIT_LAYOUT_STRING, "left");
        splitPaneView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", leftView, "right", rightView), MAIN_SPLIT_LAYOUT_STRING, "left");
        return splitPaneView;
    }

    @Override
    public T8DeveloperView createDefinitionSearchManager(T8DeveloperView parentView, String groupIdentifier)
    {
        T8MultiSplitDefinitionView splitPaneView;
        T8DefinitionNavigatorView definitionNavigator;
        T8DeveloperView leftView;
        T8DeveloperView rightView;

        definitionNavigator = new T8DefinitionNavigatorView(definitionContext, parentView, null);
        definitionNavigator.setToolBarOptionsVisible(ArrayLists.newArrayList(NavigatorToolBarOption.COPY, NavigatorToolBarOption.RENAME), false);

        leftView = new T8DefinitionSearchView(definitionContext, parentView);
        rightView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", definitionNavigator, "right", new T8DefinitionEditorView(definitionContext, parentView)), EDITOR_SPLIT_LAYOUT_STRING, "left");
        splitPaneView = new T8MultiSplitDefinitionView(definitionContext, parentView, HashMaps.newHashMap("left", leftView, "right", rightView), MAIN_SPLIT_LAYOUT_STRING, "left");
        return splitPaneView;
    }

    @Override
    public T8DeveloperView createDefinitionNavigator(T8DeveloperView parentView, String projectId, String definitionId)
    {
        try
        {
            return createDefinitionNavigator(parentView, clientContext.getDefinitionManager().getRawDefinition(context, projectId, definitionId));
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to retrieve definition from server: " + definitionId, ex);
            return null;
        }
    }

    @Override
    public T8DeveloperView createReadOnlyDefinitionNavigator(T8DeveloperView parentView, String projectId, String definitionId)
    {
        try
        {
            return createReadOnlyDefinitionNavigator(parentView, clientContext.getDefinitionManager().getRawDefinition(context, projectId, definitionId));
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to retrieve definition from server: " + definitionId, ex);
            return null;
        }
    }

    @Override
    public T8DeveloperView createDefinitionJsonManager(T8DeveloperView parentView)
    {
        return new T8DefinitionJsonView(definitionContext, parentView);
    }
}
