package com.pilog.t8.developer.definitions.selectiontreeview;

import com.pilog.t8.definition.T8DefinitionMetaData;
import java.util.LinkedList;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionMetaTreeNode extends AbstractMutableTreeTableNode
{
    private boolean selected;
    private T8DefinitionMetaData metaData;

    public T8DefinitionMetaTreeNode(String identifier)
    {
        super(identifier);
    }

    public T8DefinitionMetaData getMetaData()
    {
        return metaData;
    }

    public void setMetaData(T8DefinitionMetaData metaData)
    {
        this.metaData = metaData;
    }
    
    public LinkedList<String> getPath()
    {
        LinkedList<String> path;
        T8DefinitionMetaTreeNode node;
        
        path = new LinkedList<String>();
        node = this;
        while (node != null)
        {
            path.addFirst(node.getIdentifier());
            node = (T8DefinitionMetaTreeNode)node.getParent();
        }
        
        return path;
    }
    
    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }
    
    public void setIdentifier(String identifier)
    {
        setUserObject(identifier);
    }
    
    public String getIdentifier()
    {
        return (String)getUserObject();
    }

    @Override
    public Object getValueAt(int columnIndex)
    {
        if (columnIndex == 0) // Selected status.
        {
            return selected;
        }
        else if (columnIndex == 1) // Node label.
        {
            return getIdentifier();
        }
        else if (metaData != null)
        {
            if (columnIndex == 2) // Node type.
            {
                return "Definition";
            }
            else if (columnIndex == 3) // Definition Status.
            {
                return metaData.getStatus();
            }
            else if (columnIndex == 4) // Definition Project.
            {
                return metaData.getProjectId();
            }
            else if (columnIndex == 5) // Definition updated time.
            {
                return metaData.getUpdatedTime();
            }
            else if (columnIndex == 6) // Definition updated user identifier.
            {
                return metaData.getUpdatedUserId();
            }
            else if (columnIndex == 7) // Definition created time.
            {
                return metaData.getCreatedTime();
            }
            else if (columnIndex == 8) // Definition created user identifier.
            {
                return metaData.getCreatedUserId();
            }
            else if (columnIndex == 9) // Definition checksum.
            {
                return metaData.getChecksum();
            }
            else return null;
        }
        else return null;
    }

    @Override
    public int getColumnCount()
    {
        return 10;
    }
    
    @Override
    public boolean isEditable(int column)
    {
        return column == 0;
    }
}
