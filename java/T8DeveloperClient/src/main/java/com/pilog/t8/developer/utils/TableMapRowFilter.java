package com.pilog.t8.developer.utils;

import com.pilog.t8.utilities.strings.Strings;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;

/**
 * @author Hennie Brink
 */
public class TableMapRowFilter extends RowFilter<TableModel, Integer>
{
    private String filterString;

    @Override
    public boolean include(
            RowFilter.Entry<? extends TableModel, ? extends Integer> entry)
    {
        if (Strings.isNullOrEmpty(filterString))
        {
            return true;
        }

        for (int i = 0; i < entry.getModel().getColumnCount(); i++)
        {
            if (!Strings.isNullOrEmpty(entry.getStringValue(i)) && entry.getStringValue(i).toUpperCase().contains(filterString.toUpperCase()))
            {
                return true;
            }
        }

        return false;
    }

    public void setFilterString(String filterString)
    {
        this.filterString = filterString;
    }
}
