/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.developer.definitions.selectiontableview;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.log.T8Logger;
import java.util.HashMap;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8DefinitionRowFilter extends RowFilter<TableModel, Integer>
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8DefinitionRowFilter.class.getName());
    private String filterText;
    private String filterColumn;
    private final ExpressionEvaluator expressionEvaluator;

    public T8DefinitionRowFilter()
    {
        this.filterText = "";
        this.expressionEvaluator = new ExpressionEvaluator();
    }

    @Override
    public boolean include(
            Entry<? extends TableModel, ? extends Integer> entry)
    {
        if (getFilterText().isEmpty())
        {
            return true;
        }
        //if filter contains EPIC: then lets evaluate an epic expression
        if (getFilterColumn().equals("EPIC"))
        {
            HashMap<String, Object> paramMap;
            //create a map that we will send to epic
            paramMap = new HashMap<String, Object>(entry.getValueCount());
            String epicExpression = getFilterText();
            try
            {
                for (int i = 0; i < entry.getValueCount(); i++)
                {
                    paramMap.put(entry.getModel().getColumnName(i).replaceAll(" ", "_"), entry.getStringValue(i));
                }
                expressionEvaluator.compileExpression(epicExpression);
                return expressionEvaluator.evaluateBooleanExpression(paramMap, null);
            }
            catch (EPICSyntaxException ex)
            {
                LOGGER.log("Failed to compile expression " + epicExpression, ex);
            }
            catch (EPICRuntimeException ex)
            {
                LOGGER.log("Failed to evaluate expression " + epicExpression + " with params " + paramMap, ex);
            }
            return false;
        }
        else
        {
            for (int i = 0; i < entry.getModel().getColumnCount(); i++)
            {
                if(entry.getModel().getColumnName(i).equals(filterColumn))
                {
                    String value = entry.getStringValue(i);
                    if (value.toUpperCase().contains(filterText.toUpperCase().replace(" ", "_")))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @return the filterText
     */
    public String getFilterText()
    {
        return filterText;
    }

    /**
     * @param filterText the filterText to set
     */
    public void setFilterText(String filterText)
    {
        this.filterText = filterText;
    }

    public void setFilterColumn(String columnName)
    {
        this.filterColumn = columnName;
    }

    public String getFilterColumn()
    {
        return this.filterColumn;
    }
}
