package com.pilog.t8.developer.definitions.projectmanager;

import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphEdge.EdgeArrowType;
import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.view.Graph;
import com.pilog.graph.view.GraphEdgeRenderer;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectDependencyEdgeRenderer extends Component implements GraphEdgeRenderer
{
    private ArrayList<Point> coordinates;
    private final Polygon arrow;
    private final double startArrowAngle;
    private final double endArrowAngle;
    private final EdgeArrowType startArrowType;
    private final EdgeArrowType endArrowType;
    private final BasicStroke selectedStroke;
    private final BasicStroke defaultStroke;
    private Color edgeColor = Color.GRAY;
    private Stroke stroke;

    private final Color OUTPUT_EDGE_COLOR = Color.BLUE;
    private final Color INPUT_EDGE_COLOR = Color.GREEN;
    private final Color DEFAULT_EDGE_COLOR = new Color(230, 230, 230);

    public T8ProjectDependencyEdgeRenderer()
    {
        arrow = new Polygon();
        arrow.addPoint(20, 20);
        arrow.addPoint(26, 30);
        arrow.addPoint(14, 30);
        startArrowAngle = 0;
        endArrowAngle = 0;
        startArrowType = EdgeArrowType.NONE;
        endArrowType = EdgeArrowType.NONE;
        defaultStroke = new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        selectedStroke = new BasicStroke(5.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    }

    @Override
    public Component getRendererComponent(Graph graph, GraphEdge edge, boolean isSelected)
    {
        edgeColor = (isStartVertexSelected(graph, edge) ? OUTPUT_EDGE_COLOR : isEndVertexSelected(graph, edge) ? INPUT_EDGE_COLOR : DEFAULT_EDGE_COLOR);
        stroke = (isSelected || isEitherEdgeVertexSelected(graph, edge) ? selectedStroke : defaultStroke);
        doStraightEdgeLayout(edge);
        return this;
    }

    @Override
    public void paint(Graphics g)
    {
        Graphics2D arrowG;
        Graphics2D g2;

        g2 = (Graphics2D)g;
        g2.setStroke(stroke);
        g2.setColor(edgeColor);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Draw the edge lines.
        for (int i = 0; i < coordinates.size() -1; i++)
        {
            Point startCoordinates;
            Point endCoordinates;

            startCoordinates = coordinates.get(i);
            endCoordinates = coordinates.get(i + 1);

            g.drawLine(startCoordinates.x, startCoordinates.y, endCoordinates.x, endCoordinates.y);
        }

        // Draw the arrows at the ends of the edge.
        {
            Point startCoordinates;
            Point endCoordinates;

            startCoordinates = coordinates.get(0);
            endCoordinates = coordinates.get(coordinates.size()-1);

            if (startArrowType != EdgeArrowType.NONE)
            {
                arrowG = (Graphics2D)g2.create(startCoordinates.x-20, startCoordinates.y-20, 40, 40);
                arrowG.rotate(startArrowAngle, 20, 20);

                if (startArrowType == EdgeArrowType.FILLED)
                {
                    arrowG.fillPolygon(arrow);
                }
                else
                {
                    arrowG.drawPolygon(arrow);
                }
            }

            if (endArrowType != EdgeArrowType.NONE)
            {
                arrowG = (Graphics2D)g.create(endCoordinates.x-20, endCoordinates.y-20, 40, 40);
                arrowG.rotate(endArrowAngle, 20, 20);

                if (endArrowType == EdgeArrowType.FILLED)
                {
                    arrowG.fillPolygon(arrow);
                }
                else
                {
                    arrowG.drawPolygon(arrow);
                }
            }
        }
    }

    private void doStraightEdgeLayout(GraphEdge edge)
    {
        coordinates = new ArrayList<Point>();
        coordinates.add(edge.getStartPort().getPortCoordinates());
        coordinates.add(edge.getEndPort().getPortCoordinates());
    }

    private boolean isEitherEdgeVertexSelected(Graph graph, GraphEdge edge)
    {
        List<GraphVertex> selectedVertices;

        selectedVertices = graph.getSelectedVertices();
        if (selectedVertices.contains(edge.getStartPort().getParentVertex())) return true;
        else return selectedVertices.contains(edge.getEndPort().getParentVertex());
    }

    private boolean isStartVertexSelected(Graph graph, GraphEdge edge)
    {
        List<GraphVertex> selectedVertices;

        selectedVertices = graph.getSelectedVertices();
        return selectedVertices.contains(edge.getStartPort().getParentVertex());
    }

    private boolean isEndVertexSelected(Graph graph, GraphEdge edge)
    {
        List<GraphVertex> selectedVertices;

        selectedVertices = graph.getSelectedVertices();
        return (selectedVertices.contains(edge.getEndPort().getParentVertex()));
    }
}
