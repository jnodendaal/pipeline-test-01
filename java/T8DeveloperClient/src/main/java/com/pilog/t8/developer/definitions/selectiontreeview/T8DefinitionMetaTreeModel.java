package com.pilog.t8.developer.definitions.selectiontreeview;

import com.pilog.t8.definition.T8DefinitionMetaData;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionMetaTreeModel extends DefaultTreeTableModel
{
    public T8DefinitionMetaTreeModel(MutableTreeTableNode rootNode)
    {
        super(rootNode);
    }
    
    public T8DefinitionMetaTreeNode getRootNode()
    {
        return (T8DefinitionMetaTreeNode)getRoot();
    }
    
    public String getRootIdentifier()
    {
        T8DefinitionMetaTreeNode rootNode;
        
        rootNode = getRootNode();
        return rootNode != null ? rootNode.getIdentifier() : null;
    }
    
    public void setRootIdentifier(String identifier)
    {
        if (!identifier.equals(getRootIdentifier()))
        {
            setRoot(new T8DefinitionMetaTreeNode(identifier));
        }
    }
    
    public boolean setMetaData(LinkedList<String> path, T8DefinitionMetaData metaData)
    {
        T8DefinitionMetaTreeNode targetNode;
        
        targetNode = getNode(path);
        if (targetNode != null)
        {
            targetNode.setMetaData(metaData);
            return true;
        }
        else return false;
    }
    
    public void createPath(LinkedList<String> path)
    {
        if ((path != null) && (path.size() > 0))
        {
            T8DefinitionMetaTreeNode pathNode;
            LinkedList<String> newPath;

            // First make sure we have a root node.
            pathNode = getRootNode();
            if (pathNode == null)
            {
                setRootIdentifier(path.get(0));
                pathNode = getRootNode();
            }
            
            // No move down the path and at each step ensure that the proper node exists.
            newPath = new LinkedList<String>();
            for (String pathIdentifier : path)
            {
                T8DefinitionMetaTreeNode nextNode;
                
                newPath.add(pathIdentifier);
                nextNode = getNode(newPath);
                if (nextNode == null)
                {
                    nextNode = addNode(pathIdentifier, pathNode);
                }
                
                pathNode = nextNode;
            }
        }
    }
    
    /**
     * Adds a new node to the specified target node.  The new node is created
     * using the supplied identifier.  If the target node already contains a
     * child with the specified new identifier, that child node is returned and
     * no new child is added.
     * @param newIdentifier
     * @param targetNode
     * @return 
     */
    private T8DefinitionMetaTreeNode addNode(String newIdentifier, T8DefinitionMetaTreeNode targetNode)
    {
        T8DefinitionMetaTreeNode newNode;
        
        for (int childIndex = 0; childIndex < targetNode.getChildCount(); childIndex++)
        {
            T8DefinitionMetaTreeNode definitionNode;

            definitionNode = (T8DefinitionMetaTreeNode)targetNode.getChildAt(childIndex);
            if (definitionNode.getIdentifier().compareTo(newIdentifier) == 0) return definitionNode; // The node already exists.
            else if (definitionNode.getIdentifier().compareTo(newIdentifier) < 0)
            {
                
                
                newNode = new T8DefinitionMetaTreeNode(newIdentifier);
                insertNodeInto(newNode, targetNode, childIndex);
                return newNode;
            }
        }
        
        // Add the new node at the end of the child sequence.
        newNode = new T8DefinitionMetaTreeNode(newIdentifier);
        insertNodeInto(newNode, targetNode, targetNode.getChildCount());
        return newNode;
    }
    
    public void addNode(String newIdentifier, LinkedList<String> path)
    {
        if ((path == null) || (path.size() == 0))
        {
            setRootIdentifier(newIdentifier);
        }
        else
        {
            T8DefinitionMetaTreeNode targetNode;

            targetNode = getNode(path);
            if (targetNode != null)
            {
                for (int childIndex = 0; childIndex < targetNode.getChildCount(); childIndex++)
                {
                    T8DefinitionMetaTreeNode definitionNode;

                    definitionNode = (T8DefinitionMetaTreeNode)targetNode.getChildAt(childIndex);
                    if (definitionNode.getIdentifier().compareTo(newIdentifier) == 0) return; // The node already exists.
                    else if (definitionNode.getIdentifier().compareTo(newIdentifier) < 0)
                    {
                        insertNodeInto(new T8DefinitionMetaTreeNode(newIdentifier), targetNode, childIndex);
                        return;
                    }
                }
            }
            else throw new IllegalArgumentException("Tree path not found: " + path);
        }
    }
    
    public boolean removeNode(LinkedList<String> path)
    {
        T8DefinitionMetaTreeNode targetNode;
        
        targetNode = getNode(path);
        if (targetNode != null)
        {
            removeNodeFromParent(targetNode);
            return true;
        }
        else return false;
    }
    
    public T8DefinitionMetaTreeNode getNode(LinkedList<String> path)
    {
        T8DefinitionMetaTreeNode rootNode;
        
        rootNode = getRootNode();
        if (rootNode == null)
        {
            return null;
        }
        else if ((path == null) || (path.size() == 0))
        {
            return null;
        }
        else
        {
            LinkedList<T8DefinitionMetaTreeNode> nodeQueue;

            nodeQueue = new LinkedList<T8DefinitionMetaTreeNode>();
            nodeQueue.add(rootNode);
            while (nodeQueue.size() > 0)
            {
                T8DefinitionMetaTreeNode nextNode;
                String nextIdentifier;

                nextNode = nodeQueue.pop();
                nextIdentifier = nextNode.getIdentifier();
                if (nextIdentifier.equals(path.peekFirst()))
                {
                    // Remove the first element in the path because it was found.
                    path.removeFirst();

                    // If no more elements remain in the path, we have found the last one and we can return it.
                    if (path.size() == 0) return nextNode;

                    // Ok so, more elements remain in the path - add all of the child nodes of the one we just found so they can be evaluated next.
                    for (int childIndex = 0; childIndex < nextNode.getChildCount(); childIndex++)
                    {
                        nodeQueue.add((T8DefinitionMetaTreeNode)nextNode.getChildAt(childIndex));
                    }
                }
            }
            
            return null;
        }
    }
    
    public boolean pathExists(LinkedList<String> path)
    {
        MutableTreeTableNode targetNode;
        
        targetNode = getNode(path);
        return targetNode != null;
    }
    
    public List<T8DefinitionMetaTreeNode> getAllNodes()
    {
        LinkedList<T8DefinitionMetaTreeNode> nodeQueue;
        ArrayList<T8DefinitionMetaTreeNode> nodes;
        
        nodes = new ArrayList<T8DefinitionMetaTreeNode>();
        nodeQueue = new LinkedList<T8DefinitionMetaTreeNode>();
        nodeQueue.add(getRootNode());
        while (nodeQueue.size() > 0)
        {
            T8DefinitionMetaTreeNode nextNode;
            
            nextNode = nodeQueue.pop();
            nodes.add(nextNode);
            
            for (int childIndex = 0; childIndex < nextNode.getChildCount(); childIndex++)
            {
                nodeQueue.add((T8DefinitionMetaTreeNode)nextNode.getChildAt(childIndex));
            }
        }
        
        return nodes;
    }

    @Override
    public int getHierarchicalColumn()
    {
        return 1;
    }
    
    @Override
    public String getColumnName(int columnIndex)
    {
        if (columnIndex == 0) return "Selected";
        else if (columnIndex == 1) return "Definition";
        else if (columnIndex == 2) return "Type";
        else if (columnIndex == 3) return "Status";
        else if (columnIndex == 4) return "Project";
        else if (columnIndex == 5) return "Updated Time";
        else if (columnIndex == 6) return "Updated User";
        else if (columnIndex == 7) return "Created Time";
        else if (columnIndex == 8) return "Created User";
        else if (columnIndex == 9) return "Checksum";
        else return null;
    }
}
