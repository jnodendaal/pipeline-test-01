package com.pilog.t8.developer;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionEditorFactory;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.T8DefaultDefinitionEditor;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDefinitionEditorFactory implements T8DefinitionEditorFactory
{
    private final T8ClientContext clientContext;
    
    public T8DefaultDefinitionEditorFactory(T8ClientContext clientContext)
    {
        this.clientContext = clientContext;
    }
    
    @Override
    public T8DefinitionDatumEditor getDefaultDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType) throws Exception
    {
        return T8DefaultDefinitionEditor.getDefaultDatumEditor(definitionContext, definition, datumType);
    }
}
