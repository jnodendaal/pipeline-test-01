package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.definition.T8DefinitionTypeDefinition;
import com.pilog.t8.definition.editor.T8DefinitionEditorSetupDefinition;
import com.pilog.t8.definition.ui.datumeditor.T8DefinitionDatumEditorDefinition;
import com.pilog.t8.developer.definitions.defaulteditor.T8DefaultDefinitionEditor;
import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * @author Bouwer du Preez
 */
public class T8DatumTableCellEditor extends AbstractCellEditor implements TableCellEditor
{
    private final DefinitionTableDatumEditor editor;
    private T8DefinitionDatumEditor currentDatumEditor;
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8DefinitionContext definitionContext;
    private T8Definition editedDefinition;
    private T8DefinitionDatumType datumType;
    private T8DefinitionTypeDefinition definitionTypeDefinition;

    public T8DatumTableCellEditor(DefinitionTableDatumEditor editor)
    {
        this.editor = editor;
        this.clientContext = editor.getClientContext();
        this.sessionContext = editor.getSessionContext();
        this.definitionContext = editor.getDefinitionContext();
    }

    public void startComponent()
    {
        // Nothing to do here, because the editor is started when a new cell editor is requested.
    }

    public void stopComponent()
    {
        if (currentDatumEditor != null) currentDatumEditor.commitChanges();
    }

    @Override
    public Object getCellEditorValue()
    {
        currentDatumEditor.commitChanges();
        return editedDefinition.getDefinitionDatum(datumType.getIdentifier());
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        try
        {
            // First stop the current datum editor if there is one.
            if (currentDatumEditor != null) currentDatumEditor.commitChanges();

            // Now create the new editor.
            editedDefinition = editor.getDefinition(row);
            datumType = editor.getDatumType(row, column);

            //First load the default editor so that it can be over written with a custom editor
            currentDatumEditor = T8DefaultDefinitionEditor.getDefaultDatumEditor(definitionContext, editedDefinition, datumType);

            // Try to get a specific datum editor from the definition editor setup (if available).
            T8DefinitionEditorSetupDefinition definitionEditorSetupDefinition = getDefinitionEditorSetupDefinition(editedDefinition.getTypeMetaData());
            if (definitionEditorSetupDefinition != null)
            {
                T8DefinitionDatumEditorDefinition datumEditorDefinition;

                datumEditorDefinition = definitionEditorSetupDefinition.getDefinitionDatumEditorDefinition(datumType.getIdentifier());
                if (datumEditorDefinition != null)
                {
                    T8DefinitionDatumEditor newDatumEditorInstance = datumEditorDefinition.getNewDatumEditorInstance(definitionContext, editedDefinition, datumType);
                    //Check if it laoded succesfully, otherwise keep the default editor
                    if(newDatumEditorInstance != null)
                        currentDatumEditor = newDatumEditorInstance;
                }
            }

            if (currentDatumEditor != null) currentDatumEditor.startComponent();

            if(table.getRowHeight(row) < ((Component)currentDatumEditor).getPreferredSize().height)
            {
                table.setRowHeight(row, ((Component)currentDatumEditor).getPreferredSize().height);
            }

            return (Component)currentDatumEditor;
        }
        catch (Exception e)
        {
            T8Log.log("Exception while creating a datum editor.", e);
            return null;
        }
    }

    private T8DefinitionEditorSetupDefinition getDefinitionEditorSetupDefinition(T8DefinitionTypeMetaData typeMetaData)
    {
        try
        {
            definitionTypeDefinition = (T8DefinitionTypeDefinition)definitionContext.getInitializedDefinition(editedDefinition.getRootProjectId(), typeMetaData.getTypeId(), null);
            if (definitionTypeDefinition != null)
            {
                String definitionEditorSetupId;

                definitionEditorSetupId = definitionTypeDefinition.getDefinitionEditorSetupIdentifier();
                if (definitionEditorSetupId != null)
                {
                    return (T8DefinitionEditorSetupDefinition)definitionContext.getInitializedDefinition(editedDefinition.getRootProjectId(), definitionEditorSetupId, null);
                }
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while trying to load definition type and editor setup definition: " + typeMetaData.getTypeId(), e);
        }
        return null;
    }
}
