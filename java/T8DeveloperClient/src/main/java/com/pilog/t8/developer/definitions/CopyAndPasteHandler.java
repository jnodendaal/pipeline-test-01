package com.pilog.t8.developer.definitions;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.utilities.serialization.SerializationUtilities;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * @author Bouwer du Preez
 */
public class CopyAndPasteHandler implements ClipboardOwner
{
    private static T8Definition copiedDefinition;

    public boolean copyDefinition(T8Definition definition)
    {
        try
        {
            T8Definition parentDefinition;

            copiedDefinition = (T8Definition)SerializationUtilities.copyObjectBySerialization(definition);
            parentDefinition = copiedDefinition.getParentDefinition();
            if (parentDefinition != null)
            {
                parentDefinition.removeSubDefinition(copiedDefinition);
            }

            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(new T8DefinitionTransferable(copiedDefinition), this);

            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            copiedDefinition = null;
            return false;
        }
    }

    public boolean cutDefinition(T8Definition definition)
    {
        try
        {
            T8Definition parentDefinition;

            parentDefinition = definition.getParentDefinition();
            if (parentDefinition != null)
            {
                parentDefinition.removeSubDefinition(definition);
                copiedDefinition = (T8Definition)SerializationUtilities.copyObjectBySerialization(definition);

                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(new T8DefinitionTransferable(copiedDefinition), this);
                return true;
            }
            else return false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            copiedDefinition = null;
            return false;
        }
    }

    public T8Definition getCopiedDefinition()
    {
        T8Definition returnDefinition;

        returnDefinition = copiedDefinition;
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        for (DataFlavor dataFlavor : clipboard.getAvailableDataFlavors())
        {
            if(dataFlavor.equals(T8DefinitionTransferable.getDefinitionDataFlavor()))
            {
                try
                {
                    return (T8Definition) clipboard.getData(T8DefinitionTransferable.getDefinitionDataFlavor());
                }
                catch (UnsupportedFlavorException ex)
                {
                    T8Log.log("Failed to get Definition from clipboard. Flavor not Supported.", ex);
                }
                catch (IOException ex)
                {
                    T8Log.log("Failed to get Definition from clipboard.", ex);
                }
            }
        }
        return T8DefinitionUtilities.clearAuditingData(returnDefinition);
    }

    public boolean hasCopiedDefinition()
    {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        for (DataFlavor dataFlavor : clipboard.getAvailableDataFlavors())
        {
            if(dataFlavor.equals(T8DefinitionTransferable.getDefinitionDataFlavor()))
            {
                return true;
            }
        }
        return copiedDefinition != null;
    }

    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents)
    {

    }
}
