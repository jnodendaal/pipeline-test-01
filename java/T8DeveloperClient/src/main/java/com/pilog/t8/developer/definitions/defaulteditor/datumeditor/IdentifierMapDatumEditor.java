package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.IdentifierMapDatumEditor.IdentifierTableCellEditor.EditorType;
import com.pilog.t8.developer.utils.TableMapRowFilter;
import com.pilog.t8.utilities.collections.MapEntry;
import com.pilog.t8.utilities.components.celleditor.SearchableComboBoxTableCellEditor;
import com.pilog.t8.utilities.components.cellrenderers.CellRendererUtilities;
import com.pilog.t8.utilities.strings.FuzzyStringUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.DefaultCellEditor;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.CellEditorListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.jdesktop.swingx.JXSearchField;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTextField;

/**
 * @author Bouwer du Preez
 */
public class IdentifierMapDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private JXTable jTableIdentifiers;
    private TableRowSorter<TableModel> tableRowSorter;
    private TableMapRowFilter rowFilter;
    private int maximumElements;
    private DefaultTableModel tableModel;
    private TableCellEditor keyCellEditor;
    private TableCellEditor valueCellEditor;
    private Map<String, List<String>> optionMap;
    private boolean editable;

    public IdentifierMapDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        this.maximumElements = datumType.getMaximumElements();
        if (maximumElements == -1) maximumElements = 10000; // A very large value, since -1 indicates unbounded element count.
        initComponents();
        createOptionMap();
        setupTable();
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void commitChanges()
    {
        if (jTableIdentifiers.getCellEditor() != null) jTableIdentifiers.getCellEditor().stopCellEditing();
        if ((tableModel != null) && (tableModel.getRowCount() > 0))
        {
            LinkedHashMap<String, String> identifierMap;

            identifierMap = new LinkedHashMap<String, String>();
            for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
            {
                String keyIdentifier;
                String valueIdentifier;

                keyIdentifier = (String)tableModel.getValueAt(rowIndex, 0);
                valueIdentifier = (String)tableModel.getValueAt(rowIndex, 1);

                if ((keyIdentifier != null) && (keyIdentifier.trim().length() > 0))
                {
                    if ((valueIdentifier != null) && (valueIdentifier.trim().length() > 0))
                    {
                        identifierMap.put(keyIdentifier, valueIdentifier);
                    }
                }
            }

            setDefinitionDatum(identifierMap);
        }
        else setDefinitionDatum(null);
    }

    @Override
    public void refreshEditor()
    {
        Map<String, String> identifierMap;

        identifierMap = (Map<String, String>)getDefinitionDatum();

        clearTableModel();

        if ((identifierMap != null) && (identifierMap.size() > 0))
        {
            // Add all the rows to the table model.
            clearTableModel();
            for (String keyIdentifier : identifierMap.keySet())
            {
                addIdentifierRow(keyIdentifier, identifierMap.get(keyIdentifier));
            }
        }

        CellRendererUtilities.autoSizeTableColumns(jTableIdentifiers, 100);

        setupCellEditors();
    }

    @Override
    public void setEditable(boolean editable)
    {
        this.editable = editable;
        jButtonAdd.setEnabled(editable);
        jButtonRemove.setEnabled(editable);
        jButtonMoveUp.setEnabled(editable);
        jButtonMoveDown.setEnabled(editable);
        jButtonAutoMatch.setEnabled(editable);
        jButtonSortAlphabetic.setEnabled(editable);
    }

    private void createCellEditors()
    {

        if ((optionMap.size() == 1) && (optionMap.containsKey(null)))
        {
            this.keyCellEditor = new IdentifierTableCellEditor(EditorType.WILDCARD, optionMap);
            this.valueCellEditor = new IdentifierTableCellEditor(EditorType.INDEPENDENT_VALUE, optionMap);
        }
        else
        {
            this.keyCellEditor = new IdentifierTableCellEditor(EditorType.KEY, optionMap);
            this.valueCellEditor = new IdentifierTableCellEditor(EditorType.MAPPED_VALUE, optionMap);
        }
    }

    private void createOptionMap()
    {
        try
        {
            ArrayList<T8DefinitionDatumOption> datumOptions;

            datumOptions = definition.getDatumOptions(definitionContext, datumType.getIdentifier());
            if ((datumOptions != null) && (datumOptions.size() > 0))
            {
                optionMap = (Map<String, List<String>>)datumOptions.get(0).getValue();
                if (optionMap == null) optionMap = new HashMap<String, List<String>>();
            }
            else optionMap = null;
        }
        catch (Exception e)
        {
            T8Log.log("Exception while loading datum '" + datumType.getIdentifier() + "' options.", e);
        }
    }

    private void setupTable()
    {
        JScrollPane tableScrollPane;

        jTableIdentifiers = new JXTable();
        jTableIdentifiers.setRowHeight(27);
        jTableIdentifiers.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        tableModel = createTableModel();
        jTableIdentifiers.setModel(tableModel);

        createCellEditors();

        // Create a scroll pane that will resize vertically along with the table it contains but still allow a scroll bar on the horizontal axis.
        tableScrollPane = new JScrollPane(jTableIdentifiers)
        {
            @Override
            public Dimension getPreferredSize()
            {
                int height;

                height = 0;
                height += viewport.getView().getPreferredSize().getHeight();
                height += columnHeader.getPreferredSize().getHeight();
                if (this.horizontalScrollBar.isVisible()) height += this.horizontalScrollBar.getHeight();
                return new Dimension(0, height);
            }
        };

        tableScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        add(tableScrollPane, java.awt.BorderLayout.CENTER);

        rowFilter = new TableMapRowFilter();
        tableRowSorter = new TableRowSorter<TableModel>(tableModel);
        tableRowSorter.setRowFilter(rowFilter);
        jTableIdentifiers.setRowSorter(tableRowSorter);

        jXSearchField.setInstantSearchDelay(100);
        jXSearchField.setSearchMode(JXSearchField.SearchMode.INSTANT);
        jXSearchField.setFindAction(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                rowFilter.setFilterString(jXSearchField.getText());
                tableRowSorter.sort();
                revalidate();
            }
        });
        jXSearchField.addActionListener(jXSearchField.getFindAction());
    }

    private void addIdentifierRow(String keyIdentifier, String valueIdentifier)
    {
        Object rowData[];

        rowData = new Object[2];
        rowData[0] = keyIdentifier;
        rowData[1] = valueIdentifier;

        tableModel.addRow(rowData);
        jButtonAdd.setEnabled(tableModel.getRowCount() < maximumElements && editable);
        jButtonRemove.setEnabled(tableModel.getRowCount() > 0 && editable);
        revalidate(); // To allow the scroll pane to resize.
    }

    private void removeSelectedRows()
    {
        if (jTableIdentifiers.getCellEditor() != null) jTableIdentifiers.getCellEditor().stopCellEditing();
        while (jTableIdentifiers.getSelectedRowCount() > 0)
        {
            int selectedRowIndex;

            selectedRowIndex = jTableIdentifiers.getSelectedRow();
            tableModel.removeRow(selectedRowIndex);
        }

        jButtonAdd.setEnabled(tableModel.getRowCount() < maximumElements && editable);
        jButtonRemove.setEnabled(tableModel.getRowCount() > 0 && editable);
        revalidate(); // To allow the scroll pane to resize.
    }

    private void clearTableModel()
    {
        while (tableModel.getRowCount() != 0)
        {
            tableModel.removeRow(0);
        }
    }

    private DefaultTableModel createTableModel()
    {
        DefaultTableModel newModel;
        String[] columnNames;

        columnNames = new String[2];
        columnNames[0] = "Key Identifier";
        columnNames[1] = "Value Identifier";

        newModel = new DefaultTableModel(columnNames, 0);

        return newModel;
    }

    private void setupCellEditors()
    {
        jTableIdentifiers.getColumn("Key Identifier").setCellEditor(keyCellEditor);
        jTableIdentifiers.getColumn("Value Identifier").setCellEditor(valueCellEditor);
    }

    private List<MapEntry<Object, Object>> getAllValues()
    {
        List<MapEntry<Object, Object>> values;

        values = new ArrayList<MapEntry<Object, Object>>();
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            values.add(new MapEntry<Object, Object>(tableModel.getValueAt(rowIndex, 0), tableModel.getValueAt(rowIndex, 1)));
        }

        return values;
    }

    private LinkedHashMap<Object, Object> buildMap(List<MapEntry<Object, Object>> values)
    {
        if (values != null)
        {
            LinkedHashMap<Object, Object> map;

            map = new LinkedHashMap<Object, Object>();
            for (MapEntry<Object, Object> entry : values)
            {
                map.put(entry.getKey(), entry.getValue());
            }

            return map;
        }
        else return null;
    }

    private void moveSelectionUp()
    {
        List<MapEntry<Object, Object>> values;
        int[] selectedIndices;

        // Stop any editing that may be in progress on the table.
        if (jTableIdentifiers.getCellEditor() != null) jTableIdentifiers.getCellEditor().stopCellEditing();

        values = getAllValues();
        selectedIndices = jTableIdentifiers.getSelectedRows();
        if (selectedIndices.length > 0)
        {
            MapEntry<Object, Object> selectedValue;

            // Decrement all selected Indices.
            for (int selectedIndex : selectedIndices)
            {
                if (selectedIndex > 0)
                {
                    selectedValue = values.remove(selectedIndex);
                    values.add(selectedIndex-1, selectedValue);
                }
            }

            // Commit the updated value list.
            setDefinitionDatum(buildMap(values));

            // Refresh the editor.
            refreshEditor();

            // Decrement all selected indices (because they've been moved up).
            jTableIdentifiers.clearSelection();
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]-1);
                jTableIdentifiers.getSelectionModel().addSelectionInterval(selectedIndices[indexIndex], selectedIndices[indexIndex]);
            }
        }
    }

    private void moveSelectionDown()
    {
        List<MapEntry<Object, Object>> values;
        int[] selectedIndices;

        // Stop any editing that may be in progress on the table.
        if (jTableIdentifiers.getCellEditor() != null) jTableIdentifiers.getCellEditor().stopCellEditing();

        values = getAllValues();
        selectedIndices = jTableIdentifiers.getSelectedRows();
        if (selectedIndices.length > 0)
        {
            MapEntry<Object, Object> selectedValue;

            // Decrement all selected Indices.
            for (int selectedIndex : selectedIndices)
            {
                if (selectedIndex < values.size() -1)
                {
                    selectedValue = values.remove(selectedIndex);
                    values.add(selectedIndex+1, selectedValue);
                }
            }

            // Commit the updated value list.
            setDefinitionDatum(buildMap(values));

            // Refresh the editor.
            refreshEditor();

            // Increment all selected indices (because they've been moved down).
            jTableIdentifiers.clearSelection();
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]+1);
                jTableIdentifiers.getSelectionModel().addSelectionInterval(selectedIndices[indexIndex], selectedIndices[indexIndex]);
            }
        }
    }

    private void sortAlphabetic()
    {
        List<MapEntry<Object, Object>> values;
        TreeMap<Object, Object> sortedMap;

        // Stop any editing that may be in progress on the table.
        if (jTableIdentifiers.getCellEditor() != null) jTableIdentifiers.getCellEditor().stopCellEditing();

        values = getAllValues();
        sortedMap = new TreeMap<Object, Object>();
        for (MapEntry<Object, Object> entry : values)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        // Commit the updated value map.
        setDefinitionDatum(new LinkedHashMap<Object, Object>(sortedMap));

        // Refresh the editor.
        refreshEditor();
    }

    private void tryAutoMatch()
    {
        for (String key : optionMap.keySet())
        {
            String bestValue;
            double bestMatch;

            bestMatch = 0.0;
            bestValue = null;
            for (String value : optionMap.get(key))
            {
                String testKey;
                String testValue;
                double match;

                testKey = T8IdentifierUtilities.getLocalIdentifierPart(key);
                testValue = T8IdentifierUtilities.getLocalIdentifierPart(value);
                match = FuzzyStringUtilities.computeSimilarity(testKey, testValue);
                if ((match > 0.8) && (match > bestMatch))
                {
                    bestMatch = match;
                    bestValue = value;
                }
            }

            if (bestValue != null)
            {
                addIdentifierRow(key, bestValue);
            }
        }
    }

    public static class IdentifierTableCellEditor implements javax.swing.table.TableCellEditor
    {
        private final Map<String, List<String>> optionMap;
        private final SearchableComboBoxTableCellEditor<String> identifierCellEditor;
        private final TableCellEditor textCellEditor;
        private TableCellEditor currentCellEditor;
        private EditorType editorType;

        public enum EditorType {KEY, WILDCARD, MAPPED_VALUE, INDEPENDENT_VALUE};

        public IdentifierTableCellEditor(EditorType editorType, Map<String, List<String>> optionMap)
        {
            this.optionMap = optionMap;
            this.editorType = editorType;

            if (optionMap != null)
            {
                if ((editorType == EditorType.MAPPED_VALUE) || (editorType == EditorType.INDEPENDENT_VALUE))
                {
                    List<String> values;
                    Iterator<List<String>> iterator;

                    iterator = optionMap.values().iterator();
                    values = iterator.hasNext() ? iterator.next() : new ArrayList<String>(0);
                    identifierCellEditor = new SearchableComboBoxTableCellEditor<String>(false, values);
                    textCellEditor = new DefaultCellEditor(new JXTextField());
                    currentCellEditor = null;
                }
                else if (editorType == EditorType.KEY)
                {
                    List<String> keys;

                    keys = new ArrayList<String>(optionMap.keySet());
                    identifierCellEditor = new SearchableComboBoxTableCellEditor<String>(false, keys);
                    textCellEditor = new DefaultCellEditor(new JXTextField());
                    currentCellEditor = null;
                }
                else // WILDCARD.
                {
                    identifierCellEditor = null;
                    textCellEditor = new DefaultCellEditor(new JXTextField());
                    currentCellEditor = null;
                }
            }
            else
            {
                identifierCellEditor = null;
                textCellEditor = new DefaultCellEditor(new JXTextField());
                currentCellEditor = null;
            }
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
        {
            if (editorType == EditorType.MAPPED_VALUE)
            {
                if (optionMap != null)
                {
                    String identifier;

                    identifier = (String)table.getModel().getValueAt(row, 0);
                    if (Strings.isNullOrEmpty(identifier))
                    {
                        identifierCellEditor.setItems(null);
                        currentCellEditor = identifierCellEditor;
                        return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
                    }
                    else
                    {
                        List<String> options;

                        options = optionMap.get(identifier);
                        if (options != null)
                        {
                            identifierCellEditor.setItems(options);
                            currentCellEditor = identifierCellEditor;
                            return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
                        }
                        else
                        {
                            currentCellEditor = textCellEditor;
                            return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
                        }
                    }
                }
                else
                {
                    currentCellEditor = textCellEditor;
                    return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
                }
            }
            else if (editorType == EditorType.INDEPENDENT_VALUE)
            {
                if (optionMap != null)
                {
                    List<String> options;

                    options = optionMap.get(null); // The key for independent values is null.
                    if (options != null)
                    {
                        identifierCellEditor.setItems(options);
                        currentCellEditor = identifierCellEditor;
                        return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
                    }
                    else
                    {
                        currentCellEditor = textCellEditor;
                        return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
                    }
                }
                else
                {
                    currentCellEditor = textCellEditor;
                    return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
                }
            }
            else if (editorType == EditorType.KEY)
            {
                if (optionMap != null)
                {
                    Set<String> keys;

                    keys = new HashSet<String>(optionMap.keySet());
                    for (int i = 0; i < table.getModel().getRowCount(); i++)
                    {
                        String tableValue = (String)table.getModel().getValueAt(i, 0);
                        if(tableValue != null) keys.remove(tableValue);

                    }
                    identifierCellEditor.setItems(new ArrayList<String>(keys));
                    currentCellEditor = identifierCellEditor;
                    return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
                }
                else
                {
                    currentCellEditor = textCellEditor;
                    return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
                }
            }
            else // WILDCARD
            {
                currentCellEditor = textCellEditor;
                return currentCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
            }
        }

        @Override
        public Object getCellEditorValue()
        {
            return (currentCellEditor != null ? currentCellEditor.getCellEditorValue() : null);
        }

        @Override
        public boolean isCellEditable(EventObject anEvent)
        {
            return (currentCellEditor != null ? currentCellEditor.isCellEditable(anEvent) : true);
        }

        @Override
        public boolean shouldSelectCell(EventObject anEvent)
        {
            return (currentCellEditor != null ? currentCellEditor.shouldSelectCell(anEvent) : true);
        }

        @Override
        public boolean stopCellEditing()
        {
            return (currentCellEditor != null ? currentCellEditor.stopCellEditing() : true);
        }

        @Override
        public void cancelCellEditing()
        {
            if (currentCellEditor != null) currentCellEditor.cancelCellEditing();
        }

        @Override
        public void addCellEditorListener(CellEditorListener l)
        {
            if (currentCellEditor != null) currentCellEditor.addCellEditorListener(l);
        }

        @Override
        public void removeCellEditorListener(CellEditorListener l)
        {
            if (currentCellEditor != null) currentCellEditor.removeCellEditorListener(l);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMenu = new javax.swing.JPanel();
        jXSearchField = new org.jdesktop.swingx.JXSearchField();
        jToolBarList = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jButtonRemove = new javax.swing.JButton();
        jButtonMoveUp = new javax.swing.JButton();
        jButtonMoveDown = new javax.swing.JButton();
        jButtonSortAlphabetic = new javax.swing.JButton();
        jButtonAutoMatch = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(112, 100));
        setLayout(new java.awt.BorderLayout());

        jPanelMenu.setLayout(new java.awt.GridBagLayout());

        jXSearchField.setMinimumSize(new java.awt.Dimension(200, 23));
        jXSearchField.setPreferredSize(new java.awt.Dimension(200, 23));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelMenu.add(jXSearchField, gridBagConstraints);

        jToolBarList.setFloatable(false);
        jToolBarList.setRollover(true);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setToolTipText("Add an element to the list");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonAdd);

        jButtonRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeDocumentIcon.png"))); // NOI18N
        jButtonRemove.setText("Remove");
        jButtonRemove.setToolTipText("Remove selected elements from the list");
        jButtonRemove.setFocusable(false);
        jButtonRemove.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonRemove);

        jButtonMoveUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowUpIcon.png"))); // NOI18N
        jButtonMoveUp.setText("Up");
        jButtonMoveUp.setFocusable(false);
        jButtonMoveUp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveUp.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveUpActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonMoveUp);

        jButtonMoveDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowDownIcon.png"))); // NOI18N
        jButtonMoveDown.setText("Down");
        jButtonMoveDown.setFocusable(false);
        jButtonMoveDown.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveDown.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveDownActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonMoveDown);

        jButtonSortAlphabetic.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/sortAlphabeticIcon.png"))); // NOI18N
        jButtonSortAlphabetic.setText("Sort");
        jButtonSortAlphabetic.setFocusable(false);
        jButtonSortAlphabetic.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSortAlphabetic.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSortAlphabeticActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonSortAlphabetic);

        jButtonAutoMatch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png"))); // NOI18N
        jButtonAutoMatch.setText("Fuzzy Match");
        jButtonAutoMatch.setToolTipText("Uses fuzzy logic to find a 80% match between the identifiers");
        jButtonAutoMatch.setFocusable(false);
        jButtonAutoMatch.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButtonAutoMatch.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAutoMatch.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAutoMatchActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonAutoMatch);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelMenu.add(jToolBarList, gridBagConstraints);

        add(jPanelMenu, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        addIdentifierRow(null, null);
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveActionPerformed
        removeSelectedRows();
    }//GEN-LAST:event_jButtonRemoveActionPerformed

    private void jButtonMoveUpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveUpActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveUpActionPerformed
        moveSelectionUp();
    }//GEN-LAST:event_jButtonMoveUpActionPerformed

    private void jButtonMoveDownActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveDownActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveDownActionPerformed
        moveSelectionDown();
    }//GEN-LAST:event_jButtonMoveDownActionPerformed

    private void jButtonSortAlphabeticActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSortAlphabeticActionPerformed
    {//GEN-HEADEREND:event_jButtonSortAlphabeticActionPerformed
        sortAlphabetic();
    }//GEN-LAST:event_jButtonSortAlphabeticActionPerformed

    private void jButtonAutoMatchActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAutoMatchActionPerformed
    {//GEN-HEADEREND:event_jButtonAutoMatchActionPerformed
        tryAutoMatch();
    }//GEN-LAST:event_jButtonAutoMatchActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonAutoMatch;
    private javax.swing.JButton jButtonMoveDown;
    private javax.swing.JButton jButtonMoveUp;
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JButton jButtonSortAlphabetic;
    private javax.swing.JPanel jPanelMenu;
    private javax.swing.JToolBar jToolBarList;
    private org.jdesktop.swingx.JXSearchField jXSearchField;
    // End of variables declaration//GEN-END:variables
}
