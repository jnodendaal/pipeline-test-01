package com.pilog.t8.developer.definitions.graphview.flow;

import com.pilog.graph.model.GraphEdge.EdgeType;
import com.pilog.t8.developer.definitions.graphview.flow.T8WorkFlowGraphConfiguration.LayoutType;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.TransferHandler;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowConfigurationPanel extends javax.swing.JPanel
{
    private final T8WorkFlowGraphManager graphManager;
    private final T8WorkFlowGraphConfiguration config;
    private final Preferences preferences;

    public T8WorkFlowConfigurationPanel(T8WorkFlowGraphManager graphManager, T8WorkFlowGraphConfiguration config)
    {
        this.preferences = Preferences.userNodeForPackage(T8WorkFlowGraphManager.class);
        initComponents();
        this.graphManager = graphManager;
        this.config = config;
        initPanel();
    }

    private void initPanel()
    {
        DefaultListModel listModel;

        listModel = new DefaultListModel();
        for (String identifier : graphManager.getSwimLaneIdentifierSequence())
        {
            listModel.addElement(identifier);
        }

        listModel.addListDataListener(new SequenceListener());
        jListSwimLaneSequence.setModel(listModel);
        jListSwimLaneSequence.setDragEnabled(true);
        jListSwimLaneSequence.setDropMode(DropMode.INSERT);
        jListSwimLaneSequence.setTransferHandler(new ListDropHandler());
        new ListDragListener(jListSwimLaneSequence);

        // Add the layout types.
        for (LayoutType layoutType : LayoutType.values())
        {
            jComboBoxLayoutType.addItem(layoutType);
        }

        // Set the selected layout type and add the selection listener.
        jComboBoxLayoutType.setSelectedItem(config.getLayoutType());
        jComboBoxLayoutType.addItemListener(new LayoutTypeListener());

        // Add the edge types.
        for (EdgeType edgeType : EdgeType.values())
        {
            jComboBoxEdgeType.addItem(edgeType);
        }

        // Set the selected edge type and add the selection listener.
        jComboBoxEdgeType.setSelectedItem(config.getEdgeType());
        jComboBoxEdgeType.addItemListener(new EdgeTypeListener());

        // Set the border glow checkbox state.
        jCheckBoxBorderGlow.setSelected(config.isBorderGlowEnabled());

        // Set the shadows checkbox state.
        jCheckBoxShadows.setSelected(config.isShadowsEnabled());
    }

    private void applyLayoutType(LayoutType type)
    {
        preferences.put(T8WorkFlowGraphManager.LAYOUT_TYPE_PREFERENCE, type.toString());
        config.setLayoutType(type);
    }

    private void applyEdgeType(EdgeType type)
    {
        preferences.put(T8WorkFlowGraphManager.EDGE_TYPE_PREFERENCE, type.toString());
        config.setEdgeType(type);
    }

    private void applySwimLaneSequence()
    {
        List<String> swimLaneIdentifiers;
        ListModel listModel;

        swimLaneIdentifiers = new ArrayList<String>();
        listModel = jListSwimLaneSequence.getModel();
        for (int index = 0; index < listModel.getSize(); index++)
        {
            String identifier;

            identifier = (String)listModel.getElementAt(index);
            swimLaneIdentifiers.add(identifier);
        }

        graphManager.setSwimLaneIdentifierSequence(swimLaneIdentifiers);
    }

    private class SequenceListener implements ListDataListener
    {
        @Override
        public void intervalAdded(ListDataEvent e)
        {
            applySwimLaneSequence();
        }

        @Override
        public void intervalRemoved(ListDataEvent e)
        {
        }

        @Override
        public void contentsChanged(ListDataEvent e)
        {
        }
    }

    private class ListDragListener implements DragSourceListener, DragGestureListener
    {
        private JList list;
        private DragSource ds = new DragSource();

        public ListDragListener(JList list)
        {
            this.list = list;
            DragGestureRecognizer dgr = ds.createDefaultDragGestureRecognizer(list, DnDConstants.ACTION_MOVE, this);
        }

        @Override
        public void dragGestureRecognized(DragGestureEvent dge)
        {
            StringSelection transferable = new StringSelection(Integer.toString(list.getSelectedIndex()));
            ds.startDrag(dge, DragSource.DefaultCopyDrop, transferable, this);
        }

        @Override
        public void dragEnter(DragSourceDragEvent dsde)
        {
        }

        @Override
        public void dragExit(DragSourceEvent dse)
        {
        }

        @Override
        public void dragOver(DragSourceDragEvent dsde)
        {
        }

        @Override
        public void dragDropEnd(DragSourceDropEvent dsde)
        {
        }

        @Override
        public void dropActionChanged(DragSourceDragEvent dsde)
        {
        }
    }

    private class ListDropHandler extends TransferHandler
    {
        @Override
        public boolean canImport(TransferHandler.TransferSupport support)
        {
            if (!support.isDataFlavorSupported(DataFlavor.stringFlavor))
            {
                return false;
            }
            else
            {
                JList.DropLocation dropLocation;

                dropLocation = (JList.DropLocation)support.getDropLocation();
                if (dropLocation.getIndex() == -1)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        @Override
        public boolean importData(TransferHandler.TransferSupport support)
        {
            if (!canImport(support))
            {
                return false;
            }
            else
            {
                Transferable transferable;
                DefaultListModel listModel;
                JList.DropLocation dropLocation;
                String indexString;
                Object modelElement;
                int dropTargetIndex;
                int index;

                try
                {
                    transferable = support.getTransferable();
                    indexString = (String)transferable.getTransferData(DataFlavor.stringFlavor);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    return false;
                }

                index = Integer.parseInt(indexString);
                dropLocation = (JList.DropLocation)support.getDropLocation();
                dropTargetIndex = dropLocation.getIndex();
                listModel = (DefaultListModel)((JList)support.getComponent()).getModel();
                modelElement = listModel.getElementAt(index);
                listModel.removeElement(modelElement);
                listModel.insertElementAt(modelElement, dropTargetIndex);
                return true;
            }
        }
    }

    private class LayoutTypeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                applyLayoutType((LayoutType)e.getItem());
            }
        }
    }

    private class EdgeTypeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                applyEdgeType((EdgeType)e.getItem());
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelLayoutType = new javax.swing.JLabel();
        jComboBoxLayoutType = new javax.swing.JComboBox();
        jLabelEdgeType = new javax.swing.JLabel();
        jComboBoxEdgeType = new javax.swing.JComboBox();
        jLabelBorderGlowEnabled = new javax.swing.JLabel();
        jCheckBoxBorderGlow = new javax.swing.JCheckBox();
        jLabelShadowsEnabled = new javax.swing.JLabel();
        jCheckBoxShadows = new javax.swing.JCheckBox();
        jLabelSwimLaneSequence = new javax.swing.JLabel();
        jScrollPaneSwimLaneSequence = new javax.swing.JScrollPane();
        jListSwimLaneSequence = new javax.swing.JList();

        setLayout(new java.awt.GridBagLayout());

        jLabelLayoutType.setText("Layout Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelLayoutType, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jComboBoxLayoutType, gridBagConstraints);

        jLabelEdgeType.setText("Edge Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelEdgeType, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jComboBoxEdgeType, gridBagConstraints);

        jLabelBorderGlowEnabled.setText("Border Glow Enabled:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelBorderGlowEnabled, gridBagConstraints);

        jCheckBoxBorderGlow.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jCheckBoxBorderGlowItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jCheckBoxBorderGlow, gridBagConstraints);

        jLabelShadowsEnabled.setText("Shadows Enabled:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelShadowsEnabled, gridBagConstraints);

        jCheckBoxShadows.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jCheckBoxShadowsItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jCheckBoxShadows, gridBagConstraints);

        jLabelSwimLaneSequence.setText("Swim Lane Sequence:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelSwimLaneSequence, gridBagConstraints);

        jListSwimLaneSequence.setDragEnabled(true);
        jScrollPaneSwimLaneSequence.setViewportView(jListSwimLaneSequence);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        add(jScrollPaneSwimLaneSequence, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxBorderGlowItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jCheckBoxBorderGlowItemStateChanged
    {//GEN-HEADEREND:event_jCheckBoxBorderGlowItemStateChanged
        config.setBorderGlowEnabled(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_jCheckBoxBorderGlowItemStateChanged

    private void jCheckBoxShadowsItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jCheckBoxShadowsItemStateChanged
    {//GEN-HEADEREND:event_jCheckBoxShadowsItemStateChanged
        config.setShadowsEnabled(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_jCheckBoxShadowsItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox jCheckBoxBorderGlow;
    private javax.swing.JCheckBox jCheckBoxShadows;
    private javax.swing.JComboBox jComboBoxEdgeType;
    private javax.swing.JComboBox jComboBoxLayoutType;
    private javax.swing.JLabel jLabelBorderGlowEnabled;
    private javax.swing.JLabel jLabelEdgeType;
    private javax.swing.JLabel jLabelLayoutType;
    private javax.swing.JLabel jLabelShadowsEnabled;
    private javax.swing.JLabel jLabelSwimLaneSequence;
    private javax.swing.JList jListSwimLaneSequence;
    private javax.swing.JScrollPane jScrollPaneSwimLaneSequence;
    // End of variables declaration//GEN-END:variables
}
