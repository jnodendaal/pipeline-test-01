package com.pilog.t8.developer.definitions.navigatorview;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionSelectionEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.developer.definitions.dialog.T8IdentifierSwitchDialog;
import com.pilog.t8.developer.definitions.test.T8PainterTestFrame;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import com.pilog.t8.developer.utils.T8DefinitionValidationUtils;
import com.pilog.t8.developer.view.T8DefaultDeveloperView;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.components.messagedialogs.Toast.Style;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionNavigatorView extends T8DefaultDeveloperView implements T8DeveloperView
{
    private final T8DefinitionContext definitionContext;
    private final T8Context context;
    private final T8ClientContext clientContext;
    private final T8DefinitionManager definitionManager;
    private final T8DeveloperView parentView;
    private final T8DefinitionNavigatorTree navigatorTree;
    private final EventListenerList definitionListeners;
    private final T8SessionContext sessionContext;
    private T8DeveloperViewFactory viewFactory;
    private T8DefinitionTestHarness testHarness;
    private boolean borderVisible;

    public enum NavigatorToolBarOption {SAVE, EDIT, RENAME, COPY, TEST, CONFIGURE_TEST, SWITCH_REFERENCE};

    public T8DefinitionNavigatorView(T8DefinitionContext definitionContext, T8DeveloperView parentView, T8Definition rootDefinition)
    {
        initComponents();
        this.definitionContext = definitionContext;
        this.context = definitionContext.getContext();
        this.clientContext = definitionContext.getClientContext();
        this.sessionContext = definitionContext.getSessionContext();
        this.parentView = parentView;
        this.viewFactory = parentView.getViewFactory();
        this.definitionManager = clientContext.getDefinitionManager();
        this.definitionListeners = new EventListenerList();
        this.navigatorTree = new T8DefinitionNavigatorTree(definitionContext, parentView, rootDefinition);
        this.navigatorTree.addTreeSelectionListener(new NavigatorSelectionListener());
        this.jScrollPaneTree.setViewportView(navigatorTree);
        this.jTextFieldFind.addKeyListener(new FindKeyListener());
        this.borderVisible = true;
        setRootDefinition(rootDefinition);

        createActionBindings();
    }

    @Override
    public String getHeader()
    {
        T8Definition rootDefinition;

        rootDefinition = getRootDefinition();
        return rootDefinition.getTypeMetaData().getDisplayName() + " Structure: " + rootDefinition.getIdentifier();
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        parentView.addView(view);
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        parentView.removeView(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return getRootDefinition();
    }

    @Override
    public void setSelectedDefinition(T8Definition definition)
    {
        setRootDefinition(definition);
    }

    @Override
    public void definitionLinkActivated(String identifier)
    {
        findDefinition(T8IdentifierUtilities.getLocalIdentifierPart(identifier));
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
        parentView.notifyDefinitionLinkActivated(event);
    }

    @Override
    public void addDefinitionViewListener(T8DefinitionViewListener listener)
    {
        definitionListeners.add(T8DefinitionViewListener.class, listener);
    }

    @Override
    public void removeDefinitionViewListener(T8DefinitionViewListener listener)
    {
        definitionListeners.remove(T8DefinitionViewListener.class, listener);
    }

    public void setToolBarVisible(boolean visible)
    {
        jToolBarMain.setVisible(visible);
    }

    public void setBorderVisible(boolean visible)
    {
        this.borderVisible = visible;
    }

    public void setToolBarOptionsVisible(List<NavigatorToolBarOption> options, boolean visible)
    {
        for (NavigatorToolBarOption option : options)
        {
            switch (option)
            {
                case SAVE:
                    jButtonSaveDefinition.setVisible(visible);
                    break;
                case EDIT:
                    jButtonEditDefinition.setVisible(visible);
                    break;
                case RENAME:
                    jButtonRenameDefinition.setVisible(visible);
                    break;
                case COPY:
                    jButtonCopyDefinition.setVisible(visible);
                    break;
                case TEST:
                    jButtonTestDefinition.setVisible(visible);
                    break;
                case CONFIGURE_TEST:
                    jButtonConfigureTestHarness.setVisible(visible);
                    break;
                case SWITCH_REFERENCE:
                    jButtonSwitchReference.setVisible(visible);
                    break;
                default:
                    break;
            }
        }
    }

    public void setRootDefinition(T8Definition definition)
    {
        // Set the new root definition on the navigator tree.
        if (definition != null)
        {
            this.navigatorTree.setRootDefinition(definition);
            this.setBorder(borderVisible ? new T8ContentHeaderBorder(getHeader()) : null);
        }
        else
        {
            this.navigatorTree.setRootDefinition(null);
            this.setBorder(borderVisible ? new T8ContentHeaderBorder("Definition Navigator") : null);
        }

        // Make sure to clear the test harness.
        this.testHarness = null;

        // Set the button configuration.
        this.navigatorTree.setDefinitionValidationErrors(null);
        this.jButtonCollapseDefinition.setEnabled(definition != null);
        this.jButtonConfigureTestHarness.setEnabled(definition != null);
        this.jButtonCopyDefinition.setEnabled(definition != null);
        this.jButtonEditDefinition.setEnabled(definition != null);
        this.jButtonExpandDefinition.setEnabled(definition != null);
        this.jButtonRenameDefinition.setEnabled(definition != null);
        this.jButtonSaveDefinition.setEnabled(definition != null);
        this.jButtonTestDefinition.setEnabled(definition != null);
        this.jButtonValidateDefinition.setEnabled(definition != null);
        this.jButtonSwitchReference.setEnabled(definition != null);
    }

    public T8Definition getRootDefinition()
    {
        return navigatorTree.getRootDefinition();
    }

    public void addToolbarButton(JButton button)
    {
        jToolBarMain.add(button);
    }

    public void setSaveEnabled(boolean enabled)
    {
        jButtonSaveDefinition.setVisible(enabled);
    }

    public void setTestEnabled(boolean enabled)
    {
        jButtonTestDefinition.setVisible(enabled);
    }

    public void setCopyEnabled(boolean enabled)
    {
        jButtonCopyDefinition.setVisible(enabled);
    }

    public void setRenameEnabled(boolean enabled)
    {
        jButtonRenameDefinition.setEnabled(enabled);
    }

    private void editDefinition()
    {
        T8Definition rootDefinition;

        rootDefinition = getRootDefinition();
        if (rootDefinition != null)
        {
            try
            {
                T8Definition editableDefinition;

                editableDefinition = definitionManager.lockDefinition(context, rootDefinition.getHandle(), sessionContext.getUserIdentifier());
                setRootDefinition(editableDefinition);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while locking definition: " + rootDefinition, e);
                JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + rootDefinition + "' could not be locked.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void saveDefinition()
    {
        T8Definition definition;

        definition = getRootDefinition();
        if (definition != null)
        {
            try
            {
                // Clear the selection to force any outstanding changes to be committed.  This may be redundant in future when a better listener is available.
                navigatorTree.clearSelection();

                setRootDefinition(clientContext.getDefinitionManager().unlockDefinition(context, definition, sessionContext.getUserIdentifier()));

                Toast.makeText(clientContext.getParentWindow(), "Definition '" + definition.getIdentifier() + "' has been successfully saved.", Style.SUCCESS).display();

                this.navigatorTree.setDefinitionValidationErrors(null);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + definition + "' could not be updated.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void configureTestHarness()
    {
        T8Definition definition;

        // Get the definition to be tested.
        definition = getRootDefinition();

        // If no test harness has been initialized, initialize it now.
        if (testHarness == null) testHarness = definition.getTestHarness(clientContext, sessionContext);
        if (testHarness != null)
        {
            testHarness.configure(context);
            jButtonConfigureTestHarness.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/gearPencilIcon.png")));
        }
        else
        {
            Toast.makeText(clientContext.getParentWindow(), "No configuration options are available for this definition type.", Style.ERROR).display();
        }
    }

    private void testDefinition()
    {
        T8Definition definition;

        // Get the definition to be tested.
        definition = getRootDefinition();

        // If no test harness has been initialized, initialize it now.
        if (testHarness == null) testHarness = definition.getTestHarness(clientContext, sessionContext);

        // Test the definition using the options available.
        if (testHarness != null)
        {
            testHarness.testDefinition(context, definition);
        }
        else if (definition instanceof T8PainterDefinition)
        {
            T8PainterTestFrame.testPainterDefinition(context, (T8PainterDefinition)definition, null);
        }
        else
        {
            Toast.makeText(clientContext.getParentWindow(), "No testing of this definition type is available.", Style.ERROR).display();
        }
    }

    /**
     * This method copies the root definition currently loaded into the
     * navigation view.
     */
    private void copyDefinition()
    {
        T8DefinitionMetaData metaData;
        T8Definition rootDefinition;
        String definitionIdentifier;

        rootDefinition = getRootDefinition();
        if(rootDefinition != null)
        {
            definitionIdentifier = rootDefinition.getIdentifier().replace(rootDefinition.getTypeMetaData().getIdPrefix(), "");
        }
        else
        {
            definitionIdentifier = null;
        }

        metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this.getRootPane(), rootDefinition.getTypeMetaData(), null, null, definitionIdentifier);
        if (metaData != null)
        {
            try
            {
                T8Definition newDefinition;

                newDefinition = definitionManager.copyDefinition(context, getRootDefinition(), metaData.getId());
                newDefinition.setProjectIdentifier(metaData.getProjectId());
                parentView.addView(viewFactory.createDefinitionNavigator(parentView, newDefinition));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method renames the root definition currently loaded into the
     * navigation view.
     */
    private void renameDefinition()
    {
        T8DefinitionMetaData metaData;
        T8Definition rootDefinition;
        String definitionIdentifier;

        rootDefinition = getRootDefinition();
        if (rootDefinition != null)
        {
            definitionIdentifier = rootDefinition.getIdentifier().replace(rootDefinition.getTypeMetaData().getIdPrefix(), "");
        }
        else
        {
            definitionIdentifier = null;
        }

        metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this.getRootPane(), rootDefinition.getTypeMetaData(), null, null, definitionIdentifier);
        if (metaData != null)
        {
            try
            {
                definitionManager.renameDefinition(context, getRootDefinition(), metaData.getId());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private void switchId()
    {
        T8Definition targetDefinition;

        targetDefinition = getSelectedDefinition();
        if (targetDefinition != null)
        {
            Pair<String, String> switchIds;

            switchIds = T8IdentifierSwitchDialog.getSwitchIds(clientContext, targetDefinition);
            if (switchIds != null)
            {
                String oldIdentifier;
                String newIdentifier;

                oldIdentifier = switchIds.getValue1();
                newIdentifier = switchIds.getValue2();
                targetDefinition.changeReferenceId(oldIdentifier, newIdentifier, true);
            }
        }
    }

    private void fireDefinitionSelectionChange(T8Definition selectedDefinition)
    {
        T8DefinitionSelectionEvent event;

        event = new T8DefinitionSelectionEvent(this, selectedDefinition);
        for (T8DefinitionViewListener listener : definitionListeners.getListeners(T8DefinitionViewListener.class))
        {
            listener.selectionChanged(event);
        }
    }

    private void expandDefinition()
    {
        navigatorTree.expandTree();
    }

    private void collapseDefinition()
    {
        navigatorTree.collapseTree();
    }

    private void findDefinition(String text)
    {
        List<DefaultMutableTreeNode> nodes;
        DefaultMutableTreeNode selectedNode;

        selectedNode = navigatorTree.getSelectedNode();
        nodes = navigatorTree.findDefinitionNodes(text, jCheckBoxFilterDatums.isSelected());
        if (nodes.size() > 0)
        {
            int index;

            index = nodes.indexOf(selectedNode);
            if (index > -1)
            {
                index++;
                if (index > nodes.size() -1) index = 0;
                navigatorTree.setSelectedNode(nodes.get(index));
            }
            else navigatorTree.setSelectedNode(nodes.get(0));
        }
        else Toast.makeText(clientContext.getParentWindow(), "No node matching the entered text was found.", Style.NORMAL).display();
    }

    private void validateDefinition()
    {
        try
        {
            T8DefinitionValidationUtils.validateDefinitions(SwingUtilities.windowForComponent(this), this.parentView, context, ArrayLists.typeSafeList(getRootDefinition().getIdentifier()));
        }
        catch (Exception e)
        {
            T8Log.log("Exception while validating definitions: " + getRootDefinition(), e);
            JOptionPane.showMessageDialog(this, "An unexpected exception occurred.", "Unexpected Exception", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void createActionBindings()
    {
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("control S"), "Save_Changes");

        getActionMap().put("Save_Changes", new AbstractAction("Save_Changes")
                   {

                       @Override
                       public void actionPerformed(ActionEvent e)
                       {
                           if(jButtonSaveDefinition.isEnabled() && jButtonSaveDefinition.isVisible())
                            saveDefinition();
                       }
        });
    }

    private class FindKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                findDefinition(jTextFieldFind.getText());
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    private class NavigatorSelectionListener implements TreeSelectionListener
    {
        @Override
        public void valueChanged(TreeSelectionEvent e)
        {
            T8Definition selectedDefinition;
            TreePath selectedPath;

            selectedDefinition = null;
            selectedPath = e.getNewLeadSelectionPath();
            if (selectedPath != null)
            {
                DefaultMutableTreeNode selectedNode;
                Object nodeObject;

                selectedNode = (DefaultMutableTreeNode)selectedPath.getLastPathComponent();
                nodeObject = selectedNode.getUserObject();
                if (nodeObject instanceof T8Definition)
                {
                    selectedDefinition = (T8Definition)nodeObject;
                }
                else fireDefinitionSelectionChange(null);
            }

            fireDefinitionSelectionChange(selectedDefinition);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarMain = new javax.swing.JToolBar();
        jButtonCopyDefinition = new javax.swing.JButton();
        jButtonRenameDefinition = new javax.swing.JButton();
        jButtonEditDefinition = new javax.swing.JButton();
        jButtonSaveDefinition = new javax.swing.JButton();
        jButtonTestDefinition = new javax.swing.JButton();
        jButtonConfigureTestHarness = new javax.swing.JButton();
        jButtonExpandDefinition = new javax.swing.JButton();
        jButtonCollapseDefinition = new javax.swing.JButton();
        jButtonValidateDefinition = new javax.swing.JButton();
        jButtonSwitchReference = new javax.swing.JButton();
        jPanelFilter = new javax.swing.JPanel();
        jLabelFind = new javax.swing.JLabel();
        jTextFieldFind = new javax.swing.JTextField();
        jCheckBoxFilterDatums = new javax.swing.JCheckBox();
        jScrollPaneTree = new javax.swing.JScrollPane();

        setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonCopyDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/documentCopyIcon.png"))); // NOI18N
        jButtonCopyDefinition.setToolTipText("Copy Definition");
        jButtonCopyDefinition.setFocusable(false);
        jButtonCopyDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCopyDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCopyDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCopyDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCopyDefinition);

        jButtonRenameDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/renameDocumentIcon.png"))); // NOI18N
        jButtonRenameDefinition.setToolTipText("Rename Definition");
        jButtonRenameDefinition.setFocusable(false);
        jButtonRenameDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRenameDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRenameDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRenameDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRenameDefinition);

        jButtonEditDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/pencilRulerIcon.png"))); // NOI18N
        jButtonEditDefinition.setToolTipText("Edit Definition");
        jButtonEditDefinition.setFocusable(false);
        jButtonEditDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonEditDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonEditDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonEditDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonEditDefinition);

        jButtonSaveDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/saveChangesIcon.png"))); // NOI18N
        jButtonSaveDefinition.setToolTipText("Save Definition");
        jButtonSaveDefinition.setFocusable(false);
        jButtonSaveDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSaveDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSaveDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSaveDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSaveDefinition);

        jButtonTestDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/testIcon.png"))); // NOI18N
        jButtonTestDefinition.setToolTipText("Test Definition");
        jButtonTestDefinition.setFocusable(false);
        jButtonTestDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonTestDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonTestDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonTestDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonTestDefinition);

        jButtonConfigureTestHarness.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/configurationIcon.png"))); // NOI18N
        jButtonConfigureTestHarness.setToolTipText("Configure Test Environment");
        jButtonConfigureTestHarness.setFocusable(false);
        jButtonConfigureTestHarness.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonConfigureTestHarness.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonConfigureTestHarness.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonConfigureTestHarnessActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonConfigureTestHarness);

        jButtonExpandDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/nodeSelectChildIcon.png"))); // NOI18N
        jButtonExpandDefinition.setToolTipText("Expand Definition Structure");
        jButtonExpandDefinition.setFocusable(false);
        jButtonExpandDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonExpandDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonExpandDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonExpandDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonExpandDefinition);

        jButtonCollapseDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/nodeInsertChildIcon.png"))); // NOI18N
        jButtonCollapseDefinition.setToolTipText("Collapse Definition Structure");
        jButtonCollapseDefinition.setFocusable(false);
        jButtonCollapseDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCollapseDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCollapseDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCollapseDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCollapseDefinition);

        jButtonValidateDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/bugExclamationIcon.png"))); // NOI18N
        jButtonValidateDefinition.setToolTipText("Validate Definiton");
        jButtonValidateDefinition.setFocusable(false);
        jButtonValidateDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonValidateDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonValidateDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonValidateDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonValidateDefinition);

        jButtonSwitchReference.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowSwitchIcon.png"))); // NOI18N
        jButtonSwitchReference.setToolTipText("Switch from one referenced ID to another.");
        jButtonSwitchReference.setFocusable(false);
        jButtonSwitchReference.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSwitchReference.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSwitchReference.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSwitchReferenceActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSwitchReference);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarMain, gridBagConstraints);

        jPanelFilter.setLayout(new java.awt.GridBagLayout());

        jLabelFind.setText("Find:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelFilter.add(jLabelFind, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelFilter.add(jTextFieldFind, gridBagConstraints);

        jCheckBoxFilterDatums.setText("Datums");
        jCheckBoxFilterDatums.setToolTipText("");
        jPanelFilter.add(jCheckBoxFilterDatums, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jPanelFilter, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jScrollPaneTree, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSaveDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSaveDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonSaveDefinitionActionPerformed
        saveDefinition();
    }//GEN-LAST:event_jButtonSaveDefinitionActionPerformed

    private void jButtonTestDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonTestDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonTestDefinitionActionPerformed
        testDefinition();
    }//GEN-LAST:event_jButtonTestDefinitionActionPerformed

    private void jButtonCopyDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCopyDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonCopyDefinitionActionPerformed
        copyDefinition();
    }//GEN-LAST:event_jButtonCopyDefinitionActionPerformed

    private void jButtonRenameDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRenameDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonRenameDefinitionActionPerformed
        renameDefinition();
    }//GEN-LAST:event_jButtonRenameDefinitionActionPerformed

    private void jButtonConfigureTestHarnessActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonConfigureTestHarnessActionPerformed
    {//GEN-HEADEREND:event_jButtonConfigureTestHarnessActionPerformed
        configureTestHarness();
    }//GEN-LAST:event_jButtonConfigureTestHarnessActionPerformed

    private void jButtonExpandDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExpandDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonExpandDefinitionActionPerformed
        expandDefinition();
    }//GEN-LAST:event_jButtonExpandDefinitionActionPerformed

    private void jButtonCollapseDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCollapseDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonCollapseDefinitionActionPerformed
        collapseDefinition();
    }//GEN-LAST:event_jButtonCollapseDefinitionActionPerformed

    private void jButtonEditDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonEditDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonEditDefinitionActionPerformed
        editDefinition();
    }//GEN-LAST:event_jButtonEditDefinitionActionPerformed

    private void jButtonValidateDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonValidateDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonValidateDefinitionActionPerformed
        validateDefinition();
    }//GEN-LAST:event_jButtonValidateDefinitionActionPerformed

    private void jButtonSwitchReferenceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSwitchReferenceActionPerformed
    {//GEN-HEADEREND:event_jButtonSwitchReferenceActionPerformed
        switchId();
    }//GEN-LAST:event_jButtonSwitchReferenceActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCollapseDefinition;
    private javax.swing.JButton jButtonConfigureTestHarness;
    private javax.swing.JButton jButtonCopyDefinition;
    private javax.swing.JButton jButtonEditDefinition;
    private javax.swing.JButton jButtonExpandDefinition;
    private javax.swing.JButton jButtonRenameDefinition;
    private javax.swing.JButton jButtonSaveDefinition;
    private javax.swing.JButton jButtonSwitchReference;
    private javax.swing.JButton jButtonTestDefinition;
    private javax.swing.JButton jButtonValidateDefinition;
    private javax.swing.JCheckBox jCheckBoxFilterDatums;
    private javax.swing.JLabel jLabelFind;
    private javax.swing.JPanel jPanelFilter;
    private javax.swing.JScrollPane jScrollPaneTree;
    protected javax.swing.JTextField jTextFieldFind;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
