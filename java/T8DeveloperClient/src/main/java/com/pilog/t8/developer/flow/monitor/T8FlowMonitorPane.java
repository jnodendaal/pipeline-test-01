package com.pilog.t8.developer.flow.monitor;

import com.google.common.collect.Lists;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.data.filter.basic.T8BasicDataFilterDefinition;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.ui.table.T8Table;
import com.pilog.t8.utilities.collections.HashMaps;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;

/**
 * An abstract parent for the flow monitor panels which groups the similar
 * functions that are performed by each panel.
 *
 * @author Gavin Boshoff
 */
public abstract class T8FlowMonitorPane extends javax.swing.JPanel
{
    protected final T8ComponentController controller;
    protected final String entityId;

    private final String tableDefinitionIdentifier;
    private T8Table table;

    public T8FlowMonitorPane(T8ComponentController controller, String tableId, String entityId)
    {
        this.tableDefinitionIdentifier = tableId;
        this.entityId = entityId;
        this.controller = controller;
    }

    public void setTableUserFilter(T8DataFilter dataFilter)
    {
        this.table.setPrefilter("REFRESH_FILTER", dataFilter);
        this.table.refreshData();
    }

    public String getDataEntityIdentifier()
    {
        return this.entityId;
    }

    public String getTableDefinitionIdentifier()
    {
        return this.tableDefinitionIdentifier;
    }

    public T8Table getTable()
    {
        return this.table;
    }

    protected abstract void setColumnVisibility(T8DataFieldDefinition dataFieldDefinition, T8TableColumnDefinition tableColumnDefinition);

    protected ArrayList<T8TableColumnDefinition> getTableColumnDefinitions(T8DataEntityDefinition dataEntityDefinition)
    {
        ArrayList<T8TableColumnDefinition> columnDefinitions;

        columnDefinitions = new ArrayList<>(dataEntityDefinition.getFieldDefinitions().size());
        for (T8DataFieldDefinition dataFieldDefinition : dataEntityDefinition.getFieldDefinitions())
        {
            T8TableColumnDefinition tableColumnDefinition;

            tableColumnDefinition = new T8TableColumnDefinition(dataFieldDefinition.getIdentifier());
            tableColumnDefinition.setColumnName(T8IdentifierUtilities.convertIdentifierToTitleCase(dataFieldDefinition.getIdentifier()));
            tableColumnDefinition.setFieldIdentifier(dataFieldDefinition.getPublicIdentifier());
            tableColumnDefinition.setWidth(100);
            tableColumnDefinition.setEditable(false);
            tableColumnDefinition.setRequired(false);

            //Set field visibility.
            setColumnVisibility(dataFieldDefinition, tableColumnDefinition);

            columnDefinitions.add(tableColumnDefinition);
        }

        return columnDefinitions;
    }

    protected void setTable(T8Table table)
    {
        this.table = table;
    }

    protected void setTable(T8TableDefinition tableDefinition, String header, Border insideBorder) throws Exception
    {
        this.table = (T8Table) tableDefinition.getNewComponentInstance(this.controller);
        this.table.initializeComponent(null);
        this.table.startComponent();
        this.table.setBorder(new CompoundBorder(new T8ContentHeaderBorder(getTranslatedString(header)), insideBorder));
        add(this.table, BorderLayout.CENTER);
    }

    protected T8TableDefinition buildTableDefinition(T8DataEntityDefinition dataEntityDefinition, ArrayList<T8TableColumnDefinition> columnDefinitions)
    {
        T8TableDefinition tableDefinition;

        tableDefinition = new T8TableDefinition(this.tableDefinitionIdentifier);
        tableDefinition.setVisible(true);
        tableDefinition.setOpaque(true);
        tableDefinition.setDataEntityIdentifier(this.entityId);
        tableDefinition.setEntityDefinition(dataEntityDefinition);
        tableDefinition.setColumnDefinitions(columnDefinitions);
        tableDefinition.setAutoResizeMode(T8TableDefinition.AutoResizeMode.OFF);
        tableDefinition.setPageSize(30);
        tableDefinition.setRowHeight(25);
        tableDefinition.setAutoSelectOnRetrieve(true);
        tableDefinition.setInsertEnabled(false);
        tableDefinition.setDeleteEnabled(false);
        tableDefinition.setCommitEnabled(false);
        tableDefinition.setUserFilterEnabled(true);
        tableDefinition.setUserRefreshEnabled(true);
        tableDefinition.setUserExportEnabled(false);
        tableDefinition.setToolBarVisible(true);
        tableDefinition.setOptionsEnabled(false);
        tableDefinition.setSelectionMode(T8TableDefinition.SelectionMode.SINGLE_SELECTION);
        tableDefinition.setResizeToColumnData(true);
        tableDefinition.setResizeToColumnHeader(true);

        return tableDefinition;
    }

    protected void setOrderFilter(T8TableDefinition tableDefinition, String orderByColumn, T8DataFilter.OrderMethod orderMethod)
    {
        HashMap<String, T8DataFilter.OrderMethod> orderingMap;
        T8BasicDataFilterDefinition filterDefinition;

        filterDefinition = new T8BasicDataFilterDefinition("TABLE_ORDERING_FILTER_DEFINITION");
        orderingMap = HashMaps.createTypeSafeMap(new String[]{this.entityId+orderByColumn}, new T8DataFilter.OrderMethod[]{orderMethod});
        filterDefinition.setFieldOrderingMap(orderingMap);

        tableDefinition.setPrefilterDefinitions(Lists.newArrayList((T8DataFilterDefinition)filterDefinition));
    }

    protected String getTranslatedString(String text)
    {
        return controller.translate(text);
    }
}
