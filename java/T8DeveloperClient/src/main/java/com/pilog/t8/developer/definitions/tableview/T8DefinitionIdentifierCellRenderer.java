package com.pilog.t8.developer.definitions.tableview;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionIdentifierCellRenderer extends DefaultTableCellRenderer
{
    private T8DefinitionTableView listView;

    public static final Color EDITED_BACKGROUND_COLOR = new Color(130,180,200);
    public static final Color EDITED_FOREGROUND_COLOR = new Color(0,0,255);
    public static final Color DELETED_BACKGROUND_COLOR = new Color(255,180,180);
    public static final Color DELETED_FOREGROUND_COLOR = new Color(255,0,0);

    public T8DefinitionIdentifierCellRenderer(T8DefinitionTableView tableView)
    {
        this.listView = tableView;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean cellHasFocus, int row, int column)
    {
        Component renderComponent;
        Color textColor;
        Font renderFont;

        renderComponent = super.getTableCellRendererComponent(table, value, isSelected, cellHasFocus, row, column);

        // Reset to defaults.
        if (!isSelected) renderComponent.setBackground(Color.WHITE);
        renderFont = renderComponent.getFont().deriveFont(Font.PLAIN);
        textColor = Color.BLACK;

        if (listView.isDefinitionUpdated(row))
        {
            textColor = EDITED_FOREGROUND_COLOR;
            renderFont = renderFont.deriveFont(Font.BOLD);
            if (!isSelected) renderComponent.setBackground(EDITED_BACKGROUND_COLOR);
        }
        else if (listView.isDefinitionDeleted(row))
        {
            textColor = DELETED_FOREGROUND_COLOR;
            renderFont = renderFont.deriveFont(Font.BOLD);
            if (!isSelected) renderComponent.setBackground(DELETED_BACKGROUND_COLOR);
        }

        renderComponent.setForeground(textColor);
        renderComponent.setFont(renderFont);
        return renderComponent;
    }
}
