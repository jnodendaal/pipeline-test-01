package com.pilog.t8.developer.definitions.actionhandlers.ui.entityeditor.panel;

import com.pilog.t8.definition.ui.label.T8LabelDefinition.HorizontalTextAlignment;
import com.pilog.t8.definition.ui.label.T8LabelDefinition.VerticalTextAlignment;
import java.awt.Insets;
import java.io.Serializable;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8PanelDataEntityEditorSetup implements Serializable
{
    private boolean addLabels;
    private int verticalSpacing;
    private int horizontalSpacing;
    private Insets insets;
    private HorizontalTextAlignment labelHorizontalTextAlignment;
    private VerticalTextAlignment labelVerticalTextAlignment;
    private List<T8PanelDataEntityEditorFieldSetup> fieldSetup;
    
    public T8PanelDataEntityEditorSetup()
    {
        addLabels = true;
        verticalSpacing = 10;
        horizontalSpacing = 10;
        labelHorizontalTextAlignment = HorizontalTextAlignment.RIGHT;
        labelVerticalTextAlignment = VerticalTextAlignment.CENTER;
    }

    public boolean isAddLabels()
    {
        return addLabels;
    }

    public void setAddLabels(boolean addLabels)
    {
        this.addLabels = addLabels;
    }

    public int getVerticalSpacing()
    {
        return verticalSpacing;
    }

    public void setVerticalSpacing(int verticalSpacing)
    {
        this.verticalSpacing = verticalSpacing;
    }

    public int getHorizontalSpacing()
    {
        return horizontalSpacing;
    }

    public void setHorizontalSpacing(int horizontalSpacing)
    {
        this.horizontalSpacing = horizontalSpacing;
    }

    public Insets getInsets()
    {
        return insets;
    }

    public void setInsets(Insets insets)
    {
        this.insets = insets;
    }
    
    public List<T8PanelDataEntityEditorFieldSetup> getFieldSetup()
    {
        return fieldSetup;
    }

    public void setFieldSetup(List<T8PanelDataEntityEditorFieldSetup> fieldSetup)
    {
        this.fieldSetup = fieldSetup;
    }

    public HorizontalTextAlignment getLabelHorizontalTextAlignment()
    {
        return labelHorizontalTextAlignment;
    }

    public void setLabelHorizontalTextAlignment(HorizontalTextAlignment labelHorizontalTextAlignment)
    {
        this.labelHorizontalTextAlignment = labelHorizontalTextAlignment;
    }
    
    public VerticalTextAlignment getLabelVerticalTextAlignment()
    {
        return labelVerticalTextAlignment;
    }

    public void setLabelVerticalTextAlignment(VerticalTextAlignment labelVerticalTextAlignment)
    {
        this.labelVerticalTextAlignment = labelVerticalTextAlignment;
    }
}
