package com.pilog.t8.developer.definitions.actionhandlers.data.source;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.Action;

import static com.pilog.t8.definition.data.T8DataManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataSourceDefinitionActionHandler implements T8DefinitionActionHandler
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DataSourceDefinition definition;

    public T8DataSourceDefinitionActionHandler(T8Context context, T8DataSourceDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new FieldAutoCreationAction(context, definition));
        actions.add(new CopySQLStringToClipBoardAction(context, definition));
        if(definition instanceof T8SQLDataSourceDefinition) actions.add(new ConvertDataSourceGUID(context, (T8SQLDataSourceDefinition)definition));

        return actions;
    }

    private static class FieldAutoCreationAction extends AbstractAction
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8DataSourceDefinition definition;

        public FieldAutoCreationAction(T8Context context, T8DataSourceDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Auto-Create Data Source Fields");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                //We need to move the focus incase any datum editors need to commit new values on focus change events
                ((Component)event.getSource()).getFocusCycleRootAncestor().transferFocusDownCycle();

                List<T8DataSourceFieldDefinition> fieldDefinitions;
                T8DefinitionManager definitionManager;

                definitionManager = clientContext.getDefinitionManager();
                definitionManager.saveDefinition(context, (T8DataSourceDefinition)definition, null);
                fieldDefinitions = retrieveDataSourceFieldDefinitions(context, definition.getRootProjectId(), definition.getIdentifier());
                // Make sure that we don't set a null list value on the definition, then add the fields to the data source definition.
                if (fieldDefinitions == null) fieldDefinitions = new ArrayList<T8DataSourceFieldDefinition>();
                definition.setFieldDefinitions(fieldDefinitions);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving field definitions for data source: " + definition.getIdentifier(), e);
            }
        }
    }

    public static List<T8DataSourceFieldDefinition> retrieveDataSourceFieldDefinitions(T8Context context, String projectId, String sourceId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_PROJECT_ID, projectId);
        operationParameters.put(PARAMETER_DATA_SOURCE_IDENTIFIER, sourceId);
        return (List<T8DataSourceFieldDefinition>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_RETRIEVE_DATA_SOURCE_FIELD_DEFINITIONS, operationParameters).get(PARAMETER_FIELD_DEFINITION_LIST);
    }

    protected static class CopySQLStringToClipBoardAction extends AbstractAction implements ClipboardOwner
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8DataSourceDefinition definition;

        public CopySQLStringToClipBoardAction(T8Context context, T8DataSourceDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Copy SQL to Clipboard");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/documentCopyIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                String sqlQueryString;

                sqlQueryString = retrieveSQLString(context, definition.getRootProjectId(), definition.getIdentifier());


                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(new StringSelection(sqlQueryString), this);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving field definitions for data source: " + definition.getIdentifier(), e);
            }
        }

        @Override
        public void lostOwnership(Clipboard clipboard, Transferable contents)
        {
        }

        public static String retrieveSQLString(T8Context context, String projectId, String dataSourceId) throws Exception
        {
            HashMap<String, Object> operationParameters;

            operationParameters = new HashMap<String, Object>();
            operationParameters.put(PARAMETER_DATA_SOURCE_IDENTIFIER, dataSourceId);
            return (String) T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_SQL_QUERY_STRING, operationParameters).get(PARAMETER_SQL_QUERY_STRING);
        }
    }

    protected class ConvertDataSourceGUID extends AbstractAction implements ClipboardOwner
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8SQLDataSourceDefinition definition;

        public ConvertDataSourceGUID(T8Context context, T8SQLDataSourceDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Convert GUID values to DB Adaptor");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/pencilRulerIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (definition != null)
                {
                    String newSelectString;
                    String newWithString;
                    Map<String, String> newExpressionMap;

                    // Get the parameter expression map to update.
                    newExpressionMap = new HashMap<String, String>();
                    if (definition.getSQLParameterExpressionMap() != null) newExpressionMap.putAll(definition.getSQLParameterExpressionMap());

                    // Get the new SELECT and WITH strings.
                    newSelectString = replaceGUIDWithAdaptor(definition.getSQLSelectString(), newExpressionMap);
                    newWithString = replaceGUIDWithAdaptor(definition.getSQLWithString(), newExpressionMap);

                    // Update the definition.
                    definition.setSQLSelectString(newSelectString);
                    definition.setSQLWithString(newWithString);
                    definition.setSQLParameterExpressionMap(newExpressionMap);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving field definitions for data source: " + definition.getIdentifier(), e);
            }
        }


        private String replaceGUIDWithAdaptor(String stringToCheck, Map<String, String> parameterExpressionMap)
        {
            String replacementString = stringToCheck;
            Matcher matcher;

            // Case 1: Strings containing STUFF.
            matcher = Pattern.compile("([STUF\\(]{24}+'?)[^',]+([',]?[,0-9\\-'\\) ]{51})").matcher(replacementString);
            while (matcher.find())
            {
                Matcher replaceMatcher;

                // Find the GUID Strings.
                replaceMatcher = Pattern.compile("\\([^,\\(]+,").matcher(matcher.group());
                while (replaceMatcher.find())
                {
                    String guidStripped;

                    guidStripped = replaceMatcher.group().replaceAll("[-'\\(,]", "");
                    if (!parameterExpressionMap.containsKey(guidStripped))
                    {
                        boolean isGuid;

                        isGuid = guidStripped.matches("[A-z0-9]{32}");
                        parameterExpressionMap.put(guidStripped, "dbAdaptor.getSQLColumnHexToGUID(\"" + (isGuid ? "'" : "") + guidStripped+ (isGuid ? "'" : "") + "\")");
                    }

                    replacementString = replacementString.replaceFirst(matcher.pattern().pattern(), " <<-" + guidStripped + "->> ");
                }

                matcher.reset(replacementString);
            }

            // Case 2: Strings containing MSSQL dbo guid function.
            matcher = Pattern.compile("\\[?[DdBbOo]{3}\\]?\\.\\[?HexToGUID\\]?\\('*.+'*\\)").matcher(replacementString);
            while (matcher.find())
            {
                String guidStripped;

                guidStripped = matcher.group().replaceAll("\\[?[DdBbOo]{3}\\]?\\.\\[?HexToGUID\\]?\\(", "").replaceAll("[-']", "").replaceAll("\\)", "");
                if (!parameterExpressionMap.containsKey(guidStripped))
                {
                    boolean isGuid;

                    isGuid = guidStripped.matches("[A-z0-9]{32}");
                    parameterExpressionMap.put(guidStripped, "dbAdaptor.getSQLColumnHexToGUID(\"" + (isGuid ? "'" : "") + guidStripped+ (isGuid ? "'" : "") + "\")");
                }

                replacementString = replacementString.replaceFirst(matcher.pattern().pattern(), " <<-" + guidStripped + "->> ");
                matcher.reset(replacementString);
            }

            // Case 3: Strings just containing an MSSQL type compatible GUID.
            matcher = Pattern.compile("'{1}[A-z0-9]{8}-{1}[A-z0-9]{4}-{1}[A-z0-9]{4}-{1}[A-z0-9]{4}-{1}[A-z0-9]{12}'{1}").matcher(replacementString);
            while (matcher.find())
            {
                String guidStripped;

                guidStripped = matcher.group().replaceAll("[-']", "");
                if (!parameterExpressionMap.containsKey(guidStripped))
                {
                    parameterExpressionMap.put(guidStripped, "dbAdaptor.getSQLColumnHexToGUID(\"'" + guidStripped + "'\")");
                }

                replacementString = replacementString.replaceFirst(matcher.pattern().pattern(), " <<-" + guidStripped + "->> ");
                matcher.reset(replacementString);
            }

            // Case 4: Strings containing Oracle type GUID.
            matcher = Pattern.compile("'{1}[A-Fa-f0-9]{32}'{1}").matcher(replacementString);
            while (matcher.find())
            {
                String guidStripped;

                guidStripped = matcher.group().replaceAll("[-']", "");
                if (!parameterExpressionMap.containsKey(guidStripped))
                {
                    parameterExpressionMap.put(guidStripped, "dbAdaptor.getSQLColumnHexToGUID(\"'" + guidStripped + "'\")");
                }

                replacementString = replacementString.replaceFirst(matcher.pattern().pattern(), " <<-" + guidStripped + "->> ");
                matcher.reset(replacementString);
            }

            // Lastly make it look nice again by removing double spaces.
            replacementString = replacementString.replaceAll(" +(<<-)", " <<-").replaceAll("(->>) +", "->> ");
            return replacementString;
        }

        @Override
        public void lostOwnership(Clipboard clipboard, Transferable contents)
        {
        }
    }
}
