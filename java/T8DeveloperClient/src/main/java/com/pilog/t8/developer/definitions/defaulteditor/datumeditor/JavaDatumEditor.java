package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * @author Bouwer du Preez
 */
public class JavaDatumEditor extends T8DefaultDefinitionDatumEditor implements FocusListener
{
    private JavaEditorPanel editorPanel;

    public JavaDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        editorPanel.addFocusListener(this);
    }

    public void setEditorHeight(int height)
    {
        setPreferredSize(new Dimension(0, height));
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        editorPanel.startComponent();
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
        editorPanel.stopComponent();
    }

    @Override
    public void commitChanges()
    {
        setDefinitionDatum(editorPanel.getSourceCode());
    }

    @Override
    public void setEditable(boolean editable)
    {
        editorPanel.setEditable(editable);
    }

    @Override
    public void refreshEditor()
    {
        Object value;

        value = getDefinitionDatum();
        editorPanel.setSourceCode(value != null ? value.toString() : null);
    }

    @Override
    public void focusGained(FocusEvent e)
    {
    }

    @Override
    public void focusLost(FocusEvent e)
    {
        commitChanges();
    }

    private void initComponents()
    {
        setPreferredSize(new java.awt.Dimension(0, 500));
        setLayout(new java.awt.BorderLayout());

        editorPanel = new JavaEditorPanel(this);
        add(editorPanel, java.awt.BorderLayout.CENTER);
    }
}
