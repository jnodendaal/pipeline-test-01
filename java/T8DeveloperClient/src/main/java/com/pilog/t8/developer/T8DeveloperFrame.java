package com.pilog.t8.developer;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.developer.monitor.operation.T8OperationMonitor;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.system.T8ClientEnvironmentStatus;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.definition.T8DefinitionTypeDefinition;
import com.pilog.t8.definition.communication.T8CommunicationDefinition;
import com.pilog.t8.definition.communication.T8CommunicationServiceDefinition;
import com.pilog.t8.definition.communication.detail.provider.T8CommunicationDetailsProviderDefinition;
import com.pilog.t8.definition.communication.message.template.T8CommunicationMessageDefinition;
import com.pilog.t8.definition.data.T8IdentifierValueDefinition;
import com.pilog.t8.definition.data.cache.T8DataCacheDefinition;
import com.pilog.t8.definition.data.connection.pool.T8DataConnectionPoolDefinition;
import com.pilog.t8.definition.data.database.T8DatabaseAdaptorDefinition;
import com.pilog.t8.definition.data.document.datarecord.access.T8DataRecordAccessDefinition;
import com.pilog.t8.definition.data.document.datarecord.access.logic.T8DataAccessLogicDefinition;
import com.pilog.t8.definition.data.document.datarecord.view.T8DataRecordViewDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordMergerDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationResponseDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationServiceDefinition;
import com.pilog.t8.definition.data.document.reverse.integration.T8ReverseIntegrationDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.key.T8DataKeyGeneratorDefinition;
import com.pilog.t8.definition.data.model.T8DataModelDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateGraphDefinition;
import com.pilog.t8.definition.data.source.cte.T8CTEDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordAttachmentHandlerDefinition;
import com.pilog.t8.definition.data.type.T8DataTypeDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.editor.T8DefinitionEditorSetupDefinition;
import com.pilog.t8.definition.event.T8DefinitionEventHandlerDefinition;
import com.pilog.t8.definition.exception.T8ExceptionDefinition;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowProfileDefinition;
import com.pilog.t8.definition.flow.signal.T8FlowSignalDefinition;
import com.pilog.t8.definition.flow.task.T8TaskEscalationDefinition;
import com.pilog.t8.definition.flow.task.object.T8TaskObjectDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.functionality.access.T8FunctionalityAccessPolicyDefinition;
import com.pilog.t8.definition.functionality.group.T8FunctionalityGroupDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.help.T8HelpDisplayDefinition;
import com.pilog.t8.definition.language.T8LanguageDefinition;
import com.pilog.t8.definition.language.T8UITranslationDefinition;
import com.pilog.t8.definition.ng.T8NgComponentDefinition;
import com.pilog.t8.definition.notification.T8NotificationDefinition;
import com.pilog.t8.definition.notification.T8SystemNotificationDefinition;
import com.pilog.t8.definition.operation.T8ClientOperationDefinition;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalSettingsDefinition;
import com.pilog.t8.definition.patch.T8PatchDefinition;
import com.pilog.t8.definition.procedure.T8ProcedureDefinition;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.definition.remote.server.connection.T8ConnectionDefinition;
import com.pilog.t8.definition.service.T8ServiceDefinition;
import com.pilog.t8.definition.system.T8ConfigurationManagerResource;
import com.pilog.t8.definition.system.T8AdministrationResource;
import com.pilog.t8.definition.system.T8AdministrationResource.ValidationType;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.definition.system.property.T8SystemPropertyDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8GlobalEventDefinition;
import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxDataSourceDefinition;
import com.pilog.t8.definition.ui.dialog.T8GlobalDialogDefinition;
import com.pilog.t8.definition.ui.laf.T8LookAndFeelDefinition;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.definition.user.T8UserProfileDefinition;
import com.pilog.t8.definition.user.password.T8PasswordPolicyDefinition;
import com.pilog.t8.definition.user.property.T8UserPropertyDefinition;
import com.pilog.t8.developer.definitions.T8DeveloperDefinitionContext;
import com.pilog.t8.developer.definitions.dialog.T8InformationDialog;
import com.pilog.t8.developer.definitions.metapackagemanager.T8MetaPackageManagerPanel;
import com.pilog.t8.developer.definitions.validation.T8DefinitionQueryView;
import com.pilog.t8.developer.file.context.browser.T8FileContextBrowser;
import com.pilog.t8.developer.flow.monitor.T8FlowMonitor;
import com.pilog.t8.developer.functionality.monitor.T8FunctionalityMonitor;
import com.pilog.t8.developer.messenger.T8DeveloperMessageDialog;
import com.pilog.t8.developer.monitor.communication.T8CommunicationQueueMonitor;
import com.pilog.t8.developer.monitor.system.T8SystemMonitor;
import com.pilog.t8.developer.session.T8SessionMonitor;
import com.pilog.t8.developer.session.data.T8DataSessionMonitor;
import com.pilog.t8.security.T8ClientSecurityManager;
import com.pilog.t8.security.T8HeartbeatResponse;
import com.pilog.t8.mainserver.T8SessionExpiredException;
import com.pilog.t8.ui.laf.T8LookAndFeelFactory;
import com.pilog.t8.ui.notification.T8ClientNotificationCentre;
import com.pilog.t8.ui.operationexecutiondialog.T8DefaultOperationExecutionDialog;
import com.pilog.t8.ui.session.T8SessionExpiryDialog;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import com.pilog.version.T8DeveloperClientVersion;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.swing.animation.timing.sources.SwingTimerTimingSource;

import static com.pilog.t8.definition.system.T8AdministrationResource.PARAMETER_VALIDATION_TYPE;
import com.pilog.t8.developer.monitor.system.T8DataManagerAdministrationDialog;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DeveloperFrame extends javax.swing.JFrame
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DeveloperFrame.class);

    private T8Context context;
    private T8DeveloperClient developerClient;
    private ScheduledExecutorService executor; // Heartbeat scheduler.
    private HeartbeatThread heartbeatThread;
    private T8ClientNotificationCentre notificationCenter;

    private final static long HEARTBEAT_INTERVAL = 30000; // 30 seconds.
    /**
     * The amount of time before a session expires, that a message will be
     * displayed to inform the user that the session is about to expire.
     */
    private final static long SESSION_EXP_WARNING = (5 * 60000L); // 5 minutes
    /** The key value for the operating system name property. */
    private static final String OS_PROPERTY = "os.name";

    /** Creates new form T8DeveloperFrame */
    public T8DeveloperFrame()
    {
        try
        {
            T8ClientSecurityManager securityManager;
            T8SessionContext sessionContext;

            LOGGER.log("Starting T8Developer Version " + T8DeveloperClientVersion.VERSION + "...");

            // Initialize the Swing components.
            initComponents();

            // Create a new Security Manager and Session Context.
            securityManager = new T8ClientSecurityManager(null);
            sessionContext = securityManager.createNewSessionContext();

            setPiLogLookAndFeel();

            // Set the frame size;
            T8DeveloperFrame.this.setPreferredSize(new Dimension(1200, 800));
            T8DeveloperFrame.this.setMinimumSize(new Dimension(1200, 800));
            T8DeveloperFrame.this.setExtendedState(JFrame.MAXIMIZED_BOTH);

            // Instantiate a new client.
            developerClient = new T8DeveloperClient(sessionContext);

            // Add the new client to the JApplet.
            T8DeveloperFrame.this.add(developerClient, java.awt.BorderLayout.CENTER);
            T8DeveloperFrame.this.validate();

            // Initialize the client.
            developerClient.initializeComponent();
            context = developerClient.getContext();

            // Set the currently loaded identifier.
            T8DeveloperFrame.this.setTitle("Tech 8 Developer - " + developerClient.getDefinitionManager().getSystemDetails().getIdentifier() + " - " + context.getSessionContext().getServerURL());

            // Start the heartbeat thread.
            executor = Executors.newSingleThreadScheduledExecutor(new NameableThreadFactory("T8Developer-Heartbeat"));
            heartbeatThread = new HeartbeatThread(this, context);
            executor.scheduleWithFixedDelay(heartbeatThread, HEARTBEAT_INTERVAL, HEARTBEAT_INTERVAL, TimeUnit.MILLISECONDS);

            // Create the notification center.
            notificationCenter = new T8ClientNotificationCentre(context);
            developerClient.setNotificationCentre(notificationCenter);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing T8Developer.", e);
        }
    }

    private void setPiLogLookAndFeel()
    {
        LookAndFeel lookAndFeel;

        try
        {
            LOGGER.log(()->"JVM Set LAF : [" + UIManager.getLookAndFeel() + "] - System LAF : [" + UIManager.getSystemLookAndFeelClassName() + "] - Cross Platform LAF : [" + UIManager.getCrossPlatformLookAndFeelClassName() + "]");
            lookAndFeel = T8LookAndFeelFactory.getLookAndFeel(System.getProperty(OS_PROPERTY));

            UIManager.setLookAndFeel(lookAndFeel);
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch (UnsupportedLookAndFeelException | RuntimeException e)
        {
            LOGGER.log("Error encountered while setting Look & Feel.", e);
        }
    }

    private void setEnvironmentStatus(final T8ClientEnvironmentStatus status)
    {
        developerClient.setEnvironmentStatus(status);
        SwingUtilities.invokeLater(() ->
        {
            DecimalFormat decimalFormat;

            decimalFormat = new DecimalFormat("#0.00");

            jLabelNetworkLatency.setText("Latency: " + status.getNetworkLatency() + " ms");
            jLabelNetworkThroughput.setText("Throughput: " + decimalFormat.format(status.getNetworkThroughput() / 1000.00) + " kbit/s");
            jLabelServerActivity.setText("Server Activity: " + status.getServerActivity());
            jLabelServerLoad.setText("Server Load: " + decimalFormat.format(status.getServerLoad()) + "%");
            jLabelMaximumConcurrency.setText("Maximum Concurrency: " + status.getMaximumConcurrency());
        });
    }

    T8ClientNotificationCentre getNotificationCenter()
    {
        return notificationCenter;
    }

    private static class HeartbeatThread extends Thread
    {
        private T8Context context;
        private final T8DeveloperFrame developerFrame;
        private final T8SecurityManager securityManager;

        private boolean sessionExpired = false;

        private HeartbeatThread(T8DeveloperFrame developerFrame, T8Context context)
        {
            this.developerFrame = developerFrame;
            this.context = context;
            this.securityManager = developerFrame.getDeveloperClient().getSecurityManager();
        }

        @Override
        public void run()
        {
            if (this.context.getSessionContext().isAnonymousSession()) return;

            try
            {
                T8HeartbeatResponse response;
                T8ClientEnvironmentStatus status;
                long sessionTimeLeft;
                long latency;

                status = new T8ClientEnvironmentStatus();
                latency = System.currentTimeMillis();
                response = T8MainServerClient.heartbeat(context);
                latency = System.currentTimeMillis() - latency;
                if (response != null) // A response will only be received if the session has been logged in.
                {
                    // Update the environment status.
                    status.setNetworkLatency((int)latency);
                    status.setNetworkThroughput(T8MainServerClient.getStatistics().getAverageNetworkThroughput());
                    status.setServerActivity(response.getServerRequestsPerSecond());
                    status.setServerLoad(response.getServerLoad());
                    status.setMaximumConcurrency(response.getMaximumConcurrency());
                    developerFrame.setEnvironmentStatus(status);

                    // Check for the session expiry, if the session is about to expire show the login screen
                    sessionExpired = false;
                    sessionTimeLeft = response.getExpirationTime() - response.getServerTime();
                    if (response.getExpirationTime() > 0 && (sessionTimeLeft < HEARTBEAT_INTERVAL + 10000))
                    {
                        logout();
                    }
                    else
                    {
                        // We first check if there are pending notifications
                        if (response.hasPendingNotifications())
                        {
                            SwingUtilities.invokeLater(() ->
                            {
                                developerFrame.getNotificationCenter().refreshNotifications();
                            });
                        }
                        // Then we check if the session is about to expire
                        if (sessionTimeLeft < SESSION_EXP_WARNING)
                        {
                            if (T8SessionExpiryDialog.showExpiryDialog(developerFrame, sessionTimeLeft) == T8SessionExpiryDialog.EXTEND_SESSION)
                            {
                                this.securityManager.extendSession(this.context);
                            } else logout();
                        }
                    }
                }
            }
            catch (T8SessionExpiredException se)
            {
                // Check if this session has expired then show the login screen
                if (!sessionExpired)
                {
                    try
                    {
                        // We cannot do anything on the flow client any more unless we set a new anonymous session for the flow client
                        // securityManager.createNewSessionContext(); // TODO:  add a security manager method that takes an existing session object as input and recreates a new session from it.
                        sessionExpired = true;

                        showLogin();
                    }
                    catch (Exception ex)
                    {
                        LOGGER.log("Failed to create a new anonymous session context.", se);
                    }
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception in T8Client hearbeat.", e);
            }
        }

        private void showLogin()
        {
            // Restart the client frame.
            SwingUtilities.invokeLater(() ->
            {
                JOptionPane.showMessageDialog(developerFrame, "Your session has expired, please log in again.");
                while (!T8DeveloperLoginDialog.login(developerFrame.context, developerFrame.rootPane))
                {
                    // Do nothing, just show the login screen until the user logs in successfully.
                }
            });
        }

        private void logout() throws Exception
        {
            if (securityManager.isSessionActive(context, context.getSessionId()))
            {
                securityManager.logout(context);
                showLogin();
            }
        }
    }

    private void terminateHeartBeat()
    {
        // Terminate the heartbeat thread.
        try
        {
            executor.shutdown();
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) throw new Exception("Heartbeat thread executor could not by terminated.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while awaiting termination of heartbeat thread.", e);
        }
    }

    private boolean performClosingChecks()
    {
        // Check the developer client for unsaved changes etc.
        if (developerClient.getViewContainer().canClose())
        {
            terminateHeartBeat();
            developerClient.logout();
            return true;
        }
        return false;
    }

    public void startup()
    {
        new Thread(() ->
        {
            try
            {
                notificationCenter.startComponent();
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while starting developer.", e);
            }
        }).start();
    }

    private void shutdown()
    {
        if (performClosingChecks())
        {
            try
            {
                notificationCenter.stopComponent();
                developerClient.getNotificationManager().destroy();
                developerClient.getSecurityManager().logout(context);
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to shutdown client services", ex);
            }

            setVisible(false);
            dispose();
            System.exit(0);
        }
    }

    public T8DeveloperClient getDeveloperClient()
    {
        return developerClient;
    }

    private void addDefinitionGroupManager(String definitionGroupIdentifier, String viewHeader)
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), definitionGroupIdentifier), viewHeader);
    }

    private void addNewFlowMonitor() throws Exception
    {
        T8DeveloperView newEditor;

        newEditor = new T8FlowMonitor(context, developerClient.getViewContainer());
        developerClient.getViewContainer().addView(newEditor);
    }

    private void addNewSessionMonitor()
    {
        T8DeveloperView newEditor;

        newEditor = new T8SessionMonitor(context, developerClient.getViewContainer());
        developerClient.getViewContainer().addView(newEditor);
    }

    private void addNewDataSessionMonitor()
    {
        T8DeveloperView newEditor;

        newEditor = new T8DataSessionMonitor(context, developerClient.getViewContainer());
        developerClient.getViewContainer().addView(newEditor);
    }

    private void addNewCommunicationMonitor()
    {
        T8DeveloperView newEditor;

        newEditor = new T8CommunicationQueueMonitor(context, developerClient.getViewContainer());
        developerClient.getViewContainer().addView(newEditor);
    }

    private void addNewIconEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8IconDefinition.GROUP_IDENTIFIER), jMenuItemIconEditor.getText());
    }

    private void addNewClientOperationEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ClientOperationDefinition.GROUP_IDENTIFIER), jMenuItemClientOperationEditor.getText());
    }

    private void addNewServiceEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ServiceDefinition.GROUP_IDENTIFIER), jMenuItemServiceEditor.getText());
    }

    private void addNewMetaPackageManager()
    {
        developerClient.getViewContainer().addView(new T8MetaPackageManagerPanel(context));
    }

    private void addNewModuleEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ModuleDefinition.GROUP_IDENTIFIER), jMenuItemModuleEditor.getText());
    }

    private void addNewDialogEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8GlobalDialogDefinition.GROUP_IDENTIFIER), jMenuItemDialogEditor.getText());
    }

    private void addNewGlobalComponentEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ComponentDefinition.GROUP_IDENTIFIER), jMenuItemGlobalComponentEditor.getText());
    }

    private void addNewDataRecordAccessEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataRecordAccessDefinition.GROUP_IDENTIFIER), jMenuItemDataRecordAccessEditor.getText());
    }

    private void addNewAttachmentHandlerEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataRecordAttachmentHandlerDefinition.GROUP_IDENTIFIER), jMenuItemAttachmentHandlerEditor.getText());
    }

    private void addNewServerProcessEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ProcessDefinition.GROUP_IDENTIFIER), jMenuItemServerProcessEditor.getText());
    }

    private void addNewPainterEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8PainterDefinition.GROUP_IDENTIFIER), jMenuItemPainterEditor.getText());
    }

    private void addNewUserProfileEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8UserProfileDefinition.GROUP_IDENTIFIER), jMenuItemUserProfileEditor.getText());
    }

    private void addNewWorkFlowEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGraphManager(developerClient.getViewContainer(), T8WorkFlowDefinition.GROUP_IDENTIFIER), jMenuItemWorkFlowEditor.getText());
    }

    private void addNewDataStreamEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DStreamDefinition.GROUP_IDENTIFIER), jMenuItemDataStreamEditor.getText());
    }

    private void addNewDefinitionEditorSetupEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DefinitionEditorSetupDefinition.GROUP_IDENTIFIER), jMenuItemDefinitionEditorSetup.getText());
    }

    private void addNewWorkFlowProfileEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8WorkFlowProfileDefinition.GROUP_IDENTIFIER), jMenuItemWorkFlowProfileEditor.getText());
    }

    private void addNewDataConnectionPoolEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataConnectionPoolDefinition.GROUP_IDENTIFIER), jMenuItemDataConnectionPoolEditor.getText());
    }

    private void addNewOrganizationSetupEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8OrganizationSetupDefinition.GROUP_IDENTIFIER), jMenuItemOrganizationSetupEditor.getText());
    }

    private void addNewFileContextEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8FileContextDefinition.GROUP_IDENTIFIER), jMenuItemFileContextEditor.getText());
    }

    private void addNewFunctionalityEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8FunctionalityDefinition.GROUP_IDENTIFIER), jMenuItemFunctionalityEditor.getText());
    }

    private void addNewFunctionalityGroupEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8FunctionalityGroupDefinition.GROUP_IDENTIFIER), jMenuItemFunctionalityGroupEditor.getText());
    }

    private void addNewDataCacheEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataCacheDefinition.GROUP_IDENTIFIER), jMenuItemDataCacheEditor.getText());
    }

    private void addNewSystemEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8SystemDefinition.GROUP_IDENTIFIER), jMenuItemSystemEditor.getText());
    }

    private void addNewDefinitionSearchEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionSearchManager(developerClient.getViewContainer()));
    }

    private void addIdentifierValueEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8IdentifierValueDefinition.GROUP_IDENTIFIER), jMenuItemIdentifierValueEditor.getText());
    }

    private void addLookAndFeelEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8LookAndFeelDefinition.GROUP_IDENTIFIER), jMenuItemLookAndFeelEditor.getText());
    }

    private void addMergeConfigurationEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataRecordMergerDefinition.GROUP_IDENTIFIER), jMenuItemMergeConfigurationEditor.getText());
    }

    private void addIntegrationServiceEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8IntegrationServiceDefinition.GROUP_IDENTIFIER), jMenuItemIntegrationService.getText());
    }

    private void addIntegrationResponseEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8IntegrationResponseDefinition.GROUP_IDENTIFIER), jMenuItemIntegrationService.getText());
    }

    private void addProcedureEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ProcedureDefinition.GROUP_IDENTIFIER), jMenuItemProcedureEditor.getText());
    }

    private void addFlowTaskListEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8TaskObjectDefinition.GROUP_IDENTIFIER), jMenuItemFlowTaskListEditor.getText());
    }

    private void addAngularComponentEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8NgComponentDefinition.GROUP_IDENTIFIER), jMenuItemAngularComponentEditor.getText());
    }

    private void addTaskEscalationEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8TaskEscalationDefinition.GROUP_IDENTIFIER), jMenuItemTaskEscalationEditor.getText());
    }

    private void showDataManagerAdministrationDialog()
    {
        T8DataManagerAdministrationDialog.showDialog(SwingUtilities.getWindowAncestor(this), context);
    }

    private void addJsonEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionJsonManager(developerClient.getViewContainer()), jMenuItemJsonEditor.getText());
    }

    private void addDefinitionQueryView()
    {
        developerClient.getViewContainer().addView(new T8DefinitionQueryView(new T8DeveloperDefinitionContext(context), developerClient.getViewContainer()));
    }

    private void addReverseIntegrationServiceEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ReverseIntegrationDefinition.GROUP_IDENTIFIER), jMenuItemIntegrationService.getText());
    }

    private void addDataFileViewEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataRecordViewDefinition.GROUP_IDENTIFIER), jMenuItemDataRecordViewEditor.getText());
    }

    private void addExceptionEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ExceptionDefinition.GROUP_IDENTIFIER), jMenuItemExceptionEditor.getText());
    }

    private void addGlobalEventEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8GlobalEventDefinition.GROUP_IDENTIFIER), jMenuItemGlobalEventEditor.getText());
    }

    private void addFunctionalityAccessPolicyEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8FunctionalityAccessPolicyDefinition.GROUP_IDENTIFIER), jMenuItemFunctionalityAccessPolicyEditor.getText());
    }

    private void addNewFunctionalityMonitor() throws Exception
    {
        T8DeveloperView newEditor;

        newEditor = new T8FunctionalityMonitor(context, developerClient.getViewContainer());
        developerClient.getViewContainer().addView(newEditor);
    }

    private void addDataTypeEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataTypeDefinition.GROUP_IDENTIFIER), jMenuItemDataTypeEditor.getText());
    }

    private void addNewDataEntityEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataEntityDefinition.GROUP_IDENTIFIER), jMenuItemDataEntityEditor.getText());
    }

    private void addNewSystemPropertyEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8SystemPropertyDefinition.GROUP_IDENTIFIER), jMenuItemSystemPropertyEditor.getText());
    }

    private void addNewDatabaseAdaptorEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DatabaseAdaptorDefinition.GROUP_IDENTIFIER), jMenuItemDatabaseAdaptorEditor.getText());
    }

    private void addNewSystemLanguageEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8LanguageDefinition.GROUP_IDENTIFIER), jMenuItemSystemLanguageEditor.getText());
    }

    private void addNewUITranslationEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8UITranslationDefinition.GROUP_IDENTIFIER), jMenuItemUITranslationEditor.getText());
    }

    private void addNewDescriptionFormatEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), "@DG_DESCRIPTION_FORMAT"), jMenuItemDescriptionFormatEditor.getText());
    }

    private void addNewDefinitionTypeEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DefinitionTypeDefinition.GROUP_IDENTIFIER), jMenuItemDefinitionTypeEditor.getText());
    }

    private void addNewWorkFlowSignalEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8FlowSignalDefinition.GROUP_IDENTIFIER), jMenuItemWorkFlowSignalEditor.getText());
    }

    private void addNewDataModelEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataModelDefinition.GROUP_IDENTIFIER), jMenuItemDataModelEditor.getText());
    }

    private void addProjectEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createProjectEditor(developerClient.getViewContainer(), null), jMenuItemProjectEditor.getText());
    }

    private void addProjectManager()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createProjectManager(developerClient.getViewContainer()), jMenuItemProjectManager.getText());
    }

    private void addNewProjectEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ProjectDefinition.GROUP_IDENTIFIER), jMenuItemProjectSetup.getText());
    }

    private void addNewCommunicationServicesEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8CommunicationServiceDefinition.GROUP_IDENTIFIER), jMenuItemCommunicationServices.getText());
    }

    private void addNewCommunicationDetailProviderEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8CommunicationDetailsProviderDefinition.GROUP_IDENTIFIER), jMenuItemCommunicationDetailProvider.getText());
    }

    private void addNewCommunicationMessageTemplateEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8CommunicationMessageDefinition.GROUP_IDENTIFIER), jMenuItemCommunicationMessageTemplate.getText());
    }

    private void addNewCommunicationEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8CommunicationDefinition.GROUP_IDENTIFIER), jMenuItemCommunication.getText());
    }

    private void addNewDataKeyGeneratorEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataKeyGeneratorDefinition.GROUP_IDENTIFIER), dataKeyEditor.getText());
    }

    private void addNewPasswordPolicyEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8PasswordPolicyDefinition.GROUP_IDENTIFIER), jMenuItemPasswordPolicies.getText());
    }

    private void addRemoteServerConnectionEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8ConnectionDefinition.GROUP_IDENTIFIER), jMenuItemRemoteServerConnection.getText());
    }

    private void addDefinitionPatchEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8PatchDefinition.GROUP_IDENTIFIER), jMenuItemDefinitionPatchEditor.getText());
    }

    private void addDefinitionEventHandlerEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DefinitionEventHandlerDefinition.GROUP_IDENTIFIER), jMenuItemDefinitionEventHandlerEditor.getText());
    }

    private void addDataObjectEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataObjectDefinition.GROUP_IDENTIFIER), jMenuItemDataObjectEditor.getText());
    }

    private void addDataSourceCteEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8CTEDefinition.GROUP_IDENTIFIER), jMenuItemCteEditor.getText());
    }

    private void addFileContextBrowser()
    {
        developerClient.getViewContainer().addView(new T8FileContextBrowser(context));
    }

    private void addDataObjectStateGraphEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGraphManager(developerClient.getViewContainer(), T8DataObjectStateGraphDefinition.GROUP_IDENTIFIER), jMenuItemDataObjectStateGraphEditor.getText());
    }

    private void addDataSearchDataSourceEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataSearchComboBoxDataSourceDefinition.GROUP_IDENTIFIER), jMenuItemDataSearchComboDataSourceEditor.getText());
    }

    private void addDataAccessLogicEditor()
    {
        developerClient.getViewContainer().addView(developerClient.getViewContainer().getViewFactory().createDefinitionGroupManager(developerClient.getViewContainer(), T8DataAccessLogicDefinition.GROUP_IDENTIFIER), jMenuItemDataAccessLogicEditor.getText());
    }

    private void addSystemMonitorView()
    {
        developerClient.getViewContainer().addView(new T8SystemMonitor(context, null), jMenuItemSystemMonitor.getText());
    }

    private void validateSystem(ValidationType validationType)
    {
        try
        {
            Map<String, Object> operationParameters;
            Map<String, Object> result;
            String report;

            operationParameters = new HashMap<>();
            operationParameters.put(PARAMETER_VALIDATION_TYPE, validationType);
            result = T8DefaultOperationExecutionDialog.executeOperation(SwingUtilities.getWindowAncestor(this), context, T8AdministrationResource.OPERATION_VALIDATE_SYSTEM, "System Validation", "Validating definition meta data...", operationParameters);
            report = (String)result.get(T8AdministrationResource.PARAMETER_DEFINITION_VALIDATION_REPORT);
            T8InformationDialog.showInformation(this, developerClient.getViewContainer(), report);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while running system validation.", e);
        }
    }

    private void showServerInformation()
    {
        T8ServerRuntimeDataDialog.showServerRuntimeData(context);
    }

    private void restartServer()
    {
        int option;

        option = JOptionPane.showConfirmDialog(this, "Are you sure you want to restart the server?  All currently executing operations/flows will be stoppped.", "Restart Confirmation", JOptionPane.WARNING_MESSAGE);
        if (option == JOptionPane.YES_OPTION)
        {
            try
            {
                terminateHeartBeat();
                T8MainServerClient.restartServer(context);
                System.exit(0);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while restarting server.", e);
                JOptionPane.showMessageDialog(T8DeveloperFrame.this, "A server-side exception prevented successfully completion of the operation.", "Operation Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void sendNotificationMessage(String message)
    {
        try
        {
            T8SessionContext sessionContext;
            String userLabel;
            String userName;
            String userIdentifier;

            // Create the notifier user label.
            sessionContext = context.getSessionContext();
            userName = sessionContext.getUserName();
            userIdentifier = sessionContext.getUserIdentifier();
            if (Strings.trimToNull(userName) != null)
            {
                userLabel = userName + " " + sessionContext.getUserSurname() + " (" + userIdentifier + "): ";
            }
            else
            {
                userLabel = context.getUserId() + ": ";
            }

            // Send the notification.
            developerClient.getNotificationManager().sendNotification(context, T8SystemNotificationDefinition.IDENTIFIER, HashMaps.createTypeSafeMap(new String[]{T8SystemNotificationDefinition.PARAMETER_USER_SUBSET, T8SystemNotificationDefinition.PARAMETER_MESSAGE}, new String[]{T8SystemNotificationDefinition.UserSubset.ACTIVE_SESSIONS.toString(), userLabel + message}));

            // Toast the success.
            Toast.makeText(this, "Notification(s) sent.", Toast.Style.SUCCESS).display();
        }
        catch (Exception ex)
        {
            Toast.makeText(this, "Failed to send notification.", Toast.Style.ERROR).display();
            LOGGER.log("Failed to send communication message", ex);
        }
    }

    private void showOperationMonitor()
    {
        final T8OperationMonitor serverAdminClient;
        JDialog monitorDialog;

        serverAdminClient = new T8OperationMonitor(context);
        monitorDialog = new JDialog(this, false);
        monitorDialog.add(serverAdminClient);
        monitorDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        monitorDialog.setPreferredSize(new Dimension((int)(getWidth()* 0.40), (int)(getHeight()*0.70)));
        monitorDialog.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowOpened(WindowEvent e)
            {
                serverAdminClient.initGenericComponent();
            }

            @Override
            public void windowClosing(WindowEvent e)
            {
                serverAdminClient.destroy();
            }
        });
        monitorDialog.pack();
        monitorDialog.setVisible(true);
    }

    private void openMessenger()
    {
        T8DeveloperMessageDialog dialog;

        dialog = new T8DeveloperMessageDialog(context);
        dialog.show();
    }

    private void logout()
    {
        developerClient.logout();

        while (!T8DeveloperLoginDialog.login(context, rootPane))
        {
            // Do nothing, just show the login screen until the user logs in successfully.
        }

        //Re initialize the developer client
        developerClient.initializeComponent();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelStatusBar = new javax.swing.JPanel();
        jLabelNetworkLatency = new javax.swing.JLabel();
        jLabelNetworkThroughput = new javax.swing.JLabel();
        jLabelServerActivity = new javax.swing.JLabel();
        jLabelServerLoad = new javax.swing.JLabel();
        jLabelMaximumConcurrency = new javax.swing.JLabel();
        jMenuBarMain = new javax.swing.JMenuBar();
        jMenuFlow = new javax.swing.JMenu();
        jMenuItemWorkFlowEditor = new javax.swing.JMenuItem();
        jMenuItemWorkFlowSignalEditor = new javax.swing.JMenuItem();
        jMenuItemWorkFlowProfileEditor = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItemWorkFlowTaskEditor = new javax.swing.JMenuItem();
        jMenuItemTaskEscalationEditor = new javax.swing.JMenuItem();
        jMenuItemFlowTaskListEditor = new javax.swing.JMenuItem();
        jSeparator30 = new javax.swing.JPopupMenu.Separator();
        jMenuItemFlowMonitor = new javax.swing.JMenuItem();
        jMenuUIComponents = new javax.swing.JMenu();
        jMenuItemModuleEditor = new javax.swing.JMenuItem();
        jMenuItemDialogEditor = new javax.swing.JMenuItem();
        jMenuItemUITranslationEditor = new javax.swing.JMenuItem();
        jMenuItemGlobalComponentEditor = new javax.swing.JMenuItem();
        jMenuItemGlobalEventEditor = new javax.swing.JMenuItem();
        jSeparator11 = new javax.swing.JPopupMenu.Separator();
        jMenuItemAngularComponentEditor = new javax.swing.JMenuItem();
        jSeparator24 = new javax.swing.JPopupMenu.Separator();
        jMenuItemDataSearchComboDataSourceEditor = new javax.swing.JMenuItem();
        jMenuData = new javax.swing.JMenu();
        jMenuItemDataConnectionEditor = new javax.swing.JMenuItem();
        jMenuItemDataConnectionPoolEditor = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItemDataTypeEditor = new javax.swing.JMenuItem();
        jMenuItemDataObjectStateGraphEditor = new javax.swing.JMenuItem();
        jMenuItemDataObjectEditor = new javax.swing.JMenuItem();
        jMenuItemDataSourceEditor = new javax.swing.JMenuItem();
        jMenuItemCteEditor = new javax.swing.JMenuItem();
        jMenuItemDataEntityEditor = new javax.swing.JMenuItem();
        jMenuItemDataModelEditor = new javax.swing.JMenuItem();
        jMenuItemDatabaseAdaptorEditor = new javax.swing.JMenuItem();
        jMenuItemIdentifierValueEditor = new javax.swing.JMenuItem();
        dataKeyEditor = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItemDataCacheEditor = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenuItemDataStreamEditor = new javax.swing.JMenuItem();
        jSeparator12 = new javax.swing.JPopupMenu.Separator();
        jMenuItemDataRecordAccessEditor = new javax.swing.JMenuItem();
        jMenuItemDataAccessLogicEditor = new javax.swing.JMenuItem();
        jSeparator29 = new javax.swing.JPopupMenu.Separator();
        jMenuItemDataRecordViewEditor = new javax.swing.JMenuItem();
        jMenuFunctional = new javax.swing.JMenu();
        jMenuItemOrganizationSetupEditor = new javax.swing.JMenuItem();
        jMenuItemOrganizationOperationsSetupEditor = new javax.swing.JMenuItem();
        jMenuItemDescriptionFormatEditor = new javax.swing.JMenuItem();
        jMenuItemAttachmentHandlerEditor = new javax.swing.JMenuItem();
        jMenuItemMergeConfigurationEditor = new javax.swing.JMenuItem();
        jSeparator22 = new javax.swing.JPopupMenu.Separator();
        jMenuItemIntegrationService = new javax.swing.JMenuItem();
        jMenuItemIntegrationResponse = new javax.swing.JMenuItem();
        ReverseIntegrationSevice = new javax.swing.JMenuItem();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        jMenuCommunications = new javax.swing.JMenu();
        jMenuItemCommunicationServices = new javax.swing.JMenuItem();
        jMenuItemCommunicationDetailProvider = new javax.swing.JMenuItem();
        jMenuItemCommunicationMessageTemplate = new javax.swing.JMenuItem();
        jMenuItemCommunication = new javax.swing.JMenuItem();
        jMenuItemNotifications = new javax.swing.JMenuItem();
        jSeparator18 = new javax.swing.JPopupMenu.Separator();
        jMenuItemFileContextEditor = new javax.swing.JMenuItem();
        jMenuItemFileContextBrowser = new javax.swing.JMenuItem();
        jSeparator19 = new javax.swing.JPopupMenu.Separator();
        jMenuItemFunctionalityEditor = new javax.swing.JMenuItem();
        jMenuItemFunctionalityGroupEditor = new javax.swing.JMenuItem();
        jMenuItemFunctionalityAccessPolicyEditor = new javax.swing.JMenuItem();
        jMenuItemFunctionalityMonitor = new javax.swing.JMenuItem();
        jSeparator26 = new javax.swing.JPopupMenu.Separator();
        jMenuItemUserHelp = new javax.swing.JMenuItem();
        jMenuDefinition = new javax.swing.JMenu();
        jMenuItemProjectManager = new javax.swing.JMenuItem();
        jMenuItemProjectEditor = new javax.swing.JMenuItem();
        jMenuItemProjectSetup = new javax.swing.JMenuItem();
        jSeparator31 = new javax.swing.JPopupMenu.Separator();
        jMenuItemSearchDefinitions = new javax.swing.JMenuItem();
        jMenuItemJsonEditor = new javax.swing.JMenuItem();
        jMenuDefinitionValidation = new javax.swing.JMenu();
        jMenuItemDefinitionExistenceQueries = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        jMenuItemDefinitionEditorSetup = new javax.swing.JMenuItem();
        jMenuItemDefinitionTypeEditor = new javax.swing.JMenuItem();
        jSeparator15 = new javax.swing.JPopupMenu.Separator();
        jMenuItemExportDefinitions = new javax.swing.JMenuItem();
        jMenuItemDefinitionPatchEditor = new javax.swing.JMenuItem();
        jSeparator17 = new javax.swing.JPopupMenu.Separator();
        jMenuItemDefinitionEventHandlerEditor = new javax.swing.JMenuItem();
        jMenuReporting = new javax.swing.JMenu();
        jMenuItemReportEditor = new javax.swing.JMenuItem();
        jMenuGraphics = new javax.swing.JMenu();
        jMenuItemImageEditor = new javax.swing.JMenuItem();
        jMenuItemIconEditor = new javax.swing.JMenuItem();
        jMenuItemPainterEditor = new javax.swing.JMenuItem();
        jMenuItemLookAndFeelEditor = new javax.swing.JMenuItem();
        jMenuSystem = new javax.swing.JMenu();
        jMenuItemProcedureEditor = new javax.swing.JMenuItem();
        jMenuItemClientOperationEditor = new javax.swing.JMenuItem();
        jMenuItemServerOperationEditor = new javax.swing.JMenuItem();
        jMenuItemServerProcessEditor = new javax.swing.JMenuItem();
        jMenuItemServiceEditor = new javax.swing.JMenuItem();
        jMenuItemRemoteServerConnection = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        jMenuItemSystemEditor = new javax.swing.JMenuItem();
        jMenuItemSystemPropertyEditor = new javax.swing.JMenuItem();
        jMenuItemExceptionEditor = new javax.swing.JMenuItem();
        jSeparator7 = new javax.swing.JPopupMenu.Separator();
        jMenuItemSystemLanguageEditor = new javax.swing.JMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        jMenuSystemValidation = new javax.swing.JMenu();
        jMenuItemSystemValidation = new javax.swing.JMenuItem();
        jMenuItemSystemValidationUsages = new javax.swing.JMenuItem();
        jSeparator13 = new javax.swing.JPopupMenu.Separator();
        jMenuServerAdmin = new javax.swing.JMenu();
        jMenuItemSessionMonitor = new javax.swing.JMenuItem();
        jMenuItemDataSessionMonitor = new javax.swing.JMenuItem();
        jMenuItemOperationMonitor = new javax.swing.JMenuItem();
        jMenuItemCommunicationMonitor = new javax.swing.JMenuItem();
        jSeparator20 = new javax.swing.JPopupMenu.Separator();
        jMenuItemClearFunctionalityCache = new javax.swing.JMenuItem();
        jMenuItemReloadFunctionalityAccessPolicy = new javax.swing.JMenuItem();
        jSeparator23 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItemSystemMonitor = new javax.swing.JMenuItem();
        jSeparator21 = new javax.swing.JPopupMenu.Separator();
        jMenuItemSendNotification = new javax.swing.JMenuItem();
        jMenuItemRestartServer = new javax.swing.JMenuItem();
        jSeparator25 = new javax.swing.JPopupMenu.Separator();
        jMenuItemDataManagerAdmin = new javax.swing.JMenuItem();
        jMenuUser = new javax.swing.JMenu();
        jMenuItemUserEditor = new javax.swing.JMenuItem();
        jMenuItemUserProfileEditor = new javax.swing.JMenuItem();
        jMenuItemPasswordPolicies = new javax.swing.JMenuItem();
        jMenuItemUserPropertyEditor = new javax.swing.JMenuItem();
        jMenuDeveloper = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator27 = new javax.swing.JPopupMenu.Separator();
        jCheckBoxMenuItemValidateOnSave = new javax.swing.JCheckBoxMenuItem();
        jSeparator28 = new javax.swing.JPopupMenu.Separator();
        jMenuItemLogout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Tech 8 Developer");
        setName("Tech 8 Developer"); // NOI18N
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentShown(java.awt.event.ComponentEvent evt)
            {
                formComponentShown(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        jPanelStatusBar.setLayout(new java.awt.GridBagLayout());

        jLabelNetworkLatency.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabelNetworkLatency.setText("Latency: Pending");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelStatusBar.add(jLabelNetworkLatency, gridBagConstraints);

        jLabelNetworkThroughput.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabelNetworkThroughput.setText("Throughput: Pending");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelStatusBar.add(jLabelNetworkThroughput, gridBagConstraints);

        jLabelServerActivity.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabelServerActivity.setText("Server Activity: Pending");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelStatusBar.add(jLabelServerActivity, gridBagConstraints);

        jLabelServerLoad.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabelServerLoad.setText("Server Load: Pending");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelStatusBar.add(jLabelServerLoad, gridBagConstraints);

        jLabelMaximumConcurrency.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabelMaximumConcurrency.setText("Maximum Concurrency: Pending");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelStatusBar.add(jLabelMaximumConcurrency, gridBagConstraints);

        getContentPane().add(jPanelStatusBar, java.awt.BorderLayout.SOUTH);

        jMenuFlow.setText("Work Flow");

        jMenuItemWorkFlowEditor.setText("Work Flow Editor...");
        jMenuItemWorkFlowEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemWorkFlowEditorActionPerformed(evt);
            }
        });
        jMenuFlow.add(jMenuItemWorkFlowEditor);

        jMenuItemWorkFlowSignalEditor.setText("Signal Editor...");
        jMenuItemWorkFlowSignalEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemWorkFlowSignalEditorActionPerformed(evt);
            }
        });
        jMenuFlow.add(jMenuItemWorkFlowSignalEditor);

        jMenuItemWorkFlowProfileEditor.setText("Work Flow Profile Editor...");
        jMenuItemWorkFlowProfileEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemWorkFlowProfileEditorActionPerformed(evt);
            }
        });
        jMenuFlow.add(jMenuItemWorkFlowProfileEditor);
        jMenuFlow.add(jSeparator2);

        jMenuItemWorkFlowTaskEditor.setText("Task Editor...");
        jMenuItemWorkFlowTaskEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemWorkFlowTaskEditorActionPerformed(evt);
            }
        });
        jMenuFlow.add(jMenuItemWorkFlowTaskEditor);

        jMenuItemTaskEscalationEditor.setText("Task Escalation Editor...");
        jMenuItemTaskEscalationEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemTaskEscalationEditorActionPerformed(evt);
            }
        });
        jMenuFlow.add(jMenuItemTaskEscalationEditor);

        jMenuItemFlowTaskListEditor.setText("Task List Editor...");
        jMenuItemFlowTaskListEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemFlowTaskListEditorActionPerformed(evt);
            }
        });
        jMenuFlow.add(jMenuItemFlowTaskListEditor);
        jMenuFlow.add(jSeparator30);

        jMenuItemFlowMonitor.setText("Flow Monitor...");
        jMenuItemFlowMonitor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemFlowMonitorActionPerformed(evt);
            }
        });
        jMenuFlow.add(jMenuItemFlowMonitor);

        jMenuBarMain.add(jMenuFlow);

        jMenuUIComponents.setText("UI Components");

        jMenuItemModuleEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemModuleEditor.setText("Module Editor...");
        jMenuItemModuleEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemModuleEditorActionPerformed(evt);
            }
        });
        jMenuUIComponents.add(jMenuItemModuleEditor);

        jMenuItemDialogEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemDialogEditor.setText("Dialog Editor...");
        jMenuItemDialogEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDialogEditorActionPerformed(evt);
            }
        });
        jMenuUIComponents.add(jMenuItemDialogEditor);

        jMenuItemUITranslationEditor.setText("UI Translation Editor...");
        jMenuItemUITranslationEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemUITranslationEditorActionPerformed(evt);
            }
        });
        jMenuUIComponents.add(jMenuItemUITranslationEditor);

        jMenuItemGlobalComponentEditor.setText("Global Component Editor...");
        jMenuItemGlobalComponentEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemGlobalComponentEditorActionPerformed(evt);
            }
        });
        jMenuUIComponents.add(jMenuItemGlobalComponentEditor);

        jMenuItemGlobalEventEditor.setText("Global Event Editor...");
        jMenuItemGlobalEventEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemGlobalEventEditorActionPerformed(evt);
            }
        });
        jMenuUIComponents.add(jMenuItemGlobalEventEditor);
        jMenuUIComponents.add(jSeparator11);

        jMenuItemAngularComponentEditor.setText("Angular Component Editor...");
        jMenuItemAngularComponentEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAngularComponentEditorActionPerformed(evt);
            }
        });
        jMenuUIComponents.add(jMenuItemAngularComponentEditor);
        jMenuUIComponents.add(jSeparator24);

        jMenuItemDataSearchComboDataSourceEditor.setText("Data Search Data Source Editor...");
        jMenuItemDataSearchComboDataSourceEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataSearchComboDataSourceEditorActionPerformed(evt);
            }
        });
        jMenuUIComponents.add(jMenuItemDataSearchComboDataSourceEditor);

        jMenuBarMain.add(jMenuUIComponents);

        jMenuData.setText("Data");

        jMenuItemDataConnectionEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemDataConnectionEditor.setText("Data Connection Editor...");
        jMenuItemDataConnectionEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataConnectionEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataConnectionEditor);

        jMenuItemDataConnectionPoolEditor.setText("Data Connection Pool Editor...");
        jMenuItemDataConnectionPoolEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataConnectionPoolEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataConnectionPoolEditor);
        jMenuData.add(jSeparator3);

        jMenuItemDataTypeEditor.setText("Data Type Editor...");
        jMenuItemDataTypeEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataTypeEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataTypeEditor);

        jMenuItemDataObjectStateGraphEditor.setText("Data Object State Graph Editor...");
        jMenuItemDataObjectStateGraphEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataObjectStateGraphEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataObjectStateGraphEditor);

        jMenuItemDataObjectEditor.setText("Data Object Editor...");
        jMenuItemDataObjectEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataObjectEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataObjectEditor);

        jMenuItemDataSourceEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemDataSourceEditor.setText("Data Source Editor...");
        jMenuItemDataSourceEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataSourceEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataSourceEditor);

        jMenuItemCteEditor.setText("Data Source CTE Editor");
        jMenuItemCteEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemCteEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemCteEditor);

        jMenuItemDataEntityEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemDataEntityEditor.setText("Data Entity Editor...");
        jMenuItemDataEntityEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataEntityEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataEntityEditor);

        jMenuItemDataModelEditor.setText("Data Model Editor...");
        jMenuItemDataModelEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataModelEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataModelEditor);

        jMenuItemDatabaseAdaptorEditor.setText("Database Adaptor Editor...");
        jMenuItemDatabaseAdaptorEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDatabaseAdaptorEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDatabaseAdaptorEditor);

        jMenuItemIdentifierValueEditor.setText("Identifier Value Editor...");
        jMenuItemIdentifierValueEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemIdentifierValueEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemIdentifierValueEditor);

        dataKeyEditor.setText("Data Key Editor");
        dataKeyEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                dataKeyEditorActionPerformed(evt);
            }
        });
        jMenuData.add(dataKeyEditor);
        jMenuData.add(jSeparator4);

        jMenuItemDataCacheEditor.setText("Data Cache Editor...");
        jMenuItemDataCacheEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataCacheEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataCacheEditor);
        jMenuData.add(jSeparator5);

        jMenuItemDataStreamEditor.setText("Data Stream Editor...");
        jMenuItemDataStreamEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataStreamEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataStreamEditor);
        jMenuData.add(jSeparator12);

        jMenuItemDataRecordAccessEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemDataRecordAccessEditor.setText("Data Access Layer Editor...");
        jMenuItemDataRecordAccessEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataRecordAccessEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataRecordAccessEditor);

        jMenuItemDataAccessLogicEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemDataAccessLogicEditor.setText("Data Access Logic Editor...");
        jMenuItemDataAccessLogicEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataAccessLogicEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataAccessLogicEditor);
        jMenuData.add(jSeparator29);

        jMenuItemDataRecordViewEditor.setText("Data Record View Editor...");
        jMenuItemDataRecordViewEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataRecordViewEditorActionPerformed(evt);
            }
        });
        jMenuData.add(jMenuItemDataRecordViewEditor);

        jMenuBarMain.add(jMenuData);

        jMenuFunctional.setText("Functional");

        jMenuItemOrganizationSetupEditor.setText("Organization Setup Editor...");
        jMenuItemOrganizationSetupEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemOrganizationSetupEditorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemOrganizationSetupEditor);

        jMenuItemOrganizationOperationsSetupEditor.setText("Organization Operational Setup Editor...");
        jMenuItemOrganizationOperationsSetupEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemOrganizationOperationsSetupEditorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemOrganizationOperationsSetupEditor);

        jMenuItemDescriptionFormatEditor.setText("Description Format Editor...");
        jMenuItemDescriptionFormatEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDescriptionFormatEditorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemDescriptionFormatEditor);

        jMenuItemAttachmentHandlerEditor.setText("Attachment Handler Editor...");
        jMenuItemAttachmentHandlerEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAttachmentHandlerEditorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemAttachmentHandlerEditor);

        jMenuItemMergeConfigurationEditor.setText("Merge Configuration Editor...");
        jMenuItemMergeConfigurationEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemMergeConfigurationEditorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemMergeConfigurationEditor);
        jMenuFunctional.add(jSeparator22);

        jMenuItemIntegrationService.setText("Integration Service Editor...");
        jMenuItemIntegrationService.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemIntegrationServiceActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemIntegrationService);

        jMenuItemIntegrationResponse.setText("Integration Response Editor...");
        jMenuItemIntegrationResponse.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemIntegrationResponseActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemIntegrationResponse);

        ReverseIntegrationSevice.setText("Reverse Integration Service Editor...");
        ReverseIntegrationSevice.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                ReverseIntegrationSeviceActionPerformed(evt);
            }
        });
        jMenuFunctional.add(ReverseIntegrationSevice);
        jMenuFunctional.add(jSeparator10);

        jMenuCommunications.setText("Communications");

        jMenuItemCommunicationServices.setText("Service Editor");
        jMenuItemCommunicationServices.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemCommunicationServicesActionPerformed(evt);
            }
        });
        jMenuCommunications.add(jMenuItemCommunicationServices);

        jMenuItemCommunicationDetailProvider.setText("Detail Provider Editor");
        jMenuItemCommunicationDetailProvider.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemCommunicationDetailProviderActionPerformed(evt);
            }
        });
        jMenuCommunications.add(jMenuItemCommunicationDetailProvider);

        jMenuItemCommunicationMessageTemplate.setText("Message Template Editor");
        jMenuItemCommunicationMessageTemplate.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemCommunicationMessageTemplateActionPerformed(evt);
            }
        });
        jMenuCommunications.add(jMenuItemCommunicationMessageTemplate);

        jMenuItemCommunication.setText("Communication Editor");
        jMenuItemCommunication.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemCommunicationActionPerformed(evt);
            }
        });
        jMenuCommunications.add(jMenuItemCommunication);

        jMenuFunctional.add(jMenuCommunications);

        jMenuItemNotifications.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemNotifications.setText("Notifications");
        jMenuItemNotifications.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemNotificationsActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemNotifications);
        jMenuFunctional.add(jSeparator18);

        jMenuItemFileContextEditor.setText("File Context Editor...");
        jMenuItemFileContextEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemFileContextEditorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemFileContextEditor);

        jMenuItemFileContextBrowser.setText("File Context Browser...");
        jMenuItemFileContextBrowser.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemFileContextBrowserActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemFileContextBrowser);
        jMenuFunctional.add(jSeparator19);

        jMenuItemFunctionalityEditor.setText("Functionality Editor...");
        jMenuItemFunctionalityEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemFunctionalityEditorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemFunctionalityEditor);

        jMenuItemFunctionalityGroupEditor.setText("Functionality Group Editor...");
        jMenuItemFunctionalityGroupEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemFunctionalityGroupEditorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemFunctionalityGroupEditor);

        jMenuItemFunctionalityAccessPolicyEditor.setText("Functionality Access Policy Editor...");
        jMenuItemFunctionalityAccessPolicyEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemFunctionalityAccessPolicyEditorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemFunctionalityAccessPolicyEditor);

        jMenuItemFunctionalityMonitor.setText("Functionality Monitor");
        jMenuItemFunctionalityMonitor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemFunctionalityMonitorActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemFunctionalityMonitor);
        jMenuFunctional.add(jSeparator26);

        jMenuItemUserHelp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemUserHelp.setLabel("User Help");
        jMenuItemUserHelp.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemUserHelpActionPerformed(evt);
            }
        });
        jMenuFunctional.add(jMenuItemUserHelp);

        jMenuBarMain.add(jMenuFunctional);

        jMenuDefinition.setText("Definition");

        jMenuItemProjectManager.setText("Project Manager...");
        jMenuItemProjectManager.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemProjectManagerActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemProjectManager);

        jMenuItemProjectEditor.setText("Project Editor...");
        jMenuItemProjectEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemProjectEditorActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemProjectEditor);

        jMenuItemProjectSetup.setText("Project Setup...");
        jMenuItemProjectSetup.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemProjectSetupActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemProjectSetup);
        jMenuDefinition.add(jSeparator31);

        jMenuItemSearchDefinitions.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemSearchDefinitions.setText("Search Definitions...");
        jMenuItemSearchDefinitions.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSearchDefinitionsActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemSearchDefinitions);

        jMenuItemJsonEditor.setText("JSON Editor...");
        jMenuItemJsonEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemJsonEditorActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemJsonEditor);

        jMenuDefinitionValidation.setText("Validation...");

        jMenuItemDefinitionExistenceQueries.setText("Definition Queries...");
        jMenuItemDefinitionExistenceQueries.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDefinitionExistenceQueriesActionPerformed(evt);
            }
        });
        jMenuDefinitionValidation.add(jMenuItemDefinitionExistenceQueries);

        jMenuDefinition.add(jMenuDefinitionValidation);
        jMenuDefinition.add(jSeparator9);

        jMenuItemDefinitionEditorSetup.setText("Definition Editor Setup...");
        jMenuItemDefinitionEditorSetup.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDefinitionEditorSetupActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemDefinitionEditorSetup);

        jMenuItemDefinitionTypeEditor.setText("Definition Type Editor...");
        jMenuItemDefinitionTypeEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDefinitionTypeEditorActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemDefinitionTypeEditor);
        jMenuDefinition.add(jSeparator15);

        jMenuItemExportDefinitions.setText("Meta Package Manager...");
        jMenuItemExportDefinitions.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemExportDefinitionsActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemExportDefinitions);

        jMenuItemDefinitionPatchEditor.setText("Definition Patch Editor...");
        jMenuItemDefinitionPatchEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDefinitionPatchEditorActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemDefinitionPatchEditor);
        jMenuDefinition.add(jSeparator17);

        jMenuItemDefinitionEventHandlerEditor.setText("Event Handler Editor...");
        jMenuItemDefinitionEventHandlerEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDefinitionEventHandlerEditorActionPerformed(evt);
            }
        });
        jMenuDefinition.add(jMenuItemDefinitionEventHandlerEditor);

        jMenuBarMain.add(jMenuDefinition);

        jMenuReporting.setText("Reports");

        jMenuItemReportEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemReportEditor.setText("Report Editor...");
        jMenuItemReportEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemReportEditorActionPerformed(evt);
            }
        });
        jMenuReporting.add(jMenuItemReportEditor);

        jMenuBarMain.add(jMenuReporting);

        jMenuGraphics.setText("Graphics");

        jMenuItemImageEditor.setText("Image Editor...");
        jMenuItemImageEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemImageEditorActionPerformed(evt);
            }
        });
        jMenuGraphics.add(jMenuItemImageEditor);

        jMenuItemIconEditor.setText("Icon Editor...");
        jMenuItemIconEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemIconEditorActionPerformed(evt);
            }
        });
        jMenuGraphics.add(jMenuItemIconEditor);

        jMenuItemPainterEditor.setText("Painter Editor...");
        jMenuItemPainterEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemPainterEditorActionPerformed(evt);
            }
        });
        jMenuGraphics.add(jMenuItemPainterEditor);

        jMenuItemLookAndFeelEditor.setText("Look and Feel Editor...");
        jMenuItemLookAndFeelEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemLookAndFeelEditorActionPerformed(evt);
            }
        });
        jMenuGraphics.add(jMenuItemLookAndFeelEditor);

        jMenuBarMain.add(jMenuGraphics);

        jMenuSystem.setText("System");

        jMenuItemProcedureEditor.setText("Procedure Editor...");
        jMenuItemProcedureEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemProcedureEditorActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemProcedureEditor);

        jMenuItemClientOperationEditor.setText("Client Operation Editor...");
        jMenuItemClientOperationEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemClientOperationEditorActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemClientOperationEditor);

        jMenuItemServerOperationEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemServerOperationEditor.setText("Server Operation Editor...");
        jMenuItemServerOperationEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemServerOperationEditorActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemServerOperationEditor);

        jMenuItemServerProcessEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemServerProcessEditor.setText("Server Process Editor...");
        jMenuItemServerProcessEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemServerProcessEditorActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemServerProcessEditor);

        jMenuItemServiceEditor.setText("System Service Editor...");
        jMenuItemServiceEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemServiceEditorActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemServiceEditor);

        jMenuItemRemoteServerConnection.setText("Connection Editor...");
        jMenuItemRemoteServerConnection.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemRemoteServerConnectionActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemRemoteServerConnection);
        jMenuSystem.add(jSeparator6);

        jMenuItemSystemEditor.setText("System Configuration Editor...");
        jMenuItemSystemEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSystemEditorActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemSystemEditor);

        jMenuItemSystemPropertyEditor.setText("System Property Editor...");
        jMenuItemSystemPropertyEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSystemPropertyEditorActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemSystemPropertyEditor);

        jMenuItemExceptionEditor.setText("System Exception Editor...");
        jMenuItemExceptionEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemExceptionEditorActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemExceptionEditor);
        jMenuSystem.add(jSeparator7);

        jMenuItemSystemLanguageEditor.setText("System Language Editor...");
        jMenuItemSystemLanguageEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSystemLanguageEditorActionPerformed(evt);
            }
        });
        jMenuSystem.add(jMenuItemSystemLanguageEditor);
        jMenuSystem.add(jSeparator8);

        jMenuSystemValidation.setText("System Validation");
        jMenuSystemValidation.setToolTipText("");

        jMenuItemSystemValidation.setText("Run System Definition Validation");
        jMenuItemSystemValidation.setToolTipText("");
        jMenuItemSystemValidation.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSystemValidationActionPerformed(evt);
            }
        });
        jMenuSystemValidation.add(jMenuItemSystemValidation);

        jMenuItemSystemValidationUsages.setText("Run System Usages Validation");
        jMenuItemSystemValidationUsages.setToolTipText("");
        jMenuItemSystemValidationUsages.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSystemValidationUsagesActionPerformed(evt);
            }
        });
        jMenuSystemValidation.add(jMenuItemSystemValidationUsages);

        jMenuSystem.add(jMenuSystemValidation);
        jMenuSystem.add(jSeparator13);

        jMenuServerAdmin.setText("Server Administration");
        jMenuServerAdmin.setToolTipText("");

        jMenuItemSessionMonitor.setText("Session Monitor...");
        jMenuItemSessionMonitor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSessionMonitorActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemSessionMonitor);

        jMenuItemDataSessionMonitor.setText("Data Session Monitor...");
        jMenuItemDataSessionMonitor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataSessionMonitorActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemDataSessionMonitor);

        jMenuItemOperationMonitor.setText("Operation Monitor");
        jMenuItemOperationMonitor.setToolTipText("");
        jMenuItemOperationMonitor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemOperationMonitorActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemOperationMonitor);

        jMenuItemCommunicationMonitor.setText("Communication Monitor");
        jMenuItemCommunicationMonitor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemCommunicationMonitorActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemCommunicationMonitor);
        jMenuServerAdmin.add(jSeparator20);

        jMenuItemClearFunctionalityCache.setText("Clear Functionality Cache");
        jMenuItemClearFunctionalityCache.setToolTipText("");
        jMenuItemClearFunctionalityCache.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemClearFunctionalityCacheActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemClearFunctionalityCache);

        jMenuItemReloadFunctionalityAccessPolicy.setText("Reload Functionality Access Policy");
        jMenuItemReloadFunctionalityAccessPolicy.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemReloadFunctionalityAccessPolicyActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemReloadFunctionalityAccessPolicy);
        jMenuServerAdmin.add(jSeparator23);

        jMenuItem1.setText("Server Information...");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItem1);

        jMenuItemSystemMonitor.setText("System Monitor...");
        jMenuItemSystemMonitor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSystemMonitorActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemSystemMonitor);
        jMenuServerAdmin.add(jSeparator21);

        jMenuItemSendNotification.setText("Send Notification");
        jMenuItemSendNotification.setToolTipText("");
        jMenuItemSendNotification.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSendNotificationActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemSendNotification);

        jMenuItemRestartServer.setText("Restart Server...");
        jMenuItemRestartServer.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemRestartServerActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemRestartServer);
        jMenuServerAdmin.add(jSeparator25);

        jMenuItemDataManagerAdmin.setText("Data Manager...");
        jMenuItemDataManagerAdmin.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemDataManagerAdminActionPerformed(evt);
            }
        });
        jMenuServerAdmin.add(jMenuItemDataManagerAdmin);

        jMenuSystem.add(jMenuServerAdmin);

        jMenuBarMain.add(jMenuSystem);

        jMenuUser.setText("User");

        jMenuItemUserEditor.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemUserEditor.setText("User Editor...");
        jMenuItemUserEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemUserEditorActionPerformed(evt);
            }
        });
        jMenuUser.add(jMenuItemUserEditor);

        jMenuItemUserProfileEditor.setText("User Profile Editor...");
        jMenuItemUserProfileEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemUserProfileEditorActionPerformed(evt);
            }
        });
        jMenuUser.add(jMenuItemUserProfileEditor);

        jMenuItemPasswordPolicies.setText("Password Policy Editor");
        jMenuItemPasswordPolicies.setToolTipText("");
        jMenuItemPasswordPolicies.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemPasswordPoliciesActionPerformed(evt);
            }
        });
        jMenuUser.add(jMenuItemPasswordPolicies);

        jMenuItemUserPropertyEditor.setText("User Property Editor...");
        jMenuItemUserPropertyEditor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemUserPropertyEditorActionPerformed(evt);
            }
        });
        jMenuUser.add(jMenuItemUserPropertyEditor);

        jMenuBarMain.add(jMenuUser);

        jMenuDeveloper.setText("Developer");
        jMenuDeveloper.setToolTipText("");
        jMenuDeveloper.setAutoscrolls(true);

        jMenuItem2.setText("Messenger...");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenuDeveloper.add(jMenuItem2);
        jMenuDeveloper.add(jSeparator27);

        jCheckBoxMenuItemValidateOnSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jCheckBoxMenuItemValidateOnSave.setText("Validate On Save");
        jCheckBoxMenuItemValidateOnSave.setName(""); // NOI18N
        jCheckBoxMenuItemValidateOnSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jCheckBoxMenuItemValidateOnSaveActionPerformed(evt);
            }
        });
        jMenuDeveloper.add(jCheckBoxMenuItemValidateOnSave);
        jMenuDeveloper.add(jSeparator28);

        jMenuItemLogout.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemLogout.setText("Logout");
        jMenuItemLogout.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemLogoutActionPerformed(evt);
            }
        });
        jMenuDeveloper.add(jMenuItemLogout);

        jMenuBarMain.add(jMenuDeveloper);

        setJMenuBar(jMenuBarMain);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemDataSourceEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataSourceEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataSourceEditorActionPerformed
        addDefinitionGroupManager(com.pilog.t8.definition.data.source.T8DataSourceDefinition.GROUP_IDENTIFIER, jMenuItemDataSourceEditor.getText());
    }//GEN-LAST:event_jMenuItemDataSourceEditorActionPerformed

    private void jMenuItemDataConnectionEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataConnectionEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataConnectionEditorActionPerformed
        addDefinitionGroupManager(com.pilog.t8.definition.data.connection.T8DataConnectionDefinition.GROUP_IDENTIFIER, jMenuItemDataConnectionEditor.getText());
    }//GEN-LAST:event_jMenuItemDataConnectionEditorActionPerformed

    private void jMenuItemUserEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemUserEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemUserEditorActionPerformed
        addDefinitionGroupManager(com.pilog.t8.definition.user.T8UserDefinition.GROUP_IDENTIFIER, jMenuItemUserEditor.getText());
    }//GEN-LAST:event_jMenuItemUserEditorActionPerformed

    private void jMenuItemFlowMonitorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemFlowMonitorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemFlowMonitorActionPerformed
        try
        {
            addNewFlowMonitor();
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to add the flow monitor view.", ex);
        }
    }//GEN-LAST:event_jMenuItemFlowMonitorActionPerformed

    private void jMenuItemWorkFlowTaskEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemWorkFlowTaskEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemWorkFlowTaskEditorActionPerformed
        addDefinitionGroupManager(com.pilog.t8.definition.flow.T8WorkFlowTaskDefinition.GROUP_IDENTIFIER, jMenuItemWorkFlowTaskEditor.getText());
    }//GEN-LAST:event_jMenuItemWorkFlowTaskEditorActionPerformed

    private void jMenuItemImageEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemImageEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemImageEditorActionPerformed
        addDefinitionGroupManager(com.pilog.t8.definition.gfx.image.T8ImageDefinition.GROUP_IDENTIFIER, jMenuItemImageEditor.getText());
    }//GEN-LAST:event_jMenuItemImageEditorActionPerformed

    private void jMenuItemServerOperationEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemServerOperationEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemServerOperationEditorActionPerformed
        addDefinitionGroupManager(com.pilog.t8.definition.operation.T8ServerOperationDefinition.GROUP_IDENTIFIER, jMenuItemServerOperationEditor.getText());
    }//GEN-LAST:event_jMenuItemServerOperationEditorActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        shutdown();
    }//GEN-LAST:event_formWindowClosing

    private void jMenuItemSearchDefinitionsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSearchDefinitionsActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSearchDefinitionsActionPerformed
        addNewDefinitionSearchEditor();
    }//GEN-LAST:event_jMenuItemSearchDefinitionsActionPerformed

    private void jMenuItemDataEntityEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataEntityEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataEntityEditorActionPerformed
        addNewDataEntityEditor();
    }//GEN-LAST:event_jMenuItemDataEntityEditorActionPerformed

    private void jMenuItemSystemPropertyEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSystemPropertyEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSystemPropertyEditorActionPerformed
        addNewSystemPropertyEditor();
    }//GEN-LAST:event_jMenuItemSystemPropertyEditorActionPerformed

    private void jMenuItemDatabaseAdaptorEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDatabaseAdaptorEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDatabaseAdaptorEditorActionPerformed
        addNewDatabaseAdaptorEditor();
    }//GEN-LAST:event_jMenuItemDatabaseAdaptorEditorActionPerformed

    private void jMenuItemClientOperationEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemClientOperationEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemClientOperationEditorActionPerformed
        addNewClientOperationEditor();
    }//GEN-LAST:event_jMenuItemClientOperationEditorActionPerformed

    private void jMenuItemSystemLanguageEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSystemLanguageEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSystemLanguageEditorActionPerformed
        addNewSystemLanguageEditor();
    }//GEN-LAST:event_jMenuItemSystemLanguageEditorActionPerformed

    private void jMenuItemUITranslationEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemUITranslationEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemUITranslationEditorActionPerformed
        addNewUITranslationEditor();
    }//GEN-LAST:event_jMenuItemUITranslationEditorActionPerformed

    private void jMenuItemSystemValidationActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSystemValidationActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSystemValidationActionPerformed
        validateSystem(ValidationType.DEFINITION_VALIDATION);
    }//GEN-LAST:event_jMenuItemSystemValidationActionPerformed

    private void jMenuItemDescriptionFormatEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDescriptionFormatEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDescriptionFormatEditorActionPerformed
        addNewDescriptionFormatEditor();
    }//GEN-LAST:event_jMenuItemDescriptionFormatEditorActionPerformed

    private void jMenuItemDefinitionTypeEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDefinitionTypeEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDefinitionTypeEditorActionPerformed
        addNewDefinitionTypeEditor();
    }//GEN-LAST:event_jMenuItemDefinitionTypeEditorActionPerformed

    private void jMenuItemSessionMonitorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSessionMonitorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSessionMonitorActionPerformed
        addNewSessionMonitor();
    }//GEN-LAST:event_jMenuItemSessionMonitorActionPerformed

    private void jMenuItemWorkFlowSignalEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemWorkFlowSignalEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemWorkFlowSignalEditorActionPerformed
        addNewWorkFlowSignalEditor();
    }//GEN-LAST:event_jMenuItemWorkFlowSignalEditorActionPerformed

    private void jMenuItemDataModelEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataModelEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataModelEditorActionPerformed
        addNewDataModelEditor();
    }//GEN-LAST:event_jMenuItemDataModelEditorActionPerformed

    private void jMenuItemDataSessionMonitorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataSessionMonitorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataSessionMonitorActionPerformed
        addNewDataSessionMonitor();
    }//GEN-LAST:event_jMenuItemDataSessionMonitorActionPerformed

    private void jMenuItemIconEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemIconEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemIconEditorActionPerformed
        addNewIconEditor();
    }//GEN-LAST:event_jMenuItemIconEditorActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItem1ActionPerformed
    {//GEN-HEADEREND:event_jMenuItem1ActionPerformed
        showServerInformation();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItemServiceEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemServiceEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemServiceEditorActionPerformed
        addNewServiceEditor();
    }//GEN-LAST:event_jMenuItemServiceEditorActionPerformed

    private void jMenuItemRestartServerActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemRestartServerActionPerformed
    {//GEN-HEADEREND:event_jMenuItemRestartServerActionPerformed
        restartServer();
    }//GEN-LAST:event_jMenuItemRestartServerActionPerformed

    private void jMenuItemExportDefinitionsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemExportDefinitionsActionPerformed
    {//GEN-HEADEREND:event_jMenuItemExportDefinitionsActionPerformed
        addNewMetaPackageManager();
    }//GEN-LAST:event_jMenuItemExportDefinitionsActionPerformed

    private void jMenuItemModuleEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemModuleEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemModuleEditorActionPerformed
        addNewModuleEditor();
    }//GEN-LAST:event_jMenuItemModuleEditorActionPerformed

    private void jMenuItemProjectSetupActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemProjectSetupActionPerformed
    {//GEN-HEADEREND:event_jMenuItemProjectSetupActionPerformed
        addNewProjectEditor();
    }//GEN-LAST:event_jMenuItemProjectSetupActionPerformed

    private void jMenuItemDataRecordAccessEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataRecordAccessEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataRecordAccessEditorActionPerformed
        addNewDataRecordAccessEditor();
    }//GEN-LAST:event_jMenuItemDataRecordAccessEditorActionPerformed

    private void jMenuItemServerProcessEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemServerProcessEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemServerProcessEditorActionPerformed
        addNewServerProcessEditor();
    }//GEN-LAST:event_jMenuItemServerProcessEditorActionPerformed

    private void jMenuItemReportEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemReportEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemReportEditorActionPerformed
        addDefinitionGroupManager(com.pilog.t8.definition.report.T8ReportDefinition.GROUP_IDENTIFIER, jMenuItemReportEditor.getText());
    }//GEN-LAST:event_jMenuItemReportEditorActionPerformed

    private void jMenuItemSystemEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSystemEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSystemEditorActionPerformed
        addNewSystemEditor();
    }//GEN-LAST:event_jMenuItemSystemEditorActionPerformed

    private void jMenuItemPainterEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemPainterEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemPainterEditorActionPerformed
        addNewPainterEditor();
    }//GEN-LAST:event_jMenuItemPainterEditorActionPerformed

    private void jMenuItemUserProfileEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemUserProfileEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemUserProfileEditorActionPerformed
        addNewUserProfileEditor();
    }//GEN-LAST:event_jMenuItemUserProfileEditorActionPerformed

    private void jMenuItemWorkFlowEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemWorkFlowEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemWorkFlowEditorActionPerformed
        addNewWorkFlowEditor();
    }//GEN-LAST:event_jMenuItemWorkFlowEditorActionPerformed

    private void jMenuItemDataStreamEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataStreamEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataStreamEditorActionPerformed
        addNewDataStreamEditor();
    }//GEN-LAST:event_jMenuItemDataStreamEditorActionPerformed

    private void jMenuItemDefinitionEditorSetupActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDefinitionEditorSetupActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDefinitionEditorSetupActionPerformed
        addNewDefinitionEditorSetupEditor();
    }//GEN-LAST:event_jMenuItemDefinitionEditorSetupActionPerformed

    private void jMenuItemWorkFlowProfileEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemWorkFlowProfileEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemWorkFlowProfileEditorActionPerformed
        addNewWorkFlowProfileEditor();
    }//GEN-LAST:event_jMenuItemWorkFlowProfileEditorActionPerformed

    private void jMenuItemDataConnectionPoolEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataConnectionPoolEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataConnectionPoolEditorActionPerformed
        addNewDataConnectionPoolEditor();
    }//GEN-LAST:event_jMenuItemDataConnectionPoolEditorActionPerformed

    private void jMenuItemDataCacheEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataCacheEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataCacheEditorActionPerformed
        addNewDataCacheEditor();
    }//GEN-LAST:event_jMenuItemDataCacheEditorActionPerformed

    private void jMenuItemCommunicationServicesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemCommunicationServicesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemCommunicationServicesActionPerformed
        addNewCommunicationServicesEditor();
    }//GEN-LAST:event_jMenuItemCommunicationServicesActionPerformed

    private void jMenuItemCommunicationDetailProviderActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemCommunicationDetailProviderActionPerformed
    {//GEN-HEADEREND:event_jMenuItemCommunicationDetailProviderActionPerformed
        addNewCommunicationDetailProviderEditor();
    }//GEN-LAST:event_jMenuItemCommunicationDetailProviderActionPerformed

    private void jMenuItemCommunicationMessageTemplateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemCommunicationMessageTemplateActionPerformed
    {//GEN-HEADEREND:event_jMenuItemCommunicationMessageTemplateActionPerformed
        addNewCommunicationMessageTemplateEditor();
    }//GEN-LAST:event_jMenuItemCommunicationMessageTemplateActionPerformed

    private void jMenuItemCommunicationActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemCommunicationActionPerformed
    {//GEN-HEADEREND:event_jMenuItemCommunicationActionPerformed
        addNewCommunicationEditor();
    }//GEN-LAST:event_jMenuItemCommunicationActionPerformed

    private void jMenuItemOrganizationSetupEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemOrganizationSetupEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemOrganizationSetupEditorActionPerformed
        addNewOrganizationSetupEditor();
    }//GEN-LAST:event_jMenuItemOrganizationSetupEditorActionPerformed

    private void jMenuItemFileContextEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemFileContextEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemFileContextEditorActionPerformed
        addNewFileContextEditor();
    }//GEN-LAST:event_jMenuItemFileContextEditorActionPerformed

    private void jMenuItemFunctionalityEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemFunctionalityEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemFunctionalityEditorActionPerformed
        addNewFunctionalityEditor();
    }//GEN-LAST:event_jMenuItemFunctionalityEditorActionPerformed

    private void jMenuItemFunctionalityGroupEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemFunctionalityGroupEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemFunctionalityGroupEditorActionPerformed
        addNewFunctionalityGroupEditor();
    }//GEN-LAST:event_jMenuItemFunctionalityGroupEditorActionPerformed

    private void dataKeyEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_dataKeyEditorActionPerformed
    {//GEN-HEADEREND:event_dataKeyEditorActionPerformed
        addNewDataKeyGeneratorEditor();
    }//GEN-LAST:event_dataKeyEditorActionPerformed

    private void jMenuItemOperationMonitorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemOperationMonitorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemOperationMonitorActionPerformed
        showOperationMonitor();
    }//GEN-LAST:event_jMenuItemOperationMonitorActionPerformed

    private void jMenuItemCommunicationMonitorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemCommunicationMonitorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemCommunicationMonitorActionPerformed
        addNewCommunicationMonitor();
    }//GEN-LAST:event_jMenuItemCommunicationMonitorActionPerformed

    private void jMenuItemGlobalComponentEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemGlobalComponentEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemGlobalComponentEditorActionPerformed
        addNewGlobalComponentEditor();
    }//GEN-LAST:event_jMenuItemGlobalComponentEditorActionPerformed

    private void jMenuItemAttachmentHandlerEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAttachmentHandlerEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAttachmentHandlerEditorActionPerformed
        addNewAttachmentHandlerEditor();
    }//GEN-LAST:event_jMenuItemAttachmentHandlerEditorActionPerformed

    private void jMenuItemPasswordPoliciesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemPasswordPoliciesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemPasswordPoliciesActionPerformed
        addNewPasswordPolicyEditor();
    }//GEN-LAST:event_jMenuItemPasswordPoliciesActionPerformed

    private void jMenuItemSendNotificationActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSendNotificationActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSendNotificationActionPerformed
        String message;

        message = JOptionPane.showInputDialog("Notification Message");
        if (Strings.trimToNull(message) != null)
        {
            sendNotificationMessage(message);
        }
        else
        {
            Toast.makeText(this, "Notification(s) has been cancelled.\n Due to either the notification being cancelled or the message was empty.", Toast.Style.ERROR).display();
        }
    }//GEN-LAST:event_jMenuItemSendNotificationActionPerformed

    private void jMenuItemRemoteServerConnectionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemRemoteServerConnectionActionPerformed
    {//GEN-HEADEREND:event_jMenuItemRemoteServerConnectionActionPerformed
        addRemoteServerConnectionEditor();
    }//GEN-LAST:event_jMenuItemRemoteServerConnectionActionPerformed

    private void jMenuItemDefinitionPatchEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDefinitionPatchEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDefinitionPatchEditorActionPerformed
        addDefinitionPatchEditor();
    }//GEN-LAST:event_jMenuItemDefinitionPatchEditorActionPerformed

    private void jMenuItemDefinitionEventHandlerEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDefinitionEventHandlerEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDefinitionEventHandlerEditorActionPerformed
        addDefinitionEventHandlerEditor();
    }//GEN-LAST:event_jMenuItemDefinitionEventHandlerEditorActionPerformed

    private void jMenuItemCteEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemCteEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemCteEditorActionPerformed
        addDataSourceCteEditor();
    }//GEN-LAST:event_jMenuItemCteEditorActionPerformed

    private void jMenuItemLogoutActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemLogoutActionPerformed
    {//GEN-HEADEREND:event_jMenuItemLogoutActionPerformed
        logout();
    }//GEN-LAST:event_jMenuItemLogoutActionPerformed

    private void jMenuItemDataObjectEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataObjectEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataObjectEditorActionPerformed
        addDataObjectEditor();
    }//GEN-LAST:event_jMenuItemDataObjectEditorActionPerformed

    private void jMenuItemFileContextBrowserActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemFileContextBrowserActionPerformed
    {//GEN-HEADEREND:event_jMenuItemFileContextBrowserActionPerformed
        addFileContextBrowser();
    }//GEN-LAST:event_jMenuItemFileContextBrowserActionPerformed

    private void jMenuItemDataObjectStateGraphEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataObjectStateGraphEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataObjectStateGraphEditorActionPerformed
        addDataObjectStateGraphEditor();
    }//GEN-LAST:event_jMenuItemDataObjectStateGraphEditorActionPerformed

    private void jMenuItemIdentifierValueEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemIdentifierValueEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemIdentifierValueEditorActionPerformed
        addIdentifierValueEditor();
    }//GEN-LAST:event_jMenuItemIdentifierValueEditorActionPerformed

    private void jMenuItemLookAndFeelEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemLookAndFeelEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemLookAndFeelEditorActionPerformed
        addLookAndFeelEditor();
    }//GEN-LAST:event_jMenuItemLookAndFeelEditorActionPerformed

    private void jMenuItemMergeConfigurationEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemMergeConfigurationEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemMergeConfigurationEditorActionPerformed
        addMergeConfigurationEditor();
    }//GEN-LAST:event_jMenuItemMergeConfigurationEditorActionPerformed

    private void jMenuItemSystemValidationUsagesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSystemValidationUsagesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSystemValidationUsagesActionPerformed
        validateSystem(ValidationType.UN_USED_DEFINITIONS);
    }//GEN-LAST:event_jMenuItemSystemValidationUsagesActionPerformed

    private void jMenuItemDataTypeEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataTypeEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataTypeEditorActionPerformed
        addDataTypeEditor();
    }//GEN-LAST:event_jMenuItemDataTypeEditorActionPerformed

    private void jMenuItemIntegrationServiceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemIntegrationServiceActionPerformed
    {//GEN-HEADEREND:event_jMenuItemIntegrationServiceActionPerformed
        addIntegrationServiceEditor();
    }//GEN-LAST:event_jMenuItemIntegrationServiceActionPerformed

    private void jMenuItemClearFunctionalityCacheActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemClearFunctionalityCacheActionPerformed
    {//GEN-HEADEREND:event_jMenuItemClearFunctionalityCacheActionPerformed
        context.getClientContext().getFunctionalityManager().clearCache(context);
    }//GEN-LAST:event_jMenuItemClearFunctionalityCacheActionPerformed

    private void jMenuItemExceptionEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemExceptionEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemExceptionEditorActionPerformed
        addExceptionEditor();
    }//GEN-LAST:event_jMenuItemExceptionEditorActionPerformed

    private void jMenuItemFunctionalityAccessPolicyEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemFunctionalityAccessPolicyEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemFunctionalityAccessPolicyEditorActionPerformed
        addFunctionalityAccessPolicyEditor();
    }//GEN-LAST:event_jMenuItemFunctionalityAccessPolicyEditorActionPerformed

    private void jMenuItemReloadFunctionalityAccessPolicyActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemReloadFunctionalityAccessPolicyActionPerformed
    {//GEN-HEADEREND:event_jMenuItemReloadFunctionalityAccessPolicyActionPerformed
        context.getClientContext().getFunctionalityManager().reloadAccessPolicy(context);
    }//GEN-LAST:event_jMenuItemReloadFunctionalityAccessPolicyActionPerformed

    private void jMenuItemGlobalEventEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemGlobalEventEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemGlobalEventEditorActionPerformed
        addGlobalEventEditor();
    }//GEN-LAST:event_jMenuItemGlobalEventEditorActionPerformed

    private void jMenuItemDataSearchComboDataSourceEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataSearchComboDataSourceEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataSearchComboDataSourceEditorActionPerformed
        addDataSearchDataSourceEditor();
    }//GEN-LAST:event_jMenuItemDataSearchComboDataSourceEditorActionPerformed

    private void jMenuItemFunctionalityMonitorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemFunctionalityMonitorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemFunctionalityMonitorActionPerformed
        try
        {
            addNewFunctionalityMonitor();
        } catch (Exception ex)
        {
            Logger.getLogger(T8DeveloperFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemFunctionalityMonitorActionPerformed

    private void jMenuItemSystemMonitorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSystemMonitorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSystemMonitorActionPerformed
        addSystemMonitorView();
    }//GEN-LAST:event_jMenuItemSystemMonitorActionPerformed

    private void jMenuItemDataAccessLogicEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataAccessLogicEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataAccessLogicEditorActionPerformed
        addDataAccessLogicEditor();
    }//GEN-LAST:event_jMenuItemDataAccessLogicEditorActionPerformed

    private void jMenuItemDialogEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDialogEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDialogEditorActionPerformed
        addNewDialogEditor();
    }//GEN-LAST:event_jMenuItemDialogEditorActionPerformed

    private void jMenuItemUserHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemUserHelpActionPerformed
    {//GEN-HEADEREND:event_jMenuItemUserHelpActionPerformed
        addDefinitionGroupManager(T8HelpDisplayDefinition.GROUP_IDENTIFIER, jMenuItemUserHelp.getText());
    }//GEN-LAST:event_jMenuItemUserHelpActionPerformed

    private void jMenuItemNotificationsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemNotificationsActionPerformed
    {//GEN-HEADEREND:event_jMenuItemNotificationsActionPerformed
        addDefinitionGroupManager(T8NotificationDefinition.GROUP_IDENTIFIER, jMenuItemNotifications.getText());
    }//GEN-LAST:event_jMenuItemNotificationsActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItem2ActionPerformed
    {//GEN-HEADEREND:event_jMenuItem2ActionPerformed
        openMessenger();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void formComponentShown(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentShown
    {//GEN-HEADEREND:event_formComponentShown
        startup();
    }//GEN-LAST:event_formComponentShown

    private void jCheckBoxMenuItemValidateOnSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jCheckBoxMenuItemValidateOnSaveActionPerformed
    {//GEN-HEADEREND:event_jCheckBoxMenuItemValidateOnSaveActionPerformed
        this.context.getClientContext().getConfigurationManager().setUserProperty(this.context, T8ConfigurationManagerResource.USR_PROP_VALIDATE_ON_SAVE, this.jCheckBoxMenuItemValidateOnSave.isSelected());
    }//GEN-LAST:event_jCheckBoxMenuItemValidateOnSaveActionPerformed

    private void jMenuItemUserPropertyEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemUserPropertyEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemUserPropertyEditorActionPerformed
        addDefinitionGroupManager(T8UserPropertyDefinition.GROUP_IDENTIFIER, jMenuItemUserPropertyEditor.getText());
    }//GEN-LAST:event_jMenuItemUserPropertyEditorActionPerformed

    private void ReverseIntegrationSeviceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReverseIntegrationSeviceActionPerformed
        addReverseIntegrationServiceEditor();
    }//GEN-LAST:event_ReverseIntegrationSeviceActionPerformed

    private void jMenuItemDataRecordViewEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataRecordViewEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataRecordViewEditorActionPerformed
        addDataFileViewEditor();
    }//GEN-LAST:event_jMenuItemDataRecordViewEditorActionPerformed

    private void jMenuItemIntegrationResponseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemIntegrationResponseActionPerformed
    {//GEN-HEADEREND:event_jMenuItemIntegrationResponseActionPerformed
        addIntegrationResponseEditor();
    }//GEN-LAST:event_jMenuItemIntegrationResponseActionPerformed

    private void jMenuItemProcedureEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemProcedureEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemProcedureEditorActionPerformed
        addProcedureEditor();
    }//GEN-LAST:event_jMenuItemProcedureEditorActionPerformed

    private void jMenuItemJsonEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemJsonEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemJsonEditorActionPerformed
        addJsonEditor();
    }//GEN-LAST:event_jMenuItemJsonEditorActionPerformed

    private void jMenuItemOrganizationOperationsSetupEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemOrganizationOperationsSetupEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemOrganizationOperationsSetupEditorActionPerformed
        addDefinitionGroupManager(T8OrganizationOperationalSettingsDefinition.GROUP_IDENTIFIER, jMenuItemOrganizationOperationsSetupEditor.getText());
    }//GEN-LAST:event_jMenuItemOrganizationOperationsSetupEditorActionPerformed

    private void jMenuItemDefinitionExistenceQueriesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDefinitionExistenceQueriesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDefinitionExistenceQueriesActionPerformed
        addDefinitionQueryView();
    }//GEN-LAST:event_jMenuItemDefinitionExistenceQueriesActionPerformed

    private void jMenuItemFlowTaskListEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemFlowTaskListEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemFlowTaskListEditorActionPerformed
        addFlowTaskListEditor();
    }//GEN-LAST:event_jMenuItemFlowTaskListEditorActionPerformed

    private void jMenuItemAngularComponentEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAngularComponentEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAngularComponentEditorActionPerformed
        addAngularComponentEditor();
    }//GEN-LAST:event_jMenuItemAngularComponentEditorActionPerformed

    private void jMenuItemDataManagerAdminActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemDataManagerAdminActionPerformed
    {//GEN-HEADEREND:event_jMenuItemDataManagerAdminActionPerformed
        showDataManagerAdministrationDialog();
    }//GEN-LAST:event_jMenuItemDataManagerAdminActionPerformed

    private void jMenuItemTaskEscalationEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemTaskEscalationEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemTaskEscalationEditorActionPerformed
        addTaskEscalationEditor();
    }//GEN-LAST:event_jMenuItemTaskEscalationEditorActionPerformed

    private void jMenuItemProjectEditorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemProjectEditorActionPerformed
    {//GEN-HEADEREND:event_jMenuItemProjectEditorActionPerformed
        addProjectEditor();
    }//GEN-LAST:event_jMenuItemProjectEditorActionPerformed

    private void jMenuItemProjectManagerActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemProjectManagerActionPerformed
    {//GEN-HEADEREND:event_jMenuItemProjectManagerActionPerformed
        addProjectManager();
    }//GEN-LAST:event_jMenuItemProjectManagerActionPerformed


    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        SwingTimerTimingSource timerTimingSource;

        timerTimingSource = new SwingTimerTimingSource();
        Animator.setDefaultTimingSource(timerTimingSource);
        timerTimingSource.init();
        SwingUtilities.invokeLater(() ->
        {
            try
            {
                T8DeveloperFrame developerFrame;

                developerFrame = new T8DeveloperFrame();
                developerFrame.setVisible(true);
                while (!T8DeveloperLoginDialog.login(developerFrame.context, developerFrame.rootPane))
                {
                    // Do nothing, just show the login screen until the user logs in successfully.
                }
            }
            catch(Throwable e)
            {
                LOGGER.log("Failed to start tech 8 system.", e);
                JOptionPane.showMessageDialog(null, "Failed to start Tech 8 System. Please contact your administrator.");
                System.exit(1);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem ReverseIntegrationSevice;
    private javax.swing.JMenuItem dataKeyEditor;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItemValidateOnSave;
    private javax.swing.JLabel jLabelMaximumConcurrency;
    private javax.swing.JLabel jLabelNetworkLatency;
    private javax.swing.JLabel jLabelNetworkThroughput;
    private javax.swing.JLabel jLabelServerActivity;
    private javax.swing.JLabel jLabelServerLoad;
    private javax.swing.JMenuBar jMenuBarMain;
    private javax.swing.JMenu jMenuCommunications;
    private javax.swing.JMenu jMenuData;
    private javax.swing.JMenu jMenuDefinition;
    private javax.swing.JMenu jMenuDefinitionValidation;
    private javax.swing.JMenu jMenuDeveloper;
    private javax.swing.JMenu jMenuFlow;
    private javax.swing.JMenu jMenuFunctional;
    private javax.swing.JMenu jMenuGraphics;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItemAngularComponentEditor;
    private javax.swing.JMenuItem jMenuItemAttachmentHandlerEditor;
    private javax.swing.JMenuItem jMenuItemClearFunctionalityCache;
    private javax.swing.JMenuItem jMenuItemClientOperationEditor;
    private javax.swing.JMenuItem jMenuItemCommunication;
    private javax.swing.JMenuItem jMenuItemCommunicationDetailProvider;
    private javax.swing.JMenuItem jMenuItemCommunicationMessageTemplate;
    private javax.swing.JMenuItem jMenuItemCommunicationMonitor;
    private javax.swing.JMenuItem jMenuItemCommunicationServices;
    private javax.swing.JMenuItem jMenuItemCteEditor;
    private javax.swing.JMenuItem jMenuItemDataAccessLogicEditor;
    private javax.swing.JMenuItem jMenuItemDataCacheEditor;
    private javax.swing.JMenuItem jMenuItemDataConnectionEditor;
    private javax.swing.JMenuItem jMenuItemDataConnectionPoolEditor;
    private javax.swing.JMenuItem jMenuItemDataEntityEditor;
    private javax.swing.JMenuItem jMenuItemDataManagerAdmin;
    private javax.swing.JMenuItem jMenuItemDataModelEditor;
    private javax.swing.JMenuItem jMenuItemDataObjectEditor;
    private javax.swing.JMenuItem jMenuItemDataObjectStateGraphEditor;
    private javax.swing.JMenuItem jMenuItemDataRecordAccessEditor;
    private javax.swing.JMenuItem jMenuItemDataRecordViewEditor;
    private javax.swing.JMenuItem jMenuItemDataSearchComboDataSourceEditor;
    private javax.swing.JMenuItem jMenuItemDataSessionMonitor;
    private javax.swing.JMenuItem jMenuItemDataSourceEditor;
    private javax.swing.JMenuItem jMenuItemDataStreamEditor;
    private javax.swing.JMenuItem jMenuItemDataTypeEditor;
    private javax.swing.JMenuItem jMenuItemDatabaseAdaptorEditor;
    private javax.swing.JMenuItem jMenuItemDefinitionEditorSetup;
    private javax.swing.JMenuItem jMenuItemDefinitionEventHandlerEditor;
    private javax.swing.JMenuItem jMenuItemDefinitionExistenceQueries;
    private javax.swing.JMenuItem jMenuItemDefinitionPatchEditor;
    private javax.swing.JMenuItem jMenuItemDefinitionTypeEditor;
    private javax.swing.JMenuItem jMenuItemDescriptionFormatEditor;
    private javax.swing.JMenuItem jMenuItemDialogEditor;
    private javax.swing.JMenuItem jMenuItemExceptionEditor;
    private javax.swing.JMenuItem jMenuItemExportDefinitions;
    private javax.swing.JMenuItem jMenuItemFileContextBrowser;
    private javax.swing.JMenuItem jMenuItemFileContextEditor;
    private javax.swing.JMenuItem jMenuItemFlowMonitor;
    private javax.swing.JMenuItem jMenuItemFlowTaskListEditor;
    private javax.swing.JMenuItem jMenuItemFunctionalityAccessPolicyEditor;
    private javax.swing.JMenuItem jMenuItemFunctionalityEditor;
    private javax.swing.JMenuItem jMenuItemFunctionalityGroupEditor;
    private javax.swing.JMenuItem jMenuItemFunctionalityMonitor;
    private javax.swing.JMenuItem jMenuItemGlobalComponentEditor;
    private javax.swing.JMenuItem jMenuItemGlobalEventEditor;
    private javax.swing.JMenuItem jMenuItemIconEditor;
    private javax.swing.JMenuItem jMenuItemIdentifierValueEditor;
    private javax.swing.JMenuItem jMenuItemImageEditor;
    private javax.swing.JMenuItem jMenuItemIntegrationResponse;
    private javax.swing.JMenuItem jMenuItemIntegrationService;
    private javax.swing.JMenuItem jMenuItemJsonEditor;
    private javax.swing.JMenuItem jMenuItemLogout;
    private javax.swing.JMenuItem jMenuItemLookAndFeelEditor;
    private javax.swing.JMenuItem jMenuItemMergeConfigurationEditor;
    private javax.swing.JMenuItem jMenuItemModuleEditor;
    private javax.swing.JMenuItem jMenuItemNotifications;
    private javax.swing.JMenuItem jMenuItemOperationMonitor;
    private javax.swing.JMenuItem jMenuItemOrganizationOperationsSetupEditor;
    private javax.swing.JMenuItem jMenuItemOrganizationSetupEditor;
    private javax.swing.JMenuItem jMenuItemPainterEditor;
    private javax.swing.JMenuItem jMenuItemPasswordPolicies;
    private javax.swing.JMenuItem jMenuItemProcedureEditor;
    private javax.swing.JMenuItem jMenuItemProjectEditor;
    private javax.swing.JMenuItem jMenuItemProjectManager;
    private javax.swing.JMenuItem jMenuItemProjectSetup;
    private javax.swing.JMenuItem jMenuItemReloadFunctionalityAccessPolicy;
    private javax.swing.JMenuItem jMenuItemRemoteServerConnection;
    private javax.swing.JMenuItem jMenuItemReportEditor;
    private javax.swing.JMenuItem jMenuItemRestartServer;
    private javax.swing.JMenuItem jMenuItemSearchDefinitions;
    private javax.swing.JMenuItem jMenuItemSendNotification;
    private javax.swing.JMenuItem jMenuItemServerOperationEditor;
    private javax.swing.JMenuItem jMenuItemServerProcessEditor;
    private javax.swing.JMenuItem jMenuItemServiceEditor;
    private javax.swing.JMenuItem jMenuItemSessionMonitor;
    private javax.swing.JMenuItem jMenuItemSystemEditor;
    private javax.swing.JMenuItem jMenuItemSystemLanguageEditor;
    private javax.swing.JMenuItem jMenuItemSystemMonitor;
    private javax.swing.JMenuItem jMenuItemSystemPropertyEditor;
    private javax.swing.JMenuItem jMenuItemSystemValidation;
    private javax.swing.JMenuItem jMenuItemSystemValidationUsages;
    private javax.swing.JMenuItem jMenuItemTaskEscalationEditor;
    private javax.swing.JMenuItem jMenuItemUITranslationEditor;
    private javax.swing.JMenuItem jMenuItemUserEditor;
    private javax.swing.JMenuItem jMenuItemUserHelp;
    private javax.swing.JMenuItem jMenuItemUserProfileEditor;
    private javax.swing.JMenuItem jMenuItemUserPropertyEditor;
    private javax.swing.JMenuItem jMenuItemWorkFlowEditor;
    private javax.swing.JMenuItem jMenuItemWorkFlowProfileEditor;
    private javax.swing.JMenuItem jMenuItemWorkFlowSignalEditor;
    private javax.swing.JMenuItem jMenuItemWorkFlowTaskEditor;
    private javax.swing.JMenu jMenuReporting;
    private javax.swing.JMenu jMenuServerAdmin;
    private javax.swing.JMenu jMenuSystem;
    private javax.swing.JMenu jMenuSystemValidation;
    private javax.swing.JMenu jMenuUIComponents;
    private javax.swing.JMenu jMenuUser;
    private javax.swing.JPanel jPanelStatusBar;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JPopupMenu.Separator jSeparator11;
    private javax.swing.JPopupMenu.Separator jSeparator12;
    private javax.swing.JPopupMenu.Separator jSeparator13;
    private javax.swing.JPopupMenu.Separator jSeparator15;
    private javax.swing.JPopupMenu.Separator jSeparator17;
    private javax.swing.JPopupMenu.Separator jSeparator18;
    private javax.swing.JPopupMenu.Separator jSeparator19;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator20;
    private javax.swing.JPopupMenu.Separator jSeparator21;
    private javax.swing.JPopupMenu.Separator jSeparator22;
    private javax.swing.JPopupMenu.Separator jSeparator23;
    private javax.swing.JPopupMenu.Separator jSeparator24;
    private javax.swing.JPopupMenu.Separator jSeparator25;
    private javax.swing.JPopupMenu.Separator jSeparator26;
    private javax.swing.JPopupMenu.Separator jSeparator27;
    private javax.swing.JPopupMenu.Separator jSeparator28;
    private javax.swing.JPopupMenu.Separator jSeparator29;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator30;
    private javax.swing.JPopupMenu.Separator jSeparator31;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JPopupMenu.Separator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    // End of variables declaration//GEN-END:variables
}
