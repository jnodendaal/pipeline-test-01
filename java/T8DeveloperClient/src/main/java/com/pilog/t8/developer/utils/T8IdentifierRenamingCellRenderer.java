package com.pilog.t8.developer.utils;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8IdentifierRenamingCellRenderer extends DefaultTableCellRenderer
{
    private T8IdentifierRenamingTableModel tableModel;
    
    public T8IdentifierRenamingCellRenderer(T8IdentifierRenamingTableModel tableView)
    {
        this.tableModel = tableView;
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean cellHasFocus, int row, int column)
    {
        Component renderComponent;
        Color textColor;
        
        if (tableModel.isValid(table.convertRowIndexToModel(row)))
        {
            textColor = Color.BLACK;
        }
        else textColor = Color.RED;
        
        renderComponent = super.getTableCellRendererComponent(table, value, isSelected, cellHasFocus, row, column);
        renderComponent.setForeground(textColor);
        return renderComponent;
    }
}
