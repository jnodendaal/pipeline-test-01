package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.epic.rsyntax.HyperlinkFollowEvent;
import com.pilog.epic.rsyntax.HyperlinkFollowListener;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 * @author Bouwer du Preez
 */
public class XmlDatumEditor extends T8DefaultDefinitionDatumEditor implements FocusListener
{
    private RSyntaxTextArea rSyntaxTextArea;

    public XmlDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        rSyntaxTextArea.addFocusListener(this);
    }

    public void setEditorHeight(int height)
    {
        setPreferredSize(new Dimension(0, height));
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
        rSyntaxTextArea.setLinkGenerator(null);
    }

    @Override
    public void commitChanges()
    {
        setDefinitionDatum(rSyntaxTextArea.getText());
    }

    @Override
    public void setEditable(boolean editable)
    {
        rSyntaxTextArea.setEditable(editable);
    }

    @Override
    public void refreshEditor()
    {
        Object value;

        value = getDefinitionDatum();
        rSyntaxTextArea.setText(value != null ? value.toString() : null);
    }

    @Override
    public void focusGained(FocusEvent e)
    {
    }

    @Override
    public void focusLost(FocusEvent e)
    {
        commitChanges();
    }

    private void initComponents()
    {
        ScriptEditorLinkListener listener;

        setPreferredSize(new java.awt.Dimension(0, 500));
        setLayout(new java.awt.BorderLayout());

        rSyntaxTextArea = new RSyntaxTextArea();
        rSyntaxTextArea.setSyntaxEditingStyle(RSyntaxTextArea.SYNTAX_STYLE_XML);
        rSyntaxTextArea.setAnimateBracketMatching(true);
        rSyntaxTextArea.setAntiAliasingEnabled(true);
        rSyntaxTextArea.setAutoIndentEnabled(true);

        listener = new ScriptEditorLinkListener();
        rSyntaxTextArea.setHyperlinksEnabled(true);
        rSyntaxTextArea.setLinkScanningMask(KeyEvent.CTRL_DOWN_MASK);
        rSyntaxTextArea.setLinkGenerator(new com.pilog.epic.rsyntax.LinkGenerator(listener));

        add(new RTextScrollPane(rSyntaxTextArea), java.awt.BorderLayout.CENTER);
    }

    private class ScriptEditorLinkListener implements HyperlinkFollowListener
    {
        @Override
        public String getPatterMatchString()
        {
            return "[@$]+\\w+";
        }

        @Override
        public void hyperlinkFollowed(HyperlinkFollowEvent event)
        {
            fireDefinitionLinkActivatedEvent(definition.getRootProjectId(), event.getHyperlink());
        }
    }
}
