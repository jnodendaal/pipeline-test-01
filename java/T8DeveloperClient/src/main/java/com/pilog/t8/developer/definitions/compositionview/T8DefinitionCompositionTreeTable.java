package com.pilog.t8.developer.definitions.compositionview;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.definition.T8DefinitionComposition;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.developer.definitions.selectiontableview.T8MillisecondCellRenderer;
import com.pilog.t8.utilities.components.cellrenderers.CellRendererUtilities;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.JXTreeTable;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionCompositionTreeTable extends JXTreeTable
{
    private final  T8DefinitionCompositionView view;
    private final T8DefinitionCompositionTreeTableModel model;

    public T8DefinitionCompositionTreeTable(T8ClientContext clientContext, T8DefinitionCompositionView view)
    {
        this.view = view;
        this.model = new T8DefinitionCompositionTreeTableModel(null, false);

        setTreeTableModel(model);
        setRootVisible(true);
        setRowHeight(25);
        setGridColor(new Color(240, 240, 240));
        setTreeCellRenderer(new T8DefinitionCompositionTreeCellRenderer(clientContext.getDefinitionManager(), view));
        setCreatedTimeCellRenderer(new T8MillisecondCellRenderer(null));
        setUpdatedTimeCellRenderer(new T8MillisecondCellRenderer(null));
        addTreeExpansionListener(new ExpansionListener());
    }

    public void setComposition(T8DefinitionComposition composition, boolean createTemporaryNodes)
    {
        model.setComposition(composition, createTemporaryNodes);
        autoSizeColumns();
    }

    public void replaceComposition(T8DefinitionComposition oldComposition, T8DefinitionComposition newComposition, boolean createTemporaryNodes)
    {
        T8DefinitionCompositionTreeTableNode newNode;

        newNode = model.replaceCompositions(oldComposition, newComposition, createTemporaryNodes);
        expandPath(new TreePath(model.getPathToRoot(newNode)));
    }

    public void removeDefinition(T8DefinitionHandle handle)
    {

    }

    public void setNodeExpanded(T8DefinitionComposition composition, boolean expanded)
    {
        T8DefinitionCompositionTreeTableNode node;

        node = model.findCompositionNode(composition);
        if (node != null)
        {
            for (T8DefinitionCompositionTreeTableNode lineNode : node.getLineNodes())
            {
                System.out.println("Setting node expanded: " + lineNode);
                expandPath(lineNode.getPath());
            }
        }
    }

    public T8DefinitionHandle getSelectedDefinitionHandle()
    {
        TreePath selectionPath;

        selectionPath = getTreeSelectionModel().getSelectionPath();
        if (selectionPath != null)
        {
            T8DefinitionCompositionTreeTableNode selectedNode;
            T8DefinitionComposition selectedComposition;

            selectedNode = (T8DefinitionCompositionTreeTableNode)selectionPath.getLastPathComponent();
            selectedComposition = selectedNode.getDefinitionComposition();
            if (selectedComposition != null)
            {
                return selectedComposition.getDefinitionMetaData().getDefinitionHandle();
            }
            else return null;
        }
        else return null;
    }

    public List<T8DefinitionHandle> getSelectedDefinitionHandles()
    {
        List<T8DefinitionHandle> handles;

        handles = new ArrayList<>();
        for (TreePath selectionPath : getTreeSelectionModel().getSelectionPaths())
        {
            T8DefinitionCompositionTreeTableNode selectedNode;
            T8DefinitionComposition selectedComposition;

            selectedNode = (T8DefinitionCompositionTreeTableNode)selectionPath.getLastPathComponent();
            selectedComposition = selectedNode.getDefinitionComposition();
            if (selectedComposition != null)
            {
                handles.add(selectedComposition.getDefinitionMetaData().getDefinitionHandle());
            }
        }

        return handles;
    }

    public void setSelectedDefinition(T8DefinitionHandle handle)
    {

    }

    public List<T8DefinitionHandle> getAllDefinitionHandles()
    {
        return new ArrayList<>();
    }

    public T8DefinitionMetaData getDefinitionMetaData(T8DefinitionHandle handle)
    {
        return null;
    }

    private void setCreatedTimeCellRenderer(TableCellRenderer renderer)
    {
        int columnIndex;

        columnIndex = convertColumnIndexToView(10);
        this.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
    }

    private void setUpdatedTimeCellRenderer(TableCellRenderer renderer)
    {
        int columnIndex;

        columnIndex = convertColumnIndexToView(12);
        this.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
    }

    private void autoSizeColumns()
    {
        // The auto-sizing has be be queued for execution on the EDT after all of the tree setup has been finished.
        SwingUtilities.invokeLater(() ->
        {
            CellRendererUtilities.autoSizeTableColumns(T8DefinitionCompositionTreeTable.this, 100);
        });
    }

    private class ExpansionListener implements TreeExpansionListener
    {
        @Override
        public void treeExpanded(TreeExpansionEvent event)
        {
            autoSizeColumns();
        }

        @Override
        public void treeCollapsed(TreeExpansionEvent event)
        {
        }
    }
}
