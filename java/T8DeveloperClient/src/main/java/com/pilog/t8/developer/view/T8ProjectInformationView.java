package com.pilog.t8.developer.view;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.developer.definitions.selectiontableview.T8DefinitionTable;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.SwingWorker;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectInformationView extends T8DefaultDeveloperView implements T8DeveloperView
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ProjectInformationView.class);

    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final T8DefinitionTable publicDefinitionTable;
    private final T8DefinitionTable privateDefinitionTable;
    private T8DeveloperViewFactory viewFactory;
    private T8ProjectDefinition projectDefinition;
    private MetaLoader metaLoader;

    public T8ProjectInformationView(T8Context context, T8DeveloperViewFactory viewFactory)
    {
        initComponents();
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definitionManager = clientContext.getDefinitionManager();
        this.viewFactory = viewFactory;
        this.publicDefinitionTable = new T8DefinitionTable(context);
        this.publicDefinitionTable.setSelectedColumnVisible(false);
        this.publicDefinitionTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.jScrollPanePublicDefinitions.setViewportView(publicDefinitionTable);
        this.privateDefinitionTable = new T8DefinitionTable(context);
        this.privateDefinitionTable.setSelectedColumnVisible(false);
        this.privateDefinitionTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.jScrollPanePrivateDefinitions.setViewportView(privateDefinitionTable);
        this.setBorder(new T8ContentHeaderBorder("Project Information"));
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return projectDefinition;
    }

    @Override
    public void addView(T8DeveloperView newView)
    {
    }

    @Override
    public void addView(T8DeveloperView newView, String viewHeader)
    {
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public String getHeader()
    {
        return "Project Information";
    }

    @Override
    public void addDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    @Override
    public void removeDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    @Override
    public void setSelectedDefinition(T8Definition selectedDefinition)
    {
        if (selectedDefinition instanceof T8ProjectDefinition)
        {
            String projectId;

            projectId = selectedDefinition.getIdentifier();
            projectDefinition = (T8ProjectDefinition)selectedDefinition;
            setBorder(new T8ContentHeaderBorder("Project Information: " + projectId));

            // Start a meta loader thread to load the details of the project content.
            if (metaLoader == null)
            {
                publicDefinitionTable.setDefinitionMetaData(null);
                privateDefinitionTable.setDefinitionMetaData(null);
                metaLoader = new MetaLoader(projectId);
                metaLoader.execute();
            }
            else if (!metaLoader.getProjectId().equals(projectId)) // A loader is already in progress, so stop it and start a new instance using the latest selected project id.
            {
                publicDefinitionTable.setDefinitionMetaData(null);
                privateDefinitionTable.setDefinitionMetaData(null);
                metaLoader.stop();
                metaLoader = new MetaLoader(projectId);
                metaLoader.execute();
            }
        }
        else
        {
            // Stop and discard any meta loader currently running.
            if (metaLoader != null)
            {
                metaLoader.stop();
                metaLoader = null;
            }

            this.projectDefinition = null;
            this.setBorder(new T8ContentHeaderBorder("Project Information"));
            this.publicDefinitionTable.setDefinitionMetaData(null);
            this.privateDefinitionTable.setDefinitionMetaData(null);
        }
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
        String definitionId;

        definitionId = event.getDefinitionId();
        if (T8IdentifierUtilities.isQualifiedLocalId(definitionId) || T8IdentifierUtilities.isPublicId(definitionId))
        {
            T8DeveloperView developerView;

            developerView = getViewFactory().createDefinitionNavigator(this, event.getProjectId(), T8IdentifierUtilities.getGlobalIdentifierPart(definitionId));
            addView(developerView);

            if (T8IdentifierUtilities.isQualifiedLocalId(definitionId))
            {
                developerView.notifyDefinitionLinkActivated(new T8DefinitionLinkEvent((Component) event.getSource(), event.getProjectId(), T8IdentifierUtilities.getLocalIdentifierPart(definitionId)));
            }
        }
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    private class MetaLoader extends SwingWorker<Void, Void>
    {
        private final String projectId;
        private final List<T8DefinitionMetaData> allMetaData;
        private final List<T8DefinitionMetaData> publicMetaData;
        private final List<T8DefinitionMetaData> privateMetaData;
        private volatile boolean stopped;

        public MetaLoader(String projectId)
        {
            this.projectId = projectId;
            this.allMetaData = new ArrayList<>();
            this.publicMetaData = new ArrayList<>();
            this.privateMetaData = new ArrayList<>();
            this.stopped = false;
        }

        public String getProjectId()
        {
            return projectId;
        }

        public void stop()
        {
            this.stopped = true;
        }

        @Override
        protected Void doInBackground() throws Exception
        {
            // Load the content meta data of the project.
            try
            {
                publicMetaData.clear();
                privateMetaData.clear();
                allMetaData.addAll(definitionManager.getProjectDefinitionMetaData(projectId));
                for (T8DefinitionMetaData metaData : allMetaData)
                {
                    if (metaData.isPublic())
                    {
                        publicMetaData.add(metaData);
                    }
                    else
                    {
                        privateMetaData.add(metaData);
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading meta data for content definitions of project " + projectId, e);
                return null;
            }
        }

        @Override
        public void done()
        {
            if (!stopped)
            {
                publicDefinitionTable.setDefinitionMetaData(publicMetaData);
                privateDefinitionTable.setDefinitionMetaData(privateMetaData);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPaneMain = new javax.swing.JScrollPane();
        jPanelMain = new javax.swing.JPanel();
        jPanelProjectDetails = new javax.swing.JPanel();
        jPanelPublicDefinitions = new javax.swing.JPanel();
        jLabelPublicDefinitions = new javax.swing.JLabel();
        jScrollPanePublicDefinitions = new javax.swing.JScrollPane();
        jPanelPrivateDefinitions = new javax.swing.JPanel();
        jLabelPrivateDefinitions = new javax.swing.JLabel();
        jScrollPanePrivateDefinitions = new javax.swing.JScrollPane();

        setLayout(new java.awt.GridBagLayout());

        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jPanelProjectDetails.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelMain.add(jPanelProjectDetails, gridBagConstraints);

        jPanelPublicDefinitions.setLayout(new java.awt.BorderLayout());

        jLabelPublicDefinitions.setText("Public Definitions");
        jPanelPublicDefinitions.add(jLabelPublicDefinitions, java.awt.BorderLayout.PAGE_START);
        jPanelPublicDefinitions.add(jScrollPanePublicDefinitions, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelMain.add(jPanelPublicDefinitions, gridBagConstraints);

        jPanelPrivateDefinitions.setLayout(new java.awt.BorderLayout());

        jLabelPrivateDefinitions.setText("Private Definitions");
        jPanelPrivateDefinitions.add(jLabelPrivateDefinitions, java.awt.BorderLayout.PAGE_START);
        jPanelPrivateDefinitions.add(jScrollPanePrivateDefinitions, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelMain.add(jPanelPrivateDefinitions, gridBagConstraints);

        jScrollPaneMain.setViewportView(jPanelMain);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jScrollPaneMain, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelPrivateDefinitions;
    private javax.swing.JLabel jLabelPublicDefinitions;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JPanel jPanelPrivateDefinitions;
    private javax.swing.JPanel jPanelProjectDetails;
    private javax.swing.JPanel jPanelPublicDefinitions;
    private javax.swing.JScrollPane jScrollPaneMain;
    private javax.swing.JScrollPane jScrollPanePrivateDefinitions;
    private javax.swing.JScrollPane jScrollPanePublicDefinitions;
    // End of variables declaration//GEN-END:variables
}

