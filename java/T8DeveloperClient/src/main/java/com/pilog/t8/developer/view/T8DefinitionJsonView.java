package com.pilog.t8.developer.view;

import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionSerializer;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionJsonView extends T8DefaultDeveloperView
{
    private final T8DefinitionContext definitionContext;
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8Context context;
    private final T8DeveloperView parentView;
    private T8DeveloperViewFactory viewFactory;
    private final DefaultTableModel tableModel;

    public T8DefinitionJsonView(T8DefinitionContext definitionContext, T8DeveloperView parentView)
    {
        this.definitionContext = definitionContext;
        this.context = definitionContext.getContext();
        this.clientContext = definitionContext.getClientContext();
        this.sessionContext = clientContext.getSessionContext();
        this.parentView = parentView;
        initComponents();
        this.tableModel = (DefaultTableModel)jTableMapping.getModel();
    }

    @Override
    public String getHeader()
    {
        return "JSON View";
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSelectedDefinition(T8Definition definition)
    {
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    private void addMappingEntry(String regularExpression, String replacementString)
    {

        Object[] row;

        row = new Object[3];
        row[0] = false;
        row[1] = regularExpression;
        row[2] = replacementString;

        tableModel.addRow(row);
    }

    private void deleteSelectedRows()
    {
        int selectedIndex;

        while ((selectedIndex = jTableMapping.getSelectedRow()) > -1)
        {
            tableModel.removeRow(selectedIndex);
        }
    }

    private void loadMappingFromEntity()
    {
        T8DefinitionManager definitionManager;
        String entityId;

        definitionManager = clientContext.getDefinitionManager();

        entityId = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter the identifier of the entity to use.", "Data Entity Identifier", JOptionPane.QUESTION_MESSAGE);
        if (!Strings.isNullOrEmpty(entityId))
        {
            try
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionManager.getRawDefinition(context, null, entityId);
                if (entityDefinition != null)
                {
                    String stringFieldIdentifier;

                    stringFieldIdentifier = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter the identifier of the entity field from which to extract regular expression values.", "RegEx Entity Field Identifier", JOptionPane.QUESTION_MESSAGE);
                    if (entityDefinition.containsField(stringFieldIdentifier))
                    {
                        String replacementFieldIdentifier;

                        replacementFieldIdentifier = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter the identifier of the entity field from which to extract replacement string values.", "Replacement String Entity Field Identifier", JOptionPane.QUESTION_MESSAGE);
                        if (entityDefinition.containsField(replacementFieldIdentifier))
                        {
                            int count;

                            count = T8DataManagerOperationHandler.countDataEntities(context, null, entityId, null);
                            if (count < 50001)
                            {
                                List<T8DataEntity> retrievedData;

                                retrievedData = T8DataManagerOperationHandler.selectDataEntities(context, entityId, null, 0, 50000);
                                for (T8DataEntity entity : retrievedData)
                                {
                                    String regEx;
                                    String replacementString;

                                    regEx = (String)entity.getFieldValue(stringFieldIdentifier);
                                    replacementString = (String)entity.getFieldValue(replacementFieldIdentifier);
                                    addMappingEntry(regEx, replacementString);
                                }
                            }
                            else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity '" + replacementFieldIdentifier + "' countains " + count + " rows, exceeding the 50000 limit.", "Data Size Restriction Exceeded", JOptionPane.ERROR_MESSAGE);
                        }
                        else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity Field '" + replacementFieldIdentifier + "' could not be found in data entity '" + entityId + "'.", "Data Entity Field Not Found", JOptionPane.ERROR_MESSAGE);
                    }
                    else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity Field '" + stringFieldIdentifier + "' could not be found in data entity '" + entityId + "'.", "Data Entity Field Not Found", JOptionPane.ERROR_MESSAGE);
                }
                else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity '" + entityId + "' could not be found.", "Data Entity Not Found", JOptionPane.ERROR_MESSAGE);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving String Map from data entity.", e);
                JOptionPane.showMessageDialog(clientContext.getParentWindow(), "An unexpected exception occurred during the requested operation.", "Operation Failure", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void markProcessed(int rowIndex, boolean processed)
    {
        tableModel.setValueAt(processed, rowIndex, 0);
    }

    private void markSelectedProcessedIndicator(boolean processed)
    {
        int[] selectedIndices;

        selectedIndices = jTableMapping.getSelectedRows();
        for (int index : selectedIndices)
        {
            tableModel.setValueAt(processed, index, 0);
        }
    }

    /**
     * This entire method should be a strict server-side operation.
     * It is implemented here for experimental purposes only.
     */
    private void process()
    {
        T8DefinitionManager definitionManager;
        T8DefinitionSerializer serializer;
        int updateCount;

        updateCount = 0;
        definitionManager = clientContext.getDefinitionManager();
        serializer = new T8DefinitionSerializer(definitionManager);
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            boolean processed;

            processed = (Boolean)tableModel.getValueAt(rowIndex, 0);
            if (!processed)
            {
                try
                {
                    String regularExpression;
                    String replacementString;
                    List<T8DefinitionHandle> definitionHandles;

                    regularExpression = (String)tableModel.getValueAt(rowIndex, 1);
                    replacementString = (String)tableModel.getValueAt(rowIndex, 2);

                    T8Log.log("Processing row [" + rowIndex + "] using expression '" + regularExpression + "' and replacement string '" + replacementString + "'...");
                    definitionHandles = definitionManager.findDefinitionsByRegularExpression(context, regularExpression);
                    T8Log.log("Definitions identified: " + definitionHandles.size());
                    for (T8DefinitionHandle definitionHandle : definitionHandles)
                    {
                        try
                        {
                            T8Definition definition;
                            String definitionString;

                            T8Log.log("Processing definition: " + definitionHandle);
                            definition = definitionManager.lockDefinition(context, definitionHandle, sessionContext.getSessionIdentifier());
                            definitionString = serializer.serializeDefinition(definition);
                            definitionString = definitionString.replaceAll(regularExpression, replacementString);
                            definition = serializer.deserializeDefinition(definitionString);
                            definitionManager.unlockDefinition(context, definition, context.getSessionId());
                            updateCount++;
                        }
                        catch (Exception e)
                        {
                            T8Log.log("Exception while processing definition: " + definitionHandle + " in row: " + rowIndex, e);

                            try
                            {
                                definitionManager.unlockDefinition(context, definitionHandle, sessionContext.getSessionIdentifier());
                            }
                            catch (Exception e2)
                            {
                                T8Log.log("Exception while trying to unlock definition.", e2);
                            }
                        }
                    }

                    // Set the row as processed.
                    markProcessed(rowIndex, true);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while processing row: " + rowIndex, e);
                }
            }
        }

        JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definitions Updated: " + updateCount, "Operation Complete", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jTabbedPaneMain = new javax.swing.JTabbedPane();
        jPanelSearchAndReplace = new javax.swing.JPanel();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonAddRow = new javax.swing.JButton();
        jButtonDeleteRows = new javax.swing.JButton();
        jButtonLoadFromEntity = new javax.swing.JButton();
        jButtonMarkAsUnprocessed = new javax.swing.JButton();
        jButtonMarkAsProcessed = new javax.swing.JButton();
        jButtonProcess = new javax.swing.JButton();
        jScrollPaneMapping = new javax.swing.JScrollPane();
        jTableMapping = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jPanelSearchAndReplace.setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonAddRow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/tableInsertRowIcon.png"))); // NOI18N
        jButtonAddRow.setText("Add Row");
        jButtonAddRow.setToolTipText("Add a new row to the table.");
        jButtonAddRow.setFocusable(false);
        jButtonAddRow.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddRow.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddRowActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAddRow);

        jButtonDeleteRows.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/tableDeleteRowIcon.png"))); // NOI18N
        jButtonDeleteRows.setText("Delete Rows");
        jButtonDeleteRows.setToolTipText("Delete all selected rows from the table.");
        jButtonDeleteRows.setFocusable(false);
        jButtonDeleteRows.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDeleteRows.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDeleteRowsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDeleteRows);

        jButtonLoadFromEntity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png"))); // NOI18N
        jButtonLoadFromEntity.setText("Load from Entity");
        jButtonLoadFromEntity.setToolTipText("Load content into the table from a database entity.");
        jButtonLoadFromEntity.setFocusable(false);
        jButtonLoadFromEntity.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonLoadFromEntity.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonLoadFromEntityActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonLoadFromEntity);

        jButtonMarkAsUnprocessed.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/checkBoxUncheckIcon.png"))); // NOI18N
        jButtonMarkAsUnprocessed.setText("Mark as Unprocessed");
        jButtonMarkAsUnprocessed.setFocusable(false);
        jButtonMarkAsUnprocessed.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMarkAsUnprocessed.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMarkAsUnprocessedActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonMarkAsUnprocessed);

        jButtonMarkAsProcessed.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/checkBoxCheckIcon.png"))); // NOI18N
        jButtonMarkAsProcessed.setText("Mark as Processed");
        jButtonMarkAsProcessed.setFocusable(false);
        jButtonMarkAsProcessed.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMarkAsProcessed.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMarkAsProcessedActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonMarkAsProcessed);

        jButtonProcess.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/testIcon.png"))); // NOI18N
        jButtonProcess.setText("Process");
        jButtonProcess.setFocusable(false);
        jButtonProcess.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonProcess.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonProcessActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonProcess);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelSearchAndReplace.add(jToolBarMain, gridBagConstraints);

        jTableMapping.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Processed", "Regex", "Replacement"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableMapping.setRowHeight(25);
        jScrollPaneMapping.setViewportView(jTableMapping);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelSearchAndReplace.add(jScrollPaneMapping, gridBagConstraints);

        jTabbedPaneMain.addTab("Search & Replace", jPanelSearchAndReplace);

        add(jTabbedPaneMain, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonLoadFromEntityActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonLoadFromEntityActionPerformed
    {//GEN-HEADEREND:event_jButtonLoadFromEntityActionPerformed
        loadMappingFromEntity();
    }//GEN-LAST:event_jButtonLoadFromEntityActionPerformed

    private void jButtonMarkAsUnprocessedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMarkAsUnprocessedActionPerformed
    {//GEN-HEADEREND:event_jButtonMarkAsUnprocessedActionPerformed
        markSelectedProcessedIndicator(false);
    }//GEN-LAST:event_jButtonMarkAsUnprocessedActionPerformed

    private void jButtonMarkAsProcessedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMarkAsProcessedActionPerformed
    {//GEN-HEADEREND:event_jButtonMarkAsProcessedActionPerformed
        markSelectedProcessedIndicator(true);
    }//GEN-LAST:event_jButtonMarkAsProcessedActionPerformed

    private void jButtonProcessActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonProcessActionPerformed
    {//GEN-HEADEREND:event_jButtonProcessActionPerformed
        process();
    }//GEN-LAST:event_jButtonProcessActionPerformed

    private void jButtonAddRowActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddRowActionPerformed
    {//GEN-HEADEREND:event_jButtonAddRowActionPerformed
        addMappingEntry(null, null);
    }//GEN-LAST:event_jButtonAddRowActionPerformed

    private void jButtonDeleteRowsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteRowsActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteRowsActionPerformed
        deleteSelectedRows();
    }//GEN-LAST:event_jButtonDeleteRowsActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddRow;
    private javax.swing.JButton jButtonDeleteRows;
    private javax.swing.JButton jButtonLoadFromEntity;
    private javax.swing.JButton jButtonMarkAsProcessed;
    private javax.swing.JButton jButtonMarkAsUnprocessed;
    private javax.swing.JButton jButtonProcess;
    private javax.swing.JPanel jPanelSearchAndReplace;
    private javax.swing.JScrollPane jScrollPaneMapping;
    private javax.swing.JTabbedPane jTabbedPaneMain;
    private javax.swing.JTable jTableMapping;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
