package com.pilog.t8.developer.view;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionSelectionEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.ui.event.T8ViewHeaderChangedEvent;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Map;
import javax.swing.JPanel;
import org.jdesktop.swingx.JXMultiSplitPane;
import org.jdesktop.swingx.MultiSplitLayout;

/**
 * @author Bouwer du Preez
 */
public class T8MultiSplitDefinitionView extends T8DefaultDeveloperView implements T8DeveloperView
{
    private T8ClientContext clientContext;
    private T8SessionContext sessionContext;
    private Map<String, T8DeveloperView> views;
    private T8DeveloperView parentView;
    private T8DefinitionViewListener changeListener;
    private JXMultiSplitPane multiSplit;
    private JPanel jPanelDefinitionEditor;
    private T8DeveloperViewFactory viewFactory;
    private T8Definition selectedDefinition;
    private boolean selectionIsAdjusting; // Boolean flag to prevent looping selection events.
    private String mainViewId;

    private static String DEFAULT_LAYOUT_STRING = "(ROW (LEAF name=left weight=0.25) (LEAF name=right weight=0.75))";

    public T8MultiSplitDefinitionView(T8DefinitionContext definitionContext, T8DeveloperView parentView, Map<String, T8DeveloperView> views, String layoutString, String mainViewId)
    {
        initComponents();
        this.clientContext = definitionContext.getClientContext();
        this.sessionContext = definitionContext.getSessionContext();
        this.views = views;
        this.mainViewId = mainViewId;
        this.parentView = parentView;
        this.changeListener = new DefinitionChangeListener();
        this.jPanelDefinitionEditor = new JPanel();
        this.jPanelDefinitionEditor.setLayout(new BorderLayout());
        this.multiSplit = new JXMultiSplitPane();
        this.multiSplit.setModel(MultiSplitLayout.parseModel(layoutString));
        this.add(multiSplit, java.awt.BorderLayout.CENTER);
        this.viewFactory = parentView != null ? parentView.getViewFactory() : null;
        this.selectionIsAdjusting = false;
        ((MultiSplitLayout)this.multiSplit.getLayout()).setLayoutByWeight(true);
        ((MultiSplitLayout)this.multiSplit.getLayout()).setLayoutMode(MultiSplitLayout.NO_MIN_SIZE_LAYOUT);
        addSubViews();
    }

    public T8MultiSplitDefinitionView(T8DefinitionContext definitionContext, T8DeveloperView parentView, T8DeveloperView selectionView)
    {
        this(definitionContext, parentView, HashMaps.newHashMap("left", selectionView, "right", new T8DefinitionEditorView(definitionContext, parentView)), DEFAULT_LAYOUT_STRING, "left");
    }

    @Override
    public void startComponent()
    {
        for (T8DeveloperView view : views.values())
        {
            view.startComponent();
        }
    }

    @Override
    public void stopComponent()
    {
        for (T8DeveloperView view : views.values())
        {
            view.stopComponent();
        }
    }

    @Override
    public boolean canClose()
    {
        for (T8DeveloperView view : views.values())
        {
            if (!view.canClose())
            {
                return false;
            }
        }

        return true;
    }

    private void addSubViews()
    {
        for (String subViewIdentifier : views.keySet())
        {
            T8DeveloperView subView;

            subView = views.get(subViewIdentifier);
            subView.addDefinitionViewListener(changeListener);
            multiSplit.add((Component)subView, subViewIdentifier);
        }

        multiSplit.revalidate();
    }

    @Override
    public String getHeader()
    {
        if (views != null)
        {
            if (!Strings.isNullOrEmpty(mainViewId))
            {
                return views.get(mainViewId).getHeader();
            }
            else
            {
                for (T8DeveloperView view : views.values())
                {
                    String viewHeader;

                    viewHeader = view.getHeader();
                    if (!Strings.isNullOrEmpty(viewHeader))
                    {
                        return viewHeader;
                    }
                }
            }
        }

        return "Untitled View";
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        parentView.addView(view);
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        parentView.removeView(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
        for (T8DeveloperView view : views.values())
        {
            view.setViewFactory(viewFactory);
        }
    }

    @Override
    public void setSelectedDefinition(T8Definition definition)
    {
        selectionIsAdjusting = true;
        this.selectedDefinition = definition;
        for (T8DeveloperView subView : views.values())
        {
            subView.setSelectedDefinition(definition);
        }

        selectionIsAdjusting = false;
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return selectedDefinition;
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
        super.notifyDefinitionLinkActivated(event);

        if(T8IdentifierUtilities.isLocalId(event.getDefinitionId()))
        {
            for (T8DeveloperView t8DeveloperView : views.values())
            {
                if(event.getSource() != t8DeveloperView) t8DeveloperView.definitionLinkActivated(event.getDefinitionId());
            }
        }
        else
        {
            parentView.notifyDefinitionLinkActivated(event);
        }
    }

    @Override
    public void definitionLinkActivated(String identifier)
    {
        super.definitionLinkActivated(identifier);

        for (T8DeveloperView t8DeveloperView : views.values())
        {
            t8DeveloperView.definitionLinkActivated(identifier);
        }
    }

    private class DefinitionChangeListener implements T8DefinitionViewListener
    {
        @Override
        public void selectionChanged(T8DefinitionSelectionEvent event)
        {
            // Only handle this event if the selection is not currently being adjusted.
            if (!selectionIsAdjusting)
            {
                T8Definition newSelection;

                newSelection = event.getNewSelection();
                for (T8DeveloperView subView : views.values())
                {
                    if (event.getSource() != subView)
                    {
                        subView.setSelectedDefinition(newSelection);
                    }
                }
            }
        }

        @Override
        public void definitionLinkActivated(T8DefinitionLinkEvent event)
        {
        }

        @Override
        public void headerChanged(T8ViewHeaderChangedEvent event)
        {
            T8DeveloperView view;

            view = event.getView();
            if (views.get(mainViewId) == view)
            {
                fireHeaderChangedEvent(getHeader());
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
