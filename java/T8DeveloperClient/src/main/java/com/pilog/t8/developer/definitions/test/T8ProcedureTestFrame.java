package com.pilog.t8.developer.definitions.test;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.procedure.T8ProcedureDefinition;
import com.pilog.t8.developer.definitions.test.harness.T8ProcedureTestHarness;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.security.T8Context;
import java.awt.Dimension;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import static com.pilog.t8.definition.procedure.T8ProcedureResources.*;
import com.pilog.t8.security.T8UserLoginResponse;

/**
 * @author Bouwer du Preez
 */
public class T8ProcedureTestFrame extends javax.swing.JFrame
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8Context testContext;
    private final T8SecurityManager securityManager;
    private final T8ProcedureDefinition procedureDefinition;
    private final Preferences preferences;
    private final T8DefaultComponentContainer container;
    private final boolean testSessionCreated;

    public T8ProcedureTestFrame(T8Context context, T8ProcedureDefinition procedureDefinition, T8ProcedureTestHarness testHarness) throws Exception
    {
        initComponents();
        this.clientContext = context.getClientContext();
        this.context = context;
        this.procedureDefinition = procedureDefinition;
        this.preferences = Preferences.userNodeForPackage(T8ProcedureTestFrame.class);
        this.securityManager = clientContext.getSecurityManager();

        // If a user is defined by the test harness, create a new session context for the test.
        if ((testHarness != null) && (testHarness.getUserIdentifier() != null))
        {
            T8UserLoginResponse loginResponse;

            this.testContext = new T8Context(clientContext, securityManager.createNewSessionContext());
            T8Log.log("Loggin in as test user: " + testHarness.getUserIdentifier());
            loginResponse = securityManager.login(testContext, T8SecurityManager.LoginLocation.DEVELOPER, testHarness.getUserIdentifier(), testHarness.getUserPassword(), false);
            if (loginResponse.getResponseType().isSessionAuthenticated())
            {
                this.securityManager.switchUserProfile(testContext, testHarness.getUserProfileIdentifier());
                this.testSessionCreated = true;
            }
            else
            {
                Toast.show("Failed to login with user " + testHarness.getUserIdentifier(), Toast.Style.ERROR);
                this.testSessionCreated = false;
            }
        }
        else
        {
            this.testContext = new T8Context(context);
            this.testSessionCreated = false;
        }

        // Set the project context for the test.
        testContext.setProjectId(procedureDefinition.getRootProjectId());

        if (clientContext.getParentWindow() != null)
        {
            Dimension parentDimension = clientContext.getParentWindow().getSize();
            parentDimension.height = (int)(parentDimension.height * 0.80);
            parentDimension.width = (int)(parentDimension.width * 0.80);
            setPreferredSize(new Dimension(parentDimension.width, parentDimension.height));
        }
        else
        {
            setPreferredSize(new Dimension(600, 600));
        }

        setTitle("Procedure Definition Test: " + procedureDefinition.getIdentifier());
        pack();
        setLocationRelativeTo(clientContext.getParentWindow());

        // Set input parameters.
        setInputParameters(procedureDefinition.getInputParameterDefinitions());

        container = new T8DefaultComponentContainer(jTabbedPaneContent);
        setContentPane(container);
        if (!procedureDefinition.isLocked()) Toast.show("Definition not saved.", Toast.Style.NORMAL, 5000);
    }

    private void executeOperation()
    {
        OperationExecutor executor;

        executor = new OperationExecutor(testContext);
        executor.execute();
        container.setLocked(true);
    }

    private Map<String, Object> evaluateInputParameters()
    {
        T8ClientExpressionEvaluator expressionEvaluator;
        Map<String, Object> inputParameters;
        Map<String, String> inputParameterExpressions;
        TableModel model;

        // Stop editing.
        if (jTableInputParameters.getCellEditor() != null) jTableInputParameters.getCellEditor().stopCellEditing();

        model = jTableInputParameters.getModel();
        expressionEvaluator = new T8ClientExpressionEvaluator(context);
        inputParameters = new HashMap<String, Object>();
        inputParameterExpressions = new HashMap<String, String>();
        for (int rowIndex = 0; rowIndex < model.getRowCount(); rowIndex++)
        {
            String parameterIdentifier;
            String valueExpression;
            Object value;

            parameterIdentifier = (String)model.getValueAt(rowIndex, 0);
            valueExpression = (String)model.getValueAt(rowIndex, 1);

            if (Strings.isNullOrEmpty(valueExpression))
            {
                value = null;
            }
            else
            {
                try
                {
                    value = expressionEvaluator.evaluateExpression(valueExpression, null, null);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while evaluating value expression for task input parameter: " + parameterIdentifier, e);
                    value = null;
                }
            }

            inputParameterExpressions.put(parameterIdentifier, valueExpression);
            inputParameters.put(procedureDefinition.getIdentifier() + parameterIdentifier, value);
        }

        // Update the preferences.
        setInputParameterPreferences(inputParameterExpressions);
        return inputParameters;
    }

    private void setInputParameterPreferences(Map<String, String> inputParameterExpressions)
    {
        StringBuffer parameterString;

        parameterString = new StringBuffer();
        if (inputParameterExpressions != null)
        {
            for (String parameterIdentifier : inputParameterExpressions.keySet())
            {
                String expression;

                expression = inputParameterExpressions.get(parameterIdentifier);

                parameterString.append(parameterIdentifier);
                parameterString.append("```");
                parameterString.append(Strings.isNullOrEmpty(expression) ? "" : expression);
                parameterString.append("```");
            }
        }

        preferences.put(procedureDefinition.getIdentifier(), parameterString.toString());
    }

    private Map<String, String> getInputParameterPreferences()
    {
        String parameterString;

        parameterString = preferences.get(procedureDefinition.getIdentifier(), null);
        if (!Strings.isNullOrEmpty(parameterString))
        {
            try
            {
                Map<String, String> parameterPreferences;
                String[] splits;

                splits = parameterString.split("```");
                parameterPreferences = new HashMap<String, String>();
                for (int index = 0; index < splits.length -1; index += 2)
                {
                    String parameterIdentifier;
                    String expression;

                    parameterIdentifier = splits[index];
                    expression = splits[index+1];
                    parameterPreferences.put(parameterIdentifier, expression);
                }

                return parameterPreferences;
            }
            catch (Exception e)
            {
                T8Log.log("Exception while parsing input parameter preferences.", e);
                return null;
            }
        }
        else return null;
    }

    private void setInputParameters(List<T8DataParameterDefinition> parameterDefinitions)
    {
        DefaultTableModel model;
        Map<String, String> inputParameterPreferences;

        // Get any previously set preferences.
        inputParameterPreferences = getInputParameterPreferences();

        // Get the model to update.
        model = (DefaultTableModel)jTableInputParameters.getModel();

        // Clear the table model.
        while (model.getRowCount() > 0)
        {
            model.removeRow(0);
        }

        // Add all input parameters to the model.
        for (T8DataParameterDefinition parameterDefinition :  parameterDefinitions)
        {
            Object[] rowData;

            rowData = new Object[3];
            rowData[0] = parameterDefinition.getIdentifier();
            rowData[1] = inputParameterPreferences != null ? inputParameterPreferences.get(parameterDefinition.getIdentifier()) : null;
            model.addRow(rowData);
        }
    }

    private void setOutputParameters(List<T8DataParameterDefinition> outputParameterDefinitions, Map<String, Object> outputParameters)
    {
        DefaultTableModel model;

        // Get the model to update.
        model = (DefaultTableModel)jTableOutputParameters.getModel();

        // Clear the table model.
        while (model.getRowCount() > 0)
        {
            model.removeRow(0);
        }

        // Add all output parameters to the model.
        for (T8DataParameterDefinition parameterDefinition :  outputParameterDefinitions)
        {
            String parameterIdentifier;
            Object[] rowData;
            Object value;

            parameterIdentifier = parameterDefinition.getIdentifier();

            rowData = new Object[3];
            rowData[0] = parameterIdentifier;

            value = outputParameters.get(parameterDefinition.getPublicIdentifier());

            if (value == null)
            {
                rowData[1] = null;
                rowData[2] = "null";
            }
            else
            {
                rowData[1] = value.toString();
                rowData[2] = value.getClass().getSimpleName();
            }

            model.addRow(rowData);
        }

        jTabbedPaneContent.setSelectedIndex(1);
    }

    public static final void testProcedureDefinition(T8Context context, T8ProcedureDefinition procedureDefinition, T8ProcedureTestHarness testHarness) throws Exception
    {
        T8ProcedureTestFrame testFrame;

        testFrame = new T8ProcedureTestFrame(context, procedureDefinition, testHarness);
        testFrame.setVisible(true);
    }

    private class OperationExecutor extends SwingWorker<Map<String, Object>, Void>
    {
        private final T8Context testContext;

        public OperationExecutor(T8Context testContext)
        {
            this.testContext = testContext;
        }

        @Override
        protected Map<String, Object> doInBackground() throws Exception
        {
            Map<String, Object> procedureInputParameters;
            Map<String, Object> operationInputParameters;
            Map<String, Object> operationOutputParameters;

            procedureInputParameters = evaluateInputParameters();
            operationInputParameters = new HashMap<>();
            operationInputParameters.put(PARAMETER_PROCEDURE_IDENTIFIER, procedureDefinition.getIdentifier());
            operationInputParameters.put(PARAMETER_INPUT_PARAMETERS, procedureInputParameters);
            operationOutputParameters = T8MainServerClient.executeSynchronousOperation(testContext, OPERATION_EXECUTE_PROCEDURE, operationInputParameters);
            return (Map<String, Object>)operationOutputParameters.get(PARAMETER_OUTPUT_PARAMETERS);
        }

        @Override
        protected void done()
        {
            Map<String, Object> outputParameters;

            try
            {
                outputParameters = get();

                setOutputParameters(procedureDefinition.getOutputParameterDefinitions(), outputParameters);
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to execute operation", ex);

                jEditorPane1.setText(null);

                PrintWriter writer;
                StringWriter stringWriter;

                stringWriter = new StringWriter();
                writer = new PrintWriter(stringWriter);

                ex.printStackTrace(writer);

                jEditorPane1.setText(stringWriter.toString());
                jEditorPane1.setCaretPosition(0);

                writer.close();

                jTabbedPaneContent.setSelectedIndex(2);
            }
            container.setLocked(false);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jTabbedPaneContent = new javax.swing.JTabbedPane();
        jPanelOperationInput = new javax.swing.JPanel();
        jScrollPaneInputParameters = new javax.swing.JScrollPane();
        jTableInputParameters = new javax.swing.JTable();
        jPanelInputControls = new javax.swing.JPanel();
        jButtonExecuteProcedure = new javax.swing.JButton();
        jPanelOperationOutput = new javax.swing.JPanel();
        jScrollPaneOutputParameters = new javax.swing.JScrollPane();
        jTableOutputParameters = new javax.swing.JTable();
        jPanelOutputControls = new javax.swing.JPanel();
        jButtonClose = new javax.swing.JButton();
        jPanelExceptionDetails = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();

        jPanelOperationInput.setLayout(new java.awt.BorderLayout());

        jTableInputParameters.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Parameter Identifier", "Value Expression"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableInputParameters.setRowHeight(25);
        jScrollPaneInputParameters.setViewportView(jTableInputParameters);

        jPanelOperationInput.add(jScrollPaneInputParameters, java.awt.BorderLayout.CENTER);

        jPanelInputControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 5, 1));

        jButtonExecuteProcedure.setText("Execute Procedure");
        jButtonExecuteProcedure.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButtonExecuteProcedure.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonExecuteProcedureActionPerformed(evt);
            }
        });
        jPanelInputControls.add(jButtonExecuteProcedure);

        jPanelOperationInput.add(jPanelInputControls, java.awt.BorderLayout.PAGE_END);

        jTabbedPaneContent.addTab("Operation Input", null, jPanelOperationInput, "");

        jPanelOperationOutput.setLayout(new java.awt.BorderLayout());

        jTableOutputParameters.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Parameter Identifier", "Value", "Value Type"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableOutputParameters.setRowHeight(25);
        jScrollPaneOutputParameters.setViewportView(jTableOutputParameters);

        jPanelOperationOutput.add(jScrollPaneOutputParameters, java.awt.BorderLayout.CENTER);

        jPanelOutputControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 5, 1));

        jButtonClose.setText("Close");
        jButtonClose.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCloseActionPerformed(evt);
            }
        });
        jPanelOutputControls.add(jButtonClose);

        jPanelOperationOutput.add(jPanelOutputControls, java.awt.BorderLayout.PAGE_END);

        jTabbedPaneContent.addTab("Operation Output", null, jPanelOperationOutput, "");

        jPanelExceptionDetails.setLayout(new java.awt.BorderLayout());

        jEditorPane1.setEditable(false);
        jScrollPane1.setViewportView(jEditorPane1);

        jPanelExceptionDetails.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jTabbedPaneContent.addTab("Exception Details", null, jPanelExceptionDetails, "");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("T8Developer Module Test");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentShown(java.awt.event.ComponentEvent evt)
            {
                formComponentShown(evt);
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentShown
    {//GEN-HEADEREND:event_formComponentShown
        // Start the module when it is shown for the first time.

    }//GEN-LAST:event_formComponentShown

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        // Stop the tested component when this window is closed.

    }//GEN-LAST:event_formWindowClosing

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButtonCloseActionPerformed

    private void jButtonExecuteProcedureActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExecuteProcedureActionPerformed
    {//GEN-HEADEREND:event_jButtonExecuteProcedureActionPerformed
        executeOperation();
    }//GEN-LAST:event_jButtonExecuteProcedureActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JButton jButtonExecuteProcedure;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JPanel jPanelExceptionDetails;
    private javax.swing.JPanel jPanelInputControls;
    private javax.swing.JPanel jPanelOperationInput;
    private javax.swing.JPanel jPanelOperationOutput;
    private javax.swing.JPanel jPanelOutputControls;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPaneInputParameters;
    private javax.swing.JScrollPane jScrollPaneOutputParameters;
    private javax.swing.JTabbedPane jTabbedPaneContent;
    private javax.swing.JTable jTableInputParameters;
    private javax.swing.JTable jTableOutputParameters;
    // End of variables declaration//GEN-END:variables
}
