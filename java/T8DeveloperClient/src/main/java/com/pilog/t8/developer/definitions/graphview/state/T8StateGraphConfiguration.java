package com.pilog.t8.developer.definitions.graphview.state;

import com.pilog.graph.model.GraphEdge.EdgeType;
import java.awt.Color;
import java.awt.Font;

/**
 * @author Bouwer du Preez
 */
public class T8StateGraphConfiguration
{
    private Color swimlaneBackgroundColor = new Color(240, 240, 250);
    private Color swimlaneTextColor = Color.DARK_GRAY;
    private Color swimlangeBorderColor = Color.GRAY;

    private Color activityHighColor =  new Color(240, 240, 240);
    private Color activityLowColor = new Color(170, 170, 255);

    private Color startHighColor =  new Color(240, 240, 240);
    private Color startLowColor = new Color(170, 255, 170);
    
    private Color endHighColor =  new Color(240, 240, 240);
    private Color endLowColor = new Color(255, 170, 170);
    
    private Color signalHighColor =  new Color(240, 240, 240);
    private Color signalLowColor = new Color(170, 170, 170);
    
    private Color gatewayHighColor =  new Color(240, 240, 240);
    private Color gatewayLowColor = new Color(210, 245, 170);
    
    private Color selectedHighColor =  new Color(240, 240, 240);
    private Color selectedLowColor = new Color(220, 220, 220);
    
    private Color signHighColor = new Color(170, 170, 170);
    private Color signLowColor = new Color(120, 120, 120);

    private Font textFont = new Font("Lucida Sans", Font.BOLD, 14);    
    
    private Color nodeBorderColor = Color.GRAY;
    
    private boolean shadowsEnabled = false;
    private boolean borderGlowEnabled = false;
    private boolean gradientFillEnabled = true;

    private LayoutType layoutType = LayoutType.FREE;
    private EdgeType edgeType = EdgeType.ANGLED;

    public enum LayoutType {FREE};
    
    public T8StateGraphConfiguration()
    {
    }

    public boolean isShadowsEnabled()
    {
        return shadowsEnabled;
    }

    public void setShadowsEnabled(boolean shadowsEnabled)
    {
        this.shadowsEnabled = shadowsEnabled;
    }

    public boolean isBorderGlowEnabled()
    {
        return borderGlowEnabled;
    }

    public void setBorderGlowEnabled(boolean borderGlowEnabled)
    {
        this.borderGlowEnabled = borderGlowEnabled;
    }

    public boolean isGradientFillEnabled()
    {
        return gradientFillEnabled;
    }

    public void setGradientFillEnabled(boolean gradientFillEnabled)
    {
        this.gradientFillEnabled = gradientFillEnabled;
    }

    public Color getSwimlaneBackgroundColor()
    {
        return swimlaneBackgroundColor;
    }

    public void setSwimlaneBackgroundColor(Color swimlaneBackgroundColor)
    {
        this.swimlaneBackgroundColor = swimlaneBackgroundColor;
    }

    public Color getSwimlaneTextColor()
    {
        return swimlaneTextColor;
    }

    public void setSwimlaneTextColor(Color swinglangeTextColor)
    {
        this.swimlaneTextColor = swinglangeTextColor;
    }

    public Color getSwimlangeBorderColor()
    {
        return swimlangeBorderColor;
    }

    public void setSwimlangeBorderColor(Color swimlangeBorderColor)
    {
        this.swimlangeBorderColor = swimlangeBorderColor;
    }

    public Color getActivityHighColor()
    {
        return activityHighColor;
    }

    public void setActivityHighColor(Color activityHighColor)
    {
        this.activityHighColor = activityHighColor;
    }

    public Color getActivityLowColor()
    {
        return activityLowColor;
    }

    public void setActivityLowColor(Color activityLowColor)
    {
        this.activityLowColor = activityLowColor;
    }

    public Color getStartHighColor()
    {
        return startHighColor;
    }

    public void setStartHighColor(Color startHighColor)
    {
        this.startHighColor = startHighColor;
    }

    public Color getStartLowColor()
    {
        return startLowColor;
    }

    public void setStartLowColor(Color startLowColor)
    {
        this.startLowColor = startLowColor;
    }

    public Color getEndHighColor()
    {
        return endHighColor;
    }

    public void setEndHighColor(Color endHighColor)
    {
        this.endHighColor = endHighColor;
    }

    public Color getEndLowColor()
    {
        return endLowColor;
    }

    public void setEndLowColor(Color endLowColor)
    {
        this.endLowColor = endLowColor;
    }

    public Color getSignalHighColor()
    {
        return signalHighColor;
    }

    public void setSignalHighColor(Color signalHighColor)
    {
        this.signalHighColor = signalHighColor;
    }

    public Color getSignalLowColor()
    {
        return signalLowColor;
    }

    public void setSignalLowColor(Color signalLowColor)
    {
        this.signalLowColor = signalLowColor;
    }

    public Color getGatewayHighColor()
    {
        return gatewayHighColor;
    }

    public void setGatewayHighColor(Color gatewayHighColor)
    {
        this.gatewayHighColor = gatewayHighColor;
    }

    public Color getGatewayLowColor()
    {
        return gatewayLowColor;
    }

    public void setGatewayLowColor(Color gatewayLowColor)
    {
        this.gatewayLowColor = gatewayLowColor;
    }

    public Color getSelectedHighColor()
    {
        return selectedHighColor;
    }

    public void setSelectedHighColor(Color selectedHighColor)
    {
        this.selectedHighColor = selectedHighColor;
    }

    public Color getSelectedLowColor()
    {
        return selectedLowColor;
    }

    public void setSelectedLowColor(Color selectedLowColor)
    {
        this.selectedLowColor = selectedLowColor;
    }

    public Color getSignHighColor()
    {
        return signHighColor;
    }

    public void setSignHighColor(Color signHighColor)
    {
        this.signHighColor = signHighColor;
    }

    public Color getSignLowColor()
    {
        return signLowColor;
    }

    public void setSignLowColor(Color signLowColor)
    {
        this.signLowColor = signLowColor;
    }

    public Font getTextFont()
    {
        return textFont;
    }

    public void setTextFont(Font textFont)
    {
        this.textFont = textFont;
    }

    public Color getNodeBorderColor()
    {
        return nodeBorderColor;
    }

    public void setNodeBorderColor(Color nodeBorderColor)
    {
        this.nodeBorderColor = nodeBorderColor;
    }

    public EdgeType getEdgeType()
    {
        return edgeType;
    }

    public void setEdgeType(EdgeType edgeType)
    {
        this.edgeType = edgeType;
    }

    public LayoutType getLayoutType()
    {
        return layoutType;
    }

    public void setLayoutType(LayoutType layoutType)
    {
        this.layoutType = layoutType;
    }
}
