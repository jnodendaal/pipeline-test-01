package com.pilog.t8.developer.definitions.selectiontreeview;

import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionSelectionTree extends JXTreeTable
{
    private final T8DefinitionMetaTreeModel model;
    private final CheckBoxNodeEditor treeCellEditor;
    private final CheckBoxNodeEditor treeCellRenderer;
    private final String rootTitle;

    public T8DefinitionSelectionTree()
    {
        this.model = new T8DefinitionMetaTreeModel(null);
        this.setTreeTableModel(model);
        this.setAutoResizeMode(JXTreeTable.AUTO_RESIZE_OFF);
        this.treeCellEditor = new CheckBoxNodeEditor(this);
        this.treeCellRenderer = new CheckBoxNodeEditor(this);
        this.setCellEditor(treeCellEditor);
        this.setTreeCellRenderer(new T8DefinitionMetaTreeCellRenderer());
        this.setEditable(true);
        this.rootTitle = "Definitions";
    }

    public int getNodeCount()
    {
        return model.getAllNodes().size();
    }

    public List<T8DefinitionMetaData> getDefinitionMetaData()
    {
        List<T8DefinitionMetaData> metaData;

        metaData = new ArrayList<T8DefinitionMetaData>();
        for (MutableTreeTableNode node : model.getAllNodes())
        {
            Object userObject;

            userObject = node.getUserObject();
            if (userObject instanceof T8DefinitionMetaData)
            {
                metaData.add((T8DefinitionMetaData)userObject);
            }
        }

        return metaData;
    }

    public List<T8DefinitionHandle> getCheckedDefinitionHandles()
    {
        List<T8DefinitionHandle> checkedHandles;

        checkedHandles = new ArrayList<T8DefinitionHandle>();
        for (T8DefinitionMetaTreeNode node : model.getAllNodes())
        {
            if (node.isSelected())
            {
                LinkedList<String> path;

                path = node.getPath();
                if (path.size() == 5)
                {
                    T8DefinitionHandle definitionHandle;
                    String projectIdentifier;
                    String groupIdentifier;
                    String typeIdentifier;
                    String identifier;

                    projectIdentifier = path.get(1);
                    groupIdentifier = path.get(2);
                    typeIdentifier = path.get(3);
                    identifier = path.get(4);
                    definitionHandle = new T8DefinitionHandle(null, identifier, projectIdentifier);
                    definitionHandle.setTypeIdentifier(typeIdentifier);
                    definitionHandle.setGroupIdentifier(groupIdentifier);
                    checkedHandles.add(definitionHandle);
                }
            }
        }

        return checkedHandles;
    }

    public List<T8DefinitionMetaData> getCheckedDefinitionMetaData()
    {
        List<T8DefinitionMetaData> checkedMetaData;

        checkedMetaData = new ArrayList<T8DefinitionMetaData>();
        for (T8DefinitionMetaTreeNode node : model.getAllNodes())
        {
            if (node.isSelected())
            {
                T8DefinitionMetaData metaData;

                metaData = node.getMetaData();
                if (metaData != null) checkedMetaData.add(metaData);
            }
        }

        return checkedMetaData;
    }

    public void setDefinitionMetaData(List<T8DefinitionMetaData> definitionMetaDataList)
    {
        if (definitionMetaDataList != null)
        {
            for (T8DefinitionMetaData definitionMetaData : definitionMetaDataList)
            {
                addDefinitionMetaData(definitionMetaData);
            }
        }
    }

    private LinkedList<String> getPath(T8DefinitionMetaData definitionMetaData)
    {
        LinkedList<String> path;

        // Create the required nodes.
        path = new LinkedList<String>();
        path.add(rootTitle);
        path.add(definitionMetaData.getProjectId());
        path.add(definitionMetaData.getGroupId());
        path.add(definitionMetaData.getTypeId());
        path.add(definitionMetaData.getId());
        return path;
    }

    public void addDefinitionMetaData(T8DefinitionMetaData definitionMetaData)
    {
        MutableTreeTableNode rootNode;
        LinkedList<String> path;

        // Create the required nodes.
        path = getPath(definitionMetaData);
        model.createPath(path);
        model.setMetaData(path, definitionMetaData);

        // Make sure the root is expanded.
        rootNode = model.getRootNode();
        if (rootNode != null) expandPath(new TreePath(model.getPathToRoot(rootNode)));
    }

    public boolean removeDefinitionMetaData(T8DefinitionMetaData definitionMetaData)
    {
        LinkedList<String> path;

        // Create the required nodes.
        path = getPath(definitionMetaData);
        return model.removeNode(path);
    }

    @Override
    public TableCellEditor getCellEditor(int row, int column)
    {
        if (column == 0)
        {
            return treeCellEditor;
        }
        else return super.getCellEditor(row, column);
    }

    @Override
    public TableCellRenderer getCellRenderer(int row, int column)
    {
        if (column == 0)
        {
            return treeCellRenderer;
        }
        else return super.getCellRenderer(row, column);
    }
}
