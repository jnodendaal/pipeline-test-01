package com.pilog.t8.developer.definitions.actionhandlers.datarecord.access;

import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Gavin Boshoff
 */
public class DataRecordAccessTreeNode extends DataRecordAccessNode
{
    private static final T8Logger LOGGER = T8Log.getLogger(DataRecordAccessTreeNode.class);

    public DataRecordAccessTreeNode()
    {
        super(null);
    }

    public DataRecordAccessTreeNode(boolean loadFields)
    {
        super(null, loadFields);
    }

    public DataRecordAccessTreeNode(boolean loadFields, TerminologyProvider terminologyProvider)
    {
        super(null, loadFields, terminologyProvider);
    }

    @Override
    public DataRequirementInstance getDataRequirementInstance()
    {
        return null;
    }

    @Override
    public String getConceptID()
    {
        return null;
    }

    @Override
    public void loadChildren(T8Context context, T8OntologyClientApi ontApi, T8DataRequirementClientApi drqApi)
    {
        if (!isChildrenLoaded())
        {
            T8DataFilter filter;

            filter = new T8DataFilter(DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER);
            filter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER + EF_INDEPENDENT_INDICATOR, T8DataFilterCriterion.DataFilterOperator.EQUAL, "Y", true);

            try
            {
                for (T8DataEntity entity : T8DataManagerOperationHandler.selectDataEntities(context, filter.getEntityIdentifier(), filter))
                {
                    String drInstanceId;
                    DataRequirementInstance instance;

                    drInstanceId = (String)entity.getFieldValue(DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER + EF_DR_INSTANCE_ID);
                    instance = drqApi.retrieveDataRequirementInstance(drInstanceId, context.getLanguageId(), true, true);
                    addChild(new DataRecordAccessInstanceNode(instance, this));
                }

                sortChildren();
                setChildrenLoaded(true);
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to load root independent data requirements", ex);
            }
        }
    }

    @Override
    public String getDocPathRepresentation(String suffix)
    {
        return "";
    }
}
