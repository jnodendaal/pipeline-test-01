package com.pilog.t8.developer.monitor.system;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.server.monitor.T8ServerMonitorReport;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.definition.system.monitor.T8ServerMonitorAPIHandler;
import com.pilog.t8.developer.view.T8DefaultDeveloperView;
import com.pilog.t8.security.T8Context;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.swing.SwingWorker;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.DynamicTimeSeriesCollection;
import org.jfree.data.time.Second;

/**
 *
 * @author Hennie Brink
 */
public class T8SystemMonitor extends T8DefaultDeveloperView
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DeveloperView parentView;
    private JFreeChart sessionChart;
    private JFreeChart dataSessionChart;
    private JFreeChart opendTransactionChart;
    private JFreeChart operationsChart;
    private JFreeChart flowsChart;
    private JFreeChart communicationsChart;
    private JFreeChart javaMemoryChart;

    private ReportQueryTask queryTimer;

    public T8SystemMonitor(T8Context context, T8DeveloperView parentView)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.parentView = parentView;

        initComponents();

        setLayout(new GridBagLayout());

        createGraphs();

        startMonitor();
    }

    private void createGraphs()
    {
        sessionChart = createChart("Active Sessions");
        dataSessionChart = createChart("Active Data Sessions");
        opendTransactionChart = createChart("Open Transactions");
        operationsChart = createChart("Running Operations");
        flowsChart = createChart("Cached Flows");
        communicationsChart = createChart("Queued Communications");
        javaMemoryChart = createChart("Memory Usage");

        GridBagConstraints constraints;

        constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridheight = 1;
        constraints.gridwidth = 1;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(0, 0, 0, 0);
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.weightx = 0.1;
        constraints.weighty = 0.1;

        add(new ChartPanel(sessionChart, true), constraints);
        constraints.gridx = 1;
        add(new ChartPanel(dataSessionChart, true), constraints);
        constraints.gridx = 2;
        add(new ChartPanel(opendTransactionChart, true), constraints);
        constraints.gridx = 3;
        add(new ChartPanel(flowsChart, true), constraints);
        constraints.gridx = 0;
        constraints.gridy = 1;
        add(new ChartPanel(javaMemoryChart, true), constraints);
        constraints.gridx = 1;
        add(new ChartPanel(operationsChart, true), constraints);
        constraints.gridx = 2;
        add(new ChartPanel(communicationsChart, true), constraints);
    }

    private JFreeChart createChart(String title)
    {
        DynamicTimeSeriesCollection dataset;
        ValueAxis domain;
        ValueAxis range;
        JFreeChart result;
        XYPlot plot;

        dataset = new DynamicTimeSeriesCollection(1, 1000, new Second());
        dataset.setTimeBase(new Second(Calendar.getInstance().getTime()));
        dataset.addSeries(new float[]{}, 0, "Measured Data");
        result = ChartFactory.createTimeSeriesChart(title, "hh:mm:ss", null, dataset, false, true, false);
        plot = result.getXYPlot();
        domain = plot.getDomainAxis();
        domain.setAutoRange(true);
        range = plot.getRangeAxis();
        range.setAutoRange(true);
        return result;
    }

    private void startMonitor()
    {
        queryTimer = new ReportQueryTask();
        queryTimer.execute();
    }

    private void stopMonitor()
    {
        if(queryTimer != null) queryTimer.cancel(false);
    }

    @Override
    public String getHeader()
    {
        return "System Monitor";
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return parentView.getViewFactory();
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        parentView.setViewFactory(viewFactory);
    }

    @Override
    public void addView(T8DeveloperView view)
    {
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
    }

    @Override
    public void setSelectedDefinition(T8Definition definition)
    {
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    @Override
    public boolean canClose()
    {
        stopMonitor();
        return true;
    }

    private class ReportQueryTask extends SwingWorker<Void, T8ServerMonitorReport>
    {

        @Override
        protected Void doInBackground() throws Exception
        {
            while(!isCancelled())
            {
                T8ServerMonitorReport report;

                try
                {
                    report = (T8ServerMonitorReport)T8MainServerClient.executeSynchronousOperation(context, T8ServerMonitorAPIHandler.OPERATION_GET_SERVER_MONITOR_REPORT, null).get(T8ServerMonitorAPIHandler.PARAMETER_REPORT);
                    publish(report);
                }
                catch(Exception ex)
                {
                    T8Log.log("Failed to retrieve monitor report", ex);
                }
                Thread.sleep(TimeUnit.SECONDS.toMillis(5));
            }

            return null;
        }

        @Override
        protected void process(List<T8ServerMonitorReport> chunks)
        {
            while(!chunks.isEmpty())
            {
                T8ServerMonitorReport report;

                report = chunks.remove(0);

                appendChartData(sessionChart, report.getTotalActiveSessions());
                appendChartData(dataSessionChart, report.getTotalActiveDataSessions());
                appendChartData(opendTransactionChart, report.getTotalActiveDataTransactions());
                appendChartData(javaMemoryChart, (report.getJavaTotalMemory() - report.getJavaFreeMemory()) / 1024 / 1024);
                appendChartData(operationsChart, report.getTotalRunningOperations());
                appendChartData(flowsChart, report.getTotalCachedFlows());
                appendChartData(communicationsChart, report.getTotalQueuedCommunications());

            }
        }

        private void appendChartData(JFreeChart chart, float... values)
        {
            DynamicTimeSeriesCollection dataSet;

            dataSet = (DynamicTimeSeriesCollection)chart.getXYPlot().getDataset();
            dataSet.advanceTime();
            dataSet.appendData(values);
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 864, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 425, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
