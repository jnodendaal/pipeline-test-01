package com.pilog.t8.developer.definitions.event;

import com.pilog.t8.flow.T8FlowStatus;
import java.awt.Component;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class T8FlowStatusSelectionEvent extends EventObject
{
    private T8FlowStatus newSelection;

    public T8FlowStatusSelectionEvent(Component source, T8FlowStatus newSelection)
    {
        super(source);
        this.newSelection = newSelection;
    }

    public T8FlowStatus getNewSelection()
    {
        return newSelection;
    }
}
