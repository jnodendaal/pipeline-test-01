package com.pilog.t8.developer.definitions.graphview;

import com.pilog.graph.model.DefaultGraphModel;
import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.view.Graph;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionComparator;
import com.pilog.t8.definition.T8DefinitionConstructorParameters;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.ui.T8GraphManager;
import com.pilog.t8.definition.graph.T8GraphDefinition;
import com.pilog.t8.definition.graph.T8GraphNodeDefinition;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.scrollpopupmenu.JScrollMenu;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionGraph extends Graph
{
    private final T8DefinitionContext definitionContext;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private T8GraphDefinition graphDefinition;
    private T8GraphManager graphManager;

    public T8DefinitionGraph(T8DefinitionContext definitionContext, T8GraphDefinition graphDefinition)
    {
        super();
        this.clientContext = definitionContext.getClientContext();
        this.context = definitionContext.getContext();
        this.definitionContext = definitionContext;
        this.definitionManager = clientContext.getDefinitionManager();
        this.addMouseListener(new GraphMouseListener());
        this.addKeyListener(new GraphKeyListener());
        setGraphDefinition(graphDefinition);
    }

    public void setGraphDefinition(T8GraphDefinition definition)
    {
        if (definition != null)
        {
            try
            {
                this.graphDefinition = definition;
                this.graphManager = graphDefinition.getGraphManager(context);
                setVertexRenderer(graphManager.getVertextRenderer());
                rebuildModel();
                setEditable(!graphDefinition.isLocked());
            }
            catch (Exception e)
            {
                T8Log.log("Exception while setting graph definition: " + definition, e);
                graphDefinition = null;
                graphManager = null;
                rebuildModel();
            }
        }
        else
        {
            graphDefinition = null;
            graphManager = null;
            rebuildModel();
        }
    }

    public final void rebuildModel()
    {
        if (graphManager != null)
        {
            setModel(graphManager.buildGraphModel(graphDefinition));
            doGraphLayout();
        }
        else
        {
            setModel(new DefaultGraphModel());
            doGraphLayout();
        }
    }

    public T8GraphDefinition getGraphDefinition()
    {
        return graphDefinition;
    }

    public void addDefinition(T8GraphNodeDefinition newDefinition)
    {
        graphDefinition.addNodeDefinition(newDefinition);
        rebuildModel();
    }

    public void addDefinition(GraphVertex selectedVertex, T8GraphNodeDefinition newDefinition)
    {
        T8Definition selectedDefinition;

        selectedDefinition = (T8Definition)selectedVertex.getVertexDataObject();

        graphDefinition.addNodeDefinition(newDefinition);
        graphDefinition.addEdgeDefinition(selectedDefinition.getIdentifier(), newDefinition.getIdentifier());
        rebuildModel();
    }

    public void connectDefinitions(T8Definition parentDefinition, T8Definition childDefinition)
    {
        graphDefinition.addEdgeDefinition(parentDefinition.getIdentifier(), childDefinition.getIdentifier());
        rebuildModel();
    }

    public void disconnectDefinitions(T8Definition parentDefinition, T8Definition childDefinition)
    {
        graphDefinition.removeEdgeDefinitions(parentDefinition.getIdentifier(), childDefinition.getIdentifier(), false);
        rebuildModel();
    }

    public void deleteVertex(GraphVertex vertex)
    {
        T8Definition definition;

        definition = (T8Definition)vertex.getVertexDataObject();
        graphDefinition.removeEdgeDefinitions(definition.getIdentifier());
        graphDefinition.removeNodeDefinition(definition.getIdentifier());
        rebuildModel();
    }

    public void renameDefinition(T8Definition definition)
    {
        if (definition != null)
        {
            T8DefinitionMetaData metaData;

            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this, definition.getTypeMetaData(), definition.getParentDefinition());
            if (metaData != null)
            {
                try
                {
                    definitionManager.renameDefinition(context, definition, metaData.getId());
                    doGraphLayout();
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while renaming a copied definition.", e);
                }
            }
        }
    }

    private void displayPopupMenu(int x, int y)
    {
        ArrayList<GraphVertex> selectedVertices;

        selectedVertices = getVerticesAtComponentLocation(x, y);
        if (!selectedVertices.isEmpty()) // No Vertex right-clicked.
        {
            GraphVertex selectedVertex;

            //setSelectionPath(treePath);
            selectedVertex = selectedVertices.get(0);
            for (GraphVertex vertex : selectedVertices)
            {
                if (vertex.getVertexDataObject() instanceof T8Definition)
                {
                    selectedVertex = vertex;
                    break;
                }
            }

            // If one of the selected vertices actually has a definition data object, then display the popup menu.
            if (selectedVertex != null)
            {
                selectVertex(selectedVertex, true);
                displayVertextPopupMenu(selectedVertex, x, y);
            }
        }
        else displayGraphPopupMenu(x, y);
    }

    private void displayGraphPopupMenu(int x, int y)
    {
        JPopupMenu treePopupMenu;
        JMenuItem menuItem;

        // Create the ActionListener for the menu items.
        ActionListener menuListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                NavigatorMenuItem menuItem;
                String code;

                menuItem = (NavigatorMenuItem)event.getSource();
                code = menuItem.getCode();
                if (code.equals("ADD_VERTEX_DEFINITION"))
                {
                    T8DefinitionConstructorParameters constructorParameters;
                    T8DefinitionDatumOption datumOption;
                    String definitionTypeIdentifier;
                    T8DefinitionMetaData metaData;

                    datumOption = (T8DefinitionDatumOption)menuItem.getDataObject();
                    constructorParameters = (T8DefinitionConstructorParameters)datumOption.getValue();
                    definitionTypeIdentifier = constructorParameters.getTypeIdentifier();

                    try
                    {
                        metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, T8DefinitionGraph.this, definitionManager.getDefinitionTypeMetaData(definitionTypeIdentifier), graphDefinition);
                        if (metaData != null)
                        {
                            try
                            {
                                ArrayList<Object> parameters;

                                parameters = constructorParameters.getParameters();
                                if (parameters == null)
                                {
                                    T8GraphNodeDefinition newDefinition;

                                    newDefinition = (T8GraphNodeDefinition)definitionManager.createNewDefinition(metaData.getId(), definitionTypeIdentifier);
                                    newDefinition.setProjectIdentifier(metaData.getProjectId());
                                    newDefinition.setXCoordinate(x);
                                    newDefinition.setYCoordinate(y);
                                    addDefinition(newDefinition);
                                }
                                else
                                {
                                    T8GraphNodeDefinition newDefinition;

                                    newDefinition = (T8GraphNodeDefinition)definitionManager.createNewDefinition(metaData.getId(), definitionTypeIdentifier, parameters);
                                    newDefinition.setProjectIdentifier(metaData.getProjectId());
                                    newDefinition.setXCoordinate(x);
                                    newDefinition.setYCoordinate(y);
                                    addDefinition(newDefinition);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        T8Log.log("Exception while fetching meta data for type: " + definitionTypeIdentifier, e);
                    }
                }
            }
        };

        // Create a new popup menu.
        treePopupMenu = new JPopupMenu();

        // Create the add definition sub-menu.
        try
        {

            JMenu subMenu;

            subMenu = new JMenu("Add... ");
            for (T8DefinitionDatumOption datumOption : graphDefinition.getNodeDatumOptions(definitionContext))
            {
                menuItem = new NavigatorMenuItem(datumOption.getDisplayName(), "ADD_VERTEX_DEFINITION", null, datumOption);
                menuItem.addActionListener(menuListener);
                subMenu.add(menuItem);
            }

            treePopupMenu.add(subMenu);
        }
        catch (Exception e)
        {
            T8Log.log("Vertex Definition datum options could not be resolved successfully.", e);
        }

        // Display the newly constructed popup menu.
        treePopupMenu.show(this, x, y);
    }

    private void displayVertextPopupMenu(final GraphVertex selectedVertex, int x, int y)
    {
        final T8GraphNodeDefinition selectedNodeDefinition;
        Object vertexData;

        vertexData = selectedVertex.getVertexDataObject();
        selectedNodeDefinition = (vertexData instanceof T8GraphNodeDefinition ? (T8GraphNodeDefinition)vertexData : null);
        if (selectedNodeDefinition != null)
        {
            JPopupMenu treePopupMenu;
            JMenuItem menuItem;

            // Create the ActionListener for the menu items.
            ActionListener menuListener = new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    NavigatorMenuItem menuItem;
                    String code;

                    menuItem = (NavigatorMenuItem)event.getSource();
                    code = menuItem.getCode();
                    if (code.equals("ADD_VERTEX_DEFINITION"))
                    {
                        T8DefinitionConstructorParameters constructorParameters;
                        T8DefinitionDatumOption datumOption;
                        String definitionTypeIdentifier;
                        T8DefinitionMetaData metaData;

                        datumOption = (T8DefinitionDatumOption)menuItem.getDataObject();
                        constructorParameters = (T8DefinitionConstructorParameters)datumOption.getValue();
                        definitionTypeIdentifier = constructorParameters.getTypeIdentifier();

                        try
                        {
                            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, T8DefinitionGraph.this, definitionManager.getDefinitionTypeMetaData(definitionTypeIdentifier), graphDefinition);
                            if (metaData != null)
                            {
                                try
                                {
                                    ArrayList<Object> parameters;

                                    parameters = constructorParameters.getParameters();
                                    if (parameters == null)
                                    {
                                        T8GraphNodeDefinition newDefinition;

                                        newDefinition = (T8GraphNodeDefinition)definitionManager.createNewDefinition(metaData.getId(), definitionTypeIdentifier);
                                        newDefinition.setProjectIdentifier(metaData.getProjectId());
                                        newDefinition.setXCoordinate(selectedNodeDefinition.getXCoordinate() + 100);
                                        newDefinition.setYCoordinate(selectedNodeDefinition.getYCoordinate() + 100);
                                        addDefinition(selectedVertex, newDefinition);
                                    }
                                    else
                                    {
                                        T8GraphNodeDefinition newDefinition;

                                        newDefinition = (T8GraphNodeDefinition)definitionManager.createNewDefinition(metaData.getId(), definitionTypeIdentifier, parameters);
                                        newDefinition.setProjectIdentifier(metaData.getProjectId());
                                        newDefinition.setXCoordinate(selectedNodeDefinition.getXCoordinate() + 100);
                                        newDefinition.setYCoordinate(selectedNodeDefinition.getYCoordinate() + 100);
                                        addDefinition(selectedVertex, newDefinition);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ex.printStackTrace();
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            T8Log.log("Exception while fetching meta data for type: " + definitionTypeIdentifier, e);
                        }
                    }
                    else if (code.equals("CONNECT_DEFINITION"))
                    {
                        String definitionIdentifier;
                        T8Definition targetDefinition;

                        definitionIdentifier = (String)menuItem.getDataObject();
                        targetDefinition = graphDefinition.getNodeDefinition(definitionIdentifier);
                        connectDefinitions(selectedNodeDefinition, targetDefinition);
                    }
                    else if (code.equals("DISCONNECT_DEFINITION_FROM_PARENT"))
                    {
                        String definitionIdentifier;
                        T8Definition targetDefinition;

                        definitionIdentifier = (String)menuItem.getDataObject();
                        targetDefinition = graphDefinition.getNodeDefinition(definitionIdentifier);
                        disconnectDefinitions(targetDefinition, selectedNodeDefinition);
                    }
                    else if (code.equals("DISCONNECT_DEFINITION_FROM_CHILD"))
                    {
                        String definitionIdentifier;
                        T8Definition targetDefinition;

                        definitionIdentifier = (String)menuItem.getDataObject();
                        targetDefinition = graphDefinition.getNodeDefinition(definitionIdentifier);
                        disconnectDefinitions(selectedNodeDefinition, targetDefinition);
                    }
                    else if (code.equals("COPY_DEFINITION"))
                    {
                    }
                    else if (code.equals("DELETE_DEFINITION"))
                    {
                        deleteVertex(selectedVertex);
                    }
                    else if (code.equals("RENAME_DEFINITION"))
                    {
                        renameDefinition(selectedNodeDefinition);
                    }
                }
            };

            // Create a new popup menu.
            treePopupMenu = new JPopupMenu();

            // Create the add definition sub-menu.
            if (isEditable())
            {
                try
                {

                    JMenu subMenu;

                    subMenu = new JMenu("Add... ");
                    for (T8DefinitionDatumOption datumOption : graphDefinition.getNodeDatumOptions(definitionContext))
                    {
                        menuItem = new NavigatorMenuItem(datumOption.getDisplayName(), "ADD_VERTEX_DEFINITION", null, datumOption);
                        menuItem.addActionListener(menuListener);
                        subMenu.add(menuItem);
                    }

                    treePopupMenu.add(subMenu);
                }
                catch (Exception e)
                {
                    T8Log.log("Vertex Definition datum options could not be resolved successfully.", e);
                }

                // Create the add connection sub-menu.
                try
                {
                    JMenu subMenu;
                    ArrayList<T8GraphNodeDefinition> vertexDefinitions;

                    vertexDefinitions = graphDefinition.getNodeDefinitions();
                    subMenu = new JScrollMenu("Connect to...");
                    Collections.sort(vertexDefinitions, new T8DefinitionComparator(T8DefinitionComparator.CompareMethod.LEXICOGRAPHIC, T8DefinitionComparator.CompareType.IDENTIFIER));
                    for (T8Definition nodeDefinition : vertexDefinitions)
                    {
                        //Nodes may not be connected to themselves
                        if(!nodeDefinition.getIdentifier().equals(selectedNodeDefinition.getIdentifier()))
                        {
                            menuItem = new NavigatorMenuItem(nodeDefinition.getIdentifier(), "CONNECT_DEFINITION", null, nodeDefinition.getIdentifier());
                            menuItem.addActionListener(menuListener);
                            subMenu.add(menuItem);
                        }
                    }

                    treePopupMenu.add(subMenu);
                }
                catch (Exception e)
                {
                    T8Log.log("Node Definitions could not be resolved successfully.", e);
                }

                // Create the add disconnection sub-menu.
                try
                {
                    JMenu subMenu;
                    ArrayList<String> nodeIdentifiers;

                    subMenu = new JMenu("Disconnect from...");

                    // Add all the node's children to the menu.
                    nodeIdentifiers = graphDefinition.getChildNodeIdentifiers(selectedNodeDefinition.getIdentifier());
                    for (String nodeIdentifier : nodeIdentifiers)
                    {
                        menuItem = new NavigatorMenuItem("Output: " + nodeIdentifier, "DISCONNECT_DEFINITION_FROM_CHILD", null, nodeIdentifier);
                        menuItem.addActionListener(menuListener);
                        subMenu.add(menuItem);
                    }

                    // Add all the node's parents to the menu.
                    nodeIdentifiers = graphDefinition.getParentNodeIdentifiers(selectedNodeDefinition.getIdentifier());
                    for (String nodeIdentifier : nodeIdentifiers)
                    {
                        menuItem = new NavigatorMenuItem("Input: " + nodeIdentifier, "DISCONNECT_DEFINITION_FROM_PARENT", null, nodeIdentifier);
                        menuItem.addActionListener(menuListener);
                        subMenu.add(menuItem);
                    }

                    treePopupMenu.add(subMenu);
                }
                catch (Exception e)
                {
                    T8Log.log("Node Definitions could not be resolved successfully.", e);
                }

                menuItem = new NavigatorMenuItem("Rename Definition", "RENAME_DEFINITION", null, null);
                menuItem.addActionListener(menuListener);
                treePopupMenu.add(menuItem);


                menuItem = new NavigatorMenuItem("Delete Definition", "DELETE_DEFINITION", null, null);
                menuItem.addActionListener(menuListener);
                treePopupMenu.add(menuItem);
            }

            menuItem = new NavigatorMenuItem("Refresh Definition", "REFRESH_DEFINITION", null, null);
            menuItem.addActionListener(menuListener);
            treePopupMenu.add(menuItem);

            menuItem = new NavigatorMenuItem("Copy Definition", "COPY_DEFINITION", null, null);
            menuItem.addActionListener(menuListener);
            treePopupMenu.add(menuItem);

            // Display the newly constructed popup menu.
            treePopupMenu.show(this, x, y);
        }
    }

    public T8GraphManager getGraphManager()
    {
        return graphManager;
    }

    private class NavigatorMenuItem extends JMenuItem
    {
        private String code;
        private String datumIdentifier;
        private Object dataObject;

        public NavigatorMenuItem(String text, String code, String datumIdentifier, Object dataObject)
        {
            super(text);
            this.code = code;
            this.datumIdentifier = datumIdentifier;
            this.dataObject = dataObject;
        }

        public String getCode()
        {
            return code;
        }

        public String getDatumIdentifier()
        {
            return datumIdentifier;
        }

        public Object getDataObject()
        {
            return dataObject;
        }
    }

    private class GraphMouseListener implements MouseListener
    {
        private int pressedX;
        private int pressedY;

        @Override
        public void mouseClicked(MouseEvent e)
        {
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            pressedX = e.getX();
            pressedY = e.getY();
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                double moveSizeX;
                double moveSizeY;
                Dimension screenSize;


                screenSize = getToolkit().getScreenSize();
                moveSizeX = screenSize.getWidth() * 0.05;
                moveSizeY = screenSize.getHeight()* 0.05;

                if(e.getX() < (pressedX + moveSizeX) && e.getX() > (pressedX - moveSizeX)
                        && e.getY()< (pressedY + moveSizeY) && e.getY() > (pressedY - moveSizeY))
                {
                    displayPopupMenu(e.getX(), e.getY());
                }
            }
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    private class GraphKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
            if ((e.getKeyCode() == KeyEvent.VK_LEFT) || (e.getKeyCode() == KeyEvent.VK_RIGHT))
            {
                if (e.isControlDown())
                {
                    List<GraphVertex> selectedVertices;

                    selectedVertices = getSelectedVertices();
                    if (selectedVertices.size() > 0)
                    {
                        GraphVertex selectedVertex;
                        Object selectedObject;

                        selectedVertex = selectedVertices.get(0);
                        selectedObject = selectedVertex.getVertexDataObject();
                        if (selectedObject instanceof T8GraphNodeDefinition)
                        {
                            T8GraphNodeDefinition graphNodeDefinition;
                            int cellXCoordinate;

                            graphNodeDefinition = (T8GraphNodeDefinition)selectedObject;
                            cellXCoordinate = graphNodeDefinition.getXCoordinate();

                            // Make sure the coordinate is valid and if not, set it to the default
                            if (cellXCoordinate < 0)
                            {
                                cellXCoordinate = 1;
                            }

                            // Now adjust the coordinate according to the key pressed.
                            if ((e.getKeyCode() == KeyEvent.VK_LEFT) && (cellXCoordinate > 0))
                            {
                                cellXCoordinate--;
                                graphNodeDefinition.setXCoordinate(cellXCoordinate);
                                rebuildModel();
                            }
                            else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
                            {
                                cellXCoordinate++;
                                graphNodeDefinition.setXCoordinate(cellXCoordinate);
                                rebuildModel();
                            }
                        }
                    }
                }
            }
        }
    }
}
