package com.pilog.t8.developer.definitions.navigatorview;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.developer.definitions.navigatorview.T8DefinitionNavigatorTree.T8DefinitionNavigatorDatum;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import java.util.List;
import java.util.Objects;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionNavigatorTreeCellRenderer extends javax.swing.JPanel implements TreeCellRenderer
{
    private T8DefinitionManager definitionManager;
    private List<T8DefinitionValidationError> definitionValidationErrors;

    public T8DefinitionNavigatorTreeCellRenderer(T8DefinitionManager definitionFactory)
    {
        initComponents();
        this.definitionManager = definitionFactory;
    }

    public void setDefinitionValidationErrors(
                                              List<T8DefinitionValidationError> definitionValidationErrors)
    {
        this.definitionValidationErrors = definitionValidationErrors;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        DefaultMutableTreeNode treeNode;
        Object nodeObject;

        treeNode = (DefaultMutableTreeNode)value;
        nodeObject = treeNode.getUserObject();
        jLabelValidationMessage.setVisible(false);
        jLabelValidationMessage.setText("");
        if (nodeObject instanceof T8Definition)
        {
            T8DefinitionTypeMetaData typeMetaData;
            T8Definition definition;

            definition = (T8Definition)nodeObject;
            typeMetaData = definition.getTypeMetaData();

            jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.PROPERTY_BLUE);
            jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);
            jLabelNodeType.setText(typeMetaData.getDisplayName() + ":");
            jLabelNodeName.setText(definition.getDisplayIdentifier());

            if(definitionValidationErrors != null && !definitionValidationErrors.isEmpty())
            {
                for (T8DefinitionValidationError t8DefinitionValidationError : definitionValidationErrors)
                {
                    if(Objects.equals(t8DefinitionValidationError.getDefinitionIdentifier(), definition.getPublicIdentifier()))
                    {
                        jLabelValidationMessage.setVisible(true);
                        jLabelValidationMessage.setText(t8DefinitionValidationError.getMessage());
                        jPanelMain.setBackground(LAFConstants.INVALID_PINK);

                        break;
                    }
                }

                definitionLoop:
                for (T8Definition descendantDefinition : definition.getDescendentDefinitions())
                {
                    for (T8DefinitionValidationError t8DefinitionValidationError : definitionValidationErrors)
                    {
                        if(Objects.equals(t8DefinitionValidationError.getDefinitionIdentifier(), descendantDefinition.getPublicIdentifier()))
                        {
                            jPanelMain.setBackground(LAFConstants.INVALID_PINK);
                            break definitionLoop;
                        }
                    }
                }
            }

            validate();
            return this;
        }
        else
        {
            T8DefinitionNavigatorDatum navigatorDatum;
            T8DefinitionDatumType datumType;
            T8Definition definition;

            navigatorDatum = (T8DefinitionNavigatorDatum)nodeObject;
            definition = navigatorDatum.getDefinition();
            datumType = definition.getDatumType(navigatorDatum.getDatumIdentifier());

            jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : Color.WHITE);
            jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);

            if(navigatorDatum.isHidden())
            jLabelNodeType.setText(datumType.getDisplayName() + " (Hidden)");
            else jLabelNodeType.setText(datumType.getDisplayName());

            jLabelNodeName.setText(null);

            validate();
            return this;
        }
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    protected void firePropertyChange(String string, Object o, Object o1)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void firePropertyChange(String string, boolean bln, boolean bln1)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void firePropertyChange(String string, byte b, byte b1)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void firePropertyChange(String string, char c, char c1)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void firePropertyChange(String string, double d, double d1)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void firePropertyChange(String string, float f, float f1)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void firePropertyChange(String string, int i, int i1)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void firePropertyChange(String string, long l, long l1)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void firePropertyChange(String string, short s, short s1)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void repaint(Rectangle rctngl)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void repaint(long l, int i, int i1, int i2, int i3)
    {
    }

    /**
     * Overridden for performance reasons
     */
    @Override
    public void repaint()
    {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jLabelNodeType = new javax.swing.JLabel();
        jLabelNodeName = new javax.swing.JLabel();
        jLabelValidationMessage = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridBagLayout());

        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jLabelNodeType.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelNodeType.setText("Node Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelMain.add(jLabelNodeType, gridBagConstraints);

        jLabelNodeName.setText("Node Name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelMain.add(jLabelNodeName, gridBagConstraints);

        jLabelValidationMessage.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelMain.add(jLabelValidationMessage, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jPanelMain, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelNodeName;
    private javax.swing.JLabel jLabelNodeType;
    private javax.swing.JLabel jLabelValidationMessage;
    private javax.swing.JPanel jPanelMain;
    // End of variables declaration//GEN-END:variables
}
