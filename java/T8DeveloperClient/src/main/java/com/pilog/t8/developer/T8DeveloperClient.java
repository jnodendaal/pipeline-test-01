package com.pilog.t8.developer;

import com.pilog.t8.developer.definitions.T8DeveloperDefinitionContext;
import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.client.T8DefaultClientManagerFactory;
import com.pilog.t8.T8Client;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.T8ServiceManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionEditorFactory;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.system.T8ClientEnvironmentStatus;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.definition.graph.T8GraphDefinition;
import com.pilog.t8.definition.help.T8HelpDisplayDefinition;
import com.pilog.t8.definition.script.T8ScriptDefinition;
import com.pilog.t8.developer.definitions.dialog.delete.T8DefinitionDeletionDialog;
import com.pilog.t8.developer.definitions.dialog.load.T8DefinitionLoadingDialog;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import com.pilog.t8.developer.view.T8DefaultDeveloperViewFactory;
import com.pilog.t8.developer.view.T8TabbedDeveloperViewContainer;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.security.T8ClientSecurityManager;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8AsynchronousOperationTask;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.ui.notification.T8ClientNotificationCentre;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Window;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DeveloperClient extends javax.swing.JPanel implements T8Client
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DeveloperClient.class);

    private final T8Context context;
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8SecurityManager securityManager;
    private T8ConfigurationManager configurationManager;
    private T8CommunicationManager communicationManager;
    private T8NotificationManager notificationManager;
    private T8DefinitionManager definitionManager;
    private T8ProcessManager processManager;
    private T8ServiceManager serviceManager;
    private T8FlowManager flowManager;
    private T8FunctionalityManager functionalityManager;
    private T8FileManager fileManager;
    private T8DeveloperView rootView;
    private T8ClientEnvironmentStatus environmentStatus;
    private T8ClientNotificationCentre notificationCentre;
    private final T8DefaultComponentContainer viewContainer;

    public T8DeveloperClient(T8SessionContext sessionContext)
    {
        initComponents();
        this.sessionContext = sessionContext;
        this.clientContext = new T8ClientContext(this, sessionContext);
        this.context = new T8Context(clientContext, sessionContext);
        this.securityManager = new T8ClientSecurityManager(clientContext);
        this.viewContainer = new T8DefaultComponentContainer();
        add(viewContainer, BorderLayout.CENTER);
    }

    public T8Context getContext()
    {
        return context;
    }

    public T8ClientContext getClientContext()
    {
        return clientContext;
    }

    public T8DeveloperView getViewContainer()
    {
        return rootView;
    }

    public void initializeComponent()
    {
        try
        {
            T8DefaultClientManagerFactory factory;

            factory = new T8DefaultClientManagerFactory(context);
            definitionManager = factory.constructDefinitionManager();
            communicationManager = factory.constructCommunicationManager();
            notificationManager = factory.constructNotificationManager();
            processManager = factory.constructProcessManager();
            serviceManager = factory.constructServiceManager();
            fileManager = factory.constructFileManager();
            configurationManager = factory.constructConfigurationManager();
            flowManager = factory.constructFlowManager();
            functionalityManager = factory.constructFunctionalityManager();

            rootView = new T8TabbedDeveloperViewContainer(context, new T8DefaultDeveloperViewFactory(new T8DeveloperDefinitionContext(context)));
            viewContainer.setComponent((JComponent)rootView);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing developer client.", e);
        }
    }

    public String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    public void setNotificationCentre(T8ClientNotificationCentre notificationCentre)
    {
        this.notificationCentre = notificationCentre;
    }

    @Override
    public void refreshNotifications()
    {
        if (notificationCentre != null)
        {
            notificationCentre.refreshNotifications();
        }
    }

    @Override
    public Window getParentWindow()
    {
        return SwingUtilities.getWindowAncestor(this);
    }

    @Override
    public T8ProcessManager getProcessManager()
    {
        return processManager;
    }

    @Override
    public T8ServiceManager getServiceManager()
    {
        return serviceManager;
    }

    @Override
    public T8FlowManager getFlowManager()
    {
        return flowManager;
    }

    @Override
    public T8FunctionalityManager getFunctionalityManager()
    {
        return functionalityManager;
    }

    @Override
    public T8SecurityManager getSecurityManager()
    {
        return securityManager;
    }

    @Override
    public T8DefinitionManager getDefinitionManager()
    {
        return definitionManager;
    }

    @Override
    public T8ConfigurationManager getConfigurationManager()
    {
        return configurationManager;
    }

    @Override
    public T8CommunicationManager getCommunicationManager()
    {
        return communicationManager;
    }

    @Override
    public T8NotificationManager getNotificationManager()
    {
        return notificationManager;
    }

    @Override
    public T8FileManager getFileManager()
    {
        return fileManager;
    }

    @Override
    public void showAdministrationConsole()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        return new T8ClientExpressionEvaluator(context);
    }

    @Override
    public void dispayHelp(String helpId)
    {
        T8HelpDisplayDefinition helpDisplayDefinition;

        try
        {
            // Load and initialize the Module Definition.
            LOGGER.log("Displaying help using identifier: '" + helpId + "'");
            helpDisplayDefinition = (T8HelpDisplayDefinition)definitionManager.getInitializedDefinition(context, null, helpId, null);
            if (helpDisplayDefinition != null)
            {
                //Have the instance execute the required display intiative
                helpDisplayDefinition.getHelpDisplayInstance().display();
            }
            else throw new RuntimeException("Display Help Definition not found: " + helpId);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to display the help for identifier : " + helpId, ex);
        }
    }

    @Override
    public T8ClientEnvironmentStatus getEnvironmentStatus()
    {
        return this.environmentStatus;
    }

    void setEnvironmentStatus(T8ClientEnvironmentStatus environmentStatus)
    {
        this.environmentStatus = environmentStatus;
    }

    public void login()
    {
        T8DeveloperLoginDialog.login(context, this);
    }

    public void logout()
    {
        try
        {
            // Anonymous session is never actually logged in
            if (this.sessionContext.isAnonymousSession()) return;

            LOGGER.log("Loggin out session '" + sessionContext.getSessionIdentifier() + "'.");
            clientContext.getSecurityManager().logout(context);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while trying to logout from T8Developer.", e);
        }
    }

    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    public void reloadDefinitions()
    {
        try
        {
            definitionManager.loadDefinitionData(context);
            JOptionPane.showMessageDialog(this, "Definitions reloaded successfully.", "Definition Manager", JOptionPane.INFORMATION_MESSAGE);
        }
        catch (Exception e)
        {
            LOGGER.log(e);
            JOptionPane.showMessageDialog(this, "Definitions could not be reloaded successfully.", "Definition Manager", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void loadDefinitionNavigator(String groupIdentifier)
    {
        T8Definition newDefinition;

        newDefinition = T8DefinitionLoadingDialog.getDefinition((Frame)SwingUtilities.getAncestorOfClass(Frame.class, this), context, groupIdentifier);
        if (newDefinition != null)
        {
            rootView.addView(rootView.getViewFactory().createDefinitionNavigator(rootView, newDefinition));
        }
    }

    public void loadDefinitionGraph(String groupIdentifier)
    {
        T8Definition newDefinition;

        newDefinition = T8DefinitionLoadingDialog.getDefinition((Frame)SwingUtilities.getAncestorOfClass(Frame.class, this), context, groupIdentifier);
        if (newDefinition != null)
        {
            rootView.addView(rootView.getViewFactory().createDefinitionGraph(rootView, (T8GraphDefinition)newDefinition));
        }
    }

    public void deleteDefinition(String groupIdentifier)
    {
        T8DefinitionDeletionDialog.showDefinitionDeletionDialog((Frame)SwingUtilities.getAncestorOfClass(Frame.class, this), context, groupIdentifier);
    }

    public void createDefinitionGraph(String definitionTypeIdentifier)
    {
        try
        {
            T8DefinitionMetaData metaData;

            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(new T8DeveloperDefinitionContext(context), this.getRootPane(), definitionManager.getDefinitionTypeMetaData(definitionTypeIdentifier), null);
            if (metaData != null)
            {
                T8Definition newDefinition;

                newDefinition = definitionManager.createNewDefinition(metaData.getId(), definitionTypeIdentifier);
                newDefinition.setProjectIdentifier(metaData.getProjectId());
                rootView.addView(rootView.getViewFactory().createDefinitionGraph(rootView, (T8GraphDefinition)newDefinition));
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while creating new definition of type: " + definitionTypeIdentifier, e);
        }
    }

    public void createDefinitionNavigator(String definitionTypeIdentifier)
    {
        try
        {
            T8DefinitionMetaData metaData;

            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(new T8DeveloperDefinitionContext(context), this.getRootPane(), definitionManager.getDefinitionTypeMetaData(definitionTypeIdentifier), null);
            if (metaData != null)
            {
                T8Definition newDefinition;

                newDefinition = definitionManager.createNewDefinition(metaData.getId(), definitionTypeIdentifier);
                newDefinition.setProjectIdentifier(metaData.getProjectId());
                rootView.addView(rootView.getViewFactory().createDefinitionNavigator(rootView, newDefinition));
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while creating new definition of type: " + definitionTypeIdentifier, e);
        }
    }

    @Override
    public T8ServerOperationStatusReport getServerOperationStatus(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport stopServerOperation(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport cancelServerOperation(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport pauseServerOperation(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport resumeServerOperation(T8Context context, String string) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, Object> executeAsynchronousServerOperation(T8Context context, String operationId, Map<String, Object> operationParameters, String message) throws Exception
    {
        try
        {
            T8ServerOperationStatusReport statusReport;
            Map<String, Object> inputParameters;
            Map<String, Object> outputParameters;
            T8AsynchronousOperationTask task;
            Exception exception;
            String operationIid;

            // Execute the operation.
            inputParameters = T8IdentifierUtilities.stripNamespace(operationId, operationParameters, true);
            statusReport = T8MainServerClient.executeAsynchronousOperation(context, operationId, inputParameters);
            operationIid = statusReport.getOperationInstanceIdentifier();

            // Set the progress layer message and then lock the layer during execution of the operation.
            task = new T8AsynchronousOperationTask(context, operationIid, message);
            viewContainer.setProgressUpdateInterval(500);
            viewContainer.addTask(task);
            lockUI(message);

            // Wait for execution to complete.
            synchronized(task)
            {
                while (task.isActive())
                {
                    try
                    {
                        /*
                            Wait for this task to complete, the component container will call notify on the task object once it is complete,
                            if an exception occurs or the component container failed to call the notify some reason then this thread will wait
                            for the amount specified after which it will recheck the validation in any case and continue.
                        */
                        task.wait(10 * 1000);
                    }
                    catch (IllegalMonitorStateException | InterruptedException e)
                    {
                        LOGGER.log("Exception while waiting for asynchronous task to complete.", e);
                    }
                }
            }

            // Return the output of the operation.
            exception = task.getException();
            outputParameters = task.getResult();
            if (exception != null) throw exception;
            else return T8IdentifierUtilities.prependNamespace(operationId, outputParameters);
        }
        finally
        {
            // Unlock the layer.
            unlockUI();
            viewContainer.setIndeterminate(true);
        }
    }

    @Override
    public Map<String, Object> executeSynchronousServerOperation(T8Context context, String operationId, Map<String, Object> inputParameters, String message) throws Exception
    {
        return T8MainServerClient.executeSynchronousOperation(context, operationId, inputParameters);
    }

    @Override
    public Map<String, Object> executeClientOperation(T8Context context, String operationId, Map<String, Object> inputParameters) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8ServerOperationStatusReport executeAsynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DefinitionEditorFactory getDefinitionEditorFactory(T8Context context)
    {
        return new T8DefaultDefinitionEditorFactory(clientContext);
    }

    @Override
    public Map<String, Object> executeClientScript(T8ScriptDefinition tsd, Map<String, Object> map) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void lockUI(String message)
    {
        viewContainer.setMessage(message);
        viewContainer.setLocked(true);
    }

    @Override
    public void unlockUI()
    {
        viewContainer.setLocked(false);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
