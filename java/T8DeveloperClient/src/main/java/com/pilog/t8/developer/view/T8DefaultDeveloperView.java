package com.pilog.t8.developer.view;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionSelectionEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.ui.event.T8ViewHeaderChangedEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultDeveloperView extends JPanel implements T8DeveloperView
{
    protected List<T8DefinitionViewListener> definitionListeners;

    public T8DefaultDeveloperView()
    {
        this.definitionListeners = new ArrayList<>();
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addView(T8DeveloperView view, String viewHeader)
    {
        addView(view);
    }

    @Override
    public void addDefinitionViewListener(T8DefinitionViewListener listener)
    {
        definitionListeners.add(listener);
    }

    @Override
    public void removeDefinitionViewListener(T8DefinitionViewListener listener)
    {
        definitionListeners.remove(listener);
    }

    @Override
    public void definitionLinkActivated(String identifier)
    {
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
    }

    protected void fireHeaderChangedEvent(String newHeader)
    {
        T8ViewHeaderChangedEvent event;

        event = new T8ViewHeaderChangedEvent(this, newHeader);
        for (T8DefinitionViewListener listener : definitionListeners)
        {
            listener.headerChanged(event);
        }
    }

    protected void fireDefinitionSelectionChangedEvent(T8Definition selectedDefinition)
    {
        T8DefinitionSelectionEvent event;

        event = new T8DefinitionSelectionEvent(this, selectedDefinition);
        for (T8DefinitionViewListener listener : definitionListeners)
        {
            listener.selectionChanged(event);
        }
    }
}
