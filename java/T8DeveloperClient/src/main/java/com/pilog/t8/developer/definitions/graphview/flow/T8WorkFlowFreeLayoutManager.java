package com.pilog.t8.developer.definitions.graphview.flow;

import com.pilog.graph.model.GraphModel;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowFreeLayoutManager
{
    private final ArrayList<String> swimLaneIdentifiers; // The current list of identifiers rendered.
    private final T8WorkFlowVertexRenderer renderer;
    private final T8WorkFlowGraphConfiguration config;
    
    public T8WorkFlowFreeLayoutManager(T8WorkFlowVertexRenderer renderer, T8WorkFlowGraphConfiguration config)
    {
        this.renderer = renderer;
        this.config = config;
        this.swimLaneIdentifiers = new ArrayList<String>();
    }
    
    public GraphModel createGraphModel(T8WorkFlowDefinition graphDefinition)
    {
        List<T8WorkFlowLayoutElement> layoutElements;
        T8WorkFlowModelBuilder modelBuilder;

        // Add all vertices.
        swimLaneIdentifiers.clear();
        layoutElements = new ArrayList<T8WorkFlowLayoutElement>();
        for (T8Definition nodeDefinition : graphDefinition.getNodeDefinitions())
        {
            T8WorkFlowLayoutElement layoutElement;
            String userProfileIdentifier;
                    
            layoutElement = new T8WorkFlowLayoutElement(0, 0, (T8WorkFlowNodeDefinition)nodeDefinition, renderer);
            layoutElement.setVertexMovable(!layoutElement.isBoundaryNode());
            layoutElements.add(layoutElement);
            
            // Get the lane identifier of the element.
            userProfileIdentifier = layoutElement.getSwimLaneIdentifier();

            // Determine the swimlane index for the specified user profile.
            if (!swimLaneIdentifiers.contains(userProfileIdentifier)) swimLaneIdentifiers.add(userProfileIdentifier);;
        }
        
        // Build the model and return it.
        modelBuilder = new T8WorkFlowModelBuilder(graphDefinition, renderer, config);
        return modelBuilder.createGraphModel(layoutElements, swimLaneIdentifiers);
    }
}
