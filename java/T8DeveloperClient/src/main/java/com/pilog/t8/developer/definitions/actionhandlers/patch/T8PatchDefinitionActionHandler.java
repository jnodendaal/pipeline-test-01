package com.pilog.t8.developer.definitions.actionhandlers.patch;

import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8IdentifierUtilities;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.patch.T8PatchDefinition;
import com.pilog.t8.definition.patch.T8StringMappingPatchDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.JOptionPane;


/**
 * @author Bouwer du Preez
 */
public class T8PatchDefinitionActionHandler implements T8DefinitionActionHandler
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8PatchDefinition definition;

    public T8PatchDefinitionActionHandler(T8Context context, T8PatchDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new CreateStringMapFromEntityAction(context, definition));
        return actions;
    }

    private static class CreateStringMapFromEntityAction extends AbstractAction
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8PatchDefinition definition;

        public CreateStringMapFromEntityAction(T8Context context, final T8PatchDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Create String Map from Entity");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            String entityId;
            T8DefinitionManager definitionManager;

            definitionManager = clientContext.getDefinitionManager();

            entityId = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter the identifier of the entity to use.", "Data Entity Identifier", JOptionPane.QUESTION_MESSAGE);
            if (!Strings.isNullOrEmpty(entityId))
            {
                try
                {
                    T8DataEntityDefinition entityDefinition;

                    entityDefinition = (T8DataEntityDefinition)definitionManager.getRawDefinition(context, definition.getRootProjectId(), entityId);
                    if (entityDefinition != null)
                    {
                        String stringFieldIdentifier;

                        stringFieldIdentifier = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter the identifier of the entity field from which to extract target string values.", "Target String Entity Field Identifier", JOptionPane.QUESTION_MESSAGE);
                        if (entityDefinition.containsField(stringFieldIdentifier))
                        {
                            String replacementFieldIdentifier;

                            replacementFieldIdentifier = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter the identifier of the entity field from which to extract replacement string values.", "Replacement String Entity Field Identifier", JOptionPane.QUESTION_MESSAGE);
                            if (entityDefinition.containsField(replacementFieldIdentifier))
                            {
                                int count;

                                count = T8DataManagerOperationHandler.countDataEntities(context, null, entityId, null);
                                if (count < 50001)
                                {
                                    T8StringMappingPatchDefinition mappingDefinition;
                                    List<T8DataEntity> retrievedData;
                                    Map<String, String> stringMap;

                                    stringMap = new LinkedHashMap<String, String>();
                                    retrievedData = T8DataManagerOperationHandler.selectDataEntities(context, entityId, null, 0, 50000);
                                    for (T8DataEntity entity : retrievedData)
                                    {
                                        String targetString;
                                        String replacementString;

                                        targetString = (String)entity.getFieldValue(stringFieldIdentifier);
                                        replacementString = (String)entity.getFieldValue(replacementFieldIdentifier);
                                        stringMap.put(targetString, replacementString);
                                    }

                                    mappingDefinition = new T8StringMappingPatchDefinition(T8IdentifierUtilities.createIdentifierString(T8IdentifierUtilities.createNewGUID()));
                                    mappingDefinition.setStringMap(stringMap);
                                    definition.addStringMap(mappingDefinition);
                                }
                                else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity '" + replacementFieldIdentifier + "' countains " + count + " rows, exceeding the 50000 limit.", "Data Size Restriction Exceeded", JOptionPane.ERROR_MESSAGE);
                            }
                            else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity Field '" + replacementFieldIdentifier + "' could not be found in data entity '" + entityId + "'.", "Data Entity Field Not Found", JOptionPane.ERROR_MESSAGE);
                        }
                        else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity Field '" + stringFieldIdentifier + "' could not be found in data entity '" + entityId + "'.", "Data Entity Field Not Found", JOptionPane.ERROR_MESSAGE);
                    }
                    else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity '" + entityId + "' could not be found.", "Data Entity Not Found", JOptionPane.ERROR_MESSAGE);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while retrieving String Map from data entity.", e);
                    JOptionPane.showMessageDialog(clientContext.getParentWindow(), "An unexpected exception occurred during the requested operation.", "Operation Failure", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
