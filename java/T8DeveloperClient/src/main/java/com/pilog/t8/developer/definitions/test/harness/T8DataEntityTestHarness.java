package com.pilog.t8.developer.definitions.test.harness;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.developer.definitions.test.T8DataEntityTestFrame;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityTestHarness implements T8DefinitionTestHarness
{
    public T8DataEntityTestHarness()
    {
    }

    @Override
    public void configure(T8Context context)
    {
    }

    @Override
    public void testDefinition(T8Context context, T8Definition definition)
    {
        T8DataEntityTestFrame.testModuleDefinition(context, (T8DataEntityDefinition)definition, this);
    }
}
