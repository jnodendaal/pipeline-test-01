package com.pilog.t8.developer.definitions.tableview;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.ui.T8DeveloperView;

/**
 * Extends default list view to added specific feature required by 'usages of'
 * view.
 * 
 * @author Bouwer du Preez
 */
public class T8DefinitionUsagesTableView extends T8DefinitionTableView
{
    private final String definitionIdentifier;
    
    public T8DefinitionUsagesTableView(T8DefinitionContext definitionContext, T8DeveloperView parentView, String definitionIdentifier)
    {    
        super(definitionContext, parentView, null, false);
        this.definitionIdentifier = definitionIdentifier;
        setToolBarVisible(false);
        searchByIdentifier(definitionIdentifier);
    }
    
    @Override
    public String getHeader()
    {
        return "Usages of: " + definitionIdentifier;
    }
}
