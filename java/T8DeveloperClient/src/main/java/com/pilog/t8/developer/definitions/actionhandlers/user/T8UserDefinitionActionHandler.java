package com.pilog.t8.developer.definitions.actionhandlers.user;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.jdesktop.swingx.plaf.basic.core.BasicTransferable;

/**
 * @author Bouwer du Preez
 */
public class T8UserDefinitionActionHandler implements T8DefinitionActionHandler
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8UserDefinition definition;

    public T8UserDefinitionActionHandler(T8Context context, T8UserDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        if(Objects.equals(context.getUserId(), definition.getIdentifier()))
        {
            //Make sure that the api key can only be generated if the definition is not editable becuase the generate method saves the definition
            if(definition.isLocked())
                actions.add(new GenerateApiKeyActionHandler(context, definition));

            actions.add(new CopyApiKeyActionHandler(context, definition));
        }

        return actions;
    }

    private static class GenerateApiKeyActionHandler extends AbstractAction
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8UserDefinition definition;

        public GenerateApiKeyActionHandler(T8Context context, T8UserDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Generate new API Key");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                clientContext.getSecurityManager().generateWebServiceUserApiKey(context);
                Toast.show("New API Key generated", Toast.Style.SUCCESS);
            }
            catch (Exception e)
            {
                Toast.show("Failed to generate new API Key", Toast.Style.ERROR);
                T8Log.log("Exception while generating new api key for user definition: " + definition.getIdentifier(), e);
            }
        }
    }

    private static class CopyApiKeyActionHandler extends AbstractAction
    {
        private final T8Context context;
        private final T8UserDefinition definition;

        public CopyApiKeyActionHandler(T8Context context, T8UserDefinition definition)
        {
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Copy api key to clip board");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/documentCopyIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new BasicTransferable(definition.getApiKey(), definition.getApiKey()), null);

                Toast.show("API Key copied to clip board", Toast.Style.SUCCESS);
            }
            catch (Exception e)
            {
                Toast.show("Failed to copy api key to clip board", Toast.Style.SUCCESS);
                T8Log.log("Exception while copying api key for user definition: " + definition.getIdentifier(), e);
            }
        }
    }
}
