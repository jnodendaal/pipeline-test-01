package com.pilog.t8.developer.definitions.selectiontableview;

import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8MillisecondCellRenderer extends DefaultTableCellRenderer
{
    private DateFormat dateFormat;

    public static final String DEFAULT_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss,SSS";

    public T8MillisecondCellRenderer(String dateTimeFormat)
    {
        this.dateFormat = dateTimeFormat != null ? new SimpleDateFormat(dateTimeFormat) : new SimpleDateFormat(DEFAULT_TIMESTAMP_FORMAT);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean cellHasFocus, int row, int column)
    {
        Long milliseconds;

        milliseconds = (Long)value;
        if (value != null)
        {
            String dateTime;

            dateTime = dateFormat.format(new Date(milliseconds));
            return super.getTableCellRendererComponent(table, dateTime, isSelected, cellHasFocus, row, column);
        }
        else return super.getTableCellRendererComponent(table, value, isSelected, cellHasFocus, row, column);
    }
}
