package com.pilog.t8.developer.definitions.test.harness;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.developer.definitions.test.T8ModuleTestFrame;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleTestHarness implements T8DefinitionTestHarness
{
    private String userId;
    private String userProfileId;
    private String username;
    private char[] userPassword;

    public T8ModuleTestHarness()
    {
    }

    @Override
    public void configure(T8Context context)
    {
        T8ModuleTestHarnessConfigurationDialog.configureTestHarness(context, this);
    }

    @Override
    public void testDefinition(T8Context context, T8Definition definition)
    {
        try
        {
            T8ModuleTestFrame.testModuleDefinition(context, (T8ModuleDefinition)definition, this);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while testing module definition: " + definition, e);
        }
    }

    public String getUserIdentifier()
    {
        return userId;
    }

    public void setUserIdentifier(String userIdentifier)
    {
        this.userId = userIdentifier;
    }

    public String getUserProfileIdentifier()
    {
        return userProfileId;
    }

    public void setUserProfileIdentifier(String userProfileIdentifier)
    {
        this.userProfileId = userProfileIdentifier;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public char[] getUserPassword()
    {
        return userPassword;
    }

    public void setUserPassword(char[] userPassword)
    {
        this.userPassword = userPassword;
    }

    private String createReport(List<T8DefinitionValidationError> validationErrors)
    {
        StringBuffer report;

        report = new StringBuffer();
        report.append("<html>");
        report.append("<h1>System Validation Report</h1>");
        report.append("</br>");
        report.append("</br>");
        report.append("<h2>Validation Errors</h2>");

        if ((validationErrors != null) && (validationErrors.size() > 0))
        {
            report.append("<table border=\"1\">");
            report.append("<tr><td><b>Error Type</b></td><td><b>Project</b></td><td><b>Definition</b></td><td><b>Datum</b></td><td><b>Message</b></td></tr>");

            for (T8DefinitionValidationError error : validationErrors)
            {
                report.append("<tr><td>" + error.getErrorType() + "</td><td>" + error.getProjectIdentifier() + "</td><td>" + error.getDefinitionIdentifier() + "</td><td>" + error.getDatumIdentifier() + "</td><td>" + error.getMessage() + "</td></tr>");
            }
        }

        // Close the table tag if errors were found, else just log a message to indicate that everything is ok.
        if ((validationErrors != null) && validationErrors.size() > 0)
        {
            report.append("</table>");
        }
        else
        {
            report.append("No validation errors found.");
        }

        // Close the html tag.
        report.append("</html>");
        return report.toString();
    }
}
