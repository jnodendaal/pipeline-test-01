package com.pilog.t8.developer.definitions.projectmanager;

import com.pilog.graph.model.GraphEdge.EdgeType;
import com.pilog.t8.developer.definitions.projectmanager.T8ProjectGraphConfiguration.LayoutType;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.prefs.Preferences;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectGraphConfigurationPanel extends javax.swing.JPanel
{
    private final T8ProjectGraphModelHandler graphManager;
    private final T8ProjectGraphConfiguration config;
    private final Preferences preferences;

    public T8ProjectGraphConfigurationPanel(T8ProjectGraphModelHandler graphManager, T8ProjectGraphConfiguration config)
    {
        this.preferences = Preferences.userNodeForPackage(T8ProjectGraphModelHandler.class);
        initComponents();
        this.graphManager = graphManager;
        this.config = config;
        initPanel();
    }

    private void initPanel()
    {
        // Add the layout types.
        for (LayoutType layoutType : LayoutType.values())
        {
            jComboBoxLayoutType.addItem(layoutType);
        }

        // Set the selected layout type and add the selection listener.
        jComboBoxLayoutType.setSelectedItem(config.getLayoutType());
        jComboBoxLayoutType.addItemListener(new LayoutTypeListener());

        // Add the edge types.
        for (EdgeType edgeType : EdgeType.values())
        {
            jComboBoxEdgeType.addItem(edgeType);
        }

        // Set the selected edge type and add the selection listener.
        jComboBoxEdgeType.setSelectedItem(config.getEdgeType());
        jComboBoxEdgeType.addItemListener(new EdgeTypeListener());

        // Set the border glow checkbox state.
        jCheckBoxBorderGlow.setSelected(config.isBorderGlowEnabled());

        // Set the shadows checkbox state.
        jCheckBoxShadows.setSelected(config.isShadowsEnabled());
    }

    private void applyLayoutType(LayoutType type)
    {
        preferences.put(T8ProjectGraphModelHandler.LAYOUT_TYPE_PREFERENCE, type.toString());
        config.setLayoutType(type);
    }

    private void applyEdgeType(EdgeType type)
    {
        preferences.put(T8ProjectGraphModelHandler.EDGE_TYPE_PREFERENCE, type.toString());
        config.setEdgeType(type);
    }

    private class LayoutTypeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                applyLayoutType((LayoutType)e.getItem());
            }
        }
    }

    private class EdgeTypeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                applyEdgeType((EdgeType)e.getItem());
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelLayoutType = new javax.swing.JLabel();
        jComboBoxLayoutType = new javax.swing.JComboBox();
        jLabelEdgeType = new javax.swing.JLabel();
        jComboBoxEdgeType = new javax.swing.JComboBox();
        jLabelBorderGlowEnabled = new javax.swing.JLabel();
        jCheckBoxBorderGlow = new javax.swing.JCheckBox();
        jLabelShadowsEnabled = new javax.swing.JLabel();
        jCheckBoxShadows = new javax.swing.JCheckBox();

        setLayout(new java.awt.GridBagLayout());

        jLabelLayoutType.setText("Layout Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelLayoutType, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jComboBoxLayoutType, gridBagConstraints);

        jLabelEdgeType.setText("Edge Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelEdgeType, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jComboBoxEdgeType, gridBagConstraints);

        jLabelBorderGlowEnabled.setText("Border Glow Enabled:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelBorderGlowEnabled, gridBagConstraints);

        jCheckBoxBorderGlow.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jCheckBoxBorderGlowItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jCheckBoxBorderGlow, gridBagConstraints);

        jLabelShadowsEnabled.setText("Shadows Enabled:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelShadowsEnabled, gridBagConstraints);

        jCheckBoxShadows.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jCheckBoxShadowsItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jCheckBoxShadows, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxBorderGlowItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jCheckBoxBorderGlowItemStateChanged
    {//GEN-HEADEREND:event_jCheckBoxBorderGlowItemStateChanged
        config.setBorderGlowEnabled(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_jCheckBoxBorderGlowItemStateChanged

    private void jCheckBoxShadowsItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jCheckBoxShadowsItemStateChanged
    {//GEN-HEADEREND:event_jCheckBoxShadowsItemStateChanged
        config.setShadowsEnabled(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_jCheckBoxShadowsItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox jCheckBoxBorderGlow;
    private javax.swing.JCheckBox jCheckBoxShadows;
    private javax.swing.JComboBox jComboBoxEdgeType;
    private javax.swing.JComboBox jComboBoxLayoutType;
    private javax.swing.JLabel jLabelBorderGlowEnabled;
    private javax.swing.JLabel jLabelEdgeType;
    private javax.swing.JLabel jLabelLayoutType;
    private javax.swing.JLabel jLabelShadowsEnabled;
    // End of variables declaration//GEN-END:variables
}
