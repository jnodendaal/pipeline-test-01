package com.pilog.t8.developer.definitions.projectmanager;

import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.view.GraphSelectionEvent;
import com.pilog.graph.view.GraphSelectionListener;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionComparator;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.project.T8ProjectImportReport;
import com.pilog.t8.project.T8ProjectImportReport.T8ProjectImportResult;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import com.pilog.t8.developer.view.T8DefaultDeveloperView;
import com.pilog.t8.developer.view.T8DefaultDeveloperViewFactory;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.components.scrollpopupmenu.JScrollMenu;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.JXTextField;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectManagerView extends T8DefaultDeveloperView implements T8DeveloperView
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ProjectManagerView.class);
    private static final String FILE_PATH_PREFERENCE = "META_FILE_PATH_PREFERENCE";

    private final T8DefinitionContext definitionContext;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final T8DeveloperView parentView;
    private final T8SessionContext sessionContext;
    private final T8ProjectGraph graph;
    private final List<T8ProjectDefinition> projectDefinitions;
    private final Set<String> visibleProjectIds;
    private final Preferences preferences;
    private T8DefinitionTypeMetaData typeMeta;
    private T8DeveloperViewFactory viewFactory;
    private JXTextField searchField;

    public enum DefinitionGraphToolBarOption {LOAD, SAVE, PRINT, COPY, RENAME, TEST, CONFIGURE_TEST, REFRESH, CLEANUP};

    public T8ProjectManagerView(T8DefinitionContext definitionContext, T8DeveloperView parentView)
    {
        initComponents();
        this.context = definitionContext.getContext();
        this.clientContext = definitionContext.getClientContext();
        this.definitionManager = clientContext.getDefinitionManager();
        this.sessionContext = definitionContext.getSessionContext();
        this.definitionContext = definitionContext;
        this.parentView = parentView;
        this.projectDefinitions = new ArrayList<>();
        this.graph = new T8ProjectGraph(definitionContext);
        this.graph.addMouseListener(new GraphMouseListener());
        this.graph.addGraphSelectionListener(new ProjectSelectionListener());
        this.graph.setGridXSize(20);
        this.graph.setGridYSize(20);
        this.add(graph, BorderLayout.CENTER);
        this.viewFactory = parentView != null ? parentView.getViewFactory() : new T8DefaultDeveloperViewFactory(definitionContext);
        this.setBorder(new T8ContentHeaderBorder("Project Manager"));
        this.preferences = Preferences.userNodeForPackage(T8ProjectManagerView.class);
        this.visibleProjectIds = new HashSet<>();
        addSearchField();
        loadProjectDefinitions(null);
    }

    private void addSearchField()
    {
        searchField = new JXTextField();
        searchField.setPrompt("Search");
        searchField.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                search();
            }
        });

        jToolBarMain.add(searchField);
    }

    @Override
    public String getHeader()
    {
        return "Project Manager";
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        List<T8ProjectDefinition> selectedProjectDefinitions;

        selectedProjectDefinitions = graph.getSelectedProjectDefinitions();
        return selectedProjectDefinitions.size() > 0 ? selectedProjectDefinitions.get(0) : null;
    }

    @Override
    public void setSelectedDefinition(T8Definition definition)
    {
    }

    public void loadProjectDefinitions(Set<String> visibleProjectIds)
    {
        try
        {
            setProjectDefinitions(definitionManager.getRawGroupDefinitions(context, null, T8ProjectDefinition.GROUP_IDENTIFIER), visibleProjectIds);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while loading project definitions.", e);
        }
    }

    public void setProjectDefinitions(List<T8ProjectDefinition> projectDefinitions, Set<String> visibleProjectIds)
    {
        this.projectDefinitions.clear();
        if (projectDefinitions != null)
        {
            this.projectDefinitions.addAll(projectDefinitions);
            Collections.sort(this.projectDefinitions, new T8DefinitionComparator(T8DefinitionComparator.CompareMethod.LEXICOGRAPHIC, T8DefinitionComparator.CompareType.IDENTIFIER));
        }

        if (visibleProjectIds == null) setAllProjectsVisible();
        else setVisibleProjects(visibleProjectIds);
    }

    public void setAllProjectsVisible()
    {
        Set<String> projectIds;

        projectIds = new HashSet<>();
        for (T8ProjectDefinition projectDefinition : projectDefinitions)
        {
            projectIds.add(projectDefinition.getIdentifier());
        }

        setVisibleProjects(projectIds);
    }

    public Set<String> getVisibleProjectIds()
    {
        return new HashSet<String>(visibleProjectIds);
    }

    public void setVisibleProjects(Set<String> projectIds)
    {
        List<T8ProjectDefinition> visibleDefinitions;

        // Clear the current set of visible project ids.
        visibleProjectIds.clear();

        // Create a list of visible project definitions from the total list of available definitions.
        visibleDefinitions = new ArrayList<>();
        for (String projectId : projectIds)
        {
            T8ProjectDefinition visibleDefinition;

            visibleDefinition = getProjectDefinition(projectId);
            if (visibleDefinition != null)
            {
                visibleProjectIds.add(projectId);
                visibleDefinitions.add(visibleDefinition);
            }
        }

        // Set the visible definitions on the graph.
        graph.setProjectDefinitions(visibleDefinitions);
    }

    public void setVisibleProjectDefinitions(Set<T8ProjectDefinition> definitions)
    {
        Set<String> projectIds;

        // Create a set of all the project ids to be set as visible.
        projectIds = new HashSet<>();
        for (T8ProjectDefinition definition : definitions)
        {
            projectIds.add(definition.getIdentifier());
        }

        // Set the visible definitions.
        setVisibleProjects(projectIds);
    }

    private T8ProjectDefinition getProjectDefinition(String projectId)
    {
        for (T8ProjectDefinition projectDefinition : projectDefinitions)
        {
            if (projectId.equals(projectDefinition.getIdentifier()))
            {
                return projectDefinition;
            }
        }

        return null;
    }

    private List<T8ProjectDefinition> getProjectDefinitions(Collection<String> projectIds)
    {
        List<T8ProjectDefinition> definitions;

        definitions = new ArrayList<>();
        for (T8ProjectDefinition projectDefinition : projectDefinitions)
        {
            if (projectIds.contains(projectDefinition.getIdentifier()))
            {
                definitions.add(projectDefinition);
            }
        }

        return definitions;
    }

    private List<T8ProjectDefinition> getDependentProjectDefinitions(String projectId)
    {
        List<T8ProjectDefinition> definitions;

        definitions = new ArrayList<>();
        for (T8ProjectDefinition projectDefinition : projectDefinitions)
        {
            if (projectDefinition.getRequiredProjectIds().contains(projectId))
            {
                definitions.add(projectDefinition);
            }
        }

        return definitions;
    }

    public void rebuildGraph()
    {
        graph.updateModel();
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    private void validateDefinition()
    {
//        try
//        {
//            T8DefinitionValidationUtils.validateDefinitions(SwingUtilities.windowForComponent(this), this.parentView, this.clientContext, this.sessionContext, ArrayLists.typeSafeList(getGraphDefinition().getId()));
//        }
//        catch (Exception e)
//        {
//            T8Log.log("Exception while validating definitions: " + getGraphDefinition(), e);
//            JOptionPane.showMessageDialog(this, "An unexpected exception occurred.", "Unexpected Exception", JOptionPane.ERROR_MESSAGE);
//        }
    }

    public void addToolbarButton(JButton button)
    {
        jToolBarMain.add(button);
    }

    private void saveDefinitions()
    {

    }

    private void copyDefinition()
    {
//        T8DefinitionMetaData metaData;
//
//        metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this.getRootPane(), typeMeta, null);
//        if (metaData != null)
//        {
//            try
//            {
//                T8Definition newDefinition;
//
//                newDefinition = definitionManager.copyDefinition(sessionContext, getGraphDefinition(), metaData.getId());
//                newDefinition.setProjectId(metaData.getProjectId());
//                parentView.addView(viewFactory.createDefinitionGraph(parentView, (T8GraphDefinition)newDefinition));
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
    }

    private void renameDefinition()
    {
//        T8DefinitionMetaData metaData;
//
//        metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this.getRootPane(), typeMeta, null);
//        if (metaData != null)
//        {
//            try
//            {
//                definitionManager.renameDefinition(sessionContext, getGraphDefinition(), metaData.getId());
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        parentView.addView(view);
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        parentView.removeView(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
        parentView.notifyDefinitionLinkActivated(event);
    }

    private void setGridSnapEnabled(boolean enabled)
    {
        graph.setGridEnabled(enabled);
        graph.setSnapToGridEnabled(enabled);
        graph.repaint();
    }

    public void configureGraph()
    {
        Component configurationComponent;

        configurationComponent = graph.getGraphManager().getConfigurationComponent();
        if (configurationComponent != null)
        {
            T8GraphConfigurationDialog.showConfigurationDialog(SwingUtilities.getWindowAncestor(this), configurationComponent);
        }
    }

    private void setFocusedProject(String focusedProjectId)
    {
        T8ProjectDefinition focusedProject;

        focusedProject = getProjectDefinition(focusedProjectId);
        if (focusedProject != null)
        {
            Set<T8ProjectDefinition> visibleDefinitions;

            visibleDefinitions = new HashSet<>();
            visibleDefinitions.add(focusedProject);
            visibleDefinitions.addAll(getDependentProjectDefinitions(focusedProjectId));
            visibleDefinitions.addAll(getProjectDefinitions(focusedProject.getRequiredProjectIds()));
            setVisibleProjectDefinitions(visibleDefinitions);
        }
        else throw new IllegalArgumentException("Project not found: " + focusedProjectId);
    }

    private void search()
    {
        String searchText;

        searchText = Strings.trimToNull(searchField.getText());
        if (searchText != null)
        {
            List<String> matchingProjectIds;

            matchingProjectIds = new ArrayList<>();
            searchText = searchText.toUpperCase();
            for (T8ProjectDefinition projectDefinition : projectDefinitions)
            {
                String projectId;

                projectId = projectDefinition.getIdentifier();
                if (projectId.contains(searchText))
                {
                    matchingProjectIds.add(projectId);
                }
            }

            graph.setSelectedProjects(matchingProjectIds);
        }
    }

    private String getLocalSaveFilePath()
    {
        try
        {
            String previousPath;
            String filePath;

            previousPath = (String)preferences.get(FILE_PATH_PREFERENCE, null);
            filePath = BasicFileChooser.getSaveFilePath(this, ".t8p", "Tech 8 Project Meta", previousPath != null ? new File(previousPath) : null);
            if (filePath != null)
            {
                preferences.put(FILE_PATH_PREFERENCE, filePath);
                return filePath;
            }
            else return null;
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while getting local file path.", e);
            return null;
        }
    }

    private void deleteSelectedProjects()
    {
        Set<String> projectIds;

        // Get the ids of the projects to export.
        projectIds = graph.getSelectedProjectIds();
        if (!projectIds.isEmpty())
        {
            int dialogOption;

            // Get user confirmation.
            dialogOption = JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Are you absolutely sure you want to delete the " + projectIds.size() + " selected projects and content?", "Confirmation", JOptionPane.YES_NO_OPTION);
            if (dialogOption == JOptionPane.YES_OPTION)
            {
                try
                {
                    Set<String> visibleProjects;

                    // Get the list of currently visible projects.
                    visibleProjects = getVisibleProjectIds();

                    // Set the cursor to indicate an operation in progress.
                    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                    // Delete the projects.
                    definitionManager.deleteProjects(context, new ArrayList<>(projectIds));
                    Toast.show("Projects Deleted", Toast.Style.SUCCESS);

                    // Update the view to reflect the changes.
                    visibleProjects.removeAll(projectIds);
                    setVisibleProjects(visibleProjects);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while deleting projects: " + projectIds, e);
                    Toast.show("Unhandled exception during deletion", Toast.Style.ERROR);
                }
                finally
                {
                    // Reset the cursor to default.
                    setCursor(Cursor.getDefaultCursor());
                }
            }
        }
        else Toast.show("No projects selected for deletion", Toast.Style.ERROR);
    }

    private void exportSelectedProjects()
    {
        Set<String> projectIds;

        // Get the ids of the projects to export.
        projectIds = graph.getSelectedProjectIds();
        if (!projectIds.isEmpty())
        {
            String localFilePath;

            // Prompt the user to specify the file to export to.
            localFilePath = getLocalSaveFilePath();
            if (localFilePath != null)
            {
                T8FileManager fileManager;
                String exportFilename;
                String contextIid;

                // Create a unique context id for the export operation.
                contextIid = T8IdentifierUtilities.createNewGUID();
                exportFilename = contextIid + ".json";
                fileManager = clientContext.getFileManager();

                try
                {
                    // Set the cursor to indicate an operation in progress.
                    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                    // Export the specified projects to the temporary file context.
                    fileManager.openFileContext(context, contextIid, null, new T8Minute(5));
                    definitionManager.exportProjects(context, contextIid, exportFilename, new ArrayList<>(projectIds));

                    // Download the exported file from the temporary context to the local filepath.
                    fileManager.downloadFile(context, contextIid, localFilePath, exportFilename);
                    Toast.show("Projects Exported", Toast.Style.SUCCESS);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while exporting projects.", e);
                    Toast.show("Unhandled exception during export", Toast.Style.ERROR);
                }
                finally
                {
                    // Reset the cursor to default.
                    setCursor(Cursor.getDefaultCursor());

                    try
                    {
                        fileManager.closeFileContext(context, contextIid);
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while closing file context: " + contextIid, e);
                    }
                }
            }
        }
        else Toast.show("No projects selected for export", Toast.Style.ERROR);
    }

    private void importProjects()
    {
        T8ProjectImportReport report;

        // Show the import dialog.
        report = T8ProjectImportDialog.showImportDialog(context);
        if (report != null) // If a null report is returned, no projects were imported.
        {
            Set<String> visibleProjects;

            // Now that new projects have been imported, we will reload all project but we need to add the imported ones to the set of visible projects.
            visibleProjects = getVisibleProjectIds();
            for (T8ProjectImportResult result : report.getResults())
            {
                if (result.isSuccess())
                {
                    visibleProjects.add(result.getProjectId());
                }
            }

            // Reload project definitions, keeping the currenly visible projects and adding the new ones.
            loadProjectDefinitions(visibleProjects);
        }
    }

    private void displayPopupMenu(boolean shiftDown, int x, int y)
    {
        List<GraphVertex> selectedVertices;
        List<GraphVertex> verticesAtLocation;

        // Get the currently selected vertices and the vertices at the location of the mouse pointer.
        verticesAtLocation = graph.getVerticesAtComponentLocation(x, y);
        selectedVertices = graph.getSelectedVertices();

        // If none of the vertices at the mouse pointer are currently selected, change the selection before showing the popup menu.
        if (Collections.disjoint(selectedVertices, verticesAtLocation))
        {
            // If there are vertices at the mouse pointer, select the first one and then show the popup menu.
            if (!verticesAtLocation.isEmpty())
            {
                graph.selectVertex(verticesAtLocation.get(0), true);
                displayProjectPopupMenu(ArrayLists.newArrayList(verticesAtLocation.get(0)), x, y);
            }
            else // No vertices at mouse pointer location, so clear the selection and show the popup menu.
            {
                graph.clearSelection();
                displayProjectPopupMenu(new ArrayList<>(), x, y);
            }
        }
        else // Some or all of the vertices at the mouse location are part of the current selection, so just show the popup menu.
        {
            displayProjectPopupMenu((List)selectedVertices, x, y);
        }
    }

    private void displayProjectPopupMenu(final List<T8ProjectVertex> selectedVertices, int x, int y)
    {
        if (!selectedVertices.isEmpty())
        {
            final T8ProjectDefinition selectedProjectDefinition;
            final T8ProjectVertex selectedVertex;
            String projectId;
            JPopupMenu popupMenu;
            JMenuItem menuItem;
            JMenu subMenu;

            // Get the details of the selected project (primary).
            selectedVertex = selectedVertices.get(0);
            projectId = selectedVertex.getId();
            selectedProjectDefinition = (T8ProjectDefinition)getProjectDefinition(projectId);

            // Create the ActionListener for the menu items.
            ActionListener menuListener = (ActionEvent event) ->
            {
                NavigatorMenuItem selectedMenuItem;
                String code;

                selectedMenuItem = (NavigatorMenuItem)event.getSource();
                code = selectedMenuItem.getCode();
                switch (code)
                {
                    case "CREATE_REQUIRED_PROJECT":
                    {
                        try
                        {
                            T8DefinitionMetaData metaData;

                            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, T8ProjectManagerView.this, definitionManager.getDefinitionTypeMetaData(T8ProjectDefinition.TYPE_IDENTIFIER), null);
                            if (metaData != null)
                            {
                                T8ProjectDefinition newDefinition;

                                newDefinition = (T8ProjectDefinition)definitionManager.createNewDefinition(metaData.getId(), T8ProjectDefinition.TYPE_IDENTIFIER);
                                newDefinition.setProjectIdentifier(metaData.getProjectId());
                                //addProject(newDefinition, selectedVertex.getXCoordinate() + 100, selectedVertex.getYCoordinate() + 100);
                            }
                        }
                        catch (Exception e)
                        {
                            T8Log.log("Exception while fetching meta data for project definition type.", e);
                        }

                        break;
                    }
                    case "ADD_REQUIRED_PROJECT":
                    {
                        String requiredProjectId;

                        requiredProjectId = (String)selectedMenuItem.getDataObject();
                        graph.addRequiredProject(selectedProjectDefinition, requiredProjectId);
                        break;
                    }
                    case "REMOVE_REQUIRED_PROJECT":
                    {
                        String requiredProjectId;

                        requiredProjectId = (String)selectedMenuItem.getDataObject();
                        graph.removeRequiredProject(selectedProjectDefinition, requiredProjectId);
                        break;
                    }
                    case "SET_FOCUSED_PROJECT":
                    {
                        setFocusedProject(selectedProjectDefinition.getIdentifier());
                        break;
                    }
                    case "EDIT_PROJECT":
                    {
                        parentView.addView(viewFactory.createProjectEditor(parentView, selectedProjectDefinition.getIdentifier()));
                        break;
                    }
                    case "EXPORT_PROJECTS":
                    {
                        exportSelectedProjects();
                        break;
                    }
                    case "DELETE_PROJECTS":
                    {
                        deleteSelectedProjects();
                        break;
                    }
                }
            };

            // Create a new popup menu.
            popupMenu = new JPopupMenu();

            menuItem = new NavigatorMenuItem("Set Focus", "SET_FOCUSED_PROJECT", null, null);
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new NavigatorMenuItem("Edit", "EDIT_PROJECT", null, null);
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            subMenu = new JMenu("Create... ");
            menuItem = new NavigatorMenuItem("Required Project", "CREATE_REQUIRED_PROJECT", null, null);
            menuItem.addActionListener(menuListener);
            subMenu.add(menuItem);
            popupMenu.add(subMenu);

            // Create the sub-menu to add a required project to the selected project.
            subMenu = new JScrollMenu("Add Required Project...");
            for (T8Definition projectDefinition : projectDefinitions)
            {
                // Nodes may not be connected to themselves.
                if (!projectDefinition.getIdentifier().equals(selectedProjectDefinition.getIdentifier()))
                {
                    menuItem = new NavigatorMenuItem(projectDefinition.getIdentifier(), "ADD_REQUIRED_PROJECT", null, projectDefinition.getIdentifier());
                    menuItem.addActionListener(menuListener);
                    subMenu.add(menuItem);
                }
            }
            popupMenu.add(subMenu);

            // Create the sub-menu to remove a required project from the selected project.
            subMenu = new JScrollMenu("Remove Required Project...");
            for (String requiredProjectId : selectedProjectDefinition.getRequiredProjectIds())
            {
                menuItem = new NavigatorMenuItem(requiredProjectId, "REMOVE_REQUIRED_PROJECT", null, requiredProjectId);
                menuItem.addActionListener(menuListener);
                subMenu.add(menuItem);
            }
            popupMenu.add(subMenu);

            menuItem = new NavigatorMenuItem("Rename", "RENAME_PROJECT", null, null);
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new NavigatorMenuItem("Delete", "DELETE_PROJECTS", null, null);
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new NavigatorMenuItem("Export", "EXPORT_PROJECTS", null, null);
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            // Display the newly constructed popup menu.
            popupMenu.show(this, x, y);
        }
        else
        {
            JPopupMenu popupMenu;
            JMenuItem menuItem;

            // Create the ActionListener for the menu items.
            ActionListener menuListener = (ActionEvent event) ->
            {
                NavigatorMenuItem selectedMenuItem;
                String code;

                selectedMenuItem = (NavigatorMenuItem)event.getSource();
                code = selectedMenuItem.getCode();
                switch (code)
                {
                    case "IMPORT_PROJECTS":
                    {
                        importProjects();
                        break;
                    }
                }
            };

            // Create a new popup menu.
            popupMenu = new JPopupMenu();
            menuItem = new NavigatorMenuItem("Import", "IMPORT_PROJECTS", null, null);
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            // Display the newly constructed popup menu.
            popupMenu.show(this, x, y);
        }
    }

    private class NavigatorMenuItem extends JMenuItem
    {
        private final String code;
        private final String datumIdentifier;
        private final Object dataObject;

        public NavigatorMenuItem(String text, String code, String datumIdentifier, Object dataObject)
        {
            super(text);
            this.code = code;
            this.datumIdentifier = datumIdentifier;
            this.dataObject = dataObject;
        }

        public String getCode()
        {
            return code;
        }

        public String getDatumIdentifier()
        {
            return datumIdentifier;
        }

        public Object getDataObject()
        {
            return dataObject;
        }
    }

    private class GraphMouseListener implements MouseListener
    {
        private int pressedX;
        private int pressedY;

        @Override
        public void mouseClicked(MouseEvent e)
        {
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            pressedX = e.getX();
            pressedY = e.getY();
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                double moveSizeX;
                double moveSizeY;
                Dimension screenSize;

                screenSize = getToolkit().getScreenSize();
                moveSizeX = screenSize.getWidth() * 0.05;
                moveSizeY = screenSize.getHeight()* 0.05;

                if (e.getX() < (pressedX + moveSizeX) && e.getX() > (pressedX - moveSizeX) && e.getY()< (pressedY + moveSizeY) && e.getY() > (pressedY - moveSizeY))
                {
                    displayPopupMenu(e.isShiftDown(), e.getX(), e.getY());
                }
            }
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    private class ProjectSelectionListener implements GraphSelectionListener
    {
        @Override
        public void selectionChanged(GraphSelectionEvent e)
        {
            List<GraphVertex> selectedVertices;

            selectedVertices = e.getSelectedVertices();
            if (selectedVertices.size() > 0)
            {
                fireDefinitionSelectionChangedEvent(getProjectDefinition(selectedVertices.get(0).getId()));
            }
            else
            {
                fireDefinitionSelectionChangedEvent(null);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonPrint = new javax.swing.JButton();
        jButtonCopyDefinition = new javax.swing.JButton();
        jButtonRenameDefinition = new javax.swing.JButton();
        jButtonRefreshGraph = new javax.swing.JButton();
        jButtonConfigureTestHarness = new javax.swing.JButton();
        jButtonCenterGraph = new javax.swing.JButton();
        jButtonValidateDefinition = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/printIcon.png"))); // NOI18N
        jButtonPrint.setToolTipText("Print Workflow");
        jButtonPrint.setFocusable(false);
        jButtonPrint.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonPrint.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonPrint.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPrint.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPrintActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonPrint);

        jButtonCopyDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/documentCopyIcon.png"))); // NOI18N
        jButtonCopyDefinition.setToolTipText("Copy Definition");
        jButtonCopyDefinition.setFocusable(false);
        jButtonCopyDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCopyDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCopyDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCopyDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCopyDefinition);

        jButtonRenameDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/renameDocumentIcon.png"))); // NOI18N
        jButtonRenameDefinition.setToolTipText("Rename Definition");
        jButtonRenameDefinition.setFocusable(false);
        jButtonRenameDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRenameDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRenameDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRenameDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRenameDefinition);

        jButtonRefreshGraph.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonRefreshGraph.setToolTipText("Refresh Graph");
        jButtonRefreshGraph.setFocusable(false);
        jButtonRefreshGraph.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefreshGraph.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefreshGraph.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshGraphActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefreshGraph);

        jButtonConfigureTestHarness.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/configurationIcon.png"))); // NOI18N
        jButtonConfigureTestHarness.setToolTipText("Configure Graph");
        jButtonConfigureTestHarness.setFocusable(false);
        jButtonConfigureTestHarness.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonConfigureTestHarness.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonConfigureTestHarness.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonConfigureTestHarnessActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonConfigureTestHarness);

        jButtonCenterGraph.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowIn.png"))); // NOI18N
        jButtonCenterGraph.setToolTipText("Center Graph");
        jButtonCenterGraph.setFocusable(false);
        jButtonCenterGraph.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCenterGraph.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCenterGraph.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCenterGraphActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCenterGraph);

        jButtonValidateDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/bugExclamationIcon.png"))); // NOI18N
        jButtonValidateDefinition.setToolTipText("Validate Definition");
        jButtonValidateDefinition.setFocusable(false);
        jButtonValidateDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonValidateDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonValidateDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonValidateDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonValidateDefinition);
        jToolBarMain.add(jSeparator1);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCopyDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCopyDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonCopyDefinitionActionPerformed
        copyDefinition();
    }//GEN-LAST:event_jButtonCopyDefinitionActionPerformed

    private void jButtonRenameDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRenameDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonRenameDefinitionActionPerformed
        renameDefinition();
    }//GEN-LAST:event_jButtonRenameDefinitionActionPerformed

    private void jButtonPrintActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPrintActionPerformed
    {//GEN-HEADEREND:event_jButtonPrintActionPerformed
        T8PrintPreviewDialog.printComponent(SwingUtilities.getWindowAncestor(this), graph);
    }//GEN-LAST:event_jButtonPrintActionPerformed

    private void jButtonConfigureTestHarnessActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonConfigureTestHarnessActionPerformed
    {//GEN-HEADEREND:event_jButtonConfigureTestHarnessActionPerformed
        configureGraph();
    }//GEN-LAST:event_jButtonConfigureTestHarnessActionPerformed

    private void jButtonRefreshGraphActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshGraphActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshGraphActionPerformed
        loadProjectDefinitions(null);
    }//GEN-LAST:event_jButtonRefreshGraphActionPerformed

    private void jButtonValidateDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonValidateDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonValidateDefinitionActionPerformed
        validateDefinition();
    }//GEN-LAST:event_jButtonValidateDefinitionActionPerformed

    private void jButtonCenterGraphActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCenterGraphActionPerformed
    {//GEN-HEADEREND:event_jButtonCenterGraphActionPerformed
        this.graph.centerGraph();
    }//GEN-LAST:event_jButtonCenterGraphActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCenterGraph;
    private javax.swing.JButton jButtonConfigureTestHarness;
    private javax.swing.JButton jButtonCopyDefinition;
    private javax.swing.JButton jButtonPrint;
    private javax.swing.JButton jButtonRefreshGraph;
    private javax.swing.JButton jButtonRenameDefinition;
    private javax.swing.JButton jButtonValidateDefinition;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
