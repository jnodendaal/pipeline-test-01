package com.pilog.t8.developer.definitions.projecteditor;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.definition.T8DefinitionGroupMetaData;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.developer.definitions.selectiontableview.T8MillisecondCellRenderer;
import com.pilog.t8.utilities.cache.LRUCache;
import com.pilog.t8.utilities.components.cellrenderers.CellRendererUtilities;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.JXTreeTable;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectEditorTreeTable extends JXTreeTable
{
    private final T8ProjectEditor view;
    private final T8ProjectEditorTreeTableModel model;
    private final Map<String, T8DefinitionTypeMetaData> typeMeta;
    private final Map<String, T8DefinitionGroupMetaData> groupMeta;
    private final T8ProjectEditorTreeCellRenderer treeCellRenderer;

    // Stores the expansion states of the last 10 definitions.
    private static final LRUCache<String, Map<String, Boolean>> projectExpansionStates = new LRUCache<String, Map<String, Boolean>>(20);

    public T8ProjectEditorTreeTable(T8ClientContext clientContext, T8ProjectEditor view)
    {
        this.view = view;
        this.model = new T8ProjectEditorTreeTableModel();
        this.typeMeta = new HashMap<>();
        this.groupMeta = new HashMap<>();
        this.treeCellRenderer = new T8ProjectEditorTreeCellRenderer(clientContext.getDefinitionManager(), view);
        setTreeTableModel(model);
        setRootVisible(true);
        setRowHeight(25);
        setGridColor(new Color(240, 240, 240));
        setTreeCellRenderer(treeCellRenderer);
        setCreatedTimeCellRenderer(new T8MillisecondCellRenderer(null));
        setUpdatedTimeCellRenderer(new T8MillisecondCellRenderer(null));
        addTreeExpansionListener(new ExpansionListener());
    }

    public Map<String, T8DefinitionTypeMetaData> getTypeMeta()
    {
        return new HashMap<>(typeMeta);
    }

    public void setTypeMeta(Collection<T8DefinitionTypeMetaData> typeMeta)
    {
        this.typeMeta.clear();
        if (typeMeta != null)
        {
            for (T8DefinitionTypeMetaData type : typeMeta)
            {
                this.typeMeta.put(type.getTypeId(), type);
            }
        }
    }

    public void setGroupMeta(Collection<T8DefinitionGroupMetaData> groupMeta)
    {
        this.groupMeta.clear();
        if (groupMeta != null)
        {
            for (T8DefinitionGroupMetaData group : groupMeta)
            {
                this.groupMeta.put(group.getGroupId(), group);
            }
        }
    }

    public void setTypeMeta(Map<String, T8DefinitionTypeMetaData> typeMeta)
    {
        this.typeMeta.clear();
        if (typeMeta != null)
        {
            this.typeMeta.putAll(typeMeta);
        }
    }

    public void setProject(T8DefinitionMetaData projectMeta, List<T8DefinitionMetaData> contentMeta)
    {
        saveExpansionState();
        model.setProject(groupMeta, typeMeta, projectMeta, contentMeta, false);
        setExpansionState(projectExpansionStates.get(projectMeta.getId()));
        autoSizeColumns();
    }

    public void addDefinition(T8DefinitionMetaData definitionMeta)
    {
        model.addDefinition(groupMeta.get(definitionMeta.getGroupId()), typeMeta.get(definitionMeta.getTypeId()), definitionMeta);
    }

    public void removeDefinition(T8DefinitionHandle handle)
    {
        model.removeDefinition(handle);
    }

    public boolean isNodeExpanded(String id)
    {
        T8ProjectEditorTreeTableNode node;

        node = model.findNode(id);
        if (node != null)
        {
            return isExpanded(node.getPath());
        }
        else throw new IllegalArgumentException("Cannot find node: " + id);
    }

    public void expandNode(String id)
    {
        T8ProjectEditorTreeTableNode node;

        node = model.findNode(id);
        if (node != null)
        {
            expandPath(node.getPath());
        }
    }

    public T8DefinitionHandle getSelectedDefinitionHandle()
    {
        TreePath selectionPath;

        selectionPath = getTreeSelectionModel().getSelectionPath();
        if (selectionPath != null)
        {
            T8ProjectEditorTreeTableNode selectedNode;
            T8DefinitionMetaData selectedDefinition;

            selectedNode = (T8ProjectEditorTreeTableNode)selectionPath.getLastPathComponent();
            selectedDefinition = selectedNode.getDefinitionMetaData();
            if (selectedDefinition != null)
            {
                return selectedDefinition.getDefinitionHandle();
            }
            else return null;
        }
        else return null;
    }

    public List<T8DefinitionHandle> getSelectedDefinitionHandles()
    {
        List<T8DefinitionHandle> handles;

        handles = new ArrayList<>();
        for (TreePath selectionPath : getTreeSelectionModel().getSelectionPaths())
        {
            T8ProjectEditorTreeTableNode selectedNode;
            T8DefinitionMetaData selectedDefinition;

            selectedNode = (T8ProjectEditorTreeTableNode)selectionPath.getLastPathComponent();
            selectedDefinition = selectedNode.getDefinitionMetaData();
            if (selectedDefinition != null)
            {
                handles.add(selectedDefinition.getDefinitionHandle());
            }
        }

        return handles;
    }

    public List<T8DefinitionGroupMetaData> getSelectedDefinitionGroups()
    {
        List<T8DefinitionGroupMetaData> groups;

        groups = new ArrayList<>();
        for (TreePath selectionPath : getTreeSelectionModel().getSelectionPaths())
        {
            T8ProjectEditorTreeTableNode selectedNode;

            selectedNode = (T8ProjectEditorTreeTableNode)selectionPath.getLastPathComponent();
            if (selectedNode.isGroupNode())
            {
                groups.add(selectedNode.getDefinitionGroupMetaData());
            }
        }

        return groups;
    }

    public List<T8DefinitionTypeMetaData> getSelectedDefinitionTypes()
    {
        List<T8DefinitionTypeMetaData> types;

        types = new ArrayList<>();
        for (TreePath selectionPath : getTreeSelectionModel().getSelectionPaths())
        {
            T8ProjectEditorTreeTableNode selectedNode;

            selectedNode = (T8ProjectEditorTreeTableNode)selectionPath.getLastPathComponent();
            if (selectedNode.isTypeNode())
            {
                types.add(selectedNode.getDefinitionTypeMetaData());
            }
        }

        return types;
    }

    public void setSelectedDefinition(T8DefinitionHandle handle)
    {

    }

    public List<T8DefinitionHandle> getAllDefinitionHandles()
    {
        return new ArrayList<>();
    }

    public T8DefinitionMetaData getDefinitionMetaData(T8DefinitionHandle handle)
    {
        return null;
    }

    private void setCreatedTimeCellRenderer(TableCellRenderer renderer)
    {
        int columnIndex;

        columnIndex = convertColumnIndexToView(5);
        this.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
    }

    private void setUpdatedTimeCellRenderer(TableCellRenderer renderer)
    {
        int columnIndex;

        columnIndex = convertColumnIndexToView(7);
        this.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
    }

    private void autoSizeColumns()
    {
        // The auto-sizing has be be queued for execution on the EDT after all of the tree setup has been finished.
        SwingUtilities.invokeLater(() ->
        {
            CellRendererUtilities.autoSizeTableColumns(T8ProjectEditorTreeTable.this, 100);
        });
    }

    private void setExpansionState(Map<String, Boolean> expansionState)
    {
        if (expansionState != null)
        {
            T8ProjectEditorTreeTableNode rootNode;

            rootNode = model.getRootNode();
            if (rootNode != null)
            {
                List<T8ProjectEditorTreeTableNode> nodes;

                nodes = rootNode.getDescendantNodes();
                for (T8ProjectEditorTreeTableNode node : nodes)
                {
                    String identifier;
                    Boolean expanded;

                    // Set the expanded state.
                    identifier = node.getId();
                    expanded = expansionState.get(identifier);
                    if ((expanded != null) && (expanded))
                    {
                        expandNode(identifier);
                    }
                }
            }
        }
    }

    private void saveExpansionState()
    {
        T8ProjectEditorTreeTableNode rootNode;

        rootNode = model.getRootNode();
        if (rootNode != null)
        {
            List<T8ProjectEditorTreeTableNode> nodes;
            HashMap<String, Boolean> expansionState;
            boolean nonDefaultState; // Set to true if the expansion state has been altered from default (has to be saved).

            nonDefaultState = false;
            expansionState = new HashMap<String, Boolean>();
            nodes = rootNode.getDescendantNodes();
            for (T8ProjectEditorTreeTableNode node : nodes)
            {
                String id;
                boolean expanded;

                id = node.getId();
                expanded = isNodeExpanded(id);
                if (expanded) nonDefaultState = true;
                expansionState.put(id, expanded);
            }

            // Only save the state if it is non-default.
            if (nonDefaultState)
            {
                projectExpansionStates.put(rootNode.getId(), expansionState);
            }
        }
    }

    private class ExpansionListener implements TreeExpansionListener
    {
        @Override
        public void treeExpanded(TreeExpansionEvent event)
        {
            autoSizeColumns();
        }

        @Override
        public void treeCollapsed(TreeExpansionEvent event)
        {
        }
    }
}
