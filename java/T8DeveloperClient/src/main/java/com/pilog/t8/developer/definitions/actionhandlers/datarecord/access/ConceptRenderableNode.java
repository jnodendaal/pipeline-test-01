package com.pilog.t8.developer.definitions.actionhandlers.datarecord.access;

/**
 * @author Gavin Boshoff
 */
public interface ConceptRenderableNode
{
    public abstract String getConceptID();
}
