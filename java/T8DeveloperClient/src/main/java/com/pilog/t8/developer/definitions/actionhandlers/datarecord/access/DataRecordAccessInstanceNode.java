package com.pilog.t8.developer.definitions.actionhandlers.datarecord.access;

import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 */
public class DataRecordAccessInstanceNode extends DataRecordAccessNode
{
    private final DataRequirementInstance drInstance;
    private boolean instanceMode;

    public DataRecordAccessInstanceNode(DataRequirementInstance instance, DataRecordAccessNode parentNode)
    {
        super(parentNode);
        this.drInstance = instance;
        this.instanceMode = true;

        initialize();
    }

    private void initialize()
    {
        boolean hasChildren;

        clearChildren();

        hasChildren = false;
        for (PropertyRequirement propertyRequirement : drInstance.getDataRequirement().getPropertyRequirements())
        {
            hasChildren = hasChildren || (propertyRequirement.getValueRequirement().getRequirementType() == RequirementType.DOCUMENT_REFERENCE) || loadFields();
        }

        //Check that we do not try to load children for concepts that are of the data requirement type
        setChildrenLoaded(!hasChildren);
    }

    @Override
    public void loadChildren(T8Context context, T8OntologyClientApi ontApi, T8DataRequirementClientApi drqApi)
    {
        if (!isChildrenLoaded())
        {
            for (PropertyRequirement propertyRequirement : drInstance.getDataRequirement().getPropertyRequirements())
            {
                ValueRequirement valueRequirement;

                valueRequirement = propertyRequirement.getValueRequirement();
                if (valueRequirement.getRequirementType() == RequirementType.DOCUMENT_REFERENCE || loadFields())
                {
                    addChild(new DataRecordAccessPropertyNode(propertyRequirement, this));
                }
            }

            sortChildren();
            setChildrenLoaded(true);
        }
    }

    @Override
    public String getConceptID()
    {
        return drInstance.getConceptID();
    }

    @Override
    public DataRequirementInstance getDataRequirementInstance()
    {
        return drInstance;
    }

    @Override
    public String getDocPathRepresentation(String suffix)
    {
        return (instanceMode ? "I" : "D") + suffix + ":" + (instanceMode ? drInstance.getConceptID() : drInstance.getDataRequirement().getConceptID());
    }

    public void switchDocPathMode()
    {
        this.instanceMode = !this.instanceMode;
    }
}
