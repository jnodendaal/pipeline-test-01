package com.pilog.t8.developer.definitions.graphview.state;

import com.pilog.graph.model.DefaultGraphEdge;
import com.pilog.graph.model.DefaultGraphModel;
import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphModel;
import com.pilog.graph.model.GraphVertexPort;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateGraphDefinition;
import com.pilog.t8.definition.graph.T8GraphEdgeDefinition;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8StateModelBuilder
{
    private final T8DataObjectStateGraphDefinition definition;
    private final T8StateVertexRenderer renderer;
    private final T8StateGraphConfiguration config;

    public T8StateModelBuilder(T8DataObjectStateGraphDefinition definition, T8StateVertexRenderer renderer, T8StateGraphConfiguration config)
    {
        this.definition = definition;
        this.renderer = renderer;
        this.config = config;
    }

    public GraphModel createGraphModel(List<T8StateLayoutElement> layoutElements)
    {
        Map<String, T8StateLayoutElement> vertices;
        List<T8GraphEdgeDefinition> edgeDefinitions;
        DefaultGraphModel model;

        model = new DefaultGraphModel();
        vertices = new HashMap<String, T8StateLayoutElement>();

        // Add all layout elements to the model.
        for (T8StateLayoutElement vertexElement : layoutElements)
        {
            // Add the vertex to the collection.
            vertices.put(vertexElement.getNodeDefinition().getIdentifier(), vertexElement);

            // Add the vertex to the model.
            model.addVertex(vertexElement);
        }

        // Add all edges.
        edgeDefinitions = definition.getEdgeDefinitions();
        for (T8GraphEdgeDefinition edgeDefinition : edgeDefinitions)
        {
            T8StateLayoutElement parentVertex;
            T8StateLayoutElement childVertex;
            DefaultGraphEdge edge;

            parentVertex = vertices.get(edgeDefinition.getParentNodeIdentifier());
            childVertex = vertices.get(edgeDefinition.getChildNodeIdentifier());
            if (parentVertex == null) T8Log.log("Parent node '" + edgeDefinition.getParentNodeIdentifier() + "' referenced by edge '" + edgeDefinition.getIdentifier() + "' not found.");
            if (childVertex == null) T8Log.log("Child node '" + edgeDefinition.getChildNodeIdentifier() + "' referenced by edge '" + edgeDefinition.getIdentifier() + "' not found.");

            // Create the edge and add it to the model.
            edge = new DefaultGraphEdge(edgeDefinition.getIdentifier(), parentVertex, GraphVertexPort.PortType.DYNAMIC_CENTER, childVertex, GraphVertexPort.PortType.DYNAMIC_CENTER);
            edge.setEdgeType(config.getEdgeType());
            edge.setEdgeStartArrowType(GraphEdge.EdgeArrowType.NONE);
            edge.setEdgeEndArrowType(GraphEdge.EdgeArrowType.FILLED);
            model.addEdge(edge);
        }

        return model;
    }
}
