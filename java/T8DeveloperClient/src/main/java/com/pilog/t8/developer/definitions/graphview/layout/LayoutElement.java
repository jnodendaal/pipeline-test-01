package com.pilog.t8.developer.definitions.graphview.layout;

import java.awt.Dimension;

/**
 * @author Bouwer du Preez
 */
public interface LayoutElement
{
    public LayoutInsets getInsets();
    public Dimension getSize();
    public void doLayout(int x, int y);
}
