package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

/**
 * @author Bouwer du Preez
 */
public class ValueListDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private final SelectionChangeListener selectionListener;

    public ValueListDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        this.selectionListener = new SelectionChangeListener();
        initComponents();
        jComboBoxValue.addItemListener(selectionListener);
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void commitChanges()
    {
        commitSelectedValue();
    }

    @Override
    public void setEditable(boolean editable)
    {
        setEnabled(editable);
    }

    @Override
    public void setEnabled(boolean editable)
    {
        super.setEnabled(editable);
        jComboBoxValue.setEnabled(editable);
    }

    public void refreshDatumOptions()
    {
        try
        {
            ArrayList<T8DefinitionDatumOption> datumOptions;

            T8Log.log("Refreshing datum list for definition datum: " + definition + ":" + datumType.getIdentifier());
            datumOptions = definition.getDatumOptions(definitionContext, datumType.getIdentifier());
            if (datumOptions != null)
            {
                Collections.sort(datumOptions, new T8DefinitionDatumOption.DisplayNameComparator());
                jComboBoxValue.setModel(new DefaultComboBoxModel(datumOptions.toArray()));
            }
            else
            {
                jComboBoxValue.setModel(new DefaultComboBoxModel());
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while refreshing datum options.", e);
        }
    }

    @Override
    public void refreshEditor()
    {
        ComboBoxModel<T8DefinitionDatumOption> model;
        Object value;

        // Refresh the datum options.
        refreshDatumOptions();

        // Now refresh the selection.
        value = getDefinitionDatum();
        model = jComboBoxValue.getModel();
        for (int elementIndex = 0; elementIndex < model.getSize(); elementIndex++)
        {
            T8DefinitionDatumOption option;

            option = model.getElementAt(elementIndex);
            if (Objects.equals(value, option.getValue()))
            {
                jComboBoxValue.setSelectedItem(option);
                return;
            }
        }

        if(datumType.getDataType() != T8DataType.DEFINITION_IDENTIFIER)
        {
            jXButtonGoToDefinition.setVisible(false);
        }
        else if (definition.getDefinitionDatum(datumType.getIdentifier()) == null)
        {
            jXButtonGoToDefinition.setVisible(false);
        }
    }

    private void commitSelectedValue()
    {
        T8DefinitionDatumOption selectedOption;

        selectedOption = (T8DefinitionDatumOption) jComboBoxValue.getSelectedItem();
        setDefinitionDatum(selectedOption != null ? selectedOption.getValue() : null);

        jXButtonGoToDefinition.setVisible(selectedOption != null && selectedOption.getValue() != null && T8IdentifierUtilities.isDefinitionIdentifierType(datumType.getDataType()));
    }

    private class SelectionChangeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                commitSelectedValue();
            }
        }
    }

    private void openDefinition()
    {
        T8DefinitionDatumOption selectedOption;

        selectedOption = (T8DefinitionDatumOption) jComboBoxValue.getSelectedItem();

        if(selectedOption != null)
        {
            if(selectedOption.getValue() instanceof String)
            {
                T8DeveloperView developerView = findDeveloperView(this);
                if(developerView != null)
                {
                    developerView.notifyDefinitionLinkActivated(new T8DefinitionLinkEvent((Component)developerView, definition.getRootProjectId(), (String)selectedOption.getValue()));
                }
            }
        }
    }

    private T8DeveloperView findDeveloperView(Component component)
    {
        if(component instanceof T8DeveloperView)
        {
            return (T8DeveloperView)component;
        }
        else if(component.getParent() == null)
        {
            return null;
        }
        else
        {
            return findDeveloperView(component.getParent());
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jComboBoxValue = new com.pilog.t8.utilities.components.combobox.SearchableComboBox();
        jXButtonGoToDefinition = new org.jdesktop.swingx.JXButton();

        setLayout(new java.awt.GridBagLayout());

        jComboBoxValue.setMaximumSize(null);
        jComboBoxValue.setMinimumSize(null);
        jComboBoxValue.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        add(jComboBoxValue, gridBagConstraints);

        jXButtonGoToDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/configurationIcon.png"))); // NOI18N
        jXButtonGoToDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonGoToDefinitionActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 2, 3, 2);
        add(jXButtonGoToDefinition, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jXButtonGoToDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonGoToDefinitionActionPerformed
    {//GEN-HEADEREND:event_jXButtonGoToDefinitionActionPerformed
        openDefinition();
    }//GEN-LAST:event_jXButtonGoToDefinitionActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.pilog.t8.utilities.components.combobox.SearchableComboBox jComboBoxValue;
    private org.jdesktop.swingx.JXButton jXButtonGoToDefinition;
    // End of variables declaration//GEN-END:variables
}
