package com.pilog.t8.developer.definitions.projectmanager;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.project.T8ProjectImportReport;
import com.pilog.t8.project.T8ProjectImportReport.T8ProjectImportResult;
import com.pilog.t8.project.T8ProjectPackageIndex;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.utilities.components.cellrenderers.CellRendererUtilities;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.awt.Cursor;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.prefs.Preferences;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectImportDialog extends javax.swing.JDialog
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ProjectImportDialog.class);
    private static final String FILE_PATH_PREFERENCE = "META_FILE_PATH_PREFERENCE";

    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final T8FileManager fileManager;
    private final Preferences preferences;
    private T8ProjectImportReport report;
    private File localFile;

    public T8ProjectImportDialog(T8Context context)
    {
        super(context.getClientContext().getParentWindow(), ModalityType.DOCUMENT_MODAL);
        this.context = context;
        this.definitionManager = context.getClientContext().getDefinitionManager();
        this.fileManager = context.getClientContext().getFileManager();
        this.preferences = Preferences.userNodeForPackage(T8ProjectImportDialog.class);
        initComponents();
        setSize(550, 600);
        setLocationRelativeTo(null);
    }

    private List<String> getSelectedProjectIds()
    {
        TableModel tableModel;
        int[] selectedRows;
        List<String> projectIds;

        tableModel = jTableProjects.getModel();
        selectedRows = jTableProjects.getSelectedRows();
        projectIds = new ArrayList<>();
        for (int selectedRow : selectedRows)
        {
            projectIds.add((String)tableModel.getValueAt(selectedRow, 0));
        }

        return projectIds;
    }

    private T8ProjectImportReport getImportReport()
    {
        return report;
    }

    public static T8ProjectImportReport showImportDialog(T8Context context)
    {
        T8ProjectImportDialog dialog;

        dialog = new T8ProjectImportDialog(context);
        dialog.setVisible(true);
        return dialog.getImportReport();
    }

    private void clearProjectTable()
    {
        DefaultTableModel tableModel;

        tableModel = (DefaultTableModel)jTableProjects.getModel();
        while (tableModel.getRowCount() > 0)
        {
            tableModel.removeRow(0);
        }
    }

    private void openFile()
    {
        try
        {
            String preferredPath;
            String filePath;

            preferredPath = preferences.get(FILE_PATH_PREFERENCE, null);
            filePath = BasicFileChooser.getLoadFilePath(this, ".t8p", "Tech 8 Project Meta", preferredPath != null ? new File(preferredPath) : null);
            if (filePath != null)
            {
                clearProjectTable();
                jTextFieldFilePath.setText(filePath);
                preferences.put(FILE_PATH_PREFERENCE, filePath);
                localFile = new File(filePath);
                if (localFile.exists())
                {
                    InputStreamReader reader = null;

                    try
                    {
                        T8ProjectPackageIndex index;
                        DefaultTableModel tableModel;

                        // Set the cursor to indicate an operation in progress.
                        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                        // Read the index details from the project package to be imported.
                        reader = new InputStreamReader(new FileInputStream(localFile));
                        index = new T8ProjectPackageIndex();
                        index.read(reader);

                        // Add all of the project details to the project selection table.
                        tableModel = (DefaultTableModel)jTableProjects.getModel();
                        for (String projectId : index.getProjectIds())
                        {
                            ProjectImportDetails importDetails;
                            Object[] row;

                            importDetails = getProjectImportDetails(index, projectId);

                            row = new Object[5];
                            row[0] = projectId;
                            row[1] = importDetails.isNewProject() ? "New" : "Update";
                            row[2] = importDetails.getInsertCount();
                            row[3] = importDetails.getUpdateCount();
                            row[4] = importDetails.getDeleteCount();
                            tableModel.addRow(row);
                        }

                        // Auto-size the columns.
                        CellRendererUtilities.autoSizeTableColumns(jTableProjects, 100);
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while opening target project file: " + localFile, e);
                        Toast.show("Unexpected exception while reading source file", Toast.Style.ERROR);
                    }
                    finally
                    {
                        // Reset the cursor to default.
                        setCursor(Cursor.getDefaultCursor());

                        // Close the input stream.
                        if (reader != null) reader.close();
                    }
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while opening project meta file", e);
            Toast.show("Exception while opening project meta file", Toast.Style.ERROR);
        }
    }

    private void importProjects()
    {
        List<String> selectedProjectIds;

        report = null;
        selectedProjectIds = getSelectedProjectIds();
        if (!selectedProjectIds.isEmpty())
        {
            String uploadContextIid;

            // Generate a unique id for the temporary file upload context.
            uploadContextIid = T8IdentifierUtilities.createNewGUID();

            try
            {
                // Set the cursor to indicate an operation in progress.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                // Open the temporary file context and upload the local file.
                fileManager.openFileContext(context, uploadContextIid, null, new T8Minute(5));
                fileManager.uploadFile(context, uploadContextIid, localFile.getCanonicalPath(), localFile.getName());

                // Import the definitions from the temporary context.
                report = definitionManager.importProjects(context, uploadContextIid, localFile.getName(), selectedProjectIds);
                jTextAreaReport.setText(null);
                for (T8ProjectImportResult result : report.getResults())
                {
                    jTextAreaReport.append(result.getProjectId() + ": " + (result.isSuccess() ? "Success" : "Failure") + " - " + result.getMessage() + "\n");
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while importing projects: " + localFile, e);
                Toast.show("Exception while importing projects", Toast.Style.ERROR);
            }
            finally
            {
                // Reset the cursor to default.
                setCursor(Cursor.getDefaultCursor());

                try
                {
                    fileManager.closeFileContext(context, uploadContextIid);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while closing file context instance: " + uploadContextIid, e);
                }
            }
        }
        else
        {
            Toast.show("No projects selected for import", Toast.Style.ERROR);
        }
    }

    private ProjectImportDetails getProjectImportDetails(T8ProjectPackageIndex index, String projectId) throws Exception
    {
        T8DefinitionMetaData projectMetaData;

        projectMetaData = definitionManager.getDefinitionMetaData(null, projectId);
        if (projectMetaData != null)
        {
            List<T8DefinitionMetaData> oldMetaData;
            List<T8DefinitionMetaData> newMetaData;
            int insertCount;
            int updateCount;
            int deleteCount;

            oldMetaData = definitionManager.getProjectDefinitionMetaData(projectId);
            newMetaData = index.getProjectContentMetaData(projectId);

            insertCount = 0;
            updateCount = 0;
            deleteCount = 0;

            // Find inserts and updates.
            for (T8DefinitionMetaData newMeta : newMetaData)
            {
                boolean found;

                found = false;
                for (T8DefinitionMetaData oldMeta : oldMetaData)
                {
                    if (Objects.equals(oldMeta.getId(), newMeta.getId()))
                    {
                        if (!Objects.equals(oldMeta.getChecksum(), newMeta.getChecksum()))
                        {
                            updateCount++;
                        }

                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    insertCount++;
                }
            }

            // Find deletions.
            for (T8DefinitionMetaData oldMeta : oldMetaData)
            {
                boolean found;

                found = false;
                for (T8DefinitionMetaData newMeta : newMetaData)
                {
                    if (Objects.equals(oldMeta.getId(), newMeta.getId()))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    deleteCount++;
                }
            }

            return new ProjectImportDetails(projectId, false, insertCount, updateCount, deleteCount);
        }
        else return new ProjectImportDetails(projectId, true, index.getProjectContentMetaData(projectId).size(), 0, 0);
    }

    private static class ProjectImportDetails
    {
        private String projectId;
        private boolean newProject;
        private int updateCount;
        private int deleteCount;
        private int insertCount;

        public ProjectImportDetails(String projectId, boolean newProject, int insertCount, int updateCount, int deletionCount)
        {
            this.projectId = projectId;
            this.newProject = newProject;
            this.updateCount = updateCount;
            this.deleteCount = deletionCount;
            this.insertCount = insertCount;
        }

        public String getProjectId()
        {
            return projectId;
        }

        public void setProjectId(String projectId)
        {
            this.projectId = projectId;
        }

        public boolean isNewProject()
        {
            return newProject;
        }

        public void setNewProject(boolean newProject)
        {
            this.newProject = newProject;
        }

        public int getUpdateCount()
        {
            return updateCount;
        }

        public void setUpdateCount(int updateCount)
        {
            this.updateCount = updateCount;
        }

        public int getDeleteCount()
        {
            return deleteCount;
        }

        public void setDeleteCount(int deletionCount)
        {
            this.deleteCount = deletionCount;
        }

        public int getInsertCount()
        {
            return insertCount;
        }

        public void setInsertCount(int insertCount)
        {
            this.insertCount = insertCount;
        }

        @Override
        public String toString()
        {
            if (newProject)
            {
                return projectId + "[N]: " + insertCount + "[N]";
            }
            else
            {
                return projectId + "[U]: " + insertCount + "[N] " + updateCount + "[U] " + deleteCount + "[D]";
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelFileDetails = new javax.swing.JPanel();
        jTextFieldFilePath = new javax.swing.JTextField();
        jScrollPaneProjects = new javax.swing.JScrollPane();
        jTableProjects = new javax.swing.JTable();
        jLabelReport = new javax.swing.JLabel();
        jScrollPaneReport = new javax.swing.JScrollPane();
        jTextAreaReport = new javax.swing.JTextArea();
        jButtonFileChooser = new javax.swing.JButton();
        jButtonImport = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Project Import");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelFileDetails.setLayout(new java.awt.GridBagLayout());

        jTextFieldFilePath.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelFileDetails.add(jTextFieldFilePath, gridBagConstraints);

        jScrollPaneProjects.setMinimumSize(new java.awt.Dimension(23, 200));
        jScrollPaneProjects.setPreferredSize(new java.awt.Dimension(452, 302));

        jTableProjects.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Project Id", "Type", "Inserts", "Updates", "Deletions"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableProjects.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPaneProjects.setViewportView(jTableProjects);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanelFileDetails.add(jScrollPaneProjects, gridBagConstraints);

        jLabelReport.setText("Import Report");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanelFileDetails.add(jLabelReport, gridBagConstraints);

        jTextAreaReport.setColumns(20);
        jTextAreaReport.setRows(5);
        jScrollPaneReport.setViewportView(jTextAreaReport);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanelFileDetails.add(jScrollPaneReport, gridBagConstraints);

        jButtonFileChooser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/searchIcon.png"))); // NOI18N
        jButtonFileChooser.setText("Browse");
        jButtonFileChooser.setToolTipText("Select a file to import");
        jButtonFileChooser.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonFileChooserActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanelFileDetails.add(jButtonFileChooser, gridBagConstraints);

        jButtonImport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseImportIcon.png"))); // NOI18N
        jButtonImport.setText("Import");
        jButtonImport.setToolTipText("Import the selected projects from the source file");
        jButtonImport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonImportActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanelFileDetails.add(jButtonImport, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanelFileDetails, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonFileChooserActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonFileChooserActionPerformed
    {//GEN-HEADEREND:event_jButtonFileChooserActionPerformed
        openFile();
    }//GEN-LAST:event_jButtonFileChooserActionPerformed

    private void jButtonImportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonImportActionPerformed
    {//GEN-HEADEREND:event_jButtonImportActionPerformed
        importProjects();
    }//GEN-LAST:event_jButtonImportActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonFileChooser;
    private javax.swing.JButton jButtonImport;
    private javax.swing.JLabel jLabelReport;
    private javax.swing.JPanel jPanelFileDetails;
    private javax.swing.JScrollPane jScrollPaneProjects;
    private javax.swing.JScrollPane jScrollPaneReport;
    private javax.swing.JTable jTableProjects;
    private javax.swing.JTextArea jTextAreaReport;
    private javax.swing.JTextField jTextFieldFilePath;
    // End of variables declaration//GEN-END:variables
}
