package com.pilog.t8.developer.definitions.selectiontableview;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8Definition.T8DefinitionStatus;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.components.cellrenderers.CellRendererUtilities;
import com.pilog.t8.utilities.enums.LexicographicEnumComparator;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.swing.JTable;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionTable extends JTable
{
    private final T8DefinitionManager definitionManager;
    private final T8DefinitionTableModel tableModel;
    private final List<T8DefinitionMetaData> definitionMetaData;
    private TableColumn selectedColumn;

    public T8DefinitionTable(T8Context context)
    {
        this.definitionManager = context.getClientContext().getDefinitionManager();
        this.tableModel = new T8DefinitionTableModel();
        this.definitionMetaData = new ArrayList<>();
        setGridColor(new Color(240, 240, 240));
        setModel(tableModel);
        setRowHeight(25);
        setAutoCreateRowSorter(true);
        setCreatedTimeCellRenderer(new T8MillisecondCellRenderer(null));
        setUpdatedTimeCellRenderer(new T8MillisecondCellRenderer(null));
        setStatusComparator(new LexicographicEnumComparator<T8DefinitionStatus>());
        setLevelComparator(new LexicographicEnumComparator<T8DefinitionLevel>());
    }

    public void setSelectedColumnVisible(boolean visible)
    {
        if (!visible)
        {
            int columnIndex;

            columnIndex = tableModel.findColumn("Selected");
            if (columnIndex > -1)
            {
                TableColumn columnToRemove;

                columnToRemove = getColumnModel().getColumn(this.convertColumnIndexToView(columnIndex));
                if (columnToRemove != null)
                {
                    selectedColumn = columnToRemove;
                    getColumnModel().removeColumn(columnToRemove);
                }
            }
        }
        else
        {
            getColumnModel().addColumn(selectedColumn);
        }
    }

    public void sortByIdentifier(SortOrder sortOrder)
    {
        this.getRowSorter().setSortKeys(ArrayLists.newArrayList(new SortKey(tableModel.getIdentifierColumnIndex(), SortOrder.ASCENDING)));
    }

    public void setDefinitionHandles(List<T8DefinitionHandle> definitionHandles) throws Exception
    {
        setDefinitionMetaData(definitionManager.getDefinitionMetaData(definitionHandles));
    }

    public List<T8DefinitionMetaData> getDefinitionMetaData()
    {
        return new ArrayList<>(definitionMetaData);
    }

    public void setDefinitionMetaData(List<T8DefinitionMetaData> metaDataList)
    {
        definitionMetaData.clear();
        if (metaDataList != null) definitionMetaData.addAll(metaDataList);
        tableModel.setDefinitionMetaData(metaDataList);
        CellRendererUtilities.autoSizeTableColumns(this, 100);
    }

    public void addDefinitionMetaData(T8DefinitionMetaData metaData)
    {
        definitionMetaData.add(metaData);
        tableModel.addDefinitionMetaData(metaData);
        CellRendererUtilities.autoSizeTableColumns(this, 100);
    }

    public T8DefinitionMetaData getDefinitionMetaData(T8DefinitionHandle definitionHandle)
    {
        for (T8DefinitionMetaData metaData : definitionMetaData)
        {
            if (metaData.matchesDefinitionHandle(definitionHandle))
            {
                return metaData;
            }
        }

        return null;
    }

    public T8DefinitionMetaData removeDefinitionMetaData(T8DefinitionMetaData metaDataToRemove)
    {
        for (T8DefinitionMetaData metaData : definitionMetaData)
        {
            if (metaData == metaDataToRemove)
            {
                definitionMetaData.remove(metaData);
                tableModel.removeDefinitionMetaData(metaData);
                return metaData;
            }
        }

        return null;
    }

    public T8DefinitionMetaData removeDefinitionMetaData(T8DefinitionHandle definitionHandle)
    {
        for (T8DefinitionMetaData metaData : definitionMetaData)
        {
            if (metaData.getDefinitionHandle().equals(definitionHandle))
            {
                definitionMetaData.remove(metaData);
                tableModel.removeDefinitionMetaData(metaData);
                return metaData;
            }
        }

        return null;
    }

    public List<T8DefinitionMetaData> getCheckedDefinitionMetaData()
    {
        List<T8DefinitionMetaData> checkedMetaData;

        checkedMetaData = new ArrayList<>();
        for (T8DefinitionMetaData metaData : definitionMetaData)
        {
            if (tableModel.isSelected(metaData.getDefinitionHandle()))
            {
                checkedMetaData.add(metaData);
            }
        }

        return checkedMetaData;
    }

    public List<T8DefinitionMetaData> getSelectedDefinitionMetaData()
    {
        List<T8DefinitionMetaData> selectedMetaData;
        int[] selectedIndices;

        selectedMetaData = new ArrayList<>();
        selectedIndices = getSelectedRows();
        for (int index = 0; index < selectedIndices.length; index++)
        {
            T8DefinitionHandle selectedHandle;

            selectedHandle = tableModel.getDefinitionHandle(convertRowIndexToModel(selectedIndices[index]));
            selectedMetaData.add(getDefinitionMetaData(selectedHandle));
        }

        return selectedMetaData;
    }

    public List<T8DefinitionHandle> getCheckedDefinitionHandles()
    {
        List<T8DefinitionHandle> checkedHandles;

        checkedHandles = new ArrayList<>();
        for (T8DefinitionMetaData metaData : definitionMetaData)
        {
            if (tableModel.isSelected(metaData.getDefinitionHandle()))
            {
                checkedHandles.add(metaData.getDefinitionHandle());
            }
        }

        return checkedHandles;
    }

    public List<T8DefinitionHandle> getSelectedDefinitionHandles()
    {
        List<T8DefinitionHandle> selectedHandles;
        int[] selectedIndices;

        selectedHandles = new ArrayList<>();
        selectedIndices = getSelectedRows();
        for (int index = 0; index < selectedIndices.length; index++)
        {
            selectedHandles.add(tableModel.getDefinitionHandle(convertRowIndexToModel(selectedIndices[index])));
        }

        return selectedHandles;
    }

    /**
     * Gets all of the {@code T8DefinitionHandle} instances which are currently
     * associated with this {@code T8DefinitionTable}. Limit the use of this
     * operation, especially in cases where the number of definitions can be
     * large.
     *
     * @return The {@code List<T8DefinitionHandle} containing all of the handles
     *      for the current table view
     */
    public List<T8DefinitionHandle> getAllDefinitionHandles()
    {
        List<T8DefinitionHandle> allHandles;

        allHandles = new ArrayList<>();
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            allHandles.add(tableModel.getDefinitionHandle(convertRowIndexToModel(rowIndex)));
        }

        return allHandles;
    }

    public T8DefinitionHandle getDefinitionHandle(int rowIndex)
    {
        return tableModel.getDefinitionHandle(convertRowIndexToModel(rowIndex));
    }

    public T8DefinitionHandle getSelectedDefinitionHandle()
    {
        int[] selectedIndices;

        selectedIndices = getSelectedRows();
        if (selectedIndices.length > 0)
        {
            // Return the first selected handle.
            return tableModel.getDefinitionHandle(convertRowIndexToModel(selectedIndices[0]));
        }
        else return null;
    }

    public List<String> getSelectedIdentifiers()
    {
        List<String> selectedIdentifiers;
        int[] selectedRows;

        selectedRows = this.getSelectedRows();
        selectedIdentifiers = new ArrayList<>();
        for (int index = 0; index < selectedRows.length; index++)
        {
            selectedIdentifiers.add(tableModel.getIdentifier(convertRowIndexToModel(selectedRows[index])));
        }

        return selectedIdentifiers;
    }

    public String getSelectedIdentifier()
    {
        int selectedRow;

        selectedRow = this.getSelectedRow();
        if (selectedRow > -1)
        {
            selectedRow = convertRowIndexToModel(selectedRow);
            return selectedRow > -1 ? tableModel.getIdentifier(selectedRow) : null;
        }
        else return null;
    }

    public void setSelectedDefinition(T8DefinitionHandle definitionHandle)
    {
        int rowIndex;

        rowIndex = tableModel.getRowIndex(definitionHandle);
        if (rowIndex > -1)
        {
            rowIndex = convertRowIndexToView(rowIndex);
            if (rowIndex > -1) this.setRowSelectionInterval(rowIndex, rowIndex);
            else clearSelection();
            scrollRectToVisible(getCellRect(rowIndex,0, true));
        }
    }

    public void setIdentifierCellRenderer(TableCellRenderer renderer)
    {
        int columnIndex;

        columnIndex = convertColumnIndexToView(tableModel.getIdentifierColumnIndex());
        this.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
    }

    private void setCreatedTimeCellRenderer(TableCellRenderer renderer)
    {
        int columnIndex;

        columnIndex = convertColumnIndexToView(tableModel.getCreatedTimeColumnIndex());
        this.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
    }

    private void setUpdatedTimeCellRenderer(TableCellRenderer renderer)
    {
        int columnIndex;

        columnIndex = convertColumnIndexToView(tableModel.getUpdatedTimeColumnIndex());
        this.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
    }

    private void setStatusComparator(Comparator comparator)
    {
        int columnIndex;

        columnIndex = convertColumnIndexToView(tableModel.getStatusColumnIndex());
        ((TableRowSorter)this.getRowSorter()).setComparator(columnIndex, comparator);
    }

    private void setLevelComparator(Comparator comparator)
    {
        int columnIndex;

        columnIndex = convertColumnIndexToView(tableModel.getLevelColumnIndex());
        ((TableRowSorter)this.getRowSorter()).setComparator(columnIndex, comparator);
    }
}
