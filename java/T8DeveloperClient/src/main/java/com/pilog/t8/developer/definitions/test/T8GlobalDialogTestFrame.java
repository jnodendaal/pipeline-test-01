package com.pilog.t8.developer.definitions.test;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.ui.dialog.T8GlobalDialogDefinition;
import com.pilog.t8.developer.definitions.test.harness.T8GlobalDialogTestHarness;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.dialog.T8GlobalDialog;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class T8GlobalDialogTestFrame extends javax.swing.JFrame
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8Context testContext;
    private final T8GlobalDialogDefinition dialogDefinition;
    private final T8GlobalDialogTestHarness testHarness;
    private final Preferences preferences;
    private T8GlobalDialog dialog;

    public T8GlobalDialogTestFrame(T8Context context, T8GlobalDialogDefinition dialogDefinition, T8GlobalDialogTestHarness testHarness)
    {
        initComponents();
        this.clientContext = context.getClientContext();
        this.context = context;
        this.dialogDefinition = dialogDefinition;
        this.testHarness = testHarness;
        this.preferences = Preferences.userNodeForPackage(T8GlobalDialogTestFrame.class);

        // If a user is defined by the test harness, use it.
        testContext = new T8Context(context);
        testContext.setProjectId(dialogDefinition.getRootProjectId());

        // Instantiate a new client and add it to the UI.
        if (clientContext.getParentWindow() != null)
        {
            Dimension parentDimension = clientContext.getParentWindow().getSize();
            parentDimension.height = (int)(parentDimension.height * 0.80);
            parentDimension.width = (int)(parentDimension.width * 0.80);
            setPreferredSize(new Dimension(parentDimension.width, parentDimension.height));
        }
        else
        {
            setPreferredSize(new Dimension(600, 600));
        }

        // Initialize the frame.
        setTitle("Dialog Definition Test: " + dialogDefinition);
        pack();
        setLocationRelativeTo(clientContext.getParentWindow());

        // Set input parameters.
        setInputParameters(dialogDefinition.getInputParameterDefinitions());
    }

    private void showDialog()
    {
        try
        {
            Map<String, Object> inputParameters;
            Map<String, Object> outputParameters;
            T8DefaultComponentController testController;
            T8GlobalDialogDefinition initializedDialogDefinition;

            // Create a new test component controller.
            testController = new T8DefaultComponentController(testContext, "DIALOG_TEST", false);

            // Get the dialog input parameters.
            inputParameters = evaluateInputParameters();

            // Get the initialized dialog definition.
            initializedDialogDefinition = (T8GlobalDialogDefinition)clientContext.getDefinitionManager().initializeDefinition(testContext, dialogDefinition, inputParameters);

            // Initialize the dialog.
            dialog = (T8GlobalDialog)initializedDialogDefinition.getNewComponentInstance(testController);
            dialog.initializeComponent(inputParameters);

            // Start the dialog.
            dialog.startComponent();

            // Show the dialog.
            dialog.openDialog();

            // Get the dialog output parameters (this point will only be reached once the dialog is disposed).
            outputParameters = dialog.getOutputParameters();
            T8Log.log("Dialog output parameters: " + outputParameters);

            // Stop the dialog.
            dialog.stopComponent();

            // Request focus in the dialog.
            requestFocus();
            transferFocus();

            // Show the dialog output.
            jTabbedPaneContent.setSelectedIndex(1);
            setOutputParameters(dialogDefinition.getOutputParameterDefinitions(), outputParameters);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while initializing test global dialog: " + dialogDefinition, e);
        }
    }

    private Map<String, Object> evaluateInputParameters()
    {
        T8ClientExpressionEvaluator expressionEvaluator;
        Map<String, Object> inputParameters;
        Map<String, String> inputParameterExpressions;
        TableModel model;

        // Stop editing.
        if (jTableInputParameters.getCellEditor() != null) jTableInputParameters.getCellEditor().stopCellEditing();

        model = jTableInputParameters.getModel();
        expressionEvaluator = new T8ClientExpressionEvaluator(context);
        inputParameters = new HashMap<String, Object>();
        inputParameterExpressions = new HashMap<String, String>();
        for (int rowIndex = 0; rowIndex < model.getRowCount(); rowIndex++)
        {
            String parameterIdentifier;
            String valueExpression;
            Object value;

            parameterIdentifier = (String)model.getValueAt(rowIndex, 0);
            valueExpression = (String)model.getValueAt(rowIndex, 1);

            if (Strings.isNullOrEmpty(valueExpression))
            {
                value = null;
            }
            else
            {
                try
                {
                    value = expressionEvaluator.evaluateExpression(valueExpression, null, null);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while evaluating value expression for dialog input parameter: " + parameterIdentifier, e);
                    value = null;
                }
            }

            inputParameterExpressions.put(parameterIdentifier, valueExpression);
            inputParameters.put(dialogDefinition.getIdentifier() + parameterIdentifier, value);
        }

        // Update the preferences.
        setInputParameterPreferences(inputParameterExpressions);
        return inputParameters;
    }

    private void setInputParameterPreferences(Map<String, String> inputParameterExpressions)
    {
        StringBuffer parameterString;

        parameterString = new StringBuffer();
        if (inputParameterExpressions != null)
        {
            for (String parameterIdentifier : inputParameterExpressions.keySet())
            {
                String expression;

                expression = inputParameterExpressions.get(parameterIdentifier);

                parameterString.append(parameterIdentifier);
                parameterString.append("```");
                parameterString.append(Strings.isNullOrEmpty(expression) ? "" : expression);
                parameterString.append("```");
            }
        }

        preferences.put(dialogDefinition.getIdentifier(), parameterString.toString());
    }

    private Map<String, String> getInputParameterPreferences()
    {
        String parameterString;

        parameterString = preferences.get(dialogDefinition.getIdentifier(), null);
        if (!Strings.isNullOrEmpty(parameterString))
        {
            try
            {
                Map<String, String> parameterPreferences;
                String[] splits;

                splits = parameterString.split("```");
                parameterPreferences = new HashMap<String, String>();
                for (int index = 0; index < splits.length -1; index += 2)
                {
                    String parameterIdentifier;
                    String expression;

                    parameterIdentifier = splits[index];
                    expression = splits[index+1];
                    parameterPreferences.put(parameterIdentifier, expression);
                }

                return parameterPreferences;
            }
            catch (Exception e)
            {
                T8Log.log("Exception while parsing input parameter preferences.", e);
                return null;
            }
        }
        else return null;
    }

    private void setInputParameters(List<T8DataParameterDefinition> parameterDefinitions)
    {
        DefaultTableModel model;
        Map<String, String> inputParameterPreferences;

        // Get any previously set preferences.
        inputParameterPreferences = getInputParameterPreferences();

        // Get the model to update.
        model = (DefaultTableModel)jTableInputParameters.getModel();

        // Clear the table model.
        while (model.getRowCount() > 0)
        {
            model.removeRow(0);
        }

        // Add all input parameters to the model.
        for (T8DataParameterDefinition parameterDefinition :  parameterDefinitions)
        {
            Object[] rowData;

            rowData = new Object[3];
            rowData[0] = parameterDefinition.getIdentifier();
            rowData[1] = inputParameterPreferences != null ? inputParameterPreferences.get(parameterDefinition.getIdentifier()) : null;
            model.addRow(rowData);
        }
    }

    private void setOutputParameters(List<T8DataParameterDefinition> outputParameterDefinitions, Map<String, Object> outputParameters)
    {
        DefaultTableModel model;

        // Get the model to update.
        model = (DefaultTableModel)jTableOutputParameters.getModel();

        // Clear the table model.
        while (model.getRowCount() > 0)
        {
            model.removeRow(0);
        }

        // Add all output parameters to the model.
        for (T8DataParameterDefinition parameterDefinition :  outputParameterDefinitions)
        {
            String parameterIdentifier;
            Object[] rowData;
            Object value;

            parameterIdentifier = parameterDefinition.getIdentifier();

            rowData = new Object[3];
            rowData[0] = parameterIdentifier;

            value = outputParameters != null ? outputParameters.get(dialogDefinition.getNamespace() + parameterIdentifier) : null;
            if (value == null)
            {
                rowData[1] = null;
                rowData[2] = "null";
            }
            else
            {
                rowData[1] = value.toString();
                rowData[2] = value.getClass().getSimpleName();
            }

            model.addRow(rowData);
        }
    }

    public static final void testDialogDefinition(T8Context context, T8GlobalDialogDefinition dialogDefinition, T8GlobalDialogTestHarness testHarness)
    {
        T8GlobalDialogTestFrame testFrame;

        testFrame = new T8GlobalDialogTestFrame(context, dialogDefinition, testHarness);
        testFrame.setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jTabbedPaneContent = new javax.swing.JTabbedPane();
        jPanelInput = new javax.swing.JPanel();
        jScrollPaneInputParameters = new javax.swing.JScrollPane();
        jTableInputParameters = new javax.swing.JTable();
        jPanelInputControls = new javax.swing.JPanel();
        jButtonOpenDialog = new javax.swing.JButton();
        jPanelOutput = new javax.swing.JPanel();
        jScrollPaneOutputParameters = new javax.swing.JScrollPane();
        jTableOutputParameters = new javax.swing.JTable();
        jPanelOutputControls = new javax.swing.JPanel();
        jButtonClose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("T8Developer Dialog Test");

        jPanelInput.setLayout(new java.awt.BorderLayout());

        jTableInputParameters.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Parameter Identifier", "Value Expression"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableInputParameters.setRowHeight(25);
        jScrollPaneInputParameters.setViewportView(jTableInputParameters);

        jPanelInput.add(jScrollPaneInputParameters, java.awt.BorderLayout.CENTER);

        jPanelInputControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 5, 1));

        jButtonOpenDialog.setText("Open Dialog");
        jButtonOpenDialog.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButtonOpenDialog.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonOpenDialogActionPerformed(evt);
            }
        });
        jPanelInputControls.add(jButtonOpenDialog);

        jPanelInput.add(jPanelInputControls, java.awt.BorderLayout.PAGE_END);

        jTabbedPaneContent.addTab("Dialog Input", jPanelInput);

        jPanelOutput.setLayout(new java.awt.BorderLayout());

        jTableOutputParameters.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Parameter Identifier", "Value", "Value Type"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableOutputParameters.setRowHeight(25);
        jScrollPaneOutputParameters.setViewportView(jTableOutputParameters);

        jPanelOutput.add(jScrollPaneOutputParameters, java.awt.BorderLayout.CENTER);

        jPanelOutputControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 5, 1));

        jButtonClose.setText("Close");
        jButtonClose.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCloseActionPerformed(evt);
            }
        });
        jPanelOutputControls.add(jButtonClose);

        jPanelOutput.add(jPanelOutputControls, java.awt.BorderLayout.PAGE_END);

        jTabbedPaneContent.addTab("Dialog Output", jPanelOutput);

        getContentPane().add(jTabbedPaneContent, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonOpenDialogActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonOpenDialogActionPerformed
    {//GEN-HEADEREND:event_jButtonOpenDialogActionPerformed
        showDialog();
    }//GEN-LAST:event_jButtonOpenDialogActionPerformed

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButtonCloseActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JButton jButtonOpenDialog;
    private javax.swing.JPanel jPanelInput;
    private javax.swing.JPanel jPanelInputControls;
    private javax.swing.JPanel jPanelOutput;
    private javax.swing.JPanel jPanelOutputControls;
    private javax.swing.JScrollPane jScrollPaneInputParameters;
    private javax.swing.JScrollPane jScrollPaneOutputParameters;
    private javax.swing.JTabbedPane jTabbedPaneContent;
    private javax.swing.JTable jTableInputParameters;
    private javax.swing.JTable jTableOutputParameters;
    // End of variables declaration//GEN-END:variables
}
