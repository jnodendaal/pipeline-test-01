package com.pilog.t8.developer.definitions.actionhandlers.service.quartz;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceAPIHandler;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8QuartzServiceDefinitionActionHandler implements T8DefinitionActionHandler
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8QuartzTaskDefinitionActionHandler.class.getName());

    private T8ClientContext clientContext;
    private T8Context context;
    private T8QuartzServiceDefinition definition;

    public T8QuartzServiceDefinitionActionHandler(T8Context context, T8QuartzServiceDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>(1);
        actions.add(new T8QuartzServiceDefinitionActionHandler.RestartService(context, definition));

        return actions;
    }

    private static class RestartService extends AbstractAction
    {
        private T8ClientContext clientContext;
        private T8Context context;
        private T8QuartzServiceDefinition definition;

        public RestartService(T8Context context, T8QuartzServiceDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Restart");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                clientContext.getServiceManager().executeServiceOperation(context, definition.getIdentifier(), T8QuartzServiceAPIHandler.OPERATION_RESTART, null);
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to restart service", ex);
            }
        }
    }
}
