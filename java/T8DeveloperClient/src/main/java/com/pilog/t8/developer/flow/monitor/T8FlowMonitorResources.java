/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.developer.flow.monitor;

/**
 *
 * @author Andre Scheepers
 */
public class T8FlowMonitorResources
{
    //Flow State Fields.
    public static final String EF_FLOW_IID = "$FLOW_IID";
    public static final String EF_FLOW_ID = "$FLOW_ID";
    public static final String EF_FLOW_PARAMETERS = "$FLOW_PARAMETERS";
    public static final String EF_NODE_IID = "$NODE_IID";
    public static final String EF_NODE_ID = "$NODE_ID";
    public static final String EF_TASK_IID = "$TASK_IID";
    public static final String EF_TASK_ID = "$TASK_ID";
    public static final String EF_INPUT_NODE_ID = "$INPUT_NODE_ID";

    public static final String EF_OUTPUT_NODE_IID = "$OUTPUT_NODE_IID";
    public static final String EF_OUTPUT_NODE_ID = "$OUTPUT_NODE_ID";

    //User Fields.
    public static final String EF_INITIATOR_ID = "$INITIATOR_ID";
    public static final String EF_INITIATOR_IID = "$INITIATOR_IID";
    public static final String EF_INITIATOR_ORG_ID = "$INITIATOR_ORG_ID";
    public static final String EF_RESTRICTION_USER_ID = "$RESTRICTION_USER_ID";
    public static final String EF_RESTRICTION_PROFILE_ID = "$RESTRICTION_PROFILE_ID";
    public static final String EF_CLAIMED_BY_USER_ID = "$CLAIMED_BY_USER_ID";
    public static final String EF_USER_ID = "$USER_ID";

    //General Fields.
    public static final String EF_COMPLETED_EXECUTION_STEPS = "$COMPLETED_EXECUTION_STEPS";
    public static final String EF_PRIORITY = "$PRIORITY";
    public static final String EF_LANGUAGE_ID = "$LANGUAGE_ID";
    public static final String EF_STATUS = "$STATUS";
    public static final String EF_TYPE = "$TYPE";
    public static final String EF_INFORMATION = "$INFORMATION";

    //Paramter FIelds.
    public static final String EF_PARENT_PARAMETER_IID = "$PARENT_PARAMETER_IID";
    public static final String EF_PARAMETER_IID = "$PARAMETER_IID";
    public static final String EF_PARAMETER_ID = "$PARAMETER_ID";
    public static final String EF_PARAMETER_KEY = "$PARAMETER_KEY";
    public static final String EF_SEQUENCE = "$SEQUENCE";
    public static final String EF_VALUE = "$VALUE";

    //Property FIelds.
    public static final String EF_PARENT_PROPERTY_IID = "$PARENT_PROPERTY_IID";
    public static final String EF_PROPERTY_IID = "$PROPERTY_IID";
    public static final String EF_PROPERTY_KEY = "$PROPERTY_KEY";

    //Time Fields.
    public static final String EF_TIME = "$TIME";
    public static final String EF_TIME_ACTIVATED = "$TIME_ACTIVATED";
    public static final String EF_TIME_STARTED = "$TIME_STARTED";
    public static final String EF_TIME_CLAIMED = "$TIME_CLAIMED";
    public static final String EF_TIME_ISSUED = "$TIME_ISSUED";
    public static final String EF_TIME_COMPLETED = "$TIME_COMPLETED";

    //Key Fields
    public static final String EF_KEY_ID = "$KEY_ID";
    public static final String EF_KEY_FLOW_IID = "$KEY_FLOW_IID";
    public static final String EF_KEY_FLOW_ID = "$KEY_FLOW_ID";
    public static final String EF_KEY_NODE_IID = "$KEY_NODE_IID";
    public static final String EF_KEY_NODE_ID = "$KEY_NODE_ID";
    public static final String EF_KEY_TASK_IID = "$KEY_TASK_IID";
    public static final String EF_KEY_TASK_ID = "$KEY_TASK_ID";
    public static final String EF_KEY_DATA_OBJECT_IID = "$KEY_DATA_OBJECT_IID";
    public static final String EF_KEY_DATA_OBJECT_ID = "$KEY_DATA_OBJECT_ID";
    public static final String EF_KEY_SIGNAL_ID = "$KEY_SIGNAL_ID";

    //Event Parameter Fields.
    public static final String EF_EVENT_IID = "$EVENT_IID";
    public static final String EF_EVENT = "$EVENT";
    public static final String EF_EVENT_PARAMETERS = "$EVENT_PARAMETERS";
    public static final String EF_IDENTIFIER = "$IDENTIFIER";
    public static final String P_DATA_ENTITY_LIST = "$CP_DATA_ENTITY_LIST";
}
