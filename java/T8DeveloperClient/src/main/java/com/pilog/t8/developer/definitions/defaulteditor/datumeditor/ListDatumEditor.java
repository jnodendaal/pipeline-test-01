package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.google.common.base.Strings;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumOption.DisplayNameComparator;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.developer.utils.ListRowFilter;
import com.pilog.t8.utilities.components.list.ListPopupMenuListener;
import com.pilog.t8.utilities.components.list.popupmenu.SearchableListPopup;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.DefaultRowSorter;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import org.jdesktop.swingx.JXSearchField;
import org.jdesktop.swingx.sort.ListSortController;

/**
 * @author Bouwer du Preez
 */
public class ListDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private DefaultRowSorter rowSorter;
    private ListRowFilter rowFilter;

    public ListDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        jListElements.setModel(new DefaultListModel());
    }

    @Override
    public void initializeComponent()
    {
        rowFilter = new ListRowFilter();
        rowSorter = new ListSortController(jListElements.getModel());
        rowSorter.setRowFilter(rowFilter);
        jListElements.setRowSorter(rowSorter);

        jXSearchField.setInstantSearchDelay(100);
        jXSearchField.setSearchMode(JXSearchField.SearchMode.INSTANT);
        jXSearchField.setFindAction(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                boolean canMoveItems;

                // We want to prevent the items from being moved while the list is filtered
                canMoveItems = Strings.isNullOrEmpty(jXSearchField.getText());
                jButtonMoveUp.setEnabled(canMoveItems);
                jButtonMoveDown.setEnabled(canMoveItems);

                rowFilter.setFilterString(jXSearchField.getText());

                rowSorter.sort();
                revalidate();
            }

        });
        jXSearchField.addActionListener(jXSearchField.getFindAction());
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void commitChanges()
    {
        ListModel model;
        ArrayList elements;

        elements = new ArrayList();
        model = jListElements.getModel();
        for (int elementIndex = 0; elementIndex < model.getSize(); elementIndex++)
        {
            elements.add(model.getElementAt(elementIndex));
        }

        setDefinitionDatum(elements);
    }

    @Override
    public void setEditable(boolean editable)
    {
        jButtonAdd.setEnabled(editable);
        jButtonRemove.setEnabled(editable);
        jButtonMoveUp.setEnabled(editable);
        jButtonMoveDown.setEnabled(editable);
        jButtonSortAlphabetic.setEnabled(editable);
    }

    @Override
    public void refreshEditor()
    {
        DefaultListModel model;
        Object value;

        value = getDefinitionDatum();
        model = ((DefaultListModel)jListElements.getModel());
        model.removeAllElements();
        if (value != null)
        {
            List list;

            list = (List)value;
            for (Object element : list)
            {
                model.addElement(element);
            }
        }

        checkSize();
    }

    private void addElement()
    {
        try
        {
            ArrayList<T8DefinitionDatumOption> datumOptions;

            // If any list of options is provided, we treat this datum as a controlled list.  If a null object is returned, we allow the user to enter any value.
            datumOptions = definition.getDatumOptions(definitionContext, datumType.getIdentifier());
            if (datumOptions != null)
            {
                SearchableListPopup<T8DefinitionDatumOption> popupMenu;

                Collections.sort(datumOptions, new DisplayNameComparator());

                popupMenu = new SearchableListPopup<>(datumOptions);
                popupMenu.addPopupListener(new PopupMenuListener());
                popupMenu.show(jButtonAdd, 0, 0);
            }
            else
            {
                String value;

                value = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter a new value to add to the list.", "Datum Value Entry", JOptionPane.INFORMATION_MESSAGE);
                if (value != null)
                {
                    addElement(value);
                }
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while loading datum '" + datumType.getIdentifier() + "' options.", e);
        }
    }

    private void addElement(Object value)
    {
        DefaultListModel model;

        model = (DefaultListModel)jListElements.getModel();
        model.addElement(value);
        checkSize();
    }

    private void removeSelectedElements()
    {
        DefaultListModel model;
        int selectedIndex;

        model = (DefaultListModel)jListElements.getModel();
        while ((selectedIndex = jListElements.getSelectedIndex()) != -1)
        {
            model.remove(jListElements.convertIndexToModel(selectedIndex));
        }
        checkSize();
    }

    private List<String> getAllValues()
    {
        List<String> values;
        DefaultListModel model;

        values = new ArrayList<String>();
        model = (DefaultListModel)jListElements.getModel();
        for (int elementIndex = 0; elementIndex < model.getSize(); elementIndex++)
        {
            values.add((String)model.getElementAt(elementIndex));
        }

        return values;
    }

    private void moveSelectionUp()
    {
        List<String> values;
        int[] selectedIndices;

        values = getAllValues();
        selectedIndices = jListElements.getSelectedIndices();
        if (selectedIndices.length > 0)
        {
            String selectedValue;

            // Decrement all selected Indices.
            for (int selectedIndex : selectedIndices)
            {
                if (selectedIndex > 0)
                {
                    selectedValue = values.remove(selectedIndex);
                    values.add(selectedIndex-1, selectedValue);
                }
            }

            // Commit the updated value list.
            setDefinitionDatum(values);

            // Refresh the editor.
            refreshEditor();

            // Decrement all selected indices (because they've been moved up).
            jListElements.clearSelection();
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]-1);
                jListElements.getSelectionModel().addSelectionInterval(selectedIndices[indexIndex], selectedIndices[indexIndex]);
            }
        }
    }

    private void moveSelectionDown()
    {
        List<String> values;
        int[] selectedIndices;

        values = getAllValues();
        selectedIndices = jListElements.getSelectedIndices();
        if (selectedIndices.length > 0)
        {
            String selectedValue;

            // Decrement all selected Indices.
            for (int selectedIndex : selectedIndices)
            {
                if (selectedIndex < values.size() -1)
                {
                    selectedValue = values.remove(selectedIndex);
                    values.add(selectedIndex+1, selectedValue);
                }
            }

            // Commit the updated value list.
            setDefinitionDatum(values);

            // Refresh the editor.
            refreshEditor();

            // Increment all selected indices (because they've been moved down).
            jListElements.clearSelection();
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]+1);
                jListElements.getSelectionModel().addSelectionInterval(selectedIndices[indexIndex], selectedIndices[indexIndex]);
            }
        }
    }

    private void sortAlphabetic()
    {
        List<String> values;

        values = getAllValues();
        Collections.sort(values);

        // Commit the updated value list.
        setDefinitionDatum(values);

        // Refresh the editor.
        refreshEditor();
    }

    private class PopupMenuListener implements ListPopupMenuListener
    {
        @Override
        public void popupItemSelected(Object item)
        {
            if (item != null)
            {
                addElement(((T8DefinitionDatumOption)item).getValue());
            }
        }

        @Override
        public void popupItemsSelected(Object[] selectedItems)
        {
            for (Object item : selectedItems)
            {
                popupItemSelected(item);
            }
        }
    }

    private void checkSize()
    {
        DefaultListModel model = (DefaultListModel)jListElements.getModel();
        if(model.size() > 10)
            jListElements.setVisibleRowCount(10);
        else if(model.size() < 1)
            jListElements.setVisibleRowCount(1);
        else
            jListElements.setVisibleRowCount(model.size());
        revalidate();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarList = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jButtonRemove = new javax.swing.JButton();
        jButtonMoveDown = new javax.swing.JButton();
        jButtonMoveUp = new javax.swing.JButton();
        jButtonSortAlphabetic = new javax.swing.JButton();
        jXSearchField = new org.jdesktop.swingx.JXSearchField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListElements = new org.jdesktop.swingx.JXList();

        setMinimumSize(new java.awt.Dimension(112, 100));
        setLayout(new java.awt.GridBagLayout());

        jToolBarList.setFloatable(false);
        jToolBarList.setRollover(true);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setToolTipText("Add an element to the list");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonAdd);

        jButtonRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeDocumentIcon.png"))); // NOI18N
        jButtonRemove.setText("Remove");
        jButtonRemove.setToolTipText("Remove selected elements from the list");
        jButtonRemove.setFocusable(false);
        jButtonRemove.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonRemove);

        jButtonMoveDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowDownIcon.png"))); // NOI18N
        jButtonMoveDown.setText("Down");
        jButtonMoveDown.setFocusable(false);
        jButtonMoveDown.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveDown.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveDownActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonMoveDown);

        jButtonMoveUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowUpIcon.png"))); // NOI18N
        jButtonMoveUp.setText("Up");
        jButtonMoveUp.setFocusable(false);
        jButtonMoveUp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveUp.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveUpActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonMoveUp);

        jButtonSortAlphabetic.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/sortAlphabeticIcon.png"))); // NOI18N
        jButtonSortAlphabetic.setText("Sort");
        jButtonSortAlphabetic.setFocusable(false);
        jButtonSortAlphabetic.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSortAlphabetic.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSortAlphabeticActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonSortAlphabetic);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarList, gridBagConstraints);

        jXSearchField.setMinimumSize(new java.awt.Dimension(50, 25));
        jXSearchField.setPreferredSize(new java.awt.Dimension(200, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 0, 2);
        add(jXSearchField, gridBagConstraints);

        jScrollPane1.setViewportView(jListElements);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jScrollPane1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        addElement();
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveActionPerformed
        removeSelectedElements();
    }//GEN-LAST:event_jButtonRemoveActionPerformed

    private void jButtonMoveDownActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveDownActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveDownActionPerformed
        moveSelectionDown();
    }//GEN-LAST:event_jButtonMoveDownActionPerformed

    private void jButtonMoveUpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveUpActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveUpActionPerformed
        moveSelectionUp();
    }//GEN-LAST:event_jButtonMoveUpActionPerformed

    private void jButtonSortAlphabeticActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSortAlphabeticActionPerformed
    {//GEN-HEADEREND:event_jButtonSortAlphabeticActionPerformed
        sortAlphabetic();
    }//GEN-LAST:event_jButtonSortAlphabeticActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonMoveDown;
    private javax.swing.JButton jButtonMoveUp;
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JButton jButtonSortAlphabetic;
    private org.jdesktop.swingx.JXList jListElements;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBarList;
    private org.jdesktop.swingx.JXSearchField jXSearchField;
    // End of variables declaration//GEN-END:variables
}
