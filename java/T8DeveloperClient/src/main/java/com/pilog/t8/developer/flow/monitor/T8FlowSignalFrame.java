package com.pilog.t8.developer.flow.monitor;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.signal.T8FlowSignalDefinition;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class T8FlowSignalFrame extends JFrame
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8FlowManager flowManager;
    private final T8DefinitionManager definitionManager;
    private final Preferences preferences;
    private T8FlowSignalDefinition signalDefinition;

    public T8FlowSignalFrame(T8Context context)
    {
        initComponents();
        this.clientContext = context.getClientContext();
        this.context = context;
        this.flowManager = clientContext.getFlowManager();
        this.definitionManager = clientContext.getDefinitionManager();
        this.preferences = Preferences.userNodeForPackage(T8FlowSignalFrame.class);

        // Instantiate a new client and add it to the UI.
        if (clientContext.getParentWindow() != null)
        {
            Dimension parentDimension = clientContext.getParentWindow().getSize();
            parentDimension.height = (int)(parentDimension.height * 0.80);
            parentDimension.width = (int)(parentDimension.width * 0.80);
            setPreferredSize(new Dimension(parentDimension.width, parentDimension.height));
        }
        else
        {
            setPreferredSize(new Dimension(600, 600));
        }

        // Initialize the frame.
        setTitle("Flow Signal Broadcast");
        pack();
        setLocationRelativeTo(clientContext.getParentWindow());
        populateSignalComboBox();
    }

    private void populateSignalComboBox()
    {
        try
        {
            List<String> signalIds;

            jComboBoxSignal.removeAllItems();

            signalIds = definitionManager.getGroupDefinitionIdentifiers(signalDefinition.getRootProjectId(), T8FlowSignalDefinition.GROUP_IDENTIFIER);
            for (String signalId : signalIds)
            {
                jComboBoxSignal.addItem(signalId);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while retrieving flow signal identifiers.", e);
        }
    }

    private String getSelectedSignalId()
    {
        return (String)jComboBoxSignal.getSelectedItem();
    }

    private void sendSignal()
    {
        String signalId;

        signalId = getSelectedSignalId();
        if (signalId != null)
        {
            try
            {
                Map<String, Object> signalParameters;

                // Get the signal parameters.
                signalParameters = evaluateSignalParameters();

                // Fire the signal.
                T8Log.log("Sending flow signal " + signalId + " using parameters: " + signalParameters);
                flowManager.fireSignalEvent(context, signalId, signalParameters);
                Toast.show("Signal Sent Successfully", Toast.Style.SUCCESS);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while sending flow signal: " + signalId, e);
            }
        }
    }

    private Map<String, Object> evaluateSignalParameters()
    {
        T8ClientExpressionEvaluator expressionEvaluator;
        Map<String, Object> inputParameters;
        Map<String, String> inputParameterExpressions;
        TableModel model;

        // Stop editing.
        if (jTableInputParameters.getCellEditor() != null) jTableInputParameters.getCellEditor().stopCellEditing();

        model = jTableInputParameters.getModel();
        expressionEvaluator = new T8ClientExpressionEvaluator(context);
        inputParameters = new HashMap<String, Object>();
        inputParameterExpressions = new HashMap<String, String>();
        for (int rowIndex = 0; rowIndex < model.getRowCount(); rowIndex++)
        {
            String parameterIdentifier;
            String valueExpression;
            Object value;

            parameterIdentifier = (String)model.getValueAt(rowIndex, 0);
            valueExpression = (String)model.getValueAt(rowIndex, 1);

            if (Strings.isNullOrEmpty(valueExpression))
            {
                value = null;
            }
            else
            {
                try
                {
                    value = expressionEvaluator.evaluateExpression(valueExpression, null, null);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while evaluating value expression for dialog input parameter: " + parameterIdentifier, e);
                    value = null;
                }
            }

            inputParameterExpressions.put(parameterIdentifier, valueExpression);
            inputParameters.put(signalDefinition.getIdentifier() + parameterIdentifier, value);
        }

        // Update the preferences.
        setSignalParameterPreferences(inputParameterExpressions);
        return inputParameters;
    }

    private void setSignalParameterPreferences(Map<String, String> inputParameterExpressions)
    {
        StringBuffer parameterString;

        parameterString = new StringBuffer();
        if (inputParameterExpressions != null)
        {
            for (String parameterIdentifier : inputParameterExpressions.keySet())
            {
                String expression;

                expression = inputParameterExpressions.get(parameterIdentifier);

                parameterString.append(parameterIdentifier);
                parameterString.append("```");
                parameterString.append(Strings.isNullOrEmpty(expression) ? "" : expression);
                parameterString.append("```");
            }
        }

        preferences.put(signalDefinition.getIdentifier(), parameterString.toString());
    }

    private Map<String, String> getInputParameterPreferences()
    {
        if (signalDefinition != null)
        {
            String parameterString;

            parameterString = preferences.get(signalDefinition.getIdentifier(), null);
            if (!Strings.isNullOrEmpty(parameterString))
            {
                try
                {
                    Map<String, String> parameterPreferences;
                    String[] splits;

                    splits = parameterString.split("```");
                    parameterPreferences = new HashMap<String, String>();
                    for (int index = 0; index < splits.length -1; index += 2)
                    {
                        String parameterIdentifier;
                        String expression;

                        parameterIdentifier = splits[index];
                        expression = splits[index+1];
                        parameterPreferences.put(parameterIdentifier, expression);
                    }

                    return parameterPreferences;
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while parsing input parameter preferences.", e);
                    return null;
                }
            }
            else return null;
        }
        else return null;
    }

    private void handleSignalSelectionChange()
    {
        String signalId;

        signalId = getSelectedSignalId();
        if (signalId != null)
        {
            if ((signalDefinition == null) || (!signalId.equals(signalDefinition.getIdentifier())))
            {
                try
                {
                    signalDefinition = (T8FlowSignalDefinition)definitionManager.getRawDefinition(context, null, signalId);
                    if (signalDefinition != null)
                    {
                        setSignalParameters(signalDefinition.getParameterDefinitions());
                    }
                }
                catch (Exception e)
                {
                    T8Log.log("Exception whle retrieving signal definition: " + signalId, e);
                    signalDefinition = null;
                }
            }
        }
        else
        {
            signalDefinition = null;
        }
    }

    private void setSignalParameters(List<T8DataParameterDefinition> parameterDefinitions)
    {
        DefaultTableModel model;
        Map<String, String> inputParameterPreferences;

        // Get any previously set preferences.
        inputParameterPreferences = getInputParameterPreferences();

        // Get the model to update.
        model = (DefaultTableModel)jTableInputParameters.getModel();

        // Clear the table model.
        while (model.getRowCount() > 0)
        {
            model.removeRow(0);
        }

        // Add all input parameters to the model.
        for (T8DataParameterDefinition parameterDefinition :  parameterDefinitions)
        {
            Object[] rowData;

            rowData = new Object[3];
            rowData[0] = parameterDefinition.getIdentifier();
            rowData[1] = inputParameterPreferences != null ? inputParameterPreferences.get(parameterDefinition.getIdentifier()) : null;
            model.addRow(rowData);
        }
    }

    public static final void showFlowSignalFrame(T8Context context)
    {
        T8FlowSignalFrame signalFrame;

        signalFrame = new T8FlowSignalFrame(context);
        signalFrame.setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelSignal = new javax.swing.JLabel();
        jComboBoxSignal = new javax.swing.JComboBox<>();
        jTabbedPaneContent = new javax.swing.JTabbedPane();
        jPanelInput = new javax.swing.JPanel();
        jScrollPaneInputParameters = new javax.swing.JScrollPane();
        jTableInputParameters = new javax.swing.JTable();
        jPanelInputControls = new javax.swing.JPanel();
        jButtonSendSignal = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("T8Developer Dialog Test");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabelSignal.setText("Signal:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        getContentPane().add(jLabelSignal, gridBagConstraints);

        jComboBoxSignal.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboBoxSignalItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 10);
        getContentPane().add(jComboBoxSignal, gridBagConstraints);

        jPanelInput.setLayout(new java.awt.BorderLayout());

        jTableInputParameters.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Parameter Identifier", "Value Expression"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableInputParameters.setRowHeight(25);
        jScrollPaneInputParameters.setViewportView(jTableInputParameters);

        jPanelInput.add(jScrollPaneInputParameters, java.awt.BorderLayout.CENTER);

        jPanelInputControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 5, 1));

        jButtonSendSignal.setText("Send Signal");
        jButtonSendSignal.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButtonSendSignal.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSendSignalActionPerformed(evt);
            }
        });
        jPanelInputControls.add(jButtonSendSignal);

        jPanelInput.add(jPanelInputControls, java.awt.BorderLayout.PAGE_END);

        jTabbedPaneContent.addTab("Signal Parameters", jPanelInput);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 10, 10);
        getContentPane().add(jTabbedPaneContent, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSendSignalActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSendSignalActionPerformed
    {//GEN-HEADEREND:event_jButtonSendSignalActionPerformed
        sendSignal();
    }//GEN-LAST:event_jButtonSendSignalActionPerformed

    private void jComboBoxSignalItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboBoxSignalItemStateChanged
    {//GEN-HEADEREND:event_jComboBoxSignalItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            handleSignalSelectionChange();
        }
    }//GEN-LAST:event_jComboBoxSignalItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonSendSignal;
    private javax.swing.JComboBox<String> jComboBoxSignal;
    private javax.swing.JLabel jLabelSignal;
    private javax.swing.JPanel jPanelInput;
    private javax.swing.JPanel jPanelInputControls;
    private javax.swing.JScrollPane jScrollPaneInputParameters;
    private javax.swing.JTabbedPane jTabbedPaneContent;
    private javax.swing.JTable jTableInputParameters;
    // End of variables declaration//GEN-END:variables
}
