package com.pilog.t8.developer.definitions.projectmanager;

import com.pilog.graph.model.DefaultGraphEdge;
import com.pilog.graph.model.DefaultGraphModel;
import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphEdge.EdgeType;
import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.model.GraphVertexPort;
import com.pilog.graph.view.GraphEdgeRenderer;
import com.pilog.graph.view.GraphVertexRenderer;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.developer.definitions.projectmanager.T8ProjectGraphConfiguration.LayoutType;
import com.pilog.t8.security.T8Context;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectGraphModelHandler
{
    public static final String EDGE_TYPE_PREFERENCE = "GRAPH_EDGE_TYPE_PREFERENCE";
    public static final String LAYOUT_TYPE_PREFERENCE = "GRAPH_LAYOUT_TYPE_PREFERENCE";

    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final T8ProjectVertexRenderer vertexRenderer;
    private final T8ProjectDependencyEdgeRenderer edgeRenderer;
    private final T8ProjectGraphConfiguration config;
    private final Preferences preferences;
    private DefaultGraphModel model;

    public T8ProjectGraphModelHandler(T8Context context)
    {
        this.preferences = Preferences.userNodeForPackage(T8ProjectGraphModelHandler.class);
        this.context = context;
        this.definitionManager = context.getClientContext().getDefinitionManager();
        this.config = new T8ProjectGraphConfiguration();
        this.config.setLayoutType(LayoutType.FREE);
        this.config.setEdgeType(EdgeType.STRAIGHT);
        this.vertexRenderer = new T8ProjectVertexRenderer(config);
        this.edgeRenderer = new T8ProjectDependencyEdgeRenderer();
    }

    public GraphVertexRenderer getVertextRenderer()
    {
        return vertexRenderer;
    }

    public GraphEdgeRenderer getEdgeRenderer()
    {
        return edgeRenderer;
    }

    public DefaultGraphModel buildGraphModel(List<T8ProjectDefinition> projectDefinitions)
    {
        // Build the model and return it.
        model = new DefaultGraphModel();

        // Add all vertices.
        for (T8ProjectDefinition projectDefinition : projectDefinitions)
        {
            T8ProjectVertex projectVertex;
            String projectId;

            projectId = projectDefinition.getIdentifier();
            projectVertex = new T8ProjectVertex(projectId, vertexRenderer);
            projectVertex.setVertexMovable(true);

            // Add the vertext to the model.
            model.addVertex(projectVertex);
        }

        // Add all edges.
        for (T8ProjectDefinition projectDefinition : projectDefinitions)
        {
            List<String> requiredProjectIds;
            String projectId;

            projectId = projectDefinition.getIdentifier();
            requiredProjectIds = projectDefinition.getRequiredProjectIds();
            for (String requiredProjectId : requiredProjectIds)
            {
                T8ProjectVertex requiredVertex;
                T8ProjectVertex dependentVertex;

                requiredVertex = (T8ProjectVertex)model.getVertex(requiredProjectId);
                dependentVertex = (T8ProjectVertex)model.getVertex(projectId);
                if (requiredVertex != null)
                {
                    model.addEdge(createEdge(dependentVertex, requiredVertex));
                }
            }
        }

        return model;
    }

    public void updateGraphModel(List<T8ProjectDefinition> projectDefinitions)
    {
        List<T8ProjectDefinition> newProjects;

        // Remove all vertices that are not part of the newly specified project list.
        for (GraphVertex vertex : model.getVertices())
        {
            String projectId;
            boolean found;

            found = false;
            projectId = vertex.getId();
            for (T8ProjectDefinition projectDefinition : projectDefinitions)
            {
                if (projectId.equals(projectDefinition.getIdentifier()))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                model.removeVertex(vertex);
            }
        }

        // Add all vertices that are not currently part of the model.
        newProjects = new ArrayList<>();
        for (T8ProjectDefinition projectDefinition : projectDefinitions)
        {
            T8ProjectVertex projectVertex;
            String projectId;

            projectId = projectDefinition.getIdentifier();
            projectVertex = (T8ProjectVertex)model.getVertex(projectId);
            if (projectVertex == null)
            {
                projectVertex = new T8ProjectVertex(projectId, vertexRenderer);
                projectVertex.setVertexMovable(true);
                randomizeVertexLocations(projectVertex);

                // Add the vertext to the model.
                model.addVertex(projectVertex);
                newProjects.add(projectDefinition);
            }
        }

        // Add all edges for newly added projects.
        for (T8ProjectDefinition newProject : newProjects)
        {
            List<String> requiredProjectIds;
            String newProjectId;

            // Add edges from newly added projects to their required projects.
            newProjectId = newProject.getIdentifier();
            requiredProjectIds = newProject.getRequiredProjectIds();
            for (String requiredProjectId : requiredProjectIds)
            {
                T8ProjectVertex endVertex;
                T8ProjectVertex startVertex;

                endVertex = (T8ProjectVertex)model.getVertex(requiredProjectId);
                startVertex = (T8ProjectVertex)model.getVertex(newProjectId);
                if (endVertex != null)
                {
                    model.addEdge(createEdge(startVertex, endVertex));
                }
            }

            // Add edges from existing projects to newly added projects that are required by them.
            for (T8ProjectDefinition projectDefinition : projectDefinitions)
            {
                if (!newProjects.contains(projectDefinition))
                {
                    if (projectDefinition.getRequiredProjectIds().contains(newProjectId))
                    {
                        T8ProjectVertex endVertex;
                        T8ProjectVertex startVertex;

                        endVertex = (T8ProjectVertex)model.getVertex(newProjectId);
                        startVertex = (T8ProjectVertex)model.getVertex(projectDefinition.getIdentifier());
                        if (endVertex != null)
                        {
                            model.addEdge(createEdge(startVertex, endVertex));
                        }
                    }
                }
            }
        }
    }

    private DefaultGraphEdge createEdge(GraphVertex startVertex, GraphVertex endVertex)
    {
        DefaultGraphEdge edge;
        String edgeId;

        edgeId = startVertex.getId() + "->" + endVertex.getId();
        edge = new DefaultGraphEdge(edgeId, startVertex, GraphVertexPort.PortType.CENTER, endVertex, GraphVertexPort.PortType.CENTER);
        edge.setEdgeType(config.getEdgeType());
        edge.setEdgeStartArrowType(GraphEdge.EdgeArrowType.NONE);
        edge.setEdgeEndArrowType(GraphEdge.EdgeArrowType.FILLED);
        return edge;
    }

    public void randomizeVertexLocations(GraphVertex vertex)
    {
        Point coordinates;
        int x;
        int y;

        // Determine random coordinates.
        x = (int)Math.round(Math.random() * 10.0);
        y = (int)Math.round(Math.random() * 10.0);

        // Negate values randomly.
        if (Math.random() > 0.5) x = x * -1;
        if (Math.random() > 0.5) y = y * -1;

        // Update the vertex location.
        coordinates = vertex.getVertexCoordinates();
        coordinates.setLocation(x, y);
    }

    public void randomizeVertexLocations()
    {
        List<GraphVertex> vertices;
        Set<String> usedLocations;

        usedLocations = new HashSet<>();
        vertices = model.getVertices();
        for (GraphVertex vertex : model.getVertices())
        {
            Point coordinates;
            int x;
            int y;

            x = 0;
            y = 0;
            while (usedLocations.contains(x + ":" + y))
            {
                x = (int)Math.round(Math.random() * 10.0 * vertices.size());
                y = (int)Math.round(Math.random() * 10.0 * vertices.size());
            }

            usedLocations.add(x + ":" + y);
            coordinates = vertex.getVertexCoordinates();
            coordinates.setLocation(x, y);
        }
    }

    public void addEdge(String projectId, String requiredProjectId)
    {
        GraphVertex startVertex;
        GraphVertex endVertex;

        startVertex = model.getVertex(projectId);
        endVertex = model.getVertex(requiredProjectId);
        if (startVertex != null)
        {
            if (endVertex != null)
            {
                model.addEdge(createEdge(startVertex, endVertex));
            }
            else throw new IllegalArgumentException("Project not found in graph: " + requiredProjectId);
        }
        else throw new IllegalArgumentException("Project not found in graph: " + projectId);
    }

    public boolean removeEdge(String projectId, String requiredProjectId)
    {
        for (GraphEdge edge : model.getEdges())
        {
            if (projectId.equals(edge.getStartVertex().getId()))
            {
                if (requiredProjectId.equals(edge.getEndVertex().getId()))
                {
                    model.removeEdge(edge);
                    return true;
                }
            }
        }

        return false;
    }

    public T8ProjectVertex getVertex(String projectId)
    {
        return (T8ProjectVertex)model.getVertex(projectId);
    }

    public Dimension getPreferredSize()
    {
        return null;
    }

    public Component getConfigurationComponent()
    {
        return new T8ProjectGraphConfigurationPanel(this, config);
    }
}
