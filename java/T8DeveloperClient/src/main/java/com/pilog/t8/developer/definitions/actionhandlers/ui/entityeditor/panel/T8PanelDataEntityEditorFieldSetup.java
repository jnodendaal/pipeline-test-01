package com.pilog.t8.developer.definitions.actionhandlers.ui.entityeditor.panel;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8PanelDataEntityEditorFieldSetup implements Serializable
{
    private String fieldIdentifier;
    private String editorIdentifier;
    private String editorTypeIdentifier;
    private String editorLabel;
    
    public T8PanelDataEntityEditorFieldSetup(String fieldIdentifier, String editorIdentifier, String editorTypeIdentifier, String editorLabel)
    {
        this.fieldIdentifier = fieldIdentifier;
        this.editorIdentifier = editorIdentifier;
        this.editorTypeIdentifier = editorTypeIdentifier;
        this.editorLabel = editorLabel;
    }

    public String getFieldIdentifier()
    {
        return fieldIdentifier;
    }

    public void setFieldIdentifier(String fieldIdentifier)
    {
        this.fieldIdentifier = fieldIdentifier;
    }

    public String getEditorIdentifier()
    {
        return editorIdentifier;
    }

    public void setEditorIdentifier(String editorIdentifier)
    {
        this.editorIdentifier = editorIdentifier;
    }

    public String getEditorTypeIdentifier()
    {
        return editorTypeIdentifier;
    }

    public void setEditorTypeIdentifier(String editorTypeIdentifier)
    {
        this.editorTypeIdentifier = editorTypeIdentifier;
    }

    public String getEditorLabel()
    {
        return editorLabel;
    }

    public void setEditorLabel(String editorLabel)
    {
        this.editorLabel = editorLabel;
    }
}
