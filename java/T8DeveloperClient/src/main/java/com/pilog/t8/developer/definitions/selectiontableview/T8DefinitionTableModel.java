package com.pilog.t8.developer.definitions.selectiontableview;

import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import java.util.List;
import java.util.Objects;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;


/**
 * @author Bouwer du Preez
 */
public class T8DefinitionTableModel extends DefaultTableModel
{
    // Column Types.
    Class[] types = new Class []
    {
        java.lang.Boolean.class, java.lang.String.class, java.lang.Long.class, Object.class, Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class, java.lang.Long.class, java.lang.String.class, java.lang.Long.class, java.lang.String.class, java.lang.String.class
    };

    // Column Editability.
    boolean[] canEdit = new boolean []
    {
        true, false, false, false, false, false, false, false, false, false, false, false, false, false
    };

    public T8DefinitionTableModel()
    {
        // Construct the table columns.
        super
        (
            new Object [][]
            {

            },
            new String []
            {
                "Selected", "Definition", "Revision", "Status", "Level", "Version", "Project", "Type", "Patched", "Created Time", "Created User", "Updated Time", "Updated User", "Checksum"
            }
        );
    }

    @Override
    public Class getColumnClass(int columnIndex)
    {
        return types[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return canEdit[columnIndex];
    }

    public void setDefinitionMetaData(List<T8DefinitionMetaData> metaDataList)
    {
        // Clear existing data.
        if (getRowCount() > 0) setRowCount(0);

        // Add all of the rows to the table model.
        if (metaDataList != null)
        {
            for (int rowIndex = 0; rowIndex < metaDataList.size(); rowIndex++)
            {
                T8DefinitionMetaData definitionMeta;

                // Get the row definition meta data.
                definitionMeta = metaDataList.get(rowIndex);
                addDefinitionMetaData(definitionMeta);
            }
        }
    }

    public void addDefinitionMetaData(T8DefinitionMetaData definitionMeta)
    {
        Vector definitionRow;

        // Add all of the row values.
        definitionRow = new Vector();
        definitionRow.add(false);
        definitionRow.add(definitionMeta.getId());
        definitionRow.add(definitionMeta.getRevision());
        definitionRow.add(definitionMeta.getStatus());
        definitionRow.add(definitionMeta.getDefinitionLevel());
        definitionRow.add(definitionMeta.getVersionId());
        definitionRow.add(definitionMeta.getProjectId());
        definitionRow.add(definitionMeta.getTypeId());
        definitionRow.add(definitionMeta.isPatchedContent());
        definitionRow.add(definitionMeta.getCreatedTime());
        definitionRow.add(definitionMeta.getCreatedUserId());
        definitionRow.add(definitionMeta.getUpdatedTime());
        definitionRow.add(definitionMeta.getUpdatedUserId());
        definitionRow.add(definitionMeta.getChecksum());

        // Add the new row to the table model.
        addRow(definitionRow);
    }

    public boolean removeDefinitionMetaData(T8DefinitionMetaData metaData)
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            String rowIdentifier;
            String rowProjectIdentifier;

            rowIdentifier = (String)getValueAt(rowIndex, getIdentifierColumnIndex());
            rowProjectIdentifier = (String)getValueAt(rowIndex, getProjectIdentifierColumnIndex());
            if ((Objects.equals(rowIdentifier, metaData.getId())) && (Objects.equals(rowProjectIdentifier, metaData.getProjectId())))
            {
                removeRow(rowIndex);
                return true;
            }
        }

        return false;
    }

    public int getRowIndex(T8DefinitionHandle definitionHandle)
    {
        for (int rowIndex = 0; rowIndex < this.getRowCount(); rowIndex++)
        {
            if ((Objects.equals(definitionHandle.getDefinitionIdentifier(), getIdentifier(rowIndex))) && (Objects.equals(definitionHandle.getProjectIdentifier(), getProjectIdentifier(rowIndex))))
            {
                return rowIndex;
            }
        }

        return -1;
    }

    public T8DefinitionHandle getDefinitionHandle(int rowIndex)
    {
        T8DefinitionHandle definitionHandle;
        T8DefinitionLevel definitionLevel;
        String projectId;
        String definitionId;

        definitionId = getIdentifier(rowIndex);
        definitionLevel = getDefinitionLevel(rowIndex);
        projectId = getProjectIdentifier(rowIndex);
        definitionHandle = new T8DefinitionHandle(definitionLevel, definitionId, projectId);
        return definitionHandle;
    }

    public boolean isSelected(T8DefinitionHandle definitionHandle)
    {
        int rowIndex;

        rowIndex = getRowIndex(definitionHandle);
        if (rowIndex > -1)
        {
            Boolean value;

            value = (Boolean)getValueAt(rowIndex, 0);
            return value != null ? value : false;
        }
        else throw new IllegalArgumentException("Identifier not found: " + definitionHandle);
    }

    public List<T8DefinitionMetaData> getCheckedDefinitionMetaData()
    {
        return null;
    }

    public int getIdentifierColumnIndex()
    {
        return 1;
    }

    public int getStatusColumnIndex()
    {
        return 3;
    }

    public int getLevelColumnIndex()
    {
        return 4;
    }

    public int getSystemIdentifierColumnIndex()
    {
        return 5;
    }

    public int getProjectIdentifierColumnIndex()
    {
        return 6;
    }

    public int getCreatedTimeColumnIndex()
    {
        return 9;
    }

    public int getUpdatedTimeColumnIndex()
    {
        return 11;
    }

    public String getIdentifier(int rowIndex)
    {
        return (String)getValueAt(rowIndex, getIdentifierColumnIndex());
    }

    public T8DefinitionLevel getDefinitionLevel(int rowIndex)
    {
        return (T8DefinitionLevel)getValueAt(rowIndex, getLevelColumnIndex());
    }

    public String getProjectIdentifier(int rowIndex)
    {
        return (String)getValueAt(rowIndex, getProjectIdentifierColumnIndex());
    }

    public String getSystemIdentifier(int rowIndex)
    {
        return (String)getValueAt(rowIndex, getSystemIdentifierColumnIndex());
    }

    public int getColumnIndex(String columnName)
    {
        for (int columnIndex = 0; columnIndex < this.getColumnCount(); columnIndex++)
        {
            if (columnName.equals(getColumnName(columnIndex))) return columnIndex;
        }

        return -1;
    }
}
