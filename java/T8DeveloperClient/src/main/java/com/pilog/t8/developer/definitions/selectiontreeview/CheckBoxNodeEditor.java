package com.pilog.t8.developer.definitions.selectiontreeview;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import org.jdesktop.swingx.JXTreeTable;

/**
 * @author Bouwer du Preez
 */
public class CheckBoxNodeEditor extends AbstractCellEditor implements TableCellEditor, TableCellRenderer
{
    private ItemListener checkListener;
    private T8DefinitionMetaTreeNode editorNode;
    private JCheckBox editor;
    private JXTreeTable parentTable;
    private JTree parentTree;
    private JCheckBox checkBox = new JCheckBox();
    private Color selectionBorderColor;
    private Color selectionForeground;
    private Color selectionBackground;
    private Color textForeground;
    private Color textBackground;
    
    public CheckBoxNodeEditor(JXTreeTable table)
    {
        Font fontValue;
        Boolean booleanValue;
        
        fontValue = UIManager.getFont("Tree.font");
        if (fontValue != null)
        {
            checkBox.setFont(fontValue);
        }
        
        booleanValue = (Boolean)UIManager.get("Tree.drawsFocusBorderAroundIcon");
        parentTable = table;
        checkBox.setFocusPainted((booleanValue != null) && (booleanValue.booleanValue()));
        selectionBorderColor = UIManager.getColor("Tree.selectionBorderColor");
        selectionForeground = UIManager.getColor("Tree.selectionForeground");
        selectionBackground = UIManager.getColor("Tree.selectionBackground");
        textForeground = UIManager.getColor("Tree.textForeground");
        textBackground = UIManager.getColor("Tree.textBackground");
        this.checkListener = new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent itemEvent)
            {
                if (editorNode != null)
                {
                    toggleNode(editorNode, !editorNode.isSelected());
                    refreshNodeParentSelection(editorNode);
                    parentTree.repaint();
                    parentTable.repaint();
                }
            }
        };
    }
    
    private void refreshNodeParentSelection(T8DefinitionMetaTreeNode node)
    {
        T8DefinitionMetaTreeNode parent;
        
        parent = node;
        while ((parent = (T8DefinitionMetaTreeNode)parent.getParent()) != null)
        {
            boolean childSelected;
            
            childSelected = false;
            for (int childIndex = 0; childIndex < parent.getChildCount(); childIndex++)
            {
                T8DefinitionMetaTreeNode childNode;

                childNode = (T8DefinitionMetaTreeNode)parent.getChildAt(childIndex);
                if (childNode.isSelected())
                {
                    childSelected = true;
                    break;
                }
            }
            
            parent.setSelected(childSelected);
        }
    }
    
    private void toggleNode(T8DefinitionMetaTreeNode node, boolean selected)
    {
        // Set the node selection state.
        node.setSelected(selected);
        
        // Set the child node selection state.
        for (int childIndex = 0; childIndex < node.getChildCount(); childIndex++)
        {
            T8DefinitionMetaTreeNode childNode;
            
            childNode = (T8DefinitionMetaTreeNode)node.getChildAt(childIndex);
            toggleNode(childNode, selected);
        }
    }

    @Override
    public Object getCellEditorValue()
    {
        checkBox.removeItemListener(checkListener);
        return editorNode;
    }

    @Override
    public boolean isCellEditable(EventObject event)
    {
        return true;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        parentTree = (JTree)table.getCellRenderer(row, ((JXTreeTable)table).getHierarchicalColumn());
        editorNode = (T8DefinitionMetaTreeNode)parentTree.getPathForRow(row).getLastPathComponent();
        checkBox.setText(null);
        checkBox.setSelected(editorNode.isSelected());
        if (isSelected)
        {
            checkBox.setForeground(selectionForeground);
            checkBox.setBackground(selectionBackground);
        }
        else
        {
            checkBox.setForeground(textForeground);
            checkBox.setBackground(textBackground);
        }

        checkBox.addItemListener(checkListener);
        return checkBox;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        parentTree = (JTree)table.getCellRenderer(row, ((JXTreeTable)table).getHierarchicalColumn());
        editorNode = (T8DefinitionMetaTreeNode)parentTree.getPathForRow(row).getLastPathComponent();
        checkBox.setText(null);
        checkBox.setSelected(editorNode.isSelected());
        if (isSelected)
        {
            checkBox.setForeground(selectionForeground);
            checkBox.setBackground(selectionBackground);
        }
        else
        {
            checkBox.setForeground(textForeground);
            checkBox.setBackground(textBackground);
        }

        return checkBox;
    }
}
