package com.pilog.t8.developer.utils;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionMetaDataInputDialog extends javax.swing.JDialog
{
    private final T8SessionContext sessionContext;
    private final T8DefinitionContext definitionContext;
    private final T8DefinitionTypeMetaData typeMetaData;
    private final T8Definition contextDefinition;
    private final String restrictionProjectId;
    private boolean proceed;

    public T8DefinitionMetaDataInputDialog(T8DefinitionContext definitionContext, Component parentComponent, T8DefinitionTypeMetaData typeMetaData, T8Definition contextDefinition, String restrictionProjectId, String definitionSuffix)
    {
        super(SwingUtilities.getWindowAncestor(parentComponent), ModalityType.APPLICATION_MODAL);
        initComponents();

        this.proceed = false;
        this.sessionContext = definitionContext.getSessionContext();
        this.definitionContext = definitionContext;
        this.contextDefinition = contextDefinition;
        this.typeMetaData = typeMetaData;
        this.jLabelMessage.setText(getMessage());
        this.restrictionProjectId = restrictionProjectId;
        refreshIdentifierPrefix();
        this.jTextFieldIdentifierSuffix.getDocument().addDocumentListener(new IdentifierListener());
        ((AbstractDocument)this.jTextFieldIdentifierSuffix.getDocument()).setDocumentFilter(new IdentifierDocumentFilter());
        this.jPanelHeader.setBackground(LAFConstants.HEADER_BG_COLOR);
        this.jPanelHeader.setBorder(LAFConstants.HEADER_BORDER);
        setupDialog();
        refreshCompleteIdentifier();
        setSize(500, 220);
        setLocationRelativeTo(parentComponent);

        if (!Strings.isNullOrEmpty(definitionSuffix))
        {
            String suffix;

            // Clean up the suffix.
            suffix = T8IdentifierUtilities.createIdentifierString(definitionSuffix);
            jTextFieldIdentifierSuffix.setText(suffix);
        }
    }

    private void setupDialog()
    {
        if ((contextDefinition == null) && (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.PROJECT))
        {
            try
            {
                List<String> identifierList;

                identifierList = definitionContext.getDefinitionIdentifiers(null, T8ProjectDefinition.TYPE_IDENTIFIER);
                Collections.sort(identifierList);
                jComboBoxProject.addItem(null);
                for (String identifier : identifierList)
                {
                    jComboBoxProject.addItem(identifier);
                }

                if (!Strings.isNullOrEmpty(restrictionProjectId))
                {
                    jComboBoxProject.setSelectedItem(restrictionProjectId);
                    jComboBoxProject.setEnabled(false);
                    jTextFieldIdentifierSuffix.requestFocusInWindow();
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving project identifiers.", e);
            }
        }
        else
        {
            jComboBoxProject.setEnabled(false);
            jCheckBoxPrivate.setEnabled(false);
        }
    }

    public static T8DefinitionMetaData getNewDefinitionMetaData(T8DefinitionContext definitionContext, Component parentComponent, T8DefinitionTypeMetaData typeMetaData, T8Definition contextDefinition)
    {
        return getNewDefinitionMetaData(definitionContext, parentComponent, typeMetaData, contextDefinition, null, null);
    }

    public static T8DefinitionMetaData getNewDefinitionMetaData(T8DefinitionContext definitionContext, Component parentComponent, T8DefinitionTypeMetaData typeMetaData, T8Definition contextDefinition, String restrictionProjectId, String definitionSuffix)
    {
        T8DefinitionMetaDataInputDialog dialog;

        dialog = new T8DefinitionMetaDataInputDialog(definitionContext, parentComponent, typeMetaData, contextDefinition, restrictionProjectId, definitionSuffix);
        dialog.setVisible(true);
        return dialog.getDefinitionMetaData();
    }

    protected T8DefinitionMetaData getDefinitionMetaData()
    {
        if (proceed)
        {
            T8DefinitionMetaData metaData;
            String typeIdentifier;

            typeIdentifier = typeMetaData.getTypeId();

            metaData = new T8DefinitionMetaData();
            metaData.setId(jTextFieldCompleteIdentifier.getText());
            metaData.setDefinitionLevel(typeMetaData.getDefinitionLevel());
            metaData.setProjectId((typeIdentifier.equals(T8ProjectDefinition.TYPE_IDENTIFIER) || (typeIdentifier.equals(T8SystemDefinition.TYPE_IDENTIFIER)) || (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.SYSTEM)) ? null : (String)jComboBoxProject.getSelectedItem());
            return metaData;
        }
        else return null;
    }

    protected String getPrefix()
    {
        String definitionTypePrefix;

        definitionTypePrefix = typeMetaData.getIdPrefix();
        if (definitionTypePrefix == null) definitionTypePrefix = "";
        if (contextDefinition == null)
        {
            String projectId;

            projectId = getSelectedProjectId();
            if ((projectId != null) && (jCheckBoxPrivate.isSelected()))
            {
                return T8Definition.getPrivateIdPrefix() + definitionTypePrefix;
            }
            else
            {
                return T8Definition.getPublicIdPrefix() + definitionTypePrefix;
            }
        }
        else
        {
            return T8Definition.getLocalIdPrefix() + definitionTypePrefix;
        }
    }

    protected String getMessage()
    {
        StringBuffer message;

        message = new StringBuffer("Please enter a new unique ");
        message.append(typeMetaData.getDisplayName());
        if (contextDefinition == null)
        {
            message.append(" Global Identifier.");
        }
        else
        {
            message.append(" Local Identifier.");
        }

        return message.toString();
    }

    private void refreshIdentifierPrefix()
    {
        this.jTextFieldIdentifierPrefix.setText(getPrefix());
    }

    private void refreshCompleteIdentifier()
    {
        String suffix;

        suffix = jTextFieldIdentifierSuffix.getText();
        jTextFieldCompleteIdentifier.setText(jTextFieldIdentifierPrefix.getText() + (suffix != null ? suffix.toUpperCase() : ""));
    }

    private void refresh()
    {
        refreshIdentifierPrefix();
        refreshCompleteIdentifier();
    }

    private class IdentifierListener implements DocumentListener
    {
        @Override
        public void insertUpdate(DocumentEvent e)
        {
            refreshCompleteIdentifier();
        }

        @Override
        public void removeUpdate(DocumentEvent e)
        {
            refreshCompleteIdentifier();
        }

        @Override
        public void changedUpdate(DocumentEvent e)
        {
            refreshCompleteIdentifier();
        }
    }

    public class IdentifierDocumentFilter extends DocumentFilter
    {
        @Override
        public void insertString(DocumentFilter.FilterBypass fb, int offset, String text, AttributeSet attr) throws BadLocationException
        {
            text = text.toUpperCase();
            text = text.replace(' ', '_');
            fb.insertString(offset, text, attr);
        }

        @Override
        public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text, AttributeSet attr) throws BadLocationException
        {
            text = text.toUpperCase();
            text = text.replace(' ', '_');
            fb.replace(offset, length, text.toUpperCase(), attr);
        }
    }

    private String getSelectedProjectId()
    {
        return (String)jComboBoxProject.getSelectedItem();
    }

    private boolean checkValidity()
    {
        String projectId;
        String definitionId;
        String typeId;

        typeId = typeMetaData.getTypeId();
        definitionId = jTextFieldCompleteIdentifier.getText();
        projectId = getSelectedProjectId();

        try
        {
            if ((contextDefinition == null) && (projectId == null) && (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.PROJECT))
            {
                JOptionPane.showMessageDialog(this, "No Project selected.", "Invalid Project", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            else if ((definitionId == null) || (definitionId.trim().length() == 0))
            {
                JOptionPane.showMessageDialog(this, "No Identifier entered.", "Invalid Identifier", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            else if (!definitionContext.checkIdentifierAvailability(projectId, definitionId))
            {
                JOptionPane.showMessageDialog(this, "The Identifier entered is already in use.  Please enter a unique identifier.", "Invalid Identifier", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            else if ((contextDefinition != null) && (contextDefinition.getRootDefinition().getDescendentDefinitionIdentifiers().contains(definitionId)))
            {
                JOptionPane.showMessageDialog(this, "The Identifier entered is already in use.  Please enter a unique identifier.", "Invalid Identifier", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            else if (!isValidIdentifier(definitionId))
            {
                JOptionPane.showMessageDialog(this, "The Identifier entered is not a valid format.\nOnly alphanumeric and underscore characters are allowed.\nNo double underscores.\nNo leading or trailing underscores.\nNo underscore followed by a numeric digit.", "Invalid Identifier", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            else return true;
        }
        catch (Exception e)
        {
            T8Log.log("Exception while validating Identifier.", e);
            JOptionPane.showMessageDialog(this, "The Identifier could not be validated successfully.", "Validation Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    private boolean isValidIdentifier(String identifier)
    {
        if (contextDefinition == null)
        {
            return identifier.matches(T8Definition.getPublicIdRegex()) || identifier.matches(T8Definition.getPrivateIdRegex());
        }
        else
        {
            return identifier.matches(T8Definition.getLocalIdRegex());
        }
    }

    public void setProjectIdentifier(String identifier)
    {
        jComboBoxProject.setSelectedItem(identifier);
    }

    public void setSuffix(String suffix)
    {
        jTextFieldIdentifierSuffix.setText(suffix);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelHeader = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();
        jPanelContent = new javax.swing.JPanel();
        jLabelProject = new javax.swing.JLabel();
        jComboBoxProject = new javax.swing.JComboBox();
        jCheckBoxPrivate = new javax.swing.JCheckBox();
        jLabelMessage = new javax.swing.JLabel();
        jLabelIdentifierPrefix = new javax.swing.JLabel();
        jLabelCompleteIdentifier = new javax.swing.JLabel();
        jLabelIdentifierSuffix = new javax.swing.JLabel();
        jTextFieldIdentifierPrefix = new javax.swing.JTextField();
        jTextFieldCompleteIdentifier = new javax.swing.JTextField();
        jTextFieldIdentifierSuffix = new javax.swing.JTextField();
        jPanelControls = new javax.swing.JPanel();
        jButtonCancel = new javax.swing.JButton();
        jButtonProceed = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentShown(java.awt.event.ComponentEvent evt)
            {
                formComponentShown(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelHeader.setLayout(new java.awt.GridBagLayout());

        jLabelHeader.setText("New Identifier");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelHeader.add(jLabelHeader, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        getContentPane().add(jPanelHeader, gridBagConstraints);

        jPanelContent.setLayout(new java.awt.GridBagLayout());

        jLabelProject.setText("Project:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelContent.add(jLabelProject, gridBagConstraints);

        jComboBoxProject.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboBoxProjectItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 6);
        jPanelContent.add(jComboBoxProject, gridBagConstraints);

        jCheckBoxPrivate.setSelected(true);
        jCheckBoxPrivate.setText("Private");
        jCheckBoxPrivate.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jCheckBoxPrivateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 6);
        jPanelContent.add(jCheckBoxPrivate, gridBagConstraints);

        jLabelMessage.setText("Message");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 6, 5);
        jPanelContent.add(jLabelMessage, gridBagConstraints);

        jLabelIdentifierPrefix.setText("Identifier Prefix:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelContent.add(jLabelIdentifierPrefix, gridBagConstraints);

        jLabelCompleteIdentifier.setText("Complete Identifier:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelContent.add(jLabelCompleteIdentifier, gridBagConstraints);

        jLabelIdentifierSuffix.setText("Identifier Suffix:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelContent.add(jLabelIdentifierSuffix, gridBagConstraints);

        jTextFieldIdentifierPrefix.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelContent.add(jTextFieldIdentifierPrefix, gridBagConstraints);

        jTextFieldCompleteIdentifier.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelContent.add(jTextFieldCompleteIdentifier, gridBagConstraints);

        jTextFieldIdentifierSuffix.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt)
            {
                jTextFieldIdentifierSuffixKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelContent.add(jTextFieldIdentifierSuffix, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        getContentPane().add(jPanelContent, gridBagConstraints);

        jPanelControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 2, 2));

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCancelActionPerformed(evt);
            }
        });
        jPanelControls.add(jButtonCancel);

        jButtonProceed.setText("Proceed");
        jButtonProceed.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonProceedActionPerformed(evt);
            }
        });
        jPanelControls.add(jButtonProceed);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        getContentPane().add(jPanelControls, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonProceedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonProceedActionPerformed
    {//GEN-HEADEREND:event_jButtonProceedActionPerformed
        if (checkValidity())
        {
            proceed = true;
            dispose();
        }
    }//GEN-LAST:event_jButtonProceedActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCancelActionPerformed
    {//GEN-HEADEREND:event_jButtonCancelActionPerformed
        proceed = false;
        dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void jTextFieldIdentifierSuffixKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jTextFieldIdentifierSuffixKeyPressed
    {//GEN-HEADEREND:event_jTextFieldIdentifierSuffixKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER)
        {
            if (checkValidity())
            {
                proceed = true;
                dispose();
            }
        }
    }//GEN-LAST:event_jTextFieldIdentifierSuffixKeyPressed

    private void formComponentShown(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentShown
    {//GEN-HEADEREND:event_formComponentShown
        jTextFieldIdentifierSuffix.requestFocusInWindow();
    }//GEN-LAST:event_formComponentShown

    private void jCheckBoxPrivateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jCheckBoxPrivateActionPerformed
    {//GEN-HEADEREND:event_jCheckBoxPrivateActionPerformed
        refresh();
    }//GEN-LAST:event_jCheckBoxPrivateActionPerformed

    private void jComboBoxProjectItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboBoxProjectItemStateChanged
    {//GEN-HEADEREND:event_jComboBoxProjectItemStateChanged
        refresh();
    }//GEN-LAST:event_jComboBoxProjectItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonProceed;
    private javax.swing.JCheckBox jCheckBoxPrivate;
    private javax.swing.JComboBox jComboBoxProject;
    private javax.swing.JLabel jLabelCompleteIdentifier;
    private javax.swing.JLabel jLabelHeader;
    private javax.swing.JLabel jLabelIdentifierPrefix;
    private javax.swing.JLabel jLabelIdentifierSuffix;
    private javax.swing.JLabel jLabelMessage;
    private javax.swing.JLabel jLabelProject;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelControls;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JTextField jTextFieldCompleteIdentifier;
    private javax.swing.JTextField jTextFieldIdentifierPrefix;
    private javax.swing.JTextField jTextFieldIdentifierSuffix;
    // End of variables declaration//GEN-END:variables
}
