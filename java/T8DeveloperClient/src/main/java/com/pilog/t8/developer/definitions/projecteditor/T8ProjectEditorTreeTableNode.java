package com.pilog.t8.developer.definitions.projecteditor;

import com.pilog.t8.definition.T8DefinitionGroupMetaData;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectEditorTreeTableNode extends AbstractMutableTreeTableNode
{
    private final T8DefinitionGroupMetaData groupMetaData;
    private final T8DefinitionMetaData definitionMetaData;
    private final T8DefinitionTypeMetaData typeMetaData;

    /**
     * Creates a definition node.
     * @param groupMetaData The group meta data to add to the node.
     * @param typeMetaData The type meta data to add to the node.
     * @param definitionMetaData The definition meta data to store in this node.
     */
    public T8ProjectEditorTreeTableNode(T8DefinitionGroupMetaData groupMetaData, T8DefinitionTypeMetaData typeMetaData, T8DefinitionMetaData definitionMetaData)
    {
        super(definitionMetaData);
        this.groupMetaData = groupMetaData;
        this.typeMetaData = typeMetaData;
        this.definitionMetaData = definitionMetaData;
    }

    /**
     * Creates a temporary node using the supplied title as content.
     * @param title The node message to use as temporary content.
     */
    public T8ProjectEditorTreeTableNode(String title)
    {
        super(title);
        this.definitionMetaData = null;
        this.groupMetaData = null;
        this.typeMetaData = null;
    }

    public String getId()
    {
        if (definitionMetaData != null) return definitionMetaData.getId();
        else if (typeMetaData != null) return typeMetaData.getTypeId();
        else if (groupMetaData != null) return groupMetaData.getGroupId();
        else return null;
    }

    public T8DefinitionMetaData getDefinitionMetaData()
    {
        return definitionMetaData;
    }

    public T8DefinitionTypeMetaData getDefinitionTypeMetaData()
    {
        return typeMetaData;
    }

    public T8DefinitionGroupMetaData getDefinitionGroupMetaData()
    {
        return groupMetaData;
    }

    public String getTemporaryTitle()
    {
        return (String)getUserObject();
    }

    public boolean isDefinitionNode()
    {
        return definitionMetaData != null;
    }

    public boolean isTypeNode()
    {
        return ((typeMetaData != null) && (definitionMetaData == null));
    }

    public boolean isGroupNode()
    {
        return ((groupMetaData != null) && (typeMetaData == null) && (definitionMetaData == null));
    }

    public boolean areChildrenLoaded()
    {
        if (getChildCount() == 1)
        {
            // If the only child is temporary, children are not loaded.
            return !((T8ProjectEditorTreeTableNode)getChildAt(0)).isTemporary();
        }
        else return true;
    }

    public boolean isTemporary()
    {
        return definitionMetaData == null && typeMetaData == null && groupMetaData == null;
    }

    public void sortChildren(Comparator comparator)
    {
        children.sort(comparator);
    }

    @Override
    public Object getValueAt(int column)
    {
        //"Definition", "Version", "Status", "Level", "Project", "Type", "Patched", "Created Time", "Created User", "Updated Time", "Updated User", "Checksum"
        if (definitionMetaData != null)
        {
            switch (column)
            {
                case 0:
                    return definitionMetaData.getId();
                case 1:
                    return definitionMetaData.getRevision();
                case 2:
                    return definitionMetaData.getStatus();
                case 3:
                    return definitionMetaData.getTypeId();
                case 4:
                    return definitionMetaData.isPatchedContent();
                case 5:
                    return definitionMetaData.getCreatedTime();
                case 6:
                    return definitionMetaData.getCreatedUserId();
                case 7:
                    return definitionMetaData.getUpdatedTime();
                case 8:
                    return definitionMetaData.getUpdatedUserId();
                case 9:
                    return definitionMetaData.getChecksum();
                default:
                    return null;
            }
        }
        else return null;
    }

    @Override
    public int getColumnCount()
    {
        return T8ProjectEditorTreeTableModel.columnNames.size();
    }

    public List<T8ProjectEditorTreeTableNode> getChildNodes()
    {
        List<T8ProjectEditorTreeTableNode> childNodes;

        childNodes = new ArrayList<>();
        for (int childIndex = 0; childIndex < getChildCount(); childIndex++)
        {
            childNodes.add((T8ProjectEditorTreeTableNode)getChildAt(childIndex));
        }

        return childNodes;
    }

    public List<T8ProjectEditorTreeTableNode> getDescendantNodes()
    {
        List<T8ProjectEditorTreeTableNode> resultList;
        LinkedList<T8ProjectEditorTreeTableNode> nodeQueue;

        resultList = new ArrayList<>();
        nodeQueue = new LinkedList<T8ProjectEditorTreeTableNode>();
        nodeQueue.add(this);
        while (!nodeQueue.isEmpty())
        {
            List<T8ProjectEditorTreeTableNode> childNodes;
            T8ProjectEditorTreeTableNode nextNode;

            nextNode = nodeQueue.removeFirst();
            childNodes = nextNode.getChildNodes();
            resultList.addAll(childNodes);
            nodeQueue.addAll(childNodes);
        }

        return resultList;
    }

    public List<T8ProjectEditorTreeTableNode> getFamilyNodes()
    {
        List<T8ProjectEditorTreeTableNode> resultList;
        LinkedList<T8ProjectEditorTreeTableNode> nodeQueue;

        resultList = new ArrayList<>();
        nodeQueue = new LinkedList<T8ProjectEditorTreeTableNode>();
        nodeQueue.add(this);
        while (!nodeQueue.isEmpty())
        {
            T8ProjectEditorTreeTableNode nextNode;

            nextNode = nodeQueue.removeFirst();
            resultList.add(nextNode);
            nodeQueue.addAll(nextNode.getChildNodes());
        }

        return resultList;
    }

    public List<T8ProjectEditorTreeTableNode> getLineNodes()
    {
        List<T8ProjectEditorTreeTableNode> lineNodes;
        T8ProjectEditorTreeTableNode nextNode;

        lineNodes = new ArrayList<>();
        nextNode = this;
        lineNodes.add(nextNode);
        while (nextNode.getParent() != null)
        {
            nextNode = (T8ProjectEditorTreeTableNode)nextNode.getParent();
            lineNodes.add(nextNode);
        }

        // Reverse the order, so that the id's start from the root.
        Collections.reverse(lineNodes);
        return lineNodes;
    }

    public TreePath getPath()
    {
        return new TreePath(getLineNodes().toArray());
    }

    @Override
    public String toString()
    {
        return "T8DefinitionProjectViewTreeTableNode{" + getId() + '}';
    }
}
