package com.pilog.t8.developer.definitions.renderer;

import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionTypeMetaDataListCellRenderer extends JLabel implements ListCellRenderer
{
    public static final Color SELECTED_BACKGROUND_COLOR = new Color(130,180,200);
    public static final Color SELECTED_FOREGROUND_COLOR = new Color(0,0,255);
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        T8DefinitionTypeMetaData metaData;
        Color background;
        Color foreground;

        // Set the appropriate colors to use.
        if (isSelected)
        {
            background = SELECTED_BACKGROUND_COLOR;
            foreground = SELECTED_FOREGROUND_COLOR;
            setOpaque(true);
        }
        else
        {
            background = list.getBackground();
            foreground = list.getForeground();
            setOpaque(false);
        }

        // Set the background and foreground.
        setBackground(background);
        setForeground(foreground);

        // Determine the display text.
        metaData = (T8DefinitionTypeMetaData)value;
        if (metaData != null)
        {
            String displayName;
            
            // Try to use the display name if available, else use the type identifier.
            displayName = metaData.getDisplayName();
            setText(!Strings.isNullOrEmpty(displayName) ? displayName : metaData.getTypeId());

            // Return this label as the renderer component.
            return this;
        }
        else return null;
    }
}
