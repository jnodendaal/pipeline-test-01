package com.pilog.t8.developer.definitions.history;

import static com.pilog.t8.definition.event.T8ChangeLogHandlerAPIHandler.*;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionSerializer;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.basic.T8BasicDataFilterDefinition;
import com.pilog.t8.definition.event.T8ChangeLogHandlerDefinition;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.developer.view.T8DefaultDeveloperViewFactory;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.table.T8Table;
import com.pilog.t8.utilities.collections.HashMaps;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * @author Gavin Boshoff
 */
public class T8DefinitionHistoryPanel extends javax.swing.JPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefinitionHistoryPanel.class);

    private final T8DeveloperView parentView;
    private final T8DefinitionContext definitionContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final ChangeLogHandlerSelectionListener changeLogHandlerSelectionListener;
    private final DefinitionSelectionListener definitionSelectionListener;
    private final T8ComponentController controller;
    private final String projectId;
    private final String definitionId;
    private T8ChangeLogHandlerDefinition selectedChangeLogHandlerDefinition;
    private T8Table table;
    private T8DataEntity selectedDataEntity;
    private T8DeveloperView developerView;

    private T8DefinitionHistoryPanel(T8DeveloperView parentView, T8DefinitionContext definitionContext, String projectId, String definitionId)
    {
        this.parentView = parentView;
        this.projectId = projectId;
        this.definitionId = definitionId;
        this.definitionContext = definitionContext;
        this.context = definitionContext.getContext();
        this.definitionManager = definitionContext.getClientContext().getDefinitionManager();
        this.changeLogHandlerSelectionListener = new ChangeLogHandlerSelectionListener();
        this.definitionSelectionListener = new DefinitionSelectionListener();
        this.controller = new T8DefaultComponentController(context, "DEFINITION_HISTORY", false);
        initComponents();
        initializeComponent();
    }

    public static void showDefinitionHistoryPanel(T8DeveloperView parentView, T8DefinitionContext definitionContext, String projectId, String definitionId)
    {
        JFrame frame;

        frame = new JFrame(definitionId + " History");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setContentPane(new T8DefinitionHistoryPanel(parentView, definitionContext, projectId, definitionId));
        frame.setPreferredSize(new Dimension((int) (frame.getToolkit().getScreenSize().width * 0.7),(int) (frame.getToolkit().getScreenSize().height * 0.8)));
        frame.pack();
        frame.setVisible(true);
    }

    private void initializeComponent()
    {
        DefaultComboBoxModel<String> comboBoxModel;

        try
        {
            comboBoxModel = new DefaultComboBoxModel<>();

            for (String identifier : definitionManager.getDefinitionIdentifiers(projectId, T8ChangeLogHandlerDefinition.TYPE_IDENTIFIER))
            {
                comboBoxModel.addElement(identifier);
            }

            jComboBoxChangeHandler.setModel(comboBoxModel);
            jComboBoxChangeHandler.addItemListener(changeLogHandlerSelectionListener);

            if (comboBoxModel.getSize() > 0) setSelectedChangeLogHandler(comboBoxModel.getElementAt(0));

            T8DefaultDeveloperViewFactory viewFactory;

            viewFactory = new T8DefaultDeveloperViewFactory(definitionContext);
            developerView = viewFactory.createReadOnlyDefinitionNavigator(parentView, projectId, definitionId);

            this.jPanelDeveloperView.add((Component) developerView);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to load initialize component", ex);
        }
    }

    //TODO: GBO - Redo this. The filter that is set up should first have all the renames
    //which have occured be retrieved and then filter look up the definition history like that.
    //Also, when this is done, the time at which the rename occurred should be taken into
    //consideration as to avoid showing the history for a definition which may be completely
    //unrelated to the current definition
    private void constructTable(T8ChangeLogHandlerDefinition changeLogHandlerDefinition) throws Exception
    {
        T8BasicDataFilterDefinition filterDefinition;
        T8TableColumnDefinition columnDefinition;
        T8DataEntityDefinition entityDefinition;
        T8TableDefinition tableDefinition;
        String entityId;

        entityId = changeLogHandlerDefinition.getDataEntityIdentifier();
        entityDefinition = (T8DataEntityDefinition) definitionManager.getInitializedDefinition(context, null, entityId, null);

        filterDefinition = new T8BasicDataFilterDefinition("$DEFINITION_IDENTIFIER_FILTER");
        filterDefinition.setDataEntityIdentifier(changeLogHandlerDefinition.getDataEntityIdentifier());
        filterDefinition.setFieldCriteriaExpressionMap(HashMaps.createTypeSafeMap(new String[]{entityId+EF_DEFINITION_ID, entityId+EF_EVENT}, new String[]{"\"" + definitionId + "\"", "\"SAVED\""}));
        filterDefinition.setFieldOrderingMap(HashMaps.createSingular(changeLogHandlerDefinition.getDataEntityIdentifier() + EF_TIME, T8DataFilter.OrderMethod.DESCENDING));

        tableDefinition = new T8TableDefinition("$TABLE_META_HISTORY");
        tableDefinition.setAutoResizeMode(T8TableDefinition.AutoResizeMode.ALL_COLUMNS);
        tableDefinition.setAutoSelectOnRetrieve(true);
        tableDefinition.setCommitEnabled(false);
        tableDefinition.setDataEntityIdentifier(changeLogHandlerDefinition.getDataEntityIdentifier());
        tableDefinition.setEntityDefinition(entityDefinition);
        tableDefinition.setDeleteEnabled(false);
        tableDefinition.setInsertEnabled(false);
        tableDefinition.setOptionsEnabled(false);
        tableDefinition.setPageSize(20);
        tableDefinition.setSelectionMode(T8TableDefinition.SelectionMode.SINGLE_SELECTION);
        tableDefinition.setUserExportEnabled(false);
        tableDefinition.setPrefilterDefinitions(Arrays.asList(filterDefinition));

        columnDefinition = new T8TableColumnDefinition(EF_USER_ID);
        columnDefinition.setColumnName("User Identifier");
        columnDefinition.setEditable(false);
        columnDefinition.setFieldIdentifier(changeLogHandlerDefinition.getDataEntityIdentifier() + EF_USER_ID);
        columnDefinition.setVisible(true);
        columnDefinition.setWidth(0);
        columnDefinition.setMinWidth(0);
        tableDefinition.addColumnDefinition(columnDefinition);

        columnDefinition = new T8TableColumnDefinition(EF_TIME);
        columnDefinition.setColumnName("Event Time");
        columnDefinition.setEditable(false);
        columnDefinition.setFieldIdentifier(changeLogHandlerDefinition.getDataEntityIdentifier() + EF_TIME);
        columnDefinition.setVisible(true);
        columnDefinition.setWidth(0);
        columnDefinition.setMinWidth(0);
        tableDefinition.addColumnDefinition(columnDefinition);

        columnDefinition = new T8TableColumnDefinition(EF_EVENT);
        columnDefinition.setColumnName("Event");
        columnDefinition.setEditable(false);
        columnDefinition.setFieldIdentifier(changeLogHandlerDefinition.getDataEntityIdentifier() + EF_EVENT);
        columnDefinition.setVisible(true);
        columnDefinition.setWidth(0);
        columnDefinition.setMinWidth(0);
        tableDefinition.addColumnDefinition(columnDefinition);

        if(table != null)
        {
            table.getTable().getSelectionModel().removeListSelectionListener(definitionSelectionListener);
            table.stopComponent();
        }

        table = (T8Table) tableDefinition.getNewComponentInstance(controller);
        table.getTable().getSelectionModel().addListSelectionListener(definitionSelectionListener);
        table.initializeComponent(null);
        table.startComponent();

        this.jPanelTableHistory.removeAll();
        this.jPanelTableHistory.add(table);
    }

    private void setSelectedChangeLogHandler(String identifier)
    {
        try
        {
            selectedChangeLogHandlerDefinition = (T8ChangeLogHandlerDefinition) definitionManager.getRawDefinition(context, projectId, identifier);

            constructTable(selectedChangeLogHandlerDefinition);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to get the definition for the change log handler " + identifier, ex);
        }
    }

    public void setSelectedDataEntity(T8DataEntity selectedDataEntity)
    {
        String definitionData;
        T8DefinitionSerializer serializer;
        T8Definition deSerializedDefinition;

        this.selectedDataEntity = selectedDataEntity;

        try
        {
            definitionData = (String) selectedDataEntity.getFieldValue(selectedDataEntity.getIdentifier() + EF_DATA);
            serializer = new T8DefinitionSerializer(definitionManager);
            deSerializedDefinition = serializer.deserializeDefinition(definitionData);

            developerView.setSelectedDefinition(deSerializedDefinition);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to de serialize definition", ex);
        }
    }

    private class DefinitionSelectionListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            if(!e.getValueIsAdjusting() && e.getFirstIndex() > -1)
            {
                setSelectedDataEntity(table.getSelectedDataEntity());
            }
        }
    }

    private class ChangeLogHandlerSelectionListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if(e.getStateChange() == ItemEvent.SELECTED)
            {
                setSelectedChangeLogHandler((String)e.getItem());
            }
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanelDeveloperView = new javax.swing.JPanel();
        jPanelTableHistory = new javax.swing.JPanel();
        jComboBoxChangeHandler = new javax.swing.JComboBox<String>();

        setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        jSplitPane1.setDividerLocation(250);
        jSplitPane1.setToolTipText("");

        jPanelDeveloperView.setLayout(new java.awt.BorderLayout());
        jSplitPane1.setRightComponent(jPanelDeveloperView);

        jPanelTableHistory.setLayout(new java.awt.BorderLayout());
        jSplitPane1.setLeftComponent(jPanelTableHistory);

        jPanel1.add(jSplitPane1, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jPanel1, gridBagConstraints);

        jComboBoxChangeHandler.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jComboBoxChangeHandler, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> jComboBoxChangeHandler;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelDeveloperView;
    private javax.swing.JPanel jPanelTableHistory;
    private javax.swing.JSplitPane jSplitPane1;
    // End of variables declaration//GEN-END:variables
}
