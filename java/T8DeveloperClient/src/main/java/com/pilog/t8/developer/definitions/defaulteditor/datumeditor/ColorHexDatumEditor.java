package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumUtilities;
import java.awt.Color;
import org.jdesktop.swingx.JXColorSelectionButton;

/**
 * @author Bouwer du Preez
 */
public class ColorHexDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private final JXColorSelectionButton colorSelectionButton;
    private boolean nullColor;
    
    public ColorHexDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        colorSelectionButton = new JXColorSelectionButton();
        nullColor = false;
    }
    
    @Override
    public void initializeComponent()
    {
    }
    
    @Override
    public void startComponent()
    {
        refreshEditor();
    }
    
    @Override
    public void stopComponent()
    {
    }
    
    @Override
    public void commitChanges()
    {
        if (nullColor)
        {
            setDefinitionDatum(null);
        }
        else
        {
            Color newColor;

            newColor = colorSelectionButton.getBackground();
            setDefinitionDatum(T8DefinitionDatumUtilities.getColorHex(newColor));
        }
    }
    
    @Override
    public void refreshEditor()
    {
        Object value;
        
        value = getDefinitionDatum();
        if (value == null)
        {
            deleteColor();
        }
        else
        {
            colorSelectionButton.setBackground(T8DefinitionDatumUtilities.getColor((String)value));
            addColor();
        }
    }
    
    @Override
    public void setEditable(boolean editable)
    {
        jButtonAdd.setEnabled(editable);
        jButtonRemove.setEnabled(editable);
        if (colorSelectionButton != null) colorSelectionButton.setEnabled(editable);
    }

    private void addColor()
    {
        add(colorSelectionButton, java.awt.BorderLayout.CENTER);
        nullColor = false;
        revalidate();
    }
    
    private void deleteColor()
    {
        remove(colorSelectionButton);
        nullColor = true;
        revalidate();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jButtonRemove = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAdd);

        jButtonRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeDocumentIcon.png"))); // NOI18N
        jButtonRemove.setText("Remove");
        jButtonRemove.setFocusable(false);
        jButtonRemove.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoveActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRemove);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        addColor();
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveActionPerformed
        deleteColor();
    }//GEN-LAST:event_jButtonRemoveActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
