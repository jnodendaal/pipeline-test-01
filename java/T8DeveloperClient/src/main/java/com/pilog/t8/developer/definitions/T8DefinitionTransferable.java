package com.pilog.t8.developer.definitions;

import com.pilog.t8.definition.T8Definition;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8DefinitionTransferable implements Transferable
{
    private final DataFlavor[] supportedDataFlavors;
    private final T8Definition definition;

    public T8DefinitionTransferable(T8Definition definition)
    {
        DataFlavor definitionFlavor = getDefinitionDataFlavor();
        supportedDataFlavors = new DataFlavor[]
        {
            definitionFlavor,
            DataFlavor.stringFlavor
        };
        this.definition = definition;
    }

    public static DataFlavor getDefinitionDataFlavor()
    {
        return new DataFlavor(T8Definition.class, "T8Definition");
    }

    @Override
    public DataFlavor[] getTransferDataFlavors()
    {
        return supportedDataFlavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor)
    {
        for (DataFlavor dataFlavor : supportedDataFlavors)
        {
            if (dataFlavor.equals(flavor))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException
    {
        if(flavor == DataFlavor.stringFlavor)
        {
            return definition.getIdentifier();
        }

       return definition;
    }
}
