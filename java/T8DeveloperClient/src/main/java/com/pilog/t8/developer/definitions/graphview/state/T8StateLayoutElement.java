package com.pilog.t8.developer.definitions.graphview.state;

import com.pilog.graph.model.DefaultGraphVertex;
import com.pilog.t8.definition.graph.T8GraphNodeDefinition;
import com.pilog.t8.developer.definitions.graphview.layout.LayoutElement;
import com.pilog.t8.developer.definitions.graphview.layout.LayoutInsets;
import java.awt.Dimension;
import java.awt.Point;

/**
 * @author Bouwer du Preez
 */
public class T8StateLayoutElement extends DefaultGraphVertex implements LayoutElement
{
    private final T8GraphNodeDefinition nodeDefinition;
    private final T8StateVertexRenderer renderer;
    private final LayoutInsets insets;
    private final int elementIndex;

    public T8StateLayoutElement(int elementIndex, T8GraphNodeDefinition nodeDefinition, T8StateVertexRenderer renderer)
    {
        super(nodeDefinition.getIdentifier(), nodeDefinition.getXCoordinate(), nodeDefinition.getYCoordinate(), nodeDefinition);
        this.nodeDefinition = nodeDefinition;
        this.elementIndex = elementIndex;
        this.renderer = renderer;
        this.insets = new LayoutInsets(0, 0, 0, 0);
    }

    @Override
    public LayoutInsets getInsets()
    {
        return insets;
    }

    @Override
    public Dimension getSize()
    {
        return renderer.getVertexSize(null, this, false);
    }

    @Override
    public void doLayout(int x, int y)
    {
        Dimension size;
        Point location;

        size = getSize();
        location = new Point(x, y);
        location.translate(Math.round(size.width/2.0f), Math.round(size.height/2.0f));
        setVertexCoordinates(location);
        nodeDefinition.setXCoordinate(location.x);
        nodeDefinition.setYCoordinate(location.y);
    }

    @Override
    public void translate(int dx, int dy)
    {
        super.translate(dx, dy);
        nodeDefinition.setXCoordinate(getVertexCoordinates().x);
        nodeDefinition.setYCoordinate(getVertexCoordinates().y);
    }

    @Override
    public void setVertexCoordinates(Point coordinates)
    {
        super.setVertexCoordinates(coordinates);
        nodeDefinition.setXCoordinate(coordinates.x);
        nodeDefinition.setYCoordinate(coordinates.y);
    }

    public T8GraphNodeDefinition getNodeDefinition()
    {
        return (T8GraphNodeDefinition)getVertexDataObject();
    }

    public int getElementIndex()
    {
        return elementIndex;
    }

    public int getXCoordinate()
    {
        return getNodeDefinition().getXCoordinate();
    }

    public int getYCoordinate()
    {
        return getNodeDefinition().getYCoordinate();
    }
}
