package com.pilog.t8.developer.definitions.compositionview;

import com.pilog.t8.definition.T8DefinitionComposition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionCompositionTreeTableNode extends AbstractMutableTreeTableNode
{
    private final T8DefinitionComposition definitionComposition;
    private final T8DefinitionTypeMetaData typeMetaData;

    /**
     * Creates a composition node.
     * @param composition The composition to add to the node.
     */
    public T8DefinitionCompositionTreeTableNode(T8DefinitionComposition composition)
    {
        super(composition);
        this.definitionComposition = composition;
        this.typeMetaData = null;
    }

    /**
     * Creates a definition type node.
     * @param typeMetaData The type meta data to add to the node.
     */
    public T8DefinitionCompositionTreeTableNode(T8DefinitionTypeMetaData typeMetaData)
    {
        super(typeMetaData);
        this.definitionComposition = null;
        this.typeMetaData = typeMetaData;
    }

    /**
     * Creates a temporary node using the supplied title as content.
     * @param title The node message to use as temporary content.
     */
    public T8DefinitionCompositionTreeTableNode(String title)
    {
        super(title);
        this.definitionComposition = null;
        this.typeMetaData = null;
    }

    public T8DefinitionComposition getDefinitionComposition()
    {
        return definitionComposition;
    }

    public T8DefinitionTypeMetaData getDefinitionTypeMetaData()
    {
        return typeMetaData;
    }

    public String getTemporaryTitle()
    {
        return (String)getUserObject();
    }

    public boolean isCompositionNode()
    {
        return definitionComposition != null;
    }

    public boolean areChildrenLoaded()
    {
        if (getChildCount() == 1)
        {
            // If the only child is temporary, children are not loaded.
            return !((T8DefinitionCompositionTreeTableNode)getChildAt(0)).isTemporary();
        }
        else return true;
    }

    public boolean isTemporary()
    {
        return definitionComposition == null && typeMetaData == null;
    }

    @Override
    public Object getValueAt(int column)
    {
        if (definitionComposition != null)
        {
            T8DefinitionMetaData nodeMetaData;

            nodeMetaData = definitionComposition.getDefinitionMetaData();
            switch (column)
            {
                case 0:
                    return nodeMetaData.getId();
                case 1:
                    return nodeMetaData.getRevision();
                case 2:
                    return nodeMetaData.getStatus();
                case 3:
                    return nodeMetaData.getDefinitionLevel();
                case 4:
                    return nodeMetaData.getVersionId();
                case 5:
                    return nodeMetaData.getProjectId();
                case 8:
                    return nodeMetaData.getTypeId();
                case 9:
                    return nodeMetaData.isPatchedContent();
                case 10:
                    return nodeMetaData.getCreatedTime();
                case 11:
                    return nodeMetaData.getCreatedUserId();
                case 12:
                    return nodeMetaData.getUpdatedTime();
                case 13:
                    return nodeMetaData.getUpdatedUserId();
                case 14:
                    return nodeMetaData.getChecksum();
                default:
                    return null;
            }
        }
        else return null;
    }

    @Override
    public int getColumnCount()
    {
        return T8DefinitionCompositionTreeTableModel.columnNames.size();
    }

    public List<T8DefinitionCompositionTreeTableNode> getChildNodes()
    {
        List<T8DefinitionCompositionTreeTableNode> childNodes;

        childNodes = new ArrayList<>();
        for (int childIndex = 0; childIndex < getChildCount(); childIndex++)
        {
            childNodes.add((T8DefinitionCompositionTreeTableNode)getChildAt(childIndex));
        }

        return childNodes;
    }

    public List<T8DefinitionCompositionTreeTableNode> getFamilyNodes()
    {
        List<T8DefinitionCompositionTreeTableNode> resultList;
        LinkedList<T8DefinitionCompositionTreeTableNode> nodeQueue;

        resultList = new ArrayList<>();
        nodeQueue = new LinkedList<T8DefinitionCompositionTreeTableNode>();
        nodeQueue.add(this);
        while (!nodeQueue.isEmpty())
        {
            T8DefinitionCompositionTreeTableNode nextNode;

            nextNode = nodeQueue.removeFirst();
            resultList.add(nextNode);
            nodeQueue.addAll(nextNode.getChildNodes());
        }

        return resultList;
    }

    public List<T8DefinitionCompositionTreeTableNode> getLineNodes()
    {
        List<T8DefinitionCompositionTreeTableNode> lineNodes;
        T8DefinitionCompositionTreeTableNode nextNode;

        lineNodes = new ArrayList<>();
        nextNode = this;
        lineNodes.add(nextNode);
        while (nextNode.getParent() != null)
        {
            nextNode = (T8DefinitionCompositionTreeTableNode)nextNode.getParent();
            lineNodes.add(nextNode);
        }

        // Reverse the order, so that the id's start from the root.
        Collections.reverse(lineNodes);
        return lineNodes;
    }

    public TreePath getPath()
    {
        return new TreePath(getLineNodes());
    }
}
