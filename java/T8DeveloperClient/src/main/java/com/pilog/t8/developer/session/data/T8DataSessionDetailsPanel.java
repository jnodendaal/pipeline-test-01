package com.pilog.t8.developer.session.data;

import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataSessionDetails;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.utilities.components.cellrenderers.CellRendererUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.table.DefaultTableModel;

/**
 * @author Bouwer du Preez
 */
public class T8DataSessionDetailsPanel extends javax.swing.JPanel
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DeveloperView parentView;
    private DefaultTableModel sessionDetailsTableModel;
    private List<T8DataSessionDetails> sessionDetailsList;
    private final Calendar calendar;
    private final SimpleDateFormat formatter;

    public enum FlowStatusPanelMode {GLOBAL, SESSION};

    public T8DataSessionDetailsPanel(T8Context context, T8DeveloperView parentView)
    {
        initComponents();
        this.clientContext = context.getClientContext();
        this.context = context;
        this.parentView = parentView;
        this.sessionDetailsTableModel = (DefaultTableModel)jTableSessionDetails.getModel();
        this.calendar = Calendar.getInstance();
        this.formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss:SSS");
        this.setBorder(new T8ContentHeaderBorder("Data Session Details"));

        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem killSessionItem = new JMenuItem("Kill Session(s)");
        killSessionItem.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                for (int row : T8DataSessionDetailsPanel.this.jTableSessionDetails.getSelectedRows())
                {
                    String dataSessionIdentifier;

                    dataSessionIdentifier = (String)sessionDetailsTableModel.getValueAt(row, 0);
                    try
                    {
                        T8DataManagerOperationHandler.killDataSession(T8DataSessionDetailsPanel.this.context, dataSessionIdentifier);
                    }
                    catch (Exception ex)
                    {
                        T8Log.log("Failed to kill data session " + dataSessionIdentifier, ex);
                    }
                }
                refreshData();
            }
        });
        popupMenu.add(killSessionItem);

        this.jTableSessionDetails.setComponentPopupMenu(popupMenu);
    }

    public void refreshData()
    {
        try
        {
            // Clear the existing data.
            clear();

            // Update the flow statuses.
            sessionDetailsList = T8DataManagerOperationHandler.getDataSessionDetails(context);
            for (T8DataSessionDetails sessionDetails : sessionDetailsList)
            {
                Object[] rowData;

                rowData = new Object[7];
                rowData[0] = sessionDetails.getSessionIdentifier();
                rowData[1] = sessionDetails.getUserIdentifier();
                rowData[2] = sessionDetails.getParentSessionIdentifier();
                rowData[3] = sessionDetails.getParentThreadName();
                rowData[4] = getTimeString(sessionDetails.getCreationTime());
                rowData[5] = getTimeString(sessionDetails.getLastActivityTime());
                rowData[6] = sessionDetails.getActiveTransactionCount();

                sessionDetailsTableModel.addRow(rowData);
            }

            // Resize the table columns.
            CellRendererUtilities.autoSizeTableColumns(jTableSessionDetails, 50);

            // Update the status.
            jLabelStatus.setText("Data Sessions: " + sessionDetailsList.size());
        }
        catch (Exception e)
        {
            T8Log.log("Exception while retrieving session details.", e);
        }
    }

    private String getTimeString(long timeMillis)
    {
        calendar.setTimeInMillis(timeMillis);
        return formatter.format(calendar.getTime());
    }

    public void clear()
    {
        while (sessionDetailsTableModel.getRowCount() > 0)
        {
            sessionDetailsTableModel.removeRow(0);
        }
    }

    public T8DataSessionDetails getSelectedSessionDetails()
    {
        int selectedIndex;

        selectedIndex = jTableSessionDetails.getSelectedRow();
        if (selectedIndex != -1)
        {
            return sessionDetailsList.get(selectedIndex);
        }
        else return null;
    }

    public List<T8DataSessionDetails> getSelectedSessionDetailsList()
    {
        ArrayList<T8DataSessionDetails> selectedList;
        int[] selectedRowIndices;

        selectedRowIndices = jTableSessionDetails.getSelectedRows();
        selectedList = new ArrayList<T8DataSessionDetails>();
        for (int selectedIndex : selectedRowIndices)
        {
            selectedList.add(sessionDetailsList.get(selectedIndex));
        }

        return selectedList;
    }

    @Override
    public void addMouseListener(MouseListener listener)
    {
        jTableSessionDetails.addMouseListener(listener);
    }

    @Override
    public void removeMouseListener(MouseListener listener)
    {
        jTableSessionDetails.removeMouseListener(listener);
    }

    class DataSessionDetailsMouseListener
    {
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jScrollPaneFlowStatuses = new javax.swing.JScrollPane();
        jTableSessionDetails = new javax.swing.JTable();
        jLabelStatus = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        jTableSessionDetails.setAutoCreateRowSorter(true);
        jTableSessionDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Session Identifier", "User Identifier", "Parent Session Identifier", "Parent Thread", "Creation Time", "Last Activity Time", "Open Transactions"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableSessionDetails.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTableSessionDetails.setRowHeight(20);
        jScrollPaneFlowStatuses.setViewportView(jTableSessionDetails);

        add(jScrollPaneFlowStatuses, java.awt.BorderLayout.CENTER);
        add(jLabelStatus, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JScrollPane jScrollPaneFlowStatuses;
    private javax.swing.JTable jTableSessionDetails;
    // End of variables declaration//GEN-END:variables
}
