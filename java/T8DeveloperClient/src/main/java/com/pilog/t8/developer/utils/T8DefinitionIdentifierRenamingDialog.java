package com.pilog.t8.developer.utils;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.utilities.components.cellrenderers.CellRendererUtilities;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionIdentifierRenamingDialog extends javax.swing.JDialog
{
    private T8SessionContext sessionContext;
    private final T8DefinitionContext definitionContext;
    private final List<T8Definition> definitionsToRename;
    private T8IdentifierRenamingTableModel tableModel;
    private final T8Definition contextDefinition;
    private final boolean propagateIdentifierChange;
    private boolean proceed;

    public T8DefinitionIdentifierRenamingDialog(T8DefinitionContext definitionContext, Component parentComponent, T8Definition contextDefinition, List<T8Definition> definitionsToRename, boolean propagateIdentifierChange)
    {
        super(SwingUtilities.getWindowAncestor(parentComponent), ModalityType.APPLICATION_MODAL);
        initComponents();

        this.proceed = false;
        this.sessionContext = definitionContext.getSessionContext();
        this.definitionContext = definitionContext;
        this.contextDefinition = contextDefinition;
        this.definitionsToRename = definitionsToRename;
        this.jLabelMessage.setText("Please enter new identifiers.");
        this.propagateIdentifierChange = propagateIdentifierChange;
        setupDialog();
        setSize(1024, 600);
        setLocationRelativeTo(null);
    }

    private void setupDialog()
    {
        // Now add all of the definitions in the list to the table (their identifiers).
        tableModel = new T8IdentifierRenamingTableModel();
        tableModel.setContextDefinition(contextDefinition);
        tableModel.setDefinitions(definitionsToRename);
        jTableIdentifiers.setGridColor(new Color(240, 240, 240));
        jTableIdentifiers.setModel(tableModel);
        jTableIdentifiers.setRowHeight(25);
        jTableIdentifiers.setAutoCreateRowSorter(true);
        jTableIdentifiers.getColumnModel().getColumn(jTableIdentifiers.convertColumnIndexToView(tableModel.getCompleteIdentifierColumnIndex())).setCellRenderer(new T8IdentifierRenamingCellRenderer(tableModel));

        // Auto-size the columns.
        CellRendererUtilities.autoSizeTableColumns(jTableIdentifiers, 100);

        // Add a few key listeners.
        jTextFieldPrefix.addKeyListener(new PrefixKeyListener());
        jTextFieldSuffix.addKeyListener(new SuffixKeyListener());
        jTextFieldReplaceRegEx.addKeyListener(new ReplacementKeyListener());
        jTextFieldReplacement.addKeyListener(new ReplacementKeyListener());
    }

    public static boolean renameDefinitions(T8DefinitionContext definitionContext, Component parentComponent, T8Definition contextDefinition, List<T8Definition> definitionsToRename, boolean propagateIdentifierChange)
    {
        T8DefinitionIdentifierRenamingDialog dialog;

        dialog = new T8DefinitionIdentifierRenamingDialog(definitionContext, parentComponent, contextDefinition, definitionsToRename, propagateIdentifierChange);
        dialog.setVisible(true);
        return dialog.getSuccess();
    }

    public boolean getSuccess()
    {
        return proceed;
    }

    private boolean checkValidity()
    {
        return tableModel.areAllRowsValid();
    }

    private boolean applyRenaming()
    {
        Map<T8Definition, String> renamingMap;
        List<T8Definition> renamedDefinitions;

        renamedDefinitions = new ArrayList<T8Definition>();
        renamingMap = tableModel.getRenamingMap();
        for (T8Definition definitionToRename : renamingMap.keySet())
        {
            String newIdentifier;

            // Get the new identifier to which the definition will be renamed.
            newIdentifier = renamingMap.get(definitionToRename);

            try
            {
                // If we need to propagate the identifier change, do it.
                if (propagateIdentifierChange)
                {
                    if (renameDefinition(definitionToRename, newIdentifier))
                    {
                        // Add the definition to the list of successfully renamed definitions.
                        renamedDefinitions.add(definitionToRename);
                    }
                }
                else // Just do an unsafe local renaming.
                {
                    T8DefinitionUtilities.unsafeRenameLocalDefinition(definitionToRename, newIdentifier, true);

                    // Add the definition to the list of successfully renamed definitions.
                    renamedDefinitions.add(definitionToRename);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while renaming definition '" + definitionToRename + "' to :" + newIdentifier, e);
                if (JOptionPane.showConfirmDialog(rootPane, "Definition '" + definitionToRename + "' could not be renamed to '" + newIdentifier + "'.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                {
                    break;
                }
            }
        }

        // Remove all of the successfully renamed definitions from the table.
        tableModel.removeDefinitions(renamedDefinitions);

        // If we successfully renamed all definitions, return boolean true.
        return renamedDefinitions.size() == renamingMap.size();
    }

    public boolean renameDefinition(T8Definition definition, String newIdentifier) throws Exception
    {
        List<T8DefinitionHandle> usedByHandles;

        // Find all definitions that use the one to be renamed.
        usedByHandles = definitionContext.findDefinitionsByIdentifier(definition.getPublicIdentifier(), true);

        // If we have any remaining identifiers we know that we have to save the current definition changes before the renaming.
        if (usedByHandles.size() > 0)
        {
            int confirmation;

            confirmation = JOptionPane.showConfirmDialog(this, "The definition is used by " + usedByHandles.size() + " other definitions.  Before renaming it all change will be saved?", "", JOptionPane.YES_NO_OPTION);
            if (confirmation != JOptionPane.YES_OPTION) return false;
        }

        // Do the rename.
        definitionContext.renameDefinition(definition, newIdentifier);
        return true;
    }

    private class PrefixKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                String prefix;

                prefix = jTextFieldPrefix.getText();
                tableModel.addNewIdentifierPrefix(prefix);
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    private class SuffixKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                String suffix;

                suffix = jTextFieldSuffix.getText();
                tableModel.addNewIdentifierSuffix(suffix);
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    private class ReplacementKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                String regex;
                String replacement;

                regex = jTextFieldReplaceRegEx.getText();
                replacement = jTextFieldReplacement.getText();
                tableModel.replaceIdentifierText(regex, replacement);
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jPanelParameters = new javax.swing.JPanel();
        jLabelMessage = new javax.swing.JLabel();
        jPanelFunctions = new javax.swing.JPanel();
        jLabelPrefix = new javax.swing.JLabel();
        jTextFieldPrefix = new javax.swing.JTextField();
        jLabelSuffix = new javax.swing.JLabel();
        jTextFieldSuffix = new javax.swing.JTextField();
        jLabelReplace = new javax.swing.JLabel();
        jTextFieldReplaceRegEx = new javax.swing.JTextField();
        jLabelReplacement = new javax.swing.JLabel();
        jTextFieldReplacement = new javax.swing.JTextField();
        jScrollPaneIdentifierTable = new javax.swing.JScrollPane();
        jTableIdentifiers = new javax.swing.JTable();
        jPanelControls = new javax.swing.JPanel();
        jButtonCancel = new javax.swing.JButton();
        jButtonProceed = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setText("Refresh");
        jButtonRefresh.setToolTipText("Reset new identifiers");
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(jToolBarMain, gridBagConstraints);

        jPanelParameters.setLayout(new java.awt.GridBagLayout());

        jLabelMessage.setText("Message");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 6, 5);
        jPanelParameters.add(jLabelMessage, gridBagConstraints);

        jPanelFunctions.setLayout(new java.awt.GridBagLayout());

        jLabelPrefix.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addPrefixIcon.png"))); // NOI18N
        jLabelPrefix.setText("Prefix:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelFunctions.add(jLabelPrefix, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelFunctions.add(jTextFieldPrefix, gridBagConstraints);

        jLabelSuffix.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addSuffixIcon.png"))); // NOI18N
        jLabelSuffix.setText("Suffix:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelFunctions.add(jLabelSuffix, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelFunctions.add(jTextFieldSuffix, gridBagConstraints);

        jLabelReplace.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/replaceTextIcon.png"))); // NOI18N
        jLabelReplace.setText("Replace:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelFunctions.add(jLabelReplace, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelFunctions.add(jTextFieldReplaceRegEx, gridBagConstraints);

        jLabelReplacement.setText("Replacement String:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelFunctions.add(jLabelReplacement, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelFunctions.add(jTextFieldReplacement, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelParameters.add(jPanelFunctions, gridBagConstraints);

        jTableIdentifiers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        jTableIdentifiers.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPaneIdentifierTable.setViewportView(jTableIdentifiers);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelParameters.add(jScrollPaneIdentifierTable, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        getContentPane().add(jPanelParameters, gridBagConstraints);

        jPanelControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 2, 2));

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCancelActionPerformed(evt);
            }
        });
        jPanelControls.add(jButtonCancel);

        jButtonProceed.setText("Proceed");
        jButtonProceed.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonProceedActionPerformed(evt);
            }
        });
        jPanelControls.add(jButtonProceed);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        getContentPane().add(jPanelControls, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonProceedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonProceedActionPerformed
    {//GEN-HEADEREND:event_jButtonProceedActionPerformed
        if (checkValidity())
        {
            if (applyRenaming())
            {
                proceed = true;
                dispose();
            }
        }
        else JOptionPane.showMessageDialog(rootPane, "Some of the new identifiers are not valid.  Please correct them before proceeding.", "Invalid Identifiers", JOptionPane.ERROR_MESSAGE);
    }//GEN-LAST:event_jButtonProceedActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCancelActionPerformed
    {//GEN-HEADEREND:event_jButtonCancelActionPerformed
        proceed = false;
        dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        tableModel.resetNewIdentifiers();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonProceed;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JLabel jLabelMessage;
    private javax.swing.JLabel jLabelPrefix;
    private javax.swing.JLabel jLabelReplace;
    private javax.swing.JLabel jLabelReplacement;
    private javax.swing.JLabel jLabelSuffix;
    private javax.swing.JPanel jPanelControls;
    private javax.swing.JPanel jPanelFunctions;
    private javax.swing.JPanel jPanelParameters;
    private javax.swing.JScrollPane jScrollPaneIdentifierTable;
    private javax.swing.JTable jTableIdentifiers;
    private javax.swing.JTextField jTextFieldPrefix;
    private javax.swing.JTextField jTextFieldReplaceRegEx;
    private javax.swing.JTextField jTextFieldReplacement;
    private javax.swing.JTextField jTextFieldSuffix;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
