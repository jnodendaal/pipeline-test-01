package com.pilog.t8.developer.definitions.actionhandlers.datarecord.access;

import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.CompositeRequirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class DataRecordAccessPropertyNode extends DataRecordAccessNode
{
    private static final T8Logger LOGGER = T8Log.getLogger(DataRecordAccessPropertyNode.class);

    private final PropertyRequirement propertyRequirement;

    public DataRecordAccessPropertyNode(PropertyRequirement propertyRequirement, DataRecordAccessInstanceNode parentNode)
    {
        super(parentNode);
        this.propertyRequirement = propertyRequirement;
    }

    @Override
    public void loadChildren(T8Context context, T8OntologyClientApi ontApi, T8DataRequirementClientApi drqApi)
    {
        if (!isChildrenLoaded())
        {
            ValueRequirement valueRequirement;

            valueRequirement = propertyRequirement.getValueRequirement();
            if (valueRequirement instanceof DocumentReferenceRequirement)
            {
                try
                {
                    T8OntologyConcept classConcept;
                    DocumentReferenceRequirement referenceRequirement;

                    referenceRequirement = (DocumentReferenceRequirement)valueRequirement;
                    classConcept = ontApi.retrieveConcept(referenceRequirement.getOntologyClassId(), false, false, false, false, false);
                    if (referenceRequirement.isPrescribed())
                    {
                        addChild(new DataRecordAccessPrescribedValueNode(classConcept, propertyRequirement, null, this));
                    }
                    else
                    {
                        addChild(new DataRecordAccessOntologyNode(classConcept, propertyRequirement, null, this));
                    }
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to load property node children " + propertyRequirement.getConceptID(), ex);
                }
            }
            else if (valueRequirement instanceof CompositeRequirement)
            {
                for (FieldRequirement fieldRequirement : ((CompositeRequirement)valueRequirement).getFieldRequirements())
                {
                    addChild(new DataRecordAccessFieldNode(fieldRequirement, this));
                }
            }

            sortChildren();
            setChildrenLoaded(true);
        }
    }

    @Override
    public DataRequirementInstance getDataRequirementInstance()
    {
        return propertyRequirement.getParentDataRequirementInstance();
    }

    @Override
    public String getConceptID()
    {
        return propertyRequirement.getConceptID();
    }

    public PropertyRequirement getPropertyRequirement()
    {
        return propertyRequirement;
    }

    @Override
    public String getDocPathRepresentation(String suffix)
    {
        return "P" + suffix + ":" +  getPropertyRequirement().getConceptID();
    }
}
