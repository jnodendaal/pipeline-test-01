package com.pilog.t8.developer.definitions.graphview.flow;

import com.pilog.graph.model.DefaultGraphEdge;
import com.pilog.graph.model.DefaultGraphGroupVertex;
import com.pilog.graph.model.DefaultGraphModel;
import com.pilog.graph.model.GraphEdge;
import com.pilog.graph.model.GraphModel;
import com.pilog.graph.model.GraphVertexPort;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.graph.T8GraphEdgeDefinition;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowModelBuilder
{
    private T8WorkFlowDefinition definition;
    private final T8WorkFlowVertexRenderer renderer;
    private final T8WorkFlowGraphConfiguration config;

    public T8WorkFlowModelBuilder(T8WorkFlowDefinition definition, T8WorkFlowVertexRenderer renderer, T8WorkFlowGraphConfiguration config)
    {
        this.definition = definition;
        this.renderer = renderer;
        this.config = config;
    }

    public GraphModel createGraphModel(List<T8WorkFlowLayoutElement> layoutElements, List<String> swimLaneIdentifiers)
    {
        Map<String, T8WorkFlowLayoutElement> vertices;
        List<T8GraphEdgeDefinition> edgeDefinitions;
        DefaultGraphModel model;

        model = new DefaultGraphModel();
        vertices = new HashMap<String, T8WorkFlowLayoutElement>();

        // Add all layout elements to the model.
        for (T8WorkFlowLayoutElement vertexElement : layoutElements)
        {
            // Add the vertex to the collection.
            vertices.put(vertexElement.getNodeDefinition().getIdentifier(), vertexElement);

            // If it is a boundary node set its location.
            if (vertexElement.isBoundaryNode())
            {
                T8WorkFlowLayoutElement attachedToElement;

                attachedToElement = vertices.get(vertexElement.getAttachedToNodeIdentifier());
                if (attachedToElement != null)
                {
                    Point attachToVertexCoordinates;
                    Point attachedVertexCoordinates;
                    Dimension attachToVertexSize;
                    Dimension attachedVertexSize;

                    attachToVertexCoordinates = attachedToElement.getVertexCoordinates();
                    attachToVertexSize = attachedToElement.getSize();
                    attachedVertexSize = renderer.getVertexSize(null, vertexElement, false);
                    attachedVertexCoordinates = new Point();
                    attachedVertexCoordinates.x = attachToVertexCoordinates.x - (attachToVertexSize.width/2) + (attachedVertexSize.width/2);
                    attachedVertexCoordinates.y = attachToVertexCoordinates.y + (attachToVertexSize.height/2);

                    vertexElement.setVertexCoordinates(attachedVertexCoordinates);
                    attachedToElement.addChild(vertexElement);
                    model.addVertex(vertexElement);
                }
                else
                {
                    model.addVertex(vertexElement);
                }
            }
            else
            {
                model.addVertex(vertexElement);
            }
        }

        // Add all edges.
        edgeDefinitions = definition.getEdgeDefinitions();
        for (T8GraphEdgeDefinition edgeDefinition : edgeDefinitions)
        {
            T8WorkFlowLayoutElement parentVertex;
            T8WorkFlowLayoutElement childVertex;
            DefaultGraphEdge edge;

            parentVertex = vertices.get(edgeDefinition.getParentNodeIdentifier());
            childVertex = vertices.get(edgeDefinition.getChildNodeIdentifier());
            if (parentVertex == null) T8Log.log("Parent node '" + edgeDefinition.getParentNodeIdentifier() + "' referenced by edge '" + edgeDefinition.getIdentifier() + "' not found.");
            if (childVertex == null) T8Log.log("Child node '" + edgeDefinition.getChildNodeIdentifier() + "' referenced by edge '" + edgeDefinition.getIdentifier() + "' not found.");

            if (parentVertex.isBoundaryNode())
            {
                // For display purposes we only add a south center port type to boundary parent nodes.
                edge = new DefaultGraphEdge(edgeDefinition.getIdentifier(), parentVertex, GraphVertexPort.PortType.SOUTH_CENTER, childVertex, GraphVertexPort.PortType.DYNAMIC_CENTER);
            }
            else
            {
                edge = new DefaultGraphEdge(edgeDefinition.getIdentifier(), parentVertex, GraphVertexPort.PortType.DYNAMIC_CENTER, childVertex, GraphVertexPort.PortType.DYNAMIC_CENTER);
            }

            edge.setEdgeType(config.getEdgeType());
            edge.setEdgeStartArrowType(GraphEdge.EdgeArrowType.NONE);
            edge.setEdgeEndArrowType(GraphEdge.EdgeArrowType.FILLED);
            model.addEdge(edge);
        }

        // Add all swimlanes.
        for (int index = 0; index < swimLaneIdentifiers.size(); index++)
        {
            String swimlaneId;

            swimlaneId = swimLaneIdentifiers.get(index);
            if ((swimlaneId != null) && (!swimlaneId.equals("SYSTEM"))) // The null identifier is the one all nodes belong to that do not belong to any of the other lanes.
            {
                DefaultGraphGroupVertex swimLaneVertex;

                swimLaneVertex = new DefaultGraphGroupVertex(swimlaneId, 0, 0, swimlaneId);
                swimLaneVertex.setZDepth(-10);
                swimLaneVertex.setVertexMovable(true);
                swimLaneVertex.setVertexSelectable(false);
                swimLaneVertex.setInsets(new Insets(50, 70, 50, 50));
                model.addVertex(swimLaneVertex);

                // Add all the vertices that belong in this swim lane to it as children.
                for (T8WorkFlowLayoutElement vertex : vertices.values())
                {
                    if (swimlaneId.equals(vertex.getSwimLaneIdentifier()))
                    {
                        swimLaneVertex.addChild(vertex);
                    }
                }
            }
        }

        return model;
    }
}
