package com.pilog.t8.developer.definitions.projectmanager;

import com.pilog.graph.layout.forcedirected.ForceDirectedLayout;
import com.pilog.graph.model.GraphModel;
import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.view.Graph;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.graph.T8GraphNodeDefinition;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectGraph extends Graph
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ProjectGraph.class);

    private final T8DefinitionContext definitionContext;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final List<T8ProjectDefinition> projectDefinitions;
    private final T8ProjectGraphModelHandler modelHandler;
    private final ForceDirectedLayout layout;

    public T8ProjectGraph(T8DefinitionContext definitionContext)
    {
        super();
        this.clientContext = definitionContext.getClientContext();
        this.context = definitionContext.getContext();
        this.definitionContext = definitionContext;
        this.definitionManager = clientContext.getDefinitionManager();
        this.projectDefinitions = new ArrayList<>();
        this.modelHandler = new T8ProjectGraphModelHandler(context);
        this.layout = new ForceDirectedLayout(null, 1.0, 1000000, 0.1, 0.1, 500.0, 1.0, 500.0);
        this.setGraphLayout(layout);
        this.addKeyListener(new GraphKeyListener());
        this.setVertexRenderer(modelHandler.getVertextRenderer());
        this.setEdgeRenderer(modelHandler.getEdgeRenderer());
        this.setModel(modelHandler.buildGraphModel(projectDefinitions));
    }

    public void setProjectDefinitions(List<T8ProjectDefinition> projectDefinitions)
    {
        this.projectDefinitions.clear();
        if (projectDefinitions != null)
        {
            this.projectDefinitions.addAll(projectDefinitions);

        }

        updateModel();
    }

    public List<T8ProjectDefinition> getProjectDefinitions()
    {
        return new ArrayList<>(projectDefinitions);
    }

    private T8ProjectDefinition getProjectDefinition(String projectId)
    {
        for (T8ProjectDefinition projectDefinition : projectDefinitions)
        {
            if (projectId.equals(projectDefinition.getIdentifier()))
            {
                return projectDefinition;
            }
        }

        return null;
    }

    private void projectDefinitionUpdated(T8ProjectDefinition newDefinition)
    {
        T8ProjectDefinition oldDefinition;

        oldDefinition = getProjectDefinition(newDefinition.getIdentifier());
        if (oldDefinition != null) projectDefinitions.remove(oldDefinition);
        projectDefinitions.add(newDefinition);
    }

    public final void updateModel()
    {
        modelHandler.updateGraphModel(projectDefinitions);
        doGraphLayout();
        updateLayout();
        animate();
    }

    public final void rebuildModel()
    {
        GraphModel model;

        model = modelHandler.buildGraphModel(projectDefinitions);
        modelHandler.randomizeVertexLocations();
        setModel(model);
        doGraphLayout();
        animate();
    }

    public void setSelectedProjects(List<String> projectIds)
    {
        if (projectIds != null)
        {
            List<GraphVertex> verticesToSelect;

            verticesToSelect = new ArrayList<>();
            for (String projectId : projectIds)
            {
                GraphVertex vertexToSelect;

                vertexToSelect = modelHandler.getVertex(projectId);
                if (vertexToSelect != null)
                {
                    verticesToSelect.add(vertexToSelect);
                }
            }

            setSelectedVertices(verticesToSelect);
        }
        else clearSelection();
    }

    public Set<String> getSelectedProjectIds()
    {
        Set<String> prejectIds;

        prejectIds = new HashSet<>();
        for (GraphVertex selectedVertex : this.getSelectedVertices())
        {
            prejectIds.add(selectedVertex.getId());
        }

        return prejectIds;
    }


    public List<T8ProjectDefinition> getSelectedProjectDefinitions()
    {
        List<T8ProjectDefinition> selectedProjects;

        selectedProjects = new ArrayList<>();
        for (GraphVertex selectedVertex : this.getSelectedVertices())
        {
            selectedProjects.add(getProjectDefinition(selectedVertex.getId()));
        }

        return selectedProjects;
    }

    public void addRequiredProject(T8ProjectDefinition projectDefinition, String requiredProjectId)
    {
        try
        {
            T8ProjectDefinition lockedDefinition;
            List<String> requiredProjectIds;
            T8DefinitionHandle projectHandle;

            projectHandle = projectDefinition.getHandle();
            lockedDefinition = (T8ProjectDefinition)definitionManager.lockDefinition(context, projectHandle, context.getSessionId());
            requiredProjectIds = lockedDefinition.getRequiredProjectIds();
            if (!requiredProjectIds.contains(requiredProjectId))
            {
                requiredProjectIds.add(requiredProjectId);
                lockedDefinition.setRequiredProjectIds(requiredProjectIds);
                definitionManager.unlockDefinition(context, lockedDefinition, context.getSessionId());
                projectDefinitionUpdated(lockedDefinition);
                modelHandler.addEdge(projectDefinition.getIdentifier(), requiredProjectId);
                Toast.show(requiredProjectId + " added as required project", Toast.Style.SUCCESS);
                updateLayout();
                animate();
            }
            else
            {
                definitionManager.unlockDefinition(context, projectHandle, context.getSessionId());
                Toast.show(requiredProjectId + " already marked as required project", Toast.Style.ERROR);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while adding required project " + requiredProjectId + " to project " + projectDefinition + ".", e);
            Toast.show("Unexpected Exception: " + e.getMessage(), Toast.Style.ERROR);
        }
    }

    public void removeRequiredProject(T8ProjectDefinition projectDefinition, String requiredProjectId)
    {
        try
        {
            T8ProjectDefinition lockedDefinition;
            List<String> requiredProjectIds;
            T8DefinitionHandle projectHandle;

            projectHandle = projectDefinition.getHandle();
            lockedDefinition = (T8ProjectDefinition)definitionManager.lockDefinition(context, projectHandle, context.getSessionId());
            requiredProjectIds = lockedDefinition.getRequiredProjectIds();
            if (requiredProjectIds.contains(requiredProjectId))
            {
                requiredProjectIds.remove(requiredProjectId);
                lockedDefinition.setRequiredProjectIds(requiredProjectIds);
                definitionManager.unlockDefinition(context, lockedDefinition, context.getSessionId());
                projectDefinitionUpdated(lockedDefinition);
                modelHandler.removeEdge(projectDefinition.getIdentifier(), requiredProjectId);
                Toast.show(requiredProjectId + " remove as required project", Toast.Style.SUCCESS);
                updateLayout();
                animate();
            }
            else
            {
                definitionManager.unlockDefinition(context, projectHandle, context.getSessionId());
                Toast.show(requiredProjectId + " is not marked as required project", Toast.Style.ERROR);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while removing required project " + requiredProjectId + " from project " + projectDefinition + ".", e);
            Toast.show("Unexpected Exception: " + e.getMessage(), Toast.Style.ERROR);
        }
    }

    public T8ProjectGraphModelHandler getGraphManager()
    {
        return modelHandler;
    }

    private class GraphKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
            if ((e.getKeyCode() == KeyEvent.VK_LEFT) || (e.getKeyCode() == KeyEvent.VK_RIGHT))
            {
                if (e.isControlDown())
                {
                    List<GraphVertex> selectedVertices;

                    selectedVertices = getSelectedVertices();
                    if (selectedVertices.size() > 0)
                    {
                        GraphVertex selectedVertex;
                        Object selectedObject;

                        selectedVertex = selectedVertices.get(0);
                        selectedObject = selectedVertex.getVertexDataObject();
                        if (selectedObject instanceof T8GraphNodeDefinition)
                        {
                            T8GraphNodeDefinition graphNodeDefinition;
                            int cellXCoordinate;

                            graphNodeDefinition = (T8GraphNodeDefinition)selectedObject;
                            cellXCoordinate = graphNodeDefinition.getXCoordinate();

                            // Make sure the coordinate is valid and if not, set it to the default
                            if (cellXCoordinate < 0)
                            {
                                cellXCoordinate = 1;
                            }

                            // Now adjust the coordinate according to the key pressed.
                            if ((e.getKeyCode() == KeyEvent.VK_LEFT) && (cellXCoordinate > 0))
                            {
                                cellXCoordinate--;
                                graphNodeDefinition.setXCoordinate(cellXCoordinate);
                                updateModel();
                            }
                            else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
                            {
                                cellXCoordinate++;
                                graphNodeDefinition.setXCoordinate(cellXCoordinate);
                                updateModel();
                            }
                        }
                    }
                }
            }
        }
    }
}
