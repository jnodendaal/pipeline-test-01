package com.pilog.t8.developer.definitions;

import com.pilog.t8.definition.T8Definition;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * @author Bouwer du Preez
 */
public class TransferableT8Definition implements Transferable
{
    private T8Definition data;
    private DataFlavor flavors[] = { t8DefinitionFlavor };
    public static DataFlavor t8DefinitionFlavor = new DataFlavor(T8Definition.class, "T8Definition");

    public TransferableT8Definition(T8Definition definition)
    {
        data = definition;
    }

    @Override
    public synchronized Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException
    {
        if (isDataFlavorSupported(flavor)) return data;
        else throw new UnsupportedFlavorException(flavor);
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor)
    {
        return (flavor.getRepresentationClass() == T8Definition.class);
    }

    @Override
    public synchronized DataFlavor[] getTransferDataFlavors()
    {
        return flavors;
    }
}
