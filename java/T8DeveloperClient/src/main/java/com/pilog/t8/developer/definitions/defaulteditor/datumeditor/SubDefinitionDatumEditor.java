package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionConstructorParameters;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.event.T8DefinitionDatumEditorListener;
import com.pilog.t8.ui.event.T8DefinitionEditorListener;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.developer.definitions.defaulteditor.T8DefaultDefinitionEditor;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

/**
 * @author Bouwer du Preez
 */
public class SubDefinitionDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private List<T8DefaultDefinitionEditor> definitionEditors;
    private final T8DefinitionEditorListener subDefinitionEditorListener;
    private boolean editable;

    public SubDefinitionDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        this.definitionEditors = new ArrayList<T8DefaultDefinitionEditor>();
        this.editable = false;
        this.subDefinitionEditorListener = new SubDefinitionEditorListener();
    }

    public SubDefinitionDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType, String typeIdentifier)
    {
        this(definitionContext, definition, datumType);
        // This constructor is kept for backwards compatibility but must be removed in future.
    }

    @Override
    public void initializeComponent()
    {
        Object value;

        T8Log.log("Initializing sub-definition editor for definition datum: " + definition + ":" + datumType.getIdentifier());
        value = getDefinitionDatum();
        clearDefinitionEditors();
        if (value != null)
        {
            if (value instanceof T8Definition)
            {
                addDefinitionEditor((T8Definition)value);
            }
            else if (value instanceof List)
            {
                for (T8Definition valueDefinition : (List<T8Definition>)value)
                {
                    addDefinitionEditor(valueDefinition);
                }
            }
        }
    }

    @Override
    public void startComponent()
    {
        T8Log.log("Starting sub-definition editor for definitino datum: " + definition + ":" + datumType.getIdentifier());
        for (T8DefaultDefinitionEditor definitionEditor : definitionEditors)
        {
            definitionEditor.startComponent();
        }
    }

    @Override
    public void stopComponent()
    {
        T8Log.log("Stopping sub-definition editor for definition datum: " + definition + ":" + datumType.getIdentifier());
    }

    @Override
    public void commitChanges()
    {
        T8Log.log("Committing sub-definition editor changes for definition datum: " + definition + ":" + datumType.getIdentifier());
        for (T8DefaultDefinitionEditor definitionEditor : definitionEditors)
        {
            definitionEditor.commitChanges();
        }
    }

    @Override
    public void setEditable(boolean editable)
    {
        this.editable = editable;
        jButtonAdd.setEnabled(editable);
        for (T8DefaultDefinitionEditor editor : definitionEditors)
        {
            editor.setEditable(editable);
        }
    }

    @Override
    public void refreshEditor()
    {
        Object datumValue;

        T8Log.log("Refreshing sub-definition editor for definition datum: " + definition + ":" + datumType.getIdentifier());
        datumValue = getDefinitionDatum();
        if (datumValue == null)
        {
            clearDefinitionEditors();
        }
        else if (datumValue instanceof T8Definition)
        {
            T8Definition subDefinition;

            subDefinition = (T8Definition)datumValue;
            if (definitionEditors.isEmpty())
            {
                T8DefaultDefinitionEditor editor;

                editor = addDefinitionEditor(subDefinition);
                editor.refreshEditor();
            }
            else if (definitionEditors.size() == 1)
            {
                definitionEditors.get(0).refreshEditor();
            }
            else throw new RuntimeException("Inconsistent datum value found.  Singe definition expected, but multiple editors found.");
        }
        else if (datumValue instanceof List)
        {
            List<T8Definition> definitionList;
            List<T8DefaultDefinitionEditor> newEditorList;

            definitionList = (List<T8Definition>)datumValue;
            newEditorList = new ArrayList<T8DefaultDefinitionEditor>();

            jPanelEditors.removeAll();
            for (T8Definition valueDefinition : definitionList)
            {
                T8DefaultDefinitionEditor editor;

                editor = getDefinitionEditor(valueDefinition.getIdentifier());
                if (editor == null)
                {
                    editor = new T8DefaultDefinitionEditor(definitionContext, definition, false);
                    editor.addSystemAction(new EditorDeleteAction(editor));
                    newEditorList.add(editor);
                    jPanelEditors.add(editor);
                    editor.initializeComponent();
                    editor.setDefinitionDetailsVisible(false, false, false);
                    editor.refreshEditor();
                }
                else
                {
                    newEditorList.add(editor);
                    jPanelEditors.add(editor);
                    editor.refreshEditor();
                }
            }

            // Replace the old editor list with the new one.
            definitionEditors = newEditorList;
            jPanelEditors.revalidate();
            validate();
        }
    }

    private T8DefaultDefinitionEditor getDefinitionEditor(String identifier)
    {
        for (T8DefaultDefinitionEditor definitionEditor : definitionEditors)
        {
            if (definitionEditor.getDefinitionIdentifier().equals(identifier))
            {
                return definitionEditor;
            }
        }

        return null;
    }
    private void clearDefinitionEditors()
    {
        // First stop the editors.
        for (T8DefaultDefinitionEditor definitionEditor : definitionEditors)
        {
            definitionEditor.commitChanges();
        }

        // Remove all editors from the UI.
        jPanelEditors.removeAll();
        jPanelEditors.revalidate();
        validate();

        // Clear the editor list.
        definitionEditors.clear();
    }

    private void removeDefinition(String identifier)
    {
        for (T8DefaultDefinitionEditor definitionEditor : definitionEditors)
        {
            if (definitionEditor.getDefinitionIdentifier().equals(identifier))
            {
                // Remove the definition editor from the UI.
                definitionEditor.commitChanges();
                jPanelEditors.remove(definitionEditor);
                definitionEditors.remove(definitionEditor);
                jPanelEditors.revalidate();
                validate();

                // Remove the definition from the datum value.
                if (datumType.isSingleDefinitionType())
                {
                    setDefinitionDatum(null);
                }
                else if (datumType.isDefinitionListType())
                {
                    List<T8Definition> value;

                    value = (List<T8Definition>)getDefinitionDatum();
                    value.remove(definitionEditor.getDefinition());
                    setDefinitionDatum(new ArrayList(value));
                }

                return; // Else we'll get concurrent modification exception.
            }
        }
    }

    private T8DefaultDefinitionEditor addDefinitionEditor(T8Definition subDefinition)
    {
        T8DefaultDefinitionEditor definitionEditor;

        definitionEditor = new T8DefaultDefinitionEditor(definitionContext, subDefinition, false);
        definitionEditor.addSystemAction(new EditorDeleteAction(definitionEditor));
        definitionEditors.add(definitionEditor);
        jPanelEditors.add(definitionEditor);
        definitionEditor.initializeComponent();
        definitionEditor.setDefinitionDetailsVisible(false, false, false);
        definitionEditor.addDefinitionEditorListener(subDefinitionEditorListener);
        jPanelEditors.revalidate();
        validate();
        return definitionEditor;
    }

    private void addDefinition()
    {
        if ((datumType.getMaximumElements() == -1) || (definitionEditors.size() < datumType.getMaximumElements()))
        {
            try
            {
                T8DefinitionConstructorParameters constructorParameters;
                List<T8DefinitionDatumOption> datumOptions;
                T8Definition newDefinition;
                String typeIdentifier;

                // Get the type identifier of the definition to construct from the datum options.
                datumOptions = definition.getDatumOptions(definitionContext, datumType.getIdentifier());
                constructorParameters = (T8DefinitionConstructorParameters)datumOptions.get(0).getValue();
                typeIdentifier = constructorParameters.getTypeIdentifier();

                T8Log.log("Creating new definition of type: " + typeIdentifier);
                newDefinition = clientContext.getDefinitionManager().createNewDefinition(T8IdentifierUtilities.generateLocalIdentifier(), typeIdentifier);
                if (datumType.isSingleDefinitionType())
                {
                    T8DefaultDefinitionEditor editor;

                    T8Log.log("Setting new definition: " + newDefinition + " as datum value: " + datumType.getIdentifier() + " in definition: " + definition);
                    setDefinitionDatum(newDefinition);
                    editor = addDefinitionEditor(newDefinition);
                    editor.refreshEditor();
                }
                else if (datumType.isDefinitionListType())
                {
                    T8DefaultDefinitionEditor editor;
                    List<T8Definition> value;

                    T8Log.log("Adding new definition: " + newDefinition + " as datum value: " + datumType.getIdentifier() + " in definition: " + definition);
                    value = (List<T8Definition>)getDefinitionDatum();
                    value.add(newDefinition);
                    setDefinitionDatum(new ArrayList(value));
                    editor = addDefinitionEditor(newDefinition);
                    editor.refreshEditor();
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while creating new definition.", e);
            }
        }
        else
        {
            JOptionPane.showMessageDialog(this, "The maximum number of definitions for this type has already been added.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void fireDefinitionLinkActivatedEvent(String projectId, String definitionId)
    {
        T8DefinitionLinkEvent event;

        event = new T8DefinitionLinkEvent(this, projectId, definitionId);
        for (T8DefinitionDatumEditorListener listener : eventListeners.getListeners(T8DefinitionDatumEditorListener.class))
        {
            listener.definitionLinkActivated(event);
        }
    }

    private class EditorDeleteAction extends AbstractAction
    {
        private T8DefaultDefinitionEditor editor;

        public EditorDeleteAction(final T8DefaultDefinitionEditor editor)
        {
            this.editor = editor;
            this.putValue(Action.SHORT_DESCRIPTION, "Remove this definition");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeDocumentIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            removeDefinition(editor.getDefinitionIdentifier());
        }
    }

    private class SubDefinitionEditorListener implements T8DefinitionEditorListener
    {
        @Override
        public void definitionLinkActivated(T8DefinitionLinkEvent event)
        {
            fireDefinitionLinkActivatedEvent(event.getProjectId(), event.getDefinitionId());
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jPanelEditors = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAdd);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);

        jPanelEditors.setLayout(new javax.swing.BoxLayout(jPanelEditors, javax.swing.BoxLayout.Y_AXIS));
        add(jPanelEditors, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        addDefinition();
    }//GEN-LAST:event_jButtonAddActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JPanel jPanelEditors;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
