package com.pilog.t8.developer.view;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionSelectionEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.ui.event.T8ViewHeaderChangedEvent;
import com.pilog.t8.utilities.components.tabbedpane.tab.ClosableTabComponent;
import com.pilog.t8.utilities.components.tabbedpane.tab.TabClosedEvent;
import com.pilog.t8.utilities.components.tabbedpane.tab.TabClosingEvent;
import com.pilog.t8.utilities.components.tabbedpane.tab.TabComponentListener;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8TabbedDeveloperViewContainer extends T8DefaultDeveloperView implements T8DeveloperView
{
    private final T8Context context;
    private T8DeveloperViewFactory viewFactory;
    private final List<T8DeveloperView> views;
    private final TabListener tabListener;
    private final TabViewListener tabViewListener;

    public T8TabbedDeveloperViewContainer(T8Context context, T8DeveloperViewFactory viewFactory)
    {
        initComponents();
        this.context = context;
        this.views = new ArrayList<>();
        this.viewFactory = viewFactory;
        this.tabListener = new TabListener();
        this.tabViewListener = new TabViewListener();
    }

    @Override
    public void startComponent()
    {
        for (T8DeveloperView view : views)
        {
            view.startComponent();
        }
    }

    @Override
    public void stopComponent()
    {
        for (T8DeveloperView view : views)
        {
            view.stopComponent();
        }
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return getSelectedView().getSelectedDefinition();
    }

    private T8DeveloperView getSelectedView()
    {
        return (T8DeveloperView)jTabbedPaneViews.getSelectedComponent();
    }

    @Override
    public void addView(T8DeveloperView newView)
    {
        addView(newView, null);
    }

    @Override
    public void addView(T8DeveloperView newView, String viewHeader)
    {
        String tabTitle;

        tabTitle = viewHeader == null ? newView.getHeader() : viewHeader;
        jTabbedPaneViews.add(tabTitle, (Component)newView);
        ClosableTabComponent closableTabComponent = new ClosableTabComponent(jTabbedPaneViews, tabTitle);
        closableTabComponent.addTabComponentListener(tabListener);
        jTabbedPaneViews.setTabComponentAt(jTabbedPaneViews.getTabCount()-1, closableTabComponent);
        jTabbedPaneViews.setSelectedIndex(jTabbedPaneViews.getTabCount()-1);
        newView.addDefinitionViewListener(tabViewListener);
        views.add(newView);

        newView.startComponent();
    }

    private ClosableTabComponent getViewTabComponent(T8DeveloperView view)
    {
        int index;

        index = views.indexOf(view);
        if (index > -1)
        {
            return (ClosableTabComponent)jTabbedPaneViews.getTabComponentAt(index);
        }
        else return null;
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        view.stopComponent();
        jTabbedPaneViews.remove((Component)view);
        views.remove(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
        for (T8DeveloperView view : views)
        {
            view.setViewFactory(viewFactory);
        }
    }

    @Override
    public String getHeader()
    {
        return "Tabbed Developer View Container";
    }

    @Override
    public void addDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    @Override
    public void removeDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    @Override
    public void setSelectedDefinition(T8Definition selectedDefinition)
    {
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
        String definitionId;

        definitionId = event.getDefinitionId();
        if (T8IdentifierUtilities.isQualifiedLocalId(definitionId) || T8IdentifierUtilities.isPublicId(definitionId))
        {
            T8DeveloperView developerView;

            developerView = getViewFactory().createDefinitionNavigator(this, event.getProjectId(), T8IdentifierUtilities.getGlobalIdentifierPart(definitionId));
            addView(developerView);

            if (T8IdentifierUtilities.isQualifiedLocalId(definitionId))
            {
                developerView.notifyDefinitionLinkActivated(new T8DefinitionLinkEvent((Component) event.getSource(), event.getProjectId(), T8IdentifierUtilities.getLocalIdentifierPart(definitionId)));
            }
        }
        else
        {
            for (T8DeveloperView view : views)
            {
                if (event.getSource() != view)
                {
                    view.definitionLinkActivated(event.getDefinitionId());
                }
            }
        }
    }

    @Override
    public boolean canClose()
    {
        for (T8DeveloperView t8DeveloperView : views)
        {
            if (!t8DeveloperView.canClose())
            {
                return false;
            }
        }
        return true;
    }

    private class TabListener implements TabComponentListener
    {
        @Override
        public void tabClosed(TabClosedEvent event)
        {
            removeView((T8DeveloperView)event.getTabClosed());
        }

        @Override
        public void tabClosing(TabClosingEvent event)
        {
            Component closedComponent;

            closedComponent = event.getTabClosing();
            if (closedComponent instanceof T8DeveloperView)
            {
                if (!((T8DeveloperView)closedComponent).canClose())
                {
                    event.cancel();
                }
            }
        }
    }

    private class TabViewListener implements T8DefinitionViewListener
    {
        @Override
        public void selectionChanged(T8DefinitionSelectionEvent event)
        {
        }

        @Override
        public void definitionLinkActivated(T8DefinitionLinkEvent event)
        {
        }

        @Override
        public void headerChanged(T8ViewHeaderChangedEvent event)
        {
            ClosableTabComponent tabComponent;
            T8DeveloperView view;

            view = event.getView();
            tabComponent = getViewTabComponent(view);
            if (tabComponent != null)
            {
                tabComponent.setText(event.getNewHeader());
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPaneViews = new javax.swing.JTabbedPane();

        setLayout(new java.awt.BorderLayout());
        add(jTabbedPaneViews, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane jTabbedPaneViews;
    // End of variables declaration//GEN-END:variables
}

