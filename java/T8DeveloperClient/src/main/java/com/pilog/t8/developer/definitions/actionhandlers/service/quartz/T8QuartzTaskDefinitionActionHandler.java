/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.developer.definitions.actionhandlers.service.quartz;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceAPIHandler;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceDefinition;
import com.pilog.t8.definition.quartz.task.T8QuartzProcessTaskDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8QuartzTaskDefinitionActionHandler implements T8DefinitionActionHandler
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8QuartzTaskDefinitionActionHandler.class.getName());

    private T8ClientContext clientContext;
    private T8Context context;
    private T8QuartzProcessTaskDefinition definition;

    public T8QuartzTaskDefinitionActionHandler(T8Context context, T8QuartzProcessTaskDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>(1);
        actions.add(new T8QuartzTaskDefinitionActionHandler.TriggerTask(context, definition));

        return actions;
    }

    private static class TriggerTask extends AbstractAction
    {
        private T8ClientContext clientContext;
        private T8Context context;
        private T8QuartzProcessTaskDefinition definition;
        private T8QuartzServiceDefinition serviceDefinition;

        public TriggerTask(T8Context context, T8QuartzProcessTaskDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.serviceDefinition = (T8QuartzServiceDefinition) definition.getParentDefinition();
            this.putValue(Action.SHORT_DESCRIPTION, "Trigger Task");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/testIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            Map<String, Object> operationParameters;
            try
            {
                operationParameters = new HashMap<String,Object>(1);
                operationParameters.put(T8QuartzServiceAPIHandler.PARAMETER_TASK_IDENTIFIER, definition.getIdentifier());
                clientContext.getServiceManager().executeServiceOperation(context, serviceDefinition.getIdentifier(), T8QuartzServiceAPIHandler.OPERATION_TRIGGER_TASK, operationParameters);
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to trigger task on service", ex);
            }
        }
    }
}
