package com.pilog.t8.developer.definitions.test.harness;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.ui.dialog.T8GlobalDialogDefinition;
import com.pilog.t8.developer.definitions.test.T8GlobalDialogTestFrame;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8GlobalDialogTestHarness implements T8DefinitionTestHarness
{
    public T8GlobalDialogTestHarness()
    {
    }

    @Override
    public void configure(T8Context context)
    {
    }

    @Override
    public void testDefinition(T8Context context, T8Definition definition)
    {
        T8GlobalDialogTestFrame.testDialogDefinition(context, (T8GlobalDialogDefinition)definition, this);
    }
}
