package com.pilog.t8.developer.definitions.actionhandlers.data.entity;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityDefinitionActionHandler implements T8DefinitionActionHandler
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DataEntityDefinition definition;

    public T8DataEntityDefinitionActionHandler(T8Context context, T8DataEntityDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new FieldAutoCreationAction(context, definition));

        return actions;
    }

    private static class FieldAutoCreationAction extends AbstractAction
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8DataEntityDefinition definition;

        public FieldAutoCreationAction(T8Context context, T8DataEntityDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Auto-Create Entity Fields from Data Source");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                String dataSourceId;

                dataSourceId = definition.getDataSourceIdentifier();
                if (dataSourceId != null)
                {
                    List<T8DataSourceFieldDefinition> sourceFieldDefinitions;
                    List<T8DataEntityFieldDefinition> entityFieldDefinitions;
                    T8DataSourceDefinition dataSourceDefinition;

                    T8Log.log("Retrieving field definitions for data source: " + dataSourceId);
                    dataSourceDefinition = (T8DataSourceDefinition)clientContext.getDefinitionManager().getInitializedDefinition(context, definition.getRootProjectId(), dataSourceId, null);
                    sourceFieldDefinitions = dataSourceDefinition.getFieldDefinitions();

                    entityFieldDefinitions = new ArrayList<T8DataEntityFieldDefinition>();
                    if (sourceFieldDefinitions != null)
                    {
                        for (T8DataSourceFieldDefinition sourceFieldDefinition : sourceFieldDefinitions)
                        {
                            T8DataEntityFieldDefinition entityFieldDefinition;

                            entityFieldDefinition = new T8DataEntityFieldDefinition(sourceFieldDefinition.getIdentifier());
                            entityFieldDefinition.setSourceIdentifier(sourceFieldDefinition.getPublicIdentifier());
                            entityFieldDefinition.setKey(sourceFieldDefinition.isKey());
                            entityFieldDefinition.setRequired(sourceFieldDefinition.isRequired());
                            entityFieldDefinition.setDataType(sourceFieldDefinition.getDataType());
                            entityFieldDefinitions.add(entityFieldDefinition);
                        }
                    }

                    T8Log.log("Field definitions for data source: " + dataSourceId + ": " + sourceFieldDefinitions.size());
                    definition.setFieldDefinitions(entityFieldDefinitions);
                }
                else throw new Exception("No data source specified.");
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving field definitions for data source: " + definition.getIdentifier(), e);
            }
        }
    }
}
