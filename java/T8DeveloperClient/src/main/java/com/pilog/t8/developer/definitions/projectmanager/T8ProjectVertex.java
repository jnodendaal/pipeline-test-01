package com.pilog.t8.developer.definitions.projectmanager;

import com.pilog.graph.model.DefaultGraphVertex;
import com.pilog.t8.developer.definitions.graphview.layout.LayoutElement;
import com.pilog.t8.developer.definitions.graphview.layout.LayoutInsets;
import java.awt.Dimension;
import java.awt.Point;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectVertex extends DefaultGraphVertex implements LayoutElement
{
    private final T8ProjectVertexRenderer renderer;
    private final LayoutInsets insets;

    public T8ProjectVertex(String projectId, T8ProjectVertexRenderer renderer)
    {
        super(projectId, 0, 0, projectId);
        this.renderer = renderer;
        this.insets = new LayoutInsets(0, 0, 0, 0);
    }

    @Override
    public LayoutInsets getInsets()
    {
        return insets;
    }

    @Override
    public Dimension getSize()
    {
        return renderer.getVertexSize(null, this, false);
    }

    @Override
    public void doLayout(int x, int y)
    {
        Dimension size;
        Point location;

        size = getSize();
        location = new Point(x, y);
        location.translate(Math.round(size.width/2.0f), Math.round(size.height/2.0f));
        setVertexCoordinates(location);
    }

    public int getXCoordinate()
    {
        return (int)getVertexCoordinates().getX();
    }

    public int getYCoordinate()
    {
        return (int)getVertexCoordinates().getY();
    }
}
