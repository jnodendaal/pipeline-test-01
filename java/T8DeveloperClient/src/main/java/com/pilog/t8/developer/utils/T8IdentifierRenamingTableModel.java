package com.pilog.t8.developer.utils;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

/**
 * @author Bouwer du Preez
 */
public class T8IdentifierRenamingTableModel extends DefaultTableModel
{
    // Column Types.
    Class[] types = new Class []
    {
        String.class, String.class, String.class, String.class, String.class
    };

    // Column Editability.
    boolean[] canEdit = new boolean []
    {
        false, false, false, true, false
    };

    private IdentifierTableModelListener listener;
    private boolean refreshingCompleteIdentifiers;
    private List<T8Definition> definitionList;
    private List<Integer> invalidRows;
    private T8Definition contextDefinition;

    public T8IdentifierRenamingTableModel()
    {
        // Construct the table columns.
        super
        (
            new Object [][]
            {

            },
            new String []
            {
                "Definition Type", "Old Identifier", "Prefix", "New Identifier", "Complete Identifier"
            }
        );

        // Create the list that will hold any definitions added to the model.
        definitionList = new ArrayList<T8Definition>();
        invalidRows = new ArrayList<Integer>();

        // Add the model listener.
        listener = new IdentifierTableModelListener();
        this.addTableModelListener(listener);
        this.refreshingCompleteIdentifiers = false;
    }

    @Override
    public Class getColumnClass(int columnIndex)
    {
        return types [columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return canEdit [columnIndex];
    }

    public void setContextDefinition(T8Definition contextDefinition)
    {
        this.contextDefinition = contextDefinition;
    }

    public void setDefinitions(List<T8Definition> definitionList)
    {
        // Remove the listener so that we don't do updated unnecessarily.
        removeTableModelListener(listener);

        // Clear existing data.
        if (getRowCount() > 0) setRowCount(0);

        // Add all of the rows to the table model.
        if (definitionList != null)
        {
            for (T8Definition definition : definitionList)
            {
                addDefinition(definition);
            }
        }

        // Add the listener back on to the model.
        addTableModelListener(listener);

        // Refresh complete identifiers.
        refreshCompleteIdentifiers();
    }

    public void addDefinition(T8Definition definition)
    {
        T8DefinitionTypeMetaData typeMetaData;
        Vector definitionRow;
        String identifier;
        String typePrefix;

        typeMetaData = definition.getTypeMetaData();
        identifier = definition.getIdentifier();
        typePrefix = typeMetaData.getIdPrefix();
        if (typePrefix == null) typePrefix = ""; // Make sure we don't concatenate 'null' to the identifier.

        // Add all of the row values.
        definitionRow = new Vector();
        definitionRow.add(typeMetaData.getTypeId());
        definitionRow.add(identifier);
        definitionRow.add(typePrefix);
        definitionRow.add(T8IdentifierUtilities.getNonFixedIdentifierPart(definition));
        definitionRow.add(null);

        // Add the new row to the table model.
        addRow(definitionRow);
        definitionList.add(definition);
    }

    public void addNewIdentifierPrefix(String prefix)
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            String newIdentifier;

            newIdentifier = getNewIdentifier(rowIndex);
            setNewIdentifier(rowIndex, prefix + newIdentifier);
        }
    }

    public void addNewIdentifierSuffix(String suffix)
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            String newIdentifier;

            newIdentifier = getNewIdentifier(rowIndex);
            setNewIdentifier(rowIndex, newIdentifier + suffix);
        }
    }

    public void replaceIdentifierText(String regex, String replacementString)
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            String newIdentifier;

            newIdentifier = getNewIdentifier(rowIndex);
            newIdentifier = newIdentifier.replaceAll(regex, replacementString);
            setNewIdentifier(rowIndex, newIdentifier);
        }
    }

    public void resetNewIdentifiers()
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            T8Definition rowDefinition;

            rowDefinition = definitionList.get(rowIndex);
            setNewIdentifier(rowIndex, T8IdentifierUtilities.getNonFixedIdentifierPart(rowDefinition));
        }
    }

    public void refreshCompleteIdentifiers()
    {
        invalidRows.clear();
        refreshingCompleteIdentifiers = true;
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            String completeIdentifier;
            T8Definition definition;

            // Get the definition corresponding to the row index.
            definition = definitionList.get(rowIndex);

            // Update the complete identifier.
            completeIdentifier = (T8IdentifierUtilities.isPublicId(definition.getIdentifier()) ? T8Definition.getPublicIdPrefix() : T8Definition.getLocalIdPrefix());
            completeIdentifier += T8IdentifierUtilities.createIdentifierString(getPrefix(rowIndex) + getNewIdentifier(rowIndex));
            setCompleteIdentifier(rowIndex, completeIdentifier);

            // Check the validity of the new identifier.
            if (!isValidIdentifier(completeIdentifier))
            {
                invalidRows.add(rowIndex);
            }
        }
        refreshingCompleteIdentifiers = false;
    }

    public Map<T8Definition, String> getRenamingMap()
    {
        Map<T8Definition, String> renamingMap;

        renamingMap = new LinkedHashMap<T8Definition, String>();
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            renamingMap.put(definitionList.get(rowIndex), getCompleteIdentifier(rowIndex));
        }

        return renamingMap;
    }

    public void removeDefinitions(List<T8Definition> definitionsToRemove)
    {
        for (T8Definition definitionToRemove : definitionsToRemove)
        {
            int rowIndex;

            // Get the row index of the definition to remove.
            rowIndex = definitionList.indexOf(definitionToRemove);

            // Remove the row from all active collections where it may be in use.
            definitionList.remove(rowIndex);
            invalidRows.remove((Object)((Integer)rowIndex)); // We have to cast to Object to ensure the correct method gets called.
            removeRow(rowIndex);
        }
    }

    private boolean isValidIdentifier(String newIdentifier)
    {
        if (newIdentifier.matches(T8Definition.getIdRegex()))
        {
            if (contextDefinition != null)
            {
                return contextDefinition.getLocalDefinition(newIdentifier) == null;
            }
            else return true;
        }
        else return false;
    }

    public boolean isValid(int rowIndex)
    {
        return !invalidRows.contains(rowIndex);
    }

    public boolean areAllRowsValid()
    {
        return invalidRows.size() == 0;
    }

    public int getDefinitionTypeColumnIndex()
    {
        return 0;
    }

    public int getOldIdentifierColumnIndex()
    {
        return 1;
    }

    public int getPrefixColumnIndex()
    {
        return 2;
    }

    public int getNewIdentifierColumnIndex()
    {
        return 3;
    }

    public int getCompleteIdentifierColumnIndex()
    {
        return 4;
    }

    public String getDefinitionType(int rowIndex)
    {
        return (String)getValueAt(rowIndex, getDefinitionTypeColumnIndex());
    }

    public String getOldIdentifier(int rowIndex)
    {
        return (String)getValueAt(rowIndex, getOldIdentifierColumnIndex());
    }

    public String getPrefix(int rowIndex)
    {
        return (String)getValueAt(rowIndex, getPrefixColumnIndex());
    }

    public String getNewIdentifier(int rowIndex)
    {
        return (String)getValueAt(rowIndex, getNewIdentifierColumnIndex());
    }

    private void setNewIdentifier(int rowIndex, String identifier)
    {
        setValueAt(identifier, rowIndex, getNewIdentifierColumnIndex());
    }

    public String getCompleteIdentifier(int rowIndex)
    {
        return (String)getValueAt(rowIndex, getCompleteIdentifierColumnIndex());
    }

    private void setCompleteIdentifier(int rowIndex, String identifier)
    {
        setValueAt(identifier, rowIndex, getCompleteIdentifierColumnIndex());
    }

    public int getColumnIndex(String columnName)
    {
        for (int columnIndex = 0; columnIndex < this.getColumnCount(); columnIndex++)
        {
            if (columnName.equals(getColumnName(columnIndex))) return columnIndex;
        }

        return -1;
    }

    private class IdentifierTableModelListener implements TableModelListener
    {
        @Override
        public void tableChanged(TableModelEvent e)
        {
            if (!refreshingCompleteIdentifiers) refreshCompleteIdentifiers();
        }
    }
}
