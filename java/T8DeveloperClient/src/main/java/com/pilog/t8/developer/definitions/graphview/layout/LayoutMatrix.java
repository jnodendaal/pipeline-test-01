package com.pilog.t8.developer.definitions.graphview.layout;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class LayoutMatrix implements LayoutElement
{
    private ArrayList<ArrayList<LayoutElement>> matrix;
    private Point location;
    private LayoutInsets insets;
    private int horizontalSpacing;
    private int verticalSpacing;
    private HorizontalAllignment horizontalAllignment;
    private VerticalAllignment verticalAllignment;
    
    public enum HorizontalAllignment {LEFT, CENTER, RIGHT};
    public enum VerticalAllignment {TOP, CENTER, BOTTOM};

    private ArrayList<Integer> columnWidths;
    private ArrayList<Integer> rowHeights;

    public LayoutMatrix()
    {
        location = new Point(0, 0);
        horizontalSpacing = 5;
        verticalSpacing = 5;
        horizontalAllignment = HorizontalAllignment.CENTER;
        verticalAllignment = VerticalAllignment.CENTER;
        matrix = new ArrayList<ArrayList<LayoutElement>>();
        insets = new LayoutInsets(0, 0, 0, 0);
        columnWidths = new ArrayList<Integer>();
        rowHeights = new ArrayList<Integer>();
    }

    public int getHorizontalSpacing()
    {
        return horizontalSpacing;
    }

    public void setHorizontalSpacing(int horizontalSpacing)
    {
        this.horizontalSpacing = horizontalSpacing;
    }

    public int getVerticalSpacing()
    {
        return verticalSpacing;
    }

    public void setVerticalSpacing(int verticalSpacing)
    {
        this.verticalSpacing = verticalSpacing;
    }
    
    public void clear()
    {
        matrix.clear();
    }
    
    public int getRowCount()
    {
        return matrix.size();
    }
    
    public int getColumnCount()
    {
        if (matrix.size() > 0)
        {
            return matrix.get(0).size(); // All rows have the same amount of columns, so the size of the first row is a good enough check.
        }
        else return 0;
    }
    
    public void setInsets(LayoutInsets insets)
    {
        this.insets.setInsets(insets);
    }
    
    public void setInsets(int topInset, int rightInset, int bottomInset, int leftInset)
    {
        insets.setTopInset(topInset);
        insets.setRightInset(rightInset);
        insets.setBottomInset(bottomInset);
        insets.setLeftInset(leftInset);
    }
    
    @Override
    public LayoutInsets getInsets()
    {
        return insets;
    }
    
    @Override
    public void doLayout(int x, int y)
    {
        int cellY;

        // Calculate column and row widths.
        columnWidths.clear();
        rowHeights.clear();

        // Get all column widths;
        for (int columnIndex = 0; columnIndex < getColumnCount(); columnIndex++)
        {
            columnWidths.add(calculateColumnWidth(columnIndex));
        }
        
        // Get all row heights.
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            rowHeights.add(calculateRowHeight(rowIndex));
        }

        // Set the location of this matrix.
        location.setLocation(x, y);
        
        // Set the starting y location for the first row.
        cellY = y + insets.getTopInset();
        
        // Set the location of each element in the matrix.
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            int cellHeight;
            int cellX;

            // Set the starting X location of the next row.
            cellX = x + insets.getLeftInset();
            
            // Get the current cell height;
            cellHeight = rowHeights.get(rowIndex);
            for (int columnIndex = 0; columnIndex < getColumnCount(); columnIndex++)
            {
                LayoutElement element;
                int cellWidth;

                // Get the current cell width;
                cellWidth = columnWidths.get(columnIndex);
                
                // Get the cell element and its bounds.
                element = getElement(rowIndex, columnIndex);
                if (element != null)
                {
                    Dimension elementSize;
                    int elementX;
                    int elementY;
                    
                    elementSize = element.getSize();

                    // Set the location of the element within the cell.
                    elementX = cellX + Math.round(cellWidth/2.0f) - Math.round(elementSize.width/2.0f);
                    elementY = cellY + Math.round(cellHeight/2.0f) - Math.round(elementSize.height/2.0f);
                    element.doLayout(elementX, elementY);
                }
                
                // Translate cellX.
                cellX += cellWidth;
                cellX += horizontalSpacing;
            }
            
            // Translate cellY.
            cellY += cellHeight;
            cellY += verticalSpacing;
        }
    }
    
    @Override
    public Dimension getSize()
    {
        int width;
        int height;
        int rowCount;
        int columnCount;
        
        width = 0;
        height = 0;
        rowCount = getRowCount();
        columnCount = getColumnCount();
        
        // Add the height of all rows.
        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
        {
            height += calculateRowHeight(rowIndex);
        }
        
        // Add the width of all columns.
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
        {
            width += calculateColumnWidth(columnIndex);
        }
        
        // Add spacing.
        if (columnCount > 1) width += ((columnCount - 1) * horizontalSpacing);
        if (rowCount > 1) height += ((rowCount - 1) * verticalSpacing);
        
        // Add insets.
        width += insets.getRightInset() + insets.getLeftInset();
        height += insets.getTopInset() + insets.getBottomInset();
        
        return new Dimension(width, height);
    }
    
    private int calculateRowHeight(int rowIndex)
    {
        ArrayList<LayoutElement> row;
        int height;
        
        height = 0;
        row = matrix.get(rowIndex);
        for (LayoutElement element : row)
        {
            if (element != null)
            {
                Dimension elementSize;

                elementSize = element.getSize();
                if (elementSize.height > height) height = elementSize.height;
            }
        }
        
        return height;
    }

    public Rectangle getRowBounds(int rowIndex)
    {
        int rowHeight;
        int rowWidth;
        int columnCount;

        rowWidth = 0;
        rowHeight = rowHeights.get(rowIndex);
        for (int columnWidth : columnWidths)
        {
            rowWidth += columnWidth;
        }

        columnCount = getColumnCount();
        if (columnCount > 1) rowWidth += ((columnCount-1) * horizontalSpacing);

        return new Rectangle(rowWidth, rowHeight);
    }
    
    private int calculateColumnWidth(int columnIndex)
    {
        int width;
        
        width = 0;
        for (ArrayList<LayoutElement> row : matrix)
        {
            LayoutElement element;
            
            element = row.get(columnIndex);
            if (element != null)
            {
                Dimension elementSize;

                elementSize = element.getSize();
                if (elementSize.width > width) width = elementSize.width;
            }
        }
        
        return width;
    }

    public Point getRowLocation(int rowIndex)
    {
        Point rowLocation;

        rowLocation = new Point(location);
        rowLocation.translate(insets.getLeftInset(), insets.getTopInset());
        for (int index = 0; index < rowIndex; index++)
        {
            rowLocation.translate(0, rowHeights.get(index));
        }

        if (rowIndex > 0) rowLocation.translate(0, (verticalSpacing * rowIndex));

        return rowLocation;
    }

    public Point getColumnLocation(int columnIndex)
    {
        Point columnLocation;

        columnLocation = new Point(location);
        for (int index = 0; index < columnIndex +1; index++)
        {
            columnLocation.translate(columnWidths.get(index), 0);
        }

        return columnLocation;
    }

    public void ensureRowNumber(int rowCount)
    {
        while (matrix.size() < rowCount)
        {
            matrix.add(new ArrayList<LayoutElement>());
        }
    }
    
    public void ensureColumnNumber(int columnCount)
    {
        for (ArrayList<LayoutElement> row : matrix)
        {
            row.ensureCapacity(columnCount);
            while (row.size() < columnCount)
            {
                row.add(null);
            }
        }
    }
    
    public void ensureSize(int rowCount, int columnCount)
    {
        ensureRowNumber(rowCount);
        ensureColumnNumber(columnCount);
    }
    
    public void addElement(LayoutElement element, int row, int column)
    {
        ensureSize(row+1, column+1);
        matrix.get(row).set(column, element);
    }
    
    public void insertRow(int rowIndex)
    {
        ArrayList<LayoutElement> row;
        int columnCount;
        
        columnCount = getColumnCount();
        row = new ArrayList<LayoutElement>();
        while (row.size() < columnCount) row.add(null);
        matrix.add(rowIndex, row);
    }
    
    public void insertColumn(int columnIndex)
    {
        for (ArrayList<LayoutElement> row : matrix)
        {
            row.add(columnIndex, null);
        }
    }
    
    public LayoutElement getElement(int row, int column)
    {
        return matrix.get(row).get(column);
    }
    
    public ArrayList<LayoutElement> getElements()
    {
        ArrayList<LayoutElement> elements;
        
        elements = new ArrayList<LayoutElement>();
        for (ArrayList<LayoutElement> row : matrix)
        {
            for (LayoutElement element : row)
            {
                if (element != null)
                {
                    elements.add(element);
                }
            }
        }
        
        return elements;
    }
}
