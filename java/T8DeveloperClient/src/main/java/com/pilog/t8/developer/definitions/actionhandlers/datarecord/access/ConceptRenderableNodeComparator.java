package com.pilog.t8.developer.definitions.actionhandlers.datarecord.access;

import com.pilog.t8.data.document.TerminologyProvider;
import java.util.Comparator;

/**
 * @author Gavin Boshoff
 */
public class ConceptRenderableNodeComparator implements Comparator<ConceptRenderableNode>
{
    private final TerminologyProvider terminologyProvider;

    public ConceptRenderableNodeComparator(TerminologyProvider terminologyProvider)
    {
        this.terminologyProvider = terminologyProvider;
    }

    @Override
    public int compare(ConceptRenderableNode o1, ConceptRenderableNode o2)
    {
        String o1Term;
        String o2Term;

        o1Term = this.terminologyProvider.getTerm(null, o1.getConceptID());
        o2Term = this.terminologyProvider.getTerm(null, o2.getConceptID());

        if(o1Term == null && o2Term == null) return 0;
        else if(o1Term == null && o2Term != null) return 1;
        else if(o1Term != null && o2Term == null) return -1;
        else return o1Term.compareTo(o2Term);
    }
}
