package com.pilog.t8.developer.definitions.projecteditor;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionGroupMetaData;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectEditorTreeTableModel extends DefaultTreeTableModel
{
    // Column Types.
    public static Class[] types = new Class[]
    {
        java.lang.String.class, java.lang.Long.class, Object.class, java.lang.String.class, java.lang.Boolean.class, java.lang.Long.class, java.lang.String.class, java.lang.Long.class, java.lang.String.class, java.lang.String.class
    };

    // Column Editability.
    public static boolean[] canEdit = new boolean[]
    {
        false, false, false, false, false, false, false, false, false, false
    };

    public static List<String> columnNames = ArrayLists.newArrayList
    (
        "Definition", "Version", "Status", "Type", "Patched", "Created Time", "Created User", "Updated Time", "Updated User", "Checksum"
    );

    private Comparator<T8ProjectEditorTreeTableNode> groupComparator;
    private Comparator<T8ProjectEditorTreeTableNode> typeComparator;
    private Comparator<T8ProjectEditorTreeTableNode> definitionComparator;
    private T8DefinitionMetaData projectMeta;

    public T8ProjectEditorTreeTableModel()
    {
        super(null, columnNames);
        this.groupComparator = new AlphabeticComparator();
        this.typeComparator = groupComparator;
        this.definitionComparator = groupComparator;
        setProject(null, null, null, null, false);
    }

    public T8ProjectEditorTreeTableNode setProject(Map<String, T8DefinitionGroupMetaData> groupMeta, Map<String, T8DefinitionTypeMetaData> typeMeta, T8DefinitionMetaData projectMeta, List<T8DefinitionMetaData> contentMeta, boolean createTemporaryNodes)
    {
        T8ProjectEditorTreeTableNode newRootNode;

        this.projectMeta = projectMeta;
        newRootNode = constructModel(groupMeta, typeMeta, projectMeta, contentMeta, createTemporaryNodes);
        setRoot(newRootNode);
        sort();
        return newRootNode;
    }

    public void setSortComparators(Comparator<T8ProjectEditorTreeTableNode> typeComparator, Comparator<T8ProjectEditorTreeTableNode> definitionComparator)
    {
        this.typeComparator = typeComparator;
        this.definitionComparator = definitionComparator;
    }

    public void sort()
    {
        T8ProjectEditorTreeTableNode rootNode;

        rootNode = getRootNode();
        if (rootNode != null)
        {
            // Sort the type nodes.
            rootNode.sortChildren(typeComparator);

            // Sort the definition nodes.
            for (T8ProjectEditorTreeTableNode groupNode : rootNode.getChildNodes())
            {
                groupNode.sortChildren(definitionComparator);
                for (T8ProjectEditorTreeTableNode typeNode : groupNode.getChildNodes())
                {
                    typeNode.sortChildren(definitionComparator);
                }
            }

            // Fire the event to indicate that the model has been updated.
            modelSupport.fireTreeStructureChanged(rootNode.getPath());
        }
    }

    private T8ProjectEditorTreeTableNode constructModel(Map<String, T8DefinitionGroupMetaData> groupMeta, Map<String, T8DefinitionTypeMetaData> typeMeta, T8DefinitionMetaData projectMeta, List<T8DefinitionMetaData> contentMeta, boolean createTemporaryNodes)
    {
        if (projectMeta != null)
        {
            Map<String, T8ProjectEditorTreeTableNode> groupNodes;
            Map<String, T8ProjectEditorTreeTableNode> typeNodes;
            T8ProjectEditorTreeTableNode rootNode;

            // Create the root node using the project definition meta.
            rootNode = new T8ProjectEditorTreeTableNode(groupMeta.get(T8ProjectDefinition.GROUP_IDENTIFIER), typeMeta.get(T8ProjectDefinition.TYPE_IDENTIFIER), projectMeta);

            // Build the tree structure, grouping nodes by group id, followed by type id.
            groupNodes = new HashMap<>();
            typeNodes = new HashMap<>();
            for (T8DefinitionMetaData definitionMetaData : contentMeta)
            {
                T8DefinitionGroupMetaData definitionGroupMeta;
                T8DefinitionTypeMetaData definitionTypeMeta;
                T8ProjectEditorTreeTableNode groupNode;
                T8ProjectEditorTreeTableNode typeNode;
                T8ProjectEditorTreeTableNode definitionNode;
                String groupId;
                String typeId;

                groupId = definitionMetaData.getGroupId();
                typeId = definitionMetaData.getTypeId();

                // Get the applicable group node to which the definition node will be added.
                definitionGroupMeta = groupMeta.get(groupId);
                groupNode = groupNodes.get(groupId);
                if (groupNode == null)
                {
                    groupNode = new T8ProjectEditorTreeTableNode(definitionGroupMeta, null, null);
                    groupNodes.put(groupId, groupNode);
                    rootNode.add(groupNode);
                }

                // Get the applicable type node to which the composition node will be added (create one if it doesn't exist).
                definitionTypeMeta = typeMeta.get(typeId);
                typeNode = typeNodes.get(typeId);
                if (typeNode == null)
                {
                    typeNode = new T8ProjectEditorTreeTableNode(definitionGroupMeta, definitionTypeMeta, null);
                    typeNodes.put(typeId, typeNode);
                    groupNode.add(typeNode);
                }

                // Create the new composition node and add it to the type node.
                definitionNode = new T8ProjectEditorTreeTableNode(definitionGroupMeta, definitionTypeMeta, definitionMetaData);
                typeNode.add(definitionNode);

                // If temporary nodes are required, add one.
                if (createTemporaryNodes) definitionNode.add(new T8ProjectEditorTreeTableNode("Loading..."));
            }

            return rootNode;
        }
        else return null;
    }

    public void addDefinition(T8DefinitionGroupMetaData groupMeta, T8DefinitionTypeMetaData typeMeta, T8DefinitionMetaData definitionMeta)
    {
        T8ProjectEditorTreeTableNode definitionNode;
        T8ProjectEditorTreeTableNode groupNode;
        T8ProjectEditorTreeTableNode typeNode;
        T8ProjectEditorTreeTableNode rootNode;
        String groupId;
        String typeId;

        groupId = definitionMeta.getGroupId();
        typeId = typeMeta.getTypeId();
        groupNode = findNode(groupId);
        typeNode = findNode(typeId);
        rootNode = getRootNode();

        if (groupNode == null)
        {
            groupNode = new T8ProjectEditorTreeTableNode(groupMeta, null, null);
            insertNodeInto(groupNode, rootNode, 0);
        }

        if (typeNode == null)
        {
            typeNode = new T8ProjectEditorTreeTableNode(groupMeta, typeMeta, null);
            insertNodeInto(typeNode, groupNode, 0);
        }

        // Create the new composition node and add it to the type node.
        definitionNode = new T8ProjectEditorTreeTableNode(groupMeta, typeMeta, definitionMeta);
        insertNodeInto(definitionNode, typeNode, 0);
    }

    public void removeDefinition(T8DefinitionHandle handle)
    {
        T8ProjectEditorTreeTableNode definitionNode;

        definitionNode = findNode(handle.getDefinitionIdentifier());
        if (definitionNode != null)
        {
            definitionNode.removeFromParent();
        }
    }

    public T8ProjectEditorTreeTableNode findNode(String id)
    {
        if (root != null)
        {
            for (T8ProjectEditorTreeTableNode descendantNode : ((T8ProjectEditorTreeTableNode)root).getFamilyNodes())
            {
                if (id.equals(descendantNode.getId()))
                {
                    return descendantNode;
                }
            }

            return null;
        }
        else return null;
    }

    public T8DefinitionMetaData getProjectMeta()
    {
        return projectMeta;
    }

    public T8ProjectEditorTreeTableNode getRootNode()
    {
        return (T8ProjectEditorTreeTableNode)super.getRoot();
    }

    @Override
    public Class getColumnClass(int columnIndex)
    {
        return types[columnIndex];
    }

    private class AlphabeticComparator implements Comparator
    {
        @Override
        public int compare(Object o1, Object o2)
        {
            T8ProjectEditorTreeTableNode node1;
            T8ProjectEditorTreeTableNode node2;

            // Get the two nodes to compare.
            node1 = (T8ProjectEditorTreeTableNode)o1;
            node2 = (T8ProjectEditorTreeTableNode)o2;

            if (node1.isDefinitionNode())
            {
                String globalPrefix;
                String id1;
                String id2;

                // Get the ids of the nodes.
                id1 = node1.getId();
                id2 = node2.getId();
                globalPrefix = T8Definition.getPublicIdPrefix();

                // Do a special check to ensure that '@' global definitions are sorted before private definitions.
                if (id1.startsWith(globalPrefix))
                {
                    if (id2.startsWith(globalPrefix))
                    {
                        // Both nodes public.
                        return node1.getId().compareTo(node2.getId());
                    }
                    else
                    {
                        // Node 1 is public but node 2 is not so immediately return comparison indicator.
                        return -1;
                    }
                }
                else if (id2.startsWith(globalPrefix))
                {
                    // Node 1 is private but node 2 is not so immediately return comparison indicator.
                    return 1;
                }
                else // Both ids are private.
                {
                    return node1.getId().compareTo(node2.getId());
                }
            }
            else if (node1.isTypeNode())
            {
                return node1.getDefinitionTypeMetaData().getDisplayName().compareTo(node2.getDefinitionTypeMetaData().getDisplayName());
            }
            else // Group node.
            {
                String displayName1;
                String displayName2;

                displayName1 = node1.getDefinitionGroupMetaData().getName();
                if (displayName1 == null) displayName1 = node1.getDefinitionGroupMetaData().getGroupId(); // Because not all legacy groups have display names.
                displayName2 = node2.getDefinitionGroupMetaData().getName();
                if (displayName2 == null) displayName2 = node2.getDefinitionGroupMetaData().getGroupId(); // Because not all legacy groups have display names.
                return displayName1.compareTo(displayName2);
            }
        }
    }
}
