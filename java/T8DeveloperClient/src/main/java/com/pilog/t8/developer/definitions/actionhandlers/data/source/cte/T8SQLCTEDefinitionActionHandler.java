package com.pilog.t8.developer.definitions.actionhandlers.data.source.cte;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.data.source.cte.sql.T8SQLCTEDefinition;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * @author Bouwer du Preez
 */
public class T8SQLCTEDefinitionActionHandler implements T8DefinitionActionHandler
{
    private T8ClientContext clientContext;
    private T8SessionContext sessionContext;
    private T8SQLCTEDefinition definition;

    public T8SQLCTEDefinitionActionHandler(T8ClientContext clientContext, T8SessionContext sessionContext, T8SQLCTEDefinition definition)
    {
        this.clientContext = clientContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new ConvertDataSourceGUID(clientContext, sessionContext, definition));

        return actions;
    }

    protected class ConvertDataSourceGUID extends AbstractAction implements ClipboardOwner
    {
        private T8ClientContext clientContext;
        private T8SessionContext sessionContext;
        private T8SQLCTEDefinition definition;

        public ConvertDataSourceGUID(final T8ClientContext clientContext, final T8SessionContext sessionContext, final T8SQLCTEDefinition definition)
        {
            this.clientContext = clientContext;
            this.sessionContext = sessionContext;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Convert GUID values to DB Adaptor");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/pencilRulerIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if(definition != null)
                {
                    this.definition.setSQLCTEtring(replaceGUIDWithAdaptor(definition.getSQLCTEString(null, 0, -1), definition));
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving field definitions for data source: " + definition.getIdentifier(), e);
            }
        }


        private String replaceGUIDWithAdaptor(String stringToCheck, T8SQLCTEDefinition dataSourceDefinition)
        {
            String replacementString = stringToCheck;
            Matcher matcher;
            //Case 1: Strings containing STUFF
            matcher = Pattern.compile("([STUF\\(]{24}+'?)[^',]+([',]?[,0-9\\-'\\) ]{51})").matcher(replacementString);
            while (matcher.find() && !matcher.hitEnd())
            {
                //Find the GUID Strings
                Matcher replaceMatcher;

                replaceMatcher = Pattern.compile("\\([^,\\(]+,").matcher(matcher.group());
                while (replaceMatcher.find() && !replaceMatcher.hitEnd())
                {
                    String guidStripped = replaceMatcher.group().replaceAll("[-'\\(,]", "");

                    Map<String, String> sqlParameterExpressionMap = dataSourceDefinition.getSQLParameterExpressionMap();
                    if(sqlParameterExpressionMap == null) sqlParameterExpressionMap = new HashMap<>();
                    if (!sqlParameterExpressionMap.containsKey(guidStripped))
                    {
                        boolean isGuid = guidStripped.matches("[A-z0-9]{32}");
                        sqlParameterExpressionMap.put(guidStripped, "dbAdaptor.getSQLColumnHexToGUID(\"" + (isGuid ? "'" : "") + guidStripped+ (isGuid ? "'" : "") + "\")");
                        dataSourceDefinition.setSQLParameterExpressionMap(sqlParameterExpressionMap);
                    }

                    replacementString = replacementString.replaceFirst(matcher.pattern().pattern(), " <<-" + guidStripped + "->> ");
                }
                matcher.reset(replacementString);
            }

            //Case 2: Strings containing MSSQL dbo guid function
            matcher = Pattern.compile("\\[?[DdBbOo]{3}\\]?\\.\\[?HexToGUID\\]?\\('*.+'*\\)").matcher(replacementString);
            while (matcher.find() && !matcher.hitEnd())
            {
                String guidStripped = matcher.group().replaceAll("\\[?[DdBbOo]{3}\\]?\\.\\[?HexToGUID\\]?\\(", "").replaceAll("[-']", "").replaceAll("\\)", "");

                Map<String, String> sqlParameterExpressionMap = dataSourceDefinition.getSQLParameterExpressionMap();
                    if(sqlParameterExpressionMap == null) sqlParameterExpressionMap = new HashMap<>();
                if (!sqlParameterExpressionMap.containsKey(guidStripped))
                {
                    boolean isGuid = guidStripped.matches("[A-z0-9]{32}");
                    sqlParameterExpressionMap.put(guidStripped, "dbAdaptor.getSQLColumnHexToGUID(\"" + (isGuid ? "'" : "") + guidStripped+ (isGuid ? "'" : "") + "\")");
                    dataSourceDefinition.setSQLParameterExpressionMap(sqlParameterExpressionMap);
                }

                replacementString = replacementString.replaceFirst(matcher.pattern().pattern(), " <<-" + guidStripped + "->> ");
                matcher.reset(replacementString);
            }

            //Case 3: Strings just containing an MSSQL type compatible GUID
            matcher = Pattern.compile("'{1}[A-z0-9]{8}-{1}[A-z0-9]{4}-{1}[A-z0-9]{4}-{1}[A-z0-9]{4}-{1}[A-z0-9]{12}'{1}").matcher(replacementString);
            while (matcher.find() && !matcher.hitEnd())
            {
                String guidStripped = matcher.group().replaceAll("[-']", "");

                Map<String, String> sqlParameterExpressionMap = dataSourceDefinition.getSQLParameterExpressionMap();
                    if(sqlParameterExpressionMap == null) sqlParameterExpressionMap = new HashMap<>();
                if (!sqlParameterExpressionMap.containsKey(guidStripped))
                {
                    sqlParameterExpressionMap.put(guidStripped, "dbAdaptor.getSQLColumnHexToGUID(\"'" + guidStripped + "'\")");
                    dataSourceDefinition.setSQLParameterExpressionMap(sqlParameterExpressionMap);
                }

                replacementString = replacementString.replaceFirst(matcher.pattern().pattern(), " <<-" + guidStripped + "->> ");
                matcher.reset(replacementString);
            }

            //Case 3: Strings containing Oracle type GUID
            matcher = Pattern.compile("'{1}[A-Fa-f0-9]{32}'{1}").matcher(replacementString);
            while (matcher.find() && !matcher.hitEnd())
            {
                String guidStripped = matcher.group().replaceAll("[-']", "");

                Map<String, String> sqlParameterExpressionMap = dataSourceDefinition.getSQLParameterExpressionMap();
                if(sqlParameterExpressionMap == null) sqlParameterExpressionMap = new HashMap<>();
                if (!sqlParameterExpressionMap.containsKey(guidStripped))
                {
                    sqlParameterExpressionMap.put(guidStripped, "dbAdaptor.getSQLColumnHexToGUID(\"'" + guidStripped + "'\")");
                    dataSourceDefinition.setSQLParameterExpressionMap(sqlParameterExpressionMap);
                }

                replacementString = replacementString.replaceFirst(matcher.pattern().pattern(), " <<-" + guidStripped + "->> ");
                matcher.reset(replacementString);
            }

            //Lastly make it look nice again by removing double spaces
            replacementString = replacementString.replaceAll(" +(<<-)", " <<-").replaceAll("(->>) +", "->> ");

            return replacementString;
        }

        @Override
        public void lostOwnership(Clipboard clipboard, Transferable contents)
        {
        }
    }
}
