package com.pilog.t8.developer.document.path;

import com.pilog.epic.ide.EpicScriptEditor;
import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.developer.definitions.actionhandlers.datarecord.access.ConceptRenderableNode;
import com.pilog.t8.developer.definitions.actionhandlers.datarecord.access.DataRecordAccessFieldNode;
import com.pilog.t8.developer.definitions.actionhandlers.datarecord.access.DataRecordAccessInstanceNode;
import com.pilog.t8.developer.definitions.actionhandlers.datarecord.access.DataRecordAccessNode;
import com.pilog.t8.developer.definitions.actionhandlers.datarecord.access.DataRecordAccessOntologyNode;
import com.pilog.t8.developer.definitions.actionhandlers.datarecord.access.DataRecordAccessPropertyNode;
import com.pilog.t8.developer.definitions.actionhandlers.datarecord.access.DataRecordAccessTreeNode;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingWorker;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;

/**
 * @author Hennie Brink
 */
public class T8DocPathGenerator extends JFrame
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DocPathGenerator.class);

    private final T8Context context;
    private final T8ClientContext clientContext;
    private final EpicScriptEditor scriptEditor;
    private TerminologyProvider terminologyProvider;
    private OntologyConceptRenderer conceptRenderer;
    private T8OntologyClientApi ontApi;
    private T8DataRequirementClientApi drqApi;
    private T8TerminologyClientApi trmApi;
    private T8DefaultComponentContainer container;

    private enum DocPathMode
    {
        DEFAULT("Display"),
        ACCESS("Access"),
        REQUIREMENT("Requirement");

        private final String display;

        private DocPathMode(String display)
        {
            this.display = display;
        }

        @Override
        public String toString()
        {
            return this.display;
        }
    };

    public T8DocPathGenerator(T8Context context, EpicScriptEditor scriptEditor)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.scriptEditor = scriptEditor;

        initComponents();

        initialize();
    }

    private void initialize()
    {
        trmApi = clientContext.getConfigurationManager().getAPI(context, T8TerminologyClientApi.API_IDENTIFIER);
        ontApi = clientContext.getConfigurationManager().getAPI(context, T8OntologyClientApi.API_IDENTIFIER);
        drqApi = clientContext.getConfigurationManager().getAPI(context, T8DataRequirementClientApi.API_IDENTIFIER);
        conceptRenderer = new OntologyConceptRenderer();
        terminologyProvider = trmApi.getTerminologyProvider();
        container = new T8DefaultComponentContainer(jSplitPane1);

        jComboBoxMode.setSelectedIndex(0);
        jComboBoxMode.addItemListener(new ModeChangeListener());

        jXTreeStructure.addTreeWillExpandListener(new OntologyTreeWillExpandListener());
        jXTreeStructure.addTreeSelectionListener(new OntologyTreeSelectionChangeListener());
        jXTreeStructure.setRowHeight(25);
        jXTreeStructure.setBorder(new T8ContentHeaderBorder("Ontology Selection"));
        jXTreeStructure.setEditable(false);
        jXTreeStructure.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, 0), "Switch DR Instance Mode");
        jXTreeStructure.getActionMap().put("Switch DR Instance Mode", new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                TreePath editingPath;

                editingPath = jXTreeStructure.getLeadSelectionPath();
                if (editingPath != null)
                {
                    if (editingPath.getLastPathComponent() instanceof DataRecordAccessInstanceNode)
                    {
                        ((DataRecordAccessInstanceNode) editingPath.getLastPathComponent()).switchDocPathMode();
                        updateDocPath();
                    }
                }
            }
        });

        jPanelMain.add(container, BorderLayout.CENTER);
        pack();

        new ModelLoader().execute();
    }

    private void updateDocPath()
    {
        jTextAreaResults.setText(null);
        Iterator<TreePath> iterator;

        iterator = Arrays.asList(jXTreeStructure.getSelectionPaths()).iterator();

        while (iterator.hasNext())
        {
            TreePath path;

            path = iterator.next();

            if (jCheckBoxShorten.isSelected())
            {
                DataRecordAccessNode parent;
                DataRecordAccessNode node;
                List<DataRecordAccessNode> nodes;

                node = (DataRecordAccessNode) path.getLastPathComponent();
                parent = node;
                nodes = new ArrayList<>();

                nodes.add(node);

                do
                {
                    parent = parent.getParent();
                    nodes.add(0, parent);
                }
                while (parent != null && !(parent instanceof DataRecordAccessInstanceNode));

                jTextAreaResults.append("/");

                for (DataRecordAccessNode node1 : nodes)
                {
                    appendNodeRepresentation(node1);
                }
            }
            else
            {
                for (Object nodeObject : path.getPath())
                {
                    appendNodeRepresentation((DataRecordAccessNode) nodeObject);
                }
            }

            if (iterator.hasNext())
            {
                jTextAreaResults.append("\n");
            }
        }
    }

    private void appendNodeRepresentation(DataRecordAccessNode node)
    {
        String docPathRepresentation;

        switch ((DocPathMode) jComboBoxMode.getSelectedItem())
        {
            case ACCESS:
                docPathRepresentation = node.getDocPathRepresentation("A");
                break;
            case REQUIREMENT:
                docPathRepresentation = node.getDocPathRepresentation("R");
                break;
            default:
                docPathRepresentation = node.getDocPathRepresentation("");
                break;
        }

        if (!Strings.isNullOrEmpty(docPathRepresentation))
        {
            jTextAreaResults.append("/");
        }

        jTextAreaResults.append(docPathRepresentation);
    }

    private class ModelLoader extends SwingWorker<DataRecordAccessTreeNode, Void>
    {
        @Override
        protected DataRecordAccessTreeNode doInBackground() throws Exception
        {
            DataRecordAccessTreeNode rootNode;

            container.setMessage("Loading Independent DR Instances");
            container.setLocked(true);

            rootNode = new DataRecordAccessTreeNode(true, terminologyProvider);
            rootNode.loadChildren(context, ontApi, drqApi);

            return rootNode;
        }

        @Override
        protected void done()
        {
            try
            {
                DefaultTreeModel treeModel;
                treeModel = new DefaultTreeModel(get());

                jXTreeStructure.setModel(treeModel);
                jXTreeStructure.setCellRenderer(conceptRenderer);
                jXTreeStructure.expandPath(new TreePath(get().getChildAt(0)));
            }
            catch (InterruptedException | ExecutionException ex)
            {
                LOGGER.log("Failed to load tree model", ex);
            }

            container.setLocked(false);
        }
    }

    private class OntologyConceptRenderer extends DefaultTreeCellRenderer
    {
        private final Icon conceptIcon;
        private final Icon instanceIcon;
        private final Icon propertyIcon;
        private final Icon fieldIcon;

        private OntologyConceptRenderer()
        {
            this.conceptIcon = new ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/block.png"));
            this.instanceIcon = new ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/molecule.png"));
            this.propertyIcon = new ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/polyLineIcon.png"));
            this.fieldIcon = new ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/detailTagIcon.png"));
        }

        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
        {
            ConceptRenderableNode node;
            String nodeConcept;
            String term;

            node = (ConceptRenderableNode) value;
            nodeConcept = node.getConceptID();

            if (nodeConcept != null)
            {
                term = terminologyProvider.getTerm(null, nodeConcept);
            }
            else
            {
                term = "Independent Data Requirements";
            }

            if (node instanceof DataRecordAccessInstanceNode)
            {
                setClosedIcon(instanceIcon);
                setOpenIcon(instanceIcon);
                setLeafIcon(instanceIcon);
            }
            else if (node instanceof DataRecordAccessOntologyNode)
            {
                setClosedIcon(conceptIcon);
                setOpenIcon(conceptIcon);
                setLeafIcon(conceptIcon);
            }
            else if (node instanceof DataRecordAccessPropertyNode)
            {
                setClosedIcon(propertyIcon);
                setOpenIcon(propertyIcon);
                setLeafIcon(propertyIcon);
            }
            else if (node instanceof DataRecordAccessFieldNode)
            {
                setClosedIcon(fieldIcon);
                setOpenIcon(fieldIcon);
                setLeafIcon(fieldIcon);
            }
            else
            {
                setClosedIcon(null);
                setOpenIcon(null);
                setLeafIcon(null);
            }

            return super.getTreeCellRendererComponent(tree, term, sel, expanded, leaf, row, hasFocus);
        }
    }

    private class OntologyTreeWillExpandListener implements TreeWillExpandListener
    {

        @Override
        public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException
        {
            TreePath path;
            DataRecordAccessNode node;

            path = event.getPath();
            node = (DataRecordAccessNode) path.getLastPathComponent();
            node.loadChildren(context, ontApi, drqApi);
        }

        @Override
        public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException
        {
        }
    }

    private class OntologyTreeSelectionChangeListener implements TreeSelectionListener
    {

        @Override
        public void valueChanged(TreeSelectionEvent e)
        {
            updateDocPath();
        }
    }

    private class ModeChangeListener implements ItemListener
    {

        @Override
        public void itemStateChanged(ItemEvent e)
        {
            updateDocPath();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        jXTreeStructure = new org.jdesktop.swingx.JXTree();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaResults = new javax.swing.JTextArea();
        jPanelButtons = new javax.swing.JPanel();
        jButtonInsert = new javax.swing.JButton();
        jButtonClose = new javax.swing.JButton();
        jPanelOptions = new javax.swing.JPanel();
        jComboBoxMode = new javax.swing.JComboBox<>();
        jCheckBoxShorten = new javax.swing.JCheckBox();
        jLabelInstructions = new javax.swing.JLabel();
        jPanelMain = new javax.swing.JPanel();

        jSplitPane1.setDividerLocation(400);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("JTree");
        jXTreeStructure.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        jScrollPane3.setViewportView(jXTreeStructure);

        jSplitPane1.setTopComponent(jScrollPane3);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jTextAreaResults.setEditable(false);
        jTextAreaResults.setColumns(20);
        jTextAreaResults.setLineWrap(true);
        jTextAreaResults.setRows(5);
        jTextAreaResults.setWrapStyleWord(true);
        jScrollPane1.setViewportView(jTextAreaResults);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jScrollPane1, gridBagConstraints);

        jButtonInsert.setText("Insert");
        jButtonInsert.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonInsertActionPerformed(evt);
            }
        });
        jPanelButtons.add(jButtonInsert);

        jButtonClose.setText("Close");
        jButtonClose.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCloseActionPerformed(evt);
            }
        });
        jPanelButtons.add(jButtonClose);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jPanelButtons, gridBagConstraints);

        jPanelOptions.setLayout(new java.awt.GridBagLayout());

        jComboBoxMode.setModel(new DefaultComboBoxModel<>(DocPathMode.values()));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelOptions.add(jComboBoxMode, gridBagConstraints);

        jCheckBoxShorten.setActionCommand("Use Shortened Path");
        jCheckBoxShorten.setLabel("Use Shortened Path");
        jCheckBoxShorten.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jCheckBoxShortenActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        jPanelOptions.add(jCheckBoxShorten, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jPanelOptions, gridBagConstraints);

        jLabelInstructions.setText("Use the key \"s\" to switch between dr instance modes");
        jLabelInstructions.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 2);
        jPanel1.add(jLabelInstructions, gridBagConstraints);

        jSplitPane1.setBottomComponent(jPanel1);

        setPreferredSize(new java.awt.Dimension(700, 600));

        jPanelMain.setLayout(new java.awt.BorderLayout());
        getContentPane().add(jPanelMain, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonInsertActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonInsertActionPerformed
    {//GEN-HEADEREND:event_jButtonInsertActionPerformed
        scriptEditor.insertSourceCode(jTextAreaResults.getText());
    }//GEN-LAST:event_jButtonInsertActionPerformed

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseActionPerformed
        setVisible(false);
    }//GEN-LAST:event_jButtonCloseActionPerformed

    private void jCheckBoxShortenActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jCheckBoxShortenActionPerformed
    {//GEN-HEADEREND:event_jCheckBoxShortenActionPerformed
        updateDocPath();
    }//GEN-LAST:event_jCheckBoxShortenActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JButton jButtonInsert;
    private javax.swing.JCheckBox jCheckBoxShorten;
    private javax.swing.JComboBox<DocPathMode> jComboBoxMode;
    private javax.swing.JLabel jLabelInstructions;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelButtons;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JPanel jPanelOptions;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextArea jTextAreaResults;
    private org.jdesktop.swingx.JXTree jXTreeStructure;
    // End of variables declaration//GEN-END:variables
}
