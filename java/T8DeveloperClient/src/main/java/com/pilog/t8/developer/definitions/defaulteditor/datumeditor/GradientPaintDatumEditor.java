package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.gfx.paint.T8LinearGradientPaintDefinition;
import com.pilog.t8.definition.gfx.paint.T8PaintDefinition;
import com.pilog.t8.definition.gfx.paint.T8RadialGradientPaintDefinition;
import com.pilog.t8.developer.painter.gradient.T8GradientChooser;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;
import java.awt.RadialGradientPaint;

/**
 * @author Bouwer du Preez
 */
public class GradientPaintDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private final T8GradientChooser gradientChooser;
    
    public GradientPaintDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        gradientChooser = new T8GradientChooser();
        add(gradientChooser, java.awt.BorderLayout.CENTER);
    }
    
    @Override
    public void initializeComponent()
    {
    }
    
    @Override
    public void startComponent()
    {
        refreshEditor();
    }
    
    @Override
    public void stopComponent()
    {
    }
    
    @Override
    public void commitChanges()
    {
        MultipleGradientPaint paint;
        
        paint = gradientChooser.getGradient();
        if (paint instanceof LinearGradientPaint)
        {
            T8LinearGradientPaintDefinition paintDefinition;
            
            paintDefinition = new T8LinearGradientPaintDefinition(definition.getIdentifier() + "_LINEAR_GRADIENT_PAINT");
            paintDefinition.setPaint((LinearGradientPaint)paint);
            setDefinitionDatum(paintDefinition);
        }
        else
        {
            T8RadialGradientPaintDefinition paintDefinition;
            
            paintDefinition = new T8RadialGradientPaintDefinition(definition.getIdentifier() + "_RADIAL_GRADIENT_PAINT");
            paintDefinition.setPaint((RadialGradientPaint)paint);
            setDefinitionDatum(paintDefinition);
        }
    }
    
    @Override
    public void refreshEditor()
    {
        Object value;
        
        value = getDefinitionDatum();
        if (value != null)
        {
            gradientChooser.setGradient((MultipleGradientPaint)((T8PaintDefinition)value).getNewPaintInstance());
        }
    }
    
    @Override
    public void setEditable(boolean editable)
    {
        gradientChooser.setEnabled(editable);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
