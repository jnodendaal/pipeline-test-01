package com.pilog.t8.developer.definitions.dialog;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import javax.swing.JOptionPane;

/**
 * @author Bouwer du Preez
 */
public class T8IdentifierSwitchDialog extends javax.swing.JDialog
{
    private final T8Definition targetDefinition;
    private final T8ClientContext clientContext;
    private final T8DefinitionManager definitionManager;
    private final String projectId;
    private boolean proceed;
    private Mode mode;

    private enum Mode {LOCAL, PUBLIC};

    public T8IdentifierSwitchDialog(T8ClientContext clientContext, T8Definition targetDefinition)
    {
        super(clientContext.getParentWindow(), ModalityType.APPLICATION_MODAL);
        this.clientContext = clientContext;
        this.targetDefinition = targetDefinition;
        this.projectId = targetDefinition.getRootProjectId();
        this.definitionManager = clientContext.getDefinitionManager();
        initComponents();
        setSize(600, 220);
        setLocationRelativeTo(null);
        jRadioButtonLocal.setSelected(true);
    }

    public Pair<String, String> getSwitchIds()
    {
        if (proceed)
        {
            String fromId;
            String toId;

            fromId = (String)jComboBoxFromId.getSelectedItem();
            toId = (String)jComboBoxToId.getSelectedItem();
            return new Pair<String, String>(fromId, toId);
        }
        else return null;
    }

    public static final Pair<String, String> getSwitchIds(T8ClientContext clientContext, T8Definition targetDefinition)
    {
        T8IdentifierSwitchDialog dialog;

        dialog = new T8IdentifierSwitchDialog(clientContext, targetDefinition);
        dialog.setVisible(true);
        return dialog.getSwitchIds();
    }

    private void setLocalMode()
    {
        TreeSet<String> idSet;

        mode = Mode.LOCAL;
        jComboBoxFromId.removeAllItems();
        jComboBoxToId.removeAllItems();

        idSet = new TreeSet<>(targetDefinition.getReferenceIds(true, false, false, true));
        for (String localIdentifier : idSet)
        {
            jComboBoxFromId.addItem(localIdentifier);
        }
    }

    private void setPublicMode()
    {
        TreeSet<String> idSet;

        mode = Mode.PUBLIC;
        jComboBoxFromId.removeAllItems();
        jComboBoxToId.removeAllItems();

        idSet = new TreeSet<>(targetDefinition.getReferenceIds(false, false, true, true));
        for (String globalIdentifier : idSet)
        {
            jComboBoxFromId.addItem(globalIdentifier);
        }
    }

    private void refreshToComboBox()
    {
        String fromId;

        jComboBoxToId.removeAllItems();
        fromId = (String)jComboBoxFromId.getSelectedItem();
        if (fromId != null)
        {
            switch (mode)
            {
                case LOCAL:
                    List<String> localIds;

                    localIds = new ArrayList();
                    for (T8Definition localDefinition : targetDefinition.getLocalDefinitions())
                    {
                        localIds.add(localDefinition.getIdentifier());
                    }

                    Collections.sort(localIds);
                    for (String identifier : localIds)
                    {
                        jComboBoxToId.addItem(identifier);
                    }

                    break;
                case PUBLIC:
                    try
                    {
                        T8DefinitionMetaData fromIdMetaData;

                        fromIdMetaData = definitionManager.getDefinitionMetaData(null, fromId);
                        if (fromIdMetaData != null)
                        {
                            List<String> publicIds;

                            publicIds = definitionManager.getGroupDefinitionIdentifiers(projectId, fromIdMetaData.getGroupId());
                            Collections.sort(publicIds);
                            for (String publicId : publicIds)
                            {
                                jComboBoxToId.addItem(publicId);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        T8Log.log("Exception while fetching meta data for definition id: " + fromId, e);
                    }

                    break;
                default:
                    throw new RuntimeException("Invalid mode: " + mode);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupMode = new javax.swing.ButtonGroup();
        jPanelContent = new javax.swing.JPanel();
        jLabelInstruction = new javax.swing.JLabel();
        jPanelMode = new javax.swing.JPanel();
        jRadioButtonLocal = new javax.swing.JRadioButton();
        jRadioButtonPublic = new javax.swing.JRadioButton();
        jComboBoxFromId = new javax.swing.JComboBox();
        jComboBoxToId = new javax.swing.JComboBox<>();
        jLabelFrom = new javax.swing.JLabel();
        jLabelTo = new javax.swing.JLabel();
        jButtonProceed = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelContent.setLayout(new java.awt.GridBagLayout());

        jLabelInstruction.setText("Please select the reference from which to switch and the new identifier to switch to.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelContent.add(jLabelInstruction, gridBagConstraints);

        jPanelMode.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        buttonGroupMode.add(jRadioButtonLocal);
        jRadioButtonLocal.setText("Local References");
        jRadioButtonLocal.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jRadioButtonLocalItemStateChanged(evt);
            }
        });
        jPanelMode.add(jRadioButtonLocal);

        buttonGroupMode.add(jRadioButtonPublic);
        jRadioButtonPublic.setText("Public & Private References");
        jRadioButtonPublic.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jRadioButtonPublicItemStateChanged(evt);
            }
        });
        jPanelMode.add(jRadioButtonPublic);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        jPanelContent.add(jPanelMode, gridBagConstraints);

        jComboBoxFromId.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboBoxFromIdItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelContent.add(jComboBoxFromId, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelContent.add(jComboBoxToId, gridBagConstraints);

        jLabelFrom.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelFrom.setText("From:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanelContent.add(jLabelFrom, gridBagConstraints);

        jLabelTo.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelTo.setText("To:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanelContent.add(jLabelTo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        getContentPane().add(jPanelContent, gridBagConstraints);

        jButtonProceed.setText("Proceed");
        jButtonProceed.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonProceedActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        getContentPane().add(jButtonProceed, gridBagConstraints);

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(jButtonCancel, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCancelActionPerformed
    {//GEN-HEADEREND:event_jButtonCancelActionPerformed
        proceed = false;
        dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void jButtonProceedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonProceedActionPerformed
    {//GEN-HEADEREND:event_jButtonProceedActionPerformed
        String fromId;
        String toId;

        fromId = (String)jComboBoxFromId.getSelectedItem();
        toId = (String)jComboBoxToId.getSelectedItem();
        if (Strings.isNullOrEmpty(fromId))
        {
            proceed = false;
            JOptionPane.showMessageDialog(this, "No 'From' ID selected.", "Invalid Selection", JOptionPane.ERROR_MESSAGE);
        }
        else if (Strings.isNullOrEmpty(toId))
        {
            proceed = false;
            JOptionPane.showMessageDialog(this, "No 'To' ID selected.", "Invalid Selection", JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            proceed = true;
            dispose();
        }
    }//GEN-LAST:event_jButtonProceedActionPerformed

    private void jRadioButtonLocalItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jRadioButtonLocalItemStateChanged
    {//GEN-HEADEREND:event_jRadioButtonLocalItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            setLocalMode();
        }
    }//GEN-LAST:event_jRadioButtonLocalItemStateChanged

    private void jRadioButtonPublicItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jRadioButtonPublicItemStateChanged
    {//GEN-HEADEREND:event_jRadioButtonPublicItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            setPublicMode();
        }
    }//GEN-LAST:event_jRadioButtonPublicItemStateChanged

    private void jComboBoxFromIdItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboBoxFromIdItemStateChanged
    {//GEN-HEADEREND:event_jComboBoxFromIdItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            refreshToComboBox();
        }
    }//GEN-LAST:event_jComboBoxFromIdItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupMode;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonProceed;
    private javax.swing.JComboBox jComboBoxFromId;
    private javax.swing.JComboBox<String> jComboBoxToId;
    private javax.swing.JLabel jLabelFrom;
    private javax.swing.JLabel jLabelInstruction;
    private javax.swing.JLabel jLabelTo;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelMode;
    private javax.swing.JRadioButton jRadioButtonLocal;
    private javax.swing.JRadioButton jRadioButtonPublic;
    // End of variables declaration//GEN-END:variables
}
