package com.pilog.t8.developer.file.context.browser;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingWorker;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

/**
 * @author Hennie Brink
 */
public class T8FileContextBrowser extends javax.swing.JPanel implements T8DeveloperView
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8FileManager fileManager;
    private T8DeveloperViewFactory developerViewFactory;
    private T8FileContextDefinition selectedFileContextDefinition;
    private DefaultTreeModel treeModel;
    private String contextInstanceId;
    private T8FileDetails selectedFileDetails;

    public T8FileContextBrowser(T8Context context)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.fileManager = clientContext.getFileManager();
        initComponents();
        initialize();
    }

    private void initialize()
    {
        List<T8FileContextDefinition> definitions;
        DefaultComboBoxModel<T8FileContextDefinition> comboBoxModel;

        try
        {
            definitions = clientContext.getDefinitionManager().<T8FileContextDefinition>getRawGroupDefinitions(context, null, T8FileContextDefinition.GROUP_IDENTIFIER);
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to retrieve file context definitions", ex);
            return;
        }

        comboBoxModel = new DefaultComboBoxModel<T8FileContextDefinition>();
        for (T8FileContextDefinition t8FileContextDefinition : definitions)
        {
            comboBoxModel.addElement(t8FileContextDefinition);
        }

        jComboBoxFileContext.setModel(comboBoxModel);
        jComboBoxFileContext.addItemListener(new FileContextChangeListener());
        jComboBoxFileContext.setSelectedIndex(-1);

        treeModel = null;
        jTreeFileBrowser.setModel(null);

        jTreeFileBrowser.addTreeExpansionListener(new FileContextTreeExpansionListener());
        jTreeFileBrowser.addTreeSelectionListener(new FileContextPathSelectionListener());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    private void setSelectedFileContext(T8FileContextDefinition fileContextDefinition)
    {
        selectedFileContextDefinition = fileContextDefinition;

        openNewFileContext();

        treeModel = null;
        jTreeFileBrowser.setModel(null);

        loadNodes(new FileContextPathNode());
    }

    private void openNewFileContext()
    {
        try
        {
            if (contextInstanceId != null)
            {
                fileManager.closeFileContext(context, contextInstanceId);
            }
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to close previous file context", ex);
            Toast.show("Failed to close previous file context", Toast.Style.ERROR);
        }
        try
        {
            contextInstanceId = T8IdentifierUtilities.createNewGUID();
            fileManager.openFileContext(context, contextInstanceId, selectedFileContextDefinition.getIdentifier(), new T8Minute(5));
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to open file context", ex);
            Toast.show("Failed to open file context", Toast.Style.ERROR);
        }
    }

    private void loadNodes(FileContextPathNode node)
    {
        NodeLoader nodeLoader;

        nodeLoader = new NodeLoader(node);
        nodeLoader.execute();
    }

    private void downloadSelectedFile()
    {
        try
        {
            final String saveFilePath = BasicFileChooser.getSaveFilePath(this, selectedFileDetails.getFileName().substring(selectedFileDetails.getFileName().lastIndexOf(".")), "", new File(selectedFileDetails.getFileName()));

            new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        fileManager.downloadFile(context, contextInstanceId, saveFilePath, selectedFileDetails.getFilePath());
                    }
                    catch (Exception ex)
                    {
                        T8Log.log("Failed to save file to specified loction", ex);
                        Toast.show("Failed to save file to specified location", Toast.Style.ERROR);
                    }
                    Toast.show("File Downloaded succesfully", Toast.Style.SUCCESS);
                }
            }.start();
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to save file to specified loction", ex);
            Toast.show("Failed to save file to specified location", Toast.Style.ERROR);
        }
    }

    private void viewFileContent()
    {
        try
        {
            InputStream fileInputStream;

            fileInputStream = fileManager.getFileInputStream(context, contextInstanceId, selectedFileDetails.getFilePath());

            jEditorPaneFileContent.read(fileInputStream, null);

            fileInputStream.close();
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to retrieve file data", ex);
            Toast.show("Failed to retieve file data", Toast.Style.ERROR);
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Developer View Interface">

    @Override
    public String getHeader()
    {
        return "File Context Browser";
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return developerViewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.developerViewFactory = developerViewFactory;
    }

    @Override
    public void addView(T8DeveloperView view)
    {
    }

    @Override
    public void addView(T8DeveloperView view, String viewHeader)
    {
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
    }

    @Override
    public void definitionLinkActivated(String identifier)
    {
    }

    @Override
    public void setSelectedDefinition(T8Definition definition)
    {
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    @Override
    public void addDefinitionViewListener(T8DefinitionViewListener listener)
    {
    }

    @Override
    public void removeDefinitionViewListener(T8DefinitionViewListener listener)
    {
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

//</editor-fold>

    private class NodeLoader extends SwingWorker<Void, Void>
    {
        private final FileContextPathNode parentNode;
        private final T8FileDetails nodeFileDetails;

        public NodeLoader(FileContextPathNode parentNode)
        {
            this.parentNode = parentNode;
            this.nodeFileDetails = (T8FileDetails) parentNode.getUserObject();
        }

        @Override
        protected Void doInBackground() throws Exception
        {
            List<T8FileDetails> fileDetails;

            if(!fileManager.fileContextExists(context, contextInstanceId)) openNewFileContext();

            fileDetails = clientContext.getFileManager().getFileList(context, contextInstanceId, nodeFileDetails == null ? null : nodeFileDetails.getFilePath());

            for (T8FileDetails t8FileDetails : fileDetails)
            {
                parentNode.add(new FileContextPathNode(t8FileDetails));
            }
            parentNode.setChildrenLoaded(true);

            return null;
        }

        @Override
        protected void done()
        {
            try
            {
                if(treeModel == null)
                {
                    treeModel = new DefaultTreeModel(parentNode);
                    jTreeFileBrowser.setModel(treeModel);
                }
                else
                {
                    // Refresh the parent node's descendents.
                    treeModel.nodeStructureChanged(parentNode);

                    // Expand the node, showing its newly created children.
                    jTreeFileBrowser.expandPath(new TreePath(parentNode.getPath()));

                    treeModel.nodeChanged(parentNode);
                }
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to load node children", ex);
                Toast.show("Failed to load children", Toast.Style.ERROR);
            }
        }
    }

    private static class FileContextPathNode extends DefaultMutableTreeNode
    {
        private boolean childrenLoaded = false;
        private T8FileDetails fileDetails;

        FileContextPathNode()
        {
        }

        FileContextPathNode(T8FileDetails userObject)
        {
            super(userObject);
            this.fileDetails = userObject;
        }

        FileContextPathNode(T8FileDetails userObject, boolean allowsChildren)
        {
            super(userObject, allowsChildren);
            this.fileDetails = userObject;
        }

        public boolean isChildrenLoaded()
        {
            return childrenLoaded;
        }

        public void setChildrenLoaded(boolean childrenLoaded)
        {
            this.childrenLoaded = childrenLoaded;
        }

        @Override
        public boolean getAllowsChildren()
        {
            return fileDetails == null ? true : fileDetails.isDirectory();
        }

        @Override
        public boolean isLeaf()
        {
            return fileDetails == null ? false : !fileDetails.isDirectory();
        }

        @Override
        public String toString()
        {
            if(fileDetails == null) return "";

            String displayValue;

            displayValue = fileDetails.getFileName();
            if(!fileDetails.isDirectory())
            {
                displayValue += " (" + (fileDetails.getFileSize()/1024) + " kB)";
            }
            return displayValue;
        }
    }

    private class FileContextPathSelectionListener implements TreeSelectionListener
    {
        @Override
        public void valueChanged(TreeSelectionEvent e)
        {
            TreePath path = e.getPath();
            FileContextPathNode selectedNode = (FileContextPathNode) path.getLastPathComponent();
            selectedFileDetails = (T8FileDetails) selectedNode.getUserObject();

            jButtonDownload.setEnabled(selectedFileDetails != null && !selectedFileDetails.isDirectory());
            jButtonView.setEnabled(selectedFileDetails != null && !selectedFileDetails.isDirectory());

            if(selectedFileDetails != null)
            {
                jTextFieldFileName.setText(selectedFileDetails.getFileName());
                jTextFieldFilePath.setText(selectedFileDetails.getFilePath());
                jTextFieldFileChecksum.setText(selectedFileDetails.getMD5Checksum());
                jLabelFileSizeDisplay.setText(Long.toString(selectedFileDetails.getFileSize()));
                jCheckBoxIsDir.setSelected(selectedFileDetails.isDirectory());
            }
            else
            {
                jTextFieldFileName.setText(null);
                jTextFieldFilePath.setText(null);
                jTextFieldFileChecksum.setText(null);
                jLabelFileSizeDisplay.setText(null);
                jCheckBoxIsDir.setSelected(false);
            }
        }
    }

    private class FileContextTreeExpansionListener implements TreeExpansionListener
    {

        @Override
        public void treeExpanded(TreeExpansionEvent event)
        {
            TreePath expandPath;
            FileContextPathNode expandNode;

            expandPath = event.getPath();
            expandNode = (FileContextPathNode)expandPath.getLastPathComponent();
            if ((expandNode != null) && (!expandNode.isChildrenLoaded()))
            {
                loadNodes(expandNode);
            }
        }

        @Override
        public void treeCollapsed(TreeExpansionEvent event)
        {
        }
    }

    private class FileContextChangeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if(e.getStateChange() == ItemEvent.SELECTED)
            {
                setSelectedFileContext((T8FileContextDefinition)e.getItem());
            }
        }
    }
    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jComboBoxFileContext = new javax.swing.JComboBox();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTreeFileBrowser = new javax.swing.JTree();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPaneFileContent = new javax.swing.JEditorPane();
        jPanel2 = new javax.swing.JPanel();
        jLabelFileName = new javax.swing.JLabel();
        jLabelFilePath = new javax.swing.JLabel();
        jLabelFileSize = new javax.swing.JLabel();
        jLabelFileChecksum = new javax.swing.JLabel();
        jLabelIsDir = new javax.swing.JLabel();
        jTextFieldFileName = new javax.swing.JTextField();
        jTextFieldFilePath = new javax.swing.JTextField();
        jTextFieldFileChecksum = new javax.swing.JTextField();
        jLabelFileSizeDisplay = new javax.swing.JLabel();
        jCheckBoxIsDir = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jButtonDownload = new javax.swing.JButton();
        jButtonView = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(jComboBoxFileContext, gridBagConstraints);

        jSplitPane1.setDividerLocation(300);
        jSplitPane1.setToolTipText("");

        jScrollPane2.setViewportView(jTreeFileBrowser);

        jSplitPane1.setLeftComponent(jScrollPane2);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jEditorPaneFileContent.setEditable(false);
        jScrollPane1.setViewportView(jEditorPaneFileContent);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jScrollPane1, gridBagConstraints);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("File Details"));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabelFileName.setText("File Name:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabelFileName, gridBagConstraints);

        jLabelFilePath.setText("File Path:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabelFilePath, gridBagConstraints);

        jLabelFileSize.setText("File Size:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabelFileSize, gridBagConstraints);

        jLabelFileChecksum.setText("File MD5 Checksum:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabelFileChecksum, gridBagConstraints);

        jLabelIsDir.setText("Is Directory:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabelIsDir, gridBagConstraints);

        jTextFieldFileName.setEditable(false);
        jTextFieldFileName.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jTextFieldFileName, gridBagConstraints);

        jTextFieldFilePath.setEditable(false);
        jTextFieldFilePath.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jTextFieldFilePath, gridBagConstraints);

        jTextFieldFileChecksum.setEditable(false);
        jTextFieldFileChecksum.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jTextFieldFileChecksum, gridBagConstraints);

        jLabelFileSizeDisplay.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabelFileSizeDisplay, gridBagConstraints);

        jCheckBoxIsDir.setToolTipText("");
        jCheckBoxIsDir.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jCheckBoxIsDir, gridBagConstraints);

        jButtonDownload.setText("Download");
        jButtonDownload.setEnabled(false);
        jButtonDownload.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDownloadActionPerformed(evt);
            }
        });
        jPanel3.add(jButtonDownload);

        jButtonView.setText("View Content");
        jButtonView.setEnabled(false);
        jButtonView.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonViewActionPerformed(evt);
            }
        });
        jPanel3.add(jButtonView);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel2.add(jPanel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jPanel2, gridBagConstraints);

        jSplitPane1.setRightComponent(jPanel1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jSplitPane1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonDownloadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDownloadActionPerformed
    {//GEN-HEADEREND:event_jButtonDownloadActionPerformed
        downloadSelectedFile();
    }//GEN-LAST:event_jButtonDownloadActionPerformed

    private void jButtonViewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonViewActionPerformed
    {//GEN-HEADEREND:event_jButtonViewActionPerformed
        viewFileContent();
    }//GEN-LAST:event_jButtonViewActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDownload;
    private javax.swing.JButton jButtonView;
    private javax.swing.JCheckBox jCheckBoxIsDir;
    private javax.swing.JComboBox jComboBoxFileContext;
    private javax.swing.JEditorPane jEditorPaneFileContent;
    private javax.swing.JLabel jLabelFileChecksum;
    private javax.swing.JLabel jLabelFileName;
    private javax.swing.JLabel jLabelFilePath;
    private javax.swing.JLabel jLabelFileSize;
    private javax.swing.JLabel jLabelFileSizeDisplay;
    private javax.swing.JLabel jLabelIsDir;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextField jTextFieldFileChecksum;
    private javax.swing.JTextField jTextFieldFileName;
    private javax.swing.JTextField jTextFieldFilePath;
    private javax.swing.JTree jTreeFileBrowser;
    // End of variables declaration//GEN-END:variables
}
