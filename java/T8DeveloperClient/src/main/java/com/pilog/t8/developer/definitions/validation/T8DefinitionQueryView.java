package com.pilog.t8.developer.definitions.validation;

import com.pilog.t8.developer.view.*;
import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.cache.LRUCache;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.files.FileUtilities;
import com.pilog.t8.utilities.files.text.TextOutputFile;
import com.pilog.t8.utilities.strings.Strings;
import java.io.File;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionQueryView extends T8DefaultDeveloperView
{
    private final T8DefinitionContext definitionContext;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DeveloperView parentView;
    private T8DeveloperViewFactory viewFactory;
    private final DefaultTableModel tableModel;

    public T8DefinitionQueryView(T8DefinitionContext definitionContext, T8DeveloperView parentView)
    {
        this.definitionContext = definitionContext;
        this.clientContext = definitionContext.getClientContext();
        this.context = definitionContext.getContext();
        this.parentView = parentView;
        initComponents();
        this.tableModel = (DefaultTableModel)jTableMapping.getModel();
    }

    @Override
    public String getHeader()
    {
        return "JSON View";
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSelectedDefinition(T8Definition definition)
    {
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    private void addEntry(String identifier)
    {

        Object[] row;

        row = new Object[2];
        row[0] = identifier;
        row[1] = false;

        tableModel.addRow(row);
    }

    private void deleteSelectedRows()
    {
        int selectedIndex;

        while ((selectedIndex = jTableMapping.getSelectedRow()) > -1)
        {
            tableModel.removeRow(selectedIndex);
        }
    }

    private void loadFromEntity()
    {
        T8DefinitionManager definitionManager;
        String entityId;

        definitionManager = clientContext.getDefinitionManager();

        entityId = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter the identifier of the entity to use.", "Data Entity Identifier", JOptionPane.QUESTION_MESSAGE);
        if (!Strings.isNullOrEmpty(entityId))
        {
            try
            {
                T8DataEntityDefinition entityDefinition;

                entityDefinition = (T8DataEntityDefinition)definitionManager.getRawDefinition(context, null, entityId);
                if (entityDefinition != null)
                {
                    String fieldId;

                    fieldId = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter the identifier of the entity field from which to extract identifier values.", "Identifier Field", JOptionPane.QUESTION_MESSAGE);
                    if (entityDefinition.containsField(fieldId))
                    {
                        int count;

                        count = T8DataManagerOperationHandler.countDataEntities(context, null, entityId, null);
                        if (count < 50001)
                        {
                            List<T8DataEntity> retrievedData;
                            TreeSet<String> idSet;

                            idSet = new TreeSet<>();
                            retrievedData = T8DataManagerOperationHandler.selectDataEntities(context, entityId, null, 0, 50000);
                            for (T8DataEntity entity : retrievedData)
                            {
                                String identifier;

                                identifier = (String)entity.getFieldValue(fieldId);
                                idSet.add(identifier);
                            }

                            for (String id : idSet)
                            {
                                addEntry(id);
                            }
                        }
                        else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity '" + fieldId + "' countains " + count + " rows, exceeding the 50000 limit.", "Data Size Restriction Exceeded", JOptionPane.ERROR_MESSAGE);
                    }
                    else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity Field '" + fieldId + "' could not be found in data entity '" + entityId + "'.", "Data Entity Field Not Found", JOptionPane.ERROR_MESSAGE);
                }
                else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Entity '" + entityId + "' could not be found.", "Data Entity Not Found", JOptionPane.ERROR_MESSAGE);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving String Map from data entity.", e);
                JOptionPane.showMessageDialog(clientContext.getParentWindow(), "An unexpected exception occurred during the requested operation.", "Operation Failure", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void loadFromTextFile()
    {
        try
        {
            String filePath;
            String separator;

            separator = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter the separator character to use.", "Separator Input", HEIGHT);

            filePath = BasicFileChooser.getLoadFilePath(clientContext.getParentWindow(), null, "All Files");
            if (filePath != null)
            {
                String text;

                text = FileUtilities.readTextFile(new File (filePath), "UTF-8");
                if (text != null)
                {
                    TreeSet<String> idSet;

                    idSet = new TreeSet<>();
                    for (String identifier : text.split(Pattern.quote(separator)))
                    {
                        String trimmedId;

                        trimmedId = identifier.trim();
                        if (trimmedId.length() > 0)
                        {
                            idSet.add(trimmedId);
                        }
                    }

                    for (String id : idSet)
                    {
                        addEntry(id);
                    }

                    JOptionPane.showMessageDialog(clientContext.getParentWindow(), idSet.size() + " Definition Identifiers Imported", "Operation Complete", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void exportReport()
    {
        TextOutputFile outputFile = null;

        try
        {
            String filePath;

            filePath = BasicFileChooser.getSaveFilePath(clientContext.getParentWindow(), ".txt", "Text Files");
            if (filePath != null)
            {
                TreeSet<String> notFoundIdSet;

                notFoundIdSet = new TreeSet<>();
                for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
                {
                    String identifier;
                    boolean found;

                    found = (boolean)tableModel.getValueAt(rowIndex, 1);
                    identifier = (String)tableModel.getValueAt(rowIndex, 0);
                    if (!found)
                    {
                        notFoundIdSet.add(identifier);
                    }
                }

                outputFile = new TextOutputFile(new File(filePath), "UTF-8");
                outputFile.openFile();
                outputFile.writeLine("Identifiers Not Found");
                outputFile.writeLine("");

                for (String identifier : notFoundIdSet)
                {
                    outputFile.writeLine(identifier);
                }

                outputFile.closeFile();
                Toast.show("Report Exported Successfully", Toast.Style.SUCCESS);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (outputFile != null) outputFile.closeFile();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * This entire method should be a strict server-side operation.
     * It is implemented here for experimental purposes only.
     */
    private void process()
    {
        T8DefinitionManager definitionManager;
        LRUCache<String, T8Definition> definitionCache;
        int notFoundCount;

        definitionCache = new LRUCache<>(100);

        notFoundCount = 0;
        definitionManager = clientContext.getDefinitionManager();
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            try
            {
                String definitionId;
                boolean found;

                found = false;
                definitionId = (String)tableModel.getValueAt(rowIndex, 0);
                if (definitionId != null)
                {
                    definitionId = definitionId.trim();
                    if (T8IdentifierUtilities.getLocalIdentifierPart(definitionId) != null)
                    {
                        T8Definition definition;
                        String globalPart;
                        String localPart;

                        // Get the global and local parts of the identifier.
                        globalPart = T8IdentifierUtilities.getGlobalIdentifierPart(definitionId);
                        localPart = T8IdentifierUtilities.getLocalIdentifierPart(definitionId);

                        // Try to find the required global definition in the cache.
                        if (definitionCache.containsKey(globalPart))
                        {
                            definition = definitionCache.get(globalPart);
                        }
                        else
                        {
                            definition = definitionManager.getRawDefinition(context, null, globalPart);
                            definitionCache.put(globalPart, definition);
                        }

                        // If the definition was found, set the flag.
                        if (definition != null)
                        {
                            if (definition.getLocalDefinition(localPart) != null)
                            {
                                found = true;
                            }
                        }
                    }
                    else
                    {
                        T8DefinitionMetaData definitionMetaData;

                        definitionMetaData = definitionManager.getDefinitionMetaData(null, definitionId);
                        if (definitionMetaData != null) found = true;
                    }

                    if (found)
                    {
                        tableModel.setValueAt(true, rowIndex, 1);
                    }
                    else
                    {
                        tableModel.setValueAt(false, rowIndex, 1);
                        notFoundCount++;
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                tableModel.setValueAt(false, rowIndex, 1);
            }
        }

        JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definitions Not Found: " + notFoundCount, "Operation Complete", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jTabbedPaneMain = new javax.swing.JTabbedPane();
        jPanelExistenceChecking = new javax.swing.JPanel();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonAddRow = new javax.swing.JButton();
        jButtonDeleteRows = new javax.swing.JButton();
        jButtonLoadFromEntity = new javax.swing.JButton();
        jButtonLoadFromTextFile = new javax.swing.JButton();
        jButtonExportToTextFile = new javax.swing.JButton();
        jButtonProcess = new javax.swing.JButton();
        jScrollPaneMapping = new javax.swing.JScrollPane();
        jTableMapping = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jPanelExistenceChecking.setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonAddRow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/tableInsertRowIcon.png"))); // NOI18N
        jButtonAddRow.setText("Add Row");
        jButtonAddRow.setToolTipText("Add a new row to the table.");
        jButtonAddRow.setFocusable(false);
        jButtonAddRow.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddRow.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddRowActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAddRow);

        jButtonDeleteRows.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/tableDeleteRowIcon.png"))); // NOI18N
        jButtonDeleteRows.setText("Delete Rows");
        jButtonDeleteRows.setToolTipText("Delete all selected rows from the table.");
        jButtonDeleteRows.setFocusable(false);
        jButtonDeleteRows.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDeleteRows.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDeleteRowsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDeleteRows);

        jButtonLoadFromEntity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png"))); // NOI18N
        jButtonLoadFromEntity.setText("Load from Entity");
        jButtonLoadFromEntity.setToolTipText("Load content into the table from a database entity.");
        jButtonLoadFromEntity.setFocusable(false);
        jButtonLoadFromEntity.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonLoadFromEntity.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonLoadFromEntityActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonLoadFromEntity);

        jButtonLoadFromTextFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonLoadFromTextFile.setText("Load from Text File");
        jButtonLoadFromTextFile.setFocusable(false);
        jButtonLoadFromTextFile.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonLoadFromTextFile.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonLoadFromTextFileActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonLoadFromTextFile);

        jButtonExportToTextFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/diskArrowIcon.png"))); // NOI18N
        jButtonExportToTextFile.setText("Export Report");
        jButtonExportToTextFile.setFocusable(false);
        jButtonExportToTextFile.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonExportToTextFile.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonExportToTextFileActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonExportToTextFile);

        jButtonProcess.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/testIcon.png"))); // NOI18N
        jButtonProcess.setText("Process");
        jButtonProcess.setFocusable(false);
        jButtonProcess.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonProcess.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonProcessActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonProcess);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelExistenceChecking.add(jToolBarMain, gridBagConstraints);

        jTableMapping.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Identifier", "Found"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean []
            {
                true, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableMapping.setRowHeight(25);
        jScrollPaneMapping.setViewportView(jTableMapping);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelExistenceChecking.add(jScrollPaneMapping, gridBagConstraints);

        jTabbedPaneMain.addTab("Existence Checking", jPanelExistenceChecking);

        add(jTabbedPaneMain, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonLoadFromEntityActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonLoadFromEntityActionPerformed
    {//GEN-HEADEREND:event_jButtonLoadFromEntityActionPerformed
        loadFromEntity();
    }//GEN-LAST:event_jButtonLoadFromEntityActionPerformed

    private void jButtonProcessActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonProcessActionPerformed
    {//GEN-HEADEREND:event_jButtonProcessActionPerformed
        process();
    }//GEN-LAST:event_jButtonProcessActionPerformed

    private void jButtonAddRowActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddRowActionPerformed
    {//GEN-HEADEREND:event_jButtonAddRowActionPerformed
        addEntry(null);
    }//GEN-LAST:event_jButtonAddRowActionPerformed

    private void jButtonDeleteRowsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteRowsActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteRowsActionPerformed
        deleteSelectedRows();
    }//GEN-LAST:event_jButtonDeleteRowsActionPerformed

    private void jButtonLoadFromTextFileActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonLoadFromTextFileActionPerformed
    {//GEN-HEADEREND:event_jButtonLoadFromTextFileActionPerformed
        loadFromTextFile();
    }//GEN-LAST:event_jButtonLoadFromTextFileActionPerformed

    private void jButtonExportToTextFileActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExportToTextFileActionPerformed
    {//GEN-HEADEREND:event_jButtonExportToTextFileActionPerformed
        exportReport();
    }//GEN-LAST:event_jButtonExportToTextFileActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddRow;
    private javax.swing.JButton jButtonDeleteRows;
    private javax.swing.JButton jButtonExportToTextFile;
    private javax.swing.JButton jButtonLoadFromEntity;
    private javax.swing.JButton jButtonLoadFromTextFile;
    private javax.swing.JButton jButtonProcess;
    private javax.swing.JPanel jPanelExistenceChecking;
    private javax.swing.JScrollPane jScrollPaneMapping;
    private javax.swing.JTabbedPane jTabbedPaneMain;
    private javax.swing.JTable jTableMapping;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
