package com.pilog.t8.developer.definitions.defaulteditor.sql;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.T8DefaultDefinitionDatumEditor;
import com.pilog.t8.developer.utils.GUIDTermTooltipSupplier;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8SQLDatumEditor extends T8DefaultDefinitionDatumEditor implements FocusListener
{
    private RSyntaxTextArea rSyntaxTextArea;

    public T8SQLDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        rSyntaxTextArea.addFocusListener(this);
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
        rSyntaxTextArea.setToolTipSupplier(new GUIDTermTooltipSupplier(context));
    }

    @Override
    public void stopComponent()
    {
        rSyntaxTextArea.setToolTipSupplier(null);
    }

    @Override
    public void commitChanges()
    {
        setDefinitionDatum(rSyntaxTextArea.getText());
    }

    @Override
    public void setEditable(boolean editable)
    {
        rSyntaxTextArea.setEditable(editable);
    }

    @Override
    public void refreshEditor()
    {
        Object value;

        value = getDefinitionDatum();
        rSyntaxTextArea.setText(value != null ? value.toString() : null);
    }

    @Override
    public void focusGained(FocusEvent e)
    {
    }

    @Override
    public void focusLost(FocusEvent e)
    {
        commitChanges();
    }

    private void initComponents()
    {
        RTextScrollPane rTextScrollPane;

        setPreferredSize(new java.awt.Dimension(0, 500));
        setLayout(new java.awt.BorderLayout());

        rSyntaxTextArea = new RSyntaxTextArea();
        rSyntaxTextArea.setSyntaxEditingStyle(RSyntaxTextArea.SYNTAX_STYLE_SQL);
        rSyntaxTextArea.setAnimateBracketMatching(true);
        rSyntaxTextArea.setAntiAliasingEnabled(true);
        rSyntaxTextArea.setAutoIndentEnabled(true);
        rSyntaxTextArea.setBracketMatchingEnabled(true);
        rSyntaxTextArea.setCodeFoldingEnabled(true);

        rTextScrollPane = new RTextScrollPane(rSyntaxTextArea);
        rTextScrollPane.setFoldIndicatorEnabled(true);
        rTextScrollPane.setIconRowHeaderEnabled(true);
        rTextScrollPane.setLineNumbersEnabled(true);

        add(rTextScrollPane, java.awt.BorderLayout.CENTER);
    }
}
