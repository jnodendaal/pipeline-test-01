package com.pilog.t8.developer.definitions.graphview.state;

import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.view.DefaultVertexRenderer;
import com.pilog.graph.view.Graph;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateDefinition;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Transparency;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.DataBufferInt;
import java.awt.image.Kernel;
import java.util.HashMap;

/**
 * @author Bouwer du Preez
 */
public class T8StateVertexRenderer extends DefaultVertexRenderer
{
    private T8StateGraphConfiguration config;

    public static String KEY_BLUR_QUALITY = "blur_quality";
    public static String VALUE_BLUR_QUALITY_FAST = "fast";
    public static String VALUE_BLUR_QUALITY_HIGH = "high";

    protected BufferedImage shadow = null;
    protected BufferedImage original = null;
    protected float shadowAngle = 45;
    protected int shadowDistance = 5;
    protected int shadowSize = 5;
    protected float shadowOpacity = 0.5f;
    protected Color shadowColor = new Color(0x000000);
    protected Insets vertexInsets = new Insets(30, 30, 30, 30);

    // cached values for fast painting
    protected int distance_x = 0;
    protected int distance_y = 0;
    protected HashMap<Object, Object> hints;

    public T8StateVertexRenderer(T8StateGraphConfiguration config)
    {
        this.config = config;
        computeShadowPosition();
        hints = new HashMap<Object, Object>();
        hints.put(KEY_BLUR_QUALITY, VALUE_BLUR_QUALITY_FAST);
    }

    public T8StateGraphConfiguration getConfiguration()
    {
        return config;
    }

    @Override
    public Dimension getVertexSize(Graph graph, GraphVertex vertex, boolean isSelected)
    {
        FontMetrics fontMetrics;
        Object vertexData;

        vertexData = (Object)vertex.getVertexDataObject();
        if (vertexData instanceof T8DataObjectStateDefinition)
        {
            Dimension size;

            fontMetrics = this.getFontMetrics(config.getTextFont());
            size = new Dimension();
            size.width = fontMetrics.stringWidth(((T8DataObjectStateDefinition)vertexData).getIdentifier());
            size.width += (vertexInsets.left + vertexInsets.right);
            size.height = fontMetrics.getHeight();
            size.height += (vertexInsets.top + vertexInsets.bottom);
            return size;
        }
        else return new Dimension(50, 50);
    }

    public void setSubject(BufferedImage subject)
    {
        if (subject != null)
        {
            this.original = subject;
            refreshShadow();
        }
        else
        {
            this.original = null;
            this.original = null;
        }
    }

    private void paintDefinition(Graphics2D g2, T8Definition vertexDefinition)
    {
        Color highColor = LAFConstants.CONTENT_PANEL_BG_COLOR;
        Color lowColor = LAFConstants.PROPERTY_BLUE;
        int shapeWidth;
        int shapeHeight;

        // Set the selection color of the vertex shape.
        if (isSelected)
        {
            highColor = config.getSelectedHighColor();
            lowColor = config.getSelectedLowColor();
        }

        if (vertexDefinition instanceof T8DataObjectStateDefinition)
        {
            T8DataObjectStateDefinition stateDefinition;
            Shape vertexShape;

            stateDefinition = (T8DataObjectStateDefinition)vertexDefinition;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                if (stateDefinition.getCreatorFunctionalityIds().size() > 0)
                {
                    highColor = config.getStartHighColor();
                    lowColor = config.getStartLowColor();
                }
                else
                {
                    highColor = config.getActivityHighColor();
                    lowColor = config.getActivityLowColor();
                }
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;

            // Create the shape of the vertex and paint it.
            vertexShape = new Rectangle2D.Float(0, 0, shapeWidth, shapeHeight);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);

            // Paint the vertex label.
            paintLabel(g2, shapeWidth, shapeHeight, vertexDefinition.getIdentifier());
        }
    }

    @Override
    public void paint(Graphics g)
    {
        Graphics2D g2;
        Object vertexObject;

        // Cast graphics context to the required type.
        g2 = (Graphics2D)g;

        vertexObject = vertex.getVertexDataObject();
        if (vertexObject instanceof T8Definition)
        {
            T8Definition vertexDefinition;

            // Get the vertex definition.
            vertexDefinition = (T8Definition)vertex.getVertexDataObject();
            paintDefinition(g2, vertexDefinition);
        }
        else // Swimlane.
        {
            paintSwimLane(g2, vertex.getVertexDataObject() + "");
        }
    }

    private void paintSwimLane(Graphics g2, String swimLaneIdentifier)
    {
        Dimension size;
        Graphics2D g2LaneLabel;
        Rectangle cellBounds;
        Rectangle2D stringBounds;

        // Get the size of the swimlane.
        size = getSize();

        // Draw the swimlane outer border.
        g2.setColor(config.getSwimlaneBackgroundColor());
        g2.fillRect(0, 0, size.width-1, size.height-1);
        g2.setColor(config.getSwimlangeBorderColor());
        g2.drawRect(0, 0, size.width-1, size.height-1);

        // Create the swimlane label graphics context.
        g2LaneLabel = (Graphics2D)g2.create(0, 0, 20, size.height);

        // Set the transformation for the label cell.
        g2LaneLabel.rotate(-Math.PI/2.0);
        g2LaneLabel.translate(-size.height+1, 0);

        // Paint the label cell.
        g2LaneLabel.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2LaneLabel.setPaint(new GradientPaint(0, 0, Color.GRAY, size.height-1, 0, Color.WHITE));
        g2LaneLabel.fillRect(0, 0, size.height-1, 19);
        g2LaneLabel.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g2LaneLabel.setColor(config.getSwimlangeBorderColor());
        g2LaneLabel.drawRect(0, 0, size.height-1, 19);

        // Paint the label text.
        g2LaneLabel.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2LaneLabel.setColor(config.getSwimlaneTextColor());
        g2LaneLabel.setFont(config.getTextFont());
        cellBounds = g2LaneLabel.getClipBounds();
        stringBounds = getStringBounds(g2LaneLabel, swimLaneIdentifier);
        g2LaneLabel.drawString(swimLaneIdentifier, Math.round((cellBounds.getWidth()/2.0) - (stringBounds.getWidth()/2.0)), Math.round(1 + stringBounds.getHeight()));
        g2LaneLabel.dispose();
    }

    private void paintLabel(Graphics2D g2, int shapeWidth, int shapeHeight, String text)
    {
        Rectangle2D bounds;

        g2.setFont(config.getTextFont());
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(2));
        g2.setColor(Color.DARK_GRAY);

        bounds = getStringBounds(g2, text);
        g2.drawString(text, (float)((shapeWidth/2.0) - (bounds.getWidth()/2.0)), (float)((shapeHeight/2.0) + (bounds.getHeight()/4.0)));
    }

    private void paintShape(Graphics2D g2, Shape clipShape, Color highColor, Color lowColor, boolean fill, boolean paintShadow, int borderSize)
    {
        BufferedImage clipImage;
        Graphics2D g2Shape;
        int shapeWidth;
        int shapeHeight;

        // Create the clip image.
        clipImage = createClipImage(g2, clipShape);
        setSubject(clipImage);
        g2Shape = clipImage.createGraphics();
        shapeWidth = clipShape.getBounds().width;
        shapeHeight = clipShape.getBounds().height;

        if ((shadow != null) && (paintShadow) && (config.isShadowsEnabled()))
        {
            int x = (shapeWidth - shadow.getWidth()) / 2;
            int y = (shapeHeight - shadow.getHeight()) / 2;
            g2.drawImage(shadow, x + distance_x, y + distance_y, null);
        }

        if (original != null)
        {
            int x = (shapeWidth - original.getWidth()) / 2;
            int y = (shapeHeight - original.getHeight()) / 2;
            g2.drawImage(original, x, y, null);
        }

        // Fill the shape with a gradient.
        g2Shape.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2Shape.setComposite(AlphaComposite.SrcAtop);
        g2Shape.setPaint(new GradientPaint(0, 0, highColor, 0, shapeHeight, lowColor));
        if (fill) g2Shape.fill(clipShape);

        // Apply the border glow effect (or just a plain border).
        if (config.isBorderGlowEnabled())
        {
            paintBorderGlow(g2Shape, clipShape, highColor, lowColor, borderSize);
        }
        else
        {
            g2Shape.setPaint(config.getNodeBorderColor());
            g2Shape.draw(clipShape);
        }

        // Draw the clip image.
        g2.drawImage(clipImage, 0, 0, null);

        // Release the unused graphics context.
        g2Shape.dispose();
    }

    private Rectangle2D getStringBounds(Graphics2D g2, String string)
    {
        FontMetrics metrics;

        metrics = g2.getFontMetrics();
        return metrics.getStringBounds(string, 0, string.length(), g2);
    }

    private BufferedImage createClipImage(Graphics2D g, Shape shape)
    {
        int shapeWidth;
        int shapeHeight;

        shapeWidth = shape.getBounds().width;
        shapeHeight = shape.getBounds().height;

        // Create a translucent intermediate image in which we can perform the soft clipping.
        GraphicsConfiguration gc = g.getDeviceConfiguration();
        BufferedImage img = gc.createCompatibleImage(shapeWidth, shapeHeight, Transparency.TRANSLUCENT);
        Graphics2D g2 = img.createGraphics();

        // Clear the image so all pixels have zero alpha.
        g2.setComposite(AlphaComposite.Clear);
        g2.fillRect(0, 0, shapeWidth, shapeHeight);

        // Render our clip shape into the image.  Note that we enable
        // antialiasing to achieve the soft clipping effect.  Try
        // commenting out the line that enables antialiasing, and
        // you will see that you end up with the usual hard clipping.
        g2.setComposite(AlphaComposite.Src);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.WHITE);
        g2.fill(shape);
        g2.dispose();

        return img;
    }

    private static Color getMixedColor(Color c1, float pct1, Color c2, float pct2)
    {
        float[] clr1 = c1.getComponents(null);
        float[] clr2 = c2.getComponents(null);
        for (int i = 0; i < clr1.length; i++)
        {
            clr1[i] = (clr1[i] * pct1) + (clr2[i] * pct2);
        }
        return new Color(clr1[0], clr1[1], clr1[2], clr1[3]);
    }

    // Here's the trick... To render the glow, we start with a thick pen
    // of the "inner" color and stroke the desired shape.  Then we repeat
    // with increasingly thinner pens, moving closer to the "outer" color
    // and increasing the opacity of the color so that it appears to
    // fade towards the interior of the shape.  We rely on the "clip shape"
    // having been rendered into our destination image already so that
    // the SRC_ATOP rule will take care of clipping out the part of the
    // stroke that lies outside our shape.
    private void paintBorderGlow(Graphics2D g2, Shape clipShape, Color highColor, Color lowColor, int glowWidth)
    {
        Color glowInnerHigh;
        Color glowInnerLow;
        Color glowOuterHigh;
        Color glowOuterLow;
        int shapeWidth;
        int shapeHeight;
        int gw = glowWidth * 2;

        // Calculate colors to use.
        glowInnerHigh = highColor.brighter();
        glowInnerLow = highColor.brighter();
        glowOuterHigh = lowColor.darker();
        glowOuterLow = lowColor.darker();

        shapeWidth = clipShape.getBounds().width;
        shapeHeight = clipShape.getBounds().height;
        for (int i = gw; i >= 2; i -= 2)
        {
            float pct = (float) (gw - i) / (gw - 1);
            Color mixHi = getMixedColor(glowInnerHigh, pct, glowOuterHigh, 1.0f - pct);
            Color mixLo = getMixedColor(glowInnerLow, pct, glowOuterLow, 1.0f - pct);
            g2.setPaint(new GradientPaint(0.0f, shapeHeight * 0.25f, mixHi, 0.0f, shapeHeight, mixLo));
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, pct));
            g2.setStroke(new BasicStroke(i));
            g2.draw(clipShape);
        }
    }

    public void refreshShadow()
    {
        if (original != null)
        {
            shadow = createDropShadow(original);
        }
    }

    private void computeShadowPosition()
    {
        double angleRadians = Math.toRadians(shadowAngle);
        distance_x = (int) (Math.cos(angleRadians) * shadowDistance);
        distance_y = (int) (Math.sin(angleRadians) * shadowDistance);
    }

    private BufferedImage prepareImage(BufferedImage image)
    {
        BufferedImage subject = new BufferedImage(image.getWidth() + shadowSize * 2, image.getHeight() + shadowSize * 2, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = subject.createGraphics();
        g2.drawImage(image, null, shadowSize, shadowSize);
        g2.dispose();

        return subject;
    }

    private BufferedImage createDropShadow(BufferedImage image)
    {
        BufferedImage subject = prepareImage(image);

        if (hints.get(KEY_BLUR_QUALITY) == VALUE_BLUR_QUALITY_HIGH)
        {
            BufferedImage newShadow = new BufferedImage(subject.getWidth(), subject.getHeight(), BufferedImage.TYPE_INT_ARGB);
            BufferedImage shadowMask = createShadowMask(subject);
            getLinearBlurOp(shadowSize).filter(shadowMask, newShadow);
            return newShadow;
        }

        applyShadow(subject);
        return subject;
    }

    private void applyShadow(BufferedImage image)
    {
        int dstWidth = image.getWidth();
        int dstHeight = image.getHeight();

        int left = (shadowSize - 1) >> 1;
        int right = shadowSize - left;
        int xStart = left;
        int xStop = dstWidth - right;
        int yStart = left;
        int yStop = dstHeight - right;

        int shadowRgb = shadowColor.getRGB() & 0x00FFFFFF;

        int[] aHistory = new int[shadowSize];
        int historyIdx = 0;

        int aSum;

        int[] dataBuffer = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
        int lastPixelOffset = right * dstWidth;
        float sumDivider = shadowOpacity / shadowSize;

        // horizontal pass

        for (int y = 0, bufferOffset = 0; y < dstHeight; y++, bufferOffset = y * dstWidth)
        {
            aSum = 0;
            historyIdx = 0;
            for (int x = 0; x < shadowSize; x++, bufferOffset++)
            {
                int a = dataBuffer[bufferOffset] >>> 24;
                aHistory[x] = a;
                aSum += a;
            }

            bufferOffset -= right;

            for (int x = xStart; x < xStop; x++, bufferOffset++)
            {
                int a = (int) (aSum * sumDivider);
                dataBuffer[bufferOffset] = a << 24 | shadowRgb;

                // substract the oldest pixel from the sum
                aSum -= aHistory[historyIdx];

                // get the lastest pixel
                a = dataBuffer[bufferOffset + right] >>> 24;
                aHistory[historyIdx] = a;
                aSum += a;

                if (++historyIdx >= shadowSize)
                {
                    historyIdx -= shadowSize;
                }
            }
        }

        // vertical pass
        for (int x = 0, bufferOffset = 0; x < dstWidth; x++, bufferOffset = x)
        {
            aSum = 0;
            historyIdx = 0;
            for (int y = 0; y < shadowSize; y++, bufferOffset += dstWidth)
            {
                int a = dataBuffer[bufferOffset] >>> 24;
                aHistory[y] = a;
                aSum += a;
            }

            bufferOffset -= lastPixelOffset;

            for (int y = yStart; y < yStop; y++, bufferOffset += dstWidth)
            {
                int a = (int) (aSum * sumDivider);
                dataBuffer[bufferOffset] = a << 24 | shadowRgb;

                // substract the oldest pixel from the sum
                aSum -= aHistory[historyIdx];

                // get the lastest pixel
                a = dataBuffer[bufferOffset + lastPixelOffset] >>> 24;
                aHistory[historyIdx] = a;
                aSum += a;

                if (++historyIdx >= shadowSize)
                {
                    historyIdx -= shadowSize;
                }
            }
        }
    }

    private BufferedImage createShadowMask(BufferedImage image)
    {
        BufferedImage mask = new BufferedImage(image.getWidth(),
                image.getHeight(),
                BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = mask.createGraphics();
        g2d.drawImage(image, 0, 0, null);
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_IN,
                shadowOpacity));
        g2d.setColor(shadowColor);
        g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
        g2d.dispose();

        return mask;
    }

    private ConvolveOp getLinearBlurOp(int size)
    {
        float[] data = new float[size * size];
        float value = 1.0f / (float) (size * size);
        for (int i = 0; i < data.length; i++)
        {
            data[i] = value;
        }
        return new ConvolveOp(new Kernel(size, size, data));
    }
}
