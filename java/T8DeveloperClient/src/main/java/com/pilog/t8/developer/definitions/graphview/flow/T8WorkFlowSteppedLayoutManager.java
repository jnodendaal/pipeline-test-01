package com.pilog.t8.developer.definitions.graphview.flow;

import com.pilog.graph.model.GraphModel;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.flow.T8FlowBoundaryNodeDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.definition.graph.T8GraphNodeDefinition;
import com.pilog.t8.developer.definitions.graphview.layout.BoxLayoutMatrix;
import com.pilog.t8.developer.definitions.graphview.layout.LayoutElement;
import com.pilog.t8.developer.definitions.graphview.layout.LayoutMatrix;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowSteppedLayoutManager extends LayoutMatrix
{
    private final ArrayList<String> swimLaneIdentifiers; // The current list of identifiers rendered.
    private ArrayList<String> swimLaneIdentifierSequence; // The list of possible identifiers, indicating the sequence to display.
    private final ArrayList<T8WorkFlowLayoutElement> freeLayoutElements;
    private final T8WorkFlowVertexRenderer renderer;
    private final T8WorkFlowGraphConfiguration config;
    private final int swimLaneLabelWidth;
    private final int swimLaneInset;
    
    public T8WorkFlowSteppedLayoutManager(T8WorkFlowVertexRenderer renderer, T8WorkFlowGraphConfiguration config)
    {
        this.renderer = renderer;
        this.config = config;
        this.swimLaneLabelWidth = 20; // The width of the swimlane label.
        this.swimLaneInset = 50; // Inset inside of a swimlane.
        this.swimLaneIdentifiers = new ArrayList<String>();
        this.freeLayoutElements = new ArrayList<T8WorkFlowLayoutElement>();
        this.setHorizontalSpacing(100); // Horizontal distance between cells in the layout matrix.
        this.setVerticalSpacing(5 + (2 * swimLaneInset)); // Vertical distance between cells in the matrix.
        this.setInsets((50 + swimLaneInset), 50, 50, (50 + swimLaneLabelWidth + swimLaneInset));
    }
    
    public GraphModel createGraphModel(T8WorkFlowDefinition graphDefinition)
    {
        List<LayoutElement> cells;
        List<T8WorkFlowLayoutElement> layoutElements;
        T8WorkFlowModelBuilder modelBuilder;

        // Do the layout of the model elements.
        rebuildMatrix(graphDefinition);
        doLayout(0, 0); // This will cause the entire layout to be updated.
        layoutElements = new ArrayList<T8WorkFlowLayoutElement>();
        
        // Add all vertices.
        cells = getElements();
        for (LayoutElement cell : cells)
        {
            ArrayList<LayoutElement> vertexElements;
            BoxLayoutMatrix boxLayout;
            
            boxLayout = (BoxLayoutMatrix)cell;
            vertexElements = boxLayout.getElements();
            for (LayoutElement vertexElement : vertexElements)
            {
                T8WorkFlowLayoutElement layoutElement;
                
                layoutElement = (T8WorkFlowLayoutElement)vertexElement;
                layoutElements.add(layoutElement);
            }
        }
        
        // Add the free-layout elements.
        layoutElements.addAll(freeLayoutElements);
        
        // Build the model and return it.
        modelBuilder = new T8WorkFlowModelBuilder(graphDefinition, renderer, config);
        return modelBuilder.createGraphModel(layoutElements, swimLaneIdentifiers);
    }
    
    private void rebuildMatrix(T8WorkFlowDefinition graphDefinition)
    {
        HashMap<String, T8WorkFlowNodeDefinition> definitionMap;
        LinkedList<T8WorkFlowLayoutElement> elementQueue;
        ArrayList<T8GraphNodeDefinition> definitionList;
        int elementCount;
        
        clear();
        elementCount = 0;
        swimLaneIdentifiers.clear();
        freeLayoutElements.clear();

        // Create a map containing all flow vertex definitions indexed by their identifiers.
        definitionList = graphDefinition.getNodeDefinitions();
        definitionMap = new HashMap<String, T8WorkFlowNodeDefinition>();
        for (T8Definition vertexDefinition : definitionList)
        {
            definitionMap.put(vertexDefinition.getIdentifier(), (T8WorkFlowNodeDefinition)vertexDefinition);
        }
        
        // Create a queue for holding the definitions to be processed and add the start definition to it.
        elementQueue = new LinkedList<T8WorkFlowLayoutElement>();
        elementQueue.add(new T8WorkFlowLayoutElement(elementCount++, 0, (T8WorkFlowNodeDefinition)graphDefinition.getStartNodeDefinition(), renderer));
        definitionMap.remove(graphDefinition.getStartNodeDefinition().getIdentifier());
        while (elementQueue.size() > 0)
        {
            T8WorkFlowLayoutElement nextLayoutElement;
            ArrayList<String> childIdentifiers;
            String vertexIdentifier;
            
            // Get the next layout element and definition from the queue and process it.
            nextLayoutElement = elementQueue.pop();
            vertexIdentifier = nextLayoutElement.getNodeDefinition().getIdentifier();
            
            // Add the vertex to the layout.
            addElement(nextLayoutElement);
            
            // Get all the child definitions linked to this vertex and add them to the queue for processing.
            childIdentifiers = graphDefinition.getChildNodeIdentifiers(vertexIdentifier);
            for (String childIdentifier : childIdentifiers)
            {
                T8WorkFlowNodeDefinition childDefinition;
                
                childDefinition = definitionMap.get(childIdentifier);
                if (childDefinition != null)
                {
                    T8WorkFlowLayoutElement childElement;
                    
                    childElement = new T8WorkFlowLayoutElement(elementCount++, nextLayoutElement.getWorkFlowStep() + 1, childDefinition, renderer);
                    childElement.setVertexMovable(!childElement.isBoundaryNode());
                    elementQueue.add(childElement);
                    definitionMap.remove(childIdentifier); // Remove it from the map so no vertex is added twice to the model.
                }
            }
        }
        
        // Some nodes may not be reachable from the start node, so add all of them now.
        for (T8WorkFlowNodeDefinition nodeDefinition : definitionMap.values())
        {
            T8WorkFlowLayoutElement layoutElement;

            layoutElement = new T8WorkFlowLayoutElement(elementCount++, getColumnCount(), nodeDefinition, renderer);
            layoutElement.setVertexMovable(!layoutElement.isBoundaryNode());
            if ((layoutElement.isBoundaryNode()) && (((T8FlowBoundaryNodeDefinition)layoutElement.getNodeDefinition()).getAttachedToNodeIdentifier() != null))
            {
                freeLayoutElements.add(layoutElement);
            }
            else
            {
                addElement(layoutElement);
            }
        }
    }
    
    private void addElement(T8WorkFlowLayoutElement element)
    {
        String userProfileIdentifier;
        BoxLayoutMatrix cell;
        int swimLaneIndex;

        // Get the lane identifier of the element.
        userProfileIdentifier = element.getSwimLaneIdentifier();
        
        // Determine the swimlane index for the specified user profile.
        if (!swimLaneIdentifiers.contains(userProfileIdentifier)) createNewSwimLane(userProfileIdentifier);
        swimLaneIndex = getSwimLaneIndex(userProfileIdentifier);
        
        // Get the cell where the vertex is to be added.
        ensureSize(swimLaneIndex +1, element.getWorkFlowStep() +1);
        cell = (BoxLayoutMatrix)getElement(swimLaneIndex, element.getWorkFlowStep());
        if (cell == null)
        {
            cell = new BoxLayoutMatrix(BoxLayoutMatrix.Orientation.VERTICAL);
            cell.setVerticalSpacing(50);
            addElement(cell, swimLaneIndex, element.getWorkFlowStep());
        }
        
        // Add a new layout element to the cell.
        cell.addElement(element);
    }
    
    public Point getSwimLaneLocation(String swimLaneIdentifier)
    {
        Point rowLocation;
        
        rowLocation = getRowLocation(getSwimLaneIndex(swimLaneIdentifier));
        rowLocation.translate(-(swimLaneLabelWidth + swimLaneInset), -swimLaneInset);
        return rowLocation;
    }
    
    public Rectangle getSwimLaneBounds(String swimLaneIdentifier)
    {
        Rectangle rowBounds;
        
        rowBounds = getRowBounds(getSwimLaneIndex(swimLaneIdentifier));
        rowBounds.width += (swimLaneLabelWidth + (2 * swimLaneInset));
        rowBounds.height += (2 * swimLaneInset);
        return rowBounds;
    }

    public ArrayList<String> getSwimLaneIdentifiers()
    {
        return new ArrayList<String>(swimLaneIdentifiers);
    }
    
    public ArrayList<String> getSwimLaneIdentifierSequence()
    {
        return swimLaneIdentifierSequence;
    }
    
    public void setSwimLaneIdentifierSequence(List<String> newSequence)
    {
        List<String> sequence;
        
        // Make sure only valid identifiers are used.
        sequence = new ArrayList<String>();
        for (String swimLaneIdentifier : newSequence)
        {
            if ((swimLaneIdentifiers.contains(swimLaneIdentifier)) && (!sequence.contains(swimLaneIdentifier)))
            {
                sequence.add(swimLaneIdentifier);
            }
            else throw new IllegalArgumentException("Invalid swim lane identifier set: " + newSequence);
        }
        
        // If the new sequence list contains only valid identifiers, use it to set the swim lane identifiers list.
        if (sequence.size() == swimLaneIdentifiers.size())
        {
            swimLaneIdentifierSequence = new ArrayList<String>(sequence);
            T8Log.log("Set swim lane sequence to: " + swimLaneIdentifierSequence);
        }
        else throw new IllegalArgumentException("Invalid swim lane identifier set: " + newSequence);
    }
    
    private int createNewSwimLane(String swimLaneIdentifier)
    {
        swimLaneIdentifiers.add(swimLaneIdentifier);
        if (swimLaneIdentifierSequence == null) swimLaneIdentifierSequence = new ArrayList<String>();
        if (!swimLaneIdentifierSequence.contains(swimLaneIdentifier)) swimLaneIdentifierSequence.add(swimLaneIdentifier);
        return getSwimLaneIndex(swimLaneIdentifier);
    }
    
    private int getSwimLaneIndex(String swimLaneIdentifier)
    {
        int index;
        
        index = swimLaneIdentifierSequence != null ? swimLaneIdentifierSequence.indexOf(swimLaneIdentifier) : -1;
        if (index == -1) throw new RuntimeException("Swim Lane Sequence not defined: " + swimLaneIdentifier);
        else return index;
    }
}
