package com.pilog.t8.developer.definitions.projectmanager;

import java.awt.Component;
import java.awt.Window;

/**
 * @author Bouwer du Preez
 */
public class T8GraphConfigurationDialog extends javax.swing.JDialog
{
    protected T8GraphConfigurationDialog(Window parent, Component contentComponent)
    {
        super(parent, ModalityType.DOCUMENT_MODAL);
        initComponents();
        jPanelContent.add(contentComponent, java.awt.BorderLayout.CENTER);
        setSize(500, 500);
        setLocationRelativeTo(null);
    }

    public static void showConfigurationDialog(Window parentWindow, Component contentComponent)
    {
        T8GraphConfigurationDialog dialog;

        dialog = new T8GraphConfigurationDialog(parentWindow, contentComponent);
        dialog.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jPanelControls = new javax.swing.JPanel();
        jButtonClose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelContent.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        getContentPane().add(jPanelContent, gridBagConstraints);

        jPanelControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING));

        jButtonClose.setText("Close");
        jButtonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCloseActionPerformed(evt);
            }
        });
        jPanelControls.add(jButtonClose);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        getContentPane().add(jPanelControls, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonCloseActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelControls;
    // End of variables declaration//GEN-END:variables
}
