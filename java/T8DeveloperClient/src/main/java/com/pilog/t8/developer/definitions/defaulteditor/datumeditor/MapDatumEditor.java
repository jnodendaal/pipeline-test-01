package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.developer.utils.TableMapRowFilter;
import com.pilog.t8.utilities.collections.MapEntry;
import com.pilog.t8.utilities.components.list.ListPopupMenuListener;
import com.pilog.t8.utilities.components.list.popupmenu.SearchableListPopup;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.jdesktop.swingx.JXSearchField;

/**
 * @author Bouwer du Preez
 */
public class MapDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private final DefaultTableModel tableModel;
    private TableRowSorter<TableModel> tableRowSorter;
    private TableMapRowFilter rowFilter;

    public MapDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        tableModel = (DefaultTableModel)jTableMap.getModel();
    }

    @Override
    public void initializeComponent()
    {
        rowFilter = new TableMapRowFilter();
        tableRowSorter = new TableRowSorter<TableModel>(tableModel);
        tableRowSorter.setRowFilter(rowFilter);
        jTableMap.setRowSorter(tableRowSorter);

        jXSearchField.setInstantSearchDelay(100);
        jXSearchField.setSearchMode(JXSearchField.SearchMode.INSTANT);
        jXSearchField.setFindAction(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                rowFilter.setFilterString(jXSearchField.getText());

                tableRowSorter.sort();
                revalidate();
            }

        });
        jXSearchField.addActionListener(jXSearchField.getFindAction());
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void commitChanges()
    {
        LinkedHashMap<String, Object> valueMap;

        valueMap = new LinkedHashMap<String, Object>();
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            valueMap.put((String)tableModel.getValueAt(rowIndex, 0), tableModel.getValueAt(rowIndex, 1));
        }

        setDefinitionDatum(valueMap);
    }

    @Override
    public void setEditable(boolean editable)
    {
        jButtonAdd.setEnabled(editable);
        jButtonRemove.setEnabled(editable);
        jButtonMoveUp.setEnabled(editable);
        jButtonMoveDown.setEnabled(editable);
        jButtonSortAlphabetic.setEnabled(editable);
        jTableMap.setEnabled(editable);
    }

    @Override
    public void refreshEditor()
    {
        Object value;

        value = getDefinitionDatum();

        // Clear the model.
        while (tableModel.getRowCount() > 0) tableModel.removeRow(0);

        // Add the values in the map to the table model.
        if (value != null)
        {
            Map<String, Object> valueMap;

            valueMap = (Map<String, Object>)value;
            for (String key : valueMap.keySet())
            {
                Object[] rowData;

                rowData = new Object[2];
                rowData[0] = key;
                rowData[1] = valueMap.get(key);

                tableModel.addRow(rowData);
            }
        }
    }

    private void addMapValue()
    {
        try
        {
            SearchableListPopup<T8DefinitionDatumOption> popupMenu;
            ArrayList<T8DefinitionDatumOption> datumOptions;

            datumOptions = definition.getDatumOptions(definitionContext, datumType.getIdentifier());
            if (datumOptions != null)
            {
                popupMenu = new SearchableListPopup<>(datumOptions);
                popupMenu.addPopupListener(new PopupMenuListener());
                popupMenu.show(jButtonAdd, 0, 0);
            }
            else
            {
                addMapValue(null, null);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while loading datum '" + datumType.getIdentifier() + "' options.", e);
        }
    }

    private void addMapValue(String key, Object value)
    {
        Object[] rowData;

        rowData = new Object[2];
        rowData[0] = key;
        rowData[1] = value;

        tableModel.addRow(rowData);
    }

    private void removeSelectedMapValues()
    {
        int selectedIndex;

        while ((selectedIndex = jTableMap.getSelectedRow()) != -1)
        {
            tableModel.removeRow(selectedIndex);
        }
    }

    private List<MapEntry<Object, Object>> getAllValues()
    {
        List<MapEntry<Object, Object>> values;

        values = new ArrayList<MapEntry<Object, Object>>();
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            values.add(new MapEntry<Object, Object>(tableModel.getValueAt(rowIndex, 0), tableModel.getValueAt(rowIndex, 1)));
        }

        return values;
    }

    private LinkedHashMap<Object, Object> buildMap(List<MapEntry<Object, Object>> values)
    {
        if (values != null)
        {
            LinkedHashMap<Object, Object> map;

            map = new LinkedHashMap<Object, Object>();
            for (MapEntry<Object, Object> entry : values)
            {
                map.put(entry.getKey(), entry.getValue());
            }

            return map;
        }
        else return null;
    }

    private void moveSelectionUp()
    {
        List<MapEntry<Object, Object>> values;
        int[] selectedIndices;

        // Stop any editing that may be in progress on the table.
        if (jTableMap.getCellEditor() != null) jTableMap.getCellEditor().stopCellEditing();

        values = getAllValues();
        selectedIndices = jTableMap.getSelectedRows();
        if (selectedIndices.length > 0)
        {
            MapEntry<Object, Object> selectedValue;

            // Decrement all selected Indices.
            for (int selectedIndex : selectedIndices)
            {
                if (selectedIndex > 0)
                {
                    selectedValue = values.remove(selectedIndex);
                    values.add(selectedIndex-1, selectedValue);
                }
            }

            // Commit the updated value list.
            setDefinitionDatum(buildMap(values));

            // Refresh the editor.
            refreshEditor();

            // Decrement all selected indices (because they've been moved up).
            jTableMap.clearSelection();
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]-1);
                jTableMap.getSelectionModel().addSelectionInterval(selectedIndices[indexIndex], selectedIndices[indexIndex]);
            }
        }
    }

    private void moveSelectionDown()
    {
        List<MapEntry<Object, Object>> values;
        int[] selectedIndices;

        // Stop any editing that may be in progress on the table.
        if (jTableMap.getCellEditor() != null) jTableMap.getCellEditor().stopCellEditing();

        values = getAllValues();
        selectedIndices = jTableMap.getSelectedRows();
        if (selectedIndices.length > 0)
        {
            MapEntry<Object, Object> selectedValue;

            // Decrement all selected Indices.
            for (int selectedIndex : selectedIndices)
            {
                if (selectedIndex < values.size() -1)
                {
                    selectedValue = values.remove(selectedIndex);
                    values.add(selectedIndex+1, selectedValue);
                }
            }

            // Commit the updated value map.
            setDefinitionDatum(buildMap(values));

            // Refresh the editor.
            refreshEditor();

            // Increment all selected indices (because they've been moved down).
            jTableMap.clearSelection();
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]+1);
                jTableMap.getSelectionModel().addSelectionInterval(selectedIndices[indexIndex], selectedIndices[indexIndex]);
            }
        }
    }

    private void sortAlphabetic()
    {
        List<MapEntry<Object, Object>> values;
        TreeMap<Object, Object> sortedMap;

        // Stop any editing that may be in progress on the table.
        if (jTableMap.getCellEditor() != null) jTableMap.getCellEditor().stopCellEditing();

        values = getAllValues();
        sortedMap = new TreeMap<Object, Object>();
        for (MapEntry<Object, Object> entry : values)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        // Commit the updated value map.
        setDefinitionDatum(new LinkedHashMap<Object, Object>(sortedMap));

        // Refresh the editor.
        refreshEditor();
    }

    private class PopupMenuListener implements ListPopupMenuListener
    {
        @Override
        public void popupItemSelected(Object item)
        {
            if (item != null)
            {
                addMapValue(null, null);
            }
        }

        @Override
        public void popupItemsSelected(Object[] selectedItems)
        {
            for (Object item : selectedItems)
            {
                popupItemSelected(item);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarList = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jButtonRemove = new javax.swing.JButton();
        jButtonMoveUp = new javax.swing.JButton();
        jButtonMoveDown = new javax.swing.JButton();
        jButtonSortAlphabetic = new javax.swing.JButton();
        jXSearchField = new org.jdesktop.swingx.JXSearchField();
        jScrollPaneMap = new javax.swing.JScrollPane();
        jTableMap = new org.jdesktop.swingx.JXTable();

        setLayout(new java.awt.GridBagLayout());

        jToolBarList.setFloatable(false);
        jToolBarList.setRollover(true);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setToolTipText("Add an element to the map");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonAdd);

        jButtonRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeDocumentIcon.png"))); // NOI18N
        jButtonRemove.setText("Remove");
        jButtonRemove.setToolTipText("Remove selected elements from the map");
        jButtonRemove.setFocusable(false);
        jButtonRemove.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonRemove);

        jButtonMoveUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowUpIcon.png"))); // NOI18N
        jButtonMoveUp.setText("Up");
        jButtonMoveUp.setFocusable(false);
        jButtonMoveUp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveUp.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveUpActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonMoveUp);

        jButtonMoveDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowDownIcon.png"))); // NOI18N
        jButtonMoveDown.setText("Down");
        jButtonMoveDown.setFocusable(false);
        jButtonMoveDown.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveDown.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveDownActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonMoveDown);

        jButtonSortAlphabetic.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/sortAlphabeticIcon.png"))); // NOI18N
        jButtonSortAlphabetic.setText("Sort");
        jButtonSortAlphabetic.setFocusable(false);
        jButtonSortAlphabetic.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSortAlphabetic.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSortAlphabeticActionPerformed(evt);
            }
        });
        jToolBarList.add(jButtonSortAlphabetic);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarList, gridBagConstraints);

        jXSearchField.setMinimumSize(new java.awt.Dimension(200, 25));
        jXSearchField.setPreferredSize(new java.awt.Dimension(200, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jXSearchField, gridBagConstraints);

        jTableMap.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Key", "Value"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }
        });
        jTableMap.setRowHeight(20);
        jScrollPaneMap.setViewportView(jTableMap);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jScrollPaneMap, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        addMapValue();
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveActionPerformed
        removeSelectedMapValues();
    }//GEN-LAST:event_jButtonRemoveActionPerformed

    private void jButtonMoveUpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveUpActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveUpActionPerformed
        moveSelectionUp();
    }//GEN-LAST:event_jButtonMoveUpActionPerformed

    private void jButtonMoveDownActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveDownActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveDownActionPerformed
        moveSelectionDown();
    }//GEN-LAST:event_jButtonMoveDownActionPerformed

    private void jButtonSortAlphabeticActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSortAlphabeticActionPerformed
    {//GEN-HEADEREND:event_jButtonSortAlphabeticActionPerformed
        sortAlphabetic();
    }//GEN-LAST:event_jButtonSortAlphabeticActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonMoveDown;
    private javax.swing.JButton jButtonMoveUp;
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JButton jButtonSortAlphabetic;
    private javax.swing.JScrollPane jScrollPaneMap;
    private org.jdesktop.swingx.JXTable jTableMap;
    private javax.swing.JToolBar jToolBarList;
    private org.jdesktop.swingx.JXSearchField jXSearchField;
    // End of variables declaration//GEN-END:variables
}
