package com.pilog.t8.developer.definitions.projecteditor;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionGroupMetaData;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionLockDetails;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeComparator;
import com.pilog.t8.definition.T8DefinitionTypeComparator.CompareMethod;
import com.pilog.t8.definition.T8DefinitionTypeComparator.CompareType;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationProvider;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationType;
import com.pilog.t8.definition.event.T8DefinitionCacheReloadedEvent;
import com.pilog.t8.definition.event.T8DefinitionContextListener;
import com.pilog.t8.definition.event.T8DefinitionLockedEvent;
import com.pilog.t8.definition.event.T8DefinitionRenamedEvent;
import com.pilog.t8.definition.event.T8DefinitionSavedEvent;
import com.pilog.t8.definition.event.T8DefinitionUnlockedEvent;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionSelectionEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.developer.definitions.T8DefinitionDocumentationFrame;
import com.pilog.t8.developer.definitions.dialog.T8IdentifierSelectionDialog;
import com.pilog.t8.developer.definitions.dialog.T8InformationDialog;
import com.pilog.t8.developer.definitions.history.T8DefinitionHistoryPanel;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import com.pilog.t8.developer.utils.T8DefinitionValidationUtils;
import com.pilog.t8.developer.view.T8DefaultDeveloperView;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.components.list.dialog.SearchableListDialog;
import com.pilog.t8.utilities.components.menu.MenuItem;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.components.messagedialogs.Toast.Style;
import com.pilog.t8.utilities.components.processingdialogs.ProcessingDialog;
import com.pilog.t8.utilities.components.processingdialogs.ThreadTask;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.ExpandVetoException;

/**
 * Displays a composition break-down of a specific definition.
 *
 * @author Bouwer du Preez
 */
public class T8ProjectEditor extends T8DefaultDeveloperView implements T8DeveloperView
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ProjectEditor.class.getName());

    private final T8Context context;
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8DefinitionContext definitionContext;
    private final T8DeveloperView parentView;
    private final FilterKeyListener filterListener;
    private final SelectionListener selectionListener;
    private final ExpansionListener expansionListener;
    private final T8ProjectEditorTreeTable projectTreeTable;
    private final DefinitionContextListener definitionContextListener;
    private final HashMap<T8DefinitionHandle, T8Definition> newDefinitions;
    private final HashSet<T8DefinitionHandle> deletedDefinitions;
    private final HashMap<T8DefinitionHandle, T8Definition> lockedDefinitions;
    private final Map<String, T8DefinitionTypeMetaData> typeMeta;
    private final Map<String, T8DefinitionGroupMetaData> groupMeta;
    private final ProjectSwitchListener projectSwitchListener;
    private T8DeveloperViewFactory viewFactory;
    private T8Definition selectedDefinition;
    private String projectId;

    public T8ProjectEditor(T8DefinitionContext definitionContext, T8DeveloperView parentView, String projectId)
    {
        initComponents();
        this.context = definitionContext.getContext();
        this.clientContext = definitionContext.getClientContext();
        this.sessionContext = definitionContext.getSessionContext();
        this.definitionContext = definitionContext;
        this.typeMeta = new HashMap<>();
        this.groupMeta = new HashMap<>();
        this.parentView = parentView;
        this.viewFactory = parentView != null ? parentView.getViewFactory() : null;
        this.projectId = projectId;
        this.setBorder(new T8ContentHeaderBorder(getHeader()));
        this.definitionContextListener = new DefinitionContextListener();
        this.filterListener = new FilterKeyListener();
        this.selectionListener = new SelectionListener();
        this.expansionListener = new ExpansionListener();
        this.projectTreeTable = new T8ProjectEditorTreeTable(clientContext, this);
        this.projectTreeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.projectTreeTable.clearSelection();
        this.projectTreeTable.addMouseListener(new TableMouseListener());
        this.projectTreeTable.addTreeSelectionListener(selectionListener);
        this.projectTreeTable.addTreeWillExpandListener(expansionListener);
        this.projectTreeTable.setSelectionBackground(LAFConstants.HEADER_BG_COLOR);
        this.projectTreeTable.setSelectionForeground(Color.BLACK);
        this.jScrollPaneDefinitions.setViewportView(projectTreeTable);
        this.newDefinitions = new HashMap<>();
        this.lockedDefinitions = new HashMap<>();
        this.deletedDefinitions = new HashSet<>();
        this.jTextFieldFilter.addKeyListener(filterListener);
        this.projectSwitchListener = new ProjectSwitchListener();
        this.jComboBoxProject.addItemListener(projectSwitchListener);
        refreshFilterComboBox();
        createActionBindings();
    }

    private void setSelectableProjectIds(List<String> newProjectIds)
    {
        jComboBoxProject.removeAllItems();
        jComboBoxProject.addItem(null); // Add null as the first choice.
        Collections.sort(newProjectIds);
        for (String newProjectId : newProjectIds)
        {
            jComboBoxProject.addItem(newProjectId);
        }
    }

    private void refreshFilterComboBox()
    {
        List<String> columnNames;

        columnNames = new LinkedList<>();
        for (String columnName : T8ProjectEditorTreeTableModel.columnNames)
        {
            columnNames.add(columnName);
        }
        columnNames.add("EPIC");
        jComboFilter.setModel(new DefaultComboBoxModel<>(columnNames.toArray(new String[]{})));
    }

    @Override
    public void startComponent()
    {
        definitionContext.addContextListener(definitionContextListener);
        new InitializationLoader().execute();
    }

    @Override
    public void stopComponent()
    {
        definitionContext.removeContextListener(definitionContextListener);
    }

    @Override
    public String getHeader()
    {
        if (projectId != null)
        {
            return projectId;
        }
        else return "Project Manager";
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        parentView.addView(view);
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        parentView.removeView(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
        parentView.notifyDefinitionLinkActivated(event);
    }

    public boolean isDefinitionUpdated(T8DefinitionHandle definitionHandle)
    {
        return lockedDefinitions.containsKey(definitionHandle) || newDefinitions.containsKey(definitionHandle);
    }

    public boolean isDefinitionDeleted(T8DefinitionHandle definitionHandle)
    {
        return deletedDefinitions.contains(definitionHandle);
    }

    public boolean isDefinitionPatched(T8DefinitionHandle definitionHandle)
    {
        T8DefinitionMetaData definitionMetaData;

        definitionMetaData = projectTreeTable.getDefinitionMetaData(definitionHandle);
        if (definitionMetaData != null)
        {
            return definitionMetaData.isPatched() || definitionMetaData.isPatchedContent();
        }
        else throw new IllegalArgumentException("Definition not found: " + definitionHandle);
    }

    @Override
    public boolean canClose()
    {
        if (!newDefinitions.isEmpty() || !lockedDefinitions.isEmpty() || !deletedDefinitions.isEmpty())
        {
            int dialogOption;

            dialogOption = JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "You have unsaved changes on " + getHeader() + " do you want to save these changes?", "Confirmation", JOptionPane.YES_NO_OPTION);
            if (dialogOption != JOptionPane.YES_OPTION)
            {
                // First unlock all locked definitions.
                Iterator<Map.Entry<T8DefinitionHandle, T8Definition>> definitionIter;

                definitionIter = lockedDefinitions.entrySet().iterator();
                while (definitionIter.hasNext())
                {
                    Map.Entry<T8DefinitionHandle, T8Definition> updatedDefinition = definitionIter.next();

                    try
                    {
                        LOGGER.log("Unlocking definition: " + updatedDefinition.getValue() + " (" + updatedDefinition.getKey().getHandleIdentifier() + ")");
                        definitionContext.unlockDefinition(updatedDefinition.getKey(), sessionContext.getUserIdentifier());
                        definitionIter.remove();
                    }
                    catch (Exception e)
                    {
                        LOGGER.log(e);
                    }
                }

                return true;
            }
            else // JOptionPane.YES_OPTION
            {
                saveChanges();
                return true;
            }
        }
        else // No unsaved changes.
        {
            return true;
        }
    }

    protected void setHeaderText(String text)
    {
        this.setBorder(new T8ContentHeaderBorder(text));
    }

    public void setEditable(boolean editable)
    {
        setSaveEnabled(editable);
        setAddEnabled(editable);
        setRemoveEnabled(editable);
        setRenameEnabled(editable);
    }

    public void setSaveEnabled(boolean enabled)
    {
        jButtonSaveDefinitions.setVisible(false);
    }

    public void setAddEnabled(boolean enabled)
    {
        jButtonRemoveDefinition.setVisible(false);
    }

    public void setRemoveEnabled(boolean enabled)
    {
        jButtonRemoveDefinition.setVisible(false);
    }

    public void setRenameEnabled(boolean enabled)
    {
        jButtonRenameDefinition.setVisible(false);
    }

    public void setTitle(String title)
    {
        this.setBorder(new T8ContentHeaderBorder(title));
    }

    private void clearSelection()
    {
        projectTreeTable.clearSelection();
    }

    public void setSelectedDefinition(T8DefinitionHandle definitionHandle)
    {
        projectTreeTable.setSelectedDefinition(definitionHandle);
    }

    protected void setToolBarVisible(boolean visible)
    {
        jToolBarMain.setVisible(visible);
    }

    public void setTypeMeta(Collection<T8DefinitionTypeMetaData> typeMeta)
    {
        this.typeMeta.clear();
        if (typeMeta != null)
        {
            for (T8DefinitionTypeMetaData type : typeMeta)
            {
                this.typeMeta.put(type.getTypeId(), type);
            }
        }
    }

    public void setGroupMeta(Collection<T8DefinitionGroupMetaData> groupMeta)
    {
        this.groupMeta.clear();
        if (groupMeta != null)
        {
            for (T8DefinitionGroupMetaData group : groupMeta)
            {
                this.groupMeta.put(group.getGroupId(), group);
            }
        }
    }

    private void addNewDefinition(T8Definition definition)
    {
        T8DefinitionHandle definitionHandle;

        definitionHandle = definition.getHandle();
        newDefinitions.put(definitionHandle, definition);
        projectTreeTable.addDefinition(definition.getMetaData());
        projectTreeTable.setSelectedDefinition(definitionHandle);
    }

    private void deleteDefinition(T8DefinitionHandle definitionHandle) throws Exception
    {
        newDefinitions.remove(definitionHandle);
        lockedDefinitions.remove(definitionHandle);
        deletedDefinitions.add(definitionHandle);
    }

    protected void clearDefinitionStatus(T8DefinitionHandle definitionHandle)
    {
        lockedDefinitions.remove(definitionHandle);
        deletedDefinitions.remove(definitionHandle);
    }

    protected void filterDefinitions(String filterString)
    {
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    public List<T8DefinitionGroupMetaData> getSelectedDefinitionGroups()
    {
        return projectTreeTable.getSelectedDefinitionGroups();
    }

    public List<T8DefinitionTypeMetaData> getSelectedDefinitionTypes()
    {
        return projectTreeTable.getSelectedDefinitionTypes();
    }

    public List<T8DefinitionHandle> getSelectedDefinitionHandles()
    {
        return projectTreeTable.getSelectedDefinitionHandles();
    }

    public List<T8DefinitionHandle> getAllDefinitionHandles()
    {
        return projectTreeTable.getAllDefinitionHandles();
    }

    @Override
    public void setSelectedDefinition(T8Definition selectedDefinition)
    {
    }

    private void addNewDefinition(String typeId)
    {
        try
        {
            T8DefinitionMetaData metaData;

            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this, definitionContext.getDefinitionTypeMetaData(typeId), null, projectId, null);
            if (metaData != null)
            {
                try
                {
                    T8Definition newDefinition;

                    newDefinition = definitionContext.createNewDefinition(metaData.getId(), typeId);
                    newDefinition.setProjectIdentifier(metaData.getProjectId());
                    addNewDefinition(newDefinition);
                }
                catch (Exception e)
                {
                    LOGGER.log(e);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while fetching meta data for type: " + typeId, e);
        }
    }

    private void transferDefinitionReferences(T8Definition definition)
    {
        try
        {
            String oldIdentifier;
            String newIdentifier;
            List<String> identifierList;

            oldIdentifier = definition.getPublicIdentifier();
            identifierList = definitionContext.getGroupDefinitionIdentifiers(definition.getRootProjectId(), definition.getMetaData().getGroupId());
            newIdentifier = T8IdentifierSelectionDialog.getSelectedIdentifier(clientContext, "Please select the new identifier to which references currently\npointing to '" + oldIdentifier + "' will be transferred.", identifierList);
            if (newIdentifier != null)
            {
                int confirmation;

                confirmation = JOptionPane.showConfirmDialog(this, "All references to '" + oldIdentifier + "' will be changed to '" + newIdentifier + "'.  This operation is not reversable.\nAre you sure you want to continue?", "User Confirmation", JOptionPane.YES_NO_OPTION);
                if (confirmation != JOptionPane.YES_OPTION) return;
                else
                {
                    definitionContext.transferDefinitionReferences(oldIdentifier, newIdentifier);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception dring identifier transfer from: " + definition, e);
            JOptionPane.showMessageDialog(clientContext.getParentWindow(), "All references could not be transferred successfully.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void removeDefinitionReferences(T8DefinitionHandle definitionHandle)
    {
        if (definitionHandle != null)
        {
            int confirmation;

            // Get user confirmation.
            confirmation = JOptionPane.showConfirmDialog(this, "All references to this definition will be removed throughout the system.  Do you want to continue?", "Confirmation", JOptionPane.YES_NO_OPTION);
            if (confirmation == JOptionPane.YES_OPTION)
            {
                try
                {
                    String referenceIdToRemove;

                    // Rename the definition.
                    referenceIdToRemove = definitionHandle.getDefinitionIdentifier();
                    definitionContext.removeDefinitionReferences(referenceIdToRemove, sessionContext.getSessionIdentifier());

                    // Also update local locked definitions to reflect the update.
                    for (T8Definition lockedDefinition : lockedDefinitions.values())
                    {
                        lockedDefinition.removeReferenceId(referenceIdToRemove, true);
                    }

                    // Toast the success.
                    Toast.show("All references to definition '" + referenceIdToRemove + "' removed.");
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while removing definition references to '" + definitionHandle + "'.", e);
                    JOptionPane.showMessageDialog(clientContext.getParentWindow(), "References to Definition '" + definitionHandle + "' could not be removed.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
                }
            }
            else return;
        }
        else
        {
            Toast.makeText(clientContext.getParentWindow(), "No Definition selected.", Style.ERROR).display();
        }
    }

    private void renameSelectedDefinition()
    {
        T8Definition definition;
        String definitionIdentifier;

        definition = selectedDefinition;
        if (definition != null)
        {
            T8DefinitionMetaData metaData;
            int confirmation;

            // Get user confirmation.
            confirmation = JOptionPane.showConfirmDialog(this, "All changes will be saved before you can rename the definition?", "Save Confirmation", JOptionPane.YES_NO_OPTION);
            if (confirmation != JOptionPane.YES_OPTION) return;

            // Rename the definition.
            definitionIdentifier = definition.getIdentifier().replace(definition.getTypeMetaData().getIdPrefix(), "");
            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this, definition.getTypeMetaData(), null, definition.getProjectIdentifier(), definitionIdentifier);
            if (metaData != null)
            {
                try
                {
                    T8DefinitionHandle oldDefinitiondHandle;

                    oldDefinitiondHandle = definition.getHandle();
                    definitionContext.renameDefinition(definition, metaData.getId());

                    if (lockedDefinitions.containsKey(oldDefinitiondHandle))
                    {
                        lockedDefinitions.remove(oldDefinitiondHandle);
                        lockedDefinitions.put(metaData.getDefinitionHandle(), definition);
                    }

                    refresh();
                    setSelectedDefinition(metaData.getDefinitionHandle());
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while renaming definition '" + definition.getIdentifier() + "' to '" + metaData.getId() + "'.", e);
                    JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + definition + "' could not be renamed.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else
        {
            Toast.makeText(clientContext.getParentWindow(), "No Definition selected.", Style.ERROR).display();
        }
    }

    private void displayAddDefinitionPopupMenu(final Component source, int x, int y)
    {
        try
        {
            SearchableListDialog<T8DefinitionTypeMetaData> popup;
            T8DefinitionTypeMetaData selectedDefinitionType;
            List<T8DefinitionTypeMetaData> typeMetaDataList;
            Iterator<T8DefinitionTypeMetaData> iterator;

            // Get the list of definition type meta data and sort it.
            typeMetaDataList = definitionContext.getAllDefinitionTypeMetaData();
            Collections.sort(typeMetaDataList, new T8DefinitionTypeComparator(CompareMethod.LEXICOGRAPHIC, CompareType.DISPLAY_NAME));

            // Remove resource deifnitions.
            iterator = typeMetaDataList.iterator();
            while (iterator.hasNext())
            {
                if (iterator.next().getDefinitionLevel() == T8Definition.T8DefinitionLevel.RESOURCE)
                {
                    iterator.remove();
                }
            }

            // Create the popup.
            popup = new SearchableListDialog<T8DefinitionTypeMetaData>(SwingUtilities.windowForComponent(this), typeMetaDataList);
            popup.setCellRenderer(new DefaultListCellRenderer()
            {
                @Override
                public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
                {
                    return super.getListCellRendererComponent(list, ((T8DefinitionTypeMetaData)value).getDisplayName(), index, isSelected, cellHasFocus);
                }
            });

            // Show the popup.
            popup.setVisible(true);
            selectedDefinitionType = popup.getSelectedValue();
            if (selectedDefinitionType != null)
            {
                addNewDefinition(selectedDefinitionType.getTypeId());
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while fetching type meta data.", e);
        }
    }

    private void setProject(String projectId)
    {
        // Set the selected project on the switch combo box.
        projectSwitchListener.setEnabled(false);
        jComboBoxProject.setSelectedItem(projectId);
        projectSwitchListener.setEnabled(true);

        // Set the newly selected project and load it into the view.
        this.projectId = projectId;
        refresh();

        // Fire an event to notify listeners that the header of this view has changed.
        fireHeaderChangedEvent(getHeader());
    }

    private void refresh()
    {
        if (projectId != null)
        {
            try
            {
                // First unlock all locked definitions.
                for (T8Definition updatedDefinition : lockedDefinitions.values())
                {
                    try
                    {
                        LOGGER.log("Unlocking definition: " + updatedDefinition + " (" + updatedDefinition.getHandle().getHandleIdentifier() + ")");
                        definitionContext.unlockDefinition(updatedDefinition.getHandle(), sessionContext.getUserIdentifier());
                        lockedDefinitions.remove(updatedDefinition.getHandle());
                    }
                    catch (Exception e)
                    {
                        LOGGER.log(e);
                    }
                }

                // Now reset the state.
                newDefinitions.clear();
                lockedDefinitions.clear();
                deletedDefinitions.clear();

                // Load the project.
                new ProjectLoader(projectId).execute();
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }
        }
    }

    private void saveChanges()
    {
        ArrayList<String> definitionsToValidate;
        ArrayList<T8Definition> changesToSave;
        ArrayList<T8DefinitionHandle> definitionsToDelete;
        boolean errorEncountered;

        // Clear the selection, to ensure that all changes to the last edited definition are committed.
        clearSelection();
        errorEncountered = false;

        // Need to keep track of all the definitions which will be validated if enabled
        definitionsToValidate = new ArrayList<>();

        // Save all new definitions.
        changesToSave = new ArrayList<>(newDefinitions.values());
        for (T8Definition newDefinition : changesToSave)
        {
            try
            {
                LOGGER.log("Saving new definition: " + newDefinition + " (" + newDefinition.getHandle().getHandleIdentifier() + ")");
                LOGGER.log("Meta Data Before Save: " + newDefinition.getMetaData());
                definitionContext.saveDefinition(newDefinition, sessionContext.getSessionIdentifier());
                // We can only validate the definition if it was saved
                definitionsToValidate.add(newDefinition.getIdentifier());
                LOGGER.log("Meta Data After Save: " + newDefinition.getMetaData());
                newDefinitions.remove(newDefinition.getHandle());
            }
            catch (Exception e)
            {
                errorEncountered = true;
                LOGGER.log("Failed to save definition.", e);
                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "New Definition '" + newDefinition + "' could not be updated.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                {
                    return;
                }
            }
        }

        // Save all updated definitions.
        changesToSave = new ArrayList<>(lockedDefinitions.values());
        for (T8Definition updatedDefinition : changesToSave)
        {
            try
            {
                LOGGER.log("Saving updated definition: " + updatedDefinition + " (" + updatedDefinition.getHandle().getHandleIdentifier() + ")");
                LOGGER.log("Meta Data Before Save: " + updatedDefinition.getMetaData());
                definitionContext.unlockDefinition(updatedDefinition, sessionContext.getUserIdentifier());
                LOGGER.log("Meta Data After Save: " + updatedDefinition.getMetaData());
                lockedDefinitions.remove(updatedDefinition.getHandle());
            }
            catch (Exception e)
            {
                errorEncountered = true;
                LOGGER.log(e);
                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + updatedDefinition + "' could not be updated.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                {
                    return;
                }
            }

            definitionsToValidate.add(updatedDefinition.getIdentifier());
        }

        // Delete definitions deleted on UI from server.
        definitionsToDelete = new ArrayList<>(deletedDefinitions);
        for (T8DefinitionHandle definitionToDelete : definitionsToDelete)
        {
            try
            {
                definitionContext.deleteDefinition(definitionToDelete, true, null);
                deletedDefinitions.remove(definitionToDelete);
                projectTreeTable.removeDefinition(definitionToDelete);
            }
            catch (Exception e)
            {
                errorEncountered = true;
                LOGGER.log(e);
                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + definitionToDelete + "' could not be deleted.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                {
                    return;
                }
            }
        }

        // Show the result message.
        if (!errorEncountered)
        {
            Toast.makeText(clientContext.getParentWindow(), "Changes Saved Successfully.",Style.SUCCESS).display();
        }

        // Repaint the UI to reflect changes.
        refresh();
    }

    private void deleteSelectedDefinitions()
    {
        List<T8DefinitionHandle> selectedDefinitionHandles;

        // Delete the definitions.
        selectedDefinitionHandles = getSelectedDefinitionHandles();
        if (selectedDefinitionHandles.isEmpty()) Toast.makeText(clientContext.getParentWindow(), "No Definition(s) selected.", Style.ERROR).display();
        for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
        {
            try
            {
                deleteDefinition(selectedDefinitionHandle);
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }
        }

        // Refresh the UI.
        projectTreeTable.repaint();
        Toast.makeText(clientContext.getParentWindow(), selectedDefinitionHandles.size() + " Definitions Marked for Deletion.", Style.ERROR).display();
    }

    private class FilterKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                filterDefinitions(jTextFieldFilter.getText());
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    private void unlockDefinition(T8DefinitionHandle selectedDefinitionHandle)
    {
        try
        {
            definitionContext.unlockDefinition(selectedDefinitionHandle, sessionContext.getUserIdentifier());
            lockedDefinitions.remove(selectedDefinitionHandle);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while unlocking definition: " + selectedDefinitionHandle, e);
            JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be unlocked.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void getDefinitionLockDetails(T8DefinitionHandle selectedDefinitionHandle)
    {
        try
        {
            T8DefinitionLockDetails lockDetails;

            lockDetails = definitionContext.getDefinitionLockDetails(selectedDefinitionHandle);
            if (lockDetails != null)
            {
                StringBuffer message;

                message = new StringBuffer();
                message.append("Lock details for Definition '");
                message.append(selectedDefinitionHandle.toString()).append("'");
                message.append("\nLocked: ").append(lockDetails.isLocked());
                message.append("\nUser: ").append(lockDetails.getUserIdentifier());
                message.append("\nProfile: ").append(lockDetails.getUserProfileIdentifier());
                JOptionPane.showMessageDialog(clientContext.getParentWindow(), message, "Lock Details", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while fetching lock details for definition: " + selectedDefinitionHandle, e);
            JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Lock details for Definition '" + selectedDefinitionHandle + "' could not be fetched.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void editDefinition(T8DefinitionHandle selectedDefinitionHandle)
    {
        // When a definition is edited, we set the editable flag and fire a definition selection change.
        try
        {
            T8Definition lockedDefinition;

            lockedDefinition = definitionContext.lockDefinition(selectedDefinitionHandle, sessionContext.getUserIdentifier());
            lockedDefinitions.put(selectedDefinitionHandle, lockedDefinition);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while locking definition: " + selectedDefinitionHandle, e);
            JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be locked.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Handle the switch from non-editable to editable as a selection change
        // as we are actually switching from a non-editable version to an
        // editable version of the definition.
        handleDefinitionSelectionChange(selectedDefinitionHandle);
    }

    private void handleDefinitionSelectionChange(T8DefinitionHandle selectedDefinitionHandle)
    {
        T8DefinitionSelectionEvent event;

        // Get the selected definition.
        if (selectedDefinitionHandle == null)
        {
            selectedDefinition = null;
        }
        else if (newDefinitions.containsKey(selectedDefinitionHandle))
        {
            selectedDefinition = newDefinitions.get(selectedDefinitionHandle);
        }
        else if (lockedDefinitions.containsKey(selectedDefinitionHandle))
        {
            selectedDefinition = lockedDefinitions.get(selectedDefinitionHandle);
        }
        else
        {
            try
            {
                selectedDefinition = definitionContext.getRawDefinition(selectedDefinitionHandle);
                LOGGER.log("Loaded definition: " + selectedDefinition);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading definition: " + selectedDefinitionHandle, e);
                JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be loaded.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        // Fire the selection event.
        event = new T8DefinitionSelectionEvent(this, selectedDefinition);
        for (T8DefinitionViewListener listener : definitionListeners)
        {
            listener.selectionChanged(event);
        }
    }

    private void displayPopupMenu(int x, int y)
    {
        final List<T8DefinitionHandle> selectedDefinitionHandles;
        final List<T8DefinitionGroupMetaData> selectedDefinitionGroups;
        final List<T8DefinitionTypeMetaData> selectedDefinitionTypes;

        selectedDefinitionGroups = getSelectedDefinitionGroups();
        selectedDefinitionTypes = getSelectedDefinitionTypes();
        selectedDefinitionHandles = getSelectedDefinitionHandles();
        if (!selectedDefinitionHandles.isEmpty())
        {
            JPopupMenu popupMenu;
            JMenuItem menuItem;

            // Create the ActionListener for the menu items.
            ActionListener menuListener = (ActionEvent event) ->
            {
                JMenuItem menuItem1;
                String code;

                menuItem1 = (JMenuItem)event.getSource();
                code = menuItem1.getName();
                if (code.equals("EDIT_DEFINITION"))
                {
                    T8DefinitionHandle selectedDefinitionHandle;

                    selectedDefinitionHandle = selectedDefinitionHandles.get(0);
                    editDefinition(selectedDefinitionHandle);
                }
                else if (code.equals("GET_DEFINITION_LOCK_DETAILS"))
                {
                    T8DefinitionHandle selectedDefinitionHandle;

                    selectedDefinitionHandle = selectedDefinitionHandles.get(0);
                    getDefinitionLockDetails(selectedDefinitionHandle);
                }
                else if (code.equals("UNLOCK_DEFINITION"))
                {
                    T8DefinitionHandle selectedDefinitionHandle;

                    selectedDefinitionHandle = selectedDefinitionHandles.get(0);
                    unlockDefinition(selectedDefinitionHandle);
                }
                else if (code.equals("RENAME_DEFINITION"))
                {
                    renameSelectedDefinition();
                }
                else if (code.equals("REMOVE_DEFINITION_REFERENCES"))
                {
                    removeDefinitionReferences(selectedDefinitionHandles.get(0));
                }
                else if (code.equals("TRANSFER_DEFINITION_REFERENCES"))
                {
                    transferDefinitionReferences(selectedDefinition);
                }
                else if (code.equals("DEFINITION_COMPOSITION"))
                {
                    if (selectedDefinition != null)
                    {
                        parentView.addView(viewFactory.createDefinitionCompositionView(parentView, selectedDefinition.getRootProjectId(), selectedDefinition.getIdentifier()));
                    }
                }
                else if (code.equals("FIND_DEFINITION_USAGES"))
                {
                    if (selectedDefinition != null)
                    {
                        parentView.addView(viewFactory.createDefinitionUsagesManager(parentView, selectedDefinition));
                    }
                }
                else if (code.equals("MOVE_DEFINITIONS_TO_PROJECT"))
                {
                    moveSelectedDefinitionsToProject();
                }
                else if (code.equals("COPY_DEFINITIONS_TO_PROJECT"))
                {
                    copySelectedDefinitionsToProject();
                }
            };

            // Create a new popup menu.
            popupMenu = new JPopupMenu();

            menuItem = new JMenuItem("Edit Selected Definition");
            menuItem.setName("EDIT_DEFINITION");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Definition Composition");
            menuItem.setName("DEFINITION_COMPOSITION");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Get Definition Lock Details");
            menuItem.setName("GET_DEFINITION_LOCK_DETAILS");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Unlock Definition");
            menuItem.setName("UNLOCK_DEFINITION");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Rename Definition");
            menuItem.setName("RENAME_DEFINITION");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Remove Definition References");
            menuItem.setName("REMOVE_DEFINITION_REFERENCES");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Transfer Definition References");
            menuItem.setName("TRANSFER_DEFINITION_REFERENCES");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Find Usages");
            menuItem.setName("FIND_DEFINITION_USAGES");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Move Selected Definitions to Project");
            menuItem.setName("MOVE_DEFINITIONS_TO_PROJECT");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Copy Selected Definitions to Project");
            menuItem.setName("COPY_DEFINITIONS_TO_PROJECT");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            // Display the newly constructed popup menu.
            popupMenu.show(projectTreeTable, x, y);
        }
        else if (!selectedDefinitionGroups.isEmpty())
        {
            T8DefinitionGroupMetaData selectedGroup;
            JPopupMenu popupMenu;
            String groupId;

            // Get the selected group id.
            selectedGroup = selectedDefinitionGroups.get(0);
            groupId = selectedGroup.getGroupId();

            // Create the ActionListener for the menu items.
            ActionListener menuListener = (ActionEvent event) ->
            {
                MenuItem menuItem;
                String code;

                menuItem = (MenuItem)event.getSource();
                code = menuItem.getCode();
                if (code.equals("ADD_DEFINITION"))
                {
                    addNewDefinition((String)menuItem.getValue());
                }
            };

            // Create a new popup menu.
            popupMenu = new JPopupMenu();

            // Add an option for all types belonging to the selected group.
            for (T8DefinitionTypeMetaData typeMetaData : typeMeta.values())
            {
                // Make sure to exclude all levels except PROJECT.
                if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.PROJECT)
                {
                    // Only add types belonging to the selected group.
                    if (typeMetaData.getGroupId().equals(groupId))
                    {
                        MenuItem menuItem;

                        menuItem = new MenuItem("Add " + typeMetaData.getDisplayName(), "ADD_DEFINITION", typeMetaData.getTypeId());
                        menuItem.addActionListener(menuListener);
                        popupMenu.add(menuItem);
                    }
                }
            }

            // Display the newly constructed popup menu.
            popupMenu.show(projectTreeTable, x, y);
        }
        else if (!selectedDefinitionTypes.isEmpty())
        {
            T8DefinitionTypeMetaData selectedType;
            JPopupMenu popupMenu;
            JMenuItem menuItem;

            selectedType = selectedDefinitionTypes.get(0);

            // Create the ActionListener for the menu items.
            ActionListener menuListener = (ActionEvent event) ->
            {
                JMenuItem menuItem1;
                String code;

                menuItem1 = (JMenuItem)event.getSource();
                code = menuItem1.getName();
                if (code.equals("ADD_DEFINITION"))
                {
                    addNewDefinition(selectedType.getTypeId());
                }
            };

            // Create a new popup menu.
            popupMenu = new JPopupMenu();

            menuItem = new JMenuItem("Add " + selectedType.getDisplayName());
            menuItem.setName("ADD_DEFINITION");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            // Display the newly constructed popup menu.
            popupMenu.show(projectTreeTable, x, y);
        }
    }

    private void moveSelectedDefinitionsToProject()
    {
        if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Unsaved changes to selected definitions will be lost.  Proceed?", "Operation Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            try
            {
                List<T8DefinitionHandle> selectedDefinitionHandles;

                selectedDefinitionHandles = getSelectedDefinitionHandles();
                if (selectedDefinitionHandles.size() > 0)
                {
                    String newProjectId;

                    newProjectId = T8IdentifierSelectionDialog.getSelectedIdentifier(clientContext, "Please select a project to which the definitions will be moved.", definitionContext.getGroupDefinitionIdentifiers(null, T8ProjectDefinition.GROUP_IDENTIFIER));
                    if (newProjectId != null)
                    {
                        for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
                        {
                            try
                            {
                                LOGGER.log("Moving Definition '" + selectedDefinitionHandle + "' to project: " + newProjectId);
                                definitionContext.moveDefinitionToProject(selectedDefinitionHandle.getProjectIdentifier(), selectedDefinitionHandle.getDefinitionIdentifier(), newProjectId);
                                clearDefinitionStatus(selectedDefinitionHandle);
                                projectTreeTable.removeDefinition(selectedDefinitionHandle);
                            }
                            catch (Exception e)
                            {
                                LOGGER.log("Exception while moving Definition '" + selectedDefinitionHandle + "' to project: " + newProjectId, e);
                                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be moved to project '" + newProjectId + "'.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                                {
                                    return;
                                }
                            }
                        }
                    }

                    // Notify the user of the successful execution.
                    Toast.makeText(clientContext.getParentWindow(), "Definitions Moved Successfully", Style.SUCCESS).display();
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while moving definitions to new project.", e);
            }
        }
    }

    private void copySelectedDefinitionsToProject()
    {
        if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Unsaved changes to selected definitions will be lost.  Proceed?", "Operation Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            try
            {
                List<T8DefinitionHandle> selectedDefinitionHandles;

                selectedDefinitionHandles = getSelectedDefinitionHandles();
                if (selectedDefinitionHandles.size() > 0)
                {
                    String projectIdentifier;

                    projectIdentifier = T8IdentifierSelectionDialog.getSelectedIdentifier(clientContext, "Please select a project to which the definitions will be copied.", definitionContext.getGroupDefinitionIdentifiers(null, T8ProjectDefinition.GROUP_IDENTIFIER));
                    if (projectIdentifier != null)
                    {
                        for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
                        {
                            try
                            {
                                LOGGER.log("Copying Definition '" + selectedDefinitionHandle + "' to project: " + projectIdentifier);
                                definitionContext.copyDefinitionToProject(selectedDefinitionHandle.getProjectIdentifier(), selectedDefinitionHandle.getDefinitionIdentifier(), projectIdentifier);
                                clearDefinitionStatus(selectedDefinitionHandle);
                            }
                            catch (Exception e)
                            {
                                LOGGER.log("Exception while copying Definition '" + selectedDefinitionHandle + "' to project: " + projectIdentifier, e);
                                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be copied to project '" + projectIdentifier + "'.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                                {
                                    return;
                                }
                            }
                        }
                    }

                    // Notify the user of the successful execution.
                    Toast.makeText(clientContext.getParentWindow(), "Definitions Copied Successfully", Style.SUCCESS).display();
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while moving definitions to new project.", e);
            }
        }
    }

    private void copyDefinition()
    {
        if (selectedDefinition != null)
        {
            T8DefinitionMetaData metaData;
            String suffix;

            suffix = selectedDefinition.getIdentifier().replace(selectedDefinition.getTypeMetaData().getIdPrefix(), "") + "_COPY";

            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this.getRootPane(), selectedDefinition.getTypeMetaData(), null, selectedDefinition.getProjectIdentifier(), suffix);

            if (metaData != null)
            {
                try
                {
                    T8Definition newDefinition;

                    newDefinition = definitionContext.copyDefinition(selectedDefinition, metaData.getId());
                    newDefinition = T8DefinitionUtilities.clearAuditingData(newDefinition);
                    newDefinition.setProjectIdentifier(metaData.getProjectId());
                    addNewDefinition(newDefinition);
                }
                catch (Exception e)
                {
                    LOGGER.log(e);
                }
            }
        }
        else Toast.makeText(clientContext.getParentWindow(), "No definition selected.", Style.ERROR).display();
    }

    private void applyPatchChanges()
    {
        if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "All patched changes will be saved for selected definitions.  Proceed?", "Operation Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            try
            {
                List<T8DefinitionHandle> selectedDefinitionHandles;

                selectedDefinitionHandles = getSelectedDefinitionHandles();
                if (selectedDefinitionHandles.size() > 0)
                {
                    for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
                    {
                        if (isDefinitionPatched(selectedDefinitionHandle))
                        {
                            try
                            {
                                T8Definition definitionToUpdate;

                                // In order to apply patch changes, we simply need to load the definition and save it again.
                                LOGGER.log("Applying patch changes to Definition '" + selectedDefinitionHandle);
                                definitionToUpdate = definitionContext.lockDefinition(selectedDefinitionHandle, sessionContext.getUserIdentifier());
                                definitionContext.unlockDefinition(definitionToUpdate, sessionContext.getUserIdentifier());
                                clearDefinitionStatus(selectedDefinitionHandle);
                            }
                            catch (Exception e)
                            {
                                LOGGER.log("Exception while applying patch changes to Definition '" + selectedDefinitionHandle + "'.", e);
                                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be updated.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                                {
                                    return;
                                }
                            }
                        }
                    }

                    // Notify the user of the successful execution.
                    Toast.makeText(clientContext.getParentWindow(), "Patch Changes Applied Successfully", Style.SUCCESS).display();
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while applying patch changes.", e);
            }
        }
    }

    private void showDocumentationPopupMenu(JComponent source, Point location)
    {
        try
        {
            final List<T8DefinitionHandle> selectedDefinitionHandles;

            // Get the selected definition handles.
            selectedDefinitionHandles = getSelectedDefinitionHandles();
            if (selectedDefinitionHandles.size() > 0)
            {
                final T8DefinitionDocumentationProvider documentationProvider;
                T8Definition firstDefinition;
                JPopupMenu popupMenu;
                JMenuItem menuItem;

                // Get the documentation provider from the definition.
                firstDefinition = definitionContext.getRawDefinition(selectedDefinitionHandles.get(0));
                documentationProvider = firstDefinition.createDocumentationProvider(definitionContext);

                // Create the ActionListener for the menu items.
                ActionListener menuListener = new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent event)
                    {
                        JMenuItem menuItem;
                        String identifier;

                        menuItem = (JMenuItem)event.getSource();
                        identifier = menuItem.getName();
                        if (identifier != null)
                        {
                            T8DefinitionDocumentationFrame documentationFrame;
                            List<T8Definition> selectedDefinitions;

                            selectedDefinitions = new ArrayList<>();
                            for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
                            {
                                try
                                {
                                    selectedDefinitions.add(definitionContext.getRawDefinition(selectedDefinitionHandle));
                                }
                                catch (Exception e)
                                {
                                    LOGGER.log("Exception while generating documentation for selected definition: " + selectedDefinitionHandle, e);
                                }
                            }

                            documentationFrame = new T8DefinitionDocumentationFrame(context, selectedDefinitions, identifier);
                            documentationFrame.setVisible(true);
                        }
                    }
                };

                // Create a new popup menu.
                popupMenu = new JPopupMenu();
                for (T8DefinitionDocumentationType documentationType : documentationProvider.getDocumentationTypes())
                {
                    menuItem = new JMenuItem(documentationType.getDisplayName());
                    menuItem.setName(documentationType.getIdentifier());
                    menuItem.addActionListener(menuListener);
                    popupMenu.add(menuItem);
                }

                // Display the newly constructed popup menu.
                popupMenu.show(source, location.x, location.y);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while generating documentation for selected definitions.", e);
        }
    }

    private void validateDefinitions()
    {
        List<String> selectedDefinitionIds;

        selectedDefinitionIds = new ArrayList<>();

        try
        {
            T8DefinitionValidationUtils.validateDefinitions(SwingUtilities.windowForComponent(this), this, context, selectedDefinitionIds);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while validating definitions: " + selectedDefinitionIds, e);
            JOptionPane.showMessageDialog(this, "An unexpected exception occurred.", "Unexpected Exception", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void validateDefinitionsAsynchronously(final boolean checkAll)
    {
        List<T8DefinitionValidationError> validationErrors;

        try
        {
            validationErrors = (List<T8DefinitionValidationError>)ProcessingDialog.doTask(new ThreadTask()
            {
                @Override
                public List<T8DefinitionValidationError> doTask() throws Exception
                {
                    return validateDefinitions(checkAll);
                }
            }, "Checking Usages", 250, 300, 100, javax.swing.SwingUtilities.getWindowAncestor(this));

            if (!validationErrors.isEmpty())
            {
                T8InformationDialog informationDialog;

                informationDialog = new T8InformationDialog(clientContext.getParentWindow(), this, T8DefinitionValidationUtils.createReport(validationErrors));
                informationDialog.setVisible(true);
            }
            else Toast.show("All definitions have usages", Style.SUCCESS);
        }
        catch (Throwable thr)
        {
            LOGGER.log("Failure to complete usage checks", thr);
        }
    }

    private List<T8DefinitionValidationError> validateDefinitions(boolean checkAll)
    {
        List<T8DefinitionValidationError> validationErrors;
        List<T8DefinitionHandle> definitionHandles;

        definitionHandles = checkAll ? getAllDefinitionHandles() : getSelectedDefinitionHandles();
        validationErrors = new ArrayList<>();

        for (T8DefinitionHandle definitionHandle : definitionHandles)
        {
            if (definitionHandle != null)
            {
                Toast.show("Checking " + definitionHandle.getDefinitionIdentifier(), Style.NORMAL, 500);

                try
                {
                    List<T8DefinitionHandle> usages;

                    usages = definitionContext.findDefinitionsByIdentifier(definitionHandle.getDefinitionIdentifier(), true);
                    if (usages.size() == 1)
                    {
                        validationErrors.add(new T8DefinitionValidationError(definitionHandle.getProjectIdentifier(), definitionHandle.getDefinitionIdentifier(), "", "Definition has no usages", T8DefinitionValidationError.ErrorType.WARNING));
                    }
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to load raw definition " + definitionHandle.getDefinitionIdentifier(), ex);
                }
            }
        }

        return validationErrors;
    }

    private void createActionBindings()
    {
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("control S"), "Save_Changes");

        getActionMap().put("Save_Changes", new AbstractAction("Save_Changes")
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (jButtonSaveDefinitions.isEnabled() && jButtonSaveDefinitions.isVisible())
                {
                    saveChanges();
                }
            }
        });
    }

    private class SelectionListener implements TreeSelectionListener
    {
        @Override
        public void valueChanged(TreeSelectionEvent e)
        {
            T8DefinitionHandle selectedDefinitionHandle;

            selectedDefinitionHandle = projectTreeTable.getSelectedDefinitionHandle();
            handleDefinitionSelectionChange(selectedDefinitionHandle);
        }
    }

    private class ExpansionListener implements TreeWillExpandListener
    {
        @Override
        public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException
        {
        }

        @Override
        public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException
        {
        }
    }

    private class TableMouseListener implements MouseListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopupMenu(e.getX(), e.getY());
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopupMenu(e.getX(), e.getY());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopupMenu(e.getX(), e.getY());
            }
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    private class DefinitionContextListener implements T8DefinitionContextListener
    {
        @Override
        public void definitionSaved(T8DefinitionSavedEvent event)
        {
        }

        @Override
        public void definitionLocked(T8DefinitionLockedEvent event)
        {
        }

        @Override
        public void definitionUnlocked(T8DefinitionUnlockedEvent event)
        {
        }

        @Override
        public void definitionCacheReloaded(T8DefinitionCacheReloadedEvent event)
        {
        }

        @Override
        public void definitionRenamed(T8DefinitionRenamedEvent event)
        {
            List<T8DefinitionHandle> affectedDefinitions;
            T8DefinitionHandle affectedDefinitionHandle;
            T8Definition affectedRootDefinition;

            // Get the event parameters.
            affectedRootDefinition = event.getDefinition().getRootDefinition();
            affectedDefinitions = event.getAffectedDefinitions();
            affectedDefinitionHandle = affectedRootDefinition.getHandle();
            affectedDefinitionHandle.setDefinitionIdentifier(T8IdentifierUtilities.getGlobalIdentifierPart(event.getOldIdentifier()));

            // If only one definition was affected, it means that the rename was local and we don't need to refresh the selected definition.
            if (affectedDefinitions.size() > 1)
            {
                // If more than one definitions were affected, and the affected definition was a new addition, it will now have been saved.
                newDefinitions.remove(affectedDefinitionHandle);
            }
        }
    }

    private class InitializationLoader extends SwingWorker<Void, Void>
    {
        private List<String> projectIds;

        public InitializationLoader()
        {
        }

        @Override
        protected Void doInBackground() throws Exception
        {
            List<T8DefinitionTypeMetaData> typeMetaData;
            List<T8DefinitionGroupMetaData> groupMetaData;

            // Load the meta data of the project definition.
            projectIds = definitionContext.getGroupDefinitionIdentifiers(null, T8ProjectDefinition.GROUP_IDENTIFIER);

            // Load all definition type meta data;
            typeMetaData = definitionContext.getAllDefinitionTypeMetaData();
            setTypeMeta(typeMetaData);
            projectTreeTable.setTypeMeta(typeMetaData);

            // Load all definition group meta data;
            groupMetaData = definitionContext.getAllDefinitionGroupMetaData();
            setGroupMeta(groupMetaData);
            projectTreeTable.setGroupMeta(groupMetaData);
            return null;
        }

        @Override
        protected void done()
        {
            try
            {
                get();
                setSelectableProjectIds(projectIds);
                setProject(projectId);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading project: " + projectId, e);
            }
        }
    }

    private class ProjectLoader extends SwingWorker<List<T8DefinitionMetaData>, Void>
    {
        private T8DefinitionMetaData projectMeta;
        private final String projectId;

        public ProjectLoader(String projectId)
        {
            this.projectId = projectId;
        }

        @Override
        protected List<T8DefinitionMetaData> doInBackground() throws Exception
        {
            // Load the meta data of the project definition.
            projectMeta = definitionContext.getDefinitionMetaData(null, projectId);

            // Load the project content definition meta data.
            return definitionContext.getProjectDefinitionMetaData(projectId);
        }

        @Override
        protected void done()
        {
            try
            {
                List<T8DefinitionMetaData> contentMeta;

                contentMeta = get();
                if (contentMeta != null)
                {
                    projectTreeTable.setProject(projectMeta, contentMeta);
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading project: " + projectId, e);
            }
        }
    }

    private class ProjectSwitchListener implements ItemListener
    {
        private boolean enabled;

        public ProjectSwitchListener()
        {
            this.enabled = true;
        }

        public void setEnabled(boolean enabled)
        {
            this.enabled = enabled;
        }

        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (enabled)
            {
                if (e.getStateChange() == ItemEvent.SELECTED)
                {
                    if (canClose())
                    {
                        setProject((String)e.getItem());
                    }
                }
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPopupMenuCheckUsages = new javax.swing.JPopupMenu();
        jMenuItemSelected = new javax.swing.JMenuItem();
        jMenuItemAll = new javax.swing.JMenuItem();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonAddDefinition = new javax.swing.JButton();
        jButtonRemoveDefinition = new javax.swing.JButton();
        jButtonRenameDefinition = new javax.swing.JButton();
        jButtonCopyDefinition = new javax.swing.JButton();
        jButtonRefresh = new javax.swing.JButton();
        jButtonSaveDefinitions = new javax.swing.JButton();
        jButtonApplyPatch = new javax.swing.JButton();
        jButtonDocumentation = new javax.swing.JButton();
        jButtonValidateDefinition = new javax.swing.JButton();
        jButtonCheckUsages = new javax.swing.JButton();
        jButtonhistory = new javax.swing.JButton();
        jPanelFilter = new javax.swing.JPanel();
        jComboBoxProject = new javax.swing.JComboBox<>();
        jComboFilter = new javax.swing.JComboBox();
        jTextFieldFilter = new org.jdesktop.swingx.JXTextField("Filter");
        jScrollPaneDefinitions = new javax.swing.JScrollPane();

        jMenuItemSelected.setText("Check Selected");
        jMenuItemSelected.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSelectedActionPerformed(evt);
            }
        });
        jPopupMenuCheckUsages.add(jMenuItemSelected);

        jMenuItemAll.setText("Check All (Long Running)");
        jMenuItemAll.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAllActionPerformed(evt);
            }
        });
        jPopupMenuCheckUsages.add(jMenuItemAll);

        setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonAddDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAddDefinition.setToolTipText("Add New Item");
        jButtonAddDefinition.setFocusable(false);
        jButtonAddDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonAddDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAddDefinition);

        jButtonRemoveDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeDocumentIcon.png"))); // NOI18N
        jButtonRemoveDefinition.setToolTipText("Delete Selected Items");
        jButtonRemoveDefinition.setFocusable(false);
        jButtonRemoveDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRemoveDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemoveDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRemoveDefinition);

        jButtonRenameDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/renameDocumentIcon.png"))); // NOI18N
        jButtonRenameDefinition.setToolTipText("Rename Definition");
        jButtonRenameDefinition.setFocusable(false);
        jButtonRenameDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRenameDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRenameDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRenameDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRenameDefinition);

        jButtonCopyDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/documentCopyIcon.png"))); // NOI18N
        jButtonCopyDefinition.setToolTipText("Copy Definition");
        jButtonCopyDefinition.setFocusable(false);
        jButtonCopyDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCopyDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCopyDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCopyDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCopyDefinition);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        jButtonSaveDefinitions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/saveChangesIcon.png"))); // NOI18N
        jButtonSaveDefinitions.setToolTipText("Save Changes");
        jButtonSaveDefinitions.setFocusable(false);
        jButtonSaveDefinitions.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSaveDefinitions.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSaveDefinitions.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSaveDefinitionsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSaveDefinitions);

        jButtonApplyPatch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/bandaidArrowIcon.png"))); // NOI18N
        jButtonApplyPatch.setToolTipText("Apply Patch Changes");
        jButtonApplyPatch.setFocusable(false);
        jButtonApplyPatch.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonApplyPatch.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonApplyPatch.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonApplyPatchActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonApplyPatch);

        jButtonDocumentation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/informationIcon.png"))); // NOI18N
        jButtonDocumentation.setToolTipText("Definition Documentation");
        jButtonDocumentation.setFocusable(false);
        jButtonDocumentation.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonDocumentation.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDocumentation.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDocumentationActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDocumentation);

        jButtonValidateDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/bugExclamationIcon.png"))); // NOI18N
        jButtonValidateDefinition.setToolTipText("Validate Selected Definitions");
        jButtonValidateDefinition.setFocusable(false);
        jButtonValidateDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonValidateDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonValidateDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonValidateDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonValidateDefinition);

        jButtonCheckUsages.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/usagesIcon.png"))); // NOI18N
        jButtonCheckUsages.setToolTipText("Check Usages");
        jButtonCheckUsages.setFocusable(false);
        jButtonCheckUsages.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCheckUsages.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCheckUsages.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCheckUsagesActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCheckUsages);

        jButtonhistory.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/direction.png"))); // NOI18N
        jButtonhistory.setToolTipText("History");
        jButtonhistory.setFocusable(false);
        jButtonhistory.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonhistory.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonhistory.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonhistoryActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonhistory);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarMain, gridBagConstraints);

        jPanelFilter.setLayout(new java.awt.GridBagLayout());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanelFilter.add(jComboBoxProject, gridBagConstraints);

        jComboFilter.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboFilterItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 1);
        jPanelFilter.add(jComboFilter, gridBagConstraints);

        jTextFieldFilter.setToolTipText("<html>\n<p>\n1)Type to search across all fields and press enter.<br>\n2)To search a specific field type field name and \":\" and then search query e.g Project:core.<br>\n3)For more advance search queries you can type EPIC: and write an epic statement that will evaluate rows and only display rows that evaluate true. For fields with spaces in the name replace \"_\" for the space<br>\n</p>\n</html>"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelFilter.add(jTextFieldFilter, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jPanelFilter, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jScrollPaneDefinitions, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonAddDefinitionActionPerformed
        displayAddDefinitionPopupMenu(jButtonAddDefinition, 0, jButtonAddDefinition.getHeight());
    }//GEN-LAST:event_jButtonAddDefinitionActionPerformed

    private void jButtonSaveDefinitionsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSaveDefinitionsActionPerformed
    {//GEN-HEADEREND:event_jButtonSaveDefinitionsActionPerformed
        saveChanges();
    }//GEN-LAST:event_jButtonSaveDefinitionsActionPerformed

    private void jButtonRemoveDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveDefinitionActionPerformed
        deleteSelectedDefinitions();
    }//GEN-LAST:event_jButtonRemoveDefinitionActionPerformed

    private void jButtonRenameDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRenameDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonRenameDefinitionActionPerformed
        renameSelectedDefinition();
    }//GEN-LAST:event_jButtonRenameDefinitionActionPerformed

    private void jButtonCopyDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCopyDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonCopyDefinitionActionPerformed
        copyDefinition();
    }//GEN-LAST:event_jButtonCopyDefinitionActionPerformed

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        if (canClose())
        {
            refresh();
        }
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jComboFilterItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboFilterItemStateChanged
    {//GEN-HEADEREND:event_jComboFilterItemStateChanged
        filterDefinitions(jTextFieldFilter.getText());
    }//GEN-LAST:event_jComboFilterItemStateChanged

    private void jButtonApplyPatchActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonApplyPatchActionPerformed
    {//GEN-HEADEREND:event_jButtonApplyPatchActionPerformed
        applyPatchChanges();
    }//GEN-LAST:event_jButtonApplyPatchActionPerformed

    private void jButtonDocumentationActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDocumentationActionPerformed
    {//GEN-HEADEREND:event_jButtonDocumentationActionPerformed
        showDocumentationPopupMenu(jButtonDocumentation, new Point(0, jButtonDocumentation.getHeight()));
    }//GEN-LAST:event_jButtonDocumentationActionPerformed

    private void jButtonValidateDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonValidateDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonValidateDefinitionActionPerformed
        validateDefinitions();
    }//GEN-LAST:event_jButtonValidateDefinitionActionPerformed

    private void jButtonhistoryActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonhistoryActionPerformed
    {//GEN-HEADEREND:event_jButtonhistoryActionPerformed
        T8DefinitionHistoryPanel.showDefinitionHistoryPanel(parentView, definitionContext, selectedDefinition.getRootProjectId(), selectedDefinition.getIdentifier());
    }//GEN-LAST:event_jButtonhistoryActionPerformed

    private void jButtonCheckUsagesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCheckUsagesActionPerformed
    {//GEN-HEADEREND:event_jButtonCheckUsagesActionPerformed
        jPopupMenuCheckUsages.show(jButtonCheckUsages, 0, jButtonCheckUsages.getHeight());
    }//GEN-LAST:event_jButtonCheckUsagesActionPerformed

    private void jMenuItemSelectedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSelectedActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSelectedActionPerformed
        validateDefinitionsAsynchronously(false);
    }//GEN-LAST:event_jMenuItemSelectedActionPerformed

    private void jMenuItemAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAllActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAllActionPerformed
        validateDefinitionsAsynchronously(true);
    }//GEN-LAST:event_jMenuItemAllActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddDefinition;
    private javax.swing.JButton jButtonApplyPatch;
    private javax.swing.JButton jButtonCheckUsages;
    private javax.swing.JButton jButtonCopyDefinition;
    private javax.swing.JButton jButtonDocumentation;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JButton jButtonRemoveDefinition;
    private javax.swing.JButton jButtonRenameDefinition;
    private javax.swing.JButton jButtonSaveDefinitions;
    private javax.swing.JButton jButtonValidateDefinition;
    private javax.swing.JButton jButtonhistory;
    private javax.swing.JComboBox<String> jComboBoxProject;
    private javax.swing.JComboBox jComboFilter;
    private javax.swing.JMenuItem jMenuItemAll;
    private javax.swing.JMenuItem jMenuItemSelected;
    private javax.swing.JPanel jPanelFilter;
    private javax.swing.JPopupMenu jPopupMenuCheckUsages;
    private javax.swing.JScrollPane jScrollPaneDefinitions;
    protected javax.swing.JTextField jTextFieldFilter;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
