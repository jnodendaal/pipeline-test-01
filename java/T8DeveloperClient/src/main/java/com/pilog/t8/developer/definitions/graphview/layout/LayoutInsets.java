package com.pilog.t8.developer.definitions.graphview.layout;

/**
 * @author Bouwer du Preez
 */
public class LayoutInsets
{
    private int topInset;
    private int bottomInset;
    private int leftInset;
    private int rightInset;
    
    public LayoutInsets(int topInset, int rightInset, int bottomInset, int leftInset)
    {
        this.topInset = topInset;
        this.rightInset = rightInset;
        this.bottomInset = bottomInset;
        this.leftInset = leftInset;
    }

    public void setInsets(LayoutInsets insets)
    {
        setTopInset(insets.getTopInset());
        setBottomInset(insets.getBottomInset());
        setLeftInset(insets.getLeftInset());
        setRightInset(insets.getRightInset());
    }
    
    public int getBottomInset()
    {
        return bottomInset;
    }

    public void setBottomInset(int bottomInset)
    {
        this.bottomInset = bottomInset;
    }

    public int getLeftInset()
    {
        return leftInset;
    }

    public void setLeftInset(int leftInset)
    {
        this.leftInset = leftInset;
    }

    public int getRightInset()
    {
        return rightInset;
    }

    public void setRightInset(int rightInset)
    {
        this.rightInset = rightInset;
    }

    public int getTopInset()
    {
        return topInset;
    }

    public void setTopInset(int topInset)
    {
        this.topInset = topInset;
    }

}
