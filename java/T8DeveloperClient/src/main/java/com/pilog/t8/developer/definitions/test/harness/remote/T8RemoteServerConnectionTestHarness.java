package com.pilog.t8.developer.definitions.test.harness.remote;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.remote.server.connection.T8DefaultRemoteServerConnectionDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Test harness which is responsible for testing the
 * {@code T8RemoteServerConnectionDefinition}. Used through the developer, and
 * kept simple to only execute the connection and output the response.
 *
 * @author Gavin Boshoff
 */
public class T8RemoteServerConnectionTestHarness implements T8DefinitionTestHarness
{
    private static final T8Logger logger = com.pilog.t8.T8Log.getLogger(T8RemoteServerConnectionTestHarness.class.getSimpleName());

    @Override
    public void configure(T8Context context) {}

    @Override
    public void testDefinition(T8Context context, T8Definition definition)
    {
        T8DefaultRemoteServerConnectionDefinition remoteServerDefinition;
        HttpURLConnection connection;

        try
        {
            remoteServerDefinition = (T8DefaultRemoteServerConnectionDefinition)definition;

            logger.log(T8Logger.Level.INFO, "Using service URL : " + remoteServerDefinition.getServiceURL());
            connection = (HttpURLConnection)new URL(remoteServerDefinition.getServiceURL()).openConnection();

            logger.log(T8Logger.Level.INFO, "Actual Connection Type : " + connection.getClass());
            logger.log(T8Logger.Level.INFO, "Response Code Received : " + connection.getResponseCode());
            logger.log(T8Logger.Level.INFO, "Response Message Received : " + connection.getResponseMessage());

            Toast.show("Code: " + connection.getResponseCode() + " - Message : " + connection.getResponseMessage(), Toast.Style.SUCCESS, Toast.LENGTH_LONG);
        }
        catch (MalformedURLException ex)
        {
            logger.log("Service URL not properly formed.", ex);
            Toast.show("Failed : Malformed URL.", Toast.Style.ERROR, Toast.LENGTH_LONG);
        }
        catch (IOException ex)
        {
            logger.log("Failed to create the connection to the endpoint.", ex);
            Toast.show("Failed : Connection failed.", Toast.Style.ERROR, Toast.LENGTH_LONG);
        }
    }
}
