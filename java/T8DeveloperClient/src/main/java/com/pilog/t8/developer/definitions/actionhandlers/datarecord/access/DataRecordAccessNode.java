package com.pilog.t8.developer.definitions.actionhandlers.datarecord.access;

import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.swing.tree.TreeNode;
import org.jdesktop.swingx.tree.TreeUtilities;

/**
 * @author Gavin Boshoff
 */
public abstract class DataRecordAccessNode implements TreeNode, ConceptRenderableNode
{
    private final List<DataRecordAccessNode> children;
    protected final DataRecordAccessNode parentNode;
    private ConceptRenderableNodeComparator comparator;
    private boolean childrenLoaded;
    private boolean loadFields;

    public DataRecordAccessNode(DataRecordAccessNode parentNode)
    {
        this.parentNode = parentNode;
        this.children = new ArrayList<>();
        this.childrenLoaded = false;
        this.loadFields = false;
    }

    public DataRecordAccessNode(DataRecordAccessNode parentNode, boolean loadFields)
    {
        this(parentNode);
        this.loadFields = loadFields;
    }

    public DataRecordAccessNode(DataRecordAccessNode parentNode, boolean loadFields, TerminologyProvider terminologyProvider)
    {
        this(parentNode, loadFields);
        this.comparator = new ConceptRenderableNodeComparator(terminologyProvider);
    }

    protected boolean isChildrenLoaded()
    {
        return this.childrenLoaded;
    }

    protected void setChildrenLoaded(boolean childrenLoaded)
    {
        this.childrenLoaded = childrenLoaded;
    }

    protected void setChildren(List<DataRecordAccessNode> children)
    {
        this.children.clear();
        this.children.addAll(children);
    }

    protected void sortChildren()
    {
        Collections.sort(this.children, getComparator());
    }

    protected void addChild(DataRecordAccessNode child)
    {
        this.children.add(child);
    }

    protected void clearChildren()
    {
        this.children.clear();
    }

    public abstract DataRequirementInstance getDataRequirementInstance();

    public abstract void loadChildren(T8Context context, T8OntologyClientApi ontApi, T8DataRequirementClientApi drqApi);

    public abstract String getDocPathRepresentation(String suffix);

    public ConceptRenderableNodeComparator getComparator()
    {
        if(comparator != null) return comparator;
        else if(getParent() != null) return getParent().getComparator();
        else return null;
    }

    public boolean loadFields()
    {
        if(getParent() != null)
        {
            return getParent().loadFields();
        }
        else return loadFields;
    }

     @Override
    public TreeNode getChildAt(int childIndex)
    {
        return children.get(childIndex);
    }

    @Override
    public int getChildCount()
    {
        return this.childrenLoaded ? this.children.size() : 1;
    }

    @Override
    public DataRecordAccessNode getParent()
    {
        return parentNode;
    }

    @Override
    public int getIndex(TreeNode node)
    {
        return children.indexOf(node);
    }

    @Override
    public boolean getAllowsChildren()
    {
        return !this.childrenLoaded;
    }

    @Override
    public boolean isLeaf()
    {
        return this.childrenLoaded ? this.children.isEmpty() : false;
    }

    @Override
    public Enumeration<DataRecordAccessNode> children()
    {
        return new TreeUtilities.BreadthFirstNodeEnumeration<>(this);
    }
}
