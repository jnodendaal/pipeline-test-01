package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionComparator;
import com.pilog.t8.definition.T8DefinitionConstructorParameters;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import com.pilog.t8.ui.table.TableColumnAdjuster;
import com.pilog.t8.utilities.components.cellrenderers.CellRendererUtilities;
import com.pilog.t8.utilities.threads.Threads;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

/**
 * @author Bouwer du Preez
 */
public class DefinitionTableDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private static final T8Logger logger = T8Log.getLogger(DefinitionTableDatumEditor.class);

    private final List<T8Definition> definitionList;
    private DefaultTableModel tableModel;
    private int maximumElements;
    private JTable jTableDefinitions;
    private final T8DatumTableCellEditor tableCellEditor;
    private final boolean identifierVisible;
    private final boolean displayNameVisible;
    private final boolean descriptionVisible;
    private boolean editable;

    public DefinitionTableDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        this.tableCellEditor = new T8DatumTableCellEditor(this);
        this.maximumElements = datumType.getMaximumElements();
        this.identifierVisible = true;
        this.displayNameVisible = false;
        this.descriptionVisible = false;
        this.definitionList = new ArrayList<>();
        if (maximumElements == -1) maximumElements = 10000; // A very large value, since -1 indicates unbounded element count.
        setupTable();
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        tableCellEditor.startComponent();
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
        tableCellEditor.stopComponent();
    }

    @Override
    public void commitChanges()
    {
        if (definitionList != null)
        {
            for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
            {
                T8Definition rowDefinition;
                ArrayList<T8DefinitionDatumType> datumTypes;

                rowDefinition = definitionList.get(rowIndex);
                datumTypes = rowDefinition.getDatumTypes();
                rowDefinition.setMetaDisplayName((String)tableModel.getValueAt(rowIndex, 1));
                rowDefinition.setMetaDescription((String)tableModel.getValueAt(rowIndex, 2));

                for (int datumIndex = 0; datumIndex < datumTypes.size(); datumIndex++)
                {
                    T8DefinitionDatumType columnDatum;

                    columnDatum = datumTypes.get(datumIndex);
                    rowDefinition.setDefinitionDatum(columnDatum.getIdentifier(), tableModel.getValueAt(rowIndex, datumIndex+3));
                }
            }

            setDefinitionDatum(definitionList);
        }
        else setDefinitionDatum(null);
    }

    @Override
    public void refreshEditor()
    {
        List<T8Definition> valueList;

        // Make sure we use the EDT.
        Threads.ensureEDT();

        // Set a new table model.
        removeAllDefinitionRows();
        valueList = (List<T8Definition>)getDefinitionDatum();

        if ((valueList != null) && (valueList.size() > 0))
        {
            // Add all the rows to the table model.
            for (T8Definition rowDefinition : valueList)
            {
                addDefinitionRow(rowDefinition, false);
            }
        }

        jTableDefinitions.repaint();
    }

    @Override
    public void setEditable(boolean editable)
    {
        this.editable = editable;
        jButtonAdd.setEnabled(editable);
        jButtonRemove.setEnabled(editable);
        jButtonRename.setEnabled(editable);
        jButtonMoveUp.setEnabled(editable);
        jButtonMoveDown.setEnabled(editable);
        jButtonSortAlphabetic.setEnabled(editable);
        jTableDefinitions.setEnabled(editable);
    }

    private void setupTable()
    {
        JScrollPane tableScrollPane;
        TableColumnAdjuster columnAdjuster;

        jTableDefinitions = new JTable();
        jTableDefinitions.setRowHeight(25);
        jTableDefinitions.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        // Create a scroll pane that will resize vertically along with the table it contains but still allow a scroll bar on the horizontal axis.
        tableScrollPane = new JScrollPane(jTableDefinitions)
        {
            @Override
            public Dimension getPreferredSize()
            {
                int height;

                height = 0;
                height += viewport.getView().getPreferredSize().getHeight();
                height += columnHeader.getPreferredSize().getHeight();
                if (this.horizontalScrollBar.isVisible()) height += this.horizontalScrollBar.getHeight();
                return new Dimension(0, height);
            }
        };

        tableModel = new DefaultTableModel();
        jTableDefinitions.setModel(tableModel);

        columnAdjuster = new TableColumnAdjuster(jTableDefinitions);
        columnAdjuster.setColumnDataIncluded(true);
        columnAdjuster.setColumnHeaderIncluded(true);
        columnAdjuster.setDynamicAdjustment(true);
        columnAdjuster.setAdjustRowHeights(true);

        tableScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        add(tableScrollPane, java.awt.BorderLayout.CENTER);
    }

    private DefaultTableModel createTableModel(List<T8DefinitionDatumType> datumTypes)
    {
        DefaultTableModel newModel;
        String[] columnNames;

        columnNames = new String[datumTypes.size() + 3];
        columnNames[0] = "Identifier";
        columnNames[1] = "Display Name";
        columnNames[2] = "Description";
        for (int datumIndex = 0; datumIndex < datumTypes.size(); datumIndex++)
        {
            columnNames[datumIndex+3] = datumTypes.get(datumIndex).getDisplayName();
        }

        newModel = new DefaultTableModel(columnNames, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return column > 0; // The first column contains the definition identifiers and is not editable.
            }
        };

        return newModel;
    }

    private void setupCellEditors(List<T8DefinitionDatumType> datumTypes)
    {
        for (T8DefinitionDatumType defDatumType : datumTypes)
        {
            jTableDefinitions.getColumn(defDatumType.getDisplayName()).setCellEditor(tableCellEditor);
        }
    }

    public T8Definition getDefinition(int index)
    {
        return definitionList.get(index);
    }

    public T8DefinitionDatumType getDatumType(int rowIndex, int columnIndex)
    {
        if ((rowIndex > -1) && (columnIndex > -1))
        {
            T8Definition rowDefinition;
            List<T8DefinitionDatumType> datumTypes;
            int datumIndex;

            rowDefinition = definitionList.get(rowIndex);
            datumTypes = rowDefinition.getDatumTypes();

            datumIndex = columnIndex;
            if (identifierVisible) datumIndex--;
            if (displayNameVisible) datumIndex--;
            if (descriptionVisible) datumIndex--;

            return datumTypes.get(datumIndex);
        }
        else return null;
    }

    private void addDefinitionRow(T8Definition newDefinition, boolean commitDatumValue)
    {
        ArrayList<T8DefinitionDatumType> datumTypes;
        Object rowData[];

        if ((tableModel == null) || (tableModel.getColumnCount() == 0))
        {
            tableModel = createTableModel(newDefinition.getDatumTypes());
            jTableDefinitions.setModel(tableModel);

            // Hide unnecessary columns.
            if (!identifierVisible) jTableDefinitions.getColumnModel().removeColumn(jTableDefinitions.getColumn("Identifier"));
            if (!displayNameVisible) jTableDefinitions.getColumnModel().removeColumn(jTableDefinitions.getColumn("Display Name"));
            if (!descriptionVisible) jTableDefinitions.getColumnModel().removeColumn(jTableDefinitions.getColumn("Description"));

            // Setup the cell editors.
            setupCellEditors(newDefinition.getDatumTypes());
        }

        rowData = new Object[tableModel.getColumnCount()];
        datumTypes = newDefinition.getDatumTypes();

        rowData[0] = newDefinition.getIdentifier();
        rowData[1] = newDefinition.getMetaDisplayName();
        rowData[2] = newDefinition.getMetaDescription();
        for (int datumIndex = 0; datumIndex < datumTypes.size(); datumIndex++)
        {
            T8DefinitionDatumType rowDatum;

            rowDatum = datumTypes.get(datumIndex);
            rowData[datumIndex+3] = newDefinition.getDefinitionDatum(rowDatum.getIdentifier());
        }

        tableModel.addRow(rowData);
        definitionList.add(newDefinition);

        jButtonAdd.setEnabled((editable) && (definitionList.size() < maximumElements));
        jButtonRemove.setEnabled((editable) && (definitionList.size() > 0));
        revalidate(); // To allow the scroll pane to resize.

        // If the datum value must be committed, do it.
        if (commitDatumValue)
        {
            setDefinitionDatum(definitionList);
        }

        CellRendererUtilities.autoSizeTableColumns(jTableDefinitions, 125);
    }

    private void removeSelectedDefinitionRows()
    {
        if (jTableDefinitions.getCellEditor() != null) jTableDefinitions.getCellEditor().stopCellEditing();
        while (jTableDefinitions.getSelectedRowCount() > 0)
        {
            int selectedRowIndex;

            selectedRowIndex = jTableDefinitions.getSelectedRow();
            tableModel.removeRow(selectedRowIndex);
            definitionList.remove(selectedRowIndex);
        }

        jButtonAdd.setEnabled((editable) && (definitionList.size() < maximumElements));
        jButtonRemove.setEnabled((editable) && (definitionList.size() > 0));
        revalidate(); // To allow the scroll pane to resize.
    }

    private void removeAllDefinitionRows()
    {
        if (jTableDefinitions.getCellEditor() != null) jTableDefinitions.getCellEditor().stopCellEditing();
        while (tableModel.getRowCount() > 0)
        {
            tableModel.removeRow(0);
            definitionList.remove(0);
        }

        jButtonAdd.setEnabled((editable) && (definitionList.size() < maximumElements));
        jButtonRemove.setEnabled((editable) && (definitionList.size() > 0));
        revalidate(); // To allow the scroll pane to resize.
    }

    private void renameSelectedDefinition()
    {
        if (jTableDefinitions.getSelectedRowCount() > -1)
        {
            T8Definition selectedDefinition;
            int selectedRowIndex;

            selectedRowIndex = jTableDefinitions.getSelectedRow();
            selectedDefinition = definitionList.get(selectedRowIndex);
            if (selectedDefinition != null)
            {
                try
                {
                    T8DefinitionMetaData metaData;
                    List<T8DefinitionHandle> usageHandles;
                    Iterator<T8DefinitionHandle> handleIterator;
                    String selectedDefinitionIdentifier;

                    // Find all definitions that use the one to be deleted.
                    usageHandles = definitionContext.findDefinitionsByIdentifier(selectedDefinition.getPublicIdentifier(), true);

                    // Remove any usages of the currently edited definition (it obviously uses the definition to be deleted).
                    selectedDefinitionIdentifier = selectedDefinition.getRootDefinition().getIdentifier();
                    handleIterator = usageHandles.iterator();
                    while (handleIterator.hasNext())
                    {
                        if (handleIterator.next().getDefinitionIdentifier().equals(selectedDefinitionIdentifier))
                        {
                            handleIterator.remove();
                        }
                    }

                    // If we have any remaining identifiers we know that we have to save the current definition changes before the renaming.
                    if (usageHandles.size() > 0)
                    {
                        int confirmation;

                        confirmation = JOptionPane.showConfirmDialog(this, "The definition is used by " + usageHandles.size() + " other definition(s).  All changes will be saved before you can rename it?", "Save Confirmation", JOptionPane.YES_NO_OPTION);
                        if (confirmation != JOptionPane.YES_OPTION) return;
                    }

                    // Prompt the user for a new identifier and then rename the definition.
                    metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this, selectedDefinition.getTypeMetaData(), selectedDefinition.getParentDefinition());
                    if (metaData != null)
                    {
                        definitionContext.renameDefinition(selectedDefinition, metaData.getId());
                        tableModel.setValueAt(metaData.getId(), selectedRowIndex, 0);
                    }
                }
                catch (Exception e)
                {
                    logger.log("Exception while renaming a definition.", e);
                }
            }
        }
    }

    private void moveSelectedDefinitionsUp()
    {
        List<T8Definition> definitions;
        int[] selectedIndices;

        // Stop any editing that may be in progress on the table.
        if (jTableDefinitions.getCellEditor() != null) jTableDefinitions.getCellEditor().stopCellEditing();

        definitions = new ArrayList<>(definitionList);
        selectedIndices = jTableDefinitions.getSelectedRows();
        if (selectedIndices.length > 0)
        {
            T8Definition selectedDefinition;

            // Decrement all selected Indices.
            for (int selectedIndex : selectedIndices)
            {
                if (selectedIndex > 0)
                {
                    selectedDefinition = definitions.remove(selectedIndex);
                    definitions.add(selectedIndex-1, selectedDefinition);
                }
            }

            // Commit the updated definition list.
            setDefinitionDatum(definitions);

            // Refresh the editor.
            refreshEditor();

            // Decrement all selected indices (because they've been moved up).
            jTableDefinitions.clearSelection();
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]-1);
                jTableDefinitions.getSelectionModel().addSelectionInterval(selectedIndices[indexIndex], selectedIndices[indexIndex]);
            }
        }
    }

    private void moveSelectedDefinitionsDown()
    {
        List<T8Definition> definitions;
        int[] selectedIndices;

        // Stop any editing that may be in progress on the table.
        if (jTableDefinitions.getCellEditor() != null) jTableDefinitions.getCellEditor().stopCellEditing();

        definitions = new ArrayList<>(definitionList);
        selectedIndices = jTableDefinitions.getSelectedRows();
        if (selectedIndices.length > 0)
        {
            T8Definition selectedDefinition;

            // Decrement all selected Indices.
            for (int selectedIndex : selectedIndices)
            {
                if (selectedIndex < definitions.size() -1)
                {
                    selectedDefinition = definitions.remove(selectedIndex);
                    definitions.add(selectedIndex+1, selectedDefinition);
                }
            }

            // Commit the updated definition list.
            setDefinitionDatum(definitions);

            // Refresh the editor.
            refreshEditor();

            // Increment all selected indices (because they've been moved down).
            jTableDefinitions.clearSelection();
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]+1);
                jTableDefinitions.getSelectionModel().addSelectionInterval(selectedIndices[indexIndex], selectedIndices[indexIndex]);
            }
        }
    }

    private void sortAlphabetic()
    {
        List<T8Definition> definitions;

        // Stop any editing that may be in progress on the table.
        if (jTableDefinitions.getCellEditor() != null) jTableDefinitions.getCellEditor().stopCellEditing();

        definitions = new ArrayList<>(definitionList);

        Collections.sort(definitions, new T8DefinitionComparator());

        // Commit the updated definition list.
        setDefinitionDatum(definitions);

        // Refresh the editor.
        refreshEditor();
    }

    private void displayDefinitionPopupMenu(Component contextComponent, int x, int y)
    {
        JPopupMenu popupMenu;
        JMenuItem menuItem;

        // Create the ActionListener for the menu items.
        ActionListener menuListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                DefinitionMenuItem menuItem;
                String code;

                menuItem = (DefinitionMenuItem)event.getSource();
                code = menuItem.getCode();
                if (code.equals("ADD_DEFINITION"))
                {
                    addDefinition((T8DefinitionDatumOption)menuItem.getDataObject());
                }
            }
        };

        // Create a new popup menu.
        popupMenu = new JPopupMenu();

        // Create the definition menu.
        try
        {
            ArrayList<T8DefinitionDatumOption> datumOptions;
            datumOptions = definition.getDatumOptions(definitionContext, datumType.getIdentifier());

            //Only show the popup if there are multiple items to choose from, otherwise just add the definition
            if(datumOptions.size() == 1)
            {
                addDefinition(datumOptions.get(0));
            }
            else
            {
                for (T8DefinitionDatumOption datumOption : datumOptions)
                {
                    menuItem = new DefinitionMenuItem(datumOption.getDisplayName(), "ADD_DEFINITION", datumType.getIdentifier(), datumOption);
                    menuItem.addActionListener(menuListener);
                    popupMenu.add(menuItem);
                }

                // Display the newly constructed popup menu.
                popupMenu.show(contextComponent, x, y);
            }
        }
        catch (Exception e)
        {
            logger.log("Definition datum options could not be resolved successfully.", e);
        }
    }

    private void addDefinition(T8DefinitionDatumOption datumOption)
    {
        T8DefinitionConstructorParameters constructorParameters;
        String definitionTypeIdentifier;
        T8DefinitionMetaData metaData;

        constructorParameters = (T8DefinitionConstructorParameters)datumOption.getValue();
        definitionTypeIdentifier = constructorParameters.getTypeIdentifier();

        try
        {
            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, DefinitionTableDatumEditor.this, definitionContext.getDefinitionTypeMetaData(definitionTypeIdentifier), definition);
            if (metaData != null)
            {
                try
                {
                    ArrayList<Object> parameters;

                    parameters = constructorParameters.getParameters();
                    if (parameters == null)
                    {
                        T8Definition newDefinition;

                        newDefinition = definitionContext.createNewDefinition(metaData.getId(), definitionTypeIdentifier);
                        newDefinition.setProjectIdentifier(metaData.getProjectId());
                        addDefinitionRow(newDefinition, true);
                    }
                    else
                    {
                        T8Definition newDefinition;

                        newDefinition = definitionContext.createNewDefinition(metaData.getId(), definitionTypeIdentifier, parameters);
                        newDefinition.setProjectIdentifier(metaData.getProjectId());
                        addDefinitionRow(newDefinition, true);
                    }
                }
                catch (Exception ex)
                {
                    logger.log(ex);
                }
            }
        }
        catch (Exception e)
        {
            logger.log("Exception while fetching meta data for type: " + definitionTypeIdentifier, e);
        }
    }

    private class DefinitionMenuItem extends JMenuItem
    {
        private final String code;
        private final String datumIdentifier;
        private final Object dataObject;

        private DefinitionMenuItem(String text, String code, String datumIdentifier, Object dataObject)
        {
            super(text);
            this.code = code;
            this.datumIdentifier = datumIdentifier;
            this.dataObject = dataObject;
        }

        public String getCode()
        {
            return code;
        }

        public String getDatumIdentifier()
        {
            return datumIdentifier;
        }

        public Object getDataObject()
        {
            return dataObject;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jButtonRemove = new javax.swing.JButton();
        jButtonMoveUp = new javax.swing.JButton();
        jButtonMoveDown = new javax.swing.JButton();
        jButtonRename = new javax.swing.JButton();
        jButtonSortAlphabetic = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAdd);

        jButtonRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeDocumentIcon.png"))); // NOI18N
        jButtonRemove.setText("Remove");
        jButtonRemove.setFocusable(false);
        jButtonRemove.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRemove);

        jButtonMoveUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowUpIcon.png"))); // NOI18N
        jButtonMoveUp.setText("Up");
        jButtonMoveUp.setFocusable(false);
        jButtonMoveUp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveUp.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveUpActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonMoveUp);

        jButtonMoveDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowDownIcon.png"))); // NOI18N
        jButtonMoveDown.setText("Down");
        jButtonMoveDown.setFocusable(false);
        jButtonMoveDown.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveDown.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveDownActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonMoveDown);

        jButtonRename.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/renameDocumentIcon.png"))); // NOI18N
        jButtonRename.setText("Rename");
        jButtonRename.setFocusable(false);
        jButtonRename.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRename.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRenameActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRename);

        jButtonSortAlphabetic.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/sortAlphabeticIcon.png"))); // NOI18N
        jButtonSortAlphabetic.setText("Sort");
        jButtonSortAlphabetic.setFocusable(false);
        jButtonSortAlphabetic.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonSortAlphabetic.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSortAlphabetic.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSortAlphabeticActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSortAlphabetic);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        displayDefinitionPopupMenu(jButtonAdd, 0, jButtonAdd.getHeight());
        CellRendererUtilities.autoSizeTableColumns(jTableDefinitions, 100);
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveActionPerformed
        removeSelectedDefinitionRows();
        CellRendererUtilities.autoSizeTableColumns(jTableDefinitions, 100);
    }//GEN-LAST:event_jButtonRemoveActionPerformed

    private void jButtonMoveUpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveUpActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveUpActionPerformed
        moveSelectedDefinitionsUp();
    }//GEN-LAST:event_jButtonMoveUpActionPerformed

    private void jButtonMoveDownActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveDownActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveDownActionPerformed
        moveSelectedDefinitionsDown();
    }//GEN-LAST:event_jButtonMoveDownActionPerformed

    private void jButtonRenameActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRenameActionPerformed
    {//GEN-HEADEREND:event_jButtonRenameActionPerformed
        renameSelectedDefinition();
    }//GEN-LAST:event_jButtonRenameActionPerformed

    private void jButtonSortAlphabeticActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSortAlphabeticActionPerformed
    {//GEN-HEADEREND:event_jButtonSortAlphabeticActionPerformed
        sortAlphabetic();
    }//GEN-LAST:event_jButtonSortAlphabeticActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonMoveDown;
    private javax.swing.JButton jButtonMoveUp;
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JButton jButtonRename;
    private javax.swing.JButton jButtonSortAlphabetic;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
