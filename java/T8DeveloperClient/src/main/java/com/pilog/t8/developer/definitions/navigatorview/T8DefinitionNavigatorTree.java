package com.pilog.t8.developer.definitions.navigatorview;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionComparator;
import com.pilog.t8.definition.T8DefinitionConstructorParameters;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.developer.definitions.CopyAndPasteHandler;
import com.pilog.t8.developer.utils.T8DefinitionIdentifierRenamingDialog;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.cache.LRUCache;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.components.list.ListPopupMenuListener;
import com.pilog.t8.utilities.components.list.popupmenu.ListSubMenu;
import com.pilog.t8.utilities.components.list.popupmenu.SearchableListPopup;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DropMode;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionNavigatorTree extends JTree
{
    private static final List<String> HIDDEN_DATUMS = new ArrayList<String>();

    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionContext definitionContext;
    private final T8DefinitionNavigatorTreeCellRenderer navigatorTreeCellRenderer;
    private T8Definition rootDefinition;
    private final T8DeveloperView parentView;
    private final T8DefinitionManager definitionManager;
    private final T8DeveloperViewFactory viewFactory;
    private boolean locked;
    private final CopyAndPasteHandler copyAndPasteHandler;

    // Stores the expansion states of the last 10 definitions.
    private final LRUCache<String, Map<String, Boolean>> definitionExpansionStates = new LRUCache<String, Map<String, Boolean>>(10);

    public T8DefinitionNavigatorTree(T8DefinitionContext definitionContext, T8DeveloperView parentView, T8Definition rootDefinition)
    {
        this.setModel(null);
        this.setRowHeight(0);
        this.clientContext = definitionContext.getClientContext();
        this.context = definitionContext.getContext();
        this.definitionContext = definitionContext;
        this.parentView = parentView;
        this.viewFactory = parentView.getViewFactory();
        this.definitionManager = clientContext.getDefinitionManager();
        this.navigatorTreeCellRenderer = new T8DefinitionNavigatorTreeCellRenderer(definitionManager);
        this.setCellRenderer(navigatorTreeCellRenderer);
        this.addMouseListener(new ModuleNavigatorMouseListener());
        this.addKeyListener(new ModuleNavigatorKeyListener());
        this.setTransferHandler(new T8DefinitionNavigatorTransferHandler(context));
        this.setDragEnabled(false);
        this.setDropMode(DropMode.ON_OR_INSERT);
        this.copyAndPasteHandler = new CopyAndPasteHandler();

        setRootDefinition(rootDefinition);
    }

    public T8Definition getRootDefinition()
    {
        return rootDefinition;
    }

    public final void setRootDefinition(T8Definition rootDefinition)
    {
        saveExpansionState();
        this.rootDefinition = rootDefinition;
        if (rootDefinition != null)
        {
            DefaultMutableTreeNode rootNode;

            // Determine whether the current definition is editable.
            locked = rootDefinition.getRootDefinition().isLocked();

            // Create the root definition node.
            rootNode = new DefaultMutableTreeNode(rootDefinition);
            setModel(new DefaultTreeModel(rootNode));
            rebuildNode(rootNode);
            expandNode(rootNode);
            setExpansionState(definitionExpansionStates.get(rootDefinition.getIdentifier()));
        }
        else
        {
            locked = true;
            setModel(new DefaultTreeModel(null));
        }
    }

    private DefaultMutableTreeNode getRootNode()
    {
        TreeModel model;

        model = getModel();
        return model != null ? (DefaultMutableTreeNode)model.getRoot() : null;
    }

    private void rebuildNode(DefaultMutableTreeNode node)
    {
        T8Definition definition;
        DefaultTreeModel model;

        model = getTreeModel();
        definition = (T8Definition)node.getUserObject();
        for (T8DefinitionDatumType datumType : definition.getDatumTypes())
        {
            if (datumType.isSubDefinitionType())
            {
                DefaultMutableTreeNode groupNode;
                Object datumObject;

                // Get the existing group node or create a new one if it does not exist.
                groupNode = getGroupNode(node, datumType.getIdentifier());
                if (groupNode == null)
                {
                    groupNode = new DefaultMutableTreeNode(new T8DefinitionNavigatorDatum(definition, datumType.getIdentifier()));
                    model.insertNodeInto(groupNode, node, node.getChildCount());
                }
                ((T8DefinitionNavigatorDatum)groupNode.getUserObject()).setHidden(HIDDEN_DATUMS.contains(datumType.getIdentifier()));

                // Get the value of the datum from the definition.
                datumObject = definition.getDefinitionDatum(datumType.getIdentifier());
                if (datumObject != null)
                {
                    if (datumObject instanceof List) // The datum contains a list of definitions.
                    {
                        List<T8Definition> subDefinitions;

                        subDefinitions = (List<T8Definition>)datumObject;
                        if (subDefinitions.isEmpty())
                        {
                            // Clear the group, since it no longer contains content.
                            model.removeNodeFromParent(groupNode);
                        }
                        else // Synchronize the displayed content of the group with the actual content values.
                        {
                            // Remove all definitions nodes from the group node if the definition no longer exists.
                            for (int childNodeIndex = 0; childNodeIndex < groupNode.getChildCount(); childNodeIndex++)
                            {
                                DefaultMutableTreeNode childNode;
                                Object nodeObject;

                                childNode = (DefaultMutableTreeNode)groupNode.getChildAt(childNodeIndex);
                                nodeObject = childNode.getUserObject();
                                if (nodeObject instanceof T8Definition)
                                {
                                    if (!subDefinitions.contains((T8Definition)nodeObject))
                                    {
                                        model.removeNodeFromParent(childNode);
                                    }
                                }
                            }

                            // Add all new definition nodes.
                            for (int subDefinitionIndex = 0; subDefinitionIndex < subDefinitions.size(); subDefinitionIndex++)
                            {
                                DefaultMutableTreeNode subDefinitionNode;
                                T8Definition subDefinition;

                                subDefinition = subDefinitions.get(subDefinitionIndex);
                                subDefinitionNode = getSubDefinitionNode(groupNode, subDefinition);
                                if (subDefinitionNode == null) // If it's a new node just add it at the right index.
                                {
                                    subDefinitionNode = new DefaultMutableTreeNode(subDefinition);
                                    if(!HIDDEN_DATUMS.contains(datumType.getIdentifier()))
                                        model.insertNodeInto(subDefinitionNode, groupNode, subDefinitionIndex);
                                }
                                else // If the node already exists, move it to the right index.
                                {
                                    boolean expanded;

                                    expanded = isExpanded(new TreePath(subDefinitionNode.getPath()));
                                    model.removeNodeFromParent(subDefinitionNode);
                                    model.insertNodeInto(subDefinitionNode, groupNode, subDefinitionIndex);
                                    if (expanded) this.setExpandedState(new TreePath(subDefinitionNode.getPath()), true);
                                }

                                rebuildNode(subDefinitionNode);
                            }
                        }
                    }
                    else if (getSubDefinitionNode(groupNode, (T8Definition)datumObject) == null) // The datum contains a single definition, but the node needs to be added.
                    {
                        DefaultMutableTreeNode definitionNode;

                        // Clear all children from the group node.
                        while (groupNode.getChildCount() > 0)
                        {
                            model.removeNodeFromParent((DefaultMutableTreeNode)groupNode.getChildAt(0));
                        }

                        definitionNode = new DefaultMutableTreeNode(datumObject);
                        if (!HIDDEN_DATUMS.contains(datumType.getIdentifier()))
                        {
                            model.insertNodeInto(definitionNode, groupNode, 0);
                        }
                        rebuildNode(definitionNode);
                    }
                }
                else // The datum does not contain a value, so remove the group.
                {
                    model.removeNodeFromParent(groupNode);
                }
            }
        }
    }

    private DefaultMutableTreeNode getGroupNode(DefaultMutableTreeNode node, String datumIdentifier)
    {
        for (int childIndex = 0; childIndex < node.getChildCount(); childIndex++)
        {
            DefaultMutableTreeNode childNode;
            Object nodeObject;

            childNode = (DefaultMutableTreeNode)node.getChildAt(childIndex);
            nodeObject = childNode.getUserObject();
            if (nodeObject instanceof T8DefinitionNavigatorDatum)
            {
                if (((T8DefinitionNavigatorDatum)nodeObject).getDatumIdentifier().equals(datumIdentifier))
                {
                    return childNode;
                }
            }
        }

        return null;
    }

    private DefaultMutableTreeNode getSubDefinitionNode(DefaultMutableTreeNode node, T8Definition subDefinition)
    {
        for (int childIndex = 0; childIndex < node.getChildCount(); childIndex++)
        {
            DefaultMutableTreeNode childNode;
            Object nodeObject;

            childNode = (DefaultMutableTreeNode)node.getChildAt(childIndex);
            nodeObject = childNode.getUserObject();
            if (nodeObject instanceof T8Definition)
            {
                if (((T8Definition)nodeObject) == subDefinition)
                {
                    return childNode;
                }
            }
        }

        return null;
    }

    public void addDefinition(T8Definition parentDefinition, String datumIdentifier, T8Definition definition, int index)
    {
        DefaultMutableTreeNode parentNode;

        parentNode = getDefinitionNode(parentDefinition);
        if (parentNode != null)
        {
            parentDefinition.addSubDefinition(datumIdentifier, definition, index);
            rebuildNode(parentNode);
        }
    }

    public void addDefinition(DefaultMutableTreeNode parentNode, T8DefinitionDatumType datumType, T8Definition definition)
    {
        T8Definition parentDefinition;

        parentDefinition = (T8Definition)parentNode.getUserObject();
        parentDefinition.addSubDefinition(datumType.getIdentifier(), definition);
        rebuildNode(parentNode);
    }

    private void cutDefinition(T8Definition definition)
    {
        try
        {
            DefaultMutableTreeNode parentNode;

            parentNode = getDefinitionNode(definition.getParentDefinition());
            if (parentNode != null)
            {
                List<T8DefinitionHandle> usedByHandles;
                Iterator<T8DefinitionHandle> handleIterator;

                // Find all definitions that use the one to be deleted.
                usedByHandles = definitionManager.findDefinitionsByIdentifier(context, definition.getPublicIdentifier(), true);

                // Remove any usages of the currently edited root definition (it obviously uses the definition to be deleted).
                handleIterator = usedByHandles.iterator();
                while (handleIterator.hasNext())
                {
                    if (handleIterator.next().getDefinitionIdentifier().equals(definition.getNamespace()))
                    {
                        handleIterator.remove();
                    }
                }

                // If we have any remaining identifiers we know that we cannot delete the definition.
                if (usedByHandles.size() > 0)
                {
                    // Log the definitions using this definition.
                    for (T8DefinitionHandle usedByHandle : usedByHandles)
                    {
                        T8Log.log("Used by: " + usedByHandle);
                    }

                    // Display the error feedback message.
                    JOptionPane.showMessageDialog(this, "The definition cannot be cut because it is used by " + usedByHandles.size() + " other definitions.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    copyAndPasteHandler.cutDefinition(definition);
                    rebuildNode(parentNode);
                }
            }
            else
            {
                // Display the error feedback message.
                JOptionPane.showMessageDialog(this, "The root definition cannot be cut.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while trying to cut definition.", e);
            JOptionPane.showMessageDialog(this, "An unexpected error prevented successful completion of the definition cut.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void deleteDefinition(T8Definition definition)
    {
        DefaultMutableTreeNode definitionNode;

        definitionNode = getDefinitionNode(definition);
        if (definitionNode != null)
        {
            deleteNode(definitionNode);
        }
    }

    protected void deleteNode(DefaultMutableTreeNode node)
    {
        try
        {
            DefaultMutableTreeNode parentNode;

            parentNode = getParentDefinitionNode(node);
            if (parentNode != null)
            {
                T8Definition parentDefinition;
                T8Definition definition;
                List<T8DefinitionHandle> usedByHandles;
                Iterator<T8DefinitionHandle> handleIterator;

                parentDefinition = (T8Definition)parentNode.getUserObject();
                definition = (T8Definition)node.getUserObject();

                // Find all definitions that use the one to be deleted.
                usedByHandles = definitionManager.findDefinitionsByIdentifier(context, definition.getPublicIdentifier(), true);

                // Remove any usages of the currently edited root definition (it obviously uses the definition to be deleted).
                handleIterator = usedByHandles.iterator();
                while (handleIterator.hasNext())
                {
                    if (handleIterator.next().getDefinitionIdentifier().equals(definition.getNamespace()))
                    {
                        handleIterator.remove();
                    }
                }

                // If we have any remaining identifiers we know that we cannot delete the definition.
                if (usedByHandles.size() > 0)
                {
                    // Log the definitions using this definition.
                    for (T8DefinitionHandle usedByHandle : usedByHandles)
                    {
                        T8Log.log("Used by: " + usedByHandle);
                    }

                    // Display the error feedback message.
                    JOptionPane.showMessageDialog(this, "The definition cannot be deleted because it is used by " + usedByHandles.size() + " other definitions.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    parentDefinition.removeSubDefinition(definition);
                    rebuildNode(parentNode);
                }
            }
            else
            {
                // Display the error feedback message.
                JOptionPane.showMessageDialog(this, "The root definition cannot be deleted using this method.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while trying to delete definition.", e);
            JOptionPane.showMessageDialog(this, "An unexpected error prevented successful deletion of the definition.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
        }
    }

    protected void pasteDefinition(T8Definition targetDefinition, String targetDatumIdentifier, T8Definition definitionToPaste)
    {
        List<T8Definition> definitionsToRename;
        boolean success;

        // Create a list of all definitions that will be pasted on the target (exclude resource definitions because they cannot be renamed).
        definitionsToRename = new ArrayList<T8Definition>();
        definitionsToRename.add(definitionToPaste);
        for (T8Definition definitionToRename : definitionToPaste.getDescendentDefinitions())
        {
            if (definitionToRename.getLevel() != T8Definition.T8DefinitionLevel.RESOURCE)
            {
                definitionsToRename.add(definitionToRename);
            }
        }

        // Rename all definitions to be pasted to make sure they are unique within the context of the target definition.
        success = T8DefinitionIdentifierRenamingDialog.renameDefinitions(definitionContext, T8DefinitionNavigatorTree.this, getRootDefinition(), definitionsToRename, false);

        // Add the copied definition to the local context (only if all the definitions were successfully renamed).
        if (success)
        {
            refreshSelectedNodes();
            addDefinition(targetDefinition, targetDatumIdentifier, definitionToPaste, -1);
        }
    }

    public void renameDefinition(T8Definition definition)
    {
        try
        {
            if (definition != null)
            {
                List<T8DefinitionHandle> usedByHandles;
                Iterator<T8DefinitionHandle> handleIterator;

                // Find all definitions that use the one to be deleted.
                usedByHandles = definitionContext.findDefinitionsByIdentifier(definition.getPublicIdentifier(), true);

                // Remove any usages of the currently edited definition (it obviously uses the definition to be deleted).
                handleIterator = usedByHandles.iterator();
                while (handleIterator.hasNext())
                {
                    if (handleIterator.next().getDefinitionIdentifier().equals(definition.getIdentifier()))
                    {
                        handleIterator.remove();
                    }
                }

                // If we have any remaining identifiers we know that we have to save the current definition changes before the renaming.
                if (usedByHandles.size() > 0)
                {
                    int confirmation;

                    confirmation = JOptionPane.showConfirmDialog(this, "The definition is used by " + usedByHandles.size() + " other definition(s).  All changes will be saved before you can rename it?", "Save Confirmation", JOptionPane.YES_NO_OPTION);
                    if (confirmation != JOptionPane.YES_OPTION) return;
                }

                // Now rename the definition.
                {
                    T8DefinitionMetaData metaData;
                    String definitionIdentifier;
                    String identifierPrefix;

                    rootDefinition = getRootDefinition();
                    identifierPrefix = definition.getTypeMetaData().getIdPrefix();
                    definitionIdentifier = definition.getIdentifier();
                    if (!Strings.isNullOrEmpty(identifierPrefix)) definitionIdentifier = definitionIdentifier.replace(identifierPrefix, "");
                    metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, T8DefinitionNavigatorTree.this, definition.getTypeMetaData(), definition.getParentDefinition(), null, definitionIdentifier);
                    if (metaData != null)
                    {
                        definitionContext.renameDefinition(definition, metaData.getId());
                        refreshNode(getDefinitionNode(definition));
                    }
                }
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while trying to rename definition: " + definition, e);
        }
    }

    private DefaultMutableTreeNode getParentDefinitionNode(DefaultMutableTreeNode node)
    {
        DefaultMutableTreeNode parentNode;

        parentNode = node;
        while ((parentNode = (DefaultMutableTreeNode)parentNode.getParent()) != null)
        {
            Object userObject;

            userObject = parentNode.getUserObject();
            if (userObject instanceof T8Definition) return parentNode;
        }

        return null;
    }

    private DefaultMutableTreeNode getDefinitionNode(T8Definition definition)
    {
        LinkedList<DefaultMutableTreeNode> nodeQueue;

        nodeQueue = new LinkedList<DefaultMutableTreeNode>();
        nodeQueue.add((DefaultMutableTreeNode)getModel().getRoot());
        while (nodeQueue.size() > 0)
        {
            DefaultMutableTreeNode nextNode;
            Object userObject;

            nextNode = nodeQueue.pop();
            for (int childIndex = 0; childIndex < nextNode.getChildCount(); childIndex++)
            {
                nodeQueue.add((DefaultMutableTreeNode)nextNode.getChildAt(childIndex));
            }

            userObject = nextNode.getUserObject();
            if ((userObject instanceof T8Definition) && (userObject == definition)) return nextNode;
        }

        return null;
    }

    private DefaultTreeModel getTreeModel()
    {
        return (DefaultTreeModel)treeModel;
    }

    public void collapseTree()
    {
        DefaultMutableTreeNode rootNode;

        rootNode = getRootNode();
        if (rootNode != null)
        {
            //collapseNodeDescendants(rootNode);
            //expandNode(rootNode); // We leave the root node expanded so that the first level of the tree is visible.
            Enumeration children = rootNode.children();
            while (children.hasMoreElements())
            {
                Object child = children.nextElement();
                collapseNodeDescendants((DefaultMutableTreeNode)child);
            }
        }
    }

    private void collapseNode(DefaultMutableTreeNode node)
    {
        collapsePath(new TreePath(node.getPath()));
    }

    private void collapseNodeDescendants(DefaultMutableTreeNode node)
    {
        expandAll(node, false);
    }

    public void expandTree()
    {
        DefaultMutableTreeNode rootNode;

        rootNode = getRootNode();
        if (rootNode != null)
        {
            expandNodeDescendants(rootNode);
        }
    }

    private void setExpansionState(Map<String, Boolean> expansionState)
    {
        if (expansionState != null)
        {
            DefaultMutableTreeNode rootNode;

            rootNode = getRootNode();
            if (rootNode != null)
            {
                List<DefaultMutableTreeNode> nodes;

                nodes = getAllDescendantNodes(rootNode);
                for (DefaultMutableTreeNode node : nodes)
                {
                    Boolean expanded;
                    Object nodeObject;

                    expanded = false;
                    nodeObject = node.getUserObject();
                    if (nodeObject instanceof T8Definition)
                    {
                        expanded = (expansionState.get(((T8Definition)nodeObject).getIdentifier()));
                    }
                    else if (nodeObject instanceof T8DefinitionNavigatorDatum)
                    {
                        expanded = (expansionState.get(((T8DefinitionNavigatorDatum)nodeObject).getHandleIdentifier()));
                    }

                    // Set the expanded state.
                    if ((expanded != null) && (expanded))
                    {
                        setExpanded(node, true);
                    }
                }
            }
        }
    }

    private void saveExpansionState()
    {
        DefaultMutableTreeNode rootNode;

        rootNode = getRootNode();
        if (rootNode != null)
        {
            HashMap<String, Boolean> expansionState;
            List<DefaultMutableTreeNode> nodes;
            boolean nonDefaultState; // Set to true if the expansion state has been altered from default (has to be saved).

            nonDefaultState = false;
            expansionState = new HashMap<String, Boolean>();
            nodes = getAllDescendantNodes(rootNode);
            for (DefaultMutableTreeNode node : nodes)
            {
                Object nodeObject;
                boolean expanded;

                expanded = isExpanded(node);
                if (expanded) nonDefaultState = true;
                nodeObject = node.getUserObject();
                if (nodeObject instanceof T8Definition)
                {
                    expansionState.put(((T8Definition)nodeObject).getIdentifier(), expanded);
                }
                else if (nodeObject instanceof T8DefinitionNavigatorDatum)
                {
                    expansionState.put(((T8DefinitionNavigatorDatum)nodeObject).getHandleIdentifier(), expanded);
                }
            }

            // Only save the state if it is non-default.
            if (nonDefaultState)
            {
                definitionExpansionStates.put(rootDefinition.getIdentifier(), expansionState);
            }
        }
    }

    private boolean isExpanded(DefaultMutableTreeNode node)
    {
        return isExpanded(new TreePath(node.getPath()));
    }

    private void setExpanded(DefaultMutableTreeNode node, boolean expanded)
    {
        setExpandedState(new TreePath(node.getPath()), expanded);
    }

    private void expandNode(DefaultMutableTreeNode node)
    {
        expandPath(new TreePath(node.getPath()));
    }

    private void expandNodeChildren(DefaultMutableTreeNode node)
    {
        for (int childIndex = 0; childIndex < node.getChildCount(); childIndex++)
        {
            expandNode((DefaultMutableTreeNode)node.getChildAt(childIndex));
        }
    }

    private void expandNodeDescendants(DefaultMutableTreeNode node)
    {
       // T8Log.log("Expanded " + expandNodeDescendants(node, 0) + " nodes.");
        expandAll(node, true);
    }

    // If expand is true, expands all nodes in the tree.
    // Otherwise, collapses all nodes in the tree.
    public void expandAll(DefaultMutableTreeNode rootNode, boolean expand)
    {
        // Traverse tree from root
        expandAll(new TreePath(rootNode.getPath()), expand);
    }

    /**
     * @return Whether an expandPath was called for the last node in the parent
     * path
     */
    private boolean expandAll(TreePath parent, boolean expand)
    {
        // Traverse children
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)parent.getLastPathComponent();
        if (node.getChildCount() > 0)
        {
            boolean childExpandCalled = false;
            for (Enumeration e = node.children(); e.hasMoreElements();)
            {
                DefaultMutableTreeNode n = (DefaultMutableTreeNode)e.nextElement();
                TreePath path = parent.pathByAddingChild(n);
                childExpandCalled = expandAll(path, expand) || childExpandCalled; // the OR order is important here, don't let childExpand first. func calls will be optimized out !
            }

            if (!childExpandCalled || !expand)
            { // only if one of the children hasn't called already expand
                // Expansion or collapse must be done bottom-up, BUT only for non-leaf nodes
                if (expand)
                {
                    expandPath(parent);
                }
                else
                {
                    collapsePath(parent);
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public void refreshNode(DefaultMutableTreeNode node)
    {
        DefaultTreeModel model;

        model = (DefaultTreeModel)getModel();
        model.nodeChanged(node);
    }

    public void refreshSelectedNode()
    {
        DefaultMutableTreeNode selectedNode;

        selectedNode = getSelectedNode();
        if (selectedNode != null)
        {
            refreshNode(selectedNode);
        }
    }

    public void refreshSelectedNodes()
    {
        List<DefaultMutableTreeNode> selectedNodes;

        selectedNodes = getSelectedNodes();
        for (DefaultMutableTreeNode selectedNode : selectedNodes)
        {
            refreshNode(selectedNode);
        }
    }

    public List<DefaultMutableTreeNode> findDefinitionNodes(String text)
    {
        return findDefinitionNodes(text, false);
    }

    public List<DefaultMutableTreeNode> findDefinitionNodes(String text, boolean includeDatums)
    {
        DefaultMutableTreeNode rootNode;

        rootNode = getRootNode();
        if (rootNode != null)
        {
            return findDescendantDefinitionNodes(rootNode, text, includeDatums);
        }
        else return new ArrayList<DefaultMutableTreeNode>();
    }

    public List<DefaultMutableTreeNode> findDescendantDefinitionNodes(DefaultMutableTreeNode startNode, String filterString, boolean includeDatums)
    {
        List<DefaultMutableTreeNode> foundNodes;
        Object nodeObject;

        foundNodes = new ArrayList<DefaultMutableTreeNode>();
        nodeObject = startNode.getUserObject();
        if (nodeObject instanceof T8Definition)
        {
            T8Definition definition;
            String identifier;

            definition = (T8Definition)nodeObject;
            identifier = definition.getIdentifier();
            if ((filterString == null) || (filterString.trim().length() == 0))
            {
                foundNodes.add(startNode);
            }
            else if ((identifier.toUpperCase().contains(filterString.toUpperCase())))
            {
                foundNodes.add(startNode);
            }
            else if(includeDatums)
            {
                //Check if any of the datums contain the text
                for (Object object : definition.getDefinitionDatums().values())
                {
                    if(object != null && object.toString().toUpperCase().contains(filterString.toUpperCase()))
                    {
                        foundNodes.add(startNode);
                        //break because we only want the node to be added once
                        break;
                    }
                }
            }
//            else if (FuzzyStringUtilities.computeSimilarity(filterString, identifier) > 0.9)
//            {
//                foundNodes.add(startNode);
//            }
        }

        // Add all nodes found in the descendants of the start node to the collection.
        for (int childIndex = 0; childIndex < startNode.getChildCount(); childIndex++)
        {
            foundNodes.addAll(findDescendantDefinitionNodes((DefaultMutableTreeNode)startNode.getChildAt(childIndex), filterString, includeDatums));
        }

        return foundNodes;
    }

    public List<DefaultMutableTreeNode> getAllDescendantNodes(DefaultMutableTreeNode startNode)
    {
        List<DefaultMutableTreeNode> nodes;

        // Add all of the descendants of the start node.
        nodes = new ArrayList<DefaultMutableTreeNode>();
        for (int childIndex = 0; childIndex < startNode.getChildCount(); childIndex++)
        {
            DefaultMutableTreeNode childNode;

            childNode = (DefaultMutableTreeNode)startNode.getChildAt(childIndex);
            nodes.add(childNode);
            nodes.addAll(getAllDescendantNodes(childNode));
        }

        return nodes;
    }

    private DefaultMutableTreeNode getNodeAtLocation(int x, int y)
    {
        TreePath selectedPath;

        selectedPath = getPathForLocation(x, y);
        if (selectedPath != null)
        {
            return (DefaultMutableTreeNode)selectedPath.getLastPathComponent();
        }
        else return null;
    }

    public DefaultMutableTreeNode getSelectedNode()
    {
        TreePath selectedPath;

        selectedPath = getSelectionPath();
        if (selectedPath != null)
        {
            return (DefaultMutableTreeNode)selectedPath.getLastPathComponent();
        }
        else return null;
    }

    public List<DefaultMutableTreeNode> getSelectedNodes()
    {
        TreePath[] selectedPaths;
        List<DefaultMutableTreeNode> selectedNodes;

        selectedNodes = new ArrayList<DefaultMutableTreeNode>();
        selectedPaths = getSelectionPaths();
        if (selectedPaths != null)
        {
            for (TreePath selectedPath : selectedPaths)
            {
                selectedNodes.add((DefaultMutableTreeNode)selectedPath.getLastPathComponent());
            }
        }

        return selectedNodes;
    }

    public void setSelectedNode(DefaultMutableTreeNode node)
    {
        clearSelection();
        if (node != null)
        {
            TreePath path;

            path = new TreePath(node.getPath());
            setSelectionPath(path);
            scrollPathToVisible(path);
        }
    }

    public void setDefinitionValidationErrors(
                                              List<T8DefinitionValidationError> definitionValidationErrors)
    {
        this.navigatorTreeCellRenderer.setDefinitionValidationErrors(definitionValidationErrors);

        if(definitionValidationErrors != null)
        {
            for (T8DefinitionValidationError t8DefinitionValidationError : definitionValidationErrors)
            {
                for (DefaultMutableTreeNode defaultMutableTreeNode : findDefinitionNodes(T8IdentifierUtilities.getLocalIdentifierPart(t8DefinitionValidationError.getDefinitionIdentifier())))
                {
                    for (TreeNode treeNode : defaultMutableTreeNode.getPath())
                    {
                        expandNode((DefaultMutableTreeNode) treeNode);
                    }
                }
            }
        }

        repaint();
    }

    public boolean isNodeSelected(DefaultMutableTreeNode node)
    {
        return getSelectionModel().isPathSelected(new TreePath(node.getPath()));
    }

    private void displayPopupMenu(Component targetComponent, int x, int y)
    {
        TreePath treePath;

        treePath = this.getPathForLocation(x, y);
        if (treePath == null) // No Node right-clicked.
        {
            this.clearSelection();
        }
        else // Node right-clicked.
        {
            DefaultMutableTreeNode targetNode;
            List<DefaultMutableTreeNode> selectedNodes;

            // Get all currently selected nodes as well as the target node (at the mouse cursor location).
            selectedNodes = getSelectedNodes();
            targetNode = (DefaultMutableTreeNode)treePath.getLastPathComponent();

            // Check whether or not any nodes are currently selected.
            if (selectedNodes.size() > 0)
            {
                // If the target node is not part of the selection, override the current selection by selecting the target node.
                if ((targetNode != null) && (!selectedNodes.contains(targetNode)))
                {
                    setSelectionPath(treePath);
                    displayDefinitionPopupMenu(ArrayLists.newArrayList(targetNode), targetComponent, x, y);
                }
                else
                {
                    displayDefinitionPopupMenu(selectedNodes, targetComponent, x, y);
                }
            }
            else if (targetNode != null) // No nodes currently selected, so select target node and display popup.
            {
                setSelectionPath(treePath);
                displayDefinitionPopupMenu(ArrayLists.newArrayList(targetNode), targetComponent, x, y);
            }
        }
    }

    private void displayDefinitionPopupMenu(final List<DefaultMutableTreeNode> selectedNodes, Component targetComponent, int x, int y)
    {
        final T8Definition selectedDefinition;
        final List<T8Definition> selectedDefinitions;
        final T8DefinitionDatumType selectedDatumType;
        final DefaultMutableTreeNode selectedNode;
        JPopupMenu treePopupMenu;
        NavigatorMenuItem menuItem;
        Object nodeObject;

        // Get the first selected node (for some default actions that are only applicable to one node at a time).
        selectedNode = selectedNodes.get(0);

        // Get the selected definition.
        nodeObject = selectedNode.getUserObject();
        if (nodeObject instanceof T8Definition)
        {
            selectedDefinition = (T8Definition)nodeObject;
            selectedDatumType = null;
        }
        else
        {
            T8DefinitionNavigatorDatum datum;

            datum = (T8DefinitionNavigatorDatum)nodeObject;
            selectedDefinition = (T8Definition)((DefaultMutableTreeNode)selectedNode.getParent()).getUserObject();
            selectedDatumType = selectedDefinition.getDatumType(datum.getDatumIdentifier());
        }


        // Get all selected definitions.
        selectedDefinitions = new ArrayList<T8Definition>();
        for (DefaultMutableTreeNode node : selectedNodes)
        {
            nodeObject = node.getUserObject();
            if (nodeObject instanceof T8Definition)
            {
                selectedDefinitions.add((T8Definition)nodeObject);
            }
        }

        // Create the ActionListener for the menu items.
        ActionListener menuListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                NavigatorMenuItem menuItem;
                String code;

                menuItem = (NavigatorMenuItem)event.getSource();
                code = menuItem.getCode();
                if (code.equals("ADD_DEFINITION"))
                {
                    T8DefinitionConstructorParameters constructorParameters;
                    T8DefinitionDatumOption datumOption;
                    String definitionTypeIdentifier;
                    String datumIdentifier;
                    String identifier;

                    datumOption = (T8DefinitionDatumOption)menuItem.getDataObject();
                    constructorParameters = (T8DefinitionConstructorParameters)datumOption.getValue();
                    definitionTypeIdentifier = constructorParameters.getTypeIdentifier();
                    identifier = constructorParameters.getIdentifier();
                    datumIdentifier = menuItem.getDatumIdentifier();

                    try
                    {
                        T8DefinitionMetaData metaData;

                        // If no identifier was supplied as part of the constructor parameters, get a new one from the user.
                        if (identifier == null)
                        {
                            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, T8DefinitionNavigatorTree.this, definitionManager.getDefinitionTypeMetaData(definitionTypeIdentifier), selectedDefinition);
                            if (metaData != null) identifier = metaData.getId();
                        }
                        else metaData = null;

                        if (identifier != null)
                        {
                            ArrayList<Object> parameters;

                            parameters = constructorParameters.getParameters();
                            if (parameters == null)
                            {
                                T8Definition newDefinition;

                                newDefinition = definitionManager.createNewDefinition(identifier, definitionTypeIdentifier);
                                if (metaData != null) newDefinition.setProjectIdentifier(metaData.getProjectId());
                                addDefinition(selectedNode, selectedDefinition.getDatumType(datumIdentifier), newDefinition);
                            }
                            else
                            {
                                T8Definition newDefinition;

                                newDefinition = definitionManager.createNewDefinition(identifier, definitionTypeIdentifier, parameters);
                                if (metaData != null) newDefinition.setProjectIdentifier(metaData.getProjectId());
                                addDefinition(selectedNode, selectedDefinition.getDatumType(datumIdentifier), newDefinition);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        T8Log.log("Exception while creating new definition of type: " + definitionTypeIdentifier, e);
                    }
                }
                else if (code.equals("COPY_DEFINITION"))
                {
                    copyAndPasteHandler.copyDefinition(selectedDefinition);
                }
                else if (code.equals("CUT_DEFINITION"))
                {
                    cutDefinition(selectedDefinition);
                }
                else if (code.equals("PASTE_COPIED_DEFINITION"))
                {
                    T8Definition copiedDefinition;
                    String targetDatumIdentifier;

                    targetDatumIdentifier = menuItem.getDatumIdentifier();
                    copiedDefinition = copyAndPasteHandler.getCopiedDefinition();
                    pasteDefinition(selectedDefinition, targetDatumIdentifier, copiedDefinition);
                }
                else if (code.equals("RENAME_DEFINITION"))
                {
                    renameDefinition(selectedDefinition);
                }
                else if (code.equals("RENAME_SELECTED_DEFINITIONS"))
                {
                    T8DefinitionIdentifierRenamingDialog.renameDefinitions(definitionContext, T8DefinitionNavigatorTree.this, getRootDefinition(), selectedDefinitions, true);
                    refreshSelectedNodes();
                }
                else if (code.equals("REFRESH_DEFINITION"))
                {
                    rebuildNode(selectedNode);
                }
                else if (code.equals("DELETE_DEFINITION"))
                {
                    deleteNode(selectedNode);
                }
                else if (code.equals("FIND_DEFINITION_USAGES"))
                {
                    parentView.addView(viewFactory.createDefinitionUsagesManager(parentView, selectedDefinition));
                }
                else if (code.equals("EXPAND_DESCENDANTS"))
                {
                    expandNodeDescendants(selectedNode);
                }
                else if (code.equals("COLLAPSE_DESCENDANTS"))
                {
                    collapseNodeDescendants(selectedNode);
                }
                else if (code.equals("HIDE_DATUM_DESCENDANTS"))
                {
                    if(selectedDatumType != null)
                    {
                        HIDDEN_DATUMS.add(selectedDatumType.getIdentifier());
                    }
                }
                else if (code.equals("UNHIDE_DATUM_DESCENDANTS"))
                {
                    if(selectedDatumType != null)
                    {
                        HIDDEN_DATUMS.remove(selectedDatumType.getIdentifier());
                    }
                }
                else if (code.equals("SORT_DEFINITION_LIST_ALPHABETICALLY"))
                {
                    List<T8Definition> definitionList;

                    // Get the list of definitions to sort.
                    definitionList = (List<T8Definition>)selectedDefinition.getDefinitionDatum(selectedDatumType.getIdentifier());
                    if (definitionList != null)
                    {
                        // Sort the list of definition alphabetically according to their identifiers.
                        Collections.sort(definitionList, new T8DefinitionComparator());

                        // Put the list of definitions back in the parent definition where they were taken from.
                        selectedDefinition.setDefinitionDatum(selectedDatumType.getIdentifier(), definitionList);

                        // Refresh the parent node to reflect the changes.
                        rebuildNode((DefaultMutableTreeNode)selectedNode.getParent());
                    }
                }
            }
        };

        // Create a new popup menu.
        treePopupMenu = new JPopupMenu();

        // Create the add definition sub-menu.
        if (!locked)
        {
            try
            {
                JMenu subMenu;
                ArrayList<T8DefinitionDatumType> datumTypes;

                datumTypes = selectedDefinition.getSubDefinitionDatumTypes();
                for (T8DefinitionDatumType datumType : datumTypes)
                {
                    if (datumType.isMutable())
                    {
                        if (datumType.isSingleDefinitionType())
                        {
                            ArrayList<T8DefinitionDatumOption> datumOptions;

                            datumOptions = selectedDefinition.getDatumOptions(definitionContext, datumType.getIdentifier());
                            if (datumOptions != null)
                            {
                                // Sor the options lexicographically by display name.
                                Collections.sort(datumOptions, new T8DefinitionDatumOption.DisplayNameComparator());

                                // If there are many options, show a list popup, otherwise just build a normal sub-menu.
                                if (datumOptions.size() < 10)
                                {
                                    subMenu = new JMenu("Set " + datumType.getDisplayName());
                                    for (T8DefinitionDatumOption datumOption : datumOptions)
                                    {
                                        menuItem = new NavigatorMenuItem(datumOption.getDisplayName(), "ADD_DEFINITION", datumType.getIdentifier(), datumOption);
                                        menuItem.addActionListener(menuListener);
                                        menuItem.setToolTipText(datumType.getDescription());
                                        subMenu.add(menuItem);
                                    }

                                    treePopupMenu.add(subMenu);
                                }
                                else
                                {
                                    SearchableListPopup<NavigatorMenuItem> listMenu;
                                    List<NavigatorMenuItem> menuItems;


                                    menuItems = new ArrayList<>();
                                    for (T8DefinitionDatumOption datumOption : datumOptions)
                                    {
                                        menuItem = new NavigatorMenuItem(datumOption.getDisplayName(), "ADD_DEFINITION", datumType.getIdentifier(), datumOption);
                                        menuItem.addActionListener(menuListener);
                                        menuItem.setToolTipText(datumType.getDescription());
                                        menuItems.add(menuItem);
                                    }

                                    listMenu = new SearchableListPopup<>(menuItems);
                                    listMenu.addPopupListener(new NavigatorListPopupListener());
                                    listMenu.setCellRenderer(new NavigatorMenuItemCellRenderer());
                                    subMenu = new ListSubMenu("Set " + datumType.getDisplayName(), listMenu);
                                    subMenu.setMenuLocation(x, y);
                                    treePopupMenu.add(subMenu);
                                }
                            }
                        }
                    }
                }

                for (T8DefinitionDatumType datumType : datumTypes)
                {
                    if (datumType.isMutable())
                    {
                        if (datumType.isDefinitionListType())
                        {
                            ArrayList<T8DefinitionDatumOption> datumOptions;

                            datumOptions = selectedDefinition.getDatumOptions(definitionContext, datumType.getIdentifier());
                            if (datumOptions != null)
                            {
                                // Sor the options lexicographically by display name.
                                Collections.sort(datumOptions, new T8DefinitionDatumOption.DisplayNameComparator());

                                // If there are many options, show a list popup, otherwise just build a normal sub-menu.
                                if (datumOptions.size() < 10)
                                {
                                    subMenu = new JMenu("Add to " + datumType.getDisplayName());
                                    for (T8DefinitionDatumOption datumOption : datumOptions)
                                    {
                                        menuItem = new NavigatorMenuItem(datumOption.getDisplayName(), "ADD_DEFINITION", datumType.getIdentifier(), datumOption);
                                        menuItem.addActionListener(menuListener);
                                        menuItem.setToolTipText(datumType.getDescription());
                                        subMenu.add(menuItem);
                                    }

                                    treePopupMenu.add(subMenu);
                                }
                                else
                                {
                                    SearchableListPopup<NavigatorMenuItem> listMenu;
                                    List<NavigatorMenuItem> menuItems;

                                    menuItems = new ArrayList<>();
                                    for (T8DefinitionDatumOption datumOption : datumOptions)
                                    {
                                        menuItem = new NavigatorMenuItem(datumOption.getDisplayName(), "ADD_DEFINITION", datumType.getIdentifier(), datumOption);
                                        menuItem.addActionListener(menuListener);
                                        menuItem.setToolTipText(datumType.getDescription());
                                        menuItems.add(menuItem);
                                    }

                                    listMenu = new SearchableListPopup<>(menuItems);
                                    listMenu.addPopupListener(new NavigatorListPopupListener());
                                    listMenu.setCellRenderer(new NavigatorMenuItemCellRenderer());
                                    subMenu = new ListSubMenu("Add to " + datumType.getDisplayName(), listMenu);
                                    subMenu.setMenuLocation(x, y);
                                    treePopupMenu.add(subMenu);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                T8Log.log("Definition datum options could not be resolved successfully.", e);
            }
        }

        // Options only available when a definition is selected and not a datum.
        if ((selectedDefinition != null) && (selectedDatumType == null))
        {
            if (!locked)
            {
                menuItem = new NavigatorMenuItem("Rename Definition", "RENAME_DEFINITION", null, null);
                menuItem.addActionListener(menuListener);
                treePopupMenu.add(menuItem);

                // If more than one definition is selected, add the option to rename all of them at once.
                if (selectedDefinitions.size() > 1)
                {
                    menuItem = new NavigatorMenuItem("Rename All Selected Definitions", "RENAME_SELECTED_DEFINITIONS", null, null);
                    menuItem.addActionListener(menuListener);
                    treePopupMenu.add(menuItem);
                }
            }

            menuItem = new NavigatorMenuItem("Refresh Definition", "REFRESH_DEFINITION", null, null);
            menuItem.addActionListener(menuListener);
            treePopupMenu.add(menuItem);

            menuItem = new NavigatorMenuItem("Copy Definition", "COPY_DEFINITION", null, null);
            menuItem.addActionListener(menuListener);
            treePopupMenu.add(menuItem);

            if (!locked)
            {
                menuItem = new NavigatorMenuItem("Cut Definition", "CUT_DEFINITION", null, null);
                menuItem.addActionListener(menuListener);
                treePopupMenu.add(menuItem);
            }
        }

        // Options only available when a definition datum is selected.
        if ((selectedDefinition != null) && (selectedDatumType != null))
        {
            if (!locked)
            {
                if (selectedDatumType.isDefinitionListType())
                {
                    menuItem = new NavigatorMenuItem("Sort Definitions Alphabetically", "SORT_DEFINITION_LIST_ALPHABETICALLY", null, null);
                    menuItem.addActionListener(menuListener);
                    treePopupMenu.add(menuItem);
                }
            }
        }

        if (copyAndPasteHandler.hasCopiedDefinition() && (selectedDefinition != null) && (!locked))
        {
            JMenu subMenu;

            subMenu = new JMenu("Paste Definition");
            for (T8DefinitionDatumType datumType : selectedDefinition.getDatumTypes())
            {
                menuItem = new NavigatorMenuItem(datumType.getDisplayName(), "PASTE_COPIED_DEFINITION", datumType.getIdentifier(), datumType);
                menuItem.addActionListener(menuListener);
                subMenu.add(menuItem);
            }

            treePopupMenu.add(subMenu);
        }

        if(selectedDatumType != null)
        {
            menuItem = new NavigatorMenuItem("Hide Descendants", "HIDE_DATUM_DESCENDANTS", null, null);
            menuItem.addActionListener(menuListener);
            treePopupMenu.add(menuItem);

            menuItem = new NavigatorMenuItem("UnHide Descendants", "UNHIDE_DATUM_DESCENDANTS", null, null);
            menuItem.addActionListener(menuListener);
            treePopupMenu.add(menuItem);
        }

        menuItem = new NavigatorMenuItem("Delete Definition", "DELETE_DEFINITION", null, null);
        menuItem.addActionListener(menuListener);
        treePopupMenu.add(menuItem);

        menuItem = new NavigatorMenuItem("Find Usages", "FIND_DEFINITION_USAGES", null, null);
        menuItem.addActionListener(menuListener);
        treePopupMenu.add(menuItem);

        menuItem = new NavigatorMenuItem("Expand Tree", "EXPAND_DESCENDANTS", null, null);
        menuItem.addActionListener(menuListener);
        treePopupMenu.add(menuItem);
        menuItem = new NavigatorMenuItem("Collapse Tree", "COLLAPSE_DESCENDANTS", null, null);
        menuItem.addActionListener(menuListener);
        treePopupMenu.add(menuItem);

        // Display the newly constructed popup menu.
        treePopupMenu.show(targetComponent, x, y);
    }

    private void moveSelectedNodeUp()
    {
        if (!locked)
        {
            DefaultMutableTreeNode selectedNode;

            selectedNode = getSelectedNode();
            if (selectedNode != null)
            {
                DefaultMutableTreeNode parentNode;

                parentNode = getParentDefinitionNode(selectedNode);
                if (parentNode != null)
                {
                    Object userObject;

                    userObject = selectedNode.getUserObject();
                    if (userObject instanceof T8Definition)
                    {
                        T8Definition parentDefinition;
                        T8Definition selectedDefinition;
                        int index;

                        parentDefinition = (T8Definition)parentNode.getUserObject();
                        selectedDefinition = (T8Definition)userObject;
                        index = parentDefinition.getSubDefinitionIndex(selectedDefinition);
                        if (index > 0)
                        {
                            parentDefinition.setSubDefinitionIndex(selectedDefinition, index-1);
                            rebuildNode(parentNode);
                            setSelectedNode(getDefinitionNode(selectedDefinition));
                        }
                    }
                }
            }
        }
    }

    private void moveSelectedNodeDown()
    {
        if (!locked)
        {
            DefaultMutableTreeNode selectedNode;

            selectedNode = getSelectedNode();
            if (selectedNode != null)
            {
                DefaultMutableTreeNode parentNode;

                parentNode = getParentDefinitionNode(selectedNode);
                if (parentNode != null)
                {
                    Object userObject;

                    userObject = selectedNode.getUserObject();
                    if (userObject instanceof T8Definition)
                    {
                        T8Definition parentDefinition;
                        T8Definition selectedDefinition;
                        T8DefinitionDatumType datumType;
                        int index;

                        parentDefinition = (T8Definition)parentNode.getUserObject();
                        selectedDefinition = (T8Definition)userObject;
                        datumType = parentDefinition.getSubDefinitionDatumType(selectedDefinition);
                        index = parentDefinition.getSubDefinitionIndex(selectedDefinition);
                        if (index < parentDefinition.getDatumSize(datumType.getIdentifier()) - 1)
                        {
                            parentDefinition.setSubDefinitionIndex(selectedDefinition, index+1);
                            rebuildNode(parentNode);
                            setSelectedNode(getDefinitionNode(selectedDefinition));
                        }
                    }
                }
            }
        }
    }

    class T8DefinitionNavigatorDatum
    {
        private T8Definition definition;
        private String datumIdentifier;
        private boolean hidden;

        public T8DefinitionNavigatorDatum(T8Definition definition, String datumIdentifier)
        {
            this.definition = definition;
            this.datumIdentifier = datumIdentifier;
        }

        public T8Definition getDefinition()
        {
            return definition;
        }

        public String getDatumIdentifier()
        {
            return datumIdentifier;
        }

        public String getHandleIdentifier()
        {
            return definition.getIdentifier() + ":" + datumIdentifier;
        }

        public void setHidden(boolean hidden)
        {
            this.hidden = hidden;
        }

        public boolean isHidden()
        {
            return hidden;
        }
    }

    private class NavigatorMenuItem extends JMenuItem
    {
        private String code;
        private String datumIdentifier;
        private Object dataObject;

        public NavigatorMenuItem(String text, String code, String datumIdentifier, Object dataObject)
        {
            super(text);
            this.code = code;
            this.datumIdentifier = datumIdentifier;
            this.dataObject = dataObject;
        }

        public String getCode()
        {
            return code;
        }

        public String getDatumIdentifier()
        {
            return datumIdentifier;
        }

        public Object getDataObject()
        {
            return dataObject;
        }

        @Override
        public String toString()
        {
            return getText();
        }
    }

    private class NavigatorMenuItemCellRenderer implements ListCellRenderer
    {
        private ListCellRenderer defaultRenderer;

        public NavigatorMenuItemCellRenderer()
        {
            defaultRenderer = new DefaultListCellRenderer();
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            return defaultRenderer.getListCellRendererComponent(list, ((NavigatorMenuItem)value).getText(), index, isSelected, cellHasFocus);
        }
    }

    /**
     * This method relays a selection event on a List Popup to the regular
     * action performed event listener on the selected navigator menu item.
     */
    private class NavigatorListPopupListener implements ListPopupMenuListener
    {
        public NavigatorListPopupListener()
        {

        }

        @Override
        public void popupItemSelected(Object o)
        {
            if (o instanceof NavigatorMenuItem)
            {
                ((NavigatorMenuItem)o).doClick();
            }
        }

        @Override
        public void popupItemsSelected(Object[] selectedItems)
        {
        }
    }

    private class ModuleNavigatorKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if ((e.getKeyCode() == KeyEvent.VK_UP) && (e.isControlDown()))
            {
                moveSelectedNodeUp();
            }
            else if ((e.getKeyCode() == KeyEvent.VK_DOWN) && (e.isControlDown()))
            {
                moveSelectedNodeDown();
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    private class ModuleNavigatorMouseListener implements MouseListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopupMenu(e.getComponent(), e.getX(), e.getY());
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopupMenu(e.getComponent(), e.getX(), e.getY());
            }

            if (getNodeAtLocation(e.getX(), e.getY()) == null)
            {
                clearSelection();
            }
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopupMenu(e.getComponent(), e.getX(), e.getY());
            }
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }
}
