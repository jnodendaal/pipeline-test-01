package com.pilog.t8.developer.definitions.actionhandlers.datarecord.access;

import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class DataRecordAccessFieldNode extends DataRecordAccessNode
{
    private static final T8Logger LOGGER = T8Log.getLogger(DataRecordAccessFieldNode.class);

    private final FieldRequirement fieldRequirement;

    public DataRecordAccessFieldNode(FieldRequirement fieldRequirement, DataRecordAccessNode parentNode)
    {
        super(parentNode);
        this.fieldRequirement = fieldRequirement;
    }

    @Override
    public void loadChildren(T8Context context, T8OntologyClientApi ontApi, T8DataRequirementClientApi drqApi)
    {
        if (!isChildrenLoaded())
        {
            ValueRequirement valueRequirement;

            valueRequirement = fieldRequirement.getFirstSubRequirement();
            if (valueRequirement instanceof DocumentReferenceRequirement)
            {
                try
                {
                    T8OntologyConcept classConcept;
                    DocumentReferenceRequirement referenceRequirement;

                    referenceRequirement = (DocumentReferenceRequirement)valueRequirement;
                    classConcept = ontApi.retrieveConcept(referenceRequirement.getOntologyClassId(), false, false, false, false, false);
                    if (referenceRequirement.isPrescribed())
                    {
                        addChild(new DataRecordAccessPrescribedValueNode(classConcept, fieldRequirement.getParentPropertyRequirement(), fieldRequirement, this));
                    }
                    else
                    {
                        addChild(new DataRecordAccessOntologyNode(classConcept, fieldRequirement.getParentPropertyRequirement(), fieldRequirement, this));
                    }
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to load field node children " + fieldRequirement.getFieldID(), ex);
                }
            }

            sortChildren();
            setChildrenLoaded(true);
        }
    }

    @Override
    public DataRequirementInstance getDataRequirementInstance()
    {
        return fieldRequirement.getParentDataRequirementInstance();
    }

    @Override
    public String getConceptID()
    {
        return fieldRequirement.getFieldID();
    }

    @Override
    public String getDocPathRepresentation(String suffix)
    {
        return "F" + suffix + ":" + fieldRequirement.getFieldID();
    }
}
