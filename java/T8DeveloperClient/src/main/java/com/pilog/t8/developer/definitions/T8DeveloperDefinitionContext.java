package com.pilog.t8.developer.definitions;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionComposition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionGroupMetaData;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionLockDetails;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.event.T8DefinitionContextListener;
import com.pilog.t8.definition.event.T8DefinitionLockedEvent;
import com.pilog.t8.definition.event.T8DefinitionRenamedEvent;
import com.pilog.t8.definition.event.T8DefinitionSavedEvent;
import com.pilog.t8.definition.event.T8DefinitionUnlockedEvent;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;
import javax.swing.event.EventListenerList;

/**
 * This class exposes the functionality of the T8DefinitionManager within the
 * context of the T8Developer where certain types of meta data and definitions
 * can be cached on the client-side in specific scenarios.
 *
 * This class is currently incomplete.
 *
 * @author Bouwer du Preez
 */
public class T8DeveloperDefinitionContext implements T8DefinitionContext
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final EventListenerList listeners;

    public T8DeveloperDefinitionContext(T8Context context)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definitionManager = clientContext.getDefinitionManager();
        this.listeners = new EventListenerList();
    }

    @Override
    public T8Context getContext()
    {
        return context;
    }

    @Override
    public T8ClientContext getClientContext()
    {
        return clientContext;
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return context.getSessionContext();
    }

    @Override
    public T8Definition getRawDefinition(String projectId, String definitionId) throws Exception
    {
        return definitionManager.getRawDefinition(context, projectId, definitionId);
    }

    @Override
    public T8DefinitionComposition getDefinitionComposition(String projectId, String definitionId, boolean includeDescendants) throws Exception
    {
        return definitionManager.getDefinitionComposition(context, projectId, definitionId, includeDescendants);
    }

    @Override
    public List<T8DefinitionTypeMetaData> getGroupDefinitionTypeMetaData(String groupId) throws Exception
    {
        return definitionManager.getGroupDefinitionTypeMetaData(groupId);
    }

    @Override
    public T8DefinitionMetaData getDefinitionMetaData(String projectId, String definitionId) throws Exception
    {
        return definitionManager.getDefinitionMetaData(projectId, definitionId);
    }

    @Override
    public List<T8DefinitionMetaData> getProjectDefinitionMetaData(String projectId) throws Exception
    {
        return definitionManager.getProjectDefinitionMetaData(projectId);
    }

    @Override
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String projectId, String groupId) throws Exception
    {
        return definitionManager.getGroupDefinitionMetaData(projectId, groupId);
    }

    @Override
    public List<T8DefinitionTypeMetaData> getAllDefinitionTypeMetaData() throws Exception
    {
        return definitionManager.getAllDefinitionTypeMetaData();
    }

    @Override
    public List<T8DefinitionGroupMetaData> getAllDefinitionGroupMetaData() throws Exception
    {
        return definitionManager.getAllDefinitionGroupMetaData();
    }

    @Override
    public T8DefinitionTypeMetaData getDefinitionTypeMetaData(String typeId) throws Exception
    {
        return definitionManager.getDefinitionTypeMetaData(typeId);
    }

    @Override
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String projectId, String typeId) throws Exception
    {
        return definitionManager.getTypeDefinitionMetaData(projectId, typeId);
    }

    @Override
    public List<String> getGroupDefinitionIdentifiers(String projectId, String groupId) throws Exception
    {
        return definitionManager.getGroupDefinitionIdentifiers(projectId, groupId);
    }

    @Override
    public List<String> getDefinitionIdentifiers(String projectId, String typeId) throws Exception
    {
        return definitionManager.getDefinitionIdentifiers(projectId, typeId);
    }

    @Override
    public List<String> getDefinitionGroupIdentifiers() throws Exception
    {
        return definitionManager.getDefinitionGroupIdentifiers();
    }

    @Override
    public List<String> getDefinitionTypeIdentifiers() throws Exception
    {
        return definitionManager.getDefinitionTypeIdentifiers();
    }

    @Override
    public List<String> getDefinitionTypeIdentifiers(String groupId) throws Exception
    {
        return definitionManager.getDefinitionTypeIdentifiers(groupId);
    }

    @Override
    public List<T8Definition> getRawGroupDefinitions(String projectId, String groupId) throws Exception
    {
        return definitionManager.getRawGroupDefinitions(context, projectId, groupId);
    }

    @Override
    public T8Definition createNewDefinition(String definitionId, String typeId) throws Exception
    {
        return definitionManager.createNewDefinition(definitionId, typeId);
    }

    @Override
    public T8Definition createNewDefinition(String definitionId, String typeId, List parameters) throws Exception
    {
        return definitionManager.createNewDefinition(definitionId, typeId, parameters);
    }

    @Override
    public void removeDefinitionReferences(String definitionId, String keyId) throws Exception
    {
        definitionManager.removeDefinitionReferences(context, definitionId, keyId);
    }

    @Override
    public List<T8DefinitionHandle> renameDefinition(T8Definition definition, String newIdentifier) throws Exception
    {
        List<T8DefinitionHandle> affectedDefinitions;
        String oldIdentifier;

        oldIdentifier = definition.getPublicIdentifier();
        affectedDefinitions = definitionManager.renameDefinition(context, definition, newIdentifier);
        fireDefinitionRenamedEvent(definition, oldIdentifier, affectedDefinitions);
        return affectedDefinitions;
    }

    @Override
    public boolean checkIdentifierAvailability(String projectId, String definitionId) throws Exception
    {
        return definitionManager.checkIdentifierAvailability(context, projectId, definitionId);
    }

    @Override
    public T8DefinitionMetaData saveDefinition(T8Definition definition, String keyIdentifier) throws Exception
    {
        T8DefinitionMetaData metaData;

        metaData = definitionManager.saveDefinition(context, definition, keyIdentifier);
        fireDefinitionSavedEvent(definition);
        return metaData;
    }

    @Override
    public T8Definition getInitializedDefinition(String projectId, String definitionId, Map<String, Object> inputParameters) throws Exception
    {
        return definitionManager.getInitializedDefinition(context, projectId, definitionId, inputParameters);
    }

    @Override
    public List<T8DefinitionHandle> findDefinitionsByText(String searchString) throws Exception
    {
        return definitionManager.findDefinitionsByText(context, searchString);
    }

    @Override
    public List<T8DefinitionHandle> findDefinitionsByRegularExpression(String expression) throws Exception
    {
        return definitionManager.findDefinitionsByRegularExpression(context, expression);
    }

    @Override
    public List<T8DefinitionHandle> findDefinitionsByIdentifier(String identifier, boolean includeReferencesOnly) throws Exception
    {
        return definitionManager.findDefinitionsByIdentifier(context, identifier, includeReferencesOnly);
    }

    @Override
    public void deleteDefinition(String projectId, String definitionId, boolean safeCheck, String keyIdentifier) throws Exception
    {
        definitionManager.deleteDefinition(context, projectId, definitionId, safeCheck, keyIdentifier);
    }

    @Override
    public boolean deleteDefinition(T8DefinitionHandle definitionHandle, boolean safeDeletion, String keyIdentifier) throws Exception
    {
        return definitionManager.deleteDefinition(context, definitionHandle, safeDeletion, keyIdentifier);
    }

    @Override
    public T8Definition lockDefinition(T8DefinitionHandle definitionHandle, String keyIdentifier) throws Exception
    {
        T8Definition lockedDefinition;

        lockedDefinition = definitionManager.lockDefinition(context, definitionHandle, keyIdentifier);
        fireDefinitionLockedEvent(lockedDefinition);
        return lockedDefinition;
    }

    @Override
    public T8Definition unlockDefinition(T8DefinitionHandle definitionHandle, String keyIdentifier) throws Exception
    {
        T8Definition unlockedDefinition;

        unlockedDefinition = definitionManager.unlockDefinition(context, definitionHandle, keyIdentifier);
        fireDefinitionUnlockedEvent(unlockedDefinition);
        return unlockedDefinition;
    }

    @Override
    public T8Definition unlockDefinition(T8Definition definition, String keyIdentifier) throws Exception
    {
        T8Definition unlockedDefinition;

        unlockedDefinition = definitionManager.unlockDefinition(context, definition, keyIdentifier);
        fireDefinitionUnlockedEvent(unlockedDefinition);
        return unlockedDefinition;
    }

    @Override
    public T8DefinitionLockDetails getDefinitionLockDetails(T8DefinitionHandle definitionHandle) throws Exception
    {
        return definitionManager.getDefinitionLockDetails(context, definitionHandle);
    }

    @Override
    public T8Definition copyDefinition(T8Definition definition, String newIdentifier) throws Exception
    {
        return definitionManager.copyDefinition(context, definition, newIdentifier);
    }

    @Override
    public boolean copyDefinitionToProject(String projectId, String definitionId, String newProjectId) throws Exception
    {
        return definitionManager.copyDefinitionToProject(context, projectId, definitionId, newProjectId);
    }

    @Override
    public boolean moveDefinitionToProject(String projectId, String definitionId, String newProjectId) throws Exception
    {
        return definitionManager.moveDefinitionToProject(context, projectId, definitionId, newProjectId);
    }

    @Override
    public void transferDefinitionReferences(String oldIdentifier, String newIdentifier) throws Exception
    {
        definitionManager.transferDefinitionReferences(context, oldIdentifier, newIdentifier);
    }

    @Override
    public T8Definition getRawDefinition(T8DefinitionHandle definitionHandle) throws Exception
    {
        return definitionManager.getRawDefinition(context, definitionHandle);
    }

    private boolean containsListener(T8DefinitionContextListener listenerToFind)
    {
        for (T8DefinitionContextListener listener : listeners.getListeners(T8DefinitionContextListener.class))
        {
            if (listenerToFind == listener) return true;
        }

        return false;
    }

    @Override
    public void addContextListener(T8DefinitionContextListener listener)
    {
        if (!containsListener(listener))
        {
            listeners.add(T8DefinitionContextListener.class, listener);
        }
        else
        {
            throw new RuntimeException("Multiple additions of the same listener not allowed.");
        }
    }

    @Override
    public void removeContextListener(T8DefinitionContextListener listener)
    {
        listeners.remove(T8DefinitionContextListener.class, listener);
    }

    private void fireDefinitionSavedEvent(T8Definition definition)
    {
        T8DefinitionSavedEvent event;

        event = new T8DefinitionSavedEvent(context, definition);
        for (T8DefinitionContextListener listener : listeners.getListeners(T8DefinitionContextListener.class))
        {
            listener.definitionSaved(event);
        }
    }

    private void fireDefinitionLockedEvent(T8Definition definition)
    {
        T8DefinitionLockedEvent event;

        event = new T8DefinitionLockedEvent(context, definition);
        for (T8DefinitionContextListener listener : listeners.getListeners(T8DefinitionContextListener.class))
        {
            listener.definitionLocked(event);
        }
    }

    private void fireDefinitionUnlockedEvent(T8Definition definition)
    {
        T8DefinitionUnlockedEvent event;

        event = new T8DefinitionUnlockedEvent(context, definition);
        for (T8DefinitionContextListener listener : listeners.getListeners(T8DefinitionContextListener.class))
        {
            listener.definitionUnlocked(event);
        }
    }

    private void fireDefinitionRenamedEvent(T8Definition definition, String oldIdentifier, List<T8DefinitionHandle> affectedDefinitions)
    {
        T8DefinitionRenamedEvent event;

        event = new T8DefinitionRenamedEvent(context, definition, oldIdentifier, affectedDefinitions);
        for (T8DefinitionContextListener listener : listeners.getListeners(T8DefinitionContextListener.class))
        {
            listener.definitionRenamed(event);
        }
    }

    @Override
    public T8DefinitionMetaData finalizeDefinition(T8DefinitionHandle definitionHandle, String comment) throws Exception
    {
        return definitionManager.finalizeDefinition(context, definitionHandle, comment);
    }
}
