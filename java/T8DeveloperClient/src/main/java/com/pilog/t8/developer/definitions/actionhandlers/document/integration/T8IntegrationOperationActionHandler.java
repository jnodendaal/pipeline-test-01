package com.pilog.t8.developer.definitions.actionhandlers.document.integration;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationOperationDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationServiceDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationServiceOperationMappingDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.Action;


/**
 * @author Hennie Brink
 */
public class T8IntegrationOperationActionHandler implements T8DefinitionActionHandler
{
    private T8ClientContext clientContext;
    private T8Context context;
    private T8IntegrationOperationDefinition definition;

    public T8IntegrationOperationActionHandler(T8Context context, T8IntegrationOperationDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new ParameterAutoCreationAction(context, definition));

        return actions;
    }

    private static class ParameterAutoCreationAction extends AbstractAction
    {
        private T8ClientContext clientContext;
        private T8Context context;
        private T8IntegrationOperationDefinition definition;

        public ParameterAutoCreationAction(T8Context context, T8IntegrationOperationDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Auto-Create Parameters");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                T8IntegrationServiceDefinition serviceDefinition;

                serviceDefinition = (T8IntegrationServiceDefinition) clientContext.getDefinitionManager().getInitializedDefinition(context, definition.getRootProjectId(), definition.getIntegrationServiceIdentifier(), null);
                if(serviceDefinition != null)
                {
                    T8IntegrationServiceOperationMappingDefinition operationMappingDefinition;
                    List<T8DataParameterDefinition> inputParameters;

                    operationMappingDefinition = (T8IntegrationServiceOperationMappingDefinition) serviceDefinition.getSubDefinition(definition.getIntegrationServiceOperationIdentifier());
                    inputParameters = operationMappingDefinition.getInputParameters();

                    if(inputParameters != null)
                    {
                        Map<String, String> inputParameterMapping;
                        inputParameterMapping = definition.getInputParameterMapping();

                        if(inputParameterMapping == null) inputParameterMapping = new HashMap<>();

                        for (T8DataParameterDefinition inputParameter : inputParameters)
                        {
                            if(!definition.containsIdentifier(inputParameter.getIdentifier()))
                            {
                                T8DataParameterDefinition operationParameter;

                                operationParameter = new T8DataParameterDefinition(inputParameter.getIdentifier());
                                operationParameter.setMetaDisplayName(inputParameter.getMetaDisplayName());
                                operationParameter.setMetaDescription(inputParameter.getMetaDescription());
                                operationParameter.setDataType(inputParameter.getDataType());

                                definition.addInputParameterDefinition(operationParameter);

                                inputParameterMapping.put(operationParameter.getIdentifier(), inputParameter.getPublicIdentifier());
                            }
                        }

                        definition.setInputParameterMapping(inputParameterMapping);
                    }
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving field definitions for data source: " + definition.getIdentifier(), e);
            }
        }
    }
}
