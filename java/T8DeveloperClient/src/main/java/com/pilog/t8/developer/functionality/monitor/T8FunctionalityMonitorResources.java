/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.developer.functionality.monitor;

/**
 *
 * @author Andre Scheepers
 */
public class T8FunctionalityMonitorResources
{    
    //Functionality State Fields.
    public static final String EF_FUNCTIONALITY_IID = "$FUNCTIONALITY_IID";
    public static final String EF_FUNCTIONALITY_ID = "$FUNCTIONALITY_ID";    
    public static final String EF_DATA_OBJECT_IID = "$DATA_OBJECT_IID";    
    public static final String EF_DATA_OBJECT_ID = "$DATA_OBJECT_ID";
    public static final String EF_INITIATOR_IID = "$INITIATOR_IID";    
    public static final String EF_INITIATOR_ID = "$INITIATOR_ID";
    public static final String EF_FLOW_IID = "$FLOW_IID";
    public static final String EF_OPERATION_IID = "$OPERATION_IID";   
    public static final String EF_SESSION_ID = "$SESSION_ID";   
    
    //Functionality Block State Fields.
    public static final String EF_BLOCK_IID = "$BLOCK_IID";
    public static final String EF_BLOCKING_FUNCTIONALITY_IID = "$BLOCKING_FUNCTIONALITY_IID";    
    public static final String EF_BLOCKED_FUNCTIONALITY_ID = "$BLOCKED_FUNCTIONALITY_ID";    
    public static final String EF_BLOCKED_ROOT_ORG_ID = "$BLOCKED_ROOT_ORG_ID";
    public static final String EF_BLOCKED_ORG_ID = "$BLOCKED_ORG_ID";    
    public static final String EF_BLOCKED_DATA_OBJECT_IID = "$BLOCKED_DATA_OBJECT_IID";
    public static final String EF_BLOCKED_DATA_OBJECT_ID = "$BLOCKED_DATA_OBJECT_ID";
    public static final String EF_BLOCKED_USER_ID = "$BLOCKED_USER_ID";   
    public static final String EF_BLOCKED_PROFILE_ID = "$BLOCKED_PROFILE_ID";  
    public static final String EF_BLOCKED_MESSSAGE = "$BLOCKED_MESSSAGE";  
    
    //Event Parameter Fields.  
    public static final String P_DATA_ENTITY_LIST = "$CP_DATA_ENTITY_LIST";
}
