package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.event.T8DefinitionDatumEditorListener;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultDefinitionDatumEditor extends JPanel implements T8DefinitionDatumEditor
{
    protected T8DefinitionContext definitionContext;
    protected T8SessionContext sessionContext;
    protected T8ClientContext clientContext;
    protected T8Context context;
    protected T8Definition definition;
    protected T8DefinitionDatumType datumType;
    protected EventListenerList eventListeners;

    public T8DefaultDefinitionDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        this.definitionContext = definitionContext;
        this.context = definitionContext.getContext();
        this.clientContext = definitionContext.getClientContext();
        this.sessionContext = definitionContext.getSessionContext();
        this.definition = definition;
        this.datumType = datumType;
        this.eventListeners = new EventListenerList();
    }

    @Override
    public T8Context getContext()
    {
        return context;
    }

    @Override
    public T8ClientContext getClientContext()
    {
        return clientContext;
    }

    @Override
    public T8DefinitionContext getDefinitionContext()
    {
        return definitionContext;
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    @Override
    public String getDatumIdentifier()
    {
        return datumType.getIdentifier();
    }

    @Override
    public T8DefinitionDatumType getDatumType()
    {
        return datumType;
    }

    @Override
    public T8Definition getDefinition()
    {
        return definition;
    }

    protected void setDefinitionDatum(Object value)
    {
        definition.setDefinitionDatum(datumType.getIdentifier(), value, this);
    }

    protected Object getDefinitionDatum()
    {
        return definition.getDefinitionDatum(datumType.getIdentifier());
    }

    protected void fireDefinitionLinkActivatedEvent(String projectId, String definitionId)
    {
        T8DefinitionLinkEvent event;

        event = new T8DefinitionLinkEvent(this, projectId, definitionId);
        for (T8DefinitionDatumEditorListener listener : eventListeners.getListeners(T8DefinitionDatumEditorListener.class))
        {
            listener.definitionLinkActivated(event);
        }
    }

    @Override
    public void addDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
        eventListeners.add(T8DefinitionDatumEditorListener.class, listener);
    }

    @Override
    public void removeDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
        eventListeners.remove(T8DefinitionDatumEditorListener.class, listener);
    }

    @Override
    public abstract void initializeComponent();

    @Override
    public abstract void startComponent();

    @Override
    public abstract void commitChanges();

    @Override
    public abstract void refreshEditor();

    @Override
    public abstract void setEditable(boolean editable);
}
