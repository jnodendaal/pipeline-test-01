package com.pilog.t8.developer;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8SecurityManager.T8LoginResponseType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8PasswordChangeResponse;
import com.pilog.t8.security.T8UserLoginResponse;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Arrays;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DeveloperLoginDialog extends javax.swing.JDialog
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DeveloperLoginDialog.class);

    private final T8ClientContext clientContext;
    private final T8Context context;
    private boolean loggedIn;
    private LoginMode loginMode;

    private enum LoginMode {LOGIN, NEW_PASSWORD};

    public T8DeveloperLoginDialog(T8Context context, Component parentComponent)
    {
        super(SwingUtilities.getWindowAncestor(parentComponent), ModalityType.APPLICATION_MODAL);
        initComponents();

        this.loggedIn = false;
        this.clientContext = context.getClientContext();
        this.context = context;
        this.jPasswordField.addKeyListener(new LoginKeyListener());
        jPanelMain.setBorder(new T8ContentHeaderBorder("Developer Login"));

        setSize(500, 220);
        setLocationRelativeTo(null);
        showLoginPanel();
    }

    private void showLoginPanel()
    {
        CardLayout layout;

        layout = (CardLayout)jPanelContent.getLayout();
        layout.show(jPanelContent, LoginMode.LOGIN.toString());
        loginMode = LoginMode.LOGIN;
    }

    private void showNewPasswordPanel()
    {
        CardLayout layout;

        layout = (CardLayout)jPanelContent.getLayout();
        layout.show(jPanelContent, LoginMode.NEW_PASSWORD.toString());
        loginMode = LoginMode.NEW_PASSWORD;
    }

    public static boolean login(T8Context context, Component parentComponent)
    {
        T8DeveloperLoginDialog dialog;

        dialog = new T8DeveloperLoginDialog(context, parentComponent);
        dialog.setVisible(true);
        return dialog.loggedIn;
    }

    private boolean login(boolean endExistingSession)
    {
        if (loginMode == LoginMode.LOGIN)
        {
            String username;
            char[] password;

            username = jTextFieldUsername.getText();
            password = jPasswordField.getPassword();

            try
            {
                T8UserLoginResponse loginResponse;

                loginResponse = clientContext.getSecurityManager().login(context, T8SecurityManager.LoginLocation.DEVELOPER, username, password, endExistingSession);
                if (loginResponse.getResponseType() == T8LoginResponseType.SUCCESS || loginResponse.getResponseType() == T8LoginResponseType.PASSWORD_EXPIRY_WARNING)
                {
                    return true;
                }
                else if (loginResponse.getResponseType() == T8LoginResponseType.SUCCESS_NEW_PASSWORD)
                {
                    showNewPasswordPanel();
                    return false;
                }
                else if (loginResponse.getResponseType() == T8LoginResponseType.USER_ALREADY_LOGGED_IN)
                {
                    if (JOptionPane.showConfirmDialog(this, "User already logged in.  Do you want to end the existing session?", "Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
                    {
                        return login(true);
                    }
                    else return false;
                }
                else if (loginResponse.getResponseType() == T8LoginResponseType.USER_EXPIRED)
                {
                    JOptionPane.showMessageDialog(this, "Your user has expired. Have one of the administrators reset the user.", "User Expired", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                else if (loginResponse.getResponseType() == T8LoginResponseType.PASSWORD_EXPIRED)
                {
                    if (JOptionPane.showConfirmDialog(this, "Password has expired. Would you like to request a password reset now?", "Password Expired", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
                    {
                        if (clientContext.getSecurityManager().resetUserPassword(this.context, this.context.getUserId()))
                        {
                            showNewPasswordPanel();
                        } else JOptionPane.showMessageDialog(this, "Password reset could not be completed. Contact an administrator.", "Password Reset Failed", JOptionPane.ERROR_MESSAGE);
                    }

                    return false;
                }
                else if (loginResponse.getResponseType() == T8LoginResponseType.USER_NOT_ALLOWED)
                {
                    JOptionPane.showMessageDialog(this, "Only Developer or System users allowed for developer login.", loginResponse.getMessage(), JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                else
                {
                    JOptionPane.showMessageDialog(this, "Invalid username or password.", "Login Failure", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while trying to log in developer user: " + context.getUserId(), e);
                return false;
            }
        }
        else
        {
            char[] newPassword;
            char[] confirmedPassword;

            newPassword = jPasswordFieldNew.getPassword();
            confirmedPassword = jPasswordFieldConfirm.getPassword();

            if (Arrays.equals(newPassword, confirmedPassword))
            {
                try
                {
                    T8PasswordChangeResponse changeResponse;

                    changeResponse = clientContext.getSecurityManager().changePassword(context, newPassword);
                    if (!changeResponse.isSuccess())
                    {
                        JOptionPane.showMessageDialog(this, changeResponse.getMessage(), "Password Change Failure", JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                    else return true;
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while trying to change developer password: " + context.getUserId(), e);
                    return false;
                }
            }
            else
            {
                JOptionPane.showMessageDialog(this, "New and confirmed entries do not match.  Please try again.", "Password Change Failure", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
    }

    private class LoginKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                loggedIn = login(false);
                if (loggedIn) dispose();
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jPanelControls = new javax.swing.JPanel();
        jButtonExit = new javax.swing.JButton();
        jButtonLogin = new javax.swing.JButton();
        jPanelContent = new javax.swing.JPanel();
        jPanelLogin = new javax.swing.JPanel();
        jLabelMessage = new javax.swing.JLabel();
        jLabelUsername = new javax.swing.JLabel();
        jLabelPassword = new javax.swing.JLabel();
        jTextFieldUsername = new javax.swing.JTextField();
        jPasswordField = new javax.swing.JPasswordField();
        jPanelLoginNewPassword = new javax.swing.JPanel();
        jLabelNewPasswordMessage = new javax.swing.JLabel();
        jLabelNewPassword = new javax.swing.JLabel();
        jLabelConfirmNewPassword = new javax.swing.JLabel();
        jPasswordFieldNew = new javax.swing.JPasswordField();
        jPasswordFieldConfirm = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jPanelControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 2, 2));

        jButtonExit.setText("Exit");
        jButtonExit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonExitActionPerformed(evt);
            }
        });
        jPanelControls.add(jButtonExit);

        jButtonLogin.setText("Login");
        jButtonLogin.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonLoginActionPerformed(evt);
            }
        });
        jPanelControls.add(jButtonLogin);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelMain.add(jPanelControls, gridBagConstraints);

        jPanelContent.setLayout(new java.awt.CardLayout());

        jPanelLogin.setLayout(new java.awt.GridBagLayout());

        jLabelMessage.setText("Please enter you username and password below:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 6, 5);
        jPanelLogin.add(jLabelMessage, gridBagConstraints);

        jLabelUsername.setText("Username:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLogin.add(jLabelUsername, gridBagConstraints);

        jLabelPassword.setText("Password:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLogin.add(jLabelPassword, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelLogin.add(jTextFieldUsername, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelLogin.add(jPasswordField, gridBagConstraints);

        jPanelContent.add(jPanelLogin, "LOGIN");

        jPanelLoginNewPassword.setLayout(new java.awt.GridBagLayout());

        jLabelNewPasswordMessage.setText("Your password has been reset.  Please enter a new password.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 6, 5);
        jPanelLoginNewPassword.add(jLabelNewPasswordMessage, gridBagConstraints);

        jLabelNewPassword.setText("New Password:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLoginNewPassword.add(jLabelNewPassword, gridBagConstraints);

        jLabelConfirmNewPassword.setText("Confirm New Password:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLoginNewPassword.add(jLabelConfirmNewPassword, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelLoginNewPassword.add(jPasswordFieldNew, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelLoginNewPassword.add(jPasswordFieldConfirm, gridBagConstraints);

        jPanelContent.add(jPanelLoginNewPassword, "NEW_PASSWORD");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelMain.add(jPanelContent, gridBagConstraints);

        getContentPane().add(jPanelMain, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonLoginActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonLoginActionPerformed
    {//GEN-HEADEREND:event_jButtonLoginActionPerformed
        loggedIn = login(false);
        if (loggedIn) dispose();
    }//GEN-LAST:event_jButtonLoginActionPerformed

    private void jButtonExitActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExitActionPerformed
    {//GEN-HEADEREND:event_jButtonExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButtonExitActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonExit;
    private javax.swing.JButton jButtonLogin;
    private javax.swing.JLabel jLabelConfirmNewPassword;
    private javax.swing.JLabel jLabelMessage;
    private javax.swing.JLabel jLabelNewPassword;
    private javax.swing.JLabel jLabelNewPasswordMessage;
    private javax.swing.JLabel jLabelPassword;
    private javax.swing.JLabel jLabelUsername;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelControls;
    private javax.swing.JPanel jPanelLogin;
    private javax.swing.JPanel jPanelLoginNewPassword;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JPasswordField jPasswordField;
    private javax.swing.JPasswordField jPasswordFieldConfirm;
    private javax.swing.JPasswordField jPasswordFieldNew;
    private javax.swing.JTextField jTextFieldUsername;
    // End of variables declaration//GEN-END:variables
}
