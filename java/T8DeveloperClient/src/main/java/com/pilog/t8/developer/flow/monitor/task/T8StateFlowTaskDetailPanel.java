package com.pilog.t8.developer.flow.monitor.task;

import com.pilog.t8.ui.T8ComponentController;
import java.awt.BorderLayout;

/**
 * @author Andre Scheepers
 */
public class T8StateFlowTaskDetailPanel extends javax.swing.JPanel
{
    private final T8StateFlowTaskInPanel flowTaskInPanel;

    public T8StateFlowTaskDetailPanel(T8ComponentController controller) throws Exception
    {
        initComponents();
        flowTaskInPanel = new T8StateFlowTaskInPanel(controller);
        add(flowTaskInPanel, BorderLayout.CENTER);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    public T8StateFlowTaskInPanel getFlowTaskInPanel()
    {
        return flowTaskInPanel;
    }
}
