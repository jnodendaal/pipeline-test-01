package com.pilog.t8.developer.definitions.actionhandlers.operation.java;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.operation.java.T8DynamicJavaOperationDefinition;
import com.pilog.t8.developer.definitions.dialog.T8InformationDialog;
import com.pilog.t8.system.T8OperationCompilationResult;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.security.T8Context;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.Action;

import static com.pilog.t8.definition.system.T8AdministrationResource.*;

/**
 * @author Hennie Brink
 */
public class T8DynamicJavaOperationActionHandler implements T8DefinitionActionHandler
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DynamicJavaOperationDefinition definition;

    public T8DynamicJavaOperationActionHandler(T8Context context, T8DynamicJavaOperationDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new CompilationAction(context, definition));
        return actions;
    }

    private static class CompilationAction extends AbstractAction
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8DynamicJavaOperationDefinition definition;

        public CompilationAction(T8Context context, T8DynamicJavaOperationDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Compile Operation");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/gearPencilIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (!definition.isUpdated())
                {
                    Map<String, Object> inputParameters;
                    T8OperationCompilationResult result;

                    inputParameters = new HashMap<>();
                    inputParameters.put(PARAMETER_PROJECT_ID, definition.getRootProjectId());
                    inputParameters.put(PARAMETER_DEFINITION_ID, definition.getIdentifier());
                    result = (T8OperationCompilationResult)T8MainServerClient.executeSynchronousOperation(context, OPERATION_COMPILE_OPERATION, inputParameters).get(PARAMETER_COMPILATION_RESULT);
                    if (result.isSuccess())
                    {
                        Toast.show("Compilation Successful", Toast.Style.SUCCESS);
                    }
                    else
                    {
                        Toast.show("Compilation Failure", Toast.Style.ERROR);
                        T8InformationDialog.showInformation(clientContext.getParentWindow(), null, result.toString());
                    }
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving field definitions for data source: " + definition.getIdentifier(), e);
            }
        }
    }
}
