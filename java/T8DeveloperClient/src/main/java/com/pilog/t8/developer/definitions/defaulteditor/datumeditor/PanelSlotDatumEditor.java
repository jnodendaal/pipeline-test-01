package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class PanelSlotDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private final LinkedHashMap<String, LayoutComponent> layoutComponents;
    private LayoutComponent selectedComponent;

    private static final Color SELECTED_COLOR = new Color(222, 215, 236);

    public PanelSlotDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        layoutComponents = new LinkedHashMap<String, LayoutComponent>();
        jListLayoutComponents.setModel(new DefaultListModel());
        jListLayoutComponents.setCellRenderer(new LayoutComponentListCellRenderer());
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void commitChanges()
    {
        setDefinitionDatum(getConstraints());
    }

    @Override
    public void setEditable(boolean editable)
    {
        jFormattedTextFieldWeightX.setEnabled(editable);
    }

    @Override
    public void refreshEditor()
    {
        setConstraints((List<T8PanelSlotDefinition>)getDefinitionDatum());
    }

    public void setConstraints(List<T8PanelSlotDefinition> layoutConstraints)
    {
        DefaultListModel listModel;

        // Clear the layout components from the layout view.
        layoutComponents.clear();

        // Clear the layout component from the list view.
        listModel = (DefaultListModel)jListLayoutComponents.getModel();
        listModel.clear();

        // Add the new layout components.
        if (layoutConstraints != null)
        {
            for (T8PanelSlotDefinition constraints : layoutConstraints)
            {
                addLayoutComponent(new LayoutComponent(constraints));
            }
        }
    }

    public ArrayList<T8PanelSlotDefinition> getConstraints()
    {
        ArrayList<T8PanelSlotDefinition> constraints;

        saveSelectedComponent();
        constraints = new ArrayList<T8PanelSlotDefinition>();
        for (LayoutComponent layoutComponent : layoutComponents.values())
        {
            constraints.add(layoutComponent.getLayoutConstraintsDefinition());
        }

        return constraints;
    }

    public boolean containsLayoutComponent(String componentName)
    {
        return layoutComponents.containsKey(componentName);
    }

    public void addLayoutComponent(String componentName)
    {
        if (!layoutComponents.containsKey(componentName))
        {
            T8PanelSlotDefinition constraintsDefinition;
            LayoutComponent layoutComponent;
            GridBagConstraints newConstraints;

            newConstraints = new GridBagConstraints();
            newConstraints.gridx = 0;
            newConstraints.gridy = layoutComponents.size();
            newConstraints.weightx = 0.1;
            newConstraints.weighty = 0.1;
            newConstraints.fill = GridBagConstraints.BOTH;

            constraintsDefinition = new T8PanelSlotDefinition(componentName);
            constraintsDefinition.setGridBagConstraints(newConstraints);
            constraintsDefinition.setMinimumWidth(0);
            constraintsDefinition.setMinimumHeight(0);
            constraintsDefinition.setPreferredWidth(0);
            constraintsDefinition.setPreferredHeight(0);
            layoutComponent = new LayoutComponent(constraintsDefinition);
            addLayoutComponent(layoutComponent);
        }
        else throw new RuntimeException("Duplicate layout component identifier used: '" + componentName + "'.");
    }

    private void addLayoutComponent(LayoutComponent layoutComponent)
    {
        DefaultListModel listModel;

        layoutComponents.put(layoutComponent.getComponentName(), layoutComponent);
        jPanelLayoutView.add(layoutComponent, layoutComponent.getLayoutConstraintsDefinition().getGridBagConstraints());
        jPanelLayoutView.validate();

        listModel = (DefaultListModel)jListLayoutComponents.getModel();
        listModel.addElement(layoutComponent);
    }

    private void removeSelectedComponent()
    {
        if (selectedComponent != null)
        {
            String selectedComponentName;

            selectedComponentName = selectedComponent.getComponentName();
            ((DefaultListModel)jListLayoutComponents.getModel()).removeElement(selectedComponent);
            jPanelLayoutView.remove(selectedComponent);
            layoutComponents.remove(selectedComponentName);
            selectedComponent = null;

            redoLayout();
        }
    }

    private void setSelectedComponent(LayoutComponent component)
    {
        saveSelectedComponent();
        clearSelection();

        selectedComponent = component;
        if (selectedComponent != null)
        {
            T8PanelSlotDefinition selectedLayoutConstraints;
            GridBagConstraints selectedConstraints;
            Dimension minimumDimensions;
            Dimension preferredDimensions;

            selectedComponent.setSelected(true);
            selectedLayoutConstraints = selectedComponent.getLayoutConstraintsDefinition();

            // Set all numeric GridBag values.
            selectedConstraints = selectedLayoutConstraints.getGridBagConstraints();
            jTextFieldSlotName.setText(selectedComponent.getComponentName());
            jFormattedTextFieldGridX.setValue(selectedConstraints.gridx);
            jFormattedTextFieldGridY.setValue(selectedConstraints.gridy);
            jFormattedTextFieldGridWidth.setValue(selectedConstraints.gridwidth);
            jFormattedTextFieldGridHeight.setValue(selectedConstraints.gridheight);
            jFormattedTextFieldPaddingX.setValue(selectedConstraints.ipadx);
            jFormattedTextFieldPaddingY.setValue(selectedConstraints.ipady);
            jFormattedTextFieldWeightX.setValue(selectedConstraints.weightx);
            jFormattedTextFieldWeightY.setValue(selectedConstraints.weighty);
            jFormattedTextFieldInsetLeft.setValue(selectedConstraints.insets.left);
            jFormattedTextFieldInsetTop.setValue(selectedConstraints.insets.top);
            jFormattedTextFieldInsetRight.setValue(selectedConstraints.insets.right);
            jFormattedTextFieldInsetBottom.setValue(selectedConstraints.insets.bottom);

            // Set the dimensions of the constraints.
            minimumDimensions = selectedLayoutConstraints.getMinimumDimensions();
            jFormattedTextFieldMinSizeX.setValue(minimumDimensions.width);
            jFormattedTextFieldMinSizeY.setValue(minimumDimensions.height);
            preferredDimensions = selectedLayoutConstraints.getPreferredDimensions();
            jFormattedTextFieldPrefSizeX.setValue(preferredDimensions.width);
            jFormattedTextFieldPrefSizeY.setValue(preferredDimensions.height);

            // Set the Fill combo box to the correct index;
            if (selectedConstraints.fill == GridBagConstraints.NONE) jComboBoxFill.setSelectedIndex(0);
            else if (selectedConstraints.fill == GridBagConstraints.HORIZONTAL) jComboBoxFill.setSelectedIndex(1);
            else if (selectedConstraints.fill == GridBagConstraints.VERTICAL) jComboBoxFill.setSelectedIndex(2);
            else if (selectedConstraints.fill == GridBagConstraints.BOTH) jComboBoxFill.setSelectedIndex(3);

            // Set the Anchor combo box to the correct index;
            if (selectedConstraints.anchor == GridBagConstraints.CENTER) jComboBoxAnchor.setSelectedIndex(0);
            else if (selectedConstraints.anchor == GridBagConstraints.NORTH) jComboBoxAnchor.setSelectedIndex(1);
            else if (selectedConstraints.anchor == GridBagConstraints.NORTHEAST) jComboBoxAnchor.setSelectedIndex(2);
            else if (selectedConstraints.anchor == GridBagConstraints.EAST) jComboBoxAnchor.setSelectedIndex(3);
            else if (selectedConstraints.anchor == GridBagConstraints.SOUTHEAST) jComboBoxAnchor.setSelectedIndex(4);
            else if (selectedConstraints.anchor == GridBagConstraints.SOUTH) jComboBoxAnchor.setSelectedIndex(5);
            else if (selectedConstraints.anchor == GridBagConstraints.SOUTHWEST) jComboBoxAnchor.setSelectedIndex(6);
            else if (selectedConstraints.anchor == GridBagConstraints.WEST) jComboBoxAnchor.setSelectedIndex(7);
            else if (selectedConstraints.anchor == GridBagConstraints.NORTHWEST) jComboBoxAnchor.setSelectedIndex(8);
        }
    }

    private void saveSelectedComponent()
    {
        stopEditing();
        if (selectedComponent != null)
        {
            T8PanelSlotDefinition selectedLayoutConstraints;
            GridBagConstraints selectedConstraints;

            selectedComponent.setSelected(true);
            selectedLayoutConstraints = selectedComponent.getLayoutConstraintsDefinition();

            // Handle a possible component name change.
            layoutComponents.remove(selectedComponent.getComponentName());
            selectedComponent.setComponentName(jTextFieldSlotName.getText());
            layoutComponents.put(selectedComponent.getComponentName(), selectedComponent);

            // Set all numeric GridBag values.
            selectedConstraints = selectedLayoutConstraints.getGridBagConstraints();
            selectedConstraints.gridx = ((Number)jFormattedTextFieldGridX.getValue()).intValue();
            selectedConstraints.gridy = ((Number)jFormattedTextFieldGridY.getValue()).intValue();
            selectedConstraints.gridwidth = ((Number)jFormattedTextFieldGridWidth.getValue()).intValue();
            selectedConstraints.gridheight = ((Number)jFormattedTextFieldGridHeight.getValue()).intValue();
            selectedConstraints.ipadx = ((Number)jFormattedTextFieldPaddingX.getValue()).intValue();
            selectedConstraints.ipady = ((Number)jFormattedTextFieldPaddingY.getValue()).intValue();
            selectedConstraints.weightx = ((Number)jFormattedTextFieldWeightX.getValue()).doubleValue();
            selectedConstraints.weighty = ((Number)jFormattedTextFieldWeightY.getValue()).doubleValue();
            selectedConstraints.insets.left = ((Number)jFormattedTextFieldInsetLeft.getValue()).intValue();
            selectedConstraints.insets.top = ((Number)jFormattedTextFieldInsetTop.getValue()).intValue();
            selectedConstraints.insets.right = ((Number)jFormattedTextFieldInsetRight.getValue()).intValue();
            selectedConstraints.insets.bottom = ((Number)jFormattedTextFieldInsetBottom.getValue()).intValue();

            // Save the dimensions.
            selectedLayoutConstraints.setMinimumWidth(((Number)jFormattedTextFieldMinSizeX.getValue()).intValue());
            selectedLayoutConstraints.setMinimumHeight(((Number)jFormattedTextFieldMinSizeY.getValue()).intValue());
            selectedLayoutConstraints.setPreferredWidth(((Number)jFormattedTextFieldPrefSizeX.getValue()).intValue());
            selectedLayoutConstraints.setPreferredHeight(((Number)jFormattedTextFieldPrefSizeY.getValue()).intValue());

            // Save the selected Fill option from the combo box.
            if (jComboBoxFill.getSelectedIndex() == 0) selectedConstraints.fill = GridBagConstraints.NONE;
            else if(jComboBoxFill.getSelectedIndex() == 1) selectedConstraints.fill = GridBagConstraints.HORIZONTAL;
            else if(jComboBoxFill.getSelectedIndex() == 2) selectedConstraints.fill = GridBagConstraints.VERTICAL;
            else if(jComboBoxFill.getSelectedIndex() == 3) selectedConstraints.fill = GridBagConstraints.BOTH;

            // Save the selected Anchor option from the combo box.
            if (jComboBoxAnchor.getSelectedIndex() == 0) selectedConstraints.anchor = GridBagConstraints.CENTER;
            else if(jComboBoxAnchor.getSelectedIndex() == 1) selectedConstraints.anchor = GridBagConstraints.NORTH;
            else if(jComboBoxAnchor.getSelectedIndex() == 2) selectedConstraints.anchor = GridBagConstraints.NORTHEAST;
            else if(jComboBoxAnchor.getSelectedIndex() == 3) selectedConstraints.anchor = GridBagConstraints.EAST;
            else if(jComboBoxAnchor.getSelectedIndex() == 4) selectedConstraints.anchor = GridBagConstraints.SOUTHEAST;
            else if(jComboBoxAnchor.getSelectedIndex() == 5) selectedConstraints.anchor = GridBagConstraints.SOUTH;
            else if(jComboBoxAnchor.getSelectedIndex() == 6) selectedConstraints.anchor = GridBagConstraints.SOUTHWEST;
            else if(jComboBoxAnchor.getSelectedIndex() == 7) selectedConstraints.anchor = GridBagConstraints.WEST;
            else if(jComboBoxAnchor.getSelectedIndex() == 8) selectedConstraints.anchor = GridBagConstraints.NORTHWEST;

            selectedLayoutConstraints.setGridBagConstraints(selectedConstraints);
            redoLayout();
        }
    }

    private void clearSelection()
    {
        for (LayoutComponent layoutComponent : layoutComponents.values())
        {
            layoutComponent.setSelected(false);
        }
    }

    private void stopEditing()
    {
        try
        {
            jFormattedTextFieldGridX.commitEdit();
            jFormattedTextFieldGridY.commitEdit();
            jFormattedTextFieldGridWidth.commitEdit();
            jFormattedTextFieldGridHeight.commitEdit();
            jFormattedTextFieldPaddingX.commitEdit();
            jFormattedTextFieldPaddingY.commitEdit();
            jFormattedTextFieldWeightX.commitEdit();
            jFormattedTextFieldWeightY.commitEdit();
            jFormattedTextFieldInsetLeft.commitEdit();
            jFormattedTextFieldInsetTop.commitEdit();
            jFormattedTextFieldInsetRight.commitEdit();
            jFormattedTextFieldInsetBottom.commitEdit();
            jFormattedTextFieldMinSizeX.commitEdit();
            jFormattedTextFieldMinSizeY.commitEdit();
            jFormattedTextFieldPrefSizeX.commitEdit();
            jFormattedTextFieldPrefSizeY.commitEdit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void redoLayout()
    {
        jPanelLayoutView.removeAll();
        for (LayoutComponent layoutComponent : layoutComponents.values())
        {
            jPanelLayoutView.add(layoutComponent, layoutComponent.getLayoutConstraintsDefinition().getGridBagConstraints());
        }

        jPanelLayoutView.validate();
    }

    private class LayoutComponent extends JPanel
    {
        private JLabel jLabelComponentName;
        private T8PanelSlotDefinition layoutConstraints;

        public LayoutComponent(T8PanelSlotDefinition layoutConstraints)
        {
            this.layoutConstraints = layoutConstraints;

            jLabelComponentName = new JLabel(layoutConstraints.getIdentifier());
            jLabelComponentName.setHorizontalAlignment(JLabel.CENTER);

            setBackground(Color.LIGHT_GRAY);
            setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
            setLayout(new java.awt.BorderLayout());
            add(jLabelComponentName, java.awt.BorderLayout.CENTER);
            addMouseListener(new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent e)
                {
                    jListLayoutComponents.setSelectedValue(LayoutComponent.this, true);
                }
            });
        }

        public String getComponentName()
        {
            return jLabelComponentName.getText();
        }

        public void setComponentName(String componentName)
        {
            jLabelComponentName.setText(componentName);
        }

        public void setSelected(boolean selected)
        {
            this.setBackground(selected ? SELECTED_COLOR : Color.LIGHT_GRAY);
        }

        public T8PanelSlotDefinition getLayoutConstraintsDefinition()
        {
            return layoutConstraints;
        }
    }

    private class LayoutComponentListCellRenderer implements ListCellRenderer
    {
        private DefaultListCellRenderer renderer;

        public LayoutComponentListCellRenderer()
        {
            renderer = new DefaultListCellRenderer();
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            return renderer.getListCellRendererComponent(list, ((LayoutComponent)value).getComponentName(), index, isSelected, cellHasFocus);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelLayoutDetails = new javax.swing.JPanel();
        jLabelSlotName = new javax.swing.JLabel();
        jTextFieldSlotName = new javax.swing.JTextField();
        jLabelGridX = new javax.swing.JLabel();
        jFormattedTextFieldGridX = new javax.swing.JFormattedTextField();
        jLabelGridY = new javax.swing.JLabel();
        jFormattedTextFieldGridY = new javax.swing.JFormattedTextField();
        jLabelGridWidth = new javax.swing.JLabel();
        jFormattedTextFieldGridWidth = new javax.swing.JFormattedTextField();
        jLabelGridHeight = new javax.swing.JLabel();
        jFormattedTextFieldGridHeight = new javax.swing.JFormattedTextField();
        jLabelFill = new javax.swing.JLabel();
        jComboBoxFill = new javax.swing.JComboBox();
        jLabelPaddingX = new javax.swing.JLabel();
        jFormattedTextFieldPaddingX = new javax.swing.JFormattedTextField();
        jLabelPaddingY = new javax.swing.JLabel();
        jFormattedTextFieldPaddingY = new javax.swing.JFormattedTextField();
        jLabelAnchor = new javax.swing.JLabel();
        jComboBoxAnchor = new javax.swing.JComboBox();
        jLabelWeightX = new javax.swing.JLabel();
        jFormattedTextFieldWeightX = new javax.swing.JFormattedTextField();
        jLabelWeightY = new javax.swing.JLabel();
        jFormattedTextFieldWeightY = new javax.swing.JFormattedTextField();
        jLabelInsetLeft = new javax.swing.JLabel();
        jFormattedTextFieldInsetLeft = new javax.swing.JFormattedTextField();
        jLabelInsetTop = new javax.swing.JLabel();
        jFormattedTextFieldInsetTop = new javax.swing.JFormattedTextField();
        jLabelInsetRight = new javax.swing.JLabel();
        jFormattedTextFieldInsetRight = new javax.swing.JFormattedTextField();
        jLabelInsetBottom = new javax.swing.JLabel();
        jFormattedTextFieldInsetBottom = new javax.swing.JFormattedTextField();
        jLabelMinSizeX = new javax.swing.JLabel();
        jFormattedTextFieldMinSizeX = new javax.swing.JFormattedTextField();
        jLabelMinSizeY = new javax.swing.JLabel();
        jFormattedTextFieldMinSizeY = new javax.swing.JFormattedTextField();
        jLabelSpacer = new javax.swing.JLabel();
        jLabelPrefSizeX = new javax.swing.JLabel();
        jFormattedTextFieldPrefSizeX = new javax.swing.JFormattedTextField();
        jLabelPrefSizeY = new javax.swing.JLabel();
        jFormattedTextFieldPrefSizeY = new javax.swing.JFormattedTextField();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelLayoutView = new javax.swing.JPanel();
        jPanelSlots = new javax.swing.JPanel();
        jScrollPaneLayoutComponents = new javax.swing.JScrollPane();
        jListLayoutComponents = new javax.swing.JList();

        setLayout(new java.awt.GridBagLayout());

        jPanelLayoutDetails.setBorder(javax.swing.BorderFactory.createTitledBorder("Slot Details"));
        jPanelLayoutDetails.setLayout(new java.awt.GridBagLayout());

        jLabelSlotName.setText("Slot Identifier:");
        jLabelSlotName.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelSlotName.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelSlotName.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelSlotName, gridBagConstraints);

        jTextFieldSlotName.setEditable(false);
        jTextFieldSlotName.setMaximumSize(null);
        jTextFieldSlotName.setMinimumSize(null);
        jTextFieldSlotName.setPreferredSize(null);
        jTextFieldSlotName.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jTextFieldSlotNameFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jTextFieldSlotName, gridBagConstraints);

        jLabelGridX.setText("Grid X:");
        jLabelGridX.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelGridX.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelGridX.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelGridX, gridBagConstraints);

        jFormattedTextFieldGridX.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldGridX.setText("0");
        jFormattedTextFieldGridX.setMaximumSize(null);
        jFormattedTextFieldGridX.setMinimumSize(null);
        jFormattedTextFieldGridX.setPreferredSize(null);
        jFormattedTextFieldGridX.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldGridXFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldGridX, gridBagConstraints);

        jLabelGridY.setText("Grid Y:");
        jLabelGridY.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelGridY.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelGridY.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelGridY, gridBagConstraints);

        jFormattedTextFieldGridY.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldGridY.setText("0");
        jFormattedTextFieldGridY.setMaximumSize(null);
        jFormattedTextFieldGridY.setMinimumSize(null);
        jFormattedTextFieldGridY.setPreferredSize(null);
        jFormattedTextFieldGridY.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldGridYFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldGridY, gridBagConstraints);

        jLabelGridWidth.setText("Grid Width:");
        jLabelGridWidth.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelGridWidth.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelGridWidth.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelGridWidth, gridBagConstraints);

        jFormattedTextFieldGridWidth.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldGridWidth.setText("0");
        jFormattedTextFieldGridWidth.setMaximumSize(null);
        jFormattedTextFieldGridWidth.setMinimumSize(null);
        jFormattedTextFieldGridWidth.setPreferredSize(null);
        jFormattedTextFieldGridWidth.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldGridWidthFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldGridWidth, gridBagConstraints);

        jLabelGridHeight.setText("Grid Height:");
        jLabelGridHeight.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelGridHeight.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelGridHeight.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelGridHeight, gridBagConstraints);

        jFormattedTextFieldGridHeight.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldGridHeight.setText("0");
        jFormattedTextFieldGridHeight.setMaximumSize(null);
        jFormattedTextFieldGridHeight.setMinimumSize(null);
        jFormattedTextFieldGridHeight.setPreferredSize(null);
        jFormattedTextFieldGridHeight.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldGridHeightFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldGridHeight, gridBagConstraints);

        jLabelFill.setText("Fill:");
        jLabelFill.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelFill.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelFill.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelFill, gridBagConstraints);

        jComboBoxFill.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Horizontal", "Vertical", "Both" }));
        jComboBoxFill.setMaximumSize(null);
        jComboBoxFill.setMinimumSize(null);
        jComboBoxFill.setPreferredSize(null);
        jComboBoxFill.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboBoxFillItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jComboBoxFill, gridBagConstraints);

        jLabelPaddingX.setText("Padding X:");
        jLabelPaddingX.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelPaddingX.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelPaddingX.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelPaddingX, gridBagConstraints);

        jFormattedTextFieldPaddingX.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldPaddingX.setText("0");
        jFormattedTextFieldPaddingX.setMaximumSize(null);
        jFormattedTextFieldPaddingX.setMinimumSize(null);
        jFormattedTextFieldPaddingX.setPreferredSize(null);
        jFormattedTextFieldPaddingX.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldPaddingXFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldPaddingX, gridBagConstraints);

        jLabelPaddingY.setText("Padding Y:");
        jLabelPaddingY.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelPaddingY.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelPaddingY.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelPaddingY, gridBagConstraints);

        jFormattedTextFieldPaddingY.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldPaddingY.setText("0");
        jFormattedTextFieldPaddingY.setMaximumSize(null);
        jFormattedTextFieldPaddingY.setMinimumSize(null);
        jFormattedTextFieldPaddingY.setPreferredSize(null);
        jFormattedTextFieldPaddingY.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldPaddingYFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldPaddingY, gridBagConstraints);

        jLabelAnchor.setText("Anchor:");
        jLabelAnchor.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelAnchor.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelAnchor.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelAnchor, gridBagConstraints);

        jComboBoxAnchor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Center", "North", "Northeast", "East", "Southeast", "South", "Southwest", "West", "Northwest" }));
        jComboBoxAnchor.setMaximumSize(null);
        jComboBoxAnchor.setMinimumSize(null);
        jComboBoxAnchor.setPreferredSize(null);
        jComboBoxAnchor.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboBoxAnchorItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jComboBoxAnchor, gridBagConstraints);

        jLabelWeightX.setText("Weight X:");
        jLabelWeightX.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelWeightX.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelWeightX.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelWeightX, gridBagConstraints);

        jFormattedTextFieldWeightX.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.0"))));
        jFormattedTextFieldWeightX.setText("0.0");
        jFormattedTextFieldWeightX.setMaximumSize(null);
        jFormattedTextFieldWeightX.setMinimumSize(null);
        jFormattedTextFieldWeightX.setPreferredSize(null);
        jFormattedTextFieldWeightX.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldWeightXFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldWeightX, gridBagConstraints);

        jLabelWeightY.setText("Weight Y:");
        jLabelWeightY.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelWeightY.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelWeightY.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelWeightY, gridBagConstraints);

        jFormattedTextFieldWeightY.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.0"))));
        jFormattedTextFieldWeightY.setText("0.0");
        jFormattedTextFieldWeightY.setMaximumSize(null);
        jFormattedTextFieldWeightY.setMinimumSize(null);
        jFormattedTextFieldWeightY.setPreferredSize(null);
        jFormattedTextFieldWeightY.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldWeightYFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldWeightY, gridBagConstraints);

        jLabelInsetLeft.setText("Inset Left:");
        jLabelInsetLeft.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelInsetLeft.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelInsetLeft.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelInsetLeft, gridBagConstraints);

        jFormattedTextFieldInsetLeft.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldInsetLeft.setText("0");
        jFormattedTextFieldInsetLeft.setMaximumSize(null);
        jFormattedTextFieldInsetLeft.setMinimumSize(null);
        jFormattedTextFieldInsetLeft.setPreferredSize(null);
        jFormattedTextFieldInsetLeft.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldInsetLeftFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldInsetLeft, gridBagConstraints);

        jLabelInsetTop.setText("Inset Top:");
        jLabelInsetTop.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelInsetTop.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelInsetTop.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelInsetTop, gridBagConstraints);

        jFormattedTextFieldInsetTop.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldInsetTop.setText("0");
        jFormattedTextFieldInsetTop.setMaximumSize(null);
        jFormattedTextFieldInsetTop.setMinimumSize(null);
        jFormattedTextFieldInsetTop.setPreferredSize(null);
        jFormattedTextFieldInsetTop.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldInsetTopFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldInsetTop, gridBagConstraints);

        jLabelInsetRight.setText("Inset Right:");
        jLabelInsetRight.setMaximumSize(new java.awt.Dimension(66, 20));
        jLabelInsetRight.setMinimumSize(new java.awt.Dimension(66, 20));
        jLabelInsetRight.setPreferredSize(new java.awt.Dimension(66, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelInsetRight, gridBagConstraints);

        jFormattedTextFieldInsetRight.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldInsetRight.setText("0");
        jFormattedTextFieldInsetRight.setMaximumSize(null);
        jFormattedTextFieldInsetRight.setMinimumSize(null);
        jFormattedTextFieldInsetRight.setPreferredSize(null);
        jFormattedTextFieldInsetRight.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldInsetRightFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldInsetRight, gridBagConstraints);

        jLabelInsetBottom.setText("Inset Bottom:");
        jLabelInsetBottom.setMaximumSize(new java.awt.Dimension(80, 20));
        jLabelInsetBottom.setMinimumSize(new java.awt.Dimension(80, 20));
        jLabelInsetBottom.setPreferredSize(new java.awt.Dimension(80, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelInsetBottom, gridBagConstraints);

        jFormattedTextFieldInsetBottom.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldInsetBottom.setText("0");
        jFormattedTextFieldInsetBottom.setMaximumSize(null);
        jFormattedTextFieldInsetBottom.setMinimumSize(null);
        jFormattedTextFieldInsetBottom.setPreferredSize(null);
        jFormattedTextFieldInsetBottom.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldInsetBottomFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldInsetBottom, gridBagConstraints);

        jLabelMinSizeX.setText("Size X (Min):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelMinSizeX, gridBagConstraints);

        jFormattedTextFieldMinSizeX.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextFieldMinSizeX.setText("0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldMinSizeX, gridBagConstraints);

        jLabelMinSizeY.setText("Size Y (Min):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelMinSizeY, gridBagConstraints);

        jFormattedTextFieldMinSizeY.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        jFormattedTextFieldMinSizeY.setText("0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldMinSizeY, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 19;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelLayoutDetails.add(jLabelSpacer, gridBagConstraints);

        jLabelPrefSizeX.setText("Size X (Pref):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 17;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelPrefSizeX, gridBagConstraints);

        jFormattedTextFieldPrefSizeX.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        jFormattedTextFieldPrefSizeX.setText("0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 17;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldPrefSizeX, gridBagConstraints);

        jLabelPrefSizeY.setText("Size Y (Pref):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanelLayoutDetails.add(jLabelPrefSizeY, gridBagConstraints);

        jFormattedTextFieldPrefSizeY.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        jFormattedTextFieldPrefSizeY.setText("0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelLayoutDetails.add(jFormattedTextFieldPrefSizeY, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jPanelLayoutDetails, gridBagConstraints);

        jPanelLayoutView.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                jPanelLayoutViewMouseClicked(evt);
            }
        });
        jPanelLayoutView.setLayout(new java.awt.GridBagLayout());
        jTabbedPane1.addTab("Layout View", jPanelLayoutView);

        jPanelSlots.setLayout(new java.awt.GridBagLayout());

        jListLayoutComponents.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListLayoutComponents.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                jListLayoutComponentsValueChanged(evt);
            }
        });
        jScrollPaneLayoutComponents.setViewportView(jListLayoutComponents);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSlots.add(jScrollPaneLayoutComponents, gridBagConstraints);

        jTabbedPane1.addTab("Layout Slots", jPanelSlots);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jTabbedPane1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jListLayoutComponentsValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_jListLayoutComponentsValueChanged
    {//GEN-HEADEREND:event_jListLayoutComponentsValueChanged
        if (!evt.getValueIsAdjusting())
        {
            LayoutComponent listSelectedComponent;

            listSelectedComponent = (LayoutComponent)jListLayoutComponents.getSelectedValue();
            if (listSelectedComponent != null)
            {
                setSelectedComponent(listSelectedComponent);
            }
        }
    }//GEN-LAST:event_jListLayoutComponentsValueChanged

    private void jPanelLayoutViewMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jPanelLayoutViewMouseClicked
    {//GEN-HEADEREND:event_jPanelLayoutViewMouseClicked
        saveSelectedComponent();
        clearSelection();
    }//GEN-LAST:event_jPanelLayoutViewMouseClicked

    private void jTextFieldSlotNameFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jTextFieldSlotNameFocusLost
    {//GEN-HEADEREND:event_jTextFieldSlotNameFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jTextFieldSlotNameFocusLost

    private void jFormattedTextFieldGridXFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldGridXFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldGridXFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldGridXFocusLost

    private void jFormattedTextFieldGridYFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldGridYFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldGridYFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldGridYFocusLost

    private void jFormattedTextFieldGridWidthFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldGridWidthFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldGridWidthFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldGridWidthFocusLost

    private void jFormattedTextFieldGridHeightFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldGridHeightFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldGridHeightFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldGridHeightFocusLost

    private void jFormattedTextFieldPaddingXFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldPaddingXFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldPaddingXFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldPaddingXFocusLost

    private void jFormattedTextFieldPaddingYFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldPaddingYFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldPaddingYFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldPaddingYFocusLost

    private void jComboBoxFillItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboBoxFillItemStateChanged
    {//GEN-HEADEREND:event_jComboBoxFillItemStateChanged
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) saveSelectedComponent();
    }//GEN-LAST:event_jComboBoxFillItemStateChanged

    private void jComboBoxAnchorItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboBoxAnchorItemStateChanged
    {//GEN-HEADEREND:event_jComboBoxAnchorItemStateChanged
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) saveSelectedComponent();
    }//GEN-LAST:event_jComboBoxAnchorItemStateChanged

    private void jFormattedTextFieldWeightXFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldWeightXFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldWeightXFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldWeightXFocusLost

    private void jFormattedTextFieldWeightYFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldWeightYFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldWeightYFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldWeightYFocusLost

    private void jFormattedTextFieldInsetLeftFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldInsetLeftFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldInsetLeftFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldInsetLeftFocusLost

    private void jFormattedTextFieldInsetTopFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldInsetTopFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldInsetTopFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldInsetTopFocusLost

    private void jFormattedTextFieldInsetRightFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldInsetRightFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldInsetRightFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldInsetRightFocusLost

    private void jFormattedTextFieldInsetBottomFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldInsetBottomFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldInsetBottomFocusLost
        saveSelectedComponent();
    }//GEN-LAST:event_jFormattedTextFieldInsetBottomFocusLost


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox jComboBoxAnchor;
    private javax.swing.JComboBox jComboBoxFill;
    private javax.swing.JFormattedTextField jFormattedTextFieldGridHeight;
    private javax.swing.JFormattedTextField jFormattedTextFieldGridWidth;
    private javax.swing.JFormattedTextField jFormattedTextFieldGridX;
    private javax.swing.JFormattedTextField jFormattedTextFieldGridY;
    private javax.swing.JFormattedTextField jFormattedTextFieldInsetBottom;
    private javax.swing.JFormattedTextField jFormattedTextFieldInsetLeft;
    private javax.swing.JFormattedTextField jFormattedTextFieldInsetRight;
    private javax.swing.JFormattedTextField jFormattedTextFieldInsetTop;
    private javax.swing.JFormattedTextField jFormattedTextFieldMinSizeX;
    private javax.swing.JFormattedTextField jFormattedTextFieldMinSizeY;
    private javax.swing.JFormattedTextField jFormattedTextFieldPaddingX;
    private javax.swing.JFormattedTextField jFormattedTextFieldPaddingY;
    private javax.swing.JFormattedTextField jFormattedTextFieldPrefSizeX;
    private javax.swing.JFormattedTextField jFormattedTextFieldPrefSizeY;
    private javax.swing.JFormattedTextField jFormattedTextFieldWeightX;
    private javax.swing.JFormattedTextField jFormattedTextFieldWeightY;
    private javax.swing.JLabel jLabelAnchor;
    private javax.swing.JLabel jLabelFill;
    private javax.swing.JLabel jLabelGridHeight;
    private javax.swing.JLabel jLabelGridWidth;
    private javax.swing.JLabel jLabelGridX;
    private javax.swing.JLabel jLabelGridY;
    private javax.swing.JLabel jLabelInsetBottom;
    private javax.swing.JLabel jLabelInsetLeft;
    private javax.swing.JLabel jLabelInsetRight;
    private javax.swing.JLabel jLabelInsetTop;
    private javax.swing.JLabel jLabelMinSizeX;
    private javax.swing.JLabel jLabelMinSizeY;
    private javax.swing.JLabel jLabelPaddingX;
    private javax.swing.JLabel jLabelPaddingY;
    private javax.swing.JLabel jLabelPrefSizeX;
    private javax.swing.JLabel jLabelPrefSizeY;
    private javax.swing.JLabel jLabelSlotName;
    private javax.swing.JLabel jLabelSpacer;
    private javax.swing.JLabel jLabelWeightX;
    private javax.swing.JLabel jLabelWeightY;
    private javax.swing.JList jListLayoutComponents;
    private javax.swing.JPanel jPanelLayoutDetails;
    private javax.swing.JPanel jPanelLayoutView;
    private javax.swing.JPanel jPanelSlots;
    private javax.swing.JScrollPane jScrollPaneLayoutComponents;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextFieldSlotName;
    // End of variables declaration//GEN-END:variables

}
