package com.pilog.t8.developer.definitions.test.harness.datarecordview;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.document.datarecord.view.T8DataRecordViewDefinition;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.ui.componentcontainer.T8AsynchronousOperationTask;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import java.awt.Dimension;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import com.pilog.t8.security.T8Context;

import static com.pilog.t8.definition.api.T8DataRecordApiResource.OPERATION_API_REC_REFRESH_DATA_OBJECTS;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordViewTestFrame extends javax.swing.JFrame
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8SecurityManager securityManager;
    private final T8DefinitionManager definitionManager;
    private final T8DataRecordViewDefinition viewDefinition;
    private final T8Context testContext;
    private final T8DataRecordViewTestHarness testHarness;
    private final T8DefaultComponentContainer container;
    private final Preferences preferences;

    public T8DataRecordViewTestFrame(T8Context context, T8DataRecordViewDefinition viewDefinition, T8DataRecordViewTestHarness testHarness) throws Exception
    {
        T8ServerOperationDefinition operationDefinition;

        initComponents();
        this.clientContext = context.getClientContext();
        this.context = context;
        this.viewDefinition = viewDefinition;
        this.testHarness = testHarness;
        this.preferences = Preferences.userNodeForPackage(T8DataRecordViewTestFrame.class);

        // Create a new session.
        securityManager = clientContext.getSecurityManager();
        definitionManager = clientContext.getDefinitionManager();

        // If a user is defined by the test harness, use it.
        if ((testHarness != null) && (testHarness.getUserIdentifier() != null))
        {
            testContext = new T8Context(clientContext, securityManager.createNewSessionContext());
            T8Log.log("Loggin in as test user: " + testHarness.getUserIdentifier());
            securityManager.login(testContext, T8SecurityManager.LoginLocation.DEVELOPER, testHarness.getUserIdentifier(), testHarness.getUserPassword(), false);
            securityManager.switchUserProfile(testContext, testHarness.getUserProfileIdentifier());
        }
        else
        {
            testContext = context;
        }

        // Set the preferred size of the frame.
        if (clientContext.getParentWindow() != null)
        {
            Dimension parentDimension = clientContext.getParentWindow().getSize();
            parentDimension.height = (int)(parentDimension.height * 0.80);
            parentDimension.width = (int)(parentDimension.width * 0.80);
            setPreferredSize(new Dimension(parentDimension.width, parentDimension.height));
        }
        else
        {
            setPreferredSize(new Dimension(600, 600));
        }

        // Do the layout of the frame.
        setTitle("Data File View Definition Test: " + viewDefinition.getIdentifier());
        pack();
        setLocationRelativeTo(clientContext.getParentWindow());

        // Set input parameters of the operation.
        operationDefinition = definitionManager.getRawDefinition(context, null, OPERATION_API_REC_REFRESH_DATA_OBJECTS);
        setInputParameters(operationDefinition.getInputParameterDefinitions());
        container = new T8DefaultComponentContainer(jTabbedPaneContent);
        setContentPane(container);

        if (!viewDefinition.isLocked()) Toast.show("Definition not saved", Toast.Style.NORMAL, 5000);
    }

    private void populateDataFileView()
    {
        new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    Map<String, Object> inputParameters;

                    // Get the intput parameters.
                    inputParameters = evaluateInputParameters();

                    T8ServerOperationStatusReport statusReport;
                    Map<String, Object> outputParameters;
                    String operationIid;
                    T8AsynchronousOperationTask task;
                    Exception exception;

                    // Execute the operation.
                    statusReport = T8MainServerClient.executeAsynchronousOperation(testContext, OPERATION_API_REC_REFRESH_DATA_OBJECTS, inputParameters);
                    operationIid = statusReport.getOperationInstanceIdentifier();

                    // Set the progress layer message and then lock the layer during execution of the operation.
                    task = new T8AsynchronousOperationTask(testContext, operationIid, "Refreshing Data File Views...");
                    container.setProgressUpdateInterval(500);
                    container.addTask(task);
                    //lockUI("Refreshing Data File Views...");

                    // Wait for execution to complete.
                    synchronized(task)
                    {
                        while (task.isActive())
                        {
                            try
                            {
                                /*
                                    Wait for this task to complete, the component container will call notify on the task object once it is complete,
                                    if an exception occurs or the component container failed to call the notify some reason then this thread will wait
                                    for the amount specified after which it will recheck the validation in any case and continue.
                                */
                                task.wait(10 * 1000);
                            }
                            catch (IllegalMonitorStateException | InterruptedException e)
                            {
                                T8Log.log("Exception while waiting for asynchronous task to complete.", e);
                            }
                        }
                    }

                    // Return the output of the operation.
                    exception = task.getException();
                    outputParameters = task.getResult();
                    if (exception != null) throw exception;

                    // Show the success message.
                    Toast.show("Data File Refresh Complete", Toast.Style.SUCCESS);
                }
                catch (Exception ex)
                {
                    PrintWriter writer;
                    StringWriter stringWriter;

                    T8Log.log("Data File View Refresh failed.", ex);
                    jEditorPane1.setText(null);
                    stringWriter = new StringWriter();
                    writer = new PrintWriter(stringWriter);
                    ex.printStackTrace(writer);
                    jEditorPane1.setText(stringWriter.toString());
                    jEditorPane1.setCaretPosition(0);
                    writer.close();
                    jTabbedPaneContent.setSelectedIndex(1);
                }
            }
        }.start();
    }

    private Map<String, Object> evaluateInputParameters()
    {
        T8ClientExpressionEvaluator expressionEvaluator;
        Map<String, Object> inputParameters;
        Map<String, String> inputParameterExpressions;
        TableModel model;

        // Stop editing.
        if (jTableInputParameters.getCellEditor() != null) jTableInputParameters.getCellEditor().stopCellEditing();

        model = jTableInputParameters.getModel();
        expressionEvaluator = new T8ClientExpressionEvaluator(context);
        inputParameters = new HashMap<String, Object>();
        inputParameterExpressions = new HashMap<String, String>();
        for (int rowIndex = 0; rowIndex < model.getRowCount(); rowIndex++)
        {
            String parameterIdentifier;
            String valueExpression;
            Object value;

            parameterIdentifier = (String)model.getValueAt(rowIndex, 0);
            valueExpression = (String)model.getValueAt(rowIndex, 1);

            if (Strings.isNullOrEmpty(valueExpression))
            {
                value = null;
            }
            else
            {
                try
                {
                    value = expressionEvaluator.evaluateExpression(valueExpression, null, null);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while evaluating value expression for task input parameter: " + parameterIdentifier, e);
                    value = null;
                }
            }

            inputParameterExpressions.put(parameterIdentifier, valueExpression);
            inputParameters.put(parameterIdentifier, value);
        }

        // Update the preferences.
        setInputParameterPreferences(inputParameterExpressions);
        return inputParameters;
    }

    private void setInputParameterPreferences(Map<String, String> inputParameterExpressions)
    {
        StringBuffer parameterString;

        parameterString = new StringBuffer();
        if (inputParameterExpressions != null)
        {
            for (String parameterIdentifier : inputParameterExpressions.keySet())
            {
                String expression;

                expression = inputParameterExpressions.get(parameterIdentifier);

                parameterString.append(parameterIdentifier);
                parameterString.append("```");
                parameterString.append(Strings.isNullOrEmpty(expression) ? "" : expression);
                parameterString.append("```");
            }
        }

        preferences.put(viewDefinition.getIdentifier(), parameterString.toString());
    }

    private Map<String, String> getInputParameterPreferences()
    {
        String parameterString;

        parameterString = preferences.get(viewDefinition.getIdentifier(), null);
        if (!Strings.isNullOrEmpty(parameterString))
        {
            try
            {
                Map<String, String> parameterPreferences;
                String[] splits;

                splits = parameterString.split("```");
                parameterPreferences = new HashMap<String, String>();
                for (int index = 0; index < splits.length -1; index += 2)
                {
                    String parameterIdentifier;
                    String expression;

                    parameterIdentifier = splits[index];
                    expression = splits[index+1];
                    parameterPreferences.put(parameterIdentifier, expression);
                }

                return parameterPreferences;
            }
            catch (Exception e)
            {
                T8Log.log("Exception while parsing input parameter preferences.", e);
                return null;
            }
        }
        else return null;
    }

    private void setInputParameters(List<T8DataParameterDefinition> parameterDefinitions)
    {
        DefaultTableModel model;
        Map<String, String> inputParameterPreferences;

        // Get any previously set preferences.
        inputParameterPreferences = getInputParameterPreferences();

        // Get the model to update.
        model = (DefaultTableModel)jTableInputParameters.getModel();

        // Clear the table model.
        while (model.getRowCount() > 0)
        {
            model.removeRow(0);
        }

        // Add all input parameters to the model.
        for (T8DataParameterDefinition parameterDefinition :  parameterDefinitions)
        {
            Object[] rowData;

            rowData = new Object[3];
            rowData[0] = parameterDefinition.getIdentifier();
            rowData[1] = inputParameterPreferences != null ? inputParameterPreferences.get(parameterDefinition.getIdentifier()) : null;
            model.addRow(rowData);
        }
    }

    public static final void testViewDefinition(T8Context context, T8DataRecordViewDefinition viewDefinition, T8DataRecordViewTestHarness testHarness) throws Exception
    {
        T8DataRecordViewTestFrame testFrame;

        testFrame = new T8DataRecordViewTestFrame(context, viewDefinition, testHarness);
        testFrame.setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jTabbedPaneContent = new javax.swing.JTabbedPane();
        jPanelOperationInput = new javax.swing.JPanel();
        jScrollPaneInputParameters = new javax.swing.JScrollPane();
        jTableInputParameters = new javax.swing.JTable();
        jPanelInputControls = new javax.swing.JPanel();
        jButtonRefreshView = new javax.swing.JButton();
        jPanelExceptionDetails = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();

        jPanelOperationInput.setLayout(new java.awt.BorderLayout());

        jTableInputParameters.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Parameter Identifier", "Value Expression"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableInputParameters.setRowHeight(25);
        jScrollPaneInputParameters.setViewportView(jTableInputParameters);

        jPanelOperationInput.add(jScrollPaneInputParameters, java.awt.BorderLayout.CENTER);

        jPanelInputControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 5, 1));

        jButtonRefreshView.setText("Refresh View");
        jButtonRefreshView.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButtonRefreshView.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshViewActionPerformed(evt);
            }
        });
        jPanelInputControls.add(jButtonRefreshView);

        jPanelOperationInput.add(jPanelInputControls, java.awt.BorderLayout.PAGE_END);

        jTabbedPaneContent.addTab("Operation Input", null, jPanelOperationInput, "");

        jPanelExceptionDetails.setLayout(new java.awt.BorderLayout());

        jEditorPane1.setEditable(false);
        jScrollPane1.setViewportView(jEditorPane1);

        jPanelExceptionDetails.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jTabbedPaneContent.addTab("Exception Details", null, jPanelExceptionDetails, "");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Data Record View Test");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentShown(java.awt.event.ComponentEvent evt)
            {
                formComponentShown(evt);
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_formComponentShown
    {//GEN-HEADEREND:event_formComponentShown
        // Start the module when it is shown for the first time.

    }//GEN-LAST:event_formComponentShown

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        // Stop the tested component when this window is closed.

    }//GEN-LAST:event_formWindowClosing

    private void jButtonRefreshViewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshViewActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshViewActionPerformed
        populateDataFileView();
    }//GEN-LAST:event_jButtonRefreshViewActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRefreshView;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JPanel jPanelExceptionDetails;
    private javax.swing.JPanel jPanelInputControls;
    private javax.swing.JPanel jPanelOperationInput;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPaneInputParameters;
    private javax.swing.JTabbedPane jTabbedPaneContent;
    private javax.swing.JTable jTableInputParameters;
    // End of variables declaration//GEN-END:variables
}
