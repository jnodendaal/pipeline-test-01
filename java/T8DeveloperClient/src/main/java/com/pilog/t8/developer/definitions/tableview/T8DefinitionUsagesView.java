package com.pilog.t8.developer.definitions.tableview;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8DeveloperView;
import java.util.List;

/**
 * Extends default table view to added specific feature required by "usages of"
 * view. Since the definition itself is not considered a usage of itself, and it
 * is not expected to have recursive definitions (definitions referencing
 * themselves), the definition for which usages are searched for is excluded
 * from the resulting list.
 *
 * @author Bouwer du Preez
 */
public class T8DefinitionUsagesView extends T8DefinitionTableView
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefinitionUsagesView.class.getName());

    private final String definitionIdentifier;

    public T8DefinitionUsagesView(T8DefinitionContext definitionContext, T8DeveloperView parentView, String definitionId)
    {
        super(definitionContext, parentView, null, false, "Usages of: " + definitionId);
        this.definitionIdentifier = definitionId;
        setUsagesResults(definitionId);
    }

    private void setUsagesResults(String definitionIdentifier)
    {
        searchByIdentifier(definitionIdentifier);
    }

    @Override
    public String getHeader()
    {
        return "Usages of: " + definitionIdentifier;
    }

    @Override
    protected void searchByIdentifier(String identifier)
    {
        List<T8DefinitionHandle> definitionHandles;

        try
        {
            // Use the regular find definitions by identifier.
            definitionHandles = definitionContext.findDefinitionsByIdentifier(identifier, true);

            // Set the filter handles.
            setFilterHandles(definitionHandles);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while searching for definitions by identifier '" + identifier + "'.", e);
        }
    }
}
