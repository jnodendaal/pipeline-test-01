package com.pilog.t8.developer.definitions.dialog.delete;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.developer.definitions.dialog.T8DefinitionSelectionPanel;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.laf.LAFConstants;
import javax.swing.JOptionPane;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDeletionDialog extends javax.swing.JDialog
{
    private T8ClientContext clientContext;
    private final T8DefinitionSelectionPanel selectionPanel;
    private final T8Context context;
    private final String groupId;

    public T8DefinitionDeletionDialog(java.awt.Frame parent, T8Context context, String groupId)
    {
        super(parent, true);
        initComponents();

        this.clientContext = context.getClientContext();
        this.context = context;
        this.groupId = groupId;

        jPanelHeader.setBackground(LAFConstants.HEADER_BG_COLOR);
        jPanelHeader.setBorder(LAFConstants.HEADER_BORDER);
        selectionPanel = new T8DefinitionSelectionPanel(context, groupId);
        selectionPanel.loadDirectory();
        jPanelBody.add(selectionPanel, java.awt.BorderLayout.CENTER);

        setSize(500, 500);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void showDefinitionDeletionDialog(java.awt.Frame parent, T8Context context, String groupId)
    {
        T8DefinitionDeletionDialog dialog;

        dialog = new T8DefinitionDeletionDialog(parent, context, groupId);
    }

    private boolean deleteSelectedEntry()
    {
        T8DefinitionMetaData selectedMetaData;
        int confirmation;

        // Get the selected map name.
        selectedMetaData = selectionPanel.getSelectedDefinitionMetaData();
        confirmation = JOptionPane.showConfirmDialog(null, "Are you sure that you want do delete the Definition '" + selectedMetaData.getId() + "' ?", "Deletion Confirmation", JOptionPane.YES_NO_OPTION);
        if (confirmation == JOptionPane.YES_OPTION)
        {
            try
            {
                clientContext.getDefinitionManager().deleteDefinition(context, selectedMetaData.getDefinitionHandle(), true, null);
                JOptionPane.showMessageDialog(null, "Definition Successfully deleted.", "Deletion Completed", JOptionPane.INFORMATION_MESSAGE);
                selectionPanel.loadDirectory();
                return true;
            }
            catch (Exception e)
            {
                T8Log.log("An error prevented the successful loading of Definition '" + selectedMetaData.getId() + "'.", e);
                JOptionPane.showMessageDialog(null, "The selected Definition could not be deleted.  Please check the database connection.", "Deletion Failed", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelHeader = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();
        jPanelBody = new javax.swing.JPanel();
        jPanelFooter = new javax.swing.JPanel();
        jButtonClose = new javax.swing.JButton();
        jLabelFooterSpacer = new javax.swing.JLabel();
        jButtonDelete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelHeader.setLayout(new java.awt.GridBagLayout());

        jLabelHeader.setText("Delete Definition");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelHeader.add(jLabelHeader, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        getContentPane().add(jPanelHeader, gridBagConstraints);

        jPanelBody.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        getContentPane().add(jPanelBody, gridBagConstraints);

        jPanelFooter.setLayout(new java.awt.GridBagLayout());

        jButtonClose.setText("Close");
        jButtonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCloseActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelFooter.add(jButtonClose, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelFooter.add(jLabelFooterSpacer, gridBagConstraints);

        jButtonDelete.setText("Delete");
        jButtonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelFooter.add(jButtonDelete, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        getContentPane().add(jPanelFooter, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteActionPerformed
        if (selectionPanel.getSelectedDefinitionMetaData() != null)
        {
            deleteSelectedEntry();
        }
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonCloseActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JLabel jLabelFooterSpacer;
    private javax.swing.JLabel jLabelHeader;
    private javax.swing.JPanel jPanelBody;
    private javax.swing.JPanel jPanelFooter;
    private javax.swing.JPanel jPanelHeader;
    // End of variables declaration//GEN-END:variables
}
