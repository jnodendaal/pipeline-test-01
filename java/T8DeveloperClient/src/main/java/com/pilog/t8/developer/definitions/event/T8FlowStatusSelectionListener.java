package com.pilog.t8.developer.definitions.event;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface T8FlowStatusSelectionListener extends EventListener
{
    public void selectionChanged(T8FlowStatusSelectionEvent event);
}
