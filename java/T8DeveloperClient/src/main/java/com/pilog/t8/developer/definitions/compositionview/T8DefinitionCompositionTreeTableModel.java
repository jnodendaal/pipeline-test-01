package com.pilog.t8.developer.definitions.compositionview;

import com.pilog.t8.definition.T8DefinitionComposition;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionCompositionTreeTableModel extends DefaultTreeTableModel
{
    // Column Types.
    public static Class[] types = new Class[]
    {
        java.lang.String.class, java.lang.Long.class, Object.class, Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class, java.lang.Long.class, java.lang.String.class, java.lang.Long.class, java.lang.String.class, java.lang.String.class
    };

    // Column Editability.
    public static boolean[] canEdit = new boolean[]
    {
        false, false, false, false, false, false, false, false, false, false, false, false, false
    };

    public static List<String> columnNames = ArrayLists.newArrayList
    (
        "Definition", "Revision", "Status", "Level", "Version", "Project", "Type", "Patched", "Created Time", "Created User", "Updated Time", "Updated User", "Checksum"
    );

    private T8DefinitionComposition composition;

    public T8DefinitionCompositionTreeTableModel(T8DefinitionComposition composition, boolean createTemporaryNodes)
    {
        super
        (
            null,
            columnNames
        );

        setComposition(composition, createTemporaryNodes);
    }

    public T8DefinitionCompositionTreeTableNode setComposition(T8DefinitionComposition composition, boolean createTemporaryNodes)
    {
        T8DefinitionCompositionTreeTableNode newRootNode;

        this.composition = composition;
        newRootNode = constructModel(composition, createTemporaryNodes);
        setRoot(newRootNode);
        return newRootNode;
    }

    public T8DefinitionCompositionTreeTableNode replaceCompositions(T8DefinitionComposition oldComposition, T8DefinitionComposition newComposition, boolean createTemporaryNodes)
    {
        T8DefinitionCompositionTreeTableNode oldCompositionNode;

        oldCompositionNode = findCompositionNode(oldComposition);
        if (oldCompositionNode != null)
        {
            T8DefinitionCompositionTreeTableNode oldCompositionParentNode;

            oldCompositionParentNode = (T8DefinitionCompositionTreeTableNode)oldCompositionNode.getParent();
            if (oldCompositionParentNode == null)
            {
                // So we are replacing the root node.
                return setComposition(newComposition, createTemporaryNodes);
            }
            else
            {
                T8DefinitionCompositionTreeTableNode newCompositionNode;
                T8DefinitionComposition oldCompositionParent;
                int oldCompositionNodeIndex;

                // Replace the old composition with the new one.
                oldCompositionParent = oldComposition.getReferrer();
                oldCompositionParent.replaceReference(oldComposition, newComposition);

                // Replace the old composition node with the new one.
                oldCompositionNodeIndex = oldCompositionParentNode.getIndex(oldCompositionNode);
                newCompositionNode = constructModel(newComposition, createTemporaryNodes);
                insertNodeInto(newCompositionNode, oldCompositionParentNode, oldCompositionNodeIndex);
                removeNodeFromParent(oldCompositionNode);
                return newCompositionNode;
            }
        }
        else throw new RuntimeException("Cannot find composition node: " + oldComposition.getDefinitionId());
    }

    private T8DefinitionCompositionTreeTableNode constructModel(T8DefinitionComposition inputComposition, boolean createTemporaryNodes)
    {
        if (inputComposition != null)
        {
            LinkedList<T8DefinitionCompositionTreeTableNode> nodeQueue;
            T8DefinitionCompositionTreeTableNode rootNode;

            // Create the root node from the input composition and add it to the processing queue.
            rootNode = new T8DefinitionCompositionTreeTableNode(inputComposition);
            nodeQueue = new LinkedList<>();
            nodeQueue.add(rootNode);
            while (!nodeQueue.isEmpty())
            {
                T8DefinitionCompositionTreeTableNode nextCompositionNode;
                T8DefinitionComposition nextComposition;
                Map<String, T8DefinitionCompositionTreeTableNode> typeNodes;

                // Get the next composition node from the queue for processing.
                nextCompositionNode = nodeQueue.removeFirst();
                typeNodes = new HashMap<>();
                nextComposition = nextCompositionNode.getDefinitionComposition();
                for (T8DefinitionComposition reference : nextComposition.getReferences())
                {
                    T8DefinitionTypeMetaData referenceTypeMetaData;
                    T8DefinitionCompositionTreeTableNode typeNode;
                    T8DefinitionCompositionTreeTableNode referenceNode;

                    // Get the applicable type node to which the composition node will be added (create one if it doesn't exist).
                    referenceTypeMetaData = reference.getDefinitionTypeMetaData();
                    typeNode = typeNodes.get(referenceTypeMetaData.getTypeId());
                    if (typeNode == null)
                    {
                        typeNode = new T8DefinitionCompositionTreeTableNode(referenceTypeMetaData);
                        nextCompositionNode.add(typeNode);
                        typeNodes.put(referenceTypeMetaData.getTypeId(), typeNode);
                    }

                    // Create the new composition node and add it to the type node.
                    referenceNode = new T8DefinitionCompositionTreeTableNode(reference);
                    typeNode.add(referenceNode);

                    // If temporary nodes are required, add one.
                    if (createTemporaryNodes) referenceNode.add(new T8DefinitionCompositionTreeTableNode("Loading..."));

                    // Add the composition node to the queue for subsequent processing.
                    nodeQueue.add(referenceNode);
                }
            }

            return rootNode;
        }
        else return null;
    }

    public T8DefinitionCompositionTreeTableNode findCompositionNode(T8DefinitionComposition composition)
    {
        if (root != null)
        {
            for (T8DefinitionCompositionTreeTableNode descendantNode : ((T8DefinitionCompositionTreeTableNode)root).getFamilyNodes())
            {
                if (descendantNode.getDefinitionComposition() == composition)
                {
                    return descendantNode;
                }
            }

            return null;
        }
        else return null;
    }

    public T8DefinitionComposition getComposition()
    {
        return composition;
    }

    @Override
    public Class getColumnClass(int columnIndex)
    {
        return types[columnIndex];
    }
}
