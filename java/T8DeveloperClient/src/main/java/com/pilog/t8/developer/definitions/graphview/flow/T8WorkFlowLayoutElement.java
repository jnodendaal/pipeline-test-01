package com.pilog.t8.developer.definitions.graphview.flow;

import com.pilog.graph.model.DefaultGraphVertex;
import com.pilog.t8.definition.flow.T8FlowBoundaryNodeDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.definition.flow.node.activity.T8FlowActivityDefinition;
import com.pilog.t8.definition.flow.node.event.T8EndEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8FlowEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8StartEventDefinition;
import com.pilog.t8.definition.flow.node.gateway.T8FlowGatewayDefinition;
import com.pilog.t8.developer.definitions.graphview.layout.LayoutElement;
import com.pilog.t8.developer.definitions.graphview.layout.LayoutInsets;
import java.awt.Dimension;
import java.awt.Point;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowLayoutElement extends DefaultGraphVertex implements LayoutElement
{
    private final T8WorkFlowNodeDefinition nodeDefinition;
    private final T8WorkFlowVertexRenderer renderer;
    private final LayoutInsets insets;
    private final int elementIndex;
    private final int workFlowStep;

    public T8WorkFlowLayoutElement(int elementIndex, int workFlowStep, T8WorkFlowNodeDefinition nodeDefinition, T8WorkFlowVertexRenderer renderer)
    {
        super(nodeDefinition.getIdentifier(), nodeDefinition.getXCoordinate(), nodeDefinition.getYCoordinate(), nodeDefinition);
        this.nodeDefinition = nodeDefinition;
        this.elementIndex = elementIndex;
        this.workFlowStep = workFlowStep;
        this.renderer = renderer;
        this.insets = new LayoutInsets(0, 0, 0, 0);
    }

    @Override
    public LayoutInsets getInsets()
    {
        return insets;
    }

    @Override
    public Dimension getSize()
    {
        return renderer.getVertexSize(null, this, false);
    }

    @Override
    public void doLayout(int x, int y)
    {
        Dimension size;
        Point location;

        size = getSize();
        location = new Point(x, y);
        location.translate(Math.round(size.width/2.0f), Math.round(size.height/2.0f));
        setVertexCoordinates(location);
        nodeDefinition.setXCoordinate(location.x);
        nodeDefinition.setYCoordinate(location.y);
    }

    @Override
    public void translate(int dx, int dy)
    {
        super.translate(dx, dy);
        nodeDefinition.setXCoordinate(getVertexCoordinates().x);
        nodeDefinition.setYCoordinate(getVertexCoordinates().y);
    }

    @Override
    public void setVertexCoordinates(Point coordinates)
    {
        super.setVertexCoordinates(coordinates);
        nodeDefinition.setXCoordinate(coordinates.x);
        nodeDefinition.setYCoordinate(coordinates.y);
    }

    public T8WorkFlowNodeDefinition getNodeDefinition()
    {
        return (T8WorkFlowNodeDefinition)getVertexDataObject();
    }

    public int getElementIndex()
    {
        return elementIndex;
    }

    public int getWorkFlowStep()
    {
        return workFlowStep;
    }

    public String getSwimLaneIdentifier()
    {
        T8WorkFlowNodeDefinition nodeDefinition;
        String userProfileIdentifier;

        nodeDefinition = getNodeDefinition();
        if (nodeDefinition instanceof T8FlowActivityDefinition)
        {
            userProfileIdentifier = ((T8FlowActivityDefinition)nodeDefinition).getProfileIdentifier();
            if (userProfileIdentifier == null) userProfileIdentifier = "SYSTEM";
            return userProfileIdentifier;
        }
        else if (nodeDefinition instanceof T8StartEventDefinition)
        {
            return "SYSTEM";
        }
        else if (nodeDefinition instanceof T8EndEventDefinition)
        {
            return "SYSTEM";
        }
        else if (nodeDefinition instanceof T8FlowBoundaryNodeDefinition)
        {
            T8WorkFlowDefinition flowDefinition;
            String attachedToNodeIdentifier;

            // Try to set the gateway in the same swimlane as its parent activity.
            flowDefinition = (T8WorkFlowDefinition)nodeDefinition.getParentDefinition();
            attachedToNodeIdentifier = ((T8FlowBoundaryNodeDefinition)nodeDefinition).getAttachedToNodeIdentifier();
            if (attachedToNodeIdentifier != null)
            {
                T8WorkFlowNodeDefinition attachedToNodeDefinition;

                attachedToNodeDefinition = flowDefinition.getNodeDefinition(attachedToNodeIdentifier);
                if (attachedToNodeDefinition instanceof T8FlowActivityDefinition)
                {
                    userProfileIdentifier = ((T8FlowActivityDefinition)attachedToNodeDefinition).getProfileIdentifier();
                    if (userProfileIdentifier == null) userProfileIdentifier = "SYSTEM";
                }
                userProfileIdentifier = null;
            }
            else userProfileIdentifier = "SYSTEM";

            return userProfileIdentifier;
        }
        else if ((nodeDefinition instanceof T8FlowGatewayDefinition) || (nodeDefinition instanceof T8FlowEventDefinition)) // All gateways and remaining events, share the swimlane of their parent.
        {
            T8WorkFlowDefinition flowDefinition;
            List<String> parentNodeIdentifiers;

            // Try to set the gateway in the same swimlane as its parent activity.
            flowDefinition = (T8WorkFlowDefinition)nodeDefinition.getParentDefinition();
            parentNodeIdentifiers = flowDefinition.getParentNodeIdentifiers(nodeDefinition.getIdentifier());
            if (parentNodeIdentifiers.size() > 0)
            {
                T8WorkFlowNodeDefinition parentNodeDefinition;

                parentNodeDefinition = flowDefinition.getNodeDefinition(parentNodeIdentifiers.get(0));
                if (parentNodeDefinition instanceof T8FlowActivityDefinition)
                {
                    userProfileIdentifier = ((T8FlowActivityDefinition)parentNodeDefinition).getProfileIdentifier();
                    if (userProfileIdentifier == null) userProfileIdentifier = null;
                }
                userProfileIdentifier = null;
            }
            else userProfileIdentifier = null;

            return userProfileIdentifier;
        }
        else return "SYSTEM";
    }

    public boolean isBoundaryNode()
    {
        return (nodeDefinition instanceof T8FlowBoundaryNodeDefinition);
    }

    public String getAttachedToNodeIdentifier()
    {
        if (nodeDefinition instanceof T8FlowBoundaryNodeDefinition)
        {
            return ((T8FlowBoundaryNodeDefinition)nodeDefinition).getAttachedToNodeIdentifier();
        }
        else return null;
    }

    public int getXCoordinate()
    {
        return getNodeDefinition().getXCoordinate();
    }

    public int getYCoordinate()
    {
        return getNodeDefinition().getYCoordinate();
    }
}
