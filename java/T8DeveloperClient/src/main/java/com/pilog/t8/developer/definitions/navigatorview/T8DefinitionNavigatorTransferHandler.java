package com.pilog.t8.developer.definitions.navigatorview;

import com.pilog.t8.developer.definitions.T8DeveloperDefinitionContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionConstructorParameters;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.script.T8ComponentEventHandlerScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.developer.definitions.T8DefinitionTransferable;
import com.pilog.t8.developer.definitions.navigatorview.T8DefinitionNavigatorTree.T8DefinitionNavigatorDatum;
import com.pilog.t8.security.T8Context;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionNavigatorTransferHandler extends TransferHandler
{
    private final T8Context context;
    private final T8DefinitionContext definitionContext;
    private DefaultMutableTreeNode nodeToRemove;

    public T8DefinitionNavigatorTransferHandler(T8Context context)
    {
        this.context = context;
        this.definitionContext = new T8DeveloperDefinitionContext(context);
    }

    @Override
    public boolean canImport(JComponent component, DataFlavor[] dataFlavors)
    {
        return true;
    }

    @Override
    public boolean canImport(TransferSupport support)
    {
        // Show the drop location.
        if(support.isDrop()) support.setShowDropLocation(true);

        // Check that the desired data flavor is supported.
        if (!support.isDataFlavorSupported(T8DefinitionTransferable.getDefinitionDataFlavor()))
        {
            return false;
        }
        else return true;
    }

    @Override
    public boolean importData(TransferSupport support)
    {
        if (!canImport(support))
        {
            return false;
        }
        else if(support.isDrop())
        {
            try
            {
                T8Definition transferredDefinition;
                T8DefinitionNavigatorTree targetTree;
                DefaultMutableTreeNode targetNode;
                Object targetObject;
                JTree.DropLocation dropLocation;
                int targetIndex;
                TreePath dropPath;

                // Get the transferred definition.
                try
                {
                    Transferable transferable;

                    transferable = support.getTransferable();
                    transferredDefinition = (T8Definition)transferable.getTransferData(T8DefinitionTransferable.getDefinitionDataFlavor());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    return false;
                }

                // Get drop location info.
                dropLocation = (JTree.DropLocation)support.getDropLocation();
                targetIndex = dropLocation.getChildIndex();
                dropPath = dropLocation.getPath();
                targetNode = (DefaultMutableTreeNode)dropPath.getLastPathComponent();
                targetObject = targetNode.getUserObject();
                targetTree = (T8DefinitionNavigatorTree)support.getComponent();

                if (targetObject instanceof T8Definition && !(((T8Definition)targetObject).getIdentifier().equals(transferredDefinition.getIdentifier())))
                {
                    DefaultMutableTreeNode parentNode;
                    parentNode = (DefaultMutableTreeNode)targetNode.getParent();
                    if (parentNode != null)
                    {
                        T8Definition targetDefinition;
                        T8DefinitionNavigatorDatum datum;
                        ArrayList<T8DefinitionDatumOption> datumOptions;

                        targetDefinition = (T8Definition)targetObject;
                        datum = (T8DefinitionNavigatorDatum)parentNode.getUserObject();
                        datumOptions = targetDefinition.getParentDefinition().getDatumOptions(definitionContext, datum.getDatumIdentifier());

                        if(datumOptions != null)
                        {
                            for (T8DefinitionDatumOption t8DefinitionDatumOption : datumOptions)
                            {
                                Object datumOptionValue;

                                datumOptionValue = t8DefinitionDatumOption.getValue();
                                if(datumOptionValue instanceof T8DefinitionConstructorParameters)
                                {
                                    T8DefinitionConstructorParameters constructorParameters;

                                    constructorParameters = (T8DefinitionConstructorParameters)datumOptionValue;

                                    if(constructorParameters.getTypeIdentifier().equals(transferredDefinition.getTypeMetaData().getTypeId()))
                                    {
                                        // Configure for drop mode.
                                        if (targetIndex == -1) // DropMode.INSERT
                                        {
                                            // DropMode.ON.
                                            targetTree.addDefinition(targetDefinition.getParentDefinition(), datum.getDatumIdentifier(), transferredDefinition, targetNode.getChildCount());
                                        }
                                        else
                                        {
                                            // DropMode.INSERT.
                                            targetTree.addDefinition(targetDefinition.getParentDefinition(), datum.getDatumIdentifier(), transferredDefinition, targetIndex);
                                        }
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
        } else if(support.getComponent() instanceof T8DefinitionNavigatorTree)
        {
            T8DefinitionNavigatorTree tree;
                T8Definition transferredDefinition;
            Object selectedNodeObject;

            tree = (T8DefinitionNavigatorTree)support.getComponent();
            selectedNodeObject = tree.getSelectedNode().getUserObject();

            try
            {
                Transferable transferable;

                transferable = support.getTransferable();
                transferredDefinition = (T8Definition)transferable.getTransferData(T8DefinitionTransferable.getDefinitionDataFlavor());
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }

            if(selectedNodeObject instanceof T8Definition)
            {
                T8Definition selectedDefinition;

                selectedDefinition = (T8Definition)selectedNodeObject;
                for (T8DefinitionDatumType t8DefinitionDatumType : selectedDefinition.getDatumTypes())
                {
                    try
                    {
                        ArrayList<T8DefinitionDatumOption> datumOptions;

                        datumOptions = selectedDefinition.getDatumOptions(definitionContext, t8DefinitionDatumType.getIdentifier());
                        if(datumOptions != null)
                        {
                            for (T8DefinitionDatumOption t8DefinitionDatumOption : datumOptions)
                            {
                                Object datumOptionValue;

                                datumOptionValue = t8DefinitionDatumOption.getValue();
                                if(datumOptionValue instanceof T8DefinitionConstructorParameters)
                                {
                                    T8DefinitionConstructorParameters constructorParameters;

                                    constructorParameters = (T8DefinitionConstructorParameters)datumOptionValue;

                                    if(constructorParameters.getTypeIdentifier().equals(transferredDefinition.getTypeMetaData().getTypeId()))
                                    {
                                        tree.pasteDefinition(selectedDefinition, t8DefinitionDatumType.getIdentifier(), transferredDefinition);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        T8Log.log("Failed to paste definition, could not get datum options", ex);
                        return false;
                    }
                }
            }
            return false;
        } else return false;
    }

    @Override
    protected Transferable createTransferable(JComponent sourceComponent)
    {
        T8DefinitionNavigatorTree sourceTree;
        DefaultMutableTreeNode selectedNode;

        sourceTree = (T8DefinitionNavigatorTree)sourceComponent;
        selectedNode = sourceTree.getSelectedNode();

        nodeToRemove = selectedNode;

        if (selectedNode != null)
        {
            Object nodeObject;

            nodeObject = selectedNode.getUserObject();
            if (nodeObject instanceof T8Definition)
            {
                T8Definition definition;

                definition = (T8Definition)nodeObject;
                return new T8DefinitionTransferable(definition);
            }
            else return null;
        }
        else return null;
    }

    @Override
    protected void exportDone(JComponent source, Transferable data, int action)
    {
        if ((action & MOVE) == MOVE)
        {
            DefaultMutableTreeNode parentNode;
            Object dataObject;
            Object parentDataObject;

            if(nodeToRemove != null && nodeToRemove.getParent() != null)
            {
                parentNode = ((DefaultMutableTreeNode)nodeToRemove.getParent());
                // Remove the 'moved' definition from its previous parent.
                parentDataObject = parentNode.getUserObject();
                dataObject = nodeToRemove.getUserObject();
                if (dataObject instanceof T8ComponentDefinition)
                {
                }
                else if(dataObject instanceof T8ComponentEventHandlerScriptDefinition)
                {
                    ((T8ComponentDefinition)parentDataObject).removeEventHandlerScriptDefinition((T8ComponentEventHandlerScriptDefinition)dataObject);
                }

                // Remove node saved as nodeToRemove in createTransferable.
                ((T8DefinitionNavigatorTree)source).deleteNode(nodeToRemove);
            }
        }
    }

    @Override
    public int getSourceActions(JComponent c)
    {
        return COPY_OR_MOVE;
    }

}
