package com.pilog.t8.developer.definitions.tableview;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionLockDetails;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationProvider;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationType;
import com.pilog.t8.definition.event.T8DefinitionCacheReloadedEvent;
import com.pilog.t8.definition.event.T8DefinitionContextListener;
import com.pilog.t8.definition.event.T8DefinitionLockedEvent;
import com.pilog.t8.definition.event.T8DefinitionRenamedEvent;
import com.pilog.t8.definition.event.T8DefinitionSavedEvent;
import com.pilog.t8.definition.event.T8DefinitionUnlockedEvent;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionSelectionEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.definition.system.T8ConfigurationManagerResource;
import com.pilog.t8.developer.definitions.T8DefinitionDocumentationFrame;
import com.pilog.t8.developer.definitions.dialog.T8IdentifierSelectionDialog;
import com.pilog.t8.developer.definitions.dialog.T8InformationDialog;
import com.pilog.t8.developer.definitions.history.T8DefinitionHistoryPanel;
import com.pilog.t8.developer.definitions.renderer.T8DefinitionTypeMetaDataListCellRenderer;
import com.pilog.t8.developer.definitions.selectiontableview.T8DefinitionRowFilter;
import com.pilog.t8.developer.definitions.selectiontableview.T8DefinitionTable;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import com.pilog.t8.developer.utils.T8DefinitionValidationUtils;
import com.pilog.t8.developer.view.T8DefaultDeveloperView;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.components.list.ListPopupMenuListener;
import com.pilog.t8.utilities.components.list.popupmenu.ListPopupMenu;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.components.messagedialogs.Toast.Style;
import com.pilog.t8.utilities.components.processingdialogs.ProcessingDialog;
import com.pilog.t8.utilities.components.processingdialogs.ThreadTask;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 * Displays all definitions from a specific group in a table from which a single
 * definition can be selected.
 *
 * @author Bouwer du Preez
 */
public class T8DefinitionTableView extends T8DefaultDeveloperView implements T8DeveloperView
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefinitionTableView.class.getName());

    protected T8Context context;
    protected T8ClientContext clientContext;
    protected T8SessionContext sessionContext;
    protected T8DefinitionContext definitionContext;
    protected String groupId;
    protected T8DeveloperView parentView;
    private FilterKeyListener filterListener;
    private SearchKeyListener searchListener;
    private SelectionListener selectionListener;
    protected T8DeveloperViewFactory viewFactory;
    protected T8DefinitionTable definitionTable;
    protected T8Definition selectedDefinition;
    protected TableRowSorter<TableModel> tableRowSorter;
    protected T8DefinitionRowFilter rowFilter;
    private DefinitionContextListener definitionContextListener;
    protected final HashMap<T8DefinitionHandle, T8Definition> newDefinitions;
    protected final HashSet<T8DefinitionHandle> deletedDefinitions;
    protected final HashMap<T8DefinitionHandle, T8Definition> lockedDefinitions;

    public T8DefinitionTableView(T8DefinitionContext definitionContext, T8DeveloperView parentView, String groupIdentifier, boolean searchVisible)
    {
        this(definitionContext, parentView, groupIdentifier, searchVisible, null);
    }

    public T8DefinitionTableView(T8DefinitionContext definitionContext, T8DeveloperView parentView, String groupIdentifier, boolean searchVisible, String viewHeader)
    {
        initComponents();
        this.context = definitionContext.getContext();
        this.clientContext = definitionContext.getClientContext();
        this.sessionContext = definitionContext.getSessionContext();
        this.definitionContext = definitionContext;
        this.parentView = parentView;
        this.viewFactory = parentView != null ? parentView.getViewFactory() : null;
        this.groupId = groupIdentifier;
        this.setBorder(new T8ContentHeaderBorder(viewHeader != null ? viewHeader : getHeader()));
        this.definitionContextListener = new DefinitionContextListener();
        this.filterListener = new FilterKeyListener();
        this.searchListener = new SearchKeyListener();
        this.selectionListener = new SelectionListener();
        this.definitionTable = new T8DefinitionTable(context);
        this.definitionTable.setSelectedColumnVisible(false);
        this.definitionTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.definitionTable.clearSelection();
        this.definitionTable.addMouseListener(new TableMouseListener());
        this.definitionTable.getSelectionModel().addListSelectionListener(selectionListener);
        this.jScrollPaneDefinitions.setViewportView(definitionTable);
        this.newDefinitions = new HashMap<>();
        this.lockedDefinitions = new HashMap<>();
        this.deletedDefinitions = new HashSet<>();
        this.definitionTable.setIdentifierCellRenderer(new T8DefinitionIdentifierCellRenderer(this));
        this.jTextFieldFilter.addKeyListener(filterListener);
        this.jTextFieldSearchText.addKeyListener(searchListener);
        this.tableRowSorter = new TableRowSorter<>(definitionTable.getModel());
        this.rowFilter = new T8DefinitionRowFilter();
        this.tableRowSorter.setRowFilter(this.rowFilter);
        definitionTable.setRowSorter(tableRowSorter);

        setSearchVisible(searchVisible);
        refresh();

        //Lets populate the filter combo box with the columns contained in the table
        List<String> columnNames = new LinkedList<>();
        for (int i = 0; i < definitionTable.getColumnCount(); i++)
        {
            columnNames.add(definitionTable.getColumnName(i));
        }
        columnNames.add("EPIC");

        jComboFilter.setModel(new DefaultComboBoxModel<>(columnNames.toArray(new String[]{})));
        this.definitionTable.sortByIdentifier(SortOrder.ASCENDING);

        createActionBindings();
    }

    @Override
    public void startComponent()
    {
        definitionContext.addContextListener(definitionContextListener);
    }

    @Override
    public void stopComponent()
    {
        definitionContext.removeContextListener(definitionContextListener);
    }

    @Override
    public String getHeader()
    {
        if (groupId != null)
        {
            return "Definition Group: " + groupId;
        }
        else return "Definitions";
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        parentView.addView(view);
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        parentView.removeView(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
        parentView.notifyDefinitionLinkActivated(event);
    }

    public boolean isDefinitionUpdated(int rowIndex)
    {
        return isDefinitionUpdated(definitionTable.getDefinitionHandle(rowIndex));
    }

    public boolean isDefinitionUpdated(T8DefinitionHandle definitionHandle)
    {
        return lockedDefinitions.containsKey(definitionHandle) || newDefinitions.containsKey(definitionHandle);
    }

    public boolean isDefinitionDeleted(int rowIndex)
    {
        return isDefinitionDeleted(definitionTable.getDefinitionHandle(rowIndex));
    }

    public boolean isDefinitionDeleted(T8DefinitionHandle definitionHandle)
    {
        return deletedDefinitions.contains(definitionHandle);
    }

    public boolean isDefinitionPatched(T8DefinitionHandle definitionHandle)
    {
        T8DefinitionMetaData definitionMetaData;

        definitionMetaData = definitionTable.getDefinitionMetaData(definitionHandle);
        if (definitionMetaData != null)
        {
            return definitionMetaData.isPatched() || definitionMetaData.isPatchedContent();
        }
        else throw new IllegalArgumentException("Definition not found: " + definitionHandle);
    }

    @Override
    public boolean canClose()
    {
        if (!newDefinitions.isEmpty() || !lockedDefinitions.isEmpty() || !deletedDefinitions.isEmpty())
        {
            int dialogOption;

            dialogOption = JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "You have unsaved changes on " + getHeader() + " do you want to save these changes?");
            if (dialogOption == JOptionPane.CANCEL_OPTION) return false;
            else if (dialogOption == JOptionPane.NO_OPTION)
            {
                // First unlock all locked definitions.
                Iterator<Map.Entry<T8DefinitionHandle, T8Definition>> definitionIter;

                definitionIter = lockedDefinitions.entrySet().iterator();
                while (definitionIter.hasNext())
                {
                    Map.Entry<T8DefinitionHandle, T8Definition> updatedDefinition = definitionIter.next();

                    try
                    {
                        LOGGER.log("Unlocking definition: " + updatedDefinition.getValue() + " (" + updatedDefinition.getKey().getHandleIdentifier() + ")");
                        definitionContext.unlockDefinition(updatedDefinition.getKey(), sessionContext.getUserIdentifier());
                        definitionIter.remove();
                    }
                    catch (Exception e)
                    {
                        LOGGER.log(e);
                    }
                }

                return true;
            }
            else // JOptionPane.YES_OPTION
            {
                saveChanges();
            }
        }

        return true;
    }

    protected void setHeaderText(String text)
    {
        this.setBorder(new T8ContentHeaderBorder(text));
    }

    public void setFilterHandles(List<T8DefinitionHandle> filterHandles) throws Exception
    {
        definitionTable.setDefinitionHandles(filterHandles);
    }

    public void setEditable(boolean editable)
    {
        setSaveEnabled(editable);
        setAddEnabled(editable);
        setRemoveEnabled(editable);
        setRenameEnabled(editable);
    }

    public void setSaveEnabled(boolean enabled)
    {
        jButtonSaveDefinitions.setVisible(false);
    }

    public void setAddEnabled(boolean enabled)
    {
        jButtonRemove.setVisible(false);
    }

    public void setRemoveEnabled(boolean enabled)
    {
        jButtonRemove.setVisible(false);
    }

    public void setRenameEnabled(boolean enabled)
    {
        jButtonRenameDefinition.setVisible(false);
    }

    public void setTitle(String title)
    {
        this.setBorder(new T8ContentHeaderBorder(title));
    }

    private void clearSelection()
    {
        definitionTable.clearSelection();
    }

    public List<T8DefinitionHandle> getSelectedDefinitionHandles()
    {
        return definitionTable.getSelectedDefinitionHandles();
    }

    /**
     * Gets all of the {@code T8DefinitionHandle} instances which are currently
     * associated with the contained {@code T8DefinitionTable}.
     *
     * @return The {@code List<T8DefinitionHandle} containing all of the handles
     *      for the current table view
     */
    public List<T8DefinitionHandle> getAllDefinitionHandles()
    {
        return this.definitionTable.getAllDefinitionHandles();
    }

    public void setSelectedDefinitionHandle(T8DefinitionHandle definitionHandle)
    {
        definitionTable.setSelectedDefinition(definitionHandle);
    }

    protected void setToolBarVisible(boolean visible)
    {
        jToolBarMain.setVisible(visible);
    }

    protected void setSearchVisible(boolean visible)
    {
        jPanelSearchCriteria.setVisible(visible);
        jLabelSearch.setVisible(visible);
    }

    private void addNewDefinition(T8Definition definition)
    {
        T8DefinitionHandle definitionHandle;

        definitionHandle = definition.getHandle();
        newDefinitions.put(definitionHandle, definition);
        definitionTable.addDefinitionMetaData(definition.getMetaData());
        definitionTable.setSelectedDefinition(definitionHandle);
    }

    private void deleteDefinition(T8DefinitionHandle definitionHandle) throws Exception
    {
        newDefinitions.remove(definitionHandle);
        lockedDefinitions.remove(definitionHandle);
        deletedDefinitions.add(definitionHandle);
    }

    protected void clearDefinitionStatus(T8DefinitionHandle definitionHandle)
    {
        lockedDefinitions.remove(definitionHandle);
        deletedDefinitions.remove(definitionHandle);
    }

    protected void filterDefinitions(String filterString)
    {
        this.rowFilter.setFilterText(filterString);
        this.rowFilter.setFilterColumn((String)jComboFilter.getSelectedItem());
        this.tableRowSorter.sort();
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    @Override
    public void setSelectedDefinition(T8Definition selectedDefinition)
    {
    }

    private void addNewDefinition(String definitionTypeIdentifier)
    {
        try
        {
            T8DefinitionMetaData metaData;

            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this, definitionContext.getDefinitionTypeMetaData(definitionTypeIdentifier), null);
            if (metaData != null)
            {
                try
                {
                    T8Definition newDefinition;

                    newDefinition = definitionContext.createNewDefinition(metaData.getId(), definitionTypeIdentifier);
                    newDefinition.setProjectIdentifier(metaData.getProjectId());
                    addNewDefinition(newDefinition);
                }
                catch (Exception e)
                {
                    LOGGER.log(e);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while fetching meta data for type: " + definitionTypeIdentifier, e);
        }
    }

    private void transferDefinitionReferences(T8Definition definition)
    {
        try
        {
            String oldIdentifier;
            String newIdentifier;
            List<String> identifierList;

            oldIdentifier = definition.getPublicIdentifier();
            identifierList = definitionContext.getGroupDefinitionIdentifiers(definition.getRootProjectId(), definition.getMetaData().getGroupId());
            newIdentifier = T8IdentifierSelectionDialog.getSelectedIdentifier(clientContext, "Please select the new identifier to which references currently\npointing to '" + oldIdentifier + "' will be transferred.", identifierList);
            if (newIdentifier != null)
            {
                int confirmation;

                confirmation = JOptionPane.showConfirmDialog(this, "All references to '" + oldIdentifier + "' will be changed to '" + newIdentifier + "'.  This operation is not reversable.\nAre you sure you want to continue?", "User Confirmation", JOptionPane.YES_NO_OPTION);
                if (confirmation != JOptionPane.YES_OPTION) return;
                else
                {
                    definitionContext.transferDefinitionReferences(oldIdentifier, newIdentifier);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception dring identifier transfer from: " + definition, e);
            JOptionPane.showMessageDialog(clientContext.getParentWindow(), "All references could not be transferred successfully.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void removeDefinitionReferences(T8DefinitionHandle definitionHandle)
    {
        if (definitionHandle != null)
        {
            int confirmation;

            // Get user confirmation.
            confirmation = JOptionPane.showConfirmDialog(this, "All references to this definition will be removed throughout the system.  Do you want to continue?", "Confirmation", JOptionPane.YES_NO_OPTION);
            if (confirmation == JOptionPane.YES_OPTION)
            {
                try
                {
                    String referenceIdToRemove;

                    // Rename the definition.
                    referenceIdToRemove = definitionHandle.getDefinitionIdentifier();
                    definitionContext.removeDefinitionReferences(referenceIdToRemove, sessionContext.getSessionIdentifier());

                    // Also update local locked definitions to reflect the update.
                    for (T8Definition lockedDefinition : lockedDefinitions.values())
                    {
                        lockedDefinition.removeReferenceId(referenceIdToRemove, true);
                    }

                    // Toast the success.
                    Toast.show("All references to definition '" + referenceIdToRemove + "' removed.");
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while removing definition references to '" + definitionHandle + "'.", e);
                    JOptionPane.showMessageDialog(clientContext.getParentWindow(), "References to Definition '" + definitionHandle + "' could not be removed.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
                }
            }
            else return;
        }
        else
        {
            Toast.makeText(clientContext.getParentWindow(), "No Definition selected.", Style.ERROR).display();
        }
    }

    private void renameSelectedDefinition()
    {
        T8Definition definition;
        String definitionIdentifier;

        definition = selectedDefinition;
        if (definition != null)
        {
            T8DefinitionMetaData metaData;
            int confirmation;

            // Get user confirmation.
            confirmation = JOptionPane.showConfirmDialog(this, "All changes will be saved before you can rename the definition?", "Save Confirmation", JOptionPane.YES_NO_OPTION);
            if (confirmation != JOptionPane.YES_OPTION) return;

            // Rename the definition.
            definitionIdentifier = definition.getIdentifier().replace(definition.getTypeMetaData().getIdPrefix(), "");
            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this, definition.getTypeMetaData(), null, definition.getProjectIdentifier(), definitionIdentifier);
            if (metaData != null)
            {
                try
                {
                    T8DefinitionHandle oldDefinitiondHandle;

                    oldDefinitiondHandle = definition.getHandle();
                    definitionContext.renameDefinition(definition, metaData.getId());

                    if (lockedDefinitions.containsKey(oldDefinitiondHandle))
                    {
                        lockedDefinitions.remove(oldDefinitiondHandle);
                        lockedDefinitions.put(metaData.getDefinitionHandle(), definition);
                    }

                    refresh();
                    setSelectedDefinitionHandle(metaData.getDefinitionHandle());
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while renaming definition '" + definition.getIdentifier() + "' to '" + metaData.getId() + "'.", e);
                    JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + definition + "' could not be renamed.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else
        {
            Toast.makeText(clientContext.getParentWindow(), "No Definition selected.", Style.ERROR).display();
        }
    }

    private void displayAddDefinitionPopupMenu(final Component source, int x, int y)
    {
        ListPopupMenu popupMenu;
        List<T8DefinitionTypeMetaData> metaDataList;

        // Create the ActionListener for the menu items.
        ListPopupMenuListener menuListener = new ListPopupMenuListener()
        {
            @Override
            public void popupItemSelected(Object selectedIdentifier)
            {
                addNewDefinition(((T8DefinitionTypeMetaData)selectedIdentifier).getTypeId());
            }

            @Override
            public void popupItemsSelected(Object[] selectedItems)
            {
            }
        };

        try
        {
            List<T8DefinitionTypeMetaData> typeMetaDataList;

            // Get the list of definition type meta data and sort it.
            typeMetaDataList = definitionContext.getGroupDefinitionTypeMetaData(groupId);
            Collections.sort(typeMetaDataList, new Comparator<T8DefinitionTypeMetaData>()
            {
                @Override
                public int compare(T8DefinitionTypeMetaData o1, T8DefinitionTypeMetaData o2)
                {
                    if (o1 == null && o2 == null)
                    {
                        return 0;
                    }
                    if (o1 != null && o2 == null)
                    {
                        return -1;
                    }
                    if (o1 == null && o2 != null)
                    {
                        return 1;
                    }

                    return o1.getDisplayName().compareTo(o2.getDisplayName());
                }
            });

            // Add all definition options to the menu.
            metaDataList = new ArrayList<>();
            for (T8DefinitionTypeMetaData typeMetaData : typeMetaDataList)
            {
                // Do not show resource definitions.
                if(typeMetaData.getDefinitionLevel() != T8Definition.T8DefinitionLevel.RESOURCE)
                {
                    metaDataList.add(typeMetaData);
                }
            }

            // Create a new popup menu and show it.
            popupMenu = new ListPopupMenu(metaDataList);
            popupMenu.setCellRenderer(new T8DefinitionTypeMetaDataListCellRenderer());
            popupMenu.addPopupListener(menuListener);
            popupMenu.show(source, x, y);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while fetching type meta data for group: " + groupId, e);
        }
    }

    protected void refresh()
    {
        if (groupId != null)
        {
            try
            {
                Iterator<T8DefinitionHandle> lockedDefinitionHandles;
                List<T8DefinitionHandle> loadedHandles;

                // First unlock all locked definitions.
                lockedDefinitionHandles = lockedDefinitions.keySet().iterator();
                while (lockedDefinitionHandles.hasNext())
                {
                    T8DefinitionHandle lockedHandle = lockedDefinitionHandles.next();
                    try
                    {
                        LOGGER.log("Unlocking definition: " + lockedHandle.getDefinitionIdentifier() + " (" + lockedHandle.getHandleIdentifier() + ")");
                        definitionContext.unlockDefinition(lockedHandle, sessionContext.getUserIdentifier());
                        lockedDefinitionHandles.remove();
                    }
                    catch (Exception e)
                    {
                        LOGGER.log(e);
                    }
                }

                // Now reset the state.
                newDefinitions.clear();
                lockedDefinitions.clear();
                deletedDefinitions.clear();
                loadedHandles = loadDefinitionDirectory(groupId);
                setFilterHandles(loadedHandles);
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }
        }
    }

    protected List<T8DefinitionHandle> loadDefinitionDirectory(String groupId) throws Exception
    {
        List<T8DefinitionHandle> definitionHandleList;
        List<T8DefinitionMetaData> metaDataList;

        definitionHandleList = new ArrayList<>();
        metaDataList = clientContext.getDefinitionManager().getGroupDefinitionMetaData(null, groupId);
        for (T8DefinitionMetaData metaData : metaDataList)
        {
            definitionHandleList.add(metaData.getDefinitionHandle());
        }

        return definitionHandleList;
    }

    protected void searchByText(String searchText)
    {
        try
        {
            setFilterHandles(definitionContext.findDefinitionsByText(searchText));
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while searching for definitions by text.", e);
        }
    }

    protected void searchByIdentifier(String identifier)
    {
        try
        {
            setFilterHandles(definitionContext.findDefinitionsByIdentifier(identifier, true));
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while searching for definitions by identifier '" + identifier + "'.", e);
        }
    }

    private void saveChanges()
    {
        ArrayList<String> definitionsToValidate;
        ArrayList<T8Definition> changesToSave;
        ArrayList<T8DefinitionHandle> definitionsToDelete;
        boolean errorEncountered;

        // Clear the selection, to ensure that all changes to the last edited definition are committed.
        clearSelection();
        errorEncountered = false;

        // Need to keep track of all the definitions which will be validated if enabled
        definitionsToValidate = new ArrayList<>();

        // Save all new definitions.
        changesToSave = new ArrayList<>(newDefinitions.values());
        for (T8Definition newDefinition : changesToSave)
        {
            try
            {
                LOGGER.log("Saving new definition: " + newDefinition + " (" + newDefinition.getHandle().getHandleIdentifier() + ")");
                LOGGER.log("Meta Data Before Save: " + newDefinition.getMetaData());
                definitionContext.saveDefinition(newDefinition, sessionContext.getSessionIdentifier());
                // We can only validate the definition if it was saved
                definitionsToValidate.add(newDefinition.getIdentifier());
                LOGGER.log("Meta Data After Save: " + newDefinition.getMetaData());
                newDefinitions.remove(newDefinition.getHandle());
            }
            catch (Exception e)
            {
                errorEncountered = true;
                LOGGER.log("Failed to save definition.", e);
                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "New Definition '" + newDefinition + "' could not be updated.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                {
                    return;
                }
            }
        }

        // Save all updated definitions.
        changesToSave = new ArrayList<>(lockedDefinitions.values());
        for (T8Definition updatedDefinition : changesToSave)
        {
            try
            {
                LOGGER.log("Saving updated definition: " + updatedDefinition + " (" + updatedDefinition.getHandle().getHandleIdentifier() + ")");
                LOGGER.log("Meta Data Before Save: " + updatedDefinition.getMetaData());
                definitionContext.unlockDefinition(updatedDefinition, sessionContext.getUserIdentifier());
                LOGGER.log("Meta Data After Save: " + updatedDefinition.getMetaData());
                lockedDefinitions.remove(updatedDefinition.getHandle());
            }
            catch (Exception e)
            {
                errorEncountered = true;
                LOGGER.log(e);
                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + updatedDefinition + "' could not be updated.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                {
                    return;
                }
            }

            definitionsToValidate.add(updatedDefinition.getIdentifier());
        }

        // Delete definitions deleted on UI from server.
        definitionsToDelete = new ArrayList<>(deletedDefinitions);
        for (T8DefinitionHandle definitionToDelete : definitionsToDelete)
        {
            try
            {
                definitionContext.deleteDefinition(definitionToDelete, true, null);
                deletedDefinitions.remove(definitionToDelete);
                definitionTable.removeDefinitionMetaData(definitionToDelete);
            }
            catch (Exception e)
            {
                errorEncountered = true;
                LOGGER.log(e);
                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + definitionToDelete + "' could not be deleted.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                {
                    return;
                }
            }
        }

        // Show the result message.
        if (!errorEncountered)
        {
            Toast.makeText(clientContext.getParentWindow(), "Changes Saved Successfully.",Style.SUCCESS).display();
        }

        // If the validate on save option is enabled, we now want to validate the definitions
        if (this.clientContext.getConfigurationManager().getUserProperty(context, T8ConfigurationManagerResource.USR_PROP_VALIDATE_ON_SAVE, false))
        {
            try
            {
                T8DefinitionValidationUtils.validateDefinitions(SwingUtilities.windowForComponent(this), this, context, definitionsToValidate, true);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while validating definitions: " + definitionsToValidate, e);
                JOptionPane.showMessageDialog(this, "An unexpected exception occurred.", "Unexpected Exception", JOptionPane.ERROR_MESSAGE);
            }
        }

        // Repaint the UI to reflect changes.
        definitionTable.repaint();
    }

    private void deleteSelectedDefinitions()
    {
        List<T8DefinitionHandle> selectedDefinitionHandles;

        // Delete the definitions.
        selectedDefinitionHandles = getSelectedDefinitionHandles();
        if(selectedDefinitionHandles.isEmpty()) Toast.makeText(clientContext.getParentWindow(), "No Definition(s) selected.", Style.ERROR).display();
        for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
        {
            try
            {
                deleteDefinition(selectedDefinitionHandle);
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }
        }

        // Refresh the UI.
        definitionTable.repaint();
    }

    private class FilterKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                filterDefinitions(jTextFieldFilter.getText());
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    private class SearchKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                searchByText(jTextFieldSearchText.getText());
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    private void unlockDefinition(T8DefinitionHandle selectedDefinitionHandle)
    {
        try
        {
            definitionContext.unlockDefinition(selectedDefinitionHandle, sessionContext.getUserIdentifier());
            lockedDefinitions.remove(selectedDefinitionHandle);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while unlocking definition: " + selectedDefinitionHandle, e);
            JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be unlocked.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void getDefinitionLockDetails(T8DefinitionHandle selectedDefinitionHandle)
    {
        try
        {
            T8DefinitionLockDetails lockDetails;

            lockDetails = definitionContext.getDefinitionLockDetails(selectedDefinitionHandle);
            if (lockDetails != null)
            {
                StringBuffer message;

                message = new StringBuffer();
                message.append("Lock details for Definition '");
                message.append(selectedDefinitionHandle.toString()).append("'");
                message.append("\nLocked: ").append(lockDetails.isLocked());
                message.append("\nUser: ").append(lockDetails.getUserIdentifier());
                message.append("\nProfile: ").append(lockDetails.getUserProfileIdentifier());
                JOptionPane.showMessageDialog(clientContext.getParentWindow(), message, "Lock Details", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while fetching lock details for definition: " + selectedDefinitionHandle, e);
            JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Lock details for Definition '" + selectedDefinitionHandle + "' could not be fetched.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void editDefinition(T8DefinitionHandle selectedDefinitionHandle)
    {
        // When a definition is edited, we set the editable flag and fire a definition selection change.
        try
        {
            T8Definition lockedDefinition;

            lockedDefinition = definitionContext.lockDefinition(selectedDefinitionHandle, sessionContext.getUserIdentifier());
            lockedDefinitions.put(selectedDefinitionHandle, lockedDefinition);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while locking definition: " + selectedDefinitionHandle, e);
            JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be locked.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Handle the switch from non-editable to editable as a selection change
        // as we are actually switching from a non-editable version to an
        // editable version of the definition.
        handleDefinitionSelectionChange(selectedDefinitionHandle);
    }

    private void handleDefinitionSelectionChange(T8DefinitionHandle selectedDefinitionHandle)
    {
        T8DefinitionSelectionEvent event;

        // Get the selected definition.
        if (selectedDefinitionHandle == null)
        {
            selectedDefinition = null;
        }
        else if (newDefinitions.containsKey(selectedDefinitionHandle))
        {
            selectedDefinition = newDefinitions.get(selectedDefinitionHandle);
        }
        else if (lockedDefinitions.containsKey(selectedDefinitionHandle))
        {
            selectedDefinition = lockedDefinitions.get(selectedDefinitionHandle);
        }
        else
        {
            try
            {
                selectedDefinition = definitionContext.getRawDefinition(selectedDefinitionHandle);
                LOGGER.log("Loaded definition: " + selectedDefinition);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading definition: " + selectedDefinitionHandle, e);
                JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be loaded.\nReason: " + ExceptionUtilities.getRootCauseMessage(e), "Operation Failure", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        // Fire the selection event.
        event = new T8DefinitionSelectionEvent(this, selectedDefinition);
        for (T8DefinitionViewListener listener : definitionListeners)
        {
            listener.selectionChanged(event);
        }
    }

    private void displayPopupMenu(int x, int y)
    {
        final List<T8DefinitionHandle> selectedDefinitionHandles;

        selectedDefinitionHandles = getSelectedDefinitionHandles();
        if (selectedDefinitionHandles.size() > 0)
        {
            JPopupMenu popupMenu;
            JMenuItem menuItem;

            // Create the ActionListener for the menu items.
            ActionListener menuListener = new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    JMenuItem menuItem;
                    String code;

                    menuItem = (JMenuItem)event.getSource();
                    code = menuItem.getName();
                    if (code.equals("EDIT_DEFINITION"))
                    {
                        T8DefinitionHandle selectedDefinitionHandle;

                        selectedDefinitionHandle = selectedDefinitionHandles.get(0);
                        editDefinition(selectedDefinitionHandle);
                    }
                    else if (code.equals("GET_DEFINITION_LOCK_DETAILS"))
                    {
                        T8DefinitionHandle selectedDefinitionHandle;

                        selectedDefinitionHandle = selectedDefinitionHandles.get(0);
                        getDefinitionLockDetails(selectedDefinitionHandle);
                    }
                    else if (code.equals("UNLOCK_DEFINITION"))
                    {
                        T8DefinitionHandle selectedDefinitionHandle;

                        selectedDefinitionHandle = selectedDefinitionHandles.get(0);
                        unlockDefinition(selectedDefinitionHandle);
                    }
                    else if (code.equals("RENAME_DEFINITION"))
                    {
                        renameSelectedDefinition();
                    }
                    else if (code.equals("REMOVE_DEFINITION_REFERENCES"))
                    {
                        removeDefinitionReferences(selectedDefinitionHandles.get(0));
                    }
                    else if (code.equals("TRANSFER_DEFINITION_REFERENCES"))
                    {
                        transferDefinitionReferences(selectedDefinition);
                    }
                    else if (code.equals("DEFINITION_COMPOSITION"))
                    {
                        if (selectedDefinition != null)
                        {
                            parentView.addView(viewFactory.createDefinitionCompositionView(parentView, selectedDefinition.getRootProjectId(), selectedDefinition.getIdentifier()));
                        }
                    }
                    else if (code.equals("FIND_DEFINITION_USAGES"))
                    {
                        if (selectedDefinition != null)
                        {
                            parentView.addView(viewFactory.createDefinitionUsagesManager(parentView, selectedDefinition));
                        }
                    }
                    else if (code.equals("MOVE_DEFINITIONS_TO_PROJECT"))
                    {
                        moveSelectedDefinitionsToProject();
                    }
                    else if (code.equals("COPY_DEFINITIONS_TO_PROJECT"))
                    {
                        copySelectedDefinitionsToProject();
                    }
                }
            };

            // Create a new popup menu.
            popupMenu = new JPopupMenu();

            menuItem = new JMenuItem("Edit Selected Definition");
            menuItem.setName("EDIT_DEFINITION");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Definition Composition");
            menuItem.setName("DEFINITION_COMPOSITION");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Get Definition Lock Details");
            menuItem.setName("GET_DEFINITION_LOCK_DETAILS");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Unlock Definition");
            menuItem.setName("UNLOCK_DEFINITION");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Rename Definition");
            menuItem.setName("RENAME_DEFINITION");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Remove Definition References");
            menuItem.setName("REMOVE_DEFINITION_REFERENCES");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Transfer Definition References");
            menuItem.setName("TRANSFER_DEFINITION_REFERENCES");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Find Usages");
            menuItem.setName("FIND_DEFINITION_USAGES");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Move Selected Definitions to Project");
            menuItem.setName("MOVE_DEFINITIONS_TO_PROJECT");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            menuItem = new JMenuItem("Copy Selected Definitions to Project");
            menuItem.setName("COPY_DEFINITIONS_TO_PROJECT");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            // Display the newly constructed popup menu.
            popupMenu.show(definitionTable, x, y);
        }
    }

    private void moveSelectedDefinitionsToProject()
    {
        if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Unsaved changes to selected definitions will be lost.  Proceed?", "Operation Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            try
            {
                List<T8DefinitionHandle> selectedDefinitionHandles;

                selectedDefinitionHandles = getSelectedDefinitionHandles();
                if (selectedDefinitionHandles.size() > 0)
                {
                    String newProjectId;

                    newProjectId = T8IdentifierSelectionDialog.getSelectedIdentifier(clientContext, "Please select a project to which the definitions will be moved.", definitionContext.getGroupDefinitionIdentifiers(null, T8ProjectDefinition.GROUP_IDENTIFIER));
                    if (newProjectId != null)
                    {
                        for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
                        {
                            try
                            {
                                LOGGER.log("Moving Definition '" + selectedDefinitionHandle + "' to project: " + newProjectId);
                                definitionContext.moveDefinitionToProject(selectedDefinitionHandle.getProjectIdentifier(), selectedDefinitionHandle.getDefinitionIdentifier(), newProjectId);
                                clearDefinitionStatus(selectedDefinitionHandle);
                            }
                            catch (Exception e)
                            {
                                LOGGER.log("Exception while moving Definition '" + selectedDefinitionHandle + "' to project: " + newProjectId, e);
                                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be moved to project '" + newProjectId + "'.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                                {
                                    return;
                                }
                            }
                        }
                    }

                    // Notify the user of the successful execution.
                    Toast.makeText(clientContext.getParentWindow(), "Definitions Moved Successfully", Style.SUCCESS).display();
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while moving definitions to new project.", e);
            }
        }
    }

    private void copySelectedDefinitionsToProject()
    {
        if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Unsaved changes to selected definitions will be lost.  Proceed?", "Operation Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            try
            {
                List<T8DefinitionHandle> selectedDefinitionHandles;

                selectedDefinitionHandles = getSelectedDefinitionHandles();
                if (selectedDefinitionHandles.size() > 0)
                {
                    String newProjectId;

                    newProjectId = T8IdentifierSelectionDialog.getSelectedIdentifier(clientContext, "Please select a project to which the definitions will be copied.", definitionContext.getGroupDefinitionIdentifiers(null, T8ProjectDefinition.GROUP_IDENTIFIER));
                    if (newProjectId != null)
                    {
                        for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
                        {
                            try
                            {
                                LOGGER.log("Copying Definition '" + selectedDefinitionHandle + "' to project: " + newProjectId);
                                definitionContext.copyDefinitionToProject(selectedDefinitionHandle.getProjectIdentifier(), selectedDefinitionHandle.getDefinitionIdentifier(), newProjectId);
                                clearDefinitionStatus(selectedDefinitionHandle);
                            }
                            catch (Exception e)
                            {
                                LOGGER.log("Exception while copying Definition '" + selectedDefinitionHandle + "' to project: " + newProjectId, e);
                                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be copied to project '" + newProjectId + "'.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                                {
                                    return;
                                }
                            }
                        }
                    }

                    // Notify the user of the successful execution.
                    Toast.makeText(clientContext.getParentWindow(), "Definitions Copied Successfully", Style.SUCCESS).display();
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while moving definitions to new project.", e);
            }
        }
    }

    private void copyDefinition()
    {
        if (selectedDefinition != null)
        {
            T8DefinitionMetaData metaData;
            String suffix;

            suffix = selectedDefinition.getIdentifier().replace(selectedDefinition.getTypeMetaData().getIdPrefix(), "") + "_COPY";
            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, this.getRootPane(), selectedDefinition.getTypeMetaData(), null, selectedDefinition.getProjectIdentifier(), suffix);
            if (metaData != null)
            {
                try
                {
                    T8Definition newDefinition;

                    newDefinition = definitionContext.copyDefinition(selectedDefinition, metaData.getId());
                    newDefinition = T8DefinitionUtilities.clearAuditingData(newDefinition);
                    newDefinition.setProjectIdentifier(metaData.getProjectId());
                    addNewDefinition(newDefinition);
                }
                catch (Exception e)
                {
                    LOGGER.log(e);
                }
            }
        }
        else Toast.makeText(clientContext.getParentWindow(), "No definition selected.", Style.ERROR).display();
    }

    private void applyPatchChanges()
    {
        if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "All patched changes will be saved for selected definitions.  Proceed?", "Operation Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            try
            {
                List<T8DefinitionHandle> selectedDefinitionHandles;

                selectedDefinitionHandles = getSelectedDefinitionHandles();
                if (selectedDefinitionHandles.size() > 0)
                {
                    for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
                    {
                        if (isDefinitionPatched(selectedDefinitionHandle))
                        {
                            try
                            {
                                T8Definition definitionToUpdate;

                                // In order to apply patch changes, we simply need to load the definition and save it again.
                                LOGGER.log("Applying patch changes to Definition '" + selectedDefinitionHandle);
                                definitionToUpdate = definitionContext.lockDefinition(selectedDefinitionHandle, sessionContext.getUserIdentifier());
                                definitionContext.unlockDefinition(definitionToUpdate, sessionContext.getUserIdentifier());
                                clearDefinitionStatus(selectedDefinitionHandle);
                            }
                            catch (Exception e)
                            {
                                LOGGER.log("Exception while applying patch changes to Definition '" + selectedDefinitionHandle + "'.", e);
                                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), "Definition '" + selectedDefinitionHandle + "' could not be updated.\nReason: " + ExceptionUtilities.getRootCauseMessage(e) + "\nProceed?", "Operation Failure", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                                {
                                    return;
                                }
                            }
                        }
                    }

                    // Notify the user of the successful execution.
                    Toast.makeText(clientContext.getParentWindow(), "Patch Changes Applied Successfully", Style.SUCCESS).display();
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while applying patch changes.", e);
            }
        }
    }

    private void showDocumentationPopupMenu(JComponent source, Point location)
    {
        try
        {
            final List<T8DefinitionHandle> selectedDefinitionHandles;

            // Get the selected definition handles.
            selectedDefinitionHandles = getSelectedDefinitionHandles();
            if (selectedDefinitionHandles.size() > 0)
            {
                final T8DefinitionDocumentationProvider documentationProvider;
                T8Definition firstDefinition;
                JPopupMenu popupMenu;
                JMenuItem menuItem;

                // Get the documentation provider from the definition.
                firstDefinition = definitionContext.getRawDefinition(selectedDefinitionHandles.get(0));
                documentationProvider = firstDefinition.createDocumentationProvider(definitionContext);

                // Create the ActionListener for the menu items.
                ActionListener menuListener = new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent event)
                    {
                        JMenuItem menuItem;
                        String identifier;

                        menuItem = (JMenuItem)event.getSource();
                        identifier = menuItem.getName();
                        if (identifier != null)
                        {
                            T8DefinitionDocumentationFrame documentationFrame;
                            List<T8Definition> selectedDefinitions;

                            selectedDefinitions = new ArrayList<>();
                            for (T8DefinitionHandle selectedDefinitionHandle : selectedDefinitionHandles)
                            {
                                try
                                {
                                    selectedDefinitions.add(definitionContext.getRawDefinition(selectedDefinitionHandle));
                                }
                                catch (Exception e)
                                {
                                    LOGGER.log("Exception while generating documentation for selected definition: " + selectedDefinitionHandle, e);
                                }
                            }

                            documentationFrame = new T8DefinitionDocumentationFrame(context, selectedDefinitions, identifier);
                            documentationFrame.setVisible(true);
                        }
                    }
                };

                // Create a new popup menu.
                popupMenu = new JPopupMenu();
                for (T8DefinitionDocumentationType documentationType : documentationProvider.getDocumentationTypes())
                {
                    menuItem = new JMenuItem(documentationType.getDisplayName());
                    menuItem.setName(documentationType.getIdentifier());
                    menuItem.addActionListener(menuListener);
                    popupMenu.add(menuItem);
                }

                // Display the newly constructed popup menu.
                popupMenu.show(source, location.x, location.y);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while generating documentation for selected definitions.", e);
        }
    }

    private void validateDefinitions()
    {
        List<String> selectedDefinitions;

        // Get the selected definition identifiers.
        selectedDefinitions = getSelectedDefinitionHandles().stream().filter(definitionHandle -> definitionHandle != null).map(T8DefinitionHandle::getDefinitionIdentifier).collect(Collectors.toCollection(ArrayList::new));

        // Validate selected definitions.
        try
        {
            T8DefinitionValidationUtils.validateDefinitions(SwingUtilities.windowForComponent(this), this, context, selectedDefinitions);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while validating definitions: " + selectedDefinitions, e);
            JOptionPane.showMessageDialog(this, "An unexpected exception occurred.", "Unexpected Exception", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void finalizeSelectedDefinition()
    {
        List<T8DefinitionHandle> selectedDefinitionHandles;

        selectedDefinitionHandles = getSelectedDefinitionHandles();
        if (selectedDefinitionHandles.size() > 0)
        {
            String finalizationComment;

            finalizationComment = JOptionPane.showInputDialog(this, "Please enter a comment describing the changes that you've made to this definition.", "Add Finalization Comment", JOptionPane.PLAIN_MESSAGE);
            if (!Strings.isNullOrEmpty(finalizationComment))
            {
                finalizationComment = finalizationComment.trim();
                if (finalizationComment.length() > 25)
                {
                    try
                    {
                        T8DefinitionHandle definitionToFinalize;
                        boolean valid;

                        definitionToFinalize = selectedDefinitionHandles.get(0);
                        valid = T8DefinitionValidationUtils.validateDefinitions(SwingUtilities.windowForComponent(this), this, context, ArrayLists.newArrayList(definitionToFinalize.getDefinitionIdentifier()), true);
                        if (valid)
                        {
                            definitionContext.finalizeDefinition(definitionToFinalize, finalizationComment);
                            Toast.show("Definition Finalized", Style.SUCCESS);
                        }
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while finalizing definition: " + selectedDefinitionHandles.get(0), e);
                        JOptionPane.showMessageDialog(this, "An unexpected exception occurred.", "Unexpected Exception", JOptionPane.ERROR_MESSAGE);
                    }
                }
                else JOptionPane.showMessageDialog(this, "Please enter a more descriptive comment.", "Invalid Comment", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void checkDefinitionUsagesAsynchronously(final boolean checkAll)
    {
        List<T8DefinitionValidationError> validationErrors;

        try
        {
            validationErrors = (List<T8DefinitionValidationError>)ProcessingDialog.doTask(new ThreadTask() {
                @Override
                public List<T8DefinitionValidationError> doTask() throws Exception
                {
                    return checkDefinitionUsages(checkAll);
                }
            }, "Checking Usages", 250, 300, 100, javax.swing.SwingUtilities.getWindowAncestor(this));

            if(!validationErrors.isEmpty())
            {
                T8InformationDialog informationDialog = new T8InformationDialog(clientContext.getParentWindow(), this, T8DefinitionValidationUtils.createReport(validationErrors));
                informationDialog.setVisible(true);
            } else Toast.show("All definitions have usages", Style.SUCCESS);
        }
        catch (Throwable thr)
        {
            LOGGER.log("Failure to complete usage checks", thr);
        }
    }

    private List<T8DefinitionValidationError> checkDefinitionUsages(boolean checkAll)
    {
        List<T8DefinitionValidationError> validationErrors;
        List<T8DefinitionHandle> definitionHandles;

        definitionHandles = checkAll ? getAllDefinitionHandles() : getSelectedDefinitionHandles();
        validationErrors = new ArrayList<>();

        for (T8DefinitionHandle t8DefinitionHandle : definitionHandles)
        {
            if(t8DefinitionHandle != null)
            {
                Toast.show("Checking " + t8DefinitionHandle.getDefinitionIdentifier(), Style.NORMAL, 500);

                try
                {
                    List<T8DefinitionHandle> usages;

                    usages = definitionContext.findDefinitionsByIdentifier(t8DefinitionHandle.getDefinitionIdentifier(), true);
                    if (usages.isEmpty())
                    {
                        validationErrors.add(new T8DefinitionValidationError(t8DefinitionHandle.getProjectIdentifier(), t8DefinitionHandle.getDefinitionIdentifier(), "", "Definition has no usages", T8DefinitionValidationError.ErrorType.WARNING));
                    }
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to load raw definition " + t8DefinitionHandle.getDefinitionIdentifier(), ex);
                }
            }
        }

        return validationErrors;
    }

    private void createActionBindings()
    {
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("control S"), "Save_Changes");

        getActionMap().put("Save_Changes", new AbstractAction("Save_Changes")
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (jButtonSaveDefinitions.isEnabled() && jButtonSaveDefinitions.isVisible())
                {
                    saveChanges();
                }
            }
        });
    }

    private class SelectionListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            if (!e.getValueIsAdjusting())
            {
                T8DefinitionHandle selectedDefinitionHandle;

                selectedDefinitionHandle = definitionTable.getSelectedDefinitionHandle();
                handleDefinitionSelectionChange(selectedDefinitionHandle);
            }
        }
    }

    private class TableMouseListener implements MouseListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopupMenu(e.getX(), e.getY());
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopupMenu(e.getX(), e.getY());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopupMenu(e.getX(), e.getY());
            }
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    private class DefinitionContextListener implements T8DefinitionContextListener
    {
        @Override
        public void definitionSaved(T8DefinitionSavedEvent event)
        {
        }

        @Override
        public void definitionLocked(T8DefinitionLockedEvent event)
        {
        }

        @Override
        public void definitionUnlocked(T8DefinitionUnlockedEvent event)
        {
        }

        @Override
        public void definitionCacheReloaded(T8DefinitionCacheReloadedEvent event)
        {
        }

        @Override
        public void definitionRenamed(T8DefinitionRenamedEvent event)
        {
            List<T8DefinitionHandle> affectedDefinitions;
            T8DefinitionHandle affectedDefinitionHandle;
            T8Definition affectedRootDefinition;

            // Get the event parameters.
            affectedRootDefinition = event.getDefinition().getRootDefinition();
            affectedDefinitions = event.getAffectedDefinitions();
            affectedDefinitionHandle = affectedRootDefinition.getHandle();
            affectedDefinitionHandle.setDefinitionIdentifier(T8IdentifierUtilities.getGlobalIdentifierPart(event.getOldIdentifier()));
            System.out.println("Affected definition root: " + affectedRootDefinition);
            System.out.println("Affected definition handle: " + affectedDefinitionHandle);
            System.out.println("Affected definition count: " + affectedDefinitions.size());

            // If only one definition was affected, it means that the rename was local and we don't need to refresh the selected definition.
            if (affectedDefinitions.size() > 1)
            {
                // If more than one definitions were affected, and the affected definition was a new addition, it will now have been saved.
                newDefinitions.remove(affectedDefinitionHandle);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPopupMenuCheckUsages = new javax.swing.JPopupMenu();
        jMenuItemSelected = new javax.swing.JMenuItem();
        jMenuItemAll = new javax.swing.JMenuItem();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jButtonRemove = new javax.swing.JButton();
        jButtonRenameDefinition = new javax.swing.JButton();
        jButtonCopyDefinition = new javax.swing.JButton();
        jButtonRefresh = new javax.swing.JButton();
        jButtonSaveDefinitions = new javax.swing.JButton();
        jButtonApplyPatch = new javax.swing.JButton();
        jButtonDocumentation = new javax.swing.JButton();
        jButtonValidateDefinition = new javax.swing.JButton();
        jButtonCheckUsages = new javax.swing.JButton();
        jButtonhistory = new javax.swing.JButton();
        jButtonFinalizeDefinition = new javax.swing.JButton();
        jPanelFilter = new javax.swing.JPanel();
        jLabelSearch = new javax.swing.JLabel();
        jPanelSearchCriteria = new javax.swing.JPanel();
        jPanelTextSearch = new javax.swing.JPanel();
        jTextFieldSearchText = new javax.swing.JTextField();
        jComboFilter = new javax.swing.JComboBox();
        jTextFieldFilter = new org.jdesktop.swingx.JXTextField("Filter");
        jScrollPaneDefinitions = new javax.swing.JScrollPane();

        jMenuItemSelected.setText("Check Selected");
        jMenuItemSelected.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemSelectedActionPerformed(evt);
            }
        });
        jPopupMenuCheckUsages.add(jMenuItemSelected);

        jMenuItemAll.setText("Check All (Long Running)");
        jMenuItemAll.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAllActionPerformed(evt);
            }
        });
        jPopupMenuCheckUsages.add(jMenuItemAll);

        setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setToolTipText("Add New Item");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAdd);

        jButtonRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeDocumentIcon.png"))); // NOI18N
        jButtonRemove.setToolTipText("Delete Selected Items");
        jButtonRemove.setFocusable(false);
        jButtonRemove.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRemove.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRemove);

        jButtonRenameDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/renameDocumentIcon.png"))); // NOI18N
        jButtonRenameDefinition.setToolTipText("Rename Definition");
        jButtonRenameDefinition.setFocusable(false);
        jButtonRenameDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRenameDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRenameDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRenameDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRenameDefinition);

        jButtonCopyDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/documentCopyIcon.png"))); // NOI18N
        jButtonCopyDefinition.setToolTipText("Copy Definition");
        jButtonCopyDefinition.setFocusable(false);
        jButtonCopyDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCopyDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCopyDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCopyDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCopyDefinition);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        jButtonSaveDefinitions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/saveChangesIcon.png"))); // NOI18N
        jButtonSaveDefinitions.setToolTipText("Save Changes");
        jButtonSaveDefinitions.setFocusable(false);
        jButtonSaveDefinitions.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSaveDefinitions.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSaveDefinitions.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSaveDefinitionsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSaveDefinitions);

        jButtonApplyPatch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/bandaidArrowIcon.png"))); // NOI18N
        jButtonApplyPatch.setToolTipText("Apply Patch Changes");
        jButtonApplyPatch.setFocusable(false);
        jButtonApplyPatch.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonApplyPatch.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonApplyPatch.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonApplyPatchActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonApplyPatch);

        jButtonDocumentation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/informationIcon.png"))); // NOI18N
        jButtonDocumentation.setToolTipText("Definition Documentation");
        jButtonDocumentation.setFocusable(false);
        jButtonDocumentation.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonDocumentation.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDocumentation.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDocumentationActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDocumentation);

        jButtonValidateDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/bugExclamationIcon.png"))); // NOI18N
        jButtonValidateDefinition.setToolTipText("Validate Selected Definitions");
        jButtonValidateDefinition.setFocusable(false);
        jButtonValidateDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonValidateDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonValidateDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonValidateDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonValidateDefinition);

        jButtonCheckUsages.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addressArrowIcon.png"))); // NOI18N
        jButtonCheckUsages.setToolTipText("Check Usages");
        jButtonCheckUsages.setFocusable(false);
        jButtonCheckUsages.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCheckUsages.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCheckUsages.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCheckUsagesActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCheckUsages);

        jButtonhistory.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/clockHistoryIcon.png"))); // NOI18N
        jButtonhistory.setToolTipText("History");
        jButtonhistory.setFocusable(false);
        jButtonhistory.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonhistory.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonhistory.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonhistoryActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonhistory);

        jButtonFinalizeDefinition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/tickCircleIcon.png"))); // NOI18N
        jButtonFinalizeDefinition.setToolTipText("Finalize Definition.");
        jButtonFinalizeDefinition.setFocusable(false);
        jButtonFinalizeDefinition.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonFinalizeDefinition.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonFinalizeDefinition.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonFinalizeDefinitionActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonFinalizeDefinition);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarMain, gridBagConstraints);

        jPanelFilter.setLayout(new java.awt.GridBagLayout());

        jLabelSearch.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelSearch.setText("Search:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 1);
        jPanelFilter.add(jLabelSearch, gridBagConstraints);

        jPanelSearchCriteria.setLayout(new java.awt.CardLayout());

        jPanelTextSearch.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelTextSearch.add(jTextFieldSearchText, gridBagConstraints);

        jPanelSearchCriteria.add(jPanelTextSearch, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelFilter.add(jPanelSearchCriteria, gridBagConstraints);

        jComboFilter.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboFilter.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboFilterItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 1);
        jPanelFilter.add(jComboFilter, gridBagConstraints);

        jTextFieldFilter.setToolTipText("<html>\n<p>\n1)Type to search across all fields and press enter.<br>\n2)To search a specific field type field name and \":\" and then search query e.g Project:core.<br>\n3)For more advance search queries you can type EPIC: and write an epic statement that will evaluate rows and only display rows that evaluate true. For fields with spaces in the name replace \"_\" for the space<br>\n</p>\n</html>"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanelFilter.add(jTextFieldFilter, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jPanelFilter, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jScrollPaneDefinitions, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        displayAddDefinitionPopupMenu(jButtonAdd, 0, jButtonAdd.getHeight());
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonSaveDefinitionsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSaveDefinitionsActionPerformed
    {//GEN-HEADEREND:event_jButtonSaveDefinitionsActionPerformed
        saveChanges();
    }//GEN-LAST:event_jButtonSaveDefinitionsActionPerformed

    private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveActionPerformed
        deleteSelectedDefinitions();
    }//GEN-LAST:event_jButtonRemoveActionPerformed

    private void jButtonRenameDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRenameDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonRenameDefinitionActionPerformed
        renameSelectedDefinition();
    }//GEN-LAST:event_jButtonRenameDefinitionActionPerformed

    private void jButtonCopyDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCopyDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonCopyDefinitionActionPerformed
        copyDefinition();
    }//GEN-LAST:event_jButtonCopyDefinitionActionPerformed

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        if (JOptionPane.showConfirmDialog(this, "All unsaved changed will be lost.  Proceed?", "Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            refresh();
        }
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jComboFilterItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboFilterItemStateChanged
    {//GEN-HEADEREND:event_jComboFilterItemStateChanged
        filterDefinitions(jTextFieldFilter.getText());
    }//GEN-LAST:event_jComboFilterItemStateChanged

    private void jButtonApplyPatchActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonApplyPatchActionPerformed
    {//GEN-HEADEREND:event_jButtonApplyPatchActionPerformed
        applyPatchChanges();
    }//GEN-LAST:event_jButtonApplyPatchActionPerformed

    private void jButtonDocumentationActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDocumentationActionPerformed
    {//GEN-HEADEREND:event_jButtonDocumentationActionPerformed
        showDocumentationPopupMenu(jButtonDocumentation, new Point(0, jButtonDocumentation.getHeight()));
    }//GEN-LAST:event_jButtonDocumentationActionPerformed

    private void jButtonValidateDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonValidateDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonValidateDefinitionActionPerformed
        validateDefinitions();
    }//GEN-LAST:event_jButtonValidateDefinitionActionPerformed

    private void jButtonhistoryActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonhistoryActionPerformed
    {//GEN-HEADEREND:event_jButtonhistoryActionPerformed
        T8DefinitionHistoryPanel.showDefinitionHistoryPanel(parentView, definitionContext, selectedDefinition.getRootProjectId(), selectedDefinition.getIdentifier());
    }//GEN-LAST:event_jButtonhistoryActionPerformed

    private void jButtonCheckUsagesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCheckUsagesActionPerformed
    {//GEN-HEADEREND:event_jButtonCheckUsagesActionPerformed
        jPopupMenuCheckUsages.show(jButtonCheckUsages, 0, jButtonCheckUsages.getHeight());
    }//GEN-LAST:event_jButtonCheckUsagesActionPerformed

    private void jMenuItemSelectedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemSelectedActionPerformed
    {//GEN-HEADEREND:event_jMenuItemSelectedActionPerformed
        checkDefinitionUsagesAsynchronously(false);
    }//GEN-LAST:event_jMenuItemSelectedActionPerformed

    private void jMenuItemAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAllActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAllActionPerformed
        checkDefinitionUsagesAsynchronously(true);
    }//GEN-LAST:event_jMenuItemAllActionPerformed

    private void jButtonFinalizeDefinitionActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonFinalizeDefinitionActionPerformed
    {//GEN-HEADEREND:event_jButtonFinalizeDefinitionActionPerformed
        finalizeSelectedDefinition();
    }//GEN-LAST:event_jButtonFinalizeDefinitionActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonApplyPatch;
    private javax.swing.JButton jButtonCheckUsages;
    private javax.swing.JButton jButtonCopyDefinition;
    private javax.swing.JButton jButtonDocumentation;
    private javax.swing.JButton jButtonFinalizeDefinition;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JButton jButtonRenameDefinition;
    private javax.swing.JButton jButtonSaveDefinitions;
    private javax.swing.JButton jButtonValidateDefinition;
    private javax.swing.JButton jButtonhistory;
    private javax.swing.JComboBox jComboFilter;
    private javax.swing.JLabel jLabelSearch;
    private javax.swing.JMenuItem jMenuItemAll;
    private javax.swing.JMenuItem jMenuItemSelected;
    private javax.swing.JPanel jPanelFilter;
    private javax.swing.JPanel jPanelSearchCriteria;
    private javax.swing.JPanel jPanelTextSearch;
    private javax.swing.JPopupMenu jPopupMenuCheckUsages;
    private javax.swing.JScrollPane jScrollPaneDefinitions;
    protected javax.swing.JTextField jTextFieldFilter;
    private javax.swing.JTextField jTextFieldSearchText;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
