package com.pilog.t8.developer.definitions.metapackageview;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.metapackage.T8MetaPackageDescriptor;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.developer.definitions.dialog.T8IdentifierSelectionDialog;
import com.pilog.t8.developer.definitions.selectiontableview.T8DefinitionTable;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

/**
 * @author Bouwer du Preez
 */
public class T8PackageView extends javax.swing.JPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8PackageView.class);

    private final T8DefaultComponentContainer parentContainer;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final String identifier;
    private final Map<String, T8Definition> definitions;
    private final T8DefinitionTable definitionTable;

    public T8PackageView(T8DefaultComponentContainer parentContainer, T8Context context, String identifier)
    {
        this.parentContainer = parentContainer;
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definitionManager = clientContext.getDefinitionManager();
        this.identifier = identifier;
        initComponents();
        this.definitions = new HashMap<>();
        this.definitionTable = new T8DefinitionTable(context);
        this.definitionTable.setSelectedColumnVisible(false);
        this.definitionTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.definitionTable.clearSelection();
        this.jScrollPaneDefinitions.setViewportView(definitionTable);
    }

    public void setDescriptor(T8MetaPackageDescriptor descriptor)
    {
        jTextAreaDescription.setText(descriptor.getDescription());
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public String getDescription()
    {
        return jTextAreaDescription.getText();
    }

    public List<T8DefinitionMetaData> getDefinitionMetaData()
    {
        return definitionTable.getDefinitionMetaData();
    }

    public void addDefinition(T8Definition definition)
    {
        if (definitions.containsKey(definition.getIdentifier()))
        {
            // Just update the definition.
            definitions.put(definition.getIdentifier(), definition);
        }
        else // Add the definition to the collection and the tree.
        {
            definitions.put(definition.getIdentifier(), definition);
            definitionTable.addDefinitionMetaData(definition.getMetaData());
        }
    }

    public List<T8Definition> getDefinitions()
    {
        return new ArrayList<>(definitions.values());
    }

    public List<T8DefinitionHandle> getCheckedDefinitionHandles()
    {
        return definitionTable.getCheckedDefinitionHandles();
    }

    private void removeSelectedDefinitions()
    {
        List<T8DefinitionMetaData> selectedMetaDataList;

        selectedMetaDataList = definitionTable.getSelectedDefinitionMetaData();
        for (T8DefinitionMetaData checkedMetaData : selectedMetaDataList)
        {
            LOGGER.log("Remove definition from package: " + checkedMetaData.getId());
            definitionTable.removeDefinitionMetaData(checkedMetaData);
            definitions.remove(checkedMetaData.getId());
        }
    }

    private void importSelectedDefinitions()
    {
        List<T8DefinitionHandle> definitionHandles;
        DefinitionLoader loader;

        // Get the selected definition handles.
        definitionHandles = definitionTable.getSelectedDefinitionHandles();

        // Do the validation of all open records.
        parentContainer.setLocked(true);
        parentContainer.setProgress(0);
        parentContainer.setProgressMessage("Importing Definitions...");
        loader = new DefinitionLoader(definitionHandles);
        loader.execute();
    }

    private void importSelectedDefinitionsToProject()
    {
        List<T8DefinitionHandle> checkedHandles;
        double handlesToProcess;
        double handlesProcessed;
        String projectId;

        checkedHandles = definitionTable.getSelectedDefinitionHandles();
        handlesToProcess = checkedHandles.size();
        handlesProcessed = 0;

        try
        {
            projectId = T8IdentifierSelectionDialog.getSelectedIdentifier(clientContext, "Please select a project to which the definitions will be imported.", definitionManager.getGroupDefinitionIdentifiers(null, T8ProjectDefinition.GROUP_IDENTIFIER));
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to retrieve available project definitions", ex);
            projectId = null;
        }

        if (projectId != null)
        {
            T8ProjectDefinition projectDefinition;

            try
            {
                projectDefinition = (T8ProjectDefinition) definitionManager.getRawDefinition(context, null, projectId);
                if (projectDefinition.getDeveloperIdentifiers().contains(context.getUserId()))
                {
                    this.parentContainer.setLocked(true);
                    this.parentContainer.setProgress(0);
                    this.parentContainer.setProgressMessage("Importing...");
                    for (T8DefinitionHandle handle : checkedHandles)
                    {
                        try
                        {
                            T8Definition definition;

                            // Update the progress message
                            this.parentContainer.setProgressMessage("Importing " + handle.getDefinitionIdentifier());

                            definition = definitions.get(handle.getDefinitionIdentifier());
                            LOGGER.log("Import definition from package: " + handle.getDefinitionIdentifier());

                            definition.setProjectIdentifier(projectId);

                            definitionManager.importDefinition(context, definition);

                            // Update the progress
                            handlesProcessed++;
                            this.parentContainer.setProgress((int)((handlesProcessed / handlesToProcess) * 100.0));
                        }
                        catch (Exception e)
                        {
                            LOGGER.log("Exception while saving imported definition: " + handle.getDefinitionIdentifier(), e);
                        }
                    }

                    this.parentContainer.setLocked(false);

                    // Display the success message.
                    JOptionPane.showMessageDialog(this, checkedHandles.size() + " Defintions Imported Successfully.", "Operation Complete", JOptionPane.INFORMATION_MESSAGE);
                }
                else
                {
                    Toast.show("User does not have permission for project " + projectId, Toast.Style.ERROR);
                }
            }
            catch(Exception ex)
            {
                LOGGER.log("Failed to retrieve project definition " + projectId, ex);
            }
        }
        else
        {
            Toast.show("Invalid Project Identifier", Toast.Style.ERROR);
        }
    }

    private class DefinitionLoader extends SwingWorker<Void, Void>
    {
        private final List<T8DefinitionHandle> definitionHandles;

        private DefinitionLoader(List<T8DefinitionHandle> definitionHandles)
        {
            this.definitionHandles = definitionHandles;
        }

        @Override
        public Void doInBackground() throws Exception
        {
            double handlesToProcess;
            double handlesProcessed;

            handlesProcessed = 0;
            handlesToProcess = definitionHandles.size();
            for (T8DefinitionHandle handle : definitionHandles)
            {
                final double progress;

                try
                {
                    T8Definition definition;

                    // Update the progress message.
                    SwingUtilities.invokeLater(() -> parentContainer.setProgressMessage("Importing " + handle.getDefinitionIdentifier()));

                    definition = definitions.get(handle.getDefinitionIdentifier());
                    LOGGER.log("Import definition from package: " + handle.getDefinitionIdentifier());
                    definitionManager.importDefinition(context, definition);

                    // Update the progress percentage.
                    handlesProcessed++;
                    progress = ((handlesProcessed / handlesToProcess) * 100.0);
                    SwingUtilities.invokeLater(() -> parentContainer.setProgress((int)progress));
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while saving imported definition: " + handle.getDefinitionIdentifier(), e);
                }
            }

            return null;
        }

        @Override
        public void done()
        {
            parentContainer.setLocked(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarMain = new javax.swing.JToolBar();
        jButtonSelectedDefinitions = new javax.swing.JButton();
        jButtonSelectedDefinitionsToProject = new javax.swing.JButton();
        jButtonRemoveSelectedDefinitions = new javax.swing.JButton();
        jPanelContent = new javax.swing.JPanel();
        jLabelDescription = new javax.swing.JLabel();
        jTextAreaDescription = new javax.swing.JTextArea();
        jScrollPaneDefinitions = new javax.swing.JScrollPane();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonSelectedDefinitions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png"))); // NOI18N
        jButtonSelectedDefinitions.setText("Import Selected Definitions");
        jButtonSelectedDefinitions.setFocusable(false);
        jButtonSelectedDefinitions.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSelectedDefinitions.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSelectedDefinitionsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSelectedDefinitions);

        jButtonSelectedDefinitionsToProject.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png"))); // NOI18N
        jButtonSelectedDefinitionsToProject.setText("Import Selected Definitions To Project");
        jButtonSelectedDefinitionsToProject.setActionCommand("Import Selected Definitions To Project");
        jButtonSelectedDefinitionsToProject.setFocusable(false);
        jButtonSelectedDefinitionsToProject.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSelectedDefinitionsToProject.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSelectedDefinitionsToProjectActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSelectedDefinitionsToProject);

        jButtonRemoveSelectedDefinitions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeIcon.png"))); // NOI18N
        jButtonRemoveSelectedDefinitions.setText("Remove Selected Definitions");
        jButtonRemoveSelectedDefinitions.setFocusable(false);
        jButtonRemoveSelectedDefinitions.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemoveSelectedDefinitions.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveSelectedDefinitionsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRemoveSelectedDefinitions);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);

        jPanelContent.setLayout(new java.awt.GridBagLayout());

        jLabelDescription.setText("Description:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanelContent.add(jLabelDescription, gridBagConstraints);

        jTextAreaDescription.setColumns(20);
        jTextAreaDescription.setRows(1);
        jTextAreaDescription.setTabSize(1);
        jTextAreaDescription.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(7, 5, 2, 5);
        jPanelContent.add(jTextAreaDescription, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelContent.add(jScrollPaneDefinitions, gridBagConstraints);

        add(jPanelContent, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRemoveSelectedDefinitionsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveSelectedDefinitionsActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveSelectedDefinitionsActionPerformed
        removeSelectedDefinitions();
    }//GEN-LAST:event_jButtonRemoveSelectedDefinitionsActionPerformed

    private void jButtonSelectedDefinitionsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSelectedDefinitionsActionPerformed
    {//GEN-HEADEREND:event_jButtonSelectedDefinitionsActionPerformed
        importSelectedDefinitions();
    }//GEN-LAST:event_jButtonSelectedDefinitionsActionPerformed

    private void jButtonSelectedDefinitionsToProjectActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSelectedDefinitionsToProjectActionPerformed
    {//GEN-HEADEREND:event_jButtonSelectedDefinitionsToProjectActionPerformed
        importSelectedDefinitionsToProject();
    }//GEN-LAST:event_jButtonSelectedDefinitionsToProjectActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRemoveSelectedDefinitions;
    private javax.swing.JButton jButtonSelectedDefinitions;
    private javax.swing.JButton jButtonSelectedDefinitionsToProject;
    private javax.swing.JLabel jLabelDescription;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JScrollPane jScrollPaneDefinitions;
    private javax.swing.JTextArea jTextAreaDescription;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
