package com.pilog.t8.developer.definitions.graphview.flow;

import com.pilog.graph.model.GraphEdge.EdgeType;
import com.pilog.graph.model.GraphModel;
import com.pilog.graph.view.GraphVertexRenderer;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.ui.T8GraphManager;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.graph.T8GraphDefinition;
import com.pilog.t8.developer.definitions.graphview.flow.T8WorkFlowGraphConfiguration.LayoutType;
import com.pilog.t8.security.T8Context;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowGraphManager implements T8GraphManager
{
    public static final String EDGE_TYPE_PREFERENCE = "GRAPH_EDGE_TYPE_PREFERENCE";
    public static final String LAYOUT_TYPE_PREFERENCE = "GRAPH_LAYOUT_TYPE_PREFERENCE";

    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final T8WorkFlowVertexRenderer vertexRenderer;
    private final T8WorkFlowGraphConfiguration config;
    private final Preferences preferences;

    public T8WorkFlowGraphManager(T8Context context)
    {
        this.context = context;
        this.preferences = Preferences.userNodeForPackage(T8WorkFlowGraphManager.class);
        this.definitionManager = context.getClientContext().getDefinitionManager();
        this.config = new T8WorkFlowGraphConfiguration();
        this.config.setLayoutType(LayoutType.valueOf(preferences.get(LAYOUT_TYPE_PREFERENCE, this.config.getLayoutType().toString())));
        this.config.setEdgeType(EdgeType.valueOf(preferences.get(EDGE_TYPE_PREFERENCE, this.config.getEdgeType().toString())));
        this.vertexRenderer = new T8WorkFlowVertexRenderer(definitionManager, config);
    }

    @Override
    public GraphVertexRenderer getVertextRenderer()
    {
        return vertexRenderer;
    }

    @Override
    public GraphModel buildGraphModel(T8GraphDefinition graphDefinition)
    {
        if (config.getLayoutType() == LayoutType.STEPPED_LAYOUT)
        {
            T8WorkFlowSteppedLayoutManager layoutManager;

            layoutManager = new T8WorkFlowSteppedLayoutManager(vertexRenderer, config);
            return layoutManager.createGraphModel((T8WorkFlowDefinition)graphDefinition);
        }
        else
        {
            T8WorkFlowFreeLayoutManager layoutManager;

            layoutManager = new T8WorkFlowFreeLayoutManager(vertexRenderer, config);
            return layoutManager.createGraphModel((T8WorkFlowDefinition)graphDefinition);
        }
    }

    @Override
    public Dimension getPreferredSize()
    {
        return null;
    }

    public List<String> getSwimLaneIdentifierSequence()
    {
        return new ArrayList<String>();
    }

    public void setSwimLaneIdentifierSequence(List<String> swimLaneIdentifierSequence)
    {
    }

    @Override
    public Component getConfigurationComponent()
    {
        return new T8WorkFlowConfigurationPanel(this, config);
    }
}
