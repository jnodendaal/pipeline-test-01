package com.pilog.t8.developer.utils;

import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.org.T8ClientTerminologyProvider;
import com.pilog.t8.security.T8Context;
import java.awt.Color;
import java.awt.event.MouseEvent;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import org.fife.ui.rtextarea.RTextArea;
import org.fife.ui.rtextarea.ToolTipSupplier;

/**
 * @author Hennie Brink
 */
public class GUIDTermTooltipSupplier implements ToolTipSupplier
{
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8LRUCachedTerminologyProvider terminologyProvider;
    private int guidStartOffset;
    private Object previousHighlight;

    public GUIDTermTooltipSupplier(T8Context context)
    {
        T8TerminologyClientApi trmApi;

        this.clientContext = context.getClientContext();
        this.sessionContext = context.getSessionContext();
        trmApi = clientContext.getConfigurationManager().getAPI(context, T8TerminologyClientApi.API_IDENTIFIER);
        this.terminologyProvider = new T8LRUCachedTerminologyProvider(new T8ClientTerminologyProvider(trmApi), 100);
    }

    @Override
    public String getToolTipText(RTextArea textArea, MouseEvent e)
    {
        int offset;
        String term;

        offset = textArea.viewToModel(e.getPoint());

        try
        {
            if(previousHighlight != null)
            {
                textArea.getHighlighter().removeHighlight(previousHighlight);
            }

            term = findGuid(textArea, offset);

            if (term.matches("[A-Fa-f0-9]{32}"))
            {
                T8ConceptTerminology terminology;
                StringBuilder sb;

                sb = new StringBuilder();
                terminology = terminologyProvider.getTerminology(sessionContext.getContentLanguageIdentifier(), term);
                sb.append("<html>");

                if(terminology.getTerm() != null)
                {
                    sb.append("<div>");
                    sb.append("<h3>");
                    sb.append("Term");
                    sb.append("</h3>");
                    sb.append("<a>");
                    sb.append(terminology.getTerm());
                    sb.append("</a>");
                    sb.append("</div>");
                }

                if(terminology.getDefinition() != null)
                {
                    sb.append("<div>");
                    sb.append("<h3>");
                    sb.append("Definition");
                    sb.append("</h3>");
                    sb.append("<a>");
                    sb.append(terminology.getDefinition());
                    sb.append("</a>");
                    sb.append("</div>");
                }

                if(terminology.getAbbreviation() != null)
                {
                    sb.append("<div>");
                    sb.append("<h3>");
                    sb.append("Abbreviation");
                    sb.append("</h3>");
                    sb.append("<a>");
                    sb.append(terminology.getAbbreviation());
                    sb.append("</a>");
                    sb.append("</div>");
                }

                if(terminology.getCode() != null)
                {
                    sb.append("<div>");
                    sb.append("<h3>");
                    sb.append("Code");
                    sb.append("</h3>");
                    sb.append("<a>");
                    sb.append(terminology.getCode());
                    sb.append("</a>");
                    sb.append("</div>");
                }

                if(terminology.getConceptType() != null)
                {
                    sb.append("<div>");
                    sb.append("<h3>");
                    sb.append("Concept Type");
                    sb.append("</h3>");
                    sb.append("<a>");
                    sb.append(terminology.getConceptType().getDisplayName());
                    sb.append("</a>");
                    sb.append("</div>");
                }

                term = sb.toString();
                previousHighlight = textArea.getHighlighter().addHighlight(guidStartOffset + 1, guidStartOffset + 33, new DefaultHighlighter.DefaultHighlightPainter(new Color(0, 100, 200, 50)));
            }
            else
            {
                term = null;
            }
        }
        catch (BadLocationException ex)
        {
            T8Log.log("Could not find guid at location", ex);
            term = null;
        }

        return term;
    }

    private String findGuid(JTextArea textArea, int offset) throws BadLocationException
    {
        int currentLine;
        String currentChar;
        StringBuilder sb;

        sb = new StringBuilder();
        currentLine = textArea.getLineOfOffset(offset);

        for (int i = offset; i >= textArea.getLineStartOffset(currentLine); i--)
        {
            currentChar = textArea.getText(i, 1);

            if (currentChar.matches("[A-Fa-f0-9]+"))
            {
                sb.insert(0, currentChar);
            }
            else
            {
                guidStartOffset = i;
                break;
            }
        }

        for (int i = offset + 1; i <= textArea.getLineEndOffset(currentLine); i++)
        {
            currentChar = textArea.getText(i, 1);

            if (currentChar.matches("[A-Fa-f0-9]+"))
            {
                sb.append(currentChar);
            }
            else
            {
                break;
            }
        }

        return sb.toString();
    }
}
