package com.pilog.t8.developer.definitions.selectiontreeview;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.TreeCellRenderer;

/**
 * @author Bouwer du Preez
 */
class T8DefinitionMetaTreeCellRenderer implements TreeCellRenderer
{
    private JLabel label = new JLabel();
    private Color selectionBorderColor;
    private Color selectionForeground;
    private Color selectionBackground;
    private Color textForeground;
    private Color textBackground;

    public T8DefinitionMetaTreeCellRenderer()
    {
        Font fontValue;
        Boolean booleanValue;
        
        fontValue = UIManager.getFont("Tree.font");
        if (fontValue != null)
        {
            label.setFont(fontValue);
        }
        
        booleanValue = (Boolean)UIManager.get("Tree.drawsFocusBorderAroundIcon");
        selectionBorderColor = UIManager.getColor("Tree.selectionBorderColor");
        selectionForeground = UIManager.getColor("Tree.selectionForeground");
        selectionBackground = UIManager.getColor("Tree.selectionBackground");
        textForeground = UIManager.getColor("Tree.textForeground");
        textBackground = UIManager.getColor("Tree.textBackground");
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        T8DefinitionMetaTreeNode node;
        
        node = (T8DefinitionMetaTreeNode)value;
        label.setText(node.getIdentifier());
        return label;
    }
}
