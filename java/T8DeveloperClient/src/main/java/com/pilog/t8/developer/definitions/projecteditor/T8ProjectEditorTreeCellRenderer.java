package com.pilog.t8.developer.definitions.projecteditor;

import com.google.common.base.Strings;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.T8DefinitionGroupMetaData;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectEditorTreeCellRenderer extends javax.swing.JPanel implements TreeCellRenderer
{
    private final List<T8DefinitionValidationError> definitionValidationErrors;
    private final T8DefinitionManager definitionManager;
    private final T8ProjectEditor projectManager;

    private static final ImageIcon EDITED_ICON = new ImageIcon(T8ProjectEditorTreeCellRenderer.class.getResource("/com/pilog/t8/developer/icons/editedDefinitionIcon.png"));

    public T8ProjectEditorTreeCellRenderer(T8DefinitionManager definitionManager, T8ProjectEditor projectManager)
    {
        initComponents();
        this.definitionManager = definitionManager;
        this.projectManager = projectManager;
        this.definitionValidationErrors =  new ArrayList<>();
    }

    public void setDefinitionValidationErrors(List<T8DefinitionValidationError> definitionValidationErrors)
    {
        this.definitionValidationErrors.clear();
        if (definitionValidationErrors != null)
        {
            this.definitionValidationErrors.addAll(definitionValidationErrors);
        }
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        T8ProjectEditorTreeTableNode treeNode;

        jLabelValidationMessage.setVisible(false);
        jLabelValidationMessage.setText("");

        treeNode = (T8ProjectEditorTreeTableNode)value;
        if (treeNode.isTemporary())
        {
            jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : Color.WHITE);
            jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);

            jLabelNodeType.setIcon(null);
            jLabelNodeType.setText(treeNode.getTemporaryTitle());
            jLabelNodeName.setText(null);

            validate();
            return this;
        }
        else if (treeNode.isDefinitionNode())
        {
            T8DefinitionTypeMetaData typeMetaData;
            T8DefinitionMetaData definitionMeta;
            T8DefinitionHandle definitionHandle;

            // Get the details of the node.
            definitionMeta = treeNode.getDefinitionMetaData();
            typeMetaData = treeNode.getDefinitionTypeMetaData();
            definitionHandle = definitionMeta.getDefinitionHandle();

            // Set the text content of the node.
            jLabelNodeType.setText(typeMetaData.getDisplayName() + ":");
            jLabelNodeName.setText(definitionMeta.getId());

            // Set the color of the node based on its state.
            if (projectManager.isDefinitionUpdated(definitionHandle))
            {
                jLabelNodeType.setForeground(Color.BLACK);
                jLabelNodeName.setForeground(Color.BLACK);
                jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.EDITED_GREEN);
                jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);
            }
            else if (projectManager.isDefinitionDeleted(definitionHandle))
            {
                jLabelNodeType.setForeground(Color.WHITE);
                jLabelNodeName.setForeground(Color.WHITE);
                jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.DELETED_RED);
                jPanelMain.setBorder(LAFConstants.RED_LINE_BORDER);
            }
            else if (definitionMeta.isPublic())
            {
                jLabelNodeType.setForeground(Color.BLACK);
                jLabelNodeName.setForeground(Color.BLACK);
                jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.PROPERTY_BLUE);
                jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);
            }
            else
            {
                jLabelNodeType.setForeground(Color.BLACK);
                jLabelNodeName.setForeground(Color.BLACK);
                jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.PRIVATE_BLUE);
                jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);
            }

            if (projectManager.isDefinitionUpdated(definitionHandle))
            {
                jLabelNodeType.setIcon(EDITED_ICON);
            }
            else
            {
                jLabelNodeType.setIcon(null);
            }

            validate();
            return this;
        }
        else if (treeNode.isTypeNode())
        {
            T8DefinitionTypeMetaData typeMetaData;

            typeMetaData = treeNode.getDefinitionTypeMetaData();

            jLabelNodeType.setForeground(Color.BLACK);
            jLabelNodeName.setForeground(Color.BLACK);
            jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.LIGHT_BEIGE);
            jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);

            jLabelNodeType.setIcon(null);
            jLabelNodeType.setText(typeMetaData.getDisplayName());
            jLabelNodeName.setText(null);

            validate();
            return this;
        }
        else // treeNode.isGroupNode()
        {
            T8DefinitionGroupMetaData groupMetaData;
            String text;

            groupMetaData = treeNode.getDefinitionGroupMetaData();
            text = groupMetaData.getName();
            if (Strings.isNullOrEmpty(text)) text = groupMetaData.getGroupId();

            jLabelNodeType.setForeground(Color.BLACK);
            jLabelNodeName.setForeground(Color.BLACK);
            jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.BEIGE);
            jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);

            jLabelNodeType.setIcon(null);
            jLabelNodeType.setText(text);
            jLabelNodeName.setText(null);

            validate();
            return this;
        }
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    protected void firePropertyChange(String string, Object o, Object o1)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void firePropertyChange(String string, boolean bln, boolean bln1)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void firePropertyChange(String string, byte b, byte b1)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void firePropertyChange(String string, char c, char c1)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void firePropertyChange(String string, double d, double d1)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void firePropertyChange(String string, float f, float f1)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void firePropertyChange(String string, int i, int i1)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void firePropertyChange(String string, long l, long l1)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void firePropertyChange(String string, short s, short s1)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void repaint(Rectangle rctngl)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void repaint(long l, int i, int i1, int i2, int i3)
    {
    }

    /**
     * Overridden for performance reasons.
     */
    @Override
    public void repaint()
    {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jLabelNodeType = new javax.swing.JLabel();
        jLabelNodeName = new javax.swing.JLabel();
        jLabelValidationMessage = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridBagLayout());

        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jLabelNodeType.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelNodeType.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/editedDefinitionIcon.png"))); // NOI18N
        jLabelNodeType.setText("Node Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelMain.add(jLabelNodeType, gridBagConstraints);

        jLabelNodeName.setText("Node Name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelMain.add(jLabelNodeName, gridBagConstraints);

        jLabelValidationMessage.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelMain.add(jLabelValidationMessage, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jPanelMain, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelNodeName;
    private javax.swing.JLabel jLabelNodeType;
    private javax.swing.JLabel jLabelValidationMessage;
    private javax.swing.JPanel jPanelMain;
    // End of variables declaration//GEN-END:variables
}
