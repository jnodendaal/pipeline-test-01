package com.pilog.t8.developer.definitions.graphview.state;

import com.pilog.graph.model.GraphModel;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateGraphDefinition;
import com.pilog.t8.definition.graph.T8GraphNodeDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8StateGraphFreeLayoutManager
{
    private final T8StateVertexRenderer renderer;
    private final T8StateGraphConfiguration config;
    
    public T8StateGraphFreeLayoutManager(T8StateVertexRenderer renderer, T8StateGraphConfiguration config)
    {
        this.renderer = renderer;
        this.config = config;
    }
    
    public GraphModel createGraphModel(T8DataObjectStateGraphDefinition graphDefinition)
    {
        List<T8StateLayoutElement> layoutElements;
        T8StateModelBuilder modelBuilder;

        // Add all vertices.
        layoutElements = new ArrayList<T8StateLayoutElement>();
        for (T8Definition nodeDefinition : graphDefinition.getNodeDefinitions())
        {
            T8StateLayoutElement layoutElement;
                    
            layoutElement = new T8StateLayoutElement(0, (T8GraphNodeDefinition)nodeDefinition, renderer);
            layoutElement.setVertexMovable(true);
            layoutElements.add(layoutElement);
        }
        
        // Build the model and return it.
        modelBuilder = new T8StateModelBuilder(graphDefinition, renderer, config);
        return modelBuilder.createGraphModel(layoutElements);
    }
}
