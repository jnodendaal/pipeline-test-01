package com.pilog.t8.developer.utils;

import com.google.common.base.Strings;
import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.jdesktop.swingx.JXSearchField;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Hennie Brink
 */
public class T8ConceptLookupFrame extends javax.swing.JFrame
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final List<ConceptSelectionListener> selectionListeners;
    private T8DefaultComponentContainer componentContainer;
    private T8OntologyClientApi ontApi;

    public T8ConceptLookupFrame(T8Context context)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.selectionListeners = new ArrayList<>();

        initComponents();
        initializeControls();

        pack();
    }

    private void initializeControls()
    {
        componentContainer = new T8DefaultComponentContainer(jPanelContent);
        componentContainer.setMessage("Searching for Concepts");

        add(componentContainer, BorderLayout.CENTER);

        initializeSearchControls();
        initializeListControls();

        ontApi = clientContext.getConfigurationManager().getAPI(context, T8OntologyClientApi.API_IDENTIFIER);
    }

    private void initializeSearchControls()
    {
        jXSearchField.setSearchMode(JXSearchField.SearchMode.REGULAR);
        jXSearchField.setAction(new AbstractAction()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                doSearch();
            }
        });
        jXSearchField.setFindAction(jXSearchField.getAction());
    }

    private void initializeListControls()
    {
        jXList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jXList.setAutoCreateRowSorter(true);
        jXList.addListSelectionListener(new ListSelectionListener()
        {

            @Override
            public void valueChanged(ListSelectionEvent e)
            {
                int selectionIndex;

                selectionIndex = e.getFirstIndex();

                if (selectionIndex >= 0)
                {
                    showListItemGUID();
                }
            }
        });
    }

    public void addConceptSelectionListener(ConceptSelectionListener listener)
    {
        selectionListeners.add(listener);
    }

    public void removeConceptSelectionListener(ConceptSelectionListener listener)
    {
        selectionListeners.remove(listener);
    }

    private void doSearch()
    {
        if (!Strings.isNullOrEmpty(jXSearchField.getText()))
        {
            new ListUpdater().execute();
        }
    }

    private void showListItemGUID()
    {
        ListItem selectedItem;

        selectedItem = (ListItem) jXList.getSelectedValue();

        if (selectedItem != null)
        {
            T8OntologyConcept concept;

            jLabelValueCode.setText(selectedItem.getCode());
            jLabelValueTerm.setText(selectedItem.getTerm());
            jLabelValueDefinition.setText(selectedItem.getDefinition());
            try
            {
                concept = ontApi.retrieveConcept(selectedItem.getConceptID(), false, false, false, false, false);
                jLabelValueType.setText(concept.getConceptType().getDisplayName());
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to retrive concept information", ex);
                Toast.show("Failed to Retrieve Concept information", Toast.Style.ERROR);
            }

            jTextField.setText(selectedItem.getConceptID());
        }
    }

    private class ListUpdater extends SwingWorker<ListModel<ListItem>, Void>
    {

        @Override
        protected ListModel<ListItem> doInBackground() throws Exception
        {
            componentContainer.lock();

            DefaultListModel<ListItem> listModel;
            String terminologyEntityId;
            List<T8DataEntity> dataEntities;
            T8DataFilter filter;
            T8DataFilterCriteria searchCriteria;

            //Create the list model that will be populated
            listModel = new DefaultListModel();

            // Get the entity identifier.
            terminologyEntityId = ORG_TERMINOLOGY_DE_IDENTIFIER;

            // Create the filter to retrieve all DR Instance ID's belonging to the specified group and its descendants.
            filter = new T8DataFilter(terminologyEntityId);
            filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityId + EF_LANGUAGE_ID, DataFilterOperator.EQUAL, context.getLanguageId(), false);
            filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityId + EF_ORG_ID, DataFilterOperator.EQUAL, context.getRootOrganizationId(), false);
            //if (odtIDList != null) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ODT_ID, DataFilterOperator.IN, odtIDList, false);

            searchCriteria = new T8DataFilterCriteria();
            searchCriteria.addFilterClause(DataFilterConjunction.OR, terminologyEntityId + EF_TERM, DataFilterOperator.LIKE, jXSearchField.getText(), false);
            searchCriteria.addFilterClause(DataFilterConjunction.OR, terminologyEntityId + EF_CODE, DataFilterOperator.LIKE, jXSearchField.getText(), false);
            filter.addFilterCriteria(DataFilterConjunction.AND, searchCriteria);

            filter.addFieldOrdering(terminologyEntityId + EF_TERM, T8DataFilter.OrderMethod.ASCENDING);
            filter.addFieldOrdering(terminologyEntityId + EF_CODE, T8DataFilter.OrderMethod.ASCENDING);

            // Select the entities using the filter.
            dataEntities = T8DataManagerOperationHandler.selectDataEntities(context, terminologyEntityId, filter, 0, 100);

            for (T8DataEntity entity : dataEntities)
            {
                listModel.addElement(new ListItem(
                        (String) entity.getFieldValue(terminologyEntityId + EF_CONCEPT_ID),
                        (String) entity.getFieldValue(terminologyEntityId + EF_CODE),
                        (String) entity.getFieldValue(terminologyEntityId + EF_TERM),
                        (String) entity.getFieldValue(terminologyEntityId + EF_ABBREVIATION),
                        (String) entity.getFieldValue(terminologyEntityId + EF_DEFINITION)));
            }

            return listModel;
        }

        @Override
        protected void done()
        {
            try
            {
                ListModel<ListItem> listModel;

                listModel = get();

                jXList.setModel(listModel);
                jXList.setSelectedIndex(0);
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to retrive list items", ex);
                Toast.show("Failed to retrive list items", Toast.Style.ERROR);
            }
            finally
            {
                componentContainer.unlock();
            }
        }
    }

    private static class ListItem
    {
        private final String conceptID;
        private final String code;
        private final String term;
        private final String abbreviation;
        private final String definition;

        public ListItem(String conceptID, String code, String term,
                        String abbreviation, String definition)
        {
            this.conceptID = conceptID;
            this.code = code;
            this.term = term;
            this.abbreviation = abbreviation;
            this.definition = definition;
        }

        public String getConceptID()
        {
            return conceptID;
        }

        public String getCode()
        {
            return code;
        }

        public String getTerm()
        {
            return term;
        }

        public String getAbbreviation()
        {
            return abbreviation;
        }

        public String getDefinition()
        {
            return definition;
        }

        @Override
        public String toString()
        {
            return term + (code == null ? "" : (" (" + code + ")"));
        }
    }

    public static interface ConceptSelectionListener
    {
        public void conceptSelected(String conceptID);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jPanelSearch = new javax.swing.JPanel();
        jXSearchField = new org.jdesktop.swingx.JXSearchField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jXList = new org.jdesktop.swingx.JXList();
        jPanelConceptDetails = new javax.swing.JPanel();
        jLabelHeaderCode = new javax.swing.JLabel();
        jLabelValueCode = new javax.swing.JLabel();
        jLabelHeaderType = new javax.swing.JLabel();
        jLabelValueType = new javax.swing.JLabel();
        jLabelHeaderTerm = new javax.swing.JLabel();
        jLabelValueTerm = new javax.swing.JLabel();
        jLabelHeaderDefinition = new javax.swing.JLabel();
        jLabelValueDefinition = new javax.swing.JLabel();
        jPanelActions = new javax.swing.JPanel();
        jTextField = new javax.swing.JTextField();
        jPanelControls = new javax.swing.JPanel();
        jButtonInsert = new javax.swing.JButton();
        jButtonClose = new javax.swing.JButton();

        jPanelContent.setLayout(new java.awt.GridBagLayout());

        jPanelSearch.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Search Terms or Code", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 10))); // NOI18N
        jPanelSearch.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(jXSearchField, gridBagConstraints);

        jScrollPane1.setViewportView(jXList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jPanelSearch, gridBagConstraints);

        jPanelConceptDetails.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Selected Item", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 10))); // NOI18N
        jPanelConceptDetails.setLayout(new java.awt.GridBagLayout());

        jLabelHeaderCode.setText("Code:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanelConceptDetails.add(jLabelHeaderCode, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanelConceptDetails.add(jLabelValueCode, gridBagConstraints);

        jLabelHeaderType.setText("Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanelConceptDetails.add(jLabelHeaderType, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanelConceptDetails.add(jLabelValueType, gridBagConstraints);

        jLabelHeaderTerm.setText("Term:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanelConceptDetails.add(jLabelHeaderTerm, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanelConceptDetails.add(jLabelValueTerm, gridBagConstraints);

        jLabelHeaderDefinition.setText("Definition:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanelConceptDetails.add(jLabelHeaderDefinition, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanelConceptDetails.add(jLabelValueDefinition, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jPanelConceptDetails, gridBagConstraints);

        jPanelActions.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Actions", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 10))); // NOI18N
        jPanelActions.setLayout(new java.awt.GridBagLayout());

        jTextField.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelActions.add(jTextField, gridBagConstraints);

        jPanelControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING, 0, 0));

        jButtonInsert.setText("Insert");
        jButtonInsert.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonInsertActionPerformed(evt);
            }
        });
        jPanelControls.add(jButtonInsert);

        jButtonClose.setText("Close");
        jButtonClose.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCloseActionPerformed(evt);
            }
        });
        jPanelControls.add(jButtonClose);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelActions.add(jPanelControls, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jPanelActions, gridBagConstraints);

        setPreferredSize(new java.awt.Dimension(600, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseActionPerformed
        setVisible(false);
    }//GEN-LAST:event_jButtonCloseActionPerformed

    private void jButtonInsertActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonInsertActionPerformed
    {//GEN-HEADEREND:event_jButtonInsertActionPerformed
        ListItem selectedItem;

        selectedItem = (ListItem) jXList.getSelectedValue();

        if (selectedItem != null)
        {
            for (ConceptSelectionListener conceptSelectionListener : selectionListeners)
            {
                conceptSelectionListener.conceptSelected(selectedItem.getConceptID());
            }
        }
    }//GEN-LAST:event_jButtonInsertActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JButton jButtonInsert;
    private javax.swing.JLabel jLabelHeaderCode;
    private javax.swing.JLabel jLabelHeaderDefinition;
    private javax.swing.JLabel jLabelHeaderTerm;
    private javax.swing.JLabel jLabelHeaderType;
    private javax.swing.JLabel jLabelValueCode;
    private javax.swing.JLabel jLabelValueDefinition;
    private javax.swing.JLabel jLabelValueTerm;
    private javax.swing.JLabel jLabelValueType;
    private javax.swing.JPanel jPanelActions;
    private javax.swing.JPanel jPanelConceptDetails;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelControls;
    private javax.swing.JPanel jPanelSearch;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField;
    private org.jdesktop.swingx.JXList jXList;
    private org.jdesktop.swingx.JXSearchField jXSearchField;
    // End of variables declaration//GEN-END:variables
}
