package com.pilog.t8.developer.definitions.actionhandlers.ui.entityeditor.panel;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.entityeditor.T8DataEntityFieldEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.panel.T8PanelDataEntityEditorDefinition;
import com.pilog.t8.definition.ui.label.T8LabelDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition.Fill;
import com.pilog.t8.security.T8Context;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;


/**
 * @author Bouwer du Preez
 */
public class T8PanelDataEntityEditorActionHandler implements T8DefinitionActionHandler
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8PanelDataEntityEditorDefinition definition;

    public T8PanelDataEntityEditorActionHandler(T8Context context, T8PanelDataEntityEditorDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new AutoPopulateAction(context, definition));

        return actions;
    }

    private static class AutoPopulateAction extends AbstractAction
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8PanelDataEntityEditorDefinition definition;

        public AutoPopulateAction(T8Context context, T8PanelDataEntityEditorDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Auto-Populate Entity Field Editor Components");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/applicationFormIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                String entityId;

                entityId = definition.getEditorDataEntityIdentifier();
                if (entityId != null)
                {
                    T8PanelDataEntityEditorSetup panelSetup;
                    List<T8DataEntityFieldDefinition> fieldDefinitions;
                    List<T8PanelDataEntityEditorFieldSetup> fieldSetupList;
                    T8DataEntityDefinition entityDefinition;
                    T8DefinitionManager definitionManager;

                    definitionManager = clientContext.getDefinitionManager();
                    entityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(context, definition.getRootProjectId(), entityId, null);
                    fieldDefinitions = entityDefinition.getFieldDefinitions();

                    panelSetup = T8PanelDataEntityEditorFieldSetupDialog.getPanelSetup(context, fieldDefinitions);
                    fieldSetupList = panelSetup.getFieldSetup();
                    if (fieldSetupList != null)
                    {
                        List<T8PanelSlotDefinition> slotDefinitions;

                        slotDefinitions = new ArrayList<T8PanelSlotDefinition>();
                        for (T8PanelDataEntityEditorFieldSetup fieldSetup : fieldSetupList)
                        {
                            T8PanelSlotDefinition slotDefinition;
                            T8DataEntityFieldEditorDefinition editorDefinition;

                            if (panelSetup.isAddLabels())
                            {
                                T8LabelDefinition labelDefinition;

                                slotDefinition = new T8PanelSlotDefinition(T8Definition.getLocalIdPrefix() + T8PanelSlotDefinition.IDENTIFIER_PREFIX + "LABEL_" + fieldSetup.getEditorIdentifier().substring(1));
                                labelDefinition = new T8LabelDefinition(T8Definition.getLocalIdPrefix() + T8LabelDefinition.IDENTIFIER_PREFIX + fieldSetup.getEditorIdentifier().substring(1));
                                labelDefinition.setText(fieldSetup.getEditorLabel());
                                labelDefinition.setVerticalTextAlignment(panelSetup.getLabelVerticalTextAlignment());
                                labelDefinition.setHorizontalTextAlignment(panelSetup.getLabelHorizontalTextAlignment());
                                slotDefinition.setSlotComponentDefinition(labelDefinition);
                                slotDefinition.setFill(Fill.BOTH);
                                slotDefinition.setWeightX(0.0);
                                slotDefinition.setWeightY(0.0);
                                slotDefinition.setGridX(0);
                                slotDefinition.setGridY(fieldSetupList.indexOf(fieldSetup));
                                slotDefinition.setInsetLeft(panelSetup.getInsets().left);
                                slotDefinition.setInsetRight(panelSetup.getHorizontalSpacing() / 2);
                                slotDefinition.setInsetTop(panelSetup.getVerticalSpacing() / 2);
                                slotDefinition.setInsetBottom(panelSetup.getVerticalSpacing() / 2);
                                slotDefinitions.add(slotDefinition);

                                // Add the field editor
                                slotDefinition = new T8PanelSlotDefinition(T8Definition.getLocalIdPrefix() + T8PanelSlotDefinition.IDENTIFIER_PREFIX + fieldSetup.getEditorIdentifier().substring(1));
                                editorDefinition = (T8DataEntityFieldEditorDefinition)definitionManager.createNewDefinition(fieldSetup.getEditorIdentifier(), fieldSetup.getEditorTypeIdentifier());
                                editorDefinition.setEditorDataEntityIdentifier(entityId);
                                editorDefinition.setParentEditorIdentifier(definition.getIdentifier());
                                editorDefinition.setEditorDataEntityFieldIdentifier(entityDefinition.getIdentifier() + fieldSetup.getFieldIdentifier());
                                slotDefinition.setSlotComponentDefinition((T8ComponentDefinition)editorDefinition);
                                slotDefinition.setFill(Fill.BOTH);
                                slotDefinition.setWeightX(0.1);
                                slotDefinition.setWeightY(0.0);
                                slotDefinition.setGridX(1);
                                slotDefinition.setGridY(fieldSetupList.indexOf(fieldSetup));
                                slotDefinition.setInsetLeft(panelSetup.getHorizontalSpacing() / 2);
                                slotDefinition.setInsetRight(panelSetup.getInsets().right);
                                slotDefinition.setInsetTop(panelSetup.getVerticalSpacing() / 2);
                                slotDefinition.setInsetBottom(panelSetup.getVerticalSpacing() / 2);
                                slotDefinitions.add(slotDefinition);
                            }
                            else
                            {
                                // Add the field editor
                                slotDefinition = new T8PanelSlotDefinition(T8Definition.getLocalIdPrefix() + T8PanelSlotDefinition.IDENTIFIER_PREFIX + fieldSetup.getEditorIdentifier().substring(1));
                                editorDefinition = (T8DataEntityFieldEditorDefinition)definitionManager.createNewDefinition(fieldSetup.getEditorIdentifier(), fieldSetup.getEditorTypeIdentifier());
                                editorDefinition.setEditorDataEntityIdentifier(entityId);
                                editorDefinition.setParentEditorIdentifier(definition.getIdentifier());
                                editorDefinition.setEditorDataEntityFieldIdentifier(entityDefinition.getIdentifier() + fieldSetup.getFieldIdentifier());
                                slotDefinition.setSlotComponentDefinition((T8ComponentDefinition)editorDefinition);
                                slotDefinition.setFill(Fill.BOTH);
                                slotDefinition.setWeightX(0.1);
                                slotDefinition.setWeightY(0.0);
                                slotDefinition.setGridX(0);
                                slotDefinition.setGridY(fieldSetupList.indexOf(fieldSetup));
                                slotDefinition.setInsetLeft(panelSetup.getInsets().left);
                                slotDefinition.setInsetRight(panelSetup.getInsets().right);
                                slotDefinition.setInsetTop(panelSetup.getVerticalSpacing() / 2);
                                slotDefinition.setInsetBottom(panelSetup.getVerticalSpacing() / 2);
                                slotDefinitions.add(slotDefinition);
                            }
                        }

                        definition.setPanelSlotDefinitions(slotDefinitions);
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(clientContext.getParentWindow(), "No Data Entity Selected", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while retrieving field definitions for data source: " + definition.getIdentifier(), e);
            }
        }
    }
}
