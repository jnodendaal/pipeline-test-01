package com.pilog.t8.developer.definitions.metapackageview;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionSerializer;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.metapackage.T8DefinitionPackageFileHandler;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.components.tabbedpane.tab.ClosableTabComponent;
import com.pilog.t8.utilities.components.tabbedpane.tab.TabClosedEvent;
import com.pilog.t8.utilities.components.tabbedpane.tab.TabClosingEvent;
import com.pilog.t8.utilities.components.tabbedpane.tab.TabComponentListener;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

/**
 * @author Bouwer du Preez
 */
public class T8MetaPackageView extends javax.swing.JPanel
{
    public static final String ROOT_DEFINITION_PACKAGE_IDENTIFIER = "ROOT";

    private static final T8Logger LOGGER = T8Log.getLogger(T8MetaPackageView.class);
    private static final String FILE_PATH_PREFERENCE = "META_FILE_PATH_PREFERENCE";

    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final LinkedHashMap<String, T8PackageView> packageViews;
    private final TabComponentListener packageTabComponentListener;
    private final Preferences preferences;

    private T8DefaultComponentContainer componentContainer;

    public T8MetaPackageView(T8Context context)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definitionManager = clientContext.getDefinitionManager();
        this.packageTabComponentListener = new PackageTabComponentListener();
        this.preferences = Preferences.userNodeForPackage(T8MetaPackageView.class);
        initComponents();
        this.packageViews = new LinkedHashMap<>();
        jTextFieldFilePath.setText(preferences.get(FILE_PATH_PREFERENCE, ""));

        createComponentContainer();
    }

    private void createComponentContainer()
    {
        componentContainer = new T8DefaultComponentContainer(jPanelContent);
        componentContainer.setLocked(false);
        componentContainer.setIndeterminate(false);
        componentContainer.setProgressMessage("Loading Definitions");

        add(componentContainer, BorderLayout.CENTER);
    }

    private T8PackageView addNewPackage(String packageId)
    {
        if (packageId != null)
        {
            ClosableTabComponent tabComponent;
            T8PackageView packageView;

            packageView = new T8PackageView(this.componentContainer, context, packageId);
            tabComponent = new ClosableTabComponent(jTabbedPanePackages, packageId);
            tabComponent.addTabComponentListener(packageTabComponentListener);
            packageViews.put(packageId, packageView);
            jTabbedPanePackages.add(packageId, packageView);
            jTabbedPanePackages.setTabComponentAt(jTabbedPanePackages.getTabCount()-1, tabComponent);
            jTabbedPanePackages.setSelectedIndex(jTabbedPanePackages.getTabCount()-1);

            return packageView;
        }
        else return null;
    }

    public void addDefinitions(List<T8DefinitionHandle> definitionHandles)
    {
        DefinitionLoader loader;

        // Do the validation of all open records.
        componentContainer.setLocked(true);
        componentContainer.setProgress(0);
        componentContainer.setProgressMessage("Exporting Definitions...");
        loader = new DefinitionLoader(definitionHandles);
        loader.execute();
    }

    public void addDefinition(T8Definition definition)
    {
        T8DefinitionTypeMetaData typeMetaData;

        typeMetaData = definition.getTypeMetaData();
        if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.ROOT)
        {
            T8PackageView packageView;

            packageView = packageViews.get(ROOT_DEFINITION_PACKAGE_IDENTIFIER);
            if (packageView == null) packageView = addNewPackage(ROOT_DEFINITION_PACKAGE_IDENTIFIER);
            packageView.addDefinition(definition);
        }
        else if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.SYSTEM)
        {
            T8PackageView packageView;

            packageView = packageViews.get("SYSTEM");
            if (packageView == null) packageView = addNewPackage("SYSTEM");
            packageView.addDefinition(definition);
        }
        else if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.RESOURCE)
        {
            // Do nothing, as resource definitions cannot be exported.
        }
        else
        {
            T8PackageView packageView;
            String projectId;

            projectId = definition.getProjectIdentifier();
            packageView = packageViews.get(projectId);
            if (packageView == null) packageView = addNewPackage(projectId);
            packageView.addDefinition(definition);
        }
    }

    private byte[] getPassword(String inputString)
    {
        if (inputString != null)
        {
            byte[] passwordBytes;
            byte[] finalPassword;

            passwordBytes = inputString.getBytes();
            finalPassword = new byte[16];
            Arrays.fill(finalPassword, ((byte)'*'));
            for (int i = 0; (i < 16) && (i < passwordBytes.length); i++)
            {
                finalPassword[i] = passwordBytes[i];
            }

            return finalPassword;
        }
        else return null;
    }

    private void setFilePath()
    {
        try
        {
            String filePath;

            filePath = BasicFileChooser.getLoadFilePath(this, ".t8m", "Tech 8 Meta Package", new File(getFilePath()));
            if (filePath != null)
            {
                jTextFieldFilePath.setText(filePath);
                preferences.put(FILE_PATH_PREFERENCE, filePath);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while setting file path.", e);
        }
    }

    private String getFilePath()
    {
        return jTextFieldFilePath.getText();
    }

    private void exportMetaPackage()
    {
        byte[] passwordBytes;

        passwordBytes = getPassword(jTextFieldPassword.getText());
        if ((passwordBytes != null) && (passwordBytes.length == 16))
        {
            try
            {
                String filePath;

                filePath = getFilePath();
                if (Strings.isNullOrEmpty(filePath))
                {
                    setFilePath();
                    filePath = getFilePath();
                }

                // Now proceed if a valid file path is specified.
                if (!Strings.isNullOrEmpty(filePath))
                {
                    T8DefinitionPackageFileHandler fileHandler;

                    fileHandler = new T8DefinitionPackageFileHandler(new T8DefinitionSerializer(clientContext.getDefinitionManager()));

                    try
                    {
                        HashMap<String, List<T8Definition>> definitions;

                        fileHandler.setSecretKey(passwordBytes);
                        fileHandler.openFile(filePath, true);

                        definitions = new HashMap<String, List<T8Definition>>();
                        for (T8PackageView packageView : packageViews.values())
                        {
                            definitions.put(packageView.getIdentifier(), packageView.getDefinitions());
                        }

                        fileHandler.writeDefinitions(definitionManager, definitions);

                        // Display the success message.
                        JOptionPane.showMessageDialog(this, "Meta Packages Export Successfully.", "Operation Complete", JOptionPane.INFORMATION_MESSAGE);
                    }
                    catch (Exception e2)
                    {
                        JOptionPane.showMessageDialog(this, "Meta Packages Export Failed.", "Operation Failure", JOptionPane.ERROR_MESSAGE);
                        LOGGER.log("Exception while exporting meta package.", e2);
                        throw e2;
                    }
                    finally
                    {
                        // Make sure to always close the file.
                        fileHandler.closeFile();
                    }
                }
                else JOptionPane.showMessageDialog(this, "Please enter a file path.", "Invalid File Path", JOptionPane.ERROR_MESSAGE);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while exporting meta package.", e);
            }
        }
        else JOptionPane.showMessageDialog(this, "Please enter a password of 16 bytes to use for meta encryption.", "Invalid Password", JOptionPane.ERROR_MESSAGE);
    }

    private void importMetaPackage()
    {
        try
        {
            String filePath;
            byte[] passwordBytes;

            filePath = getFilePath();
            if(Strings.isNullOrEmpty(filePath))
            {
                setFilePath();
                filePath = getFilePath();
            }

            // Now proceed if a valid file path is specified.
            if (!Strings.isNullOrEmpty(filePath))
            {
                String password;

                password = JOptionPane.showInputDialog(this, "Please enter the password for decrypting the meta package file.", "Password Entry", JOptionPane.QUESTION_MESSAGE);
                if (password != null)
                {
                    jTextFieldPassword.setText(password);
                    passwordBytes = getPassword(password);
                    if ((passwordBytes != null) && (passwordBytes.length == 16))
                    {
                        T8DefinitionPackageFileHandler fileHandler;

                        // Create the file handler.
                        fileHandler = new T8DefinitionPackageFileHandler(new T8DefinitionSerializer(clientContext.getDefinitionManager()));

                        try
                        {
                            DefinitionImporter importer;

                            // Open the file handler.
                            fileHandler.setSecretKey(passwordBytes);
                            fileHandler.openFile(filePath, false);

                            // Do the validation of all open records.
                            componentContainer.setLocked(true);
                            componentContainer.setProgress(0);
                            componentContainer.setProgressMessage("Importing Definitions...");
                            importer = new DefinitionImporter(fileHandler);
                            importer.execute();
                        }
                        catch (Exception e)
                        {
                            JOptionPane.showMessageDialog(this, "Meta Packages could not opened sucessfully.", "Operation Failure", JOptionPane.ERROR_MESSAGE);
                            LOGGER.log("Exception while importing meta package.", e);
                            fileHandler.closeFile();
                            throw e;
                        }
                    }
                    else JOptionPane.showMessageDialog(this, "Please enter a password of 16 bytes to use for meta decryption.", "Invalid Password", JOptionPane.ERROR_MESSAGE);
                }
            }
            else JOptionPane.showMessageDialog(this, "Please enter a file path.", "Invalid File Path", JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while importing meta package.", e);
        }
    }

    private class PackageTabComponentListener implements TabComponentListener
    {
        @Override
        public void tabClosed(TabClosedEvent event)
        {
            T8PackageView closedPackageView;

            closedPackageView = (T8PackageView)event.getTabClosed();
            packageViews.remove(closedPackageView.getIdentifier());
        }

        @Override
        public void tabClosing(TabClosingEvent event)
        {
        }
    }

    private class DefinitionImporter extends SwingWorker<Void, Void>
    {
        private final T8DefinitionPackageFileHandler fileHandler;

        private DefinitionImporter(T8DefinitionPackageFileHandler fileHandler)
        {
            this.fileHandler = fileHandler;
        }

        @Override
        public Void doInBackground() throws Exception
        {
            try
            {
                double totalPackages;
                List<String> packageIds;

                packageIds = fileHandler.getPackageIdentifiers();
                totalPackages = packageIds.size();
                for (String packageId : packageIds)
                {
                    T8PackageView packageView;
                    double packageIndex;
                    double totalDefinitions;
                    double totalProgress;
                    List<String> definitionIds;

                    packageView = addNewPackage(packageId);
                    definitionIds = fileHandler.getPackageDefinitionIdentifiers(packageId);

                    totalDefinitions = definitionIds.size();
                    packageIndex = packageIds.indexOf(packageId) + 1;

                    totalProgress = ((packageIndex / totalPackages) * 100.0);
                    SwingUtilities.invokeLater(() ->
                    {
                        componentContainer.setProgress((int)totalProgress);
                        componentContainer.setProgressMessage(packageId);
                    });

                    for (String definitionId : definitionIds)
                    {
                        T8Definition definition;
                        double definitionIndex;
                        double packageProgress;

                        definitionIndex = definitionIds.indexOf(definitionId) + 1;
                        definition = fileHandler.readDefinition(definitionManager, packageId, definitionId);
                        SwingUtilities.invokeLater(() -> packageView.addDefinition(definition));

                        packageProgress = ((definitionIndex / totalDefinitions) * 100.0);
                        SwingUtilities.invokeLater(() ->
                        {
                            componentContainer.setCategoryMessage(definitionId);
                            componentContainer.setCategoryProgress((int)packageProgress);
                        });
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while importing meta package.", e);
                return null;
            }
            finally
            {
                // Make sure to always close the file.
                fileHandler.closeFile();
            }
        }

        @Override
        public void done()
        {
            componentContainer.setLocked(false);
        }
    }

    private class DefinitionLoader extends SwingWorker<Void, Void>
    {
        private final List<T8DefinitionHandle> definitionHandles;


        private DefinitionLoader(List<T8DefinitionHandle> definitionHandles)
        {
            this.definitionHandles = definitionHandles;
        }

        @Override
        public Void doInBackground() throws Exception
        {
            final T8DefaultComponentContainer container;
            double handlesToProcess;
            double handlesProcessed;

            container = componentContainer;
            handlesProcessed = 0;
            handlesToProcess = definitionHandles.size();
            for (T8DefinitionHandle definitionHandle : definitionHandles)
            {
                try
                {
                    T8Definition definition;
                    final String defintionId;
                    final double progress;

                    // Update the progress message.
                    defintionId = definitionHandle.getDefinitionIdentifier();
                    SwingUtilities.invokeLater(() -> componentContainer.setProgressMessage("Adding " + defintionId));

                    definition = definitionManager.getRawDefinition(context, definitionHandle);
                    if (definition != null) SwingUtilities.invokeLater(() -> addDefinition(definition));

                    // Update the progress.
                    handlesProcessed++;
                    progress = ((handlesProcessed / handlesToProcess) * 100.0);
                    SwingUtilities.invokeLater(() -> container.setProgress((int)progress));
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while loading definition: " + definitionHandle, e);
                }
            }

            return null;
        }

        @Override
        public void done()
        {
            componentContainer.setLocked(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarMain = new javax.swing.JToolBar();
        jButtonExport = new javax.swing.JButton();
        jButtonImport = new javax.swing.JButton();
        jPanelContent = new javax.swing.JPanel();
        jLabelFilePath = new javax.swing.JLabel();
        jTextFieldFilePath = new javax.swing.JTextField();
        jButtonFileChooser = new javax.swing.JButton();
        jLabelFileIdentifier = new javax.swing.JLabel();
        jTextFieldFileIdentifier = new javax.swing.JTextField();
        jLabelPassword = new javax.swing.JLabel();
        jTextFieldPassword = new javax.swing.JTextField();
        jTabbedPanePackages = new javax.swing.JTabbedPane();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonExport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/diskPencilIcon.png"))); // NOI18N
        jButtonExport.setText("Save File");
        jButtonExport.setFocusable(false);
        jButtonExport.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonExport.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonExport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonExportActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonExport);

        jButtonImport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/diskArrowIcon.png"))); // NOI18N
        jButtonImport.setText("Load File");
        jButtonImport.setFocusable(false);
        jButtonImport.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonImport.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonImport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonImportActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonImport);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);

        jPanelContent.setLayout(new java.awt.GridBagLayout());

        jLabelFilePath.setText("File Path:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelContent.add(jLabelFilePath, gridBagConstraints);

        jTextFieldFilePath.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 5);
        jPanelContent.add(jTextFieldFilePath, gridBagConstraints);

        jButtonFileChooser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/searchIcon.png"))); // NOI18N
        jButtonFileChooser.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonFileChooser.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonFileChooserActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelContent.add(jButtonFileChooser, gridBagConstraints);

        jLabelFileIdentifier.setText("Identifier:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelContent.add(jLabelFileIdentifier, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 5);
        jPanelContent.add(jTextFieldFileIdentifier, gridBagConstraints);

        jLabelPassword.setText("Password:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelContent.add(jLabelPassword, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 5);
        jPanelContent.add(jTextFieldPassword, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelContent.add(jTabbedPanePackages, gridBagConstraints);

        add(jPanelContent, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonExportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExportActionPerformed
    {//GEN-HEADEREND:event_jButtonExportActionPerformed
        exportMetaPackage();
    }//GEN-LAST:event_jButtonExportActionPerformed

    private void jButtonImportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonImportActionPerformed
    {//GEN-HEADEREND:event_jButtonImportActionPerformed
        importMetaPackage();
    }//GEN-LAST:event_jButtonImportActionPerformed

    private void jButtonFileChooserActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonFileChooserActionPerformed
    {//GEN-HEADEREND:event_jButtonFileChooserActionPerformed
        setFilePath();
    }//GEN-LAST:event_jButtonFileChooserActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonExport;
    private javax.swing.JButton jButtonFileChooser;
    private javax.swing.JButton jButtonImport;
    private javax.swing.JLabel jLabelFileIdentifier;
    private javax.swing.JLabel jLabelFilePath;
    private javax.swing.JLabel jLabelPassword;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JTabbedPane jTabbedPanePackages;
    private javax.swing.JTextField jTextFieldFileIdentifier;
    private javax.swing.JTextField jTextFieldFilePath;
    private javax.swing.JTextField jTextFieldPassword;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
