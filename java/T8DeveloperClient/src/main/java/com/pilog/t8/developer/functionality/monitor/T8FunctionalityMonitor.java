package com.pilog.t8.developer.functionality.monitor;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.table.T8TableAPIHandler;
import com.pilog.t8.developer.functionality.monitor.core.T8StateFuncBlockPanel;
import com.pilog.t8.developer.functionality.monitor.core.T8StateFuncPanel;
import com.pilog.t8.developer.view.T8DefaultDeveloperView;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityMonitor extends T8DefaultDeveloperView implements T8DeveloperView
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8ComponentController controller;
    private final T8DeveloperView parentView;
    private T8DeveloperViewFactory viewFactory;
    private final T8DefinitionManager definitionManager;
    private final T8FunctionalityManager functionalityManager;
    private final T8StateFuncPanel stateFuncPanel;
    private final T8StateFuncBlockPanel stateFuncBlockPanel;
    private String lastSelectedFunctionalityIID;

    public T8FunctionalityMonitor(T8Context context, T8DeveloperView parentView) throws Exception
    {
        initComponents();

        this.clientContext = context.getClientContext();
        this.context = context;
        this.parentView = parentView;
        this.controller = new T8DefaultComponentController(context, "FUNCTIONALITY_MONITOR", false);
        this.controller.addComponentEventListener(new ComponentEventListener());
        this.definitionManager = clientContext.getDefinitionManager();
        this.viewFactory = parentView.getViewFactory();
        this.functionalityManager = clientContext.getFunctionalityManager();
        this.stateFuncPanel = new T8StateFuncPanel(controller);
        this.stateFuncBlockPanel = new T8StateFuncBlockPanel(controller);
        this.jSplitPaneFunc.setLeftComponent(stateFuncPanel);
        this.jSplitPaneFunc.setRightComponent(stateFuncBlockPanel);
    }

    @Override
    public String getHeader()
    {
        return "Fucntionality Monitor";
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        parentView.addView(view);
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        parentView.removeView(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    private void refreshData()
    {
        getStateFuncPanel().getTable().refreshData();
    }

    private void releaseFunctionality()
    {
        T8Log.log("Starting to release the following functionality: " + lastSelectedFunctionalityIID);
        try
        {
            functionalityManager.releaseFunctionality(context, lastSelectedFunctionalityIID);
            JOptionPane.showMessageDialog(this, getTranslatedString("The Functionality has successfully been released."), getTranslatedString("Success"), JOptionPane.INFORMATION_MESSAGE);
            refreshData();
        }
        catch (Exception ex)
        {
            Logger.getLogger(T8FunctionalityMonitor.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, getTranslatedString("An unexpected error has stop the Fucntionality from releasing correctly. Please contact your system administrator."), getTranslatedString("Functionality Release Exception"), JOptionPane.ERROR_MESSAGE);
        }

    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    @Override
    public void setSelectedDefinition(T8Definition selectedDefinition)
    {
    }

    @Override
    public void addDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    @Override
    public void removeDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    private class ComponentEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;
            Map<String, Object> eventParameters;
            List<T8DataEntity> dataEntities;
            T8DataFilter dataFilter = null;
            T8Component component;

            eventDefinition = event.getEventDefinition();
            component = event.getComponent();
            eventParameters = event.getEventParameters();

            // Get the selected entities from the event parameters.
            dataEntities = eventParameters != null ? (List<T8DataEntity>) eventParameters.get(T8FunctionalityMonitorResources.P_DATA_ENTITY_LIST) : null;
            if (dataEntities != null)
            {
                for (T8DataEntity dataEntity : dataEntities)
                {
                    lastSelectedFunctionalityIID = (String) dataEntity.getFieldValue(dataEntity.getIdentifier() + T8FunctionalityMonitorResources.EF_FUNCTIONALITY_IID);
                    dataFilter = new T8DataFilter(dataEntity.getIdentifier());
                    dataFilter.addFilterCriteria(HashMaps.newHashMap(getStateFuncBlockPanel().getDataEntityIdentifier() + T8FunctionalityMonitorResources.EF_BLOCKING_FUNCTIONALITY_IID, dataEntity.getFieldValue(dataEntity.getIdentifier() + T8FunctionalityMonitorResources.EF_FUNCTIONALITY_IID)));
                }
            }

            //State Functionality Panel Selection Change event
            if (eventDefinition.getIdentifier().equals(T8TableAPIHandler.EVENT_ROW_SELECTION_CHANGED) && component.getComponentDefinition().getIdentifier().equals(T8StateFuncPanel.TABLE_DEFINITION_ID))
            {
                T8Log.log("State Func Panel Selection Change event has fired...");
                if (dataFilter != null)
                {
                    getStateFuncBlockPanel().getTable().setUserDefinedFilter(dataFilter);
                    getStateFuncBlockPanel().getTable().refreshData();
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jButtonReleaseFunctionality = new javax.swing.JButton();
        jSplitPaneFunc = new javax.swing.JSplitPane();

        setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        jButtonReleaseFunctionality.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/skullIcon.png"))); // NOI18N
        jButtonReleaseFunctionality.setFocusable(false);
        jButtonReleaseFunctionality.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonReleaseFunctionality.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonReleaseFunctionality.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonReleaseFunctionality.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonReleaseFunctionalityActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonReleaseFunctionality);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarMain, gridBagConstraints);

        jSplitPaneFunc.setDividerLocation(250);
        jSplitPaneFunc.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPaneFunc.setResizeWeight(0.3);
        jSplitPaneFunc.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jSplitPaneFunc, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        refreshData();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jButtonReleaseFunctionalityActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonReleaseFunctionalityActionPerformed
    {//GEN-HEADEREND:event_jButtonReleaseFunctionalityActionPerformed
        releaseFunctionality();
    }//GEN-LAST:event_jButtonReleaseFunctionalityActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JButton jButtonReleaseFunctionality;
    private javax.swing.JSplitPane jSplitPaneFunc;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables

    private String getTranslatedString(String text)
    {
        return clientContext.getConfigurationManager().getUITranslation(this.context, text);
    }

    /**
     * @return the stateFuncPanel
     */
    public T8StateFuncPanel getStateFuncPanel()
    {
        return stateFuncPanel;
    }

    /**
     * @return the stateFuncBlockPanel
     */
    public T8StateFuncBlockPanel getStateFuncBlockPanel()
    {
        return stateFuncBlockPanel;
    }
}
