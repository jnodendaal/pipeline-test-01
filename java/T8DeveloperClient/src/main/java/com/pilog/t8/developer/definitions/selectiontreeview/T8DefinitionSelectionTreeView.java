package com.pilog.t8.developer.definitions.selectiontreeview;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.security.T8Context;
import java.util.List;
import javax.swing.JButton;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionSelectionTreeView extends javax.swing.JPanel
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private T8DefinitionManager definitionManager;
    private T8DefinitionSelectionTree selectionTree;

    public T8DefinitionSelectionTreeView(T8Context context)
    {
        initComponents();
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definitionManager = clientContext.getDefinitionManager();
        this.selectionTree = new T8DefinitionSelectionTree();
        this.jScrollPaneDefinitions.setViewportView(selectionTree);
    }

    public void refreshDefinitionView()
    {
        try
        {
            selectionTree.setDefinitionMetaData(definitionManager.getAllDefinitionMetaData(context, null));
        }
        catch (Exception e)
        {
            T8Log.log("Exception while fetching definition meta data from server.", e);
        }
    }

    public List<T8DefinitionHandle> getCheckedDefinitionHandles()
    {
        return selectionTree.getCheckedDefinitionHandles();
    }

    public void addToolBarButton(JButton button)
    {
        jToolBarMain.add(button);
    }

    public void removeToolBarButton(JButton button)
    {
        jToolBarMain.remove(button);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jScrollPaneDefinitions = new javax.swing.JScrollPane();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setText("Refresh");
        jButtonRefresh.setToolTipText("Reload all definition available for export.");
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);
        add(jScrollPaneDefinitions, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        refreshDefinitionView();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JScrollPane jScrollPaneDefinitions;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
