package com.pilog.t8.developer.definitions.actionhandlers.ui.table;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

/**
 * @author Bouwer du Preez
 */
public class T8TableDefinitionActionHandler implements T8DefinitionActionHandler
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8TableDefinition definition;

    public T8TableDefinitionActionHandler(T8Context context, T8TableDefinition definition)
    {
        this.clientContext = context.getClientContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new ColumnAutoCreationAction(context, definition));

        return actions;
    }

    private static class ColumnAutoCreationAction extends AbstractAction
    {
        private T8ClientContext clientContext;
        private T8Context context;
        private T8TableDefinition definition;

        public ColumnAutoCreationAction(T8Context context, final T8TableDefinition definition)
        {
            this.clientContext = context.getClientContext();
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Create Columns From Data Source");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/databaseArrowIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            String dataEntityId;

            dataEntityId = definition.getDataEntityIdentifier();
            if ((dataEntityId != null) && (dataEntityId.length() > 0))
            {
                try
                {
                    T8DataEntityDefinition entityDefinition;

                    entityDefinition = (T8DataEntityDefinition)clientContext.getDefinitionManager().getInitializedDefinition(context, definition.getRootProjectId(), dataEntityId, null);
                    if (entityDefinition != null)
                    {
                        List<T8DataEntityFieldDefinition> fieldDefinitions;
                        ArrayList<T8TableColumnDefinition> columnDefinitions;

                        fieldDefinitions = entityDefinition.getFieldDefinitions();
                        columnDefinitions = new ArrayList<T8TableColumnDefinition>(definition.getColumnDefinitions());

                        for (T8DataFieldDefinition fieldDefinition : fieldDefinitions)
                        {
                            T8TableColumnDefinition newColumnDefinition;
                            String publicFieldIdentifier;
                            boolean found;

                            found = false;
                            publicFieldIdentifier = fieldDefinition.getPublicIdentifier();
                            for (T8TableColumnDefinition columnDefinition : definition.getColumnDefinitions())
                            {
                                if (columnDefinition.getFieldIdentifier().equals(publicFieldIdentifier))
                                {
                                    found = true;
                                    break;
                                }
                            }

                            // If field was not found add a new column for it.
                            if (!found)
                            {
                                String fieldIdentifier;

                                fieldIdentifier = fieldDefinition.getIdentifier();

                                newColumnDefinition = new T8TableColumnDefinition(fieldIdentifier + "_" + T8IdentifierUtilities.createNewGUID());
                                newColumnDefinition.setColumnName(T8IdentifierUtilities.convertIdentifierToTitleCase(fieldIdentifier));
                                newColumnDefinition.setFieldIdentifier(publicFieldIdentifier);
                                newColumnDefinition.setVisible(true);
                                newColumnDefinition.setEditable(false);
                                newColumnDefinition.setWidth(100);
                                newColumnDefinition.setRequired(fieldDefinition.isKey() || fieldDefinition.isRequired());
                                columnDefinitions.add(newColumnDefinition);
                            }
                        }

                        // Set the new collection of column definitions.
                        if (columnDefinitions == null) columnDefinitions = new ArrayList<T8TableColumnDefinition>();
                        definition.setColumnDefinitions(columnDefinitions);
                    }
                    else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "No entity selected for specified data source.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while retrieving field definitions for data source: " + dataEntityId, e);
                    JOptionPane.showMessageDialog(clientContext.getParentWindow(), "Field Definitions could not be retrieved.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
                }
            }
            else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "No Data Entity specified.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
        }
    }
}
