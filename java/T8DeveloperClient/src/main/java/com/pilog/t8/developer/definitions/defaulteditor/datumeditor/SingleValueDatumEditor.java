package com.pilog.t8.developer.definitions.defaulteditor.datumeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.format.DefaultFormatter;
import com.pilog.t8.utilities.format.EmptyFormat;
import com.pilog.t8.utilities.format.RegexFormatter;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import javax.swing.JFormattedTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

/**
 * @author Bouwer du Preez
 */
public class SingleValueDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private final T8DataType dataType;

    public SingleValueDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {
        super(definitionContext, definition, datumType);
        initComponents();
        jFormattedTextFieldValue.addPropertyChangeListener(new TextFieldPropertyChangeListener());
        jFormattedTextFieldValue.addKeyListener(new TextFieldKeyListener());
        jFormattedTextFieldValue.addFocusListener(new TextFieldFocusListener());
        dataType = datumType.getDataType();
        applyFormatter();
    }

    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void commitChanges()
    {
        String text;

        // Get the text value.
        text = jFormattedTextFieldValue.getText();

        // Trim the text value if specified.
        if ((text != null) && (datumType.isTrim())) text = text.trim();

        // Return the correct data type.
        if ((text == null) || (text.length() == 0))
        {
            setDefinitionDatum(null);
        }
        else if (dataType.equals(T8DataType.DEFINITION_IDENTIFIER))
        {
            setDefinitionDatum(text);
        }
        else if (dataType.equals(T8DataType.DEFINITION_TYPE_IDENTIFIER))
        {
            setDefinitionDatum(text);
        }
        else if (dataType.equals(T8DataType.DEFINITION_INSTANCE_IDENTIFIER))
        {
            setDefinitionDatum(text);
        }
        else if (dataType.equals(T8DataType.EPIC_EXPRESSION))
        {
            setDefinitionDatum(text);
        }
        else if ((dataType.equals(T8DataType.STRING)) || (dataType.equals(T8DataType.DISPLAY_STRING)) || (dataType.equals(T8DataType.PASSWORD_HASH)))
        {
            setDefinitionDatum(text);
        }
        else if (dataType.equals(T8DataType.GUID))
        {
            setDefinitionDatum(text);
        }
        else if (dataType.equals(T8DataType.DOUBLE))
        {
            setDefinitionDatum(Double.parseDouble(text));
        }
        else if (dataType.equals(T8DataType.FLOAT))
        {
            setDefinitionDatum(Float.parseFloat(text));
        }
        else if (dataType.equals(T8DataType.INTEGER))
        {
            setDefinitionDatum(Integer.parseInt(text));
        }
        else if (dataType.equals(T8DataType.LONG))
        {
            setDefinitionDatum(Long.parseLong(text));
        }
        else throw new RuntimeException("Invalid T8DataType encountered: " + dataType);
    }

    @Override
    public void setEditable(boolean editable)
    {
        jFormattedTextFieldValue.setEditable(editable);
    }

    @Override
    public void refreshEditor()
    {
        Object value;

        value = getDefinitionDatum();
        jFormattedTextFieldValue.setText(value != null ? value.toString() : null);
    }

    private void applyFormatter()
    {
        if (dataType.equals(T8DataType.DOUBLE)) applyDecimalFormatter(jFormattedTextFieldValue, "#0.###");
        else if (dataType.equals(T8DataType.DOUBLE)) applyDecimalFormatter(jFormattedTextFieldValue, "#0.###");
        else if (dataType.equals(T8DataType.INTEGER) || dataType.equals(T8DataType.LONG)) applyDecimalFormatter(jFormattedTextFieldValue, "#0");
        else applyRegularExpressionFormatter(jFormattedTextFieldValue, ".*");
    }

    private boolean applyRegularExpressionFormatter(JFormattedTextField textField, String pattern)
    {
        try
        {
            RegexFormatter formatter;

            formatter = new RegexFormatter(pattern, 0, DefaultFormatter.Case.ANY);
            formatter.setAllowsInvalid(true);
            formatter.setOverwriteMode(false);
            formatter.setCommitsOnValidEdit(true);

            textField.setFocusLostBehavior(JFormattedTextField.COMMIT);
            textField.setFormatterFactory(new DefaultFormatterFactory(formatter));
            return true;
        }
        catch (Exception e)
        {
            T8Log.log("While applying pattern '" + pattern + "' to default datum editor.", e);
            return false;
        }
    }

    private boolean applyDecimalFormatter(JFormattedTextField textField, String pattern)
    {
        try
        {
            NumberFormatter formatter;

            formatter = new NumberFormatter();
            formatter.setFormat(new EmptyFormat(new DecimalFormat(pattern), null));
            formatter.setAllowsInvalid(true);
            formatter.setOverwriteMode(false);
            formatter.setCommitsOnValidEdit(true);

            textField.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
            textField.setFormatterFactory(new DefaultFormatterFactory(formatter));
            return true;
        }
        catch (Exception e)
        {
            T8Log.log("While applying pattern '" + pattern + "' to default datum.", e);
            return false;
        }
    }

    private class TextFieldPropertyChangeListener implements PropertyChangeListener
    {
        @Override
        public void propertyChange(PropertyChangeEvent event)
        {
            if ("editValid".equals(event.getPropertyName()))
            {
                boolean validEdit;

                validEdit = (Boolean) event.getNewValue();
                jFormattedTextFieldValue.setBackground(validEdit ? Color.WHITE : LAFConstants.INVALID_PINK);
            }
        }
    }

    private class TextFieldKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                commitChanges();
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }

    private class TextFieldFocusListener implements FocusListener
    {
        @Override
        public void focusGained(FocusEvent e)
        {
        }

        @Override
        public void focusLost(FocusEvent e)
        {
            commitChanges();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFormattedTextFieldValue = new javax.swing.JFormattedTextField();

        setLayout(new java.awt.BorderLayout());
        add(jFormattedTextFieldValue, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFormattedTextField jFormattedTextFieldValue;
    // End of variables declaration//GEN-END:variables
}
