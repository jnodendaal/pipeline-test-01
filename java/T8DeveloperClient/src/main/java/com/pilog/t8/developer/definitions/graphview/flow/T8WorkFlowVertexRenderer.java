package com.pilog.t8.developer.definitions.graphview.flow;

import com.pilog.graph.model.GraphVertex;
import com.pilog.graph.view.DefaultVertexRenderer;
import com.pilog.graph.view.Graph;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.definition.flow.T8WorkFlowTaskDefinition;
import com.pilog.t8.definition.flow.node.activity.T8FlowActivityDefinition;
import com.pilog.t8.definition.flow.node.activity.T8ScriptActivityDefinition;
import com.pilog.t8.definition.flow.node.activity.T8TaskActivityDefinition;
import com.pilog.t8.definition.flow.node.event.T8EndEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8FlowEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8SignalAccumulationEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8SignalCatchEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8SignalTriggerEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8StartEventDefinition;
import com.pilog.t8.definition.flow.node.gateway.T8ANDGatewayDefinition;
import com.pilog.t8.definition.flow.node.gateway.T8ORGatewayDefinition;
import com.pilog.t8.definition.flow.node.gateway.T8XORGatewayDefinition;
import com.pilog.t8.definition.flow.task.user.T8WorkFlowUserTaskDefinition;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Transparency;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.DataBufferInt;
import java.awt.image.Kernel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.swing.ImageIcon;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowVertexRenderer extends DefaultVertexRenderer
{
    private final T8WorkFlowGraphConfiguration config;
    private final T8DefinitionManager definitionManager;

    public static String KEY_BLUR_QUALITY = "blur_quality";
    public static String VALUE_BLUR_QUALITY_FAST = "fast";
    public static String VALUE_BLUR_QUALITY_HIGH = "high";

    protected BufferedImage shadow = null;
    protected BufferedImage original = null;
    protected float shadowAngle = 45;
    protected int shadowDistance = 5;
    protected int shadowSize = 5;
    protected float shadowOpacity = 0.5f;
    protected Color shadowColor = new Color(0x000000);
    protected Insets vertexInsets = new Insets(30, 30, 30, 30);

    // Cached values for fast painting.
    protected int distance_x = 0;
    protected int distance_y = 0;
    protected HashMap<Object, Object> hints;
    protected Map<String, String> taskTypes;

    protected static ImageIcon scriptIcon = new javax.swing.ImageIcon(T8WorkFlowVertexRenderer.class.getResource("/com/pilog/t8/developer/icons/scriptIcon.png"));
    protected static ImageIcon userTaskIcon = new javax.swing.ImageIcon(T8WorkFlowVertexRenderer.class.getResource("/com/pilog/t8/developer/icons/userIcon.png"));
    protected static ImageIcon computerIcon = new javax.swing.ImageIcon(T8WorkFlowVertexRenderer.class.getResource("/com/pilog/t8/developer/icons/computerIcon.png"));

    public T8WorkFlowVertexRenderer(T8DefinitionManager definitionManager, T8WorkFlowGraphConfiguration config)
    {
        this.definitionManager = definitionManager;
        this.config = config;
        this.taskTypes = new HashMap<String, String>();
        cacheTaskTypes();
        computeShadowPosition();
        hints = new HashMap<Object, Object>();
        hints.put(KEY_BLUR_QUALITY, VALUE_BLUR_QUALITY_FAST);
    }

    private void cacheTaskTypes()
    {
        try
        {
            List<T8DefinitionMetaData> metaDataList;

            metaDataList = definitionManager.getGroupDefinitionMetaData(null, T8WorkFlowTaskDefinition.GROUP_IDENTIFIER);
            for (T8DefinitionMetaData metaData : metaDataList)
            {
                taskTypes.put(metaData.getId(), metaData.getTypeId());
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while caching task definition meta data.", e);
        }
    }

    private String getTaskType(String taskId)
    {
        if (!taskTypes.containsKey(taskId))
        {
            try
            {
                T8DefinitionMetaData metaData;

                metaData = definitionManager.getDefinitionMetaData(null, taskId);
                if (metaData != null)
                {
                    taskTypes.put(taskId, metaData.getTypeId());
                    return metaData.getTypeId();
                }
                else return null;
            }
            catch (Exception e)
            {
                T8Log.log("Exception while caching task definition meta data.", e);
                return null;
            }
        }
        else
        {
            return taskTypes.get(taskId);
        }
    }

    public T8WorkFlowGraphConfiguration getConfiguration()
    {
        return config;
    }

    private Image getActivityIcon(T8FlowActivityDefinition activityDefinition)
    {
        if (activityDefinition instanceof T8ScriptActivityDefinition)
        {
            return scriptIcon.getImage();
        }
        else if (activityDefinition instanceof T8TaskActivityDefinition)
        {
            T8TaskActivityDefinition taskActivityDefinition;
            String taskIdentifier;
            String taskTypeIdentifier;

            taskActivityDefinition = (T8TaskActivityDefinition)activityDefinition;
            taskIdentifier = taskActivityDefinition.getTaskIdentifier();
            taskTypeIdentifier = getTaskType(taskIdentifier);
            if (Objects.equals(taskTypeIdentifier, T8WorkFlowUserTaskDefinition.TYPE_IDENTIFIER))
            {
                return userTaskIcon.getImage();
            }
            else
            {
                return computerIcon.getImage();
            }
        }
        else return computerIcon.getImage();
    }

    @Override
    public Dimension getVertexSize(Graph graph, GraphVertex vertex, boolean isSelected)
    {
        FontMetrics fontMetrics;
        Object vertexData;

        vertexData = (Object)vertex.getVertexDataObject();
        if (vertexData instanceof T8FlowActivityDefinition)
        {
            Dimension size;

            fontMetrics = this.getFontMetrics(config.getTextFont());
            size = new Dimension();
            size.width = fontMetrics.stringWidth(((T8FlowActivityDefinition)vertexData).getIdentifier());
            size.width += (vertexInsets.left + vertexInsets.right);
            size.height = fontMetrics.getHeight();
            size.height += (vertexInsets.top + vertexInsets.bottom);
            return size;
        }
        else if (vertexData instanceof T8ORGatewayDefinition)
        {
            return new Dimension(50, 50);
        }
        else if (vertexData instanceof T8XORGatewayDefinition)
        {
            return new Dimension(50, 50);
        }
        else if (vertexData instanceof T8ANDGatewayDefinition)
        {
            return new Dimension(50, 50);
        }
        else if (vertexData instanceof T8FlowEventDefinition)
        {
            return new Dimension(50, 50);
        }
        else return new Dimension(50, 50);
    }

    public void setSubject(BufferedImage subject)
    {
        if (subject != null)
        {
            this.original = subject;
            refreshShadow();
        }
        else
        {
            this.original = null;
            this.original = null;
        }
    }

    private void paintDefinition(Graphics2D g2, T8Definition vertexDefinition)
    {
        Color highColor = LAFConstants.CONTENT_PANEL_BG_COLOR;
        Color lowColor = LAFConstants.PROPERTY_BLUE;
        int shapeWidth;
        int shapeHeight;

        // Set the selection color of the vertex shape.
        if (isSelected)
        {
            highColor = config.getSelectedHighColor();
            lowColor = config.getSelectedLowColor();
        }

        if (vertexDefinition instanceof T8FlowActivityDefinition)
        {
            T8FlowActivityDefinition activityDefinition;
            T8DefinitionValidationError validationError = null;
            Shape vertexShape;
            Image activityIcon;

            // Cast to the type we need.
            activityDefinition = (T8FlowActivityDefinition)vertexDefinition;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                if (activityDefinition.getProfileIdentifier() == null) // System activity.
                {
                    highColor = config.getSystemActivityHighColor();
                    lowColor = config.getSystemActivityLowColor();
                }
                else
                {
                    highColor = config.getActivityHighColor();
                    lowColor = config.getActivityLowColor();
                }
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;

            // Create the shape of the vertex and paint it.
            vertexShape = new RoundRectangle2D.Float(0, 0, shapeWidth, shapeHeight, 8, 8);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);

            // Paint the vertex label.
            paintLabel(g2, shapeWidth, shapeHeight, vertexDefinition.getIdentifier());

            // Paint the activity icon.
            activityIcon = getActivityIcon(activityDefinition);
            if (activityIcon != null)
            {
                g2.drawImage(activityIcon, 5, 5, null);
            }

            if(validationError != null)
            {
                paintValidationLabel(g2, shapeWidth, shapeHeight, validationError.getMessage());
            }
        }
        else if (vertexDefinition instanceof T8ORGatewayDefinition)
        {
            GeneralPath vertexShape;
            double halfWidth;
            double halfHeight;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                highColor = config.getGatewayHighColor();
                lowColor = config.getGatewayLowColor();
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;
            halfWidth = shapeWidth/2.0;
            halfHeight = shapeHeight/2.0;

            // Create the shape of the vertex and paint it.
            vertexShape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
            vertexShape.moveTo(0, halfHeight);
            vertexShape.lineTo(halfWidth, 0);
            vertexShape.lineTo(shapeWidth, halfHeight);
            vertexShape.lineTo(halfWidth, shapeHeight);
            vertexShape.lineTo(0, halfHeight);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);

            // Paint the vertex label.
            paintLabel(g2, shapeWidth, shapeHeight, "O");
        }
        else if (vertexDefinition instanceof T8XORGatewayDefinition)
        {
            GeneralPath vertexShape;
            double halfWidth;
            double halfHeight;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                highColor = config.getGatewayHighColor();
                lowColor = config.getGatewayLowColor();
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;
            halfWidth = shapeWidth/2.0;
            halfHeight = shapeHeight/2.0;

            // Create the shape of the vertex and paint it.
            vertexShape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
            vertexShape.moveTo(0, halfHeight);
            vertexShape.lineTo(halfWidth, 0);
            vertexShape.lineTo(shapeWidth, halfHeight);
            vertexShape.lineTo(halfWidth, shapeHeight);
            vertexShape.lineTo(0, halfHeight);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);

            // Paint the vertex label.
            paintLabel(g2, shapeWidth, shapeHeight, "X");
        }
        else if (vertexDefinition instanceof T8ANDGatewayDefinition)
        {
            GeneralPath vertexShape;
            double halfWidth;
            double halfHeight;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                highColor = config.getGatewayHighColor();
                lowColor = config.getGatewayLowColor();
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;
            halfWidth = shapeWidth/2.0;
            halfHeight = shapeHeight/2.0;

            // Create the shape of the vertex and paint it.
            vertexShape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
            vertexShape.moveTo(0, halfHeight);
            vertexShape.lineTo(halfWidth, 0);
            vertexShape.lineTo(shapeWidth, halfHeight);
            vertexShape.lineTo(halfWidth, shapeHeight);
            vertexShape.lineTo(0, halfHeight);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);

            // Paint the vertex label.
            paintLabel(g2, shapeWidth, shapeHeight, "+");
        }
        else if (vertexDefinition instanceof T8StartEventDefinition)
        {
            Shape vertexShape;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                highColor = config.getStartHighColor();
                lowColor = config.getStartLowColor();
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;

            // Create the shape of the vertex and paint it.
            vertexShape = new Ellipse2D.Float(0, 0, shapeWidth, shapeHeight);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);
        }
        else if (vertexDefinition instanceof T8EndEventDefinition)
        {
            Shape vertexShape;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                highColor = config.getEndHighColor();
                lowColor = config.getEndLowColor();
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;

            // Create the shape of the vertex and paint it.
            vertexShape = new Ellipse2D.Float(0, 0, shapeWidth, shapeHeight);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);
        }
        else if (vertexDefinition instanceof T8SignalTriggerEventDefinition)
        {
            Shape vertexShape;
            int centerX;
            int centerY;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                highColor = config.getSignHighColor();
                lowColor = config.getSignalLowColor();
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;
            centerX = Math.round(shapeWidth/2.0f);
            centerY = Math.round(shapeHeight/2.0f);

            // Create the shape of the vertex and paint it.
            vertexShape = new Ellipse2D.Float(0, 0, shapeWidth, shapeHeight);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);

            // Create the shape of the 'icon' and paint it.
            highColor = config.getSignHighColor();
            lowColor = config.getSignalLowColor();

            // Create a shape for the sign to be painted on the vertex.
            vertexShape = new Polygon(new int[]{centerX, centerX - 10, centerX + 10}, new int[]{centerY - 12, centerY + 8, centerY + 8}, 3);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setComposite(AlphaComposite.SrcAtop);
            g2.setPaint(new GradientPaint(0, 0, highColor, 0, shapeHeight, lowColor));
            g2.draw(vertexShape);
        }
        else if (vertexDefinition instanceof T8SignalCatchEventDefinition)
        {
            Shape vertexShape;
            int centerX;
            int centerY;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                highColor = config.getSignalHighColor();
                lowColor = config.getSignalLowColor();
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;
            centerX = Math.round(shapeWidth/2.0f);
            centerY = Math.round(shapeHeight/2.0f);

            // Create the shape of the vertex and paint it.
            vertexShape = new Ellipse2D.Float(0, 0, shapeWidth, shapeHeight);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);

            // Create the shape of the 'icon' and paint it.
            highColor = config.getSignHighColor();
            lowColor = config.getSignLowColor();

            // Create a shape for the sign to be painted on the vertex.
            vertexShape = new Polygon(new int[]{centerX, centerX - 10, centerX + 10}, new int[]{centerY - 12, centerY + 8, centerY + 8}, 3);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setComposite(AlphaComposite.SrcAtop);
            g2.setPaint(new GradientPaint(0, 0, highColor, 0, shapeHeight, lowColor));
            g2.fill(vertexShape);
        }
        else if (vertexDefinition instanceof T8SignalAccumulationEventDefinition)
        {
            Shape vertexShape;
            int centerX;
            int centerY;

            // If the shape is not selected, set its default color.
            if (!isSelected)
            {
                highColor = config.getSignalHighColor();
                lowColor = config.getSignalLowColor();
            }

            // Get the size of the shape from the renderer component size.
            shapeWidth = getWidth()-shadowSize;
            shapeHeight = getHeight()-shadowSize;
            centerX = Math.round(shapeWidth/2.0f);
            centerY = Math.round(shapeHeight/2.0f);

            // Create the shape of the vertex and paint it.
            vertexShape = new Ellipse2D.Float(0, 0, shapeWidth, shapeHeight);
            paintShape(g2, vertexShape, highColor, lowColor, true, true, 8);

            // Create the shape of the 'icon' and paint it.
            highColor = config.getSignHighColor();
            lowColor = config.getSignLowColor();

            // Create a shape for the sign to be painted on the vertex.
            vertexShape = new Polygon(new int[]{centerX, centerX - 10, centerX + 10}, new int[]{centerY - 12, centerY + 8, centerY + 8}, 3);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setComposite(AlphaComposite.SrcAtop);
            g2.setPaint(new GradientPaint(0, 0, highColor, 0, shapeHeight, lowColor));
            g2.fill(vertexShape);
        }
    }

    @Override
    public void paint(Graphics g)
    {
        Graphics2D g2;
        Object vertexObject;

        // Cast graphics context to the required type.
        g2 = (Graphics2D)g;

        vertexObject = vertex.getVertexDataObject();
        if (vertexObject instanceof T8Definition)
        {
            T8Definition vertexDefinition;

            // Get the vertex definition.
            vertexDefinition = (T8Definition)vertex.getVertexDataObject();
            paintDefinition(g2, vertexDefinition);
        }
        else // Swimlane.
        {
            paintSwimLane(g2, vertex.getVertexDataObject() + "");
        }
    }

    private void paintSwimLane(Graphics2D g2, String swimLaneIdentifier)
    {
        Rectangle2D stringBounds;
        Dimension size;

        // Get the size of the swimlane.
        size = getSize();

        // Draw the swimlane and outer border.
        g2.setColor(config.getSwimlaneBackgroundColor());
        g2.fillRect(0, 0, size.width-1, size.height-1);
        g2.setColor(config.getSwimlangeBorderColor());
        g2.drawRect(0, 0, size.width-1, size.height-1);

        // Paint the label text.
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(config.getSwimlaneTextColor());
        g2.setFont(config.getTextFont());
        stringBounds = getStringBounds((Graphics2D)g2, swimLaneIdentifier);
        g2.drawString(swimLaneIdentifier, 10, Math.round(1 + stringBounds.getHeight()));
    }

    private void paintLabel(Graphics2D g2, int shapeWidth, int shapeHeight, String text)
    {
        Rectangle2D bounds;

        g2.setFont(config.getTextFont());
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(2));
        g2.setColor(Color.DARK_GRAY);

        bounds = getStringBounds(g2, text);
        g2.drawString(text, (float)((shapeWidth/2.0) - (bounds.getWidth()/2.0)), (float)((shapeHeight/2.0) + (bounds.getHeight()/4.0)));
    }

    private void paintValidationLabel(Graphics2D g2, int shapeWidth, int shapeHeight, String text)
    {
        Rectangle2D bounds;

        g2.setFont(config.getTextFont().deriveFont(9f));
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(2));
        g2.setColor(Color.DARK_GRAY);

        bounds = getStringBounds(g2, text);

        g2.drawString(text, (float)(((shapeWidth/2.0) - (bounds.getWidth()/2.0))), (float)(((shapeHeight/2.0) + (bounds.getHeight()/4.0))*1.5));
    }

    private void paintShape(Graphics2D g2, Shape clipShape, Color highColor, Color lowColor, boolean fill, boolean paintShadow, int borderSize)
    {
        BufferedImage clipImage;
        Graphics2D g2Shape;
        int shapeWidth;
        int shapeHeight;

        // Create the clip image.
        clipImage = createClipImage(g2, clipShape);
        setSubject(clipImage);
        g2Shape = clipImage.createGraphics();
        shapeWidth = clipShape.getBounds().width;
        shapeHeight = clipShape.getBounds().height;

        if ((shadow != null) && (paintShadow) && (config.isShadowsEnabled()))
        {
            int x = (shapeWidth - shadow.getWidth()) / 2;
            int y = (shapeHeight - shadow.getHeight()) / 2;
            g2.drawImage(shadow, x + distance_x, y + distance_y, null);
        }

        if (original != null)
        {
            int x = (shapeWidth - original.getWidth()) / 2;
            int y = (shapeHeight - original.getHeight()) / 2;
            g2.drawImage(original, x, y, null);
        }

        // Fill the shape with a gradient.
        g2Shape.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2Shape.setComposite(AlphaComposite.SrcAtop);
        g2Shape.setPaint(new GradientPaint(0, 0, highColor, 0, shapeHeight, lowColor));
        if (fill) g2Shape.fill(clipShape);

        // Apply the border glow effect (or just a plain border).
        if (config.isBorderGlowEnabled())
        {
            paintBorderGlow(g2Shape, clipShape, highColor, lowColor, borderSize);
        }
        else
        {
            g2Shape.setPaint(config.getNodeBorderColor());
            g2Shape.draw(clipShape);
        }

        // Draw the clip image.
        g2.drawImage(clipImage, 0, 0, null);

        // Release the unused graphics context.
        g2Shape.dispose();
    }

    private Rectangle2D getStringBounds(Graphics2D g2, String string)
    {
        FontMetrics metrics;

        metrics = g2.getFontMetrics();
        return metrics.getStringBounds(string, 0, string.length(), g2);
    }

    private BufferedImage createClipImage(Graphics2D g, Shape shape)
    {
        int shapeWidth;
        int shapeHeight;

        shapeWidth = shape.getBounds().width;
        shapeHeight = shape.getBounds().height;

        // Create a translucent intermediate image in which we can perform the soft clipping.
        GraphicsConfiguration gc = g.getDeviceConfiguration();
        BufferedImage img = gc.createCompatibleImage(shapeWidth, shapeHeight, Transparency.TRANSLUCENT);
        Graphics2D g2 = img.createGraphics();

        // Clear the image so all pixels have zero alpha.
        g2.setComposite(AlphaComposite.Clear);
        g2.fillRect(0, 0, shapeWidth, shapeHeight);

        // Render our clip shape into the image.  Note that we enable
        // antialiasing to achieve the soft clipping effect.  Try
        // commenting out the line that enables antialiasing, and
        // you will see that you end up with the usual hard clipping.
        g2.setComposite(AlphaComposite.Src);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.WHITE);
        g2.fill(shape);
        g2.dispose();

        return img;
    }

    private static Color getMixedColor(Color c1, float pct1, Color c2, float pct2)
    {
        float[] clr1 = c1.getComponents(null);
        float[] clr2 = c2.getComponents(null);
        for (int i = 0; i < clr1.length; i++)
        {
            clr1[i] = (clr1[i] * pct1) + (clr2[i] * pct2);
        }
        return new Color(clr1[0], clr1[1], clr1[2], clr1[3]);
    }

    // Here's the trick... To render the glow, we start with a thick pen
    // of the "inner" color and stroke the desired shape.  Then we repeat
    // with increasingly thinner pens, moving closer to the "outer" color
    // and increasing the opacity of the color so that it appears to
    // fade towards the interior of the shape.  We rely on the "clip shape"
    // having been rendered into our destination image already so that
    // the SRC_ATOP rule will take care of clipping out the part of the
    // stroke that lies outside our shape.
    private void paintBorderGlow(Graphics2D g2, Shape clipShape, Color highColor, Color lowColor, int glowWidth)
    {
        Color glowInnerHigh;
        Color glowInnerLow;
        Color glowOuterHigh;
        Color glowOuterLow;
        int shapeWidth;
        int shapeHeight;
        int gw = glowWidth * 2;

        // Calculate colors to use.
        glowInnerHigh = highColor.brighter();
        glowInnerLow = highColor.brighter();
        glowOuterHigh = lowColor.darker();
        glowOuterLow = lowColor.darker();

        shapeWidth = clipShape.getBounds().width;
        shapeHeight = clipShape.getBounds().height;
        for (int i = gw; i >= 2; i -= 2)
        {
            float pct = (float) (gw - i) / (gw - 1);
            Color mixHi = getMixedColor(glowInnerHigh, pct, glowOuterHigh, 1.0f - pct);
            Color mixLo = getMixedColor(glowInnerLow, pct, glowOuterLow, 1.0f - pct);
            g2.setPaint(new GradientPaint(0.0f, shapeHeight * 0.25f, mixHi, 0.0f, shapeHeight, mixLo));
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, pct));
            g2.setStroke(new BasicStroke(i));
            g2.draw(clipShape);
        }
    }

    public void refreshShadow()
    {
        if (original != null)
        {
            shadow = createDropShadow(original);
        }
    }

    private void computeShadowPosition()
    {
        double angleRadians = Math.toRadians(shadowAngle);
        distance_x = (int) (Math.cos(angleRadians) * shadowDistance);
        distance_y = (int) (Math.sin(angleRadians) * shadowDistance);
    }

    private BufferedImage prepareImage(BufferedImage image)
    {
        BufferedImage subject = new BufferedImage(image.getWidth() + shadowSize * 2, image.getHeight() + shadowSize * 2, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = subject.createGraphics();
        g2.drawImage(image, null, shadowSize, shadowSize);
        g2.dispose();

        return subject;
    }

    private BufferedImage createDropShadow(BufferedImage image)
    {
        BufferedImage subject = prepareImage(image);

        if (hints.get(KEY_BLUR_QUALITY) == VALUE_BLUR_QUALITY_HIGH)
        {
            BufferedImage newShadow = new BufferedImage(subject.getWidth(), subject.getHeight(), BufferedImage.TYPE_INT_ARGB);
            BufferedImage shadowMask = createShadowMask(subject);
            getLinearBlurOp(shadowSize).filter(shadowMask, newShadow);
            return newShadow;
        }

        applyShadow(subject);
        return subject;
    }

    private void applyShadow(BufferedImage image)
    {
        int dstWidth = image.getWidth();
        int dstHeight = image.getHeight();

        int left = (shadowSize - 1) >> 1;
        int right = shadowSize - left;
        int xStart = left;
        int xStop = dstWidth - right;
        int yStart = left;
        int yStop = dstHeight - right;

        int shadowRgb = shadowColor.getRGB() & 0x00FFFFFF;

        int[] aHistory = new int[shadowSize];
        int historyIdx = 0;

        int aSum;

        int[] dataBuffer = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
        int lastPixelOffset = right * dstWidth;
        float sumDivider = shadowOpacity / shadowSize;

        // horizontal pass

        for (int y = 0, bufferOffset = 0; y < dstHeight; y++, bufferOffset = y * dstWidth)
        {
            aSum = 0;
            historyIdx = 0;
            for (int x = 0; x < shadowSize; x++, bufferOffset++)
            {
                int a = dataBuffer[bufferOffset] >>> 24;
                aHistory[x] = a;
                aSum += a;
            }

            bufferOffset -= right;

            for (int x = xStart; x < xStop; x++, bufferOffset++)
            {
                int a = (int) (aSum * sumDivider);
                dataBuffer[bufferOffset] = a << 24 | shadowRgb;

                // substract the oldest pixel from the sum
                aSum -= aHistory[historyIdx];

                // get the lastest pixel
                a = dataBuffer[bufferOffset + right] >>> 24;
                aHistory[historyIdx] = a;
                aSum += a;

                if (++historyIdx >= shadowSize)
                {
                    historyIdx -= shadowSize;
                }
            }
        }

        // vertical pass
        for (int x = 0, bufferOffset = 0; x < dstWidth; x++, bufferOffset = x)
        {
            aSum = 0;
            historyIdx = 0;
            for (int y = 0; y < shadowSize; y++, bufferOffset += dstWidth)
            {
                int a = dataBuffer[bufferOffset] >>> 24;
                aHistory[y] = a;
                aSum += a;
            }

            bufferOffset -= lastPixelOffset;

            for (int y = yStart; y < yStop; y++, bufferOffset += dstWidth)
            {
                int a = (int) (aSum * sumDivider);
                dataBuffer[bufferOffset] = a << 24 | shadowRgb;

                // substract the oldest pixel from the sum
                aSum -= aHistory[historyIdx];

                // get the lastest pixel
                a = dataBuffer[bufferOffset + lastPixelOffset] >>> 24;
                aHistory[historyIdx] = a;
                aSum += a;

                if (++historyIdx >= shadowSize)
                {
                    historyIdx -= shadowSize;
                }
            }
        }
    }

    private BufferedImage createShadowMask(BufferedImage image)
    {
        BufferedImage mask = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = mask.createGraphics();
        g2d.drawImage(image, 0, 0, null);
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_IN, shadowOpacity));
        g2d.setColor(shadowColor);
        g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
        g2d.dispose();

        return mask;
    }

    private ConvolveOp getLinearBlurOp(int size)
    {
        float[] data = new float[size * size];
        float value = 1.0f / (float) (size * size);
        for (int i = 0; i < data.length; i++)
        {
            data[i] = value;
        }
        return new ConvolveOp(new Kernel(size, size, data));
    }
}
