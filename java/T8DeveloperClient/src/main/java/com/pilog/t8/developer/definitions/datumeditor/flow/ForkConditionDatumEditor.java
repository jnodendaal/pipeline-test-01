package com.pilog.t8.developer.definitions.datumeditor.flow;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionConstructorParameters;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.flow.node.gateway.T8FlowForkConditionDefinition;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.T8DefaultDefinitionDatumEditor;
import com.pilog.t8.developer.utils.T8DefinitionMetaDataInputDialog;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * @author Bouwer du Preez
 */
public class ForkConditionDatumEditor extends T8DefaultDefinitionDatumEditor
{
    private final List<T8Definition> definitionList;
    private final List<ForkConditionDefinitionEditor> editorList;
    private int maximumElements;
    private boolean editable;

    public ForkConditionDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType)
    {

        super(definitionContext, definition, datumType);
        initComponents();
        this.maximumElements = datumType.getMaximumElements();
        this.definitionList = new ArrayList<T8Definition>();
        this.editorList = new ArrayList<ForkConditionDefinitionEditor>();
        if (maximumElements == -1) maximumElements = 10000; // A very large value, since -1 indicates unbounded element count.
    }
    
    @Override
    public void initializeComponent()
    {
    }

    @Override
    public void startComponent()
    {
        refreshEditor();
    }
    
    @Override
    public void stopComponent()
    {
    }

    @Override
    public void commitChanges()
    {
        if (definitionList != null)
        {
            for (ForkConditionDefinitionEditor editor : editorList)
            {
                editor.commitChanges();
            }

            setDefinitionDatum(new ArrayList<T8Definition>(definitionList));
        }
        else setDefinitionDatum(null);
    }

    @Override
    public void refreshEditor()
    {
        List<T8Definition> valueList;

        removeAllDefinitionEditors();
        valueList = (List<T8Definition>)getDefinitionDatum();
        if ((valueList != null) && (valueList.size() > 0))
        {
            for (T8Definition rowDefinition : valueList)
            {
                addDefinitionEditor(rowDefinition, false);
            }
        }
    }

    @Override
    public void setEditable(boolean editable)
    {
        this.editable = editable;
        jButtonAdd.setEnabled(editable);
    }

    private void removeAllDefinitionEditors()
    {
        definitionList.clear();
        editorList.clear();
        jPanelContent.removeAll();
        jPanelContent.revalidate();
        jPanelContent.repaint();
    }
    
    public void removeDefinitionEditor(T8Definition definitionToRemove)
    {
        int index;
        
        index = definitionList.indexOf(definitionToRemove);
        if (index > -1)
        {
            ForkConditionDefinitionEditor editor;
            
            editor = editorList.remove(index);
            definitionList.remove(definitionToRemove);
            jPanelContent.remove(editor);
            jPanelContent.revalidate();
            jPanelContent.repaint();
        }
    }
    
    private void addDefinitionEditor(T8Definition newDefinition, boolean commitNewValue)
    {
        ForkConditionDefinitionEditor editor;

        definitionList.add(newDefinition);
        if (commitNewValue) setDefinitionDatum(new ArrayList<T8Definition>(definitionList));
        
        editor = new ForkConditionDefinitionEditor(this, (T8FlowForkConditionDefinition)newDefinition);
        editorList.add(editor);
        jPanelContent.add(editor);
        jPanelContent.revalidate();
        jPanelContent.repaint();
    }
    
    private void displayDefinitionPopupMenu(Component contextComponent, int x, int y)
    {
        JPopupMenu popupMenu;
        JMenuItem menuItem;

        // Create the ActionListener for the menu items.
        ActionListener menuListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                DefinitionMenuItem menuItem;
                String code;

                menuItem = (DefinitionMenuItem)event.getSource();
                code = menuItem.getCode();
                if (code.equals("ADD_DEFINITION"))
                {
                    addDefinition((T8DefinitionDatumOption)menuItem.getDataObject());
                }
            }
        };

        // Create a new popup menu.
        popupMenu = new JPopupMenu();

        // Create the definition menu.
        try
        {
            ArrayList<T8DefinitionDatumOption> datumOptions;
            datumOptions = definition.getDatumOptions(definitionContext, datumType.getIdentifier());

            //Only show the popup if there are multiple items to choose from, otherwise just add the definition
            if(datumOptions.size() == 1)
            {
                addDefinition(datumOptions.get(0));
            }
            else
            {
                for (T8DefinitionDatumOption datumOption : datumOptions)
                {
                    menuItem = new DefinitionMenuItem(datumOption.getDisplayName(), "ADD_DEFINITION", datumType.getIdentifier(), datumOption);
                    menuItem.addActionListener(menuListener);
                    popupMenu.add(menuItem);
                }

                // Display the newly constructed popup menu.
                popupMenu.show(contextComponent, x, y);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Definition datum options could not be resolved successfully.", e);
        }
    }

    private void addDefinition(T8DefinitionDatumOption datumOption)
    {
        T8DefinitionConstructorParameters constructorParameters;
        String definitionTypeIdentifier;
        T8DefinitionMetaData metaData;

        constructorParameters = (T8DefinitionConstructorParameters)datumOption.getValue();
        definitionTypeIdentifier = constructorParameters.getTypeIdentifier();

        try
        {
            metaData = T8DefinitionMetaDataInputDialog.getNewDefinitionMetaData(definitionContext, ForkConditionDatumEditor.this, definitionContext.getDefinitionTypeMetaData(definitionTypeIdentifier), definition);
            if (metaData != null)
            {
                try
                {
                    ArrayList<Object> parameters;

                    parameters = constructorParameters.getParameters();
                    if (parameters == null)
                    {
                        T8Definition newDefinition;

                        newDefinition = (T8Definition)definitionContext.createNewDefinition(metaData.getId(), definitionTypeIdentifier);
                        newDefinition.setProjectIdentifier(metaData.getProjectId());
                        addDefinitionEditor(newDefinition, true);
                    }
                    else
                    {
                        T8Definition newDefinition;

                        newDefinition = (T8Definition)definitionContext.createNewDefinition(metaData.getId(), definitionTypeIdentifier, parameters);
                        newDefinition.setProjectIdentifier(metaData.getProjectId());
                        addDefinitionEditor(newDefinition, true);
                    }
                }
                catch (Exception ex)
                {
                    T8Log.log("Exception while constructing new fork condition editor.", ex);
                }
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while fetching meta data for type: " + definitionTypeIdentifier, e);
        }
    }
    
    public void moveDefinitionUp(T8Definition selectedDefinition)
    {
        int index;

        index = definitionList.indexOf(selectedDefinition);
        if (index > 0)
        {
            selectedDefinition = definitionList.remove(index);
            definitionList.add(index-1, selectedDefinition);
        }

        // Commit the updated definition list.
        setDefinitionDatum(new ArrayList<T8Definition>(definitionList));

        // Refresh the editor.
        refreshEditor();
    }

    public void moveDefinitionDown(T8Definition selectedDefinition)
    {
        int index;

        // Decrement all selected Indices.
        index = definitionList.indexOf(selectedDefinition);
        if (index < definitionList.size() -1)
        {
            selectedDefinition = definitionList.remove(index);
            definitionList.add(index+1, selectedDefinition);
        }

        // Commit the updated definition list.
        setDefinitionDatum(new ArrayList<T8Definition>(definitionList));

        // Refresh the editor.
        refreshEditor();
    }

    private class DefinitionMenuItem extends JMenuItem
    {
        private final String code;
        private final String datumIdentifier;
        private final Object dataObject;

        public DefinitionMenuItem(String text, String code, String datumIdentifier, Object dataObject)
        {
            super(text);
            this.code = code;
            this.datumIdentifier = datumIdentifier;
            this.dataObject = dataObject;
        }

        public String getCode()
        {
            return code;
        }

        public String getDatumIdentifier()
        {
            return datumIdentifier;
        }

        public Object getDataObject()
        {
            return dataObject;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jPanelContent = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAdd);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);

        jPanelContent.setLayout(new javax.swing.BoxLayout(jPanelContent, javax.swing.BoxLayout.Y_AXIS));
        add(jPanelContent, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        displayDefinitionPopupMenu(jButtonAdd, 0, jButtonAdd.getHeight());
    }//GEN-LAST:event_jButtonAddActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
