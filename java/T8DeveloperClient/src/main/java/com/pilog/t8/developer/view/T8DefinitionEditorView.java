package com.pilog.t8.developer.view;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.ui.T8DefinitionEditor;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.ui.event.T8DefinitionEditorListener;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.developer.definitions.defaulteditor.T8DefaultDefinitionEditor;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingWorker;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionEditorView extends T8DefaultDeveloperView implements T8DeveloperView
{
    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8DeveloperView parentView;
    private final T8DefaultComponentContainer container;
    private final List<T8DefinitionViewListener> definitionViewListeners;
    private final T8DefinitionEditorListener editorListener;
    private final T8DefinitionContext definitionContext;
    private T8DefinitionEditor currentEditor;
    private T8DeveloperViewFactory viewFactory;
    private EditorLoader loader;

    public T8DefinitionEditorView(T8DefinitionContext definitionContext, T8DeveloperView parentView)
    {
        initComponents();
        this.clientContext = definitionContext.getClientContext();
        this.sessionContext = definitionContext.getSessionContext();
        this.definitionContext = definitionContext;
        this.parentView = parentView;
        this.viewFactory = parentView != null ? parentView.getViewFactory() : new T8DefaultDeveloperViewFactory(definitionContext);
        this.container = new T8DefaultComponentContainer();
        this.definitionViewListeners = new ArrayList<T8DefinitionViewListener>();
        this.add(container, BorderLayout.CENTER);
        this.container.setComponent(jPanelEditor);
        this.editorListener = new DefinitionEditorListener();
    }

    @Override
    public String getHeader()
    {
        return "Definition Editor View";
    }

    @Override
    public void addView(T8DeveloperView tdv)
    {
        parentView.addView(tdv);
    }

    @Override
    public void removeView(T8DeveloperView tdv)
    {
        parentView.removeView(tdv);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    @Override
    public void setSelectedDefinition(T8Definition selectedDefinition)
    {
        T8Log.log("Setting selected definition: " + selectedDefinition);
        synchronized (this)
        {
            if (loader == null)
            {
                // Load the editor for the newly selected definition.
                if (selectedDefinition != null)
                {
                    // Create a new loader.
                    showLoadingView();
                    loader = new EditorLoader(selectedDefinition);
                    loader.execute();
                }
                else // Just clear the editor panel.
                {
                    // Stop the current editor (if any).
                    if (currentEditor != null)
                    {
                        currentEditor.removeDefinitionEditorListener(editorListener);
                        currentEditor.commitChanges();
                        currentEditor.stopComponent();
                        currentEditor = null;
                    }

                    // Remove the current editor.
                    jPanelEditor.removeAll();
                    jPanelEditor.repaint();
                }
            }
            else
            {
                T8Log.log("Adding definition to executing loader: " + selectedDefinition);
                loader.addDefinitionToLoad(selectedDefinition);
            }
        }
    }

    @Override
    public void notifyDefinitionLinkActivated(T8DefinitionLinkEvent event)
    {
        super.notifyDefinitionLinkActivated(event);
        parentView.notifyDefinitionLinkActivated(event);
    }

    @Override
    public void addDefinitionViewListener(T8DefinitionViewListener tl)
    {
        this.definitionViewListeners.add(tl);
    }

    @Override
    public void removeDefinitionViewListener(T8DefinitionViewListener tl)
    {
        this.definitionViewListeners.remove(tl);
    }

    public void showEditorView()
    {
        container.unlock();
    }

    public void showLoadingView()
    {
        container.setMessage("Loading Editor...");
        container.lock();
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    private class EditorLoader extends SwingWorker<Void, Void>
    {
        private boolean completed = false;
        private volatile T8Definition queuedDefinition;
        private T8DefinitionEditor newEditor;

        private EditorLoader(T8Definition definition)
        {
            addDefinitionToLoad(definition);
        }

        public boolean hasCompleted()
        {
            return completed;
        }

        public final synchronized void addDefinitionToLoad(T8Definition definition)
        {
            this.queuedDefinition = definition;
        }

        @Override
        public Void doInBackground()
        {
            String threadNameBackup;

            // Store the thread name so we can set it back when the thread compeltes.
            threadNameBackup = Thread.currentThread().getName();

            // Now start the loader.
            try
            {
                T8Definition definitionToLoad;

                definitionToLoad = null;
                while (definitionToLoad != queuedDefinition)
                {
                    // Name this thread.
                    Thread.currentThread().setName("Definition Editor Loader: " + queuedDefinition);

                    // Clear the previously loaded new editor (if any).
                    if (newEditor != null)
                    {
                        newEditor.removeDefinitionEditorListener(editorListener);
                        newEditor.stopComponent();
                        newEditor = null;
                    }

                    // Get the next definition to load.
                    definitionToLoad = queuedDefinition;
                    if (definitionToLoad != null)
                    {
                        // Load the editor.
                        try
                        {
                            // Get the definition-specific editor.
                            newEditor = definitionToLoad.getDefinitionEditor(definitionContext);

                            // If not editor wass supplied by the definition, use the default.
                            if (newEditor == null) newEditor = new T8DefaultDefinitionEditor(definitionContext, definitionToLoad, true);
                        }
                        catch (Exception e)
                        {
                            T8Log.log("Exception while creating definition editor for definition: " + definitionToLoad, e);
                            newEditor = new T8DefaultDefinitionEditor(definitionContext, definitionToLoad, true);
                        }

                        // Initialize the new definition editor.
                        T8Log.log("Initializing definition edit: " + definitionToLoad);
                        newEditor.initializeComponent();
                    }
                }

                // Return null.
                return null;
            }
            finally
            {
                // Set the completed flag.
                synchronized(T8DefinitionEditorView.this)
                {
                    loader = null;
                }

                // Reset the Swing Worker thread name, so that we no it is no longer in use for this application.
                Thread.currentThread().setName(threadNameBackup);
            }
        }

        @Override
        public void done()
        {
            // Update the UI with the new editor.
            synchronized (T8DefinitionEditorView.this)
            {
                // Stop the current editor (if any).
                if (currentEditor != null)
                {
                    currentEditor.removeDefinitionEditorListener(editorListener);
                    currentEditor.commitChanges();
                    currentEditor.stopComponent();
                }

                // Set the new editor as the current editor.
                currentEditor = newEditor;
                jPanelEditor.removeAll();
                if (currentEditor != null)
                {
                    // Add the new editor to the UI.
                    jPanelEditor.add((Component)currentEditor, java.awt.BorderLayout.CENTER);
                    jPanelEditor.validate();

                    // Start the editor.
                    currentEditor.startComponent();
                    currentEditor.addDefinitionEditorListener(editorListener);
                }

                // Repaint the editor panel to reflect the changes.
                jPanelEditor.repaint();
            }

            // Show the loaded data.
            showEditorView();
            completed = true;
        }
    }

    private class DefinitionEditorListener implements T8DefinitionEditorListener
    {
        @Override
        public void definitionLinkActivated(T8DefinitionLinkEvent event)
        {
            notifyDefinitionLinkActivated(event);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jPanelEditor = new javax.swing.JPanel();

        jPanelEditor.setLayout(new java.awt.BorderLayout());

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelEditor;
    // End of variables declaration//GEN-END:variables

}
