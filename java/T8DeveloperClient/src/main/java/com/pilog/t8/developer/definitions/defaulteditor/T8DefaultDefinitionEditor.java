package com.pilog.t8.developer.definitions.defaulteditor;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.type.T8DataTypeUtilities;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.datatype.T8DtString.T8StringType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumDependency;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionDatumType.T8DefinitionDatumOptionType;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationProvider;
import com.pilog.t8.definition.documentation.T8DefinitionDocumentationType;
import com.pilog.t8.definition.event.T8DefinitionChangeListener;
import com.pilog.t8.definition.event.T8DefinitionDatumChangeEvent;
import com.pilog.t8.definition.event.T8DefinitionDescendentDatumChangeEvent;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.T8DefinitionEditor;
import com.pilog.t8.ui.event.T8DefinitionDatumEditorListener;
import com.pilog.t8.ui.event.T8DefinitionEditorListener;
import com.pilog.t8.ui.event.T8DefinitionLinkEvent;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.T8DefinitionTypeDefinition;
import com.pilog.t8.definition.editor.T8DefinitionEditorSetupDefinition;
import com.pilog.t8.definition.ui.datumeditor.T8DefinitionDatumEditorDefinition;
import com.pilog.t8.developer.definitions.T8DefinitionDocumentationFrame;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.BooleanDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ColorHexDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DateDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DateTimeDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ExpressionDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.IdentifierExpressionMappingDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.IdentifierMapDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ImageDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.JSONDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.JavaDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.JavaScriptDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ListDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.MapDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.PasswordDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ScriptDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SingleValueDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.SubDefinitionDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.ValueListDatumEditor;
import com.pilog.t8.developer.definitions.defaulteditor.datumeditor.XmlDatumEditor;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.event.EventListenerList;
import org.jdesktop.swingx.JXTaskPane;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDefinitionEditor extends JPanel implements T8DefinitionEditor
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefinitionContext definitionContext;
    private final T8DefinitionManager definitionManager;
    private final ArrayList<T8DefinitionDatumEditor> datumEditors;
    private final T8Definition definition;
    private final T8DefinitionChangeListener definitionChangeListener;
    private T8DefinitionTypeDefinition definitionTypeDefinition;
    private T8DefinitionEditorSetupDefinition definitionEditorSetupDefinition;
    private final T8DefinitionDatumEditorListener datumEditorListener;
    private final EventListenerList eventListeners;
    private boolean editable;

    private static boolean definitionDetailsVisible = false;

    public T8DefaultDefinitionEditor(T8DefinitionContext definitionContext, T8Definition editedDefinition, boolean scrollable)
    {
        initComponents();

        this.definition = editedDefinition;
        this.clientContext = definitionContext.getClientContext();
        this.context = definitionContext.getContext();
        this.definitionContext = definitionContext;
        this.definitionManager = clientContext.getDefinitionManager();
        this.datumEditors = new ArrayList<T8DefinitionDatumEditor>();
        this.datumEditorListener = new DatumEditorListener();
        this.eventListeners = new EventListenerList();
        this.definitionChangeListener = new DefinitionChangeListener();
        this.jToggleButtonDefinitionDetails.setSelected(definitionDetailsVisible);
        this.setEditable(!editedDefinition.getRootDefinition().isLocked());

        if (scrollable)
        {
            JScrollPane jScrollPane = new JScrollPane(jPanelDefinitionContent);
            jScrollPane.getVerticalScrollBar().setUnitIncrement(20); // Increase scroll speed.
            this.jPanelContent.add(jScrollPane, java.awt.BorderLayout.CENTER);
        }
        else
        {
            jXTaskPaneContainerDatumEditors.setBackgroundPainter(null);
            jXTaskPaneContainerDatumEditors.setBackground(new Color(240, 240, 240));
            this.jPanelContent.add(jPanelDefinitionContent, java.awt.BorderLayout.CENTER);
        }
    }

    public T8Definition getDefinition()
    {
        return definition;
    }

    public String getDefinitionIdentifier()
    {
        return definition.getIdentifier();
    }

    public final void setEditable(boolean editable)
    {
        this.editable = editable;
        for (T8DefinitionDatumEditor datumEditor : datumEditors)
        {
            datumEditor.setEditable(editable);
        }
    }

    @Override
    public void setDefinitionDetailsVisible(boolean identifierVisible, boolean displayNameVisible, boolean descriptionVisible)
    {
        if (!identifierVisible && !displayNameVisible && !descriptionVisible)
        {
            jXTaskPaneDefinitionDetails.setVisible(false);
        }
        else
        {
            jXTaskPaneDefinitionDetails.setVisible(true);

            jLabelDefinitionIdentifier.setVisible(identifierVisible);
            jTextFieldDefinitionIdentifier.setVisible(identifierVisible);

            jLabelDefinitionDisplayName.setVisible(identifierVisible);
            jTextFieldDefinitionDisplayName.setVisible(identifierVisible);

            jLabelDefinitionDescription.setVisible(identifierVisible);
            jTextAreaDefinitionDescription.setVisible(identifierVisible);
        }
    }

    @Override
    public void initializeComponent()
    {
        // Log the start of the editor.
        T8Log.log("Initializing definition editor: " + definition);

        // Stop all datum editors.
        for (T8DefinitionDatumEditor datumEditor : datumEditors)
        {
            datumEditor.stopComponent();
        }

        // Clear the UI containers.
        datumEditors.clear();
        jToolBarCustom.removeAll();
        jXTaskPaneContainerDatumEditors.removeAll();

        // Only populate the containers if the edited definition is actually set.
        if (definition != null)
        {
            T8DefinitionTypeMetaData typeMetaData;

            // Get the definition type meta data.
            typeMetaData = definition.getTypeMetaData();

            try
            {
                definitionTypeDefinition = (T8DefinitionTypeDefinition)definitionContext.getInitializedDefinition(definition.getRootProjectId(), typeMetaData.getTypeId(), null);
                if (definitionTypeDefinition != null)
                {
                    String definitionEditorSetupIdentifier;

                    definitionEditorSetupIdentifier = definitionTypeDefinition.getDefinitionEditorSetupIdentifier();
                    if (definitionEditorSetupIdentifier != null)
                    {
                        definitionEditorSetupDefinition = (T8DefinitionEditorSetupDefinition)definitionManager.getInitializedDefinition(context, definition.getRootProjectId(), definitionEditorSetupIdentifier, null);
                    }
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while trying to load definition type and editor setup definition: " + typeMetaData.getTypeId(), e);
            }

            // Set the definition meta data information.
            setBorder(new T8ContentHeaderBorder(typeMetaData.getDisplayName() + " Setup: " + definition.getIdentifier()));
            jTextFieldDefinitionIdentifier.setText(definition.getIdentifier());
            jTextFieldDefinitionDisplayName.setText(definition.getMetaDisplayName());
            jTextAreaDefinitionDescription.setText(definition.getMetaDescription());
            jTextFieldVersionIdentifier.setText(definition.getVersionId());
            jTextFieldProjectIdentifier.setText(definition.getProjectIdentifier());
            jXTaskPaneDefinitionDetails.setTitle(typeMetaData.getDisplayName() + " Definition Details");
            jXTaskPaneDefinitionDetails.setVisible(definitionDetailsVisible);
            jXTaskPaneContainerDatumEditors.add(jXTaskPaneDefinitionDetails);

            // Add all the definition actions.
            addDefinitionActions();

            // Add all the datum editors.
            addDatumEditors();

            // Initialize all datum editors.
            for (T8DefinitionDatumEditor datumEditor : datumEditors)
            {
                datumEditor.initializeComponent();
            }

            // Add a change listener to the definition so that the UI can be updated when changes occur.
            definition.addChangeListener(definitionChangeListener);
        }
        else throw new RuntimeException("Invalid component definition.");
    }

    @Override
    public void startComponent()
    {
        // Start all datum editors.
        T8Log.log("Starting definition editor: " + definition);
        for (T8DefinitionDatumEditor datumEditor : datumEditors)
        {
            datumEditor.startComponent();
        }
    }

    @Override
    public void stopComponent()
    {
        // Start all datum editors.
        T8Log.log("Stopping definition editor: " + definition);
        for (T8DefinitionDatumEditor datumEditor : datumEditors)
        {
            datumEditor.stopComponent();
        }
    }

    @Override
    public void commitChanges()
    {
        if (definition != null)
        {
            // Log the stopping of the editor.
            T8Log.log("Committing changes on definition editor: " + definition);

            // Clean up the definition listeners and set the meta data changes.
            definition.removeChangeListener(definitionChangeListener);
            definition.setMetaData(jTextFieldDefinitionDisplayName.getText(), jTextAreaDefinitionDescription.getText());

            // Stop all datum editors.
            for (T8DefinitionDatumEditor datumEditor : datumEditors)
            {
                datumEditor.commitChanges();
            }
        }
    }

    @Override
    public void refreshEditor()
    {
        T8Log.log("Refreshing Definition Editor...");
        for (T8DefinitionDatumEditor datumEditor : datumEditors)
        {
            datumEditor.refreshEditor();
        }
    }

    public void addSystemAction(Action action)
    {
        JButton actionButton;

        actionButton = new JButton(action);
        actionButton.setFocusable(false);
        actionButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        actionButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        actionButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBarSystem.add(actionButton);
    }

    private void addDefinitionActions()
    {
        T8DefinitionActionHandler actionHandler;

        actionHandler = definition.getDefinitionActionHandler(context);
        if (actionHandler != null)
        {
            List<Action> actions;

            actions = actionHandler.getDefinitionEditorActions();
            if (actions != null)
            {
                for (Action action : actions)
                {
                    JButton actionButton;

                    actionButton = new JButton(action);
                    actionButton.setFocusable(false);
                    actionButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                    actionButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
                    actionButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
                    jToolBarCustom.add(actionButton);
                }
            }
        }
    }

    private void addDatumEditors()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = definition.getDatumTypes();
        for (int datumIndex = 0; datumIndex < datumTypes.size(); datumIndex++)
        {
            T8DefinitionDatumType datumType;

            // Add the datum if it is visible (not hidden).
            datumType = datumTypes.get(datumIndex);
            if (!datumType.isHidden())
            {
                try
                {
                    T8DefinitionDatumEditor datumEditor;

                    datumEditor = null;

                    // 1.  First try to get a specific datum editor from the definition editor setup (if available).
                    if (definitionEditorSetupDefinition != null)
                    {
                        T8DefinitionDatumEditorDefinition datumEditorDefinition;

                        datumEditorDefinition = definitionEditorSetupDefinition.getDefinitionDatumEditorDefinition(datumType.getIdentifier());
                        if (datumEditorDefinition != null)
                        {
                            datumEditor = datumEditorDefinition.getNewDatumEditorInstance(definitionContext, definition, datumType);
                        }
                    }

                    // 2.  If no datum editor has been instantiated, try to get an editor from the definition itself.
                    if (datumEditor == null) datumEditor = definition.getDatumEditor(definitionContext, datumType.getIdentifier());

                    // 3.  If no datum editor was supplied by the definition, create default editor for the datum.
                    if (datumEditor == null) datumEditor = getDefaultDatumEditor(definitionContext, definition, datumType);

                    // Add the datum editor to the editor pane (if a datum editor was created successfully).
                    if (datumEditor != null)
                    {
                        JXTaskPane datumEditorPane;

                        // Configure the datum editor.
                        datumEditor.setEditable(editable);

                        // Add the datum editor to the UI.
                        datumEditors.add(datumEditor);
                        datumEditorPane = new JXTaskPane(datumType.getDisplayName());
                        datumEditorPane.setOpaque(false);
                        datumEditorPane.add((Component)datumEditor);
                        jXTaskPaneContainerDatumEditors.add(datumEditorPane);

                        // Add the datum editor listener.
                        datumEditor.addDefinitionDatumEditorListener(datumEditorListener);
                    }
                }
                catch (Exception e)
                {
                    T8Log.log("Default datum editor could not be successfully constructed for datum type: " + datumType.getIdentifier(), e);
                }
            }
        }
    }

    private void showDocumentationPopupMenu(JComponent source, Point location)
    {
        final T8DefinitionDocumentationProvider documentationProvider;
        JPopupMenu popupMenu;
        JMenuItem menuItem;

        // Get the documentation provider from the definition.
        documentationProvider = definition.createDocumentationProvider(definitionContext);

        // Create the ActionListener for the menu items.
        ActionListener menuListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                JMenuItem menuItem;
                String identifier;

                menuItem = (JMenuItem)event.getSource();
                identifier = menuItem.getName();
                if (identifier != null)
                {
                    T8DefinitionDocumentationFrame documentationFrame;

                    documentationFrame = new T8DefinitionDocumentationFrame(context, (List)ArrayLists.newArrayList(definition), identifier);
                    documentationFrame.setVisible(true);
                }
            }
        };

        // Create a new popup menu.
        popupMenu = new JPopupMenu();
        for (T8DefinitionDocumentationType documentationType : documentationProvider.getDocumentationTypes())
        {
            menuItem = new JMenuItem(documentationType.getDisplayName());
            menuItem.setName(documentationType.getIdentifier());
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);
        }

        // Display the newly constructed popup menu.
        popupMenu.show(source, location.x, location.y);
    }

    public static T8DefinitionDatumEditor getDefaultDatumEditor(T8DefinitionContext definitionContext, T8Definition definition, T8DefinitionDatumType datumType) throws Exception
    {
        T8DefinitionDatumOptionType optionType;
        T8DataType dataType;

        dataType = datumType.getDataType();
        optionType = datumType.getOptionType();
        if (dataType.equals(T8DataType.BOOLEAN))
        {
            return new BooleanDatumEditor(definitionContext, definition, datumType);
        }
        else if (dataType.equals(T8DataType.DATE_TIME))
        {
            return new DateTimeDatumEditor(definitionContext, definition, datumType);
        }
        else if (dataType.equals(T8DataType.DATE))
        {
            return new DateDatumEditor(definitionContext, definition, datumType);
        }
        else if (dataType.equals(T8DataType.DEFINITION))
        {
            return new SubDefinitionDatumEditor(definitionContext, definition, datumType);
        }
        else if ((dataType.isType(T8DataType.LIST)) && (T8DataTypeUtilities.matchesAnyDataType(dataType, true, T8DataType.DEFINITION_LIST)))
        {
            return null;
        }
        else if (dataType.equals(T8DataType.CUSTOM_OBJECT))
        {
            return null;
        }
        else if (dataType.equals(T8DataType.DOUBLE))
        {
            return new SingleValueDatumEditor(definitionContext, definition, datumType);
        }
        else if (dataType.equals(T8DataType.FLOAT))
        {
            return new SingleValueDatumEditor(definitionContext, definition, datumType);
        }
        else if (dataType.equals(T8DataType.INTEGER) || dataType.equals(T8DataType.LONG))
        {
            if (optionType == T8DefinitionDatumOptionType.ENUMERATION)
            {
                return new ValueListDatumEditor(definitionContext, definition, datumType);
            } else return new SingleValueDatumEditor(definitionContext, definition, datumType);
        }
        else if ((dataType.isType(T8DataType.LIST)) || (T8DataTypeUtilities.matchesAnyDataType(dataType, true, T8DataType.DEFINITION_IDENTIFIER_LIST,T8DataType.DEFINITION_GROUP_IDENTIFIER_LIST,T8DataType.EPIC_EXPRESSION_LIST)))
        {
            return new ListDatumEditor(definitionContext, definition, datumType);
        }
        else if (dataType == T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP)
        {
            return new IdentifierExpressionMappingDatumEditor(definitionContext, definition, datumType);
        }
        else if (T8DataTypeUtilities.matchesAnyDataType(dataType, true, T8DataType.DEFINITION_IDENTIFIER_MAP, T8DataType.DEFINITION_IDENTIFIER_STRING_MAP, T8DataType.DEFINITION_IDENTIFIER_KEY_MAP, T8DataType.DEFINITION_IDENTIFIER_VALUE_MAP))
        {
            return new IdentifierMapDatumEditor(definitionContext, definition, datumType);
        }
        else if (dataType.isType(T8DtString.IDENTIFIER))
        {
            T8StringType stringType;

            stringType = ((T8DtString)dataType).getType();
            switch (stringType)
            {
                case EPIC_EXPRESSION:
                {
                    ExpressionDatumEditor editor;

                    editor = new ExpressionDatumEditor(definitionContext, definition, datumType);
                    editor.setEditorHeight(80);
                    return editor;
                }
                case EPIC_SCRIPT:
                {
                    return new ScriptDatumEditor(definitionContext, definition, datumType);
                }
                case JAVA:
                {
                    return new JavaDatumEditor(definitionContext, definition, datumType);
                }
                case JAVA_SCRIPT:
                {
                    return new JavaScriptDatumEditor(definitionContext, definition, datumType);
                }
                case JSON:
                {
                    return new JSONDatumEditor(definitionContext, definition, datumType);
                }
                case CSS:
                {
                    return new JavaScriptDatumEditor(definitionContext, definition, datumType);
                }
                case XML:
                {
                    return new XmlDatumEditor(definitionContext, definition, datumType);
                }
                case TRANSLATED_STRING:
                {
                    return new SingleValueDatumEditor(definitionContext, definition, datumType);
                }
                case PASSWORD:
                {
                    return new PasswordDatumEditor(definitionContext, definition, datumType);
                }
                case COLOR_HEX:
                {
                    return new ColorHexDatumEditor(definitionContext, definition, datumType);
                }
                case GUID:
                {
                    return new SingleValueDatumEditor(definitionContext, definition, datumType);
                }
                case DEFINITION_IDENTIFIER:
                {
                    return new ValueListDatumEditor(definitionContext, definition, datumType);
                }
                case DEFINITION_TYPE_IDENTIFIER:
                {
                    return new ValueListDatumEditor(definitionContext, definition, datumType);
                }
                case DEFINITION_GROUP_IDENTIFIER:
                {
                    return new ValueListDatumEditor(definitionContext, definition, datumType);
                }
                case DEFINITION_INSTANCE_IDENTIFIER:
                {
                    return new ValueListDatumEditor(definitionContext, definition, datumType);
                }
                default:
                {
                    if (optionType == T8DefinitionDatumOptionType.ENUMERATION)
                    {
                        return new ValueListDatumEditor(definitionContext, definition, datumType);
                    }
                    else
                    {
                        return new SingleValueDatumEditor(definitionContext, definition, datumType);
                    }
                }
            }
        }
        else if (dataType.isType(T8DtMap.IDENTIFIER))
        {
            return new MapDatumEditor(definitionContext, definition, datumType);
        }
        else if (dataType.equals(T8DataType.IMAGE))
        {
            return new ImageDatumEditor(definitionContext, definition, datumType);
        }
        else throw new RuntimeException("Unsupported T8DataType encountered: " + dataType);
    }

    protected void fireDefinitionLinkActivatedEvent(String projectId, String definitionId)
    {
        T8DefinitionLinkEvent event;

        event = new T8DefinitionLinkEvent(this, projectId, definitionId);
        for (T8DefinitionEditorListener listener : eventListeners.getListeners(T8DefinitionEditorListener.class))
        {
            listener.definitionLinkActivated(event);
        }
    }

    @Override
    public void addDefinitionEditorListener(T8DefinitionEditorListener listener)
    {
        eventListeners.add(T8DefinitionEditorListener.class, listener);
    }

    @Override
    public void removeDefinitionEditorListener(T8DefinitionEditorListener listener)
    {
        eventListeners.remove(T8DefinitionEditorListener.class, listener);
    }

    private void handleDatumChange(T8Definition changedDefinition, T8DefinitionDatumType changedDatum, Object actor)
    {
        List<T8DefinitionDatumDependency> dependencies;

        // Log the event.
        T8Log.log("Handling datum change event on datum '" + changedDatum + "' in definition: " + changedDefinition);

        // Handle editor updates from the underlying definition.
        if (changedDefinition == definition)
        {
            for (T8DefinitionDatumEditor datumEditor : datumEditors)
            {
                if (actor != datumEditor)
                {
                    if (changedDatum.getIdentifier().equals(datumEditor.getDatumIdentifier()))
                    {
                        T8Log.log("Refreshing Datum Editor " + datumEditor.getDatumIdentifier() + " on editor for definition: " + definition + "...");
                        datumEditor.refreshEditor();
                    }
                }
            }
        }
    }

    private void handleDatumDependencyChange(T8Definition changedDefinition, T8DefinitionDatumType changedDatum, Object actor)
    {
        List<T8DefinitionDatumDependency> dependencies;

        // Log the event.
        T8Log.log("Handling datum dependency change event on datum '" + changedDatum + "' in definition: " + changedDefinition);

        // Handle dependencies.
        dependencies = changedDatum.getDatumDependencies();
        for (T8DefinitionDatumDependency dependency : dependencies)
        {
            String dependencyTypeIdentifier;
            String dependencyDatumIdentifier;
            String typeIdentifier;

            System.out.println("Checking dependency '" + dependency + "' in definition editor: " + definition);
            typeIdentifier = definition.getTypeMetaData().getTypeId();
            dependencyTypeIdentifier = dependency.getDefinitionTypeIdentifier();
            dependencyDatumIdentifier = dependency.getDatumIdentifier();
            if (typeIdentifier.equals(dependencyTypeIdentifier))
            {
                for (T8DefinitionDatumEditor datumEditor : datumEditors)
                {
                    if (actor != datumEditor)
                    {
                        if ((dependencyDatumIdentifier == null) || (dependencyDatumIdentifier.equals(datumEditor.getDatumIdentifier())))
                        {
                            T8Log.log("Refreshing Dependent Datum Editor " + dependencyDatumIdentifier + " on editor for definition: " + definition + "...");
                            datumEditor.refreshEditor();
                        }
                    }
                }
            }
        }
    }

    private class DatumEditorListener implements T8DefinitionDatumEditorListener
    {
        @Override
        public void definitionLinkActivated(T8DefinitionLinkEvent event)
        {
            fireDefinitionLinkActivatedEvent(event.getProjectId(), event.getDefinitionId());
        }
    }

    private class DefinitionChangeListener implements T8DefinitionChangeListener
    {
        @Override
        public void datumChanged(T8DefinitionDatumChangeEvent event)
        {
            handleDatumChange(event.getDefinition(), event.getDatumType(), event.getActor());
        }

        @Override
        public void descendentDatumChanged(T8DefinitionDescendentDatumChangeEvent event)
        {
            T8Definition changedDefinition;
            T8DefinitionDatumType changedDatum;

            // Handle the original definition change.
            changedDefinition = event.getDescendentDefinition();
            changedDatum = event.getDatumType();
            handleDatumChange(changedDefinition, changedDatum, event.getActor());

            // Step through the definitions path and handle a change on each one.
            for (Pair<T8Definition, T8DefinitionDatumType> pathStep : changedDefinition.getDefinitionPath())
            {
                changedDefinition = pathStep.getValue1();
                changedDatum = pathStep.getValue2();
                if (changedDatum != null) // The last step in the path will not have a datum type (but we've already checked that step).
                {
                    handleDatumDependencyChange(changedDefinition, changedDatum, event.getActor());
                }
            }
        }

        @Override
        public void metaDataChanged(com.pilog.t8.definition.event.T8DefinitionMetaDataChangeEvent tdmdce)
        {
        }

        @Override
        public void descendentMetaDataChanged(com.pilog.t8.definition.event.T8DefinitionDescendentMetaDataChangeEvent tdmdc)
        {
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelDefinitionContent = new javax.swing.JPanel();
        jXTaskPaneContainerDatumEditors = new org.jdesktop.swingx.JXTaskPaneContainer();
        jXTaskPaneDefinitionDetails = new org.jdesktop.swingx.JXTaskPane();
        jPanelDefinitionDetails = new javax.swing.JPanel();
        jLabelDefinitionIdentifier = new javax.swing.JLabel();
        jTextFieldDefinitionIdentifier = new javax.swing.JTextField();
        jLabelDefinitionDisplayName = new javax.swing.JLabel();
        jTextFieldDefinitionDisplayName = new javax.swing.JTextField();
        jLabelDefinitionDescription = new javax.swing.JLabel();
        jTextAreaDefinitionDescription = new javax.swing.JTextArea();
        jLabelProject = new javax.swing.JLabel();
        jTextFieldProjectIdentifier = new javax.swing.JTextField();
        jLabelSystem = new javax.swing.JLabel();
        jTextFieldVersionIdentifier = new javax.swing.JTextField();
        jToolBarSystem = new javax.swing.JToolBar();
        jButtonDocumentation = new javax.swing.JButton();
        jToggleButtonDefinitionDetails = new javax.swing.JToggleButton();
        jToolBarCustom = new javax.swing.JToolBar();
        jPanelContent = new javax.swing.JPanel();

        jPanelDefinitionContent.setLayout(new java.awt.GridBagLayout());

        org.jdesktop.swingx.VerticalLayout verticalLayout1 = new org.jdesktop.swingx.VerticalLayout();
        verticalLayout1.setGap(14);
        jXTaskPaneContainerDatumEditors.setLayout(verticalLayout1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelDefinitionContent.add(jXTaskPaneContainerDatumEditors, gridBagConstraints);

        jXTaskPaneDefinitionDetails.setTitle("Definition Details");

        jPanelDefinitionDetails.setLayout(new java.awt.GridBagLayout());

        jLabelDefinitionIdentifier.setText("Identifier:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelDefinitionDetails.add(jLabelDefinitionIdentifier, gridBagConstraints);

        jTextFieldDefinitionIdentifier.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelDefinitionDetails.add(jTextFieldDefinitionIdentifier, gridBagConstraints);

        jLabelDefinitionDisplayName.setText("Display Name:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelDefinitionDetails.add(jLabelDefinitionDisplayName, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelDefinitionDetails.add(jTextFieldDefinitionDisplayName, gridBagConstraints);

        jLabelDefinitionDescription.setText("Description:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelDefinitionDetails.add(jLabelDefinitionDescription, gridBagConstraints);

        jTextAreaDefinitionDescription.setColumns(20);
        jTextAreaDefinitionDescription.setLineWrap(true);
        jTextAreaDefinitionDescription.setRows(1);
        jTextAreaDefinitionDescription.setWrapStyleWord(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelDefinitionDetails.add(jTextAreaDefinitionDescription, gridBagConstraints);

        jLabelProject.setText("Project:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelDefinitionDetails.add(jLabelProject, gridBagConstraints);

        jTextFieldProjectIdentifier.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelDefinitionDetails.add(jTextFieldProjectIdentifier, gridBagConstraints);

        jLabelSystem.setText("System:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 5);
        jPanelDefinitionDetails.add(jLabelSystem, gridBagConstraints);

        jTextFieldVersionIdentifier.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 5);
        jPanelDefinitionDetails.add(jTextFieldVersionIdentifier, gridBagConstraints);

        jXTaskPaneDefinitionDetails.add(jPanelDefinitionDetails, java.awt.BorderLayout.CENTER);

        setLayout(new java.awt.GridBagLayout());

        jToolBarSystem.setFloatable(false);
        jToolBarSystem.setRollover(true);

        jButtonDocumentation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/informationIcon.png"))); // NOI18N
        jButtonDocumentation.setToolTipText("Definition Documentation");
        jButtonDocumentation.setFocusable(false);
        jButtonDocumentation.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonDocumentation.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonDocumentation.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDocumentation.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDocumentationActionPerformed(evt);
            }
        });
        jToolBarSystem.add(jButtonDocumentation);

        jToggleButtonDefinitionDetails.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/detailTagIcon.png"))); // NOI18N
        jToggleButtonDefinitionDetails.setToolTipText("Toggle definition details.");
        jToggleButtonDefinitionDetails.setFocusable(false);
        jToggleButtonDefinitionDetails.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jToggleButtonDefinitionDetails.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButtonDefinitionDetails.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jToggleButtonDefinitionDetailsActionPerformed(evt);
            }
        });
        jToolBarSystem.add(jToggleButtonDefinitionDetails);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jToolBarSystem, gridBagConstraints);

        jToolBarCustom.setFloatable(false);
        jToolBarCustom.setRollover(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarCustom, gridBagConstraints);

        jPanelContent.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelContent, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonDocumentationActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDocumentationActionPerformed
    {//GEN-HEADEREND:event_jButtonDocumentationActionPerformed
        showDocumentationPopupMenu(jButtonDocumentation, new Point(0, jButtonDocumentation.getHeight()));
    }//GEN-LAST:event_jButtonDocumentationActionPerformed

    private void jToggleButtonDefinitionDetailsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jToggleButtonDefinitionDetailsActionPerformed
    {//GEN-HEADEREND:event_jToggleButtonDefinitionDetailsActionPerformed
        definitionDetailsVisible = jToggleButtonDefinitionDetails.isSelected();
        this.setDefinitionDetailsVisible(definitionDetailsVisible, definitionDetailsVisible, definitionDetailsVisible);
    }//GEN-LAST:event_jToggleButtonDefinitionDetailsActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDocumentation;
    private javax.swing.JLabel jLabelDefinitionDescription;
    private javax.swing.JLabel jLabelDefinitionDisplayName;
    private javax.swing.JLabel jLabelDefinitionIdentifier;
    private javax.swing.JLabel jLabelProject;
    private javax.swing.JLabel jLabelSystem;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelDefinitionContent;
    private javax.swing.JPanel jPanelDefinitionDetails;
    private javax.swing.JTextArea jTextAreaDefinitionDescription;
    private javax.swing.JTextField jTextFieldDefinitionDisplayName;
    private javax.swing.JTextField jTextFieldDefinitionIdentifier;
    private javax.swing.JTextField jTextFieldProjectIdentifier;
    private javax.swing.JTextField jTextFieldVersionIdentifier;
    private javax.swing.JToggleButton jToggleButtonDefinitionDetails;
    private javax.swing.JToolBar jToolBarCustom;
    private javax.swing.JToolBar jToolBarSystem;
    private org.jdesktop.swingx.JXTaskPaneContainer jXTaskPaneContainerDatumEditors;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneDefinitionDetails;
    // End of variables declaration//GEN-END:variables

}
