package com.pilog.t8.developer.session;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.ui.event.T8DefinitionViewListener;
import com.pilog.t8.ui.T8DeveloperView;
import com.pilog.t8.ui.T8DeveloperViewFactory;
import com.pilog.t8.developer.view.T8DefaultDeveloperView;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8SessionDetails;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8SessionMonitor extends T8DefaultDeveloperView implements T8DeveloperView
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DeveloperView parentView;
    private final T8SecurityManager securityManager;
    private T8DeveloperViewFactory viewFactory;
    private final T8SessionDetailsPanel sessionDetailsPanel;

    public T8SessionMonitor(T8Context context, T8DeveloperView parentView)
    {
        initComponents();
        this.clientContext = context.getClientContext();
        this.context = context;
        this.securityManager = clientContext.getSecurityManager();
        this.parentView = parentView;
        this.viewFactory = parentView.getViewFactory();
        this.sessionDetailsPanel = new T8SessionDetailsPanel(context, parentView);
        this.add(sessionDetailsPanel, java.awt.BorderLayout.CENTER);
    }

    @Override
    public String getHeader()
    {
        return "Session Monitor";
    }

    @Override
    public void addView(T8DeveloperView view)
    {
        parentView.addView(view);
    }

    @Override
    public void removeView(T8DeveloperView view)
    {
        parentView.removeView(view);
    }

    @Override
    public T8DeveloperViewFactory getViewFactory()
    {
        return viewFactory;
    }

    @Override
    public void setViewFactory(T8DeveloperViewFactory viewFactory)
    {
        this.viewFactory = viewFactory;
    }

    @Override
    public boolean canClose()
    {
        return true;
    }

    private void refreshData()
    {
        // Refresh the flow status data.
        sessionDetailsPanel.refreshData();
    }

    @Override
    public T8Definition getSelectedDefinition()
    {
        return null;
    }

    @Override
    public void setSelectedDefinition(T8Definition selectedDefinition)
    {
    }

    @Override
    public void addDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    @Override
    public void removeDefinitionViewListener(T8DefinitionViewListener tl)
    {
    }

    private void enablePerformanceStatistics()
    {
        T8SessionDetails selectedDetails;

        selectedDetails = sessionDetailsPanel.getSelectedSessionDetails();
        if (selectedDetails != null)
        {
            try
            {
                String sessionId;

                sessionId = selectedDetails.getSessionId();
                securityManager.setSessionPerformanceStatisticsEnabled(context, sessionId, true);
                refreshData();
            }
            catch (Exception e)
            {
                T8Log.log("Exception while enabling session performance statistics: " + selectedDetails.getSessionId(), e);
                Toast.show("Operation Failure", Toast.Style.ERROR);
            }
        }
        else Toast.show("No Session Selected", Toast.Style.ERROR);
    }

    private void disablePerformanceStatistics()
    {
        T8SessionDetails selectedDetails;

        selectedDetails = sessionDetailsPanel.getSelectedSessionDetails();
        if (selectedDetails != null)
        {
            try
            {
                String sessionId;

                sessionId = selectedDetails.getSessionId();
                securityManager.setSessionPerformanceStatisticsEnabled(context, sessionId, false);
                refreshData();
            }
            catch (Exception e)
            {
                T8Log.log("Exception while disabling session performance statistics: " + selectedDetails.getSessionId(), e);
                Toast.show("Operation Failure", Toast.Style.ERROR);
            }
        }
        else Toast.show("No Session Selected", Toast.Style.ERROR);
    }

    private void getPerformanceStatistics()
    {
        T8SessionDetails selectedDetails;

        selectedDetails = sessionDetailsPanel.getSelectedSessionDetails();
        if (selectedDetails != null)
        {
            try
            {
                T8PerformanceStatistics stats;
                String sessionId;

                sessionId = selectedDetails.getSessionId();
                stats = securityManager.getSessionPerformanceStatistics(context, sessionId);
                T8PerformanceStatisticsReportDialog.showPerformanceStatisticsReport(SwingUtilities.getWindowAncestor(this), stats);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while enabling session performance statistics: " + selectedDetails.getSessionId(), e);
                Toast.show("Operation Failure", Toast.Style.ERROR);
            }
        }
        else Toast.show("No Session Selected", Toast.Style.ERROR);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jButtonEnabledPerformanceStatistics = new javax.swing.JButton();
        jButtonDisablePerformanceStatistics = new javax.swing.JButton();
        jButtonGetPerformanceStatistics = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        jButtonEnabledPerformanceStatistics.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/chartPlusIcon.png"))); // NOI18N
        jButtonEnabledPerformanceStatistics.setToolTipText("Enable Performance Tracking");
        jButtonEnabledPerformanceStatistics.setFocusable(false);
        jButtonEnabledPerformanceStatistics.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonEnabledPerformanceStatistics.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonEnabledPerformanceStatistics.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonEnabledPerformanceStatisticsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonEnabledPerformanceStatistics);

        jButtonDisablePerformanceStatistics.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/chartMinusIcon.png"))); // NOI18N
        jButtonDisablePerformanceStatistics.setToolTipText("Disable Performance Tracking");
        jButtonDisablePerformanceStatistics.setFocusable(false);
        jButtonDisablePerformanceStatistics.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonDisablePerformanceStatistics.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDisablePerformanceStatistics.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDisablePerformanceStatisticsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDisablePerformanceStatistics);

        jButtonGetPerformanceStatistics.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/chartArrowIcon.png"))); // NOI18N
        jButtonGetPerformanceStatistics.setToolTipText("Get Performance Statistics");
        jButtonGetPerformanceStatistics.setFocusable(false);
        jButtonGetPerformanceStatistics.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonGetPerformanceStatistics.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonGetPerformanceStatistics.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonGetPerformanceStatisticsActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonGetPerformanceStatistics);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        refreshData();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jButtonEnabledPerformanceStatisticsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonEnabledPerformanceStatisticsActionPerformed
    {//GEN-HEADEREND:event_jButtonEnabledPerformanceStatisticsActionPerformed
        enablePerformanceStatistics();
    }//GEN-LAST:event_jButtonEnabledPerformanceStatisticsActionPerformed

    private void jButtonDisablePerformanceStatisticsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDisablePerformanceStatisticsActionPerformed
    {//GEN-HEADEREND:event_jButtonDisablePerformanceStatisticsActionPerformed
        disablePerformanceStatistics();
    }//GEN-LAST:event_jButtonDisablePerformanceStatisticsActionPerformed

    private void jButtonGetPerformanceStatisticsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonGetPerformanceStatisticsActionPerformed
    {//GEN-HEADEREND:event_jButtonGetPerformanceStatisticsActionPerformed
        getPerformanceStatistics();
    }//GEN-LAST:event_jButtonGetPerformanceStatisticsActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDisablePerformanceStatistics;
    private javax.swing.JButton jButtonEnabledPerformanceStatistics;
    private javax.swing.JButton jButtonGetPerformanceStatistics;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables


}
