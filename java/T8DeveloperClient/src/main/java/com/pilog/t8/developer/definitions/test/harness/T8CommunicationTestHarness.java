package com.pilog.t8.developer.definitions.test.harness;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.communication.T8CommunicationDefinition;
import com.pilog.t8.developer.definitions.test.T8CommunicationTestFrame;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8CommunicationTestHarness implements T8DefinitionTestHarness
{
    private String userIdentifier;
    private String userProfileIdentifier;
    private String username;
    private char[] userPassword;

    public T8CommunicationTestHarness()
    {
    }

    @Override
    public void configure(T8Context context)
    {
    }

    @Override
    public void testDefinition(T8Context context, T8Definition definition)
    {
        try
        {
            T8CommunicationDefinition communicationDefinition;

            communicationDefinition = (T8CommunicationDefinition)definition;
            T8CommunicationTestFrame.testOperationDefinition(context, communicationDefinition, this);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while testing task definition: " + definition, e);
        }
    }

    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    public void setUserIdentifier(String userIdentifier)
    {
        this.userIdentifier = userIdentifier;
    }

    public String getUserProfileIdentifier()
    {
        return userProfileIdentifier;
    }

    public void setUserProfileIdentifier(String userProfileIdentifier)
    {
        this.userProfileIdentifier = userProfileIdentifier;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public char[] getUserPassword()
    {
        return userPassword;
    }

    public void setUserPassword(char[] userPassword)
    {
        this.userPassword = userPassword;
    }
}
