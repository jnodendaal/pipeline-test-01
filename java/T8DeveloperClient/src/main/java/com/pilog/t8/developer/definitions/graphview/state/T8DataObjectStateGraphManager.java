package com.pilog.t8.developer.definitions.graphview.state;

import com.pilog.graph.model.GraphEdge.EdgeType;
import com.pilog.graph.model.GraphModel;
import com.pilog.graph.view.GraphVertexRenderer;
import com.pilog.t8.ui.T8GraphManager;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateGraphDefinition;
import com.pilog.t8.definition.graph.T8GraphDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Component;
import java.awt.Dimension;
import java.util.prefs.Preferences;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectStateGraphManager implements T8GraphManager
{
    public static final String EDGE_TYPE_PREFERENCE = "GRAPH_EDGE_TYPE_PREFERENCE";
    public static final String LAYOUT_TYPE_PREFERENCE = "GRAPH_LAYOUT_TYPE_PREFERENCE";

    private final T8Context context;
    private final T8StateVertexRenderer vertexRenderer;
    private final T8StateGraphConfiguration config;
    private final Preferences preferences;

    public T8DataObjectStateGraphManager(T8Context context)
    {
        this.preferences = Preferences.userNodeForPackage(T8DataObjectStateGraphManager.class);
        this.context = context;
        this.config = new T8StateGraphConfiguration();
        this.config.setEdgeType(EdgeType.valueOf(preferences.get(EDGE_TYPE_PREFERENCE, this.config.getEdgeType().toString())));
        this.vertexRenderer = new T8StateVertexRenderer(config);
    }

    @Override
    public GraphVertexRenderer getVertextRenderer()
    {
        return vertexRenderer;
    }

    @Override
    public GraphModel buildGraphModel(T8GraphDefinition graphDefinition)
    {
        T8StateGraphFreeLayoutManager layoutManager;

        layoutManager = new T8StateGraphFreeLayoutManager(vertexRenderer, config);
        return layoutManager.createGraphModel((T8DataObjectStateGraphDefinition)graphDefinition);
    }

    @Override
    public Dimension getPreferredSize()
    {
        return null;
    }

    @Override
    public Component getConfigurationComponent()
    {
        return new T8StateGraphConfigurationPanel(this, config);
    }
}
