package com.pilog.t8.developer;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;

import static com.pilog.t8.definition.data.T8DataManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DeveloperDataHandler
{
    public static final List<T8DataFieldDefinition> retrieveDataSourceFieldDefinitions(T8Context context, String id) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DATA_SOURCE_IDENTIFIER, id);
        return (List<T8DataFieldDefinition>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_RETRIEVE_DATA_SOURCE_FIELD_DEFINITIONS, operationParameters).get(PARAMETER_FIELD_DEFINITION_LIST);
    }
}
