package com.pilog.t8.developer.utils;

import com.pilog.t8.utilities.strings.Strings;
import javax.swing.ListModel;
import javax.swing.RowFilter;

/**
 * @author Hennie Brink
 */
public class ListRowFilter extends RowFilter<ListModel, Integer>
{
    private String filterString;

    @Override
    public boolean include(RowFilter.Entry<? extends ListModel, ? extends Integer> entry)
    {
        if (Strings.isNullOrEmpty(filterString))
        {
            return true;
        }

        if (!Strings.isNullOrEmpty(entry.getStringValue(0)) && entry.getStringValue(0).toUpperCase().contains(filterString.toUpperCase()))
        {
            return true;
        }


        return false;
    }

    public void setFilterString(String filterString)
    {
        this.filterString = filterString;
    }
}