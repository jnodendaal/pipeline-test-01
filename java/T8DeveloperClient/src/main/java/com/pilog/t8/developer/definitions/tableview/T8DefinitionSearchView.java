package com.pilog.t8.developer.definitions.tableview;

import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.ui.T8DeveloperView;

/**
 * Extends default list view to added specific feature required by search view.
 * 
 * @author Bouwer du Preez
 */
public class T8DefinitionSearchView extends T8DefinitionTableView
{
    public T8DefinitionSearchView(T8DefinitionContext definitionContext, T8DeveloperView parentView)
    {    
        super(definitionContext, parentView, null, true);
    }
    
    @Override
    public String getHeader()
    {
        return "Definition Search";
    }
}
