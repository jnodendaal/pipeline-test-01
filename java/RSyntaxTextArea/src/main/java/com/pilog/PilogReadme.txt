Changed files:
Changed the ParameterizedAutoCompletion window so that the options for the auto complete get called everytime
the autocomplete parameter changes. We did this so that regex expression can be used in order to display
different options based on a previous selected/entered parameter.