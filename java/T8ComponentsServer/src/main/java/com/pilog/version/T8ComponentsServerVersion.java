package com.pilog.version;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8ComponentsServerVersion
{
    public final static String VERSION = "45";
}
