package com.pilog.t8.ui.datasearch.combobox.server;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelGroupNode;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelItemNode;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelRootNode;
import com.pilog.t8.ui.datasearch.combobox.T8DataSearchComboBoxModelProvider;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.datasearch.combobox.datasource.entity.T8DataSearchComboBoxDataSourceEntityDefinition;
import com.pilog.t8.definition.ui.datasearch.combobox.datasource.entity.T8DataSearchComboBoxDataSourceEntityGroupNodeDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.util.List;
import java.util.Objects;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Hennie Brink
 */
public class T8DataRecordSearchComboBoxModelProviderEntity implements T8DataSearchComboBoxModelProvider
{
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8DataSearchComboBoxDataSourceEntityDefinition definition;
    private final ComboBoxModelRootNode rootNode;

    public T8DataRecordSearchComboBoxModelProviderEntity(T8Context context, T8DataSearchComboBoxDataSourceEntityDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
        this.rootNode = new ComboBoxModelRootNode();
        this.rootNode.setCompleteDataSet(true);
        this.rootNode.setOptionsVisible(definition.isOptionsVisible());
        this.rootNode.setSearchVisible(definition.isSearchVisible());
        this.rootNode.setSelectionMode(definition.getSelectionMode());
    }

    @Override
    public ComboBoxModelRootNode createRootNode(String searchQuery, int retrievalSize, T8DataFilter dataSourceFilter) throws Exception
    {
        T8DataTransaction tx;
        T8DataEntityResults scroll;
        int scrollCount;

        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        scroll = tx.scroll(definition.getDataEntityIdentifier(), createCombinedFilter(searchQuery, dataSourceFilter));

        scrollCount = 0;

        try
        {
            while (scroll.next())
            {
                T8DataEntity nextEntity;

                nextEntity = scroll.get();

                this.definition.getGroupNodeDefinitions().forEach((groupNodeDefinition)->processDataEntity(nextEntity, groupNodeDefinition));

                scrollCount++;

                if (scrollCount > retrievalSize)
                {
                    rootNode.setCompleteDataSet(false);
                    break;
                }
            }
        }
        finally
        {
            scroll.close();
        }

        if(!Strings.isNullOrEmpty(searchQuery)) //If the search query is set it is most likely never going to be a complete data set
        {
            rootNode.setCompleteDataSet(false);
        }

        return rootNode;
    }

    private T8DataFilter createCombinedFilter(String searchQuery, T8DataFilter dataSourceFilter)
    {
        T8DataFilter combinedFilter;
        T8DataFilter definitionFilter;
        List<T8DataFilterDefinition> filterDefinitions;

        filterDefinitions = definition.getPreFilters();
        combinedFilter = new T8DataFilter(definition.getDataEntityIdentifier());

        if (filterDefinitions != null && !filterDefinitions.isEmpty())
        {
            for (T8DataFilterDefinition filterDefinition : filterDefinitions)
            {
                definitionFilter = filterDefinition.getNewDataFilterInstance(context, null);

                combinedFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, definitionFilter);

                if (definitionFilter.hasFieldOrdering())
                {
                    combinedFilter.setFieldOrdering(definitionFilter.getFieldOrdering());
                }
            }
        }

        if (dataSourceFilter != null)
        {
            combinedFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, dataSourceFilter);
        }

        if (searchQuery != null)
        {
            T8DataFilterCriteria criteria;

            criteria = new T8DataFilterCriteria();
            for (T8DataSearchComboBoxDataSourceEntityGroupNodeDefinition groupNodeDefinition : definition.getGroupNodeDefinitions())
            {
                criteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.OR, groupNodeDefinition.getItemDisplayFieldIdentifier(), T8DataFilterCriterion.DataFilterOperator.LIKE, searchQuery, true);
            }
            combinedFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, criteria);
        }

        return combinedFilter;
    }

    private void processDataEntity(T8DataEntity entity, T8DataSearchComboBoxDataSourceEntityGroupNodeDefinition groupNodeDefinition)
    {
        ComboBoxModelGroupNode groupNode = null;
        String keyFieldIdentifier;
        String groupKey;
        String groupFieldNameIdentifier;
        String groupName;

        keyFieldIdentifier = groupNodeDefinition.getGroupKeyFieldIdentifier();
        groupKey = keyFieldIdentifier == null ? null : (String) entity.getFieldValue(keyFieldIdentifier);
        groupFieldNameIdentifier = groupNodeDefinition.getGroupNameFieldIdentifier();
        groupName = groupFieldNameIdentifier == null ? groupNodeDefinition.getGroupName() : (String) entity.getFieldValue(groupFieldNameIdentifier);

        for (ComboBoxModelGroupNode groupNode1 : rootNode.getGroupNodes())
        {
            if (Objects.equals(groupNode1.getGroupKey(), groupKey))
            {
                groupNode = groupNode1;
                break;
            }
        }

        //If the group node is null it does not exist so create it
        if (groupNode == null)
        {
            groupNode = new ComboBoxModelGroupNode(groupName, groupKey);
            rootNode.addGroupNode(groupNode);
        }

        //Process the items for the node
        String itemDisplayIdentifier;
        String itemDisplay;
        String itemDescriptionIdentifier;
        String itemDescription;
        String itemValueIdentifier;
        String itemValue;

        itemDisplayIdentifier = groupNodeDefinition.getItemDisplayFieldIdentifier();
        itemDescriptionIdentifier = groupNodeDefinition.getItemDescriptionFieldIdentifier();
        itemValueIdentifier = groupNodeDefinition.getItemValueFieldIdentifier();

        itemDisplay = (String) entity.getFieldValue(itemDisplayIdentifier);
        itemDescription = itemDescriptionIdentifier == null ? null : (String) entity.getFieldValue(itemDescriptionIdentifier);
        itemValue = (String) entity.getFieldValue(itemValueIdentifier);

        groupNode.addNode(new ComboBoxModelItemNode(itemDisplay, itemValue, itemDescription));
    }
}
