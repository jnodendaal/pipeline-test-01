package com.pilog.t8.ui.datarecordtable;

import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8SubDataFilter;
import com.pilog.t8.data.document.DocumentHandler;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.T8DataRecordValidator;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceDefinition;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.data.T8DataTransaction;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import static com.pilog.t8.definition.ui.datarecordtable.T8DataRecordTableAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordTableOperations
{
    public static class CountDataRecordsOperation extends T8DefaultServerOperation
    {
        public CountDataRecordsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Integer> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String recordDocumentEntityID;
            Integer dataRecordCount;
            T8DataFilter dataFilter;

            // Get all the required operation parameters.
            recordDocumentEntityID = (String)operationParameters.get(PARAMETER_DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER);
            dataFilter = (T8DataFilter)operationParameters.get(PARAMETER_DATA_FILTER);

            // Retrieve the data records.
            dataRecordCount = tx.count(recordDocumentEntityID, dataFilter);
            return HashMaps.createSingular(PARAMETER_DATA_RECORD_COUNT, dataRecordCount);
        }
    }

    public static class RetrieveDataRecordsOperation extends T8DefaultServerOperation
    {
        public RetrieveDataRecordsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8TerminologyApi trmApi;
            List<T8ConceptTerminology> terminologyList;
            List<DataRecord> dataRecords;
            List<String> parentRecordIDList;
            String recordDocumentEntityID;
            String recordStructureEntityID;
            String languageId;
            Set<String> conceptIds;
            T8DataFilter dataFilter;
            Integer pageOffset;
            Integer pageSize;

            // Get all the required operation parameters.
            recordDocumentEntityID = (String)operationParameters.get(PARAMETER_DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER);
            recordStructureEntityID = (String)operationParameters.get(PARAMETER_DATA_RECORD_STRUCTURE_ENTITY_IDENTIFIER);
            parentRecordIDList = (List<String>)operationParameters.get(PARAMETER_PARENT_DATA_RECORD_ID_LIST);
            languageId = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);
            dataFilter = (T8DataFilter)operationParameters.get(PARAMETER_DATA_FILTER);
            pageOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);

            // Get the Organization API.
            trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);

            // Create the sub-query that will retrieve only records linked to the specified parents.
            if ((parentRecordIDList != null) && (parentRecordIDList.size() > 0))
            {
                T8SubDataFilter subDataFilter;
                T8DataFilter recordFilter;

                // Create a filter on the record structure using the parent ID list.
                recordFilter = new T8DataFilter(recordStructureEntityID);
                recordFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, recordStructureEntityID + EF_PARENT_RECORD_ID, T8DataFilterCriterion.DataFilterOperator.IN, parentRecordIDList, false);

                // Map the Child Record ID field to the Document ID field.
                subDataFilter = new T8SubDataFilter(recordFilter);
                subDataFilter.setSubFilterFieldMapping(HashMaps.newHashMap(recordStructureEntityID + EF_CHILD_RECORD_ID, recordDocumentEntityID + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER));

                // Add the sub-filter to the main data filter
                if (dataFilter == null) dataFilter = new T8DataFilter(recordDocumentEntityID);
                dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, recordDocumentEntityID + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER, T8DataFilterCriterion.DataFilterOperator.IN, subDataFilter, false);
            }

            // Retrieve the data records.
            dataRecords = loadDataRecords(tx, recordDocumentEntityID, dataFilter, pageOffset, pageSize);

            // Create a set of concept ID's for which terminology will be retrieved.
            conceptIds = new HashSet<>();
            if (dataRecords != null)
            {
                // Create a set of all concept ID's used by the retrieved records.
                for (DataRecord dataRecord : dataRecords)
                {
                    conceptIds.addAll(DocumentHandler.getAllConceptIds(dataRecord));
                }

                // Because the Concept ID's used included in the Data Requirement are retrieved separately as part of the table model, remove them.
                if (dataRecords.size() > 0) conceptIds.removeAll(DocumentHandler.getAllConceptIds(dataRecords.get(0).getDataRequirement()));
            }

            // Retrieve the terminology.
            terminologyList = trmApi.retrieveTerminology(ArrayLists.typeSafeList(languageId), conceptIds);

            // Return the operation results.
            return HashMaps.createTypeSafeMap(new String[]{PARAMETER_DATA_RECORD_LIST, PARAMETER_CONCEPT_TERMINOLOGY_LIST}, new Object[]{dataRecords, terminologyList});
        }

        private List<DataRecord> loadDataRecords(T8DataTransaction tx, String entityIdentifier, T8DataFilter dataFilter, int pageOffset, int pageSize) throws Exception
        {
            List<T8DataEntity> dataEntityList;

            dataEntityList = tx.select(entityIdentifier, dataFilter, pageOffset, pageSize);
            if (dataEntityList != null)
            {
                List<DataRecord> dataRecordList;

                dataRecordList = new ArrayList<>();
                for (T8DataEntity dataEntity : dataEntityList)
                {
                    dataRecordList.add((DataRecord)dataEntity.getFieldValue(entityIdentifier + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER));
                }

                return dataRecordList;
            }
            else return null;
        }
    }

    public static class ValidateFilteredDataRecordsOperation extends T8DefaultServerOperation
    {
        public ValidateFilteredDataRecordsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8DataFileValidationReport> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                T8DataFileValidationReport validationReport;
                T8DataFilter dataFilter;
                String recordDocumentDEID;
                T8DataEntityResults entityResults = null;

                // Create a new validation report.
                validationReport = new T8DataFileValidationReport();

                // Get all of the required operation parameters.
                recordDocumentDEID = (String)operationParameters.get(PARAMETER_DATA_RECORD_DOCUMENT_ENTITY_IDENTIFIER);
                dataFilter = (T8DataFilter)operationParameters.get(PARAMETER_DATA_FILTER);

                try
                {
                    T8DataRecordValidator recordValidator;

                    // Create the record validator and initialize it.
                    recordValidator = new T8DataRecordValidator(tx);

                    // Scroll through the filtered set of Data Records and validate each one.
                    entityResults = tx.scroll(recordDocumentDEID, dataFilter);
                    while (entityResults.next())
                    {
                        T8DataEntity nextEntity;
                        DataRecord dataRecord;

                        // Get the next record from the result set.
                        nextEntity = entityResults.get();
                        dataRecord = (DataRecord)nextEntity.getFieldValue(recordDocumentDEID + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);

                        // Validate the record.
                        validationReport.addValidationReport(recordValidator.validateDataRecords(dataRecord));
                    }

                    // If any failed validation reports were found, throw the exception.
                    return HashMaps.newHashMap(PARAMETER_DATA_RECORD_VALIDATION_REPORT_LIST, validationReport);
                }
                finally
                {
                    if (entityResults != null) entityResults.close();
                }
            }
            else throw new Exception("No parameters found.");
        }
    }
}
