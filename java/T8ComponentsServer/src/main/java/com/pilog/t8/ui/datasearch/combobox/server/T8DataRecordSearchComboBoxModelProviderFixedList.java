package com.pilog.t8.ui.datasearch.combobox.server;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelGroupNode;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelItemNode;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelRootNode;
import com.pilog.t8.ui.datasearch.combobox.T8DataSearchComboBoxModelProvider;
import com.pilog.t8.definition.ui.datasearch.combobox.datasource.fixed.T8DataSearchComboBoxDataSourceFixedListDefinition;
import com.pilog.t8.definition.ui.datasearch.combobox.datasource.fixed.T8DataSearchComboBoxDataSourceFixedListGroupNodeDefinition;
import com.pilog.t8.definition.ui.datasearch.combobox.datasource.fixed.T8DataSearchComboBoxDataSourceFixedListItemDefinition;

/**
 * @author Hennie Brink
 */
public class T8DataRecordSearchComboBoxModelProviderFixedList implements T8DataSearchComboBoxModelProvider
{
    private final T8DataSearchComboBoxDataSourceFixedListDefinition definition;

    public T8DataRecordSearchComboBoxModelProviderFixedList(T8ServerContext serverContext, T8SessionContext sessionContext, T8DataSearchComboBoxDataSourceFixedListDefinition definition)
    {
        this.definition = definition;
    }

    @Override
    public ComboBoxModelRootNode createRootNode(String searchQuery, int retrievalSize, T8DataFilter dataSourceFilter) throws Exception
    {
        ComboBoxModelRootNode rootNode;

        rootNode = new ComboBoxModelRootNode();
        rootNode.setOptionsVisible(definition.isOptionsVisible());
        rootNode.setSearchVisible(definition.isSearchVisible());
        rootNode.setSelectionMode(definition.getSelectionMode());

        for (T8DataSearchComboBoxDataSourceFixedListGroupNodeDefinition groupNodeDefinition : definition.getGroupNodeDefinitions())
        {
            ComboBoxModelGroupNode groupNode;

            groupNode = new ComboBoxModelGroupNode(groupNodeDefinition.getGroupName(), groupNodeDefinition.getIdentifier());

            for (T8DataSearchComboBoxDataSourceFixedListItemDefinition groupItem : groupNodeDefinition.getGroupItems())
            {
                ComboBoxModelItemNode itemNode;

                itemNode = new ComboBoxModelItemNode();
                itemNode.setItemName(groupItem.getItemDisplay());
                itemNode.setDescription(groupItem.getItemDescription());
                itemNode.setItemValue(groupItem.getItemValue());

                groupNode.addNode(itemNode);
            }

            rootNode.addGroupNode(groupNode);
        }

        rootNode.setCompleteDataSet(true);
        return rootNode;
    }
}
