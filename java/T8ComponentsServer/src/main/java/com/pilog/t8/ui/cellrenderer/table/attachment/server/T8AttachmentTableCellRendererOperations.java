package com.pilog.t8.ui.cellrenderer.table.attachment.server;

import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.cellrenderer.table.attachment.T8AttachmentThumbnail;
import com.pilog.t8.data.document.datarecord.attachment.T8DefaultAttachmentHandler;
import com.pilog.t8.data.source.datarecord.T8AttachmentHandler;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import com.pilog.t8.definition.data.document.T8DocumentResources;
import com.pilog.t8.utilities.images.Images;
import com.pilog.t8.utilities.images.Scalr;
import com.pilog.t8.utilities.images.Scalr.Method;
import java.awt.Image;
import java.io.File;

import static com.pilog.t8.definition.ui.cellrenderer.table.attachment.T8AttachmentTableCellRendererAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8AttachmentTableCellRendererOperations
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8AttachmentTableCellRendererOperations.class);

    public static class RetrieveAttachmentThumbnailOperation extends T8DefaultServerOperation
    {
        public RetrieveAttachmentThumbnailOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8AttachmentThumbnail> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordApi recApi;
            T8FileDetails fileDetails;
            String attachmentId;
            Integer width;
            Integer height;

            // Get the operation parameters.
            attachmentId = (String) operationParameters.get(PARAMETER_ATTACHMENT_ID);
            width = (Integer) operationParameters.get(PARAMETER_WIDTH);
            height = (Integer) operationParameters.get(PARAMETER_HEIGHT);

            // Set some default values for parameters not received.
            if (width == null) width = 64;
            if (height == null) height = 64;

            // Retrieve the attachment details.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            fileDetails = recApi.retrieveAttachmentDetails(attachmentId);

            // If we found the attachment details, create a thumbnail for it.
            if (fileDetails != null)
            {
                T8AttachmentThumbnail thumbnail;

                // Generate a thumbnail depending on the media type of the attachment.
                if (fileDetails.getMediaType() == null)
                {
                    thumbnail = createNonImageAttachmentThumbnail(attachmentId, fileDetails, width, height);
                }
                else if (fileDetails.getMediaType().startsWith("image"))
                {
                    thumbnail = createImageAttachmentThumbnail(attachmentId, fileDetails, width, height);
                }
                else
                {
                    thumbnail = createNonImageAttachmentThumbnail(attachmentId, fileDetails, width, height);
                }

                // Return the generated thumbnail.
                return HashMaps.createSingular(PARAMETER_ATTACHMENT_TUMBNAIL, thumbnail);
            }
            else
            {
                // Attachment not found so return a null object.
                return HashMaps.createSingular(PARAMETER_ATTACHMENT_TUMBNAIL, null);
            }
        }

        private T8AttachmentThumbnail createImageAttachmentThumbnail(String attachmentID, T8FileDetails fileDetails, int width, int height) throws Exception
        {
            InputStream fileInputStream = null;

            try
            {
                T8AttachmentHandler attachmentHandler;
                T8AttachmentThumbnail attachmentThumbnail;
                BufferedImage attachmentImage;
                BufferedImage thumbnailImage;

                // Get an input stream from the attachment image.
                attachmentHandler = new T8DefaultAttachmentHandler(serverContext, tx, T8DocumentResources.DATA_RECORD_DOCUMENT_DE_IDENTIFIER);
                fileInputStream = attachmentHandler.getFileInputStream(attachmentID);
                attachmentImage = ImageIO.read(fileInputStream);
                fileInputStream.close();

                // Create a thumbnail image by scaling the retrieved attachment.
                thumbnailImage = Scalr.resize(attachmentImage, Method.QUALITY, width, height, Scalr.OP_ANTIALIAS);
                attachmentImage.flush();

                // Create the thumbnail object and return the result.
                attachmentThumbnail = new T8AttachmentThumbnail(attachmentID, fileDetails);
                attachmentThumbnail.setThumbnail(new ImageIcon(thumbnailImage));
                return attachmentThumbnail;
            }
            finally
            {
                if (fileInputStream != null) fileInputStream.close();
            }
        }

        private T8AttachmentThumbnail createNonImageAttachmentThumbnail(String attachmentID, T8FileDetails fileDetails, int width, int height) throws Exception
        {
            T8AttachmentThumbnail attachmentThumbnail;
            BufferedImage thumbnailImage;
            Image attachmentIcon;
            String fileName;
            File tempFile;

            // Create a temporary file using the filename and extension of the attachment.  Then get the system default icon for the filetype and use it as an attachment.
            fileName = fileDetails.getFileName();
            tempFile = File.createTempFile(fileName.substring(0, fileName.lastIndexOf('.')), fileName.substring(fileName.lastIndexOf('.')));
            attachmentIcon = sun.awt.shell.ShellFolder.getShellFolder(tempFile).getIcon(true);
            tempFile.delete();

            // Create a scaled version of the icon.
            thumbnailImage = Scalr.resize(Images.toBufferedImage(attachmentIcon), Method.QUALITY, width, height, Scalr.OP_ANTIALIAS);

            // Create the thumbnail object and return the result.
            attachmentThumbnail = new T8AttachmentThumbnail(attachmentID, fileDetails);
            attachmentThumbnail.setThumbnail(new ImageIcon(thumbnailImage));
            return attachmentThumbnail;
        }
    }
}
