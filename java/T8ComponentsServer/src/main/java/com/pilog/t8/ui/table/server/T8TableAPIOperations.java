package com.pilog.t8.ui.table.server;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.entity.T8DataEntityValidationReport;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.script.validation.entity.T8ServerEntityValidationScriptDefinition;
import com.pilog.t8.definition.ui.table.T8TableRowChange;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.definition.ui.table.script.T8TableCommitScriptDefinition;
import java.util.HashMap;

import static com.pilog.t8.definition.ui.table.T8TableAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8TableAPIOperations
{
    public static class SaveDataChangesOperation extends T8DefaultServerOperation
    {
        public SaveDataChangesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8TableRowChange> tableChanges;
            T8TableDefinition tableDefinition;
            String globalTableId;

            // Get the operation parameters.
            globalTableId = (String)operationParameters.get(PARAMETER_TABLE_ID);
            tableChanges = (List<T8TableRowChange>)operationParameters.get(PARAMETER_TABLE_ROW_CHANGE_LIST);

            // Get the table definition.
            tableDefinition = getTableDefinition(globalTableId);
            if (tableDefinition != null)
            {
                T8TableCommitScriptDefinition commitScriptDefinition;

                // Check for a commit script definition and if present, execute it.
                commitScriptDefinition = tableDefinition.getCommitScriptDefinition();
                if (commitScriptDefinition != null)
                {
                    Map<String, Object> scriptInputParameters;
                    Map<String, Object> scriptOutputParameters;
                    Map<String, Object> operationOutputParameters;
                    T8Script script;

                    // Create the script input parameters and execute the script.
                    scriptInputParameters = new HashMap<>();
                    scriptInputParameters.put(T8TableCommitScriptDefinition.PARAMETER_TABLE_CHANGE_LIST, tableChanges);
                    script = commitScriptDefinition.getNewScriptInstance(context);
                    scriptOutputParameters = script.executeScript(scriptInputParameters);

                    // Return the output entity list as operation output.
                    operationOutputParameters = new HashMap<>();
                    scriptOutputParameters = T8IdentifierUtilities.stripNamespace(scriptOutputParameters);
                    operationOutputParameters.put(PARAMETER_DATA_ENTITY_LIST, scriptOutputParameters.get(T8TableCommitScriptDefinition.PARAMETER_OUTPUT_ENTITY_LIST));
                    operationOutputParameters.put(PARAMETER_VALIDATION_REPORT_LIST, scriptOutputParameters.get(PARAMETER_VALIDATION_REPORT_LIST));
                    return operationOutputParameters;
                }
                else // If no commit script is defined, execute the default commit routine.
                {
                    T8ServerEntityValidationScriptDefinition validationScriptDefinition;

                    // Get the validation script.
                    validationScriptDefinition = tableDefinition.getServerValidationScriptDefinition();

                    // Do validation if applicable.
                    if (validationScriptDefinition != null)
                    {
                        List<T8DataEntityValidationReport> validationReportList;

                        validationReportList = validateTableChanges(context, validationScriptDefinition, tableChanges);
                        for (T8DataEntityValidationReport report : validationReportList)
                        {
                            if (!report.isValidationSuccess())
                            {
                                Map<String, Object> operationOutputParameters;

                                // Return the output entity list as operation output.
                                operationOutputParameters = new HashMap<>();
                                operationOutputParameters.put(PARAMETER_VALIDATION_REPORT_LIST, validationReportList);
                                return operationOutputParameters;
                            }
                        }
                    }

                    // Now persist the data changes.
                    for (T8TableRowChange tableChange : tableChanges)
                    {
                        T8DataEntity newEntity;
                        T8DataEntity oldEntity;

                        oldEntity = tableChange.getOldDataEntity();
                        newEntity = tableChange.getNewDataEntity();
                        switch (tableChange.getChangeType())
                        {
                            case T8TableRowChange.CHANGE_TYPE_UPDATE:
                                tx.update(newEntity);
                                break;
                            case T8TableRowChange.CHANGE_TYPE_INSERT:
                                tx.insert(newEntity);
                                break;
                            case T8TableRowChange.CHANGE_TYPE_DELETE:
                                tx.delete(oldEntity);
                                break;
                        }
                    }

                    return null;
                }
            }
            else throw new Exception("Table {"+T8IdentifierUtilities.getLocalIdentifierPart(globalTableId)+"} not found in {"+globalTableId+"} ");
        }

        private T8TableDefinition getTableDefinition(String globalTableId) throws Exception
        {
            T8Definition parentDefinition;

            parentDefinition = serverContext.getDefinitionManager().getRawDefinition(context, null, T8IdentifierUtilities.getGlobalIdentifierPart(globalTableId));
            if (parentDefinition != null)
            {
                return parentDefinition.getDescendentDefinition(T8IdentifierUtilities.getLocalIdentifierPart(globalTableId));
            } else throw new RuntimeException("Could not find parent definition {"+T8IdentifierUtilities.getGlobalIdentifierPart(globalTableId)+"} containing table {"+globalTableId+"}");
        }

        private List<T8DataEntityValidationReport> validateTableChanges(T8Context context, T8ServerEntityValidationScriptDefinition validationScriptDefinition, List<T8TableRowChange> tableChanges) throws Exception
        {
            List<T8DataEntity> entityList;

            entityList = new ArrayList<>();
            for (T8TableRowChange rowChange : tableChanges)
            {
                T8DataEntity newEntity;
                T8DataEntity oldEntity;
                T8DataEntity combinedEntity;

                newEntity = rowChange.getNewDataEntity();
                oldEntity = rowChange.getOldDataEntity();
                combinedEntity = oldEntity.copy();
                combinedEntity.setFieldValues(newEntity.getFieldValues());
                entityList.add(combinedEntity);
            }

            return serverContext.getDataManager().validateDataEntities(context, entityList, validationScriptDefinition);
        }
    }
}
