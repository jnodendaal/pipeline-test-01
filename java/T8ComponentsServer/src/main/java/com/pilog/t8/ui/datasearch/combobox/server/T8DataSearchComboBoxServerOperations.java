package com.pilog.t8.ui.datasearch.combobox.server;

import static com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxAPIHandler.*;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelRootNode;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxDataSourceDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBoxServerOperations
{
    public static class RetrieveFilterData extends T8DefaultServerOperation
    {
        public RetrieveFilterData(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, ComboBoxModelRootNode> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String dataSourcIdentifier;
            String projectId;
            T8DataFilter dataFilter;
            String searchQuery;
            Integer retrievalSize;
            T8DataSearchComboBoxDataSourceDefinition dataSourceDefinition;
            ComboBoxModelRootNode rootNode;

            projectId = (String) operationParameters.get(PARAMETER_PROJECT_ID);
            dataSourcIdentifier = (String) operationParameters.get(PARAMETER_DATA_SOURCE_IDENTIFIER);
            searchQuery = (String) operationParameters.get(PARAMETER_SEARCH_QUERY);
            retrievalSize = (Integer) operationParameters.get(PARAMETER_RETRIEVAL_SIZE);
            dataFilter = (T8DataFilter)operationParameters.get(PARAMETER_DATA_FILTER);

            if (retrievalSize == null) retrievalSize = 100;

            dataSourceDefinition = (T8DataSearchComboBoxDataSourceDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, projectId, dataSourcIdentifier, null);
            rootNode = dataSourceDefinition.createModelProvider(context).createRootNode(searchQuery, retrievalSize, dataFilter);
            return HashMaps.createSingular(PARAMETER_DATA_MODEL_ROOT_NODE, rootNode);
        }
    }
}
