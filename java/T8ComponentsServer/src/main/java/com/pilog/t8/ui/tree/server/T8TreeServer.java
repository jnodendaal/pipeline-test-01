package com.pilog.t8.ui.tree.server;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.tree.T8TreeNode;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeNodeDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.ui.tree.T8TreeAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8TreeServer
{
    public static class LoadTreeNodesOperation extends T8DefaultServerOperation
    {
        public LoadTreeNodesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                List<T8TreeNodeDefinition> childNodeDefinitions;
                List<T8TreeNode> childNodes;
                Map<String, T8DataFilter> userDefinedChildNodeFilters;
                String childNodeRestrictionIdentifier;
                T8DataManager dataManager;
                T8DataSession ds;
                T8DataEntity parentDataEntity;
                T8TreeNodeDefinition parentNodeDefinition;
                T8TreeNodeDefinition recursionNodeDefinition;
                Integer pageOffset;
                Integer pageSize;
                int nodeCount;

                // Get all the parameters and object required for the operation execution.
                parentNodeDefinition = (T8TreeNodeDefinition)operationParameters.get(PARAMETER_TREE_NODE_DEFINITION);
                parentDataEntity = (T8DataEntity)operationParameters.get(PARAMETER_DATA_ENTITY);
                childNodeRestrictionIdentifier = (String)operationParameters.get(PARAMETER_TREE_NODE_IDENTIFIER);
                pageOffset = (Integer)operationParameters.get(PARAMETER_TREE_NODE_PAGE_OFFSET);
                pageSize = (Integer)operationParameters.get(PARAMETER_TREE_NODE_PAGE_SIZE);
                userDefinedChildNodeFilters = (Map<String, T8DataFilter>)operationParameters.get(PARAMETER_TREE_NODE_FILTER_MAP);
                childNodeDefinitions = parentNodeDefinition.getChildNodeDefinitions();
                dataManager = serverContext.getDataManager();
                ds = dataManager.getCurrentSession();

                // Make sure we have valid paging details.
                if ((pageOffset == null) || (pageOffset < 0)) pageOffset = 0;
                if (pageSize == null) pageSize = 100;

                // Create a list to hold all retrieved child nodes.
                childNodes = new ArrayList<T8TreeNode>();
                nodeCount = 0;

                // If a recursion target is specified, add the recursed nodes.
                recursionNodeDefinition = parentNodeDefinition.getRecursionNodeDefinition();
                if (recursionNodeDefinition != null)
                {
                    if ((recursionNodeDefinition.isGroupNodeEnabled()) && (childNodeRestrictionIdentifier == null))
                    {
                        T8TreeNode groupNode;

                        groupNode = new T8TreeNode(recursionNodeDefinition.getGroupDisplayName(), recursionNodeDefinition.getPageSize());
                        groupNode.add(new T8TreeNode(null, null)); // Add a fake node just to enabled node expansion (which will trigger the loading of child nodes to replace the fake node).

                        // Add the group node only.
                        childNodes.add(groupNode);
                        nodeCount++;
                    }// If a child node filter is specified, only retrieve nodes of the required type.
                    else if ((childNodeRestrictionIdentifier == null) || (childNodeRestrictionIdentifier.equals(recursionNodeDefinition.getIdentifier())))
                    {
                        T8DataFilter dataFilter;
                        List<T8DataEntity> childEntities;
                        int childCount;

                        dataFilter = getNodeFilter(context, recursionNodeDefinition.getDataEntityIdentifier(), parentNodeDefinition.getRecursionPrefilterDefinitions(), parentNodeDefinition.getRecursionFieldMapping(), parentDataEntity);
                        childEntities = ds.instantTransaction().select(recursionNodeDefinition.getDataEntityIdentifier(), dataFilter, pageOffset, pageSize);
                        childCount = ds.instantTransaction().count(recursionNodeDefinition.getDataEntityIdentifier(), dataFilter);
                        childNodes.addAll(buildNodes(recursionNodeDefinition, childEntities));
                        nodeCount += childCount;
                    }
                }

                // Retrieve nodes for each of the defined child types.
                if (childNodeDefinitions != null)
                {
                    for (T8TreeNodeDefinition childNodeDefinition : childNodeDefinitions)
                    {
                        if ((childNodeDefinition.isGroupNodeEnabled()) && (childNodeRestrictionIdentifier == null))
                        {
                            T8TreeNode groupNode;

                            groupNode = new T8TreeNode(childNodeDefinition.getGroupDisplayName(), childNodeDefinition.getPageSize());
                            groupNode.add(new T8TreeNode(null, null)); // Add a fake node just to enabled node expansion (which will trigger the loading of child nodes to replace the fake node).

                            // Add the group node only.
                            childNodes.add(groupNode);
                            nodeCount++;
                        }// If a child node filter is specified, only retrieve nodes of the required type.
                        else if ((childNodeRestrictionIdentifier == null) || (childNodeRestrictionIdentifier.equals(childNodeDefinition.getIdentifier())))
                        {
                            T8DataFilter dataFilter;
                            T8DataFilter userDefinedChildNodeFilter;
                            List<T8DataEntity> childEntities;
                            int childCount;

                            // Create the filter to use for node data retrieval.
                            dataFilter = getNodeFilter(context, childNodeDefinition.getDataEntityIdentifier(), childNodeDefinition.getPrefilterDefinitions(), childNodeDefinition.getRelationshipFieldMapping(), parentDataEntity);
                            userDefinedChildNodeFilter = userDefinedChildNodeFilters != null ? userDefinedChildNodeFilters.get(childNodeDefinition.getIdentifier()) : null;
                            if ((userDefinedChildNodeFilter != null) && (userDefinedChildNodeFilter.hasFilterCriteria())) dataFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, userDefinedChildNodeFilter);

                            // Retrieve the data and build tree nodes from each retrieved entity.
                            childEntities = ds.instantTransaction().select(childNodeDefinition.getDataEntityIdentifier(), dataFilter, pageOffset, pageSize);
                            childCount = ds.instantTransaction().count(childNodeDefinition.getDataEntityIdentifier(), dataFilter);
                            childNodes.addAll(buildNodes(childNodeDefinition, childEntities));
                            nodeCount += childCount;
                        }
                    }
                }

                // Return the final list of nodes.
                return HashMaps.newHashMap(PARAMETER_TREE_NODE_LIST, childNodes, PARAMETER_TREE_NODE_COUNT, nodeCount);
            }
            else throw new Exception("No parameters found.");
        }
    }

    private static List<T8TreeNode> buildNodes(T8TreeNodeDefinition definition, List<T8DataEntity> nodeData)
    {
        List<T8TreeNode> treeNodes;

        treeNodes = new ArrayList<>();
        for (T8DataEntity entity : nodeData)
        {
            T8TreeNode node;

            // Create a new tree node.
            node = new T8TreeNode(definition, entity);

            // If required, add a fake node just to enabled node expansion (which will trigger the loading of child nodes to replace the fake node).
            if ((definition.getChildNodeDefinitions().size() > 0) || (definition.getRecursionNodeDefinition() != null))
            {
                node.add(new T8TreeNode(null, 0));
            }

            // Add the completed node to the list.
            treeNodes.add(node);
        }

        return treeNodes;
    }

    private static T8DataFilter getNodeFilter(T8Context context, String entityIdentifier, List<T8DataFilterDefinition> prefilterDefinitions, Map<String, String> fieldMapping, T8DataEntity parentEntity)
    {
        if (parentEntity != null)
        {
            T8DataFilter combinedFilter;
            T8DataFilterCriteria combinedFilterCriteria;
            LinkedHashMap<String, OrderMethod> fieldOrdering;
            Map<String, Object> keyData;

            // Create a filter to contain a combination of all applicable filters for data retrieval.
            combinedFilterCriteria = new T8DataFilterCriteria();

            // Add all available and valid prefilters.
            fieldOrdering = new LinkedHashMap<String, OrderMethod>();
            for (T8DataFilterDefinition prefilterDefinition : prefilterDefinitions)
            {
                LinkedHashMap<String, OrderMethod> prefilterOrdering;
                T8DataFilter prefilter;

                // Only include filters that have valid filter criteria.
                prefilter = prefilterDefinition.getNewDataFilterInstance(context, null);
                if ((prefilter != null) && (prefilter.hasFilterCriteria()))
                {
                    combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
                }

                // Add all prefilter field ordering to the combined filter.
                prefilterOrdering = prefilter.getFieldOrdering();
                if (prefilterOrdering != null) fieldOrdering.putAll(prefilterOrdering);
            }

            // Create a key map to use as filter criteria.
            keyData = parentEntity.getFieldValues();
            keyData = T8IdentifierUtilities.mapParameters(keyData, fieldMapping);
            combinedFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriteria(keyData));

            // Return the combined filter.
            combinedFilter = new T8DataFilter(entityIdentifier, combinedFilterCriteria);
            if (fieldOrdering.size() > 0) combinedFilter.setFieldOrdering(fieldOrdering);
            return combinedFilter;
        }
        else return null; // No filter, since no parent supplied.
    }
}
