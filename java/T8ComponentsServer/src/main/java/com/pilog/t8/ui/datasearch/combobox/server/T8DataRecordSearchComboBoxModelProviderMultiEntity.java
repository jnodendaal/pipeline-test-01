package com.pilog.t8.ui.datasearch.combobox.server;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelGroupNode;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelItemNode;
import com.pilog.t8.ui.datasearch.combobox.ComboBoxModelRootNode;
import com.pilog.t8.ui.datasearch.combobox.T8DataSearchComboBoxModelProvider;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.datasearch.combobox.datasource.entity.multi.T8DataSearchComboBoxDataSourceMultiEntityDefinition;
import com.pilog.t8.definition.ui.datasearch.combobox.datasource.entity.multi.T8DataSearchComboBoxDataSourceMultiEntityGroupNodeDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Hennie Brink
 */
public class T8DataRecordSearchComboBoxModelProviderMultiEntity implements T8DataSearchComboBoxModelProvider
{
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8DataSearchComboBoxDataSourceMultiEntityDefinition definition;
    private final ComboBoxModelRootNode rootNode;

    private int scrollCount;

    public T8DataRecordSearchComboBoxModelProviderMultiEntity(T8Context context, T8DataSearchComboBoxDataSourceMultiEntityDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
        this.rootNode = new ComboBoxModelRootNode();

        this.rootNode.setCompleteDataSet(true);
        this.rootNode.setOptionsVisible(definition.isOptionsVisible());
        this.rootNode.setSearchVisible(definition.isSearchVisible());
        this.rootNode.setSelectionMode(definition.getSelectionMode());

        this.scrollCount = 0;
    }

    @Override
    public ComboBoxModelRootNode createRootNode(String searchQuery, int retrievalSize, T8DataFilter dataSourceFilter) throws Exception
    {
        for (T8DataSearchComboBoxDataSourceMultiEntityGroupNodeDefinition groupNodeDefinition : definition.getGroupNodeDefinitions())
        {
            rootNode.addGroupNode(createGroupNode(groupNodeDefinition, searchQuery, retrievalSize, dataSourceFilter));

            if (scrollCount > retrievalSize)
            {
                rootNode.setCompleteDataSet(false);
                break;
            }
        }

        if (!Strings.isNullOrEmpty(searchQuery)) //If the search query is set it is most likely never going to be a complete data set
        {
            rootNode.setCompleteDataSet(false);
        }

        return rootNode;
    }

    private ComboBoxModelGroupNode createGroupNode(T8DataSearchComboBoxDataSourceMultiEntityGroupNodeDefinition groupNodeDefinition, String searchQuery, int retrievalSize, T8DataFilter dataSourceFilter) throws Exception
    {
        T8DataTransaction tx;
        T8DataEntityResults scroll;
        ComboBoxModelGroupNode groupNode;

        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        scroll = tx.scroll(groupNodeDefinition.getDataEntityIdentifier(), createCombinedFilter(groupNodeDefinition, searchQuery, dataSourceFilter));
        groupNode = new ComboBoxModelGroupNode(groupNodeDefinition.getGroupName(), groupNodeDefinition.getIdentifier());

        try
        {
            while (scroll.next())
            {
                T8DataEntity nextEntity;

                nextEntity = scroll.get();

                groupNode.addNode(createItemNode(nextEntity, groupNodeDefinition));

                scrollCount++;

                if (scrollCount > retrievalSize)
                {
                    rootNode.setCompleteDataSet(false);
                    break;
                }
            }
        }
        finally
        {
            scroll.close();
        }

        return groupNode;
    }

    private T8DataFilter createCombinedFilter(T8DataSearchComboBoxDataSourceMultiEntityGroupNodeDefinition groupNodeDefinition, String searchQuery, T8DataFilter dataSourceFilter)
    {
        T8DataFilter combinedFilter;
        T8DataFilter definitionFilter;
        List<T8DataFilterDefinition> filterDefinitions;

        filterDefinitions = groupNodeDefinition.getPreFilters();
        combinedFilter = new T8DataFilter(groupNodeDefinition.getDataEntityIdentifier());

        if (filterDefinitions != null && !filterDefinitions.isEmpty())
        {
            for (T8DataFilterDefinition filterDefinition : filterDefinitions)
            {
                definitionFilter = filterDefinition.getNewDataFilterInstance(context, null);

                combinedFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, definitionFilter);

                if (definitionFilter.hasFieldOrdering())
                {
                    combinedFilter.setFieldOrdering(definitionFilter.getFieldOrdering());
                }
            }
        }

        if (dataSourceFilter != null)
        {
            combinedFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, dataSourceFilter);

            if (dataSourceFilter.hasFieldOrdering())
            {
                if (combinedFilter.hasFieldOrdering())
                {
                    combinedFilter.getFieldOrdering().putAll(dataSourceFilter.getFieldOrdering());
                } else combinedFilter.setFieldOrdering(dataSourceFilter.getFieldOrdering());
            }
        }

        if (searchQuery != null)
        {
            combinedFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, groupNodeDefinition.getItemDisplayFieldIdentifier(), T8DataFilterCriterion.DataFilterOperator.LIKE, searchQuery, true);
        }

        return combinedFilter;
    }

    private ComboBoxModelItemNode createItemNode(T8DataEntity entity, T8DataSearchComboBoxDataSourceMultiEntityGroupNodeDefinition groupNodeDefinition)
    {
        //Process the items for the node
        String itemDescriptionIdentifier;
        String itemDisplayIdentifier;
        String itemValueIdentifier;
        String itemDescription;
        Object displayValue;
        String itemDisplay;
        Object itemValue;

        itemDisplayIdentifier = groupNodeDefinition.getItemDisplayFieldIdentifier();
        itemDescriptionIdentifier = groupNodeDefinition.getItemDescriptionFieldIdentifier();
        itemValueIdentifier = groupNodeDefinition.getItemValueFieldIdentifier();

        displayValue = entity.getFieldValue(itemDisplayIdentifier);
        itemDisplay = (displayValue instanceof String) ? ((String)displayValue) : String.valueOf(displayValue);
        itemDescription = itemDescriptionIdentifier == null ? null : (String) entity.getFieldValue(itemDescriptionIdentifier);
        itemValue = entity.getFieldValue(itemValueIdentifier);

        return new ComboBoxModelItemNode(itemDisplay, itemValue, itemDescription);
    }
}
