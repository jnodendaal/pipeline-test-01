/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.table.export.generator;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.ui.table.export.ExportProperty;
import com.pilog.t8.ui.table.export.T8StreamExportGenerator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class XMLStreamExportGenerator implements T8StreamExportGenerator
{
    private final List<ExportProperty> propertys;

    public XMLStreamExportGenerator()
    {
        propertys = new ArrayList<ExportProperty>();
    }

    @Override
    public List<ExportProperty> getProperties()
    {
        return propertys;
    }

    @Override
    public T8DStreamDefinition createExportDefinition(String fileName,
                                                      T8TableDefinition tableDefinition,
                                                      T8DataFilter dataFilter)
    {
        throw new UnsupportedOperationException("Not supported yet by data stream.");
    }
}
