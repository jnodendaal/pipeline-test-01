/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.table.export.generator;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.dstream.node.input.T8DataEntityInputNodeDefinition;
import com.pilog.t8.definition.dstream.node.output.T8DataFileOutputNodeDefinition;
import com.pilog.t8.definition.dstream.output.T8DStreamOutputDefinition;
import com.pilog.t8.definition.dstream.output.T8DataFileOutputDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputDefinition;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.ui.table.export.ExportProperty;
import com.pilog.t8.ui.table.export.T8StreamExportGenerator;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class DataFileStreamExportGenerator implements T8StreamExportGenerator
{
    private final List<ExportProperty> propertys;

    public DataFileStreamExportGenerator()
    {
        propertys = new ArrayList<ExportProperty>();

        propertys.add(new ExportProperty("Password", ExportProperty.PropertyType.Text));
    }

    @Override
    public List<ExportProperty> getProperties()
    {
        return propertys;
    }

    @Override
    public T8DStreamDefinition createExportDefinition(String fileName,
                                                      T8TableDefinition tableDefinition,
                                                      T8DataFilter dataFilter)  throws Exception
    {
        T8DStreamDefinition streamDefinition;
        T8DataFileOutputDefinition outputDefinition;
        T8DataEntityInputNodeDefinition nodeDefinition;
        T8DataFileOutputNodeDefinition outputNodeDefinition;

        Map<String, String> outputPamareterMapping;
        Map<String, String> columnValueExpressions;

        if(Strings.isNullOrEmpty((String) propertys.get(0).getPropertyValue())) throw new Exception("A password must be provided.");

        streamDefinition = new T8DStreamDefinition(T8Definition.getLocalIdPrefix() + T8DStreamDefinition.IDENTIFIER_PREFIX + "TABLE_EXPORT");
        streamDefinition.addStreamParameterDefinition(filterParameterDefinition);
        streamDefinition.setLoggingEnabled(false);

        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                streamDefinition.addStreamParameterDefinition(new T8DataParameterDefinition(t8TableColumnDefinition.getIdentifier(), t8TableColumnDefinition.getColumnName(), t8TableColumnDefinition.getMetaDescription(), tableDefinition.getEntityDefinition().getFieldDataType(t8TableColumnDefinition.getFieldIdentifier())));
            }
        }

        outputDefinition = new T8DataFileOutputDefinition(T8Definition.getLocalIdPrefix() + T8ExcelFileOutputDefinition.IDENTIFIER_PREFIX + "TABLE_EXPORT_OUTPUT_NODE");

        outputDefinition.setPassword((String) propertys.get(0).getPropertyValue());

        int fieldIndex = 0;
        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                outputDefinition.addColumnIdentifier(t8TableColumnDefinition.getColumnName());
            }
        }

        outputDefinition.setFileName(fileName.endsWith(".t8data") ? fileName
                                     : fileName + ".t8data");

        streamDefinition.setOutputDefinitions(Arrays.asList((T8DStreamOutputDefinition) outputDefinition));

        nodeDefinition = new T8DataEntityInputNodeDefinition(T8Definition.getLocalIdPrefix() + "TABLE_EXPORT_ROOT_NODE");
        outputPamareterMapping = new HashMap<String, String>();

        nodeDefinition.setDataEntityIdentifier(tableDefinition.getDataEntityIdentifier());
        nodeDefinition.setDataFilterInputParameterIdentifier(filterParameterDefinition.getIdentifier());

        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                T8DataEntityFieldDefinition fieldDefinition;

                fieldDefinition = tableDefinition.getEntityDefinition().getFieldDefinition(t8TableColumnDefinition.getFieldIdentifier());
                outputPamareterMapping.put(fieldDefinition.getPublicIdentifier(), t8TableColumnDefinition.getIdentifier());
            }
        }

        nodeDefinition.setOutputParameterMapping(outputPamareterMapping);

        outputNodeDefinition = new T8DataFileOutputNodeDefinition(T8Definition.getLocalIdPrefix() + "TABLE_EXPORT_OUTPUT_NODE");

        outputNodeDefinition.setOutputIdentifier(outputDefinition.getIdentifier());

        columnValueExpressions = new HashMap<String, String>();

        fieldIndex = 0;
        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                columnValueExpressions.put(t8TableColumnDefinition.getColumnName(), t8TableColumnDefinition.getIdentifier());
            }
        }

        outputNodeDefinition.setColumnValueExpressions(columnValueExpressions);

        nodeDefinition.setChildNodeDefinitions(Arrays.asList((T8DStreamNodeDefinition) outputNodeDefinition));

        streamDefinition.setRootNodeDefinitions(Arrays.asList((T8DStreamNodeDefinition) nodeDefinition));

        return streamDefinition;
    }
}
