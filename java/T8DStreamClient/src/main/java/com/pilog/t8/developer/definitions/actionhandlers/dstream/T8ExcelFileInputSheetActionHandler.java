package com.pilog.t8.developer.definitions.actionhandlers.dstream;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputFieldDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputSheetDefinition;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelFileInputSheetActionHandler implements T8DefinitionActionHandler
{
    private T8ClientContext clientContext;
    private T8SessionContext sessionContext;
    private T8ExcelFileInputSheetDefinition definition;
    
    public T8ExcelFileInputSheetActionHandler(T8ClientContext clientContext, T8SessionContext sessionContext, T8ExcelFileInputSheetDefinition definition)
    {
        this.clientContext = clientContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
    }
    
    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;
        
        actions = new ArrayList<javax.swing.Action>();
        actions.add(new AddExcelFieldsAsStreamParametersAction(clientContext, sessionContext, definition));
        
        return actions;
    }
    
    private static class AddExcelFieldsAsStreamParametersAction extends AbstractAction
    {
        private T8ClientContext clientContext;
        private T8SessionContext sessionContext;
        private T8ExcelFileInputSheetDefinition definition;
        
        public AddExcelFieldsAsStreamParametersAction(final T8ClientContext clientContext, final T8SessionContext sessionContext, final T8ExcelFileInputSheetDefinition definition)
        {
            this.clientContext = clientContext;
            this.sessionContext = sessionContext;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Add all fields on the sheet to the stream as stream parameters.");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/dstream/icons/databaseGoIcon.png")));
        }
        
        @Override
        public void actionPerformed(ActionEvent event)
        {
            T8DStreamDefinition streamDefinition;
            
            streamDefinition = (T8DStreamDefinition)definition.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            for (T8ExcelFileInputFieldDefinition fieldDefinition : definition.getFieldDefinitions())
            {
                String fieldIdentifier;
                
                fieldIdentifier = fieldDefinition.getIdentifier();
                fieldIdentifier = fieldIdentifier.substring(1);
                streamDefinition.addStreamParameterDefinition(new T8DataParameterDefinition("$P_" + fieldIdentifier, null, null, T8DataType.STRING));
            }
        }
    }
}
