/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.ui.table.export;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class ExportProperty
{
    public enum PropertyType
    {
        Text,
        Boolean,
        Number
    }

    private String propertyName;
    private PropertyType propertyType;
    private Object propertyValue;

    public ExportProperty(String propertyName, PropertyType propertyType)
    {
        this.propertyName = propertyName;
        this.propertyType = propertyType;
    }

    public ExportProperty(String propertyName, PropertyType propertyType, Object propertyValue)
    {
        this(propertyName, propertyType);
        this.propertyValue = propertyValue;
    }
    public String getPropertyName()
    {
        return propertyName;
    }

    public PropertyType getPropertyType()
    {
        return propertyType;
    }

    public void setPropertyValue(Object propertyValue)
    {
        this.propertyValue = propertyValue;
    }

    public Object getPropertyValue()
    {
        return propertyValue;
    }


}
