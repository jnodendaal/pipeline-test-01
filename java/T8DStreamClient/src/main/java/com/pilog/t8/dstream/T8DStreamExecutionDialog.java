package com.pilog.t8.dstream;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerOperation.T8ServerOperationStatus;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import com.pilog.t8.security.T8Context;
import java.awt.BorderLayout;
import java.awt.Window;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import static com.pilog.t8.definition.dstream.T8DStreamAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamExecutionDialog extends javax.swing.JDialog
{
    private final T8ConfigurationManager configurationManager;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DStreamDefinition streamDefinition;
    private final Map<String, Object> inputParameters;
    private final T8DStreamFileContext fileContext;
    private String operationIid;
    private long startTime;
    private boolean executionCompleted = false;

    public T8DStreamExecutionDialog(Window parent, T8Context context, Map<String, Object> inputParameters, T8DStreamDefinition streamDefinition, T8DStreamFileContext fileContext)
    {
        super(parent, ModalityType.APPLICATION_MODAL);
        this.clientContext = context.getClientContext();
        this.context = context;
        this.configurationManager = clientContext.getConfigurationManager();
        initComponents();
        this.setTitle(getTranslatedString(streamDefinition.getExecutionDialogHeaderText()));
        this.inputParameters = inputParameters;
        this.streamDefinition = streamDefinition;
        this.fileContext = fileContext;
        this.jXTitledPanelContent.getContentContainer().setLayout(new BorderLayout());
        this.jXTitledPanelContent.getContentContainer().add(jPanelProgressIndicators, java.awt.BorderLayout.CENTER);
        this.jXTitledPanelContent.setTitleFont(LAFConstants.MAIN_HEADER_FONT);
        this.jXTitledPanelContent.setTitleForeground(LAFConstants.MAIN_HEADER_TEXT_COLOR);
        this.jXTitledPanelContent.setTitle(streamDefinition.getExecutionDialogHeaderText());
        this.jXTitledPanelContent.revalidate();
        this.jLabelOverallProgressText.setText(getTranslatedString("Please click the Start button to begin the process."));

        setSize(500, 230);
        setLocationRelativeTo(null);
    }

    private String getTranslatedString(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    public static final boolean showDialog(Window parentWindow, T8Context context, Map<String, Object> inputParameters, T8DStreamDefinition streamDefinition, T8DStreamFileContext fileContext)
    {
        T8DStreamExecutionDialog dialog;

        dialog = new T8DStreamExecutionDialog(parentWindow, context, inputParameters, streamDefinition, fileContext);
        dialog.setVisible(true);

        return dialog.isExecutionCompleted();
    }

    private void updateProgress(final T8DStreamExecutionStatus executionStatus)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                if ((executionStatus == null) || (executionStatus.getOverallProgress() < 1))
                {
                    jLabelEstimatedTimeRemainingValue.setText("Undetermined");
                    jLabelElapsedTimeValue.setText("Undetermined");

                    jProgressBarOverall.setIndeterminate(true);
                    jProgressBarOverall.setStringPainted(false);
                    jLabelOverallProgressText.setText(getTranslatedString("Preparing source data..."));
                    jLabelOverallProgressText.invalidate();

                    jProgressBarPhase.setIndeterminate(true);
                    jProgressBarPhase.setStringPainted(false);
                    jLabelPhaseProgressText.setText(getTranslatedString("Preparing source data..."));
                    jLabelPhaseProgressText.invalidate();
                }
                else
                {
                    long estimatedDuration;
                    long remainingTime;
                    long elapsedTime;
                    double progress;

                    progress = executionStatus.getOverallProgress();
                    elapsedTime = System.currentTimeMillis() - startTime;
                    estimatedDuration = (long)(((double)elapsedTime) / progress * 100.00);
                    remainingTime = estimatedDuration - elapsedTime;

                    jLabelEstimatedTimeRemainingValue.setText(buildTimeString(remainingTime));
                    jLabelElapsedTimeValue.setText(buildTimeString(elapsedTime));

                    jProgressBarOverall.setIndeterminate(false);
                    jProgressBarOverall.setStringPainted(true);
                    jProgressBarOverall.setMinimum(0);
                    jProgressBarOverall.setValue((int) executionStatus.getOverallProgress());
                    jProgressBarOverall.setMaximum(100);
                    jLabelOverallProgressText.setText(executionStatus.getOverallProgressText());
                    jLabelOverallProgressText.invalidate();

                    jProgressBarPhase.setIndeterminate(false);
                    jProgressBarPhase.setStringPainted(true);
                    jProgressBarPhase.setMinimum(0);
                    jProgressBarPhase.setValue((int) executionStatus.getPhaseProgress());
                    jProgressBarPhase.setMaximum(100);
                    if(executionStatus.getPhaseIterationCount() > 0)
                    {
                        jProgressBarPhase.setString(executionStatus.getPhaseIterationsCompleted() + " of " + executionStatus.getPhaseIterationCount());
                    }
                    jLabelPhaseProgressText.setText(executionStatus.getPhaseProgressText());
                    jLabelPhaseProgressText.invalidate();
                }
            }
        });
    }

    private String buildTimeString(long millis)
    {
        StringBuffer timeString;
        long secondInMillis = 1000;
        long minuteInMillis = secondInMillis * 60;
        long hourInMillis = minuteInMillis * 60;
        long elapsedHours;
        long elapsedMinutes;
        long elapsedSeconds;
        long diff;

        diff = millis;
        elapsedHours = diff / hourInMillis;
        diff = diff % hourInMillis;
        elapsedMinutes = diff / minuteInMillis;
        diff = diff % minuteInMillis;
        elapsedSeconds = diff / secondInMillis;

        timeString = new StringBuffer();
        if (elapsedHours > 100)
        {
            timeString.append("undeterminable");
        }
        else
        {
            if (elapsedHours > 0)
            {
                timeString.append(elapsedHours);
                timeString.append(" hours, ");
            }

            if ((elapsedHours > 0) || (elapsedMinutes > 0))
            {
                timeString.append(elapsedMinutes);
                timeString.append(" minutes, ");
            }

            if ((elapsedHours > 0) || (elapsedMinutes > 0) || (elapsedSeconds > 0))
            {
                timeString.append(elapsedSeconds);
                timeString.append(" seconds");
            }
        }

        return timeString.toString();
    }

    private void startExecution()
    {
        HashMap<String, Object> operationParameters;

        // Update the UI.
        jButtonStart.setEnabled(false);
        jButtonStop.setEnabled(true);
        jButtonClose.setEnabled(false);
        jLabelOverallProgressText.setText(getTranslatedString("Preparing source data..."));

        // Create the operation parameters collection.
        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DSTREAM_DEFINITION, streamDefinition);
        operationParameters.put(PARAMETER_DSTREAM_FILE_CONTEXT, fileContext);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, inputParameters);

        // Start the process.
        try
        {
            T8ServerOperationStatusReport statusReport;

            startTime = System.currentTimeMillis();
            statusReport = T8MainServerClient.executeAsynchronousOperation(context, OPERATION_DSTREAM_EXECUTION_SERVER, operationParameters);
            operationIid = statusReport.getOperationInstanceIdentifier();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        // Start the progress checker thread;
        new Thread(new Checker()).start();
    }

    private void stopExecution()
    {
        try
        {
            // Update the UI.
            jButtonStart.setEnabled(false);
            jButtonStop.setEnabled(false);

            T8MainServerClient.stopOperation(context, operationIid);
            completeProcessing();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void completeProcessing()
    {
        // Enable the close button once the process has been completed.
        jButtonStart.setEnabled(false);
        jButtonStop.setEnabled(false);
        jButtonClose.setEnabled(true);
        jProgressBarPhase.setString(null);
        updateProgress(new T8DStreamExecutionStatus(100, getTranslatedString("Processing Complete."), 100, getTranslatedString("Processing Complete")));
        executionCompleted = true;
    }

    private class Checker implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                T8ServerOperationStatus status;

                status = T8ServerOperationStatus.IN_PROGRESS;
                while (status == T8ServerOperationStatus.IN_PROGRESS)
                {
                    T8ServerOperationStatusReport statusReport;
                    T8DStreamExecutionStatus statusObject;

                    statusReport = T8MainServerClient.getOperationStatus(context, operationIid);
                    statusObject = (T8DStreamExecutionStatus)statusReport.getProgressReportObject();
                    status = statusReport.getOperationStatus();

                    if (status == T8ServerOperationStatus.FAILED)
                    {
                        Throwable throwable;

                        throwable = statusReport.getOperationThrowable();
                        if (throwable != null)
                        {
                            if (throwable instanceof T8DStreamException)
                            {
                                T8DStreamExecutionFailureDialog.showFailureDialog(context, (T8DStreamException)throwable);
                            }
                            else
                            {
                                JOptionPane.showMessageDialog(T8DStreamExecutionDialog.this, ExceptionUtilities.getRootCauseMessage(throwable), getTranslatedString("Operation Failure"), JOptionPane.ERROR_MESSAGE);
                            }
                        }
                        else throw new Exception("Data Stream operation failed but no operation throwable was returned from the server.");

                        completeProcessing();
                        return; // End the checker thread.
                    }
                    else if (status == T8ServerOperationStatus.IN_PROGRESS)
                    {
                        updateProgress(statusObject);
                        Thread.sleep(500);
                    }
                    else
                    {
                        completeProcessing();
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                T8Log.log("An exception occurred in progress checker Thread.", e);
            }
        }
    }

    private void closeDialog()
    {
        dispose();
    }

    public boolean isExecutionCompleted()
    {
        return executionCompleted;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelProgressIndicators = new javax.swing.JPanel();
        jLabelOverall = new javax.swing.JLabel();
        jLabelOverallProgressText = new javax.swing.JLabel();
        jProgressBarOverall = new javax.swing.JProgressBar();
        jLabelPhase = new javax.swing.JLabel();
        jLabelPhaseProgressText = new javax.swing.JLabel();
        jProgressBarPhase = new javax.swing.JProgressBar();
        jPanelTimeRemaining = new javax.swing.JPanel();
        jLabelEstimatedTimeRemaining = new javax.swing.JLabel();
        jLabelEstimatedTimeRemainingValue = new javax.swing.JLabel();
        jLabelElapsedTime = new javax.swing.JLabel();
        jLabelElapsedTimeValue = new javax.swing.JLabel();
        jXTitledPanelContent = new org.jdesktop.swingx.JXTitledPanel();
        jPanelFooter = new javax.swing.JPanel();
        jButtonClose = new javax.swing.JButton();
        jButtonStop = new javax.swing.JButton();
        jButtonStart = new javax.swing.JButton();

        jPanelProgressIndicators.setLayout(new java.awt.GridBagLayout());

        jLabelOverall.setText("Overall:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanelProgressIndicators.add(jLabelOverall, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        jPanelProgressIndicators.add(jLabelOverallProgressText, gridBagConstraints);

        jProgressBarOverall.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 10, 10);
        jPanelProgressIndicators.add(jProgressBarOverall, gridBagConstraints);

        jLabelPhase.setText("Phase:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanelProgressIndicators.add(jLabelPhase, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        jPanelProgressIndicators.add(jLabelPhaseProgressText, gridBagConstraints);

        jProgressBarPhase.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 10, 10);
        jPanelProgressIndicators.add(jProgressBarPhase, gridBagConstraints);

        jPanelTimeRemaining.setOpaque(false);
        jPanelTimeRemaining.setLayout(new java.awt.GridBagLayout());

        jLabelEstimatedTimeRemaining.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelEstimatedTimeRemaining.setText("Time Remaining:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelTimeRemaining.add(jLabelEstimatedTimeRemaining, gridBagConstraints);

        jLabelEstimatedTimeRemainingValue.setText("0 hours, 0 minutes, 0 seconds");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        jPanelTimeRemaining.add(jLabelEstimatedTimeRemainingValue, gridBagConstraints);

        jLabelElapsedTime.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelElapsedTime.setText("Elapsed Time:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanelTimeRemaining.add(jLabelElapsedTime, gridBagConstraints);

        jLabelElapsedTimeValue.setText("0 hours, 0 minutes, 0 seconds");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 10);
        jPanelTimeRemaining.add(jLabelElapsedTimeValue, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelProgressIndicators.add(jPanelTimeRemaining, gridBagConstraints);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jXTitledPanelContent.setBorder(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        getContentPane().add(jXTitledPanelContent, gridBagConstraints);

        jPanelFooter.setLayout(new java.awt.GridBagLayout());

        jButtonClose.setText(getTranslatedString("Close"));
        jButtonClose.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCloseActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        jPanelFooter.add(jButtonClose, gridBagConstraints);

        jButtonStop.setText(getTranslatedString("Stop"));
        jButtonStop.setEnabled(false);
        jButtonStop.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonStopActionPerformed(evt);
            }
        });
        jPanelFooter.add(jButtonStop, new java.awt.GridBagConstraints());

        jButtonStart.setText(getTranslatedString("Start"));
        jButtonStart.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonStartActionPerformed(evt);
            }
        });
        jPanelFooter.add(jButtonStart, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        getContentPane().add(jPanelFooter, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonStopActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonStopActionPerformed
    {//GEN-HEADEREND:event_jButtonStopActionPerformed
        stopExecution();
    }//GEN-LAST:event_jButtonStopActionPerformed

    private void jButtonStartActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonStartActionPerformed
    {//GEN-HEADEREND:event_jButtonStartActionPerformed
        startExecution();
    }//GEN-LAST:event_jButtonStartActionPerformed

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCloseActionPerformed
    {//GEN-HEADEREND:event_jButtonCloseActionPerformed
        closeDialog();
    }//GEN-LAST:event_jButtonCloseActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JButton jButtonStart;
    private javax.swing.JButton jButtonStop;
    private javax.swing.JLabel jLabelElapsedTime;
    private javax.swing.JLabel jLabelElapsedTimeValue;
    private javax.swing.JLabel jLabelEstimatedTimeRemaining;
    private javax.swing.JLabel jLabelEstimatedTimeRemainingValue;
    private javax.swing.JLabel jLabelOverall;
    private javax.swing.JLabel jLabelOverallProgressText;
    private javax.swing.JLabel jLabelPhase;
    private javax.swing.JLabel jLabelPhaseProgressText;
    private javax.swing.JPanel jPanelFooter;
    private javax.swing.JPanel jPanelProgressIndicators;
    private javax.swing.JPanel jPanelTimeRemaining;
    private javax.swing.JProgressBar jProgressBarOverall;
    private javax.swing.JProgressBar jProgressBarPhase;
    private org.jdesktop.swingx.JXTitledPanel jXTitledPanelContent;
    // End of variables declaration//GEN-END:variables
}
