package com.pilog.t8.developer.definitions.actionhandlers.dstream;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationNodeDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRequirementMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DirectDataRequirementInstanceMappingDefinition;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.security.T8Context;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import static com.pilog.t8.definition.dstream.T8DStreamAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementInstanceMappingActionHandler implements T8DefinitionActionHandler
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DirectDataRequirementInstanceMappingDefinition definition;

    public T8DataRequirementInstanceMappingActionHandler(T8ClientContext clientContext, T8Context context, T8DirectDataRequirementInstanceMappingDefinition definition)
    {
        this.clientContext = clientContext;
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new DataRequirementInstanceMappingCreationAction(clientContext, context, definition));

        return actions;
    }

    private static class DataRequirementInstanceMappingCreationAction extends AbstractAction
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8DirectDataRequirementInstanceMappingDefinition definition;

        public DataRequirementInstanceMappingCreationAction(final T8ClientContext clientContext, final T8Context context, final T8DirectDataRequirementInstanceMappingDefinition definition)
        {
            this.clientContext = clientContext;
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Auto-Create Mapping from Data Requirement Instance");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/dstream/icons/databaseGoIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            if (!Strings.isNullOrEmpty(definition.getDataRequirementInstanceIdentifier()))
            {
                try
                {
                    List<T8DataRequirementMappingDefinition> mappingDefinitions;
                    T8DataRecordCreationNodeDefinition parentDefinition;

                    parentDefinition = (T8DataRecordCreationNodeDefinition)definition.getAncestorDefinition(T8DataRecordCreationNodeDefinition.TYPE_IDENTIFIER);
                    mappingDefinitions = createDataRequirementMappingDefinitions(clientContext, context, definition.getRootProjectId(), definition.getDataRequirementInstanceIdentifier());
                    if (mappingDefinitions != null)
                    {
                        for (T8DataRequirementMappingDefinition mappingDefinition : mappingDefinitions)
                        {
                            parentDefinition.removeDataRequirementMappingDefinition(mappingDefinition.getDataRequirementIdentifier());
                            parentDefinition.addDataRequirementMappingDefinition(mappingDefinition);
                        }
                    }
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while creating Data Requirement Mapping Definitions.", e);
                }
            }
            else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "The selected definition does not specify the applicable Data Requirement Instance.", "Incomplete Input", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static List<T8DataRequirementMappingDefinition> createDataRequirementMappingDefinitions(T8ClientContext clientContext, T8Context context, String projectId, String drInstanceId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DR_INSTANCE_ID, drInstanceId);
        return (List<T8DataRequirementMappingDefinition>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_DSTREAM_CREATE_DR_MAPPING_DEFINITIONS, operationParameters).get(PARAMETER_DR_MAPPING_DEFINITION_LIST);
    }
}
