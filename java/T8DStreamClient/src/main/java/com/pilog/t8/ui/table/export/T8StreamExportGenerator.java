/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.ui.table.export;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import java.util.List;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public interface T8StreamExportGenerator
{
    public final T8DataParameterDefinition filterParameterDefinition = new T8DataParameterDefinition(T8Definition.getLocalIdPrefix() + "DATA_FILTER", "Data Filter", "Data Filter", T8DataType.CUSTOM_OBJECT);

    public List<ExportProperty> getProperties();

    public T8DStreamDefinition createExportDefinition(String fileName, T8TableDefinition tableDefinition, T8DataFilter dataFilter) throws Exception;
}
