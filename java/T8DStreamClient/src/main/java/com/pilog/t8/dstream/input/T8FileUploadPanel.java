package com.pilog.t8.dstream.input;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.dstream.input.T8DStreamInputFileDefinition;
import com.pilog.t8.definition.dstream.input.T8DStreamInputPanelListener;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import java.awt.CardLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8FileUploadPanel extends InputPanel
{
    private final T8ComponentController controller;
    private final T8DStreamInputFileDefinition inputDefinition;
    private final T8Context context;
    private final T8FileManager fileManager;
    private final List<T8DStreamInputPanelListener> inputPanelListeners;
    private String localFilePath;
    private UploadCheckerThread uploadCheckerThread;
    private T8DStreamFileContext fileContext;

    private static File LAST_SELECTED_FILE = null;

    public T8FileUploadPanel(T8ComponentController controller, T8DStreamInputFileDefinition fileInputDefinition)
    {
        initComponents();
        this.controller = controller;
        this.context = controller.getContext();
        this.fileManager = controller.getClientContext().getFileManager();
        this.inputDefinition = fileInputDefinition;
        this.inputPanelListeners = new ArrayList<T8DStreamInputPanelListener>();
    }

    @Override
    public void initialize(T8DStreamFileContext fileContext, Map<String, Object> inputParameters)
    {
        this.fileContext = fileContext;
        this.jLabelFileName.setText(getRequiredFileName());
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public boolean validateInput()
    {
        return uploadCheckerThread == null ? true : uploadCheckerThread.isComplete();
    }

    @Override
    public Map<String, Object> getInputData()
    {
        return null;
    }

    @Override
    public void addInputListener(T8DStreamInputPanelListener listener)
    {
        inputPanelListeners.add(listener);
    }

    @Override
    public void removeInputListener(T8DStreamInputPanelListener listener)
    {
        inputPanelListeners.remove(listener);
    }

    private void uploadFile()
    {
        // Set the last selected file to the current folder if it has not already been set.
        if (LAST_SELECTED_FILE == null)
        {
            try
            {
                LAST_SELECTED_FILE = new File(new File(getRequiredFileName()).getCanonicalPath());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        // Get the target file path that identifies the file that will be uploaded.
        try
        {
            localFilePath = BasicFileChooser.getLoadFilePath(SwingUtilities.getWindowAncestor(this), null, "All Files", LAST_SELECTED_FILE);
            if (localFilePath != null)
            {
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            T8FileDetails fileDetails;

                            fileContext.addInputFileName(inputDefinition, new File(localFilePath).getName());
                            fileManager.createDirectory(context, fileContext.getFileContextInstanceIdentifier(), fileContext.getInputDirectoryPath(inputDefinition));
                            fileDetails = fileManager.uploadFile(context, fileContext.getFileContextInstanceIdentifier(), localFilePath, fileContext.getInputFilePath(inputDefinition));
                            jLabelStatusText.setText(fileDetails.getFileName() + " (" + fileDetails.getFileSize() + " bytes) uploaded successfully.");
                            stopUploadChecker();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            JOptionPane.showMessageDialog(T8FileUploadPanel.this, "The selected file could not be uploaded successfully.  Please try again.", "Upload Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }.start();

                // Show the progress of the upload.
                startUploadChecker();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(T8FileUploadPanel.this, "The selected file could not be uploaded successfully.  Please try again.", "Upload Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private String getRequiredFileName()
    {
        return inputDefinition.getFileName();
    }

    private void showStatusText()
    {
        CardLayout layout;

        layout = (CardLayout)jPanelStatus.getLayout();
        layout.show(jPanelStatus, "TEXT");
    }

    private void showStatusProgress()
    {
        CardLayout layout;

        layout = (CardLayout)jPanelStatus.getLayout();
        layout.show(jPanelStatus, "PROGRESS");
    }

    private void startUploadChecker()
    {
        showStatusProgress();
        uploadCheckerThread = new UploadCheckerThread();
        uploadCheckerThread.start();
    }

    private void stopUploadChecker()
    {
        if (uploadCheckerThread != null)
        {
            uploadCheckerThread.stopChecker();
        }

        showStatusText();
    }

    private class UploadCheckerThread extends Thread
    {
        private boolean stopFlag;
        private boolean complete;
        private int progress;

        public UploadCheckerThread()
        {
            stopFlag = false;
            complete = false;
            progress = 0;
        }

        @Override
        public void run()
        {
            SwingUtilities.invokeLater(new Runnable()
            {

                @Override
                public void run()
                {
                    jButtonUpload.setEnabled(false);
                    for (T8DStreamInputPanelListener t8DStreamInputPanelListener : inputPanelListeners)
                    {
                        t8DStreamInputPanelListener.inputStateChanged(T8FileUploadPanel.this);
                    }
                }
            });
            try
            {
                while ((progress < 100) && (!stopFlag))
                {
                    progress = fileManager.getUploadOperationProgress(context, localFilePath);
                    SwingUtilities.invokeLater(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            jProgressBarUpload.setValue(progress);
                        }
                    });

                    Thread.sleep(500);
                }
                complete = true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            SwingUtilities.invokeLater(new Runnable()
            {

                @Override
                public void run()
                {
                    jButtonUpload.setEnabled(true);
                    for (T8DStreamInputPanelListener t8DStreamInputPanelListener : inputPanelListeners)
                    {
                        t8DStreamInputPanelListener.inputStateChanged(T8FileUploadPanel.this);
                    }
                }
            });
        }

        public void stopChecker()
        {
            stopFlag = true;
        }

        public boolean isComplete()
        {
            return complete;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelRequiredFile = new javax.swing.JLabel();
        jLabelFileName = new javax.swing.JLabel();
        jLabelStatus = new javax.swing.JLabel();
        jPanelStatus = new javax.swing.JPanel();
        jPanelStatusText = new javax.swing.JPanel();
        jLabelStatusText = new org.jdesktop.swingx.JXLabel();
        jPanelStatusProgress = new javax.swing.JPanel();
        jProgressBarUpload = new javax.swing.JProgressBar();
        jButtonUpload = new javax.swing.JButton();

        setMinimumSize(null);
        setName(""); // NOI18N
        setPreferredSize(null);
        setLayout(new java.awt.GridBagLayout());

        jLabelRequiredFile.setText("Required File:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        add(jLabelRequiredFile, gridBagConstraints);

        jLabelFileName.setText("FileName");
        jLabelFileName.setMaximumSize(null);
        jLabelFileName.setMinimumSize(null);
        jLabelFileName.setName(""); // NOI18N
        jLabelFileName.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(jLabelFileName, gridBagConstraints);

        jLabelStatus.setText("Status:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        add(jLabelStatus, gridBagConstraints);

        jPanelStatus.setLayout(new java.awt.CardLayout());

        jPanelStatusText.setLayout(new java.awt.GridBagLayout());

        jLabelStatusText.setText("Please click on the Upload button to upload the required file");
        jLabelStatusText.setToolTipText("");
        jLabelStatusText.setLineWrap(true);
        jLabelStatusText.setMinimumSize(null);
        jLabelStatusText.setName(""); // NOI18N
        jLabelStatusText.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelStatusText.add(jLabelStatusText, gridBagConstraints);

        jPanelStatus.add(jPanelStatusText, "TEXT");

        jPanelStatusProgress.setLayout(new java.awt.GridBagLayout());

        jProgressBarUpload.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelStatusProgress.add(jProgressBarUpload, gridBagConstraints);

        jPanelStatus.add(jPanelStatusProgress, "PROGRESS");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(jPanelStatus, gridBagConstraints);

        jButtonUpload.setText("Upload");
        jButtonUpload.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonUploadActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        add(jButtonUpload, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonUploadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonUploadActionPerformed
    {//GEN-HEADEREND:event_jButtonUploadActionPerformed
        uploadFile();
    }//GEN-LAST:event_jButtonUploadActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonUpload;
    private javax.swing.JLabel jLabelFileName;
    private javax.swing.JLabel jLabelRequiredFile;
    private javax.swing.JLabel jLabelStatus;
    private org.jdesktop.swingx.JXLabel jLabelStatusText;
    private javax.swing.JPanel jPanelStatus;
    private javax.swing.JPanel jPanelStatusProgress;
    private javax.swing.JPanel jPanelStatusText;
    private javax.swing.JProgressBar jProgressBarUpload;
    // End of variables declaration//GEN-END:variables

}
