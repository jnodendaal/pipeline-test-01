/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.table.export.generator;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.input.T8DataEntityInputNodeDefinition;
import com.pilog.t8.definition.dstream.node.output.T8ExcelFileOutputNodeDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputFieldDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputSheetDefinition;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.ui.table.export.ExportProperty;
import com.pilog.t8.ui.table.export.T8StreamExportGenerator;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class ExcelStreamExportGenerator implements T8StreamExportGenerator
{
    private final List<ExportProperty> properties;

    public ExcelStreamExportGenerator()
    {
        properties = new ArrayList<>();
        properties.add(new ExportProperty("Sheet Name", ExportProperty.PropertyType.Text, "Data"));
    }

    @Override
    public List<ExportProperty> getProperties()
    {
        return properties;
    }

    @Override
    public T8DStreamDefinition createExportDefinition(String fileName, T8TableDefinition tableDefinition, T8DataFilter dataFilter)
    {
        T8DStreamDefinition streamDefinition;
        T8ExcelFileOutputDefinition outputDefinition;
        T8ExcelFileOutputSheetDefinition outputSheetDefinition;
        T8DataEntityInputNodeDefinition nodeDefinition;
        T8ExcelFileOutputNodeDefinition outputNodeDefinition;

        Map<String, String> outputPamareterMapping;
        Map<String, String> sheetOutputParameterMapping;

        streamDefinition = new T8DStreamDefinition(T8Definition.getLocalIdPrefix() + T8DStreamDefinition.IDENTIFIER_PREFIX + "TABLE_EXPORT");
        streamDefinition.addStreamParameterDefinition(filterParameterDefinition);
        streamDefinition.setLoggingEnabled(false);

        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                streamDefinition.addStreamParameterDefinition(new T8DataParameterDefinition(t8TableColumnDefinition.getIdentifier(), t8TableColumnDefinition.getColumnName(), t8TableColumnDefinition.getMetaDescription(), tableDefinition.getEntityDefinition().getFieldDataType(t8TableColumnDefinition.getFieldIdentifier())));
            }
        }

        outputDefinition = new T8ExcelFileOutputDefinition(T8Definition.getLocalIdPrefix() + T8ExcelFileOutputDefinition.IDENTIFIER_PREFIX + "TABLE_EXPORT_OUTPUT_NODE");
        outputSheetDefinition = new T8ExcelFileOutputSheetDefinition(T8Definition.getLocalIdPrefix() + T8ExcelFileOutputSheetDefinition.IDENTIFIER_PREFIX + "TABLE_EXPORT_OUTPUT_EXCEL_SHEET");

        outputSheetDefinition.setSheetName((String) (Strings.isNullOrEmpty((String)properties.get(0).getPropertyValue()) ? fileName : properties.get(0).getPropertyValue()));
        int fieldIndex = 0;
        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                T8ExcelFileOutputFieldDefinition t8ExcelFileOutputFieldDefinition;

                t8ExcelFileOutputFieldDefinition = new T8ExcelFileOutputFieldDefinition(T8Definition.getLocalIdPrefix() + T8ExcelFileOutputFieldDefinition.IDENTIFIER_PREFIX + fieldIndex++);
                t8ExcelFileOutputFieldDefinition.setExcelColumnIdentifier(t8TableColumnDefinition.getColumnName());

                outputSheetDefinition.addFieldDefinition(t8ExcelFileOutputFieldDefinition);
            }
        }

        outputDefinition.setFileName(fileName.endsWith(".xlsx") ? fileName
                                     : fileName + ".xlsx");
        outputDefinition.setSheetDefinitions(Arrays.asList(outputSheetDefinition));

        streamDefinition.setOutputDefinitions(Arrays.asList(outputDefinition));

        nodeDefinition = new T8DataEntityInputNodeDefinition(T8Definition.getLocalIdPrefix() + "TABLE_EXPORT_ROOT_NODE");
        outputPamareterMapping = new HashMap<>();

        nodeDefinition.setDataEntityIdentifier(tableDefinition.getDataEntityIdentifier());
        nodeDefinition.setDataFilterInputParameterIdentifier(filterParameterDefinition.getIdentifier());

        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                T8DataEntityFieldDefinition fieldDefinition;

                fieldDefinition = tableDefinition.getEntityDefinition().getFieldDefinition(t8TableColumnDefinition.getFieldIdentifier());
                outputPamareterMapping.put(fieldDefinition.getPublicIdentifier(), t8TableColumnDefinition.getIdentifier());
            }
        }

        nodeDefinition.setOutputParameterMapping(outputPamareterMapping);

        outputNodeDefinition = new T8ExcelFileOutputNodeDefinition(T8Definition.getLocalIdPrefix() + "TABLE_EXPORT_OUTPUT_NODE");

        outputNodeDefinition.setOutputIdentifier(outputDefinition.getIdentifier());
        outputNodeDefinition.setSheetIdentifier(outputSheetDefinition.getIdentifier());

        sheetOutputParameterMapping = new HashMap<>();

        fieldIndex = 0;
        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                T8ExcelFileOutputFieldDefinition fieldDefinition;

                fieldDefinition = outputSheetDefinition.getFieldDefinitions().get(fieldIndex++);
                sheetOutputParameterMapping.put(t8TableColumnDefinition.getIdentifier(), fieldDefinition.getIdentifier());
            }
        }

        outputNodeDefinition.setInputParameterMapping(sheetOutputParameterMapping);

        nodeDefinition.setChildNodeDefinitions(Arrays.asList(outputNodeDefinition));

        streamDefinition.setRootNodeDefinitions(Arrays.asList(nodeDefinition));

        return streamDefinition;
    }

}
