package com.pilog.t8.ui.table.export;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.definition.ui.table.export.T8TableExporter;
import com.pilog.t8.dstream.T8DStreamExecutor;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.ui.table.export.T8StreamExportGeneratorFactory.FileType;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.components.messagedialogs.Toast.Style;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;
import javax.swing.text.NumberFormatter;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8TableExportConfigurator extends JDialog implements T8TableExporter
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8TableExportConfigurator.class);

    private final T8ClientContext clientContext;
    private final T8SessionContext sessionContext;
    private final T8ComponentController controller;
    private final T8DataFilter dataFilter;
    private final T8TableDefinition t8TableDefinition;

    private T8StreamExportGenerator exportGenerator;

    public T8TableExportConfigurator(T8ComponentController controller, T8DataFilter dataFilter, T8TableDefinition tableDefinition)
    {
        super(controller.getParentWindow(), ModalityType.APPLICATION_MODAL);
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.sessionContext = controller.getSessionContext();
        this.dataFilter = dataFilter;
        this.t8TableDefinition = tableDefinition;

        initComponents();

        setSelectedFileType(FileType.Excel);
        jXTextFieldFileName.setText(tableDefinition.getMetaDisplayName());

        pack();
        setLocationRelativeTo(controller.getParentWindow());
    }

    @Override
    public void export()
    {
        setVisible(true);
    }

    private void doExport()
    {
        //Verify that a valid filename is specified
        if (Strings.isNullOrEmpty(jXTextFieldFileName.getText()))
        {
            jXTextFieldFileName.setBackground(LAFConstants.INVALID_PINK);
            JOptionPane.showMessageDialog(this, translate("Please enter a file name before attempting to continue."), translate("No File Name"), JOptionPane.ERROR_MESSAGE);
            return;
        }
        else
        {
            jXTextFieldFileName.setBackground(Color.white);
        }

        //Create a new export definition using the specified export generator and export the file using the dstream
        if (exportGenerator != null)
        {
            try
            {
                T8DStreamDefinition createExportDefinition;
                createExportDefinition = exportGenerator.createExportDefinition(jXTextFieldFileName.getText(), t8TableDefinition, dataFilter);

                T8DStreamExecutor executor;
                Map<String, Object> streamParameters;

                executor = new T8DStreamExecutor(controller);
                streamParameters = new HashMap<>();
                streamParameters.put(createExportDefinition.getIdentifier() + T8StreamExportGenerator.filterParameterDefinition.getIdentifier(), dataFilter);

                setVisible(false);
                executor.executeStream(createExportDefinition, streamParameters, controller.getParentWindow());
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to perform the export", ex);
                Toast.makeText(this, translate(ex.getMessage()), Style.ERROR).display();
            }
        }
    }

    private void setSelectedFileType(FileType type)
    {
        exportGenerator = T8StreamExportGeneratorFactory.getGenerator(type);

        if (exportGenerator.getProperties() != null && !exportGenerator.getProperties().isEmpty())
        {
            jPanelFileTypeSettings.removeAll();
            for (final ExportProperty exportProperty : exportGenerator.getProperties())
            {
                JLabel propertyLabel;
                JComponent propertyValue;

                switch (exportProperty.getPropertyType())
                {
                    case Boolean:
                    {
                        propertyValue = new JCheckBox();
                        ((JCheckBox) propertyValue).setSelected((Boolean) exportProperty.getPropertyValue());
                        break;
                    }
                    case Number:
                    {
                        propertyValue = new JFormattedTextField(new NumberFormatter());
                        ((JTextComponent) propertyValue).setText((String) exportProperty.getPropertyValue());
                        break;
                    }
                    case Text:
                    {
                        propertyValue = new JTextField();
                        ((JTextComponent) propertyValue).setText((String) exportProperty.getPropertyValue());
                        break;
                    }
                    default: throw new IllegalStateException("Unhandled type detected. Type: " + exportProperty.getPropertyType());
                }

                propertyLabel = new JLabel(translate(exportProperty.getPropertyName()));

                propertyValue.addFocusListener(new FocusListener()
                {
                    @Override
                    public void focusGained(FocusEvent e)
                    {
                    }

                    @Override
                    public void focusLost(FocusEvent e)
                    {
                        Component component = e.getComponent();
                        if (component instanceof JCheckBox)
                        {
                            exportProperty.setPropertyValue(((JCheckBox) component).isSelected());
                        }
                        else if (component instanceof JTextComponent)
                        {
                            exportProperty.setPropertyValue(((JTextComponent) component).getText());
                        }
                    }
                });

                jPanelFileTypeSettings.add(propertyLabel);
                jPanelFileTypeSettings.add(propertyValue);
            }
            jPanelFileTypeSettings.setVisible(true);
        }
        else
        {
            jPanelFileTypeSettings.removeAll();
            jPanelFileTypeSettings.setVisible(false);
        }
    }

    private String translate(String text)
    {
        return controller.translate(text);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupFileType = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelBasicSettings = new javax.swing.JPanel();
        jXButtonExport = new org.jdesktop.swingx.JXButton();
        jPanelSettings = new javax.swing.JPanel();
        jPanelFileType = new javax.swing.JPanel();
        jRadioButtonExcel = new javax.swing.JRadioButton();
        jRadioButtonText = new javax.swing.JRadioButton();
        jRadioButtonXml = new javax.swing.JRadioButton();
        jRadioButtonDataFile = new javax.swing.JRadioButton();
        jPanelFileName = new javax.swing.JPanel();
        jXTextFieldFileName = new org.jdesktop.swingx.JXTextField();
        jPanelFileTypeSettings = new javax.swing.JPanel();

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));

        jPanelBasicSettings.setBackground(new java.awt.Color(255, 255, 255));
        jPanelBasicSettings.setOpaque(false);
        jPanelBasicSettings.setPreferredSize(new java.awt.Dimension(550, 230));
        jPanelBasicSettings.setLayout(new java.awt.GridBagLayout());

        jXButtonExport.setText(translate("Export")
        );
        jXButtonExport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonExportActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelBasicSettings.add(jXButtonExport, gridBagConstraints);

        jPanelSettings.setOpaque(false);
        jPanelSettings.setLayout(new java.awt.GridBagLayout());

        jPanelFileType.setBorder(javax.swing.BorderFactory.createTitledBorder(translate("File Type")
        ));
        jPanelFileType.setName(""); // NOI18N
        jPanelFileType.setOpaque(false);
        jPanelFileType.setPreferredSize(new java.awt.Dimension(300, 60));
        jPanelFileType.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        buttonGroupFileType.add(jRadioButtonExcel);
        jRadioButtonExcel.setSelected(true);
        jRadioButtonExcel.setText(translate("Excel"));
        jRadioButtonExcel.setOpaque(false);
        jRadioButtonExcel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jRadioButtonExcelActionPerformed(evt);
            }
        });
        jPanelFileType.add(jRadioButtonExcel);

        buttonGroupFileType.add(jRadioButtonText);
        jRadioButtonText.setText(translate("Text File")
        );
        jRadioButtonText.setOpaque(false);
        jRadioButtonText.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jRadioButtonTextActionPerformed(evt);
            }
        });
        jPanelFileType.add(jRadioButtonText);

        buttonGroupFileType.add(jRadioButtonXml);
        jRadioButtonXml.setText(translate("XML File")
        );
        jRadioButtonXml.setEnabled(false);
        jRadioButtonXml.setOpaque(false);
        jRadioButtonXml.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jRadioButtonXmlActionPerformed(evt);
            }
        });
        jPanelFileType.add(jRadioButtonXml);

        buttonGroupFileType.add(jRadioButtonDataFile);
        jRadioButtonDataFile.setText(translate("Data File")
        );
        jRadioButtonDataFile.setEnabled(false);
        jRadioButtonDataFile.setOpaque(false);
        jRadioButtonDataFile.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jRadioButtonDataFileActionPerformed(evt);
            }
        });
        jPanelFileType.add(jRadioButtonDataFile);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSettings.add(jPanelFileType, gridBagConstraints);

        jPanelFileName.setBorder(javax.swing.BorderFactory.createTitledBorder(translate("File Name")
        ));
        jPanelFileName.setName(""); // NOI18N
        jPanelFileName.setOpaque(false);
        jPanelFileName.setLayout(new java.awt.BorderLayout());

        jXTextFieldFileName.setToolTipText("");
        jXTextFieldFileName.setPrompt(translate("File Name")
        );
        jPanelFileName.add(jXTextFieldFileName, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSettings.add(jPanelFileName, gridBagConstraints);

        jPanelFileTypeSettings.setBorder(javax.swing.BorderFactory.createTitledBorder(translate("File Type Properties")
        ));
        jPanelFileTypeSettings.setOpaque(false);
        jPanelFileTypeSettings.setLayout(new java.awt.GridLayout(0, 2));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSettings.add(jPanelFileTypeSettings, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelBasicSettings.add(jPanelSettings, gridBagConstraints);

        jTabbedPane1.addTab("Basic Settings", jPanelBasicSettings);

        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jRadioButtonDataFileActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jRadioButtonDataFileActionPerformed
    {//GEN-HEADEREND:event_jRadioButtonDataFileActionPerformed
        setSelectedFileType(FileType.Data);
    }//GEN-LAST:event_jRadioButtonDataFileActionPerformed

    private void jRadioButtonXmlActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jRadioButtonXmlActionPerformed
    {//GEN-HEADEREND:event_jRadioButtonXmlActionPerformed
        setSelectedFileType(FileType.XML);
    }//GEN-LAST:event_jRadioButtonXmlActionPerformed

    private void jRadioButtonTextActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jRadioButtonTextActionPerformed
    {//GEN-HEADEREND:event_jRadioButtonTextActionPerformed
        setSelectedFileType(FileType.Text);
    }//GEN-LAST:event_jRadioButtonTextActionPerformed

    private void jRadioButtonExcelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jRadioButtonExcelActionPerformed
    {//GEN-HEADEREND:event_jRadioButtonExcelActionPerformed
        setSelectedFileType(FileType.Excel);
    }//GEN-LAST:event_jRadioButtonExcelActionPerformed

    private void jXButtonExportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonExportActionPerformed
    {//GEN-HEADEREND:event_jXButtonExportActionPerformed
        doExport();
    }//GEN-LAST:event_jXButtonExportActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupFileType;
    private javax.swing.JPanel jPanelBasicSettings;
    private javax.swing.JPanel jPanelFileName;
    private javax.swing.JPanel jPanelFileType;
    private javax.swing.JPanel jPanelFileTypeSettings;
    private javax.swing.JPanel jPanelSettings;
    private javax.swing.JRadioButton jRadioButtonDataFile;
    private javax.swing.JRadioButton jRadioButtonExcel;
    private javax.swing.JRadioButton jRadioButtonText;
    private javax.swing.JRadioButton jRadioButtonXml;
    private javax.swing.JTabbedPane jTabbedPane1;
    private org.jdesktop.swingx.JXButton jXButtonExport;
    private org.jdesktop.swingx.JXTextField jXTextFieldFileName;
    // End of variables declaration//GEN-END:variables
}
