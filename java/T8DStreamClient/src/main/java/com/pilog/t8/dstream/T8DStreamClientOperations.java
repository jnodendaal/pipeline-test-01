package com.pilog.t8.dstream;

import com.pilog.t8.client.T8DefaultSynchronousClientOperation;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.operation.java.T8JavaClientOperationDefinition;
import java.util.Map;

import static com.pilog.t8.definition.dstream.T8DStreamAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamClientOperations
{
    public static class DStreamExecutionOperation extends T8DefaultSynchronousClientOperation
    {
        public DStreamExecutionOperation(T8ComponentController controller, T8JavaClientOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(controller, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(T8SessionContext sessionContext, Map<String, Object> operationParameters) throws Exception
        {
            T8DStreamExecutor executor;
            String streamId;
            Map<String, Object> inputParameters;

            streamId = (String)operationParameters.get(PARAMETER_DSTREAM_DEFINITION_IDENTIFIER);
            inputParameters = (Map<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETERS);
            executor = new T8DStreamExecutor(controller);
            executor.executeStream(streamId, inputParameters, controller.getParentWindow());
            return null;
        }
    }
}
