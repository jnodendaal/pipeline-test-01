package com.pilog.t8.dstream;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.ParserContext;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.time.T8Day;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.dstream.T8DStreamAPIHandler;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.input.StreamInputDialog;
import com.pilog.t8.dstream.output.StreamOutputDialog;
import com.pilog.t8.security.T8Context;
import java.awt.Window;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.swing.JOptionPane;

import static com.pilog.t8.definition.dstream.T8DStreamAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamExecutor
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DStreamExecutor.class);

    private final T8ComponentController controller;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private T8SessionContext sessionContext;
    private final T8FileManager fileManager;
    private String fileContextIdentifier;

    public T8DStreamExecutor(T8ComponentController controller)
    {
        this.controller = controller;
        this.context = controller.getContext();
        this.clientContext = controller.getClientContext();
        this.fileManager = clientContext.getFileManager();
    }

    public void executeStream(String streamId, Map<String, Object> streamInputParameters, Window parentWindow)
    {
        try
        {
            T8DStreamDefinition streamDefinition;

            streamDefinition = (T8DStreamDefinition)clientContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), streamId, null);
            if (streamDefinition != null)
            {
                executeStream(streamDefinition, streamInputParameters, parentWindow);
            }
            else throw new Exception("Data Stream definition not found: " + streamId);
        }
        catch (Exception e)
        {
            LOGGER.log(e);
            JOptionPane.showMessageDialog(parentWindow, "Data Stream could not be loaded from database.  Execution aborted.", "Data Stream Execution", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void executeStream(T8DStreamDefinition streamDefinition, Map<String, Object> streamInputParameters, Window parentWindow)
    {
        try
        {
            Map<String, Object> executionInputParameters;
            T8DStreamFileContext fileContext;

            // Log the execution.
            LOGGER.log("Executing stream '" + streamDefinition + "' using input parameters: " + streamInputParameters);

            // Create a new GUID that will uniquely identify this execution session.
            this.fileContextIdentifier = getNewGUID();
            this.sessionContext = sessionContext;

            // Create a new file context on the server.
            fileContext = new T8DStreamFileContext(fileContextIdentifier);
            fileManager.openFileContext(context, fileContextIdentifier, null, new T8Day(1));
            fileManager.createDirectory(context, fileContextIdentifier, fileContext.getInputDirectoryPath());
            fileManager.createDirectory(context, fileContextIdentifier, fileContext.getOutputDirectoryPath());

            // Make sure we have an input parameters collection.
            executionInputParameters = new HashMap<>();
            if (streamInputParameters != null) executionInputParameters.putAll(streamInputParameters);

            // If the stream requires user input, display the input dialog.
            if (streamDefinition.getInputDefinitions().size() > 0)
            {
                Map<String, Object> additionalInputParameters;

                additionalInputParameters = StreamInputDialog.getInputData(controller, streamDefinition, fileContext, streamInputParameters);
                if (additionalInputParameters == null) return; // User canceled the operation.
                else executionInputParameters.putAll(additionalInputParameters);
            }

            // Show the Stream Execution Dialog to handle stream execution.
            executionInputParameters = T8IdentifierUtilities.prependNamespace(streamDefinition.getNamespace(), executionInputParameters, false);
            if(streamDefinition.isExecuteAsProcess())
            {
                HashMap<String, Object> operationParameters;
                T8ProcessDetails processDetails;

                operationParameters = new HashMap<>();
                operationParameters.put(PARAMETER_DSTREAM_DEFINITION, streamDefinition);
                operationParameters.put(PARAMETER_DSTREAM_FILE_CONTEXT, fileContext);
                operationParameters.put(PARAMETER_INPUT_PARAMETERS, executionInputParameters);
                processDetails = clientContext.getProcessManager().startProcess(context, T8DStreamAPIHandler.PROCESS_DSTREAM_EXECUTION_SERVER, T8IdentifierUtilities.prependNamespace(T8DStreamAPIHandler.PROCESS_DSTREAM_EXECUTION_SERVER, operationParameters, false));
            }
            else
            {
                if(T8DStreamExecutionDialog.showDialog(parentWindow, context, executionInputParameters, streamDefinition, fileContext))
                {
                    // If the stream has any outputs, show the output dialog.
                    if ((streamDefinition.getOutputDefinitions().size() > 0) || (streamDefinition.isLoggingEnabled()))
                    {
                        StreamOutputDialog.showOutputData(parentWindow, context, streamDefinition, fileContext);
                    }
                }
            }
        }
        catch (T8DStreamException e)
        {
            LOGGER.log("While executing DStream.", e);
            JOptionPane.showMessageDialog(parentWindow, e.getMessage(), "Stream Error", JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception e)
        {
            LOGGER.log("While executing DStream.", e);
            JOptionPane.showMessageDialog(parentWindow, "An unexpected error prevented successful stream execution.", "Stream Error", JOptionPane.ERROR_MESSAGE);
        }
        finally
        {
            try
            {
                //If this stream is run as a process do not close the context now, let the server do it
                if(!streamDefinition.isExecuteAsProcess()) fileManager.closeFileContext(context, fileContextIdentifier);
            }
            catch (Exception e)
            {
                LOGGER.log("Failed to close file context " + fileContextIdentifier, e);
            }
        }
    }

    private static String getNewGUID()
    {
        UUID id;

        id = UUID.randomUUID();
        return id.toString().replace("-", "").toUpperCase();
    }

    private HashMap<String, Object> evaluateParameterExpressions(HashMap<String, String> parameterExpressions) throws Exception
    {
        ExpressionEvaluator expressionEvaluator;
        HashMap<String, Object> expressionParameters;
        HashMap<String, Object> parameterValues;

        expressionParameters = new HashMap();
        expressionParameters.put("SESSION", sessionContext);

        parameterValues = new HashMap<String, Object>();
        expressionEvaluator = new ExpressionEvaluator();
        for (String parameterName : parameterExpressions.keySet())
        {
            ParserContext context;
            Object value;

            context = new ParserContext();
            context.addVariableDeclarations(expressionParameters.keySet());
            expressionEvaluator.compileExpression(parameterName, context);
            value = expressionEvaluator.evaluateExpression(expressionParameters, null);
            parameterValues.put(parameterName, value);
        }

        return parameterValues;
    }
}
