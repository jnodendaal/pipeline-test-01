package com.pilog.t8.developer.definitions.actionhandlers.dstream;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationNodeDefinition;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordCreationNodeActionHandler implements T8DefinitionActionHandler
{
    private T8ClientContext clientContext;
    private T8SessionContext sessionContext;
    private T8DataRecordCreationNodeDefinition definition;
    
    public T8DataRecordCreationNodeActionHandler(T8ClientContext clientContext, T8SessionContext sessionContext, T8DataRecordCreationNodeDefinition definition)
    {
        this.clientContext = clientContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
    }
    
    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;
        
        actions = new ArrayList<javax.swing.Action>();
        actions.add(new TestAction(clientContext, sessionContext, definition));
        
        return actions;
    }
    
    private static class TestAction extends AbstractAction
    {
        private T8ClientContext clientContext;
        private T8SessionContext sessionContext;
        private T8DataRecordCreationNodeDefinition definition;
        
        public TestAction(final T8ClientContext clientContext, final T8SessionContext sessionContext, final T8DataRecordCreationNodeDefinition definition)
        {
            this.clientContext = clientContext;
            this.sessionContext = sessionContext;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Auto-Create Mapping from Data Requirement");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/dstream/icons/databaseGoIcon.png")));
        }
        
        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                
            }
            catch (Exception e)
            {
                T8Log.log("Exception while scanning content of Excel file.", e);
            }
        }
    }
}
