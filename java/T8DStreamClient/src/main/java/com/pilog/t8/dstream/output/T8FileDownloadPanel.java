package com.pilog.t8.dstream.output;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.definition.dstream.output.T8DStreamOutputDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import java.awt.CardLayout;
import java.io.File;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8FileDownloadPanel extends OutputPanel
{
    private T8Context context;
    private T8FileManager fileManager;
    private String localFilePath;
    private UploadCheckerThread uploadCheckerThread;
    private T8FileDetails remoteFileDetails;
    private T8DStreamFileContext fileContext;

    private static File LAST_SELECTED_FOLDER = null;

    public T8FileDownloadPanel(T8Context context, T8FileDetails remoteFileDetails)
    {
        initComponents();
        this.context = context;
        this.fileManager = context.getClientContext().getFileManager();
        this.remoteFileDetails = remoteFileDetails;
    }

    @Override
    public void initialize(T8DStreamOutputDefinition outputDefinition, T8DStreamFileContext fileContext)
    {
        this.fileContext = fileContext;
        this.jLabelFileName.setText(remoteFileDetails.getFileName() + " (" + remoteFileDetails.getFileSize() + " bytes)");
    }

    private void downloadFile()
    {
        // Get the target file path to which the file will be downloaded.
        try
        {
            File selectedFile;

            // Determine the preselected file in the file chooser.
            if (LAST_SELECTED_FOLDER != null) selectedFile = new File(LAST_SELECTED_FOLDER.getCanonicalFile() + "/" + remoteFileDetails.getFileName());
            else selectedFile = new File(new File(remoteFileDetails.getFileName()).getCanonicalPath());

            // Get the local file path to which the file will be downloaded.
            localFilePath = BasicFileChooser.getSaveFilePath(SwingUtilities.getWindowAncestor(this), null, "All Files", selectedFile, BasicFileChooser.FileSelectionMode.FILES);
            if (localFilePath != null)
            {
                // Update the last selected folder path.
                LAST_SELECTED_FOLDER = new File(localFilePath);
                if (LAST_SELECTED_FOLDER.getParentFile() != null) LAST_SELECTED_FOLDER = LAST_SELECTED_FOLDER.getParentFile();

                // Execute the downloader thread.
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            long fileSize;

                            fileSize = fileManager.downloadFile(context, fileContext.getFileContextInstanceIdentifier(), localFilePath, remoteFileDetails.getFilePath());
                            jLabelStatusText.setText(fileSize + " bytes downloaded successfully");
                            stopDownloadChecker();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            JOptionPane.showMessageDialog(T8FileDownloadPanel.this, "The selected file could not be downloaded successfully.  Please try again.", "Download Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }.start();

                // Show the progress of the upload.
                startDownloadChecker();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(T8FileDownloadPanel.this, "The selected file could not be downloaded successfully.  Please try again.", "Download Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void showStatusText()
    {
        CardLayout layout;

        layout = (CardLayout)jPanelStatus.getLayout();
        layout.show(jPanelStatus, "TEXT");
    }

    private void showStatusProgress()
    {
        CardLayout layout;

        layout = (CardLayout)jPanelStatus.getLayout();
        layout.show(jPanelStatus, "PROGRESS");
    }

    private void startDownloadChecker()
    {
        showStatusProgress();
        uploadCheckerThread = new UploadCheckerThread();
        uploadCheckerThread.start();
    }

    private void stopDownloadChecker()
    {
        if (uploadCheckerThread != null)
        {
            uploadCheckerThread.stopChecker();
        }

        showStatusText();
    }

    private class UploadCheckerThread extends Thread
    {
        private boolean stopFlag;
        private int progress;

        public UploadCheckerThread()
        {
            stopFlag = false;
            progress = 0;
        }

        @Override
        public void run()
        {
            try
            {
                while ((progress < 100) && (!stopFlag))
                {
                    progress = fileManager.getDownloadOperationProgress(context, localFilePath);
                    SwingUtilities.invokeLater(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            jProgressBarUpload.setValue(progress);
                        }
                    });

                    Thread.sleep(500);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        public void stopChecker()
        {
            stopFlag = true;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelOutputFile = new javax.swing.JLabel();
        jLabelFileName = new javax.swing.JLabel();
        jLabelStatus = new javax.swing.JLabel();
        jPanelStatus = new javax.swing.JPanel();
        jPanelStatusText = new javax.swing.JPanel();
        jLabelStatusText = new javax.swing.JLabel();
        jPanelStatusProgress = new javax.swing.JPanel();
        jProgressBarUpload = new javax.swing.JProgressBar();
        jButtonDownload = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jLabelOutputFile.setText("Output File:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        add(jLabelOutputFile, gridBagConstraints);

        jLabelFileName.setText("FileName");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(jLabelFileName, gridBagConstraints);

        jLabelStatus.setText("Status:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        add(jLabelStatus, gridBagConstraints);

        jPanelStatus.setLayout(new java.awt.CardLayout());

        jPanelStatusText.setLayout(new java.awt.GridBagLayout());

        jLabelStatusText.setText("Please click on the Download button to download the file");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelStatusText.add(jLabelStatusText, gridBagConstraints);

        jPanelStatus.add(jPanelStatusText, "TEXT");

        jPanelStatusProgress.setLayout(new java.awt.GridBagLayout());

        jProgressBarUpload.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelStatusProgress.add(jProgressBarUpload, gridBagConstraints);

        jPanelStatus.add(jPanelStatusProgress, "PROGRESS");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(jPanelStatus, gridBagConstraints);

        jButtonDownload.setText("Download");
        jButtonDownload.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDownloadActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        add(jButtonDownload, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonDownloadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDownloadActionPerformed
    {//GEN-HEADEREND:event_jButtonDownloadActionPerformed
        downloadFile();
    }//GEN-LAST:event_jButtonDownloadActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDownload;
    private javax.swing.JLabel jLabelFileName;
    private javax.swing.JLabel jLabelOutputFile;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JLabel jLabelStatusText;
    private javax.swing.JPanel jPanelStatus;
    private javax.swing.JPanel jPanelStatusProgress;
    private javax.swing.JPanel jPanelStatusText;
    private javax.swing.JProgressBar jProgressBarUpload;
    // End of variables declaration//GEN-END:variables

}
