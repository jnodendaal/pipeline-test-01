/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.table.export;

import com.pilog.t8.ui.table.export.generator.DataFileStreamExportGenerator;
import com.pilog.t8.ui.table.export.generator.ExcelStreamExportGenerator;
import com.pilog.t8.ui.table.export.generator.TextStreamExportGenerator;
import com.pilog.t8.ui.table.export.generator.XMLStreamExportGenerator;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8StreamExportGeneratorFactory
{
    public enum FileType
    {
        Excel,
        Text,
        XML,
        Data
    }

    public static T8StreamExportGenerator getGenerator(FileType fileType)
    {
        switch(fileType)
        {
            case Data: return new DataFileStreamExportGenerator();
            case Excel: return new ExcelStreamExportGenerator();
            case Text: return new TextStreamExportGenerator();
            case XML: return new XMLStreamExportGenerator();
            default: return null;
        }
    }
}
