package com.pilog.t8.developer.definitions.actionhandlers.dstream;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationNodeDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRequirementMappingDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import static com.pilog.t8.definition.dstream.T8DStreamAPIHandler.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementMappingActionHandler implements T8DefinitionActionHandler
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DataRequirementMappingDefinition definition;

    public T8DataRequirementMappingActionHandler(T8ClientContext clientContext, T8Context context, T8DataRequirementMappingDefinition definition)
    {
        this.clientContext = clientContext;
        this.context = context;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new DataRequirementMappingCreationAction(clientContext, context, definition));
        return actions;
    }

    private static class DataRequirementMappingCreationAction extends AbstractAction
    {
        private final T8ClientContext clientContext;
        private final T8Context context;
        private final T8DataRequirementMappingDefinition definition;

        public DataRequirementMappingCreationAction(final T8ClientContext clientContext, final T8Context context, final T8DataRequirementMappingDefinition definition)
        {
            this.clientContext = clientContext;
            this.context = context;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Auto-Create Mapping from Data Requirement");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/dstream/icons/databaseGoIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            if (!Strings.isNullOrEmpty(definition.getDataRequirementIdentifier()))
            {
                try
                {
                    List<T8DataRequirementMappingDefinition> mappingDefinitions;
                    T8DataRecordCreationNodeDefinition parentDefinition;

                    parentDefinition = (T8DataRecordCreationNodeDefinition)definition.getParentDefinition();
                    mappingDefinitions = createDataRequirementMappingDefinitions(context, definition.getRootProjectId(), definition.getDataRequirementIdentifier());
                    if (mappingDefinitions != null)
                    {
                        for (T8DataRequirementMappingDefinition mappingDefinition : mappingDefinitions)
                        {
                            parentDefinition.removeDataRequirementMappingDefinition(mappingDefinition.getDataRequirementIdentifier());
                            parentDefinition.addDataRequirementMappingDefinition(mappingDefinition);
                        }
                    }
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while creating Data Requirement Mapping Definitions.", e);
                }
            }
            else JOptionPane.showMessageDialog(clientContext.getParentWindow(), "The selected definition does not specify the applicable Data Requirement.", "Incomplete Input", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static List<T8DataRequirementMappingDefinition> createDataRequirementMappingDefinitions(T8Context context, String projectId, String drId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DR_ID, drId);
        return (List<T8DataRequirementMappingDefinition>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_DSTREAM_CREATE_DR_MAPPING_DEFINITIONS, operationParameters).get(PARAMETER_DR_MAPPING_DEFINITION_LIST);
    }
}
