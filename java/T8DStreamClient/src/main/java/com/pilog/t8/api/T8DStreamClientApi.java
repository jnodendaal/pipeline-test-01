package com.pilog.t8.api;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.dstream.T8DStreamExecutor;
import java.util.Map;

/**
 *
 * @author Pieter Strydom
 */
public class T8DStreamClientApi implements T8Api
{
    private final T8ComponentController controller;

    public static final String API_IDENTIFIER = "@API_DSTREAM_CLIENT";

    public T8DStreamClientApi(T8ComponentController controller)
    {
        this.controller = controller;
    }

    public void executeStream(String streamId, Map<String, Object> inputParameters)
    {
        T8DStreamExecutor executor;

        executor = new T8DStreamExecutor(controller);
        executor.executeStream(streamId, inputParameters, controller.getParentWindow());
    }
}
