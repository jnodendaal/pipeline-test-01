/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.table.export.generator;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.dstream.node.input.T8DataEntityInputNodeDefinition;
import com.pilog.t8.definition.dstream.node.output.T8TextFileOutputNodeDefinition;
import com.pilog.t8.definition.dstream.output.T8DStreamOutputDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputDefinition;
import com.pilog.t8.definition.dstream.output.textfile.T8TextFileOutputDefinition;
import com.pilog.t8.definition.dstream.output.textfile.T8TextFileOutputFieldDefinition;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.ui.table.export.ExportProperty;
import com.pilog.t8.ui.table.export.T8StreamExportGenerator;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class TextStreamExportGenerator implements T8StreamExportGenerator
{
    private final List<ExportProperty> propertys;

    public TextStreamExportGenerator()
    {
        propertys = new ArrayList<ExportProperty>();

        propertys.add(new ExportProperty("Seperator", ExportProperty.PropertyType.Text));
        propertys.add(new ExportProperty("Extension", ExportProperty.PropertyType.Text, ".txt"));
    }

    @Override
    public List<ExportProperty> getProperties()
    {
        return propertys;
    }

    @Override
    public T8DStreamDefinition createExportDefinition(String fileName,
                                                      T8TableDefinition tableDefinition,
                                                      T8DataFilter dataFilter)
    {
        T8DStreamDefinition streamDefinition;
        T8TextFileOutputDefinition outputDefinition;
        T8DataEntityInputNodeDefinition nodeDefinition;
        T8TextFileOutputNodeDefinition outputNodeDefinition;

        Map<String, String> outputPamareterMapping;
        Map<String, String> outputParameterMappings;

        streamDefinition = new T8DStreamDefinition(T8Definition.getLocalIdPrefix() + T8DStreamDefinition.IDENTIFIER_PREFIX + "TABLE_EXPORT");
        streamDefinition.addStreamParameterDefinition(filterParameterDefinition);
        streamDefinition.setLoggingEnabled(false);

        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                streamDefinition.addStreamParameterDefinition(new T8DataParameterDefinition(t8TableColumnDefinition.getIdentifier(), t8TableColumnDefinition.getColumnName(), t8TableColumnDefinition.getMetaDescription(), tableDefinition.getEntityDefinition().getFieldDataType(t8TableColumnDefinition.getFieldIdentifier())));
            }
        }

        outputDefinition = new T8TextFileOutputDefinition(T8Definition.getLocalIdPrefix() + T8ExcelFileOutputDefinition.IDENTIFIER_PREFIX + "TABLE_EXPORT_OUTPUT_NODE");

        outputDefinition.setSeparatorString((String) (Strings.isNullOrEmpty((String)propertys.get(0).getPropertyValue())
                                                      ? "\t" : propertys.get(0).getPropertyValue()));

        int fieldIndex = 0;
        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                T8TextFileOutputFieldDefinition t8TextFileOutputFieldDefinition = new T8TextFileOutputFieldDefinition(T8Definition.getLocalIdPrefix() + T8TextFileOutputFieldDefinition.IDENTIFIER_PREFIX + fieldIndex++);
                t8TextFileOutputFieldDefinition.setTextColumnIdentifier(t8TableColumnDefinition.getColumnName());

                outputDefinition.addFieldDefinition(t8TextFileOutputFieldDefinition);
            }
        }

        outputDefinition.setFileName((String) (Strings.isNullOrEmpty((String)propertys.get(1).getPropertyValue()) ? ".txt"
                                               : propertys.get(1).getPropertyValue()));

        streamDefinition.setOutputDefinitions(Arrays.asList((T8DStreamOutputDefinition) outputDefinition));

        nodeDefinition = new T8DataEntityInputNodeDefinition(T8Definition.getLocalIdPrefix() + "TABLE_EXPORT_ROOT_NODE");
        outputPamareterMapping = new HashMap<String, String>();

        nodeDefinition.setDataEntityIdentifier(tableDefinition.getDataEntityIdentifier());
        nodeDefinition.setDataFilterInputParameterIdentifier(filterParameterDefinition.getIdentifier());

        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                T8DataEntityFieldDefinition fieldDefinition;

                fieldDefinition = tableDefinition.getEntityDefinition().getFieldDefinition(t8TableColumnDefinition.getFieldIdentifier());
                outputPamareterMapping.put(fieldDefinition.getPublicIdentifier(), t8TableColumnDefinition.getIdentifier());
            }
        }

        nodeDefinition.setOutputParameterMapping(outputPamareterMapping);

        outputNodeDefinition = new T8TextFileOutputNodeDefinition(T8Definition.getLocalIdPrefix() + "TABLE_EXPORT_OUTPUT_NODE");

        outputNodeDefinition.setOutputIdentifier(outputDefinition.getIdentifier());

        outputParameterMappings = new HashMap<String, String>();

        fieldIndex = 0;
        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                T8TextFileOutputFieldDefinition fieldDefinition;

                fieldDefinition = outputDefinition.getFieldDefinitions().get(fieldIndex++);
                outputParameterMappings.put(t8TableColumnDefinition.getIdentifier(), fieldDefinition.getIdentifier());
            }
        }

        outputNodeDefinition.setInputParameterMapping(outputParameterMappings);

        nodeDefinition.setChildNodeDefinitions(Arrays.asList((T8DStreamNodeDefinition) outputNodeDefinition));

        streamDefinition.setRootNodeDefinitions(Arrays.asList((T8DStreamNodeDefinition) nodeDefinition));

        return streamDefinition;
    }

}
