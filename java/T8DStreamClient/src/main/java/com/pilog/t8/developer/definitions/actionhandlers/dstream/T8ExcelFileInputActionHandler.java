package com.pilog.t8.developer.definitions.actionhandlers.dstream;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputFieldDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputSheetDefinition;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelFileInputActionHandler implements T8DefinitionActionHandler
{
    private T8ClientContext clientContext;
    private T8SessionContext sessionContext;
    private T8ExcelFileInputDefinition definition;

    public T8ExcelFileInputActionHandler(T8ClientContext clientContext, T8SessionContext sessionContext, T8ExcelFileInputDefinition definition)
    {
        this.clientContext = clientContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
    }

    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;

        actions = new ArrayList<javax.swing.Action>();
        actions.add(new ExcelFileContentScanAction(clientContext, sessionContext, definition));

        return actions;
    }

    private static class ExcelFileContentScanAction extends AbstractAction
    {
        private T8ClientContext clientContext;
        private T8SessionContext sessionContext;
        private T8ExcelFileInputDefinition definition;

        public ExcelFileContentScanAction(final T8ClientContext clientContext, final T8SessionContext sessionContext, final T8ExcelFileInputDefinition definition)
        {
            this.clientContext = clientContext;
            this.sessionContext = sessionContext;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Auto-Create Excel input from existing file");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/dstream/icons/databaseGoIcon.png")));
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            clientContext.getParentWindow().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            try
            {
                String fileName;

                fileName = BasicFileChooser.getLoadFilePath(clientContext.getParentWindow(), ".xlsx", "Excel Files");
                definition.setFileName(new File(fileName).getName());
                Workbook excelWorkbook = WorkbookFactory.create(new File(fileName));
                for (int i = 0; i < excelWorkbook.getNumberOfSheets(); i++)
                {
                    Sheet excelSheet = excelWorkbook.getSheetAt(i);

                    //Create a new sheet definition using the sheet name
                    String sheetName = convertToT8CompatableDefinitionName(T8ExcelFileInputSheetDefinition.IDENTIFIER_PREFIX,excelSheet.getSheetName());
                    T8ExcelFileInputSheetDefinition sheetDefinition = new T8ExcelFileInputSheetDefinition(sheetName);
                    sheetDefinition.setSheetName(excelSheet.getSheetName());
                    //Get the first row in the sheet and use it as column names
                    Row columnRow = excelSheet.getRow(excelSheet.getFirstRowNum());

                    //Iterate through the cells and use their contents as field names
                    Iterator<Cell> cellIterator = columnRow.cellIterator();
                    while(cellIterator.hasNext())
                    {
                        Cell nextCell = cellIterator.next();

                        //Create a new field definition from the cell information
                        String cellContents = convertToT8CompatableDefinitionName(T8ExcelFileInputFieldDefinition.IDENTIFIER_PREFIX, nextCell.getStringCellValue());
                        T8ExcelFileInputFieldDefinition inputFieldDefinition = new T8ExcelFileInputFieldDefinition(cellContents);
                        inputFieldDefinition.setExcelColumnIdentifier(nextCell.getStringCellValue());
                        inputFieldDefinition.setDataTypeString(T8DtString.IDENTIFIER);
                        sheetDefinition.addFieldDefinition(inputFieldDefinition);
                    }
                    definition.addSheetDefinition(sheetDefinition);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while scanning content of Excel file.", e);
            }
            clientContext.getParentWindow().setCursor(Cursor.getDefaultCursor());
        }

        private String convertToT8CompatableDefinitionName(String prefix, String name)
        {
            return T8Definition.getLocalIdPrefix() + prefix + name.toUpperCase().replaceAll(" ", "_");
        }
    }
}
