package com.pilog.t8.developer.definitions.actionhandlers.dstream;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionActionHandler;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputFieldDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputSheetDefinition;
import com.pilog.t8.definition.dstream.node.input.T8ExcelFileInputNodeDefinition;
import com.pilog.t8.utilities.strings.FuzzyStringUtilities;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelFileInputNodeActionHandler implements T8DefinitionActionHandler
{
    private T8ClientContext clientContext;
    private T8SessionContext sessionContext;
    private T8ExcelFileInputNodeDefinition definition;
    
    public T8ExcelFileInputNodeActionHandler(T8ClientContext clientContext, T8SessionContext sessionContext, T8ExcelFileInputNodeDefinition definition)
    {
        this.clientContext = clientContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
    }
    
    @Override
    public List<Action> getDefinitionEditorActions()
    {
        ArrayList<javax.swing.Action> actions;
        
        actions = new ArrayList<javax.swing.Action>();
        actions.add(new MapExcelFieldsToStreamParametersAction(clientContext, sessionContext, definition));
        
        return actions;
    }
    
    private static class MapExcelFieldsToStreamParametersAction extends AbstractAction
    {
        private T8ClientContext clientContext;
        private T8SessionContext sessionContext;
        private T8ExcelFileInputNodeDefinition definition;
        
        public MapExcelFieldsToStreamParametersAction(final T8ClientContext clientContext, final T8SessionContext sessionContext, final T8ExcelFileInputNodeDefinition definition)
        {
            this.clientContext = clientContext;
            this.sessionContext = sessionContext;
            this.definition = definition;
            this.putValue(Action.SHORT_DESCRIPTION, "Auto-map all Excel Fields to the closest matching Stream parameters.");
            this.putValue(Action.SMALL_ICON, new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/dstream/icons/databaseGoIcon.png")));
        }
        
        @Override
        public void actionPerformed(ActionEvent event)
        {
            String matchPrecision;
            
            matchPrecision = JOptionPane.showInputDialog(clientContext.getParentWindow(), "Please enter a matching precision (0.0 < precision < 1.0).");
            if (matchPrecision != null)
            {
                try
                {
                    T8DStreamDefinition streamDefinition;
                    T8ExcelFileInputSheetDefinition sheetDefinition;
                    Map<String, String> mapping;
                    double precision;

                    precision = Double.parseDouble(matchPrecision);
                    mapping = new HashMap<String, String>();
                    streamDefinition = (T8DStreamDefinition)definition.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
                    sheetDefinition = (T8ExcelFileInputSheetDefinition)definition.getLocalDefinition(definition.getSheetIdentifier());
                    if (sheetDefinition != null)
                    {
                        for (T8ExcelFileInputFieldDefinition fieldDefinition : sheetDefinition.getFieldDefinitions())
                        {
                            String fieldIdentifier;

                            fieldIdentifier = fieldDefinition.getIdentifier();
                            for (T8DataParameterDefinition parameterDefinition : streamDefinition.getStreamParameterDefinitions())
                            {
                                String parameterIdentifier;

                                parameterIdentifier = parameterDefinition.getIdentifier();
                                if (FuzzyStringUtilities.computeSimilarity(fieldIdentifier, parameterIdentifier) > precision)
                                {
                                    mapping.put(fieldIdentifier, parameterIdentifier);
                                }
                            }
                        }
                    }

                    definition.setOutputParameterMapping(mapping);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while trying to match Excel field identifiers to stream parameter identifiers.", e);
                }
            }
        }
    }
}
