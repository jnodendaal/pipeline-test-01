package com.pilog.t8.dstream;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionTestHarness;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamDefinitionTestHarness implements T8DefinitionTestHarness
{
    @Override
    public void testDefinition(T8Context context, T8Definition streamDefinition)
    {
        T8ComponentController controller;

        controller = new T8DefaultComponentController(new T8Context(context).setProjectId(streamDefinition.getRootProjectId()), streamDefinition.getIdentifier(), false);
        controller.start();

        try
        {
            T8DStreamExecutor executor;
            T8DStreamDefinition initializedStreamDefinition;

            // Get the initialized stream definition.
            initializedStreamDefinition = (T8DStreamDefinition)context.getClientContext().getDefinitionManager().initializeDefinition(context, streamDefinition, null);
            executor = new T8DStreamExecutor(controller);
            executor.executeStream(initializedStreamDefinition, null, controller.getClientContext().getParentWindow());
        }
        catch (Exception e)
        {
            T8Log.log("Exception while testing DStream Definition: " + streamDefinition, e);
        }
        finally
        {
            controller.stop();
        }
    }

    @Override
    public void configure(T8Context tcc)
    {
    }
}
