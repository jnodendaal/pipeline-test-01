package com.pilog.version;

/**
 * @author  Bouwer du Preez
 */
public class T8DStreamClientVersion
{
    public final static String VERSION = "89";
}
