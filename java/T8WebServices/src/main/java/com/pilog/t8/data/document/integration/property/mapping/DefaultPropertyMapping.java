package com.pilog.t8.data.document.integration.property.mapping;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import com.pilog.t8.definition.data.document.integration.property.T8DefaultPropertyMappingDefinition;
import com.pilog.t8.webservices.document.integration.ObjectFactory;
import com.pilog.t8.webservices.document.integration.RecordContent;
import com.pilog.t8.webservices.document.integration.RecordContent.Property;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.pilog.t8.definition.data.document.integration.property.T8DefaultPropertyMappingDefinition.MappingTypes.*;

/**
 * @author Hennie Brink
 */
public class DefaultPropertyMapping implements DataRecordIntegrationPropertyMapper<RecordContent.Property>
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8DefaultPropertyMappingDefinition definition;
    private TerminologyProvider terminologyProvider;
    private OntologyProvider ontologyProvider;
    private T8DataRecordPropertyValueStringGenerator valueStringGenerator;
    private ObjectFactory factory;

    public DefaultPropertyMapping(T8ServerContext serverContext, T8SessionContext sessionContext, T8DefaultPropertyMappingDefinition definition)
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
    }

    @Override
    public void initialize(TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, T8DataRecordPropertyValueStringGenerator valueStringGenerator)
    {
        this.terminologyProvider = terminologyProvider;
        this.ontologyProvider = ontologyProvider;
        this.valueStringGenerator = valueStringGenerator;

        this.factory = new ObjectFactory();
    }

    @Override
    public RecordContent.Property doPropertyMapping(DataRecord dataRecord)
    {
        RecordProperty recordProperty;
        Property property;

        recordProperty = dataRecord.getRecordProperty(definition.getPropertyID());
        property = factory.createRecordContentProperty();
        property.setPropertyTerm(definition.getInterfaceTerm());

        //Check if the recordProperty has valid content, excluding the FFT value.
        if(recordProperty != null && recordProperty.hasContent(false))
        {
            //Set the property value depending on the type defined in the definition
            switch(definition.getMappingType())
            {
                case CODE:
                        property.setValue(getPropertyValueConceptTerminology(recordProperty).getCode());
                    break;
                case CONCEPT:
                        property.setValue(getPropertyValueConceptTerminology(recordProperty).getConceptId());
                    break;
                case TERM:
                        property.setValue(getPropertyValueConceptTerminology(recordProperty).getTerm());
                    break;
                case ABBREVIATION:
                        property.setValue(getPropertyValueConceptTerminology(recordProperty).getAbbreviation());
                    break;
                case DEFINITION:
                        property.setValue(getPropertyValueConceptTerminology(recordProperty).getDefinition());
                    break;
                default:
                    if (definition.isDateValue())
                    {
                        Date dateValue;
                        SimpleDateFormat dateFormat = new SimpleDateFormat(definition.getDatePattern());

                        dateValue = new Date(Long.parseLong(valueStringGenerator.generateValueString(recordProperty).toString()));
                        property.setValue(dateFormat.format(dateValue));
                    }
                    else
                        property.setValue(valueStringGenerator.generateValueString(recordProperty).toString());
            }
        }

        return property;
    }

    private T8ConceptTerminology getPropertyValueConceptTerminology(RecordProperty property)
    {
        DataRecord parentRecord;
        T8ConceptTerminology terminology;
        String conceptID;


        parentRecord = property.getParentDataRecord();
        conceptID = getPropertyValueDescendantConceptID(property);

        //We need a valid concept ID for codes, so make sure that we found one
        if(conceptID == null) throw new RuntimeException(String.format("No concept ID value found for property %s on record %s", property.getPropertyID(), parentRecord.getID()));

        //First try and find the concept terminology on the data record self, if it is not found then ask the terminology provider
        terminology = terminologyProvider.getTerminology(null, conceptID);

        //If the terminology is still null we recieved an invalid concept ID
        if(terminology == null) throw new RuntimeException(String.format("The concept id %s on property %s for record %s is invalid", conceptID, property.getPropertyID(), parentRecord.getID()));
        return terminology;
    }

    private String getPropertyValueDescendantConceptID(RecordProperty property)
    {
        RecordValue value;

        //Currently we only support the value group types for concept ID's
        value = property.getRecordValue();
        //value = property.getDescendantValue(RequirementType.ONTOLOGY_CLASS, null, null);
        if (value == null) value = property.getDescendantValue(RequirementType.ONTOLOGY_CLASS, null, null);
        return value != null ? value.getValue() : null;
    }

    @Override
    public List<Property> doPropertyMappings(DataRecord dataRecord) throws Exception
    {
        throw new UnsupportedOperationException("This method is only supported by a Custom Proeprty Mapping."); //To change body of generated methods, choose Tools | Templates.
    }

}
