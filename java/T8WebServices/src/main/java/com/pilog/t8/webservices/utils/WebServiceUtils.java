package com.pilog.t8.webservices.utils;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.mainserver.T8MainServer;
import javax.servlet.ServletContext;

/**
 * @author Gavin Boshoff
 */
public class WebServiceUtils
{    
    private static final T8Logger logger = T8Log.getLogger(WebServiceUtils.class);
    
    public static T8ServerContext getT8ServerContext(ServletContext servletContext)
    {
        return (T8ServerContext) servletContext.getAttribute(T8MainServer.T8_SERVER_CONTEXT_IDENTIFIER);
    }
    
    /*
        This method is to encode certain chars PI/PO cant resolve.
        For the first iteration and for the current requirement we just need to encode the following:
        ^ (circumflex accent)
    
    */
    public static String encodeInterfaceURL(String url)
    {
        String encodedURL;
        String context;
        String operation;
        
        encodedURL = "";
        
        //split the context from the main operation call for the URL.
        if (url.startsWith("https")) 
        {
            context = url.substring(0, url.indexOf("/", url.indexOf(":", 6)) + 1);
            operation = url.substring(url.indexOf("/", url.indexOf(":", 6)) + 1, url.length());
        }
        else if (url.startsWith("http")) 
        {
            context = url.substring(0, url.indexOf("/", url.indexOf(":", 5)) + 1);
            operation = url.substring(url.indexOf("/", url.indexOf(":", 5)) + 1, url.length());
        }
        else 
        {
            context = url.substring(0, url.indexOf("/", url.indexOf(":")) + 1);
            operation = url.substring(url.indexOf("/", url.indexOf(":", 0)) + 1, url.length());
        }            
            
        // print context and operation
        logger.log(T8Logger.Level.INFO, "context: " + context);
        logger.log(T8Logger.Level.INFO, "operation: " + operation);
                
        // print before encode
        logger.log(T8Logger.Level.INFO, "Original URL: " + url);
                
        // encode the url
        encodedURL += context + operation.replace("^", "%5E");
        
        // print after encode
        logger.log(T8Logger.Level.INFO, "Encoded URL: " + encodedURL);
                
        return encodedURL;
    }
}
