package com.pilog.t8.webservices.service;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.webservices.entities.exception.WebServiceException;
import com.pilog.t8.webservices.utils.WebServiceUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.mainserver.T8DefaultHttpServletRequest;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.authentication.T8AuthenticationResponse;
import com.pilog.t8.time.T8Hour;

/**
 * @author Hennie Brink
 */
@Path("DataRecordAttachment")
public class DataRecordAttachment
{
    private static final T8Logger LOGGER = T8Log.getLogger(DataRecordAttachment.class);

    @Context
    ServletConfig sc;
    @Context
    ServletContext servletContext;
    HttpServletRequest hreq;

    public DataRecordAttachment(@Context ServletContext servletContext, @Context HttpServletRequest httpServletRequest)
    {
        this.servletContext = servletContext;
        this.hreq = httpServletRequest;
    }

    @Path("/{AttachmentID}")
    @HEAD
    public Response getAttachmentOptions(@PathParam("AttachmentID") String attachmentId) throws Exception
    {
        T8DataRecordApi recApi;
        T8SessionContext sessionContext;
        T8ServerContext serverContext;
        T8FileDetails fileDetails;
        T8DataTransaction tx;
        T8Context context;

        try
        {
            try
            {
                T8AuthenticationResponse authenticationResponse;

                serverContext = WebServiceUtils.getT8ServerContext(servletContext);
                authenticationResponse = serverContext.getSecurityManager().authenticate(new T8DefaultHttpServletRequest(hreq));
                sessionContext = authenticationResponse.getSessionContext();
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to authenticate api", ex);
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }

            try
            {
                // Validate the input parameters.
                if (Strings.isNegativeInteger(attachmentId))
                {
                    throw new IllegalArgumentException("Invalid attachment ID");
                }

                // Create the data session for this thread
                context = serverContext.getSecurityManager().createContext(sessionContext);
                serverContext.getDataManager().openDataSession(context);

                tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
                recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
                fileDetails = recApi.retrieveAttachmentDetails(attachmentId);
            }
            finally
            {
                // Close the data session and access context for this thread
                serverContext.getDataManager().closeCurrentSession();
                serverContext.getSecurityManager().destroyContext();
            }

            if (fileDetails == null)
            {
                return Response.status(Response.Status.GONE).build();
            }

            return Response.ok()
                    .header("Content-Disposition", "attachment; filename=" + fileDetails.getFileName())
                    .header("Content-MD5", fileDetails.getMD5Checksum())
                    .header("Content-Length", fileDetails.getFileSize()).build();
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to get attachment file details for attachemnt " + attachmentId, ex);
            WebServiceException er = new WebServiceException(ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(er).build();
        }
    }

    @Path("/{AttachmentID}")
    @GET
    @Produces({MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_XML})
    public Response getAttachment(@PathParam("AttachmentID") String attachmentId) throws Exception
    {
        final T8SessionContext sessionContext;
        final T8ServerContext serverContext;
        final InputStream fileInputStream;
        final T8FileDetails fileDetails;
        final T8Context context;

        T8DataRecordApi recApi;
        StreamingOutput stream;
        T8DataTransaction tx;

        try
        {
            try
            {
                T8AuthenticationResponse authenticationResponse;

                serverContext = WebServiceUtils.getT8ServerContext(servletContext);
                authenticationResponse = serverContext.getSecurityManager().authenticate(new T8DefaultHttpServletRequest(hreq));
                sessionContext = authenticationResponse.getSessionContext();
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to authenticate api", ex);
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }

            //Validate the input parameters
            if (Strings.isNegativeInteger(attachmentId))
            {
                throw new IllegalArgumentException("Invalid attachment ID");
            }

            try
            {
                // Create the data session for this thread
                context = serverContext.getSecurityManager().createContext(sessionContext);
                serverContext.getDataManager().openDataSession(context);

                tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
                recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

                fileDetails = recApi.exportAttachment(attachmentId, T8IdentifierUtilities.createNewGUID(), null, null, null);
                fileInputStream = serverContext.getFileManager().getFileInputStream(context, fileDetails.getContextIid(), fileDetails.getFileName());
                stream = new StreamingOutput()
                {
                    @Override
                    public void write(OutputStream os) throws IOException, WebApplicationException
                    {
                        int read;
                        while ((read = fileInputStream.read()) != -1)
                        {
                            os.write(read);
                        }

                        fileInputStream.close();
                        os.close();
                        try
                        {
                            serverContext.getFileManager().closeFileContext(context, fileDetails.getContextIid());
                        }
                        catch (Exception ex)
                        {
                            LOGGER.log("Failed to close file context " + fileDetails, ex);
                        }
                    }
                };
            }
            finally
            {
                // Close the data session and access context for this thread
                serverContext.getDataManager().closeCurrentSession();
                serverContext.getSecurityManager().destroyContext();
            }

            return Response.ok(stream)
                    .header("Content-Disposition", "attachment; filename=" + fileDetails.getFileName())
                    .header("Content-MD5", fileDetails.getMD5Checksum())
                    .header("Content-Length", fileDetails.getFileSize()).build();
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to get attachment  " + attachmentId, ex);
            WebServiceException er = new WebServiceException(ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(er).build();
        }
    }

    @Path("{ContextInstanceID}/{AttachmentID}/{FileName}")
    @PUT
    @POST
    @Consumes("application/octet-stream")
    public Response setAttachment(@Context HttpServletRequest request, @PathParam("ContextInstanceID") String contextIid, @PathParam("AttachmentID") String attachmentID, @PathParam("FileName") String fileName, InputStream fileInputStream) throws Exception
    {
        T8ServerContext serverContext;
        T8SessionContext sessionContext;
        T8Context context;
        String filePath;

        try
        {
            try
            {
                T8AuthenticationResponse authenticationResponse;

                serverContext = WebServiceUtils.getT8ServerContext(servletContext);
                authenticationResponse = serverContext.getSecurityManager().authenticate(new T8DefaultHttpServletRequest(hreq));
                sessionContext = authenticationResponse.getSessionContext();
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to authenticate api", ex);
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }

            //Validate the input parameters
            if (Strings.isNegativeInteger(attachmentID))
            {
                throw new IllegalArgumentException("Invalid attachment ID");
            }

            // Create the data session for this thread.
            context = serverContext.getSecurityManager().createContext(sessionContext);

            if (serverContext.getFileManager().openFileContext(context, contextIid, null, new T8Hour(1)))
            {
                filePath = attachmentID + '/' + fileName;
                LOGGER.log(()->"Setting temporary file using ContextInstanceID {"+contextIid+"} - AttachmentID {"+attachmentID+"} - FileName {"+fileName+"} - Temporary Path {"+filePath+"}");
                try(OutputStream fileOutputStream = serverContext.getFileManager().getFileOutputStream(context, contextIid, filePath, false))
                {
                    int read;
                    while((read = fileInputStream.read()) != -1)
                    {
                        fileOutputStream.write(read);
                    }
                    fileOutputStream.close();
                }

                return Response.status(Response.Status.CREATED).build();
            }
            else
            {
                return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
            }
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to put attachment  " + attachmentID, ex);
            WebServiceException er = new WebServiceException(ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(er).build();
        }
        finally
        {
            fileInputStream.close();
        }
    }
}
