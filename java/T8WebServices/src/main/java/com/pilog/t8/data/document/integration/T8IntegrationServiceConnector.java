/**
 * Created on 09 Mar 2016, 11:02:36 AM
 */
package com.pilog.t8.data.document.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.remote.server.connection.T8SoapRemoteEndpointConnectionDefinition;
import com.pilog.t8.webservices.document.integration.Creations;
import com.pilog.t8.webservices.document.integration.Extensions;
import com.pilog.t8.webservices.document.integration.Flags;
import com.pilog.t8.webservices.document.integration.MDQMInterface;
import com.pilog.t8.webservices.document.integration.MDQMInterface_Service;
import com.pilog.t8.webservices.document.integration.Updates;
import java.net.URL;
import javax.xml.ws.BindingProvider;
import com.pilog.t8.definition.remote.server.connection.T8Connection;
import com.pilog.t8.webservices.utils.WebServiceUtils;

/**
 * @author Gavin Boshoff
 */
public class T8IntegrationServiceConnector implements T8Connection
{
    private final T8SoapRemoteEndpointConnectionDefinition definition;
    private final MDQMInterface servicePort;

    public T8IntegrationServiceConnector(T8ServerContext serverContext, T8SessionContext sessionContext, T8SoapRemoteEndpointConnectionDefinition definition)
    {
        this.definition = definition;
        this.servicePort = createInterfacePort();
    }

    /**
     * Builds and sets up the {@code MDQMInterface} port object. Authentication
     * details are only set if available.
     *
     * @return The {@code MDQMInterface} port object
     *
     * @throws Exception If the definition data is not available or the WSDL
     *      location URL is invalid
     */
    private MDQMInterface createInterfacePort()
    {
        MDQMInterface_Service service;
        MDQMInterface port;
        URL wsdlLocation;
        String serviceUrl;

        wsdlLocation = getClass().getResource("/META-INF/wsdl/MDQMInterfaceService.wsdl");

        service = new MDQMInterface_Service(wsdlLocation);
        port = service.getMDQMInterfacePort();
        
        //Set service URL and encode the URL
        serviceUrl = this.definition.getURL();
        serviceUrl = WebServiceUtils.encodeInterfaceURL(serviceUrl);

        //Set the endpoint address, since this will
        ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceUrl);
        //Only set the authentication details if there is any specified
        if (this.definition.getAuthUsername() != null && !this.definition.getAuthUsername().isEmpty())
        {
            ((BindingProvider) port).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, this.definition.getAuthUsername());
            ((BindingProvider) port).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, this.definition.getAuthPassword());
        }

        return port;
    }

    void executeCreate(Creations creations)
    {
        this.servicePort.create(creations);
    }

    void executeUpdate(Updates updates)
    {
        this.servicePort.update(updates);
    }

    void executeFlags(Flags flags)
    {
        this.servicePort.flag(flags);
    }

    void executeExtension(Extensions extensions)
    {
        this.servicePort.extend(extensions);
    }
}