package com.pilog.t8.webservices;

import com.pilog.t8.webservices.service.DataRecordAttachment;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Gavin Boshoff
 */
@ApplicationPath("Rest")
public class Webservice extends Application
{
    @Override
    public Set<Class<?>> getClasses()
    {
        Set<Class<?>> s = new HashSet<>();
        s.add(DataRecordAttachment.class);
        return s;
    }
}