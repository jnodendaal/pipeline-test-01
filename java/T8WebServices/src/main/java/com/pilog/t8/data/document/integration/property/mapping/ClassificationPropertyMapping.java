package com.pilog.t8.data.document.integration.property.mapping;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.definition.data.document.integration.T8DataRecordMappingDefinition;
import com.pilog.t8.webservices.document.integration.ObjectFactory;
import com.pilog.t8.webservices.document.integration.RecordContent;
import com.pilog.t8.webservices.document.integration.RecordContent.Property;
import java.util.List;

/**
 * @author Pieta
 */
public class ClassificationPropertyMapping implements DataRecordIntegrationPropertyMapper<RecordContent.Property>
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8DataRecordMappingDefinition definition;
    private TerminologyProvider terminologyProvider;
    private OntologyProvider ontologyProvider;
    private T8DataRecordPropertyValueStringGenerator valueStringGenerator;
    private ObjectFactory factory;

    public ClassificationPropertyMapping(T8ServerContext serverContext,
                                  T8SessionContext sessionContext,
                                  T8DataRecordMappingDefinition definition)
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
    }

    @Override
    public void initialize(TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, T8DataRecordPropertyValueStringGenerator valueStringGenerator)
    {
        this.terminologyProvider = terminologyProvider;
        this.ontologyProvider = ontologyProvider;
        this.valueStringGenerator = valueStringGenerator;

        this.factory = new ObjectFactory();
    }

    @Override
    public RecordContent.Property doPropertyMapping(DataRecord dataRecord) throws Exception
    {
        Property property;
        T8ConceptTerminology terminology;
        List<T8OntologyLink> ontologyLinks;
        String value;

        property = factory.createRecordContentProperty();
        property.setPropertyTerm(definition.getRecordClassificationPropertyTerm());

        ontologyLinks = dataRecord.getOntology().getOntologyLinks();

        for (T8OntologyLink ontologyLink : ontologyLinks)
        {
            if (ontologyLink.getOntologyStructureID().equals(definition.getRecordClassificationStructureID()))
            {
               terminology = terminologyProvider.getTerminology(null, ontologyLink.getOntologyClassID());

               if (terminology != null && terminology.getCode() != null) value = terminology.getCode();
               else value = "";

               property.setValue(value);
               break;
            }
        }

        return property;
    }

    @Override
    public List<RecordContent.Property> doPropertyMappings(DataRecord dataRecord) throws Exception
    {
        throw new UnsupportedOperationException("This method is only supported by a Custom Property Mapping."); //To change body of generated methods, choose Tools | Templates.
    }

}
