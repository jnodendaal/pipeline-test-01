package com.pilog.t8.data.document.integration;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8OrganizationApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.definition.data.document.integration.T8DataRecordFlagKeyMappingDefinition;
import com.pilog.t8.definition.data.document.integration.T8DataRecordFlagMappingDefinition;
import com.pilog.t8.definition.data.document.integration.T8DataRecordMappingDefinition;
import com.pilog.t8.definition.data.document.integration.T8DataRecordPropertyMappingDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationServiceDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationServiceOperationMappingDefinition;
import com.pilog.t8.definition.data.document.integration.operation.T8IntegrationCreationOperationDefinition;
import com.pilog.t8.definition.data.document.integration.operation.T8IntegrationExtensionOperationDefinition;
import com.pilog.t8.definition.data.document.integration.operation.T8IntegrationSetFlagsOperationDefinition;
import com.pilog.t8.definition.data.document.integration.operation.T8IntegrationUpdateOperationDefinition;
import com.pilog.t8.definition.remote.server.connection.T8SoapRemoteEndpointConnectionDefinition;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.webservices.document.integration.*;
import com.pilog.t8.webservices.document.integration.Record.SubRecords;
import com.pilog.t8.webservices.document.integration.RecordContent.Property;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datastring.T8DataString;
import com.pilog.t8.definition.data.document.integration.T8DataRecordDescriptionMappingDefinition;
import com.pilog.t8.definition.data.document.integration.T8DataRecordExternalNumberingDefinition;
import com.pilog.t8.definition.data.document.integration.T8DataRecordExternalNumberingDefinition.ExternalNumberTypes;
import com.pilog.t8.definition.data.document.integration.organization.T8DataRecordOrganizationMappingDefinition;
import com.pilog.t8.definition.data.document.integration.T8SubDataRecordMappingDefinition;
import com.pilog.t8.definition.data.document.integration.custom.T8CustomMappingDefinition;
import com.pilog.t8.definition.data.document.integration.organization.T8DataRecordDocPathOrganizationMappingDefinition;
import com.pilog.t8.definition.data.document.integration.organization.T8DataRecordEpicOrganizationMappingDefinition;
import com.pilog.t8.definition.data.document.integration.property.T8DefaultPropertyMappingDefinition;
import com.pilog.t8.definition.data.document.integration.property.T8ExpressionPropertyMappingDefinition;
import com.pilog.t8.definition.data.key.T8SequentialDataKeyGeneratorDefinition;
import com.pilog.t8.webservices.document.integration.RecordDescriptions.Description;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.security.T8Context;
import java.util.Optional;

/**
 * @author Andre Scheepers
 */
public class T8DefaultIntegrationService implements DataRecordIntegrationService<Record>
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultIntegrationService.class);

    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8IntegrationServiceDefinition definition;
    private final ObjectFactory factory;
    private final T8IntegrationServicePersistenceHandler persistenceHandler;
    private T8IntegrationServiceOperationMappingDefinition operationDefinition;
    private T8DataTransaction tx;
    private T8DataRecordApi recApi;
    private T8OntologyApi ontApi;
    private T8OrganizationApi orgApi;
    private T8TerminologyApi trmApi;
    private TerminologyProvider terminologyProvider;
    private OntologyProvider ontologyProvider;
    private T8DataRecordPropertyValueStringGenerator valueStringGenerator;
    private DocPathExpressionEvaluator evaluator;
    private T8ServerExpressionEvaluator expressionEvaluator;
    private Map<String, String> inputParameters;

    private Boolean isPropertyClassificationIncluded = false;

    public T8DefaultIntegrationService(T8Context context, T8IntegrationServiceDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
        this.factory = new ObjectFactory();
        this.persistenceHandler = new T8IntegrationServicePersistenceHandler(context);
        this.inputParameters = new HashMap<>();
    }

    @Override
    public void initialize(T8DataTransaction tx, String operationIdentifier)
    {
        //Create some defaults that will be used
        this.orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        this.ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        this.orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        this.trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
        this.terminologyProvider = trmApi.getTerminologyProvider();
        this.ontologyProvider = ontApi.getOntologyProvider();

        //Configure and setup Doc Path Evaluator
        this.evaluator = new DocPathExpressionEvaluator();
        this.evaluator.setOntologyProvider(ontologyProvider);
        this.evaluator.setTerminologyProvider(terminologyProvider);

        //Configure the value string generator
        this.valueStringGenerator = new T8DataRecordPropertyValueStringGenerator();
        this.valueStringGenerator.setTerminologyProvider(terminologyProvider);
        this.valueStringGenerator.setIncludeDataRequirementConcepts(false);
        this.valueStringGenerator.setIncludeDataRequirementInstanceConcepts(false);
        this.valueStringGenerator.setIncludeSectionConcepts(false);
        this.valueStringGenerator.setPropertyConceptRenderType(T8DataRecordValueStringGenerator.ConceptRenderType.ABBREVIATION);

        //Set the tx that will be used for the load of the marked independent records.
        this.tx = tx;

        //Set the language ID that will be used for terminologies etc
        setLanguageID(orgApi.getDefaultContentLanguageId());

        //Get the operation definition that will be required for the record mappings
        operationDefinition = (T8IntegrationServiceOperationMappingDefinition) definition.getSubDefinition(T8IdentifierUtilities.stripNamespace(operationIdentifier));

        persistenceHandler.setOperationID(operationIdentifier);

        this.expressionEvaluator = new T8ServerExpressionEvaluator(context);
    }

    @Override
    public String executeOperation(Map<String, Object> parameters) throws Exception
    {
        T8IntegrationServiceConnector connector;
        Map<String, Object> operationParameters;

        operationParameters = T8IdentifierUtilities.stripNamespace(parameters);
        connector = getConnector();

        if (operationDefinition instanceof T8IntegrationCreationOperationDefinition)
        {
            return executeCreation(connector, operationParameters);
        }
        else if (operationDefinition instanceof T8IntegrationUpdateOperationDefinition)
        {
            return executeUpdate(connector, operationParameters);
        }
        else if (operationDefinition instanceof T8IntegrationSetFlagsOperationDefinition)
        {
            return executeSetFlags(connector, operationParameters);
        }
        else if (operationDefinition instanceof T8IntegrationExtensionOperationDefinition)
        {
            return executeExtension(connector, operationParameters);
        }
        else
        {
            throw new Exception("Unsupported operation type " + operationDefinition.getClass());
        }
    }

    private T8IntegrationServiceConnector getConnector() throws Exception
    {
        T8SoapRemoteEndpointConnectionDefinition connectionDefinition;

        connectionDefinition = this.serverContext.getDefinitionManager().getInitializedDefinition(context, operationDefinition.getRootProjectId(), operationDefinition.getRemoteServerConnectionIdentifier(), null);
        return (T8IntegrationServiceConnector)connectionDefinition.getNewConnectionInstance(this.context);
    }

    public void setLanguageID(String languageID)
    {
        //Sets the language ID on all the applicable objects
        this.valueStringGenerator.setLanguageID(languageID);
        this.terminologyProvider.setLanguage(languageID);
    }

    private String executeCreation(T8IntegrationServiceConnector connector, Map<String, Object> parameters) throws Exception
    {
        List<DataRecord> dataRecords;
        String operationIID;

        dataRecords = (List<DataRecord>) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_DATA_RECORDS);
        operationIID = (String) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_OPERATION_IID);
        inputParameters = (Map) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_INPUT_PARAMETERS);
        persistenceHandler.setOperationIID(operationIID);

        if (dataRecords == null || dataRecords.isEmpty())
        {
            throw new Exception("No data records recieved for creation operation");
        }

        Creations creations;

        creations = factory.createCreations();
        creations.setOperationID(operationIID);
        creations.getRecord().addAll(mapDataRecords(dataRecords));

        try
        {
            connector.executeCreate(creations);
            persistenceHandler.logOperationSuccessEvent(factory.createCreations(creations));
        }
        catch (Exception ex)
        {
            persistenceHandler.logOperationFailureEvent(factory.createCreations(creations));
            throw ex;
        }

        return creations.getOperationID();
    }

    private String executeUpdate(T8IntegrationServiceConnector connector, Map<String, Object> parameters) throws Exception
    {
        List<DataRecord> dataRecords;
        String operationIID;

        dataRecords = (List<DataRecord>) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_DATA_RECORDS);
        operationIID = (String) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_OPERATION_IID);
        inputParameters = (Map) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_INPUT_PARAMETERS);
        persistenceHandler.setOperationIID(operationIID);

        if (dataRecords == null || dataRecords.isEmpty())
        {
            throw new Exception("No data records recieved for update operation");
        }

        Updates updates;

        updates = factory.createUpdates();
        updates.setOperationID(operationIID);
        updates.getRecord().addAll(mapDataRecords(dataRecords));

        try
        {
            connector.executeUpdate(updates);
            persistenceHandler.logOperationSuccessEvent(factory.createUpdates(updates));
        }
        catch (Exception ex)
        {
            persistenceHandler.logOperationFailureEvent(factory.createUpdates(updates));
            throw ex;
        }

        return updates.getOperationID();
    }

    private String executeSetFlags(T8IntegrationServiceConnector connector, Map<String, Object> parameters) throws Exception
    {
        List<DataRecord> dataRecords;
        String operationIID;

        dataRecords = (List<DataRecord>) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_DATA_RECORDS);
        operationIID = (String) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_OPERATION_IID);
        inputParameters = (Map) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_INPUT_PARAMETERS);
        persistenceHandler.setOperationIID(operationIID);

        if (dataRecords == null || dataRecords.isEmpty())
        {
            throw new Exception("No data records recieved for deletion operation");
        }

        Flags flags;

        flags = factory.createFlags();
        flags.setOperationID(operationIID);
        flags.getRecord().addAll(mapDataFlags(dataRecords));

        try
        {
            connector.executeFlags(flags);
            persistenceHandler.logOperationSuccessEvent(factory.createFlags(flags));
        }
        catch (Exception ex)
        {
            persistenceHandler.logOperationFailureEvent(factory.createFlags(flags));
            throw ex;
        }

        return flags.getOperationID();
    }

    private String executeExtension(T8IntegrationServiceConnector connector, Map<String, Object> parameters) throws Exception
    {
        List<DataRecord> dataRecords;
        String operationIID;

        dataRecords = (List<DataRecord>) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_DATA_RECORDS);
        operationIID = (String) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_OPERATION_IID);
        inputParameters = (Map) parameters.get(T8IntegrationServiceOperationMappingDefinition.P_INPUT_PARAMETERS);
        persistenceHandler.setOperationIID(operationIID);

        if (dataRecords == null || dataRecords.isEmpty())
        {
            throw new Exception("No data records recieved for extension operation");
        }

        Extensions extensions;

        extensions = factory.createExtensions();
        extensions.setOperationID(operationIID);
        extensions.getRecord().addAll(mapDataRecords(dataRecords));

        try
        {
            connector.executeExtension(extensions);
            persistenceHandler.logOperationSuccessEvent(factory.createExtensions(extensions));
        }
        catch (Exception ex)
        {
            persistenceHandler.logOperationFailureEvent(factory.createExtensions(extensions));
            throw ex;
        }

        return extensions.getOperationID();
    }

    //<editor-fold defaultstate="collapsed" desc="Data Record Mapping">
    public Record mapDataRecord(DataRecord record) throws Exception
    {
        T8DataRecordMappingDefinition mappingDefinition;
        Record mappedRecord;

        mappingDefinition = (T8DataRecordMappingDefinition) definition.getSubDefinition(operationDefinition.getRecordMappingIdentifier());
        mappedRecord = mapSubDataRecord(record, operationDefinition, mappingDefinition, true, true);

        return mappedRecord;
    }

    public Collection<Record> mapDataRecords(Collection<DataRecord> records) throws Exception
    {
        Collection<Record> mappedRecords;

        mappedRecords = new ArrayList<>();

        for (DataRecord dataRecord : records)
        {
            mappedRecords.add(mapDataRecord(dataRecord));
        }

        return mappedRecords;
    }

    public Collection<Flags.Record> mapDataFlags(Collection<DataRecord> dataRecords) throws Exception
    {
        Collection<Flags.Record> mappedFlags;

        mappedFlags = new ArrayList<>();

        for (DataRecord dataRecord : dataRecords)
        {
            mappedFlags.add(mapDataFlag(dataRecord));
        }

        return mappedFlags;
    }

    public Flags.Record mapDataFlag(DataRecord dataRecord) throws Exception
    {
        HashMap<String, Object> expressionParameters;
        T8DataRecordFlagMappingDefinition flagMappingDefinition;
        Flags.Record flagRecord;
        Flags.Record.Flag flag;

        flagMappingDefinition = (T8DataRecordFlagMappingDefinition) definition.getLocalDefinition(((T8IntegrationSetFlagsOperationDefinition) operationDefinition).getFlagMappingIdentifier());

        try
        {
            flagRecord = factory.createFlagsRecord();
            flag = factory.createFlagsRecordFlag();
            flagRecord.setHeader(mapDataRecordHeader(dataRecord));
            flagRecord.getFlag().add(flag);
            flag.setFlagID(T8IdentifierUtilities.createNewGUID());
            flag.setType(flagMappingDefinition.getFlagType());

            expressionParameters = new HashMap<>();
            expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
            expressionParameters.put("dataRecord", dataRecord);
            expressionParameters.put("terminologyProvider", terminologyProvider);

            flag.setValue((String) expressionEvaluator.evaluateExpression(flagMappingDefinition.getFlagValueExpression(), expressionParameters, null));

            if (flagMappingDefinition.getFlagKeys() != null)
            {
                for (T8DataRecordFlagKeyMappingDefinition flagKeyDefinition : flagMappingDefinition.getFlagKeys())
                {
                    Flags.Record.Flag.Key flagKey;

                    try
                    {
                        flagKey = factory.createFlagsRecordFlagKey();
                        flagKey.setTerm(flagKeyDefinition.getKeyID());
                        flagKey.setValue((String) expressionEvaluator.evaluateExpression(flagKeyDefinition.getKeyValueExpression(), expressionParameters, null));

                        flag.getKey().add(flagKey);
                    } catch (EPICRuntimeException | EPICSyntaxException ex)
                    {
                        LOGGER.log("Failed to map flag key " + flagKeyDefinition.getIdentifier(), ex);
                        throw ex;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to while attempting to map the flag mapping definition " + flagMappingDefinition.getIdentifier(), ex);
            throw ex;
        }

        return flagRecord;
    }

    public RecordHeader mapDataRecordHeader(DataRecord record) throws Exception
    {
        T8DataRecordMappingDefinition mappingDefinition;
        Record mappedRecord;

        mappingDefinition = (T8DataRecordMappingDefinition) definition.getSubDefinition(operationDefinition.getRecordMappingIdentifier());
        mappedRecord = mapSubDataRecord(record, operationDefinition, mappingDefinition, false, false);

        return mappedRecord.getHeader();
    }

    public Collection<RecordHeader> mapDataRecordHeaders(Collection<DataRecord> records) throws Exception
    {
        Collection<RecordHeader> mappedRecords;

        mappedRecords = new ArrayList<>();

        for (DataRecord dataRecord : records)
        {
            mappedRecords.add(mapDataRecordHeader(dataRecord));
        }

        return mappedRecords;
    }

    private Record mapSubDataRecord(DataRecord record, T8IntegrationServiceOperationMappingDefinition operationDefinition, T8DataRecordMappingDefinition mappingDefinition, boolean includeContent, boolean isRootDataRecord) throws Exception
    {
        Record mappedRecord;
        List<T8SubDataRecordMappingDefinition> subRecordDefinitions;

        mappedRecord = factory.createRecord();

        try
        {
            if (record != null)
            {
                //Set the record details
                mappedRecord.setHeader(createRecordHeader(record, mappingDefinition));

                if (includeContent)
                {
                    //Check if the record mapping should include the property classifications for the root record
                    if (isRootDataRecord && mappingDefinition.isRecordClassificationsPropertyIncluded())
                        isPropertyClassificationIncluded = true;

                    mappedRecord.setContent(createRecordContent(record, mappingDefinition));

                    subRecordDefinitions = mappingDefinition.getSubRecordDefinitions();

                    //Add all the subrecord details, if they exist
                    if (subRecordDefinitions != null && !subRecordDefinitions.isEmpty())
                    {
                        SubRecords subRecords;

                        subRecords = factory.createRecordSubRecords();
                        for (T8SubDataRecordMappingDefinition t8DataRecordMappingDefinition : subRecordDefinitions)
                        {
                            Object result;

                            result = evaluator.evaluateExpression(record.getRootRecord(), t8DataRecordMappingDefinition.getRecordPathExpression());

                            if (result instanceof List)
                            {
                                List dataRecords;

                                dataRecords = (List) result;
                                for (Object subRecord : dataRecords)
                                {
                                    if (subRecord instanceof DataRecord)
                                    {
                                        if (t8DataRecordMappingDefinition.isLoadIndependentRecords())
                                        {
                                            subRecords.getRecord().add(mapSubDataRecordDetail(loadIndependentRecord(((DataRecord) subRecord).getRootRecord(), (DataRecord) subRecord), operationDefinition, t8DataRecordMappingDefinition, includeContent));
                                        } else
                                        {
                                            subRecords.getRecord().add(mapSubDataRecordDetail((DataRecord) subRecord, operationDefinition, t8DataRecordMappingDefinition, includeContent));
                                        }
                                    } else
                                    {
                                        throw new RuntimeException(String.format("Invalid expressions result for sub record expression %s on mapping %s, exptected a list containing DataRecord, but found %s instead.", t8DataRecordMappingDefinition.getRecordPathExpression(), mappingDefinition.getPublicIdentifier(), subRecord.getClass()));
                                    }
                                }
                            }
                            else if (result instanceof DataRecord)
                            {
                                if (t8DataRecordMappingDefinition.isLoadIndependentRecords())
                                {
                                    subRecords.getRecord().add(mapSubDataRecordDetail(loadIndependentRecord(((DataRecord) result).getRootRecord(), (DataRecord) result), operationDefinition, t8DataRecordMappingDefinition, includeContent));
                                } else
                                {
                                    subRecords.getRecord().add(mapSubDataRecordDetail((DataRecord) result, operationDefinition, t8DataRecordMappingDefinition, includeContent));
                                }
                            }
                            else if (result != null)
                            {
                                throw new RuntimeException(String.format("Invalid expressions result for sub record expression %s on mapping %s, exptected List<DataRecord>, but found %s instead.", t8DataRecordMappingDefinition.getRecordPathExpression(), mappingDefinition.getPublicIdentifier(), result.getClass()));
                            }
                        }
                        mappedRecord.setSubRecords(subRecords);
                    }
                }

                if (isRootDataRecord && mappingDefinition.isRecordDescritptionIncluded())
                {
                    RecordDescriptions recordDescriptions;
                    List<String> organizationIds;

                    recordDescriptions = new RecordDescriptions();
                    organizationIds = new ArrayList<>();
                    for (T8DataRecordOrganizationMappingDefinition organizationMapping : mappingDefinition.getInterfaceOrganizationDefinitions())
                    {
                        if (organizationMapping instanceof T8DataRecordDocPathOrganizationMappingDefinition)
                        {
                            organizationIds.add((String) evaluator.evaluateExpression(record, organizationMapping.getOrganizationID()));
                        }
                        else if (organizationMapping instanceof T8DataRecordEpicOrganizationMappingDefinition)
                        {
                            HashMap<String, Object> expressionParameters;

                            expressionParameters = new HashMap<>();
                            expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
                            expressionParameters.put("dataRecord", record);
                            expressionParameters.put("terminologyProvider", terminologyProvider);
                            organizationIds.add((String) expressionEvaluator.evaluateExpression(organizationMapping.getOrganizationID(), expressionParameters, null));
                        }
                        else
                        {
                            organizationIds.add(organizationMapping.getOrganizationID());
                        }
                    }

                    for (T8DataRecordDescriptionMappingDefinition dataRecordDescriptionMappingDefinition : mappingDefinition.getDataRecordDescriptionMappingDefinition())
                    {
                        System.out.println("ACS - Description Type: " + dataRecordDescriptionMappingDefinition.getDescriptionType());
                        System.out.println("ACS - Description Language: " + dataRecordDescriptionMappingDefinition.getDescriptionLanguage());

                        Description description;
                        List<String> descriptionValues;
                        Integer splitChar;
                        String descriptionValue = null;

                        descriptionValues = new ArrayList<>();
                        splitChar = dataRecordDescriptionMappingDefinition.getSplitChar();

                        //Check if any conditional overrides are enabled for the description mapping definition.
                        if (dataRecordDescriptionMappingDefinition.isConditionalOverrideIncluded()) descriptionValue = getConditionalDescriptionValue(dataRecordDescriptionMappingDefinition, record);

                        //Check if the description Value was updated if not retrive the value from the record descriptions.
                        if (descriptionValue == null || descriptionValue.equals("")) descriptionValue = getDescriptionValue(record, dataRecordDescriptionMappingDefinition, organizationIds);

                        if (splitChar == 0 || splitChar >= descriptionValue.length()) descriptionValues.add(descriptionValue);
                        else
                        {
                            while (0 < splitChar && splitChar < descriptionValue.length())
                            {
                                descriptionValues.add(descriptionValue.substring(0, splitChar - 1));
                                descriptionValue = descriptionValue.substring(splitChar - 1);
                            }

                            if (splitChar != 0 && splitChar >= descriptionValue.length() && descriptionValue.length() > 0) descriptionValues.add(descriptionValue);
                        }
                        for (String descriptionValuePartical : descriptionValues)
                        {
                            description = new Description();
                            description.setDescriptionType(dataRecordDescriptionMappingDefinition.getDescriptionType());
                            description.setLanguage(dataRecordDescriptionMappingDefinition.getDescriptionLanguage());
                            description.setValue(descriptionValuePartical);
                            recordDescriptions.getDescription().add(description);
                        }
                    }
                    mappedRecord.setDescriptions(recordDescriptions);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to map sub record " + record + " for mapping definition " + mappingDefinition.getPublicIdentifier(), ex);
        }

        return mappedRecord;
    }

    private Record mapSubDataRecordDetail(DataRecord record, T8IntegrationServiceOperationMappingDefinition operationDefinition, T8SubDataRecordMappingDefinition mappingDefinition, boolean includeContent) throws Exception
    {
        Record mappedRecord;
        List<T8SubDataRecordMappingDefinition> subRecordDefinitions;

        mappedRecord = factory.createRecord();

        try
        {
            if (record != null)
            {
                //Set the record details
                mappedRecord.setHeader(createSubRecordHeader(record, mappingDefinition));

                if (includeContent)
                {
                    mappedRecord.setContent(createRecordContent(record, mappingDefinition));

                    subRecordDefinitions = mappingDefinition.getSubRecordDefinitions();

                    //Add all the subrecord details, if they exist
                    if (subRecordDefinitions != null && !subRecordDefinitions.isEmpty())
                    {
                        SubRecords subRecords;

                        subRecords = factory.createRecordSubRecords();
                        for (T8SubDataRecordMappingDefinition t8DataRecordMappingDefinition : subRecordDefinitions)
                        {
                            Object result;

                            result = evaluator.evaluateExpression(record.getRootRecord(), t8DataRecordMappingDefinition.getRecordPathExpression());

                            if (result instanceof List)
                            {
                                List dataRecords;

                                dataRecords = (List) result;
                                for (Object subRecord : dataRecords)
                                {
                                    if (subRecord instanceof DataRecord)
                                    {
                                        subRecords.getRecord().add(mapSubDataRecordDetail((DataRecord) subRecord, operationDefinition, t8DataRecordMappingDefinition, includeContent));
                                    } else
                                    {
                                        throw new RuntimeException(String.format("Invalid expressions result for sub record expression %s on mapping %s, exptected a list containing DataRecord, but found %s instead.", t8DataRecordMappingDefinition.getRecordPathExpression(), mappingDefinition.getPublicIdentifier(), subRecord.getClass()));
                                    }
                                }
                            }
                            else if (result instanceof DataRecord)
                            {
                                subRecords.getRecord().add(mapSubDataRecordDetail((DataRecord) result, operationDefinition, t8DataRecordMappingDefinition, includeContent));
                            }
                            else if (result != null)
                            {
                                throw new RuntimeException(String.format("Invalid expressions result for sub record expression %s on mapping %s, exptected List<DataRecord>, but found %s instead.", t8DataRecordMappingDefinition.getRecordPathExpression(), mappingDefinition.getPublicIdentifier(), result.getClass()));
                            }
                        }
                        mappedRecord.setSubRecords(subRecords);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to map sub record " + record + " for mapping definition " + mappingDefinition.getPublicIdentifier(), ex);
        }

        return mappedRecord;
    }

    private RecordHeader createRecordHeader(DataRecord record, T8DataRecordMappingDefinition mappingDefinition) throws Exception
    {
        RecordHeader header;

        try
        {
            //Create a new record header and set the information that will identify this record
            header = factory.createRecordHeader();
            if (operationDefinition instanceof T8IntegrationCreationOperationDefinition || operationDefinition instanceof T8IntegrationExtensionOperationDefinition)
                header.setExternalReference(getExternalReferenceCode(record, mappingDefinition.getExternalReferencePathExpression(), mappingDefinition.getExternalNumberDefinition()));
            else
                header.setExternalReference(getExternalReferenceCode(record, mappingDefinition.getExternalReferencePathExpression()));
            header.setRecordID(record.getID());
            header.setRecordTerm(mappingDefinition.getRecordInterfaceTerm());

            for (T8DataRecordOrganizationMappingDefinition organizationMapping : mappingDefinition.getInterfaceOrganizationDefinitions())
            {
                RecordHeader.Organization organization;

                organization = new RecordHeader.Organization();
                if (organizationMapping instanceof T8DataRecordDocPathOrganizationMappingDefinition)
                {
                    organization.setOrganizationID((String) evaluator.evaluateExpression(record, organizationMapping.getOrganizationID()));
                }
                else if (organizationMapping instanceof T8DataRecordEpicOrganizationMappingDefinition)
                {
                    HashMap<String, Object> expressionParameters;

                    expressionParameters = new HashMap<>();
                    expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
                    expressionParameters.put("dataRecord", record);
                    expressionParameters.put("terminologyProvider", terminologyProvider);
                    organization.setOrganizationID((String) expressionEvaluator.evaluateExpression(organizationMapping.getOrganizationID(), expressionParameters, null));
                }
                else
                {
                    organization.setOrganizationID(organizationMapping.getOrganizationID());
                }

                organization.setOrganizationTerm(organizationMapping.getOrganizationTerm());

                if (organizationMapping instanceof T8DataRecordDocPathOrganizationMappingDefinition)
                {
                    organization.setValue((String) evaluator.evaluateExpression(record, organizationMapping.getOrganizationValue()));
                }
                else if (organizationMapping instanceof T8DataRecordEpicOrganizationMappingDefinition)
                {
                    HashMap<String, Object> expressionParameters;

                    expressionParameters = new HashMap<>();
                    expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
                    expressionParameters.put("dataRecord", record);
                    expressionParameters.put("terminologyProvider", terminologyProvider);
                    organization.setValue((String) expressionEvaluator.evaluateExpression(organizationMapping.getOrganizationValue(), expressionParameters, null));
                }
                else
                {
                    organization.setValue(organizationMapping.getOrganizationValue());
                }
                header.getOrganization().add(organization);
            }
        }
        catch (EPICRuntimeException | EPICSyntaxException ex)
        {
            throw new Exception("Failed to map record header for mapping definition " + mappingDefinition.getPublicIdentifier(), ex);
        }

        return header;
    }

    private RecordHeader createSubRecordHeader(DataRecord record, T8SubDataRecordMappingDefinition mappingDefinition) throws Exception
    {
        RecordHeader header;

        try
        {
            //Create a new record header and set the information that will identify this record
            header = factory.createRecordHeader();
            header.setRecordID(record.getID());
            header.setRecordTerm(mappingDefinition.getRecordInterfaceTerm());

            for (T8DataRecordOrganizationMappingDefinition organizationMapping : mappingDefinition.getInterfaceOrganizationDefinitions())
            {
                RecordHeader.Organization organization;

                organization = new RecordHeader.Organization();
                if (organizationMapping instanceof T8DataRecordDocPathOrganizationMappingDefinition)
                {
                    organization.setOrganizationID((String) evaluator.evaluateExpression(record, organizationMapping.getOrganizationID()));
                }
                else if (organizationMapping instanceof T8DataRecordEpicOrganizationMappingDefinition)
                {
                    HashMap<String, Object> expressionParameters;

                    expressionParameters = new HashMap<>();
                    expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
                    expressionParameters.put("dataRecord", record);
                    expressionParameters.put("terminologyProvider", terminologyProvider);
                    organization.setOrganizationID((String) expressionEvaluator.evaluateExpression(organizationMapping.getOrganizationID(), expressionParameters, null));
                }
                else
                {
                    organization.setOrganizationID(organizationMapping.getOrganizationID());
                }

                organization.setOrganizationTerm(organizationMapping.getOrganizationTerm());

                if (organizationMapping instanceof T8DataRecordDocPathOrganizationMappingDefinition)
                {
                    organization.setValue((String) evaluator.evaluateExpression(record, organizationMapping.getOrganizationValue()));
                }
                else if (organizationMapping instanceof T8DataRecordEpicOrganizationMappingDefinition)
                {
                    HashMap<String, Object> expressionParameters;

                    expressionParameters = new HashMap<>();
                    expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
                    expressionParameters.put("dataRecord", record);
                    expressionParameters.put("terminologyProvider", terminologyProvider);
                    organization.setValue((String) expressionEvaluator.evaluateExpression(organizationMapping.getOrganizationValue(), expressionParameters, null));
                }
                else
                {
                    organization.setValue(organizationMapping.getOrganizationValue());
                }
                header.getOrganization().add(organization);
            }
        }
        catch (EPICRuntimeException | EPICSyntaxException ex)
        {
            throw new Exception("Failed to map record header for mapping definition " + mappingDefinition.getPublicIdentifier(), ex);
        }

        return header;
    }

    private RecordContent createRecordContent(DataRecord record, T8Definition mappingDefinition) throws Exception
    {
        RecordContent content;
        List<T8DataRecordPropertyMappingDefinition> propertyMappings;

        content = factory.createRecordContent();
        if (mappingDefinition instanceof T8DataRecordMappingDefinition)
        {
            propertyMappings = ((T8DataRecordMappingDefinition) mappingDefinition).getPropertyDefinitions();
        }
        else if (mappingDefinition instanceof T8SubDataRecordMappingDefinition)
        {
            propertyMappings = ((T8SubDataRecordMappingDefinition) mappingDefinition).getPropertyDefinitions();
        }
        else
        {
            throw new Exception("The Mapping Definition for the Data Records/Subs either is null or it doesnt exist.");
        }

        try
        {
            if (propertyMappings != null && !propertyMappings.isEmpty())
            {
                //For every property definition defined, create a new property object and add it to the content.
                for (T8DataRecordPropertyMappingDefinition propertyDefinition : propertyMappings)
                {
                    Map<String, Object> expressionParameters;
                    String enabledExpression;
                    String defaultValueExpression;

                    expressionParameters = new HashMap<>();
                    expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
                    expressionParameters.put("dataRecord", record);

                    if (propertyDefinition instanceof T8CustomMappingDefinition)
                    {
                        expressionParameters.put("propertiesValueString", "false");

                        try
                        {
                            DataRecordIntegrationPropertyMapper<Property> propertyMapper;
                            List<Property> mappedProperties;

                            propertyMapper = propertyDefinition.<Property>createNewPropertyMapperInstance(context);
                            propertyMapper.initialize(terminologyProvider, ontologyProvider, valueStringGenerator);
                            mappedProperties = propertyMapper.doPropertyMappings(record);

                            for (Property mappedProperty : mappedProperties)
                            {
                                if (Strings.isNullOrEmpty(mappedProperty.getValue()))
                                {
                                    mappedProperty.setValue(mappedProperty.getValue());
                                }

                                content.getProperty().add(mappedProperty);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Failed to map property " + propertyDefinition.getPublicIdentifier() + " for data record " + record, ex);
                        }
                    }
                    else
                    {
                        if (propertyDefinition instanceof T8ExpressionPropertyMappingDefinition)
                        {
                            enabledExpression = ((T8ExpressionPropertyMappingDefinition) propertyDefinition).getEnabledExpression();
                            defaultValueExpression = ((T8ExpressionPropertyMappingDefinition) propertyDefinition).getDefaultValueExpression();
                        }
                        else if (propertyDefinition instanceof T8DefaultPropertyMappingDefinition)
                        {
                            enabledExpression = ((T8DefaultPropertyMappingDefinition) propertyDefinition).getEnabledExpression();
                            defaultValueExpression = ((T8DefaultPropertyMappingDefinition) propertyDefinition).getDefaultValueExpression();
                        }
                        else
                        {
                            throw new IllegalArgumentException("The Property Definition is not a valid property mapping definition.\nMapping Definition: " + propertyDefinition.toString());
                        }

                        try
                        {
                            if (Strings.isNullOrEmpty(enabledExpression) || expressionEvaluator.evaluateBooleanExpression(enabledExpression, expressionParameters, null))
                            {
                                DataRecordIntegrationPropertyMapper<Property> propertyMapper;
                                Property mappedProperty;

                                if (propertyDefinition instanceof T8CustomMappingDefinition)
                                {
                                    return content;
                                }
                                propertyMapper = propertyDefinition.<Property>createNewPropertyMapperInstance(context);
                                propertyMapper.initialize(terminologyProvider, ontologyProvider, valueStringGenerator);
                                mappedProperty = propertyMapper.doPropertyMapping(record);

                                if (Strings.isNullOrEmpty(mappedProperty.getValue()) && !Strings.isNullOrEmpty(defaultValueExpression))
                                {
                                    Object expressionResult;

                                    expressionResult = expressionEvaluator.evaluateExpression(defaultValueExpression, expressionParameters, null);
                                    mappedProperty.setValue(expressionResult == null ? null : expressionResult.toString());
                                }

                                if (mappedProperty.getValue() != null)
                                {
                                    content.getProperty().add(mappedProperty);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Failed to map property " + propertyDefinition.getPublicIdentifier() + " for data record " + record, ex);
                        }
                    }
                }

                //Do property mapping for the record classification, if it should be included
                if (isPropertyClassificationIncluded)
                {
                    DataRecordIntegrationPropertyMapper<Property> propertyMapper;
                    Property mappedProperty;

                    propertyMapper = ((T8DataRecordMappingDefinition)mappingDefinition).<Property>createNewPropertyMapperInstance(context);
                    propertyMapper.initialize(terminologyProvider,ontologyProvider, valueStringGenerator);
                    mappedProperty = propertyMapper.doPropertyMapping(record);
                    content.getProperty().add(mappedProperty);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to map record content for mapping definition " + mappingDefinition.getPublicIdentifier(), ex);
        }

        return content;
    }

    private String getExternalReferenceCode(DataRecord record, String referenceExpression) throws EPICSyntaxException, EPICRuntimeException, Exception
    {
        Map<String, Object> expressionParameters;

        expressionParameters = new HashMap<>();
        expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
        expressionParameters.put("dataRecord", record);
        expressionParameters.put("terminologyProvider", terminologyProvider);

        if (!Strings.isNullOrEmpty(referenceExpression))
        {
            String externalReference = (String) evaluator.evaluateExpression(record, referenceExpression);
            if (!Strings.isNullOrEmpty(externalReference)) return externalReference;
        }
        return null;
    }

    private String getExternalReferenceCode(DataRecord record, String referenceExpression, T8DataRecordExternalNumberingDefinition externalNumberDefinition) throws EPICSyntaxException, EPICRuntimeException, Exception
    {
        Map<String, Object> expressionParameters;

        expressionParameters = new HashMap<>();
        expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
        expressionParameters.put("dataRecord", record);
        expressionParameters.put("terminologyProvider", terminologyProvider);

        if (!Strings.isNullOrEmpty(referenceExpression))
        {
            String externalReference = (String) evaluator.evaluateExpression(record, referenceExpression);
            if (!Strings.isNullOrEmpty(externalReference)) return externalReference;
        }

        if (externalNumberDefinition != null && (Strings.isNullOrEmpty(externalNumberDefinition.getEnabledExpression()) ||
                expressionEvaluator.evaluateBooleanExpression(externalNumberDefinition.getEnabledExpression(), expressionParameters, null)))
        {
            if (externalNumberDefinition.getValueType().equals(ExternalNumberTypes.DATA_KEY))
            {
                T8DefinitionManager definitionManager;
                T8Definition dataKeyDefinition;

                definitionManager = (T8DefinitionManager) serverContext.getDefinitionManager();
                if (externalNumberDefinition.getValueExpression() != null)
                {
                    dataKeyDefinition = definitionManager.getRawDefinition(context, externalNumberDefinition.getRootProjectId(), externalNumberDefinition.getValueExpression());
                    if (dataKeyDefinition instanceof T8SequentialDataKeyGeneratorDefinition)
                        return serverContext.getDataManager().generateKey(externalNumberDefinition.getValueExpression());
                }
            }
            if (externalNumberDefinition.getValueType().equals(ExternalNumberTypes.INPUT_PARAMETER))
            {
                if (inputParameters == null || !inputParameters.containsKey(externalNumberDefinition.getValueExpression()))
                {
                    System.out.println("****************************************************");
                    System.out.println("INTEGRATION MAPPING - EXTERNAL NUMBER EXCEPTION");
                    System.out.println("====================================================");
                    System.out.println("One of the following issues may have caused this: ");
                    System.out.println("* Either the wrong External Number Type was Used.");
                    System.out.println("* Or either the input Parameters werent set.");
                    if (inputParameters != null)
                        System.out.println("* Or either the input Parameters isnt containg the mapped parameter key.\n List of keys: " + inputParameters.keySet());
                    System.out.println("****************************************************");
                    throw new RuntimeException("There was an Integration Mapping - External Number error.\n Please contact your System Administrator.");
                }
                return (String) inputParameters.get(externalNumberDefinition.getValueExpression());
            }
            if (externalNumberDefinition.getValueType().equals(ExternalNumberTypes.VALUE_STRING))
                return externalNumberDefinition.getValueExpression();
        }
        return null;
    }

    private DataRecord loadIndependentRecord(DataRecord rootDataRecord, DataRecord dataRecord) throws Exception
    {
        Set<DataRecord> targetParentRecords;

        // Fetch the parent records to which the independent records we want to retrieve, are linked.
        targetParentRecords = rootDataRecord.findDataRecordsByDrInstance(dataRecord.getDataRequirementInstanceID());
        for (DataRecord targetParentRecord : targetParentRecords)
        {
            List<String> recordIDsToRetrieve;

            // Find the record ID's referencing the independent records we want to retrieve.
            // TODO: Currently the VA property iD is hard coded.
            recordIDsToRetrieve = targetParentRecord.getSubRecordIDsReferencedFromProperty("6E2CAE09AD664010A0F761405CEF89E5");
            for (String fileId : recordIDsToRetrieve)
            {
                DataRecord independentRecord;

                // Retrieve the independent record and add it to the target parent.
                independentRecord = recApi.retrieveFile(fileId, context.getSessionContext().getContentLanguageIdentifier(), true, true, false, false, false, false);
                if (!targetParentRecord.containsSubRecord(independentRecord.getID()))
                    targetParentRecord.addSubRecord(independentRecord);
            }
            return targetParentRecord;
        }

        return dataRecord;
    }

    private String getDescriptionValue(DataRecord record, T8DataRecordDescriptionMappingDefinition dataRecordDescriptionMappingDefinition, List<String> organizationIds)
    {
        Optional<T8DataString> descriptionValue = record.getDataStrings().stream()
                //Add filter for Organizations.
                .filter(description -> organizationIds.contains(ontologyProvider.getOntologyLinks(description.getInstanceID()).get(0).getOrganizationID()))
                //Add filter for Language
                .filter(description -> description.getLanguageID().contains(dataRecordDescriptionMappingDefinition.getDescriptionLanguageConcept()))
                //Add filter for Description Type
                .filter(description -> description.getTypeID().contains(dataRecordDescriptionMappingDefinition.getDescriptionTypeConcept()))
                //Get First value - as there should always only be one.
                .findFirst();

        //Checks if the combination has a description else send a null value.
        return (descriptionValue.isPresent()) ? descriptionValue.get().getString() : null;
    }

    private String getConditionalDescriptionValue(T8DataRecordDescriptionMappingDefinition dataRecordDescriptionMappingDefinition, DataRecord record) throws EPICSyntaxException, EPICRuntimeException
    {
        String descriptionValue = null;
        String enabledExpression;

        enabledExpression = dataRecordDescriptionMappingDefinition.getConditionalOverrideEnabledExpression();
        if (!Strings.isNullOrEmpty(enabledExpression))
        {
            HashMap<String, Object> expressionParameters;

            expressionParameters = new HashMap<>();
            expressionParameters.put("OPERATION_ID", operationDefinition.getIdentifier());
            expressionParameters.put("dataRecord", record);
            expressionParameters.put("terminologyProvider", terminologyProvider);

            if (expressionEvaluator.evaluateBooleanExpression(enabledExpression, expressionParameters, null))
            {
                Object docPathResult = null;
                String docPathExpression;
                String expression;

                //Evaluate the doc path expression.
                docPathExpression = dataRecordDescriptionMappingDefinition.getConditionalOverrideDocPathExpression();

                if(!Strings.isNullOrEmpty(docPathExpression))
                    docPathResult = evaluator.evaluateExpression(record, docPathExpression);

                //Evaluate the value expression.
                expression = dataRecordDescriptionMappingDefinition.getConditionalOverrideEpicExpression();

                if(!Strings.isNullOrEmpty(expression))
                {
                    //Add doc path result to the expression paramters.
                    expressionParameters.put("docPathResult", docPathResult);
                    descriptionValue = (String) expressionEvaluator.evaluateExpression(expression, expressionParameters, null);
                }
            }
        }
        return descriptionValue;
    }
}
