package com.pilog.t8.data.document.integration.field.mapping;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.integration.DataRecordIntegrationFieldMapper;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import com.pilog.t8.definition.data.document.integration.field.T8DefaultFieldMappingDefinition;
import com.pilog.t8.webservices.document.integration.ObjectFactory;
import com.pilog.t8.webservices.document.integration.RecordContent;
import com.pilog.t8.webservices.document.integration.RecordContent.Property;
import java.util.Objects;
import com.pilog.t8.api.T8Api;

import static com.pilog.t8.definition.data.document.integration.field.T8DefaultFieldMappingDefinition.MappingTypes.*;

/**
 * @author Andre Scheepers
 */
public class DefaultFieldMapping implements DataRecordIntegrationFieldMapper<RecordContent.Property>
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8DefaultFieldMappingDefinition definition;

    private T8LRUCachedTerminologyProvider terminologyProvider;
    private T8DataRecordPropertyValueStringGenerator valueStringGenerator;
    private ObjectFactory factory;

    public DefaultFieldMapping(T8ServerContext serverContext,
                                  T8SessionContext sessionContext,
                                  T8DefaultFieldMappingDefinition definition)
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
    }

    @Override
    public void initialize(T8Api orgApi,
                           T8LRUCachedTerminologyProvider terminologyProvider,
                           T8DataRecordPropertyValueStringGenerator valueStringGenerator)
    {
        this.terminologyProvider = terminologyProvider;
        this.valueStringGenerator = valueStringGenerator;

        this.factory = new ObjectFactory();
    }

    @Override
    public RecordContent.Property doFieldMapping(DataRecord dataRecord)
    {
        RecordValue recordValue;
        Property property;

        recordValue = dataRecord.getRecordProperty(definition.getPropertyID()).getField(definition.getFieldID());
        property = factory.createRecordContentProperty();
        property.setPropertyTerm(definition.getInterfaceTerm());

        if(recordValue != null)
        {
            //Set the property value depending on the type defined in the definition
            switch(definition.getMappingType())
            {
                case CODE:
                        property.setValue(getRecordValueConceptTerminology(recordValue).getCode());
                    break;
                case CONCEPT:
                        property.setValue(getRecordValueConceptTerminology(recordValue).getConceptId());
                    break;
                case TERM:
                        property.setValue(getRecordValueConceptTerminology(recordValue).getTerm());
                    break;
                case ABBREVIATION:
                        property.setValue(getRecordValueConceptTerminology(recordValue).getAbbreviation());
                    break;
                case DEFINITION:
                        property.setValue(getRecordValueConceptTerminology(recordValue).getDefinition());
                    break;
                default:
                    property.setValue(valueStringGenerator.generateValueString(recordValue).toString());
            }
        }

        return property;
    }

    private T8ConceptTerminology getRecordValueConceptTerminology(RecordValue value)
    {
        DataRecord parentRecord;
        T8ConceptTerminology terminology;
        String conceptID;


        parentRecord = value.getParentDataRecord();
        conceptID = value.getValue();

        //We need a valid concept ID for codes, so make sure that we found one
        if(conceptID == null) throw new RuntimeException(String.format("No concept ID value found for field %s on record %s", "", parentRecord.getID()));

        //First try and find the concept terminology on the data record self, if it is not found then ask the terminology provider
        terminology = getDataRecordConceptTerminology(parentRecord, conceptID);

        if(terminology == null) terminology = terminologyProvider.getTerminology(null, conceptID);

        //If the terminology is still null we recieved an invalid concept ID
        if(terminology == null) throw new RuntimeException(String.format("The concept id %s on field %s for record %s is invalid", conceptID, "", parentRecord.getID()));
        return terminology;
    }

    private T8ConceptTerminology getDataRecordConceptTerminology(DataRecord record, String conceptID)
    {
        T8ConceptTerminologyList terminology;

        terminology = record.getContentTerminology(true);

        if(terminology != null)
        {
            for (T8ConceptTerminology t8ConceptTerminology : terminology)
            {
                if(Objects.equals(t8ConceptTerminology.getConceptId(), conceptID) && Objects.equals(sessionContext.getContentLanguageIdentifier(), t8ConceptTerminology.getLanguageId()))
                    return t8ConceptTerminology;
            }
        }

        return null;
    }

}
