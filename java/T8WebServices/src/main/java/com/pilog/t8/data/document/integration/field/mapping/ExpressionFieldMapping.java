package com.pilog.t8.data.document.integration.field.mapping;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.integration.DataRecordIntegrationFieldMapper;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import com.pilog.t8.definition.data.document.integration.property.T8ExpressionPropertyMappingDefinition;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.webservices.document.integration.ObjectFactory;
import com.pilog.t8.webservices.document.integration.RecordContent;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.security.T8Context;

/**
 * @author Andre Scheepers
 */
public class ExpressionFieldMapping implements DataRecordIntegrationFieldMapper<RecordContent.Property>
{
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8ExpressionPropertyMappingDefinition definition;
    private T8LRUCachedTerminologyProvider terminologyProvider;
    private T8DataRecordPropertyValueStringGenerator valueStringGenerator;
    private ObjectFactory factory;

    public ExpressionFieldMapping(T8Context context, T8ExpressionPropertyMappingDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public void initialize(T8Api orgApi,
                           T8LRUCachedTerminologyProvider terminologyProvider,
                           T8DataRecordPropertyValueStringGenerator valueStringGenerator)
    {
        this.terminologyProvider = terminologyProvider;
        this.valueStringGenerator = valueStringGenerator;

        this.factory = new ObjectFactory();
    }

    @Override
    public RecordContent.Property doFieldMapping(DataRecord dataRecord) throws Exception
    {
        String docPathExpression;
        Object docPathResult;

        docPathExpression = definition.getDocPathExpression();
        docPathResult = null;

        if(!Strings.isNullOrEmpty(docPathExpression))
        {
            DocPathExpressionEvaluator evaluator;

            evaluator = new DocPathExpressionEvaluator();
            docPathResult = evaluator.evaluateExpression(dataRecord, docPathExpression);
        }

        String epicExpression;
        Object epicExpressionResult;

        epicExpression = definition.getEpicExpression();
        epicExpressionResult = null;

        if(!Strings.isNullOrEmpty(epicExpression))
        {
            T8ServerExpressionEvaluator evaluator;
            Map<String, Object> parameters;

            evaluator = new T8ServerExpressionEvaluator(context);
            parameters = new HashMap<>();

            parameters.put("terminologyProvider", terminologyProvider);
            parameters.put("valueStringGenerator", valueStringGenerator);
            parameters.put("dataRecord", dataRecord);
            parameters.put("docPathResult", docPathResult);

            epicExpressionResult = evaluator.evaluateExpression(epicExpression, parameters, null);
        }

        RecordContent.Property property;

        property = factory.createRecordContentProperty();
        property.setPropertyTerm(definition.getInterfaceTerm());

        if(epicExpressionResult != null) property.setValue(epicExpressionResult.toString());
        else if(docPathResult != null) property.setValue(docPathResult.toString());

       return property;
    }
}
