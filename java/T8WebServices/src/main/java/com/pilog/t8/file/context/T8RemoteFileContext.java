package com.pilog.t8.file.context;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.file.T8FileContext;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.commons.io.RandomAccessStream;
import com.pilog.t8.definition.file.context.T8RemoteFileContextDefinition;
import com.pilog.t8.definition.remote.server.connection.T8DefaultRemoteServerConnectionDefinition;
import com.pilog.t8.definition.remote.server.connection.T8RemoteConnectionUser;
import com.pilog.t8.definition.remote.server.connection.T8RemoteUserDefinition;
import com.pilog.t8.security.T8AuthenticationUtilities;
import com.pilog.t8.security.T8Context;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.StringTokenizer;
import javax.ws.rs.core.Response;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;

/**
 * @author Hennie Brink
 */
public class T8RemoteFileContext implements T8FileContext
{
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8FileManager fileManager;
    private final T8RemoteFileContextDefinition definition;
    private final String contextInstanceID;
    private final URLCodec urlCodec;

    private T8DefaultRemoteServerConnectionDefinition remoteServerConnectionDefinition;
    private String remoteServerURL;
    private String apiKey;

    public T8RemoteFileContext(T8Context context ,T8FileManager fileManager, T8RemoteFileContextDefinition definition, String contextInstanceID)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.fileManager = fileManager;
        this.definition = definition;
        this.contextInstanceID = contextInstanceID;
        this.urlCodec = new URLCodec();
    }

    @Override
    public String getId()
    {
        return definition.getIdentifier();
    }

    @Override
    public String getIid()
    {
        return contextInstanceID;
    }

    @Override
    public boolean open() throws Exception
    {
        T8DefinitionManager definitionManager;

        definitionManager = serverContext.getDefinitionManager();
        remoteServerConnectionDefinition = (T8DefaultRemoteServerConnectionDefinition) definitionManager.getInitializedDefinition(context, definition.getRootProjectId(), definition.getRemoteServerConnectionIdentifier(), null);

        this.remoteServerURL = remoteServerConnectionDefinition.getServiceURL();
        this.remoteServerURL += "/DataRecordAttachment/";

        if (context.getSessionContext().isSystemSession())
        {
            T8RemoteUserDefinition userDefinition;

            userDefinition = remoteServerConnectionDefinition.getDefaultRemoteUserDefinition();
            if (userDefinition != null)
            {
                this.apiKey = userDefinition.getApiKey();
                return true;
            }
            else throw new Exception("No remote user definition specified for connection: " + remoteServerConnectionDefinition);
        }
        else
        {
            T8RemoteConnectionUser user;

            user = serverContext.getSecurityManager().getRemoteConnectionUser(context, remoteServerConnectionDefinition.getIdentifier());
            if (user != null)
            {
                this.apiKey = user.getApiKey();
                return true;
            }
            else throw new Exception("No remote user found for connection: " + remoteServerConnectionDefinition.getIdentifier());
        }
    }

    @Override
    public boolean close() throws Exception
    {
        return true;
    }

    @Override
    public InputStream getFileInputStream(String filePath) throws Exception
    {
        URL fileLocation;
        URLConnection fileConnection;

        fileLocation = new URL(this.remoteServerURL + getEncodedPath(filePath));
        fileConnection = fileLocation.openConnection();

        // Configure the http connection settings.
        if (fileConnection instanceof HttpURLConnection)
        {
            HttpURLConnection httpConnection;

            httpConnection = (HttpURLConnection) fileConnection;
            httpConnection.setReadTimeout(60000); // We don't want the method to hang, rather fail the process
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(false);
            httpConnection.setInstanceFollowRedirects(true);
            httpConnection.setDefaultUseCaches(false);
            httpConnection.setRequestMethod("GET");

            // Add the authentication headers and open the connection.
            T8AuthenticationUtilities.addTokenAuthentication(httpConnection, apiKey);
            httpConnection.connect();
        }

        return fileConnection.getInputStream();
    }

    @Override
    public OutputStream getFileOutputStream(String filePath, boolean append) throws Exception
    {
        URL fileLocation;
        URLConnection fileConnection;

        fileLocation = new URL((remoteServerURL + getIid() + "/" + getEncodedPath(filePath)));
        fileConnection = fileLocation.openConnection();

        //Configure the http connection settings
        if(fileConnection instanceof HttpURLConnection)
        {
            HttpURLConnection httpConnection;

            httpConnection = (HttpURLConnection) fileConnection;
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true);
            httpConnection.setInstanceFollowRedirects(true);
            httpConnection.setDefaultUseCaches(false);
            httpConnection.setChunkedStreamingMode(1024 * 1024); // Set chunked streaming mode to 1MB so that the output stream can be written without a content length.
            httpConnection.setRequestMethod("PUT");
            httpConnection.setRequestProperty("Content-Type", "application/octet-stream");

            // Add the authentication headers and open the connection.
            T8AuthenticationUtilities.addTokenAuthentication(httpConnection, apiKey);
            httpConnection.connect();
            return new HttpOutputStreamWrapper(httpConnection);
        }

        return fileConnection.getOutputStream();
    }

    @Override
    public RandomAccessStream getFileRandomAccessStream(String filePath) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public File getFile(String filePath) throws Exception
    {
        return new File(new URI(this.remoteServerURL + filePath));
    }

    @Override
    public boolean fileExists(String filePath) throws Exception
    {
        URL fileLocation;
        URLConnection fileConnection;

        fileLocation = new URL(this.remoteServerURL + getEncodedPath(filePath));
        fileConnection = fileLocation.openConnection();

        // Configure the http connection settings.
        if (fileConnection instanceof HttpURLConnection)
        {
            HttpURLConnection httpConnection;

            httpConnection = (HttpURLConnection) fileConnection;
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(false);
            httpConnection.setInstanceFollowRedirects(true);
            httpConnection.setRequestMethod("HEAD");

            // Add the authentication headers.
            T8AuthenticationUtilities.addTokenAuthentication(httpConnection, apiKey);
            return httpConnection.getResponseCode() == Response.Status.OK.getStatusCode();
        }

        return false;
    }

    @Override
    public boolean createDirectory(String filePath) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean deleteFile(String path) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getFileSize(String path) throws Exception
    {
        URL fileLocation;
        URLConnection fileConnection;
        long fileSize;

        fileLocation = new URL(this.remoteServerURL + getEncodedPath(path));
        fileConnection = fileLocation.openConnection();
        fileSize = -1;

        if (fileConnection instanceof HttpURLConnection)
        {
            HttpURLConnection httpConnection;

            httpConnection = (HttpURLConnection) fileConnection;
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true);
            httpConnection.setInstanceFollowRedirects(true);
            httpConnection.setRequestMethod("HEAD");

            // Add the authentication headers.
            T8AuthenticationUtilities.addTokenAuthentication(httpConnection, apiKey);
            fileSize = httpConnection.getContentLength();
            httpConnection.disconnect();
        }

        return fileSize;
    }

    @Override
    public String getMD5Checksum(String path) throws Exception
    {
        URL fileLocation;
        URLConnection fileConnection;
        String md5;

        fileLocation = new URL(this.remoteServerURL + getEncodedPath(path));
        fileConnection = fileLocation.openConnection();
        md5 = null;

        if(fileConnection instanceof HttpURLConnection)
        {
            HttpURLConnection httpConnection;

            httpConnection = (HttpURLConnection) fileConnection;
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true);
            httpConnection.setInstanceFollowRedirects(true);
            httpConnection.setRequestMethod("HEAD");

            // Add the authentication headers.
            T8AuthenticationUtilities.addTokenAuthentication(httpConnection, apiKey);

            md5 = httpConnection.getHeaderField("Content-MD5");
            httpConnection.disconnect();
        }

        return md5;
    }

    @Override
    public T8FileDetails getFileDetails(String filePath) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<T8FileDetails> getFileList(String directoryPath) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void renameFile(String filePath, String newFilename) throws Exception, FileNotFoundException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private class HttpOutputStreamWrapper extends OutputStream
    {
        private final HttpURLConnection httpURLConnection;
        private final OutputStream wrappedOutputStream;

        private HttpOutputStreamWrapper(HttpURLConnection httpURLConnection) throws IOException
        {
            this.httpURLConnection = httpURLConnection;
            this.wrappedOutputStream =  httpURLConnection.getOutputStream();
        }

        @Override
        public void write(int b) throws IOException
        {
            this.wrappedOutputStream.write(b);
        }

        @Override
        public void flush() throws IOException
        {
            this.wrappedOutputStream.flush();
        }

        @Override
        public void close() throws IOException
        {
            this.wrappedOutputStream.close();

            //Check the server response
            int responseCode = this.httpURLConnection.getResponseCode();
            if(responseCode != Response.Status.ACCEPTED.getStatusCode()
                    && responseCode != Response.Status.CREATED.getStatusCode()
                    && responseCode != Response.Status.OK.getStatusCode())
            {
                throw new IOException("Server did not recieve resource and returned with status code " + responseCode);
            }
        }
    }

    private String getEncodedPath(String path) throws EncoderException
    {
        StringTokenizer tokenizer;
        String encodedFilePath;

        tokenizer = new StringTokenizer(path, "/");
        encodedFilePath = "";

        while (tokenizer.hasMoreTokens())
        {
            if (!encodedFilePath.isEmpty()) encodedFilePath += "/";
            encodedFilePath += urlCodec.encode(tokenizer.nextToken());
        }

        return !encodedFilePath.isEmpty() ? encodedFilePath.replaceAll("\\+", "%20") : path;
    }
}
