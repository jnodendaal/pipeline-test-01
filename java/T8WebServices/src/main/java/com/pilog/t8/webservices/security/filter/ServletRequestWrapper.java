package com.pilog.t8.webservices.security.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Gavin Boshoff
 */
public class ServletRequestWrapper extends javax.servlet.http.HttpServletRequestWrapper
{
    private final Map<String, String> headerMap;

    public ServletRequestWrapper(HttpServletRequest request)
    {
        super(request);
        headerMap = new HashMap<>();
    }

    public void addHeader(String name, String value)
    {
        headerMap.put(name, value);
    }

    @Override
    public Enumeration<String> getHeaderNames()
    {
        HttpServletRequest request = (HttpServletRequest) getRequest();
        List<String> list = new ArrayList<>();
        for (Enumeration<String> e = request.getHeaderNames(); e.hasMoreElements();)
        {
            list.add(e.nextElement());
        }

        for (Iterator<String> i = headerMap.keySet().iterator(); i.hasNext();)
        {
            list.add(i.next());
        }

        return Collections.enumeration(list);
    }

    @Override
    public String getHeader(String name)
    {
        Object value;
        if ((value = headerMap.get("" + name)) != null)
        {
            return value.toString();
        }
        else
        {
            return ((HttpServletRequest) getRequest()).getHeader(name);
        }
    }
}
