package com.pilog.t8.data.document.integration.property.mapping;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import com.pilog.t8.definition.data.document.integration.custom.T8CustomFieldMappingDefinition;
import com.pilog.t8.definition.data.document.integration.custom.T8CustomMappingDefinition;
import com.pilog.t8.definition.data.document.integration.custom.T8CustomMappingDefinition.SplitTypes;
import com.pilog.t8.definition.data.document.integration.custom.property.T8CustomExpressionPropertyMappingDefinition;
import com.pilog.t8.definition.data.document.integration.custom.property.T8CustomPropertyMappingDefinition;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.webservices.document.integration.ObjectFactory;
import com.pilog.t8.webservices.document.integration.RecordContent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andre Scheepers
 */
public class CustomPropertyMapping implements DataRecordIntegrationPropertyMapper<RecordContent.Property>
{
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8CustomMappingDefinition definition;
    private TerminologyProvider terminologyProvider;
    private OntologyProvider ontologyProvider;
    private T8DataRecordPropertyValueStringGenerator valueStringGenerator;
    private ObjectFactory factory;

    public CustomPropertyMapping(T8Context context, T8CustomMappingDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public void initialize(TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, T8DataRecordPropertyValueStringGenerator valueStringGenerator)
    {
        this.terminologyProvider = terminologyProvider;
        this.ontologyProvider = ontologyProvider;
        this.valueStringGenerator = valueStringGenerator;

        this.factory = new ObjectFactory();
    }

    @Override
    public RecordContent.Property doPropertyMapping(DataRecord dataRecord) throws Exception
    {
        throw new UnsupportedOperationException("This method is only supported by either a Default/Expression Proeprty Mapping."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<RecordContent.Property> doPropertyMappings(DataRecord dataRecord) throws Exception
    {
        String propertiesValueString;
        T8ServerExpressionEvaluator evaluator;
        Map<String, Object> parameters;


        evaluator = new T8ServerExpressionEvaluator(context);
        parameters = new HashMap<>();

        parameters.put("terminologyProvider", terminologyProvider);
        parameters.put("valueStringGenerator", valueStringGenerator);
        parameters.put("dataRecord", dataRecord);
        propertiesValueString = propertiesValueStringBuilder(dataRecord, evaluator, parameters);
        parameters.put("propertiesValueString", propertiesValueString);

        if (definition.getSplitType().equals(SplitTypes.WORD.toString())) return setFieldMappingValuesByWord(dataRecord, evaluator, parameters, propertiesValueString);
        else if (definition.getSplitType().equals(SplitTypes.CHARACTER.toString())) return setFieldMappingValuesByChar(dataRecord, evaluator, parameters, propertiesValueString);
        else throw new RuntimeException("Custom Property Mapping: " + definition.getIdentifier() + " has an incorrect or null Split Type.\nWORD and CHARACTER is valid.");
    }

    private String propertiesValueStringBuilder(DataRecord dataRecord, T8ServerExpressionEvaluator evaluator, Map<String, Object> parameters) throws EPICSyntaxException, EPICRuntimeException
    {
        String valueString = "";
        String propertyValue = "";
        String separator = definition.getSeparator();

        if(Strings.isNullOrEmpty(separator)) separator = " ";

        for (T8CustomPropertyMappingDefinition propertyMappingDfinition : definition.getPropertyList())
        {
            String docPathExpression;
            Object docPathResult;
            T8CustomExpressionPropertyMappingDefinition customPropertyDefinition;

            customPropertyDefinition = (T8CustomExpressionPropertyMappingDefinition) propertyMappingDfinition;
            docPathExpression = customPropertyDefinition.getDocPathExpression();
            docPathResult = null;

            if(!Strings.isNullOrEmpty(docPathExpression))
            {
                DocPathExpressionEvaluator docPathEvaluator;

                docPathEvaluator = new DocPathExpressionEvaluator();
                docPathEvaluator.setTerminologyProvider(terminologyProvider);
                docPathEvaluator.setOntologyProvider(ontologyProvider);
                docPathResult = docPathEvaluator.evaluateExpression(dataRecord, docPathExpression);

                propertyValue = docPathResult != null ? docPathResult.toString() : "";
            }

            String epicExpression;
            Object epicExpressionResult;

            epicExpression = customPropertyDefinition.getEpicExpression();

            if(!Strings.isNullOrEmpty(epicExpression))
            {
                parameters.put("docPathResult", docPathResult);
                epicExpressionResult = evaluator.evaluateExpression(epicExpression, parameters, null);
                propertyValue = epicExpressionResult != null ? epicExpressionResult.toString() : null;
            }
            valueString += propertyValue + separator;
        }
        return valueString.substring(0, valueString.length() - separator.length());
    }

    private List<RecordContent.Property> setFieldMappingValuesByChar(DataRecord dataRecord, T8ServerExpressionEvaluator evaluator, Map<String, Object> parameters, String propertiesValueString) throws EPICSyntaxException, EPICRuntimeException
    {
        List<RecordContent.Property> properties;
        int first = 0;
        int last = 0;

        properties = new ArrayList<>();

        for (T8CustomFieldMappingDefinition customFieldMappingDefinition : definition.getFieldList())
        {
            RecordContent.Property property;

            if(!Strings.isNullOrEmpty(customFieldMappingDefinition.getEnabledMappingExpression()))
            {
                Boolean epicExpressionResult;
                String value;

                epicExpressionResult = (Boolean) evaluator.evaluateExpression(customFieldMappingDefinition.getEnabledMappingExpression(), parameters, null);
                if (epicExpressionResult)
                {
                    property = factory.createRecordContentProperty();
                    property.setPropertyTerm(customFieldMappingDefinition.getInterfaceTerm());

                    last += Integer.parseInt(customFieldMappingDefinition.getInterfaceTermSize() != null ? customFieldMappingDefinition.getInterfaceTermSize() : "0");
                    if (propertiesValueString.length() > first)
                    {
                        if (propertiesValueString.length() > last)
                            value = propertiesValueString.substring(first, last);
                        else value = propertiesValueString.substring(first);
                    }
                    else value = "";

                    property.setValue(value != null ? value : null);
                    properties.add(property);
                    first = last;
                }
            }
        }
       return properties;
    }

    private List<RecordContent.Property> setFieldMappingValuesByWord(DataRecord dataRecord, T8ServerExpressionEvaluator evaluator, Map<String, Object> parameters, String propertiesValueString) throws EPICSyntaxException, EPICRuntimeException
    {
        List<RecordContent.Property> properties;
        int previousFieldLength = 0;
        int previousStart = 0;

        properties = new ArrayList<>();

        for (T8CustomFieldMappingDefinition customFieldMappingDefinition : definition.getFieldList())
        {
            if(!Strings.isNullOrEmpty(customFieldMappingDefinition.getEnabledMappingExpression()))
            {
                Boolean epicExpressionResult;

                epicExpressionResult = (Boolean) evaluator.evaluateExpression(customFieldMappingDefinition.getEnabledMappingExpression(), parameters, null);
                if (epicExpressionResult)
                {
                    RecordContent.Property property;
                    int fieldLength;
                    int fieldSize;
                    int lastIndexOf;
                    String subPropertyString;

                    fieldLength = Integer.parseInt(customFieldMappingDefinition.getInterfaceTermSize());
                    fieldSize = fieldLength+previousFieldLength;
                    fieldSize = (fieldSize > propertiesValueString.length()) ? propertiesValueString.length() : fieldSize;
                    lastIndexOf = propertiesValueString.substring(previousStart, fieldSize).lastIndexOf(' ') + previousStart;
                    lastIndexOf = (lastIndexOf == -1) ? propertiesValueString.length() : lastIndexOf;
                    subPropertyString = propertiesValueString.substring(previousStart, lastIndexOf);

                    previousStart = lastIndexOf+1;
                    previousFieldLength = fieldLength;

                    property = factory.createRecordContentProperty();
                    property.setPropertyTerm(customFieldMappingDefinition.getInterfaceTerm());
                    property.setValue(subPropertyString);
                    properties.add(property);

                    if (propertiesValueString.length() <= previousStart) break;
                }
            }
        }
       return properties;
    }
}
