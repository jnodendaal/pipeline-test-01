/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.webservices.exceptions;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class WebServiceErrorCodes
{
    public static class WebServiceErrorCode
    {
        public String code;
        public String message;

        public WebServiceErrorCode(String code, String message)
        {
            this.code = code;
            this.message = message;
        }
    }

    public static WebServiceErrorCode SERVER_INTERNAL_EXCEPTION = new WebServiceErrorCode("00000", "Internal Server Error.");
    public static WebServiceErrorCode SERVER_NOT_READY = new WebServiceErrorCode("00001", "Server not yet ready, please try again later.");
    public static WebServiceErrorCode BAD_REQUEST = new WebServiceErrorCode("00100", "Invalid request parameters.");
    public static WebServiceErrorCode GENERAL_ERROR = new WebServiceErrorCode("00101", "General Error.");
    public static WebServiceErrorCode DATA_REQUIREMENT_NOT_FOUND = new WebServiceErrorCode("10000", "The referenced data requirement could not be found.");

}
