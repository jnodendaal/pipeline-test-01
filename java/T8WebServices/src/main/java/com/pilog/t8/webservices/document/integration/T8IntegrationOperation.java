package com.pilog.t8.webservices.document.integration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.data.document.integration.DataRecordIntegrationService;
import com.pilog.t8.definition.data.document.integration.T8IntegrationOperationDefinition;
import com.pilog.t8.definition.data.document.integration.T8IntegrationServiceDefinition;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8IntegrationOperation extends T8DefaultServerOperation
{
    private final T8IntegrationOperationDefinition integrationDefinition;

    public T8IntegrationOperation(T8ServerContext serverContext, T8Context context, T8IntegrationOperationDefinition definition, String operationIid)
    {
        super(context, definition, operationIid);
        this.integrationDefinition = definition;
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
    {
        DataRecordIntegrationService<Object> integrationService;
        T8IntegrationServiceDefinition serviceDefinition;
        Map<String, Object> mappedParameters;
        String operationID;

        //Get the neccesarry definitions
        serviceDefinition = (T8IntegrationServiceDefinition) this.serverContext.getDefinitionManager().getInitializedDefinition(this.context, this.integrationDefinition.getRootProjectId(), this.integrationDefinition.getIntegrationServiceIdentifier(), null);

        //Perform the parameter mappings
        if (this.integrationDefinition.getInputParameterMapping() != null)
        {
            mappedParameters = T8IdentifierUtilities.mapParameters(operationParameters, this.integrationDefinition.getInputParameterMapping());
            mappedParameters = T8IdentifierUtilities.stripNamespace(mappedParameters);
        } else mappedParameters = new HashMap<>();

        //Now create a new service instance and execute it
        integrationService = serviceDefinition.getNewIntegrationServiceInstance(this.context);

        try
        {
            integrationService.initialize(this.tx, this.integrationDefinition.getIntegrationServiceOperationIdentifier());
            operationID = integrationService.executeOperation(mappedParameters);

            return HashMaps.createSingular(T8IntegrationOperationDefinition.P_OPERATION_ID, operationID);
        }
        catch (Exception e)
        {
            throw new Exception("Failed to execute integration service " + serviceDefinition.getPublicIdentifier(), e);
        }
    }

    @Override
    public double getProgress(T8Context context)
    {
        return -1;
    }

    @Override
    public T8ProgressReport getProgressReport(T8Context context)
    {
        return null;
    }

    @Override
    public void stop(T8Context context)
    {
    }

    @Override
    public void cancel(T8Context context)
    {
    }

    @Override
    public void pause(T8Context context)
    {
    }

    @Override
    public void resume(T8Context context)
    {
    }
}
