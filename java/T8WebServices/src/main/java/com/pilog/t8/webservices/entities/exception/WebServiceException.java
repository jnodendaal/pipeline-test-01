/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.webservices.entities.exception;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hennie.brink
 */
@XmlRootElement(name = "Exception")
@XmlAccessorType(XmlAccessType.NONE)
public class WebServiceException
{
    private String exceptionClass;
    private String exceptionMessage;
    private WebServiceException cause;

    public WebServiceException()
    {
    }

    public WebServiceException(Throwable cause)
    {
        exceptionClass = cause.getClass().getCanonicalName();
        exceptionMessage = cause.getMessage();
        setCause(cause.getCause());
    }

    @XmlAttribute(name = "ExceptionClass")
    public String getExceptionClass()
    {
        return exceptionClass;
    }

    public void setExceptionClass(String exceptionClass)
    {
        this.exceptionClass = exceptionClass;
    }

    @XmlAttribute(name = "ExceptionMessage")
    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage)
    {
        this.exceptionMessage = exceptionMessage;
    }

    @XmlElement(name = "Cause")
    public WebServiceException getCause()
    {
        return cause;
    }

    public void setCause(WebServiceException cause)
    {
        this.cause = cause;
    }

    public void setCause(Throwable cause)
    {
        if(cause != null) this.cause = new WebServiceException(cause);
    }
}
