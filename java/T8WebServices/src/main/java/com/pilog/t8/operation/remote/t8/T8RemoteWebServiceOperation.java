package com.pilog.t8.operation.remote.t8;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.definition.operation.remote.T8RemoteWebServiceOperationDefinition;
import com.pilog.t8.definition.remote.server.connection.T8ConnectionDefinition;
import com.pilog.t8.data.T8JsonParameterSerializer;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.webservice.T8WebServiceConnector;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8RemoteWebServiceOperation implements T8ServerOperation
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8RemoteWebServiceOperation.class.getName());
    private final T8RemoteWebServiceOperationDefinition definition;
    private final T8Context context;
    private final T8ServerContext serverContext;

    private T8WebServiceConnector connector;

    public T8RemoteWebServiceOperation(T8Context context, T8RemoteWebServiceOperationDefinition definition, String operationInstanceIdentifier)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;

        initialize();
    }

    @Override
    public void init()
    {
    }

    private void initialize()
    {
        T8ConnectionDefinition remoteConnectionDefinition;

        try
        {
            remoteConnectionDefinition = this.serverContext.getDefinitionManager().getRawDefinition(this.context, context.getProjectId(), this.definition.getRemoteServerConnectionDefinitionIdentifier());
            this.connector = (T8WebServiceConnector)remoteConnectionDefinition.getNewConnectionInstance(this.context);
        }
        catch (Exception ex)
        {
            LOGGER.log("Unable to get the definition for the remote server connection: " + definition.getRemoteServerConnectionDefinitionIdentifier(), ex);
            throw new RuntimeException("Failed to initialize the connector for the Remote Web Service Operation : " + this.definition, ex);
        }
    }

    @Override
    public double getProgress(T8Context context)
    {
        return 0;
    }

    @Override
    public T8ProgressReport getProgressReport(T8Context context)
    {
        return null;
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
    {
        Map<String, Object> outputParameters;
        Map<String, Object> inputParameters;

        // Map the web service operation input to the server operation input.
        inputParameters = T8IdentifierUtilities.mapParameters(operationParameters, definition.getInputParameterMapping());

        outputParameters = executeOperation(inputParameters, this.definition.getRemoteOperationIdentifier(), executionType);

        return T8IdentifierUtilities.prependNamespace(definition.getIdentifier(),  T8IdentifierUtilities.mapParameters(outputParameters, definition.getOutputParameterMapping()));
    }

    @Override
    public void stop(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void cancel(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void pause(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void resume(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private Map<String, Object> executeOperation(Map<String, Object> inputParameters, String remoteOperationIdentifier, T8ServerOperationExecutionType executionType) throws Exception
    {
        Map<String, Object> outputParameters;
        T8JsonParameterSerializer jsonHandler;
        CharSequence transferJSON;
        String jsonResponse;

        // We get the JSON handler to allow us to convert the parameters to JSON
        jsonHandler = new T8JsonParameterSerializer();

        // Create the JSON String from the input parameters
        transferJSON = jsonHandler.generateMappedInputParameterJSON(this.definition, this.definition.getInputParameterMapping(), inputParameters);

        // Make the remote operation call and get the response
        jsonResponse = this.connector.executeRemoteOperation("POST", executionType.toString(), remoteOperationIdentifier, transferJSON.toString());

        // Parse the response to give the correct output parameter map
        outputParameters = jsonHandler.parseMappedOutputParameterJSON(this.definition, this.definition.getOutputParameterMapping(), jsonResponse);

        return outputParameters;
    }
}
