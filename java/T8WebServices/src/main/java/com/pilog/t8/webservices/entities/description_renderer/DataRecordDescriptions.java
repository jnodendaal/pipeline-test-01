/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.webservices.entities.description_renderer;

import java.util.*;
import javax.xml.bind.annotation.*;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
@XmlRootElement(name="DataRecordDescriptions")
@XmlAccessorType(XmlAccessType.NONE)
public class DataRecordDescriptions
{
    @XmlAttribute(name="DescriptionRendererIdentifier",required=true)
    private String DescriptionRendererIdentifier;

    @XmlElementWrapper(name="DescriptionClassificationIdentifiers")
    @XmlElements(@XmlElement(name="DescriptionClassificationIdentifier", type=String.class))
    private List<String> DescriptionClassificationIdentifiers;

    @XmlElementWrapper(name="LanguageIDs")
    @XmlElements(@XmlElement(name="LanguageID", type=String.class))
    private List<String> LanguageIDs;

    private String DataEntityIdentifier = "@E_DATA_RECORD_DETAILS";

    @XmlElementWrapper(name="Records")
    @XmlElements(@XmlElement(name="RecordID", type=String.class))
    private ArrayList<String> recordIDs;

    private String queueId;

    /**
     * @return the queueId
     */
    public String getQueueId()
    {
        return queueId;
    }

    /**
     * @param queueId the queueId to set
     */
    public void setQueueId(String queueId)
    {
        this.queueId = queueId;
    }

    /**
     * @return the DescriptionRendererIdentifier
     */
    public String getDescriptionRendererIdentifier()
    {
        return DescriptionRendererIdentifier;
    }

    /**
     * @param DescriptionRendererIdentifier the DescriptionRendererIdentifier to set
     */
    public void setDescriptionRendererIdentifier(String DescriptionRendererIdentifier)
    {
        this.DescriptionRendererIdentifier = DescriptionRendererIdentifier;
    }

    /**
     * @return the DescriptionTypeIdentifier
     */
    public List<String> getDescriptionClassificationIdentifiers()
    {
        return DescriptionClassificationIdentifiers;
    }

    /**
     * @param DescriptionTypeIdentifier the DescriptionTypeIdentifier to set
     */
    public void setDescriptionClassificationIdentifiers(List<String> DescriptionClassificationIdentifier)
    {
        this.DescriptionClassificationIdentifiers = DescriptionClassificationIdentifier;
    }

    public void addDescriptionClassificationIdentifier(String DescriptionClassificationeIdentifier)
    {
        if(this.DescriptionClassificationIdentifiers == null)
            this.DescriptionClassificationIdentifiers = new ArrayList<String>(1);
        this.DescriptionClassificationIdentifiers.add(DescriptionClassificationeIdentifier);
    }

    /**
     * @return the LanguageID
     */
    public List<String> getLanguageIDs()
    {
        return LanguageIDs;
    }

    /**
     * @param LanguageID the LanguageID to set
     */
    public void setLanguageIDs(List<String> LanguageID)
    {
        this.LanguageIDs = LanguageID;
    }

    public void addLanguageIDs(String LanguageID)
    {
        if(this.LanguageIDs == null)
            this.LanguageIDs = new ArrayList<String>(1);
        this.LanguageIDs.add(LanguageID);
    }

    /**
     * @return the DataEntityIdentifier
     */
    public String getDataEntityIdentifier()
    {
        return DataEntityIdentifier;
    }

    /**
     * @param DataEntityIdentifier the DataEntityIdentifier to set
     */
    public void setDataEntityIdentifier(String DataEntityIdentifier)
    {
        this.DataEntityIdentifier = DataEntityIdentifier;
    }

    /**
     * @return the recordIDs
     */
    public ArrayList<String> getRecordIDs()
    {
        return recordIDs;
    }

    /**
     * @param recordIDs the recordIDs to set
     */
    public void setRecordIDs(ArrayList<String> recordIDs)
    {
        this.recordIDs = recordIDs;
    }

    @Override
    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Description Renderer Identifier = "+DescriptionRendererIdentifier);
        str.append(" ,Description Clasification Identifiers = "+DescriptionClassificationIdentifiers);
        str.append(" ,Langauge ID's = "+LanguageIDs);
        str.append(" ,Data Entity Identigier = "+DataEntityIdentifier);
        str.append(" ,Record ID's = "+recordIDs);
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }


}
