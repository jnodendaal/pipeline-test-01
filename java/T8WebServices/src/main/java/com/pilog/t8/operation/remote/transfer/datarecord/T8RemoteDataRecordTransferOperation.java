package com.pilog.t8.operation.remote.transfer.datarecord;

import static com.pilog.t8.definition.remote.transfer.datarecord.T8RemoteDataRecordTransferAPIHandler.*;

import com.pilog.json.JsonObject;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.operation.progressreport.T8DefaultProgressReport;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.datatype.T8DtDataRecord;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.remote.server.connection.T8ConnectionDefinition;
import com.pilog.t8.mainserver.T8OperationExecutionRequest;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.thread.T8ContextRunnable;
import com.pilog.t8.utilities.threads.DynamicThreadPool;
import com.pilog.t8.webservice.T8WebServiceConnector;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Gavin Boshoff
 */
public class T8RemoteDataRecordTransferOperation extends T8DefaultServerOperation
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8RemoteDataRecordTransferOperation.class);
    private static final T8DataType DATA_RECORD_TYPE = new T8DtDataRecord();

    // Counters
    private final AtomicInteger totalProcessed;
    private final AtomicInteger succeeded;
    private int totalToProcess;

    private String remoteOperationId;
    private String callBackOperationId;
    private String callBackRecordIDIdentifier;
    private String remoteFileContextId;
    private T8ConnectionDefinition remoteServerConnectionDefinition;
    private T8DataRecordApi recApi;

    public T8RemoteDataRecordTransferOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
    {
        super(context, definition, operationIid);
        this.totalProcessed = new AtomicInteger();
        this.succeeded = new AtomicInteger();
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
    {
        T8DataEntityResults dataEntityResults = null;
        String remoteServerConnectionID;
        String recordIDFieldIdentifier;
        Boolean includeRecordOntology;
        ExecutorService threadPool;
        Boolean includeSubRecords;
        T8DataFilter dataFilter;
        Integer threadCount;
        long startTime;

        remoteOperationId = (String)operationParameters.get(PARAMETER_REMOTE_OPERATION_IDENTIFIER);
        remoteServerConnectionID = (String) operationParameters.get(PARAMETER_REMOTE_CONNECTION_IDENTIFIER);
        remoteFileContextId = (String) operationParameters.get(PARAMETER_REMOTE_FILE_CONTEXT_IDENTIFIER);
        callBackOperationId = (String) operationParameters.get(PARAMETER_CALL_BACK_OPERATION_IDENTIFIER);
        callBackRecordIDIdentifier = (String) operationParameters.get(PARAMETER_CALL_BACK_RECORD_ID_PARAMETER_IDENTIFIER);
        threadCount = (Integer) operationParameters.getOrDefault(PARAMETER_THREAD_COUNT, 1);
        dataFilter = (T8DataFilter) operationParameters.get(PARAMETER_DATA_FILTER);
        includeRecordOntology = (Boolean) operationParameters.getOrDefault(PARAMETER_INCLUDE_RECORD_ONTOLOGY, true);
        includeSubRecords = (Boolean) operationParameters.getOrDefault(PARAMETER_INCLUDE_SUB_RECORDS, true);
        recordIDFieldIdentifier = (String) operationParameters.get(PARAMETER_RECORD_ID_FIELD_IDENTIFIER);

        startTime = System.currentTimeMillis();

        remoteServerConnectionDefinition = (T8ConnectionDefinition) serverContext.getDefinitionManager().getInitializedDefinition(context, null, remoteServerConnectionID, operationParameters);

        if (remoteServerConnectionDefinition == null)
        {
            throw new IllegalArgumentException("Invalid remote server connection identifier supplied " + remoteServerConnectionID);
        }

        //Make sure the thread count is atleast one
        threadCount = (threadCount < 1) ? 1 : threadCount;

        threadPool = new DynamicThreadPool(1, threadCount, threadCount * 2, 1, TimeUnit.MINUTES);

        recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

        totalToProcess = tx.count(dataFilter.getEntityIdentifier(), dataFilter);

        try
        {
            //Create a new web service connection
            T8WebServiceConnector connector;

            connector = (T8WebServiceConnector)this.remoteServerConnectionDefinition.getNewConnectionInstance(context);

            dataEntityResults = tx.scroll(dataFilter.getEntityIdentifier(), dataFilter);

            while (dataEntityResults.next() && !stopFlag && !cancelFlag)
            {
                DataRecord transferRecord;
                T8DataEntity dataEntity;
                String fileId;

                dataEntity = dataEntityResults.get();
                fileId = (String) dataEntity.getFieldValue(recordIDFieldIdentifier);
                transferRecord = recApi.retrieveFile(fileId, context.getSessionContext().getContentLanguageIdentifier(), includeRecordOntology, false, false, false, false, includeSubRecords);

                threadPool.submit(new RecordTransferHandler(context, transferRecord, connector));
            }
        }
        finally
        {
            if (dataEntityResults != null) dataEntityResults.close();
        }

        //Wait for all transfers to complete
        threadPool.shutdown();
        threadPool.awaitTermination(10, TimeUnit.MINUTES);

        LOGGER.log("Processed " + totalProcessed + " of which " + succeeded + " successfully transferred records in " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime) + " seconds.");

        return null;
    }

    @Override
    public double getProgress(T8Context context)
    {
        if(totalToProcess < 1) return 0;
        else return ((double)totalProcessed.get() / (double)totalToProcess) * 100.00;
    }

    @Override
    public T8ProgressReport getProgressReport(T8Context context)
    {
        return new T8DefaultProgressReport("Transferred (" + totalProcessed + "/" + totalToProcess + ") - Succeeded (" + succeeded + "/" + totalProcessed + ") ", getProgress(context));
    }

    private void transferDataRecord(T8WebServiceConnector connector, DataRecord dataRecord) throws IOException, MalformedURLException, Exception
    {
        JsonObject model;

        model = new JsonObject().add(this.remoteOperationId+"$P_DATA_RECORD", DATA_RECORD_TYPE.serialize(dataRecord));

        connector.executeRemoteOperation("POST", T8OperationExecutionRequest.ExecutionType.SYNCHRONOUS.toString(), this.remoteOperationId, model.toString());
    }

    private class RecordTransferHandler extends T8ContextRunnable
    {
        private final T8WebServiceConnector connector;
        private final DataRecord dataRecord;

        private RecordTransferHandler(T8Context context, DataRecord dataRecord, T8WebServiceConnector connector)
        {
            super(context, "RDRT-"+callBackOperationId);
            this.dataRecord = dataRecord;
            this.connector = connector;
        }

        @Override
        public void exec()
        {
            try
            {
                //Transfer all of the record attachments
                transferRecordAttachments(dataRecord);

                //Transfer the record
                transferDataRecord(this.connector, dataRecord);

                //Record transfer succesfully, call the call back operation
                if (callBackOperationId != null)
                {
                    Map<String, Object> operationParameters;

                    operationParameters = new HashMap<>();
                    operationParameters.put(callBackRecordIDIdentifier, dataRecord.getID());

                    serverContext.executeSynchronousOperation(context, callBackOperationId, operationParameters);
                }

                // Update the number of successful transfers
                succeeded.incrementAndGet();
            }
            catch (Throwable ex)
            {
                LOGGER.log("Failed to transfer data record " + dataRecord.getID(), ex);
            }
            finally
            {
                //Update the progress
                totalProcessed.incrementAndGet();
            }
        }

        private void transferRecordAttachments(DataRecord dataRecord) throws Exception
        {
            //Transfer sub records first so that we do not get dependency issues
            for (DataRecord subDataRecord : dataRecord.getSubRecords())
            {
                transferRecordAttachments(subDataRecord);
            }

            Map<String, T8AttachmentDetails> attachmentDetails;
            Map<String, T8AttachmentDetails> remoteAttachmentDetails;
            try
            {
                attachmentDetails = recApi.retrieveRecordAttachmentDetails(dataRecord.getID());
            }
            catch (Exception ex)
            {
                throw new Exception("Could not get attachment details for record " + dataRecord.getID(), ex);
            }

            remoteAttachmentDetails = new HashMap<>();
            //Transfer all of the attachments
            for (String attachmentId : attachmentDetails.keySet())
            {
                T8AttachmentDetails fileDetails;

                fileDetails = recApi.exportAttachment(attachmentId, T8IdentifierUtilities.createNewGUID(), remoteFileContextId, attachmentId, null);

                remoteAttachmentDetails.put(attachmentId, fileDetails);
                //Make sure the file context is closed
                serverContext.getFileManager().closeFileContext(context, fileDetails.getContextIid());
            }

            //Set the new remote attachment details on the record so that they can be reloaded on the remote server
            dataRecord.setAttachmentDetails(remoteAttachmentDetails);
        }
    }
}
