package com.pilog.t8.data.document.integration.property.mapping;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.integration.DataRecordIntegrationPropertyMapper;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import com.pilog.t8.definition.data.document.integration.property.T8ExpressionPropertyMappingDefinition;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.webservices.document.integration.ObjectFactory;
import com.pilog.t8.webservices.document.integration.RecordContent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class ExpressionPropertyMapping implements DataRecordIntegrationPropertyMapper<RecordContent.Property>
{
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8ExpressionPropertyMappingDefinition definition;
    private TerminologyProvider terminologyProvider;
    private OntologyProvider ontologyProvider;
    private T8DataRecordPropertyValueStringGenerator valueStringGenerator;
    private ObjectFactory factory;

    public ExpressionPropertyMapping(T8Context context, T8ExpressionPropertyMappingDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public void initialize(TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, T8DataRecordPropertyValueStringGenerator valueStringGenerator)
    {
        this.terminologyProvider = terminologyProvider;
        this.ontologyProvider = ontologyProvider;
        this.valueStringGenerator = valueStringGenerator;
        this.factory = new ObjectFactory();
    }

    @Override
    public RecordContent.Property doPropertyMapping(DataRecord dataRecord) throws Exception
    {
        String docPathExpression;
        Object docPathResult;
        RecordContent.Property property;

        property = factory.createRecordContentProperty();
        property.setPropertyTerm(definition.getInterfaceTerm());

        docPathExpression = definition.getDocPathExpression();
        docPathResult = null;

        if(!Strings.isNullOrEmpty(docPathExpression))
        {
            DocPathExpressionEvaluator evaluator;

            evaluator = new DocPathExpressionEvaluator();
            evaluator.setTerminologyProvider(terminologyProvider);
            evaluator.setOntologyProvider(ontologyProvider);
            docPathResult = evaluator.evaluateExpression(dataRecord, docPathExpression);

            property.setValue(docPathResult != null ? docPathResult.toString() : "");
        }

        String epicExpression;
        Object epicExpressionResult;

        epicExpression = definition.getEpicExpression();
        epicExpressionResult = null;

        if(!Strings.isNullOrEmpty(epicExpression))
        {
            T8ServerExpressionEvaluator evaluator;
            Map<String, Object> parameters;

            evaluator = new T8ServerExpressionEvaluator(context);
            parameters = new HashMap<>();

            parameters.put("terminologyProvider", terminologyProvider);
            parameters.put("valueStringGenerator", valueStringGenerator);
            parameters.put("dataRecord", dataRecord);
            parameters.put("docPathResult", docPathResult);

            epicExpressionResult = evaluator.evaluateExpression(epicExpression, parameters, null);
            property.setValue(epicExpressionResult != null ? epicExpressionResult.toString() : null);
        }

       return property;
    }

    @Override
    public List<RecordContent.Property> doPropertyMappings(DataRecord dataRecord) throws Exception
    {
        throw new UnsupportedOperationException("This method is only supported by a Custom Proeprty Mapping."); //To change body of generated methods, choose Tools | Templates.
    }
}
