package com.pilog.t8.data.document.integration;

import static com.pilog.t8.definition.data.document.integration.T8IntegrationServiceAPIResources.*;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import java.io.StringWriter;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBElement;

/**
 * @author Hennie Brink
 */
public class T8IntegrationServicePersistenceHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8IntegrationServicePersistenceHandler.class);

    private static enum EventStatus {SUCCESS, FAILURE};

    private static enum EventDirection {SENT, RECEIVED};

    private final T8ServerContext serverContext;
    private final T8Context context;

    private String operationID;
    private String operationIID;

    public T8IntegrationServicePersistenceHandler(T8Context context)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
    }

    public void setOperationID(String operationID)
    {
        this.operationID = operationID;
    }

    public void setOperationIID(String operationIID)
    {
        this.operationIID = operationIID;
    }

    public <T extends Object> void logOperationSuccessEvent(JAXBElement<T> operationContent)
    {
        logOperationEvent(operationContent, EventStatus.SUCCESS, EventDirection.SENT);
    }

    public <T extends Object> void logOperationFailureEvent(JAXBElement<T> operationContent)
    {
        logOperationEvent(operationContent, EventStatus.FAILURE, EventDirection.SENT);
    }

    public <T extends Object> void logReceivedEvent(JAXBElement<T> operationContent)
    {
        logOperationEvent(operationContent, EventStatus.SUCCESS, EventDirection.RECEIVED);
    }

    private <T extends Object> void logOperationEvent(JAXBElement<T> operationContent, EventStatus status, EventDirection direction)
    {
        StringWriter writer;
        T8DataTransaction tx;
        T8DataEntity insertEntity;

        try
        {
            writer = new StringWriter();
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
            insertEntity = tx.create(DE_HISTORY_INTEGRATION_SERVICE, null);

            JAXB.marshal(operationContent, writer);

            insertEntity.setFieldValue(insertEntity.getIdentifier() + EF_EVENT_IID, T8IdentifierUtilities.createNewGUID());
            insertEntity.setFieldValue(insertEntity.getIdentifier() + EF_DIRECTION, direction.toString());
            insertEntity.setFieldValue(insertEntity.getIdentifier() + EF_STATUS, status.toString());
            insertEntity.setFieldValue(insertEntity.getIdentifier() + EF_OPERATION_ID, operationID);
            insertEntity.setFieldValue(insertEntity.getIdentifier() + EF_OPERATION_IID, operationIID);
            insertEntity.setFieldValue(insertEntity.getIdentifier() + EF_MESSAGE_DATA, writer.toString());

            tx.insert(insertEntity);

            writer.close();
        }
        catch (Exception e)
        {
            LOGGER.log("Failed to log operation event for operation " + operationID, e);
            throw new RuntimeException("Internal connection failure prevented the successful handling of the Integration Service request/response.");
        }
    }
}
