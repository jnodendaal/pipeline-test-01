package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8RestWebServicesVersion
{
    public final static String VERSION = "170";
}
