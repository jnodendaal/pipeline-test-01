/**
 * Created on Jun 19, 2017, 12:01:42 PM
 */
package com.pilog.t8.data.util.integration;

import java.io.StringReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * @author Gavin Boshoff
 */
public class IntegrationXmlHandler
{
    private final Unmarshaller responseUnmarshaller;

    public IntegrationXmlHandler()
    {
        try
        {
            responseUnmarshaller = JAXBContext.newInstance(OperationResponse.class).createUnmarshaller();
        }
        catch (JAXBException jaxbEx)
        {
            throw new RuntimeException("Failed to initialize the Integration XML Handler.", jaxbEx);
        }
    }

    public OperationResponse unmarshallIntegrationResponse(String operationResponseXml)
    {
        try
        {
            return (OperationResponse) responseUnmarshaller.unmarshal(new StringReader(operationResponseXml));
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Failed to unmarshall the Integration Response XML.", ex);
        }
    }
}
