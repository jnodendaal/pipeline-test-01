/**
 * Created on Jun 19, 2017, 11:26:04 AM
 */
package com.pilog.t8.data.util.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Gavin Boshoff
 */
@XmlRootElement(name = "operationResult")
@XmlAccessorType(XmlAccessType.NONE)
public class OperationResult
{
    @XmlElement(name = "errorMessage")
    private String errorMessage;

    @XmlElement(name = "errorCode")
    private String errorCode;

    @XmlElement(name = "success")
    private String success;

    @XmlElement(name = "record")
    private Record record;

    public OperationResult() {}

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode()
    {
        return errorCode;
    }

    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }

    public boolean isSuccess()
    {
        return "true".equals(success);
    }

    public String getSuccess()
    {
        return success;
    }

    public void setSuccess(String success)
    {
        this.success = success;
    }

    public Record getRecord()
    {
        return record;
    }

    public void setRecord(Record record)
    {
        this.record = record;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("OperationResult{");
        toStringBuilder.append("errorMessage=").append(this.errorMessage);
        toStringBuilder.append(",errorCode=").append(this.errorCode);
        toStringBuilder.append(",success=").append(this.success);
        toStringBuilder.append(",record=").append(this.record);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}