/**
 * Created on Jun 19, 2017, 11:26:29 AM
 */
package com.pilog.t8.data.util.integration;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Gavin Boshoff
 */
@XmlRootElement(name = "record")
@XmlAccessorType(XmlAccessType.NONE)
public class Record
{
    @XmlElements(@XmlElement(name = "organization", type = Organization.class))
    private List<Organization> organizations;

    @XmlElement(name = "externalReference")
    private String externalReference;

    @XmlElement(name = "recordTerm")
    private String recordTerm;

    @XmlElement(name = "recordID")
    private String recordId;

    public Record() {}

    public List<Organization> getOrganizations()
    {
        return organizations;
    }

    public void setOrganizations(List<Organization> organizations)
    {
        this.organizations = organizations;
    }

    public String getExternalReference()
    {
        return externalReference;
    }

    public void setExternalReference(String externalReference)
    {
        this.externalReference = externalReference;
    }

    public String getRecordTerm()
    {
        return recordTerm;
    }

    public void setRecordTerm(String recordTerm)
    {
        this.recordTerm = recordTerm;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("Record{");
        toStringBuilder.append("organizations=").append(this.organizations);
        toStringBuilder.append(",externalReference=").append(this.externalReference);
        toStringBuilder.append(",recordTerm=").append(this.recordTerm);
        toStringBuilder.append(",recordId=").append(this.recordId);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}