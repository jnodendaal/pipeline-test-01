/**
 * Created on Jun 19, 2017, 11:27:22 AM
 */
package com.pilog.t8.data.util.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Gavin Boshoff
 */
@XmlRootElement(name = "organization")
@XmlAccessorType(XmlAccessType.NONE)
public class Organization
{
    @XmlAttribute(name = "organizationTerm")
    private String organizationTerm;

    @XmlElement(name = "value")
    private String organizationCode;

    @XmlAttribute(name = "organizationID")
    private String organizationId;

    public Organization() {}

    public String getOrganizationTerm()
    {
        return organizationTerm;
    }

    public void setOrganizationTerm(String organizationTerm)
    {
        this.organizationTerm = organizationTerm;
    }

    public String getOrganizationCode()
    {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode)
    {
        this.organizationCode = organizationCode;
    }

    public String getOrganizationId()
    {
        return organizationId;
    }

    public void setOrganizationId(String organizationId)
    {
        this.organizationId = organizationId;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("Organization{");
        toStringBuilder.append("organizationTerm=").append(this.organizationTerm);
        toStringBuilder.append(",organizationCode=").append(this.organizationCode);
        toStringBuilder.append(",organizationId=").append(this.organizationId);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}