/**
 * Created on Jun 19, 2017, 11:24:59 AM
 */
package com.pilog.t8.data.util.integration;

import com.pilog.t8.utilities.strings.Strings;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Gavin Boshoff
 */
@XmlRootElement(namespace = "http://pilog.com/", name = "operationResponse")
@XmlAccessorType(XmlAccessType.NONE)
public class OperationResponse
{
    @XmlAttribute(name = "operationID")
    private String operationId;

    @XmlElements(@XmlElement(name = "operationResult", type = OperationResult.class))
    private List<OperationResult> operationResults;

    public OperationResponse() {}

    public String getOperationId()
    {
        return operationId;
    }

    public void setOperationId(String operationId)
    {
        this.operationId = operationId;
    }

    public List<OperationResult> getOperationResults()
    {
        return operationResults;
    }

    public void setOperationResults(List<OperationResult> operationResults)
    {
        this.operationResults = operationResults;
    }

    public String findExternalReference(String organizationId)
    {
        if (Strings.isNullOrEmpty(organizationId)) throw new NullPointerException("Cannot search for null Organization ID");
        Record record;

        for (OperationResult operationResult : operationResults)
        {
            record = operationResult.getRecord();
            for (Organization organization : record.getOrganizations())
            {
                if (organizationId.equals(organization.getOrganizationId()))
                {
                    return record.getExternalReference();
                }
            }
        }

        return null;
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("OperationResponse{");
        toStringBuilder.append("operationId=").append(this.operationId);
        toStringBuilder.append(",operationResults=").append(this.operationResults);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }
}