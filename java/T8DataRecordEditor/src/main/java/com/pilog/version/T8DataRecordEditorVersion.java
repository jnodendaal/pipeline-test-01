package com.pilog.version;

/**
 * @author  Bouwer du Preez
 */
public class T8DataRecordEditorVersion
{
    public final static String VERSION = "700";
}
