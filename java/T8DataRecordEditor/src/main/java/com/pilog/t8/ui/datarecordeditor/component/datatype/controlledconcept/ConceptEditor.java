package com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept;

import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.api.T8OrganizationClientApi;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarecord.access.layer.ConceptAccessLayer;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.ConceptValue;
import com.pilog.t8.data.document.datarecord.value.DependencyValue;
import com.pilog.t8.data.document.datarequirement.DataDependency;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8ConceptPair;
import com.pilog.t8.data.ontology.T8ConceptGraphPath;
import com.pilog.t8.data.ontology.T8ConceptGraphUtilities;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConcept.T8ConceptElementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.datastructures.tuples.DefaultPairComparator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.laf.T8ButtonUI;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.component.datatype.DefaultRecordValueEditor;
import com.pilog.t8.utilities.strings.StringUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import java.util.Collections;
import javax.swing.SwingUtilities;
import com.pilog.t8.ui.dialog.info.T8InformationDialog;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.ui.datarecordeditor.AutoCompletableEditor;
import com.pilog.t8.ui.datarecordeditor.ValueAutoCompleter;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Set;
import com.pilog.t8.utilities.format.DefaultFormatter;
import com.pilog.t8.utilities.format.RegexFormatter;
import java.util.regex.PatternSyntaxException;
import javax.swing.JFormattedTextField;
import javax.swing.text.DefaultFormatterFactory;
import java.math.BigDecimal;
import java.util.Arrays;
import javax.swing.SwingWorker;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Color;

import static com.pilog.t8.ui.datarecord.DataRecordEditorContainer.*;

/**
 * @author Bouwer du Preez
 */
public abstract class ConceptEditor extends DefaultRecordValueEditor implements AutoCompletableEditor
{
    private static final T8Logger LOGGER = T8Log.getLogger(ConceptEditor.class);

    private final T8LookAndFeelManager lafManager;
    private final T8ComponentController controller;
    private final T8OrganizationClientApi orgApi;
    private final T8OntologyClientApi ontApi;
    private final List<Pair<String, String>> conceptDependencies;
    private final T8OntologyStructure ontology;
    private final T8OrganizationStructure orgStructure;
    private String stringFormatPattern;
    private String stringMaskPattern;
    private Integer lengthRestriction;
    private String textCase;
    private boolean editableDependency;
    private boolean prescribed;
    private boolean allowNewValue;
    private boolean restrictToApprovedValue;
    private ValueAutoCompleter autoCompleter;
    private final TextKeyListener textKeyListener;
    private final T8DataRecordClientApi recApi;
    private final ValueRequirement ontologyClassRequirement;
    private final ConceptValue conceptValue;
    private final ConceptAccessLayer conceptAccessLayer;
    private String newText; // The new concept term currently entered (if allowed).
    private boolean verificationInProgress;

    public ConceptEditor(DataRecordEditorContainer container, RecordValue recordValue, ConceptAccessLayer conceptAccessLayer)
    {
        super(container, recordValue);

        initComponents();
        this.conceptValue = (ConceptValue)recordValue;
        this.conceptAccessLayer = conceptAccessLayer;
        this.controller = new T8DefaultComponentController(new T8Context(context), this.getClass().getCanonicalName(), false);
        this.lafManager = context.getClientContext().getConfigurationManager().getLookAndFeelManager(context);
        this.orgApi = controller.getApi(T8OrganizationClientApi.API_IDENTIFIER);
        this.ontApi = controller.getApi(T8OntologyClientApi.API_IDENTIFIER);
        this.ontologyClassRequirement = valueRequirement.getSubRequirement(RequirementType.ONTOLOGY_CLASS, null, null);
        this.conceptDependencies = new ArrayList<Pair<String, String>>();
        this.editableDependency = true;
        this.verificationInProgress = false;
        this.ontology = ontApi.getOntologyStructure();
        this.orgStructure = orgApi.getOrganizationStructure();
        this.jButtonClear.setUI(new ClearButtonUI(lafManager));
        this.jButtonLookup.setUI(new T8ButtonUI(lafManager));
        this.textKeyListener = new TextKeyListener();
        this.jFormattedTextFieldDisplay.addKeyListener(textKeyListener);
        this.recApi = (T8DataRecordClientApi)context.getClientContext().getConfigurationManager().getAPI(context, T8DataRecordClientApi.API_IDENTIFIER);
        setRequirementAttributes();
        applyFormatter();
        refreshDataDependency();
        refreshEditor();
        applyAutoCompletion();
    }

    private void setRequirementAttributes()
    {
        BigDecimal lengthRestrictionValue;

        lengthRestrictionValue = (BigDecimal)valueRequirement.getAttribute(RequirementAttribute.MAXIMUM_STRING_LENGTH.toString());
        lengthRestriction = lengthRestrictionValue != null ? lengthRestrictionValue.intValue() : 0;
        prescribed = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
        allowNewValue = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.ALLOW_NEW_VALUE.toString()));
        restrictToApprovedValue = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));
        textCase = (String)valueRequirement.getAttribute(RequirementAttribute.CASE.toString());
    }

    private void applyFormatter()
    {
        applyRegularExpressionFormatter(jFormattedTextFieldDisplay, ".*", lengthRestriction, textCase);
    }

    private void applyAutoCompletion()
    {
        autoCompleter = new ValueAutoCompleter(this, jFormattedTextFieldDisplay, container.getTerminologyProvider(), recApi);
        autoCompleter.setUpdateDelay(500); // The number of milliseconds to delay the update of auto-completion list while the user is changing data.
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();

        // Set access layer overrides to requirement attributes.
        if (!conceptAccessLayer.isAllowNewValue()) allowNewValue = false;

        // Set buttons enabled/disabled.
        jButtonLookup.setEnabled(editableAccess && editableDependency);
        jButtonClear.setEnabled(editableAccess && editableDependency);
        autoCompleter.setEnabled(editableAccess && editableDependency);

        // Set editability on the text field.
        jFormattedTextFieldDisplay.setEditable(editableAccess && editableDependency);
    }

    @Override
    public void refreshEditor()
    {
        String conceptId;

        conceptId = conceptValue.getConceptId();
        if (conceptId != null)
        {
            // Set the display String.
            jFormattedTextFieldDisplay.setText(getDisplayString(conceptId));

            // Set the definition as the tooltip on the text field.
            jFormattedTextFieldDisplay.setToolTipText(getToolTip(conceptId));

            // Set buttons enabled/disabled.
            jButtonLookup.setEnabled(editableAccess && editableDependency);
            jButtonClear.setEnabled(editableAccess && editableDependency);
        }
        else if (!verificationInProgress)
        {
            String term;

            term = conceptValue.getTerm();
            jFormattedTextFieldDisplay.setText(term);

            // Set buttons enabled/disabled.
            jButtonLookup.setEnabled(editableAccess && editableDependency);
            jButtonClear.setEnabled(editableAccess && editableDependency);
        }
    }

    @Override
    public boolean commitChanges()
    {
        // First check if the user has entered new text into the concept field.
        if (newText != null)
        {
            // If the new text in the field has been completely removed, simple clear the concept.
            if (Strings.trimToNull(newText) == null)
            {
                clearSelectedConcept();
                return true;
            }
            else
            {
                // If the new text has been entered, trim it and use at as the new concept term.
                newText = newText.trim();
                setSelectedTerm(newText);
                newText = null;

                if (allowNewValue)
                {
                    jFormattedTextFieldDisplay.setBackground(Color.WHITE);
                    return true;
                }
                else
                {
                    jFormattedTextFieldDisplay.setBackground(LAFConstants.INVALID_PINK);
                    return false;
                }
            }
        }
        else if (verificationInProgress)
        {
            jFormattedTextFieldDisplay.setBackground(LAFConstants.INVALID_PINK);
            return false;
        }
        else
        {
            jFormattedTextFieldDisplay.setBackground(Color.WHITE);
            return true;
        }
    }

    private void onFocusLost()
    {
        if (editableAccess && editableDependency)
        {
            // Hide suggestion popup.
            autoCompleter.hidePopup();

            // Commit all outstanding changes.
            commitChanges();

            // If no specific concept has been set on the record value, try to resolve it using the entered term.
            if ((conceptValue.getConceptId() == null) && (!Strings.isNullOrEmpty(conceptValue.getTerm())))
            {
                verificationInProgress = true;
                jFormattedTextFieldDisplay.setEditable(false);
                jFormattedTextFieldDisplay.setText("Verifying...");
                new ConceptVerifier(conceptValue.getTerm()).execute();
            }
        }
    }

    @Override
    public void refreshDataDependency()
    {
        Map<DataDependency, String> requiredConceptIds;
        List<Pair<String, String>> dependencyList;

        // Get the dependency values.
        requiredConceptIds = ((DependencyValue)recordValue).getDependencyParentConceptIds(container.getDataRecordProvider());

        // Create the new concept dependency set.
        dependencyList = new ArrayList<Pair<String, String>>();
        for (DataDependency dependencyRequirement : requiredConceptIds.keySet())
        {
            String dependencyConceptID;
            Pair<String, String> dependency;

            dependencyConceptID = requiredConceptIds.get(dependencyRequirement);
            dependency = new Pair<String, String>(dependencyRequirement.getRelationId(), dependencyConceptID);
            if (!dependencyList.contains(dependency)) dependencyList.add(dependency);
        }

        // Sort the dependency list (this step is important for consistency, since the order of the list depends on keys fetched from a map).
        Collections.sort(dependencyList, new DefaultPairComparator(true, true, false));

        // Set the new concept dependencies.
        conceptDependencies.clear();
        conceptDependencies.addAll(dependencyList);

        // Set editability.
        editableDependency = true;
    }

    private String getDisplayString(String conceptId)
    {
        String term;
        String code;
        String displayString;

        // Get the term and code for the value concept and construct a display String.
        term = terminologyProvider.getTerm(null, conceptId);
        code = terminologyProvider.getCode(conceptId);
        if (term != null)
        {
            // If the code is available, append it to the term.
            if (!Strings.isNullOrEmpty(code))
            {
                displayString = term + " (" + code + ")";

                // Make sure the display value is not too long.  If it is, use the term only.
                if (displayString.length() > 100) displayString = term;
            }
            else displayString = term;
        }
        else
        {
            displayString = "No Term Found";
        }

        // Return the resultant display String.
        return displayString;
    }

    private String getToolTip(String conceptId)
    {
        String definition;

        definition = terminologyProvider.getDefinition(null, conceptId);
        if (definition != null)
        {
            return StringUtilities.createToolTipHTML(StringUtilities.wrapText(100, definition));
        }
        else return null;
    }

    private void lookup()
    {
        Map<String, Object> selectedDialogValue;
        List<String> orgIdList;
        List<String> ocIdList;
        List<String> conceptIdList;

        // Check for dependent values and if we find any, get user confirmation first.
        if (!confirmRelatedDataChange()) return;

        // Create the list of accesible organization levels.
        orgIdList = getFilterOrgIdList();

        // Create a list of all groups to include (roots and their descendants).
        ocIdList = getFilterOntologyClassIdList();

        // Create a list of all the specific concepts to include in the filter.
        conceptIdList = getFilterConceptIdList();

        // Get the selected value.
        selectedDialogValue = container.getSelectedValue(dataRecord, valueRequirement, orgIdList, ocIdList, conceptIdList, new ArrayList<Pair<String, String>>(conceptDependencies));
        if (selectedDialogValue != null)
        {
            // Set the selected value of the editor.
            setSelectedValue(convertDialogOutputToConceptTerminology(selectedDialogValue));
        }
    }

    T8ConceptTerminology convertDialogOutputToConceptTerminology(Map<String, Object> selectedValueFields)
    {
        T8ConceptTerminology conceptTerminology;
        String conceptID;

        // Clear the namespace from the returned values.
        selectedValueFields = T8IdentifierUtilities.stripNamespace(selectedValueFields);

        // Get the selected concept ID from the selected entity/
        conceptID = (String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_ID);
        conceptTerminology = new T8ConceptTerminology(T8OntologyConceptType.VALUE, conceptID);

        // Create the concept terminology object so that we can use it to set the selected value of this editor.
        conceptTerminology.setLanguageId(context.getSessionContext().getContentLanguageIdentifier());
        conceptTerminology.setTermId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_TERM_ID));
        conceptTerminology.setTerm((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_TERM));
        conceptTerminology.setCodeId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_CODE_ID));
        conceptTerminology.setCode((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_CODE));
        conceptTerminology.setDefinitionId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_DEFINITION_ID));
        conceptTerminology.setDefinition((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_DEFINITION));
        return conceptTerminology;
    }

    List<String> getFilterOrgIdList()
    {
        List<String> orgIDList;
        String drInstanceOrgID;
        List<T8OntologyLink> drInstanceLinks;
        T8OntologyConcept drInstanceOntology;

        // Get the organization to which the Data Requirement instance is linked.
        drInstanceOntology = (T8OntologyConcept)propertyRequirement.getParentDataRequirementInstance().getOntology();
        if (drInstanceOntology == null) throw new RuntimeException("No Ontology Found in DR Instance object: " + propertyRequirement.getParentDataRequirementInstance());
        drInstanceLinks = drInstanceOntology.getOntologyLinks();
        if (drInstanceLinks.isEmpty()) throw new RuntimeException("No Organization Links Found in DR Instance Ontology object: " + drInstanceOntology);
        drInstanceOrgID = drInstanceLinks.get(0).getOrganizationID();

        // Create the list of accesible organization levels.
        orgIDList = new ArrayList<String>();
        orgIDList.addAll(orgStructure.getOrganizationLineageIDList(context.getSessionContext().getOrganizationIdentifier())); // Add a filter to include all concepts linked to the user's session organization lineage.
        orgIDList.retainAll(orgStructure.getOrganizationLineageIDList(drInstanceOrgID)); // In the list of organizations accessible by the user, retain only those that are also in the lineage of the organization to which the document is linked.
        T8Log.log("Including organizations: " + orgIDList);
        return orgIDList;
    }

    List<String> getFilterOntologyClassIdList()
    {
        List<String> odtIDList;
        Set<String> accessLayerFilterConceptGroupIDSet;

        // Create the filter ID sets.
        if (fieldAccessLayer != null)
        {
            accessLayerFilterConceptGroupIDSet = fieldAccessLayer.getFilterConceptGroupIDSet();
        }
        else
        {
            accessLayerFilterConceptGroupIDSet = propertyAccessLayer.getFilterConceptGroupIDSet();
        }

        // Create a list of all groups to include (roots and their descendants).
        odtIDList = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(accessLayerFilterConceptGroupIDSet))
        {
            for (String odtID : accessLayerFilterConceptGroupIDSet)
            {
                // Get the list of ODT_ID's to use for retrieval.
                odtIDList.add(odtID); // Add the group itself.
                odtIDList.addAll(ontology.getDescendantClassIDList(odtID)); // Add the descendants of the group.
            }
        }

        // If no specific filter was set, add the default group specified in the data requirement.
        if ((odtIDList.isEmpty()) && (ontologyClassRequirement != null))
        {
            String odtID;

            odtID = ontologyClassRequirement.getValue();
            odtIDList.add(odtID);
            odtIDList.addAll(ontology.getDescendantClassIDList(odtID)); // Add the descendants of the group.
        }

        // Return the list of groups to filter by.
        return odtIDList;
    }

    List<String> getFilterConceptIdList()
    {
        List<String> conceptIDList;
        Set<String> accessLayerFilterConceptIDSet;

        // Create the filter ID sets.
        if (fieldAccessLayer != null)
        {
            accessLayerFilterConceptIDSet = fieldAccessLayer.getFilterConceptIDSet();
        }
        else
        {
            accessLayerFilterConceptIDSet = propertyAccessLayer.getFilterConceptIDSet();
        }

        // Create a list of all the specific concepts to include in the filter.
        conceptIDList = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(accessLayerFilterConceptIDSet))
        {
            conceptIDList.addAll(accessLayerFilterConceptIDSet);
        }

        // Return the list of concepts to filter by.
        return conceptIDList;
    }

    abstract void clearSelectedConcept();
    abstract void setSelectedConcept(String conceptId);
    abstract void setSelectedTerm(String term);

    @Override
    public void setAutoCompletedValue(RecordValue value)
    {
        if (value != null)
        {
            newText = null;
            setSelectedConcept(value.getValue());
        }
    }

    @Override
    public void setAutoCompletionState(boolean valid, Object currentSelection)
    {
        if (valid)
        {
            jFormattedTextFieldDisplay.setBackground(Color.WHITE);
        }
        else
        {
            if (!allowNewValue)
            {
                jFormattedTextFieldDisplay.setBackground(LAFConstants.INVALID_PINK);
            }
        }
    }

    private boolean confirmRelatedDataChange()
    {
        List<DependencyValue> dependentValues;

        // Get all record values that are dependent on this one.
        dependentValues = ((DependencyValue)recordValue).getDependentValues();
        if (dependentValues.size() > 0)
        {
            boolean found;

            // Find al dependent values and check for at least one that actually has content that will be affected.
            found = false;
            for (DependencyValue value : dependentValues)
            {
                if (value.hasContent(false))
                {
                    found = true;
                    break;
                }
            }

            // If affected content was found, get the user confirmation, otherwise just proceed.
            if (found)
            {
                return T8InformationDialog.showConfirmationMessage(context, SwingUtilities.getWindowAncestor(this), translate("Confirmation"), translate("Changing this value will clear the related data.  Do you want to continue?"), createDependentValueString(dependentValues));
            }
            else return true;
        }
        else return true;
    }

    @Override
    public void addMouseListener(MouseListener listener)
    {
        super.addMouseListener(listener);
        jFormattedTextFieldDisplay.addMouseListener(listener);
        jButtonLookup.addMouseListener(listener);
        jButtonClear.addMouseListener(listener);
    }

    @Override
    public void removeMouseListener(MouseListener listener)
    {
        super.removeMouseListener(listener);
        jFormattedTextFieldDisplay.removeMouseListener(listener);
        jButtonLookup.removeMouseListener(listener);
        jButtonClear.removeMouseListener(listener);
    }

    private boolean isDependencyPathRequired()
    {
        if (conceptDependencies.isEmpty())
        {
            return false;
        }
        else
        {
            for (Pair<String, String> conceptDependency : conceptDependencies)
            {
                if (conceptDependency.getValue2() == null) return true;
            }

            return false;
        }
    }

    private synchronized void clearEditorValue()
    {
        // Remove selected value from the data record.
        clearSelectedConcept();

        // Clear the variable holding new text entered into the editor.
        newText = null;

        // Clear the UI text.
        jFormattedTextFieldDisplay.setText(null);

        // Clear the Tooltip.
        jFormattedTextFieldDisplay.setToolTipText(null);
    }

    private synchronized void setSelectedValue(T8ConceptTerminology conceptTerminology)
    {
        // First check that a valid concept is selected.
        if (conceptTerminology != null)
        {
            String selectedConceptId;

            // Get the selected concept ID.
            selectedConceptId = conceptTerminology.getConceptId();

            // Check whether or not we need to set the dependency path.
            if (isDependencyPathRequired())
            {
                if (dataDependencies.size() > 1)
                {
                    JOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(this), translate("The value cannot be set because it depends on multiple parent values."), translate("Invalid Selection"), JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                else if (dataDependencies.size() == 1)
                {
                    List<String> conceptRelationIds;

                    // Get the CONCEPT_GRAPH_ID path to use for resolution of parent dependencies.
                    conceptRelationIds = dataDependencies.get(0).getRelationIdPath();

                    // Fetch the dependency path from the server.
                    try
                    {
                        List<T8ConceptPair> edges;

                        // Fetch the edges defining the dependency path.
                        edges = ontApi.retrieveCrossConceptRelationPath(conceptRelationIds, selectedConceptId, context.getSessionContext().getContentLanguageIdentifier(), true, false, true);
                        if ((edges != null) && (edges.size() > 0))
                        {
                            if (T8ConceptGraphUtilities.treePaths(edges))
                            {
                                T8ConceptGraphPath path;

                                // Add the terminology of each edge to the cache.
                                for (T8ConceptPair edge : edges)
                                {
                                    terminologyProvider.addTerminology(edge.getTerminology());
                                }

                                // Create the concept graph path.
                                path = new T8ConceptGraphPath();
                                path.addPathEdges(edges);

                                // Set the dependency path values on the data record.
                                dataRecord.setAdjusting(true);
                                ((DependencyValue)recordValue).setDependencyValues(path);
                                dataRecord.setAdjusting(false, false);
                            }
                            else
                            {
                                JOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(this), translate("The relationship for the selected value defines more than one parent."), translate("Invalid Selection"), JOptionPane.INFORMATION_MESSAGE);
                                return;
                            }
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(this), translate("The selected value is not valid in this context."), translate("Invalid Selection"), JOptionPane.INFORMATION_MESSAGE);
                            return;
                        }
                    }
                    catch (Exception e)
                    {
                        T8Log.log("Exception while fetching dependency path in concept graph '" + conceptRelationIds + "', starting from concept ID '" + selectedConceptId + "'.", e);
                        return;
                    }
                }
            }

            // Add the selected terminology to the cache (this terminology will
            // not be in the cache, because not all terminology from the
            // controlled value group is cached when the editor is initialized.
            terminologyProvider.addTerminology(conceptTerminology);

            // Set the new value on the editor.
            newText = null;
            setSelectedConcept(selectedConceptId);
        }
        else
        {
            clearEditorValue();
        }

        // Refresh the UI.
        refreshEditor();
    }

    private boolean applyRegularExpressionFormatter(JFormattedTextField textField, String pattern, int lengthRestriction, String textCase)
    {
        try
        {
            RegexFormatter formatter;

            formatter = new RegexFormatter(pattern, lengthRestriction, textCase != null ? DefaultFormatter.Case.valueOf(textCase) : DefaultFormatter.Case.ANY);
            formatter.setAllowsInvalid(true);
            formatter.setOverwriteMode(false);
            formatter.setCommitsOnValidEdit(false);

            textField.setFocusLostBehavior(JFormattedTextField.COMMIT);
            textField.setFormatterFactory(new DefaultFormatterFactory(formatter));
            return true;
        }
        catch (PatternSyntaxException e)
        {
            LOGGER.log("While applying pattern '" + pattern + "' to component.", e);
            return false;
        }
    }

    private class TextKeyListener extends KeyAdapter
    {
        @Override
        public void keyReleased(KeyEvent e)
        {
            int keyCode;

            keyCode = e.getKeyCode();
            if ((keyCode != KeyEvent.VK_DOWN) && (keyCode != KeyEvent.VK_UP) && (keyCode != KeyEvent.VK_ENTER) && (keyCode != KeyEvent.VK_ESCAPE) && (keyCode != KeyEvent.VK_SHIFT))
            {
                // Get the text value from the display field.
                newText = jFormattedTextFieldDisplay.getText();
            }
        }
    }

    private static class ClearButtonUI extends T8ButtonUI
    {
        private final ImageIcon unarmedIcon;
        private final ImageIcon armedIcon;

        public ClearButtonUI(T8LookAndFeelManager lafManager)
        {
            super(lafManager);
            armedIcon = new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/cross-script.png"));
            unarmedIcon = new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"));
        }

        @Override
        protected void paintIcon(Graphics g, JComponent c, Rectangle iconRect)
        {
            Graphics2D g2;
            JButton button;

            g2 = (Graphics2D) g.create();
            button = (JButton) c;

            g2.translate(iconRect.x, iconRect.y);

            if(button.getModel().isRollover()) armedIcon.paintIcon(c, g2, 0, 0);
            else unarmedIcon.paintIcon(c, g2, 0, 0);

            g2.dispose();
        }
    }

    private class ConceptVerifier extends SwingWorker<String, Void>
    {
        private final String matchString;

        private ConceptVerifier(String matchString)
        {
            this.matchString = matchString;
        }

        @Override
        protected String doInBackground() throws Exception
        {
            List<T8ConceptElementType> elementTypes;
            String conceptId;

            LOGGER.log("Resolving concept id for text: " + matchString);
            elementTypes = Arrays.asList(T8ConceptElementType.values());
            conceptId = recApi.resolveConceptId(dataRecord, dataRecord.getID(), valueRequirement.getPropertyID(), valueRequirement.getFieldID(), valueRequirement.getRequirementType().getID(), matchString, elementTypes);
            return conceptId;
        }

        @Override
        public void done()
        {
            try
            {
                String conceptId;

                conceptId = get();
                if (conceptId != null)
                {
                    jFormattedTextFieldDisplay.setText("Verifying...");
                    setSelectedConcept(conceptId);
                    jFormattedTextFieldDisplay.setBackground(Color.WHITE);
                }
                else
                {
                    jFormattedTextFieldDisplay.setText(matchString);
                    jFormattedTextFieldDisplay.setEditable(true);
                    jFormattedTextFieldDisplay.setBackground(allowNewValue ? Color.WHITE : LAFConstants.INVALID_PINK);
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while resolving concept ID for new text: " + matchString, e);
            }
            finally
            {
                verificationInProgress = false;
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jFormattedTextFieldDisplay = new javax.swing.JFormattedTextField();
        jButtonLookup = new javax.swing.JButton();
        jButtonClear = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jFormattedTextFieldDisplay.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                jFormattedTextFieldDisplayFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jFormattedTextFieldDisplay, gridBagConstraints);

        jButtonLookup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/searchIcon.png"))); // NOI18N
        jButtonLookup.setToolTipText("Search for Data Value");
        jButtonLookup.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonLookup.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonLookupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
        add(jButtonLookup, gridBagConstraints);

        jButtonClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"))); // NOI18N
        jButtonClear.setToolTipText("Clear Data Value");
        jButtonClear.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonClear.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonClearActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 5);
        add(jButtonClear, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonLookupActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonLookupActionPerformed
    {//GEN-HEADEREND:event_jButtonLookupActionPerformed
        lookup();
    }//GEN-LAST:event_jButtonLookupActionPerformed

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonClearActionPerformed
    {//GEN-HEADEREND:event_jButtonClearActionPerformed
        // Check for dependent values and if we find any, get user confirmation first.
        if (confirmRelatedDataChange())
        {
            clearEditorValue();
        }
    }//GEN-LAST:event_jButtonClearActionPerformed

    private void jFormattedTextFieldDisplayFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_jFormattedTextFieldDisplayFocusLost
    {//GEN-HEADEREND:event_jFormattedTextFieldDisplayFocusLost
        onFocusLost();
    }//GEN-LAST:event_jFormattedTextFieldDisplayFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClear;
    private javax.swing.JButton jButtonLookup;
    private javax.swing.JFormattedTextField jFormattedTextFieldDisplay;
    // End of variables declaration//GEN-END:variables
}
