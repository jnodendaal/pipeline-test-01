package com.pilog.t8.ui.datarecordeditor.component.propertytype;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultPropertyValueComponent extends JPanel implements DataRecordEditor
{
    private final DataRecordEditorContainer container;
    private final T8Context context;
    private final T8ConfigurationManager configurationManager;
    private final SectionRequirement sectionRequirement;
    private final List<T8DataValidationError> validationErrors;
    private final List<T8DataValidationError> accessValidationErrors;
    private final List<T8DataValidationError> requirementValidationErrors;
    private DataRecordEditor recordValueEditor;
    private final DataRecord dataRecord;
    private final RecordProperty recordProperty;
    private final PropertyRequirement propertyRequirement;
    private final PropertyAccessLayer accessLayer;
    private boolean entryRequired; // Flag to indicate if entry of this property is required.

    public T8DefaultPropertyValueComponent(DataRecordEditorContainer container, RecordProperty recordProperty)
    {
        this.dataRecord = recordProperty.getParentDataRecord();
        this.recordProperty = recordProperty;
        this.propertyRequirement = recordProperty.getPropertyRequirement();
        this.accessLayer = dataRecord.getAccessLayer().getPropertyAccessLayer(recordProperty.getPropertyID());
        this.sectionRequirement = propertyRequirement.getParentSectionRequirement();
        this.context = container.getAccessContext();
        this.configurationManager = context.getClientContext().getConfigurationManager();

        // Now initialize the UI components.
        initComponents();
        this.container = container;
        this.validationErrors = new ArrayList<T8DataValidationError>();
        this.accessValidationErrors = new ArrayList<T8DataValidationError>();
        this.requirementValidationErrors = new ArrayList<T8DataValidationError>();
        this.setBackground(LAFConstants.CONTENT_GREY);

        init();
        bindKeys();
        refreshAccess();
    }

    private void init()
    {
        ValueRequirement propertyValueRequirement;
        String fft;

        propertyValueRequirement = propertyRequirement.getValueRequirement();
        if (propertyValueRequirement != null)
        {
            RecordValue recordValue;

            // Get the property value or create it if required.
            recordValue = recordProperty.getRecordValue();
            if (recordValue == null)
            {
                recordValue = RecordValue.createValue(propertyValueRequirement);
                recordProperty.setRecordValue(recordValue);
            }

            // Create the value editor.
            recordValueEditor = container.getEditorComponentFactory().createDataRecordEditorComponent(recordValue);
            if (recordValueEditor != null)
            {
                jPanelContent.add((Component)recordValueEditor, java.awt.BorderLayout.CENTER);
                jPanelContent.revalidate();
            }
        }

        // Set the value and FFT Strings.
        fft = recordProperty.getFFT();
        jTextAreaFFT.setText(fft);

        // If there is FFT text, show the FFT component.
        if (!Strings.isNullOrEmpty(fft))
        {
            setFFTVisible(true);
        }
    }

    @Override
    public void refreshEditor()
    {
        if (recordValueEditor != null)
        {
            recordValueEditor.refreshEditor();
        }
    }

    @Override
    public void refreshDataDependency()
    {
        if (recordValueEditor != null)
        {
            recordValueEditor.refreshDataDependency();
        }
    }

    @Override
    public void refreshAccess()
    {
        jTextAreaFFT.setEditable(accessLayer.isEditable() && accessLayer.isFftEditable());
        entryRequired = (accessLayer.isRequired());
        if (recordValueEditor != null)
        {
            recordValueEditor.refreshAccess();
        }
    }

    private void bindKeys()
    {
        InputMap inputMap;
        ActionMap actionMap;

        // Get the correct InputMap and ActionMap.
        inputMap = this.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        actionMap = this.getActionMap();

        // Create a key binding for the Enter key pressed.
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "enterPressed");
        actionMap.put("enterPressed", new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
            }
        });
    }

    public void setFFTVisible(boolean visible)
    {
        if (visible)
        {
            jPanelFFT.setVisible(true);
        }
        else if (Strings.isNullOrEmpty(jTextAreaFFT.getText()))
        {
            // Only hide the FFT if it is empty.
            jPanelFFT.setVisible(false);
        }

        // Revalidate the panel.
        revalidate();
    }

    public PropertyRequirement getPropertyRequirement()
    {
        return propertyRequirement;
    }

    @Override
    public DataRecordEditorContainer getDataRecordEditorContainer()
    {
        return container;
    }

    @Override
    public boolean hasChanges()
    {
        if (recordValueEditor != null) return recordValueEditor.hasChanges();
        else return false;
    }

    @Override
    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    @Override
    public Value getValue()
    {
        return recordProperty;
    }

    @Override
    public PropertyRequirement getRequirement()
    {
        return propertyRequirement;
    }

    @Override
    public boolean commitChanges()
    {
        // If the Record Property is null, create it.
        if (recordProperty == null) throw new RuntimeException("Commit changes called on property editor wihtout any property value set: " + propertyRequirement);

        // Commit the FFT and value editor changes.
        if (recordValueEditor != null)
        {
            recordProperty.setFFT(jTextAreaFFT.getText());
            return recordValueEditor.commitChanges();
        }
        else
        {
            recordProperty.setRecordValue(null);
            recordProperty.setFFT(null);
            return true;
        }
    }

    @Override
    public void setValidationErrors(List<T8DataValidationError> validationErrors)
    {
        this.validationErrors.clear();
        if (validationErrors != null) this.validationErrors.addAll(validationErrors);
        if (recordValueEditor != null) recordValueEditor.setValidationErrors(validationErrors);
    }

    @Override
    public List<T8DataValidationError> getValidationErrors()
    {
        List<T8DataValidationError> errors;

        errors = new ArrayList<T8DataValidationError>();
        errors.addAll(requirementValidationErrors);
        errors.addAll(accessValidationErrors);
        errors.addAll(validationErrors);
        if (recordValueEditor != null) errors.addAll(recordValueEditor.getValidationErrors());

        return errors;
    }

    public void setSelected(boolean selected)
    {
        setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.WHITE_LINE_BORDER);
    }

    private String translate(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    @Override
    public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed)
    {
        return super.processKeyBinding(ks, e, condition, pressed);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelEditor = new javax.swing.JPanel();
        jPanelContent = new javax.swing.JPanel();
        jPanelFFT = new javax.swing.JPanel();
        jLabelFFT = new javax.swing.JLabel();
        jTextAreaFFT = new javax.swing.JTextArea();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        setLayout(new java.awt.GridBagLayout());

        jPanelEditor.setOpaque(false);
        jPanelEditor.setLayout(new java.awt.GridBagLayout());

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelEditor.add(jPanelContent, gridBagConstraints);

        jPanelFFT.setOpaque(false);
        jPanelFFT.setLayout(new java.awt.GridBagLayout());

        jLabelFFT.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelFFT.setText(translate("FFT").concat(":"));
        jLabelFFT.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelFFT.add(jLabelFFT, gridBagConstraints);

        jTextAreaFFT.setColumns(20);
        jTextAreaFFT.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextAreaFFT.setLineWrap(true);
        jTextAreaFFT.setRows(1);
        jTextAreaFFT.setWrapStyleWord(true);
        jTextAreaFFT.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(171, 173, 179)), javax.swing.BorderFactory.createEmptyBorder(2, 2, 2, 2)));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanelFFT.add(jTextAreaFFT, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelEditor.add(jPanelFFT, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelEditor, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelFFT;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelEditor;
    private javax.swing.JPanel jPanelFFT;
    private javax.swing.JTextArea jTextAreaFFT;
    // End of variables declaration//GEN-END:variables
}
