package com.pilog.t8.ui.datarecordeditor.component.propertytype;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.access.layout.T8PropertyLayout;
import com.pilog.t8.data.document.datarecord.access.layout.T8PropertyLayout.PropertyHeaderLayoutType;
import com.pilog.t8.data.document.datarecord.access.layout.T8PropertyLayout.PropertyHeaderTextType;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.T8DataRequirementComment;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultPropertyHeaderComponent extends javax.swing.JPanel
{
    private final T8DefaultPropertyValueComponent propertyComponent;
    private final PropertyRequirement propertyRequirement;
    private final TerminologyProvider terminologyProvider;
    private final PropertyAccessLayer propertyAccessLayer;
    private final T8PropertyLayout propertyLayout;
    private JTextPane jTextPaneHeader;

    private static final Font KEY_FONT = new java.awt.Font("Tahoma", 1, 11);
    private static final Font NON_KEY_FONT = new java.awt.Font("Tahoma", 0, 11);

    public T8DefaultPropertyHeaderComponent(DataRecordEditorContainer container, RecordProperty recordProperty, T8DefaultPropertyValueComponent propertyComponent)
    {
        initComponents();
        this.propertyComponent = propertyComponent;
        this.propertyRequirement = recordProperty.getPropertyRequirement();
        this.propertyAccessLayer = recordProperty.getParentDataRecord().getAccessLayer().getPropertyAccessLayer(recordProperty.getPropertyID());
        this.terminologyProvider = container.getTerminologyProvider();
        this.propertyLayout = propertyAccessLayer.getLayout();
        this.setBackground(LAFConstants.PROPERTY_BLUE);
        configureHeader();
        setKey(propertyRequirement.isCharacteristic());
        setEntryRequired(propertyAccessLayer.isRequired());
        jTextPaneHeader.setToolTipText(createToolTipText(null));
    }

    private void configureHeader()
    {
        jTextPaneHeader = new JTextPane();
        jTextPaneHeader.setFont(NON_KEY_FONT);
        jTextPaneHeader.setOpaque(false);
        jTextPaneHeader.setEditable(false);

        if (propertyLayout != null)
        {
            if (propertyLayout.getHeaderTextType() ==  PropertyHeaderTextType.TERM)
            {
                jTextPaneHeader.setText(propertyRequirement != null ? terminologyProvider.getTerm(null, propertyRequirement.getConceptID()) : null);
            }
            else
            {
                jTextPaneHeader.setText(propertyRequirement != null ? terminologyProvider.getDefinition(null, propertyRequirement.getConceptID()) : null);
            }

            if (propertyLayout.getHeaderLayoutType() == PropertyHeaderLayoutType.TOP)
            {
                GridBagConstraints gridBagConstraints;

                setLeftAlignment();

                gridBagConstraints = new GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 0;
                gridBagConstraints.fill = GridBagConstraints.BOTH;
                gridBagConstraints.weightx = 0.0;
                gridBagConstraints.weighty = 0.0;
                gridBagConstraints.insets = new Insets(5, 5, 5, 0);
                gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
                add(jLabelRequired, gridBagConstraints);

                gridBagConstraints = new GridBagConstraints();
                gridBagConstraints.gridx = 1;
                gridBagConstraints.gridy = 0;
                gridBagConstraints.fill = GridBagConstraints.BOTH;
                gridBagConstraints.weightx = 0.1;
                gridBagConstraints.weighty = 0.1;
                gridBagConstraints.insets = new Insets(5, 5, 5, 5);
                gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
                add(jTextPaneHeader, gridBagConstraints);
            }
            else
            {
                GridBagConstraints gridBagConstraints;

                setRightAlignment();

                gridBagConstraints = new GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 0;
                gridBagConstraints.fill = GridBagConstraints.BOTH;
                gridBagConstraints.weightx = 0.1;
                gridBagConstraints.weighty = 0.1;
                gridBagConstraints.insets = new Insets(5, 5, 5, 5);
                gridBagConstraints.anchor = GridBagConstraints.NORTHEAST;
                add(jTextPaneHeader, gridBagConstraints);

                gridBagConstraints = new GridBagConstraints();
                gridBagConstraints.gridx = 1;
                gridBagConstraints.gridy = 0;
                gridBagConstraints.fill = GridBagConstraints.BOTH;
                gridBagConstraints.weightx = 0.0;
                gridBagConstraints.weighty = 0.0;
                gridBagConstraints.insets = new Insets(5, 0, 5, 5);
                gridBagConstraints.anchor = GridBagConstraints.NORTHEAST;
                add(jLabelRequired, gridBagConstraints);
            }
        }
        else
        {
            GridBagConstraints gridBagConstraints;

            jTextPaneHeader.setText(propertyRequirement != null ? terminologyProvider.getTerm(null, propertyRequirement.getConceptID()) : null);

            setRightAlignment();

            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 0.1;
            gridBagConstraints.weighty = 0.1;
            gridBagConstraints.insets = new Insets(5, 5, 5, 5);
            gridBagConstraints.anchor = GridBagConstraints.NORTHEAST;
            add(jTextPaneHeader, gridBagConstraints);

            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 1;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 0.0;
            gridBagConstraints.weighty = 0.0;
            gridBagConstraints.insets = new Insets(5, 0, 5, 5);
            gridBagConstraints.anchor = GridBagConstraints.NORTHEAST;
            add(jLabelRequired, gridBagConstraints);
        }
    }

    private void setLeftAlignment()
    {
        StyledDocument doc = jTextPaneHeader.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_LEFT);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }

    private void setRightAlignment()
    {
        StyledDocument doc = jTextPaneHeader.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_RIGHT);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }

    public PropertyRequirement getPropertyRequirement()
    {
        return propertyRequirement;
    }

    public T8DefaultPropertyValueComponent getPropertyComponent()
    {
        return propertyComponent;
    }

    public final void setKey(boolean key)
    {
        jTextPaneHeader.setFont(key ? KEY_FONT : NON_KEY_FONT);
    }

    public final void setEntryRequired(boolean entryRequired)
    {
        jLabelRequired.setVisible(entryRequired);
    }

    public void setSelected(boolean selected)
    {
        setBorder(selected ? LAFConstants.GRAY_LINE_BORDER : LAFConstants.WHITE_LINE_BORDER);
    }

    public void refreshValidityDisplay()
    {
        List<T8DataValidationError> propertyValidationErrors;

        // First get all requirement validation errors for the property and add them to the normal validation errors.
        propertyValidationErrors = propertyComponent.getValidationErrors();

        // Update the visual color of the header according to its validity.
        if (!propertyValidationErrors.isEmpty())
        {
            this.setBackground(LAFConstants.INVALID_PINK);
            jTextPaneHeader.setToolTipText(createToolTipText(propertyValidationErrors));
        }
        else
        {
            this.setBackground(LAFConstants.PROPERTY_BLUE);
            jTextPaneHeader.setToolTipText(createToolTipText(propertyValidationErrors));
        }
    }

    private String createToolTipText(List<T8DataValidationError> errors)
    {
        List<T8DataRequirementComment> comments;
        StringBuffer text;
        String definition;
        String formatDescription;

        definition = terminologyProvider.getDefinition(null, propertyRequirement.getConceptID());
        if (!propertyRequirement.isComposite())
        {
            ValueRequirement valueRequirement;

            valueRequirement = propertyRequirement.getValueRequirement();
            formatDescription = valueRequirement != null ? (String)valueRequirement.findRequirementAttribute(RequirementAttribute.FORMAT_DESCRIPTION.getDisplayName()) : null;
        }
        else // Format descriptions will be displayed on field headers.
        {
            formatDescription = null;
        }

        // Create a new buffer.
        text = new StringBuffer();
        text.append("<html>");

        // Append the definition.
        text.append("<h2>Definition</h2>");
        text.append("<p width=\"500\">");
        text.append(definition != null ? definition : "Not Available");
        text.append("</p>");

        // Append the format description if we found one.
        if (!Strings.isNullOrEmpty(formatDescription))
        {
            text.append("<h2>Format Description</h2>");
            text.append("<p width=\"500\">");
            text.append(formatDescription);
            text.append("</p>");
        }

        // Append the comments for this property.
        comments = propertyRequirement.getParentDataRequirementInstance().getPropertyComments(propertyRequirement.getConceptID());
        if (comments.size() > 0)
        {
            text.append("<h2>Comments</h2>");
            text.append("<ul width=\"500\">");
            for (T8DataRequirementComment comment : comments)
            {
                text.append("<li>");
                text.append(terminologyProvider.getTerm(null, comment.getTypeId()));
                text.append(": ");
                text.append(comment.getComment());
                text.append("</li>");
            }
            text.append("</ul>");
        }

        // Append the validation errors (if any).
        if ((errors != null) && (errors.size() > 0))
        {
            Set<String> addedMessages;

            // Create a set to hold all messages appended to the list.
            addedMessages = new HashSet<String>();

            text.append("<h2>Validation Errors</h2>");
            text.append("<ul>");
            for (T8DataValidationError error : errors)
            {
                String message;

                // Get the error message and only append it to the list if we have not already appended the same message.
                message = error.getErrorMessage();
                if (!addedMessages.contains(message))
                {
                    text.append("<li>");
                    text.append(message);
                    text.append("</li>");
                    addedMessages.add(message);
                }
            }
            text.append("</ul>");
        }

        text.append("</html>");
        return text.toString();
    }

    public void refreshAccess()
    {
        setEntryRequired(propertyAccessLayer.isRequired());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jLabelRequired = new javax.swing.JLabel();

        jLabelRequired.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabelRequired.setForeground(new java.awt.Color(255, 0, 51));
        jLabelRequired.setText("*");

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelRequired;
    // End of variables declaration//GEN-END:variables
}
