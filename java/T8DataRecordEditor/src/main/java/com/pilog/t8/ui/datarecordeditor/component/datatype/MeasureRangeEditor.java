package com.pilog.t8.ui.datarecordeditor.component.datatype;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.datarecord.access.layer.MeasuredRangeAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.LowerBoundNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.datarecord.value.UpperBoundNumber;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.AutoCompletableEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept.ConceptEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept.QOMConceptEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept.UOMConceptEditor;
import java.awt.BorderLayout;
import java.awt.Dimension;

/**
 * @author Bouwer du Preez
 */
public class MeasureRangeEditor extends DefaultRecordValueEditor implements AutoCompletableEditor
{
    private final MeasuredRange range;
    private final MeasuredRangeAccessLayer mrAccessLayer;
    private DefaultRecordValueEditor lowerValueEditor;
    private DefaultRecordValueEditor upperValueEditor;
    private ConceptEditor uomEditor;
    private ConceptEditor qomEditor;
    private boolean prescribed;

    public MeasureRangeEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        this.range = (MeasuredRange)recordValue;
        this.mrAccessLayer = (MeasuredRangeAccessLayer)valueAccessLayer;
        initComponents();
        setRequirementAttributes();
        initializeNumericEditors();
        initializeConceptEditors();
        refreshEditor();
    }

    private void setRequirementAttributes()
    {
        prescribed = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
    }

    private void initializeNumericEditors()
    {
        LowerBoundNumber lowerBoundNumericValue;
        UpperBoundNumber upperBoundNumericValue;

        // Try to find an existing numeric value or create a new one if it does not yet exist.
        lowerBoundNumericValue = range.getLowerBoundNumberObject();
        if (lowerBoundNumericValue == null)
        {
            // No value found, so create one.
            lowerBoundNumericValue = range.setLowerBoundNumber(null);
        }

        // Create the numeric editor.
        lowerValueEditor = new StringEditor(container, lowerBoundNumericValue, prescribed ? this : null, true);
        jPanelLowerValueSlot.add(lowerValueEditor, java.awt.BorderLayout.CENTER);

        // Try to find an existing numeric value or create a new one if it does not yet exist.
        upperBoundNumericValue = range.getUpperBoundNumberObject();
        if (upperBoundNumericValue == null)
        {
            // No value found, so create one.
            upperBoundNumericValue = range.setUpperBoundNumber(null);
        }

        // Create the numeric editor.
        upperValueEditor = new StringEditor(container, upperBoundNumericValue, prescribed ? this : null, true);
        jPanelUpperValueSlot.add(upperValueEditor, java.awt.BorderLayout.CENTER);
    }

    private void initializeConceptEditors()
    {
        RecordValue uomValue;
        RecordValue qomValue;
        Dimension size;

        // Get the UOM value and construct an editor for it.
        uomValue = recordValue.getSubValue(RequirementType.UNIT_OF_MEASURE, null, null);
        if (uomValue == null)
        {
            ValueRequirement uomRequirement;

            uomRequirement = valueRequirement.getDescendentRequirement(RequirementType.UNIT_OF_MEASURE, null, null);
            if (uomRequirement != null)
            {
                uomValue = RecordValue.createValue(uomRequirement);
                recordValue.setSubValue(uomValue);
            }
            else throw new RuntimeException("Measured Number without UOM specification found in: " + propertyRequirement);
        }

        // Create the UOM editor.
        size = new Dimension(250, 10);
        uomEditor = new UOMConceptEditor(container, uomValue, mrAccessLayer.getUomAccessLayer());
        uomEditor.setMinimumSize(size);
        uomEditor.setPreferredSize(size);
        jPanelUOMSlot.add(uomEditor, BorderLayout.CENTER);

        // Get the QOM value and construct an editor for it.
        qomValue = recordValue.getSubValue(RequirementType.QUALIFIER_OF_MEASURE, null, null);
        if (qomValue == null)
        {
            ValueRequirement qomRequirement;

            qomRequirement = valueRequirement.getDescendentRequirement(RequirementType.QUALIFIER_OF_MEASURE, null, null);
            if (qomRequirement != null)
            {
                qomValue = RecordValue.createValue(qomRequirement);
                recordValue.setSubValue(qomValue);
            }
        }

        // Create the QOM editor (optional).
        if (qomValue != null)
        {
            size = new Dimension(250, 10);
            qomEditor = new QOMConceptEditor(container, qomValue);
            qomEditor.setMinimumSize(size);
            qomEditor.setPreferredSize(size);
            jPanelQOMSlot.add(qomEditor, BorderLayout.CENTER);
        }
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();

        // Refresh sub-editor access.
        lowerValueEditor.refreshAccess();
        upperValueEditor.refreshAccess();
        if (uomEditor != null) uomEditor.refreshAccess();
        if (qomEditor != null) qomEditor.refreshAccess();
    }

    @Override
    public final void refreshEditor()
    {
        // Execute the default behavior.
        super.refreshEditor();

        // Refresh child editors.
        lowerValueEditor.refreshEditor();
        upperValueEditor.refreshEditor();
        if (uomEditor != null) uomEditor.refreshEditor();
        if (qomEditor != null) qomEditor.refreshEditor();
    }

    @Override
    public boolean commitChanges()
    {
        System.out.println("Committing range: lv:" + ((RecordValue)lowerValueEditor.getValue()).getValue());
        System.out.println("Committing range: uv:" + ((RecordValue)upperValueEditor.getValue()).getValue());
        // Commit sub-editor changes.
        lowerValueEditor.commitChanges();
        upperValueEditor.commitChanges();
        if (uomEditor != null) uomEditor.commitChanges();
        if (qomEditor != null) qomEditor.commitChanges();
        System.out.println("Committed range: lv:" + ((RecordValue)lowerValueEditor.getValue()).getValue());
        System.out.println("Committed range: uv:" + ((RecordValue)upperValueEditor.getValue()).getValue());
        return true;
    }

    @Override
    public void setAutoCompletedValue(RecordValue autoCompletedValue)
    {
        // TODO:  Implement setting of the auto-completed value.
        T8Log.log("Auto-completed value: " + autoCompletedValue);
        if (autoCompletedValue != null)
        {
            MeasuredRange autoCompletedRange;

            // We are going to do a multi-step update, so set the adjusting flag.
            dataRecord.setAdjusting(true);

            // Do the value update.
            autoCompletedRange = (MeasuredRange)autoCompletedValue;
            range.setPrescribedValueID(autoCompletedRange.getPrescribedValueID());
            range.setLowerBoundNumber(autoCompletedRange.getLowerBoundNumber());
            range.setUpperBoundNumber(autoCompletedRange.getUpperBoundNumber());
            range.setUomId(autoCompletedRange.getUomId());
            range.setQomId(autoCompletedRange.getQomId());
            System.out.println("Setting UOM: " + autoCompletedRange.getUomId());
            System.out.println("Setting QOM: " + autoCompletedRange.getQomId());

            if (uomEditor != null) System.out.println("UOM Editor value: " + uomEditor.getValue());

            // Multi-step update complete.
            dataRecord.setAdjusting(false);
        }
    }

    @Override
    public void setAutoCompletionState(boolean valid, Object currentSelection)
    {
    }

    @Override
    public DataRecord getContextRecord()
    {
        return dataRecord.getDataFile();
    }

    @Override
    public RecordValue getTargetValue()
    {
        return recordValue;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelLowerValueSlot = new javax.swing.JPanel();
        jLabelTo = new javax.swing.JLabel();
        jPanelUpperValueSlot = new javax.swing.JPanel();
        jPanelUOMSlot = new javax.swing.JPanel();
        jPanelQOMSlot = new javax.swing.JPanel();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelLowerValueSlot.setOpaque(false);
        jPanelLowerValueSlot.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelLowerValueSlot, gridBagConstraints);

        jLabelTo.setText(translate("to"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelTo, gridBagConstraints);

        jPanelUpperValueSlot.setOpaque(false);
        jPanelUpperValueSlot.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelUpperValueSlot, gridBagConstraints);

        jPanelUOMSlot.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jPanelUOMSlot, gridBagConstraints);

        jPanelQOMSlot.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jPanelQOMSlot, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelTo;
    private javax.swing.JPanel jPanelLowerValueSlot;
    private javax.swing.JPanel jPanelQOMSlot;
    private javax.swing.JPanel jPanelUOMSlot;
    private javax.swing.JPanel jPanelUpperValueSlot;
    // End of variables declaration//GEN-END:variables

}
