package com.pilog.t8.ui.datarecordeditor.component.sectiontype;

import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.SectionAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarecord.access.layout.T8PropertyLayout;
import com.pilog.t8.data.document.datarecord.access.layout.T8PropertyLayout.PropertyHeaderLayoutType;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.component.propertytype.T8DefaultPropertyHeaderComponent;
import com.pilog.t8.ui.datarecordeditor.component.propertytype.T8DefaultPropertyValueComponent;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultSectionEditor extends JPanel implements DataRecordEditor
{
    private final DataRecordEditorContainer container;
    private final LinkedHashMap<String, T8DefaultPropertyHeaderComponent> propertyHeaderComponents;
    private final LinkedHashMap<String, DataRecordEditor> propertyEditors;
    private final CachedTerminologyProvider terminologyProvider;
    private final T8ContentHeaderBorder sectionHeaderBorder;
    private final DataRecord dataRecord;
    private final RecordSection recordSection;
    private final SectionRequirement sectionRequirement;
    private final SectionAccessLayer accessLayer;

    public static final Color HEADER_COLOR = new Color(240, 240, 240);

    public T8DefaultSectionEditor(DataRecordEditorContainer container, RecordSection section)
    {
        this.dataRecord = section.getParentDataRecord();
        this.recordSection = section;
        this.sectionRequirement = section.getSectionRequirement();
        this.accessLayer = dataRecord.getAccessLayer().getSectionAccessLayer(section.getSectionID());

        initComponents();

        this.container = container;
        this.propertyHeaderComponents = new LinkedHashMap<String, T8DefaultPropertyHeaderComponent>();
        this.propertyEditors = new LinkedHashMap<String, DataRecordEditor>();
        this.terminologyProvider = container.getTerminologyProvider();
        this.sectionHeaderBorder = new T8ContentHeaderBorder(terminologyProvider.getTerm(null, sectionRequirement.getConceptID()));
        this.setBorder(sectionHeaderBorder);

        init();
    }

    private void init()
    {
        boolean propertyVisible;
        int gridY;

        gridY = 0;
        propertyVisible = false;
        jPanelContent.removeAll();
        propertyHeaderComponents.clear();
        propertyEditors.clear();
        if (sectionRequirement != null)
        {
            for (PropertyRequirement propertyRequirement : sectionRequirement.getPropertyRequirements())
            {
                T8DefaultPropertyValueComponent editorComponent;
                T8DefaultPropertyHeaderComponent headerComponent;
                GridBagConstraints gridBagConstraints;
                T8PropertyLayout propertyLayout;
                PropertyAccessLayer propertyAccessLayer;
                RecordProperty recordProperty;
                boolean visible;

                // Get the record property or construct it if required.
                recordProperty = dataRecord.getRecordProperty(propertyRequirement.getConceptID());
                if (recordProperty == null)
                {
                    recordProperty = new RecordProperty(propertyRequirement);
                    dataRecord.getRecordSection(sectionRequirement.getConceptID()).addRecordProperty(recordProperty);
                }

                // Get the property access layer.
                propertyAccessLayer = accessLayer.getPropertyAccessLayer(propertyRequirement.getConceptID());
                propertyLayout = propertyAccessLayer.getLayout();
                visible = propertyAccessLayer.isVisible();
                if (visible) propertyVisible = true;

                editorComponent = new T8DefaultPropertyValueComponent(container, recordProperty);
                editorComponent.setVisible(visible);
                headerComponent = new T8DefaultPropertyHeaderComponent(container, recordProperty, editorComponent);
                headerComponent.setVisible(visible);
                propertyHeaderComponents.put(propertyRequirement.getConceptID(), headerComponent);
                propertyEditors.put(propertyRequirement.getConceptID(), editorComponent);

                if ((propertyLayout != null) && (propertyLayout.getHeaderLayoutType() ==  PropertyHeaderLayoutType.TOP))
                {
                    gridBagConstraints = new GridBagConstraints();
                    gridBagConstraints.gridwidth = 2;
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = gridY++;
                    gridBagConstraints.fill = GridBagConstraints.BOTH;
                    gridBagConstraints.weightx = 0.1;
                    gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
                    jPanelContent.add(headerComponent, gridBagConstraints);

                    gridBagConstraints = new GridBagConstraints();
                    gridBagConstraints.gridwidth = 2;
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = gridY++;
                    gridBagConstraints.fill = GridBagConstraints.BOTH;
                    gridBagConstraints.weightx = 0.1;
                    jPanelContent.add(editorComponent, gridBagConstraints);
                }
                else
                {
                    gridBagConstraints = new GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = gridY;
                    gridBagConstraints.fill = GridBagConstraints.BOTH;
                    gridBagConstraints.weightx = 0.0;
                    gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
                    jPanelContent.add(headerComponent, gridBagConstraints);

                    gridBagConstraints = new GridBagConstraints();
                    gridBagConstraints.gridx = 1;
                    gridBagConstraints.gridy = gridY++;
                    gridBagConstraints.fill = GridBagConstraints.BOTH;
                    gridBagConstraints.weightx = 0.1;
                    jPanelContent.add(editorComponent, gridBagConstraints);
                }
            }
        }

        // If no properties are visible, hide the entire section.
        setVisible(propertyVisible);
        jPanelContent.validate();
    }

    public void setSectionHeaderVisible(boolean visible)
    {
        this.setBorder(visible ? sectionHeaderBorder : null);
    }

    @Override
    public void refreshEditor()
    {
        for (DataRecordEditor propertyEditor : propertyEditors.values())
        {
            propertyEditor.refreshEditor();
        }
    }

    @Override
    public void refreshDataDependency()
    {
        for (DataRecordEditor propertyEditor : propertyEditors.values())
        {
            propertyEditor.refreshDataDependency();
        }
    }

    @Override
    public void refreshAccess()
    {
        boolean propertyVisible;

        propertyVisible = false;
        for (T8DefaultPropertyHeaderComponent propertyHeader : propertyHeaderComponents.values())
        {
            PropertyAccessLayer propertyAccessLayer;
            DataRecordEditor propertyEditor;
            boolean visible;

            propertyAccessLayer = accessLayer.getPropertyAccessLayer(propertyHeader.getPropertyRequirement().getConceptID());
            propertyEditor = propertyHeader.getPropertyComponent();
            visible = propertyAccessLayer.isVisible();

            if (visible) propertyVisible = true;
            propertyHeader.setVisible(visible);
            ((Component)propertyEditor).setVisible(visible);
            propertyHeader.refreshAccess();
            propertyEditor.refreshAccess();
        }

        // If none of the properties are visible, hide the section itself as well.
        setVisible(propertyVisible);
    }

    @Override
    public boolean commitChanges()
    {
        // Check the validity of this editor.
        if (recordSection == null) throw new RuntimeException("Commit changes called on section editor without any section value set: " + sectionRequirement);

        // Commit all changes to the property components.
        for (DataRecordEditor propertyEditor : propertyEditors.values())
        {
            if (!propertyEditor.commitChanges()) return false;
        }

        // Return result.
        return true;
    }

    @Override
    public DataRecordEditorContainer getDataRecordEditorContainer()
    {
        return container;
    }

    @Override
    public boolean hasChanges()
    {
        for (DataRecordEditor propertyEditor : propertyEditors.values())
        {
            if (propertyEditor.hasChanges()) return true;
        }

        return false;
    }

    @Override
    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    @Override
    public Value getValue()
    {
        return recordSection;
    }

    @Override
    public SectionRequirement getRequirement()
    {
        return sectionRequirement;
    }

    @Override
    public List<T8DataValidationError> getValidationErrors()
    {
        List<T8DataValidationError> validationErrors;

        validationErrors = new ArrayList<T8DataValidationError>();
        for (DataRecordEditor propertyComponent : propertyEditors.values())
        {
            List<T8DataValidationError> propertyValidationErros;

            propertyValidationErros = propertyComponent.getValidationErrors();
            if (propertyValidationErros != null) validationErrors.addAll(propertyValidationErros);
        }

        return validationErrors;
    }

    @Override
    public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed)
    {
        return super.processKeyBinding(ks, e, condition, pressed);
    }

    public void setPropertyFFTVisible(boolean visible)
    {
        for (DataRecordEditor propertyEditor : propertyEditors.values())
        {
            ((T8DefaultPropertyValueComponent)propertyEditor).setFFTVisible(visible);
        }
    }

    private void updateMouseHover(MouseEvent mouseEvent)
    {
        for (DataRecordEditor propertyEditor : propertyEditors.values())
        {
            if (((T8DefaultPropertyValueComponent)propertyEditor).contains(SwingUtilities.convertMouseEvent(this, mouseEvent, (Component)propertyEditor).getPoint()))
            {
                ((T8DefaultPropertyValueComponent)propertyEditor).setSelected(true);
            }
            else
            {
                ((T8DefaultPropertyValueComponent)propertyEditor).setSelected(false);
            }
        }
    }

    public ArrayList<T8DefaultPropertyHeaderComponent> getPropertyHeaderComponents()
    {
        return new ArrayList<T8DefaultPropertyHeaderComponent>(propertyHeaderComponents.values());
    }

    /**
     * Distributes the list of validation errors to the corresponding property
     * header components.
     *
     * @param validationErrors The validation errors to distribute/set on the
     * applicable property header components.
     */
    @Override
    public void setValidationErrors(List<T8DataValidationError> validationErrors)
    {
        clearValidationErrors();
        if (validationErrors != null)
        {
            // Set the validation errors on the property header components.
            for (T8DefaultPropertyHeaderComponent propertyHeader : propertyHeaderComponents.values())
            {
                List<T8DataValidationError> propertyValidationErrors;

                propertyValidationErrors = new ArrayList<T8DataValidationError>();
                for (T8DataValidationError validationError : validationErrors)
                {
                    PropertyRequirement propertyRequirement;

                    propertyRequirement = propertyHeader.getPropertyRequirement();
                    if (validationError.isApplicableTo(propertyRequirement))
                    {
                        propertyValidationErrors.add(validationError);
                    }
                }

                // Set the validation errors on the property component.
                propertyHeader.getPropertyComponent().setValidationErrors(propertyValidationErrors);
            }
        }


        // Refresh the validity of the header components.
        for (T8DefaultPropertyHeaderComponent propertyHeader : propertyHeaderComponents.values())
        {
            // Update the visual color of the header according to its validity.
            propertyHeader.refreshValidityDisplay();
        }
    }

    public void clearValidationErrors()
    {
        for (T8DefaultPropertyHeaderComponent propertyHeader : propertyHeaderComponents.values())
        {
            propertyHeader.getPropertyComponent().setValidationErrors(null);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));
        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelContent.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseExited(java.awt.event.MouseEvent evt)
            {
                jPanelContentMouseExited(evt);
            }
        });
        jPanelContent.addMouseMotionListener(new java.awt.event.MouseMotionAdapter()
        {
            public void mouseMoved(java.awt.event.MouseEvent evt)
            {
                jPanelContentMouseMoved(evt);
            }
        });
        jPanelContent.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelContent, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jPanelContentMouseMoved(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jPanelContentMouseMoved
    {//GEN-HEADEREND:event_jPanelContentMouseMoved
        updateMouseHover(evt);
    }//GEN-LAST:event_jPanelContentMouseMoved

    private void jPanelContentMouseExited(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jPanelContentMouseExited
    {//GEN-HEADEREND:event_jPanelContentMouseExited
        updateMouseHover(evt);
    }//GEN-LAST:event_jPanelContentMouseExited

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelContent;
    // End of variables declaration//GEN-END:variables
}
