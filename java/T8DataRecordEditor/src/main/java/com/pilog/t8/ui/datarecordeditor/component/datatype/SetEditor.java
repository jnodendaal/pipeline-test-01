package com.pilog.t8.ui.datarecordeditor.component.datatype;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.Constants;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import javax.swing.JButton;

/**
 * @author Bouwer du Preez
 */
public class SetEditor extends DefaultRecordValueEditor
{
    private ArrayList<DataRecordEditor> childEditors;
    private int gridY;

    public SetEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        childEditors = new ArrayList<DataRecordEditor>();
        gridY = 0;
        initComponents();
        refreshEditor();
    }
    
    @Override
    public boolean hasChanges()
    {
        return false;
    }
    
    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();
        
        // Hide the Add button completely if the value is not editableAccess.
        jButtonAdd.setVisible(editableAccess);
        
        // Refresh access on child editors.
        for (DataRecordEditor childEditor : childEditors)
        {
            childEditor.refreshAccess();
        }
    }
    
    @Override
    public final void refreshEditor()
    {
        // Execute default refreshEditor behavior.
        super.refreshEditor();
        
        
        // Refresh child editors.
        for (DataRecordEditor childEditor : childEditors)
        {
            childEditor.refreshEditor();
        }
    }
    
    private void rebuildContent()
    {
        // Clear the UI.
        jPanelContent.removeAll();
        childEditors.clear();
        gridY = 0;

        // Add all of the sub-value editors (document references).
        for (RecordValue subValue : recordValue.getSubValues())
        {
            addValueEditor(subValue);
        }

        // Validate the container.
        jPanelContent.validate();
    }

    @Override
    public boolean commitChanges()
    {
        boolean success;
        
        success = true;
        for (DataRecordEditor fieldComponent : childEditors)
        {
            if (!fieldComponent.commitChanges()) success = false;
        }
        
        return success;
    }
    
    private void addNewValue()
    {
        if (valueRequirement != null)
        {
            RecordValue newValue;
            
            newValue = RecordValue.createValue(valueRequirement.getFirstSubRequirement());
            addValueEditor(newValue);
        }
    }

    private void addValueEditor(RecordValue subValue)
    {
        DataRecordEditor recordValueEditor;
        
        recordValueEditor = container.getEditorComponentFactory().createDataRecordEditorComponent(subValue);
        if (recordValueEditor != null)
        {
            JButton removeButton;
            GridBagConstraints gridBagConstraints;
        
            childEditors.add(recordValueEditor);
            childEditors.add(recordValueEditor);
            removeButton = buildRemoveButton(recordValueEditor);
            
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new Insets(5, 5, 5, 5);
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = gridY;
            gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
            gridBagConstraints.weightx = 0.0;
            gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
            jPanelContent.add(removeButton, gridBagConstraints);

            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 1;
            gridBagConstraints.gridy = gridY++;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 0.1;
            jPanelContent.add((Component)recordValueEditor, gridBagConstraints);

            jPanelContent.invalidate();
            jPanelContent.revalidate();
        }
    }

    private void removeValue(JButton removeButton, DataRecordEditor recordValueEditor)
    {
        // Remove the value from the underlying Data Record document.
        recordValue.removeSubValue(recordValueEditor.getValue());
        
        // Rebuild the UI of this editor.
        rebuildContent();
    }

    private JButton buildRemoveButton(final DataRecordEditor valueEditor)
    {
        final JButton jButtonRemove;

        jButtonRemove = new javax.swing.JButton();
        jButtonRemove.setEnabled(editableAccess);
        jButtonRemove.setText(null);
        jButtonRemove.setIcon(Constants.CUT_ICON);
        jButtonRemove.setToolTipText(translate("Remove Value"));
        jButtonRemove.setHorizontalAlignment(JButton.TRAILING);
        jButtonRemove.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonRemove.setMaximumSize(new java.awt.Dimension(23, 23));
        jButtonRemove.setMinimumSize(new java.awt.Dimension(23, 23));
        jButtonRemove.setPreferredSize(new java.awt.Dimension(23, 23));
        jButtonRemove.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                removeValue(jButtonRemove, valueEditor);
            }
        });

        return jButtonRemove;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelControls = new javax.swing.JPanel();
        jButtonAdd = new javax.swing.JButton();
        jPanelContent = new javax.swing.JPanel();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelControls.setOpaque(false);
        jPanelControls.setLayout(new java.awt.GridBagLayout());

        jButtonAdd.setText(translate("Add"));
        jButtonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        jPanelControls.add(jButtonAdd, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jPanelControls, gridBagConstraints);

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelContent, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        addNewValue();
    }//GEN-LAST:event_jButtonAddActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelControls;
    // End of variables declaration//GEN-END:variables
}
