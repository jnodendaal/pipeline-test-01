package com.pilog.t8.ui.datarecordeditor;

import static com.pilog.t8.definition.ui.datarecordeditor.T8DataRecordEditorAPIHandler.*;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.definition.ui.datarecordeditor.T8DataRecordEditorDefinition.AccessValidationLevel;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordEditorOperationHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordEditorOperationHandler.class);

    private final T8DataRecordEditor recordEditor;

    public T8DataRecordEditorOperationHandler(T8DataRecordEditor recordEditor)
    {
        this.recordEditor = recordEditor;
    }

    public Map<String, Object> executeOperation(String operationIdentifier, final Map<String, Object> operationParameters)
    {
        try
        {
            if (operationIdentifier.equals(OPERATION_GET_ROOT_DATA_RECORD))
            {
                return HashMaps.createSingular(PARAMETER_DATA_RECORD, recordEditor.getRootDataRecord());
            }
            else if (operationIdentifier.equals(OPERATION_HAS_UNSAVED_CHANGES))
            {
                return HashMaps.createSingular(PARAMETER_HAS_UNSAVED_CHANGES, recordEditor.hasUnsavedChanges());
            }
            else if (operationIdentifier.equals(OPERATION_GET_RECORD_ID))
            {
                return HashMaps.createSingular(PARAMETER_RECORD_ID, recordEditor.getRecordID());
            }
            else if (operationIdentifier.equals(OPERATION_SAVE_RECORD_CHANGES))
            {
                AccessValidationLevel accessValidationLevel;
                boolean suppressToast;

                // Check whether or not the toast will be suppressed
                suppressToast = Boolean.TRUE.equals(operationParameters.get(PARAMETER_SUPPRESS_TOAST));

                // Get the access validation level.
                try
                {
                    String accessValidationLevelParameter;

                    accessValidationLevelParameter = (String)operationParameters.get(PARAMETER_ACCESS_VALIDATION_LEVEL);
                    accessValidationLevel = accessValidationLevelParameter != null ? AccessValidationLevel.valueOf(accessValidationLevelParameter) : AccessValidationLevel.FINAL;
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while parsing operation parameters. Paramters : " + operationParameters, e);
                    accessValidationLevel = AccessValidationLevel.FINAL;
                }

                // Execute the operation and return the result.
                return HashMaps.createSingular(PARAMETER_SUCCESS, recordEditor.saveSessionDataRecords(accessValidationLevel, suppressToast));
            }
            else if (operationIdentifier.equals(OPERATION_CREATE_NEW_RECORD))
            {
                DataRecord newRecord;

                // Make sure to clear the editor on the EDT.
                SwingUtilities.invokeAndWait(() ->
                {
                    recordEditor.clear();
                });

                // Now create the new record.
                newRecord = recordEditor.createNewDocument(null, null, null, (String)operationParameters.get(PARAMETER_RECORD_ID), (String)operationParameters.get(PARAMETER_DR_INSTANCE_ID), true);
                return HashMaps.createSingular(PARAMETER_DATA_RECORD, newRecord);
            }
            else if (operationIdentifier.equals(OPERATION_LOAD_RECORD))
            {
                DataRecord loadedRecord;
                String operationId;
                String accessId;

                // Make sure to clear the editor on the EDT.
                SwingUtilities.invokeAndWait(() -> recordEditor.clear());

                // Get the operation parameters required.
                operationId = (String)operationParameters.get(PARAMETER_OPEN_RECORD_ID);
                accessId = (String)operationParameters.get(PARAMETER_ACCESS_IDENTIFIER);

                // If an access identifier is specified, set the new access on the editor.
                if (accessId != null) recordEditor.setAccess(accessId);

                // Load the specified record.
                loadedRecord = recordEditor.loadDocument((String)operationParameters.get(PARAMETER_RECORD_ID), true);

                // Show the specified record.
                if (operationId != null)
                {
                    SwingUtilities.invokeAndWait(() -> recordEditor.showDocument(operationId));
                }

                // No result to return.
                return HashMaps.createSingular(PARAMETER_DATA_RECORD, loadedRecord);
            }
            else if (operationIdentifier.equals(OPERATION_ADD_RECORD))
            {
                DataRecord newRecord;

                // Get the record to add.
                newRecord = (DataRecord)operationParameters.get(PARAMETER_DATA_RECORD);

                // Make sure to clear the editor on the EDT.
                SwingUtilities.invokeAndWait(() ->
                {
                    // Add the new document to the UI.
                    recordEditor.addDocument(newRecord);

                    // Add any potential descendants of the newly created record.
                    for (DataRecord descendant : newRecord.getDescendantRecords())
                    {
                        recordEditor.addDocument(descendant);
                    }

                    // Refresh the editor to reflect the model changes.
                    recordEditor.refreshEditor();

                    // Show the added record.
                    recordEditor.showDocument(newRecord.getID());
                });

                // No result to return.
                return null;
            }
            else if (operationIdentifier.equals(OPERATION_CLEAR))
            {
                // Make sure to clear the editor on the EDT.
                SwingUtilities.invokeAndWait(() ->
                {
                    recordEditor.clear();
                });

                // No results to return.
                return null;
            }
            else throw new RuntimeException("Invalid Operation Identifier: '" + operationIdentifier + "'.");
        }
        catch (Exception e)
        {
            throw new RuntimeException("Operation Exception: '" + operationIdentifier + "'.", e);
        }
    }
}
