package com.pilog.t8.ui.datarecordeditor;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class RecordValueCellRenderer implements ListCellRenderer
{
    private final DefaultListCellRenderer defaultRenderer;
    private final T8DataRecordDefinitionGenerator stringGenerator;

    private static final Icon APPROVED_VALUE_ICON = new ImageIcon(RecordValueCellRenderer.class.getResource("/com/pilog/t8/ui/datarecordeditor/icons/smallTickIcon.png"));

    public RecordValueCellRenderer(TerminologyProvider terminologyProvider)
    {
        this.defaultRenderer = new DefaultListCellRenderer();
        this.stringGenerator = new T8DataRecordDefinitionGenerator(terminologyProvider);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        RecordValue recordValue;
        String displayValue;
        boolean approvedValue;
        JLabel label;

        recordValue = (RecordValue)value;
        if (recordValue != null)
        {
            StringBuilder generatedString;

            approvedValue = recordValue.isApproved();
            generatedString = stringGenerator.generateValueString(recordValue);
            displayValue = generatedString != null ? generatedString.toString() : null;
        }
        else
        {
            displayValue = null;
            approvedValue = false;
        }

        // Use the default renderer to return a renderer using the generated display string.
        label = (JLabel) defaultRenderer.getListCellRendererComponent(list, displayValue, index, isSelected, cellHasFocus);
        label.setIcon(approvedValue ? APPROVED_VALUE_ICON : null);
        return label;
    }
}
