package com.pilog.t8.ui.datarecordviewer.model;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordContent;

/**
 * @author Bouwer du Preez
 */
public class T8DRInstanceComparisonType extends T8AbstractComparisonType
{
    private final String drInstanceId;

    public T8DRInstanceComparisonType(String drInstanceId, TerminologyProvider terminologyProvider)
    {
        super(terminologyProvider);
        this.drInstanceId = drInstanceId;
    }

    @Override
    public String getGroupDisplayName()
    {
        return terminologyProvider.getTerm(null, drInstanceId);
    }

    @Override
    public boolean matchesRecord(RecordContent record)
    {
        return record.getDrInstanceId().equals(drInstanceId);
    }

    @Override
    public String toString()
    {
        return "T8DRInstanceComparisonType{" + "drInstanceID=" + drInstanceId + '}';
    }
}
