package com.pilog.t8.ui.datarecordeditor.view.navigation;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.ui.datarecordeditor.T8DataRecordEditor;
import com.pilog.t8.ui.datarecordeditor.view.navigation.NavigationTreeNode.NavigationNodeType;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.components.button.RoundButtonUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Iterator;
import org.jdesktop.swingx.JXButton;

/**
 * @author Bouwer du Preez
 */
public class BreadCrumbView extends javax.swing.JPanel
{
    private final BreadCrumbActionListener breadCrumbActionListener;
    private final T8DataRecordEditor parentEditor;
    private NavigationTree navigationTree;

    public BreadCrumbView(T8DataRecordEditor parentEditor)
    {
        initComponents();
        this.parentEditor = parentEditor;
        this.breadCrumbActionListener = new BreadCrumbActionListener();
        this.setBackground(Color.white);
        showBreadCrumbView();
    }

    public void setNavigationTree(NavigationTree navigationTree)
    {
        this.navigationTree = navigationTree;
    }

    public final void showBreadCrumbView()
    {
        removeAll();
        add(jPanelBreadcrumb, BorderLayout.CENTER);
        revalidate();
        repaint();
    }

    public void updateBreadCrumbs()
    {
        jPanelBreadcrumb.removeAll();

        if (navigationTree.getSelectionPath() != null)
        {
            Object[] path;
            TerminologyProvider terminologyProvider;

            path = navigationTree.getSelectionPath().getPath();
            terminologyProvider = parentEditor.getTerminologyProvider();

            for (Iterator it = Arrays.asList(path).iterator(); it.hasNext();)
            {
                Object pathObject;
                NavigationTreeNode treeNode;

                pathObject = it.next();
                treeNode = (NavigationTreeNode)pathObject;
                if (treeNode.getNodeType() == NavigationNodeType.RECORD)
                {
                    DataRecord dataRecord;
                    String drInstanceTerm;
                    String drInstanceCode;
                    String drInstanceDefinition;
                    String drInstanceID;
                    String recordID;
                    JXButton breadCrumb;
                    RoundButtonUI buttonUI;

                    recordID = treeNode.getRecordID();
                    dataRecord = treeNode.getRecord();
                    drInstanceID = dataRecord.getDataRequirementInstanceID();
                    drInstanceTerm = terminologyProvider.getTerm(null, drInstanceID);
                    drInstanceCode = terminologyProvider.getCode(drInstanceID);
                    drInstanceDefinition = terminologyProvider.getDefinition(null, drInstanceID);
                    breadCrumb = new JXButton();
                    buttonUI = new RoundButtonUI(1, 6f, 6f);

                    // If the DR Instance Term was not found, use the DR Term.
                    if (drInstanceTerm == null)
                    {
                        drInstanceTerm = terminologyProvider.getTerm(null, dataRecord.getDataRequirementID());
                    }

                    // Determine the background color of the document.
                    if (!parentEditor.isContentValid(dataRecord.getID()))
                    {
                        breadCrumb.setBackground(LAFConstants.INVALID_PINK);
                        buttonUI.setArmedColour(LAFConstants.INVALID_PINK.darker());
                        buttonUI.setRolloverColour(LAFConstants.INVALID_PINK.brighter());
                    }
                    else
                    {
                        breadCrumb.setBackground(LAFConstants.PROPERTY_BLUE);
                        buttonUI.setArmedColour(LAFConstants.PROPERTY_BLUE.darker());
                        buttonUI.setRolloverColour(LAFConstants.PROPERTY_BLUE.brighter());
                    }
                    if(!it.hasNext())
                    {
                        breadCrumb.setBackground(LAFConstants.SELECTION_ORANGE);
                        buttonUI.setArmedColour(LAFConstants.SELECTION_ORANGE.darker());
                        buttonUI.setRolloverColour(LAFConstants.SELECTION_ORANGE.brighter());
                    }

                    // Set the bread crumb text.
                    if ((drInstanceCode != null) && (drInstanceCode.length() < 10))
                    {
                        breadCrumb.setText(drInstanceTerm + " (" + drInstanceCode + ")");
                    }
                    else
                    {
                        breadCrumb.setText(drInstanceTerm);
                    }

                    // Set the bread crumb tooltip.
                    breadCrumb.setToolTipText(createBreadCrumbToolTipText(drInstanceCode, drInstanceDefinition));

                    breadCrumb.setActionCommand(recordID);
                    breadCrumb.addActionListener(breadCrumbActionListener);
                    breadCrumb.setUI(buttonUI);

                    jPanelBreadcrumb.add(breadCrumb);
                }
            }

            revalidate();
        }
    }

    private String createBreadCrumbToolTipText(String code, String definition)
    {
        StringBuffer text;

        // Create a new buffer.
        text = new StringBuffer();
        text.append("<html>");

        // Append the code.
        text.append("<h2>Code</h2>");
        text.append("<p width=\"500\">");
        text.append(code != null ? code : "Not Available");
        text.append("</p>");

        // Append the definition.
        text.append("<h2>Definition</h2>");
        text.append("<p width=\"500\">");
        text.append(definition != null ? definition : "Not Available");
        text.append("</p>");

        text.append("</html>");
        return text.toString();
    }

    private class BreadCrumbActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            navigationTree.setSelectedNode(e.getActionCommand());
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jPanelBreadcrumb = new javax.swing.JPanel();

        jPanelBreadcrumb.setName(""); // NOI18N
        jPanelBreadcrumb.setOpaque(false);
        com.pilog.t8.utilities.components.layout.WrapLayout wrapLayout1 = new com.pilog.t8.utilities.components.layout.WrapLayout();
        wrapLayout1.setAlignment(java.awt.FlowLayout.LEFT);
        wrapLayout1.setHgap(2);
        wrapLayout1.setVgap(2);
        jPanelBreadcrumb.setLayout(wrapLayout1);

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelBreadcrumb;
    // End of variables declaration//GEN-END:variables
}
