package com.pilog.t8.ui.datarecordeditor;

import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.structure.DataRecordStructure;
import com.pilog.t8.data.document.datarecord.structure.DataRecordStructureDFIterator;
import com.pilog.t8.data.document.datarecord.structure.DataRecordStructureNode;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datafile.T8DataFileSaveResult;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.data.org.T8ClientDataRecordProvider;
import com.pilog.t8.data.org.T8ClientTerminologyProvider;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.datarecordeditor.T8DataRecordEditorAPIHandler;
import com.pilog.t8.definition.ui.datarecordeditor.T8DataRecordEditorDefinition;
import com.pilog.t8.definition.ui.datarecordeditor.T8DataRecordEditorDefinition.AccessValidationLevel;
import com.pilog.t8.definition.ui.module.T8ModuleDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecord.DataRecordEditorFactory;
import com.pilog.t8.ui.datarecordeditor.view.content.ContentView;
import com.pilog.t8.ui.datarecordeditor.view.content.RecordEditorFactory;
import com.pilog.t8.ui.datarecordeditor.view.navigation.BreadCrumbView;
import com.pilog.t8.ui.datarecordeditor.view.navigation.NavigationModel;
import com.pilog.t8.ui.datarecordeditor.view.navigation.NavigationTreeView;
import com.pilog.t8.ui.optionpane.T8OptionPane;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.jdesktop.swingx.MultiSplitLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import static com.pilog.t8.ui.datarecord.DataRecordEditorContainer.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordEditor extends JPanel implements T8Component, DataRecordEditorContainer
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordEditor.class);
    private static final String SPLIT_LAYOUT = "(ROW (LEAF name=left weight=0.4) (LEAF name=right weight=0.6))";
    private final T8Context context;
    private final T8Context accessContext;
    private final T8DataRecordEditorDefinition definition;
    private final T8ComponentController controller;
    private final T8ConfigurationManager configurationManager;
    private final T8DataRecordClientApi recApi;
    private final T8TerminologyClientApi trmApi;
    private final T8OntologyClientApi ontApi;
    private final DataRecordProvider dataRecordProvider;
    private final T8LRUCachedTerminologyProvider terminologyProvider;
    private final T8DefaultComponentContainer container;
    private final T8DataRecordEditorOperationHandler operationHandler;
    private final DataRecordEditorFactory componentFactory;
    private final RecordManager recordContainer;
    private final NavigationTreeView navigationView;
    private final BreadCrumbView breadCrumbView;
    private final NavigationModel navigationModel;
    private final String saveAccessId;
    private String finalAccessId;
    private T8DataFileValidationReport validationReport;
    private String currentValidationErrorRecordID; // Stores record ID of the last record that was navigated to using the 'Next Error' button.

    public static final Icon APPROVED_VALUE_ICON = new ImageIcon(RecordValueCellRenderer.class.getResource("/com/pilog/t8/ui/datarecordeditor/icons/smallTickIcon.png"));

    public T8DataRecordEditor(T8DataRecordEditorDefinition definition, T8ComponentController controller)
    {
        // Get the configuration manager and session context before initializing the components to translate UI correctly.
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.context = controller.getContext();
        this.accessContext = controller.getContext();
        this.definition = definition;
        this.saveAccessId = definition.getCommitDataRecordAccessIdentifier();
        this.finalAccessId = definition.getDataRecordAccessIdentifier();
        initComponents();
        this.container = new T8DefaultComponentContainer();
        this.add(container, java.awt.BorderLayout.CENTER);
        this.container.setComponent(jPanelMain);
        this.controller = controller;
        this.recApi = controller.getApi(T8DataRecordClientApi.API_IDENTIFIER);
        this.trmApi = controller.getApi(T8TerminologyClientApi.API_IDENTIFIER);
        this.ontApi = controller.getApi(T8OntologyClientApi.API_IDENTIFIER);
        this.dataRecordProvider = new T8ClientDataRecordProvider(recApi);
        this.terminologyProvider = new T8LRUCachedTerminologyProvider(new T8ClientTerminologyProvider(trmApi), 3000);
        this.terminologyProvider.setLanguage(context.getSessionContext().getContentLanguageIdentifier());
        this.terminologyProvider.setDebugLogCacheMisses(true);
        this.componentFactory = new RecordEditorFactory(this);
        this.navigationModel = new NavigationModel();
        this.recordContainer = new RecordManager(this, navigationModel);
        this.operationHandler = new T8DataRecordEditorOperationHandler(this);
        this.breadCrumbView = new BreadCrumbView(this);
        this.navigationView = new NavigationTreeView(this);
        this.navigationView.setNavigationModel(navigationModel);
        this.navigationView.setBreadCrumbView(breadCrumbView);
        this.jPanelBreadCrumbs.add(breadCrumbView, java.awt.BorderLayout.CENTER);
        this.jPanelNavigation.add(navigationView, java.awt.BorderLayout.CENTER);
        this.jPanelNavigation.setBorder(new T8ContentHeaderBorder(getTranslatedString("Navigation View")));
        this.jPanelContent.setBorder(new T8ContentHeaderBorder(getTranslatedString("Content View")));
        this.validationReport = new T8DataFileValidationReport();
        this.recordContainer.setAccessHandler(definition.getDataRecordAccessDefinition());
        this.jXMultiSplitPaneContent.setModel(MultiSplitLayout.parseModel(SPLIT_LAYOUT));
        ((MultiSplitLayout)this.jXMultiSplitPaneContent.getLayout()).setLayoutMode(MultiSplitLayout.NO_MIN_SIZE_LAYOUT);
        ((MultiSplitLayout)this.jXMultiSplitPaneContent.getLayout()).setLayoutByWeight(true);
        jXMultiSplitPaneContent.add(jPanelNavigation, "left");
        jXMultiSplitPaneContent.add(jPanelContent, "right");
        jXMultiSplitPaneContent.setOpaque(false);
        configureComponents();
        addKeyBindings();
    }

    private void configureComponents()
    {
        SwingUtilities.invokeLater(() ->
        {
            jButtonSaveDocument.setVisible(definition.isCommitEnabled());
            jSeparatorSave.setVisible(definition.isCommitEnabled());

            jButtonExportDocument.setVisible(definition.isExportEnabled());
            jSeparatorExport.setVisible(definition.isExportEnabled());

            jButtonGotoNextError.setVisible(false);

            jButtonShowHistory.setVisible(definition.getDataRecordHistoryModuleIdentifier() != null);
            jSeparatorHistory.setVisible(definition.getDataRecordHistoryModuleIdentifier()!= null);

            jButtonRefresh.setVisible(!definition.isToolbarRefreshHidden());
            jSeparatorRefresh.setVisible(!definition.isToolbarRefreshHidden());

            jToggleButtonPropertyFFT.setVisible(!definition.isToolbarPropertyFFTHidden());
            jSeparatorPropFFT.setVisible(!definition.isToolbarPropertyFFTHidden());

            jToggleButtonTreeNavigation.setVisible(!definition.isToolbarTreeNavigationHidden());
            jSeparatorTreeNavigation.setVisible(!definition.isToolbarTreeNavigationHidden());

            jToolBarMainControls.setVisible(!definition.isToolbarHidden());
        });
    }

    private void addKeyBindings()
    {
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_F12, InputEvent.CTRL_DOWN_MASK), "showDocPathConsole");
        getActionMap().put("showDocPathConsole", new AbstractAction("showDocPathConsole")
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                DataRecord dataFile;

                dataFile = getRootDataRecord();
                if (dataFile != null)
                {
                    dataFile = dataFile.copy(true, true, true, true, true, true);
                    dataFile.normalize(true);
                    new AdminConsole(T8DataRecordEditor.this, recordContainer).setVisible(true);
                }
                else
                {
                    Toast.show("No root record found in editor.", Toast.Style.ERROR);
                }
            }
        });
    }

    private String getTranslatedString(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    @Override
    public void unlockUI()
    {
        container.unlock();
    }

    @Override
    public void lockUI(final String message)
    {
        container.setMessage(message);
        container.lock();
    }

    /**
     * Removes all loaded documents and clears all cached data.
     */
    public void clear()
    {
        removeAllDocuments();
    }

    public void refreshNavigationView(String recordID)
    {
        navigationModel.nodeChanged(navigationModel.findNode(recordID));
    }

    public void refreshNavigationView(Collection<String> recordIDSet)
    {
        for (String recordID : recordIDSet)
        {
            refreshNavigationView(recordID);
        }
    }

    public TerminologyGenerator getTerminologyGenerator()
    {
        return recordContainer.getTerminologyGenerator();
    }

    /**
     * Returns boolean true if any of the records currently loaded into the
     * editor has changes that have not been saved yet.
     *
     * @return boolean true if any of the records currently loaded into the
     * editor has changes that have not been saved yet.
     */
    public boolean hasUnsavedChanges()
    {
        return recordContainer.hasUnsavedChanges();
    }

    /**
     * Creates a new document container component from the supplied data record
     * and then adds the document container to the UI.  The data record document
     * is also added to the collection of loaded documents and the navigation
     * view is updated to reflect the newly added document.  If there are any
     * validation errors available for the new document they are also added to
     * the component.
     *
     * This method also adds the supplied document to the sub-record collections
     * of all applicable parent records.  If no parent records are found and a
     * root record has already been added to the UI, this method will throw an
     * exception.
     *
     * @param dataRecord The document to add to the UI.
     */
    @Override
    public void addDocument(final DataRecord dataRecord)
    {
        // Check that this method is called from the EDT.
        if (!SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Method must be called from EDT.");

        // Add the new record to the container.
        recordContainer.addRecord(dataRecord);
    }

    /**
     * This method removes the specified data record from the UI and also from
     * its parents.
     * @param recordID The id of the data record to be removed.
     */
    @Override
    public void removeDocument(String recordID)
    {
        recordContainer.removeRecord(recordID);
    }

    public void removeAllDocuments()
    {
        if (!SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Must be called from EDT.");
        recordContainer.removeAllDocuments();
    }

    @Override
    public void showDocument(String recordID)
    {
        recordContainer.setCurrentEditor(recordID);
    }

    public void setEditor(ContentView editor)
    {
        if (editor != null)
        {
            jPanelContentEditor.removeAll();
            jPanelContentEditor.add(editor);
            jPanelContentEditor.revalidate();
            navigationView.setSelectedNode(editor.getRecordID());
            repaint();
            editor.requestFocus();
        }
        else
        {
            jPanelContentEditor.removeAll();
            jPanelContentEditor.revalidate();
            navigationView.setSelectedNode(null);
            repaint();
        }
    }

    public DataRecord getDataRecord(String recordID)
    {
        return recordContainer.getDataRecord(recordID);
    }

    public DataRecord getRootDataRecord()
    {
        return recordContainer.getRootDataRecord();
    }

    @Override
    public DataRecord getDocument(String recordID)
    {
        return recordContainer.getDataRecord(recordID);
    }

    @Override
    public boolean containsDocument(String recordID)
    {
        return recordContainer.containsDataRecord(recordID);
    }

    public String getRecordID()
    {
        return navigationModel.getRootRecordID();
    }

    @Override
    public DataRecordEditorFactory getEditorComponentFactory()
    {
        return componentFactory;
    }

    @Override
    public T8DataRecordAccessHandler getAccessHandler()
    {
        return recordContainer.getAccessHandler();
    }

    @Override
    public DataRecordProvider getDataRecordProvider()
    {
        return dataRecordProvider;
    }

    @Override
    public CachedTerminologyProvider getTerminologyProvider()
    {
        return terminologyProvider;
    }

    @Override
    public OntologyProvider getOntologyProvider()
    {
        return ontApi.getOntologyProvider();
    }

    public void setToolBarVisible(boolean visible)
    {
        jToolBarMainControls.setVisible(visible);
    }

    protected Container getContentPane()
    {
        return jPanelContentEditor;
    }

    protected void setValidationReport(T8DataFileValidationReport newReport)
    {
        // Set the local instance collection.
        validationReport = newReport != null ? newReport : new T8DataFileValidationReport();
        currentValidationErrorRecordID = null;

        // Show the next error buttons if we have any validation errors.
        if (validationReport.hasErrors())
        {
            jButtonGotoNextError.setVisible(true);
        }
        else
        {
            jButtonGotoNextError.setVisible(false);
        }

        // Set the validation errors on all the applicable document components.
        recordContainer.setValidationReport(validationReport);

        // Show the next error.
        SwingUtilities.invokeLater(this::navigateToNextValidationError);
    }

    private void navigateToNextValidationError()
    {
        if (validationReport.hasErrors())
        {
            Iterator<String> recordIDIterator;
            String firstID;

            // Get an iterator over the record ID's in the error collection.
            firstID = null;
            recordIDIterator = validationReport.getRecordIdList().iterator();
            while (recordIDIterator.hasNext())
            {
                String nextRecordID;

                // Get the next record ID and make sure there are errors for the specified record.
                nextRecordID = recordIDIterator.next();
                if (validationReport.hasNonReferenceErrors(nextRecordID))
                {
                    // Set the first ID if it has not been set.
                    if (firstID == null) firstID = nextRecordID;

                    // If we have not yet navigated to a record, navigate to the first.
                    if (currentValidationErrorRecordID == null)
                    {
                        currentValidationErrorRecordID = firstID;
                        showDocument(currentValidationErrorRecordID);
                        return;
                    }
                    else
                    {
                        // If we have found the currently navigated to record, we now need to find the next in line.
                        if (nextRecordID.equals(currentValidationErrorRecordID))
                        {
                            // Keep iterating over the record ID's to find the next record to navigate to.
                            while (recordIDIterator.hasNext())
                            {
                                nextRecordID = recordIDIterator.next();
                                if (validationReport.hasNonReferenceErrors(nextRecordID))
                                {
                                    currentValidationErrorRecordID = nextRecordID;
                                    showDocument(currentValidationErrorRecordID);
                                    return;
                                }
                            }

                            // We could not find a next-in-line record so we ask the user if they want to restart at the first.
                            if (shouldRestartErrorLoop())
                            {
                                currentValidationErrorRecordID = firstID;
                                showDocument(currentValidationErrorRecordID);
                                return;
                            } else return;
                        }
                    }
                }
            }
        }
    }

    private boolean shouldRestartErrorLoop()
    {
        if (this.validationReport.getNonReferenceErrorRecordCount() <= 1)
        {
            JOptionPane.showMessageDialog(this, getTranslatedString("Only errors found are currently being displayed."), getTranslatedString("Review Complete"), JOptionPane.INFORMATION_MESSAGE);
            return false;
        }

        return (JOptionPane.showConfirmDialog(this, getTranslatedString("All errors have been shown. Do you want to restart from the first?"), getTranslatedString("Restart Review"), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);
    }

    /**
     * Creates a new record and adds it to the UI.  This method can only be
     * called from a non-EDT thread.
     * @param parentRecordId The ID of the parent record to which the newly
     * created record will be added.
     * @param propertyId The ID of the property in the parent record to which
     * the newly created record will be linked.
     * @param fieldId The ID of the field in the parent record to which
     * the newly created record will be linked.
     * @param recordId The ID to assign to the newly created record.
     * @param drInstanceId The ID of the DR Instance to use for creation of the
     * new record.
     * @param refresh A boolean flag indicating whether or not the container
     * must be refreshed after creation and addition of the new record.
     * @return The newly created record (after it has been added to the UI).
     * @throws Exception
     */
    @Override
    public DataRecord createNewDocument(String parentRecordId, String propertyId, String fieldId, String recordId, String drInstanceId, final boolean refresh) throws Exception
    {
        try
        {
            DocumentCreator documentCreator;
            final DataRecord newRecord;

            // Set the access context for the operation.
            accessContext.setDataAccessId(finalAccessId);

            // Create and run the creator Thread.
            lockUI(getTranslatedString("Creating New Document").concat("..."));
            documentCreator = new DocumentCreator(getRootDataRecord(), parentRecordId, propertyId, fieldId, drInstanceId);
            documentCreator.execute();

            // Wait for completion of the worker before continuing.
            newRecord = documentCreator.get();

            // Assign the specified record id (if any).
            if (recordId != null) newRecord.setID(recordId);

            // Add the record to the UI.
            SwingUtilities.invokeAndWait(() ->
            {
                try
                {
                    // Add the new document to the UI.
                    addDocument(newRecord);

                    // Add any potential descendants of the newly created record.
                    for (DataRecord descendant : newRecord.getDescendantRecords())
                    {
                        addDocument(descendant);
                    }

                    // Refresh the editor to reflect the model changes.
                    if (refresh) refreshEditor();

                    // Show the created record.
                    showDocument(newRecord.getID());
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while adding newly created document to Record Editor UI.", e);
                }
            });

            // Return the newly created record.
            return newRecord;
        }
        catch (InterruptedException | ExecutionException | InvocationTargetException e)
        {
            LOGGER.log("Exception while creating new document using Record ID '" + recordId + "' and DR Instance ID '" + drInstanceId + "'.", e);
            T8OptionPane.showMessageDialog(this, getTranslatedString("The specified document could not be successfully created."), getTranslatedString("Status"), T8OptionPane.ERROR_MESSAGE, controller);
            throw e;
        }
        finally
        {
            // Unlock the UI.
            unlockUI();
        }
    }

    @Override
    public DataRecord changeDocumentDRInstance(String recordID, String drInstanceID)
    {
        try
        {
            DRInstanceChanger drInstanceChanger;
            DataRecord changedRecord;

            // Create and run the loader Thread.
            lockUI(getTranslatedString("Applying Document Changes").concat("..."));
            drInstanceChanger = new DRInstanceChanger(recordID, drInstanceID, recordContainer.getAccessIdentifier());
            drInstanceChanger.execute();

            // Wait for completion of the worker before continuing.
            changedRecord = drInstanceChanger.get();
            Toast.show(getTranslatedString("Changes Applied Successfully"), Toast.Style.SUCCESS);
            return changedRecord;
        }
        catch (InterruptedException | ExecutionException e)
        {
            LOGGER.log("Exception while starting DR Instance Change thread.", e);
            T8OptionPane.showMessageDialog(this, getTranslatedString("The specified document change could not be applied successfully."), getTranslatedString("Status"), T8OptionPane.ERROR_MESSAGE, controller);
            return null;
        }
    }

    @Override
    public DataRecord loadDocument(final String recordId, final boolean refresh)
    {
        try
        {
            DocumentLoader documentLoader;
            final DataRecord retrievedRecord;
            long startTime;

            // Make sure this method is not called from the EDT.
            if (SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Cannot invoke this method from EDT.");

            // Set the access context for the operation.
            accessContext.setDataAccessId(finalAccessId);

            // Create and run the loader Thread.
            startTime = System.currentTimeMillis();
            lockUI(getTranslatedString("Loading Document").concat("..."));
            documentLoader = new DocumentLoader(recordId);
            documentLoader.execute();

            // Wait for completion of the worker before continuing.
            retrievedRecord = documentLoader.get();

            // Add the record to the UI.
            SwingUtilities.invokeAndWait(() ->
            {
                try
                {
                    long uiConstructionStartTime;

                    uiConstructionStartTime = System.currentTimeMillis();
                    if (retrievedRecord != null)
                    {
                        List<DataRecord> recordsToAdd;

                        // Add all of the retrieved data records to the UI.
                        recordsToAdd = new ArrayList<>();
                        recordsToAdd.add(retrievedRecord);
                        recordsToAdd.addAll(retrievedRecord.getDescendantRecords());
                        for (DataRecord recordToAdd : recordsToAdd)
                        {
                            // Add the terminology retrieved as part of the Data Record to the terminology provider.
                            terminologyProvider.addTerminology(recordToAdd.getContentTerminology(false));

                            // Add the document to the UI.
                            addDocument(recordToAdd);
                        }

                        // Refresh the editor to reflect the model changes.
                        if (refresh) refreshEditor();

                        // Show the requested record.
                        showDocument(recordId);
                    }

                    T8Log.log((System.currentTimeMillis() - uiConstructionStartTime) + "ms:UI Construction time: " + retrievedRecord);
                    T8Log.log((System.currentTimeMillis() - startTime) + "ms:Total load time: " + retrievedRecord);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while retrieving data record: " + recordId, e);
                }
            });

            // Return the retrieved record.
            return retrievedRecord;
        }
        catch (RuntimeException | InterruptedException | ExecutionException | InvocationTargetException e)
        {
            LOGGER.log("Exception while loading Record.", e);
            T8OptionPane.showMessageDialog(this, getTranslatedString("The specified document could not be loaded."), getTranslatedString("Status"), T8OptionPane.ERROR_MESSAGE, controller);
            return null;
        }
        finally
        {
            // Unlock the UI.
            unlockUI();
        }
    }

    @Override
    public void commitChanges()
    {
        recordContainer.commitChanges();
    }

    public void setAccess(String accessId) throws Exception
    {
        // Set the access identifier to use for local reference.
        finalAccessId =  accessId;

        // Set the new access handler to use for local validation of editor content.
        recordContainer.setAccessHandler(finalAccessId);

        // Set the access context to use for any server requests.
        accessContext.setDataAccessId(finalAccessId);
    }

    public boolean saveSessionDataRecords(AccessValidationLevel validationLevel, boolean suppressToast)
    {
        DataRecord dataFile;

        // Commit all records.
        recordContainer.commitChanges();

        // Get the root data record.
        dataFile = getRootDataRecord();

        // Set the access identifier to use for this specific server request.
        if (validationLevel == AccessValidationLevel.SAVE)
        {
            accessContext.setDataAccessId(saveAccessId);
        }
        else
        {
            accessContext.setDataAccessId(finalAccessId); // Set the default access.
        }

        try
        {
            HashMap<String, Object> eventParameters;
            T8DataFileSaveResult result;
            DataFileSaver documentSaver;

            // Create and run the saver Thread.
            LOGGER.log("Saving Record Changes using Access: " + accessContext.getDataAccessIds());
            lockUI(getTranslatedString("Saving Document").concat("..."));
            documentSaver = new DataFileSaver(dataFile);
            documentSaver.execute();

            // Wait for completion of the worker before continuing.
            result = documentSaver.get();
            if (result.isSaved())
            {
                // Report the event to the component controller.
                eventParameters = new HashMap<>();
                controller.reportEvent(this, definition.getComponentEventDefinition(T8DataRecordEditorAPIHandler.EVENT_RECORD_CHANGES_SAVED), eventParameters);
                if (!suppressToast) Toast.show(getTranslatedString("Changes Saved Successfully"), Toast.Style.SUCCESS);
                return true;
            }
            else
            {
                JOptionPane.showMessageDialog(this, getTranslatedString("Please correct invalid data.") + "\n" + getTranslatedString("You can view validation errors by hovering the mouse over the indicated Property labels."), getTranslatedString("Invalid Content"), JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        catch (Throwable e)
        {
            LOGGER.log("Exception while starting Record Change saver thread.", e);
            JOptionPane.showMessageDialog(this, getTranslatedString("An unexpected exception prevented successful saving of Record Changes.\nPlease try again or contact your system administrator.\n\nHint: Check your network connection and ensure that all attachments have completed uploading."), getTranslatedString("Operation Failure"), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        finally
        {
            // Reset access identifier to the default.
            accessContext.setDataAccessId(finalAccessId); // Set the default access.
            unlockUI();
        }
    }

    public boolean validateSessionDataRecords()
    {
        try
        {
            DataRecord dataFile;
            DataFileValidator documentValidator;

            // Commit outstanding changes.
            recordContainer.commitChanges();

            // Get the list of all records.
            dataFile = recordContainer.getRootDataRecord();

            // Do the validation of all open records.
            lockUI(getTranslatedString("Validating Documents").concat("..."));
            documentValidator = new DataFileValidator(dataFile);
            documentValidator.execute();

            // Wait for completion of the worker before continuing.
            documentValidator.get();

            // Return the result indicator.
            return isSessionValid();
        }
        catch (Throwable e)
        {
            LOGGER.log("Exception while validating session documents.", e);
            T8OptionPane.showMessageDialog(this, getTranslatedString("The updated records could not be validated successfully."), getTranslatedString("Status"), T8OptionPane.ERROR_MESSAGE, controller);
            return false;
        }
    }

    public void refreshEditor()
    {
        recordContainer.refreshEditors();
    }

    public boolean isSessionValid()
    {
        return recordContainer.isEditorContentValid();
    }

    public boolean isContentValid(String recordID)
    {
        return recordContainer.isEditorContentValid(recordID);
    }

    protected void exportDataFile()
    {
        T8DataRecordExportDialog.exportDataFile(SwingUtilities.getWindowAncestor(this), terminologyProvider, recordContainer.getRootDataRecord());
    }

    @Override
    public Map<String, Object> getSelectedValue(DataRecord dataRecord, ValueRequirement valueRequirement, List<String> orgIDList, List<String> odtIDList, List<String> conceptIDList, List<Pair<String, String>> conceptDependencyList)
    {
        String dialogIdentifier;

        dialogIdentifier = definition.getValueLookupDialogIdentifier();
        if (!Strings.isNullOrEmpty(dialogIdentifier))
        {
            Map<String, Object> outputParameters;
            Map<String, Object> inputParameters;
            String propertyId;
            String fieldId;
            String title;

            // Determine the title for the lookup dialog.
            fieldId = valueRequirement.getFieldID();
            propertyId = valueRequirement.getPropertyID();
            if (fieldId != null) title = terminologyProvider.getTerm(null, fieldId);
            else title = terminologyProvider.getTerm(null, propertyId);

            // Create the dialog input parameter map and then open the dialog.
            inputParameters = new HashMap<>();
            inputParameters.put(dialogIdentifier + PARAMETER_VALUE_LOOKUP_IN_TITLE, title);
            inputParameters.put(dialogIdentifier + PARAMETER_VALUE_LOOKUP_IN_DATA_RECORD, dataRecord);
            inputParameters.put(dialogIdentifier + PARAMETER_VALUE_LOOKUP_IN_VALUE_REQUIREMENT, valueRequirement);
            inputParameters.put(dialogIdentifier + PARAMETER_VALUE_LOOKUP_IN_FILTER_ORG_ID_LIST, orgIDList);
            inputParameters.put(dialogIdentifier + PARAMETER_VALUE_LOOKUP_IN_FILTER_ODT_ID_LIST, odtIDList);
            inputParameters.put(dialogIdentifier + PARAMETER_VALUE_LOOKUP_IN_FILTER_CONCEPT_ID_LIST, conceptIDList);
            inputParameters.put(dialogIdentifier + PARAMETER_VALUE_LOOKUP_IN_FILTER_CONCEPT_DEPENDENCY_LIST, conceptDependencyList);
            outputParameters = controller.showDialog(dialogIdentifier, inputParameters, true);

            // Strip the dialog namespace from the output parameters.
            return T8IdentifierUtilities.stripNamespace(outputParameters);
        }
        else throw new RuntimeException("No value lookup dialog identifier is set for this data record editor.");
    }

    @Override
    public Map<String, Object> getSelectedClassification(String ontologyStructureID, List<String> oCIDList)
    {
        String dialogIdentifier;

        dialogIdentifier = definition.getClassificationLookupDialogIdentifier();
        if (!Strings.isNullOrEmpty(dialogIdentifier))
        {
            Map<String, Object> outputParameters;
            Map<String, Object> inputParameters;
            String title;

            // Determine the title for the lookup dialog.
            title = terminologyProvider.getTerm(null, ontologyStructureID);

            // Create the dialog input parameter map and then open the dialog.
            inputParameters = new HashMap<>();
            inputParameters.put(dialogIdentifier + PARAMETER_CLASSIFICATION_LOOKUP_IN_TITLE, title);
            inputParameters.put(dialogIdentifier + PARAMETER_CLASSIFICATION_LOOKUP_IN_FILTER_ORG_ID, context.getSessionContext().getRootOrganizationIdentifier());
            inputParameters.put(dialogIdentifier + PARAMETER_CLASSIFICATION_LOOKUP_IN_FILTER_OS_ID, ontologyStructureID);
            inputParameters.put(dialogIdentifier + PARAMETER_CLASSIFICATION_LOOKUP_IN_FILTER_OC_ID_LIST, oCIDList);
            outputParameters = controller.showDialog(dialogIdentifier, inputParameters, true);

            // Strip the dialog namespace from the output parameters.
            return T8IdentifierUtilities.stripNamespace(outputParameters);
        }
        else throw new RuntimeException("No classification lookup dialog identifier is set for this data record editor.");
    }

    @Override
    public String getDescriptionClassificationIdentifier()
    {
        return definition.getDescriptionClassificationIdentifier();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }

    @Override
    public T8Context getAccessContext()
    {
        return accessContext;
    }

    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public String getProjectId()
    {
        return definition.getRootProjectId();
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        jXMultiSplitPaneContent.getMultiSplitLayout().displayNode("left", !definition.isTreeNavigationHidden());
    }

    @Override
    public void startComponent()
    {
        try
        {
            // Set the default access context.
            setAccess(finalAccessId);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception when setting access '" + finalAccessId + "' during record editor startup.", e);
        }
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private void setPropertyFFTVisible(boolean visible)
    {
        this.recordContainer.setPropertyFFTVisible(visible);
    }

    private void reloadRecords()
    {
        final String rootRecordID;

        // Get the root record ID.
        rootRecordID = navigationModel.getRootRecordID();

        // Remove all documents.
        removeAllDocuments();

        // If a document was previously loaded, load it again.
        if (rootRecordID != null)
        {
            new Thread()
            {
                @Override
                public void run()
                {
                    loadDocument(rootRecordID, true);
                }
            }.start();
        }
    }

    private void showHistory()
    {
        T8ModuleDefinition moduleDefinition;
        String moduleId;

        //Load the dialog definition
        moduleId = definition.getDataRecordHistoryModuleIdentifier();
        if (!Strings.isNullOrEmpty(moduleId))
        {
            try
            {

                moduleDefinition = (T8ModuleDefinition)controller.getClientContext().getDefinitionManager().getInitializedDefinition(context, definition.getRootProjectId(), moduleId, null);
                if (moduleDefinition == null) throw new RuntimeException("Data Record history module not found: " + moduleId);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Could not load Data Record history definition in editor: " + definition, e);
            }
        }
        else throw new RuntimeException("No Data Record history module identifier is set for this data record editor.");

        //Show the dialog
        Map<String, Object> moduleParameters;
        T8Component module;
        JDialog dialog;

        try
        {
            moduleParameters = new HashMap<>();
            moduleParameters.put("$P_RECORD_ID", getRecordID());
            moduleParameters.put("$P_ROOT_RECORD_ID", getRootDataRecord().getID());
            moduleParameters = T8IdentifierUtilities.prependNamespace(moduleId, moduleParameters);

            module = moduleDefinition.getNewComponentInstance(controller);
            module.initializeComponent(moduleParameters);

            dialog = new JDialog(SwingUtilities.windowForComponent(this));
            dialog.getContentPane().add((Component) module);

            module.startComponent();

            dialog.setPreferredSize(calculateDialogModuleSize());
            dialog.pack();
            dialog.setModal(true);
            dialog.setLocationRelativeTo(SwingUtilities.windowForComponent(this));
            dialog.setVisible(true);

            module.stopComponent();

            dialog.dispose();
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to create the Data Record History Dialog", ex);
        }
    }

    private Dimension calculateDialogModuleSize()
    {
        Dimension parentSize;
        Dimension dialogSize;

        parentSize = SwingUtilities.windowForComponent(this).getSize();
        dialogSize = new Dimension();

        dialogSize.height = (int)(parentSize.height * 0.8);
        dialogSize.width = (int)(parentSize.width * 0.8);

        return dialogSize;
    }

    private void toggleTreeNavigation()
    {
        jXMultiSplitPaneContent.getMultiSplitLayout().displayNode("left", !jToggleButtonTreeNavigation.isSelected());
    }

    /**
     * Loads a document from the database and then adds the document to the UI.
     * The loaded document is not necessarily visible after this thread has
     * finished execution.
     */
    private class DRInstanceChanger extends SwingWorker<DataRecord, Void>
    {
        private final String targetRecordId;
        private final String drInstanceId;
        private DataRecord outputTargetRecord;
        private DataRecord dataFile;
        private String accessIdentifier;

        private DRInstanceChanger(String recordID, String drInstanceID, String accessIdentifier)
        {
            this.targetRecordId = recordID;
            this.drInstanceId = drInstanceID;
            this.accessIdentifier = accessIdentifier;
        }

        @Override
        public DataRecord doInBackground() throws Exception
        {
            DataRecord outputRootRecord;

            // Add the target record and all of its already loaded/altered descendants.
            dataFile = getRootDataRecord();

            // Commit changes on all editors.
            commitChanges();

            // Do the change on the server and then update the UI with the returned records.
            outputRootRecord = recApi.changeRecordDrInstance(dataFile, targetRecordId, drInstanceId, context.getSessionContext().getContentLanguageIdentifier(), true, true, true, true, true, true);
            if (outputRootRecord == null) throw new Exception("Data Record not found: " + dataFile.getID());
            else
            {
                DocumentReferenceList referenceList;

                // Find the updated target record.
                outputTargetRecord = outputRootRecord.findDataRecord(targetRecordId);
                outputTargetRecord.removeFromReferrerRecords();

                // Clear dependent values if (required).
                referenceList =  dataFile.findDependentReference(targetRecordId);
                if (ontApi.validateConceptRelations(referenceList.getDependantLinks(targetRecordId)).size() > 0)
                {
                    referenceList.clearDependentValues(targetRecordId);
                }

                // Return the target record.
                return outputTargetRecord;
            }
        }

        @Override
        public void done()
        {
            if (outputTargetRecord != null)
            {
                DataRecordStructureDFIterator iterator;
                DataRecordStructure dataRecordStructure;

                // Remove the changed record editor and it's descendants.
                removeDocument(targetRecordId);

                // Create a structure from the output records, so that we can establish the correct tree levels.
                dataRecordStructure = new DataRecordStructure(null);
                dataRecordStructure.setRecords(targetRecordId, outputTargetRecord.getDataRecords());

                // Add all of the retrieved data records to the UI (in the proper hierarchy).
                iterator = dataRecordStructure.getDepthFirstIterator();
                while (iterator.hasNext())
                {
                    DataRecordStructureNode nextNode;
                    DataRecord dataRecord;

                    // Get the next node from the structure and from the node, get the data record it holds.
                    nextNode = iterator.next();
                    dataRecord = nextNode.getDataRecord();
                    addDocument(dataRecord);
                }

                // Refresh the editor to reflect the model changes.
                refreshEditor();

                // Show the requested record.
                showDocument(targetRecordId);
            }

            // Unlock the UI.
            unlockUI();
        }
    }

    private class DocumentCreator extends SwingWorker<DataRecord, Void>
    {
        private final DataRecord dataFile;
        private final String parentRecordId;
        private final String propertyId;
        private final String fieldId;
        private final String drInstanceId;

        private DocumentCreator(DataRecord rootDataRecord, String parentRecordId, String propertyId, String fieldId, String drInstanceId)
        {
            this.dataFile = rootDataRecord;
            this.parentRecordId = parentRecordId;
            this.propertyId = propertyId;
            this.fieldId = fieldId;
            this.drInstanceId = drInstanceId;
        }

        @Override
        protected DataRecord doInBackground() throws Exception
        {
            DataRecord newRecord;

            // Create the new record using the API.
            newRecord = recApi.createRecord(dataFile, parentRecordId, propertyId, fieldId, drInstanceId, context.getSessionContext().getContentLanguageIdentifier(), true, true);

            // Remove the new record from its parent (we already have its parent on the client editor).
            for (DataRecord parentRecord : newRecord.getReferrerRecords())
            {
                parentRecord.removeSubRecord(newRecord);
            }

            // Add the terminology retrieved as part of the DR Instance to the terminology provider.
            terminologyProvider.addTerminology(newRecord.getContentTerminology(true));

            // Return the new record.
            return newRecord;
        }
    }

    /**
     * Loads a document from the database and then adds the document to the UI.
     * The loaded document is not necessarily visible after this thread has
     * finished execution.
     */
    private class DocumentLoader extends SwingWorker<DataRecord, Void>
    {
        private final String recordId;
        private DataRecord retrievedFile;

        private DocumentLoader(String recordId)
        {
            this.recordId = recordId;
        }

        @Override
        public DataRecord doInBackground() throws Exception
        {
            // Retrieve the data file by using the record ID of one of its content records.
            retrievedFile = dataRecordProvider.getDataFileByContentRecord(recordId, context.getSessionContext().getContentLanguageIdentifier(), true, true, true, true, true, true);
            if (retrievedFile == null) throw new Exception("Data Record not found: " + recordId);
            return retrievedFile;
        }
    }

    private class DataFileSaver extends SwingWorker<T8DataFileSaveResult, Void>
    {
        private final DataRecord dataFile;

        private DataFileSaver(DataRecord dataFile)
        {
            this.dataFile = dataFile;
        }

        @Override
        public T8DataFileSaveResult doInBackground() throws Exception
        {
            T8DataFileSaveResult result;

            // Save all affected documents.
            result = recApi.saveFileStateChange(dataFile);
            if (result.isSaved())
            {
                // Reset the saved state of the record container so that existng change are records as 'saved'.
                recordContainer.resetSavedState(result.getNewFile());

                // Clear existing validation errors.
                setValidationReport(null);
            }
            else
            {
                // Set the new validation errors.
                setValidationReport(result.getValidationReport());
            }

            // Return void.
            return result;
        }

        @Override
        public void done()
        {
            // Refresh the editor to reflect changes to the local state after successful save.
            refreshEditor();

            // Unlock the UI.
            unlockUI();
        }
    }

    private class DataFileValidator extends SwingWorker<Void, Void>
    {
        private final DataRecord dataFile;
        private T8DataFileValidationReport validationReport;

        private DataFileValidator(DataRecord dataFile)
        {
            this.dataFile = dataFile;
        }

        @Override
        public Void doInBackground() throws Exception
        {
            validationReport = recApi.validateFileStateChange(dataFile);
            return null;
        }

        @Override
        public void done()
        {
            // Set the validation errors on the main record editor.
            setValidationReport(validationReport);

            // Unlock the UI.
            unlockUI();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jToolBarMainControls = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jSeparatorRefresh = new javax.swing.JToolBar.Separator();
        jButtonSaveDocument = new javax.swing.JButton();
        jSeparatorSave = new javax.swing.JToolBar.Separator();
        jButtonExportDocument = new javax.swing.JButton();
        jSeparatorExport = new javax.swing.JToolBar.Separator();
        jToggleButtonPropertyFFT = new javax.swing.JToggleButton();
        jSeparatorPropFFT = new javax.swing.JToolBar.Separator();
        jToggleButtonTreeNavigation = new javax.swing.JToggleButton();
        jSeparatorTreeNavigation = new javax.swing.JToolBar.Separator();
        jButtonShowHistory = new javax.swing.JButton();
        jSeparatorHistory = new javax.swing.JToolBar.Separator();
        jButtonGotoNextError = new javax.swing.JButton();
        jXMultiSplitPaneContent = new org.jdesktop.swingx.JXMultiSplitPane();
        jPanelNavigation = new javax.swing.JPanel();
        jPanelContent = new javax.swing.JPanel();
        jPanelBreadCrumbs = new javax.swing.JPanel();
        jPanelContentEditor = new javax.swing.JPanel();

        jPanelMain.setBackground(new java.awt.Color(255, 255, 255));
        jPanelMain.setOpaque(false);
        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jToolBarMainControls.setFloatable(false);
        jToolBarMainControls.setRollover(true);
        jToolBarMainControls.setOpaque(false);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setText(getTranslatedString("Refresh"));
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setOpaque(false);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jButtonRefresh);
        jToolBarMainControls.add(jSeparatorRefresh);

        jButtonSaveDocument.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/databaseSaveIcon.png"))); // NOI18N
        jButtonSaveDocument.setText(getTranslatedString("Save Changes"));
        jButtonSaveDocument.setOpaque(false);
        jButtonSaveDocument.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSaveDocument.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSaveDocumentActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jButtonSaveDocument);
        jToolBarMainControls.add(jSeparatorSave);

        jButtonExportDocument.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/exportIcon.png"))); // NOI18N
        jButtonExportDocument.setText(getTranslatedString("Export Document"));
        jButtonExportDocument.setOpaque(false);
        jButtonExportDocument.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonExportDocument.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonExportDocumentActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jButtonExportDocument);
        jToolBarMainControls.add(jSeparatorExport);

        jToggleButtonPropertyFFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/pageWhiteTextIcon.png"))); // NOI18N
        jToggleButtonPropertyFFT.setText(getTranslatedString("Property FFT"));
        jToggleButtonPropertyFFT.setFocusable(false);
        jToggleButtonPropertyFFT.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButtonPropertyFFT.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jToggleButtonPropertyFFTActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jToggleButtonPropertyFFT);
        jToolBarMainControls.add(jSeparatorPropFFT);

        jToggleButtonTreeNavigation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/treeExpandIcon.png"))); // NOI18N
        jToggleButtonTreeNavigation.setText(getTranslatedString("Hide Tree"));
        jToggleButtonTreeNavigation.setFocusable(false);
        jToggleButtonTreeNavigation.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButtonTreeNavigation.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jToggleButtonTreeNavigationActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jToggleButtonTreeNavigation);
        jToolBarMainControls.add(jSeparatorTreeNavigation);

        jButtonShowHistory.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/history.png"))); // NOI18N
        jButtonShowHistory.setText("View History");
        jButtonShowHistory.setFocusable(false);
        jButtonShowHistory.setOpaque(false);
        jButtonShowHistory.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonShowHistory.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonShowHistoryActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jButtonShowHistory);
        jToolBarMainControls.add(jSeparatorHistory);

        jButtonGotoNextError.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/exclamationIcon.png"))); // NOI18N
        jButtonGotoNextError.setText("Go to Next Error");
        jButtonGotoNextError.setToolTipText("Navigate to the next validation error.");
        jButtonGotoNextError.setFocusable(false);
        jButtonGotoNextError.setOpaque(false);
        jButtonGotoNextError.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonGotoNextError.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonGotoNextErrorActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jButtonGotoNextError);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelMain.add(jToolBarMainControls, gridBagConstraints);

        jXMultiSplitPaneContent.setBackground(new java.awt.Color(255, 255, 255));
        jXMultiSplitPaneContent.setOpaque(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanelMain.add(jXMultiSplitPaneContent, gridBagConstraints);

        jPanelNavigation.setBackground(new java.awt.Color(255, 255, 255));
        jPanelNavigation.setLayout(new java.awt.BorderLayout());

        jPanelContent.setBackground(new java.awt.Color(255, 255, 255));
        jPanelContent.setLayout(new java.awt.BorderLayout());

        jPanelBreadCrumbs.setBackground(new java.awt.Color(255, 255, 255));
        jPanelBreadCrumbs.setOpaque(false);
        jPanelBreadCrumbs.setLayout(new java.awt.BorderLayout());
        jPanelContent.add(jPanelBreadCrumbs, java.awt.BorderLayout.NORTH);

        jPanelContentEditor.setBackground(new java.awt.Color(255, 255, 255));
        jPanelContentEditor.setLayout(new java.awt.BorderLayout());
        jPanelContent.add(jPanelContentEditor, java.awt.BorderLayout.CENTER);

        setBackground(new java.awt.Color(255, 255, 255));
        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonExportDocumentActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExportDocumentActionPerformed
    {//GEN-HEADEREND:event_jButtonExportDocumentActionPerformed
        exportDataFile();
    }//GEN-LAST:event_jButtonExportDocumentActionPerformed

    private void jButtonSaveDocumentActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSaveDocumentActionPerformed
    {//GEN-HEADEREND:event_jButtonSaveDocumentActionPerformed
        // Save changes in a new thread.
        new Thread()
        {
            @Override
            public void run()
            {
                saveSessionDataRecords(AccessValidationLevel.SAVE, false);
            }
        }.start();
    }//GEN-LAST:event_jButtonSaveDocumentActionPerformed

    private void jToggleButtonPropertyFFTActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jToggleButtonPropertyFFTActionPerformed
    {//GEN-HEADEREND:event_jToggleButtonPropertyFFTActionPerformed
        setPropertyFFTVisible(jToggleButtonPropertyFFT.isSelected());
    }//GEN-LAST:event_jToggleButtonPropertyFFTActionPerformed

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        int option;

        option = JOptionPane.showConfirmDialog(this, getTranslatedString("Refresh all open documents and revert unsaved changes?"), getTranslatedString("Confirmation"), JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.YES_OPTION) reloadRecords();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jToggleButtonTreeNavigationActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jToggleButtonTreeNavigationActionPerformed
    {//GEN-HEADEREND:event_jToggleButtonTreeNavigationActionPerformed
        toggleTreeNavigation();
    }//GEN-LAST:event_jToggleButtonTreeNavigationActionPerformed

    private void jButtonShowHistoryActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonShowHistoryActionPerformed
    {//GEN-HEADEREND:event_jButtonShowHistoryActionPerformed
        showHistory();
    }//GEN-LAST:event_jButtonShowHistoryActionPerformed

    private void jButtonGotoNextErrorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonGotoNextErrorActionPerformed
    {//GEN-HEADEREND:event_jButtonGotoNextErrorActionPerformed
        navigateToNextValidationError();
    }//GEN-LAST:event_jButtonGotoNextErrorActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonExportDocument;
    private javax.swing.JButton jButtonGotoNextError;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JButton jButtonSaveDocument;
    private javax.swing.JButton jButtonShowHistory;
    private javax.swing.JPanel jPanelBreadCrumbs;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelContentEditor;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JPanel jPanelNavigation;
    private javax.swing.JToolBar.Separator jSeparatorExport;
    private javax.swing.JToolBar.Separator jSeparatorHistory;
    private javax.swing.JToolBar.Separator jSeparatorPropFFT;
    private javax.swing.JToolBar.Separator jSeparatorRefresh;
    private javax.swing.JToolBar.Separator jSeparatorSave;
    private javax.swing.JToolBar.Separator jSeparatorTreeNavigation;
    private javax.swing.JToggleButton jToggleButtonPropertyFFT;
    private javax.swing.JToggleButton jToggleButtonTreeNavigation;
    private javax.swing.JToolBar jToolBarMainControls;
    private org.jdesktop.swingx.JXMultiSplitPane jXMultiSplitPaneContent;
    // End of variables declaration//GEN-END:variables
}
