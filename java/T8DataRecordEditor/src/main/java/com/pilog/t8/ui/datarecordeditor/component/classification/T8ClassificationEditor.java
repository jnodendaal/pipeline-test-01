package com.pilog.t8.ui.datarecordeditor.component.classification;

import com.pilog.t8.api.T8OrganizationClientApi;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.data.org.T8Organization;
import com.pilog.t8.data.document.datarecord.access.layer.ClassificationAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.data.document.validation.T8RecordClassificationValidationError;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ClassificationEditor extends javax.swing.JPanel
{
    private final DataRecordEditorContainer container;
    private final DataRecord dataRecord;
    private final DataRecordAccessLayer accessLayer;
    private final List<T8ClassificationHeaderComponent> headerComponents;
    private final List<T8ClassificationValueComponent> valueComponents;
    private final List<T8OntologyLink> ontologyLinks;
    private final T8OrganizationClientApi orgApi;

    public T8ClassificationEditor(DataRecordEditorContainer container, DataRecord dataRecord)
    {
        initComponents();

        this.container = container;
        this.dataRecord = dataRecord;
        this.accessLayer = dataRecord.getAccessLayer();
        this.headerComponents = new ArrayList<T8ClassificationHeaderComponent>();
        this.valueComponents = new ArrayList<T8ClassificationValueComponent>();
        this.ontologyLinks = new ArrayList<T8OntologyLink>();
        this.orgApi = container.getAccessContext().getClientContext().getConfigurationManager().getAPI(container.getAccessContext(), T8OrganizationClientApi.API_IDENTIFIER);
        setAvailableOntologyLinks();
        rebuildContent();
    }

    public boolean hasVisibleContent()
    {
        return (ontologyLinks.size() > 0);
    }

    private void setAvailableOntologyLinks()
    {
        T8OrganizationStructure orgStructure;
        T8Organization rootNode;
        List<String> osIdList;
        T8OntologyConcept concept;

        // Get the organization structure.
        orgStructure = orgApi.getOrganizationStructure();
        rootNode = orgStructure.getRootNode();

        // First add all links already available on the record.
        osIdList = new ArrayList<String>();
        concept = dataRecord.getOntology();
        if (concept != null)
        {
            for (T8OntologyLink link : concept.getOntologyLinks())
            {
                ClassificationAccessLayer classificationAccessLayer;
                String osId;

                osId = link.getOntologyStructureID();
                classificationAccessLayer = accessLayer.getClassificationAccessLayer(osId);
                if ((!osIdList.contains(osId)) && (classificationAccessLayer != null) && (classificationAccessLayer.isVisible()))
                {
                    osIdList.add(osId);
                    ontologyLinks.add(link);
                }
            }
        }

        // Next, add all ontology structures additional to the default and not already set on the concept.
        for (String osId : rootNode.getOntologyStructureIDList())
        {
            ClassificationAccessLayer classificationAccessLayer;

            classificationAccessLayer = accessLayer.getClassificationAccessLayer(osId);
            if ((!osIdList.contains(osId)) && (classificationAccessLayer != null) && (classificationAccessLayer.isVisible()))
            {
                osIdList.add(osId);
                ontologyLinks.add(new T8OntologyLink(rootNode.getID(), osId, null, dataRecord.getID()));
            }
        }
    }

    public boolean commitChanges()
    {
        T8OntologyConcept recordOntology;

        // Add the changes to the data record.
        recordOntology = dataRecord.getOntology();
        for (T8OntologyLink link : ontologyLinks)
        {
            // If the ontology class has been cleared from the link, remove it from the document.
            if (link.getOntologyClassID() == null)
            {
                recordOntology.removeOntologyLink(link);
            }
            else if (!recordOntology.containsEquivalentLink(link))
            {
                // Add the link to the record.
                recordOntology.addOntologyLink(link);
            }
        }

        return true;
    }

    private void rebuildContent()
    {
        int gridY;

        gridY = 0;
        jPanelContent.removeAll();
        headerComponents.clear();
        valueComponents.clear();
        for (T8OntologyLink link : ontologyLinks)
        {
            T8ClassificationValueComponent valueComponent;
            T8ClassificationHeaderComponent headerComponent;
            GridBagConstraints gridBagConstraints;

            valueComponent = new T8ClassificationValueComponent(container, dataRecord, link);
            headerComponent = new T8ClassificationHeaderComponent(container, dataRecord, valueComponent, link);
            headerComponents.add(headerComponent);
            valueComponents.add(valueComponent);

            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = gridY;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 0.0;
            gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
            jPanelContent.add(headerComponent, gridBagConstraints);

            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 1;
            gridBagConstraints.gridy = gridY++;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 0.1;
            jPanelContent.add(valueComponent, gridBagConstraints);
        }

        jPanelContent.validate();
    }

    public boolean hasChanges()
    {
        return false;
    }

    public void refreshAccess()
    {
        for (T8ClassificationHeaderComponent headerComponent : headerComponents)
        {
            headerComponent.refreshAccess();
        }
    }

    public void clearValidationErrors()
    {
        for (T8ClassificationHeaderComponent headerComponent : headerComponents)
        {
            headerComponent.setValidationErrors(null);
        }
    }

    /**
     * Distributes the list of validation errors to the corresponding classification
     * header components.
     *
     * @param validationErrors The validation errors to distribute/set on the
     * applicable classification header components.
     */
    public void setValidationErrors(List<T8DataValidationError> validationErrors)
    {
        clearValidationErrors();
        if (validationErrors != null)
        {
            // Set the validation errors on the header components.
            for (T8ClassificationHeaderComponent headerComponent : headerComponents)
            {
                List<T8RecordClassificationValidationError> classificationValidtionErrors;
                String headerOsId;

                headerOsId = headerComponent.getOntologyLink().getOntologyStructureID();
                classificationValidtionErrors = new ArrayList<T8RecordClassificationValidationError>();
                for (T8DataValidationError validationError : validationErrors)
                {
                    if (validationError instanceof T8RecordClassificationValidationError)
                    {
                        T8RecordClassificationValidationError classificationError;

                        classificationError = (T8RecordClassificationValidationError)validationError;
                        if (headerOsId.equals(classificationError.getOntologyStructureId()))
                        {
                            classificationValidtionErrors.add(classificationError);
                        }
                    }
                }

                // Set the validation errors on the header component.
                headerComponent.setValidationErrors(classificationValidtionErrors);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridBagLayout());

        jPanelContent.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelContent, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelContent;
    // End of variables declaration//GEN-END:variables

}
