package com.pilog.t8.ui.datarecordeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.access.T8DefaultDataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.event.T8DataRecordChangeListener;
import com.pilog.t8.data.document.datarecord.event.T8DataRecordChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordPropertyAddedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordPropertyValueChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordValueChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordValueStructureChangedEvent;
import com.pilog.t8.data.document.datarecord.value.ConceptValue;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.definition.data.document.datarecord.access.T8DataRecordAccessDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecordeditor.view.content.ContentView;
import com.pilog.t8.ui.datarecordeditor.view.navigation.NavigationModel;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class RecordManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordEditor.class);

    private final T8Context context;
    private final T8ComponentController controller;
    private final HashMap<String, ContentView> recordEditors;
    private final HashMap<String, DataRecord> records;
    private final T8DataRecordEditor parentEditor;
    private final NavigationModel navigationModel;
    private final DataRecordChangeListener recordChangeListener;
    private final TerminologyGenerator terminologyGenerator;
    private final String projectId;
    private T8DataFileValidationReport validationReport;
    private ContentView currentEditor;
    private T8DataRecordAccessHandler accessHandler;
    private boolean propertyFFTVisible;
    private String rootRecordID;
    private boolean programmaticRefresh;

    public RecordManager(T8DataRecordEditor parentEditor, NavigationModel navigationModel)
    {
        this.parentEditor = parentEditor;
        this.context = parentEditor.getAccessContext();
        this.controller = parentEditor.getController();
        this.navigationModel = navigationModel;
        this.recordChangeListener = new DataRecordChangeListener();
        this.terminologyGenerator = new TerminologyGenerator(parentEditor);
        this.recordEditors = new HashMap<>();
        this.records = new HashMap<>();
        this.propertyFFTVisible = false;
        this.programmaticRefresh = false;
        this.validationReport = new T8DataFileValidationReport();
        this.projectId = parentEditor.getComponentDefinition().getRootProjectId();
    }

    public void setAccessHandler(String accessId) throws Exception
    {
        // We only need to set a new access handler if the requested identifier differs from the one we already have.
        if (!Objects.equals(accessId, accessHandler.getIdentifier()))
        {
            if (accessId == null)
            {
                setAccessHandler((T8DataRecordAccessDefinition)null);
            }
            else
            {
                T8DataRecordAccessDefinition accessDefinition;

                accessDefinition = (T8DataRecordAccessDefinition)context.getClientContext().getDefinitionManager().getInitializedDefinition(context, projectId, accessId, null);
                if (accessDefinition != null)
                {
                    setAccessHandler(accessDefinition);
                }
                else throw new Exception("Access Definition not found: " + accessId);
            }
        }
    }

    public void setAccessHandler(T8DataRecordAccessDefinition accessDefinition)
    {
        // Creation of new handler.
        if (accessDefinition != null)
        {
            accessHandler = accessDefinition.getNewDataRecordAccessHandlerInstance(context);
        }
        else
        {
            accessHandler = new T8DefaultDataRecordAccessHandler(null, true);
        }
    }

    public TerminologyGenerator getTerminologyGenerator()
    {
        return terminologyGenerator;
    }

    /**
     * Returns boolean true if any of the records currently loaded into the
     * editor has changes that have not been saved yet.
     *
     * @return boolean true if any of the records currently loaded into the
     * editor has changes that have not been saved yet.
     */
    public boolean hasUnsavedChanges()
    {
        DataRecord currentFile;

        currentFile = getRootDataRecord();
        if (currentFile != null)
        {
            DataRecord savedStateFile;

            savedStateFile = currentFile.getSavedState();
            if (savedStateFile == null) return true;
            else
            {
                for (String recordId : recordEditors.keySet())
                {
                    ContentView recordEditor;
                    DataRecord currentRecord;
                    DataRecord savedStateRecord;

                    // Get the record from the editor and also its saved state.
                    recordEditor = recordEditors.get(recordId);
                    recordEditor.commitChanges();
                    currentRecord = recordEditor.getDataRecord();
                    savedStateRecord = savedStateFile.findDataRecord(recordId);
                    if (savedStateRecord == null) return true; // The record is newly inserted.
                    else
                    {
                        String currentStateString;
                        String savedStateString;

                        // Compare the state strings of the two records to determine whether or not their content differs.
                        currentStateString = generateSavedStateValueString(currentRecord);
                        savedStateString = generateSavedStateValueString(savedStateRecord);
                        if (!Objects.equals(currentStateString, savedStateString)) return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * This method returns a value String for the specified record that is
     * specifically used for saved-state checking.
     *
     * @param record The record for which to generate the value String.
     * @return The generated Value String.
     */
    private String generateSavedStateValueString(DataRecord record)
    {
        T8DataRecordValueStringGenerator valueStringGenerator;
        String stateString;

        valueStringGenerator = new T8DataRecordValueStringGenerator();
        valueStringGenerator.setIncludeSubDocumentReferences(true);
        valueStringGenerator.setIncludePropertyFFT(true);
        valueStringGenerator.setIncludeRecordFFT(true);
        valueStringGenerator.setIncludeUnresolvedTerms(true);
        valueStringGenerator.setIncludeOntologyLinks(true);
        stateString = valueStringGenerator.generateValueString(record).toString();

        // Add all particle settings.
        for (T8DataStringParticleSettings particleSettings : record.getDataStringParticleSettings())
        {
            stateString += particleSettings.isInclude();
            stateString += particleSettings.getId();
            stateString += particleSettings.getDataTypeId();
            stateString += particleSettings.getDrId();
            stateString += particleSettings.getDrInstanceId();
            stateString += particleSettings.getDsTypeId();
            stateString += particleSettings.getLanguageId();
            stateString += particleSettings.getPropertyId();
            stateString += particleSettings.getFieldId();
            stateString += particleSettings.getOrgId();
            stateString += particleSettings.getRecordId();
            stateString += ":";
            stateString += particleSettings.getPriority();
            stateString += ",";
            stateString += particleSettings.getSequence();
        }

        // Add all ontology links settings.
        for (T8OntologyLink ontologyLink : record.getOntologyLinks())
        {
            stateString += ontologyLink.getOrganizationID();
            stateString += ontologyLink.getOntologyStructureID();
            stateString += ontologyLink.getOntologyClassID();
            stateString += ontologyLink.getConceptID();
        }

        // Return the final state string.
        return stateString;
    }

    public void setPropertyFFTVisible(boolean visible)
    {
        this.propertyFFTVisible = visible;
        for (ContentView editor : recordEditors.values())
        {
            editor.setPropertyFFTVisible(visible);
        }
    }

    public void removeRecord(String recordID)
    {
        DataRecord recordToBeRemoved;
        DataRecord parentRecord;

        // Make sure we are using the EDT.
        if(!SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Must be called from EDT.");

        // Get the data record to be removed.
        recordToBeRemoved = records.get(recordID);
        parentRecord = recordToBeRemoved.getParentRecord();
        T8Log.log("Removing document '" + recordToBeRemoved + "' from parent: " + parentRecord);

        // Clear all listeners from the record.
        recordToBeRemoved.clearChangeListeners();

        // First remove all of the child documents.
        for (String subRecordID : recordToBeRemoved.getSubRecordReferenceIDSet(true, true))
        {
            if (containsDataRecord(subRecordID))
            {
                removeRecord(subRecordID);
            }
        }

        // Remove the record from its parent.
        if (parentRecord != null)
        {
            parentRecord.removeSubRecord(recordToBeRemoved);
        }

        // If the record to be removed is the one currently displayed, show its parent.
        if (currentEditor.getRecordID().equals(recordID))
        {
            setCurrentEditor(parentRecord != null ? parentRecord.getID() : null);
        }

        // Remove the record from the state collections.
        navigationModel.removeRecord(recordID);
        recordEditors.remove(recordID);
        records.remove(recordID);
    }

    public void removeAllDocuments()
    {
        if (!SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Must be called from EDT.");

        // First clear all listeners from the records.
        for (DataRecordEditor recordEditor : recordEditors.values())
        {
            recordEditor.getDataRecord().clearChangeListeners();
        }

        // Now clear the records from the UI and all local collections.
        navigationModel.clear();
        recordEditors.clear();
        records.clear();
        setValidationReport(null);
        parentEditor.showDocument(null);
    }

    public void addRecord(final DataRecord dataRecord)
    {
        String recordId;

        // Log the operation.
        LOGGER.log(() -> "Adding record: " + dataRecord + " with ODT_ID: " + dataRecord.getDataRequirementInstance().getOntology().getGroupID());

        // Get the ID of the new Record to add.
        recordId = dataRecord.getID();
        if (Strings.isNullOrEmpty(recordId)) throw new RuntimeException("Data Record object has empty ID: " + dataRecord);

        // Add the record to the records collection.
        if (records.isEmpty())
        {
            if (dataRecord.isIndependentRecord())
            {
                // This is the new root record.
                rootRecordID = recordId;
                records.put(recordId, dataRecord);

                // Queue the record for terminology regeneration.
                terminologyGenerator.regenerateDataRecordTerminology(recordId);

                // This is a new root document so add a root to the navigation model.
                navigationModel.setRootRecord(dataRecord);

                // Now add the root record changes listener to the new root record.
                dataRecord.addChangeListener(recordChangeListener);
            }
            else throw new RuntimeException("Cannot load dependent record as root in record editor: " + dataRecord);
        }
        else
        {
            List<String> referrerRecordIdList;

            // Find the parents of the record to be added.
            referrerRecordIdList = new ArrayList<>();
            for (DataRecord referrrerRecord : records.values())
            {
                if (referrrerRecord.containsReference(recordId))
                {
                    referrerRecordIdList.add(referrrerRecord.getID());
                    if (!referrrerRecord.containsSubRecord(dataRecord.getID())) referrrerRecord.addSubRecord(dataRecord);
                }
            }

            // Get the parent record (null if we are adding the root) and add the new record to it as a sub-record.
            if (referrerRecordIdList.isEmpty())
            {
                throw new RuntimeException("No parents found to which record belongs: " + dataRecord);
            }
            else
            {
                // Add the record to the collection.
                records.put(recordId, dataRecord);

                // Queue the record for terminology regeneration.
                terminologyGenerator.regenerateDataRecordTerminology(recordId);

                // Add the record to the navigation model.
                for (String referrerRecordID : referrerRecordIdList)
                {
                    // Add a new node to the navigation model.
                    navigationModel.addRecord(referrerRecordID, dataRecord);
                }
            }
        }
    }

    public ContentView getEditor(String recordId)
    {
        if (recordEditors.containsKey(recordId))
        {
            return recordEditors.get(recordId);
        }
        else
        {
            ContentView newRecordEditor;
            DataRecord dataRecord;

            // Get the data record for which to creat a new editor.
            dataRecord = records.get(recordId);

            // Log the operation.
            LOGGER.log(() -> "Creating editor for record: " + dataRecord);

            // Create a new editor for the Data Record and add it to the UI.
            dataRecord.setAdjusting(true); // Prevent redundant events from firing while we are adjuting the record.
            newRecordEditor = new ContentView(parentEditor, dataRecord);
            newRecordEditor.setValidationErrors(validationReport.getValidationErrors(recordId));
            newRecordEditor.setPropertyFFTVisible(propertyFFTVisible);
            recordEditors.put(recordId, newRecordEditor);
            dataRecord.setAdjusting(false, false);

            // Add the new editor to the cache.
            recordEditors.put(recordId, newRecordEditor);
            return newRecordEditor;
        }
    }

    public void commitChanges()
    {
        for (ContentView recordEditor : recordEditors.values())
        {
            recordEditor.commitChanges();
        }
    }

    public DataRecord getRootDataRecord()
    {
        return getDataRecord(rootRecordID);
    }

    public List<DataRecord> getDataRecords()
    {
        return new ArrayList<DataRecord>(records.values());
    }

    public DataRecord getDataRecord(String recordID)
    {
        return records.get(recordID);
    }

    public boolean containsDataRecord(String recordID)
    {
        return records.containsKey(recordID);
    }

    public void refreshEditors()
    {
        // Check the flag to ensure we are not performing a programmatic refresh (to prevent cyclic refreshes).
        if (!programmaticRefresh)
        {
            // Set the flag to indicate that a programmatic refresh has started and to prevent changes we make during the refresh from triggering subsequent refresh cycles.
            programmaticRefresh = true;

            // Commit outstanding data changes.
            T8Log.log("Starting Outstanding Commits...");
            for (DataRecordEditor editor : getDepthFirstEditorList())
            {
                editor.commitChanges();
            }
            T8Log.log("Finished Outstanding Commits...");

            // Refresh data dependencies.
            T8Log.log("Starting Dependency Refresh...");
            for (DataRecordEditor editor : getDepthFirstEditorList())
            {
                editor.refreshDataDependency();
            }
            T8Log.log("Finished Dependency Refresh...");

            // Refresh access logic.
            T8Log.log("Starting Access Refresh...");
            accessHandler.refreshDynamicState(getRootDataRecord());
            for (DataRecordEditor editor : recordEditors.values())
            {
                editor.refreshAccess();
            }
            T8Log.log("Finished Access Refresh.");

            // Refresh the editors so that they reflect all data object model updates that may have resulted from the previous steps in this refresh routine.
            T8Log.log("Starting Editor Refresh...");
            for (DataRecordEditor editor : recordEditors.values())
            {
                editor.refreshEditor();
            }
            T8Log.log("Finished Editor Refresh...");

            // Reset the flag to indicate that programmatic refresh has ended.
            programmaticRefresh = false;
        }
    }

    public boolean isEditorContentValid()
    {
        // Check the record validation error collection.
        if (validationReport.hasErrors()) return false;

        // Check each one of the document components.
        for (ContentView documentComponent : recordEditors.values())
        {
            if (!documentComponent.isContentValid()) return false;
        }

        // After all checks succeed, return true.
        return true;
    }

    public boolean isEditorContentValid(String recordID)
    {
        // Check the record validation error collection.
        if (validationReport.hasErrors(recordID)) return false;
        else return true;
    }

    public T8DataFileValidationReport getValidationReport()
    {
        return validationReport;
    }

    protected void setValidationReport(T8DataFileValidationReport newReport)
    {
        // Set the local instance collection.
        validationReport = newReport != null ? newReport : new T8DataFileValidationReport();

        // Set the validation errors on all the applicable document components.
        for (ContentView editor : recordEditors.values())
        {
            List<T8DataValidationError> recordErrors;
            String recordID;

            // Get the errors applicable to this record.
            recordID = editor.getRecordID();
            recordErrors = validationReport.getValidationErrors(recordID);
            if (recordErrors == null) recordErrors = new ArrayList<>();

            T8Log.log("Record validation errors for record: " + recordID + ": " + recordErrors);
            editor.setValidationErrors(recordErrors);

            // Repaint document component to allow any conditional rendering to take place after validation.
            editor.repaint();
        }
    }

    private List<DataRecordEditor> getDepthFirstEditorList()
    {
        List<DataRecordEditor> editorList;
        DataRecord rootRecord;

        // Create the result list.
        editorList = new ArrayList<>();

        // Get the root record;
        rootRecord = getRootDataRecord();
        if (rootRecord != null)
        {
            // Add the editors to the result list using depth-first order.
            for (String recordID : rootRecord.getDepthFirstRecordIDList())
            {
                DataRecordEditor editor;

                editor = recordEditors.get(recordID);
                if (editor != null)
                {
                    if (!editorList.contains(editor))
                    {
                        editorList.add(editor);
                    }
                }
            }
        }

        // Return the result list.
        return editorList;
    }

    public void setCurrentEditor(String recordId)
    {
        if (!SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Must be called from EDT.");
        if (recordId == null)
        {
            T8Log.log("Showing document: None");
            currentEditor = null;
            parentEditor.setEditor(currentEditor);
        }
        else
        {
            if ((currentEditor == null) || (!recordId.equals(currentEditor.getRecordID())))
            {
                T8Log.log("Showing document: " + recordId);
                currentEditor = getEditor(recordId);
                parentEditor.setEditor(currentEditor);
            }
        }
    }

    public ContentView getCurrentEditor()
    {
        return currentEditor;
    }

    public T8DataRecordAccessHandler getAccessHandler()
    {
        return accessHandler;
    }

    public String getAccessIdentifier()
    {
        return accessHandler.getIdentifier();
    }

    /**
     * This method uses the input saved-state file and updates the local version of the file (currently displayed in the editor)
     * with any information that may have been newly added or resolved in the saved state that was processed on the server.
     * An example of such an update is concept values that are entered as terms/codes/abbreviations on the client editor and
     * resolved to the proper concepts when saved.
     * @param savedState
     */
    public void resetSavedState(DataRecord savedState)
    {
        DataRecord dataFile;

        // Get the local data file currently displayed in the editor.
        dataFile = getRootDataRecord();
        if (dataFile != null)
        {
            // Synchronize all resolved concepts.
            for (DataRecord localRecord : dataFile.getDataFileRecords())
            {
                DataRecord savedStateRecord;

                // Get the saved state record and check all its concept values.
                savedStateRecord = savedState.findDataRecord(localRecord.getID());
                if (savedStateRecord != null)
                {
                    // Iterate over all values in the local record to find those that need to be synchronized.
                    for (RecordValue localValue : localRecord.getAllValues())
                    {
                        // If the local value is a concept, set the local concept id to the resolved id from the saved state.
                        if ((localValue != null) && (localValue instanceof ConceptValue))
                        {
                            ConceptValue localConceptValue;

                            localConceptValue = (ConceptValue)localValue;
                            if ((localConceptValue.getConceptId() == null) && (!Strings.isNullOrEmpty(localConceptValue.getTerm())))
                            {
                                RecordProperty savedStateProperty;

                                savedStateProperty = savedStateRecord.getRecordProperty(localValue.getPropertyID());
                                if (savedStateProperty != null)
                                {
                                    ConceptValue savedStateValue;
                                    String fieldId;

                                    // If the local value is on field level, get the saved state field value or else get the saved state property value.
                                    fieldId = localValue.getFieldID();
                                    if (fieldId != null)
                                    {
                                        Field field;

                                        field = savedStateProperty.getField(fieldId);
                                        savedStateValue = field != null ? (ConceptValue)field.getFieldValue() : null;
                                    }
                                    else
                                    {
                                        savedStateValue = (ConceptValue)savedStateProperty.getRecordValue();
                                    }

                                    // If the saved state value was found, set the local concept to the saved state concept.
                                    if (savedStateValue != null)
                                    {
                                        localConceptValue.setConceptId(savedStateValue.getConceptId(), false);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Finally reset the saved state of the record.
            dataFile.saveState();
        }
    }

    private class DataRecordChangeListener implements T8DataRecordChangeListener
    {
        @Override
        public void recordValueChanged(T8RecordValueChangedEvent event)
        {
            handleDataRecordChanged(event);
        }

        @Override
        public void recordPropertyAdded(T8RecordPropertyAddedEvent event)
        {
            handleDataRecordChanged(event);
        }

        @Override
        public void recordPropertyValueChanged(T8RecordPropertyValueChangedEvent event)
        {
            handleDataRecordChanged(event);
        }

        @Override
        public void recordValueStructureChanged(T8RecordValueStructureChangedEvent event)
        {
            handleDataRecordChanged(event);
        }

        @Override
        public void dataRecordChanged(T8DataRecordChangedEvent event)
        {
            handleDataRecordChanged(event);
        }

        private void handleDataRecordChanged(T8DataRecordChangedEvent event)
        {
            if ((!event.isAdjusting()) && (!programmaticRefresh))
            {
                String recordID;

                // Refresh the editor to reflect the model changes.
                LOGGER.log("Record Event: " + event);
                refreshEditors();
                recordID = event.getDataRecord().getID();
                terminologyGenerator.regenerateDataRecordTerminology(recordID);
            }
        }
    }
}
