package com.pilog.t8.ui.datarecordeditor;

import java.awt.Color;
import javax.swing.ImageIcon;

/**
 * @author Bouwer du Preez
 */
public class Constants
{
    public static final Color FOCUS_COLOR_LIGHT = new Color(230, 230, 255);
    public static final Color FOCUS_COLOR_DARK = new Color(180, 180, 255);
    public static final Color BACKGROUND_COLOR = Color.WHITE;
    
    public static final String LANGUAGE_ID = "CC0092ADF415443DB36744B5D9EF96E9"; // English used as default language.

    public static final String ACTION_SAVE = "SAVE";
    public static final boolean TEST_MODE = false;

    public static final ImageIcon CUT_ICON = new javax.swing.ImageIcon(Constants.class.getResource("/com/pilog/t8/ui/datarecordeditor/icons/cutIcon.png"));
    public static final ImageIcon CLASS_CHANGE_ICON = new javax.swing.ImageIcon(Constants.class.getResource("/com/pilog/t8/ui/datarecordeditor/icons/pageEditIcon.png"));

    public static final int MAX_DESCRIPTION_LENGTH = 100;

    public static final String DOC_CHANGE_PREFIX = "DOC_CHANGE";
    public static final String DOC_CHANGE_PROPERTY_VALUE_CHANGED = "DOC_CHANGE_PROPERTY_VALUE_CHANGED";
}
