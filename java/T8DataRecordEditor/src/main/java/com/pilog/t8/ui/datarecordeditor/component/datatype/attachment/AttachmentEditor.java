package com.pilog.t8.ui.datarecordeditor.component.datatype.attachment;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarequirement.value.AttachmentListRequirement;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.Constants;
import com.pilog.t8.ui.datarecordeditor.component.datatype.DefaultRecordValueEditor;
import com.pilog.t8.ui.dialog.info.T8InformationDialog;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.strings.StringUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class AttachmentEditor extends DefaultRecordValueEditor
{
    private final ArrayList<AttachmentFileEditor> attachmentLinkEditors;
    private final Set<String> fileExtensionRestrictions;
    private final AttachmentList attachmentList;
    private final AttachmentListRequirement attachmentListRequirement;
    private long fileSizeRestriction = 52428800; // The default restriction is 50 MB.
    private boolean allowDuplicateFilenames;
    private int maximumAttachments;
    private int gridY;

    private static final T8Logger logger = T8Log.getLogger(AttachmentEditor.class);
    private static File LAST_SELECTED_FOLDER = null;

    public AttachmentEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        gridY = 0;
        fileExtensionRestrictions = new HashSet<>();
        attachmentLinkEditors = new ArrayList<>();
        attachmentList = (AttachmentList)recordValue;
        attachmentListRequirement = (AttachmentListRequirement)valueRequirement;
        initComponents();
        fetchRequirementAttributes();
        rebuildContent();
    }

    private void fetchRequirementAttributes()
    {
        long fileSizeRestrictionAttributeValue;
        String fileTypeRestrictionAttributeValue;

        allowDuplicateFilenames = attachmentListRequirement.isAllowDuplicateFilenames();
        maximumAttachments = attachmentListRequirement.getMaximumAttachmentCount();

        // Set default values for settings that may not be null.
        if (maximumAttachments < 0) maximumAttachments = 1000000;

        // Get the attachment file requirement.
        fileSizeRestrictionAttributeValue = attachmentListRequirement.getFileSizeRestriction();
        fileTypeRestrictionAttributeValue = attachmentListRequirement.getFileTypeRestriction();

        if (fileSizeRestrictionAttributeValue > 0)
        {
            fileSizeRestriction = fileSizeRestrictionAttributeValue;
        }

        fileExtensionRestrictions.clear();
        if (!Strings.isNullOrEmpty(fileTypeRestrictionAttributeValue))
        {
            String[] extensions;

            extensions = fileTypeRestrictionAttributeValue.split(",");
            for (String extension : extensions)
            {
                String trimmedExtension;

                trimmedExtension = extension.trim().toUpperCase();
                if (trimmedExtension.startsWith(".")) trimmedExtension = trimmedExtension.substring(1);
                fileExtensionRestrictions.add(trimmedExtension);
            }
        }
    }

    private void rebuildContent()
    {
        // Clear the UI.
        jPanelContent.removeAll();
        attachmentLinkEditors.clear();
        gridY = 0;

        // Add all of the attachment link editors.
        for (String attachmentID : attachmentList.getAttachmentIds())
        {
            addAttachmentLinkEditor(attachmentID);
        }

        // Validate the container.
        jPanelContent.revalidate();
        jPanelContent.repaint();
    }

    private void refreshAttachmentAdditionButtonState()
    {
        int attachmentCount;
        boolean enabled;

        // Get the current attachment count.
        attachmentCount = attachmentList.getAttachmentCount();

        // Set the visibility of the add button.
        enabled = (attachmentCount < maximumAttachments);
        jButtonAddNew.setVisible(insertEnabled && enabled && editableAccess);
   }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void refreshAccess()
    {
        // Call the super implementation.
        super.refreshAccess();

        // Hide the 'Add' button completely if the value is not editableAccess.
        refreshAttachmentAdditionButtonState();
    }

    @Override
    public final void refreshEditor()
    {
        // Execute default refreshEditor behavior.
        super.refreshEditor();

        // Refresh button state.
        refreshAttachmentAdditionButtonState();
    }

    @Override
    public boolean commitChanges()
    {
        boolean result;

        // Commit changes on each of the attachment link editors.
        result = true;
        for (AttachmentFileEditor linkEditor : attachmentLinkEditors)
        {
            if (!linkEditor.commitChanges()) result = false;
        }

        // Return the value.
        return result;
    }

    /**
     * This method checks all the currently added attachment links for the
     * specified filename and returns a boolean value to indicate whether or not
     * the filename was found.
     * @param filename The filename to check for (case insensitive).
     * @return A boolean value indicating whether or not the specified filename
     * was found among the added attachments.
     */
    private boolean containsAttachmentFilename(String filename)
    {
        for (AttachmentFileEditor linkEditor : attachmentLinkEditors)
        {
            T8FileDetails attachmentDetails;

            attachmentDetails = ((AttachmentFileEditor)linkEditor).getAttachmentDetails();
            if (attachmentDetails != null)
            {
                if (filename.equalsIgnoreCase(attachmentDetails.getFileName())) return true;
            }
        }

        return false;
    }

    private void addAttachmentLink()
    {
        try
        {
            String localFilePath;

            // Get the target file path that identifies the file that will be uploaded.
            localFilePath = BasicFileChooser.getLoadFilePath(SwingUtilities.getWindowAncestor(this), null, "All Files", LAST_SELECTED_FOLDER, BasicFileChooser.FileSelectionMode.FILES);
            if (localFilePath != null)
            {
                File localFile;
                String fileExtension;

                // Update the last selected folder path.
                localFile = new File(localFilePath);
                LAST_SELECTED_FOLDER = localFile;
                if (LAST_SELECTED_FOLDER.getParentFile() != null) LAST_SELECTED_FOLDER = LAST_SELECTED_FOLDER.getParentFile();

                // Get the local file extension.
                if (localFilePath.indexOf('.') > -1)
                {
                    fileExtension = localFilePath.substring(localFilePath.lastIndexOf('.') + 1);
                }
                else fileExtension = null;

                // Check that the local file is valid, and then start the upload.
                if (!localFile.exists())
                {
                    JOptionPane.showMessageDialog(this, translate("The selected file could not be found: " + localFilePath), translate("Upload Error"), JOptionPane.ERROR_MESSAGE);
                }
                else if ((fileSizeRestriction > 0) && (localFile.length() > fileSizeRestriction))
                {
                    JOptionPane.showMessageDialog(this, translate("The selected file is too large.") + "\n" + translate("A maximum file size of ") + StringUtilities.getFileSizeString(fileSizeRestriction) + translate(" is allowed."), translate("Upload Error"), JOptionPane.ERROR_MESSAGE);
                }
                else if ((fileExtensionRestrictions.size() > 0) && (!fileExtensionRestrictions.contains(fileExtension != null ? fileExtension.toUpperCase() : null)))
                {
                    T8InformationDialog.showErrorMessage(context, SwingUtilities.getWindowAncestor(this), translate("Upload Error"), translate("The selected file extension '") + fileExtension + translate("' is not allowed."), translate("Permitted file types are: ") + fileExtensionRestrictions);
                }
                else if ((!allowDuplicateFilenames) && (containsAttachmentFilename(localFile.getName())))
                {
                    JOptionPane.showMessageDialog(this, translate("Duplicate Filename '") + localFile.getName() + translate("' not allowed."), translate("Upload Error"), JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    AttachmentFileEditor attachmentLinkComponent;
                    String newAttachmentId;

                    // Create the attachment link component.
                    dataRecord.setAdjusting(true);
                    newAttachmentId = T8IdentifierUtilities.createNewGUID();
                    attachmentList.addAttachment(newAttachmentId);
                    dataRecord.addAttachmentDetails(newAttachmentId, new T8AttachmentDetails(newAttachmentId, null, null, localFile.getName(), localFile.getName(), 0, null, null)); // Temporary file details until the file has been uploaded asynchronously.
                    attachmentLinkComponent = addAttachmentLinkEditor(newAttachmentId);
                    dataRecord.setAdjusting(false, true);

                    // Start the attachment operation.
                    attachmentLinkComponent.uploadAttachment(localFilePath);
                }
            }
        }
        catch (Exception e)
        {
            logger.log("Exception while adding attchmet link editor.", e);
            JOptionPane.showMessageDialog(this, "The selected file could not be uploaded successfully.  Please try again.", "Upload Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private AttachmentFileEditor addAttachmentLinkEditor(String newAttachmentID)
    {
        GridBagConstraints gridBagConstraints;
        AttachmentFileEditor attachmentLinkEditor;
        Component removeButton;

        // Create the attachment link editor.
        attachmentLinkEditor = new AttachmentFileEditor(container, dataRecord, newAttachmentID);
        attachmentLinkEditors.add(attachmentLinkEditor);

        removeButton = buildRemoveButton(attachmentLinkEditor);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(0, 0, 0, 0);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = gridY;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
        jPanelContent.add(removeButton, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = gridY++;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelContent.add(attachmentLinkEditor, gridBagConstraints);

        jPanelContent.invalidate();
        jPanelContent.revalidate();

        return attachmentLinkEditor;
    }

    private void removeAttachmentLink(Component removeComponent, AttachmentFileEditor attachmentLinkComponent)
    {
        // Remove the editor from the child editors colleciton.
        attachmentLinkEditors.remove(attachmentLinkComponent);

        // Remove the value from the underlying Data Record document.
        attachmentList.removeAttachment(attachmentLinkComponent.getAttachmentID());

        // Remove the attachment link Editor from the UI.
        jPanelContent.remove(removeComponent);
        jPanelContent.remove(attachmentLinkComponent);
        jPanelContent.invalidate();
        jPanelContent.revalidate();
    }

    private Component buildRemoveButton(final AttachmentFileEditor attachmentLinkComponent)
    {
        final JToolBar buttonBar;
        final JButton jButtonRemove;

        buttonBar = new JToolBar();
        buttonBar.setFloatable(false);

        jButtonRemove = new javax.swing.JButton();
        jButtonRemove.setVisible(editableAccess);
        jButtonRemove.setText(translate("Remove"));
        jButtonRemove.setIcon(Constants.CUT_ICON);
        jButtonRemove.setToolTipText(translate("Remove Attachment"));
        jButtonRemove.setHorizontalAlignment(JButton.TRAILING);
        jButtonRemove.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonRemove.addActionListener((evt) -> removeAttachmentLink(buttonBar, attachmentLinkComponent));
        buttonBar.add(jButtonRemove);
        return buttonBar;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jToolBarReferenceControls = new javax.swing.JToolBar();
        jButtonAddNew = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelContent, gridBagConstraints);

        jToolBarReferenceControls.setFloatable(false);
        jToolBarReferenceControls.setRollover(true);

        jButtonAddNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/addAttachmentIcon.png"))); // NOI18N
        jButtonAddNew.setText(translate("Add Attachment..."));
        jButtonAddNew.setFocusable(false);
        jButtonAddNew.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddNew.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddNewActionPerformed(evt);
            }
        });
        jToolBarReferenceControls.add(jButtonAddNew);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarReferenceControls, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddNewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddNewActionPerformed
    {//GEN-HEADEREND:event_jButtonAddNewActionPerformed
        addAttachmentLink();
    }//GEN-LAST:event_jButtonAddNewActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddNew;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JToolBar jToolBarReferenceControls;
    // End of variables declaration//GEN-END:variables
}
