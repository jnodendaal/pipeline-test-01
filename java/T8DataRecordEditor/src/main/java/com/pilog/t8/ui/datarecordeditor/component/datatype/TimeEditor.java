package com.pilog.t8.ui.datarecordeditor.component.datatype;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.TimeValue;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.GridBagConstraints;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

/**
 * @author Bouwer du Preez
 */
public class TimeEditor extends DefaultRecordValueEditor
{
    private JSpinner timePicker;
    private DateFormat timeFormat;

    private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";

    public TimeEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        initComponents();
        initFormat();
        initTimePicker();
        refreshEditor();
    }

    private void initFormat()
    {
        String formatString;

        formatString = (String)valueRequirement.getAttribute(RequirementAttribute.DATE_TIME_FORMAT_PATTERN.toString());
        if (formatString != null)
        {
            try
            {
                timeFormat = new SimpleDateFormat(formatString);
            }
            catch (IllegalArgumentException e)
            {
                T8Log.log("Invalid time format: " + formatString, e);
                timeFormat = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
            }
        }
        else timeFormat = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
    }

    private void initTimePicker()
    {
        GridBagConstraints gridBagConstraints;
        JSpinner.DateEditor timeEditor;

        timePicker = new JSpinner(new TimeSpinnerModel());
        timeEditor = new JSpinner.DateEditor(timePicker, DEFAULT_TIME_FORMAT);
        timePicker.setEditor(timeEditor);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(timePicker, gridBagConstraints);
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();

        // Set time picker editability.
        timePicker.setEnabled(editableAccess);
    }

    @Override
    public void refreshEditor()
    {
        String valueString;

        // Execute the default behavior.
        super.refreshEditor();

        // Get the date value.
        valueString = recordValue.getValue();

        // Set the data on the UI.
        if (Strings.isNullOrEmpty(valueString))
        {
            timePicker.setValue(null);
        }
        else
        {
            try
            {
                Long milliseconds;

                milliseconds = Long.parseLong(valueString);
                timePicker.setValue(new Date(milliseconds));
            }
            catch (Exception e)
            {
                T8Log.log("Invalid Time Millisecond Value: " + valueString, e);
                timePicker.setValue(null);
            }
        }
    }

    @Override
    public boolean commitChanges()
    {
        try
        {
            Date timeValue;

            timePicker.commitEdit();
            timeValue = (Date)timePicker.getValue();
            ((TimeValue)recordValue).setTime(timeValue);
            return true;
        }
        catch (Exception e)
        {
            T8Log.log("Exception while commiting Data Editor changes.", e);
            return false;
        }
    }

    @Override
    public void addMouseListener(MouseListener listener)
    {
        super.addMouseListener(listener);
        timePicker.addMouseListener(listener);
    }

    @Override
    public void removeMouseListener(MouseListener listener)
    {
        super.removeMouseListener(listener);
        timePicker.removeMouseListener(listener);
    }

    private class TimeSpinnerModel extends SpinnerDateModel
    {
        private boolean nullValue;

        @Override
        public void setValue(Object value)
        {
            if (value != null)
            {
                nullValue = false;
                super.setValue(value);
            }
            else nullValue = true;
        }

        @Override
        public Object getValue()
        {
            if (!nullValue)
            {
                return super.getValue();
            }
            else
            {
                return null;
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
