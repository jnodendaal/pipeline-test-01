package com.pilog.t8.ui.datarecordeditor.component.datatype;

import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.ui.datarecordeditor.ValueAutoCompleter;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.AutoCompletableEditor;
import com.pilog.t8.ui.datarecordeditor.T8DataRecordEditor;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.components.textfield.IconTextField;
import com.pilog.t8.utilities.format.DefaultFormatter.Case;
import com.pilog.t8.utilities.format.EmptyFormat;
import com.pilog.t8.utilities.format.RegexFormatter;
import com.pilog.t8.utilities.format.RegexMaskFormatter;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.util.Objects;
import java.util.regex.PatternSyntaxException;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JFormattedTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

/**
 * @author Bouwer du Preez
 */
public class StringEditor extends DefaultRecordValueEditor implements AutoCompletableEditor
{
    private static final T8Logger LOGGER = T8Log.getLogger(StringEditor.class);

    private final T8DataRecordClientApi recApi;
    private String formatPattern;
    private String maskPattern;
    private Integer lengthRestriction;
    private String textCase;
    private boolean prescribed;
    private BigDecimal minimumNumericValue;
    private BigDecimal maximumNumericValue;
    private IconTextField jFormattedTextFieldValue;
    private final AutoCompletableEditor autoCompleteTarget; // The parent editor to which auto-completed values will be set when entered into the text field.  If null, the value will simply be set on the text field editor.
    private ValueAutoCompleter autoCompleter;
    private boolean commitTrimmed;

    public StringEditor(DataRecordEditorContainer container, RecordValue recordValue, AutoCompletableEditor autCompleteTarget)
    {
        this(container, recordValue, autCompleteTarget, false);
    }

    // TODO: GBO - To be reviewed by BDP as standard behavior. Currently resolved to be used only where necessary
    public StringEditor(DataRecordEditorContainer container, RecordValue recordValue, AutoCompletableEditor autCompleteTarget, boolean commitTrimmed)
    {
        super(container, recordValue);
        initComponents();
        this.autoCompleteTarget = autCompleteTarget;
        this.recApi = container.getAccessContext().getClientContext().getConfigurationManager().getAPI(context, T8DataRecordClientApi.API_IDENTIFIER);
        this.commitTrimmed = commitTrimmed;
        constructTextField();
        extractRequirementAttributes();
        applyFormatter();
        unbindKeys();
        refreshEditor();

        // Apply auto-completion if this value is prescribedValue.
        applyAutoCompletion();
    }

    private void constructTextField()
    {
        GridBagConstraints gridBagConstraints;

        // Create the text field.
        jFormattedTextFieldValue = new IconTextField();
        jFormattedTextFieldValue.setIconAlignment(IconTextField.IconAlignment.RIGHT);
        jFormattedTextFieldValue.addFocusListener(new java.awt.event.FocusAdapter()
        {
            @Override
            public void focusLost(FocusEvent evt)
            {
                commitChanges();
            }
        });

        // Add the text field to the UI layout.
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jFormattedTextFieldValue, gridBagConstraints);

        // Add the property change listener to the text field so we can react to value changes.
        jFormattedTextFieldValue.addPropertyChangeListener(new TextFieldPropertyChangeListener());
    }

    private void unbindKeys()
    {
        InputMap inputMap = jFormattedTextFieldValue.getInputMap();

        // Unbind the enter key to ensure that the property component higher up in the component tree can process the event.
        inputMap.put(KeyStroke.getKeyStroke("ENTER"), "none");
    }

    private void extractRequirementAttributes()
    {
        BigDecimal lengthRestrictionValue;

        lengthRestrictionValue = (BigDecimal)valueRequirement.getAttribute(RequirementAttribute.MAXIMUM_STRING_LENGTH.toString());
        lengthRestriction = lengthRestrictionValue != null ? lengthRestrictionValue.intValue() : 0;
        minimumNumericValue = (BigDecimal)valueRequirement.getAttribute(RequirementAttribute.MINIMUM_NUMERIC_VALUE.toString());
        maximumNumericValue = (BigDecimal)valueRequirement.getAttribute(RequirementAttribute.MAXIMUM_NUMERIC_VALUE.toString());
        formatPattern = (String)valueRequirement.getAttribute(RequirementAttribute.FORMAT_PATTERN.toString());
        maskPattern = (String)valueRequirement.getAttribute(RequirementAttribute.FORMAT_MASK.toString());
        textCase = (String)valueRequirement.getAttribute(RequirementAttribute.CASE.toString());
        prescribed = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));

        // Ensure that the primitive values are valid.
        if (lengthRestriction < 0) lengthRestriction = 0;
    }

    private void applyAutoCompletion()
    {
        // If we have a non-null auto-complete target, use it, otherwise use this text field as the auto-complete target.
        if (autoCompleteTarget != null)
        {
            autoCompleter = new ValueAutoCompleter(autoCompleteTarget, jFormattedTextFieldValue, container.getTerminologyProvider(), recApi);
            autoCompleter.setUpdateDelay(500); // The number of milliseconds to delay the update of auto-completion list while the user is changing data.
        }
        else if (prescribed)
        {
            autoCompleter = new ValueAutoCompleter(this, jFormattedTextFieldValue, container.getTerminologyProvider(), recApi);
            autoCompleter.setUpdateDelay(500); // The number of milliseconds to delay the update of auto-completion list while the user is changing data.
        }
    }

    @Override
    public DataRecord getContextRecord()
    {
        return dataRecord.getDataFile();
    }

    @Override
    public RecordValue getTargetValue()
    {
        return recordValue;
    }

    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();

        // Set editability on the text field.
        jFormattedTextFieldValue.setEditable(editableAccess);
        if (autoCompleter != null) autoCompleter.setEnabled(editableAccess);
    }

    @Override
    public void refreshEditor()
    {
        // Execute the default refreshEditor behavior.
        super.refreshEditor();

        jFormattedTextFieldValue.setText(recordValue.getValue());
        jFormattedTextFieldValue.setIcon(recordValue.isApproved() ? T8DataRecordEditor.APPROVED_VALUE_ICON : null);
    }

    @Override
    public boolean commitChanges()
    {
        if (commitTrimmed)
        {
            recordValue.setValue(Strings.trimToNull(getTextValue(recordValue.getValue())));
        }
        else
        {
            recordValue.setValue(getTextValue(recordValue.getValue()));
        }

        return true;
    }

    /**
     * Returns the last valid value entered in the formatted text field.  If the
     * current value entered is valid, it is returned.  If not, then the last
     * value set on the formatted text field (Old value) is returned.
     * @return The last valid value entered in the formatted text field.
     */
    private String getTextValue(String oldValue)
    {
        Object editorValue;
        String editorStringValue;

        if (jFormattedTextFieldValue.isEditValid())
        {
            try
            {
                // First commit the changes on the editor.
                jFormattedTextFieldValue.commitEdit();

                // Now return the text value.
                editorValue = jFormattedTextFieldValue.getValue();
                editorStringValue = editorValue != null ? editorValue.toString() : null;
                return Strings.isNullOrEmpty(editorStringValue) ? null : editorStringValue;
            }
            catch (ParseException e) // We don't do anything with the exception.  Its existence indicates that the new value entered in the text field is not valid.
            {
                // Value could not be committed, so just set the return the old value.
                return oldValue;
            }
        }
        else return Strings.trimToNull(jFormattedTextFieldValue.getText());
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void addMouseListener(MouseListener listener)
    {
        super.addMouseListener(listener);
        jFormattedTextFieldValue.addMouseListener(listener);
    }

    @Override
    public void removeMouseListener(MouseListener listener)
    {
        super.removeMouseListener(listener);
        jFormattedTextFieldValue.removeMouseListener(listener);
    }

    private void applyFormatter()
    {
        switch (valueRequirement.getRequirementType())
        {
            case STRING_TYPE:
                if ((!Strings.isNullOrEmpty(formatPattern)) && (!Strings.isNullOrEmpty(maskPattern)))
                {
                    applyRegexMaskFormatter(jFormattedTextFieldValue, maskPattern, formatPattern);
                }
                else if (!Strings.isNullOrEmpty(formatPattern))
                {
                    applyRegularExpressionFormatter(jFormattedTextFieldValue, formatPattern, lengthRestriction, textCase);
                }
                else if (!Strings.isNullOrEmpty(maskPattern))
                {
                    applyRegexMaskFormatter(jFormattedTextFieldValue, maskPattern, null);
                }
                else
                {
                    applyRegularExpressionFormatter(jFormattedTextFieldValue, ".*", lengthRestriction, textCase);
                }
                break;
            case INTEGER_TYPE:
                if (!Strings.isNullOrEmpty(formatPattern))
                {
                    applyDecimalFormatter(jFormattedTextFieldValue, formatPattern, minimumNumericValue, maximumNumericValue);
                }
                else
                {
                    // No explicit Integer Format is set, so we set the default (any postive integer).
                    applyDecimalFormatter(jFormattedTextFieldValue, RequirementType.DEFAULT_POSITIVE_INTEGER_TYPE_FORMAT, minimumNumericValue, maximumNumericValue);
                }
                break;
            case REAL_TYPE:
                if (!Strings.isNullOrEmpty(formatPattern))
                {
                    applyDecimalFormatter(jFormattedTextFieldValue, formatPattern, minimumNumericValue, maximumNumericValue);
                }
                else
                {
                    // No explicit Real Format is set, so we set the default (any decimal value).
                    applyDecimalFormatter(jFormattedTextFieldValue, RequirementType.DEFAULT_REAL_TYPE_FORMAT, null, null);
                }
                break;
            case NUMBER:
            case LOWER_BOUND_NUMBER:
            case UPPER_BOUND_NUMBER:
                if ((!Strings.isNullOrEmpty(formatPattern)) && (!Strings.isNullOrEmpty(maskPattern)))
                {
                    applyRegexMaskFormatter(jFormattedTextFieldValue, maskPattern, formatPattern);
                }
                else if (!Strings.isNullOrEmpty(formatPattern))
                {
                    applyRegularExpressionFormatter(jFormattedTextFieldValue, formatPattern, lengthRestriction, textCase);
                }
                else if (!Strings.isNullOrEmpty(maskPattern))
                {
                    applyRegexMaskFormatter(jFormattedTextFieldValue, maskPattern, null);
                }
                else
                {
                    applyRegularExpressionFormatter(jFormattedTextFieldValue, ".*", lengthRestriction, textCase);
                }
                break;
            default:
                break;
        }
    }

    private boolean applyRegularExpressionFormatter(JFormattedTextField textField, String pattern, int lengthRestriction, String textCase)
    {
        try
        {
            RegexFormatter formatter;

            formatter = new RegexFormatter(pattern, lengthRestriction, textCase != null ? Case.valueOf(textCase) : Case.ANY);
            formatter.setAllowsInvalid(true);
            formatter.setOverwriteMode(false);
            formatter.setCommitsOnValidEdit(false);

            textField.setFocusLostBehavior(JFormattedTextField.COMMIT);
            textField.setFormatterFactory(new DefaultFormatterFactory(formatter));
            return true;
        }
        catch (PatternSyntaxException e)
        {
            LOGGER.log("While applying pattern '" + pattern + "' to component.", e);
            return false;
        }
    }

    private boolean applyRegexMaskFormatter(JFormattedTextField textField, String maskString, String patternString)
    {
        try
        {
            RegexMaskFormatter formatter;

            formatter = new RegexMaskFormatter(maskString, patternString);
            formatter.setPlaceholderCharacter('ˍ');
            formatter.setAllowBlankField(true);
            formatter.setAllowsInvalid(false);
            formatter.setOverwriteMode(true);
            formatter.setCommitsOnValidEdit(true); // This is important to enforce the mask.
            formatter.setValueContainsLiteralCharacters(true);

            textField.setFocusLostBehavior(JFormattedTextField.COMMIT);
            textField.setFormatterFactory(new DefaultFormatterFactory(formatter));
            return true;
        }
        catch (ParseException | PatternSyntaxException e)
        {
            LOGGER.log("While applying mask '" + maskString + "' and pattern '" + patternString + "' to component.", e);
            return false;
        }
    }

    private boolean applyDecimalFormatter(JFormattedTextField textField, String pattern, BigDecimal minimumValue, BigDecimal maximumValue)
    {
        try
        {
            NumberFormatter formatter;
            DecimalFormat format;

            // Create the format.
            format = new DecimalFormat(pattern);
            format.setParseBigDecimal(true);

            // Create the formatter.
            formatter = new BigDecimalFormatter(this.jFormattedTextFieldValue);
            formatter.setFormat(new EmptyFormat(format));
            formatter.setAllowsInvalid(true);
            formatter.setOverwriteMode(false);
            formatter.setCommitsOnValidEdit(false);
            formatter.setValueClass(BigDecimal.class);
            if (minimumValue != null) formatter.setMinimum(minimumValue);
            if (maximumValue != null) formatter.setMaximum(maximumValue);

            // Apply the formatter to the text field.
            textField.setFocusLostBehavior(JFormattedTextField.COMMIT);
            textField.setFormatterFactory(new DefaultFormatterFactory(formatter, formatter, formatter));
            return true;
        }
        catch (Exception e)
        {
            LOGGER.log("While applying pattern '" + pattern + "' to component.", e);
            return false;
        }
    }

    private class TextFieldPropertyChangeListener implements PropertyChangeListener
    {
        private boolean committingChanges = false;

        @Override
        public void propertyChange(PropertyChangeEvent event)
        {
            if (editableAccess)
            {
                if ("invalidValue".equals(event.getPropertyName()))
                {
                    boolean invalidValue;

                    invalidValue = (Boolean)event.getNewValue();
                    jFormattedTextFieldValue.setBackground(!invalidValue ? Color.WHITE : LAFConstants.INVALID_PINK);
                }
                else if ("editValid".equals(event.getPropertyName()))
                {
                    boolean validEdit;

                    validEdit = (Boolean) event.getNewValue();
                    jFormattedTextFieldValue.setBackground(validEdit ? Color.WHITE : LAFConstants.INVALID_PINK);
                }
                else if ("value".equals(event.getPropertyName()))
                {
                    if (!committingChanges)
                    {
                        Object oldValue;
                        Object newValue;

                        oldValue = event.getOldValue();
                        newValue = event.getNewValue();
                        if (!Objects.equals(oldValue, newValue))
                        {
                            committingChanges = true;
                            commitChanges();
                            committingChanges = false;
                        }
                    }
                }
            }
        }
    }

    @Override
    public void addNotify()
    {
        super.addNotify();
    }

    /**
     * This method along with the overridden addNotify() simply delegates key
     * strokes from the panel (this class) to the actual editor component so
     * that text is properly entered into the text editor when this class is
     * used in a table cell.
     * @return
     */
    @Override
    public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed)
    {
        InputMap map = jFormattedTextFieldValue.getInputMap(condition);
        ActionMap am = jFormattedTextFieldValue.getActionMap();

        if (map != null && am != null && isEnabled())
        {
            Object binding = map.get(ks);
            Action action = (binding == null) ? null : am.get(binding);
            if (action != null)
            {
                return SwingUtilities.notifyAction(action, ks, e, jFormattedTextFieldValue, e.getModifiers());
            }
        }

        return false;
    }

    @Override
    public void setAutoCompletedValue(RecordValue value)
    {
        jFormattedTextFieldValue.setText(value != null ? value.getValue() : null);
    }

    @Override
    public void setAutoCompletionState(boolean valid, Object currentSelection)
    {
    }

    /**
     * We override the default number formatter to force all values to be
     * formatted before being set on the formatted text field.
     */
    private static class BigDecimalFormatter extends NumberFormatter
    {
        private final JFormattedTextField formattedField;

        private BigDecimalFormatter(JFormattedTextField formattedField)
        {
            this.formattedField = formattedField;
        }

        @Override
        public Object stringToValue(String text) throws ParseException
        {
            BigDecimal value;
            Format f;

            // Get the format to use.
            f = getFormat();

            // Parse the text in the text field to get a BigDecimal value.
            // If the pattern is #0.00 and the user entered '1' then the
            // BigDecimal value returned from the format will at this stage be
            // '1'.
            value = (BigDecimal)f.parseObject(text);

            //Check that the value is within the specified range
            if (value != null)
            {
                //Work with the formatted value
                value = new BigDecimal(f.format(value));

                if ((getMinimum() != null) && (value.compareTo((BigDecimal)getMinimum()) < 0))
                {
                    formattedField.firePropertyChange("invalidValue", false, true);
                }
                else if ((getMaximum() != null) && (value.compareTo((BigDecimal)getMaximum()) > 0))
                {
                    formattedField.firePropertyChange("invalidValue", false, true);
                }
                else formattedField.firePropertyChange("invalidValue", true, false);

                return value;
            } else return null;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
