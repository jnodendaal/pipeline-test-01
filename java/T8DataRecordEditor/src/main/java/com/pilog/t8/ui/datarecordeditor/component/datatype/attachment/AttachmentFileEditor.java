package com.pilog.t8.ui.datarecordeditor.component.datatype.attachment;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Hour;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.utilities.components.desktop.DesktopUtil;
import java.awt.CardLayout;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class AttachmentFileEditor extends JPanel
{
    private final DataRecordProvider dataRecordProvider;
    private final T8FileManager fileManager;
    private final T8DataRecordClientApi recApi;
    private final T8Context context;
    private UploadCheckerThread uploadCheckerThread;
    private DownloadCheckerThread downloadCheckerThread;
    private final DataRecord parentRecord;
    private final String attachmentId;
    private T8AttachmentDetails attachmentDetails;

    private static final T8Logger LOGGER = T8Log.getLogger(AttachmentFileEditor.class);
    private static final String SYS_PROP_TEMP_DIR = "java.io.tmpdir";

    public AttachmentFileEditor(DataRecordEditorContainer container, DataRecord parentRecord, String attachmentID)
    {
        this.attachmentId = attachmentID;
        this.parentRecord = parentRecord;
        this.context = container.getAccessContext();
        this.dataRecordProvider = container.getDataRecordProvider();
        this.fileManager = context.getClientContext().getFileManager();
        this.recApi = context.getClientContext().getConfigurationManager().getAPI(context, T8DataRecordClientApi.API_IDENTIFIER);
        initComponents();

        // Set the text of the hyperlink.
        jXHyperlinkFileName.setText(attachmentID);

        // Refresh attachment details.
        refreshEditor();
    }

    public String getAttachmentID()
    {
        return attachmentId;
    }

    T8FileDetails getAttachmentDetails()
    {
        return attachmentDetails;
    }

    public boolean hasChanges()
    {
        return false;
    }

    public void refreshEditor()
    {
        // Get the attachment ID and try to fetch the attachment description from the container.
        attachmentDetails = dataRecordProvider.getAttachmentDetails(attachmentId);
        if (attachmentDetails != null)
        {
            // Set the attachment description on the hyperlink.
            jXHyperlinkFileName.setText(attachmentDetails.getOriginalFileName());
            jLabelStatusText.setText(attachmentDetails.getFileSize() + " bytes");
        }
        else
        {
            jXHyperlinkFileName.setText(attachmentId);
            jLabelStatusText.setText(null);
        }
    }

    public boolean commitChanges()
    {
        // Add the uploaded attachment details to the record so that it can be found when the record is persisted.
        parentRecord.addAttachmentDetails(attachmentId, attachmentDetails);
        return true;
    }

    private void showStatus(String statusView)
    {
        ((CardLayout)jPanelStatus.getLayout()).show(jPanelStatus, statusView);
    }

    void uploadAttachment(final String localFilePath)
    {
        // Get the target file path that identifies the file that will be uploaded.
        try
        {
            final File localFile;

            // Check that the local file exists.
            localFile = new File(localFilePath);
            if (localFile.exists())
            {
                // Set the attachment description on the hyperlink.
                jXHyperlinkFileName.setText(localFile.getName());

                // Start a new thread to handle the upload.
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            T8FileDetails uploadedFileDetails;
                            String uploadContextIid;

                            // Create the temporary file context for the upload operation (the context will be maintained for 1 hour).
                            uploadContextIid = T8IdentifierUtilities.createNewGUID();
                            fileManager.openFileContext(context, uploadContextIid, null, new T8Hour(1));

                            // Upload the file to a temporary location on the server.
                            uploadedFileDetails = fileManager.uploadFile(context, uploadContextIid, localFilePath, localFile.getName());
                            attachmentDetails = new T8AttachmentDetails(attachmentId, uploadedFileDetails, localFile.getName());

                            // Stop the upload checker and set the status text.
                            jLabelStatusText.setText(attachmentDetails.getFileSize() + " bytes uploaded successfully");
                            stopChecker(uploadCheckerThread);

                            // Commit the attachment details changes to reflect the uploaded file.
                            commitChanges();
                        }
                        catch (Exception e)
                        {
                            LOGGER.log("Exception while uploading attachment file.", e);
                            JOptionPane.showMessageDialog(AttachmentFileEditor.this, "The selected file could not be uploaded successfully.  Please try again.", "Upload Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }.start();

                // Show the progress of the upload.
                startUploadChecker(localFilePath);
            }
            else JOptionPane.showMessageDialog(this, "Local file not found: " + localFilePath, "Upload Error", JOptionPane.ERROR_MESSAGE);
        }
        catch (HeadlessException e)
        {
            LOGGER.log("Exception while uploading attachment file.", e);
            JOptionPane.showMessageDialog(this, "The selected file could not be uploaded successfully.  Please try again.", "Upload Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void downloadAttachment()
    {
        // Get the target file path to which the file will be downloaded.
        try
        {
            final String fileContextIid;
            final String remoteFileName;
            final String canonicalPath;

            T8FileDetails remoteAttachmentDetails;

            // Get the attachment details (we will only find it if the file we are trying to download has already been saved as an attachment to the document).
            remoteAttachmentDetails = dataRecordProvider.getAttachmentDetails(attachmentId);
            if (remoteAttachmentDetails != null)
            {
                // Export the file to a temporary context so that it can be downloaded.
                fileContextIid = T8IdentifierUtilities.createNewGUID();
                recApi.exportAttachment(attachmentId, fileContextIid, null, null, null);
                remoteFileName = remoteAttachmentDetails.getFileName();
            }
            else // The attachment details were not found in the cache, this means we are trying to download a file that was uploaded and has not been saved as an attachment yet.
            {
                fileContextIid = attachmentDetails.getContextIid();
                remoteFileName = attachmentDetails.getOriginalFileName();
                if (!fileManager.fileExists(context, fileContextIid, remoteFileName))
                {
                    JOptionPane.showMessageDialog(AttachmentFileEditor.this, "Temporary access to the requested attachment has expired.\nPlease reload this document and try again.", "Download Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }

            // Get the local file path to which the file will be downloaded.
            canonicalPath = new File(System.getProperty(SYS_PROP_TEMP_DIR), remoteFileName).getCanonicalPath();

            // Execute the downloader thread.
            new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        long fileSize;

                        fileSize = fileManager.downloadFile(context, fileContextIid, canonicalPath, remoteFileName);
                        jLabelStatusText.setText(fileSize + " bytes downloaded successfully");
                        stopChecker(downloadCheckerThread);

                        openFile(canonicalPath);
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while downloading attachment file.", e);
                        JOptionPane.showMessageDialog(AttachmentFileEditor.this, "The selected file could not be downloaded successfully.  Please try again.", "Download Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }.start();

            // Show the progress of the download.
            startDownloadChecker(canonicalPath);
        }
        catch (FileNotFoundException fnfe)
        {
            LOGGER.log("Exception while downloading attachment file.", fnfe);
            JOptionPane.showMessageDialog(this, "The selected file no longer exists on server. Please contact your administrator.", "Download Error", JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while downloading attachment file.", e);
            JOptionPane.showMessageDialog(this, "The selected file could not be downloaded successfully.  Please try again.", "Download Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void openFile(String filePath)
    {
        boolean fileOpened = false;

        try
        {
            fileOpened = DesktopUtil.openFile(filePath);
        }
        catch (IOException ioe)
        {
            LOGGER.log("Failed to open the file: " + filePath, ioe);
        }

        if (!fileOpened) JOptionPane.showMessageDialog(this, "The file could not be opened. This might be because no application is associated with this file.", "File Open Error", JOptionPane.ERROR_MESSAGE);
    }

    private void startUploadChecker(String localFilePath)
    {
        showStatus("PROGRESS");
        uploadCheckerThread = new UploadCheckerThread(localFilePath);
        uploadCheckerThread.start();
    }

    private void startDownloadChecker(String localFilePath)
    {
        showStatus("PROGRESS");
        downloadCheckerThread = new DownloadCheckerThread(localFilePath);
        downloadCheckerThread.start();
    }

    private void stopChecker(CheckerThread checker)
    {
        if (checker != null) checker.stopChecker();
        showStatus("TEXT");
    }

    private abstract class CheckerThread extends Thread
    {
        protected final String localFilePath;
        private boolean stopFlag;
        private int progress;

        private CheckerThread(String filePath)
        {
            localFilePath = filePath;
            stopFlag = false;
            progress = 0;
        }

        @Override
        public void run()
        {
            try
            {
                while ((progress < 100) && (!stopFlag))
                {
                    progress = getProgress();
                    SwingUtilities.invokeLater(() ->
                    {
                        jProgressBar.setValue(progress);
                    });

                    Thread.sleep(250);
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception in Attachment Upload Checker thread.", e);
            }
        }

        public void stopChecker()
        {
            stopFlag = true;
        }

        protected abstract int getProgress() throws Exception;
    }

    private class UploadCheckerThread extends CheckerThread
    {
        private UploadCheckerThread(String filePath)
        {
            super(filePath);
        }

        @Override
        protected int getProgress() throws Exception
        {
            return fileManager.getUploadOperationProgress(context, this.localFilePath);
        }
    }

    private class DownloadCheckerThread extends CheckerThread
    {
        private DownloadCheckerThread(String filePath)
        {
            super(filePath);
        }

        @Override
        protected int getProgress() throws Exception
        {
            return fileManager.getDownloadOperationProgress(context, this.localFilePath);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelFileName = new javax.swing.JLabel();
        jXHyperlinkFileName = new org.jdesktop.swingx.JXHyperlink();
        jLabelSize = new javax.swing.JLabel();
        jPanelStatus = new javax.swing.JPanel();
        jPanelStatusText = new javax.swing.JPanel();
        jLabelStatusText = new javax.swing.JLabel();
        jPanelStatusProgress = new javax.swing.JPanel();
        jProgressBar = new javax.swing.JProgressBar();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jLabelFileName.setText("File:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        add(jLabelFileName, gridBagConstraints);

        jXHyperlinkFileName.setText("Attachment Hyperlink");
        jXHyperlinkFileName.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXHyperlinkFileNameActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(jXHyperlinkFileName, gridBagConstraints);

        jLabelSize.setText("Size:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        add(jLabelSize, gridBagConstraints);

        jPanelStatus.setLayout(new java.awt.CardLayout());

        jPanelStatusText.setLayout(new java.awt.GridBagLayout());

        jLabelStatusText.setText("File Size");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelStatusText.add(jLabelStatusText, gridBagConstraints);

        jPanelStatus.add(jPanelStatusText, "TEXT");

        jPanelStatusProgress.setLayout(new java.awt.GridBagLayout());

        jProgressBar.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelStatusProgress.add(jProgressBar, gridBagConstraints);

        jPanelStatus.add(jPanelStatusProgress, "PROGRESS");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        add(jPanelStatus, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jXHyperlinkFileNameActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXHyperlinkFileNameActionPerformed
    {//GEN-HEADEREND:event_jXHyperlinkFileNameActionPerformed
        downloadAttachment();
    }//GEN-LAST:event_jXHyperlinkFileNameActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelFileName;
    private javax.swing.JLabel jLabelSize;
    private javax.swing.JLabel jLabelStatusText;
    private javax.swing.JPanel jPanelStatus;
    private javax.swing.JPanel jPanelStatusProgress;
    private javax.swing.JPanel jPanelStatusText;
    private javax.swing.JProgressBar jProgressBar;
    private org.jdesktop.swingx.JXHyperlink jXHyperlinkFileName;
    // End of variables declaration//GEN-END:variables
}
