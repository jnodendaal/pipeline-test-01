package com.pilog.t8.ui.datarecordeditor.view.navigation;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.tree.DefaultTreeModel;

/**
 * @author Bouwer du Preez
 */
public class NavigationModel extends DefaultTreeModel
{
    public NavigationModel()
    {
        super(null);
    }

    public DataRecord getRootRecord()
    {
        NavigationTreeNode rootNode;

        rootNode = getRootNode();
        return rootNode != null ? rootNode.getRecord() : null;
    }

    public void setRootRecord(DataRecord rootRecord)
    {
        this.setRoot(rootRecord != null ? new NavigationTreeNode(rootRecord) : null);
    }

    public String getRootRecordID()
    {
        DataRecord rootRecord;

        rootRecord = getRootRecord();
        return rootRecord != null ? rootRecord.getID() : null;
    }

    private NavigationTreeNode getRootNode()
    {
        return (NavigationTreeNode)getRoot();
    }

    public void clear()
    {
        setRoot(null);
    }

    public void structureChanged()
    {
        nodeStructureChanged(root);
    }

    public List<NavigationTreeNode> getNodeChildren(NavigationTreeNode node)
    {
        List<NavigationTreeNode> children;

        children = new ArrayList<NavigationTreeNode>();
        for (int index = 0; index < node.getChildCount(); index++)
        {
            children.add((NavigationTreeNode)node.getChildAt(index));
        }

        return children;
    }

    public NavigationTreeNode findNode(String recordID)
    {
        NavigationTreeNode rootNode;

        // Without a root node we can't find any other nodes.
        rootNode = getRootNode();
        if (rootNode != null)
        {
            LinkedList<NavigationTreeNode> nodeList;

            nodeList = new LinkedList<NavigationTreeNode>();
            nodeList.add(this.getRootNode());
            while (nodeList.size() > 0)
            {
                NavigationTreeNode nextNode;

                nextNode = nodeList.pollFirst();
                if (recordID.equals(nextNode.getRecordID()))
                {
                    return nextNode;
                }
                else
                {
                    for (int childIndex = 0; childIndex < nextNode.getChildCount(); childIndex++)
                    {
                        nodeList.add((NavigationTreeNode)nextNode.getChildAt(childIndex));
                    }
                }
            }
        }

        // Node not found.
        return null;
    }

    public void addRecord(String parentRecordID, DataRecord newRecord)
    {
        NavigationTreeNode parentNode;

        parentNode = findNode(parentRecordID);
        if (parentNode != null)
        {
            parentNode.addSubRecord(newRecord);
            reload(parentNode);
        }
    }

    public void removeRecord(String recordID)
    {
        NavigationTreeNode node;

        node = findNode(recordID);
        if (node != null)
        {
            removeNodeFromParent(node);
        }
    }

    public String findParentRecordID(String recordID)
    {
        NavigationTreeNode node;

        node = findNode(recordID);
        if (node != null)
        {
            NavigationTreeNode parentNode;

            parentNode = (NavigationTreeNode)node.getParent();
            if (parentNode != null)
            {
                return (String) parentNode.getUserObject();
            }
            else return null;
        }
        else return null;
    }
}
