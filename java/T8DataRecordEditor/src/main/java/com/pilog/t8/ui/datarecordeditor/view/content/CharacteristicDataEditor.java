package com.pilog.t8.ui.datarecordeditor.view.content;

import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.SectionAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.component.sectiontype.T8DefaultSectionEditor;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * @author Bouwer du Preez
 */
public class CharacteristicDataEditor extends JPanel implements DataRecordEditor
{
    private final DataRecordEditorContainer container;
    private final DataRequirementInstance dataRequirementInstance;
    private final LinkedHashMap<String, DataRecordEditor> sectionEditors;
    private final DataRecordAccessLayer accessLayer;
    private final DataRecord dataRecord;

    public CharacteristicDataEditor(DataRecordEditorContainer container, DataRecord dataRecord)
    {
        this.dataRecord = dataRecord;
        this.dataRequirementInstance = dataRecord.getDataRequirementInstance();
        this.accessLayer = dataRecord.getAccessLayer();

        initComponents();

        this.container = container;
        this.sectionEditors = new LinkedHashMap<String, DataRecordEditor>();
        this.jPanelContent.setBackground(LAFConstants.CONTENT_PANEL_BG_COLOR);

        init();
    }

    private void init()
    {
        jPanelContent.removeAll();
        if (dataRequirementInstance != null)
        {
            ArrayList<SectionRequirement> sectionRequirements;
            Iterator<SectionRequirement> sectionIterator;
            DataRequirement dataRequirement;

            dataRequirement = dataRequirementInstance.getDataRequirement();
            sectionRequirements = dataRequirement.getSectionRequirements();
            sectionIterator = sectionRequirements.iterator();
            //for (SectionRequirement sectionRequirement : sectionRequirements)
            while (sectionIterator.hasNext())
            {
                SectionRequirement sectionRequirement;
                SectionAccessLayer sectionAccessLayer;

                // Only add the section if access (visibility) is set or if no access handler is specified.
                sectionRequirement = sectionIterator.next();
                sectionAccessLayer = accessLayer.getSectionAccessLayer(sectionRequirement.getConceptID());
                if (sectionAccessLayer.isVisible())
                {
                    T8DefaultSectionEditor editorComponent;
                    RecordSection recordSection;

                    recordSection = dataRecord.getRecordSection(sectionRequirement.getConceptID());
                    if (recordSection == null)
                    {
                        recordSection = new RecordSection(sectionRequirement);
                        dataRecord.setRecordSection(recordSection);
                    }

                    editorComponent = new T8DefaultSectionEditor(container, recordSection);
                    editorComponent.setSectionHeaderVisible(sectionRequirements.size() > 1);
                    sectionEditors.put(sectionRequirement.getConceptID(), editorComponent);
                    jPanelContent.add(editorComponent);
                    if (sectionIterator.hasNext()) jPanelContent.add((Box.createRigidArea(new Dimension(0, 10))));
                }
            }
        }

        jPanelContent.validate();
    }

    @Override
    public void refreshEditor()
    {
        for (DataRecordEditor sectionEditor : sectionEditors.values())
        {
            sectionEditor.refreshEditor();
        }
    }

    @Override
    public void refreshDataDependency()
    {
        for (DataRecordEditor sectionEditor : sectionEditors.values())
        {
            sectionEditor.refreshDataDependency();
        }
    }

    @Override
    public void refreshAccess()
    {
        for (DataRecordEditor sectionEditor : sectionEditors.values())
        {
            sectionEditor.refreshAccess();
        }
    }

    @Override
    public boolean hasChanges()
    {
        for (DataRecordEditor sectionEditor : sectionEditors.values())
        {
            if (sectionEditor.hasChanges()) return true;
        }

        return false;
    }

    @Override
    public DataRequirementInstance getRequirement()
    {
        return dataRequirementInstance;
    }

    @Override
    public DataRecordEditorContainer getDataRecordEditorContainer()
    {
        return container;
    }

    @Override
    public boolean commitChanges()
    {
        for (DataRecordEditor sectionEditor : sectionEditors.values())
        {
            if (!sectionEditor.commitChanges()) return false;
        }

        return true;
    }

    @Override
    public Value getValue()
    {
        return dataRecord;
    }

    @Override
    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    @Override
    public List<T8DataValidationError> getValidationErrors()
    {
        List<T8DataValidationError> requirementValidationErrors;

        requirementValidationErrors = new ArrayList<T8DataValidationError>();
        for (DataRecordEditor sectionEditor : sectionEditors.values())
        {
            List<T8DataValidationError> sectionValidationErrors;

            sectionValidationErrors = sectionEditor.getValidationErrors();
            if (sectionValidationErrors != null) requirementValidationErrors.addAll(sectionValidationErrors);
        }

        return requirementValidationErrors;
    }



    public void setPropertyFFTVisible(boolean visible)
    {
        for (DataRecordEditor sectionEditor : sectionEditors.values())
        {
            ((T8DefaultSectionEditor)sectionEditor).setPropertyFFTVisible(visible);
        }
    }

    @Override
    public void setValidationErrors(List<T8DataValidationError> validationErrors)
    {
        for (DataRecordEditor sectionEditor : sectionEditors.values())
        {
            ((T8DefaultSectionEditor)sectionEditor).setValidationErrors(validationErrors);
        }
    }

    @Override
    public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed)
    {
        return super.processKeyBinding(ks, e, condition, pressed);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));
        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new javax.swing.BoxLayout(jPanelContent, javax.swing.BoxLayout.Y_AXIS));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelContent, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelContent;
    // End of variables declaration//GEN-END:variables
}
