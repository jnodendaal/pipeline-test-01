package com.pilog.t8.ui.datarecordeditor.component.datatype.composite;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.layer.FieldAccessLayer;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.T8DataRequirementComment;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class FieldHeaderComponent extends javax.swing.JPanel
{
    private final FieldRequirement fieldRequirement;
    private final TerminologyProvider terminologyProvider;
    private final DataRecordEditor fieldValueEditor;
    private final FieldAccessLayer accessLayer;
    private List<T8DataValidationError> validationErrors;

    public FieldHeaderComponent(DataRecordEditorContainer container, RecordValue fieldValue, DataRecordEditor fieldValueEditor, boolean entryRequired)
    {
        initComponents();
        this.fieldRequirement = (FieldRequirement)fieldValue.getValueRequirement();
        this.accessLayer = fieldValue.getParentDataRecord().getAccessLayer().getFieldAccessLayer(fieldValue.getPropertyID(), fieldValue.getFieldID());
        this.terminologyProvider = container.getTerminologyProvider();
        this.fieldValueEditor = fieldValueEditor;
        jLabelHeader.setText(fieldRequirement != null ? terminologyProvider.getTerm(null, fieldRequirement.getValue()) : null);
        jLabelHeader.setToolTipText(createToolTipText(null));
        setEntryRequired(entryRequired);
    }

    public String getFieldID()
    {
        return fieldRequirement.getValue();
    }

    public FieldRequirement getFieldRequirement()
    {
        return fieldRequirement;
    }

    public final void setEntryRequired(boolean entryRequired)
    {
        jLabelRequired.setVisible(entryRequired);
    }

    public ValueRequirement getValueRequirement()
    {
        return fieldRequirement;
    }

    public DataRecordEditor getFieldValueEditor()
    {
        return fieldValueEditor;
    }

    public void setValidationErrors(List<T8DataValidationError> validationErrors)
    {
        this.validationErrors = validationErrors;
        refreshValidityDisplay();
    }

    public void refreshValidityDisplay()
    {
        List<T8DataValidationError> requirementValidationErrors;
        List<T8DataValidationError> allValidationErrors;

        allValidationErrors = new ArrayList<T8DataValidationError>();

        // First get all requirement validation errors for the property and add them to the normal validation errors.
        requirementValidationErrors = fieldValueEditor.getValidationErrors();
        if ((requirementValidationErrors != null) && (requirementValidationErrors.size() > 0))
        {
            allValidationErrors.addAll(requirementValidationErrors);
        }

        // Add all validations set on the header.
        if (validationErrors != null) allValidationErrors.addAll(validationErrors);

        // Update the visual color of the header according to its validity.
        if (!allValidationErrors.isEmpty())
        {
            this.setBackground(LAFConstants.INVALID_PINK);
            jLabelHeader.setToolTipText(createToolTipText(allValidationErrors));
        }
        else
        {
            this.setBackground(LAFConstants.CONTENT_PANEL_BG_COLOR);
            jLabelHeader.setToolTipText(createToolTipText(allValidationErrors));
        }
    }

    private String createToolTipText(List<T8DataValidationError> errors)
    {
        List<T8DataRequirementComment> comments;
        StringBuffer text;
        String definition;
        String formatDescription;

        definition = terminologyProvider.getDefinition(null, fieldRequirement.getValue());
        formatDescription = fieldRequirement != null ? (String)fieldRequirement.findRequirementAttribute(RequirementAttribute.FORMAT_DESCRIPTION.toString()) : null;

        // Create a new buffer.
        text = new StringBuffer();
        text.append("<html>");

        // Append the definition.
        text.append("<h2>Definition</h2>");
        text.append(definition != null ? definition : "Not Available");

        // Append the format description if we found one.
        if (!Strings.isNullOrEmpty(formatDescription))
        {
            text.append("<h2>Format Description</h2>");
            text.append(formatDescription);
        }

        // Append the comments for this property.
        comments = fieldRequirement.getParentDataRequirementInstance().getFieldComments(fieldRequirement.getPropertyID(), fieldRequirement.getFieldID());
        if (comments.size() > 0)
        {
            text.append("<h2>Comments</h2>");
            text.append("<ul width=\"500\">");
            for (T8DataRequirementComment comment : comments)
            {
                text.append("<li>");
                text.append(terminologyProvider.getTerm(null, comment.getTypeId()));
                text.append(": ");
                text.append(comment.getComment());
                text.append("</li>");
            }
            text.append("</ul>");
        }

        if ((errors != null) && (errors.size() > 0))
        {
            Set<String> addedMessages;

            // Create a set to hold all messages appended to the list.
            addedMessages = new HashSet<String>();

            text.append("<h2>Validation Errors</h2>");
            text.append("<ul>");
            for (T8DataValidationError error : errors)
            {
                String message;

                // Get the error message and only append it to the list if we have not already appended the same message.
                message = error.getErrorMessage();
                if (!addedMessages.contains(message))
                {
                    text.append("<li>");
                    text.append(message);
                    text.append("</li>");
                    addedMessages.add(message);
                }
            }
            text.append("</ul>");
        }

        text.append("</html>");
        return text.toString();
    }

    public void refreshAccess()
    {
        setEntryRequired(accessLayer.isRequired());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelHeader = new javax.swing.JLabel();
        jLabelRequired = new javax.swing.JLabel();

        setLayout(new java.awt.GridBagLayout());

        jLabelHeader.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelHeader.setText("Field");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelHeader, gridBagConstraints);

        jLabelRequired.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabelRequired.setForeground(new java.awt.Color(255, 0, 51));
        jLabelRequired.setText("*");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        add(jLabelRequired, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelHeader;
    private javax.swing.JLabel jLabelRequired;
    // End of variables declaration//GEN-END:variables
}
