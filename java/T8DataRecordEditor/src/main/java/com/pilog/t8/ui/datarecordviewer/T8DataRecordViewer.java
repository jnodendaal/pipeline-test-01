package com.pilog.t8.ui.datarecordviewer;

import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.CachedDataRecordProvider;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.T8LRUCachedDataRecordProvider;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.PropertyContent;
import com.pilog.t8.data.document.datarecord.RecordContent;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.org.T8ClientDataRecordProvider;
import com.pilog.t8.data.org.T8ClientTerminologyProvider;
import static com.pilog.t8.definition.api.T8DataRecordHistoryApiResource.OPERATION_RETRIEVE_FILE_HISTORY_SNAPSHOTS;
import static com.pilog.t8.definition.api.T8DataRecordHistoryApiResource.PARAMETER_FILE_ID;
import static com.pilog.t8.definition.api.T8DataRecordHistoryApiResource.PARAMETER_SNAPSHOT_MAP;
import static com.pilog.t8.definition.api.T8DataRecordHistoryApiResource.PARAMETER_TRANSACTION_ID_LIST;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.datarecordviewer.T8ComparisonType;
import com.pilog.t8.definition.ui.datarecordviewer.T8ComparisonTypeDefinition;
import com.pilog.t8.definition.ui.datarecordviewer.T8DataRecordViewerDefinition;
import com.pilog.t8.definition.ui.datarecordviewer.T8DataRecordViewerDefinition.ColumnHeaderType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.ui.datarecordviewer.model.T8DataRecordTableModel;
import com.pilog.t8.ui.optionpane.T8OptionPane;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.components.cellrenderers.TextAreaTableCellRenderer;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordViewer extends javax.swing.JPanel implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordViewer.class);

    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DataRecordViewerDefinition definition;
    private final T8ComponentController controller;
    private final T8ConfigurationManager configurationManager;
    private final T8DataRecordClientApi recApi;
    private final T8TerminologyClientApi trmApi;
    private final T8OntologyClientApi ontApi;
    private final CachedDataRecordProvider dataRecordProvider;
    private final CachedTerminologyProvider terminologyProvider;
    private final OntologyProvider ontologyProvider;
    private final T8DefaultComponentContainer container;
    private final JScrollPane scrollPane;
    private final PropertyTable propertyTable;
    private final HeaderTable headerTable;
    private final T8DataRecordViewerOperationHandler operationHandler;
    private final Map<String, RecordContent> dataFiles;
    private final TextAreaTableCellRenderer headerCellRenderer;
    private final PropertyCellRenderer propertyCellRenderer;
    private final List<T8ComparisonType> comparisonTypes;

    public T8DataRecordViewer(T8DataRecordViewerDefinition definition, T8ComponentController controller)
    {
        // Get the configuration manager and session context before initializing the components to translate UI correctly.
        this.context = controller.getContext();
        this.clientContext = controller.getClientContext();
        this.configurationManager = clientContext.getConfigurationManager();
        this.definition = definition;

        // Now initialize the UI components.
        initComponents();
        this.container = new T8DefaultComponentContainer();
        this.add(container, java.awt.BorderLayout.CENTER);
        this.container.setComponent(jPanelMain);
        this.controller = controller;
        this.recApi = controller.getApi(T8DataRecordClientApi.API_IDENTIFIER);
        this.trmApi = controller.getApi(T8TerminologyClientApi.API_IDENTIFIER);
        this.ontApi = controller.getApi(T8OntologyClientApi.API_IDENTIFIER);
        this.dataRecordProvider = new T8LRUCachedDataRecordProvider(new T8ClientDataRecordProvider(recApi), 50);
        this.terminologyProvider = new T8LRUCachedTerminologyProvider(new T8ClientTerminologyProvider(trmApi), 200);
        this.terminologyProvider.setLanguage(context.getSessionContext().getContentLanguageIdentifier());
        this.ontologyProvider = ontApi.getOntologyProvider();
        this.operationHandler = new T8DataRecordViewerOperationHandler(this);
        this.dataFiles = new LinkedHashMap<>();
        this.headerTable = new HeaderTable(null);
        this.headerTable.setGridColor(Color.GRAY);
        this.propertyTable = new PropertyTable(null);
        this.propertyTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.propertyTable.setGridColor(Color.GRAY);
        this.propertyTable.setHeaderTable(headerTable);
        this.headerTable.setPropertyTable(propertyTable);
        this.scrollPane = new JScrollPane();
        this.scrollPane.setViewportView(propertyTable);
        this.scrollPane.setRowHeaderView(headerTable);
        this.jPanelContent.add(java.awt.BorderLayout.CENTER, scrollPane);
        this.headerCellRenderer = new TextAreaTableCellRenderer();
        this.headerCellRenderer.setOpaque(false);
        this.propertyCellRenderer = new PropertyCellRenderer();
        this.propertyCellRenderer.setOpaque(false);
        this.comparisonTypes = new ArrayList<>();
        configureComponents();
    }

    private void configureComponents()
    {
        jLabelLanguage.setVisible(definition.isLanguageSwitchEnabled());
        jLabelLanguageValue.setVisible(definition.isLanguageSwitchEnabled());
        jSeparatorLanguage.setVisible(definition.isLanguageSwitchEnabled());

        // Create all comparison types.
        for (T8ComparisonTypeDefinition comparisonTypeDefinition : definition.getComparisionTypeDefinitions())
        {
            try
            {
                comparisonTypes.add(comparisonTypeDefinition.createComparisonType(controller, terminologyProvider, ontologyProvider));
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while constructing record comparison type: " + comparisonTypeDefinition, e);
            }
        }
    }

    protected String translate(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    public void unlockUI()
    {
        container.unlock();
    }

    public void lockUI(final String message)
    {
        container.setMessage(message);
        container.lock();
    }

    /**
     * Removes all loaded documents and clears all cached data.
     */
    public void clear()
    {
        removeAllDocuments();
        dataRecordProvider.clearCache();

        // Clear shared comparison types of residual content.
        for (T8ComparisonType comparisonType : comparisonTypes)
        {
            comparisonType.clear();
        }
    }

    public void addDataRecords(final List<DataRecord> dataFilesToAdd, final Map<String, String> displayHeaders)
    {
        List<RecordContent> records;

        records = new ArrayList<>();
        for (DataRecord dataRecord : dataFilesToAdd)
        {
            records.add(dataRecord.getContent(true));
        }

        addRecords(records, displayHeaders);
    }

    public void addRecords(final List<RecordContent> dataFilesToAdd, final Map<String, String> displayHeaders)
    {
        List<String> columnNames;

        // Add the new records to the collection.
        for (RecordContent dataFileToAdd : dataFilesToAdd)
        {
            dataFiles.put(dataFileToAdd.getId(), dataFileToAdd);
        }

        // Create the column names for the model.
        columnNames = ArrayLists.typeSafeList("Data Requirement Instance", "Property");
        for (String fileId : dataFiles.keySet())
        {
            if ((displayHeaders != null) && (displayHeaders.containsKey(fileId)))
            {
                columnNames.add(displayHeaders.get(fileId));
            }
            else if (definition.getColumnHeaderType() == ColumnHeaderType.PROPERTY_VALUE_STRING)
            {
                StringBuilder propertyValueString;
                PropertyContent recordProperty;
                RecordContent dataFile;

                // Get the data file and from it the property to use as column header.
                dataFile = dataFiles.get(fileId);
                recordProperty = dataFile.getProperty(definition.getColumnHeaderPropertyIdentifier());
                if (recordProperty != null)
                {
                    T8DataRecordDefinitionGenerator valueGenerator;

                    // Generate a value string from the property so that it can be used as a column header on the UI.
                    valueGenerator = new T8DataRecordDefinitionGenerator(terminologyProvider);
                    propertyValueString = valueGenerator.generateValueString(recordProperty);
                    columnNames.add(propertyValueString != null ? propertyValueString.toString() : fileId);
                }
                else columnNames.add(fileId);
            }
            else if (definition.getColumnHeaderType() == ColumnHeaderType.CODE)
            {
                T8OntologyConcept concept;
                T8OntologyCode code;

                // If no code type is specified just use the first one found.
                concept = ontologyProvider.getOntologyConcept(fileId);
                if (Strings.isNullOrEmpty(definition.getColumnHeaderCodeTypeIdentifier()))
                {
                    code = concept.getPrimaryCode();
                    if (code == null)
                    {
                        code = concept.getCodes().isEmpty() ? null : concept.getCodes().get(0);
                    }
                }
                else
                {
                    code = concept.getCodeByType(definition.getColumnHeaderCodeTypeIdentifier());
                }

                // Use the code if we found one, or else just the record ID.
                if (code != null)
                {
                    columnNames.add(code.getCode());
                }
                else columnNames.add(fileId);
            }
            else if (definition.getColumnHeaderType() == ColumnHeaderType.ALTERED_DATE)
            {
                RecordContent dataFile;
                T8Timestamp alteredTime;
                SimpleDateFormat formatter;

                // Get the data file and the last time it was altered (updated).
                dataFile = dataFiles.get(fileId);
                alteredTime = dataFile.getLatestUpdateTime();
                if (alteredTime == null) alteredTime = dataFile.getInsertedAt();

                // If there is no altered time, we assume it is the current record and assign the current time.
                if (alteredTime == null) alteredTime = new T8Timestamp(System.currentTimeMillis());

                // Format the timestamp into a presentable string and add it to the column names collection.
                formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
                columnNames.add(formatter.format(new Date(alteredTime.getMilliseconds())));
            }
            else
            {
                columnNames.add(fileId);
            }
        }

        // Set the model.
        setModel(new T8DataRecordTableModel(comparisonTypes, terminologyProvider, columnNames, new ArrayList<>(dataFiles.values())));
    }

    public void setModel(final TableModel tableModel)
    {
        TableColumnModel columnModel;
        JTableHeader corner;
        Dimension headerDimension;

        // Set the new model on both the content table as well as the row header table.
        propertyTable.setModel(tableModel);
        headerTable.setModel(tableModel);

        // Remove row header columns from the main table.
        columnModel = propertyTable.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0)); // Remove DR Instance column.
        columnModel.removeColumn(columnModel.getColumn(0)); // Removed Property column.
        for (int columnIndex = 0; columnIndex < columnModel.getColumnCount(); columnIndex++)
        {
            columnModel.getColumn(columnIndex).setPreferredWidth(300);
        }

        // Remove row header columns from the main table.
        columnModel = headerTable.getColumnModel();
        while (columnModel.getColumnCount() > 2)
        {
            columnModel.removeColumn(columnModel.getColumn(2));
        }

        // Set the renderer on all table columns.
        headerTable.setCellRenderer(headerCellRenderer);
        propertyTable.setCellRenderer(propertyCellRenderer);

        // Set the size of the header columns
        columnModel = headerTable.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(170);
        columnModel.getColumn(1).setPreferredWidth(200);

        headerDimension = headerTable.getPreferredScrollableViewportSize();
        headerDimension.width = headerTable.getPreferredSize().width;
        headerTable.setPreferredScrollableViewportSize(headerDimension);
        scrollPane.setRowHeaderView(headerTable);

        corner = headerTable.getTableHeader();
        corner.setReorderingAllowed(false);
        corner.setResizingAllowed(false);

        scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, corner);

        // Refresh row heights.
        scrollPane.validate();
        headerTable.validate();
        propertyTable.validate();
        headerTable.autoSizeRowHeights();
    }

    public void removeDocument(String recordId)
    {
    }

    public void removeAllDocuments()
    {
        dataFiles.clear();
    }

    public RecordContent getDocument(String recordId)
    {
        return dataFiles.get(recordId);
    }

    public boolean containsDocument(String recordId)
    {
        return dataFiles.containsKey(recordId);
    }

    public CachedDataRecordProvider getDataRecordProvider()
    {
        return dataRecordProvider;
    }

    public CachedTerminologyProvider getTerminologyProvider()
    {
        return terminologyProvider;
    }

    public OntologyProvider getOntologyProvider()
    {
        return ontologyProvider;
    }

    public void setToolBarVisible(boolean visible)
    {
        jToolBarMainControls.setVisible(visible);
    }

    protected Container getContentPane()
    {
        return jPanelContent;
    }

    public void loadFileHistory(String fileId, List<String> txIidList)
    {
        try
        {
            List<RecordContent> retrievedRecords;
            HistoryLoader historyLoader;

            // Make sure this method is not called from the EDT.
            if (SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Cannot invoke this method from EDT.");

            // Create and run the loader Thread.
            lockUI(translate("Loading Historic Snapshots").concat("..."));
            historyLoader = new HistoryLoader(fileId, txIidList);
            historyLoader.execute();

            // Wait for completion of the worker before continuing.
            retrievedRecords = historyLoader.get();
            SwingUtilities.invokeAndWait(() ->
            {
                addRecords(retrievedRecords, null);
            });
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while loading record history snapshots.", e);
            T8OptionPane.showMessageDialog(this, translate("The specified history snapshots could not be loaded."), translate("Status"), T8OptionPane.ERROR_MESSAGE, controller);
        }
        finally
        {
            unlockUI();
        }
    }

    public void loadDataRecords(List<String> recordIds, Map<String, String> displayHeaders, String accessIdentifier)
    {
        try
        {
            List<DataRecord> retrievedRecords;
            DocumentLoader documentLoader;

            // Make sure this method is not called from the EDT.
            if (SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Cannot invoke this method from EDT.");

            // Create and run the loader Thread.
            lockUI(translate("Loading Documents").concat("..."));
            documentLoader = new DocumentLoader(recordIds, displayHeaders, accessIdentifier);
            documentLoader.execute();

            // Wait for completion of the worker before continuing.
            retrievedRecords = documentLoader.get();
            SwingUtilities.invokeAndWait(() ->
            {
                addDataRecords(retrievedRecords, displayHeaders);
            });
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while loading Data Records.", e);
            T8OptionPane.showMessageDialog(this, translate("The specified documents could not be loaded."), translate("Status"), T8OptionPane.ERROR_MESSAGE, controller);
        }
        finally
        {
            unlockUI();
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }

    public T8ClientContext getClientContext()
    {
        return controller.getClientContext();
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private void refresh()
    {
    }

    /**
     * Loads a Data Record from the database and then adds the document to the
     * UI.
     */
    private class DocumentLoader extends SwingWorker<List<DataRecord>, Void>
    {
        private final List<String> recordIDList;
        private final Map<String, String> displayHeaders;
        private List<DataRecord> retrievedDataRecords;
        private final String accessIdentifier;

        private DocumentLoader(List<String> recordIDList, Map<String, String> displayHeaders, String accessIdentifier)
        {
            this.recordIDList = recordIDList;
            this.displayHeaders = displayHeaders;
            this.accessIdentifier = accessIdentifier;
        }

        @Override
        public List<DataRecord> doInBackground() throws Exception
        {
            // Retrieve the data record and its descendants.
            retrievedDataRecords = dataRecordProvider.getDataRecords(recordIDList, context.getSessionContext().getContentLanguageIdentifier(), true, true, true, true, false, true, true);
            for (DataRecord retrievedDataRecord : retrievedDataRecords)
            {
                // Add the terminology retrieved as part of the Data Record to the terminology provider.
                terminologyProvider.addTerminology(retrievedDataRecord.getContentTerminology(true));
            }

            return retrievedDataRecords;
        }
    }

    /**
     * Loads a Record Hisotry snapshots from the database and then adds the documents to the
     * UI.
     */
    private class HistoryLoader extends SwingWorker<List<RecordContent>, Void>
    {
        private final String fileId;
        private final List<String> txIidList;
        private final List<RecordContent> retrievedRecords;

        private HistoryLoader(String fileId, List<String> txIidList)
        {
            this.fileId = fileId;
            this.txIidList = txIidList;
            this.retrievedRecords = new ArrayList<>();
        }

        @Override
        public List<RecordContent> doInBackground() throws Exception
        {
            Map<String, Object> serverOperationParameters;
            Map<String, RecordContent> operationResult;

            // Execute the operation.
            serverOperationParameters = new HashMap<>();
            serverOperationParameters.put(OPERATION_RETRIEVE_FILE_HISTORY_SNAPSHOTS + PARAMETER_FILE_ID, fileId);
            serverOperationParameters.put(OPERATION_RETRIEVE_FILE_HISTORY_SNAPSHOTS + PARAMETER_TRANSACTION_ID_LIST, txIidList);
            operationResult = (Map<String, RecordContent>)controller.executeSynchronousServerOperation(OPERATION_RETRIEVE_FILE_HISTORY_SNAPSHOTS, serverOperationParameters, translate("Loading Record History Snapshots"))
                    .get(OPERATION_RETRIEVE_FILE_HISTORY_SNAPSHOTS + PARAMETER_SNAPSHOT_MAP);

            retrievedRecords.clear();
            for (String txIid : operationResult.keySet())
            {
                RecordContent record;

                // Assign a new ID for the data record so that the viewer can display it.
                record = operationResult.get(txIid);
                record.setId(txIid == null ? T8IdentifierUtilities.createNewGUID() : txIid);
                retrievedRecords.add(record);
            }

            return retrievedRecords;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jToolBarMainControls = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jSeparatorRefresh = new javax.swing.JToolBar.Separator();
        jLabelLanguage = new javax.swing.JLabel();
        jLabelLanguageValue = new javax.swing.JLabel();
        jSeparatorLanguage = new javax.swing.JToolBar.Separator();
        jToggleButtonMarkMatches = new javax.swing.JToggleButton();
        jToggleButtonMarkMismatches = new javax.swing.JToggleButton();
        jPanelContent = new javax.swing.JPanel();

        jPanelMain.setOpaque(false);
        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jToolBarMainControls.setFloatable(false);
        jToolBarMainControls.setRollover(true);
        jToolBarMainControls.setOpaque(false);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setText(translate("Refresh"));
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setOpaque(false);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jButtonRefresh);
        jToolBarMainControls.add(jSeparatorRefresh);

        jLabelLanguage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/textIcon.png"))); // NOI18N
        jLabelLanguage.setText(translate("Language").concat(":  "));
        jToolBarMainControls.add(jLabelLanguage);

        jLabelLanguageValue.setText("English US");
        jToolBarMainControls.add(jLabelLanguageValue);
        jToolBarMainControls.add(jSeparatorLanguage);

        jToggleButtonMarkMatches.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/markMatchIcon.png"))); // NOI18N
        jToggleButtonMarkMatches.setSelected(true);
        jToggleButtonMarkMatches.setText(translate("Mark Matches"));
        jToggleButtonMarkMatches.setFocusable(false);
        jToggleButtonMarkMatches.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButtonMarkMatches.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jToggleButtonMarkMatchesActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jToggleButtonMarkMatches);

        jToggleButtonMarkMismatches.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/markMismatchIcon.png"))); // NOI18N
        jToggleButtonMarkMismatches.setSelected(true);
        jToggleButtonMarkMismatches.setText(translate("Mark Mismatches"));
        jToggleButtonMarkMismatches.setFocusable(false);
        jToggleButtonMarkMismatches.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButtonMarkMismatches.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jToggleButtonMarkMismatchesActionPerformed(evt);
            }
        });
        jToolBarMainControls.add(jToggleButtonMarkMismatches);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelMain.add(jToolBarMainControls, gridBagConstraints);

        jPanelContent.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelMain.add(jPanelContent, gridBagConstraints);

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jToggleButtonMarkMatchesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jToggleButtonMarkMatchesActionPerformed
    {//GEN-HEADEREND:event_jToggleButtonMarkMatchesActionPerformed
        propertyCellRenderer.setMarkMatch(jToggleButtonMarkMatches.isSelected());
        repaint();
    }//GEN-LAST:event_jToggleButtonMarkMatchesActionPerformed

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        refresh();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jToggleButtonMarkMismatchesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jToggleButtonMarkMismatchesActionPerformed
    {//GEN-HEADEREND:event_jToggleButtonMarkMismatchesActionPerformed
        propertyCellRenderer.setMarkMismatch(jToggleButtonMarkMismatches.isSelected());
        repaint();
    }//GEN-LAST:event_jToggleButtonMarkMismatchesActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JLabel jLabelLanguage;
    private javax.swing.JLabel jLabelLanguageValue;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JToolBar.Separator jSeparatorLanguage;
    private javax.swing.JToolBar.Separator jSeparatorRefresh;
    private javax.swing.JToggleButton jToggleButtonMarkMatches;
    private javax.swing.JToggleButton jToggleButtonMarkMismatches;
    private javax.swing.JToolBar jToolBarMainControls;
    // End of variables declaration//GEN-END:variables
}
