package com.pilog.t8.ui.datarecordeditor.component.datatype.prescribed;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import java.util.Map;

import static com.pilog.t8.ui.datarecord.DataRecordEditorContainer.*;

/**
 * @author Bouwer du Preez
 */
public class StandardMeasuredNumberEditor extends StandardValueEditor
{
    private final MeasuredNumber measuredNumber;

    public StandardMeasuredNumberEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        this.measuredNumber = (MeasuredNumber)recordValue;
    }

    @Override
    synchronized void setSelectedValue(Map<String, Object> selectedValueFields)
    {
        // First check that a valid concept is selected.
        if (selectedValueFields != null)
        {
            String numericValue;
            String uomID;
            String qomID;

            // Clear the namespace from the returned values.
            selectedValueFields = T8IdentifierUtilities.stripNamespace(selectedValueFields);

            // Get the selected numeric value.
            numericValue = (String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_VALUE);

            // Get the selected uom ID from the selected values.
            uomID = (String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_UOM_ID);
            if (uomID != null)
            {
                T8ConceptTerminology uomTerminology;

                uomTerminology = new T8ConceptTerminology(T8OntologyConceptType.UNIT_OF_MEASURE, uomID);
                uomTerminology.setLanguageId(context.getSessionContext().getContentLanguageIdentifier());
                uomTerminology.setTermId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_UOM_TERM_ID));
                uomTerminology.setTerm((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_UOM_TERM));
                uomTerminology.setCodeId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_UOM_CODE_ID));
                uomTerminology.setCode((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_UOM_CODE));
                uomTerminology.setDefinitionId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_UOM_DEFINITION_ID));
                uomTerminology.setDefinition((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_UOM_DEFINITION));
                terminologyProvider.addTerminology(uomTerminology);
            }

            // Get the selected qom ID from the selected values.
            qomID = (String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_ID);
            if (qomID != null)
            {
                T8ConceptTerminology qomTerminology;

                qomTerminology = new T8ConceptTerminology(T8OntologyConceptType.QUALIFIER_OF_MEASURE, qomID);
                qomTerminology.setLanguageId(context.getSessionContext().getContentLanguageIdentifier());
                qomTerminology.setTermId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_TERM_ID));
                qomTerminology.setTerm((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_TERM));
                qomTerminology.setCodeId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_CODE_ID));
                qomTerminology.setCode((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_CODE));
                qomTerminology.setDefinitionId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_DEFINITION_ID));
                qomTerminology.setDefinition((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_DEFINITION));
                terminologyProvider.addTerminology(qomTerminology);
            }

            // Set the new value on the document.
            dataRecord.setAdjusting(true);
            measuredNumber.setNumber(numericValue);
            measuredNumber.setUomId(uomID);
            measuredNumber.setQomId(qomID);
            dataRecord.setAdjusting(false);
        }
        else
        {
            clearSelectedValue();
        }
    }

    @Override
    void clearSelectedValue()
    {
        // Clear the values from the document.
        dataRecord.setAdjusting(true);
        measuredNumber.setNumber(null);
        measuredNumber.setUomId(null);
        measuredNumber.setQomId(null);
        dataRecord.setAdjusting(false);
    }
}
