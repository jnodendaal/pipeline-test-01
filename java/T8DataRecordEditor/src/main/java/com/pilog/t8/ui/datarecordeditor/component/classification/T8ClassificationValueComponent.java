package com.pilog.t8.ui.datarecordeditor.component.classification;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarecord.access.layer.ClassificationAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.ui.laf.T8ButtonUI;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.strings.StringUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.border.Border;

import static com.pilog.t8.ui.datarecord.DataRecordEditorContainer.*;

/**
 * @author Bouwer du Preez
 */
public class T8ClassificationValueComponent extends javax.swing.JPanel
{
    private final DataRecordEditorContainer container;
    private final CachedTerminologyProvider terminologyProvider;
    private final T8Context context;
    private final T8ConfigurationManager configurationManager;
    private final T8LookAndFeelManager lafManager;
    private final ClassificationAccessLayer accessLayer;
    private final boolean editable;
    private T8OntologyLink link;

    private static Border EDITABLE_BORDER = javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(171, 173, 179)), javax.swing.BorderFactory.createEmptyBorder(2, 2, 2, 2));

    public T8ClassificationValueComponent(DataRecordEditorContainer container, DataRecord dataRecord, T8OntologyLink ontologyLink)
    {
        // Get the configuration manager before initializing the components to translate UI correctly.
        this.context = container.getAccessContext();
        this.configurationManager = context.getClientContext().getConfigurationManager();
        this.terminologyProvider = container.getTerminologyProvider();
        this.link = ontologyLink;
        this.accessLayer = dataRecord.getAccessLayer().getClassificationAccessLayer(link.getOntologyStructureID());
        this.editable = accessLayer.isEditable();
        this.lafManager = context.getClientContext().getConfigurationManager().getLookAndFeelManager(context);
        initComponents();
        this.jButtonClear.setUI(new ClearButtonUI(lafManager));
        this.jButtonLookup.setUI(new T8ButtonUI(lafManager));
        this.container = container;
        setBackground(LAFConstants.CONTENT_GREY);
        refresh();
    }

    public void setOntologyLink(T8OntologyLink ontologyLink)
    {
        this.link = ontologyLink;
        refresh();
    }

    public T8OntologyLink getOntologyLink()
    {
        return link;
    }

    public boolean hasChanges()
    {
        return false;
    }

    public void refresh()
    {
        String ontologyClassID;

        ontologyClassID = link.getOntologyClassID();
        if (ontologyClassID != null)
        {
            // Set the display String.
            jTextFieldDisplayValue.setText(getDisplayString(ontologyClassID));

            // Set the definition as the tooltip on the text field.
            jTextFieldDisplayValue.setToolTipText(getToolTip(ontologyClassID));
        }
        else
        {
            jTextFieldDisplayValue.setText(null);
        }

        // Set buttons enabled/disabled.
        jButtonLookup.setEnabled(editable && accessLayer.isUpdateEnabled());
        jButtonClear.setEnabled(editable && accessLayer.isDeleteEnabled() && accessLayer.isUpdateEnabled());
    }

    public boolean commitChanges()
    {
        return true;
    }

    private String getDisplayString(String conceptID)
    {
        String term;
        String code;
        String displayString;

        // Get the term and code for the value concept and construct a display String.
        term = terminologyProvider.getTerm(null, conceptID);
        code = terminologyProvider.getCode(conceptID);
        if (term != null)
        {
            // If the code is available, append it to the term.
            if (!Strings.isNullOrEmpty(code))
            {
                displayString = term + " (" + code + ")";

                // Make sure the display value is not too long.  If it is, use the term only.
                if (displayString.length() > 100) displayString = term;
            }
            else displayString = term;
        }
        else
        {
            displayString = "No Term Found";
        }

        // Return the resultant display String.
        return displayString;
    }

    private String getToolTip(String conceptID)
    {
        String definition;

        definition = terminologyProvider.getDefinition(null, conceptID);
        if (definition != null)
        {
            return StringUtilities.createToolTipHTML(StringUtilities.wrapText(100, definition));
        }
        else return null;
    }

    private void lookup()
    {
        Map<String, Object> selectedDialogValue;
        List<String> ontologyClassIDSet;
        String ontologyStructureID;

        // Get the ID's of the requirement we are filtering on.
        ontologyStructureID = link.getOntologyStructureID();

        // Create a list of all the specific concepts to include in the filter.
        ontologyClassIDSet = getFilterOntologyClassIDList();

        // Get the selected value.
        selectedDialogValue = container.getSelectedClassification(ontologyStructureID, ontologyClassIDSet);
        if (selectedDialogValue != null)
        {
            // Set the selected value of the editor.
            setSelectedOntologyClass(convertDialogOutputToConceptTerminology(selectedDialogValue));
        }
    }

    T8ConceptTerminology convertDialogOutputToConceptTerminology(Map<String, Object> selectedValueFields)
    {
        T8ConceptTerminology conceptTerminology;
        String conceptID;

        // Clear the namespace from the returned values.
        selectedValueFields = T8IdentifierUtilities.stripNamespace(selectedValueFields);

        // Get the selected concept ID from the selected entity/
        conceptID = (String)selectedValueFields.get(PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_ID);
        conceptTerminology = new T8ConceptTerminology(T8OntologyConceptType.ONTOLOGY_CLASS, conceptID);

        // Create the concept terminology object so that we can use it to set the selected value of this editor.
        conceptTerminology.setLanguageId(context.getSessionContext().getContentLanguageIdentifier());
        conceptTerminology.setTermId((String)selectedValueFields.get(PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_TERM_ID));
        conceptTerminology.setTerm((String)selectedValueFields.get(PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_TERM));
        conceptTerminology.setCodeId((String)selectedValueFields.get(PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_CODE_ID));
        conceptTerminology.setCode((String)selectedValueFields.get(PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_CODE));
        conceptTerminology.setDefinitionId((String)selectedValueFields.get(PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_DEFINITION_ID));
        conceptTerminology.setDefinition((String)selectedValueFields.get(PARAMETER_CLASSIFICATION_LOOKUP_OUT_OC_DEFINITION));
        return conceptTerminology;
    }

    List<String> getFilterOntologyClassIDList()
    {
        Set<String> accessLayerFilterOntologyClassIDSet;
        List<String> conceptIDList;

        // Create the filter ID sets.
        accessLayerFilterOntologyClassIDSet = accessLayer.getFilterOntologyClassIDSet();

        // Create a list of all the specific concepts to include in the filter.
        conceptIDList = new ArrayList<>();
        if (!CollectionUtilities.isNullOrEmpty(accessLayerFilterOntologyClassIDSet))
        {
            conceptIDList.addAll(accessLayerFilterOntologyClassIDSet);
        }

        // Return the list of concepts to filter by.
        return conceptIDList;
    }

    @Override
    public void addMouseListener(MouseListener listener)
    {
        super.addMouseListener(listener);
        jTextFieldDisplayValue.addMouseListener(listener);
        jButtonLookup.addMouseListener(listener);
        jButtonClear.addMouseListener(listener);
    }

    @Override
    public void removeMouseListener(MouseListener listener)
    {
        super.removeMouseListener(listener);
        jTextFieldDisplayValue.removeMouseListener(listener);
        jButtonLookup.removeMouseListener(listener);
        jButtonClear.removeMouseListener(listener);
    }

    private synchronized void setSelectedOntologyClass(T8ConceptTerminology conceptTerminology)
    {
        // First check that a valid concept is selected.
        if (conceptTerminology != null)
        {
            String selectedOntologyClassID;

            // Get the selected concept ID.
            selectedOntologyClassID = conceptTerminology.getConceptId();

            // Update the link.
            link.setOntologyClassID(selectedOntologyClassID);

            // Add the selected terminology to the cache (this terminology will
            // not be in the cache, because not all terminology from the
            // classification group is cached when the editor is initialized.
            terminologyProvider.addTerminology(conceptTerminology);
        }
        else
        {
            // Update the link.
            link.setOntologyClassID(null);
        }

        // Refresh the UI.
        refresh();
    }

    private static class ClearButtonUI extends T8ButtonUI
    {
        private final ImageIcon unarmedIcon;
        private final ImageIcon armedIcon;

        private ClearButtonUI(T8LookAndFeelManager lafManager)
        {
            super(lafManager);
            armedIcon = new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/cross-script.png"));
            unarmedIcon = new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"));
        }

        @Override
        protected void paintIcon(Graphics g, JComponent c, Rectangle iconRect)
        {
            Graphics2D g2;
            JButton button;

            g2 = (Graphics2D) g.create();
            button = (JButton) c;

            g2.translate(iconRect.x, iconRect.y);

            if (button.getModel().isRollover()) armedIcon.paintIcon(c, g2, 0, 0);
            else unarmedIcon.paintIcon(c, g2, 0, 0);

            g2.dispose();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelValueEditor = new javax.swing.JPanel();
        jTextFieldDisplayValue = new javax.swing.JTextField();
        jButtonLookup = new javax.swing.JButton();
        jButtonClear = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        setLayout(new java.awt.BorderLayout());

        jPanelValueEditor.setOpaque(false);
        jPanelValueEditor.setLayout(new java.awt.GridBagLayout());

        jTextFieldDisplayValue.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelValueEditor.add(jTextFieldDisplayValue, gridBagConstraints);

        jButtonLookup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/searchIcon.png"))); // NOI18N
        jButtonLookup.setToolTipText("Search for Data Value");
        jButtonLookup.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonLookup.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonLookupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
        jPanelValueEditor.add(jButtonLookup, gridBagConstraints);

        jButtonClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"))); // NOI18N
        jButtonClear.setToolTipText("Clear Data Value");
        jButtonClear.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonClear.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonClearActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 5);
        jPanelValueEditor.add(jButtonClear, gridBagConstraints);

        add(jPanelValueEditor, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonLookupActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonLookupActionPerformed
    {//GEN-HEADEREND:event_jButtonLookupActionPerformed
        lookup();
    }//GEN-LAST:event_jButtonLookupActionPerformed

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonClearActionPerformed
    {//GEN-HEADEREND:event_jButtonClearActionPerformed
        setSelectedOntologyClass(null);
    }//GEN-LAST:event_jButtonClearActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClear;
    private javax.swing.JButton jButtonLookup;
    private javax.swing.JPanel jPanelValueEditor;
    private javax.swing.JTextField jTextFieldDisplayValue;
    // End of variables declaration//GEN-END:variables
}
