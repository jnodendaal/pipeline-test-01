package com.pilog.t8.ui.datarecordeditor.component.datatype.documentreference;

import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.DependencyValue;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.Constants;
import com.pilog.t8.data.document.datarequirement.DataDependency;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.datastructures.tuples.DefaultPairComparator;
import com.pilog.t8.ui.datarecordeditor.component.datatype.DefaultRecordValueEditor;
import com.pilog.t8.ui.dialog.info.T8InformationDialog;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class DocumentReferenceEditor extends DefaultRecordValueEditor
{
    private final ArrayList<DocumentLinkEditor> linkEditors;
    private final ValueRequirement drInstanceGroupRequirement;
    private final DocumentReferenceLookupHandler lookupHandler;
    private final List<Pair<String, String>> conceptDependencies;
    private final DocumentReferenceRequirement documentReferenceListRequirement;
    private final DocumentReferenceList documentReferenceList;
    private final T8DataRecordClientApi recApi;
    private boolean independent;
    private boolean local;
    private int minimumReferences;
    private int maximumReferences;
    private int maximumReferencesPerDRInstance;
    private boolean prescribed;
    protected boolean editableDependency;
    private int gridY;

    public DocumentReferenceEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        this.documentReferenceList = (DocumentReferenceList)recordValue;
        this.documentReferenceListRequirement = (DocumentReferenceRequirement)valueRequirement;
        this.conceptDependencies = new ArrayList<Pair<String, String>>();
        this.linkEditors = new ArrayList<DocumentLinkEditor>();
        this.drInstanceGroupRequirement = valueRequirement.getSubRequirement(RequirementType.ONTOLOGY_CLASS, null, null);
        this.recApi = (T8DataRecordClientApi)context.getClientContext().getConfigurationManager().getAPI(context, T8DataRecordClientApi.API_IDENTIFIER);
        this.gridY = 0;
        this.editableDependency = true;
        fetchRequirementAttributes();
        initComponents();
        this.lookupHandler = new DocumentReferenceLookupHandler(this, documentReferenceList, drInstanceGroupRequirement.getValue(), prescribed, true);
        refreshEditor();
    }

    private void fetchRequirementAttributes()
    {
        independent = documentReferenceListRequirement.isIndependent();
        local = documentReferenceListRequirement.isLocal();
        prescribed = documentReferenceListRequirement.isPrescribed();
        minimumReferences = documentReferenceListRequirement.getMinimumReferenceCount();
        maximumReferences = documentReferenceListRequirement.getMaximumReferenceCount();
        maximumReferencesPerDRInstance = documentReferenceListRequirement.getMaximumReferencesPerDRInstanceCount();

        // Set default values for settings that may not be null.
        if (minimumReferences < 0) minimumReferences = 0;
        if (maximumReferences < 0) maximumReferences = 1000000;
        if (maximumReferencesPerDRInstance < 0) maximumReferencesPerDRInstance = 1000000;
    }

    private boolean isDRInstanceChangeAllowed()
    {
        return ((editableAccess) && (updateEnabled));
    }

    private boolean isReferenceDeletionAllowed()
    {
        return ((editableAccess) && (deleteEnabled));
    }

    private void rebuildContent()
    {
        // Clear the content of the editor so that the new reference components can be added.
        jPanelContent.removeAll();
        linkEditors.clear();
        gridY = 0;

        // Add all of the sub-value editors (document references).
        for (String recordID : documentReferenceList.getReferenceIds())
        {
            addDocumentLinkEditor(recordID);
        }

        // Validate the container.
        jPanelContent.revalidate();
        jPanelContent.repaint();
    }

    @Override
    public String translate(String inputString)
    {
        return super.translate(inputString);
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void refreshAccess()
    {
        // Call the super implementation.
        super.refreshAccess();

        // Set the filter ID sets.
        if (fieldAccessLayer != null)
        {
            lookupHandler.setFilterConceptIDSet(fieldAccessLayer.getFilterConceptIDSet());
            lookupHandler.setFilterGroupIDSet(fieldAccessLayer.getFilterConceptGroupIDSet());
            lookupHandler.setFilterOrganizationSetType(fieldAccessLayer.getFilterOrganizationSetType());
            lookupHandler.setOrganizationRestrictions(fieldAccessLayer.getOrganizationRestrictions());
        }
        else
        {
            lookupHandler.setFilterConceptIDSet(propertyAccessLayer.getFilterConceptIDSet());
            lookupHandler.setFilterGroupIDSet(propertyAccessLayer.getFilterConceptGroupIDSet());
            lookupHandler.setFilterOrganizationSetType(propertyAccessLayer.getFilterOrganizationSetType());
            lookupHandler.setOrganizationRestrictions(propertyAccessLayer.getOrganizationRestrictions());
        }

        // Refresh the link button state.
        refreshReferenceAdditionButtonState();
    }

    @Override
    public void refreshEditor()
    {
        // Execute the default behavior.
        super.refreshEditor();

        // Rebuild the UI.
        rebuildContent();
        refreshReferenceAdditionButtonState();
    }

    private void disableReferenceAdditionButtons()
    {
        jButtonAddNew.setEnabled(false);
        jButtonLinkExisting.setEnabled(false);
    }

    private void refreshReferenceAdditionButtonState()
    {
        int referenceCount;
        boolean enabled;

        referenceCount = this.documentReferenceList.getReferenceCount();

        enabled = (referenceCount < maximumReferences);
        jButtonAddNew.setVisible(!independent && insertEnabled && enabled && editableAccess && editableDependency);
        jButtonLinkExisting.setVisible(independent && insertEnabled && enabled && editableAccess && editableDependency);
        jButtonAddNew.setEnabled(jButtonAddNew.isVisible());
        jButtonLinkExisting.setEnabled(jButtonLinkExisting.isVisible());
   }

    @Override
    public boolean commitChanges()
    {
        // We don't have to do anything because the changed made by this editor
        // are committed as they are made.
        return true;
    }

    @Override
    public void refreshDataDependency()
    {
        Map<DataDependency, String> requiredConceptIDs;

        // Get the dependency values.
        requiredConceptIDs = documentReferenceList.getDependencyParentConceptIds(container.getDataRecordProvider());
        if (!requiredConceptIDs.containsValue(null))
        {
            List<Pair<String, String>> dependencyList;

            // Create the new concept dependency set.
            dependencyList = new ArrayList<Pair<String, String>>();
            for (DataDependency dependencyRequirement : requiredConceptIDs.keySet())
            {
                String dependencyConceptID;
                Pair<String, String> dependency;

                dependencyConceptID = requiredConceptIDs.get(dependencyRequirement);
                dependency = new Pair<String, String>(dependencyRequirement.getRelationId(), dependencyConceptID);
                if (!dependencyList.contains(dependency)) dependencyList.add(dependency);
            }

            // Sort the dependency list (this step is important for consistency, since the order of the list depends on keys fetched from a map).
            Collections.sort(dependencyList, new DefaultPairComparator(true, true, false));

            // Set the new concept dependencies.
            conceptDependencies.clear();
            conceptDependencies.addAll(dependencyList);

            // Set the edititability.
            editableDependency = true;
        }
        else
        {
            // Set the dependency concept to null (because it could not be found).
            conceptDependencies.clear();

            // Set the edititability.
            editableDependency = false;
        }
    }

    private void linkExistingSubRecord()
    {
        lookupHandler.showLinkSubRecordLookupDialog(conceptDependencies, valueRequirement.getParentPropertyRequirement(), prescribed);
    }

    private int getDRInstanceReferenceCount(String drInstanceID)
    {
        int count;

        count = 0;
        for (String referenceRecordId : documentReferenceList.getReferenceIds())
        {
            DataRecord referencedRecord;

            referencedRecord = dataRecord.findDataRecord(referenceRecordId);
            if ((referencedRecord != null) && (referencedRecord.getDataRequirementInstance().getConceptID().equals(drInstanceID)))
            {
                count++;
            }
        }

        return count;
    }

    /**
     * Adds a link editor to this document reference editor.  If a record ID is
     * supplied, the record is loaded into the container and the link is
     * initialized to point to the specified record.  If no record ID is
     * supplied a new record is created in the container using the specified
     * DR Instance.  This method can only be called from a non-EDT.
     * @param recordId The record ID (optional) of the record to load and to
     * which the new link editor will point.
     * @param drInstanceId The Data Requirement Instance ID that will be used
     * for the reference.
     */
    public void addDocumentLinkEditor(final String recordId, final String drInstanceId)
    {
        int drInstanceReferenceCount;

        drInstanceReferenceCount = getDRInstanceReferenceCount(drInstanceId);
        if (drInstanceReferenceCount >= maximumReferencesPerDRInstance)
        {
            StringBuffer message;

            message = new StringBuffer();
            message.append(translate("A maximum of"));
            message.append(" ");
            message.append(maximumReferencesPerDRInstance);
            message.append(" '");
            message.append(terminologyProvider.getTerm(null, drInstanceId));
            message.append("' ");
            message.append(maximumReferencesPerDRInstance > 1 ? translate("references is allowed.") : translate("reference is allowed."));
            JOptionPane.showMessageDialog(context.getClientContext().getParentWindow(), message, translate("Invalid Selection"), JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            // Firstly, disable the control buttons so that we don't allow invalid additions.
            disableReferenceAdditionButtons();

            // Set the adjusting flag because we don't want this editor to refreshEditor when changes are made to the document.
            dataRecord.setAdjusting(true);

            if (Strings.isNullOrEmpty(recordId))
            {
                try
                {
                    DocumentLinkEditor linkEditor;
                    DataRecord newDataRecord;
                    String newRecordID;
                    String parentRecordID;
                    String parentPropertyID;
                    String parentFieldID;

                    // Get all of the ID's required for new record creation.
                    newRecordID = T8IdentifierUtilities.createNewGUID();
                    parentRecordID = recordValue.getParentDataRecord().getID();
                    parentPropertyID = recordValue.getPropertyID();
                    parentFieldID = recordValue.getFieldID();

                    // Add the new document reference editor to the UI.
                    documentReferenceList.addReference(newRecordID);
                    linkEditor = addDocumentLinkEditor(newRecordID);

                    // Create the new record.
                    newDataRecord = container.createNewDocument(parentRecordID, parentPropertyID, parentFieldID, newRecordID, drInstanceId, false);

                    // Refresh the link editor definition
                    linkEditor.refreshLinkText(newDataRecord);

                    // Refresh the state of the link buttons.
                    refreshReferenceAdditionButtonState();
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while creating new record.", e);
                }
            }
            else // We don't have to create a new record, we're just creating a link to an existing document.
            {
                // Add the new document reference editor to the UI.
                documentReferenceList.addReference(recordId);
                addDocumentLinkEditor(recordId);

                // Refresh the state of the link buttons.
                refreshReferenceAdditionButtonState();
            }

            // Reset the adjusting flag.
            dataRecord.setAdjusting(false, true);
        }
    }

    private DocumentLinkEditor addDocumentLinkEditor(String recordId)
    {
        GridBagConstraints gridBagConstraints;
        DocumentLinkEditor linkEditor;
        Component buttonPanel;

        // Create the sub-record editor component.
        linkEditor = new DocumentLinkEditor(container, dataRecord, recordId);
        linkEditors.add(linkEditor);

        buttonPanel = buildDocumentLinkButtons(linkEditor, isDRInstanceChangeAllowed());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(0, 0, 0, 0);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = gridY;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
        jPanelContent.add(buttonPanel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = gridY++;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelContent.add(linkEditor, gridBagConstraints);

        jPanelContent.invalidate();
        jPanelContent.revalidate();

        return linkEditor;
    }

    private void removeDocumentLink(DocumentLinkEditor linkEditor)
    {
        String referencedRecordId;

        // Get the reference id to be removed.
        referencedRecordId = linkEditor.getRecordID();

        // Set the adjusting flag.
        dataRecord.setModelAdjusting(true, false);

        // Remove the editor from the Container UI.
        if (container.containsDocument(referencedRecordId))
        {
            container.removeDocument(referencedRecordId);
        }

        // Remove the reference from the underlying Data Record document.
        documentReferenceList.removeReference(referencedRecordId, true);

        // Set the adjusting flag.
        dataRecord.setModelAdjusting(false, true);

        // Refresh the state of the link buttons.
        refreshReferenceAdditionButtonState();

        // Rebuild the UI of this editor.
        rebuildContent();
    }

    private void changeSubRecordDRInstance(DocumentLinkEditor subRecordComponent)
    {
        lookupHandler.showDrInstanceChangeLookupDialog(subRecordComponent, conceptDependencies, valueRequirement.getParentPropertyRequirement(), prescribed);
    }

    private Component buildDocumentLinkButtons(final DocumentLinkEditor linkEditor, boolean drInstanceChangeAllowed)
    {
        final JToolBar buttonBar;
        final JButton jButtonRemove;
        final JButton jButtonDRInstanceChange;

        buttonBar = new JToolBar();
        buttonBar.setFloatable(false);

        // Add the remove button.
        jButtonRemove = new javax.swing.JButton();
        jButtonRemove.setVisible(isReferenceDeletionAllowed());
        jButtonRemove.setText(null);
        jButtonRemove.setIcon(Constants.CUT_ICON);
        jButtonRemove.setToolTipText(translate("Remove Sub-Record"));
        jButtonRemove.setHorizontalAlignment(JButton.TRAILING);
        jButtonRemove.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonRemove.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                // Check for dependent values and if we find any, get user confirmation first.
                if (confirmReferenceRemoval(linkEditor.getRecordID()))
                {
                    removeDocumentLink(linkEditor);
                }
            }
        });
        buttonBar.add(jButtonRemove);

        // Add the DR Instance change button if required.
        jButtonDRInstanceChange = new javax.swing.JButton();
        jButtonDRInstanceChange.setVisible(isDRInstanceChangeAllowed());
        jButtonDRInstanceChange.setText(null);
        jButtonDRInstanceChange.setIcon(Constants.CLASS_CHANGE_ICON);
        jButtonDRInstanceChange.setToolTipText(translate("Change instance type of the referenced record."));
        jButtonDRInstanceChange.setHorizontalAlignment(JButton.TRAILING);
        jButtonDRInstanceChange.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonDRInstanceChange.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                // Check for dependent values and if we find any, get user confirmation first.
                if (confirmDRInstanceChange(linkEditor))
                {
                    changeSubRecordDRInstance(linkEditor);
                }
            }
        });
        buttonBar.add(jButtonDRInstanceChange);

        return buttonBar;
    }

    private boolean confirmReferenceRemoval(String referencedRecordId)
    {
        List<DependencyValue> dependentValues;

        // If the record reference is dependent, make sure no other local reference to the record to be removed exists.
        if (!documentReferenceList.isIndependent())
        {
            Set<String> independentReferenceSet;

            // Get the set of independent record reference id's in the data file.
            independentReferenceSet = dataRecord.getDataFileRecords()
                    .stream()
                    .map(record -> record.getSubRecordReferenceIDSet(false, true))
                    .flatMap(s -> s.stream())
                    .collect(Collectors.toSet());

            // If the dependent record to be deleted is reference from another property in this data file, prevent the action.
            if (independentReferenceSet.contains(referencedRecordId))
            {
                T8InformationDialog.showInformationMessage(context, SwingUtilities.getWindowAncestor(this), translate("Invalid Action"), translate("The referenced record cannot be deleted until all references to it has been removed."), createLocalReferenceValueString(referencedRecordId));
                return false;
            }
        }

        // Check for dependent values and display a warning if any are found.
        dependentValues = documentReferenceList.getDependentValues(referencedRecordId);
        if (dependentValues.size() > 0)
        {
            return T8InformationDialog.showConfirmationMessage(context, SwingUtilities.getWindowAncestor(this), translate("Confirmation"), translate("Changing this value will clear the related data.  Do you want to continue?"), createDependentValueString(dependentValues));
        }
        else
        {
            return (JOptionPane.showConfirmDialog(context.getClientContext().getParentWindow(), translate("Remove the record and its related data?"), translate("Confirmation"), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);
        }
    }

    private boolean confirmDRInstanceChange(DocumentLinkEditor linkEditor)
    {
        List<DataRecord> recordList;
        List<RecordValue> attachmentValueList;
        List<DependencyValue> dependentValues;
        DataRecord targetRecord;

        // Get the target record and create a list containing it and all of its descendants.
        targetRecord = dataRecord.findDataRecord(linkEditor.getRecordID());
        recordList = new ArrayList<DataRecord>();
        recordList.add(targetRecord);
        recordList.addAll(targetRecord.getDescendantRecords());

        // From the list of records that may be affected by the DR Instance change, find all attachments that may be affected.
        attachmentValueList = new ArrayList<RecordValue>();
        for (DataRecord record : recordList)
        {
            for (RecordValue value : record.getAllValues())
            {
                if (value.getRequirementType() == RequirementType.ATTACHMENT)
                {
                    attachmentValueList.add(value);
                }
            }
        }

        // Confirm impact on dependent values.
        dependentValues = documentReferenceList.getDependentValues(linkEditor.getRecordID());
        if (dependentValues.size() > 0)
        {
            return T8InformationDialog.showConfirmationMessage(context, SwingUtilities.getWindowAncestor(this), translate("Confirmation"), translate("Changing this value will clear the related data.  Do you want to continue?"), createDependentValueString(dependentValues));
        }

        // If we found attachment that may be affected, get user confirmation first.
        if (attachmentValueList.size() > 0)
        {
            return T8InformationDialog.showConfirmationMessage(context, SwingUtilities.getWindowAncestor(this), translate("Confirmation"), translate("Changing this value may cause attachment data to be lost.  Do you want to continue?"), createDependentValueString(attachmentValueList));
        }
        else return true;
    }

    protected List<T8ConceptTerminology> retrieveDrInstanceOptions(int pageOffset, int pageSize)
    {
        DataRecord contextRecord;
        RecordValue targetValue;
        String targetRecordID;
        String targetPropertyID;
        String targetFieldID;
        String targetDataTypeID;

        // Get the parameters required in order to retrieve DR Instance Options.
        contextRecord = dataRecord.getRootRecord();
        targetValue = recordValue;
        targetRecordID = targetValue.getRecordID();
        targetPropertyID = targetValue.getPropertyID();
        targetFieldID = targetValue.getFieldID();
        targetDataTypeID = targetValue.getRequirementTypeID();

        // Retrieve the DR Instance Options.
        try
        {
            return recApi.retrieveDrInstanceOptions(contextRecord, targetRecordID, targetPropertyID, targetFieldID, targetDataTypeID, pageOffset, pageSize);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while retrieving DR Instance options for context record '" + contextRecord + "', target path '" + targetRecordID + "'.", e);
            return new ArrayList<T8ConceptTerminology>();
        }
    }

    /**
     * We override the default method to prevent the hyper links from hogging horizontal space.
     * We need this component to only supply its vertical size preference.
     * @return The adjusted preferred size of this component.
     */
    @Override
    public Dimension getPreferredSize()
    {
        Dimension preferredSize;

        preferredSize = super.getPreferredSize();
        preferredSize.setSize(0, preferredSize.getHeight());
        return preferredSize;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jToolBarReferenceControls = new javax.swing.JToolBar();
        jButtonAddNew = new javax.swing.JButton();
        jButtonLinkExisting = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelContent, gridBagConstraints);

        jToolBarReferenceControls.setFloatable(false);
        jToolBarReferenceControls.setRollover(true);

        jButtonAddNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAddNew.setText(translate("Add New..."));
        jButtonAddNew.setFocusable(false);
        jButtonAddNew.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddNew.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddNewActionPerformed(evt);
            }
        });
        jToolBarReferenceControls.add(jButtonAddNew);

        jButtonLinkExisting.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/searchIcon.png"))); // NOI18N
        jButtonLinkExisting.setText(translate("Link Existing..."));
        jButtonLinkExisting.setFocusable(false);
        jButtonLinkExisting.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonLinkExisting.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonLinkExistingActionPerformed(evt);
            }
        });
        jToolBarReferenceControls.add(jButtonLinkExisting);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jToolBarReferenceControls, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddNewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddNewActionPerformed
    {//GEN-HEADEREND:event_jButtonAddNewActionPerformed
        List<T8ConceptTerminology> drInstanceOptions;

        // Make sure to commit existing edits.
        container.commitChanges();

        // Add the new link.
        drInstanceOptions = retrieveDrInstanceOptions(0, 2);
        if (drInstanceOptions.size() == 1)
        {
            new Thread()
            {
                @Override
                public void run()
                {
                    T8ConceptTerminology firstValue;
                    String drInstanceId;

                    firstValue = drInstanceOptions.get(0);

                    drInstanceId = firstValue.getConceptId();
                    terminologyProvider.addTerminology(firstValue);
                    addDocumentLinkEditor(null, drInstanceId);
                }
            }.start();
        }
        else
        {
            lookupHandler.showNewSubRecordLookupDialog(new ArrayList<>(conceptDependencies), valueRequirement.getParentPropertyRequirement(), prescribed);
        }
    }//GEN-LAST:event_jButtonAddNewActionPerformed

    private void jButtonLinkExistingActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonLinkExistingActionPerformed
    {//GEN-HEADEREND:event_jButtonLinkExistingActionPerformed
        linkExistingSubRecord();
    }//GEN-LAST:event_jButtonLinkExistingActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddNew;
    private javax.swing.JButton jButtonLinkExisting;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JToolBar jToolBarReferenceControls;
    // End of variables declaration//GEN-END:variables
}
