package com.pilog.t8.ui.datarecordeditor.component.classification;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.layer.ClassificationAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.validation.T8RecordClassificationValidationError;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.laf.LAFConstants;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ClassificationHeaderComponent extends javax.swing.JPanel
{
    private final T8ClassificationValueComponent valueComponent;
    private final TerminologyProvider terminologyProvider;
    private final List<T8RecordClassificationValidationError> validationErrors;
    private final DataRecord record;
    private final ClassificationAccessLayer classificationAccessLayer;
    private T8OntologyLink link;

    public T8ClassificationHeaderComponent(DataRecordEditorContainer container, DataRecord record, T8ClassificationValueComponent valueComponent, T8OntologyLink link)
    {
        initComponents();
        this.record = record;
        this.classificationAccessLayer = record.getAccessLayer().getClassificationAccessLayer(link.getOntologyStructureID());
        this.terminologyProvider = container.getTerminologyProvider();
        this.valueComponent = valueComponent;
        this.setBackground(LAFConstants.PROPERTY_BLUE);
        this.jLabelHeader.setText(null);
        this.setOntologyLink(link);
        this.validationErrors = new ArrayList<>();
    }

    public T8OntologyLink getOntologyLink()
    {
        return link;
    }

    private void setOntologyLink(T8OntologyLink link)
    {
        this.link = link;
        if (link != null)
        {
            String displayName;

            displayName = terminologyProvider.getTerm(null, link.getOntologyStructureID());
            jLabelHeader.setText(displayName);
        }
        else
        {
            jLabelHeader.setText(null);
        }
    }

    public final void setEntryRequired(boolean entryRequired)
    {
        jLabelRequired.setVisible(entryRequired);
    }

    public void setValidationErrors(List<T8RecordClassificationValidationError> errors)
    {
        validationErrors.clear();
        if (errors != null)
        {
            validationErrors.addAll(errors);
        }

        // Update the visual color of the header according to its validity.
        if (!validationErrors.isEmpty())
        {
            this.setBackground(LAFConstants.INVALID_PINK);
            jLabelHeader.setToolTipText(createToolTipText());
        }
        else
        {
            this.setBackground(LAFConstants.PROPERTY_BLUE);
            jLabelHeader.setToolTipText(createToolTipText());
        }
    }

    private String createToolTipText()
    {
        StringBuffer text;
        String term;
        String definition;
        String osId;

        osId = link.getOntologyStructureID();
        term = terminologyProvider.getTerm(null, osId);
        definition = terminologyProvider.getDefinition(null, osId);

        // Create a new buffer.
        text = new StringBuffer();
        text.append("<html>");

        // Append the definition.
        text.append("<h2>Classification</h2>");
        text.append("<p width=\"500\">");
        text.append(term != null ? term : "Not Available");
        text.append("</p>");

        // Append the definition.
        text.append("<h2>Definition</h2>");
        text.append("<p width=\"500\">");
        text.append(definition != null ? definition : "Not Available");
        text.append("</p>");

        // Append the validation errors (if any).
        if ((validationErrors != null) && (validationErrors.size() > 0))
        {
            Set<String> addedMessages;

            // Create a set to hold all messages appended to the list.
            addedMessages = new HashSet<String>();

            text.append("<h2>Validation Errors</h2>");
            text.append("<ul>");
            for (T8RecordClassificationValidationError error : validationErrors)
            {
                String message;

                // Get the error message and only append it to the list if we have not already appended the same message.
                message = error.getErrorMessage();
                if (!addedMessages.contains(message))
                {
                    text.append("<li>");
                    text.append(message);
                    text.append("</li>");
                    addedMessages.add(message);
                }
            }
            text.append("</ul>");
        }

        text.append("</html>");
        return text.toString();
    }

    public void refreshAccess()
    {
        setEntryRequired(classificationAccessLayer.isRequired());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelHeader = new javax.swing.JLabel();
        jLabelRequired = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        setLayout(new java.awt.GridBagLayout());

        jLabelHeader.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabelHeader.setText("Classification Structure");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelHeader, gridBagConstraints);

        jLabelRequired.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabelRequired.setForeground(new java.awt.Color(255, 0, 51));
        jLabelRequired.setText("*");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        add(jLabelRequired, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelHeader;
    private javax.swing.JLabel jLabelRequired;
    // End of variables declaration//GEN-END:variables
}
