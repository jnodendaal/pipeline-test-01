package com.pilog.t8.ui.datarecordeditor.component.datatype.prescribed;

import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.api.T8OrganizationClientApi;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.laf.T8ButtonUI;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.component.datatype.DefaultRecordValueEditor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 * @author Bouwer du Preez
 */
public abstract class StandardValueEditor extends DefaultRecordValueEditor
{
    private final T8LookAndFeelManager lafManager;
    private final T8ComponentController controller;
    private final T8OrganizationClientApi orgApi;
    private final T8OntologyClientApi ontApi;
    protected final T8OntologyStructure ontologyStructure;
    protected final T8OrganizationStructure orgStructure;
    protected final T8DataRecordDefinitionGenerator stringGenerator;
    private boolean prescribed;
    private boolean allowNewValue;
    private boolean restrictToApprovedValue;

    public StandardValueEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);

        initComponents();
        this.controller = new T8DefaultComponentController(new T8Context(context), this.getClass().getCanonicalName(), false);
        this.lafManager = context.getClientContext().getConfigurationManager().getLookAndFeelManager(context);
        this.orgApi = controller.getApi(T8OrganizationClientApi.API_IDENTIFIER);
        this.ontApi = controller.getApi(T8OntologyClientApi.API_IDENTIFIER);
        this.stringGenerator = new T8DataRecordDefinitionGenerator(terminologyProvider);
        this.ontologyStructure = ontApi.getOntologyStructure();
        this.orgStructure = orgApi.getOrganizationStructure();
        jButtonClear.setUI(new ClearButtonUI(lafManager));
        jButtonLookup.setUI(new T8ButtonUI(lafManager));
        setRequirementAttributes();
        refreshEditor();
    }

    private void setRequirementAttributes()
    {
        prescribed = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
        allowNewValue = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.ALLOW_NEW_VALUE.toString()));
        restrictToApprovedValue = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();

        // Set buttons enabled/disabled.
        jButtonLookup.setEnabled(editableAccess);
        jButtonClear.setEnabled(editableAccess);
    }

    @Override
    public void refreshEditor()
    {
        // Set the display String.
        jTextFieldDisplayValue.setText(getDisplayString());

        // Set the tooltip on the text field.
        jTextFieldDisplayValue.setToolTipText(getToolTip());

        // Set buttons enabled/disabled.
        jButtonLookup.setEnabled(editableAccess);
        jButtonClear.setEnabled(editableAccess);
    }

    @Override
    public boolean commitChanges()
    {
        // Nothing to do here, because changes are commited as they are made.
        return true;
    }

    private String getDisplayString()
    {
        StringBuilder definition;

        definition = stringGenerator.generateValueString(recordValue);
        return definition != null ? definition.toString() : null;
    }

    private String getToolTip()
    {
        return null;
    }

    private void lookup()
    {
        Map<String, Object> selectedDialogValue;
        List<String> orgIdList;

        // Create the list of accesible organization levels.
        orgIdList = new ArrayList<String>();
        orgIdList.addAll(orgStructure.getOrganizationLineageIDList(context.getSessionContext().getOrganizationIdentifier())); // Add a filter to include all concepts linked to the user's session organization lineage.

        // Get the selected value.
        selectedDialogValue = container.getSelectedValue(recordValue.getParentDataRecord(), recordValue.getValueRequirement(), orgIdList, null, null, null);
        setSelectedValue(selectedDialogValue);
    }

    abstract void clearSelectedValue();
    abstract void setSelectedValue(Map<String, Object> selectedDialogValue);

    @Override
    public void addMouseListener(MouseListener listener)
    {
        super.addMouseListener(listener);
        jTextFieldDisplayValue.addMouseListener(listener);
        jButtonLookup.addMouseListener(listener);
        jButtonClear.addMouseListener(listener);
    }

    @Override
    public void removeMouseListener(MouseListener listener)
    {
        super.removeMouseListener(listener);
        jTextFieldDisplayValue.removeMouseListener(listener);
        jButtonLookup.removeMouseListener(listener);
        jButtonClear.removeMouseListener(listener);
    }

    private static class ClearButtonUI extends T8ButtonUI
    {
        private final ImageIcon unarmedIcon;
        private final ImageIcon armedIcon;

        public ClearButtonUI(T8LookAndFeelManager lafManager)
        {
            super(lafManager);
            armedIcon = new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/cross-script.png"));
            unarmedIcon = new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"));
        }

        @Override
        protected void paintIcon(Graphics g, JComponent c, Rectangle iconRect)
        {
            Graphics2D g2;
            JButton button;

            g2 = (Graphics2D) g.create();
            button = (JButton) c;

            g2.translate(iconRect.x, iconRect.y);

            if(button.getModel().isRollover()) armedIcon.paintIcon(c, g2, 0, 0);
            else unarmedIcon.paintIcon(c, g2, 0, 0);

            g2.dispose();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jTextFieldDisplayValue = new javax.swing.JTextField();
        jButtonLookup = new javax.swing.JButton();
        jButtonClear = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jTextFieldDisplayValue.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jTextFieldDisplayValue, gridBagConstraints);

        jButtonLookup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/searchIcon.png"))); // NOI18N
        jButtonLookup.setToolTipText("Search for Data Value");
        jButtonLookup.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonLookup.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonLookupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
        add(jButtonLookup, gridBagConstraints);

        jButtonClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png"))); // NOI18N
        jButtonClear.setToolTipText("Clear Data Value");
        jButtonClear.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonClear.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonClearActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 5);
        add(jButtonClear, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonLookupActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonLookupActionPerformed
    {//GEN-HEADEREND:event_jButtonLookupActionPerformed
        lookup();
    }//GEN-LAST:event_jButtonLookupActionPerformed

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonClearActionPerformed
    {//GEN-HEADEREND:event_jButtonClearActionPerformed
        clearSelectedValue();
    }//GEN-LAST:event_jButtonClearActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClear;
    private javax.swing.JButton jButtonLookup;
    private javax.swing.JTextField jTextFieldDisplayValue;
    // End of variables declaration//GEN-END:variables
}
