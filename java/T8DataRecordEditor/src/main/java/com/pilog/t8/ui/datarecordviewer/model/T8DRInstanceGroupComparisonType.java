package com.pilog.t8.ui.datarecordviewer.model;

import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordContent;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.definition.ui.datarecordviewer.T8DRInstanceGroupComparisonTypeDefinition;
import com.pilog.t8.ui.T8ComponentController;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DRInstanceGroupComparisonType extends T8AbstractComparisonType
{
    private final T8DRInstanceGroupComparisonTypeDefinition definition;
    private final T8OntologyStructure ontologyStructure;
    private final T8OntologyClientApi ontApi;
    private final OntologyProvider ontologyProvider;
    private final String drInstanceOcId;
    private final List<String> ocIdList;

    public T8DRInstanceGroupComparisonType(T8ComponentController controller, TerminologyProvider terminologyProvider, OntologyProvider ontologyProvider, T8DRInstanceGroupComparisonTypeDefinition definition)
    {
        super(terminologyProvider);
        this.definition = definition;
        this.drInstanceOcId = definition.getDRInstanceGroupIdentifier();
        this.ontApi = controller.getApi(T8OntologyClientApi.API_IDENTIFIER);
        this.ontologyStructure = ontApi.getOntologyStructure();
        this.ocIdList = ontologyStructure.getDescendantClassIDList(drInstanceOcId);
        this.ocIdList.add(drInstanceOcId);
        this.ontologyProvider = ontologyProvider;
    }

    @Override
    public String getGroupDisplayName()
    {
        return terminologyProvider.getTerm(null, drInstanceOcId);
    }

    @Override
    public boolean matchesRecord(RecordContent record)
    {
        Set<String> recordDrInstanceGroups;
        T8OntologyConcept drInstanceConcept;

        drInstanceConcept = ontologyProvider.getOntologyConcept(record.getDrInstanceId());
        recordDrInstanceGroups = drInstanceConcept.getGroupIDSet();
        for (String recordDRInstanceGroup : recordDrInstanceGroups)
        {
            if (ocIdList.contains(recordDRInstanceGroup)) return true;
        }

        return false;
    }

    @Override
    public String toString()
    {
        return "T8DRInstanceGroupComparisonType{" + "drInstanceGroupID=" + drInstanceOcId + '}';
    }
}
