package com.pilog.t8.ui.datarecordeditor.component.datatype.documentreference;

import com.pilog.t8.T8Log;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.api.T8OrganizationClientApi;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.data.org.T8OrganizationStructure.OrganizationSetType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import java.util.Map;
import com.pilog.t8.data.document.datarecord.T8DataRecordValueFilterHandler;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.security.T8Context;
import java.util.Set;
import java.util.List;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import java.util.ArrayList;
import java.util.HashSet;

import static com.pilog.t8.ui.datarecord.DataRecordEditorContainer.*;

/**
 * @author Bouwer du Preez
 */
public class DocumentReferenceLookupHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(DocumentReferenceLookupHandler.class);

    private final DataRecordEditorContainer container;
    private final DocumentReferenceList referenceList;
    private final T8ComponentController controller;
    private final DocumentReferenceEditor editor;
    private final T8OrganizationClientApi orgApi;
    private final T8OntologyClientApi ontApi;
    private final T8DataRecordValueFilterHandler filterHandler;
    private final T8Context context;
    private final Set<String> filterConceptIDSet;
    private final Set<String> filterClassIDSet;
    private final String ontologyClassID;
    private final boolean prescribedValue;
    private final boolean restrictToApprovedValue;
    private OrganizationSetType filterOrganizationSetType;
    private Map<Integer, OrganizationSetType> organizationRestrictions;

    public DocumentReferenceLookupHandler(DocumentReferenceEditor editor, DocumentReferenceList referenceList, String ontologyClassID, boolean prescribedValue, boolean restrictToApprovedValue)
    {
        this.container = editor.getDataRecordEditorContainer();
        this.context = container.getAccessContext();
        this.controller = new T8DefaultComponentController(new T8Context(context), this.getClass().getCanonicalName(), false);
        this.referenceList = referenceList;
        this.editor = editor;
        this.orgApi = controller.getApi(T8OrganizationClientApi.API_IDENTIFIER);
        this.ontApi = controller.getApi(T8OntologyClientApi.API_IDENTIFIER);
        this.filterHandler = new T8DataRecordValueFilterHandler(context, orgApi.getOrganizationStructure(), ontApi.getOntologyStructure());
        this.ontologyClassID = ontologyClassID;
        this.prescribedValue = prescribedValue;
        this.restrictToApprovedValue = restrictToApprovedValue;
        this.filterConceptIDSet = new HashSet<String>();
        this.filterClassIDSet = new HashSet<String>();
        this.filterClassIDSet.add(ontologyClassID);
        this.filterOrganizationSetType = OrganizationSetType.LINEAGE;
        this.organizationRestrictions = null;
    }

    private String translate(String inputString)
    {
        return editor.translate(inputString);
    }

    public void setFilterOrganizationSetType(OrganizationSetType setType)
    {
        this.filterOrganizationSetType = setType;
    }

    public void setOrganizationRestrictions(Map<Integer, OrganizationSetType> restrictions)
    {
        this.organizationRestrictions = restrictions;
    }

    public void setFilterConceptIDSet(Set<String> filterConceptIDSet)
    {
        this.filterConceptIDSet.clear();
        if (filterConceptIDSet != null)
        {
            this.filterConceptIDSet.addAll(filterConceptIDSet);
        }
    }

    public void setFilterGroupIDSet(Set<String> filterGroupIDSet)
    {
        this.filterClassIDSet.clear();
        if (filterGroupIDSet != null)
        {
            this.filterClassIDSet.addAll(filterGroupIDSet);
        }

        // Add the default ontology class if no others have been specified.
        if (this.filterClassIDSet.isEmpty())
        {
            this.filterClassIDSet.add(ontologyClassID);
        }
    }

    public void showNewSubRecordLookupDialog(List<Pair<String, String>> conceptDependencies, PropertyRequirement propertyRequirement, boolean prescribed)
    {
        Map<String, Object> selectedDialogValue;
        T8OntologyStructure ontologyStructure;
        T8OrganizationStructure orgStructure;
        List<String> orgIdList;
        List<String> ocIdList;
        List<String> conceptIdList;
        String drInstanceId;
        String drId;
        String propertyId;
        String drInstanceOrgId;
        List<T8OntologyLink> drInstanceLinks;
        T8OntologyConcept drInstanceOntology;

        // Get the standardization flag from the property requirement.
        drInstanceId = propertyRequirement.getParentDataRequirementInstance().getConceptID();
        drId = propertyRequirement.getParentDataRequirement().getConceptID();
        propertyId = propertyRequirement.getConceptID();
        drInstanceOntology = (T8OntologyConcept)propertyRequirement.getParentDataRequirementInstance().getOntology();
        if (drInstanceOntology == null) throw new RuntimeException("No Ontology Found in DR Instance object: " + propertyRequirement.getParentDataRequirementInstance());
        drInstanceLinks = drInstanceOntology.getOntologyLinks();
        if (drInstanceLinks.isEmpty()) throw new RuntimeException("No Organization Links Found in DR Instance Ontology object: " + drInstanceOntology);
        drInstanceOrgId = drInstanceLinks.get(0).getOrganizationID();

        // Get the organization structure and ontology data structure.
        ontologyStructure = ontApi.getOntologyStructure();
        orgStructure = orgApi.getOrganizationStructure();

        // Create a list of all the specific concepts to include in the filter.
        conceptIdList = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(filterConceptIDSet))
        {
            conceptIdList.addAll(filterConceptIDSet);
        }

        // Create a list of all groups to include (roots and their descendants).
        ocIdList = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(filterClassIDSet))
        {
            for (String ocId : filterClassIDSet)
            {
                // Get the list of ODT_ID's to use for retrieval.
                ocIdList.add(ocId); // Add the class itself.
                ocIdList.addAll(ontologyStructure.getDescendantClassIDList(ocId)); // Add the descendants of the class.
            }
        }

        // Create the list of accesible organization levels.
        orgIdList = orgStructure.getOrganizationIdSet(filterOrganizationSetType, context.getSessionContext().getOrganizationIdentifier()); // Add a filter to include all organizations in the filter set type.
        orgIdList.retainAll(orgStructure.getOrganizationLineageIDList(drInstanceOrgId)); // Add a filter to include only concepts linked to the record's organization lineage.

        // If a restriction on organization selection is set, apply it now.
        if (organizationRestrictions != null)
        {
            DataRecord parentRecord;

            parentRecord = referenceList.getParentDataRecord();
            for (String referencedRecordId : referenceList.getReferenceIds())
            {
                DataRecord referencedRecord;

                referencedRecord = parentRecord.findDataRecord(referencedRecordId);
                if (referencedRecord != null)
                {
                    for (T8OntologyLink ontologyLink : referencedRecord.getOntologyLinks())
                    {
                        OrganizationSetType restriction;
                        String orgId;
                        int level;

                        orgId = ontologyLink.getOrganizationID();
                        level = orgStructure.getOrganizationLevel(orgId);
                        restriction = organizationRestrictions.get(level); // Check for a restriction on the specific level.
                        if (restriction == null) restriction = organizationRestrictions.get(null); // Check for the default restriction for all levels.
                        if (restriction != null)
                        {
                            orgIdList.removeAll(orgStructure.getOrganizationIdSet(restriction, orgId));
                        }
                    }
                }
            }
        }

        // Get the selected value.
        LOGGER.log("Filter Organization Set Type: " + filterOrganizationSetType);
        LOGGER.log("Organization Restrictions: " + organizationRestrictions);
        LOGGER.log("Including organizations: " + orgIdList);
        selectedDialogValue = container.getSelectedValue(referenceList.getParentDataRecord(), referenceList.getValueRequirement(), orgIdList, ocIdList, conceptIdList, conceptDependencies);
        if (selectedDialogValue != null)
        {
            final String selectedDRInstanceID;

            // Get the selected ID's.
            selectedDRInstanceID = (String)selectedDialogValue.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_ID);

            // Start a new thread for this part because we are currently in the EDT.
            new Thread()
            {
                @Override
                public void run()
                {
                    editor.addDocumentLinkEditor(null, selectedDRInstanceID);
                }
            }.start();
        }
    }

    public void showLinkSubRecordLookupDialog(List<Pair<String, String>> conceptDependencies, PropertyRequirement propertyRequirement, boolean prescribed)
    {
        Map<String, Object> selectedDialogValue;
        T8OntologyStructure ontologyStructure;
        T8OrganizationStructure orgStructure;
        List<String> orgIdList;
        List<String> ocIdList;
        List<String> conceptIdList;
        String drInstanceId;
        String drId;
        String propertyId;
        String drInstanceOrgId;
        List<T8OntologyLink> drInstanceLinks;
        T8OntologyConcept drInstanceOntology;

        // Get the standardization flag from the property requirement.
        drInstanceId = propertyRequirement.getParentDataRequirementInstance().getConceptID();
        drId = propertyRequirement.getParentDataRequirement().getConceptID();
        propertyId = propertyRequirement.getConceptID();
        drInstanceOntology = (T8OntologyConcept)propertyRequirement.getParentDataRequirementInstance().getOntology();
        if (drInstanceOntology == null) throw new RuntimeException("No Ontology Found in DR Instance object: " + propertyRequirement.getParentDataRequirementInstance());
        drInstanceLinks = drInstanceOntology.getOntologyLinks();
        if (drInstanceLinks.isEmpty()) throw new RuntimeException("No Organization Links Found in DR Instance Ontology object: " + drInstanceOntology);
        drInstanceOrgId = drInstanceLinks.get(0).getOrganizationID();

        // Get the organization structure and ontology data structure.
        ontologyStructure = ontApi.getOntologyStructure();
        orgStructure = orgApi.getOrganizationStructure();

        // Create a list of all the specific concepts to include in the filter.
        conceptIdList = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(filterConceptIDSet))
        {
            conceptIdList.addAll(filterConceptIDSet);
        }

        // Create a list of all groups to include (roots and their descendants).
        ocIdList = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(filterClassIDSet))
        {
            for (String ocId : filterClassIDSet)
            {
                // Get the list of ODT_ID's to use for retrieval.
                ocIdList.add(ocId); // Add the class itself.
                ocIdList.addAll(ontologyStructure.getDescendantClassIDList(ocId)); // Add the descendants of the class.
            }
        }

        // Create the list of accesible organization levels.
        orgIdList = orgStructure.getOrganizationIdSet(filterOrganizationSetType, context.getSessionContext().getOrganizationIdentifier()); // Add a filter to include all organizations in the filter set type.
        orgIdList.retainAll(orgStructure.getOrganizationLineageIDList(drInstanceOrgId)); // Add a filter to include only concepts linked to the record's organization lineage.

        // If a restriction on organization selection is set, apply it now.
        if (organizationRestrictions != null)
        {
            DataRecord parentRecord;

            parentRecord = referenceList.getParentDataRecord();
            for (String referencedRecordId : referenceList.getReferenceIds())
            {
                DataRecord referencedRecord;

                referencedRecord = parentRecord.findDataRecord(referencedRecordId);
                if (referencedRecord != null)
                {
                    for (T8OntologyLink ontologyLink : referencedRecord.getOntologyLinks())
                    {
                        OrganizationSetType restriction;
                        String orgId;
                        int level;

                        orgId = ontologyLink.getOrganizationID();
                        level = orgStructure.getOrganizationLevel(orgId);
                        restriction = organizationRestrictions.get(level); // Check for a restriction on the specific level.
                        if (restriction == null) restriction = organizationRestrictions.get(null); // Check for the default restriction for all levels.
                        if (restriction != null)
                        {
                            orgIdList.removeAll(orgStructure.getOrganizationIdSet(restriction, orgId));
                        }
                    }
                }
            }
        }

        // Get the selected value.
        LOGGER.log("Filter Organization Set Type: " + filterOrganizationSetType);
        LOGGER.log("Organization Restrictions: " + organizationRestrictions);
        LOGGER.log("Including organizations: " + orgIdList);
        selectedDialogValue = container.getSelectedValue(referenceList.getParentDataRecord(), referenceList.getValueRequirement(), orgIdList, ocIdList, conceptIdList, conceptDependencies);
        if (selectedDialogValue != null)
        {
            final String selectedDrInstanceId;
            final String selectedRecordId;

            // Get the selected ID's.
            selectedDrInstanceId = (String)selectedDialogValue.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_ID);
            selectedRecordId = (String)selectedDialogValue.get(PARAMETER_VALUE_LOOKUP_OUT_RECORD_ID);

            // Start a new thread for this part because we are currently in the EDT.
            new Thread()
            {
                @Override
                public void run()
                {
                    editor.addDocumentLinkEditor(selectedRecordId, selectedDrInstanceId);
                }
            }.start();
        }
    }

    public void showDrInstanceChangeLookupDialog(DocumentLinkEditor subRecordComponent, List<Pair<String, String>> conceptDependencies, PropertyRequirement propertyRequirement, boolean prescribed)
    {
        Map<String, Object> selectedDialogValue;
        T8OntologyStructure ontologyStructure;
        T8OrganizationStructure orgStructure;
        List<String> orgIdList;
        List<String> ocIdList;
        List<String> conceptIdList;
        String drInstanceId;
        String drID;
        String propertyID;
        String drInstanceOrgID;
        List<T8OntologyLink> drInstanceLinks;
        T8OntologyConcept drInstanceOntology;

        // Get the standardization flag from the property requirement.
        drInstanceId = propertyRequirement.getParentDataRequirementInstance().getConceptID();
        drID = propertyRequirement.getParentDataRequirement().getConceptID();
        propertyID = propertyRequirement.getConceptID();
        drInstanceOntology = (T8OntologyConcept)propertyRequirement.getParentDataRequirementInstance().getOntology();
        if (drInstanceOntology == null) throw new RuntimeException("No Ontology Found in DR Instance object: " + propertyRequirement.getParentDataRequirementInstance());
        drInstanceLinks = drInstanceOntology.getOntologyLinks();
        if (drInstanceLinks.isEmpty()) throw new RuntimeException("No Organization Links Found in DR Instance Ontology object: " + drInstanceOntology);
        drInstanceOrgID = drInstanceLinks.get(0).getOrganizationID();

        // Get the organization structure and ontology data structure.
        ontologyStructure = ontApi.getOntologyStructure();
        orgStructure = orgApi.getOrganizationStructure();

        // Create a list of all the specific concepts to include in the filter.
        conceptIdList = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(filterConceptIDSet))
        {
            conceptIdList.addAll(filterConceptIDSet);
        }

        // Create a list of all groups to include (roots and their descendants).
        ocIdList = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(filterClassIDSet))
        {
            for (String ocID : filterClassIDSet)
            {
                // Get the list of ODT_ID's to use for retrieval.
                ocIdList.add(ocID); // Add the class itself.
                ocIdList.addAll(ontologyStructure.getDescendantClassIDList(ocID)); // Add the descendants of the class.
            }
        }

        // Create the list of accesible organization levels.
        orgIdList = orgStructure.getOrganizationIdSet(filterOrganizationSetType, context.getSessionContext().getOrganizationIdentifier()); // Add a filter to include all organizations in the filter set type.
        orgIdList.retainAll(orgStructure.getOrganizationLineageIDList(drInstanceOrgID)); // Add a filter to include only concepts linked to the record's organization lineage.

        // If a restriction on organization selection is set, apply it now.
        if (organizationRestrictions != null)
        {
            DataRecord parentRecord;

            parentRecord = referenceList.getParentDataRecord();
            for (String referencedRecordID : referenceList.getReferenceIds())
            {
                DataRecord referencedRecord;

                referencedRecord = parentRecord.findDataRecord(referencedRecordID);
                if (referencedRecord != null)
                {
                    for (T8OntologyLink ontologyLink : referencedRecord.getOntologyLinks())
                    {
                        OrganizationSetType restriction;
                        String orgId;
                        int level;

                        orgId = ontologyLink.getOrganizationID();
                        level = orgStructure.getOrganizationLevel(orgId);
                        restriction = organizationRestrictions.get(level); // Check for a restriction on the specific level.
                        if (restriction == null) restriction = organizationRestrictions.get(null); // Check for the default restriction for all levels.
                        if (restriction != null)
                        {
                            orgIdList.removeAll(orgStructure.getOrganizationIdSet(restriction, orgId));
                        }
                    }
                }
            }
        }

        // Get the selected value.
        LOGGER.log("Filter Organization Set Type: " + filterOrganizationSetType);
        LOGGER.log("Organization Restrictions: " + organizationRestrictions);
        LOGGER.log("Including organizations: " + orgIdList);
        selectedDialogValue = container.getSelectedValue(referenceList.getParentDataRecord(), referenceList.getValueRequirement(), orgIdList, ocIdList, conceptIdList, conceptDependencies);
        if (selectedDialogValue != null)
        {
            String selectedDRInstanceID;

            // Get the selected DR Instance ID and value requirement.
            selectedDRInstanceID = (String)selectedDialogValue.get(PARAMETER_VALUE_LOOKUP_OUT_CONCEPT_ID);
            subRecordComponent.changeDataRequirementInstance(selectedDRInstanceID);
        }
    }
}
