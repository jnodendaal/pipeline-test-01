package com.pilog.t8.ui.datarecordviewer;

import com.pilog.t8.utilities.components.table.configuredcelltable.CellConfiguredTable;
import java.awt.Component;
import java.awt.Rectangle;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class PropertyTable extends CellConfiguredTable
{
    private HeaderTable headerTable;
    
    public PropertyTable(TableModel model)
    {
        super(model);
    }
    
    public void setHeaderTable(HeaderTable headerTable)
    {
        this.headerTable = headerTable;
    }
    
    @Override
    public void autoSizeRowHeights()
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            int newRowHeight;

            // Calculate the new row height.
            newRowHeight = getRowHeight();
            
            // First find the row height needed by this table.
            for (int columnIndex = 0; columnIndex < getColumnCount(); columnIndex++)
            {
                Rectangle cellRect;
                Component comp;

                cellRect = getCellRect(rowIndex, columnIndex, true);
                comp = prepareRenderer(getCellRenderer(rowIndex, columnIndex), rowIndex, columnIndex);
                comp.setBounds(cellRect);
                newRowHeight = Math.max(newRowHeight, comp.getPreferredSize().height);
            }
            
            // Now also check the property header table.
            for (int columnIndex = 0; columnIndex < headerTable.getColumnCount(); columnIndex++)
            {
                Rectangle cellRect;
                Component comp;

                cellRect = headerTable.getCellRect(rowIndex, columnIndex, true);
                comp = headerTable.prepareRenderer(headerTable.getCellRenderer(rowIndex, columnIndex), rowIndex, columnIndex);
                comp.setBounds(cellRect);
                newRowHeight = Math.max(newRowHeight, comp.getPreferredSize().height);
            }

            // Set the row height on this table.
            setRowHeight(rowIndex, newRowHeight);
            headerTable.setRowHeight(rowIndex, newRowHeight);
        }
    }
}
