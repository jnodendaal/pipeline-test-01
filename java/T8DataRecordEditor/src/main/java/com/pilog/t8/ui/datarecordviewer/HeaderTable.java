package com.pilog.t8.ui.datarecordviewer;

import com.pilog.t8.utilities.components.table.configuredcelltable.CellConfiguredTable;
import java.awt.Component;
import java.awt.Rectangle;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class HeaderTable extends CellConfiguredTable
{
    private PropertyTable propertyTable;
    
    public HeaderTable(TableModel model)
    {
        super(model);
    }
    
    public void setPropertyTable(PropertyTable propertyTable)
    {
        this.propertyTable = propertyTable;
    }
    
    @Override
    public void autoSizeRowHeights()
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            int newRowHeight;

            // Calculate the new row height.
            newRowHeight = getRowHeight();
            
            // First find the row height needed by this table.
            for (int columnIndex = 0; columnIndex < getColumnCount(); columnIndex++)
            {
                Rectangle cellRect;
                Component comp;

                cellRect = getCellRect(rowIndex, columnIndex, true);
                comp = prepareRenderer(getCellRenderer(rowIndex, columnIndex), rowIndex, columnIndex);
                comp.setBounds(cellRect);
                newRowHeight = Math.max(newRowHeight, comp.getPreferredSize().height);
            }
            
            // Now also check the property value table.
            for (int columnIndex = 0; columnIndex < propertyTable.getColumnCount(); columnIndex++)
            {
                Rectangle cellRect;
                Component comp;

                cellRect = propertyTable.getCellRect(rowIndex, columnIndex, true);
                comp = propertyTable.prepareRenderer(propertyTable.getCellRenderer(rowIndex, columnIndex), rowIndex, columnIndex);
                comp.setBounds(cellRect);
                newRowHeight = Math.max(newRowHeight, comp.getPreferredSize().height);
            }

            // Set the row height on this table.
            setRowHeight(rowIndex, newRowHeight);
            propertyTable.setRowHeight(rowIndex, newRowHeight);
        }
    }
}
