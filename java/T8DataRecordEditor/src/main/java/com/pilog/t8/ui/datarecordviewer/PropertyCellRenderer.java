package com.pilog.t8.ui.datarecordviewer;

import com.pilog.t8.utilities.components.cellrenderers.TextAreaTableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.util.Objects;
import javax.swing.JTable;

/**
 * @author Bouwer du Preez
 */
public class PropertyCellRenderer extends TextAreaTableCellRenderer
{
    private final Color matchColor;
    private final Color mismatchColor;
    private boolean markMatch;
    private boolean markMismatch;

    public PropertyCellRenderer()
    {
        this.matchColor = new Color(176, 247, 164);
        this.mismatchColor = new Color(255, 180, 180);
        this.markMatch = true;
        this.markMismatch = true;
    }

    public void setMarkMatch(boolean markMatch)
    {
        this.markMatch = markMatch;
    }

    public void setMarkMismatch(boolean markMismatch)
    {
        this.markMismatch = markMismatch;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        TextAreaTableCellRenderer rendererComponent;
        int matchResult;

        rendererComponent = (TextAreaTableCellRenderer)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        rendererComponent.setOpaque(false);

        matchResult = rowMatch(table, row);
        if ((matchResult == 1) && (markMatch))
        {
            rendererComponent.setBackground(matchColor);
            rendererComponent.setOpaque(true);
        }
        else if ((matchResult == -1) && (markMismatch))
        {
            rendererComponent.setBackground(mismatchColor);
            rendererComponent.setOpaque(true);
        }

        return rendererComponent;
    }

    private int rowMatch(JTable table, int rowIndex)
    {
        Object matchValue;

        matchValue = null;
        for (int columnIndex = 0; columnIndex < table.getColumnCount(); columnIndex++)
        {
            Object value;

            value = table.getValueAt(rowIndex, columnIndex);
            if (columnIndex == 0) matchValue = value;
            else if (!Objects.equals(matchValue, value))
            {
                return -1;
            }
        }

        if (matchValue == null) return 0;
        else return 1;
    }
}
