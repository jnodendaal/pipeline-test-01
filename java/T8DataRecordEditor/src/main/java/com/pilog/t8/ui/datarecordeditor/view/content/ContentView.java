package com.pilog.t8.ui.datarecordeditor.view.content;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.document.datastring.T8DataString;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.component.additional.T8AdditionalDataComponent;
import com.pilog.t8.ui.datarecordeditor.component.additional.T8AdditionalDataComponent.AdditionalData;
import com.pilog.t8.ui.datarecordeditor.component.classification.T8ClassificationEditor;
import com.pilog.t8.ui.datarecordeditor.component.datastringsettings.T8ParticleSettingsEditor;
import com.pilog.t8.ui.datarecordeditor.component.description.T8DescriptiveDataComponent;
import com.pilog.t8.ui.datarecordeditor.component.reference.T8ReferenceDataComponent;
import com.pilog.t8.ui.taskpanecontainer.T8TaskPaneContainer;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class ContentView extends JPanel implements DataRecordEditor
{
    private final T8DescriptiveDataComponent descriptionComponent;
    private final T8AdditionalDataComponent additionalDataComponent;
    private final CharacteristicDataEditor characteristicDataEditor;
    private final T8ClassificationEditor classificationEditor;
    private final T8ParticleSettingsEditor particleSettingsEditor;
    private final T8ReferenceDataComponent referenceDataComponent;
    private final DataRecordEditorContainer container;
    private final T8Context context;
    private final T8ConfigurationManager configurationManager;
    private final DataRequirementInstance dataRequirementInstance;
    private final DataRecordAccessLayer accessLayer;
    private final DataRecord dataRecord;
    private final boolean fftEditable;
    private final boolean editable;

    public String ADDITIONAL_DATA_KEY_FFT = "FFT";

    public ContentView(DataRecordEditorContainer container, DataRecord dataRecord)
    {
        this.dataRecord = dataRecord;
        this.dataRequirementInstance = dataRecord.getDataRequirementInstance();
        this.accessLayer = dataRecord.getAccessLayer();
        this.context = container.getAccessContext();
        this.configurationManager = context.getClientContext().getConfigurationManager();

        initComponents();
        this.container = container;
        this.descriptionComponent = new T8DescriptiveDataComponent(container);
        this.additionalDataComponent = new T8AdditionalDataComponent(container);
        this.referenceDataComponent = new T8ReferenceDataComponent(container);
        this.characteristicDataEditor = new CharacteristicDataEditor(container, dataRecord);
        this.classificationEditor = new T8ClassificationEditor(container, dataRecord);
        this.particleSettingsEditor = new T8ParticleSettingsEditor(container, dataRecord);
        this.fftEditable = accessLayer.isFftEditable();
        this.editable = accessLayer.isEditable();

        jXTaskPaneDescriptions.setLayout(new BorderLayout());
        jXTaskPaneCharacteristicData.setLayout(new BorderLayout());
        jXTaskPaneDescriptions.add(descriptionComponent, java.awt.BorderLayout.CENTER);
        jXTaskPaneAdditionalData.add(additionalDataComponent, java.awt.BorderLayout.CENTER);
        jXTaskPaneReferenceData.add(referenceDataComponent, java.awt.BorderLayout.CENTER);
        jXTaskPaneCharacteristicData.add(characteristicDataEditor, java.awt.BorderLayout.CENTER);
        jXTaskPaneClassification.add(classificationEditor, java.awt.BorderLayout.CENTER);
        jXTaskPaneSettings.add(particleSettingsEditor, java.awt.BorderLayout.CENTER);

        init();
    }

    private void init()
    {
        List<T8DataString> descriptionHandles;
        T8OntologyConcept ontology;

        // Set the Descriptive Data.
        // If there is nothing to show, hide it
        descriptionHandles = dataRecord.getDataStrings();
        if ((descriptionHandles != null) && (descriptionHandles.size() > 0))
        {
            jXTaskPaneDescriptions.setCollapsed(false);
            descriptionComponent.setDescriptions(descriptionHandles);
        } else jXTaskPaneDescriptions.setVisible(false);

        // Set the Additional Data.
        if (dataRecord.hasFFT())
        {
            jXTaskPaneAdditionalData.setCollapsed(false);
            additionalDataComponent.setAdditionalData(ArrayLists.newArrayList(new AdditionalData(ADDITIONAL_DATA_KEY_FFT, "FFT", dataRecord.getFFT(), fftEditable && editable)));
        }
        else if (!fftEditable)
        {
            // If there is no FFT and it can't be edited, hide it
            jXTaskPaneAdditionalData.setVisible(false);
        }
        else
        {
            jXTaskPaneAdditionalData.setCollapsed(true);
            additionalDataComponent.setAdditionalData(ArrayLists.newArrayList(new AdditionalData(ADDITIONAL_DATA_KEY_FFT, "FFT", null, fftEditable && editable)));
        }

        // Set the reference data.
        ontology = dataRecord.getOntology();
        if (ontology != null)
        {
            List<T8OntologyCode> codes;

            codes = ontology.getCodes();
            referenceDataComponent.setReferenceData(codes);
            jXTaskPaneReferenceData.setCollapsed(CollectionUtilities.isNullOrEmpty(codes));
        }
        else
        {
            jXTaskPaneReferenceData.setCollapsed(true);
        }

        // Collapse the classification category if it has not visible content.
        jXTaskPaneClassification.setCollapsed(!classificationEditor.hasVisibleContent());

        // Collapse the settings category if it has not visible content.
        jXTaskPaneSettings.setCollapsed(!particleSettingsEditor.hasVisibleContent());

        // Set the scroll bar model to the top position.  The model update is executed asynchronously on the EDT
        // after all currently exectuting UI updates in order to ensure that it takes place.  This is a known scrollpane quirk.
        SwingUtilities.invokeLater(() ->
        {
            jScrollPaneDocument.getVerticalScrollBar().getModel().setValue(0);
        });
    }

    public boolean isContentValid()
    {
        return (characteristicDataEditor.getValidationErrors().isEmpty());
    }

    @Override
    public void refreshEditor()
    {
        characteristicDataEditor.refreshEditor();
        particleSettingsEditor.refreshEditor();
    }

    @Override
    public void refreshDataDependency()
    {
        characteristicDataEditor.refreshDataDependency();
    }

    @Override
    public void refreshAccess()
    {
        characteristicDataEditor.refreshAccess();
        classificationEditor.refreshAccess();
        particleSettingsEditor.refreshAccess();
    }

    @Override
    public DataRecordEditorContainer getDataRecordEditorContainer()
    {
        return container;
    }

    @Override
    public boolean hasChanges()
    {
        if (characteristicDataEditor.hasChanges()) return true;
        else return (classificationEditor.hasChanges());
    }

    @Override
    public DataRequirementInstance getRequirement()
    {
        return dataRequirementInstance;
    }

    @Override
    public boolean commitChanges()
    {
        dataRecord.setFFT(additionalDataComponent.getAdditionalDataValues().get(ADDITIONAL_DATA_KEY_FFT));
        classificationEditor.commitChanges();
        particleSettingsEditor.commitChanges();
        return characteristicDataEditor.commitChanges();
    }

    @Override
    public Value getValue()
    {
        return dataRecord;
    }

    @Override
    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    public String getRecordID()
    {
        return dataRecord != null ? dataRecord.getID() : null;
    }

    public void setPropertyFFTVisible(boolean visible)
    {
        characteristicDataEditor.setPropertyFFTVisible(visible);
    }

    @Override
    public void setValidationErrors(List<T8DataValidationError> validationErrors)
    {
        characteristicDataEditor.setValidationErrors(validationErrors);
        classificationEditor.setValidationErrors(validationErrors);
    }

    @Override
    public List<T8DataValidationError> getValidationErrors()
    {
        return characteristicDataEditor.getValidationErrors();
    }

    private String translate(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    @Override
    public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed)
    {
        return super.processKeyBinding(ks, e, condition, pressed);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPaneDocument = new javax.swing.JScrollPane();
        jXTaskPaneContainerDocument = new T8TaskPaneContainer();
        jXTaskPaneReferenceData = new org.jdesktop.swingx.JXTaskPane();
        jXTaskPaneCharacteristicData = new org.jdesktop.swingx.JXTaskPane();
        jXTaskPaneClassification = new org.jdesktop.swingx.JXTaskPane();
        jXTaskPaneDescriptions = new org.jdesktop.swingx.JXTaskPane();
        jXTaskPaneSettings = new org.jdesktop.swingx.JXTaskPane();
        jXTaskPaneAdditionalData = new org.jdesktop.swingx.JXTaskPane();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridBagLayout());

        jScrollPaneDocument.setBackground(new java.awt.Color(255, 255, 255));

        jXTaskPaneContainerDocument.setBackground(new java.awt.Color(255, 255, 255));
        org.jdesktop.swingx.VerticalLayout verticalLayout1 = new org.jdesktop.swingx.VerticalLayout();
        verticalLayout1.setGap(14);
        jXTaskPaneContainerDocument.setLayout(verticalLayout1);

        jXTaskPaneReferenceData.setTitle(translate("Reference Data"));
        jXTaskPaneContainerDocument.add(jXTaskPaneReferenceData);

        jXTaskPaneCharacteristicData.setTitle(translate("Characteristic Data"));
        jXTaskPaneContainerDocument.add(jXTaskPaneCharacteristicData);

        jXTaskPaneClassification.setTitle(translate("Classification"));
        jXTaskPaneContainerDocument.add(jXTaskPaneClassification);

        jXTaskPaneDescriptions.setTitle(translate("Descriptions"));
        jXTaskPaneContainerDocument.add(jXTaskPaneDescriptions);

        jXTaskPaneSettings.setTitle(translate("Settings"));
        jXTaskPaneContainerDocument.add(jXTaskPaneSettings);

        jXTaskPaneAdditionalData.setTitle(translate("Additional Data"));
        jXTaskPaneContainerDocument.add(jXTaskPaneAdditionalData);

        jScrollPaneDocument.setViewportView(jXTaskPaneContainerDocument);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jScrollPaneDocument, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPaneDocument;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneAdditionalData;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneCharacteristicData;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneClassification;
    private org.jdesktop.swingx.JXTaskPaneContainer jXTaskPaneContainerDocument;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneDescriptions;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneReferenceData;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneSettings;
    // End of variables declaration//GEN-END:variables
}
