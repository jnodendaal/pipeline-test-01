package com.pilog.t8.ui.datarecordeditor.view.navigation;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * @author Bouwer du Preez
 */
public class NavigationTreeNode extends DefaultMutableTreeNode
{
    private final NavigationNodeType nodeType;

    public enum NavigationNodeType {RECORD, PROPERTY};

    public NavigationTreeNode(DataRecord record)
    {
        super(record);
        this.nodeType = NavigationNodeType.RECORD;
    }

    public NavigationTreeNode(RecordProperty property)
    {
        super(property);
        this.nodeType = NavigationNodeType.PROPERTY;
    }

    public NavigationNodeType getNodeType()
    {
        return nodeType;
    }

    public DataRecord getRecord()
    {
        return nodeType == NavigationNodeType.RECORD ? (DataRecord)getUserObject() : null;
    }

    public String getRecordID()
    {
        return nodeType == NavigationNodeType.RECORD ? getRecord().getID() : null;
    }

    public RecordProperty getProperty()
    {
        return nodeType == NavigationNodeType.PROPERTY ? (RecordProperty)getUserObject() : null;
    }

    public String getPropertyID()
    {
        return nodeType == NavigationNodeType.PROPERTY ? getProperty().getPropertyID() : null;
    }

    private NavigationTreeNode getPropertyNode(String propertyID)
    {
        for (int childIndex = 0; childIndex < getChildCount(); childIndex++)
        {
            NavigationTreeNode childNode;

            childNode = (NavigationTreeNode)getChildAt(childIndex);
            if (propertyID.equals(childNode.getPropertyID()))
            {
                return childNode;
            }
        }

        return null;
    }

    private int getPropertyInsertionIndex(RecordProperty property)
    {
        int newPropertyIndex;

        newPropertyIndex = property.getPropertyRequirement().getIndexInDataRequirement();
        for (int childIndex = 0; childIndex < getChildCount(); childIndex++)
        {
            NavigationTreeNode childNode;
            int existingPropertyIndex;

            childNode = (NavigationTreeNode)getChildAt(childIndex);
            existingPropertyIndex = childNode.getProperty().getPropertyRequirement().getIndexInDataRequirement();
            if (newPropertyIndex < existingPropertyIndex) return childIndex;
        }

        // By default, this method will return the appropriate index for insertion at the end of the list of child nodes.
        return getChildCount();
    }

    public void addSubRecord(DataRecord newRecord)
    {
        if (nodeType == NavigationNodeType.RECORD)
        {
            DataRecord parentRecord;
            RecordProperty parentProperty;

            parentRecord = getRecord();
            parentProperty = parentRecord.getPropertyContainingReference(newRecord.getID());
            if (parentProperty != null)
            {
                NavigationTreeNode propertyNode;
                String propertyID;

                // Find the property node to which the record node has to be added.
                propertyID = parentProperty.getPropertyID();
                propertyNode = getPropertyNode(propertyID);

                // If the property node was not found, create it now.
                if (propertyNode == null)
                {
                    propertyNode = new NavigationTreeNode(parentProperty);
                    insert(propertyNode, getPropertyInsertionIndex(parentProperty));
                }

                // Add the record node to the property node.
                propertyNode.add(new NavigationTreeNode(newRecord));
            }
            else throw new RuntimeException("Parent property not found for reference '" + newRecord.getID() + "' on parent record: " + parentRecord);
        }
        else throw new RuntimeException("Records can only be added on 'RECORD' type nodes.");
    }
}
