package com.pilog.t8.ui.datarecordeditor.view.content;

import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecord.DataRecordEditorFactory;
import com.pilog.t8.ui.datarecordeditor.component.datatype.SetEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.attachment.AttachmentEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.BooleanEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.composite.CompositeEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.DateEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.documentreference.DocumentReferenceEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.composite.FieldEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.LocalStringEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.MeasureNumberEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.MeasureRangeEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.TextEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.StringEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.TimeEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept.ControlledConceptEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.prescribed.StandardMeasuredNumberEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.prescribed.StandardMeasuredRangeEditor;

/**
 * @author Bouwer du Preez
 */
public class RecordEditorFactory implements DataRecordEditorFactory
{
    private final DataRecordEditorContainer container;

    public RecordEditorFactory(DataRecordEditorContainer container)
    {
        this.container = container;
    }

    @Override
    public DataRecordEditor createDataRecordEditorComponent(RecordValue recordValue)
    {
        ValueRequirement valueRequirement;
        RequirementType requirementType;

        valueRequirement = recordValue.getValueRequirement();
        requirementType = valueRequirement.getRequirementType();
        switch (requirementType)
        {
            case ATTACHMENT:
                return new AttachmentEditor(container, recordValue);
            case BOOLEAN_TYPE:
                return new BooleanEditor(container, recordValue);
            case COMPLEX_TYPE:
                return new StringEditor(container, recordValue, null);
            case COMPOSITE_TYPE:
                return new CompositeEditor(container, recordValue);
            case CONTROLLED_CONCEPT:
                return new ControlledConceptEditor(container, recordValue);
            case CURRENCY_TYPE:
                return new StringEditor(container, recordValue, null);
            case DATE_TIME_TYPE:
                return new DateEditor(container, recordValue);
            case DATE_TYPE:
                return new DateEditor(container, recordValue);
            case DOCUMENT_REFERENCE:
                return new DocumentReferenceEditor(container, recordValue);
            case FIELD_TYPE:
                return new FieldEditor(container, recordValue);
            case FILE_TYPE:
                return new StringEditor(container, recordValue, null);
            case INTEGER_TYPE:
                return new StringEditor(container, recordValue, null);
            case LOCALIZED_TEXT_TYPE:
                return new SetEditor(container, recordValue);
            case LOCAL_STRING_TYPE:
                return new LocalStringEditor(container, recordValue);
            case LOWER_BOUND_NUMBER:
                return new StringEditor(container, recordValue, null);
            case MEASURED_RANGE:
            {
                Boolean prescribed;
                Boolean restrictToApprovedValue;

                // Get the standardization flags.
                prescribed = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
                restrictToApprovedValue = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));

                // If the value is standardized and restricted, use the standard value editor.
                if ((prescribed) && (restrictToApprovedValue))
                {
                    return new StandardMeasuredRangeEditor(container, recordValue);
                }
                else
                {
                    return new MeasureRangeEditor(container, recordValue);
                }
            }
            case MEASURED_NUMBER:
            {
                Boolean prescribed;
                Boolean restrictToApprovedValue;

                // Get the standardization flags.
                prescribed = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
                restrictToApprovedValue = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.RESTRICT_TO_APPROVED_VALUE.toString()));

                // If the value is standardized and restricted, use the standard value editor.
                if ((prescribed) && (restrictToApprovedValue))
                {
                    return new StandardMeasuredNumberEditor(container, recordValue);
                }
                else
                {
                    return new MeasureNumberEditor(container, recordValue);
                }
            }
            case NUMBER:
                return new StringEditor(container, recordValue, null);
            case PRESCRIBED_CURRENCY_TYPE:
                return new StringEditor(container, recordValue, null);
            case QUALIFIER_OF_MEASURE:
                return new StringEditor(container, recordValue, null);
            case UNIT_OF_MEASURE:
                return new StringEditor(container, recordValue, null);
            case RATIONAL_TYPE:
                return new StringEditor(container, recordValue, null);
            case REAL_TYPE:
                return new StringEditor(container, recordValue, null);
            case SET_TYPE:
                return new SetEditor(container, recordValue);
            case STRING_TYPE:
                return new StringEditor(container, recordValue, null);
            case TEXT_TYPE:
                return new TextEditor(container, recordValue);
            case TIME_TYPE:
                return new TimeEditor(container, recordValue);
            case UPPER_BOUND_NUMBER:
                return new StringEditor(container, recordValue, null);
            case ONTOLOGY_CLASS:
                return new StringEditor(container, recordValue, null);
            case YEAR_MONTH_TYPE:
                return new StringEditor(container, recordValue, null);
            case YEAR_TYPE:
                return new StringEditor(container, recordValue, null);
            default:
                throw new RuntimeException("Invalid data type encountered in Data Requirement: " + requirementType);
        }
    }
}
