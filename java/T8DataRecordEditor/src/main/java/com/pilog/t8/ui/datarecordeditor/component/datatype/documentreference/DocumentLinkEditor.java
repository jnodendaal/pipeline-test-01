package com.pilog.t8.ui.datarecordeditor.component.datatype.documentreference;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.event.T8DataRecordChangeListener;
import com.pilog.t8.data.document.datarecord.event.T8DataRecordChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordPropertyAddedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordPropertyValueChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordValueChangedEvent;
import com.pilog.t8.data.document.datarecord.event.T8RecordValueStructureChangedEvent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator.AttachmentRenderType;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Dimension;
import java.util.Objects;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public class DocumentLinkEditor extends JPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(DocumentLinkEditor.class);

    private final TerminologyProvider terminologyProvider;
    private final ReferenceChangeListener referenceChangeListener;
    private final DataRecordEditorContainer container;
    private final DataRecord parentRecord;
    private final String recordId;

    public DocumentLinkEditor(DataRecordEditorContainer container, DataRecord parentRecord, String recordID)
    {
        this.container = container;
        this.terminologyProvider = container.getTerminologyProvider();
        this.referenceChangeListener = new ReferenceChangeListener();
        this.parentRecord = parentRecord;
        this.recordId = recordID;
        initComponents();
        init();
    }

    private void init()
    {
        DataRecord linkRecord;

        // Add a document listener to the record so that we can update reference links if the concent of the links changes.
        parentRecord.addChangeListener(referenceChangeListener);

        // Set the reference link text.
        linkRecord = parentRecord.findDataRecord(recordId);
        if (linkRecord != null)
        {
            refreshLinkText(linkRecord);
        }
        else
        {
            String recordDefinition;

            recordDefinition = terminologyProvider.getDefinition(null, recordId);
            if (!Strings.isNullOrEmpty(recordDefinition))
            {
                jXHyperlinkDocumentReference.setText(recordDefinition);
            }
            else
            {
                jXHyperlinkDocumentReference.setText("No description");
            }
        }
    }

    public String getRecordID()
    {
        return recordId;
    }

    @Override
    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }

    @Override
    public Dimension getMinimumSize()
    {
        Dimension minimumSize;

        minimumSize = super.getMinimumSize();
        minimumSize.width = 0;
        return minimumSize;
    }

    public void changeDataRequirementInstance(final String drInstanceID)
    {
        DataRecord referencedRecord;

        // Do the DR Instance change.
        referencedRecord = container.changeDocumentDRInstance(recordId, drInstanceID);
        if (referencedRecord != null)
        {
            refreshLinkText(referencedRecord);
        }
    }

    private void editSubRecord()
    {
        if (container.containsDocument(recordId))
        {
            container.showDocument(recordId);
        }
        else
        {
            // Load the document in a new thread.
            new Thread()
            {
                @Override
                public void run()
                {
                    container.loadDocument(recordId, true);
                }
            }.start();
        }
    }

    public String refreshLinkText(final DataRecord record)
    {
        T8DataRecordDefinitionGenerator definitionGenerator;
        String newDefinition;

        // Generate a definition from the specified record.
        definitionGenerator = new T8DataRecordDefinitionGenerator(container.getTerminologyProvider());
        definitionGenerator.addExcludedRequirementType(RequirementType.BOOLEAN_TYPE);
        definitionGenerator.setAttachmentRenderType(AttachmentRenderType.FILE_NAME);
        newDefinition = definitionGenerator.generateValueString(record).toString();

        // Update the text displayed on the hyperlink.
        jXHyperlinkDocumentReference.setText(newDefinition);

        // Return the generated definition as result.
        return newDefinition;
    }

    /**
     * A change listener which is responsible for the notification that the
     * referenced document has been updated, regardless of the event from which
     * it had originated.
     */
    private class ReferenceChangeListener implements T8DataRecordChangeListener
    {
        @Override
        public void recordValueChanged(T8RecordValueChangedEvent event)
        {
            checkReferencedDocumentUpdated(event);
        }

        @Override
        public void recordPropertyAdded(T8RecordPropertyAddedEvent event)
        {
            checkReferencedDocumentUpdated(event);
        }

        @Override
        public void recordPropertyValueChanged(T8RecordPropertyValueChangedEvent event)
        {
            checkReferencedDocumentUpdated(event);
        }

        @Override
        public void recordValueStructureChanged(T8RecordValueStructureChangedEvent event)
        {
            checkReferencedDocumentUpdated(event);
        }

        @Override
        public void dataRecordChanged(T8DataRecordChangedEvent event)
        {
            checkReferencedDocumentUpdated(event);
        }

        private void checkReferencedDocumentUpdated(T8DataRecordChangedEvent event)
        {
            if (Objects.equals(recordId, event.getDataRecord().getID()) && (!event.isAdjusting()))
            {
                refreshLinkText(event.getDataRecord());
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jXHyperlinkDocumentReference = new org.jdesktop.swingx.JXHyperlink();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jXHyperlinkDocumentReference.setText("No Description");
        jXHyperlinkDocumentReference.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXHyperlinkDocumentReferenceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jXHyperlinkDocumentReference, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jXHyperlinkDocumentReferenceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXHyperlinkDocumentReferenceActionPerformed
    {//GEN-HEADEREND:event_jXHyperlinkDocumentReferenceActionPerformed
        editSubRecord();
    }//GEN-LAST:event_jXHyperlinkDocumentReferenceActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.jdesktop.swingx.JXHyperlink jXHyperlinkDocumentReference;
    // End of variables declaration//GEN-END:variables

}
