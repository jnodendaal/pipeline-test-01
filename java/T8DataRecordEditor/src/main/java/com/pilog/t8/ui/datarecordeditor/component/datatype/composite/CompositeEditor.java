package com.pilog.t8.ui.datarecordeditor.component.datatype.composite;

import com.pilog.t8.data.document.datarecord.access.layer.FieldAccessLayer;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.component.datatype.DefaultRecordValueEditor;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class CompositeEditor extends DefaultRecordValueEditor
{
    private final List<FieldHeaderComponent> fieldHeaderComponents;
    private final ArrayList<DataRecordEditor> fieldEditors;

    public CompositeEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        this.fieldHeaderComponents = new ArrayList<FieldHeaderComponent>();
        this.fieldEditors = new ArrayList<DataRecordEditor>();
        initComponents();
        init();
    }

    private void init()
    {
        int gridY;

        gridY = 0;
        removeAll();
        fieldEditors.clear();
        fieldHeaderComponents.clear();
        for (ValueRequirement fieldRequirement : valueRequirement.getSubRequirements())
        {
            FieldHeaderComponent headerComponent;
            FieldAccessLayer fieldAccessLayer;
            DataRecordEditor editorComponent;
            GridBagConstraints gridBagConstraints;
            boolean entryRequired;
            Field field;

            // Get the field from the document of create it if it does not exist.
            field = (Field)recordValue.getSubValue(RequirementType.FIELD_TYPE, fieldRequirement.getFieldID(), null);
            if (field == null)
            {
                field = new Field(fieldRequirement);
                recordValue.setSubValue(field);
            }

            // Get the field access layer and create the field editor.
            fieldAccessLayer = propertyAccessLayer.getFieldAccessLayer(fieldRequirement.getFieldID());
            entryRequired = fieldAccessLayer.isRequired();
            editorComponent = container.getEditorComponentFactory().createDataRecordEditorComponent(field);
            headerComponent = new FieldHeaderComponent(container, field, editorComponent, entryRequired);
            fieldEditors.add(editorComponent);
            fieldHeaderComponents.add(headerComponent);

            // Add the field header component to the layout.
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = gridY;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 0.0;
            gridBagConstraints.anchor = GridBagConstraints.WEST;
            gridBagConstraints.insets = new Insets(0, 5, 0, 0);
            add(headerComponent, gridBagConstraints);

            // Add the field editor component to the layout.
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 1;
            gridBagConstraints.gridy = gridY++;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 0.1;
            gridBagConstraints.insets = new Insets(0, 0, 0, 0);
            add((Component)editorComponent, gridBagConstraints);
        }

        validate();
    }

    @Override
    public void refreshEditor()
    {
        // Execute the default refreshEditor behavior.
        super.refreshEditor();

        // Set the data record on each of the fields contained by the composite type.
        for (DataRecordEditor fieldComponent : fieldEditors)
        {
            fieldComponent.refreshEditor();
        }
    }

    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();

        // Refresh access on field editors.
        for (FieldHeaderComponent fieldHeader : fieldHeaderComponents)
        {
            DataRecordEditor fieldEditor;
            FieldAccessLayer fieldAccessLayer;
            String fieldID;
            boolean visible;

            fieldEditor = fieldHeader.getFieldValueEditor();
            fieldID = ((ValueRequirement)fieldEditor.getRequirement()).getFieldID();
            fieldAccessLayer = propertyAccessLayer.getFieldAccessLayer(fieldID);
            visible = fieldAccessLayer.isVisible();

            fieldHeader.setVisible(visible);
            ((Component)fieldEditor).setVisible(visible);
            fieldHeader.refreshAccess();
            fieldEditor.refreshAccess();
        }
    }

    @Override
    public void refreshDataDependency()
    {
        for (DataRecordEditor fieldComponent : fieldEditors)
        {
            fieldComponent.refreshDataDependency();
        }
    }

    @Override
    public boolean hasChanges()
    {
        for (DataRecordEditor fieldComponent : fieldEditors)
        {
            if (fieldComponent.hasChanges()) return true;
        }

        return false;
    }

    @Override
    public boolean commitChanges()
    {
        boolean success;

        success = true;
        for (DataRecordEditor fieldComponent : fieldEditors)
        {
            if (!fieldComponent.commitChanges()) success = false;
        }

        return success;
    }

    @Override
    public void setValidationErrors(List<T8DataValidationError> validationErrors)
    {
        if (validationErrors != null)
        {
            for (FieldHeaderComponent fieldComponent : fieldHeaderComponents)
            {
                List<T8DataValidationError> fieldValidationErrors;
                FieldRequirement fieldRequirement;

                fieldRequirement = fieldComponent.getFieldRequirement();
                fieldValidationErrors = new ArrayList<T8DataValidationError>();
                for (T8DataValidationError validationError : validationErrors)
                {
                    if (validationError.isApplicableTo(fieldRequirement))
                    {
                        fieldValidationErrors.add(validationError);
                    }
                }

                fieldComponent.setValidationErrors(fieldValidationErrors);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
