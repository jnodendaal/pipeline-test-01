package com.pilog.t8.ui.datarecordeditor.view.navigation;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.ui.datarecordeditor.T8DataRecordEditor;
import com.pilog.t8.ui.datarecordeditor.view.navigation.NavigationTreeNode.NavigationNodeType;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.border.AbstractBorder;
import javax.swing.tree.TreeCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class NavigationTreeCellRenderer extends JPanel implements TreeCellRenderer
{
    private final T8DataRecordEditor parentEditor;

    private static final AbstractBorder UNSELECTED_BORDER  = new RoundedRectangleBorder(new java.awt.Color(204, 204, 204), 1, 5);
    private static final AbstractBorder SELECTED_BORDER  = new RoundedRectangleBorder(new java.awt.Color(255, 204, 0), 1, 5);

    public NavigationTreeCellRenderer(T8DataRecordEditor parentEditor)
    {
        initComponents();
        this.jLabelIcon.setVisible(false);
        this.parentEditor = parentEditor;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        NavigationTreeNode treeNode;

        treeNode = (NavigationTreeNode)value;
        if (treeNode.getNodeType() == NavigationNodeType.RECORD)
        {
            TerminologyProvider terminologyProvider;
            DataRecord dataRecord;
            Color backgroundColor;
            String drInstanceCode;
            String drInstanceTerm;
            String definition;
            String drInstanceID;
            String recordID;

            recordID = treeNode.getRecordID();
            dataRecord = treeNode.getRecord();
            drInstanceID = dataRecord.getDataRequirementInstanceID();
            terminologyProvider = parentEditor.getTerminologyProvider();
            definition = terminologyProvider.getDefinition(null, recordID);
            drInstanceTerm = terminologyProvider.getTerm(null, drInstanceID);
            drInstanceCode = terminologyProvider.getCode(drInstanceID);

            // If the DR Instance Term was not found, use the DR Term.
            if (drInstanceTerm == null)
            {
                drInstanceTerm = terminologyProvider.getTerm(null, dataRecord.getDataRequirementID());
            }

            // Check the definition for validity.
            if (!Strings.isNullOrEmpty(definition))
            {
                // We are displaying the DR Instance as the node type, so we need to remove it from the front of the definition.
                // This is a hack, as it relies on a very specified definition formatting to work correctly (a term followed by a colon).
                // TODO:  Implement proper removal of DR Instance term and any suffixes.
                if (definition.startsWith(drInstanceTerm))
                {
                    definition = definition.substring(drInstanceTerm.length());
                    if (definition.startsWith(":")) definition = definition.substring(1);
                }
            }
            else
            {
                // Set the definition to an empty string.
                definition = "";
            }

            // Determine the background color of the document.
            if (!parentEditor.isContentValid(dataRecord.getID()))
            {
                backgroundColor = LAFConstants.INVALID_PINK;
            }
            else
            {
                if (selected)
                {
                    backgroundColor = LAFConstants.SELECTION_ORANGE;
                }
                else
                {
                    backgroundColor = LAFConstants.PROPERTY_BLUE;
                }
            }

            jLabelNodeType.setText(drInstanceTerm + (drInstanceCode != null ? " (" + drInstanceCode + ")" : ""));
            jLabelNodeName.setText(definition);
            jLabelNodeName.setVisible(true);
            jPanelMain.setBackground(backgroundColor);
            jPanelMain.setBorder(selected ? SELECTED_BORDER : UNSELECTED_BORDER);

            doLayout();
            return this;
        }
        else
        {
            TerminologyProvider terminologyProvider;
            RecordProperty recordProperty;
            Color backgroundColor;
            String term;
            String propertyID;

            recordProperty = treeNode.getProperty();
            propertyID = recordProperty.getPropertyID();
            terminologyProvider = parentEditor.getTerminologyProvider();
            term = terminologyProvider.getTerm(null, propertyID);

            // Determine the background color of the document.
            if (selected)
            {
                backgroundColor = LAFConstants.SELECTION_ORANGE;
            }
            else
            {
                backgroundColor = LAFConstants.PROPERTY_KEY_HEADER_BG_COLOR;
            }

            jLabelNodeType.setText(term);
            jLabelNodeName.setVisible(false);
            jPanelMain.setBackground(backgroundColor);
            jPanelMain.setBorder(selected ? SELECTED_BORDER : UNSELECTED_BORDER);

            doLayout();
            return this;
        }
    }

    private static class RoundedRectangleBorder extends AbstractBorder
    {
        private final Color color;
        private final int thickness;
        private final int radii;
        private Insets insets = null;
        private BasicStroke stroke = null;
        private final int strokePad;
        RenderingHints hints;

        RoundedRectangleBorder(Color color, int thickness, int cornerRadius)
        {
            this.thickness = thickness;
            this.radii = cornerRadius;
            this.color = color;
            this.stroke = new BasicStroke(thickness);
            this.strokePad = thickness / 2;
            this.hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            this.insets = new Insets(2, 2, 2, 2);
        }

        @Override
        public Insets getBorderInsets(Component c)
        {
            return insets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets insets)
        {
            return getBorderInsets(c);
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height)
        {
            Graphics2D g2 = (Graphics2D) g;
            RoundRectangle2D.Double bubble;
            Component parent;
            int bottomLineY;
            Area area;

            // Create the rounded rectangle.
            bottomLineY = height - thickness;
            bubble = new RoundRectangle2D.Double(
                    0 + strokePad,
                    0 + strokePad,
                    width - thickness,
                    bottomLineY,
                    radii,
                    radii);

            area = new Area(bubble);
            g2.setRenderingHints(hints);

            // Paint the BG color of the parent, as the background of the border.
            parent = c.getParent();
            if (parent != null)
            {
                Area borderRegion;
                Rectangle rect;
                Color bg;

                bg = parent.getBackground();
                rect = new Rectangle(0, 0, width, height);
                borderRegion = new Area(rect);
                borderRegion.subtract(area);
                g2.setColor(bg);
                g2.fill(borderRegion);
            }

            // Paint the rounded rectangle border.
            g2.setColor(color);
            g2.setStroke(stroke);
            g2.draw(bubble);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jLabelIcon = new javax.swing.JLabel();
        jLabelNodeType = new javax.swing.JLabel();
        jLabelNodeName = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridBagLayout());

        jPanelMain.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jPanelMain.setLayout(new java.awt.GridBagLayout());
        jPanelMain.add(jLabelIcon, new java.awt.GridBagConstraints());

        jLabelNodeType.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabelNodeType.setText("Node Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 1, 5);
        jPanelMain.add(jLabelNodeType, gridBagConstraints);

        jLabelNodeName.setText("Node Name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 2, 5);
        jPanelMain.add(jLabelNodeName, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
        add(jPanelMain, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelIcon;
    private javax.swing.JLabel jLabelNodeName;
    private javax.swing.JLabel jLabelNodeType;
    private javax.swing.JPanel jPanelMain;
    // End of variables declaration//GEN-END:variables
}
