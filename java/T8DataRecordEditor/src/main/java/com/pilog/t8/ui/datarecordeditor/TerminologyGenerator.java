package com.pilog.t8.ui.datarecordeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator.AttachmentRenderType;
import java.util.HashSet;
import java.util.Set;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class TerminologyGenerator
{
    private final T8DataRecordEditor recordEditor;
    private final T8DataRecordDefinitionGenerator definitionGenerator;
    private GeneratorThread generatorThread;

    private static final int GENERATION_DELAY = 1000;

    public TerminologyGenerator(T8DataRecordEditor recordEditor)
    {
        this.recordEditor = recordEditor;
        this.definitionGenerator = new T8DataRecordDefinitionGenerator(recordEditor.getTerminologyProvider());
        this.definitionGenerator.addExcludedRequirementType(RequirementType.BOOLEAN_TYPE);
        this.definitionGenerator.setAttachmentRenderType(AttachmentRenderType.FILE_NAME);
    }

    public T8DataRecordDefinitionGenerator getDefinitionGenerator()
    {
        return definitionGenerator;
    }

    /**
     * This method regenerates the ontology (term & definition) of the specified
     * record as soon as possible and updates the UI to reflect these changes.
     * This method can be called from any thread and can be called as often as
     * needed - it groups requests to avoid redundant request processing.
     *
     * @param recordID The ID of the data record for which to regenerate
     * ontology.
     */
    public synchronized void regenerateDataRecordTerminology(String recordID)
    {
        // If we don't have a generator thread available, create a new one.
        if (generatorThread == null)
        {
            generatorThread = new GeneratorThread();

            // Add the record ID to the generator thread.
            generatorThread.addRecordID(recordID);
            generatorThread.start();
        }
        else
        {
            // Add the record ID to the generator thread.
            generatorThread.addRecordID(recordID);
        }
    }

    private class GeneratorThread extends Thread
    {
        private final Set<String> recordIDSet;

        private GeneratorThread()
        {
            recordIDSet = new HashSet<String>();
        }

        public void addRecordID(String recordID)
        {
            synchronized (recordIDSet)
            {
                recordIDSet.add(recordID);
            }
        }

        @Override
        public void run()
        {
            try
            {
                // Wait a while - any additional requests that are received
                // during this delay will be added to the queue and then
                // processed as a group instead of responding to each request
                // individually.
                Thread.sleep(GENERATION_DELAY);

                // Now process the entire set
                generatorThread = null; // We clear the thread reference because at this stage, the generator cannot be used for subsequent requests.

                // Now generate the ontology for this batch.
                T8Log.log("Generating definition for " + recordIDSet.size() + " records");
                for (String recordID : recordIDSet)
                {
                    DataRecord record;

                    record = recordEditor.getDocument(recordID);
                    if (record != null)
                    {
                        String newDefinition;
                        T8ConceptTerminology terminology;

                        newDefinition = definitionGenerator.generateValueString(record).toString();
                        terminology = recordEditor.getTerminologyProvider().getTerminology(null, recordID);
                        terminology.setDefinition(newDefinition);
                    }
                }

                // Repaint the parent record editor.
                SwingUtilities.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        recordEditor.refreshNavigationView(recordIDSet);
                    }
                });
            }
            catch (Exception e)
            {
                T8Log.log("Exception in definition generation thread.", e);
            }
            finally
            {
                // Make sure not to hold on to this thread.
                generatorThread = null;
            }
        }
    }
}
