package com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.QualifierOfMeasure;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import java.util.Map;

import static com.pilog.t8.ui.datarecord.DataRecordEditorContainer.*;

/**
 * @author Bouwer du Preez
 */
public class QOMConceptEditor extends ConceptEditor
{
    private final QualifierOfMeasure qom;

    public QOMConceptEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super (container, recordValue, null);
        this.qom = (QualifierOfMeasure)recordValue;
    }

    @Override
    void clearSelectedConcept()
    {
        // Remove selected value.
        dataRecord.setAdjusting(true);
        qom.setTerm(null);
        qom.setConceptID(null);
        dataRecord.setAdjusting(false, true);
    }

    @Override
    void setSelectedConcept(String conceptID)
    {
        // Set the new value on the editor.
        System.out.println("Setting new QOM: " + conceptID);
        dataRecord.setAdjusting(true);
        qom.setTerm(null);
        qom.setConceptID(conceptID);
        dataRecord.setAdjusting(false, true);
    }

    @Override
    void setSelectedTerm(String term)
    {
        // Set the new value on the editor.
        System.out.println("Setting new term: " + term);
        dataRecord.setAdjusting(true);
        qom.setTerm(term);
        qom.setConceptID(null);
        dataRecord.setAdjusting(false, true);
    }

    @Override
    T8ConceptTerminology convertDialogOutputToConceptTerminology(Map<String, Object> selectedValueFields)
    {
        T8ConceptTerminology conceptTerminology;
        String conceptID;

        // Clear the namespace from the returned values.
        selectedValueFields = T8IdentifierUtilities.stripNamespace(selectedValueFields);

        // Get the selected concept ID from the selected entity/
        conceptID = (String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_ID);
        conceptTerminology = new T8ConceptTerminology(T8OntologyConceptType.VALUE, conceptID);

        // Create the concept terminology object so that we can use it to set the selected value of this editor.
        conceptTerminology.setLanguageId(context.getSessionContext().getContentLanguageIdentifier());
        conceptTerminology.setTermId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_TERM_ID));
        conceptTerminology.setTerm((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_TERM));
        conceptTerminology.setCodeId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_CODE_ID));
        conceptTerminology.setCode((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_CODE));
        conceptTerminology.setDefinitionId((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_DEFINITION_ID));
        conceptTerminology.setDefinition((String)selectedValueFields.get(PARAMETER_VALUE_LOOKUP_OUT_QOM_DEFINITION));
        return conceptTerminology;
    }

    @Override
    public DataRecord getContextRecord()
    {
        return dataRecord.getDataFile();
    }

    @Override
    public RecordValue getTargetValue()
    {
        return recordValue;
    }
}
