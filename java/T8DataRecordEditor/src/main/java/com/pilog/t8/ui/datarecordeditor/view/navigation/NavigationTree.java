package com.pilog.t8.ui.datarecordeditor.view.navigation;

import java.util.LinkedList;
import javax.swing.JTree;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class NavigationTree extends JTree
{
    public NavigationTree(NavigationModel model)
    {
        this.setModel(model);
        this.setNodeIndentation(7);
        this.setRowHeight(-1); // Force the renderer to be queried for row height.
    }

    private void setNodeIndentation(int indent)
    {
        BasicTreeUI basicTreeUI;

        basicTreeUI = (BasicTreeUI)this.getUI();
        basicTreeUI.setRightChildIndent(indent);
    }

    public void refreshStructure()
    {
        if (treeModel != null) getModel().structureChanged();
    }

    public final void setModel(NavigationModel model)
    {
        super.setModel(model);
    }

    @Override
    public NavigationModel getModel()
    {
        return (NavigationModel)treeModel;
    }

    public void setSelectedNode(String recordID)
    {
        if (recordID != null)
        {
            if (treeModel != null)
            {
                NavigationTreeNode node;

                node = getModel().findNode(recordID);
                if (node != null)
                {
                    TreePath path;

                    path = new TreePath(node.getPath());
                    setSelectionPath(path);
                }
            }
        }
        else this.clearSelection();
    }

    public void expandAllNodes()
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            expandPath(getPathForRow(rowIndex));
        }
    }

    public void expandDescendantNodes(NavigationTreeNode node)
    {
        LinkedList<NavigationTreeNode> nodeQueue;

        nodeQueue = new LinkedList<NavigationTreeNode>();
        nodeQueue.add(node);
        while (!nodeQueue.isEmpty())
        {
            NavigationTreeNode nextNode;

            nextNode = nodeQueue.removeFirst();
            expandPath(new TreePath(nextNode.getPath()));
            for (int childIndex = 0; childIndex < nextNode.getChildCount(); childIndex++)
            {
                nodeQueue.add((NavigationTreeNode)nextNode.getChildAt(childIndex));
            }
        }
    }
}
