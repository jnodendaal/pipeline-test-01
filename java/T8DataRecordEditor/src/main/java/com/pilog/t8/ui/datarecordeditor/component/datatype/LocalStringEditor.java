package com.pilog.t8.ui.datarecordeditor.component.datatype;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.LocalizedStringValue;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.document.datarequirement.ValueRequirementListCellRenderer;
import java.util.List;

/**
 * Notes on usage of LOCALIZED_TEXT_TYPE:
 *

Data Requirement Structure:

PARENT_DATA_TYPE_ID     DATA_TYPE_ID            VALUE
---------------------------------------------------------------
                        LOCALIZED_TEXT_TYPE
LOCALIZED_TEXT_TYPE	LOCAL_STRING_TYPE
LOCAL_STRING_TYPE	STRING_TYPE
LOCAL_STRING_TYPE	LANGUAGE_TYPE           LANGUAGE_ID_1
LOCAL_STRING_TYPE	LANGUAGE_TYPE           LANGAUGE_ID_2
LOCAL_STRING_TYPE	LANGUAGE_TYPE		LANGUAGE_ID_3



Data Record Structure:

PARENT_DATA_TYPE_ID		DATA_TYPE_ID			VALUE
--------------------------------------------------------------------------------
				LOCALIZED_TEXT_TYPE
LOCALIZED_TEXT_TYPE		LOCAL_STRING_TYPE		The LOCALIZED_TEXT_TYPE may contain one or more LOCAL_STRING_TYPE values, one for each language cataloged.
LOCAL_STRING_TYPE		STRING_TYPE			Any String value.
LOCAL_STRING_TYPE		LANGUAGE_TYPE                   The language ID must link to the correct LANGUAGE_TYPE in the data requirement.
*/

/*
 * @author Bouwer du Preez
 */
public class LocalStringEditor extends DefaultRecordValueEditor
{
    private DefaultRecordValueEditor textFieldEditorComponent;

    public LocalStringEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        initComponents();
        jComboBoxLanguage.setRenderer(new ValueRequirementListCellRenderer(container.getTerminologyProvider()));
        initializeTextEditor();
        populateComboBoxes();
    }

    private void initializeTextEditor()
    {
        ValueRequirement textValueRequirement;

        textValueRequirement = getTextValueRequirement();
        if (textValueRequirement != null)
        {
            RecordValue textValue;

            textValue = RecordValue.createValue(textValueRequirement);
            textFieldEditorComponent = new StringEditor(container, textValue, null);
            jPanelTextComponent.add(textFieldEditorComponent, java.awt.BorderLayout.CENTER);
        }
        else throw new RuntimeException("No text requirement set in: " + valueRequirement);
    }

    private void populateComboBoxes()
    {
        jComboBoxLanguage.removeAllItems();
        jComboBoxLanguage.addItem(null);
        for (ValueRequirement subRequirement : valueRequirement.getDescendentRequirements(RequirementType.LANGUAGE_TYPE, null, null))
        {
            jComboBoxLanguage.addItem(subRequirement);
        }
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();

        // Set combobox editability and refreshEditor access on text editor.
        jComboBoxLanguage.setEnabled(editableAccess);
        textFieldEditorComponent.refreshAccess();
    }

    @Override
    public void refreshEditor()
    {
        RecordValue languageValue;

        // Execute the default behavior.
        super.refreshEditor();

        // Refresh the child editor.
        textFieldEditorComponent.refreshEditor();

        // Set the selected Language
        languageValue = recordValue.getDescendantValue(RequirementType.LANGUAGE_TYPE, null, null);
        jComboBoxLanguage.setSelectedItem(languageValue.getValueRequirement());
    }

    @Override
    public boolean commitChanges()
    {
        ValueRequirement selectedLanguage;

        // Commit child editor changes.
        textFieldEditorComponent.commitChanges();

        // Commit the UOM change.
        selectedLanguage = getSelectedLanguageRequirement();
        if (selectedLanguage != null)
        {
            recordValue.setSubValue(new LocalizedStringValue(selectedLanguage));
        }
        else
        {
            recordValue.removeSubValues(RequirementType.LANGUAGE_TYPE, null, null);
        }

        // Return the result.
        return selectedLanguage != null;
    }

    private ValueRequirement getTextValueRequirement()
    {
        List<ValueRequirement> valueRequirements;

        valueRequirements = valueRequirement.getDescendentRequirements(RequirementType.STRING_TYPE, null, null);
        return valueRequirements.size() > 0 ? valueRequirements.get(0) : null;
    }

    private ValueRequirement getSelectedLanguageRequirement()
    {
        return (ValueRequirement)jComboBoxLanguage.getSelectedItem();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelTextComponent = new javax.swing.JPanel();
        jComboBoxLanguage = new javax.swing.JComboBox();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelTextComponent.setOpaque(false);
        jPanelTextComponent.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelTextComponent, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jComboBoxLanguage, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox jComboBoxLanguage;
    private javax.swing.JPanel jPanelTextComponent;
    // End of variables declaration//GEN-END:variables

}
