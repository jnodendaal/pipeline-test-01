package com.pilog.t8.ui.datarecordeditor.view.navigation;

import com.pilog.t8.ui.datarecordeditor.T8DataRecordEditor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class NavigationTreeView extends javax.swing.JPanel
{
    private final NavigationTree navigationTree;
    private final T8DataRecordEditor parentEditor;
    private final NavigationTreeModelListener modelListener;
    private final NavigationSelectionListener selectionListener;
    private boolean selectionAdjusting;
    private NavigationMode navigationMode;
    private BreadCrumbView breadCrumbView;
    private enum NavigationMode {TREE, BREAD_CRUMBS};

    public NavigationTreeView(T8DataRecordEditor parentEditor)
    {
        initComponents();
        this.parentEditor = parentEditor;
        this.navigationTree = new NavigationTree(null);
        this.navigationTree.setRowHeight(-1);
        this.selectionListener = new NavigationSelectionListener();
        this.navigationTree.addTreeSelectionListener(selectionListener);
        this.navigationTree.setCellRenderer(new NavigationTreeCellRenderer(parentEditor));
        this.modelListener = new NavigationTreeModelListener();
        this.setBackground(Color.white);
        this.selectionAdjusting = false;
        showTreeView();
    }

    @Override
    public Dimension getMinimumSize()
    {
        if (navigationMode == NavigationMode.TREE)
        {
            return navigationTree.getPreferredSize();
        }
        else return super.getMinimumSize();
    }

    public void setBreadCrumbView(BreadCrumbView view)
    {
        this.breadCrumbView = view;
        if (breadCrumbView != null) breadCrumbView.setNavigationTree(navigationTree);
    }

    public void setNavigationModel(NavigationModel model)
    {
        if (model != null) model.addTreeModelListener(modelListener);
        navigationTree.setModel(model);
        revalidate();
    }

    public void setSelectedNode(String recordID)
    {
        if (!selectionAdjusting)
        {
            navigationTree.setSelectedNode(recordID);
            if (breadCrumbView != null) breadCrumbView.updateBreadCrumbs();
        }
    }

    public final void showTreeView()
    {
        JScrollPane scrollPane;

        scrollPane = new JScrollPane(navigationTree);
        scrollPane.setPreferredSize(new Dimension(150, 150));

        removeAll();
        add(scrollPane, BorderLayout.CENTER);
        navigationMode = NavigationMode.TREE;
        revalidate();
        repaint();
    }

    private class NavigationSelectionListener implements TreeSelectionListener
    {
        @Override
        public void valueChanged(TreeSelectionEvent e)
        {
            TreePath selectedPath;

            // Set a flag to prevent duplication of selection changes.
            selectionAdjusting = true;

            // Get the selection path.
            selectedPath = e.getNewLeadSelectionPath();
            if (selectedPath != null)
            {
                NavigationTreeNode selectedNode;

                selectedNode = (NavigationTreeNode)selectedPath.getLastPathComponent();
                parentEditor.showDocument(selectedNode.getRecordID());
            }

            // Update the breadcrumbs.
            if (breadCrumbView != null) breadCrumbView.updateBreadCrumbs();

            // Set the flag back to default to allow subsequent selection changes.
            selectionAdjusting = false;
        }
    }

    private class NavigationTreeModelListener implements TreeModelListener
    {
        @Override
        public void treeNodesChanged(TreeModelEvent e)
        {
        }

        @Override
        public void treeNodesInserted(TreeModelEvent e)
        {
            navigationTree.expandPath(e.getTreePath());
        }

        @Override
        public void treeNodesRemoved(TreeModelEvent e)
        {
        }

        @Override
        public void treeStructureChanged(TreeModelEvent e)
        {
            TreePath path;

            path = e.getTreePath();
            if (path != null)
            {
                navigationTree.expandDescendantNodes((NavigationTreeNode)path.getLastPathComponent());
            }
        }
    }

    public void expandAllNodes()
    {
        navigationTree.expandAllNodes();
    }

    public void refreshStructure()
    {
        TreePath selectionPath;

        // First we remove the selection listener to ensure that no unnecessary events are dispatched.
        navigationTree.removeTreeSelectionListener(selectionListener);

        // Now refresh the structure, making sure that the previously selected node stays selected.
        selectionPath = navigationTree.getSelectionPath();
        navigationTree.refreshStructure();
        navigationTree.expandAllNodes();
        navigationTree.setSelectionPath(selectionPath);

        // Now add the listener that we removed back.
        navigationTree.addTreeSelectionListener(selectionListener);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
