package com.pilog.t8.ui.datarecordviewer.model;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordContent;
import com.pilog.t8.definition.ui.datarecordviewer.T8ComparisonType;
import com.pilog.t8.utilities.components.table.configuredcelltable.CellConfiguredTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordTableModel extends CellConfiguredTableModel
{
    public T8DataRecordTableModel(List<T8ComparisonType> comparisonTypes, TerminologyProvider terminologyProvider, List<String> columnLabels, List<RecordContent> rootRecords)
    {
        super(columnLabels, 0);
        constructModel(comparisonTypes, terminologyProvider, rootRecords);
    }

    private void constructModel(List<T8ComparisonType> comparisonTypes, TerminologyProvider terminologyProvider, List<RecordContent> rootRecords)
    {
        T8DataRecordTableStructure tableStructure;
        List<String> rootRecordSequence;

        tableStructure = new T8DataRecordTableStructure(comparisonTypes, terminologyProvider);
        for (RecordContent rootRecord : rootRecords)
        {
            tableStructure.addRecord(rootRecord);
            for (RecordContent descendant : rootRecord.getDescendantRecords())
            {
                tableStructure.addRecord(descendant);
            }
        }

        rootRecordSequence = new ArrayList<>();
        for (RecordContent rootRecord : rootRecords)
        {
            rootRecordSequence.add(rootRecord.getId());
        }

        tableStructure.addDataRecordRowsToModel(this, rootRecordSequence);
    }

    @Override
    public boolean isCellEditable(int row, int column)
    {
        // All cells non editable.
        return false;
    }
}
