package com.pilog.t8.ui.datarecordviewer.model;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordContent;
import com.pilog.t8.definition.ui.datarecordviewer.T8ComparisonType;
import com.pilog.t8.utilities.components.table.configuredcelltable.CellConfiguration;
import com.pilog.t8.utilities.components.table.configuredcelltable.CellConfiguredTableModel;
import com.pilog.t8.utilities.date.Dates;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public abstract class T8AbstractComparisonType implements T8ComparisonType
{
    protected final List<T8DataRecordGroup> dataRecordGroups;
    protected final TerminologyProvider terminologyProvider;

    public static final Color INSTANCE_HEADER_BG_COLOR = new Color(196, 196, 255);
    public static final Color PROPERTY_HEADER_BG_COLOR = new Color(215, 215, 255);
    private static final Font NON_KEY_FONT = new java.awt.Font("Tahoma", 0, 11);

    public T8AbstractComparisonType(TerminologyProvider terminologyProvider)
    {
        this.dataRecordGroups = new ArrayList<>();
        this.terminologyProvider = terminologyProvider;
    }

    public abstract String getGroupDisplayName();

    @Override
    public void clear()
    {
        dataRecordGroups.clear();
    }

    @Override
    public abstract boolean matchesRecord(RecordContent record);

    @Override
    public void addDataRecord(RecordContent record)
    {
        if (!matchesRecord(record))
        {
            throw new RuntimeException("Data Record '" + record + "' does not match group: " + this);
        }
        else
        {
            T8DataRecordGroup bestMatchingGroup;
            T8DataRecordGroup firstOpenGroup; // A group that does not yet contain a record from the same root as the record being added.
            int bestMatchScore;

            // Find the record group where the new records fits the best.
            bestMatchScore = 0;
            bestMatchingGroup = null;
            firstOpenGroup = null;
            for (T8DataRecordGroup recordGroup : dataRecordGroups)
            {
                int score;

                score = recordGroup.getMatchScore(record);
                if ((score == -1) && (firstOpenGroup == null))
                {
                    firstOpenGroup = recordGroup;
                }
                else if (score > bestMatchScore)
                {
                    bestMatchingGroup = recordGroup;
                    bestMatchScore = score;
                }
            }

            // If we found a group, add the record to it and if we are replacing a record already in the group, add it to another group.
            if ((bestMatchScore > 0) && (bestMatchingGroup != null))
            {
                RecordContent replacedRecord;

                replacedRecord = bestMatchingGroup.addDataRecord(record);
                if (replacedRecord != null) addDataRecord(replacedRecord);
            }
            else if (firstOpenGroup != null)
            {
                // We couldn't find a good match, but this group still has room for a record from the this root, so add it here.
                firstOpenGroup.addDataRecord(record);
            }
            else // We didn't find a match or an open group, so we add a new group with the new record as the first in the group.
            {
                T8DataRecordGroup newGroup;

                //TODO: GBO - Fix the date format pattern to be configurable
                newGroup = new T8DataRecordGroup(terminologyProvider, Dates.STD_SYS_TS_FORMAT);
                newGroup.addDataRecord(record);
                dataRecordGroups.add(newGroup);
            }
        }
    }

    public void addToModel(CellConfiguredTableModel model, List<String> rootRecordSequence)
    {
        for (T8DataRecordGroup recordGroup : dataRecordGroups)
        {
            addDataRecordRowsToModel(model, rootRecordSequence, recordGroup);
        }
    }

    private void addDataRecordRowsToModel(CellConfiguredTableModel model, List<String> rootRecordSequence, T8DataRecordGroup recordGroup)
    {
        CellConfiguration cellConfiguration;
        List<String> propertyIdList;
        int startRowIndex;
        int endRowIndex;

        // Get the row index where this group will be added to the model.
        startRowIndex = model.getRowCount();
        endRowIndex = startRowIndex;
        cellConfiguration = model.getCellConfiguration();

        // Add all of the properties contained by the record group to the model.
        propertyIdList = recordGroup.getPropertyIds();
        if (!propertyIdList.isEmpty())
        {
            for (String propertyId : propertyIdList)
            {
                String[] rowData;
                List<String> propertyValues;

                // Generate the property values for this record group.
                propertyValues = recordGroup.generatePropertyValues(rootRecordSequence, propertyId);

                rowData = new String[2 + propertyValues.size()];
                rowData[0] = getGroupDisplayName();
                rowData[1] = terminologyProvider.getTerm(null, propertyId);

                // Add the property values to the data row.
                for (int index = 0; index < propertyValues.size(); index++)
                {
                    rowData[2 + index] = propertyValues.get(index);
                }

                // Add the row to the model and configure the cells.
                model.addRow(rowData);
                cellConfiguration.setBackground(PROPERTY_HEADER_BG_COLOR, endRowIndex, 1);
                cellConfiguration.setForeground(Color.GRAY, endRowIndex, 1);
                cellConfiguration.setFont(NON_KEY_FONT, endRowIndex, 1);
                endRowIndex++;
            }

            // Set the cell spanning of the DR Instance cell to cover the entire height of all the properties added.
            cellConfiguration.combine(startRowIndex, 0, propertyIdList.size(), 1);
            cellConfiguration.setBackground(INSTANCE_HEADER_BG_COLOR, startRowIndex, 0);
        }
    }
}
