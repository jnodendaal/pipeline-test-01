package com.pilog.t8.ui.datarecordeditor;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;

/**
 * @author Bouwer du Preez
 */
public interface AutoCompletableEditor
{
    public DataRecord getContextRecord();
    public RecordValue getTargetValue();
    public void setAutoCompletedValue(RecordValue value);
    public void setAutoCompletionState(boolean valid, Object currentSelection);
}
