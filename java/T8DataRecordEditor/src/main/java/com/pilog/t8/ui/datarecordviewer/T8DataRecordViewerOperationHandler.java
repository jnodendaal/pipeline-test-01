package com.pilog.t8.ui.datarecordviewer;

import com.pilog.t8.definition.ui.datarecordviewer.T8DataRecordViewerAPIHandler;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordViewerOperationHandler
{
    private final T8DataRecordViewer recordViewer;

    public T8DataRecordViewerOperationHandler(T8DataRecordViewer recordEditor)
    {
        this.recordViewer = recordEditor;
    }

    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        try
        {
            if (operationIdentifier.equals(T8DataRecordViewerAPIHandler.OPERATION_LOAD_RECORDS))
            {
                List<String> dataFileIDList;
                Map<String, String> displayHeaders;
                Boolean clearCurrentRecords;

                // Get the required parameters.
                dataFileIDList = (List<String>)operationParameters.get(T8DataRecordViewerAPIHandler.PARAMETER_RECORD_ID_LIST);
                displayHeaders = (Map<String, String>)operationParameters.get(T8DataRecordViewerAPIHandler.PARAMETER_DATA_FILE_HEADER_MAP);

                // Clear the current records if specified.
                clearCurrentRecords = (Boolean)operationParameters.get(T8DataRecordViewerAPIHandler.PARAMETER_CLEAR_CURRENT_RECORDS);
                if ((clearCurrentRecords == null) || (clearCurrentRecords)) recordViewer.clear();

                // Load the new records into the viewer.
                recordViewer.loadDataRecords(dataFileIDList, displayHeaders, null);
                return null;
            }
            else if (operationIdentifier.equals(T8DataRecordViewerAPIHandler.OPERATION_LOAD_ROOT_RECORD_HISTORY_SNAPSHOTS))
            {
                Boolean clearCurrentRecords;
                List<String> txIidList;
                String fileId;

                fileId = (String)operationParameters.get(T8DataRecordViewerAPIHandler.PARAMETER_ROOT_RECORD_ID);
                txIidList = (List<String>)operationParameters.get(T8DataRecordViewerAPIHandler.PARAMETER_TRANSACTION_ID_LIST);
                clearCurrentRecords = (Boolean)operationParameters.get(T8DataRecordViewerAPIHandler.PARAMETER_CLEAR_CURRENT_RECORDS);

                // Load the history snapshots.
                if ((clearCurrentRecords == null) || (clearCurrentRecords)) recordViewer.clear();
                recordViewer.loadFileHistory(fileId, txIidList);
                return null;
            }
            else throw new RuntimeException("Invalid Operation Identifier: '" + operationIdentifier + "'.");
        }
        catch (Exception e)
        {
            throw new RuntimeException("Operation Exception: '" + operationIdentifier + "'.", e);
        }
    }
}
