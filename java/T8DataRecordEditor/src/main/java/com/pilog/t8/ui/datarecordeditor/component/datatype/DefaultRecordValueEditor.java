package com.pilog.t8.ui.datarecordeditor.component.datatype;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.access.layer.FieldAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.ValueAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.data.document.datarequirement.DataDependency;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.security.T8Context;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * @author Bouwer du Preez
 */
public abstract class DefaultRecordValueEditor extends JPanel implements DataRecordEditor
{
    protected DataRecordEditorContainer container;
    protected final CachedTerminologyProvider terminologyProvider;
    protected T8Context context;
    protected T8ConfigurationManager configurationManager;
    protected List<T8DataValidationError> requirementValidationErrors;
    protected DataRecord dataRecord;
    protected RecordValue recordValue;
    protected PropertyRequirement propertyRequirement;
    protected FieldRequirement fieldRequirement;
    protected ValueRequirement valueRequirement; // The requirement to which the editor is constructed.
    protected PropertyAccessLayer propertyAccessLayer;
    protected FieldAccessLayer fieldAccessLayer;
    protected ValueAccessLayer valueAccessLayer;
    protected List<DataDependency> dataDependencies;
    protected boolean editableAccess;
    protected boolean insertEnabled;
    protected boolean updateEnabled;
    protected boolean deleteEnabled;

    public DefaultRecordValueEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        this.dataRecord = recordValue.getParentDataRecord();
        this.recordValue = recordValue;
        this.valueRequirement = recordValue.getValueRequirement();
        this.fieldRequirement = valueRequirement.getFieldRequirement();
        this.propertyRequirement = valueRequirement.getParentPropertyRequirement();
        this.propertyAccessLayer = dataRecord.getAccessLayer().getPropertyAccessLayer(recordValue.getPropertyID());
        this.fieldAccessLayer = dataRecord.getAccessLayer().getFieldAccessLayer(recordValue.getPropertyID(), recordValue.getFieldID());
        this.valueAccessLayer = fieldAccessLayer != null ? fieldAccessLayer.getValueAccessLayer() : propertyAccessLayer.getValueAccessLayer();

        // Get the configuration manager before initializing the components to translate UI correctly
        this.container = container;
        this.context = container.getAccessContext();
        this.configurationManager = context.getClientContext().getConfigurationManager();
        this.terminologyProvider = container.getTerminologyProvider();
        this.requirementValidationErrors = new ArrayList<T8DataValidationError>();
        this.dataDependencies = valueRequirement.getDataDependencies();

        // Initialize the UI.
        initComponents();

        // Fet the access values applicable to all value types.
        if (fieldAccessLayer != null)
        {
            this.editableAccess = fieldAccessLayer.isEditable();
            this.insertEnabled = fieldAccessLayer.isInsertEnabled();
            this.updateEnabled = fieldAccessLayer.isUpdateEnabled();
            this.deleteEnabled = fieldAccessLayer.isDeleteEnabled();
        }
        else
        {
            this.editableAccess = propertyAccessLayer.isEditable();
            this.insertEnabled = propertyAccessLayer.isInsertEnabled();
            this.updateEnabled = propertyAccessLayer.isUpdateEnabled();
            this.deleteEnabled = propertyAccessLayer.isDeleteEnabled();
        }
    }

    @Override
    public ValueRequirement getRequirement()
    {
        return valueRequirement;
    }

    @Override
    public Value getValue()
    {
        return recordValue;
    }

    @Override
    public DataRecordEditorContainer getDataRecordEditorContainer()
    {
        return container;
    }

    @Override
    public void refreshEditor()
    {
    }

    @Override
    public void refreshAccess()
    {
        // Refresh access settings on this node.
        if (fieldAccessLayer != null)
        {
            this.editableAccess = fieldAccessLayer.isEditable();
            this.insertEnabled = fieldAccessLayer.isInsertEnabled();
            this.updateEnabled = fieldAccessLayer.isUpdateEnabled();
            this.deleteEnabled = fieldAccessLayer.isDeleteEnabled();
        }
        else
        {
            this.editableAccess = propertyAccessLayer.isEditable();
            this.insertEnabled = propertyAccessLayer.isInsertEnabled();
            this.updateEnabled = propertyAccessLayer.isUpdateEnabled();
            this.deleteEnabled = propertyAccessLayer.isDeleteEnabled();
        }
    }

    @Override
    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    @Override
    public void setValidationErrors(List<T8DataValidationError> validationErrors)
    {
        // Nothing to do here.
    }

    @Override
    public List<T8DataValidationError> getValidationErrors()
    {
        List<T8DataValidationError> validationErrors;

        validationErrors = new ArrayList<T8DataValidationError>(this.requirementValidationErrors);
        return validationErrors;
    }

    public void clearValidationErrors()
    {
        requirementValidationErrors.clear();
    }

    protected String translate(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    @Override
    public void refreshDataDependency()
    {
        // Currently no default behavior. TODO:  Implement dependency check for editability only.
    }

//    @Override
//    public void refreshDataDependency()
//    {
//        Map<DataDependency, String> requiredValues;
//
//        // Find the value for each requirement.
//        requiredValues = recordValue.getDependencyConceptIDs(container.getDataRecordProvider());
//        if (requiredValues.containsValue(null))
//        {
//            editableAccess = false;
//        }
//        else
//        {
//            editableAccess = (fieldAccessLayer != null ? fieldAccessLayer.isEditable() : propertyAccessLayer.isEditable());
//        }
//    }

    @Override
    public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed)
    {
        return super.processKeyBinding(ks, e, condition, pressed);
    }

    protected String createDependentValueString(List<? extends RecordValue> dependentValues)
    {
        StringBuffer valueString;

        valueString = new StringBuffer();
        for (RecordValue dependentValue : dependentValues)
        {
            String drInstanceId;
            String propertyId;
            String fieldId;

            drInstanceId = dependentValue.getDataRequirementInstanceID();
            propertyId = dependentValue.getPropertyID();
            fieldId = dependentValue.getFieldID();

            valueString.append(terminologyProvider.getTerm(null, drInstanceId));
            valueString.append("->");
            valueString.append(terminologyProvider.getTerm(null, propertyId));

            if (fieldId != null)
            {
                valueString.append("->");
                valueString.append(terminologyProvider.getTerm(null, fieldId));
            }

            valueString.append("\n");
        }

        return valueString.toString();
    }

    protected String createLocalReferenceValueString(String referencedRecordId)
    {
        StringBuffer valueString;

        valueString = new StringBuffer();
        for (DataRecord localRecord : dataRecord.getDataFileRecords())
        {
            for (RecordProperty referencingProperty : localRecord.getPropertiesContainingReference(referencedRecordId))
            {
                ValueRequirement referenceRequirement;

                // Make sure to only include independent local references.
                referenceRequirement = referencingProperty.getRecordValue().getValueRequirement();
                if ((referenceRequirement instanceof DocumentReferenceRequirement) && ((DocumentReferenceRequirement)referenceRequirement).isLocal())
                {
                    String drInstanceId;
                    String propertyId;

                    drInstanceId = localRecord.getDataRequirementInstanceID();
                    propertyId = referencingProperty.getPropertyID();

                    valueString.append(terminologyProvider.getTerm(null, drInstanceId));
                    valueString.append("->");
                    valueString.append(terminologyProvider.getTerm(null, propertyId));

                    valueString.append("\n");
                }
            }
        }

        return valueString.toString();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
