package com.pilog.t8.ui.datarecordeditor;

import com.pilog.t8.api.T8DataRecordClientApi;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.utilities.components.textautocompleter.TextAutoCompleter;
import java.util.List;
import javax.swing.text.JTextComponent;

/**
 * @author Bouwer du Preez
 */
public class ValueAutoCompleter extends TextAutoCompleter
{
    private static final int MAX_LIST_SIZE = 20;

    private final T8DataRecordClientApi recApi;
    private final AutoCompletableEditor editor;
    private final CachedTerminologyProvider terminologyProvider;

    public ValueAutoCompleter(AutoCompletableEditor editor, JTextComponent textComponent, CachedTerminologyProvider terminologyProvider, T8DataRecordClientApi recApi)
    {
        super(textComponent);
        this.editor = editor;
        this.recApi = recApi;
        this.terminologyProvider = terminologyProvider;
        this.list.setCellRenderer(new RecordValueCellRenderer(terminologyProvider));
    }

    @Override
    protected boolean updateListData()
    {
        DataRecord contextRecord;
        RecordValue targetValue;
        String targetRecordId;
        String targetPropertyId;
        String targetFieldId;
        String targetDataTypeId;
        String searchPrefix;

        // Get the currently entered string in the text field.
        searchPrefix = textComponent.getText();
        contextRecord = editor.getContextRecord();
        targetValue = editor.getTargetValue();
        targetRecordId = targetValue.getRecordID();
        targetPropertyId = targetValue.getPropertyID();
        targetFieldId = targetValue.getFieldID();
        targetDataTypeId = targetValue.getRequirementTypeID();

        // Retrieve the standard values matching the entered text.
        try
        {
            List<RecordValue> matchedValues;

            // Retrieve the matched value suggestions.
            matchedValues = recApi.retrieveValueSuggestions(contextRecord, targetRecordId, targetPropertyId, targetFieldId, targetDataTypeId, searchPrefix, 0, MAX_LIST_SIZE);

            // Add the terminology for all suggested values to the local provider.
            for (RecordValue matchedValue : matchedValues)
            {
                matchedValue.addContentTerminology(terminologyProvider);
            }

            // Set the new data on the list.
            list.setListData(matchedValues.toArray());
            return true;
        }
        catch (Exception e)
        {
            T8Log.log("Exception while retrieving value suggestions for context record '" + contextRecord + "', target path '" + targetRecordId + "' and string prefix '" + searchPrefix + "'.", e);
            return false;
        }
    }

    @Override
    protected void listItemSelected(Object selectedObject)
    {
        editor.setAutoCompletedValue((RecordValue)selectedObject);
    }

    @Override
    protected void listDataUpdated()
    {
        if (list.getModel().getSize() > 0)
        {
            editor.setAutoCompletionState(true, list.getSelectedValue());
        }
        else
        {
            editor.setAutoCompletionState(false, null);
        }
    }
}
