package com.pilog.t8.ui.datarecordeditor.component.datastringsettings;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.ParticleSettingsAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ParticleSettingsEditor extends javax.swing.JPanel
{
    private final DataRecordEditorContainer container;
    private final DataRecord dataRecord;
    private final DataRecordAccessLayer accessLayer;
    private final List<T8ParticleSettingsHeaderComponent> headerComponents;
    private final List<T8ParticleSettingsValueComponent> valueComponents;
    private final List<T8DataStringParticleSettings> particleSettings;

    public T8ParticleSettingsEditor(DataRecordEditorContainer container, DataRecord dataRecord)
    {
        initComponents();

        this.container = container;
        this.dataRecord = dataRecord;
        this.accessLayer = dataRecord.getAccessLayer();
        this.headerComponents = new ArrayList<T8ParticleSettingsHeaderComponent>();
        this.valueComponents = new ArrayList<T8ParticleSettingsValueComponent>();
        this.particleSettings = new ArrayList<T8DataStringParticleSettings>();
        setAvailableParticleSettings();
        rebuildContent();
    }

    public boolean hasVisibleContent()
    {
        for (ParticleSettingsAccessLayer particleAccessLayer : accessLayer.getParticleSettingsAccessLayers())
        {
            if (particleAccessLayer.isVisible()) return true;
        }

        return false;
    }

    private boolean setAvailableParticleSettings()
    {
        List<T8DataStringParticleSettings> recordParticleSettings;
        List<T8DataStringParticleSettings> editorParticleSettings;

        // Get the list of currently active particle settings on the editor.
        editorParticleSettings = new ArrayList<>(particleSettings);

        // Clear the list of editor particle settings, so that it can be refreshed.
        particleSettings.clear();

        // Add all particles for which we have an access layer.
        recordParticleSettings = dataRecord.getDataStringParticleSettings();
        for (ParticleSettingsAccessLayer particleAccessLayer : accessLayer.getParticleSettingsAccessLayers())
        {
            if (particleAccessLayer.isVisible())
            {
                boolean exists;

                exists = false;
                for (T8DataStringParticleSettings particle : recordParticleSettings)
                {
                    if (particleAccessLayer.isApplicableTo(particle))
                    {
                        particleSettings.add(particle);
                        exists = true;
                        break;
                    }
                }

                if (!exists)
                {
                    for (T8DataStringParticleSettings particle : editorParticleSettings)
                    {
                        if (particleAccessLayer.isApplicableTo(particle))
                        {
                            particleSettings.add(particle);
                            exists = true;
                            break;
                        }
                    }
                }

                if (!exists)
                {
                    T8DataStringParticleSettings newParticle;

                    newParticle = new T8DataStringParticleSettings(T8IdentifierUtilities.createNewGUID());
                    newParticle.setRecordId(dataRecord.getID());
                    newParticle.setOrgId(particleAccessLayer.getOrgId());
                    newParticle.setDsTypeId(particleAccessLayer.getDsTypeId());
                    if (particleAccessLayer.isApplicableTo(newParticle))
                    {
                        particleSettings.add(newParticle);
                    }
                    else throw new RuntimeException("New particle not applicable to access layer for which it was created: " + newParticle + ", " + particleAccessLayer);
                }
            }
        }

        // Return true if we made changes to the applicable particle collection.
        return (((particleSettings.size() != editorParticleSettings.size())) || (!particleSettings.containsAll(editorParticleSettings)));
    }

    private T8ParticleSettingsValueComponent getEditor(T8DataStringParticleSettings settings)
    {
        for (T8ParticleSettingsValueComponent valueComponent : valueComponents)
        {
            if (valueComponent.getParticleSettings() == settings)
            {
                return valueComponent;
            }
        }

        return null;
    }

    public void refreshAccess()
    {
    }

    public void refreshEditor()
    {
        // Only rebuild the content of the UI if the available particle settings changed.
        if (setAvailableParticleSettings())
        {
            rebuildContent();
        }
    }

    public boolean commitChanges()
    {
        dataRecord.clearDataStringParticleSettings();
        for (T8DataStringParticleSettings settings : particleSettings)
        {
            T8ParticleSettingsValueComponent editor;

            editor = getEditor(settings);
            if (!editor.isDeleted())
            {
                dataRecord.addDataStringParticleSettings(settings);
            }
        }

        return true;
    }

    private void rebuildContent()
    {
        int gridY;

        T8Log.log("Rebuilding particle settings editor for record: " + dataRecord);

        gridY = 0;
        jPanelContent.removeAll();
        headerComponents.clear();
        valueComponents.clear();
        for (T8DataStringParticleSettings settings : particleSettings)
        {
            T8ParticleSettingsValueComponent valueComponent;
            T8ParticleSettingsHeaderComponent headerComponent;
            GridBagConstraints gridBagConstraints;
            boolean newSettings;

            newSettings = !dataRecord.containsDataStringParticleSettings(settings);
            valueComponent = new T8ParticleSettingsValueComponent(container, dataRecord, settings, newSettings);
            headerComponent = new T8ParticleSettingsHeaderComponent(container, valueComponent, settings);
            headerComponents.add(headerComponent);
            valueComponents.add(valueComponent);

            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = gridY;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 0.0;
            gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
            jPanelContent.add(headerComponent, gridBagConstraints);

            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 1;
            gridBagConstraints.gridy = gridY++;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 0.1;
            jPanelContent.add(valueComponent, gridBagConstraints);
        }

        jPanelContent.validate();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridBagLayout());

        jPanelContent.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelContent, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelContent;
    // End of variables declaration//GEN-END:variables

}
