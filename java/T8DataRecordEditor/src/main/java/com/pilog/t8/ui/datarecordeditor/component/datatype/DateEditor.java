package com.pilog.t8.ui.datarecordeditor.component.datatype;

import static com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator.DEFAULT_DATE_FORMAT;
import static com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator.DEFAULT_DATE_TIME_FORMAT;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Date;
import com.pilog.t8.data.document.datarecord.access.layer.DateTimeValueAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.DateValueAccessLayer;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.utilities.components.datetimepicker.JXDateTimePicker;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.calendar.DatePickerFormatter;
import org.jdesktop.swingx.plaf.basic.BasicDatePickerUI;

/**
 * @author Bouwer du Preez
 */
public class DateEditor extends DefaultRecordValueEditor
{
    private static final T8Logger LOGGER = T8Log.getLogger(DateEditor.class);

    private JXDatePicker datePicker;
    private DateFormat dateFormat;
    private DateFormat timeFormat;
    private final boolean hasTime; // Boolean flag to indicate whether or not the time of the data is recorded.
    private boolean adjusting; // Flag to indicate that the value of this editor is being updated programmatically (prevents cyclic refresh).
    private final DateSelectionListener selectionListener;
    private final DateFocusListener focusListener;

    public DateEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        this.hasTime = (valueRequirement.getRequirementType() == RequirementType.DATE_TIME_TYPE);
        this.selectionListener = new DateSelectionListener();
        this.focusListener = new DateFocusListener();
        this.adjusting = false;
        initComponents();
        initFormat();
        initDatePicker();
        refreshEditor();
    }

    private void initFormat()
    {
        String formatString;

        formatString = (String)valueRequirement.getAttribute(RequirementAttribute.DATE_TIME_FORMAT_PATTERN.getIdentifier());
        if (formatString != null)
        {
            try
            {
                dateFormat = new SimpleDateFormat(formatString);
            }
            catch (IllegalArgumentException e)
            {
                LOGGER.log("Invalid date format: " + formatString, e);
                dateFormat = new SimpleDateFormat(hasTime ? DEFAULT_DATE_TIME_FORMAT : DEFAULT_DATE_FORMAT);
            }
        }
        else dateFormat = new SimpleDateFormat(hasTime ? DEFAULT_DATE_TIME_FORMAT : DEFAULT_DATE_FORMAT);
    }

    private void initDatePicker()
    {
        GridBagConstraints gridBagConstraints;

        // Create an appropriate editor, depending on whether or not we have to edit a Time value.
        if (!hasTime)
        {
            datePicker = new JXDatePicker();
            datePicker.setFormats(dateFormat);
        }
        else
        {
            JXDateTimePicker dateTimePicker;

            dateTimePicker = new JXDateTimePicker();
            dateTimePicker.setFormats(dateFormat);
            dateTimePicker.setTimeFormat(timeFormat);
            datePicker = dateTimePicker;
        }

        // Change the default icon on the date picker.
        datePicker.setUI(new DatePickerUI());

        //This is required for the year spinner to be shown
        datePicker.getMonthView().setZoomable(true);

        // Add the date picker listeners.
        datePicker.addActionListener(selectionListener);
        datePicker.getEditor().addFocusListener(focusListener);

        // Add the picker to the UI.
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(datePicker, gridBagConstraints);
    }

    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();

        // Set date picker editability using enabled property, since editable property does not disable popup
        datePicker.setEnabled(editableAccess);

        // Set the allowed date selection range.
        if (valueAccessLayer != null)
        {
            if (valueAccessLayer instanceof DateValueAccessLayer)
            {
                DateValueAccessLayer dtValueAccessLayer;

                // Avoid multiple casts
                dtValueAccessLayer = (DateValueAccessLayer)valueAccessLayer;

                // Use a convenience method since the different value access layers currently behave the same
                applyValueAccessRestrictions(dtValueAccessLayer.getUpperBound(), dtValueAccessLayer.getLowerBound(), dtValueAccessLayer.isRestrictToDatePickerSelection());
            }
            else if (valueAccessLayer instanceof DateTimeValueAccessLayer)
            {
                DateTimeValueAccessLayer dtValueAccessLayer;

                // Avoid multiple casts
                dtValueAccessLayer = (DateTimeValueAccessLayer)valueAccessLayer;

                // Use a convenience method since the different value access layers currently behave the same
                applyValueAccessRestrictions(dtValueAccessLayer.getUpperBound(), dtValueAccessLayer.getLowerBound(), dtValueAccessLayer.isRestrictToDatePickerSelection());
            }
        }
    }

    private void applyValueAccessRestrictions(T8Date upperBound, T8Date lowerBound, boolean restrictToDatePickerSelection)
    {
        // Set the lower bound of the date selection.
        if (lowerBound != null) setLowerBound(new Date(lowerBound.getMilliseconds()));
        else setLowerBound(null);

        // Set the upper bound of the date selection.
        if (upperBound != null) setUpperBound(new Date(upperBound.getMilliseconds()));
        else setUpperBound(null);

        // Only if the date picker is editable should we consider the restriction to the actual Data Picker.
        if (this.editableAccess && restrictToDatePickerSelection)
        {
            datePicker.getEditor().setEditable(false);
        }
    }

    @Override
    public void refreshEditor()
    {
        String valueString;

        // Execute the default behavior.
        super.refreshEditor();

        // Get the date value.
        valueString = recordValue.getValue();

        // Set the data on the UI.
        if (Strings.isNullOrEmpty(valueString))
        {
            datePicker.setDate(null);
        }
        else
        {
            try
            {
                Long milliseconds;

                milliseconds = Long.parseLong(valueString);
                datePicker.setDate(new Date(milliseconds));
            }
            catch (NumberFormatException e)
            {
                LOGGER.log("Invalid Data Millisecond Value: " + valueString, e);
                datePicker.setDate(null);
            }
        }
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public boolean commitChanges()
    {
        try
        {
            Date date;

            datePicker.commitEdit();
            date = datePicker.getDate();
            recordValue.setValue(date != null ? ((Long)date.getTime()).toString() : null);
            return true;
        }
        catch (ParseException e)
        {
            LOGGER.log("Exception while commiting Data Editor changes.", e);
            return false;
        }
    }

    @Override
    public void addMouseListener(MouseListener listener)
    {
        super.addMouseListener(listener);
        datePicker.addMouseListener(listener);
    }

    @Override
    public void removeMouseListener(MouseListener listener)
    {
        super.removeMouseListener(listener);
        datePicker.removeMouseListener(listener);
    }

    /**
     * Sets a lower bound date restriction which prevents a user selecting a
     * date before this date.<br/>
     * <br/>
     * Use in combination with {@link #setUpperBound(java.util.Date)} to create
     * an allowable range of dates from which can be selected.
     *
     * @param lowerBound The lowest {@code Date} which a can be selected.
     *      {@code null} clears any existing lower bound
     *
     * @throws RuntimeException If an attempt is made to set the upper and lower
     *      bounds the same, or if the lower bound is being set to after the
     *      upper bound
     */
    public void setLowerBound(Date lowerBound)
    {
        // Need to validate range against the upper-bound if it is set.
        if (lowerBound != null)
        {
            Date upperBound;

            upperBound = this.datePicker.getMonthView().getUpperBound();
            if (upperBound != null && (lowerBound.after(upperBound) || lowerBound.equals(upperBound)))
            {
                throw new RuntimeException("Lower Boundary value for Date property cannot be set after or equal to the Upper Boundary.");
            }
        }
        this.datePicker.getMonthView().setLowerBound(lowerBound);
    }

    /**
     * Sets a upper bound date restriction which prevents a user selecting a
     * date after this date.<br/>
     * <br/>
     * Use in combination with {@link #setLowerBound(java.util.Date)} to create
     * an allowable range of dates from which can be selected.
     *
     * @param upperBound The highest {@code Date} which a can be selected
     *
     * @throws RuntimeException If an attempt is made to set the upper and lower
     *      bounds the same, or if the upper bound is being set to before the
     *      lower bound
     */
    public void setUpperBound(Date upperBound)
    {
        // Need to validate range against the lower-bound if it is set.
        if (upperBound != null)
        {
            Date lowerBound;

            lowerBound = this.datePicker.getMonthView().getLowerBound();
            if (lowerBound != null && (upperBound.before(lowerBound) || upperBound.equals(lowerBound)))
            {
                throw new RuntimeException("Upper Boundary value for Date property cannot be set before or equal to the Lower Boundary.");
            }
        }
        this.datePicker.getMonthView().setUpperBound(upperBound);
    }

    /**
     * Clears the upper and lower boundaries of the current {@code DateEditor},
     * if set.
     */
    public void clearBoundaries()
    {
        setLowerBound(null);
        setUpperBound(null);
    }

    /**
     * Convenience method to set the upper bound of the date chooser to the
     * current {@code Date}. This prevents being able to select any date after
     * the current date.<br/>
     * <br/>
     * If the current day is also to be excluded, make use of the
     * {@link #setUpperBound(java.util.Date)} method.
     */
    public void disallowFutureDates()
    {
        this.datePicker.getMonthView().setUpperBound(new Date());
    }

    /**
     * Convenience method to set the lower bound of the date chooser to the
     * current {@code Date}. This prevents being able to select any date before
     * the current date.<br/>
     * <br/>
     * If the current day is also to be excluded, make use of the
     * {@link #setLowerBound(java.util.Date)} method.
     */
    public void disallowPastDates()
    {
        this.datePicker.getMonthView().setLowerBound(new Date());
    }

    private class DateSelectionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (!adjusting)
            {
                adjusting = true;
                commitChanges();
                adjusting = false;
            }
        }
    }

    private class DateFocusListener implements FocusListener
    {
        @Override
        public void focusGained(FocusEvent e)
        {
        }

        @Override
        public void focusLost(FocusEvent e)
        {
            if (!adjusting)
            {
                adjusting = true;
                commitChanges();
                adjusting = false;
            }
        }
    }

    /**
     * We override the two main components created by the basic UI in order to
     * change the appearance slightly.  We add a custom icon on the date picker
     * and also use a normal formatted text field since the one provided by the
     * default UI fiddles with the preferred size of the editor.
     */
    private static class DatePickerUI extends BasicDatePickerUI
    {
        @Override
        protected JFormattedTextField createEditor()
        {
            JFormattedTextField f = new JFormattedTextField(new DatePickerFormatter.DatePickerFormatterUIResource(datePicker.getLocale()));
            f.setName("dateField");
            return f;
        }

        @Override
        protected JButton createPopupButton()
        {
            JButton popupButton;
            popupButton = super.createPopupButton();
            popupButton.setIcon(new ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/calendarSelectIcon.png")));
            popupButton.setMargin(new Insets(0, 0, 0, 0));
            return popupButton;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
