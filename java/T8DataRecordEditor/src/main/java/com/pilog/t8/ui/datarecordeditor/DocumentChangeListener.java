package com.pilog.t8.ui.datarecordeditor;

import java.beans.PropertyChangeListener;

/**
 * @author Bouwer du Preez
 */
public interface DocumentChangeListener extends PropertyChangeListener
{
    public static final String DOC_CHANGE_SUB_RECORD_NAVIGATION = "DOC_CHANGE_SUB_RECORD_NAVIGATION";
}
