package com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept;

import com.pilog.t8.data.document.datarecord.access.layer.ConceptAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;

/**
 * @author Bouwer du Preez
 */
public class ControlledConceptEditor extends ConceptEditor
{
    private final ControlledConcept controlledConcept;

    public ControlledConceptEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue, (ConceptAccessLayer)recordValue.getParentDataRecord().getAccessLayer().getValueAccessLayer(recordValue.getPropertyID(), recordValue.getFieldID()));
        this.controlledConcept = (ControlledConcept)recordValue;
    }

    @Override
    void clearSelectedConcept()
    {
        // Remove selected value.
        dataRecord.setAdjusting(true);
        controlledConcept.setTerm(null);
        controlledConcept.setCode(null);
        controlledConcept.clearConcept(true);
        dataRecord.setAdjusting(false, true);
    }

    @Override
    void setSelectedConcept(String conceptId)
    {
        // Set the new value on the editor.
        System.out.println("Setting new value: " + conceptId);
        dataRecord.setAdjusting(true);
        controlledConcept.setTerm(null);
        controlledConcept.setCode(null);
        controlledConcept.setConceptId(conceptId, true);
        dataRecord.setAdjusting(false, true);
    }

    @Override
    void setSelectedTerm(String term)
    {
        // Set the new value on the editor.
        System.out.println("Setting new term: " + term);
        dataRecord.setAdjusting(true);
        controlledConcept.setTerm(term);
        controlledConcept.setCode(null);
        controlledConcept.setConceptId(null, true);
        dataRecord.setAdjusting(false, true);
    }

    @Override
    public DataRecord getContextRecord()
    {
        return dataRecord.getDataFile();
    }

    @Override
    public RecordValue getTargetValue()
    {
        return recordValue;
    }
}
