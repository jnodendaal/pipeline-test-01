package com.pilog.t8.ui.datarecordviewer.model;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.PropertyContent;
import com.pilog.t8.data.document.datarecord.RecordContent;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import com.pilog.t8.data.document.valuestring.T8DataRecordPropertyValueStringGenerator;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator.AttachmentRenderType;
import com.pilog.t8.utilities.date.Dates;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A record group is a collection of records that have been closely matched based on their contents any other arbitrary matching criteria.
 * In the comparison view, a group is displayed as a single row on the table with each column in the
 * row containing the contents of one of the records in a group.
 *
 * @author Bouwer du Preez
 */
public class T8DataRecordGroup
{
    private final List<RecordContent> records;
    private final T8DataRecordDefinitionGenerator valueGenerator;

    public T8DataRecordGroup(TerminologyProvider terminologyProvider)
    {
        this(terminologyProvider, Dates.STD_SYS_TS_FORMAT);
    }

    public T8DataRecordGroup(TerminologyProvider terminologyProvider, String dateTimeFormatPattern)
    {
        this.records = new ArrayList<>();
        this.valueGenerator = new T8DataRecordDefinitionGenerator(terminologyProvider);
        this.valueGenerator.setDateTimeFormatPattern(dateTimeFormatPattern);
        this.valueGenerator.setAttachmentRenderType(AttachmentRenderType.FILE_NAME);
    }

    /**
     * Returns the list of distinct property id's used by the records in this group.
     * @return A list of distinct property id's in this group.
     */
    public List<String> getPropertyIds()
    {
        List<String> propertyIds;

        propertyIds = new ArrayList<>();
        for (RecordContent record : records)
        {
            for (PropertyContent property : record.getProperties())
            {
                String propertyId;

                propertyId = property.getId();
                if (!propertyIds.contains(propertyId))
                {
                    propertyIds.add(propertyId);
                }
            }
        }

        return propertyIds;
    }

    /**
     * This method compares the specified data record with all of the other records in this group to calculate a score indicating
     * the degree to which the new record matches those already in the group.  A score of 0 indicates a non-match which means the
     * record cannot be added to this group.  A score of -1 indicates an open position which means that the record may be added
     * to this group regardless of its score.
     * @param record The new record to be matched to this group.
     * @return The match score indicating the degree to which the specified record matches those in this group.
     */
    public int getMatchScore(RecordContent record)
    {
        T8DataRecordPropertyValueStringGenerator propertyGenerator;
        Map<String, String> newRecordPropertyValues;
        RecordContent existingSibling; // A record from the same root, already in this group.
        int bestScore;

        // Create a value string generator to use for generating values to compare.
        propertyGenerator = new T8DataRecordPropertyValueStringGenerator();
        propertyGenerator.setAttachmentRenderType(AttachmentRenderType.CHECKSUM);

        // Create a map containing the property value Strings of the new record to be scored.
        newRecordPropertyValues = new HashMap<>();
        for (PropertyContent newRecordProperty : record.getProperties())
        {
            StringBuilder value;

            value = propertyGenerator.generateValueString(newRecordProperty);
            newRecordPropertyValues.put(newRecordProperty.getId(), value != null ? value.toString() : null);
        }

        // Compare the value Strings of the new record with those of the existing records.
        bestScore = 0;
        existingSibling = null;
        for (RecordContent existingRecord : records)
        {
            // Never score a record if it is compared with itself (this is necessary for the recursive call of this method).
            if (existingRecord != record)
            {
                // Never compare a record to another record from the same root.
                if (!existingRecord.getRootRecord().getId().equals(record.getRootRecord().getId()))
                {
                    int recordScore;

                    recordScore = 0;
                    for (String propertyId : newRecordPropertyValues.keySet())
                    {
                        StringBuilder existingPropertyValue;
                        PropertyContent existingRecordProperty;

                        existingRecordProperty = existingRecord.getProperty(propertyId);
                        existingPropertyValue = existingRecordProperty != null ? propertyGenerator.generateValueString(existingRecordProperty) : null;
                        if (Objects.equals(existingPropertyValue, newRecordPropertyValues.get(propertyId)))
                        {
                            recordScore++;
                        }
                    }

                    if (recordScore > bestScore)
                    {
                        bestScore = recordScore;
                    }
                }
                else
                {
                    // Ok so we know that this group already contains a record from the same root
                    // so this needs to be taken into account when the final score is calculated.
                    existingSibling = existingRecord;
                }
            }
        }

        // If we found a sibling (a record from the same root as the one to be matched) we need to score
        // the sibling to see if it is a bette match to this group than the specified new record.
        if (existingSibling != null)
        {
            int existingSiblingScore;

            // Score the existing sibling and if its score is better than the new record, return 0 (we cannot add the new record to this group).
            existingSiblingScore = getMatchScore(existingSibling);
            if (existingSiblingScore >= bestScore) return 0;
            else return bestScore;
        }
        else
        {
            // If our best score was a 0 but we have no existing sibling, we can return -1 to indicate an open position (without a match on existing records).
            if (bestScore == 0) return -1;
            else return bestScore;
        }
    }

    /**
     * Adds the record to this record group and returns the record it
     * replaces (if any).
     * @param record The data record to add to this group.
     * @return The record replaced by the new record (if any).
     */
    public RecordContent addDataRecord(RecordContent record)
    {
        RecordContent existingSibling;

        existingSibling = null;
        for (RecordContent existingRecord : records)
        {
            if (existingRecord.getRootRecord().getId().equals(record.getRootRecord().getId()))
            {
                existingSibling = existingRecord;
                break;
            }
        }

        if (existingSibling != null)
        {
            records.remove(existingSibling);
            records.add(record);
            return existingSibling;
        }
        else
        {
            records.add(record);
            return null;
        }
    }

    /**
     * Generates and returns a list of the specified record property values.
     * The list of values will be ordered according the the specified sequence
     * of root record ID's.
     * @param rootRecordSequence The sequence of the root records that will be
     * used to order the property values generated.
     * @param propertyId The ID of the property for which the values will be
     * generated.  For each of the root records specified in the sequence, this
     * property values will be generated and added to the result list.
     * @return The list of generated property value Strings.  If a property does
     * not exist in one of the records in this group, a null values will be
     * added to the corresponding index of the result list.
     */
    public List<String> generatePropertyValues(List<String> rootRecordSequence, String propertyId)
    {
        List<String> propertyValues;

        propertyValues = new ArrayList<>();
        for (String rootRecordId : rootRecordSequence)
        {
            boolean found;

            found = false;
            for (RecordContent record : records)
            {
                if (record.getRootRecord().getId().equals(rootRecordId))
                {
                    PropertyContent recordProperty;
                    StringBuilder value;

                    recordProperty = record.getProperty(propertyId);
                    value = recordProperty != null ? valueGenerator.generateValueString(recordProperty) : null;
                    propertyValues.add(value != null ? value.toString() : null);

                    found = true;
                }
            }

            //Make sure that if this record is not part of this group make sure to set its property values to null so it will get added to the
            //table model correctly
            if(!found)
            {
                propertyValues.add(null);
            }
        }

        return propertyValues;
    }
}
