package com.pilog.t8.ui.datarecordeditor.component.datatype;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.datarecord.access.layer.MeasuredNumberAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept.ConceptEditor;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.Number;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.ui.datarecord.DataRecordEditorContainer;
import com.pilog.t8.ui.datarecordeditor.AutoCompletableEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept.QOMConceptEditor;
import com.pilog.t8.ui.datarecordeditor.component.datatype.controlledconcept.UOMConceptEditor;
import java.awt.BorderLayout;
import java.awt.Dimension;

/**
 * @author Bouwer du Preez
 */
public class MeasureNumberEditor extends DefaultRecordValueEditor implements AutoCompletableEditor
{
    private final MeasuredNumber measuredNumber;
    private final MeasuredNumberAccessLayer mnAccessLayer;
    private DefaultRecordValueEditor numericEditor;
    private ConceptEditor uomEditor;
    private ConceptEditor qomEditor;
    private boolean prescribed;

    public MeasureNumberEditor(DataRecordEditorContainer container, RecordValue recordValue)
    {
        super(container, recordValue);
        this.measuredNumber = (MeasuredNumber)recordValue;
        this.mnAccessLayer = (MeasuredNumberAccessLayer)valueAccessLayer;
        initComponents();
        setRequirementAttributes();
        initializeNumericEditor();
        initializeConceptEditors();
        refreshEditor();
    }

    private void setRequirementAttributes()
    {
        prescribed = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.PRESCRIBED_VALUE.toString()));
    }

    private void initializeNumericEditor()
    {
        Number number;

        // Try to find an existing numeric value or create a new one if it does not yet exist.
        number = measuredNumber.getNumberObject();
        if (number == null)
        {
            // No value found, so create one.
            number = measuredNumber.setNumber(null);
        }

        // Create the numeric editor.
        numericEditor = new StringEditor(container, number, prescribed ? this : null, true);
        jPanelNumericSlot.add(numericEditor, java.awt.BorderLayout.CENTER);
    }

    private void initializeConceptEditors()
    {
        RecordValue uomValue;
        RecordValue qomValue;
        Dimension size;

        // Get the UOM value and construct an editor for it.
        uomValue = recordValue.getSubValue(RequirementType.UNIT_OF_MEASURE, null, null);
        if (uomValue == null)
        {
            ValueRequirement uomRequirement;

            uomRequirement = valueRequirement.getDescendentRequirement(RequirementType.UNIT_OF_MEASURE, null, null);
            if (uomRequirement != null)
            {
                uomValue = RecordValue.createValue(uomRequirement);
                recordValue.setSubValue(uomValue);
            }
            else throw new RuntimeException("Measured Number without UOM specification found in: " + propertyRequirement);
        }

        // Create the UOM editor.
        size = new Dimension(250, 10);
        uomEditor = new UOMConceptEditor(container, uomValue, mnAccessLayer.getUomAccessLayer());
        uomEditor.setMinimumSize(size);
        uomEditor.setPreferredSize(size);
        jPanelUOMSlot.add(uomEditor, BorderLayout.CENTER);

        // Get the QOM value and construct an editor for it.
        qomValue = recordValue.getSubValue(RequirementType.QUALIFIER_OF_MEASURE, null, null);
        if (qomValue == null)
        {
            ValueRequirement qomRequirement;

            qomRequirement = valueRequirement.getDescendentRequirement(RequirementType.QUALIFIER_OF_MEASURE, null, null);
            if (qomRequirement != null)
            {
                qomValue = RecordValue.createValue(qomRequirement);
                recordValue.setSubValue(qomValue);
            }
        }

        // Create the QOM editor (optional).
        if (qomValue != null)
        {
            size = new Dimension(250, 10);
            qomEditor = new QOMConceptEditor(container, qomValue);
            qomEditor.setMinimumSize(size);
            qomEditor.setPreferredSize(size);
            jPanelQOMSlot.add(qomEditor, BorderLayout.CENTER);
        }
    }

    @Override
    public boolean hasChanges()
    {
        return false;
    }

    @Override
    public void refreshAccess()
    {
        // Execute default behavior.
        super.refreshAccess();

        // Refresh sub-editor access.
        numericEditor.refreshAccess();
        if (uomEditor != null) uomEditor.refreshAccess();
        if (qomEditor != null) qomEditor.refreshAccess();
    }

    @Override
    public final void refreshEditor()
    {
        // Execute the default behavior.
        super.refreshEditor();

        // Refresh child editors.
        numericEditor.refreshEditor();
        if (uomEditor != null) uomEditor.refreshEditor();
        if (qomEditor != null) qomEditor.refreshEditor();
    }

    @Override
    public boolean commitChanges()
    {
        // Commit sub-editor changes.
        numericEditor.commitChanges();
        if (uomEditor != null) uomEditor.commitChanges();
        if (qomEditor != null) qomEditor.commitChanges();
        return true;
    }

    private ValueRequirement getNumericValueRequirement()
    {
        ValueRequirement numericRequirement;

        numericRequirement = valueRequirement.getFirstSubRequirement();
        if (RequirementType.getNumericDataTypes().contains(numericRequirement.getRequirementType()))
        {
            return numericRequirement;
        }
        else return null;
    }

    @Override
    public void setAutoCompletedValue(RecordValue autoCompletedValue)
    {
        // TODO:  Implement setting of the auto-completed value.
        T8Log.log("Auto-completed value: " + autoCompletedValue);
        if (autoCompletedValue != null)
        {
            MeasuredNumber autoCompletedNumber;

            // We are going to do a multi-step update, so set the adjusting flag.
            dataRecord.setAdjusting(true);

            // Do the value update.
            autoCompletedNumber = (MeasuredNumber)autoCompletedValue;
            measuredNumber.setPrescribedValueID(autoCompletedNumber.getPrescribedValueID());
            measuredNumber.setNumber(autoCompletedNumber.getNumber());
            measuredNumber.setUomId(autoCompletedNumber.getUomId());
            measuredNumber.setQomId(autoCompletedNumber.getQomId());

            // Multi-step update complete.
            dataRecord.setAdjusting(false);
        }
    }

    @Override
    public void setAutoCompletionState(boolean valid, Object currentSelection)
    {
    }

    @Override
    public DataRecord getContextRecord()
    {
        return dataRecord.getDataFile();
    }

    @Override
    public RecordValue getTargetValue()
    {
        return recordValue;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelNumericSlot = new javax.swing.JPanel();
        jPanelUOMSlot = new javax.swing.JPanel();
        jPanelQOMSlot = new javax.swing.JPanel();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelNumericSlot.setOpaque(false);
        jPanelNumericSlot.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelNumericSlot, gridBagConstraints);

        jPanelUOMSlot.setOpaque(false);
        jPanelUOMSlot.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jPanelUOMSlot, gridBagConstraints);

        jPanelQOMSlot.setOpaque(false);
        jPanelQOMSlot.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jPanelQOMSlot, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelNumericSlot;
    private javax.swing.JPanel jPanelQOMSlot;
    private javax.swing.JPanel jPanelUOMSlot;
    // End of variables declaration//GEN-END:variables
}
