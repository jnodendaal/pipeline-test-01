package com.pilog.t8.ui.datarecordviewer.model;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordContent;
import com.pilog.t8.definition.ui.datarecordviewer.T8ComparisonType;
import com.pilog.t8.utilities.components.table.configuredcelltable.CellConfiguredTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordTableStructure
{
    private final List<T8ComparisonType> comparisonTypes;
    private final TerminologyProvider terminologyProvider;

    public T8DataRecordTableStructure(List<T8ComparisonType> comparisonTypes, TerminologyProvider terminologyProvider)
    {
        this.comparisonTypes = new ArrayList<T8ComparisonType>();
        this.terminologyProvider = terminologyProvider;

        if (comparisonTypes != null)
        {
            this.comparisonTypes.addAll(comparisonTypes);
        }
    }

    public void addRecord(RecordContent record)
    {
        T8DRInstanceComparisonType newType;

        // Check whether or not the new record matches one of the existing comparison types.
        for (T8ComparisonType comparisonType : comparisonTypes)
        {
            if (comparisonType.matchesRecord(record))
            {
                comparisonType.addDataRecord(record);
                return;
            }
        }

        // No existing type for the DR Instance found, so we have to create a new one (using the defautl comparison type).
        newType = new T8DRInstanceComparisonType(record.getDrInstanceId(), terminologyProvider);
        newType.addDataRecord(record);
        comparisonTypes.add(newType);
    }

    public void addDataRecordRowsToModel(CellConfiguredTableModel model, List<String> rootRecordSequence)
    {
        for (T8ComparisonType comparisonType : comparisonTypes)
        {
            ((T8AbstractComparisonType)comparisonType).addToModel(model, rootRecordSequence);
        }
    }
}
