package com.pilog.t8.ui.datarecordeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.object.T8DataObjectProvider;
import com.pilog.t8.data.object.T8ClientDataObjectProvider;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 * @author Bouwer du Preez
 */
public class AdminConsole extends javax.swing.JDialog
{
    private final T8DataRecordEditor editor;
    private final DataRecord rootDataRecord;
    private final DataRecord currentRecord;
    private final TerminologyProvider terminologyProvider;
    private final OntologyProvider ontologyProvider;
    private final DataRecordProvider recordProvider;
    private final T8DataObjectProvider dataObjectProvider;
    private final RecordManager recordContainer;
    private final Timer keypressTimer;

    public AdminConsole(T8DataRecordEditor editor, RecordManager recordContainer)
    {
        super(SwingUtilities.getWindowAncestor(editor), ModalityType.DOCUMENT_MODAL);
        this.editor = editor;
        this.recordContainer = recordContainer;
        initComponents();
        this.rootDataRecord = recordContainer.getRootDataRecord();
        this.currentRecord = recordContainer.getCurrentEditor().getDataRecord();
        this.terminologyProvider = editor.getTerminologyProvider();
        this.ontologyProvider = editor.getOntologyProvider();
        this.recordProvider = editor.getDataRecordProvider();
        this.dataObjectProvider = new T8ClientDataObjectProvider(editor.getAccessContext());
        setTitle("Admin Console");
        setSize(500, 700);
        setLocationRelativeTo(SwingUtilities.getWindowAncestor(editor));

        this.keypressTimer = new Timer(500, new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                executeExpression();
            }
        });
        this.keypressTimer.setRepeats(false);

        createDefaultPaths(currentRecord);
    }

    private void executeExpression()
    {
        DocPathExpressionEvaluator expressionEvaluator;
        String expressionString;
        Object output;

        // Get the input expression.
        expressionString = jTextAreaInput.getText();
        if (expressionString != null) expressionString = expressionString.trim();

        // Parse the expression.
        T8Log.log("Parsing document path expression: " + expressionString);
        expressionEvaluator = new DocPathExpressionEvaluator();
        expressionEvaluator.setTerminologyProvider(terminologyProvider);
        expressionEvaluator.setOntologyProvider(ontologyProvider);
        expressionEvaluator.setRecordProvider(recordProvider);
        expressionEvaluator.setDataObjectProvider(dataObjectProvider);
        expressionEvaluator.parseExpression(expressionString);

        // Print the steps for debugging purposes.
        T8Log.log("Expression Steps:");
        expressionEvaluator.printPathSteps();

        // Evaluate the expression.
        output = expressionEvaluator.evaluateExpression(rootDataRecord, expressionString);
        T8Log.log("Output: " + output);
        if (output == null)
        {
            jTextAreaOutput.setText("null");
        }
        else if (output instanceof List)
        {
            jTextAreaOutput.setText("");
            for (Object listElement : (List)output)
            {
                jTextAreaOutput.append(listElement + "\n");
            }
        }
        else
        {
            jTextAreaOutput.setText(output.toString());
        }
        keypressTimer.stop();
    }

    private void createDefaultPaths(DataRecord currentRecord)
    {
        jXTaskPanePaths.setLayout(new GridLayout(0, 1));

        jXTaskPanePaths.add(createCurrentPathComponent("//I:"+currentRecord.getDataRequirementInstanceID()));
        jXTaskPanePaths.add(createCurrentPathComponent("//D:"+currentRecord.getDataRequirementID()));

        //Build a path to the root record
        DataRecord parentRecord;
        DataRecord childRecord;
        StringBuilder pathToRecord;

        pathToRecord = new StringBuilder("I:" + currentRecord.getDataRequirementInstanceID());
        parentRecord = currentRecord.getParentRecord();
        childRecord = currentRecord;

        while(parentRecord != null)
        {
            propertyLoop:
            for (RecordProperty recordProperty : parentRecord.getRecordProperties())
            {
                for (RecordValue descendantValue : recordProperty.getAllRecordValues())
                {
                    if(descendantValue.hasContent(false) && Objects.equals(descendantValue.getValue(), childRecord.getID()))
                    {
                        if(!Strings.isNullOrEmpty(descendantValue.getFieldID()))
                        {
                            pathToRecord.insert(0, "/");
                            pathToRecord.insert(0, descendantValue.getFieldID());
                            pathToRecord.insert(0, "F:");
                        }

                        pathToRecord.insert(0, "/");
                        pathToRecord.insert(0, descendantValue.getPropertyID());
                        pathToRecord.insert(0, "P:");

                        pathToRecord.insert(0, "/");
                        pathToRecord.insert(0, descendantValue.getDataRequirementID());
                        pathToRecord.insert(0, "D:");

                        pathToRecord.insert(0, "|");
                        pathToRecord.insert(0, descendantValue.getDataRequirementInstanceID());
                        pathToRecord.insert(0, "I:");

                        childRecord = parentRecord;
                        parentRecord = parentRecord.getParentRecord();

                        break propertyLoop;
                    }
                }
            }
        }

        pathToRecord.insert(0, "/");
        jXTaskPanePaths.add(createCurrentPathComponent(pathToRecord.toString()));
    }

    private JComponent createCurrentPathComponent(final String path)
    {
        JPanel pathPanel;
        JTextArea pathTextArea;
        JButton pathInsertButton;

        pathPanel = new JPanel(new BorderLayout());

        pathTextArea = new JTextArea(path);
        pathTextArea.setEditable(false);
        pathPanel.add(pathTextArea, BorderLayout.CENTER);

        pathInsertButton = new JButton("Insert");
        pathInsertButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                jTextAreaInput.setText(path);
                executeExpression();
            }
        });
        pathPanel.add(pathInsertButton, BorderLayout.EAST);

        return pathPanel;
    }

    private void printValidationReport()
    {
        T8DataFileValidationReport report;

        report = recordContainer.getValidationReport();
        if (report != null)
        {
            jTextAreaOutput.setText(report.toString());
        }
        else
        {
            jTextAreaOutput.setText("No validation report set.");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarMain = new javax.swing.JToolBar();
        jButtonPrintValidationReport = new javax.swing.JButton();
        jPanelContent = new javax.swing.JPanel();
        jXTaskPanePaths = new org.jdesktop.swingx.JXTaskPane();
        jScrollPaneInput = new javax.swing.JScrollPane();
        jTextAreaInput = new javax.swing.JTextArea();
        jScrollPaneOutput = new javax.swing.JScrollPane();
        jTextAreaOutput = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);

        jButtonPrintValidationReport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/pageWhiteTextIcon.png"))); // NOI18N
        jButtonPrintValidationReport.setText("Print Validation Report");
        jButtonPrintValidationReport.setFocusable(false);
        jButtonPrintValidationReport.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPrintValidationReport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPrintValidationReportActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonPrintValidationReport);

        getContentPane().add(jToolBarMain, java.awt.BorderLayout.PAGE_START);

        jPanelContent.setLayout(new java.awt.GridBagLayout());

        jXTaskPanePaths.setCollapsed(true);
        jXTaskPanePaths.setTitle("Current Paths");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jXTaskPanePaths, gridBagConstraints);

        jScrollPaneInput.setMinimumSize(new java.awt.Dimension(50, 100));

        jTextAreaInput.setColumns(20);
        jTextAreaInput.setRows(5);
        jTextAreaInput.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt)
            {
                jTextAreaInputKeyPressed(evt);
            }
        });
        jScrollPaneInput.setViewportView(jTextAreaInput);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jScrollPaneInput, gridBagConstraints);

        jTextAreaOutput.setColumns(20);
        jTextAreaOutput.setRows(5);
        jScrollPaneOutput.setViewportView(jTextAreaOutput);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContent.add(jScrollPaneOutput, gridBagConstraints);

        getContentPane().add(jPanelContent, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextAreaInputKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jTextAreaInputKeyPressed
    {//GEN-HEADEREND:event_jTextAreaInputKeyPressed
        if(keypressTimer.isRunning())
            keypressTimer.restart();
        else keypressTimer.start();
    }//GEN-LAST:event_jTextAreaInputKeyPressed

    private void jButtonPrintValidationReportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPrintValidationReportActionPerformed
    {//GEN-HEADEREND:event_jButtonPrintValidationReportActionPerformed
        printValidationReport();
    }//GEN-LAST:event_jButtonPrintValidationReportActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonPrintValidationReport;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JScrollPane jScrollPaneInput;
    private javax.swing.JScrollPane jScrollPaneOutput;
    private javax.swing.JTextArea jTextAreaInput;
    private javax.swing.JTextArea jTextAreaOutput;
    private javax.swing.JToolBar jToolBarMain;
    private org.jdesktop.swingx.JXTaskPane jXTaskPanePaths;
    // End of variables declaration//GEN-END:variables
}
