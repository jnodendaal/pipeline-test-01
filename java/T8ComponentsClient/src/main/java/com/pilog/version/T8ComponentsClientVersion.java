package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8ComponentsClientVersion
{
    public final static String VERSION = "388";
}
