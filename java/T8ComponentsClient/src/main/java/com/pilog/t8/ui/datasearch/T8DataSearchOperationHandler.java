package com.pilog.t8.ui.datasearch;

import static com.pilog.t8.definition.ui.datasearch.T8DataSearchAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataSearchOperationHandler extends T8ComponentOperationHandler
{
    private final T8DataSearch dataSearch;

    public T8DataSearchOperationHandler(T8DataSearch dataSearch)
    {
        super(dataSearch);
        this.dataSearch = dataSearch;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        switch (operationIdentifier)
        {
            case OPERATION_CLEAR_QUICK_SEARCH_TEXT:
                Boolean search;

                search = (Boolean)operationParameters.get(PARAMETER_SEARCH);
                dataSearch.clearSearchText(search != null && search);
                return null;
            case OPERATION_SET_QUICK_SEARCH_TEXT:
                dataSearch.setSearchText((String)operationParameters.get(PARAMETER_TEXT));
                return null;
            case OPERATION_SET_SEARCH_FIELDS:
                dataSearch.setSearchFields((List<String>)operationParameters.get(PARAMETER_FIELD_IDS));
                return null;
            case OPERATION_GET_QUICK_SEARCH_TEXT:
                return HashMaps.createSingular(PARAMETER_TEXT, dataSearch.getSearchText());
            default:
                return super.executeOperation(operationIdentifier, operationParameters);
        }
    }
}
