package com.pilog.t8.ui.datarecord.celleditor;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.validation.T8DataValidationError;
import com.pilog.t8.ui.datarecord.DataRecordEditor;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * @author Bouwer du Preez
 */
public class PropertyValueEditorComponent extends JPanel implements PropertyEditorComponent
{
    private final SectionRequirement sectionRequirement;
    private final PropertyRequirement propertyRequirement;
    private DataRecordEditor recordValueEditor;
    private List<T8DataValidationError> requirementValidationErrors;
    private DataRecord dataRecord;
    private RecordProperty recordProperty;

    public PropertyValueEditorComponent(TableCellPropertyResolver resolver, PropertyRequirement propertyRequirement)
    {
        initComponents();
        this.propertyRequirement = propertyRequirement;
        this.sectionRequirement = propertyRequirement.getParentSectionRequirement();
        this.setBackground(LAFConstants.CONTENT_GREY);
        initializePropertyValueEditor(resolver);
        bindKeys();
    }

    private void initializePropertyValueEditor(TableCellPropertyResolver resolver)
    {
        ValueRequirement propertyValueRequirement;

        propertyValueRequirement = propertyRequirement.getValueRequirement();
        if (propertyValueRequirement != null)
        {
            RecordValue propertyValue;

            propertyValue = RecordValue.createValue(propertyValueRequirement);
            recordValueEditor = resolver.getCellEditorComponentFactory().createDataRecordEditorComponent(propertyValue);
            jPanelContent.add((Component)recordValueEditor, java.awt.BorderLayout.CENTER);
            jPanelContent.revalidate();
        }
    }

    private void bindKeys()
    {
        InputMap inputMap;
        ActionMap actionMap;

        // Get the correct InputMap and ActionMap.
        inputMap = this.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        actionMap = this.getActionMap();

        // Create a key binding for the Enter key pressed.
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ALT, 0), "altPressed");
        actionMap.put("altPressed", new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // TODO: Switch display mode.
            }
        });
    }

    public PropertyRequirement getPropertyRequirement()
    {
        return propertyRequirement;
    }

    @Override
    public void commitChanges()
    {
        // First make sure we have a property set.
        if (recordProperty == null) throw new RuntimeException("Commit changes called without any property value set on editor: " + propertyRequirement);

        // Commit changes on the value editor.
        if (recordValueEditor != null)
        {
            recordValueEditor.commitChanges();
        }
        else
        {
            recordProperty.setRecordValue(null);
            recordProperty.setFFT(null);
        }
    }

    @Override
    public void setDataRecord(DataRecord dataRecord)
    {
        // Set the DataRecord and RecordProperty of this editor.
        this.dataRecord = dataRecord;
        this.recordProperty = dataRecord.getRecordProperty(propertyRequirement.getConceptID());
        if (recordProperty == null)
        {
            recordProperty = new RecordProperty(propertyRequirement);
            dataRecord.getRecordSection(sectionRequirement.getConceptID()).addRecordProperty(recordProperty);
        }
    }

    @Override
    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    @Override
    public RecordProperty getRecordProperty()
    {
        return recordProperty;
    }

    public List<T8DataValidationError> getRequirementValidationErrors()
    {
        return requirementValidationErrors;
    }

    public void setSelected(boolean selected)
    {
        setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.WHITE_LINE_BORDER);
    }

    @Override
    public void addMouseListener(MouseListener listener)
    {
        if (recordValueEditor != null)
        {
            ((Component)recordValueEditor).addMouseListener(listener);
        }
    }

    /**
     * This method is a hack that simply delegates key strokes from the panel
     * (this class) to the actual editor component so that text is properly
     * entered into the text editor when this class is used in a table cell.
     */
    @Override
    protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed)
    {
        return recordValueEditor.processKeyBinding(ks, e, condition, pressed);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelEditor = new javax.swing.JPanel();
        jPanelContent = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        setLayout(new java.awt.GridBagLayout());

        jPanelEditor.setOpaque(false);
        jPanelEditor.setLayout(new java.awt.GridBagLayout());

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelEditor.add(jPanelContent, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelEditor, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelEditor;
    // End of variables declaration//GEN-END:variables
}
