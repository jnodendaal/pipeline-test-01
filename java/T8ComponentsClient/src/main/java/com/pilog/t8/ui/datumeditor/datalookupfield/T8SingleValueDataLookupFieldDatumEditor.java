package com.pilog.t8.ui.datumeditor.datalookupfield;

import com.pilog.t8.client.T8ClientScriptRunner;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.ui.event.T8DefinitionDatumEditorListener;
import com.pilog.t8.definition.script.T8DatumEditorConfigurationScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.datalookupfield.T8DataLookupFieldAPIHandler;
import com.pilog.t8.definition.ui.datumeditor.datalookupfield.T8SingleValueDataLookupFieldDatumEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datalookupfield.T8DataLookupField;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public class T8SingleValueDataLookupFieldDatumEditor extends JPanel implements T8DefinitionDatumEditor
{
    private final T8SingleValueDataLookupFieldDatumEditorDefinition editorDefinition;
    private final T8DefaultComponentController controller;
    private final T8DefinitionContext definitionContext;
    private final T8Definition targetDefinition;
    private final T8DefinitionDatumType datumType;
    private T8DataLookupField lookupField;

    public T8SingleValueDataLookupFieldDatumEditor(T8DefinitionContext definitionContext, T8SingleValueDataLookupFieldDatumEditorDefinition editorDefinition, T8Definition targetDefinition, T8DefinitionDatumType datumType) throws Exception
    {
        this.editorDefinition = editorDefinition;
        this.controller = new T8DefaultComponentController(new T8Context(definitionContext.getClientContext(), definitionContext.getSessionContext()).setProjectId(editorDefinition.getRootProjectId()), editorDefinition.getIdentifier(), false);
        this.controller.addComponentEventListener(new SelectionEventListener());
        this.definitionContext = definitionContext;
        this.targetDefinition = targetDefinition;
        this.datumType = datumType;
        initComponents();
        initEditor();
        configureEditor();
    }

    private void initEditor() throws Exception
    {
        lookupField = (T8DataLookupField)editorDefinition.getNewComponentInstance(controller);
        controller.registerComponent(lookupField);
        add(lookupField, java.awt.BorderLayout.CENTER);
    }

    private void configureEditor()
    {
        T8DatumEditorConfigurationScriptDefinition configurationScriptDefinition;

        configurationScriptDefinition = editorDefinition.getConfigurationScriptDefinition();
        if ((configurationScriptDefinition != null) && (configurationScriptDefinition.getScript() != null))
        {
            try
            {
                T8ClientScriptRunner scriptRunner;
                Map<String, Object> scriptParameters;

                scriptParameters = new HashMap<>();
                scriptParameters.put(T8DatumEditorConfigurationScriptDefinition.PARAMETER_DEFINITION_IDENTIFIER, getDefinition().getIdentifier());
                scriptParameters.put(T8DatumEditorConfigurationScriptDefinition.PARAMETER_DEFINITION, getDefinition());
                scriptParameters.put(T8DatumEditorConfigurationScriptDefinition.PARAMETER_DATUM_IDENTIFIER, getDatumIdentifier());
                scriptParameters.put(T8DatumEditorConfigurationScriptDefinition.PARAMETER_DATUM_TYPE, getDatumType());

                scriptRunner = new T8ClientScriptRunner(controller);
                scriptRunner.runScript(configurationScriptDefinition, scriptParameters);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while executing datum editor configuration script in editor: " + editorDefinition, e);
            }
        }
    }

    protected void setDefinitionDatum(Object value)
    {
        targetDefinition.setDefinitionDatum(datumType.getIdentifier(), value, this);
    }

    protected Object getDefinitionDatum()
    {
        return targetDefinition.getDefinitionDatum(datumType.getIdentifier());
    }

    @Override
    public void initializeComponent()
    {
        lookupField.initializeComponent(null);
    }

    @Override
    public void startComponent()
    {
        lookupField.startComponent();
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
        lookupField.stopComponent();
    }

    @Override
    public void commitChanges()
    {
        commitValue();
    }

    @Override
    public void setEditable(boolean editable)
    {
        lookupField.setEnabled(editable);
    }

    @Override
    public void refreshEditor()
    {
        Map<String, Object> key;

        key = new HashMap<>();
        key.put(editorDefinition.getValueFieldIdentifier(), getDefinitionDatum());
        lookupField.setSelectedDataEntityKey(key);
    }

    private void commitValue()
    {
        T8DataEntity selectedEntity;

         //Only commit the changes if the lookup field entity is retrieved
        if(lookupField.isEntityRetrieved())
        {
            selectedEntity = lookupField.getSelectedDataEntity();
            setDefinitionDatum(selectedEntity != null ? selectedEntity.getFieldValue(editorDefinition.getValueFieldIdentifier()) : null);
        }
    }

    @Override
    public T8Context getContext()
    {
        return definitionContext.getContext();
    }

    @Override
    public T8ClientContext getClientContext()
    {
        return definitionContext.getClientContext();
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return definitionContext.getSessionContext();
    }

    @Override
    public T8DefinitionContext getDefinitionContext()
    {
        return definitionContext;
    }

    @Override
    public String getDatumIdentifier()
    {
        return datumType.getIdentifier();
    }

    @Override
    public T8DefinitionDatumType getDatumType()
    {
        return datumType;
    }

    @Override
    public T8Definition getDefinition()
    {
        return targetDefinition;
    }

    @Override
    public void addDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
    }

    @Override
    public void removeDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
    }

    private class SelectionEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;

            eventDefinition = event.getEventDefinition();
            if (eventDefinition.getIdentifier().equals(T8DataLookupFieldAPIHandler.EVENT_SELECTION_CHANGED))
            {
                commitValue();
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
