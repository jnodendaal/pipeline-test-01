package com.pilog.t8.ui.celleditor.table.numeric;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellEditableComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent.EditParameter;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.celleditor.table.number.T8NumericTableCellEditorDefinition;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.format.EmptyFormat;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JFormattedTextField;
import javax.swing.border.Border;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import org.jdesktop.swingx.JXFormattedTextField;
import org.jdesktop.swingx.text.NumberFormatExt;
import org.jdesktop.swingx.text.StrictNumberFormatter;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8NumericTableCellEditor extends JXFormattedTextField implements T8TableCellEditorComponent
{
    private final T8NumericTableCellEditorDefinition definition;
    private final T8SessionContext sessionContext;
    private final T8ComponentController controller;
    private final Border defaultBorder;
    private Object oldValue;

    public T8NumericTableCellEditor(T8NumericTableCellEditorDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.sessionContext = controller.getSessionContext();
        this.defaultBorder = getBorder();
        addPropertyChangeListener(new TextFieldPropertyChangeListener());
        applyFormatter();
    }

    @Override
    public void setEditorData(T8TableCellEditableComponent sourceComponent, Map<String, Object> dataRow, String editorKey, Map<EditParameter, Object> editParameters)
    {
        oldValue = dataRow.get(definition.getTargetFieldIdentifier());
        setText(oldValue != null ? oldValue.toString() : null);

        if ((Boolean)editParameters.get(EditParameter.SELECTED))
        {
            setBorder(LAFConstants.SELECTED_NODE_BORDER);
        }
        else
        {
            setBorder(defaultBorder);
        }
    }

    @Override
    public Map<String, Object> getEditorData()
    {
        if (isEditValid())
        {
            try
            {
                commitEdit();
                return HashMaps.newHashMap(definition.getTargetFieldIdentifier(), getValue());
            }
            catch (Exception e)
            {
                T8Log.log("Exception while committing numeric value edit: " + getText(), e);
                return HashMaps.newHashMap(definition.getTargetFieldIdentifier(), oldValue);
            }
        }
        else
        {
            return HashMaps.newHashMap(definition.getTargetFieldIdentifier(), oldValue);
        }
    }

    @Override
    public boolean singleClickEdit()
    {
        return false;
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String string, Map<String, Object> map)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private void applyFormatter()
    {
        try
        {
            NumberFormatter formatter;

            if(definition.useStrictFormatting())
            {
                formatter = new StrictNumberFormatter(getNumberFormat(definition));
            }
            else
            {
                formatter = new NumberFormatter();
            }

            if (definition.getNumberFormatType() == T8NumericTableCellEditorDefinition.FormattingTypes.GENERAL)
            {
                formatter.setFormat(new EmptyFormat(new DecimalFormat(definition.getFormatPattern()), null));
            }

            formatter.setAllowsInvalid(true);
            formatter.setOverwriteMode(false);
            formatter.setCommitsOnValidEdit(false);

            setFocusLostBehavior(JFormattedTextField.COMMIT);
            setFormatterFactory(new DefaultFormatterFactory(formatter));
        }
        catch (Exception e)
        {
            T8Log.log("While applying pattern '" + definition.getFormatPattern() + "' to component.", e);
        }
    }

    private static NumberFormat getNumberFormat(T8NumericTableCellEditorDefinition definition)
    {
        switch(definition.getNumberFormatType())
        {
            case CURRENCY   : return NumberFormatExt.getCurrencyInstance();
            case GENERAL    : return NumberFormatExt.getNumberInstance();
            case INTEGER    : return NumberFormatExt.getIntegerInstance();
            case PERCENTAGE : return NumberFormatExt.getPercentInstance();
            default         : return NumberFormat.getInstance();
        }
    }

    private class TextFieldPropertyChangeListener implements PropertyChangeListener
    {
        @Override
        public void propertyChange(PropertyChangeEvent event)
        {
            if (isEditable())
            {
                if ("editValid".equals(event.getPropertyName()))
                {
                    boolean validEdit;

                    validEdit = (Boolean) event.getNewValue();
                    setBackground(validEdit ? Color.WHITE : LAFConstants.INVALID_PINK);
                }
            }
        }
    }
}
