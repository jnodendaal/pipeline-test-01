package com.pilog.t8.ui.datalookupfield;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.datalookupfield.T8DataLookupFieldAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupFieldOperationHandler extends T8ComponentOperationHandler
{

    private T8DataLookupField lookupField;

    public T8DataLookupFieldOperationHandler(T8DataLookupField lookupField)
    {
        super(lookupField);
        this.lookupField = lookupField;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8DataLookupFieldAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8DataLookupFieldAPIHandler.PARAMETER_DATA_ENTITY, lookupField.getSelectedDataEntity());
        }
        else if (operationIdentifier.equals(T8DataLookupFieldAPIHandler.OPERATION_CLEAR))
        {
            lookupField.clear();
            return null;
        }
        else if (operationIdentifier.equals(T8DataLookupFieldAPIHandler.OPERATION_GET_SELECTED_DATA_VALUE))
        {
            return HashMaps.newHashMap(T8DataLookupFieldAPIHandler.PARAMETER_DATA_VALUE, lookupField.getSelectedDataValue());
        }
        else if (operationIdentifier.equals(T8DataLookupFieldAPIHandler.OPERATION_SET_SELECTED_DATA_ENTITY_KEY))
        {
            Map<String, Object> keyMap;

            keyMap = (Map<String, Object>)operationParameters.get(T8DataLookupFieldAPIHandler.PARAMETER_DATA_VALUE_MAP);
            lookupField.setSelectedDataEntityKey(keyMap);
            return null;
        }
        else if (operationIdentifier.equals(T8DataLookupFieldAPIHandler.OPERATION_SET_PREFILTER))
        {
            T8DataFilter dataFilter;
            Boolean refresh;
            String filterIdentifier;

            dataFilter = (T8DataFilter)operationParameters.get(T8DataLookupFieldAPIHandler.PARAMETER_DATA_FILTER);
            refresh = (Boolean)operationParameters.get(T8DataLookupFieldAPIHandler.PARAMETER_REFRESH_DATA);
            filterIdentifier = (String)operationParameters.get(T8DataLookupFieldAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
            if (refresh == null) refresh = true;

            lookupField.setPrefilter(filterIdentifier, dataFilter);
            if (refresh) lookupField.refreshData();
            return null;
        }
        else if (operationIdentifier.equals(T8DataLookupFieldAPIHandler.OPERATION_SET_EDITABLE))
        {
            Boolean editable;

            editable = (Boolean)operationParameters.get(T8DataLookupFieldAPIHandler.PARAMETER_EDITABLE);
            if (editable == null) editable = true;

            lookupField.setEditable(editable);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
