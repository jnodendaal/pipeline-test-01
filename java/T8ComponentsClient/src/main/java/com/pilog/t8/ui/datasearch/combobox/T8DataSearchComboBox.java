package com.pilog.t8.ui.datasearch.combobox;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxAPIHandler;
import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.table.T8Table;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.swing.JComponent;
import javax.swing.border.Border;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.PropertySetter;
import org.jdesktop.core.animation.timing.interpolators.AccelerationInterpolator;
import org.jdesktop.swingx.painter.CompoundPainter;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBox extends JComponent implements T8Component
{
    private final T8ComponentController controller;
    private final T8DataSearchComboBoxDefinition definition;
    private final T8DataSearchComboBoxOperationHandler operationHandler;
    private final ComboBoxMouseListener mouseListener;

    private ComboBoxPopup comboBoxPopup;
    private Animator backgroundAnimator;
    private Painter<T8DataSearchComboBox> backgroundPainter;
    private Painter<T8DataSearchComboBox> activatedPainter;
    private float backgroundOpacity;

    private boolean isFiltered;
    private Set<Object> currentFilter;

    public T8DataSearchComboBox(T8DataSearchComboBoxDefinition definition, T8ComponentController controller)
    {
        this.controller = controller;
        this.definition = definition;
        this.operationHandler = new T8DataSearchComboBoxOperationHandler(this);
        this.mouseListener = new ComboBoxMouseListener();
        this.backgroundOpacity = 0;
        this.currentFilter = new HashSet<>();
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.addMouseListener(mouseListener);
        this.setFont(new Font("Tahoma", Font.PLAIN, 11));
        this.comboBoxPopup = new ComboBoxPopup(this);

        //Start the animator
        this.backgroundAnimator = new Animator.Builder()
                .addTarget(PropertySetter.getTarget(this, "backgroundOpacity", 0.0f, 1.0f))
                .setDuration(200, TimeUnit.MILLISECONDS)
                .setInterpolator(new AccelerationInterpolator(0.3f, 0.2f))
                .build();
    }

    @Override
    public void startComponent()
    {
        comboBoxPopup.startComponent();
    }

    @Override
    public void stopComponent()
    {
        comboBoxPopup.stopComponent();
    }

    protected void applyFilter()
    {
        mouseListener.setPressed(false);

        T8DataFilter dataFilter;

        dataFilter = getDataFilter();
        isFiltered = dataFilter.hasFilterCriteria();

        if(isFilterUpdated())
        {
            if(definition.getTargetTableIdentifier() != null)
            {
                T8Table targetTable;

                targetTable = (T8Table) controller.findComponent(definition.getTargetTableIdentifier());
                targetTable.setPrefilter(definition.getIdentifier(), dataFilter);
                targetTable.refreshData();
            }

            Map<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            eventParameters.put(T8DataSearchComboBoxAPIHandler.PARAMETER_DATA_FILTER, dataFilter);

            controller.reportEvent(this, definition.getComponentEventDefinition(T8DataSearchComboBoxAPIHandler.EVENT_SEARCH), eventParameters);

            currentFilter = comboBoxPopup.getSelectedValues();
        }
    }

    public T8DataFilter getDataFilter()
    {
        T8DataFilter dataFilter;
        Set<Object> selectedValues;

        selectedValues = comboBoxPopup.getSelectedValues();
        dataFilter = new T8DataFilter(definition.getDataEntityIdentifier());

        if(!selectedValues.isEmpty())
        {
            if(!definition.isOrConjunction())
            {
                for (String searchFieldIdentifier : definition.getSearchFieldIdentifiers())
                {
                    dataFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, searchFieldIdentifier, selectedValues);
                }
            }
            else
            {
                for (String searchFieldIdentifier : definition.getSearchFieldIdentifiers())
                {
                    dataFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.OR, searchFieldIdentifier, selectedValues);
                }
            }
        }

        return dataFilter;
    }

    /**
     * Clears the data filter by clearing the selected values on the combo-box
     * pop-up and then applying this new "filter".<br/>
     * <br/>
     * The filter does not have to be applied, which means that the set filter
     * is cleared, but the actual data source will maintain the data set
     * displayed before the filter was cleared.
     *
     * @param applyFilter Specifies whether or not to apply the cleared data
     *      filter. {@code true} to apply, {@code false} to only clear
     */
    public void clearDataFilter(boolean applyFilter)
    {
        comboBoxPopup.clearSelectedValues();
        if (applyFilter) applyFilter();
    }

    /**
     * Sets the selected values on the combo box for display. The selected
     * values will alter the filter and therefore it is assumed the next logical
     * step would be to retrieve the filter, and as such it is returned.
     *
     * @param valueList The {@code List} of object values to be set as selected
     *      on the combo box
     * @param clearCurrentSelection A convenience method to clear the current
     *      selection before setting the new selection
     *
     * @return The {@code T8DataFilter} as built from the new set of selected
     *      values
     */
    T8DataFilter setSelectedValues(List<Object> valueList, boolean clearCurrentSelection)
    {
        this.comboBoxPopup.setSelectedValues(valueList, clearCurrentSelection);
        return getDataFilter();
    }

    void refreshDataSource()
    {
        this.comboBoxPopup.refreshDataSource();
    }

    void setDataSourceFilter(T8DataFilter dataFilter, boolean refresh)
    {
        this.comboBoxPopup.setDataSourceFilter(dataFilter, refresh);
    }

    protected String getTranslation(String text)
    {
        return getController().getClientContext().getConfigurationManager().getUITranslation(controller.getContext(), text);
    }

    private boolean isFilterUpdated()
    {
        Set<Object> newValues;

        newValues = comboBoxPopup.getSelectedValues();
        return !(currentFilter.containsAll(newValues) && newValues.containsAll(currentFilter));
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        Graphics2D g2;

        g2 = (Graphics2D) g.create();

        // Set the default painter.
        if (mouseListener.isPressed())
        {
            if (activatedPainter != null)
            {
                activatedPainter.paint(g2, this, getWidth(), getHeight());
            }
        }
        else if (backgroundPainter != null)
        {
            Composite currentComposite;

            currentComposite = g2.getComposite();

            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, getBackgroundOpacity()));

            backgroundPainter.paint(g2, this, getWidth(), getHeight());

            g2.setComposite(currentComposite);
        }

        Border border;
        Insets borderInsets;

        border = getBorder();
        if (border != null)
        {
            border.paintBorder(this, g2, 0, 0, getWidth(), getHeight());
            borderInsets = border.getBorderInsets(this);
        }
        else
        {
            borderInsets = new Insets(1, 1, 1, 1);
        }

        FontMetrics fontMetrics;
        int adjustedHeight;
        String displayString;

        fontMetrics = g2.getFontMetrics(getFont());
        adjustedHeight = getHeight() - borderInsets.top - borderInsets.bottom;
        adjustedHeight /= 2;
        adjustedHeight += fontMetrics.getAscent() / 2;
        displayString = definition.getSearchDisplayName();

        if (displayString != null && fontMetrics.stringWidth(displayString) < getWidth())
        {
            g2.setColor(getForeground());
            g2.drawString(displayString, borderInsets.left + 5, adjustedHeight);
        }

        Rectangle chevronBounds;
        Polygon chevron;

        chevronBounds = new Rectangle();
        chevronBounds.height = 4;
        chevronBounds.width = 6;
        chevronBounds.y = (getHeight() / 2) - (chevronBounds.height / 2) + 1;
        chevronBounds.x = getWidth() - chevronBounds.width - 5;

        chevron = new Polygon();

        chevron.addPoint(chevronBounds.x, chevronBounds.y);
        chevron.addPoint(chevronBounds.x + (chevronBounds.width / 2), chevronBounds.y + chevronBounds.height);
        chevron.addPoint(chevronBounds.x + chevronBounds.width + 1, chevronBounds.y);

        g2.setColor(isFiltered ? Color.RED : Color.GRAY);

        g2.fill(chevron);

        //Draw the border
        if(getBorder() != null)
        {
            getBorder().paintBorder(this, g2, 0, 0, getWidth(), getHeight());
        }
        else if(mouseListener.isRollover() || mouseListener.isPressed())
        {
            g2.setStroke(new BasicStroke(1f));
            g2.setColor(new Color(200, 200, 200));
            g2.draw(new Rectangle(0, 0, getWidth() - 1, getHeight() - 1));
        }

        super.paintComponent(g);

        g2.dispose();
    }

    public void setBackgroundOpacity(float backgroundOpacity)
    {
        this.backgroundOpacity = backgroundOpacity;
        repaint();
    }

    public List<Object> getSelectedValues()
    {
        return new ArrayList<>(comboBoxPopup.getSelectedValues());
    }

    public float getBackgroundOpacity()
    {
        return backgroundOpacity;
    }

    @Override
    public void setVisible(boolean aFlag)
    {
        super.setVisible(aFlag);

        if (!aFlag)
        {
            comboBoxPopup.setVisible(aFlag);
        }
    }

    @Override
    public Dimension getSize()
    {
        Dimension size;

        size = super.getSize();
        if (size == null || (size.getHeight() == 1 && size.getWidth() == 1))
        {
            return getPreferredSize();
        } else return size;
    }

    @Override
    public void invalidate()
    {
        super.invalidate();

        //After the size has ben changed we need to reinitialize the painters with the new size
        initializePainters();
    }

    @Override
    public Dimension getPreferredSize()
    {
        Dimension preferredSize;

        preferredSize = super.getPreferredSize();
        if (preferredSize != null && preferredSize.getHeight() != 1 && preferredSize.getWidth() != 1)
        {
            return preferredSize;
        }
        else
        {
            String displayString;
            FontMetrics fontMetrics;
            int width;
            int height;

            displayString = definition.getSearchDisplayName();
            fontMetrics = getFontMetrics(getFont());

            if (displayString == null)
            {
                displayString = "";
            }

            width = fontMetrics.stringWidth(displayString);
            height = fontMetrics.getHeight();

            //Add 5 to the width because we inset the string by 5
            width += 10;
            height += 10;

            //Add 15 width for the chevron
            width += 10;

            if (getBorder() != null)
            {
                Insets borderInsets;

                borderInsets = getBorder().getBorderInsets(this);
                width += borderInsets.left + borderInsets.right;
                height += borderInsets.top + borderInsets.bottom;
            }

            if (getInsets() != null)
            {
                Insets insets;

                insets = getInsets();
                width += insets.left + insets.right;
                height += insets.top + insets.bottom;
            }

            if (width < 50)
            {
                width = 50;
            }

            return new Dimension(width, height);
        }
    }

    @Override
    public Dimension getMinimumSize()
    {
        return getPreferredSize();
    }

    private void initializePainters()
    {
        T8LookAndFeelManager lafManager;
        T8Painter t8Painter;

        lafManager = controller.getClientContext().getConfigurationManager().getLookAndFeelManager(controller.getContext());
        // Initialize painter for painting the default background of the button.
        t8Painter = lafManager.getDataSearchCombBoxBackgroundPainter();
        if (t8Painter == null && getWidth() > 1 && getHeight() > 1)
        {
            Paint paint;

            paint = new LinearGradientPaint(getWidth() / 2, -5, getWidth() / 2, getHeight() * 2, new float[]{0.1f, 0.6f}, new Color[]{Color.white, new Color(204, 204, 204)});
            backgroundPainter = new CompoundPainter<>(new MattePainter(paint));
        }
        else if(t8Painter != null)
        {
            backgroundPainter = new T8PainterAdapter(t8Painter);
        }

        // Initialize painter for painting the focused background of the button.
        t8Painter = lafManager.getDataSearchComboBoxActivatedPainter();
        if (t8Painter == null && getWidth() > 1 && getHeight() > 1)
        {
            Paint paint;

            paint = new LinearGradientPaint(getWidth() / 2, -5, getWidth() / 2, getHeight() * 2, new float[]{0.1f, 0.6f}, new Color[]{new Color(204, 204, 204), Color.white});
            activatedPainter = new CompoundPainter<>(new MattePainter(paint));
        }
        else if(t8Painter != null)
        {
            activatedPainter = new T8PainterAdapter(t8Painter);
        }
    }

    private class ComboBoxMouseListener extends MouseAdapter
    {
        private boolean rollover;
        private boolean pressed = false;
        private boolean armed;

        @Override
        public void mouseEntered(MouseEvent e)
        {
            rollover = true;

            if (!backgroundAnimator.isRunning())
            {
                backgroundAnimator.start();
            }
            else if (!backgroundAnimator.reverseNow()) //If the animator failed to reverse, then just stop and start it again
            {
                backgroundAnimator.restart();
            }
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
            rollover = false;
            if (!backgroundAnimator.isRunning())
            {
                backgroundAnimator.startReverse();
            }
            else if(!backgroundAnimator.reverseNow()) //If the animator failed to reverse, then just stop and start it again
            {
                backgroundAnimator.restartReverse();
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            armed = e.getButton() == MouseEvent.BUTTON1;
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (armed && contains(e.getPoint()))
            {
                setPressed(!pressed);

                if(!pressed) applyFilter();
            }
            armed = false;
        }

        public void setPressed(boolean pressed)
        {
            this.pressed = pressed;
            comboBoxPopup.setVisible(pressed);
            repaint();
        }

        public boolean isPressed()
        {
            return pressed;
        }

        public boolean isRollover()
        {
            return rollover;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="T8Component Methods">
    @Override
    public T8ComponentController getController()
    {
        return controller;
    }

    public T8Context getContext()
    {
        return controller.getContext();
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public T8DataSearchComboBoxDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>();
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }
//</editor-fold>
}
