package com.pilog.t8.ui.table.object;

import com.pilog.t8.ui.table.T8Table;
import com.pilog.t8.data.object.T8ComponentDataObjectProvider;

/**
 * @author Bouwer du Preez
 */
public interface T8TableDataObjectProvider extends T8ComponentDataObjectProvider
{
    public void setTable(T8Table table);
    public boolean providesDataObject(String objectIdentifier);
}
