package com.pilog.t8.ui.entityeditor.datacombobox;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.ui.entityeditor.textfield.T8TextFieldDataEntityEditorAPIHandler;
import com.pilog.t8.ui.datacombobox.T8DataComboBoxOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBoxDataEntityEditorOperationHandler extends T8DataComboBoxOperationHandler
{
    private T8DataComboBoxDataEntityEditor comboBox;

    public T8DataComboBoxDataEntityEditorOperationHandler(T8DataComboBoxDataEntityEditor comboBox)
    {
        super(comboBox);
        this.comboBox = comboBox;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_COMMIT_CHANGES))
        {
            comboBox.commitEditorChanges();
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_SET_DATA_ENTITY))
        {
            comboBox.setEditorDataEntity((T8DataEntity)operationParameters.get(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY));
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_GET_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY, comboBox.getEditorDataEntity());
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
