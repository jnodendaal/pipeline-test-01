package com.pilog.t8.ui.datalookupdialog;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.button.T8ButtonDefinition;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogAPIHandler;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogDefinition;
import com.pilog.t8.definition.ui.datasearch.T8DataSearchAPIHandler;
import com.pilog.t8.definition.ui.datasearch.T8DataSearchDefinition;
import com.pilog.t8.definition.ui.dialog.T8DialogAPIHandler;
import com.pilog.t8.definition.ui.table.T8TableAPIHandler;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import com.pilog.t8.ui.button.T8Button;
import com.pilog.t8.ui.datasearch.T8DataSearch;
import com.pilog.t8.ui.dialog.T8Dialog;
import com.pilog.t8.ui.table.T8Table;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupDialog extends T8Dialog
{
    private final T8ComponentController parentController;
    private final T8ComponentController controller;
    private final T8DataLookupDialogDefinition definition;
    private T8Table table;
    private T8DataSearch search;
    private final T8DataLookupDialogOperationHandler operationHandler;
    private List<T8DataEntity> selectedEntities;

    private static final String CANCEL_BUTTON_IDENTIFIER = "$TEMP_CANCEL_BUTTON";
    private static final String PROCEED_BUTTON_IDENTIFIER = "$TEMP_PROCEED_BUTTON";

    public T8DataLookupDialog(T8DataLookupDialogDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.parentController = controller;
        this.controller = new T8DefaultComponentController(controller, new T8ProjectContext(controller.getContext(), definition.getRootProjectId()), definition.getIdentifier(), false);
        this.controller.addComponentEventListener(new DialogEventListener());
        this.definition = definition;
        this.operationHandler = new T8DataLookupDialogOperationHandler(this);
    }

    @Override
    public T8ComponentController getController()
    {
        return parentController;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        try
        {
            T8TableDefinition tableDefinition;
            T8DataSearchDefinition searchDefinition;
            T8ButtonDefinition cancelButtonDefinition;
            T8ButtonDefinition proceedButtonDefinition;
            GridBagConstraints constraints;
            T8Button cancelButton;
            T8Button proceedButton;
            Container container;

            // Initialize the super component (dialog).
            super.initializeComponent(inputParameters);
            setHeaderTitle("Data Lookup");

            tableDefinition = definition.getTableDefinition();
            table = (T8Table)tableDefinition.getNewComponentInstance(controller);
            table.initializeComponent(null);

            searchDefinition = definition.getDataSearchDefinition();
            if (searchDefinition != null)
            {
                search = (T8DataSearch)searchDefinition.getNewComponentInstance(controller);
                search.initializeComponent(null);
            }

            cancelButtonDefinition = new T8ButtonDefinition(CANCEL_BUTTON_IDENTIFIER);
            cancelButtonDefinition.setButtonText("Cancel");
            cancelButton = (T8Button)cancelButtonDefinition.getNewComponentInstance(controller);
            cancelButton.initializeComponent(null);

            proceedButtonDefinition = new T8ButtonDefinition(PROCEED_BUTTON_IDENTIFIER);
            proceedButtonDefinition.setButtonText("Proceed");
            proceedButton = (T8Button)proceedButtonDefinition.getNewComponentInstance(controller);
            proceedButton.initializeComponent(null);

            container = getContentPanel();
            container.removeAll(); //We will be replacing the default contents with out own
            container.setLayout(new GridBagLayout());

            if (this.search != null)
            {
                constraints = new GridBagConstraints();
                constraints.insets = new Insets(5, 5, 0, 5);
                constraints.gridx = 0;
                constraints.gridy = 0;
                constraints.gridwidth = 2;
                constraints.gridheight = 1;
                constraints.fill = GridBagConstraints.HORIZONTAL;
                constraints.weightx = 0.1;
                container.add(search, constraints);
            }

            constraints = new GridBagConstraints();
            constraints.insets = new Insets(5, 5, 5, 5);
            constraints.gridx = 0;
            constraints.gridy = 1;
            constraints.gridwidth = 2;
            constraints.gridheight = 1;
            constraints.fill = GridBagConstraints.BOTH;
            constraints.weightx = 0.1;
            constraints.weighty = 0.1;
            container.add(table, constraints);

            constraints = new GridBagConstraints();
            constraints.insets = new Insets(0, 5, 5, 5);
            constraints.gridx = 0;
            constraints.gridy = 2;
            constraints.gridwidth = 1;
            constraints.gridheight = 1;
            constraints.anchor = GridBagConstraints.WEST;
            constraints.fill = GridBagConstraints.NONE;
            container.add(cancelButton, constraints);

            constraints = new GridBagConstraints();
            constraints.insets = new Insets(0, 5, 5, 5);
            constraints.gridx = 1;
            constraints.gridy = 2;
            constraints.gridwidth = 1;
            constraints.gridheight = 1;
            constraints.anchor = GridBagConstraints.EAST;
            constraints.fill = GridBagConstraints.NONE;
            container.add(proceedButton, constraints);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while initializing data lookup dialog content.", e);
        }
    }

    @Override
    public void startComponent()
    {
        if (search != null) search.startComponent();
        if (table != null)
        {
            table.startComponent();
            if (this.search == null) this.table.refreshData();
        }
    }

    @Override
    public void stopComponent()
    {
        if (search != null) search.stopComponent();
        if (table != null) table.stopComponent();
        dispose();
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private void handleEntitySelectionEvent(List<T8DataEntity> selectedEntityList)
    {
        if ((selectedEntityList != null) && (selectedEntityList.size() > 0))
        {
            int maximumSelectionQuantity;

            maximumSelectionQuantity = definition.getMaximumSelectionQuantity();
            if (selectedEntityList.size() <= maximumSelectionQuantity)
            {
                setSelectedDataEntities(selectedEntityList);
                executeOperation(T8DialogAPIHandler.OPERATION_CLOSE_DIALOG, new HashMap<>());
            }
            else
            {
                if (definition.getMaximumSelectionQuantity() == 1)
                {
                    JOptionPane.showMessageDialog(this, "Please select only one row of data before proceeding.", "Invalid Selection", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    JOptionPane.showMessageDialog(this, "Please select " + maximumSelectionQuantity + " rows of data or less before proceeding.", "Invalid Selection", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else
        {
            JOptionPane.showMessageDialog(this, "Please select a row of data before proceeding.", "Invalid Selection", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void setPrefilter(String identifier, T8DataFilter prefilter)
    {
        if (table != null)
        {
            table.setPrefilter(identifier, prefilter);
        }
    }

    public void setSelectedDataEntities(List<T8DataEntity> entityList)
    {
        HashMap<String, Object> eventParameters;

        // Set the selected entity and display value.
        selectedEntities = entityList;

        // Report the selection changed event.
        eventParameters = new HashMap<>();
        eventParameters.put(T8DataLookupDialogAPIHandler.PARAMETER_DATA_ENTITY, entityList != null ? entityList.get(0) : null);
        eventParameters.put(T8DataLookupDialogAPIHandler.PARAMETER_DATA_ENTITY_LIST, entityList);
        parentController.reportEvent(T8DataLookupDialog.this, definition.getComponentEventDefinition(T8DataLookupDialogAPIHandler.EVENT_SELECTION_CHANGED), eventParameters);
    }

    public T8DataEntity getSelectedDataEntity()
    {
        if ((selectedEntities != null) && (selectedEntities.size() > 0))
        {
            return selectedEntities.get(0);
        }
        else return null;
    }

    public List<T8DataEntity> getSelectedDataEntities()
    {
        return selectedEntities;
    }

    public void refreshData()
    {
        table.refreshData();
    }

    public String getDataEntityIdentifier()
    {
        return definition.getDataEntityIdentifier();
    }

    private class DialogEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;
            T8Component component;
            Map<String, Object> eventParameters;
            String componentIdentifier;

            eventDefinition = event.getEventDefinition();
            component = event.getComponent();
            eventParameters = event.getEventParameters();
            componentIdentifier = component.getComponentDefinition().getIdentifier();
            if (componentIdentifier.equals(CANCEL_BUTTON_IDENTIFIER))
            {
                executeOperation(T8DialogAPIHandler.OPERATION_CLOSE_DIALOG, new HashMap<>());
            }
            else if (componentIdentifier.equals(PROCEED_BUTTON_IDENTIFIER))
            {
                List<T8DataEntity> selectedEntityList;

                selectedEntityList = table.getSelectedDataEntities();
                handleEntitySelectionEvent(selectedEntityList);
            }
            else if (eventDefinition.getIdentifier().equals(T8TableAPIHandler.EVENT_DOUBLE_CLICKED))
            {
                List<T8DataEntity> selectedEntityList;

                selectedEntityList = table.getSelectedDataEntities();
                handleEntitySelectionEvent(selectedEntityList);
            }
            // If the search is not null and used, this will intercept the search event
            else if (eventDefinition.getIdentifier().equals(T8DataSearchAPIHandler.EVENT_SEARCH))
            {
                T8DataFilter dataFilter;

                dataFilter = (T8DataFilter)eventParameters.get(T8DataSearchAPIHandler.PARAMETER_DATA_FILTER);
                table.setPrefilter(null, dataFilter);
                table.refreshData();
            }
        }
    }
}
