/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.filter.panel.component.date;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.time.T8Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class DatePickerOnComponent extends DatePickerSingleDateComponent
{
    public DatePickerOnComponent()
    {
        super("On");
    }

    @Override
    public T8DataFilterCriteria getFilterCriteria(
            Date datePicker, String dataEntityFieldIdentifier)
    {
        T8DataFilterCriteria filterCriteria = new T8DataFilterCriteria();
        if (datePicker != null)
        {
            Calendar yesterdayMidnight = Calendar.getInstance();
            yesterdayMidnight.setTime(datePicker);
            yesterdayMidnight.set(Calendar.HOUR, 0);
            yesterdayMidnight.set(Calendar.MINUTE, 0);
            yesterdayMidnight.set(Calendar.SECOND, 0);
            yesterdayMidnight.set(Calendar.MILLISECOND, 0);

            Calendar todayMidnight = Calendar.getInstance();
            todayMidnight.setTime(datePicker);
            todayMidnight.set(Calendar.HOUR, 24);
            todayMidnight.set(Calendar.MINUTE, 0);
            todayMidnight.set(Calendar.SECOND, 0);
            todayMidnight.set(Calendar.MILLISECOND, 0);

            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND,
                                           dataEntityFieldIdentifier,
                                           T8DataFilterCriterion.DataFilterOperator.GREATER_THAN_OR_EQUAL,
                                           new T8Timestamp(yesterdayMidnight.getTimeInMillis()));

            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND,
                                           dataEntityFieldIdentifier,
                                           T8DataFilterCriterion.DataFilterOperator.LESS_THAN_OR_EQUAL,
                                           new T8Timestamp(todayMidnight.getTimeInMillis()));
        }
        return filterCriteria;
    }
}
