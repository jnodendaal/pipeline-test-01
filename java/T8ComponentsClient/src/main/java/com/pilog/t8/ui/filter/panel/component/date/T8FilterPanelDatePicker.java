package com.pilog.t8.ui.filter.panel.component.date;

import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.T8FilterPanelDatePickerDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.filter.panel.T8FilterPanelComponent;
import com.pilog.t8.ui.filter.panel.component.T8FilterPanelComponentOperationHandler;
import com.pilog.t8.ui.filter.panel.component.list.T8FilterPanelCheckedList;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.metal.MetalComboBoxIcon;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelDatePicker extends JComponent implements ActionListener, AncestorListener, T8FilterPanelComponent, T8Component
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8FilterPanelCheckedList.class.getName());
    private final T8FilterPanelDatePickerDefinition definition;
    private final T8FilterPanelComponentOperationHandler operationHandler;
    private final T8ComponentController controller;
    private final DatePickerComponentFactory datePickerComponentFactory;
    private final List<ChangeListener> changeListeners;
    private T8DataFilterCriteria lastCriteria;
    protected JComponent drop_down_comp;
    protected JComponent visible_comp;
    protected JXButton arrow;
    protected JWindow popup;
    protected String dataEntityValueIdentifier;

    public T8FilterPanelDatePicker(T8FilterPanelDatePickerDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.dataEntityValueIdentifier = definition.getDataEntityKeyFieldIdentifier();
        this.operationHandler = new T8FilterPanelComponentOperationHandler(this);

        Icon displayIcon = null;

        if (definition.getIconDefinition() != null)
        {
            displayIcon = definition.getIconDefinition().getImage().getImageIcon();
        }

        if (displayIcon == null)
        {
            displayIcon = new MetalComboBoxIcon();
        }

        arrow = new JXButton(displayIcon);

        if (definition.isTransparentSet() != null && definition.isTransparentSet())
        {
            arrow.setOpaque(false);
            arrow.setContentAreaFilled(false);
            arrow.setBorderPainted(false);
        }
        Insets insets = arrow.getMargin();
        arrow.setMargin(new Insets(insets.top, 1, insets.bottom, 1));
        arrow.addActionListener(this);
        addAncestorListener(this);

        datePickerComponentFactory = new DatePickerComponentFactory();
        arrow.setText(getTranslatedString(definition.getDisplayLabel()));
        arrow.setHorizontalAlignment(SwingConstants.LEFT);
        arrow.setIconTextGap(5);
        this.changeListeners = new ArrayList<>();
        lastCriteria = new T8DataFilterCriteria();
    }

//<editor-fold defaultstate="collapsed" desc="Boiler Plate Code">

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier,
                                                Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public void refresh() throws Exception
    {
        //do nothing
    }

    @Override
    public Component getComponent()
    {
        return this;
    }

    @Override
    public String toString()
    {
        return this.definition.getIdentifier();
    }

    @Override
    public void ancestorAdded(AncestorEvent event)
    {
        hidePopup();
    }

    @Override
    public void ancestorRemoved(AncestorEvent event)
    {
        hidePopup();
    }

    @Override
    public void ancestorMoved(AncestorEvent event)
    {
        if (event.getSource() != popup)
        {
            hidePopup();
        }
    }

//</editor-fold>

    @Override
    public void initializeComponent(
            Map<String, Object> inputParameters)
    {
        try
        {
            initialize(controller.getContext());
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to initialize component " + definition.getIdentifier(), ex);
            throw new RuntimeException(ex);
        }
    }

    protected void setupLayout()
    {
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        setLayout(gbl);

        c.weightx = 1.0;
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 0;
        c.fill = c.BOTH;
        c.anchor = GridBagConstraints.WEST;
        //gbl.setConstraints(visible_comp, c);
        //add(visible_comp);

        //c.weightx = 0;
        //c.gridx++;
        gbl.setConstraints(arrow, c);
        add(arrow);

    }

    @Override
    public T8DataFilterCriteria getFilterCriteria()
    {
        for (DatePickerComponent datePickerComponent : datePickerComponentFactory.getDatePickerComponents())
        {
            if (datePickerComponent.isSelected())
            {
                return datePickerComponent.getFilterCriteria(dataEntityValueIdentifier);
            }
        }
        return new T8DataFilterCriteria();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        // build pop-up window
        popup = new JWindow(getFrame(null));
        Dimension componentSize = new Dimension(getSize().width, drop_down_comp.getPreferredSize().height);
        if (getSize().width < 350)
        {
            componentSize = new Dimension(350, drop_down_comp.getPreferredSize().height);
        }
        drop_down_comp.setSize(componentSize);
        drop_down_comp.setPreferredSize(componentSize);
        popup.getContentPane().add(drop_down_comp);
        popup.addWindowFocusListener(new WindowAdapter()
        {
            @Override
            public void windowLostFocus(WindowEvent evt)
            {
                popup.setVisible(false);
                boolean filtered = false;
                for (DatePickerComponent datePickerComponent : datePickerComponentFactory.getDatePickerComponents())
                {
                    if (datePickerComponent.isSelected())
                    {
                        filtered = true;
                        break;
                    }
                }
                (arrow).setText(getTranslatedString(definition.getDisplayLabel()) + (filtered
                                                                                     ? (": " + getTranslatedString("Filtered"))
                                                                                     : ""));
                T8DataFilterCriteria newCriteria = getFilterCriteria();
                if (newCriteria != null && !newCriteria.equalsFilterCriteria(lastCriteria))
                {
                    lastCriteria = newCriteria;
                    fireStateChanged();
                }
            }
        });
        popup.pack();

        // show the pop-up window
        Point pt = arrow.getLocationOnScreen();
        pt.translate(0, arrow.getHeight());
        popup.setLocation(pt);
        popup.toFront();
        popup.setVisible(true);
        popup.requestFocusInWindow();
    }

    protected Frame getFrame(Component comp)
    {
        if (comp == null)
        {

            comp = this;
        }
        if (comp.getParent() instanceof Frame)
        {
            return (Frame) comp.getParent();
        }
        return getFrame(comp.getParent());

    }

    public void hidePopup()
    {
        if (popup != null && popup.isVisible())
        {
            popup.setVisible(false);
        }
    }

    @Override
    public void addChangeListener(ChangeListener changeListener)
    {
        changeListeners.add(changeListener);
    }

    @Override
    public void removeChangeListener(ChangeListener changeListener)
    {
        changeListeners.remove(changeListener);
    }

    @Override
    public void fireStateChanged()
    {
        for (ChangeListener changeListener : changeListeners)
        {
            changeListener.stateChanged(new ChangeEvent(this));
        }
    }

    @Override
    public void initialize(T8Context context) throws Exception
    {
        JXPanel hostPanel = new JXPanel();
        hostPanel.setLayout(new BoxLayout(hostPanel, BoxLayout.Y_AXIS));
        hostPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK, 1), BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        ButtonGroup radioButtonGroup = new ButtonGroup();

        GridBagConstraints gridBagConstraints = new GridBagConstraints(0, 0, 1, 1, 0.1, 0.1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0);
        addNoFilterOption(hostPanel, gridBagConstraints, radioButtonGroup);
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        for (DatePickerComponent datePickerComponent : datePickerComponentFactory.getDatePickerComponents())
        {
            datePickerComponent.initialize(controller);

            JXPanel containerPanel = new JXPanel();
            containerPanel.setOpaque(false);
            containerPanel.setLayout(new GridBagLayout());

            JRadioButton radioButton = new DatePickerRadioButton(datePickerComponent);
            radioButton.setOpaque(false);
            radioButtonGroup.add(radioButton);

            gridBagConstraints.gridx = 0;
            gridBagConstraints.weightx = 0.0;
            containerPanel.add(radioButton, gridBagConstraints);
            gridBagConstraints.gridx = 1;
            gridBagConstraints.weightx = 0.1;
            containerPanel.add(datePickerComponent.getComponent(), gridBagConstraints);

            hostPanel.add(containerPanel);
        }

        if (definition.getBackgroundPainterDefinition() != null)
        {
            hostPanel.setBackgroundPainter(new T8PainterAdapter(definition.getBackgroundPainterDefinition().getNewPainterInstance()));
        }
        drop_down_comp = hostPanel;

        setupLayout();
    }

    private void addNoFilterOption(JXPanel hostPanel,
                                   GridBagConstraints gridBagConstraints,
                                   ButtonGroup radioButtonGroup)
    {

        JXPanel containerPanel = new JXPanel();
        containerPanel.setOpaque(false);
        containerPanel.setLayout(new GridBagLayout());

        JRadioButton radioButton = new JRadioButton();
        radioButton.setOpaque(false);
        radioButtonGroup.add(radioButton);
        radioButton.setSelected(true);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.weightx = 0.0;
        containerPanel.add(radioButton, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        containerPanel.add(new JXLabel(getTranslatedString("No Filter")), gridBagConstraints);

        hostPanel.add(containerPanel);
    }

    private String getTranslatedString(String text)
    {
        return controller.getClientContext().getConfigurationManager().getUITranslation(controller.getContext(), text);
    }

    @Override
    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
    }

    class DatePickerRadioButton extends JRadioButton implements ChangeListener
    {
        private final DatePickerComponent datePickerComponent;

        DatePickerRadioButton(DatePickerComponent datePickerComponent)
        {
            this.datePickerComponent = datePickerComponent;
            addChangeListener(this);
        }

        @Override
        public void stateChanged(ChangeEvent e)
        {
            datePickerComponent.setSelected(isSelected());
        }
    }
}
