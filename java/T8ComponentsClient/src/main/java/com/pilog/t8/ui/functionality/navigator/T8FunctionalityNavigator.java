package com.pilog.t8.ui.functionality.navigator;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewClosedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewListener;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewUpdatedEvent;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.functionality.navigator.T8FunctionalityNavigatorDefinition;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.functionality.view.FunctionalityViewPane;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingWorker;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityNavigator extends JXPanel implements T8Component
{
    private final T8FunctionalityNavigatorDefinition definition;
    private final T8ComponentController controller;
    private final T8FunctionalityManager functionalityManager;
    private final T8ConfigurationManager configurationManager;
    private final T8Context context;
    private FunctionalityViewPane viewPane;
    private T8FunctionalityNavigatorMenuPanel menuPanel;
    private final FunctionalityViewListener functionalityViewListener;
    private T8FunctionalityAccessHandle executionHandle;

    public T8FunctionalityNavigator(T8FunctionalityNavigatorDefinition definition, T8ComponentController controller)
    {
        this.controller = controller;
        this.context = controller.getContext();
        this.functionalityManager = controller.getClientContext().getFunctionalityManager();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.definition = definition;
        this.functionalityViewListener = new FunctionalityViewListener();
        initComponents();
        setupComponents();
        setupPainters();
    }

    private void setupComponents()
    {
        GridBagConstraints constraints;

        constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridheight = 1;
        constraints.gridwidth = 1;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.insets = new Insets(0, 0, 0, 0);
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.weightx = 0.1;
        constraints.weighty = 0.1;

        viewPane = new FunctionalityViewPane(controller);
        viewPane.setNavigationBarVisible(false);
        viewPane.setViewOpaque(false);
        add(viewPane, constraints);

        menuPanel = new T8FunctionalityNavigatorMenuPanel(this);
        constraints.weightx = 0;
        constraints.gridx = 0;
        add(menuPanel, constraints);
    }

    private void setupPainters()
    {
        T8PainterDefinition painterDefinition;

        // Initialize painter for painting the background of the entire component.
        painterDefinition = definition.getBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            setBackgroundPainter(new T8PainterAdapter(painterDefinition.getNewPainterInstance()));
        }
    }

    public String translate(String inputString)
    {
        return controller.translate(inputString);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8FunctionalityNavigatorDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        loadFunctionalityGroup(definition.getFunctionalityGroupIdentifier(), null, true);
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        viewPane.closeAllFunctionalityViews();
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    protected void loadFunctionalityGroup(String functionalityGroupIdentifier, String selectedGroupIdentifier, boolean closeExistingFunctionalityViews)
    {
        FunctionalityGroupLoader loader;

        loader = new FunctionalityGroupLoader(functionalityManager, functionalityGroupIdentifier, selectedGroupIdentifier, closeExistingFunctionalityViews);
        loader.execute();
    }

    void handleFunctionalitySelection(final T8FunctionalityHandle functionalityHandle)
    {
        try
        {
            // Close existing functionality views.
            viewPane.closeAllFunctionalityViews();

            // Execute the new functionality in a separate thread (we don't want to block the EDT).
            new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        T8Log.log("Invoking Functionality: " + functionalityHandle);
                        executionHandle = viewPane.accessFunctionality(functionalityHandle.getId(), functionalityHandle.getPublicParameters());
                        if (executionHandle != null)
                        {
                            T8FunctionalityView functionalityView;

                            functionalityView = viewPane.getFunctionalityView(executionHandle.getFunctionalityIid());
                            if (functionalityView != null)
                            {
                                functionalityView.addFunctionalityViewListener(functionalityViewListener);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        T8Log.log("Exception while invoking functionality: " + functionalityHandle, e);
                    }
                }
            }.start();
        }
        catch (Exception e)
        {
            T8Log.log("Exception while invoking functionality: " + functionalityHandle, e);
        }
    }

    void setSelectedFunctionalityGroup(T8FunctionalityGroupHandle functionalityGroup, boolean closeExistingFunctionalityViews)
    {
        try
        {
            int selectedFunctionalityMenuItem = -1;

            T8Log.log("Opening Functionality Group: " + functionalityGroup);
            if (closeExistingFunctionalityViews) viewPane.closeAllFunctionalityViews();
            else selectedFunctionalityMenuItem = menuPanel.getSelectedMenuItemIndex();

            menuPanel.setFunctionalityGroup(functionalityGroup);
            menuPanel.setSelectedMenuItemIndex(selectedFunctionalityMenuItem);

            // Invoke the group overview functionality (if one is specified).
            if (functionalityGroup != null)
            {
                final T8FunctionalityHandle overviewFunctionality;

                overviewFunctionality = functionalityGroup.getOverviewFunctionality();
                if (overviewFunctionality != null)
                {
                    // Execute the new group overview functionality in a separate thread (we don't want to block the EDT).
                    new Thread()
                    {
                        @Override
                        public void run()
                        {
                            try
                            {
                                T8Log.log("Invoking Group overview Functionality: " + overviewFunctionality);
                                executionHandle = viewPane.accessFunctionality(overviewFunctionality.getId(), overviewFunctionality.getPublicParameters());
                                if (executionHandle != null)
                                {
                                    T8FunctionalityView functionalityView;

                                    functionalityView = viewPane.getFunctionalityView(executionHandle.getFunctionalityIid());
                                    if (functionalityView != null)
                                    {
                                        functionalityView.addFunctionalityViewListener(functionalityViewListener);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                T8Log.log("Exception while invoking group overview functionality: " + overviewFunctionality, e);
                            }
                        }
                    }.start();
                }
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while opening functionality group: " + functionalityGroup, e);
        }
    }

    private class FunctionalityViewListener implements T8FunctionalityViewListener
    {
        @Override
        public void functionalityViewUpdated(T8FunctionalityViewUpdatedEvent event)
        {
            T8Log.log("Functionality View " + event.getFunctionalityView() + " Updated.  Refreshing functionality navigator.");
            loadFunctionalityGroup(definition.getFunctionalityGroupIdentifier(), menuPanel.getFunctionalityGroup().getIdentifier(), false);
        }

        @Override
        public void functionalityViewClosed(T8FunctionalityViewClosedEvent event)
        {
            T8Log.log("Functionality View " + event.getFunctionalityView() + " Closed.  Refreshing functionality navigator.");
            loadFunctionalityGroup(definition.getFunctionalityGroupIdentifier(), menuPanel.getFunctionalityGroup().getIdentifier(), true);
        }
    }

    private class FunctionalityGroupLoader extends SwingWorker<Void, Void>
    {
        private final T8FunctionalityManager functionalityManager;
        private final boolean closeExistingFunctionalityViews;
        private final String groupId;
        private final String selectedGroupIdentifier;
        private T8FunctionalityGroupHandle handle;

        private FunctionalityGroupLoader(T8FunctionalityManager functionalityManager, String groupIdentifier, String selectedGroupIdentifier, boolean closeExistingFunctionalityViews)
        {
            this.functionalityManager = functionalityManager;
            this.groupId = groupIdentifier;
            this.selectedGroupIdentifier = selectedGroupIdentifier;
            this.closeExistingFunctionalityViews = closeExistingFunctionalityViews;
        }

        @Override
        protected Void doInBackground() throws Exception
        {
            handle = functionalityManager.getFunctionalityGroupHandle(context, groupId, null, null);
            return null;
        }

        @Override
        public void done()
        {
            try
            {
                // Set the selected group.
                if (selectedGroupIdentifier == null)
                {
                    setSelectedFunctionalityGroup(handle, this.closeExistingFunctionalityViews);
                }
                else
                {
                    setSelectedFunctionalityGroup(handle.getFunctionalityGroup(selectedGroupIdentifier), this.closeExistingFunctionalityViews);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while loading functionality group: " + groupId, e);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
