package com.pilog.t8.ui.table;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.entity.T8DataEntityValidationError;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8TableRowHeader
{
    private final T8DefaultTableModel model;
    private int rowIndex;
    private boolean ticked;
    private T8DataEntity dataEntity;
    private List<T8DataEntityValidationError> validationErrors;

    public T8TableRowHeader(T8DefaultTableModel model, T8DataEntity entity, int rowIndex)
    {
        this.model = model;
        this.dataEntity = entity;
        this.rowIndex = rowIndex;
        this.ticked = false;
    }

    public T8DataEntity getDataEntity()
    {
        return dataEntity;
    }

    public void setDataEntity(T8DataEntity dataEntity)
    {
        this.dataEntity = dataEntity;
    }

    public int getRowIndex()
    {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex)
    {
        this.rowIndex = rowIndex;
    }

    public boolean isTicked()
    {
        return ticked;
    }

    public void setTicked(boolean ticked)
    {
        if (this.ticked != ticked)
        {
            this.ticked = ticked;
            this.model.rowHeaderTickSelectionChanged(this);
        }
    }

    public List<T8DataEntityValidationError> getValidationErrors()
    {
        return validationErrors;
    }

    public void setValidationErrors(List<T8DataEntityValidationError> validationErrors)
    {
        this.validationErrors = validationErrors;
    }

    public void addValidationErrors(List<T8DataEntityValidationError> validationErrors)
    {
        if (this.validationErrors == null)
        {
            this.validationErrors = validationErrors;
        }
        else this.validationErrors.addAll(validationErrors);
    }
}
