package com.pilog.t8.ui.file.uploadpanel;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.file.uploadpanel.T8FileUploadPanelDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Bouwer du Preez
 */
public class T8FileUploadPanel extends FileUploadPanel implements T8Component
{
    private final T8FileUploadPanelOperationHandler operationHandler;
    private final T8ComponentController controller;
    private final T8FileUploadPanelDefinition definition;

    public T8FileUploadPanel(T8FileUploadPanelDefinition definition, T8ComponentController controller)
    {
        super(controller);
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8FileUploadPanelOperationHandler(this);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> hm1)
    {
        String fileContextId;
        String fileContextInstanceId = null;
        String filePath;
        String toolTipText;
        
        //set values from definition.
        fileContextId = definition.getFileContextId();
        //set fileContextInstanceId if the file context id has been supplied.
        if (fileContextId != null) fileContextInstanceId = UUID.randomUUID().toString();
        filePath = definition.getFilePath();
        toolTipText = definition.getTooltipText();
                
        //Set the parent file upload class values from the definition.
        setUploadContext(fileContextId, fileContextInstanceId, filePath);
        setToolTipText(toolTipText);  
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        System.out.println("Calling stop component for T8FileUploadPanel...");
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }
}
