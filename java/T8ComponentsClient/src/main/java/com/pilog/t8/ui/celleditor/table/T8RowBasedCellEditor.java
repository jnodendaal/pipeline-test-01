/**
 * Created on 29 Jun 2015, 4:16:21 PM
 */
package com.pilog.t8.ui.celleditor.table;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * A {@code TableCellEditor} implementation which allows for cell editors per
 * row for the column to which the editor is attached. The editor essentially
 * contains multiple editors, of which a default editor is required for
 * initialization. After initialization, additional editors can be added per row
 * which will override the default editor.
 *
 * @author Gavin Boshoff
 */
public class T8RowBasedCellEditor extends AbstractCellEditor implements TableCellEditor
{
    private final Map<Integer, TableCellEditor> rowSpecificEditors;
    private final TableCellEditor defaultCellEditor;

    private TableCellEditor lastSelectedEditor;

    public T8RowBasedCellEditor(TableCellEditor defaultCellEditor)
    {
        this.rowSpecificEditors = new HashMap<>();
        this.defaultCellEditor = defaultCellEditor;
    }

    /**
     * Adds a editor which will be used for the row specified.
     *
     * @param row The {@code int} table row for which the editor should be used
     * @param editor The {@code TableCellEditor} to be used for the specified
     *      row
     */
    public void addTableCellEditor(int row, TableCellEditor editor)
    {
        this.rowSpecificEditors.put(row, editor);
    }

    @Override
    public Object getCellEditorValue()
    {
        return this.lastSelectedEditor.getCellEditorValue();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        if ((this.lastSelectedEditor = this.rowSpecificEditors.get(row)) == null)
        {
            this.lastSelectedEditor = this.defaultCellEditor;
        }

        return this.lastSelectedEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
    }
}