package com.pilog.t8.ui.multisplitpane;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.multisplitpane.T8MultiSplitPaneDefinition;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.jdesktop.swingx.JXMultiSplitPane;
import org.jdesktop.swingx.MultiSplitLayout;

/**
 *
 * @author Bouwer.duPreez
 */
public class T8MultiSplitPane extends JXMultiSplitPane implements T8Component
{
    private final T8MultiSplitPaneDefinition definition;
    private final T8ComponentController controller;
    private final LinkedHashMap<String, T8Component> childComponents;
    private final T8MultiSplitPaneOperationHandler operationHandler;

    public T8MultiSplitPane(T8MultiSplitPaneDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.setOpaque(false);
        this.operationHandler = new T8MultiSplitPaneOperationHandler(this);
        this.childComponents = new LinkedHashMap<>();
    }
    
    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8PainterDefinition backgroundPainterDefinition;
        String layout;
        
        backgroundPainterDefinition = definition.getBackgroundPainterDefinition();
        if (backgroundPainterDefinition != null)
        {
            this.setBackgroundPainter(new T8PainterAdapter(backgroundPainterDefinition.getNewPainterInstance()));
        }
        
        layout = definition.getLayout();
        if (layout == null)
        {
            throw new RuntimeException("Invalid Multi-Split layout encountered: " + layout);
        }
        else
        {
            MultiSplitLayout.Node modelRoot;
            
            layout = layout.replace('_', '.');
            layout = layout.replace('$', 'C');
            modelRoot = MultiSplitLayout.parseModel(layout);
            getMultiSplitLayout().setLayoutMode(MultiSplitLayout.NO_MIN_SIZE_LAYOUT);
            getMultiSplitLayout().setLayoutByWeight(true);
            getMultiSplitLayout().setModel(modelRoot);
        }
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        String componentIdentifier;
        String formattedIdentifier;
        
        componentIdentifier = component.getComponentDefinition().getIdentifier();
        childComponents.put(componentIdentifier, component);
        
        // The multi-split layout does not like '_' characters in the names of leafs, so rather user '.'.
        formattedIdentifier = componentIdentifier.replace('_', '.');
        formattedIdentifier = formattedIdentifier.replace('$', 'C');
        add((Component)component, formattedIdentifier); 
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        return childComponents.get(identifier);
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        T8Component childComponent;
        
        childComponent = childComponents.get(identifier);
        if (childComponent != null)
        {
            this.remove((Component)childComponent);
            return childComponent;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(childComponents.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
        childComponents.clear();
    }
}
