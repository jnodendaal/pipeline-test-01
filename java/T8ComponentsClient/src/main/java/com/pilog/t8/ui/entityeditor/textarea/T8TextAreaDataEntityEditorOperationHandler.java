package com.pilog.t8.ui.entityeditor.textarea;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.ui.entityeditor.textfield.T8TextFieldDataEntityEditorAPIHandler;
import com.pilog.t8.ui.textarea.T8TextAreaOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextAreaDataEntityEditorOperationHandler extends T8TextAreaOperationHandler
{
    private final T8TextAreaDataEntityEditor textArea;

    public T8TextAreaDataEntityEditorOperationHandler(T8TextAreaDataEntityEditor textArea)
    {
        super(textArea);
        this.textArea = textArea;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_COMMIT_CHANGES))
        {
            textArea.commitEditorChanges();
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_SET_DATA_ENTITY))
        {
            textArea.setEditorDataEntity((T8DataEntity)operationParameters.get(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY));
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_GET_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY, textArea.getEditorDataEntity());
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
