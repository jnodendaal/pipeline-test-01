package com.pilog.t8.ui.table;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.ui.table.T8TableAPIHandler;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The table model columns each have the following three important attributes:
 *  - Column Identifier:   The identifier of the column definition that will
 *                         also be the name of the column in the table model.
 *  - Column Name:         The display name of the column.
 *  - Source Name:         The identifier of the data source field from which
 *                         the column's data is obtained.
 *
 * @author Bouwer du Preez
 */
public class T8TableModelHandler
{
    private final T8Table table;
    private final T8TableDefinition tableDefinition;
    private final ArrayList<T8TableColumnDefinition> columnDefinitions;
    private final T8RowTickListener tickListener;
    private T8DefaultTableModel tableModel;

    public T8TableModelHandler(T8Table table, T8TableDefinition tableDefinition)
    {
        this.tableDefinition = tableDefinition;
        this.columnDefinitions = tableDefinition.getColumnDefinitions();
        this.tableModel = null;
        this.tickListener = new TickListener(tableDefinition.getTickSelectionMode() == T8TableDefinition.TickSelectionMode.SINGLE_SELECTION);
        this.table = table;
    }

    public void setModel(T8DefaultTableModel tableModel)
    {
        if (this.tableModel != null)
        {
            this.tableModel.removeRowTickListener(tickListener);
        }

        this.tableModel = tableModel;
        this.tableModel.addRowTickListener(tickListener);
    }

    public void clearData()
    {
        tableModel.clear();
    }

    /**
     * Returns the index in the model of the specified column.  This method
     * takes into account that the first column of the model (index 0) contains
     * a system value i.e. this method will return the column definition index
     * + 1 (for the system column).
     *
     * @param columnIdentifier The identifier of the column for which an index
     * will be returned.
     * @return The index of the specified column or -1 if the column identifier
     * is not found.
     */
    public int getColumnIndex(String columnIdentifier)
    {
        for (int columnIndex = 0; columnIndex < columnDefinitions.size(); columnIndex++)
        {
            if (columnDefinitions.get(columnIndex).getIdentifier().equals(columnIdentifier)) return columnIndex + 1;
        }

        return -1;
    }

    public Map<String, Object> getDataRow(int rowIndex)
    {
        // If the table model is an instance of the T8Default we can use a shortcut.
        return tableModel.getDataEntity(rowIndex).getFieldValues();
    }

    public List<Map<String, Object>> getDataRows(int[] rowIndices)
    {
        List<Map<String, Object>> dataRows;

        dataRows = new ArrayList<>();
        for (int rowIndex : rowIndices)
        {
            // If the table model is an instance of the T8Default we can use a shortcut.
            dataRows.add(tableModel.getDataEntity(rowIndex).getFieldValues());
        }

        return dataRows;
    }

    public List<T8DataEntity> getDataEntities()
    {
        List<T8DataEntity> dataEntities;

        // First check if the model is a T8 Default Table model, if it is we can take a shortcut
        dataEntities = new ArrayList<>();
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            dataEntities.add(tableModel.getDataEntity(rowIndex));
        }

        return dataEntities;
    }

    public List<T8DataEntity> getDataEntities(int[] rowIndices)
    {
        List<T8DataEntity> dataEntities;

        // First check if the model is a T8 Default Table model, if it is we can take a shortcut
        dataEntities = new ArrayList<>();
        for (int row : rowIndices)
        {
           dataEntities.add(tableModel.getDataEntity(row));
        }

        return dataEntities;
    }

    /**
     * Gets the data entity at the specified row index. Row index range is set
     * at [0 - ({@code getRowCount()}-1)].
     *
     * @param row The row index
     *
     * @return The {@code T8DataEntity} at the specified row index
     */
    public T8DataEntity getDataEntity(int row)
    {
        return tableModel.getDataEntity(row);
    }

    public void setRowDataValues(Map<String, Object> dataValues, int rowIndex)
    {
        for (int columnIndex = 0; columnIndex < columnDefinitions.size(); columnIndex++)
        {
            String fieldIdentifier;

            fieldIdentifier = columnDefinitions.get(columnIndex).getFieldIdentifier();
            if (dataValues.containsKey(fieldIdentifier))
            {
                tableModel.setValueAt(dataValues.get(fieldIdentifier), rowIndex, columnIndex + 1); // Add one to the column index to take into account that the first column holds a system value.
            }
        }
    }

    public T8TableRowHeader getRowHeader(int rowIndex)
    {
        return (T8TableRowHeader)tableModel.getValueAt(rowIndex, 0);
    }

    public int getRowIndex(T8DataEntity entity)
    {
        return tableModel.getRowIndex(entity);
    }

    public Object getAdditionalRenderData(int row, String fieldId)
    {
        return tableModel.getAdditionalRenderData(row, fieldId);
    }

    public List<T8DataEntity> getTickedDataEntities()
    {
        return tableModel.getTickedDataEntities();
    }

    public boolean areAllRowsTicked()
    {
        return tableModel.areAllRowsTicked();
    }

    public void setAllRowsTicked(boolean ticked)
    {
        tableModel.setAllRowsTicked(ticked);
    }

    private void applySingleTickSelection(List<T8TableRowHeader> modifiedRowHeaders)
    {
        // We are only looking for newly ticked rows. If more than one row is
        // newly ticked, we just use the first and all the rest will be removed (unticked).
        for (T8TableRowHeader modifiedRowHeader : modifiedRowHeaders)
        {
            if (modifiedRowHeader.isTicked())
            {
                tableModel.applySingleTickSelection(modifiedRowHeader);
                table.getTableViewHandler().repaintRowHeaders();
                break;
            }
        }
    }

    private class TickListener implements T8RowTickListener
    {
        private final boolean singleTickSelection;

        private TickListener(boolean singleTickSelection)
        {
            this.singleTickSelection = singleTickSelection;
        }

        @Override
        public void tickSelectionChanged(T8RowTickSelectionChangedEvent event)
        {
            // We first make sure we only have a single selection, if that is the expected selection mode
            if (singleTickSelection) applySingleTickSelection(event.getModifiedRowHeaders());

            HashMap<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            table.getController().reportEvent(table, tableDefinition.getComponentEventDefinition(T8TableAPIHandler.EVENT_TICK_SELECTION_CHANGED), eventParameters);
        }
    }
}
