package com.pilog.t8.ui.table;

import java.util.Collections;
import java.util.EventObject;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8RowTickSelectionChangedEvent extends EventObject
{
    private final List<T8TableRowHeader> modifiedRowHeaders;

    public T8RowTickSelectionChangedEvent(List<T8TableRowHeader> source)
    {
        super(source);
        this.modifiedRowHeaders = source;
    }

    public List<T8TableRowHeader> getModifiedRowHeaders()
    {
        return Collections.unmodifiableList(modifiedRowHeaders);
    }
}
