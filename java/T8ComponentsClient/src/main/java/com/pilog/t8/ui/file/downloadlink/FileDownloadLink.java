package com.pilog.t8.ui.file.downloadlink;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import java.awt.CardLayout;
import java.io.File;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class FileDownloadLink extends JPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(FileDownloadLink.class);

    private final T8ComponentController controller;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8FileManager fileManager;
    private String localFilePath;
    private UploadCheckerThread uploadCheckerThread;
    private T8FileDetails remoteFileDetails;
    private String inactiveText;

    private static File LAST_SELECTED_FOLDER = null;

    public FileDownloadLink(T8ComponentController controller)
    {
        initComponents();
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.fileManager = clientContext.getFileManager();
        this.inactiveText = null;
    }

    public String getInactiveText()
    {
        return inactiveText;
    }

    public void setInactiveText(String noFileText)
    {
        this.inactiveText = noFileText;
    }

    public void setRemoteFileDetails(String contextIid, String filePath)
    {
        try
        {
            T8FileDetails fileDetails;

            fileDetails = fileManager.getFileDetails(context, contextIid, filePath);
            setRemoteFileDetails(fileDetails);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while fetching remote file details from context: " + contextIid + ", path: " + filePath, e);
        }
    }

    public void setRemoteFileDetails(T8FileDetails remoteFileDetails)
    {
        this.remoteFileDetails = remoteFileDetails;
        if (this.remoteFileDetails != null)
        {
            jXHyperlinkFileName.setText(remoteFileDetails.getFileName() + " (" + remoteFileDetails.getFileSize() + " bytes)");
            jXHyperlinkFileName.setEnabled(true);
            jXHyperlinkFileName.invalidate();
            showLink();
        }
        else
        {
            jXHyperlinkFileName.setText(inactiveText);
            jXHyperlinkFileName.setEnabled(false);
            jXHyperlinkFileName.invalidate();
            showLink();
        }
    }

    private void downloadFile()
    {
        // Get the target file path to which the file will be downloaded.
        try
        {
            File selectedFile;

            // Determine the preselected file in the file chooser.
            if (LAST_SELECTED_FOLDER != null) selectedFile = new File(LAST_SELECTED_FOLDER.getCanonicalFile() + "/" + remoteFileDetails.getFileName());
            else selectedFile = new File(new File(remoteFileDetails.getFileName()).getCanonicalPath());

            // Get the local file path to which the file will be downloaded.
            localFilePath = BasicFileChooser.getSaveFilePath(SwingUtilities.getWindowAncestor(this), null, "All Files", selectedFile, BasicFileChooser.FileSelectionMode.FILES);
            if (localFilePath != null)
            {
                // Update the last selected folder path.
                LAST_SELECTED_FOLDER = new File(localFilePath);
                if (LAST_SELECTED_FOLDER.getParentFile() != null) LAST_SELECTED_FOLDER = LAST_SELECTED_FOLDER.getParentFile();

                // Execute the downloader thread.
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            long fileSize;

                            fileSize = fileManager.downloadFile(context, remoteFileDetails.getContextIid(), localFilePath, remoteFileDetails.getFilePath());
                            stopDownloadChecker();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            JOptionPane.showMessageDialog(FileDownloadLink.this, "The selected file could not be downloaded successfully.  Please try again.", "Download Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }.start();

                // Show the progress of the upload.
                startDownloadChecker();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(FileDownloadLink.this, "The selected file could not be downloaded successfully.  Please try again.", "Download Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void showLink()
    {
        CardLayout layout;

        layout = (CardLayout)getLayout();
        layout.show(this, "TEXT");
    }

    private void showDownloadProgress()
    {
        CardLayout layout;

        layout = (CardLayout)getLayout();
        layout.show(this, "PROGRESS");
    }

    private void startDownloadChecker()
    {
        showDownloadProgress();
        uploadCheckerThread = new UploadCheckerThread();
        uploadCheckerThread.start();
    }

    private void stopDownloadChecker()
    {
        if (uploadCheckerThread != null)
        {
            uploadCheckerThread.stopChecker();
        }

        showLink();
    }

    private class UploadCheckerThread extends Thread
    {
        private boolean stopFlag;
        private int progress;

        public UploadCheckerThread()
        {
            stopFlag = false;
            progress = 0;
        }

        @Override
        public void run()
        {
            try
            {
                while ((progress < 100) && (!stopFlag))
                {
                    progress = fileManager.getDownloadOperationProgress(context, localFilePath);
                    SwingUtilities.invokeLater(() ->
                    {
                        jProgressBarUpload.setValue(progress);
                    });

                    Thread.sleep(500);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        public void stopChecker()
        {
            stopFlag = true;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelLink = new javax.swing.JPanel();
        jXHyperlinkFileName = new org.jdesktop.swingx.JXHyperlink();
        jPanelProgress = new javax.swing.JPanel();
        jProgressBarUpload = new javax.swing.JProgressBar();

        setPreferredSize(null);
        setLayout(new java.awt.CardLayout());

        jPanelLink.setPreferredSize(null);
        jPanelLink.setLayout(new java.awt.GridBagLayout());

        jXHyperlinkFileName.setText("File Hyperlink");
        jXHyperlinkFileName.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXHyperlinkFileNameActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelLink.add(jXHyperlinkFileName, gridBagConstraints);

        add(jPanelLink, "TEXT");

        jPanelProgress.setPreferredSize(null);
        jPanelProgress.setLayout(new java.awt.GridBagLayout());

        jProgressBarUpload.setPreferredSize(null);
        jProgressBarUpload.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelProgress.add(jProgressBarUpload, gridBagConstraints);

        add(jPanelProgress, "PROGRESS");
    }// </editor-fold>//GEN-END:initComponents

    private void jXHyperlinkFileNameActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXHyperlinkFileNameActionPerformed
    {//GEN-HEADEREND:event_jXHyperlinkFileNameActionPerformed
        downloadFile();
    }//GEN-LAST:event_jXHyperlinkFileNameActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelLink;
    private javax.swing.JPanel jPanelProgress;
    private javax.swing.JProgressBar jProgressBarUpload;
    private org.jdesktop.swingx.JXHyperlink jXHyperlinkFileName;
    // End of variables declaration//GEN-END:variables

}
