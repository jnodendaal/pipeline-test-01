package com.pilog.t8.ui.filter.panel.component.date;

import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.utilities.components.datetimepicker.JXDateTimePicker;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;
import javax.swing.JComponent;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;

/**
 * @author hennie.brink@pilog.co.za
 */
public abstract class DatePickerSingleDateComponent extends JXPanel implements DatePickerComponent
{
    private Boolean isSelected = false;
    private JXDateTimePicker datePicker;
    private T8ComponentController controller;
    private String label;

    public DatePickerSingleDateComponent(String label)
    {
        this.label = label;
        datePicker = new JXDateTimePicker(new Date());
        datePicker.setMinimumSize(new Dimension(250, 20));
        datePicker.setPreferredSize(new Dimension(250, 20));
    }

    @Override
    public Boolean isSelected()
    {
        return isSelected;
    }

    public abstract T8DataFilterCriteria getFilterCriteria(
            Date datePicker, String dataEntityFieldIdentifier);

    @Override
    public T8DataFilterCriteria getFilterCriteria(
            String dataEntityFieldIdentifier)
    {
        return getFilterCriteria(datePicker.getDate(), dataEntityFieldIdentifier);
    }

    @Override
    public void initialize(T8ComponentController controller)
    {
        this.controller = controller;
        setOpaque(false);
        setLayout(new GridBagLayout());

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        gridBagConstraints.ipadx = 0;
        gridBagConstraints.ipady = 0;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;

        gridBagConstraints.gridx = 0;
        add(new JXLabel(getTranslatedString(label)), gridBagConstraints);

        JXPanel spacer = new JXPanel();
        spacer.setOpaque(false);
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.gridx = 1;
        add(spacer,gridBagConstraints);

        gridBagConstraints.gridx = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.fill = GridBagConstraints.NONE;
        gridBagConstraints.anchor = GridBagConstraints.EAST;
        add(datePicker, gridBagConstraints);

        invalidate();
    }

    @Override
    public JComponent getComponent()
    {
        return this;
    }

    @Override
    public void setSelected(Boolean selected)
    {
        this.isSelected = selected;
    }

    private String getTranslatedString(String text)
    {
        return controller.getClientContext().getConfigurationManager().getUITranslation(controller.getContext(), text);
    }

}
