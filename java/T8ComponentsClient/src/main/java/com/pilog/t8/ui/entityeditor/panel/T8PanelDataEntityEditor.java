package com.pilog.t8.ui.entityeditor.panel;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8ComponentUtilities;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.entityeditor.panel.T8PanelDataEntityEditorAPIHandler;
import com.pilog.t8.definition.ui.entityeditor.panel.T8PanelDataEntityEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.ui.optionpane.T8OptionPane;
import com.pilog.t8.ui.panel.T8Panel;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Container;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

/**
 * @author Bouwer du Preez
 */
public class T8PanelDataEntityEditor extends T8Panel implements T8DataEntityEditorComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8PanelDataEntityEditor.class);

    private final T8PanelDataEntityEditorDefinition definition;
    private final T8PanelDataEntityEditorOperationHandler operationHandler;
    private T8PanelDataEntityEditorDataHandler dataHandler;
    private T8ConfigurationManager configurationManager;
    private T8ClientContext clientContext;
    private T8Context context;
    private T8DefaultComponentContainer container;
    private Map<String, Object> key; // The values used as filter values and which will also be inserted by default into any newly added entity.
    private Map<String, T8DataFilter> prefilters;
    private T8DataEntity editorEntity;
    private T8DataFilter userDefinedFilter;
    private LinkedHashMap<String, OrderMethod> fieldOrdering;
    private DataLoader lastDataLoader;

    public T8PanelDataEntityEditor(T8PanelDataEntityEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();

        // Initialize the UI.
        initComponents();

        // Do the rest of the UI setup.
        this.operationHandler = new T8PanelDataEntityEditorOperationHandler(this);
        this.dataHandler = new T8PanelDataEntityEditorDataHandler(definition);
        this.prefilters = new HashMap<>();
        this.container = new T8DefaultComponentContainer();
        this.fieldOrdering = new LinkedHashMap<>();
        this.add(container, java.awt.BorderLayout.CENTER);
        this.setContentContainer(container);
        container.setComponent(jPanelMain);
    }

    public String getTranslatedString(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public void setEditorDataEntity(T8DataEntity dataEntity)
    {
        editorEntity = dataEntity;
        for (T8Component childComponent : T8ComponentUtilities.getDescendentComponents(this))
        {
            if (childComponent instanceof T8DataEntityEditorComponent)
            {
                T8DataEntityEditorComponent entityEditorChild;

                entityEditorChild = (T8DataEntityEditorComponent)childComponent;
                if (definition.getIdentifier().equals(entityEditorChild.getParentEditorIdentifier()))
                {
                    entityEditorChild.setEditorDataEntity(dataEntity);
                }
            }
        }
    }

    public void setEditorDataEntityFieldValues(Map<String, Object> fieldVaues)
    {
        if (editorEntity != null)
        {
            editorEntity.setFieldValues(fieldVaues);
            setEditorDataEntity(editorEntity);
        }
    }

    @Override
    public T8DataEntity getEditorDataEntity()
    {
        return editorEntity;
    }

    @Override
    public boolean commitEditorChanges()
    {
        boolean success;

        success = true;
        for (T8Component childComponent : T8ComponentUtilities.getDescendentComponents(this))
        {
            if (childComponent instanceof T8DataEntityEditorComponent)
            {
                T8DataEntityEditorComponent entityEditorChild;

                entityEditorChild = (T8DataEntityEditorComponent)childComponent;
                if (definition.getIdentifier().equals(entityEditorChild.getParentEditorIdentifier()))
                {
                    if (!entityEditorChild.commitEditorChanges())
                    {
                        success = false;
                    }
                }
            }
        }

        return success;
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return definition.getEditorDataEntityIdentifier();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return definition.getParentEditorIdentifier();
    }

    @Override
    public T8PanelDataEntityEditorDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        // Initialize the content panel.
        super.initializeComponent(inputParameters);

        // Initialize all the available prefilters.
        for (T8DataFilterDefinition prefilterDefinition : definition.getPrefilterDefinitions())
        {
            prefilters.put(prefilterDefinition.getIdentifier(), prefilterDefinition.getNewDataFilterInstance(context, null));
        }

        // Enabled the required functionality.
        jButtonInsertNewEntity.setVisible(definition.isInsertEnabled());
        jButtonDeleteSelectedEntities.setVisible(definition.isDeleteEnabled());
        jButtonCommitChanges.setVisible(definition.isCommitEnabled());
        jButtonFilter.setVisible(definition.isUserFilterEnabled());
        jButtonRefresh.setVisible(definition.isUserRefreshEnabled());
        jButtonOptions.setVisible(definition.isOptionsEnabled());

        // Set the visibility of the tool bar.
        jToolBarMain.setVisible(definition.isToolBarVisible());
        jToolBarPageControls.setVisible(definition.isToolBarVisible());
        setToolTipText(definition.getTooltipText());
    }

    /**
     * This method is very important.  It redirects all sub-component additions/
     * removals to the JPanel inside of the component container.
     * @return The container where all sub-components are located.
     */
    @Override
    public Container getContentContainer()
    {
        return jPanelContent;
    }

    public T8DataFilter getUserDefinedFilter()
    {
        return userDefinedFilter;
    }

    public void setUserDefinedFilter(T8DataFilter filter)
    {
        this.userDefinedFilter = filter;
        this.dataHandler.resetPageOffset();
    }

    public void setPrefilter(String identifier, T8DataFilter filter)
    {
        prefilters.put(identifier, filter);
        this.dataHandler.resetPageOffset();
    }

    public T8DataFilter getPrefilter(String identifier)
    {
        return prefilters.get(identifier);
    }

    public Map<String, Object> getKey()
    {
        return key;
    }

    public void setKey(Map<String, Object> fieldValues)
    {
        key = fieldValues;
    }

    /**
     * Creates and returns a data filter that includes all of the pre-filters set
     * on the table along with the user-set filter criteria and ordering.
     *
     * @return The combined filter to use when retrieving data to display in the
     * table.
     */
    public T8DataFilter getCombinedFilter()
    {
        T8DataFilterCriteria combinedFilterCriteria;
        T8DataFilter combinedFilter;

        // Create a filter to contain a combination of all applicable filters for data retrieval.
        combinedFilterCriteria = new T8DataFilterCriteria();

        // Add the key filter values if necessary.
        if ((key != null) && (key.size() > 0))
        {
            combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, new T8DataFilterCriteria(key));
        }

        // Add all available and valid prefilters.
        for (T8DataFilter prefilter : prefilters.values())
        {
            // Only include filters that have valid filter criteria.
            if ((prefilter != null) && (prefilter.hasFilterCriteria()))
            {
                combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
            }
        }

        // If field ordering is null or empty check if one of the pre-filters have an ordering.
        if (fieldOrdering == null || fieldOrdering.isEmpty())
        {
            for (T8DataFilter prefilter : prefilters.values())
            {
                // Only include filters that have valid filter criteria.
                if ((prefilter != null) && (prefilter.hasFieldOrdering()))
                {
                    fieldOrdering = prefilter.getFieldOrdering();
                }
            }
        }

        // Add the regular user-defined filter if applicable.
        if ((userDefinedFilter != null) && (userDefinedFilter.hasFilterCriteria())) combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, userDefinedFilter.getFilterCriteria());

        // Create the combined filter from the filter criteria.
        combinedFilter = new T8DataFilter(definition.getEditorDataEntityIdentifier(), combinedFilterCriteria);

        // Add the user-defined ordering if applicable.
        combinedFilter.setFieldOrdering(fieldOrdering);

        // Return a new filter created from the combined criteria.
        return combinedFilter;
    }

    public void saveChanges()
    {
        T8DataEntity entityToSave;

        // First commit all changes on the editor.
        commitEditorChanges();

        // Get the updated entity.
        entityToSave = getEditorDataEntity();
        if (entityToSave != null)
        {
            DataSaver saver;

            // Create a new loader.
            saver = new DataSaver(entityToSave);

            // Run the loader in a new Thread.
            new Thread(saver).start();

            try
            {
                // Wait a while and if the loader still has not completed, display the processing panel.
                Thread.sleep(100);
                if (!saver.hasCompleted())
                {
                    container.setMessage(getTranslatedString("Saving Data..."));
                    container.lock();
                }
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }

            //Notify clients of the save event
            HashMap<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            eventParameters.put(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY, entityToSave);
            controller.reportEvent(this, definition.getComponentEventDefinition(T8PanelDataEntityEditorAPIHandler.EVENT_DATA_ENTITY_SAVED), eventParameters);
        }
    }

    public void refreshData()
    {
        if(SwingUtilities.isEventDispatchThread()) throw new RuntimeException("This method must not be called from EDT");
        if(lastDataLoader != null) lastDataLoader.cancel(false);

        // Create a new loader.
        lastDataLoader = new DataLoader();

        // Run the loader in a new Thread.
        lastDataLoader.execute();

        try
        {
            // Wait a while and if the loader still has not completed, display the processing panel.
            Thread.sleep(100);
            if (!lastDataLoader.isDone())
            {
                container.setMessage(getTranslatedString("Loading Data..."));
                container.lock();
            }

            refreshEntityCount();
        }
        catch (Exception e)
        {
            LOGGER.log(e);
        }
    }

    public void refreshEntityCount()
    {
        DataCounter counter;

        // Hide the existing record count until it has been updated.
        jLabelRecordCount.setVisible(true);

        // Create a new loader.
        counter = new DataCounter();

        // Run the loader in a new Thread.
        new Thread(counter).start();
    }

    public void previousEntity()
    {
        dataHandler.decrementPage();
        new Thread()
        {

            @Override
            public void run()
            {
                refreshData();
            }

        }.start();
    }

    public void nextEntity()
    {
        dataHandler.incrementPage();
        new Thread()
        {

            @Override
            public void run()
            {
                refreshData();
            }

        }.start();
    }

    public void deleteEntity()
    {

    }

    public void insertEntity()
    {

    }

    public void showUserDefinedFilterView()
    {

    }

    private void retrieveSpecificEntity()
    {
        String row;

        row = T8OptionPane.showInputDialog(this, getTranslatedString("Go to Row"), getTranslatedString("Go to Row"), T8OptionPane.PLAIN_MESSAGE, controller);
        if(Strings.isInteger(row))
        {
            dataHandler.goToPage(dataHandler.findRowPage(Integer.parseInt(row)));
            new Thread()
            {

                @Override
                public void run()
                {
                    refreshData();
                }

            }.start();
        }
    }

    private class DataLoader extends SwingWorker<T8DataEntity, Void>
    {
        @Override
        protected T8DataEntity doInBackground() throws Exception
        {
            try
            {
                // Use the combined filter to retrieve the filtered data and to construct a table model.
                return dataHandler.loadDataEntity(controller.getContext(), getCombinedFilter());
            }
            catch (Exception e)
            {
                LOGGER.log(e);
                return null;
            }
        }

        @Override
        protected void done()
        {
            if(!isCancelled())
            {
                T8DataEntity dataEntity;
                try
                {
                    dataEntity = get();
                    // Set the page number label.
                    jLabelPageStartOffset.setText("" + dataHandler.getPageOffset());
                    jLabelPageEndOffset.setText("" + (dataHandler.getPageOffset() + dataHandler.getPageSize() -1));

                    // Set the new entity on the editor.
                    setEditorDataEntity(dataEntity);

                    // Show the loaded data.
                    container.unlock();
                }
                catch(Exception ex)
                {
                    LOGGER.log(ex);
                }
            }
        }
    }

    private class DataSaver implements Runnable
    {
        private final T8DataEntity entityToSave;
        private boolean completed = false;

        private DataSaver(T8DataEntity entityToSave)
        {
            this.entityToSave = entityToSave;
        }

        public boolean hasCompleted()
        {
            return completed;
        }

        @Override
        public void run()
        {
            try
            {
                dataHandler.saveDataEntity(controller.getContext(), entityToSave);
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }

            // Set the completed flag.
            completed = true;

            // Show the loaded data.
            container.unlock();
        }
    }

    private class DataCounter implements Runnable
    {
        private boolean completed = false;

        public boolean hasCompleted()
        {
            return completed;
        }

        @Override
        public void run()
        {
            try
            {
                final int recordCount;

                // Use the combined filter to count the available entities.
                recordCount = dataHandler.countDataEntities(controller.getContext(), getCombinedFilter());

                // Update the UI to reflect the record count.
                SwingUtilities.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        jLabelRecordCount.setText("(" + recordCount + ")");
                        jLabelRecordCount.setVisible(true);
                    }
                });
            }
            catch (Exception e)
            {
                LOGGER.log(e);
                SwingUtilities.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        jLabelRecordCount.setVisible(false);
                    }
                });
            }

            // Set the completed flag.
            completed = true;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonCommitChanges = new javax.swing.JButton();
        jButtonRefresh = new javax.swing.JButton();
        jButtonFilter = new javax.swing.JButton();
        jButtonInsertNewEntity = new javax.swing.JButton();
        jButtonDeleteSelectedEntities = new javax.swing.JButton();
        jButtonOptions = new javax.swing.JButton();
        jToolBarPageControls = new javax.swing.JToolBar();
        jButtonPreviousPage = new javax.swing.JButton();
        jLabelPageRow = new javax.swing.JLabel();
        jLabelPageStartOffset = new javax.swing.JLabel();
        jLabelPageRangeDash = new javax.swing.JLabel();
        jLabelPageEndOffset = new javax.swing.JLabel();
        jButtonNextPage = new javax.swing.JButton();
        jButtonGoToRow = new javax.swing.JButton();
        jLabelRecordCount = new javax.swing.JLabel();
        jPanelContent = new javax.swing.JPanel();

        jPanelMain.setOpaque(false);
        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);
        jToolBarMain.setOpaque(false);

        jButtonCommitChanges.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/commitIcon.png"))); // NOI18N
        jButtonCommitChanges.setText(configurationManager.getUITranslation(context, "Commit"));
        jButtonCommitChanges.setToolTipText("Commit Changes");
        jButtonCommitChanges.setFocusable(false);
        jButtonCommitChanges.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonCommitChanges.setOpaque(false);
        jButtonCommitChanges.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCommitChanges.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCommitChangesActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCommitChanges);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setText(configurationManager.getUITranslation(context, "Refresh"));
        jButtonRefresh.setToolTipText("Refresh Data");
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setOpaque(false);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        jButtonFilter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/filterIcon.png"))); // NOI18N
        jButtonFilter.setText(configurationManager.getUITranslation(context, "Filter"));
        jButtonFilter.setFocusable(false);
        jButtonFilter.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonFilter.setOpaque(false);
        jButtonFilter.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonFilter.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonFilterActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonFilter);

        jButtonInsertNewEntity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/tableRowInsertIcon.png"))); // NOI18N
        jButtonInsertNewEntity.setText(configurationManager.getUITranslation(context, "Insert"));
        jButtonInsertNewEntity.setToolTipText("Insert New Row");
        jButtonInsertNewEntity.setFocusable(false);
        jButtonInsertNewEntity.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonInsertNewEntity.setOpaque(false);
        jButtonInsertNewEntity.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonInsertNewEntity.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonInsertNewEntityActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonInsertNewEntity);

        jButtonDeleteSelectedEntities.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/tableRowDeleteIcon.png"))); // NOI18N
        jButtonDeleteSelectedEntities.setText(configurationManager.getUITranslation(context, "Delete"));
        jButtonDeleteSelectedEntities.setToolTipText("Delete Selected Rows");
        jButtonDeleteSelectedEntities.setFocusable(false);
        jButtonDeleteSelectedEntities.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonDeleteSelectedEntities.setOpaque(false);
        jButtonDeleteSelectedEntities.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDeleteSelectedEntities.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDeleteSelectedEntitiesActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDeleteSelectedEntities);

        jButtonOptions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/gearIcon.png"))); // NOI18N
        jButtonOptions.setText(configurationManager.getUITranslation(context, "Options"));
        jButtonOptions.setToolTipText("Show the table options menu");
        jButtonOptions.setFocusable(false);
        jButtonOptions.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonOptions.setOpaque(false);
        jButtonOptions.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBarMain.add(jButtonOptions);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelMain.add(jToolBarMain, gridBagConstraints);

        jToolBarPageControls.setFloatable(false);
        jToolBarPageControls.setRollover(true);
        jToolBarPageControls.setOpaque(false);

        jButtonPreviousPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pagePreviousIcon.png"))); // NOI18N
        jButtonPreviousPage.setToolTipText(getTranslatedString("Previous Page"));
        jButtonPreviousPage.setFocusable(false);
        jButtonPreviousPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonPreviousPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonPreviousPage.setOpaque(false);
        jButtonPreviousPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPreviousPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPreviousPageActionPerformed(evt);
            }
        });
        jToolBarPageControls.add(jButtonPreviousPage);

        jLabelPageRow.setText(getTranslatedString("Row: "));
        jToolBarPageControls.add(jLabelPageRow);
        jToolBarPageControls.add(jLabelPageStartOffset);

        jLabelPageRangeDash.setText("-");
        jToolBarPageControls.add(jLabelPageRangeDash);
        jToolBarPageControls.add(jLabelPageEndOffset);

        jButtonNextPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pageNextIcon.png"))); // NOI18N
        jButtonNextPage.setToolTipText(getTranslatedString("Next Page"));
        jButtonNextPage.setFocusable(false);
        jButtonNextPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonNextPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonNextPage.setOpaque(false);
        jButtonNextPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonNextPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonNextPageActionPerformed(evt);
            }
        });
        jToolBarPageControls.add(jButtonNextPage);

        jButtonGoToRow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/control-cursor.png"))); // NOI18N
        jButtonGoToRow.setFocusable(false);
        jButtonGoToRow.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonGoToRow.setOpaque(false);
        jButtonGoToRow.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonGoToRowActionPerformed(evt);
            }
        });
        jToolBarPageControls.add(jButtonGoToRow);

        jLabelRecordCount.setText("(0)");
        jToolBarPageControls.add(jLabelRecordCount);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelMain.add(jToolBarPageControls, gridBagConstraints);

        jPanelContent.setOpaque(false);
        jPanelContent.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelMain.add(jPanelContent, gridBagConstraints);

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCommitChangesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCommitChangesActionPerformed
    {//GEN-HEADEREND:event_jButtonCommitChangesActionPerformed
        saveChanges();
    }//GEN-LAST:event_jButtonCommitChangesActionPerformed

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        new Thread()
        {

            @Override
            public void run()
            {
                refreshData();
            }

        }.start();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jButtonFilterActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonFilterActionPerformed
    {//GEN-HEADEREND:event_jButtonFilterActionPerformed
        showUserDefinedFilterView();
    }//GEN-LAST:event_jButtonFilterActionPerformed

    private void jButtonInsertNewEntityActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonInsertNewEntityActionPerformed
    {//GEN-HEADEREND:event_jButtonInsertNewEntityActionPerformed
        insertEntity();
    }//GEN-LAST:event_jButtonInsertNewEntityActionPerformed

    private void jButtonDeleteSelectedEntitiesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteSelectedEntitiesActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteSelectedEntitiesActionPerformed
        deleteEntity();
    }//GEN-LAST:event_jButtonDeleteSelectedEntitiesActionPerformed

    private void jButtonPreviousPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPreviousPageActionPerformed
    {//GEN-HEADEREND:event_jButtonPreviousPageActionPerformed
        previousEntity();
    }//GEN-LAST:event_jButtonPreviousPageActionPerformed

    private void jButtonNextPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonNextPageActionPerformed
    {//GEN-HEADEREND:event_jButtonNextPageActionPerformed
        nextEntity();
    }//GEN-LAST:event_jButtonNextPageActionPerformed

    private void jButtonGoToRowActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonGoToRowActionPerformed
    {//GEN-HEADEREND:event_jButtonGoToRowActionPerformed
        retrieveSpecificEntity();
    }//GEN-LAST:event_jButtonGoToRowActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCommitChanges;
    private javax.swing.JButton jButtonDeleteSelectedEntities;
    private javax.swing.JButton jButtonFilter;
    private javax.swing.JButton jButtonGoToRow;
    private javax.swing.JButton jButtonInsertNewEntity;
    private javax.swing.JButton jButtonNextPage;
    private javax.swing.JButton jButtonOptions;
    private javax.swing.JButton jButtonPreviousPage;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JLabel jLabelPageEndOffset;
    private javax.swing.JLabel jLabelPageRangeDash;
    private javax.swing.JLabel jLabelPageRow;
    private javax.swing.JLabel jLabelPageStartOffset;
    private javax.swing.JLabel jLabelRecordCount;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JToolBar jToolBarMain;
    private javax.swing.JToolBar jToolBarPageControls;
    // End of variables declaration//GEN-END:variables
}
