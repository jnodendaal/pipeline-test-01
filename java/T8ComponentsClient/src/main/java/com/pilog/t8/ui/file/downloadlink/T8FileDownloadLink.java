package com.pilog.t8.ui.file.downloadlink;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.file.downloadlink.T8FileDownloadLinkDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FileDownloadLink extends FileDownloadLink implements T8Component
{
    private final T8FileDownloadLinkOperationHandler operationHandler;
    private final T8ComponentController controller;
    private final T8FileDownloadLinkDefinition definition;

    public T8FileDownloadLink(T8FileDownloadLinkDefinition definition, T8ComponentController controller)
    {
        super(controller);
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8FileDownloadLinkOperationHandler(this);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> hm1)
    {
        setToolTipText(definition.getTooltipText());
        setInactiveText(definition.getInactiveText());
    }

    @Override
    public void startComponent()
    {
        setRemoteFileDetails(null);
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }
}
