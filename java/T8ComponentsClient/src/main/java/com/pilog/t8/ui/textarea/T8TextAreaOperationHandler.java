package com.pilog.t8.ui.textarea;

import com.pilog.t8.definition.ui.textarea.T8TextAreaAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextAreaOperationHandler extends T8ComponentOperationHandler
{
    private static final String TEXT_AREA_NEW_LINE = "\n";

    private final T8TextArea textArea;

    public T8TextAreaOperationHandler(T8TextArea textArea)
    {
        super(textArea);
        this.textArea = textArea;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        switch (operationIdentifier)
        {
            case T8TextAreaAPIHandler.OPERATION_GET_TEXT:
                return HashMaps.createSingular(T8TextAreaAPIHandler.PARAMETER_TEXT, textArea.getText());
            case T8TextAreaAPIHandler.OPERATION_SET_TEXT:
                textArea.setText((String)operationParameters.get(T8TextAreaAPIHandler.PARAMETER_TEXT));
                return null;
            case T8TextAreaAPIHandler.OPERATION_SET_EDITABLE:
                textArea.setEditable((Boolean)operationParameters.get(T8TextAreaAPIHandler.PARAMETER_EDITABLE));
                return null;
            case T8TextAreaAPIHandler.OPERATION_APPEND_TEXT:
                this.textArea.append((String)operationParameters.get(T8TextAreaAPIHandler.PARAMETER_TEXT));
                return null;
            case T8TextAreaAPIHandler.OPERATION_APPEND_LINE:
                this.textArea.append(TEXT_AREA_NEW_LINE);
                this.textArea.append((String)operationParameters.get(T8TextAreaAPIHandler.PARAMETER_TEXT));
                return null;
            default:
                return super.executeOperation(operationIdentifier, operationParameters);
        }
    }
}
