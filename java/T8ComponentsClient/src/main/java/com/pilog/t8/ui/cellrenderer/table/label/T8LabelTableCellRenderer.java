package com.pilog.t8.ui.cellrenderer.table.label;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellRenderableComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent.RenderParameter;
import com.pilog.t8.definition.gfx.font.T8FontDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8ConditionalCellFormattingDefinition;
import com.pilog.t8.definition.ui.cellrenderer.table.label.T8LabelTableCellRendererDefinition;
import com.pilog.t8.definition.ui.cellrenderer.table.label.T8LabelTableCellRendererFormatDefinition;
import java.awt.Color;
import java.awt.Font;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.border.Border;
import org.jdesktop.swingx.JXLabel;

/**
 * @author Bouwer du Preez
 */
public class T8LabelTableCellRenderer extends JXLabel implements T8TableCellRendererComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8LabelTableCellRenderer.class);

    private final T8LabelTableCellRendererDefinition definition;
    private final T8ComponentController controller;
    private final T8SessionContext sessionContext;
    private final ExpressionEvaluator formattingExpressionEvaluator;
    private final T8LabelTableCellRendererFormatDefinition formatterDefinition;
    private final Format formatter;

    private ExpressionEvaluator valueExpressionEvaluator;

    public T8LabelTableCellRenderer(T8LabelTableCellRendererDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.sessionContext = controller.getSessionContext();
        this.formattingExpressionEvaluator = new ExpressionEvaluator();
        this.formatterDefinition = definition.getFormatter();
        this.formatter = (this.formatterDefinition != null ? this.formatterDefinition.getFormat() : null);
        this.setOpaque(true);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String string, Map<String, Object> map)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> map)
    {
        String valueExpression;

        valueExpression = definition.getDisplayValueExpression();
        if (valueExpression != null)
        {
            try
            {
                valueExpressionEvaluator = new ExpressionEvaluator();
                valueExpressionEvaluator.compileExpression(valueExpression);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while compiling display value expression in renderer: " + definition, e);
                valueExpressionEvaluator = null;
            }
        }
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public void setRendererData(T8TableCellRenderableComponent renderableComponent, Map<String, Object> dataRow, String rendererFieldIdentifier, Map<RenderParameter, Object> renderParameters)
    {
        HashMap<String, Object> expressionParameters;
        Color foregroundColor;
        Color backgroundColor;
        Border cellBorder;
        Font cellFont;
        Icon icon;
        boolean isSelected;
        boolean hasFocus;
        Object value;

        icon = null;
        // Get the display value of the cell.
        if (valueExpressionEvaluator != null)
        {
            try
            {
                value = valueExpressionEvaluator.evaluateExpression(dataRow, null);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while evaluating display value expression in renderer: " + definition, e);
                value = "Error";
            }
        }
        else
        {
            value = dataRow.get(rendererFieldIdentifier);
        }

        if (value != null && formatter != null)
        {
            try
            {
                value = formatter.format(value);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while formatting the display value in renderer: " + definition, e);
                value = "Error";
            }
        }

        // Get the selected and focused parameters.
        isSelected = (renderParameters.get(RenderParameter.SELECTED) != null ? (Boolean)renderParameters.get(RenderParameter.SELECTED) : false);
        hasFocus = (renderParameters.get(RenderParameter.FOCUSED) != null ? (Boolean)renderParameters.get(RenderParameter.FOCUSED) : false);

        // Get the default formatting values.
        cellFont = renderableComponent.getCellFont();
        if (isSelected)
        {
            foregroundColor = renderableComponent.getSelectedCellForegroundColor();
            backgroundColor = renderableComponent.getSelectedCellBackgroundColor();
            cellBorder = hasFocus ? renderableComponent.getFocusedSelectedBorder() : renderableComponent.getSelectedBorder();
        }
        else
        {
            foregroundColor = renderableComponent.getUnselectedCellForegroundColor();
            backgroundColor = renderableComponent.getUnselectedCellBackgroundColor();
            cellBorder = hasFocus ? renderableComponent.getFocusedBorder() : renderableComponent.getUnselectedBorder();
        }

        // Create a collection of parameters as input to the conditional formatting expressions.
        expressionParameters = new HashMap<>();
        if (dataRow != null) expressionParameters.putAll(dataRow);
        for (RenderParameter parameter : renderParameters.keySet())
        {
            expressionParameters.put(parameter.toString(), renderParameters.get(parameter));
        }

        // Evaluate each of the conditional formatting expressions and apply the first one for which the result is true.
        for (T8ConditionalCellFormattingDefinition formattingDefinition : definition.getConditionalFormattingDefinitions())
        {
            try
            {
                String conditionExpression;

                conditionExpression = formattingDefinition.getConditionExpression();
                if ((conditionExpression == null) || (formattingExpressionEvaluator.evaluateBooleanExpression(conditionExpression, expressionParameters, null)))
                {
                    Color bgColor;
                    Color bgsColor;
                    T8FontDefinition fontDefinition;

                    bgColor = formattingDefinition.getBackgroundColor();
                    bgsColor = formattingDefinition.getSelectedBackgroundColor();
                    fontDefinition = formattingDefinition.getFontDefinition();

                    if (isSelected)
                    {
                        if (bgsColor != null) backgroundColor = bgsColor;
                    }
                    else if (bgColor != null)
                    {
                        backgroundColor = bgColor;
                    }

                    // Set the cell font if the conditional formatting overrides it.
                    if (fontDefinition != null)
                    {
                        cellFont = fontDefinition.getNewFontInstance();
                    }

                    T8IconDefinition iconDefinition;

                    iconDefinition = formattingDefinition.getIconDefinition();
                    if (iconDefinition != null)
                    {
                        icon = iconDefinition.getImage().getImageIcon();
                    }
                    break;
                }
            }
            catch (EPICSyntaxException | EPICRuntimeException e)
            {
                LOGGER.log("Exception while evaluating condition expression for formatting definition: " + formattingDefinition, e);
            }
        }

        // Set the formatting.
        setFont(cellFont);
        setForeground(foregroundColor);
        setBackground(backgroundColor);
        setBorder(cellBorder);
        setIcon(icon);

        // Set the value of the cell on the label.
        setText(value == null ? "" : (value.toString()));
    }

    @Override
    public Object getAdditionalRenderData(Map<String, Object> dataRow, String fieldIdentifier)
    {
        return null;
    }
}
