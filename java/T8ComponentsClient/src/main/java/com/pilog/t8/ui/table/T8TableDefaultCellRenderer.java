package com.pilog.t8.ui.table;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8TableDefaultCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer
{
    private final T8Table tableComponent;

    public T8TableDefaultCellRenderer(T8Table tableComponent)
    {
        this.tableComponent = tableComponent;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        int modelColumnIndex;
        Color backgroundColor;
        JTable.DropLocation dropLocation;

        dropLocation = table.getDropLocation();
        modelColumnIndex = tableComponent.getTableViewHandler().convertViewToModelIndex(column);
        if ((dropLocation) != null && (!dropLocation.isInsertRow()) && (!dropLocation.isInsertColumn()) && (dropLocation.getRow() == row) && (dropLocation.getColumn() == column))
        {
            isSelected = true;
        }

        backgroundColor = table.getBackground();
        setForeground(table.getForeground());
        setBackground(table.getModel().isCellEditable(row, modelColumnIndex) ? backgroundColor : new Color(backgroundColor.getRed() - 1, backgroundColor.getGreen() - 1, backgroundColor.getBlue() - 1));
        setFont(table.getFont());

        if (hasFocus)
        {
            if (isSelected)
            {
                setBorder(T8Table.FOCUS_SELECTED_BORDER);
            }
            else
            {
                setBorder(T8Table.FOCUS_BORDER);
            }
        }
        else if (isSelected)
        {
            setBorder(T8Table.SELECTED_BORDER);
        }
        else
        {
            setBorder(T8Table.UNSELECTED_BORDER);
        }

        setValue(value == null ? "" : value.toString());
        return this;
    }
}
