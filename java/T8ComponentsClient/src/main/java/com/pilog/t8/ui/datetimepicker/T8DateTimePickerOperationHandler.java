package com.pilog.t8.ui.datetimepicker;

import static com.pilog.t8.definition.ui.datetimepicker.T8DateTimePickerAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DateTimePickerOperationHandler extends T8ComponentOperationHandler
{
    private final T8DateTimePicker dateTimePicker;

    public T8DateTimePickerOperationHandler(T8DateTimePicker dateTimePicker)
    {
        super(dateTimePicker);
        this.dateTimePicker = dateTimePicker;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        switch (operationIdentifier)
        {
            case OPERATION_GET_VALUE:
                return HashMaps.createSingular(PARAMETER_VALUE, dateTimePicker.getValue());
            case OPERATION_SET_VALUE:
                dateTimePicker.setValue((Long)operationParameters.get(PARAMETER_VALUE));
                return null;
            case OPERATION_SET_EDITABLE:
                dateTimePicker.setEditable((Boolean)operationParameters.get(PARAMETER_EDITABLE));
                return null;
            default:
                return super.executeOperation(operationIdentifier, operationParameters);
        }
    }
}
