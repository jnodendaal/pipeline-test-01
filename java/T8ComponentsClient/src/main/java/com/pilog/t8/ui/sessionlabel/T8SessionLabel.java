package com.pilog.t8.ui.sessionlabel;

import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.sessionlabel.T8SessionLabelDefinition;
import com.pilog.t8.ui.label.T8Label;

/**
 * @author Bouwer du Preez
 */
public class T8SessionLabel extends T8Label implements T8Component
{
    public T8SessionLabel(T8SessionLabelDefinition labelDefinition, T8ComponentController controller)
    {
        super(labelDefinition, controller);
    }
}
