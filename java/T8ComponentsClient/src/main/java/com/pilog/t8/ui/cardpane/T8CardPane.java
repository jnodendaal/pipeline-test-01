package com.pilog.t8.ui.cardpane;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.cardpane.T8CardPaneAPIHandler;
import com.pilog.t8.definition.ui.cardpane.T8CardPaneDefinition;
import java.awt.CardLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public class T8CardPane extends JPanel implements T8Component
{
    private final T8CardPaneDefinition definition;
    private final T8ComponentController controller;
    private final T8CardPaneOperationHandler operationHandler;
    private final LinkedHashMap<String, T8Component> childComponents;
    private final CardLayout cardLayout;
    private String shownComponentIdentifier;

    public T8CardPane(T8CardPaneDefinition definition, T8ComponentController controller)
    {
        this.cardLayout = new CardLayout();
        this.controller = controller;
        this.setOpaque(false);
        this.setLayout(cardLayout);
        this.definition = definition;
        this.operationHandler = new T8CardPaneOperationHandler(this);
        this.childComponents = new LinkedHashMap<>();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
        showComponent(definition.getInitialComponentIdentifier());
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        String componentIdentifier;

        componentIdentifier = component.getComponentDefinition().getIdentifier();
        childComponents.put(componentIdentifier, component);
        add((Component) component, componentIdentifier);
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        return childComponents.get(identifier);
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        T8Component childComponent;

        childComponent = childComponents.get(identifier);
        if (childComponent != null)
        {
            this.remove((Component) childComponent);
            return childComponent;
        }
        else
        {
            return null;
        }
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(childComponents.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
        childComponents.clear();
    }

    /**
     * Returns the identifier for the currently displayed component. In other
     * words the top card in the layout.
     *
     * @return The {@code String} identifier for the shown component
     */
    String getShownComponent()
    {
        return this.shownComponentIdentifier;
    }

    public void showComponent(String componentIdentifier)
    {
        cardLayout.show(this, componentIdentifier);
        this.shownComponentIdentifier = componentIdentifier;
        triggerComponentSwitchedEvent(componentIdentifier);
    }

    private void triggerComponentSwitchedEvent(String componentIdentifier)
    {
        HashMap<String, Object> eventParameters;

        eventParameters = new HashMap<>();
        eventParameters.put(T8CardPaneAPIHandler.PARAMETER_COMPONENT_IDENTIFIER, componentIdentifier);
        controller.reportEvent(T8CardPane.this, definition.getComponentEventDefinition(T8CardPaneAPIHandler.EVENT_COMPONENT_SWITCHED), eventParameters);
    }
}
