package com.pilog.t8.ui.scrollpane;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.scrollpane.T8ScrollPaneDefinition;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JScrollPane;

/**
 * @author Bouwer du Preez
 */
public class T8ScrollPane extends JScrollPane implements T8Component
{
    private T8ScrollPaneDefinition definition;
    private T8ComponentController controller;
    private T8Component childComponent;

    public T8ScrollPane(T8ScrollPaneDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.childComponent = null;
        this.setOpaque(definition.isOpaque());
        this.getViewport().setOpaque(definition.isOpaque());
        this.setBorder(null);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        childComponent = component;
        setViewportView((Component)childComponent);
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        return childComponent;
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        if (childComponent != null)
        {
            if (childComponent.getComponentDefinition().getIdentifier().equals(identifier))
            {
                T8Component removedComponent;

                removedComponent = childComponent;
                setViewportView(null);
                childComponent = null;
                return removedComponent;
            }
            else return null;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        ArrayList<T8Component> childComponents;

        childComponents = new ArrayList<>();
        if (childComponent != null) childComponents.add(childComponent);
        return childComponents;
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponent != null ? 1 : 0;
    }

    @Override
    public void clearChildComponents()
    {
        setViewportView(null);
        childComponent = null;
    }
}
