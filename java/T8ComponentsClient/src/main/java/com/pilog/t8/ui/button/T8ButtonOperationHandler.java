package com.pilog.t8.ui.button;

import com.pilog.t8.definition.ui.button.T8ButtonAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ButtonOperationHandler extends T8ComponentOperationHandler
{
    private T8Button button;

    public T8ButtonOperationHandler(T8Button button)
    {
        super(button);
        this.button = button;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8ButtonAPIHandler.OPERATION_ENABLE_BUTTON))
        {
            button.setEnabled(true);
            return null;
        }
        else if(operationIdentifier.equals(T8ButtonAPIHandler.OPERATION_DISABLE_BUTTON))
        {
            button.setEnabled(false);
            return null;
        }
        else if(operationIdentifier.equals(T8ButtonAPIHandler.OPERATION_SET_TEXT))
        {
            button.setText((String)operationParameters.get(T8ButtonAPIHandler.PARAMETER_TEXT));
            return null;
        }
        else if(operationIdentifier.equals(T8ButtonAPIHandler.OPERATION_SET_ICON))
        {
            button.changeIcon((String)operationParameters.get(T8ButtonAPIHandler.PARAMETER_ICON_IDENTIFIER));
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
