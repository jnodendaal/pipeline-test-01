package com.pilog.t8.ui.dataentitylist;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.dataentitylist.T8DataEntityListDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;

import static com.pilog.t8.definition.data.T8DataManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityListDataHandler
{
    private final T8DataEntityListDefinition listDefinition;

    public T8DataEntityListDataHandler(T8DataEntityListDefinition comboBoxDefinition)
    {
        this.listDefinition = comboBoxDefinition;
    }

    public List<T8DataEntity> loadEntityData(T8Context context, T8DataFilter dataFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, listDefinition.getDataEntityIdentifier());
        operationParameters.put(PARAMETER_DATA_FILTER, dataFilter);
        operationParameters.put(PARAMETER_PAGE_OFFSET, 0);
        operationParameters.put(PARAMETER_PAGE_SIZE, 5000);

        return (List<T8DataEntity>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_SELECT_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_LIST);
    }
}
