package com.pilog.t8.ui.spinner;

import static com.pilog.t8.definition.ui.spinner.T8NumericSpinnerAPIHandler.*;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.spinner.T8NumericSpinnerDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author Hennie Brink
 */
public class T8NumericSpinner extends JSpinner implements T8Component
{
    private final T8ComponentController controller;
    private final T8NumericSpinnerDefinition definition;
    private final T8NumericSpinnerOperationHandler operationHandler;

    public T8NumericSpinner(T8ComponentController controller, T8NumericSpinnerDefinition definition)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8NumericSpinnerOperationHandler(this);
        this.addChangeListener(new NumericSpinnerChangeListener());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        SpinnerModel model;
        Number minimumValue;
        Number maximumValue;
        Number stepValue;

        // Get the spinner configuration from the definition.
        setToolTipText(definition.getTooltipText());
        minimumValue = determineValueType(definition.getMinimumValue());
        maximumValue = determineValueType(definition.getMaximumValue());
        stepValue = determineValueType(definition.getStepValue());

        // Set the spinner model.
        model = new SpinnerNumberModel(minimumValue, (Comparable)minimumValue, (Comparable)maximumValue, stepValue);
        setModel(model);

        // Add a few listeners.
        JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor)getEditor();
        editor.getTextField().addFocusListener(new NumericSpinnerFocusListener());
    }

    private Number determineValueType(String valueString)
    {
        if (Strings.isInteger(valueString)) return Integer.parseInt(valueString);
        else return Double.parseDouble(valueString);
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void setMaxValue(Comparable maxValue)
    {
        ((SpinnerNumberModel)getModel()).setMaximum(maxValue);
        if (((Comparable)getValue()).compareTo(maxValue) > 0)
        {
            setValue(maxValue);
        }
    }

    public void setMinValue(Comparable minValue)
    {
        ((SpinnerNumberModel)getModel()).setMinimum(minValue);
        if (((Comparable)getValue()).compareTo(minValue) < 0)
        {
            setValue(minValue);
        }
    }

    private void reportEvent(String eventIdentifier)
    {
        Map<String, Object> parameters;

        parameters = new HashMap<>();
        parameters.put(PARAMETER_VALUE, getValue());
        controller.reportEvent(T8NumericSpinner.this, definition.getComponentEventDefinition(eventIdentifier), parameters);
    }

    private class NumericSpinnerChangeListener implements ChangeListener
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            reportEvent(EVENT_VALUE_CHANGED);
        }
    }

    private class NumericSpinnerFocusListener implements FocusListener
    {
        @Override
        public void focusGained(FocusEvent e)
        {
            reportEvent(EVENT_FOCUS_GAINED);
        }

        @Override
        public void focusLost(FocusEvent e)
        {
            reportEvent(EVENT_FOCUS_LOST);
        }
    }
}
