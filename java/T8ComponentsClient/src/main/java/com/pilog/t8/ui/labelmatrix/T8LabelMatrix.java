package com.pilog.t8.ui.labelmatrix;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.structure.matrix.T8HashedSparseMatrix;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.labelmatrix.T8LabelMatrixColumnDefinition;
import com.pilog.t8.definition.ui.labelmatrix.T8LabelMatrixDefinition;
import com.pilog.t8.definition.ui.labelmatrix.T8LabelMatrixFormattingDefinition.HorizontalTextAlignment;
import com.pilog.t8.definition.ui.labelmatrix.T8LabelMatrixRowDefinition;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * @author Bouwer du Preez
 */
public class T8LabelMatrix extends JPanel implements T8Component
{
    private T8LabelMatrixDefinition definition;
    private T8ComponentController controller;
    private T8LabelMatrixOperationHandler operationHandler;
    private T8HashedSparseMatrix<JLabel> labelMatrix;
    private int halfVerticalSpacing;
    private int halfHorizontalSpacing;

    public T8LabelMatrix(T8LabelMatrixDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.setLayout(new GridBagLayout());
        this.setOpaque(false);
        this.operationHandler = new T8LabelMatrixOperationHandler(this);
        this.labelMatrix = new T8HashedSparseMatrix<>();
        this.halfVerticalSpacing  = Math.round((float)definition.getVerticalSpacing() / 2.0f);
        this.halfHorizontalSpacing  = Math.round((float)definition.getHorizontalSpacing() / 2.0f);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void clear()
    {
        labelMatrix.clear();
        removeAll();
        revalidate();
    }

    public String getText(int rowIndex, int columnIndex)
    {
        JLabel label;

        label = labelMatrix.get(rowIndex, columnIndex);
        return label != null ? label.getText() : null;
    }

    public void setText(int rowIndex, int columnIndex, String text)
    {
        JLabel label;

        label = labelMatrix.get(rowIndex, columnIndex);
        if (label != null)
        {
            label.setText(text);
        }
        else addLabel(rowIndex, columnIndex, text);
    }

    public void addRow(List<String> rowStrings)
    {
        int rowIndex;

        rowIndex = labelMatrix.getRowSize();
        for (int columnIndex = 0; columnIndex < rowStrings.size(); columnIndex++)
        {
            setText(rowIndex, columnIndex, rowStrings.get(columnIndex));
        }
    }

    public void addColumn(List<String> columnStrings)
    {
        int columnIndex;

        columnIndex = labelMatrix.getColumnSize();
        for (int rowIndex = 0; rowIndex < columnStrings.size(); rowIndex++)
        {
            setText(rowIndex, columnIndex, columnStrings.get(rowIndex));
        }
    }

    private JLabel addLabel(int rowIndex, int columnIndex, String text)
    {
        GridBagConstraints constraints;
        JLabel label;

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridy = rowIndex;
        constraints.gridx = columnIndex;
        constraints.weightx = getColumnWeight(columnIndex);
        constraints.weighty = getRowWeight(rowIndex);
        constraints.insets = new Insets(halfVerticalSpacing, halfHorizontalSpacing, halfVerticalSpacing, halfHorizontalSpacing);

        label = new JLabel(text);
        label.setHorizontalAlignment(getHorizontalTextAlignment(rowIndex, columnIndex));
        labelMatrix.put(rowIndex, columnIndex, label);
        add(label, constraints);
        revalidate();
        return label;
    }

    private T8LabelMatrixRowDefinition getRowDefinition(int rowIndex)
    {
        for (T8LabelMatrixRowDefinition rowDefinition : definition.getRowDefinitions())
        {
            if (rowDefinition.getRowIndex() == rowIndex) return rowDefinition;
        }

        return null;
    }

    private T8LabelMatrixColumnDefinition getColumnDefinition(int columnIndex)
    {
        for (T8LabelMatrixColumnDefinition columnDefinition : definition.getColumnDefinitions())
        {
            if (columnDefinition.getColumnIndex() == columnIndex) return columnDefinition;
        }

        return null;
    }

    private double getRowWeight(int rowIndex)
    {
        T8LabelMatrixRowDefinition rowDefinition;

        rowDefinition = getRowDefinition(rowIndex);
        return rowDefinition != null ? rowDefinition.getWeight() : 0.0;
    }

    private double getColumnWeight(int columnIndex)
    {
        T8LabelMatrixColumnDefinition columnDefinition;

        columnDefinition = getColumnDefinition(columnIndex);
        return columnDefinition != null ? columnDefinition.getWeight() : 0.0;
    }

    private int getHorizontalTextAlignment(int rowIndex, int columnIndex)
    {
        T8LabelMatrixColumnDefinition columnDefinition;
        HorizontalTextAlignment textAlignment;

        // Determine the horizontal text alignment (column definition takes precedence).
        columnDefinition = getColumnDefinition(columnIndex);
        if (columnDefinition != null)
        {
            textAlignment = columnDefinition.getHorizontalTextAlignment();
        }
        else
        {
            T8LabelMatrixRowDefinition rowDefinition;

            rowDefinition = getRowDefinition(rowIndex);
            if (rowDefinition != null)
            {
                textAlignment = columnDefinition.getHorizontalTextAlignment();
            }
            else textAlignment = HorizontalTextAlignment.CENTER;
        }

        // Convert the text alignment enum to the corresponding Swing Constant.
        if (textAlignment == HorizontalTextAlignment.CENTER) return SwingConstants.CENTER;
        else if (textAlignment == HorizontalTextAlignment.TRAILING) return SwingConstants.TRAILING;
        else return SwingConstants.LEADING;
    }
}
