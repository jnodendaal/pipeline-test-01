package com.pilog.t8.ui.button;

import com.pilog.t8.ui.laf.T8ButtonUI;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.button.T8ButtonAPIHandler;
import com.pilog.t8.definition.ui.button.T8ButtonDefinition;
import com.pilog.t8.definition.ui.toolbar.T8ToolBarDefinition;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8Button extends JXButton implements T8Component
{
    private final T8ButtonDefinition definition;
    protected final T8ComponentController controller;
    private final T8ButtonOperationHandler operationHandler;
    private final ArrayList<T8Component> childComponents;
    private final ButtonActionListener buttonActionListener;
    private Painter backgroundPainter;
    private Painter focusBackgroundPainter;
    private Painter selectedBackgroundPainter;
    private boolean mouseOver;
    private JPopupMenu buttonMenu;

    public T8Button(T8ButtonDefinition buttonDefinition, T8ComponentController controller)
    {
        this.definition = buttonDefinition;
        this.controller = controller;
        this.operationHandler = new T8ButtonOperationHandler(this);
        this.buttonActionListener = new ButtonActionListener();
        this.childComponents = new ArrayList<>();
        this.addActionListener(buttonActionListener);
        this.setOpaque(definition.isOpaque());
        setupPainters();
    }

    private void setupPainters()
    {
        T8ButtonUI buttonUI;
        boolean toolBarButton;

        // Check whether or not this button's container is a tool bar.
        toolBarButton = (definition.getParentDefinition() instanceof T8ToolBarDefinition);
        buttonUI = new T8ButtonUI(controller.getClientContext().getConfigurationManager().getLookAndFeelManager(controller.getContext()), definition);
        buttonUI.setIsToolbarButton(toolBarButton);

        setUI(buttonUI);
    }

    @Override
    public Dimension getMinimumSize()
    {
        return getPreferredSize();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8IconDefinition iconDefinition;

        iconDefinition = definition.getIconDefinition();
        if (iconDefinition != null)
        {
            this.setIcon(iconDefinition.getImage().getImageIcon());
        }

        setText(definition.getButtonText());
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
        if(T8c instanceof JMenu)
        {
            //We will not actually use the menu, we only need its items

            buttonMenu = ((JMenu)T8c).getPopupMenu();
            buttonMenu.pack();

            childComponents.add(T8c);
        }
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        for (T8Component t8Component : childComponents)
        {
            if(t8Component.getComponentDefinition().getIdentifier().equals(string)) return t8Component;
        }
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        Iterator<T8Component> iterator = childComponents.iterator();
        while(iterator.hasNext())
        {
            T8Component t8Component = iterator.next();
            if(t8Component.getComponentDefinition().getIdentifier().equals(string))
            {
                iterator.remove();
                return t8Component;
            }
        }
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return childComponents;
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
    }

    void changeIcon(String iconIdentifier)
    {
        try
        {
            T8IconDefinition iconDefinition;

            iconDefinition = (T8IconDefinition)controller.getClientContext().getDefinitionManager().getInitializedDefinition(controller.getContext(), definition.getRootProjectId(), iconIdentifier, null);
            if (iconDefinition != null)
            {
                this.setIcon(iconDefinition.getImage().getImageIcon());
            }
            else this.setIcon(null);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while chaning button '" + definition + "' icon to: " + iconIdentifier, e);
        }
    }

    private class ButtonActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            HashMap<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            controller.reportEvent(T8Button.this, definition.getComponentEventDefinition(T8ButtonAPIHandler.EVENT_BUTTON_PRESSED), eventParameters);

            if(buttonMenu != null)
            {
                Point location = new Point();
                location.y =  T8Button.this.getHeight();
                location.y -= T8Button.this.getInsets().bottom;
                location.x = 0;

                buttonMenu.show(T8Button.this, location.x, location.y);
            }
        }
    }
}
