package com.pilog.t8.ui.tree;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.tree.T8TreeAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TreeOperationHandler extends T8ComponentOperationHandler
{
    private final T8Tree tree;

    public T8TreeOperationHandler(T8Tree table)
    {
        super(table);
        this.tree = table;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8TreeAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITIES))
        {
            return HashMaps.newHashMap(T8TreeAPIHandler.PARAMETER_DATA_ENTITY_LIST, tree.getSelectedDataEntities());
        }
        else if (operationIdentifier.equals(T8TreeAPIHandler.OPERATION_GET_FIRST_SELECTED_DATA_ENTITY))
        {
            List<T8DataEntity> selectedEntities;

            selectedEntities = tree.getSelectedDataEntities();
            if ((selectedEntities != null) && (selectedEntities.size() > 0))
            {
                return HashMaps.newHashMap(T8TreeAPIHandler.PARAMETER_DATA_ENTITY, selectedEntities.get(0));
            }
            else return null;
        }
        else if (operationIdentifier.equals(T8TreeAPIHandler.OPERATION_REFRESH))
        {
            tree.rebuildTree();
            return null;
        }
        else if (operationIdentifier.equals(T8TreeAPIHandler.OPERATION_REFRESH_SELECTED_NODE))
        {
            tree.rebuildSelectedNode();
            return null;
        }
        else if (operationIdentifier.equals(T8TreeAPIHandler.OPERATION_REFRESH_SELECTED_NODE_PARENT))
        {
            tree.rebuildSelectedNodeParent();
            return null;
        }
        else if (operationIdentifier.equals(T8TreeAPIHandler.OPERATION_SET_NODE_PREFILTER))
        {
            String nodeIdentifier;
            String filterIdentifier;
            T8DataFilter prefilter;
            Boolean refreshParentNodes;
            Boolean refreshTree;
            
            nodeIdentifier = (String)operationParameters.get(T8TreeAPIHandler.PARAMETER_TREE_NODE_IDENTIFIER);
            filterIdentifier = (String)operationParameters.get(T8TreeAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
            prefilter = (T8DataFilter)operationParameters.get(T8TreeAPIHandler.PARAMETER_DATA_FILTER);
            refreshParentNodes = (Boolean)operationParameters.get(T8TreeAPIHandler.PARAMETER_REFRESH_PARENT_NODES);
            refreshTree = (Boolean)operationParameters.get(T8TreeAPIHandler.PARAMETER_REFRESH_TREE);
            if (refreshParentNodes == null) refreshParentNodes = false;
            if (refreshTree == null) refreshParentNodes = true;
            
            tree.setNodePrefilter(nodeIdentifier, filterIdentifier, prefilter);
            if (refreshTree) tree.rebuildTree(); // We don't need to refresh parent nodes if the entire tree will be refreshed.
            else if (refreshParentNodes) tree.rebuildNodeParents(nodeIdentifier);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
