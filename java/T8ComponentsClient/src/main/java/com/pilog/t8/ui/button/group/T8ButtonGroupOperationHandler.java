/**
 * Created on 10 Sep 2015, 7:57:26 AM
 */
package com.pilog.t8.ui.button.group;

import static com.pilog.t8.definition.ui.button.group.T8ButtonGroupAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8ButtonGroupOperationHandler extends T8ComponentOperationHandler
{
    private final T8ButtonGroup<?> buttonGroup;

    public T8ButtonGroupOperationHandler(T8ButtonGroup<?> buttonGroup)
    {
        super(buttonGroup);
        this.buttonGroup = buttonGroup;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        switch (operationIdentifier)
        {
            case OPERATION_ENABLE_GROUP:
                this.buttonGroup.setEnabled(true);
                break;
            case OPERATION_DISABLE_GROUP:
                this.buttonGroup.setEnabled(false);
                break;
            case OPERATION_SET_SELECTED:
                this.buttonGroup.setSelected((String)operationParameters.get(PARAMETER_CHILD_IDENTIFIER));
                break;
            case OPERATION_GET_SELECTED:
                return HashMaps.createSingular(PARAMETER_CHILD_IDENTIFIER, this.buttonGroup.getSelectedIdentifier());
            default:
                return super.executeOperation(operationIdentifier, operationParameters);
        }

        return null;
    }
}