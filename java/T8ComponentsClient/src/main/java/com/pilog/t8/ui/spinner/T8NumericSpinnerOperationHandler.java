package com.pilog.t8.ui.spinner;

import static com.pilog.t8.definition.ui.spinner.T8NumericSpinnerAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NumericSpinnerOperationHandler extends T8ComponentOperationHandler
{
    private final T8NumericSpinner spinner;

    public T8NumericSpinnerOperationHandler(T8NumericSpinner spinner)
    {
        super(spinner);
        this.spinner = spinner;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(OPERATION_GET_VALUE))
        {
            return HashMaps.createSingular(PARAMETER_VALUE, spinner.getValue());
        }
        else if(operationIdentifier.equals(OPERATION_SET_VALUE))
        {
            spinner.setValue(operationParameters.get(PARAMETER_VALUE));
            return null;
        }
        else if(operationIdentifier.equals(OPERATION_SET_MIN_VALUE))
        {
            spinner.setMinValue((Comparable)operationParameters.get(PARAMETER_VALUE));
            return null;
        }
        else if(operationIdentifier.equals(OPERATION_SET_MAX_VALUE))
        {
            spinner.setMaxValue((Comparable)operationParameters.get(PARAMETER_VALUE));
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
