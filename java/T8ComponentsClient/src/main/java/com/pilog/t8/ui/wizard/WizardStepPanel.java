package com.pilog.t8.ui.wizard;

import com.pilog.t8.definition.ui.wizard.T8WizardStep;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.border.EmptyBorder;

/**
 * @author Bouwer du Preez
 */
public class WizardStepPanel extends javax.swing.JPanel
{
    private final T8WizardStep step;
    private final T8Wizard wizard;
    private final List<WizardStepPanel> subStepPanels;

    private static final ImageIcon grayIcon = new javax.swing.ImageIcon(WizardStepPanel.class.getResource("/com/pilog/t8/ui/icons/statusGrayIcon.png"));
    private static final ImageIcon greenIcon = new javax.swing.ImageIcon(WizardStepPanel.class.getResource("/com/pilog/t8/ui/icons/statusGreenIcon.png"));

    public WizardStepPanel(T8Wizard wizard, T8WizardStep step, int leftIndent, int rightIndent)
    {
        this.step = step;
        this.wizard = wizard;
        this.subStepPanels = new ArrayList<>();
        initComponents();

        // Set the step header.
        if (!step.isCategory())
        {
            jLabelStep.setVisible(false);
            jToggleButtonStep.setText(step.getName());
            jToggleButtonStep.setToolTipText(step.getDescription());
            jToggleButtonStep.setIcon(grayIcon);
        }
        else
        {
            jToggleButtonStep.setVisible(false);
            jLabelStep.setText(step.getName());
            jLabelStep.setToolTipText(step.getDescription());
            setBorder(new EmptyBorder(10, 0, 0, 0));
        }

        // Add the sub-steps.
        jPanelSubSteps.setBorder(new EmptyBorder(0, leftIndent, 0, rightIndent));
        for (T8WizardStep subStep : step.getSubSteps())
        {
            WizardStepPanel subPanel;

            subPanel = new WizardStepPanel(wizard, subStep, 20, 0);
            subStepPanels.add(subPanel);
            jPanelSubSteps.add(subPanel);
        }
    }

    public void refresh()
    {
        // Set this step selected if it is the one referred to.
        jToggleButtonStep.setSelected(step.isSelected());
        jToggleButtonStep.setEnabled(step.isSelected() || step.isCompleted());

        // Refresh the icon.
        if (step.isCompleted())
        {
            jToggleButtonStep.setIcon(greenIcon);
        }
        else
        {
            jToggleButtonStep.setIcon(grayIcon);
        }

        // Make sure the selection is updated on sub-step panels.
        for (WizardStepPanel subStepPanel : subStepPanels)
        {
            subStepPanel.refresh();
        }
    }

    private void selectStep()
    {
        this.wizard.navigateToStep(this.step.getIdentifier());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBarHeader = new javax.swing.JToolBar();
        jToggleButtonStep = new javax.swing.JToggleButton();
        jLabelStep = new javax.swing.JLabel();
        jPanelSubSteps = new javax.swing.JPanel();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jToolBarHeader.setFloatable(false);
        jToolBarHeader.setRollover(true);
        jToolBarHeader.setOpaque(false);

        jToggleButtonStep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/statusGrayIcon.png"))); // NOI18N
        jToggleButtonStep.setText("Step Title Button");
        jToggleButtonStep.setFocusable(false);
        jToggleButtonStep.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButtonStep.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jToggleButtonStepActionPerformed(evt);
            }
        });
        jToolBarHeader.add(jToggleButtonStep);

        jLabelStep.setText("Step Label");
        jToolBarHeader.add(jLabelStep);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(jToolBarHeader, gridBagConstraints);

        jPanelSubSteps.setOpaque(false);
        jPanelSubSteps.setLayout(new javax.swing.BoxLayout(jPanelSubSteps, javax.swing.BoxLayout.Y_AXIS));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jPanelSubSteps, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jToggleButtonStepActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jToggleButtonStepActionPerformed
    {//GEN-HEADEREND:event_jToggleButtonStepActionPerformed
        selectStep();
    }//GEN-LAST:event_jToggleButtonStepActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelStep;
    private javax.swing.JPanel jPanelSubSteps;
    private javax.swing.JToggleButton jToggleButtonStep;
    private javax.swing.JToolBar jToolBarHeader;
    // End of variables declaration//GEN-END:variables
}
