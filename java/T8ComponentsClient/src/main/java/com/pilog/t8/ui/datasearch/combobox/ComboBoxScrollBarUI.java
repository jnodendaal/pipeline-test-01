package com.pilog.t8.ui.datasearch.combobox;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;

/**
 * @author Hennie Brink
 */
public class ComboBoxScrollBarUI extends BasicScrollBarUI
{
    private static final Color TRANSPARENT_COLOR = new Color(0, 0, 0, 0);

    public ComboBoxScrollBarUI()
    {
    }

    @Override
    protected void installDefaults()
    {
        super.installDefaults();

        scrollBarWidth = 10;
        trackColor = TRANSPARENT_COLOR;
        trackHighlightColor = TRANSPARENT_COLOR;
    }

    @Override
    protected JButton createDecreaseButton(int orientation)
    {
        return super.createDecreaseButton(orientation);
    }

    @Override
    protected JButton createIncreaseButton(int orientation)
    {
        return super.createIncreaseButton(orientation);
    }

    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds)
    {
        super.paintThumb(g, c, thumbBounds);
    }

    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds)
    {
        super.paintTrack(g, c, trackBounds);
    }

    @Override
    protected void paintIncreaseHighlight(Graphics g)
    {
        super.paintIncreaseHighlight(g);
    }

    @Override
    protected void paintDecreaseHighlight(Graphics g)
    {
        super.paintDecreaseHighlight(g);
    }



}
