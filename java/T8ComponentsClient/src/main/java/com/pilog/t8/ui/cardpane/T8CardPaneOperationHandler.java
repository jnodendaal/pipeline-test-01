package com.pilog.t8.ui.cardpane;

import static com.pilog.t8.definition.ui.cardpane.T8CardPaneAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CardPaneOperationHandler extends T8ComponentOperationHandler
{
    private final T8CardPane cardPane;

    public T8CardPaneOperationHandler(T8CardPane cardPane)
    {
        super(cardPane);
        this.cardPane = cardPane;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        switch (operationIdentifier)
        {
            case OPERATION_SHOW_COMPONENT:
                this.cardPane.showComponent((String)operationParameters.get(PARAMETER_COMPONENT_IDENTIFIER));
                return null;
            case OPERATION_CURRENT_COMPONENT:
                return HashMaps.createSingular(PARAMETER_COMPONENT_IDENTIFIER, this.cardPane.getShownComponent());
            default:
                return super.executeOperation(operationIdentifier, operationParameters);
        }
    }
}
