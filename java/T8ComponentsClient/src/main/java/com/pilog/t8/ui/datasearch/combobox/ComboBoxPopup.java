package com.pilog.t8.ui.datasearch.combobox;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxDefinition;
import com.pilog.t8.definition.ui.list.T8ListDefinition.ListSelectionMode;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.MouseInputAdapter;
import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXSearchField;
import org.jdesktop.swingx.painter.MattePainter;

/**
 * @author Hennie Brink
 */
public class ComboBoxPopup extends T8DefaultComponentContainer
{
    private final int PAGING_SIZE = 100;

    private final T8DataSearchComboBoxDefinition definition;
    private final T8DataSearchComboBox owner;
    private final JXPanel dataView;
    private final FocusListenerPanel focusListenerPanel;

    private ComboBoxDataLoader loader;
    private T8DataFilter dataFilter;
    private JXSearchField searchField;
    private JXList dataList;
    private JButton optionsButton;
    private JPopupMenu optionsMenu;
    private JCheckBoxMenuItem showSelectedMenuItem;
    private JLabel infoLabel;
    private JLayeredPane popupLayeredPane;

    public ComboBoxPopup(T8DataSearchComboBox owner)
    {
        this.owner = owner;
        this.definition = owner.getComponentDefinition();

        this.dataView = new JXPanel();
        this.focusListenerPanel = new FocusListenerPanel();

        initialize();
    }

    private void initialize()
    {
        setMessage(owner.getTranslation("Processing...."));
        setComponent(dataView);
        setBorder(new EtchedBorder());

        //Add the components
        JMenuItem optionsMenuItem;
        JPanel searchPanel;
        JScrollPane jScrollPane;
        SearchActionListener actionListener;

        infoLabel = new JLabel();
        searchPanel = new JPanel(new BorderLayout(5, 0));
        searchField = new JXSearchField(owner.getTranslation(owner.getComponentDefinition().getSearchPrompt()));
        optionsButton = new JButton(new ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/menuIcon.png")));
        optionsMenu = new JPopupMenu();
        dataList = new JXList();
        dataList.setModel(new ComboBoxListModel(new ComboBoxModelRootNode()));
        jScrollPane = new JScrollPane(dataList);

        jScrollPane.setOpaque(false);
        jScrollPane.getViewport().setOpaque(false);
        jScrollPane.setBorder(null);

        actionListener = new SearchActionListener();
        searchField.setOpaque(false);
        searchField.setRecentSearchesSaveKey(definition.getIdentifier());
        searchField.setFindAction(actionListener);
        searchField.addActionListener(actionListener);
        searchField.setInstantSearchDelay(500);

        infoLabel.setVisible(false);
        infoLabel.setForeground(new Color(200, 200, 200));

        optionsButton.setContentAreaFilled(false);
        optionsButton.setBorder(new EmptyBorder(0, 0, 0, 2));
        optionsButton.setFocusPainted(false);
        optionsButton.addActionListener(new OptionsActionListener());

        optionsMenuItem = new JMenuItem(owner.getTranslation("Clear Selection"));
        optionsMenuItem.addActionListener(new ClearSelectionActionListener());
        optionsMenu.add(optionsMenuItem);

        showSelectedMenuItem = new JCheckBoxMenuItem(owner.getTranslation("Show Selected"));
        showSelectedMenuItem.addActionListener(new ShowSelectedActionListener());
        optionsMenu.add(showSelectedMenuItem);

        optionsMenuItem = new JMenuItem(owner.getTranslation("Refresh"));
        optionsMenuItem.addActionListener(new RefreshDataSourceActionListener());
        optionsMenu.add(optionsMenuItem);

        dataList.setCellRenderer(new ComboBoxModelRenderer());
        dataList.addMouseListener(new ComboBoxListMouseListener(this, dataList));
        dataList.setRolloverEnabled(true);
        dataList.setSelectionMode(ListSelectionMode.SINGLE_SELECTION.ordinal());
        dataList.setOpaque(false);
        dataList.setBorder(null);

        searchPanel.setOpaque(false);
        searchPanel.add(searchField, BorderLayout.CENTER);
        searchPanel.add(optionsButton, BorderLayout.EAST);

        dataView.setLayout(new BorderLayout(5, 5));
        dataView.setBorder(new EmptyBorder(5, 5, 5, 5));
        dataView.add(searchPanel, BorderLayout.NORTH);
        dataView.add(jScrollPane, BorderLayout.CENTER);
        dataView.add(infoLabel, BorderLayout.SOUTH);
    }

    void startComponent()
    {
        JLayeredPane layeredPane;

         if(popupLayeredPane != null)
         {
             stopComponent();
             //Make sure that if the main layered pane is not null that all previous refereneces will be removed from the previous layered pane
         }

        layeredPane = JLayeredPane.getLayeredPaneAbove(owner);
        layeredPane.add(focusListenerPanel, new Integer(JLayeredPane.POPUP_LAYER - 1));
        layeredPane.add(this, JLayeredPane.POPUP_LAYER);

        popupLayeredPane = layeredPane;

        setVisible(false);
    }

    void stopComponent()
    {
        JLayeredPane layeredPane;

        layeredPane = popupLayeredPane;
        layeredPane.remove(focusListenerPanel);
        layeredPane.remove(this);

        popupLayeredPane = null;

        setVisible(false);
    }

    Set<Object> getSelectedValues()
    {
        ComboBoxListModel model;

        model = (ComboBoxListModel) dataList.getModel();

        return model.getSelectedValues();
    }

    void setSelectedValues(List<Object> valueList, boolean clearSelection)
    {
        ComboBoxListModel model;

        // First we clear the current selection if specified
        if (clearSelection) clearSelectedValues();

        // Now we set the selected values
        model = (ComboBoxListModel) this.dataList.getModel();
        model.setSelectedValues(valueList);

        this.dataList.repaint();
    }

    /**
     * Clears the selected values on the model for the pop-up.
     */
    void clearSelectedValues()
    {
        ComboBoxListModel model;

        model = (ComboBoxListModel) dataList.getModel();
        model.clearCurrentSelections();

        dataList.repaint();
    }

    void showLoadingView()
    {
        setLocked(true);

        calculateLocation();
    }

    void showDataView(ComboBoxListModel newData)
    {
        dataList.setModel(newData);
        searchField.setSearchMode(newData.allowInstantSearch() ? JXSearchField.SearchMode.INSTANT : JXSearchField.SearchMode.REGULAR);

        setLocked(false);

        infoLabel.setVisible(false);
        showSelectedMenuItem.setSelected(false);
        searchField.setVisible(newData.isSearchVisible());
        optionsButton.setVisible(newData.isOptionsVisible());

        if (dataList.getModel().getSize() > 900)
        {
            infoLabel.setText(owner.getTranslation("*Refine Search Criteria"));
            infoLabel.setVisible(true);
        }

        dataList.setVisibleRowCount(dataList.getModel().getSize() < 11 ? dataList.getModel().getSize() : 10);

        calculateLocation();
    }

    @Override
    public void invalidate()
    {
        super.invalidate();

        if(owner == null) return; //Do not process the invalidate if we have not been completely initialized

        Dimension contentSize;
        Dimension prefferedSize;

        contentSize = getPreferredSize();
        prefferedSize = new Dimension();
        prefferedSize.width = contentSize.width;
        prefferedSize.height = contentSize.height;

        //Try to set the size the same as the owner combo box
        if (prefferedSize.width < owner.getWidth())
        {
            prefferedSize.width = owner.getWidth();
        }

        //Try to obtain a preffered size between the range
        if (prefferedSize.width < 280 || prefferedSize.height > 350)
        {
            prefferedSize.width = 280;
        }

        //Try to obtain a height within the preffered range
        if (prefferedSize.height > 450)
        {
            prefferedSize.height = 450;
        }

        setSize(prefferedSize);

        if (getWidth() > 0 && getHeight() > 0)
        {
            Paint paint;

            paint = new LinearGradientPaint(getWidth() / 2, 0, getWidth() / 2, getHeight() * 2, new float[]{0.1f, 0.6f}, new Color[]{new Color(255, 255, 255), new Color(240, 240, 245)});
            dataView.setBackgroundPainter(new MattePainter(paint));
        }
    }

    private void calculateLocation()
    {
        invalidate();

        Point comboLocation;
        Dimension comboSize;
        Point location;

        comboLocation = SwingUtilities.convertPoint(owner, 0, 0, JLayeredPane.getLayeredPaneAbove(owner));
        comboSize = owner.getSize();
        location = new Point();

        location.y = comboLocation.y + (int) comboSize.getHeight();
        location.x = comboLocation.x;

        //Check that we are within the screen bounds
        Dimension screenSize;
        Dimension mySize;

        screenSize = SwingUtilities.getRootPane(owner).getSize();
        mySize = getSize();

        if (location.x + mySize.width > screenSize.width)
        {
            location.x -= (location.x + mySize.width) - screenSize.width;
        }

        if (location.y + mySize.height > screenSize.height)
        {
            location.y -= (location.y + mySize.height) - screenSize.height;
        }

        setLocation(location);
    }

    @Override
    public void setVisible(boolean visible)
    {
        super.setVisible(visible);
        focusListenerPanel.setVisible(visible);

        if (visible)
        {
            loadNewData(null);
            calculateLocation();
            requestFocus();
            transferFocus();
        }
    }

    private void loadNewData(String searchQuery)
    {
        if (this.loader == null)
        {
            this.loader = new ComboBoxDataLoader(this, owner.getContext(), definition.getDataSourceIdentifier(), PAGING_SIZE);
        }

        if ((this.loader.getState() == SwingWorker.StateValue.DONE || loader.getState() == SwingWorker.StateValue.STARTED) && !Objects.equals(searchQuery, loader.getSearchQuery()))
        {
            this.loader.cancel(false);
            this.loader = new ComboBoxDataLoader(this, owner.getContext(), definition.getDataSourceIdentifier(), PAGING_SIZE);
        }
        this.loader.setSearchQuery(searchQuery);
        this.loader.setDataSourceFilter(this.dataFilter);
        this.loader.execute();
    }

    void refreshDataSource()
    {
        if (this.loader != null) //Only refresh the data source if the data has already been loaded
        {
            String currentQuery;

            currentQuery = loader.getSearchQuery();

            this.loader = new ComboBoxDataLoader(this, owner.getContext(), definition.getDataSourceIdentifier(), PAGING_SIZE);
            this.loader.setSearchQuery(currentQuery);
            this.loader.setDataSourceFilter(this.dataFilter);
            this.loader.execute();
        }
    }

    void setDataSourceFilter(T8DataFilter dataFilter, boolean refresh)
    {
        this.dataFilter = dataFilter;
        if (refresh) refreshDataSource();
    }

    /**
     * Simply refreshes the data source with a larger page size.
     */
    public void loadAdditionalData()
    {
        ComboBoxListModel model;
        String currentQuery;

        currentQuery = loader.getSearchQuery();
        model = (ComboBoxListModel) dataList.getModel();

        loader = new ComboBoxDataLoader(this, owner.getContext(), definition.getDataSourceIdentifier(), model.getNodesSize());
        loader.setSearchQuery(currentQuery);
        loader.setDataSourceFilter(this.dataFilter);
        loader.increaseDataSetSize(PAGING_SIZE);
        loader.execute();
    }

    private class SearchActionListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (searchField.getSearchMode() == JXSearchField.SearchMode.INSTANT)
            {
                ComboBoxListModel model;

                model = (ComboBoxListModel) dataList.getModel();
                model.setSearchQuery(searchField.getText());
            }
            else
            {
                loadNewData(searchField.getText());
            }
        }
    }

    private class OptionsActionListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            Point location;
            JButton button;

            button = (JButton) e.getSource();
            location = new Point();
            location.y = button.getHeight();
            location.y -= button.getInsets().bottom;
            location.x = 0;

            optionsMenu.show(button, location.x, location.y);
        }
    }

    private class ClearSelectionActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            clearSelectedValues();
        }
    }

    private class RefreshDataSourceActionListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            refreshDataSource();
        }

    }

    private class ShowSelectedActionListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            JCheckBoxMenuItem source;
            ComboBoxListModel model;

            source = (JCheckBoxMenuItem) e.getSource();
            model = (ComboBoxListModel) dataList.getModel();
            model.setOnlyShowSelected(source.isSelected());

            dataList.repaint();

            infoLabel.setText(owner.getTranslation("*Showing Selected Items"));
            infoLabel.setVisible(source.isSelected());

            invalidate();
        }
    }

    private class FocusListenerPanel extends JComponent
    {

        FocusListenerPanel()
        {
            setVisible(false);
            setOpaque(false);
            setLocation(0, 0);
            addMouseListener(new MouseInputAdapter()
            {
                private boolean armed;

                @Override
                public void mousePressed(MouseEvent e)
                {
                    armed = e.getButton() == MouseEvent.BUTTON1;
                }

                @Override
                public void mouseReleased(MouseEvent e)
                {
                    Point comboBoxPopupPoint;

                    comboBoxPopupPoint = e.getLocationOnScreen();
                    SwingUtilities.convertPointFromScreen(comboBoxPopupPoint, ComboBoxPopup.this);

                    if(armed && !ComboBoxPopup.this.contains(comboBoxPopupPoint) && FocusListenerPanel.this.contains(e.getPoint()))
                    {
                        owner.applyFilter();
                    }
                    armed = false;
                }
            });
        }

        @Override
        public void invalidate()
        {
            super.invalidate();

            setSize(SwingUtilities.getRootPane(owner).getSize());
        }
    }
}
