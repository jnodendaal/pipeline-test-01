package com.pilog.t8.ui.filter.panel.component.list;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.list.T8FilterPanelCheckedListDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.list.T8FilterPanelListComponentAPIHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponent;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItem;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItemDataSource;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListSelectionModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JWindow;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.LineBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.metal.MetalComboBoxIcon;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXSearchField;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelCheckedList extends JComponent implements ActionListener, AncestorListener, T8FilterPanelListComponent, T8Component
{
    private final T8FilterPanelCheckedListDefinition definition;
    private final List<ChangeListener> changeListeners;
    private final T8ComponentController controller;
    private final T8FilterPanelListComponentOperationHandler operationHandler;
    private CheckListManager checkListManager;
    private DataSourceListModel listModel;
    private T8DataFilterCriteria lastCriteria;
    private DefaultFilterPanelListComponentItemCellRenderer renderer;
    private Map<String, Object> inputParameters;
    private JComponent drop_down_comp;
    private JButton arrow;
    private JWindow popup;
    private String dataEntityValueIdentifier;
    private T8FilterPanelListComponentItemDataSource dataSource;
    private DataLoader lastDataLoader;

    public T8FilterPanelCheckedList(T8FilterPanelCheckedListDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8FilterPanelListComponentOperationHandler(this);
        if (definition.getDatasourceDefinition() != null)
        {
            this.dataSource = definition.getDatasourceDefinition().getInstance();
        }
        this.dataEntityValueIdentifier = definition.getDataEntityKeyFieldIdentifier();
        Icon displayIcon = null;

        if (definition.getIconDefinition() != null)
        {
            displayIcon = definition.getIconDefinition().getImage().getImageIcon();
        }

        if (displayIcon == null)
        {
            displayIcon = new MetalComboBoxIcon();
        }

        arrow = new JXButton(displayIcon);

        if (definition.isTransparentSet() != null && definition.isTransparentSet())
        {
            arrow.setOpaque(false);
            arrow.setContentAreaFilled(false);
            arrow.setBorderPainted(false);
        }
        Insets insets = arrow.getMargin();
        arrow.setMargin(new Insets(insets.top, 1, insets.bottom, 1));
        arrow.addActionListener(this);
        addAncestorListener(this);
        arrow.setHorizontalAlignment(SwingConstants.LEFT);
        arrow.setIconTextGap(5);
        this.changeListeners = new ArrayList<>();

        lastCriteria = new T8DataFilterCriteria();

        setupLayout();
    }
//<editor-fold defaultstate="collapsed" desc="Boiler Plate">

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return inputParameters;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier,
                                                Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public void addChangeListener(ChangeListener changeListener)
    {
        changeListeners.add(changeListener);
    }

    @Override
    public void removeChangeListener(ChangeListener changeListener)
    {
        changeListeners.remove(changeListener);
    }

    @Override
    public void fireStateChanged()
    {
        for (ChangeListener changeListener : changeListeners)
        {
            changeListener.stateChanged(new ChangeEvent(this));
        }
        Map<String, Object> eventParameters;
        java.util.List<Object> values;

        values = new ArrayList<>();
        eventParameters = new HashMap<>();

        if (checkListManager != null && listModel != null)
        {
            for (int i = 0; i < listModel.getSize(); i++)
            {
                if (checkListManager.getSelectionModel().isSelectedIndex(i))
                {
                    values.add(listModel.getElementAt(i).getValue());
                }
            }
        }
        eventParameters.put(T8FilterPanelListComponentAPIHandler.PARAMETER_NEW_VALUE_LIST, values);

        controller.reportEvent(this, definition.getComponentEventDefinition(T8FilterPanelListComponentAPIHandler.EVENT_SELECTION_CHANGED), eventParameters);
    }

    @Override
    public Component getComponent()
    {
        return this;
    }

    @Override
    public String toString()
    {
        return this.definition.getIdentifier();
    }

    @Override
    public void ancestorAdded(AncestorEvent event)
    {
        hidePopup();
    }

    @Override
    public void ancestorRemoved(AncestorEvent event)
    {
        hidePopup();
    }

    @Override
    public void ancestorMoved(AncestorEvent event)
    {
        if (event.getSource() != popup)
        {
            hidePopup();
        }
    }

    @Override
    public void setNewDataSourceFilter(T8DataFilter dataFilter) throws Exception
    {
        this.dataSource.setNewDataSourceFilter(dataFilter);
    }

    private T8ClientContext getClientContext()
    {
        return controller.getClientContext();
    }

    private String getTranslatedString(String text)
    {
        return controller.getClientContext().getConfigurationManager().getUITranslation(controller.getContext(), text);
    }

    @Override
    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
    }
//</editor-fold>

    @Override
    public T8DataFilterCriteria getFilterCriteria()
    {
        if (checkListManager != null && listModel != null)
        {
            T8DataFilterCriteria filterCriteria = new T8DataFilterCriteria();
            T8DataFilterCriteria customFilterCriterias = new T8DataFilterCriteria();
            java.util.List<Object> values = new ArrayList<>();
            for (int i = 0; i < listModel.getSize(); i++)
            {
                if (checkListManager.getSelectionModel().isSelectedIndex(i))
                {
                    if (!definition.isTagField() && listModel.getElementAt(i).hasCustomClause())
                    {
                        customFilterCriterias.addFilterClause(T8DataFilterClause.DataFilterConjunction.OR, listModel.getElementAt(i).getFilterClause(controller.getContext(), dataEntityValueIdentifier));
                    }
                    else
                    {
                        values.add(listModel.getElementAt(i).getValue());
                    }
                }
            }
            if (customFilterCriterias.hasCriteria())
            {
                filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, customFilterCriterias);
            }
            if (values.isEmpty())
            {
                return filterCriteria;
            }
            if (!definition.isTagField())
            {
                filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND,
                                               dataEntityValueIdentifier,
                                               T8DataFilterCriterion.DataFilterOperator.IN,
                                               values);
            }
            else
            {
                for (Object object : values)
                {
                    if (object != null)
                    {
                        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.OR,
                                                       dataEntityValueIdentifier,
                                                       T8DataFilterCriterion.DataFilterOperator.EQUAL,
                                                       object.toString());
                    }
                }
            }
            return filterCriteria;
        }
        return null;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.inputParameters = inputParameters;
        if (definition.getRendererComponentDefinition() != null)
        {
            try
            {
                this.renderer = (DefaultFilterPanelListComponentItemCellRenderer) (ListCellRenderer<T8FilterPanelListComponentItem>) definition.getRendererComponentDefinition().getNewComponentInstance(controller);

                ((T8Component) renderer).initializeComponent(inputParameters);
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to load cell renderer.", ex);
            }
        }

        try
        {
            initialize(controller.getContext());
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to initialize component " + definition.getIdentifier(), ex);
            throw new RuntimeException(ex);
        }
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
        if (renderer instanceof T8Component)
        {
            ((T8Component) renderer).startComponent();
        }
        if(definition.isLoadOnStartup())
        {
            lastDataLoader = new DataLoader();
            lastDataLoader.execute();
        }
    }

    @Override
    public void stopComponent()
    {
        if (renderer instanceof T8Component)
        {
            ((T8Component) renderer).stopComponent();
        }
        if (lastDataLoader != null)
        {
            lastDataLoader.cancel(false);
        }
    }

    @Override
    public void refresh() throws Exception
    {
        if (lastDataLoader != null)
        {
            lastDataLoader.cancel(false);
        }
        //Re load the data
        lastDataLoader = new DataLoader();
        lastDataLoader.execute();
    }

    protected void setupLayout()
    {
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        setLayout(gbl);

        c.weightx = 1.0;
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 0;
        c.fill = c.BOTH;
        c.anchor = GridBagConstraints.WEST;
        //gbl.setConstraints(visible_comp, c);
        //add(visible_comp);

        //c.weightx = 0;
        //c.gridx++;
        gbl.setConstraints(arrow, c);
        add(arrow);

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        //check if the data has been loaded
        if(lastDataLoader == null)
        {
            lastDataLoader = new DataLoader();
            lastDataLoader.execute();
        }
        Dimension componentSize = new Dimension(getSize().width, drop_down_comp.getPreferredSize().height);
        if (getSize().width < 350)
        {
            componentSize = new Dimension(350, drop_down_comp.getPreferredSize().height);
        }
        drop_down_comp.setSize(componentSize);
        drop_down_comp.setPreferredSize(componentSize);
        popup.pack();
        // show the pop-up window
        Point pt = arrow.getLocationOnScreen();
        pt.translate(0, arrow.getHeight());
        popup.setLocation(pt);
        popup.toFront();
        popup.setVisible(true);
        popup.requestFocusInWindow();
        popup.invalidate();
    }

    protected Frame getFrame(Component comp)
    {
        if (comp == null)
        {

            comp = this;
        }
        if (comp.getParent() instanceof Frame)
        {
            return (Frame) comp.getParent();
        }
        return getFrame(comp.getParent());

    }

    public void hidePopup()
    {
        if (popup != null && popup.isVisible())
        {
            popup.setVisible(false);
        }
    }

    @Override
    public void initialize(T8Context context) throws Exception
    {
        //visible_comp = new JXLabel(getTranslatedString(definition.getDisplayLabel()) + ": " + getTranslatedString("All"));

        //Tell the user we are loading data
        arrow.setText(getTranslatedString(definition.getDisplayLabel()));
    }

    class DataSourceListModel implements ListModel<T8FilterPanelListComponentItem>
    {
        private java.util.List<T8FilterPanelListComponentItem> list;

        DataSourceListModel()
        {
            this(new ArrayList<T8FilterPanelListComponentItem>());
        }

        DataSourceListModel(java.util.List<T8FilterPanelListComponentItem> list)
        {
            this.list = list;
        }

        public java.util.List<T8FilterPanelListComponentItem> getListCopy()
        {
            return new ArrayList<>(list);
        }

        public void addItem(T8FilterPanelListComponentItem item)
        {
            list.add(item);
        }

        public void removeItem(T8FilterPanelListComponentItem item)
        {
            list.remove(item);
        }

        public boolean contains(T8FilterPanelListComponentItem item)
        {
            return list.contains(item);
        }

        @Override
        public int getSize()
        {
            return list.size();
        }

        @Override
        public T8FilterPanelListComponentItem getElementAt(int index)
        {
            return list.get(index);
        }

        @Override
        public void addListDataListener(ListDataListener l)
        {
        }

        @Override
        public void removeListDataListener(ListDataListener l)
        {
        }
    }

    class CheckListManager extends MouseAdapter implements ListSelectionListener, ActionListener
    {
        private final ListSelectionModel selectionModel = new DefaultListSelectionModel();
        private final int hotspot = new JCheckBox().getPreferredSize().width;
        private JList list = new JList();

        CheckListManager(JList list)
        {
            this.list = list;
            list.registerKeyboardAction(this, KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), JComponent.WHEN_FOCUSED);
            list.addMouseListener(this);
            selectionModel.addListSelectionListener(this);
        }

        public ListSelectionModel getSelectionModel()
        {
            return selectionModel;
        }

        private void toggleSelection(int index)
        {
            if (index < 0)
            {
                return;
            }

            if (selectionModel.isSelectedIndex(index))
            {
                selectionModel.removeSelectionInterval(index, index);
            }
            else
            {
                selectionModel.addSelectionInterval(index, index);
            }
        }

        /*------------------------------[ MouseListener ]-------------------------------------*/
        @Override
        public void mouseClicked(MouseEvent me)
        {
            int index = list.locationToIndex(me.getPoint());
            if (index < 0)
            {
                return;
            }
            if (me.getX() > list.getCellBounds(index, index).x + hotspot)
            {
                return;
            }
            toggleSelection(index);
        }

        /*-----------------------------[ ListSelectionListener ]---------------------------------*/
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            list.repaint(list.getCellBounds(e.getFirstIndex(), e.getLastIndex()));
        }

        /*-----------------------------[ ActionListener ]------------------------------*/
        @Override
        public void actionPerformed(ActionEvent e)
        {
            toggleSelection(list.getSelectedIndex());
        }
    }

    private class DataLoader extends SwingWorker<Void, Void>
    {
        @Override
        protected Void doInBackground() throws Exception
        {
            SwingUtilities.invokeAndWait(new Runnable()
            {

                @Override
                public void run()
                {
                    setEnabled(false);
                    arrow.setText(getTranslatedString("Loading data.."));
                }
            });
            if (dataSource != null)
            {
                dataSource.initialize(controller.getContext(), inputParameters);
            }
            else
            {
                throw new Exception("No Data source provided, please configure data source for component in definition " + definition.getPublicIdentifier());
            }

            //We have nothing to return
            return null;
        }

        @Override
        protected void done()
        {
            if (!isCancelled())
            {
                try
                {
                    //This does not do anything except rethrow the exception from the datasource if there was any
                    get();

                    //Create a new list model to add the items received from the data source to
                    listModel = new DataSourceListModel();

                    //Load the items from the data source into the list model
                    while (dataSource.hasNext())
                    {
                        listModel.addItem(dataSource.next());
                    }

                    //Create a new UI list using the list model
                    final JXList list = new JXList(listModel);

                    //create a checklist manager using the list model so that we can know what items are checked
                    checkListManager = new CheckListManager(list);

                    //Eye candy :)
                    list.setOpaque(false);

                    //Text field used to search and filter the list
                    final JXSearchField textField = new JXSearchField();
                    textField.addActionListener(new ActionListener()
                    {

                        @Override
                        public void actionPerformed(ActionEvent e)
                        {
                            String text = textField.getText().trim();
                            for (int i = 0; i < listModel.getSize(); i++)
                            {
                                T8FilterPanelListComponentItem t8FilterPanelListComponentItem = listModel.getElementAt(i);
                                if (t8FilterPanelListComponentItem.getDisplayName().contains(text) || Strings.isNullOrEmpty(text))
                                {
                                    t8FilterPanelListComponentItem.setVisible(true);
                                }
                                else
                                {
                                    t8FilterPanelListComponentItem.setVisible(false);
                                }
                            }
                            list.updateUI();
                        }
                    });


                    JButton applyButton;

                    applyButton = new JButton(getTranslatedString("Apply"));
                    applyButton.addActionListener(new ActionListener()
                    {

                        @Override
                        public void actionPerformed(ActionEvent e)
                        {
                            T8FilterPanelCheckedList.this.requestFocus();
                        }
                    });
                    applyButton.setOpaque(false);
                    applyButton.setContentAreaFilled(false);

                    //Panel to host the search/filter box and list
                    JXPanel hostPanel = new JXPanel();
                    hostPanel.setLayout(new BorderLayout(3,3));
                    hostPanel.setBorder(new LineBorder(Color.BLACK, 1));

                    hostPanel.add(textField, BorderLayout.NORTH);
                    hostPanel.add(applyButton, BorderLayout.SOUTH);

                    //Scroll pane for the lis
                    JScrollPane scrollPane = new JScrollPane(list);
                    //scrollPane.setBackground(new Color(0, 0, 0, 0));
                    scrollPane.setOpaque(false);
                    scrollPane.getViewport().setOpaque(false);
                    scrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, new JXPanel());

                    hostPanel.add(scrollPane, BorderLayout.CENTER);


                    //Custom cell render for the list model to render icons etc
                    if (renderer == null)
                    {
                        renderer = new DefaultFilterPanelListComponentItemCellRenderer(checkListManager.getSelectionModel());
                    }
                    else
                    {
                        renderer.setSelectionModel(checkListManager.getSelectionModel());
                    }
                    list.setCellRenderer(renderer);

                    //if a background painter for the list was defined set it
                    if (definition.getBackgroundPainterDefinition() != null)
                    {
                        hostPanel.setBackgroundPainter(new T8PainterAdapter(definition.getBackgroundPainterDefinition().getNewPainterInstance()));
                    }
                    //This is the panel that will show when the user clicks the arrow to select items
                    drop_down_comp = hostPanel;

                    //Tell the user that all options are showing and that nothing is selected
                    arrow.setText(getTranslatedString(definition.getDisplayLabel()) + ": " + getTranslatedString("All"));

                    // build pop-up window
                    popup = new JWindow(getFrame(null));
                    popup.setContentPane(drop_down_comp);

                    //Add a focus listener so that we will know to close the popup menu if the user clicks outside of it
                    popup.addWindowFocusListener(new WindowAdapter()
                    {
                        @Override
                        public void windowLostFocus(WindowEvent evt)
                        {
                            //The popup window is closing so lets update the selected items and trigger the filter event
                            popup.setVisible(false);
                            int count = 0;
                            for (int i = 0; i < listModel.getSize(); i++)
                            {
                                if (checkListManager.getSelectionModel().isSelectedIndex(i))
                                {
                                    count++;
                                }
                            }
                            if (count > 0)
                            {
                                (arrow).setText(getTranslatedString(definition.getDisplayLabel()) + ": " + count + getTranslatedString(" selected"));
                            }
                            else
                            {
                                (arrow).setText(getTranslatedString(definition.getDisplayLabel()) + ": " + getTranslatedString("All"));
                            }
                            T8DataFilterCriteria newCriteria = getFilterCriteria();
                            if (newCriteria != null && !newCriteria.equalsFilterCriteria(lastCriteria))
                            {
                                lastCriteria = newCriteria;
                                fireStateChanged();
                            }
                        }
                    });

                    //Make it nice and tight so everything will fit neatly
                    popup.pack();

                    //Allow the user again to do user things
                    setEnabled(true);
                }
                catch (Exception ex)
                {
                    //Tell the user we failed in our quest
                    arrow.setText(getTranslatedString("Failed to load data.."));
                    T8Log.log("Failed to load data source for " + definition.getPublicIdentifier(), ex);
                }
            }
        }
    }
}
