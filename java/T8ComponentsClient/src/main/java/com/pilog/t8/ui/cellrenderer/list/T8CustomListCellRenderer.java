package com.pilog.t8.ui.cellrenderer.list;

import com.pilog.t8.client.T8ClientScriptRunner;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.list.T8ListCellRenderableComponent;
import com.pilog.t8.ui.list.T8ListCellRendererComponent;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.cellrenderer.list.T8CustomListCellRendererDefinition;
import com.pilog.t8.definition.ui.cellrenderer.list.T8CustomListCellRendererScriptDefinition;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * @author Hennie Brink
 */
public class T8CustomListCellRenderer extends JPanel implements ListCellRenderer, T8Component, T8ListCellRendererComponent
{
    private final T8ComponentController controller;
    private final T8CustomListCellRendererDefinition definition;
    private T8ClientScriptRunner renderScript;
    private T8Component component;

    public T8CustomListCellRenderer(T8ComponentController controller, T8CustomListCellRendererDefinition definition)
    {
        this.controller = controller;
        this.definition = definition;
        setLayout(new BorderLayout());
        setOpaque(false);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        if (renderScript != null)
        {
            Map<String, Object> inputParameters;

            inputParameters = new HashMap<>();
            inputParameters.put(T8CustomListCellRendererScriptDefinition.PARAMETER_INDEX, index);
            inputParameters.put(T8CustomListCellRendererScriptDefinition.PARAMETER_FOCUSED, cellHasFocus);
            inputParameters.put(T8CustomListCellRendererScriptDefinition.PARAMETER_SELECTED, isSelected);
            inputParameters.put(T8CustomListCellRendererScriptDefinition.PARAMETER_VALUE, value);

            try
            {
                renderScript.runScript(inputParameters);
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to execute script " + definition.getPublicIdentifier(), ex);
            }
        }

        return this;
    }

    @Override
    public void setRendererData(T8ListCellRenderableComponent sourceComponent, Map<String, Object> dataRow, String rendererKey, Map<RenderParameter, Object> renderParameters)
    {
        if (renderScript != null)
        {
            Map<String, Object> inputParameters;

            inputParameters = new HashMap<>();
            inputParameters.put(T8CustomListCellRendererScriptDefinition.PARAMETER_INDEX, renderParameters.get(RenderParameter.INDEX));
            inputParameters.put(T8CustomListCellRendererScriptDefinition.PARAMETER_FOCUSED, renderParameters.get(RenderParameter.FOCUSED));
            inputParameters.put(T8CustomListCellRendererScriptDefinition.PARAMETER_SELECTED, renderParameters.get(RenderParameter.SELECTED));
            inputParameters.put(T8CustomListCellRendererScriptDefinition.PARAMETER_VALUE, dataRow);

            try
            {
                renderScript.runScript(inputParameters);
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to execute script " + definition.getPublicIdentifier(), ex);
            }
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8CustomListCellRendererScriptDefinition scriptDefinition;

        scriptDefinition = definition.getPreRendererScriptDefinition();
        if (scriptDefinition != null)
        {
            try
            {
                renderScript = new T8ClientScriptRunner(controller);
                renderScript.prepareScript(scriptDefinition);
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to create pre render script " + scriptDefinition.getPublicIdentifier(), ex);
            }
        }

        setOpaque(definition.isOpaque());
        setVisible(definition.isVisible());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        component = childComponent;
        add((Component)component, BorderLayout.CENTER);
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return component;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        ArrayList<T8Component> components;

        components = new ArrayList<>();
        components.add(component);
        return components;
    }

    @Override
    public int getChildComponentCount()
    {
        return component == null ? 0 : 1;
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
    }
}
