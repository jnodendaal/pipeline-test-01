package com.pilog.t8.ui.file.uploadpanel;

/**
 *
 * @author Andre Scheepers
 */
public class T8FileUploadLink
{
    private String linkText;
    private String linkHReference;
    
    public T8FileUploadLink()
    {}
    
    public T8FileUploadLink(String linkText, String linkHReference)
    {
        this.linkText = linkText;
        this.linkHReference = linkHReference;        
    }

    public String getLinkText()
    {
        return linkText;
    }

    public void setLinkText(String linkText)
    {
        this.linkText = linkText;
    }

    public String getLinkHReference()
    {
        return linkHReference;
    }

    public void setLinkHReference(String linkHReference)
    {
        this.linkHReference = linkHReference;
    }
}
