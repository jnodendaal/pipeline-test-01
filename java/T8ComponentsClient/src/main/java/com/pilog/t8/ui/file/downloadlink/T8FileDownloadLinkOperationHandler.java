package com.pilog.t8.ui.file.downloadlink;

import com.pilog.t8.definition.ui.file.downloadlink.T8FileDownloadLinkApiHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FileDownloadLinkOperationHandler extends T8ComponentOperationHandler
{
    private final T8FileDownloadLink link;

    public T8FileDownloadLinkOperationHandler(T8FileDownloadLink link)
    {
        super(link);
        this.link = link;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8FileDownloadLinkApiHandler.OPERATION_SET_REMOTE_FILE_DETAILS))
        {
            String fileContextIid;
            String filePath;

            fileContextIid = (String)operationParameters.get(T8FileDownloadLinkApiHandler.PARAMETER_FILE_CONTEXT_IID);
            filePath = (String)operationParameters.get(T8FileDownloadLinkApiHandler.PARAMETER_FILE_PATH);

            link.setRemoteFileDetails(fileContextIid, filePath);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
