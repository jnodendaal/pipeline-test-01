package com.pilog.t8.ui.table;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.celleditor.table.T8RowBasedCellEditor;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.ui.table.filter.T8FilterPanelTableCellEditorAdaptor;
import com.pilog.t8.ui.table.filter.T8FilterPanelTableCellRendererAdaptor;
import com.pilog.t8.utilities.components.celleditor.ComboBoxTableCellEditor;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

/**
 * @author Bouwer du Preez
 */
public class T8TableFilterPanel extends javax.swing.JPanel implements T8TableCellEditableComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8TableFilterPanel.class);

    private final EnumSet<DataFilterOperator> defaultOperators;
    private final T8RowBasedCellEditor operatorCellEditor;
    private final T8ConfigurationManager configurationManager;
    private final T8Context context;
    private final T8Table parentTable;
    private final T8TableDefinition tableDefinition;
    private final DefaultTableModel tableModel;
    private List<T8TableColumnDefinition> filterColumns;

    public T8TableFilterPanel(T8Table parentTable)
    {
        this.context = parentTable.getController().getContext();
        this.configurationManager = context.getClientContext().getConfigurationManager();
        initComponents();
        this.setBorder(new T8ContentHeaderBorder(parentTable.getTranslatedString("Filter Criteria")));
        this.parentTable = parentTable;
        this.tableDefinition = parentTable.getComponentDefinition();
        this.tableModel = (DefaultTableModel) jTableFilterValues.getModel();

        this.defaultOperators = T8DataType.getDefaultOperatorSet();
        this.operatorCellEditor = new T8RowBasedCellEditor(new ComboBoxTableCellEditor(true, getOperatorDisplayWrappers(this.defaultOperators)));

        this.jTableFilterValues.getColumn("Operator").setPreferredWidth(50);
        this.jTableFilterValues.getColumn("Operator").setCellEditor(this.operatorCellEditor);

        rebuildFilterTable();

        jTableFilterValues.addPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(PropertyChangeEvent evt)
            {
                if ("tableCellEditor".equals(evt.getPropertyName()))
                {
                    if (!jTableFilterValues.isEditing() && jTableFilterValues.getSelectedColumn() == 2)
                    {
                        Object filterValue;
                        int selectedRow;

                        selectedRow = jTableFilterValues.getSelectedRow();
                        filterValue = jTableFilterValues.getValueAt(selectedRow, 2);

                        if (jTableFilterValues.getValueAt(selectedRow, 1) == null)
                        {
                            if (filterValue instanceof String && !Strings.isNullOrEmpty((String) filterValue))
                            {
                                jTableFilterValues.setValueAt(new DataFilterOperatorDisplayWrapper(DataFilterOperator.EQUAL), selectedRow, 1);
                            }
                            else if (filterValue instanceof T8Timestamp)
                            {
                                jTableFilterValues.setValueAt(new DataFilterOperatorDisplayWrapper(DataFilterOperator.THIS_DAY), selectedRow, 1);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public Map<String, Object> getRowEntityFieldValues(int row)
    {
        return new HashMap<>(0);
    }

    private void rebuildFilterTable()
    {
        ArrayList<T8TableColumnDefinition> columnDefinitions;
        EnumSet<DataFilterOperator> typeSpecificOperators;
        T8ExpressionEvaluator expressionEvaluator;
        T8DataEntityDefinition entityDefinition;
        T8DataType fieldDataType;

        clearFilterTable();
        filterColumns = new ArrayList<>();
        expressionEvaluator = new T8ClientExpressionEvaluator(context);
        columnDefinitions = tableDefinition.getColumnDefinitions();
        entityDefinition = tableDefinition.getEntityDefinition();
        if (entityDefinition != null)
        {
            for (T8TableColumnDefinition columnDefinition : columnDefinitions)
            {
                // Only add visible columns to the filter table.
                if (columnDefinition.isVisible())
                {
                    Object[] dataRow;

                    dataRow = new Object[3];
                    dataRow[0] = columnDefinition.getColumnName();
                    dataRow[1] = null;
                    dataRow[2] = null;
                    if (!Strings.isNullOrEmpty(columnDefinition.getDefaultValueExpression()))
                    {
                        try
                        {
                            dataRow[2] = expressionEvaluator.evaluateExpression(columnDefinition.getDefaultValueExpression(), null, null);
                        } catch (EPICSyntaxException | EPICRuntimeException ex)
                        {
                            LOGGER.log("Failed to evaluate default expression on column definition " + columnDefinition.getPublicIdentifier(), ex);
                        }
                    }

                    fieldDataType = entityDefinition.getFieldDataType(columnDefinition.getFieldIdentifier());
                    typeSpecificOperators = fieldDataType.getApplicableOperators();
                    if (!this.defaultOperators.equals(typeSpecificOperators))
                    {
                        this.operatorCellEditor.addTableCellEditor(this.jTableFilterValues.getRowCount(), new ComboBoxTableCellEditor(true, getOperatorDisplayWrappers(typeSpecificOperators)));
                    }

                    tableModel.addRow(dataRow);
                    filterColumns.add(columnDefinition);
                }
            }

            jTableFilterValues.getColumn("Filter Value").setCellEditor(new T8FilterPanelTableCellEditorAdaptor(parentTable, this));
            jTableFilterValues.getColumn("Filter Value").setCellRenderer(new T8FilterPanelTableCellRendererAdaptor(parentTable, this));
        }
        else
        {
            throw new RuntimeException("No initialized entity definition found in table definition: " + tableDefinition + ", entity specified: " + tableDefinition.getDataEntityIdentifier());
        }
    }

    private T8DataFilterCriteria getFilterCriteria()
    {
        T8DataFilterCriteria filterCriteria;

        stopEditing();
        filterCriteria = new T8DataFilterCriteria();
        for (int columnIndex = 0; columnIndex < filterColumns.size(); columnIndex++)
        {
            if (tableModel.getValueAt(columnIndex, 1) != null)
            {
                DataFilterOperatorDisplayWrapper operatorDisplayWrapper;
                Object filterValue;
                String fieldIdentifier;

                filterValue = tableModel.getValueAt(columnIndex, 2);
                fieldIdentifier = filterColumns.get(columnIndex).getFieldIdentifier();
                operatorDisplayWrapper = (DataFilterOperatorDisplayWrapper) tableModel.getValueAt(columnIndex, 1);

                if (operatorDisplayWrapper.getOperator() == DataFilterOperator.IN || operatorDisplayWrapper.getOperator() == DataFilterOperator.NOT_IN)
                {
                    List<String> filterValues;
                    StringTokenizer stringTokenizer;

                    filterValues = new ArrayList<>();
                    stringTokenizer = new StringTokenizer((String) filterValue, ",");
                    while (stringTokenizer.hasMoreTokens())
                    {
                        filterValues.add(stringTokenizer.nextToken().trim());
                    }

                    filterValue = filterValues;
                }

                filterCriteria.addFilterClause(DataFilterConjunction.AND, fieldIdentifier, operatorDisplayWrapper.getOperator(), filterValue);
            }
        }

        return filterCriteria;
    }

    private void clearFilterTable()
    {
        while (tableModel.getRowCount() > 0)
        {
            tableModel.removeRow(0);
        }
    }

    private void clearFilterValues()
    {
        // We first need to make sure we stop editing
        stopEditing();

        while (tableModel.getRowCount() > 0)
        {
            tableModel.removeRow(0);
        }

        rebuildFilterTable();

        repaint();
    }

    private void stopEditing()
    {
        if (jTableFilterValues.getCellEditor() != null)
        {
            jTableFilterValues.getCellEditor().stopCellEditing();
        }
    }

    @Override
    public void cellDataEdited(Map<String, Object> dataRow, int rowIndex)
    {
        for (int i = 0; i < tableModel.getRowCount(); i++)
        {
            T8TableColumnDefinition columnDefinition = filterColumns.get(i);
            if (dataRow.containsKey(columnDefinition.getFieldIdentifier()))
            {
                tableModel.setValueAt(dataRow.get(columnDefinition.getFieldIdentifier()), i, 2);
                break;
            }
        }
    }

    @Override
    public Font getCellFont()
    {
        return parentTable.getCellFont();
    }

    @Override
    public Color getSelectedCellBackgroundColor()
    {
        return parentTable.getSelectedCellBackgroundColor();
    }

    @Override
    public Color getSelectedCellForegroundColor()
    {
        return parentTable.getSelectedCellForegroundColor();
    }

    @Override
    public Color getUnselectedCellBackgroundColor()
    {
        return parentTable.getUnselectedCellBackgroundColor();
    }

    @Override
    public Color getUnselectedCellForegroundColor()
    {
        return parentTable.getUnselectedCellForegroundColor();
    }

    @Override
    public Border getSelectedBorder()
    {
        return parentTable.getSelectedBorder();
    }

    @Override
    public Border getUnselectedBorder()
    {
        return parentTable.getUnselectedBorder();
    }

    @Override
    public Border getFocusedBorder()
    {
        return parentTable.getFocusedBorder();
    }

    @Override
    public Border getFocusedSelectedBorder()
    {
        return parentTable.getFocusedSelectedBorder();
    }

    @Override
    public T8ComponentController getController()
    {
        return parentTable.getController();
    }


    //<editor-fold defaultstate="collapsed" desc="Unused Inherited Methods">
    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return null;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }
    //</editor-fold>

    public T8TableColumnDefinition getColumnDefinition(int row)
    {
        return filterColumns.get(row);
    }

    private String translate(String text)
    {
        return configurationManager.getUITranslation(context, text);
    }

    private List<DataFilterOperatorDisplayWrapper> getOperatorDisplayWrappers(EnumSet<DataFilterOperator> filterOperators)
    {
        List<DataFilterOperatorDisplayWrapper> displayWrappers;

        displayWrappers = new ArrayList<>();
        for (DataFilterOperator dataFilterOperator : filterOperators)
        {
            displayWrappers.add(new DataFilterOperatorDisplayWrapper(dataFilterOperator));
        }
        Collections.sort(displayWrappers);

        return displayWrappers;
    }

    @Override
    public Object getAdditionalRenderData(int row, String fieldIdentifier)
    {
        return null;
    }

    private class DataFilterOperatorDisplayWrapper implements Comparable<DataFilterOperatorDisplayWrapper>
    {
        private final DataFilterOperator operator;

        private DataFilterOperatorDisplayWrapper(DataFilterOperator operator)
        {
            this.operator = operator;
        }

        public DataFilterOperator getOperator()
        {
            return operator;
        }

        @Override
        public String toString()
        {
            return translate(operator.getDisplayString());
        }

        @Override
        public int compareTo(DataFilterOperatorDisplayWrapper o)
        {
            return toString().compareTo(o.toString());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPaneFilterValues = new javax.swing.JScrollPane();
        jTableFilterValues = new javax.swing.JTable();
        jLabelSpacer = new javax.swing.JLabel();
        jButtonClear = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jButtonApply = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jTableFilterValues.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Column Name", "Operator", "Filter Value"
            }
        )
        {
            boolean[] canEdit = new boolean []
            {
                false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableFilterValues.setRowHeight(25);
        jScrollPaneFilterValues.setViewportView(jTableFilterValues);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jScrollPaneFilterValues, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        add(jLabelSpacer, gridBagConstraints);

        jButtonClear.setText("Clear");
        jButtonClear.setMaximumSize(new java.awt.Dimension(65, 23));
        jButtonClear.setMinimumSize(new java.awt.Dimension(65, 23));
        jButtonClear.setPreferredSize(new java.awt.Dimension(65, 23));
        jButtonClear.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonClearActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(jButtonClear, gridBagConstraints);

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(jButtonCancel, gridBagConstraints);

        jButtonApply.setText("Apply");
        jButtonApply.setMaximumSize(new java.awt.Dimension(65, 23));
        jButtonApply.setMinimumSize(new java.awt.Dimension(65, 23));
        jButtonApply.setPreferredSize(new java.awt.Dimension(65, 23));
        jButtonApply.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonApplyActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 4);
        add(jButtonApply, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonClearActionPerformed
    {//GEN-HEADEREND:event_jButtonClearActionPerformed
        clearFilterValues();
    }//GEN-LAST:event_jButtonClearActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCancelActionPerformed
    {//GEN-HEADEREND:event_jButtonCancelActionPerformed
        parentTable.showDataView();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void jButtonApplyActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonApplyActionPerformed
    {//GEN-HEADEREND:event_jButtonApplyActionPerformed
        parentTable.setUserDefinedFilter(new T8DataFilter(parentTable.getComponentDefinition().getDataEntityIdentifier(), getFilterCriteria()));
        parentTable.refreshData();
    }//GEN-LAST:event_jButtonApplyActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonApply;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonClear;
    private javax.swing.JLabel jLabelSpacer;
    private javax.swing.JScrollPane jScrollPaneFilterValues;
    private javax.swing.JTable jTableFilterValues;
    // End of variables declaration//GEN-END:variables
}
