package com.pilog.t8.ui.functionality.menubar;

import com.pilog.t8.definition.ui.functionality.menubar.T8FunctionalityMenuBarAPIHandler;
import com.pilog.t8.ui.menubar.T8MenuBarOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityMenuBarOperationHandler extends T8MenuBarOperationHandler
{
    private final T8FunctionalityMenuBar menuBar;

    public T8FunctionalityMenuBarOperationHandler(T8FunctionalityMenuBar menuBar)
    {
        super(menuBar);
        this.menuBar = menuBar;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8FunctionalityMenuBarAPIHandler.OPERATION_SET_DATA_OBJECT))
        {
            String objectIid;
            String objectId;

            objectId = (String)operationParameters.get(T8FunctionalityMenuBarAPIHandler.PARAMETER_DATA_OBJECT_ID);
            objectIid = (String)operationParameters.get(T8FunctionalityMenuBarAPIHandler.PARAMETER_DATA_OBJECT_IID);
            menuBar.setDataObject(objectId, objectIid);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
