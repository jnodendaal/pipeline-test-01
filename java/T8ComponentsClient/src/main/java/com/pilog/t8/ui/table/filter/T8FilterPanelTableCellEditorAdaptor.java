/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.ui.table.filter;

import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent.EditParameter;
import com.pilog.t8.ui.celleditor.T8TableCellEditorAdaptor;
import com.pilog.t8.ui.table.T8Table;
import com.pilog.t8.ui.table.T8TableFilterPanel;
import java.awt.Component;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelTableCellEditorAdaptor extends DefaultCellEditor
{
    private final T8Table sourceTable;
    private final T8TableFilterPanel editableComponent;
    private T8TableCellEditorComponent editorComponent;
    private String editorCellIdentifier;
    private int rowIndex;

    public T8FilterPanelTableCellEditorAdaptor(T8Table sourceTable, T8TableFilterPanel editableComponent)
    {
        super(new JTextField());
        this.sourceTable = sourceTable;
        this.editableComponent = editableComponent;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        TableCellEditor sourceEditor = sourceTable.getTable().getColumnModel().getColumn(row).getCellEditor();

        if(sourceEditor == null)
        {
            editorComponent = null;
            return super.getTableCellEditorComponent(table, value, isSelected, row, column);
        }
        editorComponent = ((T8TableCellEditorAdaptor)sourceEditor).getEditorComponent();

        HashMap<String, Object> dataRow;
        HashMap<EditParameter, Object> editParameters;
        TableModel tableModel;

        // Create the edit parameter list.
        editParameters = new HashMap<>();
        editParameters.put(EditParameter.SELECTED, isSelected);
        editParameters.put(EditParameter.ROW, row);
        editParameters.put(EditParameter.COLUMN, column);

        // Get the row of data from the source table.
        tableModel = table.getModel();
        dataRow = new HashMap<>();
        if(tableModel.getRowCount() > 0)
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            dataRow.put(editableComponent.getColumnDefinition(rowIndex).getFieldIdentifier(), tableModel.getValueAt(rowIndex, 2));
        }

        // Record some meta data about the cell being edited.
        rowIndex = row;
        editorCellIdentifier = editableComponent.getColumnDefinition(row).getFieldIdentifier();

        // Set the editor component content and return it.
        editorComponent.setEditorData(editableComponent, dataRow, editorCellIdentifier, editParameters);

        return (Component)editorComponent;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent)
    {
       return super.isCellEditable(anEvent);
    }

    @Override
    public Object getCellEditorValue()
    {
        if(editorComponent == null) return super.getCellEditorValue();
        Map<String, Object> editedDataRow;

        editedDataRow = editorComponent.getEditorData();
        if (editedDataRow != null)
        {
            Map<String, Object> additionalColumnData; // Columns apart from the one being edited.

            additionalColumnData = new HashMap<>(editedDataRow);
            additionalColumnData.remove(editorCellIdentifier); // Remove the column being edited.

            // First set the additional columns values on the editable component, then return the edited column value so it can also be set (automatically).
            if (additionalColumnData.size() > 0) editableComponent.cellDataEdited(additionalColumnData, rowIndex);
            return editedDataRow.get(editorCellIdentifier);
        }
        else return null;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent)
    {
        return super.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing()
    {
        return super.stopCellEditing();
    }

    @Override
    public void cancelCellEditing()
    {
        super.cancelCellEditing();
    }

    @Override
    public void addCellEditorListener(CellEditorListener l)
    {
        super.addCellEditorListener(l);
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l)
    {
        super.removeCellEditorListener(l);
    }
}
