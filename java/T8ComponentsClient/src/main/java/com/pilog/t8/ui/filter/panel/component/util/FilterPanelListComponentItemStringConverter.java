/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.filter.panel.component.util;

import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItem;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class FilterPanelListComponentItemStringConverter extends ObjectToStringConverter
{

    @Override
    public String getPreferredStringForItem(Object item)
    {
        if (item == null)
        {
            return "";
        }
        return ((T8FilterPanelListComponentItem) item).getDisplayName();
    }

}
