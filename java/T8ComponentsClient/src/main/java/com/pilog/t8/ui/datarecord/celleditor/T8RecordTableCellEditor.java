package com.pilog.t8.ui.datarecord.celleditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import java.awt.Component;
import java.awt.event.MouseListener;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * @author Bouwer du Preez
 */
public class T8RecordTableCellEditor extends AbstractCellEditor implements TableCellEditor
{
    private PropertyEditorComponent editorComponent;
    private TableCellPropertyResolver propertyResolver;
    private MouseListener mouseListener;
    private EditMode editMode;
    private boolean singleClickEdit;
    
    public enum EditMode {PROPERTY_VALUE, PROPERTY_FFT};
    
    public T8RecordTableCellEditor(MouseListener mouseListener, TableCellPropertyResolver propertyResolver)
    {
        this.mouseListener = mouseListener;
        this.propertyResolver = propertyResolver;
        this.singleClickEdit = false;
        this.editMode = EditMode.PROPERTY_VALUE;
    }
    
    public void setEditMode(EditMode mode)
    {
        editMode = mode;
    }
    
    @Override
    public boolean isCellEditable(EventObject e)
    {
        return propertyResolver.isCellEditable(e);
    }

    @Override
    public Object getCellEditorValue()
    {
        if (editorComponent != null)
        {
            // Commit editor changes and return the edited property.
            editorComponent.commitChanges();
            return editorComponent.getRecordProperty();
        }
        else return null;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        PropertyRequirement propertyRequirement;
        
        T8Log.log("Editing cell at row: " + row + ", column: " + column);
        propertyRequirement = propertyResolver.getViewCellPropertyRequirement(row, column);
        if (propertyRequirement != null)
        {
            DataRecord dataRecord;
            
            // Get the Data Record being edited.
            dataRecord = propertyResolver.getDataRecord(row);
            
            // Create the appropriate editor component depending on the display mode.
            if (editMode == EditMode.PROPERTY_VALUE)
            {
                editorComponent = new PropertyValueEditorComponent(propertyResolver, propertyRequirement);
            }
            else
            {
                editorComponent = new PropertyFFTEditorComponent(propertyResolver, propertyRequirement);
            }
            
            // Set the editor value and return it.
            editorComponent.setDataRecord(dataRecord);
            editorComponent.addMouseListener(mouseListener);
            return (Component)editorComponent;
        }
        else throw new RuntimeException("Property Requirement for column: " + column + " not found.");
    }
}
