package com.pilog.t8.ui.table;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * @author Bouwer du Preez
 */
public class T8TableRowHeaderCellEditor extends AbstractCellEditor implements TableCellEditor
{
    private final T8TableRowHeaderCellRenderer renderer;
    private T8TableRowHeader rowHeader;
    private boolean adjusting;

    public T8TableRowHeaderCellEditor(T8TableRowHeaderCellRenderer renderer)
    {
        this.renderer = renderer;
        this.renderer.addCheckBoxItemListener(new TickListener());
        this.adjusting = false;
    }

    @Override
    public Object getCellEditorValue()
    {
        rowHeader.setTicked(renderer.isCheckBoxTicked());
        return rowHeader;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        Component component;

        // Get the row header to edit.
        rowHeader = (T8TableRowHeader)value;

        // Create an editor component by using the renderer.
        adjusting = true; // Make sure to set this flag, so that programmatic tick changes won't be handled.
        component = renderer.getTableCellRendererComponent(table, value, isSelected, isSelected, row, column);
        adjusting = false; // Unset the flag, to indicate programmatic changes have finished.

        // Return the editor component.
        return component;
    }

    private class TickListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if ((!adjusting) && (rowHeader != null))
            {
                rowHeader.setTicked(e.getStateChange() == ItemEvent.SELECTED);
            }
        }
    }
}
