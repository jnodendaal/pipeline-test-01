package com.pilog.t8.ui.entitymenu;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.entitymenu.T8DataEntityMenuItemAPIHandler;
import com.pilog.t8.definition.ui.entitymenu.T8DataEntityMenuItemDefinition;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JMenuItem;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityMenuItem extends JMenuItem implements T8Component
{
    private final T8ComponentController controller;
    private final T8DataEntityMenuItemDefinition definition;
    private final T8DataEntityMenuItemOperationHandler operationHandler;
    private T8DataEntity dataEntity;

    public T8DataEntityMenuItem(T8DataEntityMenuItemDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8DataEntityMenuItemOperationHandler(this);
        this.addActionListener(new MenuItemActionListener());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8IconDefinition iconDefinition;

        iconDefinition = definition.getIconDefinition();
        if (iconDefinition != null)
        {
            this.setIcon(iconDefinition.getImage().getImageIcon());
        }

        this.setText(definition.getText());
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void setDataEntity(T8DataEntity dataEntity)
    {
        T8ExpressionEvaluator expressionEvaluator;
        Map<String, Object> expressionParameters;
        String text;
        String textExpression;
        String visibleExpression;
        String enabledExpression;

        // Set the data entity.
        this.dataEntity = dataEntity;

        // Get the settings for this menu item.
        text = definition.getText();
        textExpression = definition.getTextExpression();
        visibleExpression = definition.getVisibleExpression();
        enabledExpression = definition.getEnabledExpression();

        // Create a new expression evaluator.
        expressionEvaluator = new T8ClientExpressionEvaluator(controller.getContext());

        // Create the expression parameter map.
        expressionParameters = dataEntity.getFieldValues();

        // Evaluate the text of the menu item.
        if (!Strings.isNullOrEmpty(textExpression))
        {
            try
            {
                setText((String)expressionEvaluator.evaluateExpression(textExpression, expressionParameters, null));
            }
            catch (Exception e)
            {
                setText("Error");
                throw new RuntimeException("Exception while evaluating menu item '" + definition + "' text expression: " + textExpression, e);
            }
        }
        else
        {
            setText(text);
        }

        // Evaluate the enabled expression.
        if (!Strings.isNullOrEmpty(enabledExpression))
        {
            try
            {
                setEnabled(expressionEvaluator.evaluateBooleanExpression(enabledExpression, expressionParameters, null));
            }
            catch (Exception e)
            {
                setEnabled(true);
                throw new RuntimeException("Exception while evaluating menu item '" + definition + "' enabled expression: " + enabledExpression, e);
            }
        }

        // Evaluate the enabled expression.
        if (!Strings.isNullOrEmpty(visibleExpression))
        {
            try
            {
                setVisible(expressionEvaluator.evaluateBooleanExpression(visibleExpression, expressionParameters, null));
            }
            catch (Exception e)
            {
                setVisible(true);
                throw new RuntimeException("Exception while evaluating menu item '" + definition + "' visible expression: " + visibleExpression, e);
            }
        }
    }

    private class MenuItemActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            HashMap<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            eventParameters.put(T8DataEntityMenuItemAPIHandler.PARAMETER_MENU_ITEM_IDENTIFIER, definition.getIdentifier());
            eventParameters.put(T8DataEntityMenuItemAPIHandler.PARAMETER_DATA_ENTITY, dataEntity);
            controller.reportEvent(T8DataEntityMenuItem.this, definition.getComponentEventDefinition(T8DataEntityMenuItemAPIHandler.EVENT_MENU_ITEM_SELECTED), eventParameters);
        }
    }
}
