package com.pilog.t8.ui.entityeditor.concepttypelabel;

import com.pilog.t8.ui.label.T8LabelOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptTypeLabelDataEntityOperationHandler extends T8LabelOperationHandler
{
    private T8ConceptTypeLabelDataEntityEditor conceptLabel;
    
    public T8ConceptTypeLabelDataEntityOperationHandler(T8ConceptTypeLabelDataEntityEditor label)
    {
        super(label);
        this.conceptLabel = label;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return super.executeOperation(operationIdentifier, operationParameters);
    }
}
