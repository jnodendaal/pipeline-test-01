package com.pilog.t8.ui.panel;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition.BorderType;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import org.jdesktop.swingx.border.DropShadowBorder;

/**
 * @author Bouwer du Preez
 */
public class T8PanelSlot extends JComponent implements T8Component
{
    private final T8PanelSlotDefinition definition;
    private final T8ComponentController controller;
    private T8Component childComponent;
    private final T8PanelSlotOperationHandler operationHandler;
    private T8ContentHeaderBorder contentHeaderBorder;
    private ActionListener borderActionListener;

    private GridBagConstraints slotConstraints;

    public T8PanelSlot(T8PanelSlotDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.childComponent = null;
        this.operationHandler = new T8PanelSlotOperationHandler(this);
        this.setOpaque(false);
        initComponents();
        initConstraints();
    }

    private void initConstraints()
    {
        slotConstraints = new GridBagConstraints();
        slotConstraints.gridx = 0;
        slotConstraints.gridy = 0;
        slotConstraints.fill = GridBagConstraints.BOTH;
        slotConstraints.insets = new Insets(definition.getPaddingY(), definition.getPaddingX(), definition.getPaddingY(), definition.getPaddingX());
        slotConstraints.weightx = 0.1;
        slotConstraints.weighty = 0.1;
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        BorderType borderType;

        borderType = definition.getBorderType();
        if (borderType == BorderType.BEVEL)
        {
            setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        }
        else if (borderType == BorderType.ETCHED)
        {
            setBorder(javax.swing.BorderFactory.createEtchedBorder());
        }
        else if (borderType == BorderType.LINE)
        {
            setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        }
        else if (borderType == BorderType.TITLED || borderType == BorderType.CONTENT)
        {
            setTitle(definition.getTitle());
        }
        else if (borderType == BorderType.SHADOW)
        {
            setBorder(new DropShadowBorder());
        }
        else
        {
            setBorder(new EmptyBorder(0, 0, 0, 0));
        }
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    public void setSlotComponent(String componentIdentifier)
    {
        T8Component component;
        
        component = controller.findComponent(componentIdentifier);
        if (component != null)
        {
            addChildComponent(component, null);
            revalidate();
        }
        else throw new RuntimeException("Cannot find component to set as new slot content: " + componentIdentifier);
    }
    
    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        removeAll();
        childComponent = component;
        add((Component)childComponent, slotConstraints);
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        return childComponent;
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        if (childComponent != null)
        {
            if (childComponent.getComponentDefinition().getIdentifier().equals(identifier))
            {
                T8Component removedComponent;

                removedComponent = childComponent;
                removeAll();
                childComponent = null;
                return removedComponent;
            }
            else return null;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        ArrayList<T8Component> childComponents;

        childComponents = new ArrayList<>();
        if (childComponent != null) childComponents.add(childComponent);
        return childComponents;
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponent != null ? 1 : 0;
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
        childComponent = null;
    }

    public void setTitle(String title)
    {
        switch (definition.getBorderType())
        {
            case TITLED:
                setBorder(javax.swing.BorderFactory.createTitledBorder(title));
                break;
            case CONTENT:
                contentHeaderBorder = new T8ContentHeaderBorder(this, title);
                contentHeaderBorder.setCollapsible(definition.isCollapsible());
                borderActionListener = new BorderActionListener();
                contentHeaderBorder.addActionListener(borderActionListener);
                setBorder(contentHeaderBorder);
                break;
        }
    }
    
    private void toggleCollapsedState()
    {
        if (definition.isCollapsible())
        {
            if (childComponent != null)
            {
                Component contentComponent;
                
                contentComponent = ((Component)childComponent);
                contentComponent.setVisible(!contentComponent.isVisible());
                contentHeaderBorder.setCollapsed(!contentComponent.isVisible());
            }
        }
    }

    private class BorderActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            toggleCollapsedState();
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
