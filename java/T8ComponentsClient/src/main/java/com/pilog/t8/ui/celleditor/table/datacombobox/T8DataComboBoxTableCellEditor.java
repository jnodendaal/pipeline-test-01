package com.pilog.t8.ui.celleditor.table.datacombobox;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellEditableComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent.EditParameter;
import com.pilog.t8.definition.ui.celleditor.table.datacombobox.T8DataComboBoxTableCellEditorDefinition;
import com.pilog.t8.ui.datacombobox.T8DataComboBox;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBoxTableCellEditor extends T8DataComboBox implements T8TableCellEditorComponent
{
    private T8DataComboBoxTableCellEditorDefinition definition;
    private T8ComponentController controller;
    private String editorColumnIdentifier;
    
    public T8DataComboBoxTableCellEditor(T8DataComboBoxTableCellEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.controller = controller;
        this.definition = definition;
    }
    
    @Override
    public void setEditorData(T8TableCellEditableComponent sourceComponent, Map<String, Object> dataRow, String editorColumnIdentifier, Map<EditParameter, Object> editParameters)
    {
        Map<String, String> inputFieldMapping;
        Map<String, Object> inputData;
        
        inputFieldMapping = definition.getEditorInputFieldMapping();
        inputData = T8IdentifierUtilities.mapParameters(dataRow, inputFieldMapping);
        setSelectedKey(inputData);
    }

    @Override
    public Map<String, Object> getEditorData()
    {
        T8DataEntity selectedEntity;
        
        selectedEntity = getSelectedDataEntity();
        if (selectedEntity != null)
        {
            Map<String, String> outputFieldMapping;

            outputFieldMapping = definition.getEditorOutputFieldMapping();
            return T8IdentifierUtilities.mapParameters(selectedEntity.getFieldValues(), outputFieldMapping);
        }
        else return null;
    }

    @Override
    public boolean singleClickEdit()
    {
        return false;
    }
}
