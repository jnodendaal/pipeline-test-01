package com.pilog.t8.ui.cellrenderer.table.identifier;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellRenderableComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent.RenderParameter;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.cellrenderer.table.identifier.T8DefinitionIdentifierTableCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.cache.LRUCache;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.border.Border;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionIdentifierTableCellRenderer extends JLabel implements T8TableCellRendererComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefinitionIdentifierTableCellRenderer.class);

    private final T8DefinitionIdentifierTableCellRendererDefinition definition;
    private final T8DefinitionManager definitionManager;
    private final T8ComponentController controller;
    private final T8Context context;

    private ExpressionEvaluator namespaceExpressionEvaluator;
    private ExpressionEvaluator valueExpressionEvaluator;
    private LRUCache<String, T8Definition> definitionCache;
    private boolean definitionsInitialized;
    private boolean definitionGroupsInitialized;

    //These will be used to lock the calling thread if the definitions are still loading when start component is called
    private final Object definitionLock;
    private final Object definitionGroupLock;

    public T8DefinitionIdentifierTableCellRenderer(T8DefinitionIdentifierTableCellRendererDefinition definition, T8ComponentController controller)
    {
        this.definitionLock = new Object();
        this.definitionGroupLock = new Object();
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.definitionManager = controller.getClientContext().getDefinitionManager();
        this.setOpaque(true);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String string, Map<String, Object> map)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> map)
    {
        String valueExpression;
        String namespaceExpression;
        List<String> cacheDefinitionIdentifiers;
        List<String> cacheDefinitionGroupIdentifiers;
        int cacheSize;

        // Get the cache size and create it.
        cacheSize = definition.getDefinitionCacheSize();
        definitionCache = new LRUCache<>(cacheSize);

        // If a cache group is specified, cache it.
        cacheDefinitionIdentifiers = definition.getCacheDefinitionIdentifiers();
        if ((cacheDefinitionIdentifiers != null) && (cacheDefinitionIdentifiers.size() > 0))
        {
            cacheDefinitions(cacheDefinitionIdentifiers);
        } else definitionsInitialized = true;

        // If a cache group is specified, cache it.
        cacheDefinitionGroupIdentifiers = definition.getCacheDefinitionGroupIdentifiers();
        if ((cacheDefinitionGroupIdentifiers != null) && (cacheDefinitionGroupIdentifiers.size() > 0))
        {
            cacheDefinitionGroups(cacheDefinitionGroupIdentifiers);
        } else definitionGroupsInitialized = true;

        // Get the namespace expression and compile it.
        namespaceExpression = definition.getNamespaceExpression();
        if (!Strings.isNullOrEmpty(namespaceExpression))
        {
            try
            {
                namespaceExpressionEvaluator = new ExpressionEvaluator();
                namespaceExpressionEvaluator.compileExpression(namespaceExpression);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while compiling display namespace expression in renderer: " + definition, e);
                namespaceExpressionEvaluator = null;
            }
        }

        // Get the value expression and compile it.
        valueExpression = definition.getDisplayValueExpression();
        if (!Strings.isNullOrEmpty(valueExpression))
        {
            try
            {
                valueExpressionEvaluator = new ExpressionEvaluator();
                valueExpressionEvaluator.compileExpression(valueExpression);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while compiling display value expression in renderer: " + definition, e);
                valueExpressionEvaluator = null;
            }
        }
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
        synchronized(definitionLock)
        {
            if (!definitionsInitialized)
            {
                try
                {
                    LOGGER.log(String.format("Waiting for %1$s to finish initializing.", definition.getIdentifier()));
                    definitionLock.wait();
                }
                catch (InterruptedException ex)
                {
                    LOGGER.log("Interupted while wating for definitions to get loaded.", ex);
                }
            }
        }
        synchronized(definitionGroupLock)
        {
            if (!definitionGroupsInitialized)
            {
                try
                {
                    LOGGER.log(String.format("Waiting for %1s to finish initializing.", definition.getIdentifier()));
                    definitionGroupLock.wait();
                }
                catch (InterruptedException ex)
                {
                    LOGGER.log("Interupted while wating for definitions groups to get loaded.", ex);
                }
            }
        }
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public void setRendererData(T8TableCellRenderableComponent renderableComponent, Map<String, Object> dataRow, String rendererFieldIdentifier, Map<RenderParameter, Object> renderParameters)
    {
        Map<String, Object> expressionParameters;
        Color foregroundColor;
        Color backgroundColor;
        Border cellBorder;
        Font cellFont;
        boolean isSelected;
        boolean hasFocus;
        String value;
        String namespace;

        // Get the namespace of the value in the cell.
        if (namespaceExpressionEvaluator != null)
        {
            try
            {
                Object result;

                result = namespaceExpressionEvaluator.evaluateExpression(dataRow, null);
                namespace = result != null ? result.toString() : null;
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while evaluating display value expression in renderer: " + definition, e);
                namespace = null;
            }
        }
        else namespace = null;

        // Get the display value of the cell.
        try
        {
            String identifier;

            identifier = (String)dataRow.get(rendererFieldIdentifier);
            if (identifier != null)
            {
                //This will probably never happen, but lets check any way in case the locked thread was interrupted
                if(definitionsInitialized && definitionGroupsInitialized)
                {
                    T8Definition rendererDefinition;

                    rendererDefinition = loadDefinition(namespace != null ? (namespace + identifier) : identifier);
                    if (rendererDefinition != null)
                    {
                        Object result;

                        expressionParameters = rendererDefinition.getDefinitionDatums();
                        expressionParameters.put("IDENTIFIER", rendererDefinition.getIdentifier());
                        expressionParameters.put("DISPLAY_NAME", rendererDefinition.getMetaDisplayName());
                        result = valueExpressionEvaluator.evaluateExpression(expressionParameters, null);
                        value = result != null ? result.toString() : null;
                    }
                    else value = identifier;
                }
                else value = identifier;
            }
            else value = null;
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while evaluating display value expression in renderer: " + definition, e);
            value = "Error";
        }

        // Get the selected and focused parameters.
        isSelected = Boolean.TRUE.equals(renderParameters.get(RenderParameter.SELECTED));
        hasFocus = Boolean.TRUE.equals(renderParameters.get(RenderParameter.FOCUSED));

        // Get the default formatting values.
        cellFont = renderableComponent.getCellFont();
        if (isSelected)
        {
            foregroundColor = renderableComponent.getSelectedCellForegroundColor();
            backgroundColor = renderableComponent.getSelectedCellBackgroundColor();
            cellBorder = hasFocus ? renderableComponent.getFocusedSelectedBorder() : renderableComponent.getSelectedBorder();
        }
        else
        {
            foregroundColor = renderableComponent.getUnselectedCellForegroundColor();
            backgroundColor = renderableComponent.getUnselectedCellBackgroundColor();
            cellBorder = hasFocus ? renderableComponent.getFocusedBorder() : renderableComponent.getUnselectedBorder();
        }

        // Set the formatting.
        setFont(cellFont);
        setForeground(foregroundColor);
        setBackground(backgroundColor);
        setBorder(cellBorder);

        // Set the value of the cell on the label.
        setText(value);
    }

    private T8Definition loadDefinition(String identifier)
    {
        String globalId;
        String localIdentifier;
        T8Definition requestedDefinition;

        // Get the global definition we are looking for.
        globalId = T8IdentifierUtilities.getGlobalIdentifierPart(identifier);
        requestedDefinition = definitionCache.get(globalId);
        if (requestedDefinition == null)
        {
            try
            {
                requestedDefinition = definitionManager.getInitializedDefinition(context, definition.getRootProjectId(), globalId, null);
                definitionCache.put(globalId, requestedDefinition);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while trying to cache definition: " + globalId, e);
                return null;
            }
        }

        // If the definition we are looking for is a local one, fetch it.
        localIdentifier = T8IdentifierUtilities.getLocalIdentifierPart(identifier);
        if (localIdentifier != null)
        {
            return requestedDefinition.getLocalDefinition(localIdentifier);
        }
        else return requestedDefinition;
    }

    private void cacheDefinitions(final List<String> definitionIds)
    {
        definitionsInitialized = false;
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                for (String definitionId : definitionIds)
                {
                    try
                    {
                        T8Definition loadedDefinition;

                        loadedDefinition = definitionManager.getInitializedDefinition(context, definition.getRootProjectId(), definitionId, null);
                        if (loadedDefinition != null)
                        {
                            definitionCache.put(definitionId, loadedDefinition);
                        }
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while caching definition group: " + definitionId, e);
                    }
                    synchronized(definitionLock)
                    {
                        definitionsInitialized = true;
                        definitionLock.notifyAll();
                    }
                }
            }
        }).start();
    }

    private void cacheDefinitionGroups(final List<String> groupIds)
    {
        definitionGroupsInitialized = false;
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                for (String groupId : groupIds)
                {
                    try
                    {
                        List<T8Definition> definitions;

                        definitions = definitionManager.getInitializedGroupDefinitions(context, definition.getRootProjectId(), groupId, null);
                        if (definitions != null)
                        {
                            for (T8Definition retrievedDefinition : definitions)
                            {
                                definitionCache.put(retrievedDefinition.getIdentifier(), retrievedDefinition);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while caching definition group: " + groupId, e);
                    }
                    synchronized(definitionGroupLock)
                    {
                        definitionGroupsInitialized = true;
                        definitionGroupLock.notifyAll();
                    }
                }
            }
        }).start();
    }

    @Override
    public Object getAdditionalRenderData(Map<String, Object> dataRow, String fieldIdentifier)
    {
        return null;
    }
}
