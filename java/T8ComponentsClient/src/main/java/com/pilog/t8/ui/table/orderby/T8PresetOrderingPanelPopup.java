package com.pilog.t8.ui.table.orderby;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.ui.table.T8Table;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LinearGradientPaint;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.PropertySetter;
import org.jdesktop.core.animation.timing.TimingTargetAdapter;
import org.jdesktop.core.animation.timing.interpolators.AccelerationInterpolator;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.border.DropShadowBorder;
import org.jdesktop.swingx.painter.MattePainter;

/**
 * @author Hennie Brink
 */
public class T8PresetOrderingPanelPopup extends JWindow
{
    protected final T8PresetOrderingPanel orderingPanel;
    private final T8Table table;

    public T8PresetOrderingPanelPopup(T8Table table,
                                      Map<String, T8DataFilter.OrderMethod> existingOrdering)
    {
        super(SwingUtilities.windowForComponent(table));
        this.orderingPanel = new T8PresetOrderingPanel(table.getComponentDefinition(), existingOrdering);
        this.table = table;

        createUI();

        setLocationRelativeTo(table);

        setFocusable(true);
        setFocusableWindowState(true);
        setAutoRequestFocus(true);

        PopupWindowListener listener = new PopupWindowListener(this);

        addWindowListener(listener);
        addWindowFocusListener(listener);
    }

    private void createUI()
    {
        JXPanel contentPanel;

        contentPanel = new JXPanel(new GridBagLayout());

        contentPanel.setBorder(new DropShadowBorder());

        setContentPane(contentPanel);

        JScrollPane jScrollPane = new JScrollPane(orderingPanel);
        jScrollPane.setOpaque(false);
        jScrollPane.getViewport().setOpaque(false);
        jScrollPane.getVerticalScrollBar().setUnitIncrement(25);

        add(jScrollPane, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

        JXButton applyButton;
        JXButton resetButton;
        JXPanel buttonPanel;

        applyButton = new JXButton(table.getTranslatedString("Apply"));
        applyButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                table.setFieldOrdering(orderingPanel.getNewOrdering());
                close();
            }
        });
        resetButton = new JXButton(table.getTranslatedString("Clear"));
        resetButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                orderingPanel.clearOrdering();
            }
        });
        buttonPanel = new JXPanel();
        buttonPanel.setOpaque(false);
        buttonPanel.add(resetButton);
        buttonPanel.add(applyButton);

        add(buttonPanel, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

        Dimension preferredSize = getPreferredSize();

        if (table.getPreferredSize().height < preferredSize.height)
        {
            preferredSize.height = table.getPreferredSize().height;
        }
        preferredSize.width = contentPanel.getPreferredScrollableViewportSize().width + 50;
        setPreferredSize(preferredSize);

        pack();

        contentPanel.setBackgroundPainter(new MattePainter(new LinearGradientPaint(getWidth() / 2, 0f, getWidth() / 2, getHeight(), new float[]
        {
            0f, 0.5f, 1f
        }, new Color[]
        {
            getBackground(), Color.WHITE, getBackground()
        })));
    }

    public static void showOrderingPopup(JComponent caller,
                                         T8Table table,
                                         Map<String, T8DataFilter.OrderMethod> existingOrdering)
    {
        T8PresetOrderingPanelPopup orderingPanelPopup;

        orderingPanelPopup = new T8PresetOrderingPanelPopup(table, existingOrdering);

        Point callerLocation = caller.getLocationOnScreen();
        callerLocation.y += caller.getHeight();

        orderingPanelPopup.setLocation(callerLocation);

        Dimension endSize = orderingPanelPopup.getSize();
        orderingPanelPopup.setSize((int) (endSize.getWidth() * 0.3), (int) (endSize.getHeight() * 0.3));
        orderingPanelPopup.setOpacity(0.0f);

        orderingPanelPopup.setVisible(true);

        new Animator.Builder()
                .addTarget(PropertySetter.getTargetTo(orderingPanelPopup, "size", endSize))
                .addTarget(PropertySetter.getTargetTo(orderingPanelPopup, "opacity", 1.0f))
                .setDuration(200, TimeUnit.MILLISECONDS)
                .setInterpolator(new AccelerationInterpolator(0.3f, 0.2f))
                .build()
                .start();
    }

    private void close()
    {
        new Animator.Builder()
                .addTarget(PropertySetter.getTargetTo(this, "size", new Dimension(0, 0)))
                .addTarget(PropertySetter.getTargetTo(this, "opacity", 0.0f))
                .addTarget(new TimingTargetAdapter()
                        {

                            @Override
                            public void end(Animator source)
                            {
                                dispose();
                            }

                })
                .setDuration(200, TimeUnit.MILLISECONDS)
                .setInterpolator(new AccelerationInterpolator(0.3f, 0.2f))
                .build()
                .start();
    }

    private class PopupWindowListener extends WindowAdapter
    {
        private PopupWindowListener(T8PresetOrderingPanelPopup orderingPanel)
        {
        }

        @Override
        public void windowLostFocus(final WindowEvent e)
        {
            close();
        }
    }
}
