package com.pilog.t8.ui.table;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.table.AbstractTableModel;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultTableModel extends AbstractTableModel
{
    private final T8TableDefinition tableDefinition;
    private final List<T8TableColumnDefinition> columnDefinitions;
    private final int pageOffset;
    private final ExpressionEvaluator expressionEvaluator;
    private final List<ValueChangeListener> changeListeners;
    private List<T8RowTickListener> rowTickListeners;
    private List<DataRow> modelData;
    private boolean programmaticTickSelection;

    public T8DefaultTableModel(T8TableDefinition tableDefinition, int pageOffset)
    {
        this.tableDefinition = tableDefinition;
        this.columnDefinitions = new ArrayList<>(tableDefinition.getColumnDefinitions());
        this.changeListeners = new ArrayList<>();
        this.rowTickListeners = new ArrayList<>();
        this.modelData = new ArrayList<>();
        this.pageOffset = pageOffset;
        this.programmaticTickSelection = false;
        addRowHeaderColumn();
        this.expressionEvaluator = new ExpressionEvaluator();
    }

    public T8DefaultTableModel(T8TableDefinition tableDefinition, List<T8DataEntity> modelData, int pageOffset)
    {
        this(tableDefinition, pageOffset);
        addDataEntities(modelData, 0);
    }

    public void addRowTickListener(T8RowTickListener listener)
    {
        this.rowTickListeners.add(listener);
    }

    public void removeRowTickListener(T8RowTickListener listener)
    {
        this.rowTickListeners.remove(listener);
    }

    private void addRowHeaderColumn()
    {
        T8TableColumnDefinition rowHeaderColumnDefinition;

        rowHeaderColumnDefinition = new T8TableColumnDefinition(T8Table.ROW_HEADER_COLUMN_NAME);
        rowHeaderColumnDefinition.setColumnName(T8Table.ROW_HEADER_COLUMN_NAME);
        rowHeaderColumnDefinition.setEditable(tableDefinition.isTickSelectionEnabled());
        columnDefinitions.add(0, rowHeaderColumnDefinition);
    }

    public void clear()
    {
        while (getRowCount() > 0)
        {
            removeRow(0);
        }
    }

    public void addDataEntity(T8DataEntity entity)
    {
        addDataEntity(entity, this.modelData.size());
    }

    public void addDataEntity(T8DataEntity entity, int row)
    {
        addDataEntities(ArrayLists.newArrayList(entity), row);
    }

    public final void addDataEntities(List<T8DataEntity> entities, int startRow)
    {
        if (entities.size() > 0)
        {
            for (int i = 0; i < entities.size(); i++)
            {
                T8TableRowHeader rowHeader;
                T8DataEntity entity;

                entity = entities.get(i);
                rowHeader = new T8TableRowHeader(this, entity, this.pageOffset + i);
                this.modelData.add(i, new DataRow(entity, rowHeader));
            }

            fireTableRowsInserted(startRow, startRow + entities.size() - 1);
        }
    }

    /**
     * Gets the data entity at the specified row index. Row index range is set
     * at [0 - ({@code getRowCount()}-1)].
     *
     * @param row The row index
     *
     * @return The {@code T8DataEntity} at the specified row index
     */
    public T8DataEntity getDataEntity(int row)
    {
        checkRowValid(row);
        T8DataEntity entity = this.modelData.get(row).getRowEntity();
        if(entity == null) throw new NullPointerException("Null entity found at row index " + row);
        return entity;
    }

    public T8DataEntity getDataEntity(String guid)
    {
        for (DataRow dataRow : modelData)
        {
            if (dataRow.getRowEntity().getGUID().equals(guid))
            {
                return dataRow.getRowEntity();
            }
        }

        return null;
    }

    public List<T8DataEntity> getTickedDataEntities()
    {
        List<T8DataEntity> tickedEntities;

        tickedEntities = new ArrayList<>();
        for (DataRow row : modelData)
        {
            if (row.getRowHeader().isTicked())
            {
                tickedEntities.add(row.getRowEntity());
            }
        }

        return tickedEntities;
    }

    public boolean areAllRowsTicked()
    {
        for (DataRow row : modelData)
        {
            if (!row.getRowHeader().isTicked())
            {
                return false;
            }
        }

        return true;
    }

    public void setAllRowsTicked(boolean ticked)
    {
        List<T8TableRowHeader> rowHeaders;

        rowHeaders = new ArrayList<>();
        programmaticTickSelection = true;
        for (DataRow row : modelData)
        {
            T8TableRowHeader rowHeader;

            rowHeader = row.getRowHeader();
            rowHeader.setTicked(ticked);
            rowHeaders.add(rowHeader);
        }
        programmaticTickSelection = false;
        rowHeaderTickSelectionChanged(rowHeaders);
    }

    void applySingleTickSelection(T8TableRowHeader newRowSelection)
    {
        T8TableRowHeader rowHeader;

        programmaticTickSelection = true;
        for (DataRow row : modelData)
        {
            rowHeader = row.getRowHeader();

            // We have found the row we expect to be ticked
            if (rowHeader == newRowSelection) continue;

            // We only care if the header is ticked
            if (rowHeader.isTicked()) rowHeader.setTicked(false);
        }
        programmaticTickSelection = false;
    }

    public int getRowIndex(T8DataEntity entity)
    {
        for (int rowIndex = 0; rowIndex < modelData.size(); rowIndex++)
        {
            T8DataEntity modelEntity;

            modelEntity = modelData.get(rowIndex).getRowEntity();
            if (modelEntity == entity)
            {
                return rowIndex;
            }
        }

        return -1;
    }

    public void removeRow(int row)
    {
        checkRowValid(row);
        modelData.remove(row);
        fireTableRowsDeleted(row, row);
    }

    @Override
    public boolean isCellEditable(int row, int column)
    {
        T8TableColumnDefinition columnDefinition;

        columnDefinition = getColumnDefinition(column);
        if (columnDefinition.isEditable())
        {
            // If this cell is editable, and no editable expression is defined, return true.
            if (Strings.isNullOrEmpty(columnDefinition.getCellEditableExpression())) return true;

            try
            {
                //The cell editable expression needs to be evaluated to determine editability
                T8DataEntity dataEntity;

                dataEntity = getDataEntity(row);
                if(dataEntity == null) return false;

                return this.expressionEvaluator.evaluateBooleanExpression(columnDefinition.getCellEditableExpression(), dataEntity.getFieldValues(), null);
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to evaluate expression", ex);
                return false;
            }
        }
        else return false;
    }

    @Override
    public int getRowCount()
    {
        return modelData.size();
    }

    @Override
    public int getColumnCount()
    {
        return columnDefinitions.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        checkRowValid(rowIndex);
        //Firstyl check if the column index is 0, if it is then we should return the row header
        if(columnIndex == 0) return this.modelData.get(rowIndex).getRowHeader();

        T8DataEntity entity = getDataEntity(rowIndex);
        T8TableColumnDefinition column = getColumnDefinition(columnIndex);

        return entity.getFieldValue(column.getFieldIdentifier());
    }

    @Override
    public int findColumn(String columnName)
    {
        for (int i = 0; i < columnDefinitions.size(); i++)
        {
            T8TableColumnDefinition t8TableColumnDefinition = columnDefinitions.get(i);
            //If the column is found return its index + 1 so that we exclude the row header column
            if(t8TableColumnDefinition.getIdentifier().equals(columnName)) return i + 1;
        }

        //No columns found so return -1
        return -1;
    }

    @Override
    public String getColumnName(int column)
    {
        checkColumnValid(column);
        T8TableColumnDefinition columnDefinition = getColumnDefinition(column);

        //Return the field identifier becuase it is only the code that will use this method and it expects a field identifier in return
        return columnDefinition.getFieldIdentifier();
    }

    @Override
    public void setValueAt(Object newValue, int rowIndex, int columnIndex)
    {
        checkRowValid(rowIndex);
        if (columnIndex == 0)
        {
            this.modelData.get(rowIndex).setRowHeader((T8TableRowHeader) newValue);
        }
        else
        {
            T8TableColumnDefinition column;
            T8DataEntity entity;
            Object oldValue;
            String fieldId;

            entity = getDataEntity(rowIndex);
            column = getColumnDefinition(columnIndex);
            fieldId = column.getFieldIdentifier();
            oldValue = entity.getFieldValue(fieldId);
            entity.setFieldValue(fieldId, newValue);
            fireValueUpdated(rowIndex, columnIndex, oldValue, newValue);
            fireTableCellUpdated(rowIndex, columnIndex);
        }
    }

    private void fireValueUpdated(int rowIndex, int columnIndex, Object oldValue, Object newValue)
    {
        ValueChangedEvent event;

        event = new ValueChangedEvent(this, rowIndex, columnIndex, oldValue, newValue);
        for (ValueChangeListener valueChangeListener : changeListeners)
        {
            valueChangeListener.valueChanged(event);
        }
    }

    public void addValueChangeListener(ValueChangeListener listener)
    {
        this.changeListeners.add(listener);
    }

    public void removeValueChangeListener(ValueChangeListener listener)
    {
        this.changeListeners.remove(listener);
    }

    public void setAdditionalRenderData(int row, String fieldIdentifier, Object data)
    {
        DataRow requiredRow;

        checkRowValid(row);

        requiredRow = modelData.get(row);
        requiredRow.setAdditionalRenderData(fieldIdentifier, data);

        fireTableRowsUpdated(row, row);
    }

    public Object getAdditionalRenderData(int row, String fieldIdentifier)
    {
        DataRow requiredRow;

        checkRowValid(row);

        requiredRow = modelData.get(row);

        return requiredRow.getAdditionalRenderData(fieldIdentifier);
    }

    private void checkColumnValid(int column)
    {
        if (column < 0 || column >= this.columnDefinitions.size()) throw new IllegalArgumentException("Invalid column specified: " + column);
    }

    private void checkRowValid(int row)
    {
        if (row < 0 || row >= this.modelData.size()) throw new IllegalArgumentException("Invalid row specified: " + row);
    }

    private T8TableColumnDefinition getColumnDefinition(int column)
    {
        checkColumnValid(column);
        T8TableColumnDefinition columnDefinition = this.columnDefinitions.get(column);
        if (columnDefinition == null) throw new NullPointerException("Null column found at index " + column);
        return columnDefinition;
    }

    void rowHeaderTickSelectionChanged(T8TableRowHeader header)
    {
        rowHeaderTickSelectionChanged(ArrayLists.typeSafeList(header));
    }

    void rowHeaderTickSelectionChanged(List<T8TableRowHeader> headers)
    {
        if (!programmaticTickSelection)
        {
            T8RowTickSelectionChangedEvent event;

            event = new T8RowTickSelectionChangedEvent(headers);
            for (T8RowTickListener listener : rowTickListeners)
            {
                listener.tickSelectionChanged(event);
            }
        }
    }

    public interface ValueChangeListener
    {
        public void valueChanged(ValueChangedEvent event);
    }

    public static class ValueChangedEvent extends EventObject
    {
        private int rowIndex;
        private int columnIndex;
        private Object oldValue;
        private Object newValue;

        public ValueChangedEvent(T8DefaultTableModel model, int rowIndex, int columnIndex, Object oldValue, Object newValue)
        {
            super(model);
            this.rowIndex = rowIndex;
            this.columnIndex = columnIndex;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        public int getRowIndex()
        {
            return rowIndex;
        }

        public int getColumnIndex()
        {
            return columnIndex;
        }

        public Object getOldValue()
        {
            return oldValue;
        }

        public Object getNewValue()
        {
            return newValue;
        }
    }

    private static class DataRow
    {
        private T8DataEntity rowEntity;
        private T8DataEntity oldEntity;
        private T8TableRowHeader rowHeader;
        private Map<String, Object> additionalRenderData;

        DataRow()
        {
            this.additionalRenderData = new HashMap<>();
        }

        DataRow(T8DataEntity rowEntity, T8TableRowHeader rowHeader)
        {
            this();
            this.rowEntity = rowEntity;
            this.rowHeader = rowHeader;
        }

        public T8DataEntity getRowEntity()
        {
            return rowEntity;
        }

        public void setRowEntity(T8DataEntity rowEntity)
        {
            this.rowEntity = rowEntity;
        }

        public T8DataEntity getOldEntity()
        {
            return oldEntity;
        }

        public void setOldEntity(T8DataEntity oldEntity)
        {
            this.oldEntity = oldEntity;
        }

        public T8TableRowHeader getRowHeader()
        {
            return rowHeader;
        }

        public void setRowHeader(T8TableRowHeader rowHeader)
        {
            this.rowHeader = rowHeader;
        }

        public void setAdditionalRenderData(String fieldIdentifier, Object data)
        {
            this.additionalRenderData.put(fieldIdentifier, data);
        }

        public Object getAdditionalRenderData(String fieldIdentifier)
        {
            return this.additionalRenderData.get(fieldIdentifier);
        }
    }
}