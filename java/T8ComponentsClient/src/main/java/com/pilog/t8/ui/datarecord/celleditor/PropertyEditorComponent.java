package com.pilog.t8.ui.datarecord.celleditor;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import java.awt.event.MouseListener;

/**
 * @author Bouwer du Preez
 */
public interface PropertyEditorComponent
{
    public void commitChanges();
    public void setDataRecord(DataRecord dataRecord);
    public DataRecord getDataRecord();
    public RecordProperty getRecordProperty();
    public void addMouseListener(MouseListener listener);
}
