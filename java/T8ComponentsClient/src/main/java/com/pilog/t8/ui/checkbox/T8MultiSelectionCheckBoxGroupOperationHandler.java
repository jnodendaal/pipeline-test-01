/**
 * Created on 10 Sep 2015, 11:49:51 AM
 */
package com.pilog.t8.ui.checkbox;

import static com.pilog.t8.definition.ui.checkbox.T8MultiSelectionCheckBoxGroupAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8MultiSelectionCheckBoxGroupOperationHandler extends T8ComponentOperationHandler
{
    private final T8MultiSelectionCheckBoxGroup checkboxGroup;

    T8MultiSelectionCheckBoxGroupOperationHandler(T8MultiSelectionCheckBoxGroup checkboxGroup)
    {
        super(checkboxGroup);
        this.checkboxGroup = checkboxGroup;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        switch (operationIdentifier)
        {
            case OPERATION_ENABLE_GROUP:
                this.checkboxGroup.setEnabled(true);
                break;
            case OPERATION_DISABLE_GROUP:
                this.checkboxGroup.setEnabled(false);
                break;
            case OPERATION_SET_SELECTED:
                this.checkboxGroup.setSelected((List<String>)operationParameters.get(PARAMETER_SELECTED_IDENTIFIER_LIST));
                break;
            case OPERATION_GET_SELECTED:
                return HashMaps.createSingular(PARAMETER_SELECTED_IDENTIFIER_LIST, this.checkboxGroup.getSelectedIdentifiers());
            default:
                return super.executeOperation(operationIdentifier, operationParameters);
        }

        return null;
    }
}