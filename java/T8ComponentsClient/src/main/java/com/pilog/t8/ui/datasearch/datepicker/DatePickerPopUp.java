package com.pilog.t8.ui.datasearch.datepicker;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.components.datetimepicker.JXDateTimePicker;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.Point;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.Date;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.MattePainter;

/**
 * @author Hennie Brink
 */
public class DatePickerPopUp extends JXPanel
{
    private final T8DataSearchDatePicker owner;
    private final FocusListenerPanel focusListenerPanel;

    private JLayeredPane popupLayeredPane;
    private RadioButtonListener activeButton;

    public DatePickerPopUp(T8DataSearchDatePicker owner)
    {
        this.owner = owner;

        this.focusListenerPanel = new FocusListenerPanel();
        this.setLayout(new GridBagLayout());

        initialize();
    }

    private void initialize()
    {
        ButtonGroup radioButtonGroup;
        JRadioButton noFilterButton;

        radioButtonGroup = new ButtonGroup();
        noFilterButton = new JRadioButton(owner.getTranslation("Any Date"));
        radioButtonGroup.add(noFilterButton);

        noFilterButton.setOpaque(false);
        noFilterButton.setSelected(true);
        noFilterButton.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    activeButton = null;
                }
            }
        });

        add(noFilterButton, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 0, 5, 5), 0, 0));

        addDatePickerPane(radioButtonGroup, owner.getTranslation("On This Date"), 1, FilterType.ON);
        addDatePickerPane(radioButtonGroup, owner.getTranslation("Before This Date"), 2, FilterType.BEFORE);
        addDatePickerPane(radioButtonGroup, owner.getTranslation("After This Date"), 3, FilterType.AFTER);
        addDatePickerPane(radioButtonGroup, owner.getTranslation("Between These Dates"), 4, FilterType.BETWEEN);

        setBorder(new EtchedBorder());
    }

    protected void startComponent()
    {
        JLayeredPane layeredPane;

         if(popupLayeredPane != null)
         {
             stopComponent();
             //Make sure that if the main layered pane is not null that all previous refereneces will be removed from the previous layered pane
         }

        layeredPane = JLayeredPane.getLayeredPaneAbove(owner);
        layeredPane.add(focusListenerPanel, new Integer(JLayeredPane.POPUP_LAYER - 1));
        layeredPane.add(this, JLayeredPane.POPUP_LAYER);

        popupLayeredPane = layeredPane;

        setVisible(false);
    }

    protected void stopComponent()
    {
        JLayeredPane layeredPane;

        layeredPane = popupLayeredPane;
        layeredPane.remove(focusListenerPanel);
        layeredPane.remove(this);

        popupLayeredPane = null;

        setVisible(false);
    }

    public T8DataFilterClause createFilterFieldClause(String fieldIdentifier)
    {
        if(activeButton == null || activeButton.getFilterType() == FilterType.NULL)
            return null;

        boolean useLongValues;
        long dayStart;
        long dayEnd;

        useLongValues = this.owner.getComponentDefinition().isUseNumericDateValues();
        dayStart = zeroDateToMidnight(activeButton.getDateFrom(), true).getTimeInMillis();
        dayEnd = zeroDateToMidnight(activeButton.getDateFrom(), false).getTimeInMillis();

        switch(activeButton.getFilterType())
        {
            case ON:
            {
                T8DataFilterCriteria filterCriteria;

                filterCriteria = new T8DataFilterCriteria();
                filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, fieldIdentifier, T8DataFilterCriterion.DataFilterOperator.GREATER_THAN_OR_EQUAL, useLongValues ? dayStart : new T8Timestamp(dayStart));
                filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, fieldIdentifier, T8DataFilterCriterion.DataFilterOperator.LESS_THAN_OR_EQUAL, useLongValues ? dayEnd : new T8Timestamp(dayEnd));

                return filterCriteria;
            }
            case AFTER:
            {
                return new T8DataFilterCriterion(fieldIdentifier, T8DataFilterCriterion.DataFilterOperator.GREATER_THAN_OR_EQUAL, useLongValues ? dayStart : new T8Timestamp(dayStart));
            }
            case BEFORE:
            {
                return new T8DataFilterCriterion(fieldIdentifier, T8DataFilterCriterion.DataFilterOperator.LESS_THAN_OR_EQUAL, useLongValues ? dayEnd : new T8Timestamp(dayEnd));
            }
            case BETWEEN:
            {
                T8DataFilterCriteria filterCriteria;

                // We need to overwrite the day end value using the 'to date'
                dayEnd = zeroDateToMidnight(activeButton.getDateTo(), false).getTimeInMillis();

                filterCriteria = new T8DataFilterCriteria();
                filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, fieldIdentifier, T8DataFilterCriterion.DataFilterOperator.GREATER_THAN_OR_EQUAL, useLongValues ? dayStart : new T8Timestamp(dayStart));
                filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, fieldIdentifier, T8DataFilterCriterion.DataFilterOperator.LESS_THAN_OR_EQUAL, useLongValues ? dayEnd : new T8Timestamp(dayEnd));

                return filterCriteria;
            }
            default:
                throw new IllegalStateException("Invalid filter type detected. Type : " + activeButton.getFilterType());
        }
    }

    public Date getDateFrom()
    {
        if(activeButton == null || activeButton.getDateFrom() == null) return null;
        else return activeButton.getDateFrom();
    }

    public Date getDateTo()
    {
        if(activeButton == null || activeButton.getDateTo() == null) return null;
        else return activeButton.getDateTo();
    }

    public Calendar zeroDateToMidnight(Date date, boolean dayStart)
    {
        Calendar todayMidnight = Calendar.getInstance();
        todayMidnight.setTime(date);
        todayMidnight.set(Calendar.HOUR, dayStart ? 0 : 24);
        todayMidnight.set(Calendar.MINUTE, 0);
        todayMidnight.set(Calendar.SECOND, 0);
        todayMidnight.set(Calendar.MILLISECOND, 0);

        return todayMidnight;
    }

    @Override
    public void invalidate()
    {
        super.invalidate();

        Dimension contentSize;
        Dimension prefferedSize;

        contentSize = getPreferredSize();
        prefferedSize = new Dimension();
        prefferedSize.width = contentSize.width;
        prefferedSize.height = contentSize.height;

        //Try to set the size the same as the owner combo box
        if (prefferedSize.width < owner.getWidth())
        {
            prefferedSize.width = owner.getWidth();
        }

        //Try to obtain a preffered size between the range
        if (prefferedSize.width < 280 || prefferedSize.height > 350)
        {
            prefferedSize.width = 280;
        }

        //Try to obtain a height within the preffered range
        if (prefferedSize.height < 150)
        {
            prefferedSize.height = 150;
        }
        if (prefferedSize.height > 450)
        {
            prefferedSize.height = 450;
        }

        setSize(prefferedSize);

        if (getWidth() > 0 && getHeight() > 0)
        {
            Paint paint;

            paint = new LinearGradientPaint(getWidth() / 2, 0, getWidth() / 2, getHeight() * 2, new float[]
                                    {
                                        0.1f, 0.6f
            }, new Color[]
                                    {
                                        new Color(255, 255, 255),
                                        new Color(240, 240, 245)
            });
            setBackgroundPainter(new MattePainter(paint));
        }
    }

    private void calculateLocation()
    {
        invalidate();

        Point comboLocation;
        Dimension comboSize;
        Point location;

        comboLocation = SwingUtilities.convertPoint(owner, 0, 0, JLayeredPane.getLayeredPaneAbove(owner));
        comboSize = owner.getSize();
        location = new Point();

        location.y = comboLocation.y + (int) comboSize.getHeight();
        location.x = comboLocation.x;

        //Check that we are within the screen bounds
        Dimension screenSize;
        Dimension mySize;

        screenSize = SwingUtilities.getRootPane(owner).getSize();
        mySize = getSize();

        if (location.x + mySize.width > screenSize.width)
        {
            location.x -= (location.x + mySize.width) - screenSize.width;
        }

        if (location.y + mySize.height > screenSize.height)
        {
            location.y -= (location.y + mySize.height) - screenSize.height;
        }

        setLocation(location);
    }

    @Override
    public void setVisible(boolean visible)
    {
        super.setVisible(visible);
        focusListenerPanel.setVisible(visible);

        if (visible)
        {
            calculateLocation();
            requestFocus();
            transferFocus();
        }
    }

    private void addDatePickerPane(ButtonGroup group, String name, int index, FilterType filterType)
    {
        GridBagConstraints constraints;
        JXDateTimePicker datePickerFrom;
        JXDateTimePicker datePickerTo;
        JRadioButton radioButton;

        radioButton = new JRadioButton(name);
        datePickerFrom = new JXDateTimePicker(new Date());
        datePickerTo = new JXDateTimePicker(new Date());
        constraints = new GridBagConstraints();

        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridheight = 1;
        constraints.gridwidth = 1;
        constraints.gridy = index;
        constraints.insets = new Insets(0, 0, 5, 5);
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.weighty = 0.1;

        //Add the radio Button
        radioButton.setOpaque(false);

        constraints.gridx = 0;
        constraints.weightx = 0;
        add(radioButton, constraints);
        group.add(radioButton);

        //Add the date picker panel
        constraints.gridx = 1;
        constraints.weightx = 1;
        constraints.insets.left = 0;
        if(filterType == FilterType.BETWEEN)
        {
            JPanel rangePanel;

            rangePanel = new JPanel(new BorderLayout(5, 0));
            rangePanel.setOpaque(false);

            rangePanel.add(datePickerFrom, BorderLayout.WEST);
            rangePanel.add(datePickerTo, BorderLayout.EAST);
            rangePanel.add(new JLabel(owner.getTranslation("to")), BorderLayout.CENTER);

            add(rangePanel, constraints);
        }
        else
        {
            add(datePickerFrom, constraints);
        }

        if(filterType != FilterType.BETWEEN)
        {
            radioButton.addItemListener(new RadioButtonListener(filterType, datePickerFrom));
        }
        else
        {
            radioButton.addItemListener(new RadioButtonListener(filterType, datePickerFrom, datePickerTo));
        }
    }

    private enum FilterType
    {
        NULL,
        ON,
        BEFORE,
        AFTER,
        BETWEEN
    }

    private class RadioButtonListener implements ItemListener
    {
        private final FilterType filterType;
        private final JXDateTimePicker datePickerFrom;
        private final JXDateTimePicker datePickerTo;

        private RadioButtonListener(FilterType filterType, JXDateTimePicker datePicker)
        {
            this.filterType = filterType;
            this.datePickerFrom = datePicker;
            this.datePickerTo = null;
        }

        private RadioButtonListener(FilterType filterType, JXDateTimePicker datePickerFrom, JXDateTimePicker datePickerTo)
        {
            this.filterType = filterType;
            this.datePickerFrom = datePickerFrom;
            this.datePickerTo = datePickerTo;
        }

        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if(e.getStateChange() == ItemEvent.SELECTED)
            {
                activeButton = this;
            }
        }

        public FilterType getFilterType()
        {
            return filterType;
        }

        public Date getDateFrom()
        {
            return datePickerFrom == null ? null : datePickerFrom.getDate();
        }

        public Date getDateTo()
        {
            return datePickerTo == null ? null : datePickerTo.getDate();
        }
    }

    private class FocusListenerPanel extends JComponent
    {

        private FocusListenerPanel()
        {
            setVisible(false);
            setOpaque(false);
            setLocation(0, 0);
            addMouseListener(new MouseAdapter()
            {
                private boolean armed;

                @Override
                public void mousePressed(MouseEvent e)
                {
                    armed = e.getButton() == MouseEvent.BUTTON1;
                }

                @Override
                public void mouseReleased(MouseEvent e)
                {
                    Point comboBoxPopupPoint;

                    comboBoxPopupPoint = e.getLocationOnScreen();
                    SwingUtilities.convertPointFromScreen(comboBoxPopupPoint, DatePickerPopUp.this);

                    if(armed && !DatePickerPopUp.this.contains(comboBoxPopupPoint) && FocusListenerPanel.this.contains(e.getPoint()))
                    {
                        owner.applyFilter();
                    }
                    armed = false;
                }
            });
        }

        @Override
        public void invalidate()
        {
            super.invalidate();

            setSize(SwingUtilities.getRootPane(owner).getSize());
        }
    }
}
