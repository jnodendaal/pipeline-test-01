package com.pilog.t8.ui.dataentitylist;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.ui.list.T8ListCellRenderableComponent;
import com.pilog.t8.ui.list.T8ListCellRendererComponent;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.datacombobox.T8DataComboBoxAPIHandler;
import com.pilog.t8.definition.ui.dataentitylist.T8DataEntityListAPIHandler;
import com.pilog.t8.definition.ui.dataentitylist.T8DataEntityListDefinition;
import com.pilog.t8.definition.ui.dataentitylist.T8DataEntityListDefinition.EntityRetrievalType;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogAPIHandler;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import com.pilog.t8.ui.busypanel.T8BusyPanel;
import com.pilog.t8.ui.cellrenderer.T8ListCellRendererAdaptor;
import com.pilog.t8.ui.datalookupdialog.T8DataLookupDialog;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.components.list.ListPopupMenuListener;
import com.pilog.t8.utilities.components.list.popupmenu.ListPopupMenu;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityList extends JPanel implements T8Component, T8ListCellRenderableComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataEntityList.class);

    private final T8DataEntityListDefinition definition;
    private final T8ComponentController parentController;
    private final T8ComponentController controller;
    private final T8DataEntityListOperationHandler operationHandler;
    private final T8DataEntityListDataHandler dataHandler;
    private T8DataLookupDialog lookupDialog;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private T8DataEntityDefinition entityDefinition;
    private T8ListCellRendererAdaptor rendererAdaptor;
    private final Map<String, T8DataFilter> lookupPrefilters;
    private final Map<String, T8DataFilter> listPrefilters;
    private Map<String, Object> selectedKey; // This field is set while the data loader thread is busy, in order for a value to be set after data has finished loading.
    private List<T8DataEntity> preloadedData;
    private final T8BusyPanel busyPanel;

    public static final Border UNSELECTED_BORDER = new EmptyBorder(1, 1, 1, 1);
    public static final Border SELECTED_BORDER = new LineBorder(Color.BLUE, 1);
    public static final Border FOCUS_BORDER = UIManager.getBorder("Table.focusCellHighlightBorder");
    public static final Border FOCUS_SELECTED_BORDER = new CompoundBorder(SELECTED_BORDER, FOCUS_BORDER);

    public T8DataEntityList(T8DataEntityListDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.parentController = controller;
        this.controller = new T8DefaultComponentController(controller, new T8ProjectContext(controller.getContext(), definition.getRootProjectId()), definition.getIdentifier(), false);
        this.controller.addComponentEventListener(new SelectionEventListener());
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();

        initComponents();

        this.jListElements.setModel(new DefaultListModel());
        this.operationHandler = new T8DataEntityListOperationHandler(this);
        this.dataHandler = new T8DataEntityListDataHandler(definition);
        this.lookupPrefilters = new HashMap<>();
        this.listPrefilters = new HashMap<>();
        this.busyPanel = new T8BusyPanel("Loading Data...");
        this.add(busyPanel, "LOADING_VIEW");
        this.jListElements.setCellRenderer(new T8DataEntityListCellRenderer(definition));
        this.jListElements.addListSelectionListener(new SelectionChangeListener());
        this.jListElements.setVisibleRowCount(5);
        jScrollPane1.getViewport().setOpaque(false);

        if(definition.getVisibleRowCount() != null && definition.getVisibleRowCount() > 1) this.jListElements.setVisibleRowCount(definition.getVisibleRowCount());
    }

    @Override
    public T8ComponentController getController()
    {
        return parentController;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        // Set the visibility of the tool bar.
        jToolBarMain.setVisible(definition.isToolBarVisible());

        // Set the visibility of each of the individual buttons on the toolbar
        jButtonAdd.setVisible(definition.isAddEnabled());
        jButtonRemove.setVisible(definition.isRemoveEnabled());
        jButtonMoveUp.setVisible(definition.isIndexIncrementEnabled());
        jButtonMoveDown.setVisible(definition.isIndexDecrementEnabled());

        // Initialize all the available list prefilters.
        for (T8DataFilterDefinition prefilterDefinition : definition.getListPrefilterDefinitions())
        {
            listPrefilters.put(prefilterDefinition.getIdentifier(), prefilterDefinition.getNewDataFilterInstance(context, null));
        }

        // Initialize all the available lookup prefilters.
        for (T8DataFilterDefinition prefilterDefinition : definition.getPrefilterDefinitions())
        {
            lookupPrefilters.put(prefilterDefinition.getIdentifier(), prefilterDefinition.getNewDataFilterInstance(context, null));
        }

        // Retrieve the entity data.
        if (definition.getEntityRetrievalType() == EntityRetrievalType.PRELOADED)
        {
            refreshLookupData();
        }

        setToolTipText(definition.getTooltipText());

        setOpaque(definition.isOpaque());

        setVisible(definition.isVisible());
    }

    @Override
    public void startComponent()
    {
        if (definition.isAutoRetrieve())
        {
            refreshListData();
        }
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
        if (T8c instanceof T8ListCellRendererComponent)
        {
            // Initialize the renderer.
            rendererAdaptor = new T8ListCellRendererAdaptor(this, (T8ListCellRendererComponent) T8c);
            jListElements.setCellRenderer(rendererAdaptor);
        }
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public String getDisplayFieldIdentifier()
    {
        return definition.getDisplayFieldIdentifier();
    }

    public JList getList()
    {
        return jListElements;
    }

    public void clearLookupPrefilters()
    {
        lookupPrefilters.clear();
    }

    public void setLookupPrefilter(String identifier, T8DataFilter filter)
    {
        lookupPrefilters.put(identifier, filter);

        if(lookupDialog != null) lookupDialog.setPrefilter(identifier, filter);
    }

    public T8DataFilter getLookupPrefilter(String identifier)
    {
        return lookupPrefilters.get(identifier);
    }

    public void clearListPrefilters()
    {
        listPrefilters.clear();
    }

    public void setListPrefilter(String identifier, T8DataFilter filter)
    {
        listPrefilters.put(identifier, filter);
    }

    public T8DataFilter getListPrefilter(String identifier)
    {
        return listPrefilters.get(identifier);
    }

    public T8DataFilter getCombinedListDataFilter()
    {
        T8DataFilter combinedFilter;

        // Create a filter to contain a combination of all applicable filters for data retrieval.
        combinedFilter = new T8DataFilter(definition.getDataEntityIdentifier(), new T8DataFilterCriteria());

        // Add all available and valid prefilters.
        for (T8DataFilter prefilter : listPrefilters.values())
        {
            // Only include filters that have valid filter criteria.
            if ((prefilter != null) && (prefilter.hasFilterCriteria()))
            {
                combinedFilter.getFilterCriteria().addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
            }

            // Add field ordering (the last applicable prefilter will override the previous ones).
            if (prefilter != null && prefilter.hasFieldOrdering())
            {
                combinedFilter.setFieldOrdering(prefilter.getFieldOrdering());
            }
        }

        return combinedFilter;
    }

    public void showListView()
    {
        ((CardLayout)this.getLayout()).show(this, "LIST_VIEW");
    }

    public void showLoadingView()
    {
        ((CardLayout)this.getLayout()).show(this, "LOADING_VIEW");
    }

    public void clearListData()
    {
        ((DefaultListModel)jListElements.getModel()).clear();
    }

    public void refreshListData()
    {
        DataModelLoader loader;

        // Create a new loader.
        loader = new DataModelLoader(getCombinedListDataFilter());

        // Run the loader in a new Thread.
        loader.execute();

        try
        {
            // Wait a while and if the loader still has not completed, display the processing panel.
            Thread.sleep(100);
            if (!loader.isDone())
            {
                showLoadingView();
            }
        }
        catch (Exception e)
        {
            LOGGER.log(e);
        }
    }

    public void refreshLookupData()
    {
        if(definition.getEntityRetrievalType() == EntityRetrievalType.LOOKUP_DIALOG)
        {
            if(lookupDialog != null) lookupDialog.refreshData();
        }
        else
        {
            DataPreloader loader;

            // Create a new loader.
            loader = new DataPreloader();

            // Run the loader in a new Thread.
            loader.execute();

            try
            {
                // Wait a while and if the loader still has not completed, display the processing panel.
                Thread.sleep(100);
                if (!loader.isDone())
                {
                    showLoadingView();
                }
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }
        }
    }

    public T8DataEntity getSelectedDataEntity()
    {
        return (T8DataEntity)jListElements.getSelectedValue();
    }

    public List<T8DataEntity> getSelectedDataEntities()
    {
        Object[]  selectedObjects;
        List<T8DataEntity> selectedEntities;

        selectedEntities = new ArrayList<>();
        selectedObjects = jListElements.getSelectedValues();
        for (Object selectedObject : selectedObjects)
        {
            selectedEntities.add((T8DataEntity)selectedObject);
        }

        return selectedEntities;
    }

    public void setSelectedDataEntity(T8DataEntity entity)
    {
        setSelectedKey(entity != null ? entity.getKeyFieldValues() : null);
    }

    public void setSelectedKey(Map<String, Object> key)
    {
        selectedKey = key;
        if (key == null)
        {
            jListElements.setSelectedValue(null, true);
        }
        else
        {
            ListModel model;
            T8DataFilter dataFilter;

            // Create a data filter from the key.
            dataFilter = new T8DataFilter(definition.getDataEntityIdentifier(), key);

            // Run through all of the entities in the combobox and select the one that matches the filter.
            model = jListElements.getModel();
            for (int entityIndex = 0; entityIndex < model.getSize(); entityIndex++)
            {
                T8DataEntity entity;

                entity = (T8DataEntity)model.getElementAt(entityIndex);
                if ((entity != null) && (dataFilter.includesEntity(entity)))
                {
                    jListElements.setSelectedIndex(entityIndex);
                    return;
                }
            }
        }
    }

    private void fireSequenceChangeEvent()
    {
        HashMap<String, Object> eventParameters;

        eventParameters = new HashMap<>();
        parentController.reportEvent(T8DataEntityList.this, definition.getComponentEventDefinition(T8DataEntityListAPIHandler.EVENT_SEQUENCE_CHANGE), eventParameters);
    }

    private void fireSelectionChangeEvent(List<T8DataEntity> selectedEntities)
    {
        HashMap<String, Object> eventParameters;

        eventParameters = new HashMap<>();
        eventParameters.put(T8DataComboBoxAPIHandler.PARAMETER_DATA_ENTITY, selectedEntities.size() > 0 ? selectedEntities.get(0) : null);
        eventParameters.put(T8DataEntityListAPIHandler.PARAMETER_DATA_ENTITY_LIST, selectedEntities);
        parentController.reportEvent(T8DataEntityList.this, definition.getComponentEventDefinition(T8DataEntityListAPIHandler.EVENT_SELECTION_CHANGE), eventParameters);
    }

    private void fireEntitiesAddedEvent(List<T8DataEntity> addedEntities)
    {
        HashMap<String, Object> eventParameters;

        eventParameters = new HashMap<>();
        eventParameters.put(T8DataEntityListAPIHandler.PARAMETER_DATA_ENTITY_LIST, addedEntities);
        parentController.reportEvent(T8DataEntityList.this, definition.getComponentEventDefinition(T8DataEntityListAPIHandler.EVENT_DATA_ENTITIES_ADDED), eventParameters);
    }

    private void fireEntitiesRemovedEvent(List<T8DataEntity> removedEntities)
    {
        HashMap<String, Object> eventParameters;

        eventParameters = new HashMap<>();
        eventParameters.put(T8DataEntityListAPIHandler.PARAMETER_DATA_ENTITY_LIST, removedEntities);
        parentController.reportEvent(T8DataEntityList.this, definition.getComponentEventDefinition(T8DataEntityListAPIHandler.EVENT_DATA_ENTITIES_REMOVED), eventParameters);
    }

    @Override
    public Font getCellFont()
    {
        return jListElements.getFont();
    }

    @Override
    public Color getSelectedCellBackgroundColor()
    {
        return jListElements.getBackground(); // Do not color the background when cell is selected.  The border indicates selection.
    }

    @Override
    public Color getSelectedCellForegroundColor()
    {
        return jListElements.getForeground();
    }

    @Override
    public Color getUnselectedCellBackgroundColor()
    {
        return jListElements.getBackground();
    }

    @Override
    public Color getUnselectedCellForegroundColor()
    {
        return jListElements.getForeground();
    }

    @Override
    public Border getSelectedBorder()
    {
        return SELECTED_BORDER;
    }

    @Override
    public Border getUnselectedBorder()
    {
        return UNSELECTED_BORDER;
    }

    @Override
    public Border getFocusedBorder()
    {
        return FOCUS_BORDER;
    }

    @Override
    public Border getFocusedSelectedBorder()
    {
        return FOCUS_SELECTED_BORDER;
    }

    public List<T8DataEntity> getDataEntityList()
    {
        DefaultListModel model;
        List<T8DataEntity> entityList;

        entityList = new ArrayList<>();
        model = (DefaultListModel)jListElements.getModel();
        for (int elementIndex = 0; elementIndex < model.getSize(); elementIndex++)
        {
            entityList.add((T8DataEntity)model.getElementAt(elementIndex));
        }

        return entityList;
    }

    public void setDataEntityList(List<T8DataEntity> entityList)
    {
        DefaultListModel model;

        model = (DefaultListModel)jListElements.getModel();
        model.clear();
        if (entityList != null)
        {
            for (T8DataEntity entity : entityList)
            {
                addDataEntity(entity);
            }
        }
    }

    public void addDataEntityList(List<T8DataEntity> entityList)
    {
        DefaultListModel model;

        model = (DefaultListModel)jListElements.getModel();
        if (entityList != null)
        {
            for (T8DataEntity entity : entityList)
            {
                addDataEntity(entity);
            }
        }
    }

    /**
     * This method uses the supplied list of keys to retrieve data entities that
     * are then added to the list model.  The boolean parameter oneEntityPerKey
     * may be used to force only one entity to be added to the list, per key in
     * the key list.
     *
     * @param keyList The list of keys to use for entity retrieval.
     * @param oneEntityPerKey A boolean value that may be set to 'true' to force
     * only one entity for each of the supplied keys to be added to the list.
     * @throws Exception
     */
    public void setDataEntityListKeys(List<Map<String, Object>> keyList, boolean oneEntityPerKey) throws Exception
    {
        if ((keyList != null) && (keyList.size() > 0))
        {
            KeyDataModelLoader loader;

            // Create a new loader.
            loader = new KeyDataModelLoader(keyList, oneEntityPerKey);

            // Run the loader in a new Thread.
            new Thread(loader).start();

            try
            {
                // Wait a while and if the loader still has not completed, display the processing panel.
                Thread.sleep(100);
                if (!loader.hasCompleted())
                {
                    showLoadingView();
                }
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }
        }
        else
        {
            jListElements.setModel(new DefaultListModel());
        }
    }

    private void addEntities()
    {
        if (definition.getEntityRetrievalType() == EntityRetrievalType.PRELOADED)
        {
            try
            {
                List<T8DataEntity> entityList;
                ListPopupMenu popupMenu;

                if (preloadedData != null)
                {
                    entityList = preloadedData;
                }
                else
                {
                    throw new RuntimeException("No preloaded data found.");
                }

                popupMenu = new ListPopupMenu(entityList);
                popupMenu.setCellRenderer(new T8DataEntityListCellRenderer(definition));
                popupMenu.addPopupListener(new PopupMenuListener());
                popupMenu.show(jButtonAdd, 0, 0);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while adding entity to list: " + definition, e);
            }
        }
        else if (definition.getEntityRetrievalType() == EntityRetrievalType.LOOKUP_DIALOG)
        {
            if (lookupDialog == null)
            {
                T8DataLookupDialogDefinition lookupDialogDefinition;

                // Initialize the dialog if it has not been initialized yet.
                lookupDialogDefinition = definition.getLookupDialogDefinition();
                if (lookupDialogDefinition != null)
                {
                    try
                    {
                        lookupDialog = (T8DataLookupDialog)lookupDialogDefinition.getNewComponentInstance(controller);
                        lookupDialog.initializeComponent(selectedKey);
                        for (T8DataFilter lookupPrefilter : lookupPrefilters.values())
                        {
                            lookupDialog.setPrefilter(lookupPrefilter.getIdentifier(), lookupPrefilter);
                        }
                    }
                    catch (Exception e)
                    {
                        T8Log.log("Exception while initialization data lookup dialog in data entity list: " + definition, e);
                    }
                }
                else throw new RuntimeException("No lookup dialog definition specified: " + definition);
            }

            // Show the dialog.
            lookupDialog.executeOperation(T8DataLookupDialogAPIHandler.OPERATION_OPEN_DIALOG, new HashMap<>());
        }
    }

    private void addDataEntity(T8DataEntity entity)
    {
        DefaultListModel<T8DataEntity> model;

        // Get the entity list model.
        model = (DefaultListModel<T8DataEntity>)jListElements.getModel();

        // If we allow duplicate entries to be added just add them, otherwise check the existing list elements
        if (definition.isAllowDuplicateEntries())
        {
            model.addElement(entity);
        }
        else
        {
            Enumeration<T8DataEntity> enumeration;
            Map<String, Object> keyFields;
            boolean matchFound;

            enumeration = model.elements();
            keyFields = entity.getKeyFieldValues();
            matchFound = false;

            // We can only match on key field values, so if we don't have any key values just skip the matching.
            if (keyFields.size() > 0)
            {
                while(enumeration.hasMoreElements())
                {
                    T8DataEntity listEntity;

                    listEntity = enumeration.nextElement();
                    if (listEntity.getKeyFieldValues().equals(entity.getKeyFieldValues()))
                    {
                        matchFound = true;
                        break;
                    }
                }
            }

            // If no entity in the list matches the new one, add it.
            if (!matchFound) model.addElement(entity);
        }
    }

    public void removeSelectedDataEntities()
    {
        List<T8DataEntity> removedEntities;
        DefaultListModel model;
        int selectedIndex;

        removedEntities = new ArrayList<>();
        model = (DefaultListModel)jListElements.getModel();
        while ((selectedIndex = jListElements.getSelectedIndex()) != -1)
        {
            removedEntities.add((T8DataEntity)model.elementAt(selectedIndex));
            model.remove(selectedIndex);
        }

        // Fire an event to indicate that entities have been removed from the list.
        fireEntitiesRemovedEvent(removedEntities);
    }

    private void moveSelectedEntitiesUp()
    {
        List<T8DataEntity> selectedEntities;
        int[] selectedIndices;

        selectedIndices = jListElements.getSelectedIndices();
        selectedEntities = getSelectedDataEntities();
        if (selectedEntities.size() > 0)
        {
            DefaultListModel model;

            model = (DefaultListModel)jListElements.getModel();
            for (T8DataEntity selectedEntity : selectedEntities)
            {
                int entityIndex;

                entityIndex = model.indexOf(selectedEntity);
                if (entityIndex > 0)
                {
                    model.removeElement(selectedEntity);
                    model.add(entityIndex-1, selectedEntity);
                }
            }

            // Decrement all selected indices (because they've been moved up).
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]-1);
            }

            // Set the new selected indices.
            jListElements.setSelectedIndices(selectedIndices);

            // Fire the sequence change event.
            fireSequenceChangeEvent();
        }
    }

    private void moveSelectedEntitiesDown()
    {
        List<T8DataEntity> selectedEntities;
        int[] selectedIndices;

        selectedIndices = jListElements.getSelectedIndices();
        selectedEntities = getSelectedDataEntities();
        if (selectedEntities.size() > 0)
        {
            DefaultListModel model;

            model = (DefaultListModel)jListElements.getModel();
            for (T8DataEntity selectedEntity : selectedEntities)
            {
                int entityIndex;

                entityIndex = model.indexOf(selectedEntity);
                if (entityIndex < jListElements.getModel().getSize() -1)
                {
                    model.removeElement(selectedEntity);
                    model.add(entityIndex+1, selectedEntity);
                }
            }

            // Increment all selected indices (because they've been moved down).
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]+1);
            }

            // Set the new selected indices.
            jListElements.setSelectedIndices(selectedIndices);

            // Fire the sequence change event.
            fireSequenceChangeEvent();
        }
    }

    //Overwrite the set enabled method so that we can disable the components as swing does not do this on its own
    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        jButtonAdd.setEnabled(enabled);
        jButtonMoveDown.setEnabled(enabled);
        jButtonMoveUp.setEnabled(enabled);
        jButtonRemove.setEnabled(enabled);
        jListElements.setEnabled(enabled);
        jPanelListView.setEnabled(enabled);
        jToolBarMain.setEnabled(enabled);
    }

    private class SelectionEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;
            T8Component component;
            Map<String, Object> eventParameters;
            String eventIdentifier;

            eventDefinition = event.getEventDefinition();
            component = event.getComponent();
            eventParameters = event.getEventParameters();
            eventIdentifier = eventDefinition.getPublicIdentifier();

            if (eventDefinition.getIdentifier().equals(T8DataLookupDialogAPIHandler.EVENT_SELECTION_CHANGED))
            {
                List<T8DataEntity> selectedEntities;

                selectedEntities = (List<T8DataEntity>)eventParameters.get(T8DataLookupDialogAPIHandler.PARAMETER_DATA_ENTITY_LIST);
                if (selectedEntities != null)
                {
                    for (T8DataEntity selectedEntity : selectedEntities)
                    {
                        addDataEntity(selectedEntity);
                    }

                    // Fire an event to indicate entities have been added to the list.
                    fireEntitiesAddedEvent(selectedEntities);
                }
            }
        }
    }

    private class SelectionChangeListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            if (!e.getValueIsAdjusting())
            {
                fireSelectionChangeEvent(getSelectedDataEntities());
            }
        }
    }

    private class PopupMenuListener implements ListPopupMenuListener
    {
        @Override
        public void popupItemSelected(Object item)
        {
            if (item != null)
            {
                addDataEntity((T8DataEntity)item);

                // Fire an event to indicate entities have been added to the list.
                fireEntitiesAddedEvent(ArrayLists.newArrayList((T8DataEntity)item));
            }
        }

        @Override
        public void popupItemsSelected(Object[] selectedItems)
        {
            for (Object item : selectedItems)
            {
                popupItemSelected(item);
            }
        }
    }

    private class DataPreloader extends SwingWorker<Void, Void>
    {
        @Override
        protected Void doInBackground() throws Exception
        {
            try
            {
                T8DataFilterCriteria combinedFilter;

                // Create a filter to contain a combination of all applicable filters for data retrieval.
                combinedFilter = new T8DataFilterCriteria();

                // Add all available and valid prefilters.
                for (T8DataFilter prefilter : lookupPrefilters.values())
                {
                    // Only include filters that have valid filter criteria.
                    if ((prefilter != null) && (prefilter.hasFilterCriteria()))
                    {
                        combinedFilter.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
                    }
                }

                // Use the combined filter to retrieve the filtered data and to construct a combobox model.
                preloadedData = dataHandler.loadEntityData(controller.getContext(), new T8DataFilter(definition.getDataEntityIdentifier(), combinedFilter));
            }
            catch (Exception e)
            {
                LOGGER.log(e);
                preloadedData = null;
            }

            return null;
        }

        @Override
        protected void done()
        {
            // Show the loaded data.
            showListView();
        }
    }

    private class KeyDataModelLoader implements Runnable
    {
        private boolean completed = false;
        private boolean oneEntityPerKey;
        private List<Map<String, Object>> keyList;

        private KeyDataModelLoader(List<Map<String, Object>> keyList, boolean oneEntityPerKey)
        {
            this.keyList = keyList;
            this.oneEntityPerKey = oneEntityPerKey;
        }

        public boolean hasCompleted()
        {
            return completed;
        }

        @Override
        public void run()
        {
            final DefaultListModel model;
            List<T8DataEntity> entityList;

            model = new DefaultListModel();

            try
            {
                T8DataFilter combinedFilter;

                // Create a filter to contain a combination of all applicable filters for data retrieval.
                combinedFilter = new T8DataFilter(definition.getDataEntityIdentifier(), keyList);

                // Add all available and valid prefilters.
                for (T8DataFilter prefilter : listPrefilters.values())
                {
                    // Only include filters that have valid filter criteria.
                    if ((prefilter != null) && (prefilter.hasFilterCriteria()))
                    {
                        combinedFilter.addFilterCriteria(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
                    }
                }

                // Load the entity list and add all of them to the new model.
                entityList = dataHandler.loadEntityData(controller.getContext(), combinedFilter);
                if (oneEntityPerKey) // If only one entity per key needs to be added, iterate the keys and find a matching entity.
                {
                    for (Map<String, Object> key : keyList)
                    {
                        T8DataEntity entity;

                        entity = T8DataUtilities.findDataEntity(entityList, key);
                        if (entity != null)
                        {
                            model.addElement(entity);
                        }
                    }
                }
                else
                {
                    if (entityList != null)
                    {
                        for (T8DataEntity entity : entityList)
                        {
                            model.addElement(entity);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LOGGER.log(e);
                preloadedData = null;
            }

            try
            {
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        // Set the model.
                        jListElements.setModel(model);

                        // Set the completed flag.
                        completed = true;

                        // Show the loaded data.
                        showListView();
                    }
                });
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while setting data entity list model: " + definition, e);
            }
        }
    }

    private class DataModelLoader extends SwingWorker<DefaultListModel, Void>
    {
        private final T8DataFilter dataFilter;

        private DataModelLoader(T8DataFilter dataFilter)
        {
            this.dataFilter = dataFilter;
        }

        @Override
        protected DefaultListModel doInBackground() throws Exception
        {
            final DefaultListModel model;
            List<T8DataEntity> entityList;

            model = new DefaultListModel();

            try
            {
                // Load the entity list and add all of them to the new model.
                entityList = dataHandler.loadEntityData(controller.getContext(), dataFilter);
                if (entityList != null)
                {
                    for (T8DataEntity entity : entityList)
                    {
                        model.addElement(entity);
                    }
                }
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }

            return model;
        }

        @Override
        protected void done()
        {
            try
            {
                // Set the model.
                jListElements.setModel(get());

                // Show the loaded data.
                showListView();
            }
            catch (InterruptedException | ExecutionException ex)
            {
                LOGGER.log(ex);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelListView = new javax.swing.JPanel();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonAdd = new javax.swing.JButton();
        jButtonRemove = new javax.swing.JButton();
        jButtonMoveUp = new javax.swing.JButton();
        jButtonMoveDown = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListElements = new javax.swing.JList();

        setName(""); // NOI18N
        setOpaque(false);
        setLayout(new java.awt.CardLayout());

        jPanelListView.setName(""); // NOI18N
        jPanelListView.setOpaque(false);
        jPanelListView.setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);
        jToolBarMain.setOpaque(false);

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setToolTipText("Add an element to the list");
        jButtonAdd.setFocusable(false);
        jButtonAdd.setOpaque(false);
        jButtonAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAdd);

        jButtonRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/removeDocumentIcon.png"))); // NOI18N
        jButtonRemove.setText("Remove");
        jButtonRemove.setToolTipText("Remove selected elements from the list");
        jButtonRemove.setFocusable(false);
        jButtonRemove.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemove.setOpaque(false);
        jButtonRemove.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRemove);

        jButtonMoveUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowUpIcon.png"))); // NOI18N
        jButtonMoveUp.setText("Up");
        jButtonMoveUp.setFocusable(false);
        jButtonMoveUp.setOpaque(false);
        jButtonMoveUp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveUp.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveUpActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonMoveUp);

        jButtonMoveDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/developer/icons/arrowDownIcon.png"))); // NOI18N
        jButtonMoveDown.setText("Down");
        jButtonMoveDown.setFocusable(false);
        jButtonMoveDown.setOpaque(false);
        jButtonMoveDown.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonMoveDown.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonMoveDownActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonMoveDown);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelListView.add(jToolBarMain, gridBagConstraints);

        jListElements.setMaximumSize(null);
        jListElements.setMinimumSize(null);
        jListElements.setName(""); // NOI18N
        jListElements.setOpaque(false);
        jListElements.setPreferredSize(null);
        jScrollPane1.setViewportView(jListElements);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelListView.add(jScrollPane1, gridBagConstraints);

        add(jPanelListView, "LIST_VIEW");
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddActionPerformed
    {//GEN-HEADEREND:event_jButtonAddActionPerformed
        addEntities();
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveActionPerformed
        removeSelectedDataEntities();
    }//GEN-LAST:event_jButtonRemoveActionPerformed

    private void jButtonMoveUpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveUpActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveUpActionPerformed
        moveSelectedEntitiesUp();
    }//GEN-LAST:event_jButtonMoveUpActionPerformed

    private void jButtonMoveDownActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonMoveDownActionPerformed
    {//GEN-HEADEREND:event_jButtonMoveDownActionPerformed
        moveSelectedEntitiesDown();
    }//GEN-LAST:event_jButtonMoveDownActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonMoveDown;
    private javax.swing.JButton jButtonMoveUp;
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JList jListElements;
    private javax.swing.JPanel jPanelListView;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
