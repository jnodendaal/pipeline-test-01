package com.pilog.t8.ui.file.uploadpanel;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.log.T8Logger.Level;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.utilities.components.filechoosers.BasicFileChooser;
import com.pilog.t8.utilities.files.FileUtilities;
import java.awt.CardLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 * @author Bouwer du Preez
 */
public class FileUploadPanel extends JPanel
{
    private final T8ComponentController controller;
    private final T8Context context;
    private final T8FileManager fileManager;
    private UploadCheckerThread uploadCheckerThread;
    private DownloadCheckerThread downloadCheckerThread;
    private File currentlyUploadingFile;
    private HTMLDocument doc;
    private String contextId;
    private String contextIid;
    private String remoteFilePath;
    private final boolean closeFileContext;

    private static File LAST_SELECTED_FOLDER = null;
    private T8Logger LOGGER = T8Log.getLogger(FileUploadPanel.class);

    //Construct enum for the different Hyper Link Actions.
    enum FileUploadHyperLinkListenerAction{DOWNLOAD_ACTION, DELETE_ACTION};

    public FileUploadPanel(T8ComponentController controller)
    {
        initComponents();
        this.controller = controller;
        this.context = controller.getContext();
        this.fileManager = controller.getClientContext().getFileManager();
        this.closeFileContext = false;
        jTextPaneView.addHyperlinkListener(new FileUploadHyperLinkListener());
    }

    public void setUploadContext(String contextId, String contextIid, String filePath)
    {
        LOGGER.log(Level.INFO, "Set Upload Context...");
        //Check if the context has changed, to reset the card components.
        if ((this.contextId != null && !this.contextId.equals(contextId)) ||
                (this.contextIid != null && !this.contextIid.equals(contextIid)) ||
                    (this.remoteFilePath != null && !this.remoteFilePath.equals(filePath)))
                        clearCardComponents();

        this.contextId = contextId;
        this.contextIid = contextIid;
        this.remoteFilePath = filePath;
    }

    /*
        This is to be used only if the process uses either the definition setup for the context or
        the temporary context. (Which refers to the files/temp/ path in the APplication server.)
    */
    public void setUploadFilePath(String filePath)
    {
        //Check if the file path has changed, to reset the card components.
        if (this.remoteFilePath != null && !this.remoteFilePath.equals(filePath))
            clearCardComponents();

        this.remoteFilePath = filePath;
    }

    private void uploadFiles()
    {
        try
        {
            String selectedFilePath;

            //Change the content value card showing component
            changeCardComponent("cardLog");

            // Get the target file path that identifies the file/files that will be uploaded.
            selectedFilePath = BasicFileChooser.getLoadFilePath(SwingUtilities.getWindowAncestor(this), null, "All Files", LAST_SELECTED_FOLDER);
            if (selectedFilePath != null)
            {
                final List<File> filesToUpload;
                File selectedFile;

                // Create a file from the selected path.
                selectedFile = new File(selectedFilePath);

                // Update the last selected folder path.
                LAST_SELECTED_FOLDER = selectedFile;
                if ((selectedFile.isFile()) && (selectedFile.getParentFile() != null)) LAST_SELECTED_FOLDER = LAST_SELECTED_FOLDER.getParentFile();

                // Create the list of files to upload.
                // Do not include sub-directories as this could potentially create situations where too many files are mistakenly uploaded.
                filesToUpload = FileUtilities.listFiles(selectedFile, false);

                // Make sure the destination file context is open.
                if (contextIid == null) contextIid = T8IdentifierUtilities.createNewGUID();
                fileManager.openFileContext(context, contextIid, contextId, new T8Minute(30));

                // Start the file upload.
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        for (File fileToUpload : filesToUpload)
                        {
                            try
                            {
                                T8FileDetails fileDetails;
                                String remotePath;

                                // Set the next file to be uploaded and start the process.
                                T8Log.log("Uploading file: " + fileToUpload.getCanonicalPath());
                                currentlyUploadingFile = fileToUpload;
                                remotePath = remoteFilePath != null ? remoteFilePath + "/" + fileToUpload.getName() : fileToUpload.getName();

                                // Update the log.
                                SwingUtilities.invokeLater(() ->
                                {
                                    jTextAreaLog.append("Uploading file: " + fileToUpload.getName() + "...");
                                });

                                // Upload the file.
                                fileDetails = fileManager.uploadFile(context, contextIid, fileToUpload.getCanonicalPath(), remotePath);

                                // Update the log.
                                SwingUtilities.invokeLater(() ->
                                {
                                    jTextAreaLog.append(fileDetails.getFileSize() + " bytes uploaded successfully.\n");
                                });
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                JOptionPane.showMessageDialog(controller.getParentWindow(), "The selected file could not be uploaded successfully.  Please try again.", "Upload Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }

                        // Close the file context if required.
                        if (closeFileContext)
                        {
                            try
                            {
                                fileManager.closeFileContext(context, contextIid);
                            }
                            catch (Exception e)
                            {
                                T8Log.log("Exception while closing file context after file upload: " + contextIid, e);
                            }
                        }

                        // Stop the upload checker.
                        stopUploadChecker();
                    }
                }.start();

                // Show the progress of the upload.
                startUploadChecker();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(controller.getParentWindow(), "The selected file could not be uploaded successfully.  Please try again.", "Upload Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void viewFiles()
    {
        HTMLEditorKit editorKit;
        List<T8FileUploadLink> fileUploadLinks;
        List<T8FileDetails> fileDetails;
        StringBuilder htmlBuilder;

        try
        {
            changeCardComponent("cardView");

            // Make sure the destination file context is open.
            System.out.println("contextIid: " + contextIid);
            System.out.println("remoteFilePath: " + remoteFilePath);
            if (contextIid == null) contextIid = T8IdentifierUtilities.createNewGUID();
            fileManager.openFileContext(context, contextIid, contextId, new T8Minute(30));
            fileDetails = new LinkedList<>();

            //Get all the files from the current context.
            try
            {
                fileDetails = fileManager.getFileList(context, contextIid, remoteFilePath);
            }
            catch (FileNotFoundException e)
            {
                LOGGER.log(T8Logger.Level.WARNING, "The current directory selected does not exist yet.", e);
            }

            fileUploadLinks = new LinkedList<>();

            for (T8FileDetails fileDetail : fileDetails)
            {
                fileUploadLinks.add(new T8FileUploadLink(fileDetail.getFileName(), fileDetail.getFilePath()));
            }

            //configure the text pane
            doc = new HTMLDocument();
            editorKit = new HTMLEditorKit();
            jTextPaneView.setEditorKit(editorKit);
            jTextPaneView.setDocument(doc);
            jTextPaneView.setEditable(false);
            jTextPaneView.setContentType("text/html");

            htmlBuilder = new StringBuilder();
            htmlBuilder.append("<table>");
            for (T8FileUploadLink fileUploadLink : fileUploadLinks)
            {
                htmlBuilder.append("<tr>");
                htmlBuilder.append("<td><u><a href=\"");
                htmlBuilder.append(fileUploadLink.getLinkHReference());
                htmlBuilder.append("\" style=\"color: blue\">");
                htmlBuilder.append(fileUploadLink.getLinkText());
                htmlBuilder.append("</a></u></td> &emsp; &emsp;");
                htmlBuilder.append("<td style=\"padding-left:5em\"><u><a href=\"");
                htmlBuilder.append(fileUploadLink.getLinkHReference());
                htmlBuilder.append("<<delete>>\" style=\"color: blue\">Delete</a></u></td>");
                htmlBuilder.append("</tr>");
            }
            htmlBuilder.append("</table>");
            editorKit.insertHTML(doc, doc.getLength(), htmlBuilder.toString(), 0, 0, null);
        }
        catch (Exception ex)
        {
            LOGGER.log(T8Logger.Level.FATAL, "An unexpected error occured during the initialization of the File Upload Panel Components.", ex);
        }

        // Close the file context if required.
        if (closeFileContext)
        {
            try
            {
                fileManager.closeFileContext(context, contextIid);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while closing file context after file upload: " + contextIid, e);
            }
        }
    }

    private void downloadFile(String fileName)
    {
        // Get the target file path to which the file will be downloaded.
        try
        {
            File selectedFile;
            String localFilePath;

            // Determine the preselected file in the file chooser.
            if (LAST_SELECTED_FOLDER != null) selectedFile = new File(LAST_SELECTED_FOLDER.getCanonicalFile() + "/" + fileName);
            else selectedFile = new File(remoteFilePath + "/" + fileName);

            // Get the local file path to which the file will be downloaded.
            localFilePath = BasicFileChooser.getSaveFilePath(SwingUtilities.getWindowAncestor(this), null, "All Files", selectedFile, BasicFileChooser.FileSelectionMode.FILES);
            if (localFilePath != null)
            {
                // Update the last selected folder path.
                LAST_SELECTED_FOLDER = new File(localFilePath);
                if (LAST_SELECTED_FOLDER.getParentFile() != null) LAST_SELECTED_FOLDER = LAST_SELECTED_FOLDER.getParentFile();

                // Execute the downloader thread.
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            long fileSize;

                            // Update the log.
                            SwingUtilities.invokeLater(() ->
                            {
                                jTextAreaLog.append("Downloading file: " + selectedFile.getName() + "...");
                            });

                            fileSize = fileManager.downloadFile(context, contextIid, localFilePath, fileName);
                            JOptionPane.showMessageDialog(FileUploadPanel.this, "The selected file was downloaded successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);

                            // Update the log.
                            SwingUtilities.invokeLater(() ->
                            {
                                jTextAreaLog.append(fileSize + " bytes downloaded successfully.\n");
                            });

                            stopDownloadChecker();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            JOptionPane.showMessageDialog(FileUploadPanel.this, "The selected file could not be downloaded successfully.  Please try again.", "Download Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }.start();

                // Show the progress of the upload.
                startDownloadChecker();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(FileUploadPanel.this, "The selected file could not be downloaded successfully.  Please try again.", "Download Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void deleteFile(String fileName)
    {
        try
        {
            int conformation = JOptionPane.showConfirmDialog(FileUploadPanel.this, "Are you sure you want to remove this file from the selected temporary directory?", "Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            System.out.println("conformation: " + conformation);
            if (conformation == JOptionPane.YES_OPTION)
            {
                // Update the log.
                SwingUtilities.invokeLater(() ->
                {
                    jTextAreaLog.append("Deleting file: " + fileName + "...");
                });

                fileManager.deleteFile(context, contextIid, fileName);

                // Update the log.
                SwingUtilities.invokeLater(() ->
                {
                    jTextAreaLog.append("\nFile: " + fileName + " successfully deleted.\n");
                });
            }
        }
        catch (Exception ex)
        {
            LOGGER.log(T8Logger.Level.FATAL, "Something caused the deletion process to fail.", ex);
        }
    }

    private void startDownloadChecker()
    {
        showStatusProgress();
        downloadCheckerThread = new DownloadCheckerThread();
        downloadCheckerThread.start();
    }

    private void stopDownloadChecker()
    {
        if (downloadCheckerThread != null)
        {
            downloadCheckerThread.stopChecker();
        }
        showStatusText();
    }

    private void changeCardComponent(String componentName)
    {

        CardLayout contentCardLayout;

        contentCardLayout = (CardLayout) jPanelContent.getLayout();
        contentCardLayout.show(jPanelContent, componentName);
    }

    private void clearCardComponents()
    {
        LOGGER.log(Level.INFO, "Clearing the Card Components...");
        jTextAreaLog.setText("");
        jTextPaneView.setText("");
        invalidate();
        revalidate();
    }

    private void startUploadChecker()
    {
        showStatusProgress();
        uploadCheckerThread = new UploadCheckerThread();
        uploadCheckerThread.start();
    }

    private void stopUploadChecker()
    {
        if (uploadCheckerThread != null)
        {
            uploadCheckerThread.stopChecker();
        }

        showStatusText();
    }

    private void showStatusText()
    {
        CardLayout layout;

        layout = (CardLayout)jPanelStatus.getLayout();
        layout.show(jPanelStatus, "TEXT");
    }

    private void showStatusProgress()
    {
        CardLayout layout;

        layout = (CardLayout)jPanelStatus.getLayout();
        layout.show(jPanelStatus, "PROGRESS");
    }

    private class UploadCheckerThread extends Thread
    {
        private boolean stopFlag;
        private boolean complete;
        private int progress;

        public UploadCheckerThread()
        {
            stopFlag = false;
            complete = false;
            progress = 0;
        }

        @Override
        public void run()
        {
            SwingUtilities.invokeLater(() ->
            {
                jButtonUpload.setEnabled(false);
            });

            try
            {
                while ((progress < 100) && (!stopFlag))
                {
                    if (currentlyUploadingFile != null)
                    {
                        progress = fileManager.getUploadOperationProgress(context, currentlyUploadingFile.getCanonicalPath());
                        SwingUtilities.invokeLater(() ->
                        {
                            jProgressBarUpload.setValue(progress);
                        });
                    }

                    Thread.sleep(500);
                }
                complete = true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            SwingUtilities.invokeLater(() ->
            {
                jButtonUpload.setEnabled(true);
            });
        }

        public void stopChecker()
        {
            stopFlag = true;
        }

        public boolean isComplete()
        {
            return complete;
        }
    }

    private class DownloadCheckerThread extends Thread
    {
        private boolean stopFlag;
        private int progress;

        public DownloadCheckerThread()
        {
            stopFlag = false;
            progress = 0;
        }

        @Override
        public void run()
        {
            try
            {
                while ((progress < 100) && (!stopFlag))
                {
                    System.out.println("progress: " + progress);
                    progress = fileManager.getDownloadOperationProgress(context, currentlyUploadingFile.getCanonicalPath());
                    SwingUtilities.invokeLater(() ->
                    {
                        jProgressBarUpload.setValue(progress);
                    });

                    Thread.sleep(500);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        public void stopChecker()
        {
            stopFlag = true;
        }
    }

    private class FileUploadHyperLinkListener implements HyperlinkListener
    {
        @Override
        public void hyperlinkUpdate(HyperlinkEvent e)
        {
            FileUploadHyperLinkListenerAction action;

            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
            {
                if ((e.getURL() != null && e.getURL().toString().contains("<<delete>>")) ||
                        (e.getDescription() != null && e.getDescription().contains("<<delete>>")))
                        action = FileUploadHyperLinkListenerAction.DELETE_ACTION;
                else action = FileUploadHyperLinkListenerAction.DOWNLOAD_ACTION;

                //Process according to the action executed.
                switch (action)
                {
                    case DOWNLOAD_ACTION:
                    {
                        downloadFile(e.getDescription());
                    }
                    break;
                    case DELETE_ACTION:
                    {
                        String fileName;

                        fileName = e.getDescription().substring(0, e.getDescription().indexOf("<<delete>>"));
                        deleteFile(fileName);
                        viewFiles();
                    }
                    break;
                }
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelStatus = new javax.swing.JLabel();
        jPanelStatus = new javax.swing.JPanel();
        jPanelStatusText = new javax.swing.JPanel();
        jLabelStatusText = new org.jdesktop.swingx.JXLabel();
        jPanelStatusProgress = new javax.swing.JPanel();
        jProgressBarUpload = new javax.swing.JProgressBar();
        jButtonUpload = new javax.swing.JButton();
        jButtonView = new javax.swing.JButton();
        jPanelContent = new javax.swing.JPanel();
        jScrollPaneLog = new javax.swing.JScrollPane();
        jTextAreaLog = new javax.swing.JTextArea();
        jScrollPaneView = new javax.swing.JScrollPane();
        jTextPaneView = new javax.swing.JTextPane();

        setName(""); // NOI18N
        setLayout(new java.awt.GridBagLayout());

        jLabelStatus.setText("Status:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jLabelStatus, gridBagConstraints);

        jPanelStatus.setLayout(new java.awt.CardLayout());

        jPanelStatusText.setLayout(new java.awt.GridBagLayout());

        jLabelStatusText.setText("Please click the Upload button to upload files.");
        jLabelStatusText.setToolTipText("");
        jLabelStatusText.setLineWrap(true);
        jLabelStatusText.setName(""); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelStatusText.add(jLabelStatusText, gridBagConstraints);

        jPanelStatus.add(jPanelStatusText, "TEXT");

        jPanelStatusProgress.setLayout(new java.awt.GridBagLayout());

        jProgressBarUpload.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelStatusProgress.add(jProgressBarUpload, gridBagConstraints);

        jPanelStatus.add(jPanelStatusProgress, "PROGRESS");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 0);
        add(jPanelStatus, gridBagConstraints);

        jButtonUpload.setText("Upload");
        jButtonUpload.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonUploadActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jButtonUpload, gridBagConstraints);

        jButtonView.setText("View");
        jButtonView.setToolTipText("The view of all the files in the selected context.");
        jButtonView.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonViewActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jButtonView, gridBagConstraints);

        jPanelContent.setLayout(new java.awt.CardLayout());

        jTextAreaLog.setEditable(false);
        jTextAreaLog.setColumns(20);
        jTextAreaLog.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        jTextAreaLog.setRows(5);
        jScrollPaneLog.setViewportView(jTextAreaLog);

        jPanelContent.add(jScrollPaneLog, "cardLog");

        jScrollPaneView.setViewportView(jTextPaneView);

        jPanelContent.add(jScrollPaneView, "cardView");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jPanelContent, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonUploadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonUploadActionPerformed
    {//GEN-HEADEREND:event_jButtonUploadActionPerformed
        uploadFiles();
    }//GEN-LAST:event_jButtonUploadActionPerformed

    private void jButtonViewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonViewActionPerformed
    {//GEN-HEADEREND:event_jButtonViewActionPerformed
        viewFiles();
    }//GEN-LAST:event_jButtonViewActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonUpload;
    private javax.swing.JButton jButtonView;
    private javax.swing.JLabel jLabelStatus;
    private org.jdesktop.swingx.JXLabel jLabelStatusText;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelStatus;
    private javax.swing.JPanel jPanelStatusProgress;
    private javax.swing.JPanel jPanelStatusText;
    private javax.swing.JProgressBar jProgressBarUpload;
    private javax.swing.JScrollPane jScrollPaneLog;
    private javax.swing.JScrollPane jScrollPaneView;
    private javax.swing.JTextArea jTextAreaLog;
    private javax.swing.JTextPane jTextPaneView;
    // End of variables declaration//GEN-END:variables

}
