package com.pilog.t8.ui.filter.panel.component.list;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.list.T8FilterPanelComboboxDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.list.T8FilterPanelListComponentAPIHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.busypanel.T8BusyPainter;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponent;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItem;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItemDataSource;
import com.pilog.t8.ui.filter.panel.component.model.T8FilterPanelDataSourceModel;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.ComboBoxModel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jdesktop.swingx.JXComboBox;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelCombobox extends JXComboBox implements T8FilterPanelListComponent, T8Component, ItemListener
{
    private static final int BUSY_PAINTER_DIAMETER = 20;
    private final T8FilterPanelComboboxDefinition definition;
    private final T8FilterPanelListComponentItemDataSource dataSource;
    private final String dataEntityValueIdentifier;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8ComponentController controller;
    private final List<ChangeListener> changeListeners;
    private final T8FilterPanelListComponentOperationHandler operationHandler;
    private final T8BusyPainter busyPainter;
    private String dataEntityIdentifier;
    private DefaultFilterPanelListComponentItemCellRenderer cellRenderer;
    private DataLoader lasDataLoader;
    private Map<String, Object> inputParameters;

    public T8FilterPanelCombobox(T8FilterPanelComboboxDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.dataEntityValueIdentifier = definition.getDataEntityKeyFieldIdentifier();
        this.dataSource = definition.getDatasourceDefinition().getInstance();
        this.controller = controller;
        this.changeListeners = new ArrayList<>();
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.operationHandler = new T8FilterPanelListComponentOperationHandler(this);
        this.busyPainter = new T8BusyPainter(this, BUSY_PAINTER_DIAMETER, 8, 3);
        this.addMouseListener(new ComboMouseListener());
    }

    @Override
    public T8DataFilterCriteria getFilterCriteria()
    {
        if (this.getSelectedItem() == null || (this.getSelectedItem() != null && ((T8FilterPanelListComponentItem) this.getSelectedItem()).getValue() == null))
        {
            return new T8DataFilterCriteria();
        }
        T8DataFilterCriteria filterCriteria = new T8DataFilterCriteria();
        if (!definition.isTagField())
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, ((T8FilterPanelListComponentItem) this.getSelectedItem()).getFilterClause(context, dataEntityValueIdentifier));
        }
        else
        {
            if (((T8FilterPanelListComponentItem) this.getSelectedItem()).getValue() != null)
            {
                filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.OR,
                                               dataEntityValueIdentifier,
                                               T8DataFilterCriterion.DataFilterOperator.EQUAL,
                                               ((T8FilterPanelListComponentItem) this.getSelectedItem()).getValue().toString());
            }
        }
        return filterCriteria;
    }

    @Override
    public void initialize(T8Context context) throws Exception
    {
        setModel(new ListComboBoxModel(Arrays.asList(new T8FilterPanelListComponentItem(getTranslatedString(definition.getDisplayLabel()), null))));
        setSelectedIndex(0);
        setEnabled(false);
        setToolTipText(definition.getTooltipText());
    }

//<editor-fold defaultstate="collapsed" desc="Boiler Plate">
    @Override
    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        this.dataEntityIdentifier = dataEntityIdentifier;
    }

    @Override
    public Component getComponent()
    {
        return this;
    }

    public T8ClientContext getClientContext()
    {
        return clientContext;
    }

    @Override
    public String toString()
    {
        return this.definition.getIdentifier();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return inputParameters;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier,
                                                Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void stopComponent()
    {
        if (renderer instanceof T8Component)
        {
            ((T8Component) renderer).stopComponent();
        }
        if (lasDataLoader != null)
        {
            lasDataLoader.cancel(false);
        }
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public void setNewDataSourceFilter(T8DataFilter dataFilter) throws Exception
    {
        this.dataSource.setNewDataSourceFilter(dataFilter);
    }

    private String getTranslatedString(String text)
    {
        return controller.getClientContext().getConfigurationManager().getUITranslation(context, text);
    }

    @Override
    public void addChangeListener(ChangeListener changeListener)
    {
        changeListeners.add(changeListener);
    }

    @Override
    public void removeChangeListener(ChangeListener changeListener)
    {
        changeListeners.remove(changeListener);
    }

    @Override
    public void fireStateChanged()
    {
        for (ChangeListener changeListener : changeListeners)
        {
            changeListener.stateChanged(new ChangeEvent(this));
        }
        Map<String, Object> eventParameters;
        T8FilterPanelListComponentItem selectedItem;
        selectedItem = (T8FilterPanelListComponentItem) this.getSelectedItem();
        eventParameters = new HashMap<>();
        eventParameters.put(T8FilterPanelListComponentAPIHandler.PARAMETER_NEW_VALUE, selectedItem == null
                                                                                      ? null
                                                                                      : selectedItem.getValue());

        controller.reportEvent(this, definition.getComponentEventDefinition(T8FilterPanelListComponentAPIHandler.EVENT_SELECTION_CHANGED), eventParameters);
    }

    @Override
    public void itemStateChanged(ItemEvent e)
    {
        if (e.getStateChange() == ItemEvent.SELECTED)
        {
            fireStateChanged();
        }
    }

    protected ListCellRenderer<T8FilterPanelListComponentItem> getCustomRenderer()
    {
        return new DefaultFilterPanelListComponentItemCellRenderer();
    }
//</editor-fold>

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.inputParameters = inputParameters;
        try
        {
            initialize(context);
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to initialize component " + definition.getIdentifier(), ex);
            throw new RuntimeException(ex);
        }

        if (definition.getRendererComponentDefinition() != null)
        {
            try
            {
                T8Component renderer;

                renderer = definition.getRendererComponentDefinition().getNewComponentInstance(controller);
                renderer.initializeComponent(inputParameters);

                this.renderer = (ListCellRenderer) renderer;
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to load cell renderer.", ex);
            }
        }
        else
        {
            this.renderer = getCustomRenderer();
        }
        setRenderer(renderer);
    }

    @Override
    public void startComponent()
    {
        if (renderer instanceof T8Component)
        {
            ((T8Component) renderer).startComponent();
        }
        if(definition.isLoadOnStartup())
        {
            //Load the data from the data source
            refresh();
        }
    }

    @Override
    public void refresh()
    {
        refresh(false);
    }

    public void refresh(boolean showPopupOnCompletion)
    {
        if (lasDataLoader != null)
        {
            lasDataLoader.cancel(false);
        }
        setEnabled(false);
        setModel(new DataModel());
        //Re-execute the Dataloader
        lasDataLoader = new DataLoader(showPopupOnCompletion);
        lasDataLoader.execute();
    }

    @Override
    public void paint(Graphics g)
    {
        if (lasDataLoader != null && !lasDataLoader.isDone())
        {
            Graphics2D g2;
            Rectangle clipBounds;
            FontMetrics fontMetrics;
            float textY;

            g2 = (Graphics2D)g;
            clipBounds = g2.getClipBounds();

            busyPainter.paint(g2, this, BUSY_PAINTER_DIAMETER, BUSY_PAINTER_DIAMETER);
            g2.setColor(getForeground());
            fontMetrics = g2.getFontMetrics();
            textY = clipBounds.y + (clipBounds.height / 2.0f) + (fontMetrics.getAscent() / 2.0f);
            g2.drawString(getTranslatedString("Loading Data..."), clipBounds.x + BUSY_PAINTER_DIAMETER + 10, textY);
        }
        else
        {
            super.paint(g);
        }
    }

    private class ComboMouseListener extends MouseAdapter
    {

        @Override
        public void mouseClicked(MouseEvent e)
        {
            if(lasDataLoader == null)
            {
                try
                {
                    refresh(true);
                }
                catch (Exception ex)
                {
                    T8Log.log("Failed to refresh data", ex);
                }
                //The initial data is loaded, and we can remove this listener now
                removeMouseListener(this);
            }
        }
    }

    private class DataModel extends T8FilterPanelDataSourceModel implements ComboBoxModel<T8FilterPanelListComponentItem>
    {
        private T8FilterPanelListComponentItem _selectedItem;

        private DataModel()
        {
        }

        private DataModel(T8FilterPanelListComponentItemDataSource dataSource) throws Exception
        {
            super(dataSource);
        }

        private DataModel(ArrayList<T8FilterPanelListComponentItem> _items)
        {
            super(_items);
        }

        @Override
        public void setSelectedItem(Object anItem)
        {
            _selectedItem = (T8FilterPanelListComponentItem) anItem;
        }

        @Override
        public Object getSelectedItem()
        {
            return _selectedItem;
        }
    }

    private class DataLoader extends SwingWorker<DataModel, Void>
    {
        private final boolean showPopupOnCompletion;

        private DataLoader(boolean showPopupOnCompletion)
        {
            this.showPopupOnCompletion = showPopupOnCompletion;
        }

        @Override
        protected DataModel doInBackground() throws Exception
        {
            //Initializ the datasource in the backround
            dataSource.initialize(context, inputParameters);
            return new DataModel(dataSource);
        }

        @Override
        protected void done()
        {
            if (!isCancelled())
            {
                try
                {
                    //Remove the item listener, if it exists, so that we do not trigger on data loading events, such as refresh
                    removeItemListener((ItemListener) getComponent());

                    //Set the new combobox model the data retrieved from the data source
                    setModel(get());

                    //We do not want anything to be selected initially
                    setSelectedIndex(-1);

                    //Set before we add the listener so the filter does not trigger
                    if (getItemCount() > 0)
                    {
                        //Check if the first item is null, if it is it is probably a display field so lets select it
                        if (getItemAt(0) == null || (getItemAt(0) != null && ((T8FilterPanelListComponentItem) getItemAt(0)).getValue() == null))
                        {
                            setSelectedIndex(0);
                        }
                    }
                    //First check if there is any data to display, if not then disable the combo box and tell the user
                    if (getModel().getSize() < 1)
                    {
                        setEnabled(false);
                        setModel(new ListComboBoxModel(Arrays.asList(new T8FilterPanelListComponentItem(getTranslatedString("No data.."), null))));
                    }
                    else if (getModel().getSize() == 1 && (getModel().getElementAt(0) != null && ((T8FilterPanelListComponentItem) getModel().getElementAt(0)).getValue() == null))
                    {
                        //This is probably only a display item with no other items to show, so dont enable the component because it would be useless
                        setEnabled(false);
                    }
                    else
                    {
                        //Enable the combo box so the user can make changes
                        setEnabled(true);
                    }

                    //Add the item listener last so that the selection change is not triggered while we change selections initially
                    addItemListener((ItemListener) getComponent());

                    if(showPopupOnCompletion && isEnabled())
                    {
                        //Make sure to only invoke the show later so that the EDT can finish processing layout events etc that need to
                        //happen before hand
                        SwingUtilities.invokeLater(new Runnable()
                        {

                            @Override
                            public void run()
                            {
                                showPopup();
                            }
                        });
                    }
                }
                catch (Exception ex)
                {
                    //If the data loading failed just set the combobox to a new empty datasource stating the fact.
                    setModel(new ListComboBoxModel(Arrays.asList(new T8FilterPanelListComponentItem(getTranslatedString("Failed to load data.."), null))));

                    //Select the first item so the user can see the message
                    setSelectedIndex(0);

                    //Disable the combo box so the user can not make any changes or do any other stupid things users tend to do
                    setEnabled(false);
                    T8Log.log("Failed to load data source for " + definition.getPublicIdentifier(), ex);
                }
            }
        }
    }
}
