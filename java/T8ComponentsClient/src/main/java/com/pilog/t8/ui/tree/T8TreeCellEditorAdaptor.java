package com.pilog.t8.ui.tree;

import com.pilog.t8.T8Log;
import com.pilog.t8.ui.tree.T8TreeNode.NodeType;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeNodeDefinition;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class T8TreeCellEditorAdaptor extends AbstractCellEditor implements TreeCellEditor
{
    private T8TreeCellEditableComponent editableComponent;
    private T8TreeCellEditorComponent editorComponent;
    private Map<String, T8TreeCellEditorComponent> editorComponents;
    private Map<String, T8TreeCellEditorComponent> groupEditorComponents;
    private T8TreeLoadingCellRenderer loadingCellRenderer;
    private T8TreeNodeEditorListener editorListener;
    private T8TreeDefinition treeDefinition;
    private T8TreeNode editedNode;
    
    public T8TreeCellEditorAdaptor(T8TreeCellEditableComponent editableComponent, T8TreeDefinition treeDefinition, T8TreeNodeEditorListener editorListener)
    {
        this.editableComponent = editableComponent;
        this.editorComponents = new HashMap<>();
        this.groupEditorComponents = new HashMap<>();
        this.loadingCellRenderer = new T8TreeLoadingCellRenderer((JComponent)editableComponent);
        this.editorListener = editorListener;
        this.treeDefinition = treeDefinition;
        cacheNodeEditors(treeDefinition.getRootNodeDefinition());
    }
    
    private void cacheNodeEditors(T8TreeNodeDefinition nodeDefinition)
    {
        T8ComponentDefinition editorDefinition;
        List<T8TreeNodeDefinition> childNodeDefinitions;

        // Get the editor definition of the node and instantiate an editor component if available.
        editorDefinition = nodeDefinition.getEditorComponentDefinition();
        if (editorDefinition != null)
        {
            try
            {
                T8TreeCellEditorComponent editorComponent;

                editorComponent = (T8TreeCellEditorComponent)editorDefinition.getNewComponentInstance(editableComponent.getController());
                editorComponent.initializeComponent(null);
                editorComponent.addNodeEditorListener(editorListener);
                editorComponents.put(nodeDefinition.getIdentifier(), editorComponent);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while instantiating tree cell editor component: " + editorDefinition, e);
            }
        }
        
//        // Get the group editor definition of the node and instantiate an editor component if available.
//        editorDefinition = nodeDefinition.getGroupEditorComponentDefinition();
//        if (editorDefinition != null)
//        {
//            T8TreeCellEditorComponent editorComponent;
//            
//            editorComponent = (T8TreeCellEditorComponent)editorDefinition.getNewComponentInstance(editableComponent.getController());
//            editorComponent.initializeComponent(null);
//            groupEditorComponents.put(nodeDefinition.getIdentifier(), editorComponent);
//        }
        
        // Cache the renderers of the descendent nodes.
        childNodeDefinitions = nodeDefinition.getChildNodeDefinitions();
        if (childNodeDefinitions != null)
        {
            for (T8TreeNodeDefinition childNodeDefinition : childNodeDefinitions)
            {
                cacheNodeEditors(childNodeDefinition);
            }
        }
    }
    
    public void startAnimation()
    {
        T8Log.log("Starting animation.");
        loadingCellRenderer.startAnimation();
    }
    
    public void stopAnimation()
    {
        T8Log.log("Stopping animation.");
        loadingCellRenderer.stopAnimation();
    }
    
    @Override
    public boolean isCellEditable(EventObject eventObject)
    {
        if (eventObject instanceof MouseEvent)
        {
            TreePath selectionPath;
            JTree tree;

            tree = (JTree)eventObject.getSource();

            selectionPath = tree.getPathForLocation(((MouseEvent)eventObject).getX(), ((MouseEvent)eventObject).getY());
            if ((selectionPath != null) && (selectionPath.getPathCount() > 0))
            {
                T8TreeNode node;

                node = (T8TreeNode)selectionPath.getLastPathComponent();
                return editorComponents.containsKey(node.getIdentifier());
            }
            else return false;
        }
        else return false;
    }

    @Override
    public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row)
    {
        T8TreeNodeDefinition nodeDefinition;
        
        editedNode = (T8TreeNode)value;
        nodeDefinition = treeDefinition.getNodeDefinition(editedNode.getIdentifier());
        if (nodeDefinition != null)
        {
            String nodeIdentifier;
            
            // Get the node identifier from its definition.
            nodeIdentifier = nodeDefinition.getIdentifier();

            // Render the node according to its type.
            if (editedNode.getNodeType() == NodeType.GROUP)
            {
                // Find the cached renderer component and use it if available.
                if (groupEditorComponents.containsKey(nodeIdentifier))
                {
                    // Set the editor component content and return it.
                    editorComponent = groupEditorComponents.get(nodeIdentifier);
                    editorComponent.setEditorNode(editableComponent, editedNode, true, true, true, expanded, leaf, row, editedNode.getChildNodePageNumber(), editedNode.getChildNodePageCount());
                    return (Component)editorComponent;
                }
                else
                {
                    // No editor defined, so return null;
                    editorComponent = null;
                    return (Component)editorComponent;
                }
            }
            else
            {
                // Find the cached renderer component and use it if available.
                if (editorComponents.containsKey(nodeIdentifier))
                {
                    // Set the editor component content and return it.
                    editorComponent = editorComponents.get(nodeIdentifier);
                    editorComponent.setEditorNode(editableComponent, editedNode, true, true, true, expanded, leaf, row, editedNode.getChildNodePageNumber(), editedNode.getChildNodePageCount());
                    return (Component)editorComponent;
                }
                else
                {
                    // No editor defined, so return null;
                    editorComponent = null;
                    return (Component)editorComponent;
                }
            }
        }
        else // The node does not contain a definition, so just render an empty String as its display value.
        {
            return loadingCellRenderer;
        }
    }
    
    @Override
    public Object getCellEditorValue()
    {
        return editedNode;
    }
}
