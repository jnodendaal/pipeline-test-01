package com.pilog.t8.ui.datarecord;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.ui.datarecord.celleditor.T8RecordTableCellEditor;
import com.pilog.t8.ui.datarecord.celleditor.T8RecordTableCellRenderer;
import com.pilog.t8.ui.datarecord.celleditor.TableCellPropertyResolver;
import com.pilog.t8.utilities.components.celleditor.ComboBoxTableCellEditor;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * @author Bouwer du Preez
 */
public class FilterTable extends JTable
{
    private DefaultTableModel tableModel;
    private List<RecordPropertyTableColumnDefinition> filterColumnDefinitions;
    private TableCellPropertyResolver propertyResolver;

    public FilterTable(List<RecordPropertyTableColumnDefinition> filterColumnDefinitions, TableCellPropertyResolver propertyResolver)
    {
        this.filterColumnDefinitions = filterColumnDefinitions;
        this.propertyResolver = propertyResolver;
        tableModel = new javax.swing.table.DefaultTableModel
        (
            new Object [][]
            {

            },
            new String []
            {
                "Column Name", "Operator", "Filter Value"
            }
        )
        {
            boolean[] canEdit = new boolean []
            {
                false, true, true
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        };

        setModel(tableModel);
        setRowHeight(25);
        getColumnModel().getColumn(1).setPreferredWidth(50);
        getColumnModel().getColumn(1).setCellEditor(new ComboBoxTableCellEditor(true, getFilterOperators()));
        getColumnModel().getColumn(2).setCellRenderer(new T8RecordTableCellRenderer(propertyResolver));
        getColumnModel().getColumn(2).setCellEditor(new T8RecordTableCellEditor(null, propertyResolver));
    }

    private List<T8DataFilterCriterion.DataFilterOperator> getFilterOperators()
    {
        ArrayList<T8DataFilterCriterion.DataFilterOperator> operators;

        // Only specific operators can be used on the Data Record filter.
        operators = new ArrayList<>();
        operators.add(T8DataFilterCriterion.DataFilterOperator.EQUAL);
        operators.add(T8DataFilterCriterion.DataFilterOperator.LIKE);
        return operators;
    }

    public void rebuildFilterTable(List<RecordPropertyTableColumnDefinition> columnDefinitions)
    {
        clearFilterTable();
        this.filterColumnDefinitions = columnDefinitions;
        for (RecordPropertyTableColumnDefinition columnDefinition : filterColumnDefinitions)
        {
            Object[] dataRow;

            dataRow = new Object[3];
            dataRow[0] = columnDefinition.getColumnName();
            dataRow[1] = null;
            dataRow[2] = null;

            tableModel.addRow(dataRow);
        }
    }

    public T8DataFilterCriteria getFilterCriteria()
    {
        T8DataFilterCriteria filterCriteria;
        T8DataRecordValueStringGenerator valueStringGenerator;

        // The configuration of this Value String generator is very important
        // because the format of the generated String must match the format of
        // the PROPERTY value Strings in the database in order for the filter to
        // work properly.
        valueStringGenerator = new T8DataRecordValueStringGenerator();
        valueStringGenerator.setIncludePropertyConcepts(false);

        stopEditing();
        filterCriteria = new T8DataFilterCriteria();
        for (int columnIndex = 0; columnIndex < filterColumnDefinitions.size(); columnIndex++)
        {
            if (tableModel.getValueAt(columnIndex, 1) != null)
            {
                Object filterValue;

                filterValue = tableModel.getValueAt(columnIndex, 2);
                if (filterValue != null)
                {
                    RecordProperty recordProperty;
                    String propertyValueString;
                    String fieldIdentifier;

                    recordProperty = (RecordProperty)filterValue;
                    propertyValueString = valueStringGenerator.generateValueString(recordProperty).toString();
                    fieldIdentifier = "${" + recordProperty.getPropertyID() + "}";
                    filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, fieldIdentifier, (T8DataFilterCriterion.DataFilterOperator)tableModel.getValueAt(columnIndex, 1), propertyValueString);
                }
            }
        }

        return filterCriteria;
    }

    private void clearFilterTable()
    {
        while (tableModel.getRowCount() > 0) tableModel.removeRow(0);
    }

    public void clearFilterValues()
    {
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            tableModel.setValueAt(null, rowIndex, 1);
            tableModel.setValueAt(null, rowIndex, 2);
        }

        repaint();
    }

    public void stopEditing()
    {
        if (getCellEditor() != null) getCellEditor().stopCellEditing();
    }

    public int getFocusedRowIndex()
    {
        return getSelectionModel().getLeadSelectionIndex();
    }

    public void setFocusedRowIndex(int rowIndex)
    {
        getSelectionModel().setLeadSelectionIndex(rowIndex);
    }

    public int getFocusedColumnIndex()
    {
        return getColumnModel().getSelectionModel().getLeadSelectionIndex();
    }

    public void setFocusedColumnIndex(int columnIndex)
    {
        getColumnModel().getSelectionModel().setLeadSelectionIndex(columnIndex);
    }

    public int getRowIndexForLocation(Point location)
    {
        return rowAtPoint(location);
    }

    public int getColumnIndexForLocation(Point location)
    {
        return columnAtPoint(location);
    }
}
