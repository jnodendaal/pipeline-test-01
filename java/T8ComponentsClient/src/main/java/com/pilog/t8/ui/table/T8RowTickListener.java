package com.pilog.t8.ui.table;

/**
 * @author Bouwer du Preez
 */
public interface T8RowTickListener
{
    public void tickSelectionChanged(T8RowTickSelectionChangedEvent event);
}
