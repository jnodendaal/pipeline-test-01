/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.filter.panel.component.model;

import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItem;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItemDataSource;
import java.util.ArrayList;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelDataSourceModel implements
        ListModel<T8FilterPanelListComponentItem>
{
    private static final T8Logger logger = com.pilog.t8.T8Log.getLogger(T8FilterPanelDataSourceModel.class.getName());
    private ArrayList<T8FilterPanelListComponentItem> _items;
    private ArrayList<ListDataListener> _listeners;
    private T8FilterPanelListComponentItem _selectedItem;

    public T8FilterPanelDataSourceModel()
    {
        this(new ArrayList<T8FilterPanelListComponentItem>());
    }

    public T8FilterPanelDataSourceModel(
            T8FilterPanelListComponentItemDataSource dataSource) throws Exception
    {
        this();
        boolean next = dataSource.hasNext();
        int failCount = 0;
        while (next)
        {
            try
            {
                T8FilterPanelListComponentItem item = dataSource.next();
                //Do not add duplicates
                if (!_items.contains(item))
                {
                    _items.add(item);
                }
                next = dataSource.hasNext();
            }
            catch (Exception ex)
            {
                logger.log(ex);
                failCount++;
                if (failCount > 3)
                {
                    next = false;
                }
            }
        }
    }

    public T8FilterPanelDataSourceModel(
            ArrayList<T8FilterPanelListComponentItem> _items)
    {
        this._items = _items;
        this._listeners = new ArrayList<>();
        this._selectedItem = null;
    }

    public void addElement(T8FilterPanelListComponentItem element)
    {
        //Do not add duplicates
        if (!_items.contains(element))
        {
            _items.add(element);
        }
    }

    public void addElementAt(int index, T8FilterPanelListComponentItem element)
    {
        _items.add(index, element);
    }

    @Override
    public int getSize()
    {
        return _items.size();
    }

    @Override
    public T8FilterPanelListComponentItem getElementAt(int index)
    {
        return _items.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l)
    {
        _listeners.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l)
    {
        _listeners.remove(l);
    }
}
