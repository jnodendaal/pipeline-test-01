package com.pilog.t8.ui.celleditor.table.attachment;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.cellrenderer.table.attachment.T8AttachmentThumbnail;
import com.pilog.t8.ui.table.T8TableCellEditableComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.celleditor.table.attachment.T8AttachmentTableCellEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Day;
import com.pilog.t8.ui.attachment.T8AttachmentViewer;
import com.pilog.t8.ui.dialog.exception.T8ExceptionDialog;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingWorker;

/**
 * @author Hennie Brink
 */
public class T8AttachmentTableCellEditor extends T8AttachmentViewer implements T8TableCellEditorComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8AttachmentTableCellEditor.class);
    private final T8AttachmentTableCellEditorDefinition definition;
    private T8Context context;
    private Map<String, Object> editingRow;

    public T8AttachmentTableCellEditor(T8ComponentController controller, T8AttachmentTableCellEditorDefinition definition)
    {
        super(controller);
        this.definition = definition;
        this.context = controller.getContext();
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        super.initializeComponent(inputParameters);
    }

    @Override
    public boolean singleClickEdit()
    {
        return false;
    }

    @Override
    public void setEditorData(T8TableCellEditableComponent sourceComponent, Map<String, Object> dataRow, String fieldIdentifier, Map<EditParameter, Object> editParameters)
    {
        T8AttachmentThumbnail thumbnail;

        thumbnail = (T8AttachmentThumbnail)sourceComponent.getAdditionalRenderData((int) editParameters.get(EditParameter.ROW), fieldIdentifier);
        setAttachmentThumbnail(thumbnail);
        setSelected((boolean) editParameters.get(EditParameter.SELECTED));
        setFocused(true);
        setLoading(false);
        setBorder(sourceComponent.getUnselectedBorder());

        if (isFocused())
        {
            setBorder(sourceComponent.getFocusedBorder());
        }
        if (isSelected())
        {
            setBorder(sourceComponent.getSelectedBorder());
        }
        if (isFocused() && isSelected())
        {
            setBorder(sourceComponent.getFocusedSelectedBorder());
        }
        editingRow = dataRow;
        loadImage();
    }

    @Override
    public Map<String, Object> getEditorData()
    {
        //We dont really allow edits, we just want to enable the user to view and download attachments
        return new HashMap<>();
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
    }

    private void loadImage()
    {
        try
        {
            T8FileDetails fileDetails;
            File tempFile;
            T8FileManager fileManager;
            T8ClientContext clientContext;
            String fileName;
            String fileContextId;
            String fileContextIid;
            String fileExtension;

            fileDetails = getAttachmentThumbnail().getFileDetails();
            fileContextId = fileDetails.getContextId();
            fileContextIid = fileDetails.getContextIid();
            fileName = fileDetails.getFileName();
            fileName = fileName.substring(0, fileName.lastIndexOf('.'));
            fileExtension = fileDetails.getFileName();
            fileExtension = fileExtension.substring(fileExtension.lastIndexOf('.'));

            clientContext = getController().getClientContext();
            fileManager = clientContext.getFileManager();

            if(fileContextId == null)
            {
                fileContextId = fileDetails.getFilePath();
            }

            tempFile = File.createTempFile(fileName, fileExtension);
            tempFile.deleteOnExit();

            fileManager.openFileContext(context, fileContextIid, fileContextId, new T8Day(1));

            clientContext.lockUI(translate("Downloading File " + fileName));

            new FileDownloadListener(fileManager, tempFile, fileContextIid).execute();
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to load file", ex);
            T8ExceptionDialog.showExceptionDialog(context, "File Load Failed", "Failed to load the attachment", ex);
        }
    }

    private class FileDownloadListener extends SwingWorker<Void, Long>
    {
        private final T8FileManager fileManager;
        private final String fileContextIid;
        private final File file;

        FileDownloadListener(T8FileManager fileManager, File file, String fileContextIID)
        {
            this.fileManager = fileManager;
            this.file = file;
            this.fileContextIid = fileContextIID;
        }

        @Override
        protected Void doInBackground() throws Exception
        {
            long progress;

            progress = fileManager.downloadFile(context, fileContextIid, file.getPath(), getAttachmentThumbnail().getAttachmentID());

            while (progress < 100 && progress != -1)
            {
                progress = fileManager.getDownloadOperationProgress(context, file.getPath());
                publish(progress);

                Thread.sleep(100);
            }

            return null;
        }

        @Override
        protected void done()
        {
            try
            {
                T8AttachmentTableCellEditorImageViewer imageViewer;
                Dimension parentSize;
                Double dialogWidth;
                Double dialogHeight;

                get();
                imageViewer = new T8AttachmentTableCellEditorImageViewer(null, true);
                imageViewer.paintComponent(file);

                parentSize = getController().getClientContext().getParentWindow().getSize();
                dialogWidth = parentSize.width * (80/100.00);
                dialogHeight = parentSize.height * (80/100.00);
                imageViewer.setSize(dialogWidth.intValue(), dialogHeight.intValue());

                imageViewer.setLocationRelativeTo(null);
                imageViewer.setVisible(true);
            }
            catch (Exception e)
            {
                LOGGER.log("Failed to open downloaded file", e);
                T8ExceptionDialog.showExceptionDialog(context, translate("Download Failed"), translate("Failed to download the selected file to view"), e);
            }
            finally
            {
                getController().getClientContext().unlockUI();
                try
                {
                    if (fileManager.fileContextExists(context, fileContextIid))
                    {
                        fileManager.closeFileContext(context, fileContextIid);
                    }
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to close file context " + fileContextIid, ex);
                }
            }
        }
    }
}
