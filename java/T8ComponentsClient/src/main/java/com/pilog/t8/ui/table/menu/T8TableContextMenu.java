package com.pilog.t8.ui.table.menu;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.table.menu.T8TableContextMenuDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.table.T8Table;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * @author Gavin Boshoff
 */
public class T8TableContextMenu extends JPopupMenu implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8TableContextMenu.class);

    private final T8TableContextMenuOperationHandler operationHandler;
    /** The set of standard menu items. */
    private final Map<StandardMenuItem, JMenuItem> standardMenus;
    private final T8ConfigurationManager configurationManager;
    private final T8TableContextMenuDefinition definition;
    /** Menu items defined through the definition. */
    private final Map<String, T8Component> additionalMenus;
    /** The {@code T8ComponentController} pushed through from the table. */
    private final T8ComponentController controller;
    private final T8Context context;

    /** The table to which the context menu belongs/applies. */
    private T8Table originTable;
    /** The index of the column on which the menu was displayed. */
    private int columnIndex;
    /** The index of the row on which the menu was displayed. */
    private int rowIndex;

    /**
     * An {@code enum} which defines a set of standard menu items with their
     * associated labels. These can be added to, to handle standard context
     * menu functionality on a table.
     */
    public enum StandardMenuItem
    {
        COPY_ROWS("Copy Rows..."),
        EXPORT("Export..."),
        FILL_DOWN("Fill Down...");

        private final String label;

        private StandardMenuItem(String label)
        {
            this.label = label;
        }

        private String getLabel()
        {
            return this.label;
        }
    };

    public T8TableContextMenu(T8TableContextMenuDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;

        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.context = controller.getContext();

        this.operationHandler = new T8TableContextMenuOperationHandler(this);
        this.standardMenus = new EnumMap<>(StandardMenuItem.class);
        this.additionalMenus = new HashMap<>();

        this.rowIndex = -1;
    }

    @Override
    public T8ComponentController getController()
    {
        return this.controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return this.operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return this.definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.originTable = (T8Table)inputParameters.get("$P_TABLE");
        addStandardMenuItems();

        this.definition.getChildComponentDefinitions().stream().forEach((childComponentDefinition) -> this.controller.addComponent(this, childComponentDefinition, null));
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        // We want to make sure we remove all listeners to prevent any lasting references
        clearChildComponents();
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        if (childComponent instanceof JMenuItem) addAdditionalMenu(childComponent);
        else LOGGER.log(T8Logger.Level.ERROR, "Found invalid component type: " + childComponent.getClass().getSimpleName());
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return this.additionalMenus.get(childComponentIdentifier);
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        JMenuItem additionalMenu;

        additionalMenu = (JMenuItem)this.additionalMenus.remove(childComponentIdentifier);
        // Remove the action listener
        for (ActionListener actionListener : additionalMenu.getActionListeners())
        {
            additionalMenu.removeActionListener(actionListener);
        }
        // Remove the menu item
        this.remove(additionalMenu);

        return (T8Component)additionalMenu;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(this.additionalMenus.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return this.additionalMenus.size();
    }

    @Override
    public void clearChildComponents()
    {
        new ArrayList<>(this.additionalMenus.keySet()).stream().forEach(this::removeChildComponent);
    }

    public void setCurrentColumnIndex(int columnIndex)
    {
        this.columnIndex = columnIndex;
    }

    public int getCurrentColumnIndex()
    {
        return this.columnIndex;
    }

    public void setCurrentRowIndex(int rowIndex)
    {
        this.rowIndex = rowIndex;
    }

    public int getCurrentRowIndex()
    {
        return this.rowIndex;
    }

    void setVisible(StandardMenuItem menuItem, boolean visible)
    {
        this.standardMenus.get(menuItem).setVisible(visible);
    }

    void setVisible(String menuItemIdentifier, boolean visible)
    {
        T8Component menuItemComponent;

        menuItemComponent = this.additionalMenus.get(menuItemIdentifier);
        if (menuItemComponent != null)
        {
            ((JMenuItem)menuItemComponent).setVisible(visible);
        }
    }

    private String translate(String phrase)
    {
        return this.configurationManager.getUITranslation(context, phrase);
    }

    private void addStandardMenuItems()
    {
        addStandardMenuItem(StandardMenuItem.COPY_ROWS, this.definition.isHideCopyRows(), e -> this.originTable.copyRows(this.rowIndex));
        addStandardMenuItem(StandardMenuItem.EXPORT, this.definition.isHideExport(), e -> this.originTable.export());
        addStandardMenuItem(StandardMenuItem.FILL_DOWN, this.definition.isHideFillDown(), e -> this.originTable.fillDown(this.rowIndex, this.columnIndex));
    }

    private void addStandardMenuItem(StandardMenuItem standardItem, boolean hidden, ActionListener l)
    {
        JMenuItem menuItem;

        menuItem = new JMenuItem(translate(standardItem.getLabel()));
        menuItem.addActionListener(l);
        menuItem.setVisible(!hidden);
        add(menuItem);

        this.standardMenus.put(standardItem, menuItem);
    }

    private void addAdditionalMenu(T8Component additionalMenu)
    {
        this.additionalMenus.put(additionalMenu.getComponentDefinition().getIdentifier(), additionalMenu);
        this.add((JMenuItem)additionalMenu);
    }
}