package com.pilog.t8.ui.entityeditor.checkbox;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.ui.entityeditor.textfield.T8TextFieldDataEntityEditorAPIHandler;
import com.pilog.t8.ui.checkbox.T8CheckBoxOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CheckBoxDataEntityEditorOperationHandler extends T8CheckBoxOperationHandler
{
    private T8CheckBoxDataEntityEditor checkBoxDataEntityEditor;

    public T8CheckBoxDataEntityEditorOperationHandler(T8CheckBoxDataEntityEditor textField)
    {
        super(textField);
        this.checkBoxDataEntityEditor = textField;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_COMMIT_CHANGES))
        {
            checkBoxDataEntityEditor.commitEditorChanges();
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_SET_DATA_ENTITY))
        {
            checkBoxDataEntityEditor.setEditorDataEntity((T8DataEntity)operationParameters.get(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY));
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_GET_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY, checkBoxDataEntityEditor.getEditorDataEntity());
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
