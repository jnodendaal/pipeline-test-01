package com.pilog.t8.ui.filter.panel.component;

import com.pilog.t8.T8Log;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.definition.ui.filter.panel.component.T8FilterPanelComponentAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.ui.filter.panel.T8FilterPanelComponent;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8FilterPanelComponentOperationHandler extends T8ComponentOperationHandler
{
    private final T8FilterPanelComponent filterPanelComponent;

    public T8FilterPanelComponentOperationHandler(T8FilterPanelComponent wizard)
    {
        super((T8Component)wizard);
        this.filterPanelComponent = wizard;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier,
                                                Map<String, Object> operationParameters)
    {
        if(T8FilterPanelComponentAPIHandler.OPERATION_REFRESH.equals(operationIdentifier))
        {
            try
            {
                filterPanelComponent.refresh();
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to refresh filter panel component.", ex);
            }
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
