package com.pilog.t8.ui.datasearch;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterExpressionParser;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.datasearch.T8DataSearchAPIHandler;
import com.pilog.t8.definition.ui.datasearch.T8DataSearchDefinition;
import com.pilog.t8.definition.ui.datasearch.T8DataSearchDefinition.T8SearchType;
import com.pilog.t8.ui.dialog.exception.T8ExceptionDialog;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class T8DataSearch extends JXPanel implements T8Component
{
    private final T8ComponentController controller;
    private final T8DataSearchDefinition definition;
    private final T8DataSearchOperationHandler operationHandler;
    private final T8QuickDataSearch quickSearchPanel;
    private final DataFilterConjunction defaultConjunction;
    private final T8DataFilterExpressionParser expressionParser;
    private final boolean fullTextEnabled;
    private T8SearchType quickSearchType;

    public T8DataSearch(T8DataSearchDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8DataSearchOperationHandler(this);
        this.quickSearchPanel = new T8QuickDataSearch(this);
        this.quickSearchType = definition.getSearchType();
        this.defaultConjunction = definition.getWhitespaceConjunction();
        this.fullTextEnabled = definition.isFullTextEnabled();
        this.expressionParser = new T8DataFilterExpressionParser(definition.getDataEntityIdentifier(), definition.getSearchFieldIdentifiers());
        this.expressionParser.setWhitespaceConjunction(defaultConjunction);
        initComponents();
        this.jComboBoxSearchType.setRenderer(new SearchTypeRenderer());
        if (definition.isSearchTypeSwitchEnabled()) add(jPanelSearchType, BorderLayout.WEST);
        add(quickSearchPanel, BorderLayout.CENTER);
        revalidate();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8DataSearchDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        DefaultComboBoxModel model;

        // Add all of the search types to the search type combo box.
        model = (DefaultComboBoxModel)jComboBoxSearchType.getModel();
        for (T8SearchType searchType : T8SearchType.values())
        {
            model.addElement(searchType);
        }

        // Set the selected search type (default).
        jComboBoxSearchType.setSelectedItem(definition.getSearchType());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void showQuickSearch()
    {
        removeAll();
        add(quickSearchPanel, java.awt.BorderLayout.CENTER);
        revalidate();
    }

    public void doQuickSearch(String searchText)
    {
        String entityId;

        // Get the identifierof the entity to be searched.
        entityId = definition.getDataEntityIdentifier();
        if (entityId != null)
        {
            HashMap<String, Object> eventParameters;
            T8DataFilter dataFilter = null;
            List<String> searchTerms = null;

            // Parse the data filter from the search expression.
            try
            {
                if (quickSearchType == T8SearchType.QUICK)
                {
                    dataFilter = expressionParser.parseQuickExpression(searchText, fullTextEnabled);
                    searchTerms = expressionParser.parseQuickExpressionSearchTerms(searchText);
                }
                else
                {
                    dataFilter = expressionParser.parseExpression(searchText, fullTextEnabled);
                    searchTerms = expressionParser.parseExpressionSearchTerms(searchText);
                }
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to parse expression " + searchText, ex);
                T8ExceptionDialog.showExceptionDialog(controller.getContext(), "Invalid Expression", "Failed to parse the supplied expression, please ensure it is correct", ex);
            }

            // Fire the search event.
            eventParameters = new HashMap<>();
            eventParameters.put(T8DataSearchAPIHandler.PARAMETER_SEARCH_TYPE, quickSearchType);
            eventParameters.put(T8DataSearchAPIHandler.PARAMETER_SEARCH_TERMS, searchTerms);
            eventParameters.put(T8DataSearchAPIHandler.PARAMETER_DATA_FILTER, dataFilter);
            controller.reportEvent(this, definition.getComponentEventDefinition(T8DataSearchAPIHandler.EVENT_SEARCH), eventParameters);
        }
        else throw new RuntimeException("No Search Data entity set.");
    }

    public void clearSearchText(boolean search)
    {
        this.quickSearchPanel.clearSearch(search);
    }

    public void setSearchText(String text)
    {
        quickSearchPanel.setSearchString(text);
    }

    public String getSearchText()
    {
        return quickSearchPanel.getSearchString();
    }

    public void setSearchFields(List<String> fieldIds)
    {
        expressionParser.setFieldIds(fieldIds);
    }

    private static class SearchTypeRenderer extends DefaultListCellRenderer
    {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            T8SearchType searchType;

            searchType = (T8SearchType)value;
            return super.getListCellRendererComponent(list, searchType.getDisplayName(), index, isSelected, cellHasFocus);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelSearchType = new javax.swing.JPanel();
        jComboBoxSearchType = new javax.swing.JComboBox<>();

        jPanelSearchType.setOpaque(false);
        jPanelSearchType.setLayout(new java.awt.GridBagLayout());

        jComboBoxSearchType.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                jComboBoxSearchTypeItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelSearchType.add(jComboBoxSearchType, gridBagConstraints);

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxSearchTypeItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_jComboBoxSearchTypeItemStateChanged
    {//GEN-HEADEREND:event_jComboBoxSearchTypeItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            this.quickSearchType = (T8SearchType)jComboBoxSearchType.getSelectedItem();
        }
    }//GEN-LAST:event_jComboBoxSearchTypeItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> jComboBoxSearchType;
    private javax.swing.JPanel jPanelSearchType;
    // End of variables declaration//GEN-END:variables
}
