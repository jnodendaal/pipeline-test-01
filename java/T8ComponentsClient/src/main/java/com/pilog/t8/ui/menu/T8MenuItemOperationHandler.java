package com.pilog.t8.ui.menu;

import com.pilog.t8.definition.ui.menu.T8MenuAPIHandler;
import com.pilog.t8.definition.ui.menuitem.T8MenuItemAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8MenuItemOperationHandler extends T8ComponentOperationHandler
{
    private T8Menu menu;

    public T8MenuItemOperationHandler(T8Menu menu)
    {
        super(menu);
        this.menu = menu;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if(T8MenuAPIHandler.OPERATION_SET_TEXT.equals(operationIdentifier))
        {
            String text = (String) operationParameters.get(T8MenuItemAPIHandler.PARAMETER_TEXT);
            menu.setText(text);
            return null;
        } else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
