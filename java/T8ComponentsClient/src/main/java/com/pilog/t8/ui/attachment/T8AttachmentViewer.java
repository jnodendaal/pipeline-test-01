package com.pilog.t8.ui.attachment;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.cellrenderer.table.attachment.T8AttachmentThumbnail;
import com.pilog.t8.utilities.strings.StringUtilities;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JComponent;

/**
 * @author Hennie Brink
 */
public abstract class T8AttachmentViewer extends JComponent implements T8Component
{
    private final T8ComponentController controller;
    private T8AttachmentThumbnail attachmentThumbnail;
    private boolean selected;
    private boolean focused;
    private boolean loading;
    private String loadingString;

    public T8AttachmentViewer(T8ComponentController controller)
    {
        this.controller = controller;
        this.loading = true;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        loadingString = translate("Loading...");
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        Graphics2D g2;

        g2 = (Graphics2D) g.create();
        if (loading)
        {
            FontMetrics fontMetrics;
            Insets insets;
            int x;
            int y;

            fontMetrics = g2.getFontMetrics();
            insets = getBorder().getBorderInsets(this);

            y = insets.top;
            y += getHeight() / 2;
            y -= insets.bottom;

            x = insets.left;
            x += getWidth() / 2;
            x -= insets.right;

            x -= fontMetrics.stringWidth(loadingString) / 2;
            y += fontMetrics.getHeight() / 2;

            g2.drawString(loadingString, x, y);
        }
        else if (attachmentThumbnail != null)
        {
            Insets insets;
            Rectangle clip;
            Image thumbImage;

            insets = getBorder().getBorderInsets(this);
            clip = new Rectangle();
            clip.x = insets.left;
            clip.y = insets.top;
            clip.height = getHeight() - insets.bottom;
            clip.width = getWidth() - insets.right;

            thumbImage = attachmentThumbnail.getThumbnail().getImage();

            if (thumbImage.getHeight(null) > clip.height)
            {
                thumbImage = thumbImage.getScaledInstance(-1, clip.height, Image.SCALE_FAST);
            }

            int x;
            int y;

            x = clip.x;
            x += clip.width / 2;
            x -= thumbImage.getWidth(null) / 2;

            y = clip.y;
            y += clip.height / 2;
            y -= thumbImage.getHeight(null) / 2;

            g2.drawImage(thumbImage, x, y, null);
        }

        getBorder().paintBorder(this, g2, 0, 0, getWidth(), getHeight());
        g2.dispose();
    }

    @Override
    public Dimension getPreferredSize()
    {
        if (loading)
        {
            FontMetrics fontMetrics;
            int width;
            int height;

            fontMetrics = getFontMetrics(getFont());

            width = fontMetrics.stringWidth(loadingString);
            height = fontMetrics.getHeight();

            width += 10;
            height += 10;

            return new Dimension(width, height);
        }
        else if (attachmentThumbnail != null)
        {
            int width;
            int height;

            width = attachmentThumbnail.getThumbnail().getIconWidth();
            height = attachmentThumbnail.getThumbnail().getIconHeight();

            return new Dimension(width, height);
        }
        else return new Dimension(5, 5);
    }

    protected String translate(String text)
    {
        return controller.getClientContext().getConfigurationManager().getUITranslation(controller.getContext(), text);
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setFocused(boolean focused)
    {
        this.focused = focused;
    }

    public boolean isFocused()
    {
        return focused;
    }

    public boolean isLoading()
    {
        return loading;
    }

    public void setLoading(boolean loading)
    {
        this.loading = loading;
    }

    public void setAttachmentThumbnail(T8AttachmentThumbnail newThumbnail)
    {
        this.attachmentThumbnail = newThumbnail;
        if (attachmentThumbnail != null)
        {
            StringBuilder tootipText;

            tootipText = new StringBuilder("<html>");

            tootipText.append("<div style=\"width: 100px;\">");
            tootipText.append("<h4><em>");
            tootipText.append(translate("File Name:"));
            tootipText.append("</em></h4>");
            tootipText.append("<p align=\"right\">");
            tootipText.append(attachmentThumbnail.getFileDetails().getFileName());
            tootipText.append("</p>");
            tootipText.append("</div>");

            tootipText.append("<div>");
            tootipText.append("<h4><em>");
            tootipText.append(translate("File Size:"));
            tootipText.append("</em></h4>");
            tootipText.append("<p align=\"right\">");
            tootipText.append(StringUtilities.getFileSizeString(attachmentThumbnail.getFileDetails().getFileSize()));
            tootipText.append("</p>");
            tootipText.append("</div>");

            tootipText.append("</html>");
            setToolTipText(tootipText.toString());
        }
        else
        {
            setToolTipText(null);
        }
    }

    public T8AttachmentThumbnail getAttachmentThumbnail()
    {
        return attachmentThumbnail;
    }

    //<editor-fold defaultstate="collapsed" desc="T8Component Implementation Methods">
    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }
//</editor-fold>
}
