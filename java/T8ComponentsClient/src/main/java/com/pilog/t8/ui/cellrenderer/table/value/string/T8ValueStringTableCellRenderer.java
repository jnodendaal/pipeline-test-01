package com.pilog.t8.ui.cellrenderer.table.value.string;

import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellRenderableComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8LRUTerminologyCache;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.cellrenderer.table.label.T8LabelTableCellRendererDefinition;
import com.pilog.t8.definition.ui.cellrenderer.table.value.string.T8ValueStringTableCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.cellrenderer.table.label.T8LabelTableCellRenderer;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.strings.StringUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.JComponent;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8ValueStringTableCellRenderer extends JComponent implements T8TableCellRendererComponent
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8ValueStringTableCellRenderer.class.getName());
    private final T8ValueStringTableCellRendererDefinition definition;
    private final T8ComponentController controller;
    private final T8Context context;
    private T8LabelTableCellRenderer tableCellRenderer;
    private T8LRUTerminologyCache terminologyCache;
    private final T8DataRecordValueStringGenerator valueStringGenerator;
    private final DataHandler dataHandler;
    private final T8TerminologyClientApi trmApi;
    private final String languageId;

    public T8ValueStringTableCellRenderer(T8ValueStringTableCellRendererDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.languageId = context.getSessionContext().getContentLanguageIdentifier();
        this.dataHandler = new DataHandler();
        this.trmApi = controller.getApi(T8TerminologyClientApi.API_IDENTIFIER);
        this.valueStringGenerator = new T8DataRecordValueStringGenerator();
    }

    @Override
    public void setRendererData(T8TableCellRenderableComponent sourceComponent, Map<String, Object> dataRow, String editorKey, Map<RenderParameter, Object> renderParameters)
    {
        Set<String> concepts;
        String fieldValue;
        StringBuffer valueString;

        tableCellRenderer.setRendererData(sourceComponent, dataRow, editorKey, renderParameters);
        fieldValue = (String) dataRow.get(editorKey);
        if (!Strings.isNullOrEmpty(fieldValue))
        {
            valueString = new StringBuffer(fieldValue);
            if (Strings.isNullOrEmpty(valueString.toString())) return;

            concepts = new LinkedHashSet<>();

            if(definition.isSingleConceptField())
            {
                concepts.add(fieldValue);
                dataHandler.addConcepts(concepts);

                if(terminologyCache.containsTerm(languageId, fieldValue))
                {
                    tableCellRenderer.setText(terminologyCache.getTerm(languageId, fieldValue));
                }
                else
                {
                    tableCellRenderer.setText(fieldValue);
                }
            }
            else
            {
                // Removed already cached concepts.
                concepts.addAll(StringUtilities.findStrings(null, T8IdentifierUtilities.CONCEPT_ID_PATTERN, null, valueString));
                concepts.removeAll(terminologyCache.getCachedConceptIDs(languageId));

                //if there are concepts to translate, then translate them
                if(!concepts.isEmpty())
                {
                    dataHandler.addConcepts(concepts);
                }

                try
                {
                    tableCellRenderer.setText(valueStringGenerator.replaceValueStringConcepts(valueString, languageId, Boolean.TRUE).toString());
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to replace value string concepts for value string " + valueString, ex);
                }
            }
        }
        else
        {
            tableCellRenderer.setText("");
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8LabelTableCellRendererDefinition definition;

        definition = new T8LabelTableCellRendererDefinition(this.definition.getIdentifier());
        this.tableCellRenderer = new T8LabelTableCellRenderer(definition, controller);
        setLayout(new BorderLayout());
        add(this.tableCellRenderer, BorderLayout.CENTER);

        this.terminologyCache = new T8LRUTerminologyCache(1000);
        this.valueStringGenerator.setTerminologyProvider(terminologyCache);
        this.valueStringGenerator.setLanguageID(languageId);

        this.dataHandler.start();
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        this.dataHandler.stopDataHandler();
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public String translate(String stringToTranslate)
    {
        return getController().getClientContext().getConfigurationManager().getUITranslation(context, stringToTranslate);
    }

    class DataHandler extends Thread
    {
        final Set<String> conceptIds;
        //The exclusions list will be used to store concepts for which no translations exist
        final Set<String> exclusions;
        final AtomicBoolean stopFlag;

        DataHandler()
        {
            this.conceptIds = Collections.synchronizedSet(new LinkedHashSet<String>());
            this.exclusions = Collections.synchronizedSet(new LinkedHashSet<String>());
            this.stopFlag = new AtomicBoolean(false);
        }

        public void stopDataHandler()
        {
            this.stopFlag.set(true);
        }

        @Override
        public void run()
        {
            //loop until all concepts have been cached
            while(!stopFlag.get())
            {
                synchronized(conceptIds)
                {
                    if(conceptIds.isEmpty())
                    {
                        try
                        {
                            conceptIds.wait();
                        }
                        catch (InterruptedException ex)
                        {
                            T8Log.log("Failed while waiting for concept ids to be added", ex);
                        }
                    }
                    updateTerminologyCache();
                }
            }
        }

        private void updateTerminologyCache()
        {
            List<String> cachedConceptIDs;

            cachedConceptIDs = new ArrayList<>(conceptIds);

            try
            {

                List<T8ConceptTerminology> terminologies;

                terminologies = trmApi.retrieveTerminology(ArrayLists.newArrayList(languageId), cachedConceptIDs);
                for (T8ConceptTerminology t8ConceptTerminology : terminologies)
                {
                    terminologyCache.setConceptTerminology(t8ConceptTerminology);
                    conceptIds.remove(t8ConceptTerminology.getConceptId());
                    cachedConceptIDs.remove(t8ConceptTerminology.getConceptId());
                }

                //Add all the concepts that were not cached to the exclusion list so we do not try to retrieve them again
                exclusions.addAll(cachedConceptIDs);
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to retrieve concept terminologies.", ex);
                //if the operation is not working do not call it again with these concepts
                exclusions.addAll(cachedConceptIDs);
                conceptIds.removeAll(cachedConceptIDs);
            }
        }

        void addConcepts(Set<String> concepts)
        {
            synchronized(conceptIds)
            {
                conceptIds.addAll(concepts);
                conceptIds.removeAll(exclusions);
                conceptIds.removeAll(terminologyCache.getCachedConceptIDs(languageId));

                if(!conceptIds.isEmpty()) conceptIds.notifyAll();
            }
        }
    }

    @Override
    public Object getAdditionalRenderData(Map<String, Object> dataRow, String fieldIdentifier)
    {
        return null;
    }
}
