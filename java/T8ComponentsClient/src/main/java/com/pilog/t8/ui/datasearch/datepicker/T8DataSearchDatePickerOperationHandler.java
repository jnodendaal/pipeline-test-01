package com.pilog.t8.ui.datasearch.datepicker;

import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxAPIHandler;
import com.pilog.t8.definition.ui.datasearch.datepicker.T8DataSearchDatePickerAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchDatePickerOperationHandler extends T8ComponentOperationHandler
{
    private final T8DataSearchDatePicker datePicker;

    public T8DataSearchDatePickerOperationHandler(T8DataSearchDatePicker datePicker)
    {
        super(datePicker);
        this.datePicker = datePicker;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier,
                                                Map<String, Object> operationParameters)
    {
        if(T8DataSearchDatePickerAPIHandler.COMPONENT_OPERATION_GET_DATA_FILTER.equals(operationIdentifier))
        {
            return HashMaps.create(T8DataSearchComboBoxAPIHandler.PARAMETER_DATA_FILTER, datePicker.getDataFilter());
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }


}
