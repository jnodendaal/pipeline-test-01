package com.pilog.t8.ui.entityeditor.conceptlabel;

import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.definition.ui.entityeditor.conceptlabel.T8ConceptLabelDataEntityEditorDefinition;
import com.pilog.t8.definition.ui.entityeditor.conceptlabel.T8ConceptLabelDataEntityEditorDefinition.DisplayType;
import com.pilog.t8.ui.label.T8Label;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8ConceptLabelDataEntityEditor extends T8Label implements T8DataEntityEditorComponent
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8ConceptLabelDataEntityEditor.class.getName());
    private final T8ConceptLabelDataEntityEditorDefinition definition;
    private final T8ConceptLabelDataEntityOperationHandler operationHandler;
    private final T8TerminologyClientApi trmApi;
    private T8DataEntity editorEntity;
    private final String languageId;

    public T8ConceptLabelDataEntityEditor(T8ConceptLabelDataEntityEditorDefinition conceptLabelDefinition, T8ComponentController controller)
    {
        super(conceptLabelDefinition, controller);
        this.definition = conceptLabelDefinition;
        this.operationHandler = new T8ConceptLabelDataEntityOperationHandler(this);
        this.trmApi = controller.getApi(T8TerminologyClientApi.API_IDENTIFIER);
        this.languageId = controller.getContext().getSessionContext().getContentLanguageIdentifier();
    }

    @Override
    public Map<String, Object> executeOperation(String operationId, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationId, operationParameters);
    }

    @Override
    public void setEditorDataEntity(T8DataEntity dataEntity)
    {
        editorEntity = dataEntity;
        if (editorEntity != null)
        {
            String fieldIdentifier;

            fieldIdentifier = definition.getEditorDataEntityFieldIdentifier();
            if (fieldIdentifier != null)
            {
                Object value;

                value = editorEntity.getFieldValue(fieldIdentifier);
                try
                {
                    setText(getTranslatedValue(definition.getDisplayType(), value));
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to get the translated value for concept " + value + " using display type " + definition.getDisplayType(), ex);
                    setText(value == null ? null : value.toString());
                }
                try
                {
                    if (definition.getTooltipDisplayType() == DisplayType.DEFAULT)
                    {
                        setToolTipText(definition.getTooltipText());
                    }
                    else
                    {
                        setToolTipText(getTranslatedValue(definition.getTooltipDisplayType(), value));
                    }
                }
                catch (Exception ex)
                {
                    LOGGER.log("Failed to get the translated value for concept " + value + " using display type " + definition.getTooltipDisplayType(), ex);
                    setText(value == null ? null : value.toString());
                }
            }
            else
            {
                setText(null);
                setToolTipText(definition.getTooltipText());
            }
        }
        else
        {
            setText(null);
            setToolTipText(definition.getTooltipText());
        }
    }

    private String getTranslatedValue(DisplayType displayType, Object conceptID) throws Exception
    {
        if (conceptID == null)
        {
            return null;
        }

        T8ConceptTerminology firstTerminology = getFirstTerminology(conceptID);

        if (firstTerminology == null)
        {
            return conceptID.toString();
        }

        switch (displayType)
        {
            case ABBREVIATION:
            {
                return firstTerminology.getAbbreviation();
            }
            case CODE:
            {
                return firstTerminology.getCode();
            }
            case DEFINITION:
            {
                return firstTerminology.getDefinition();
            }
            case TERM:
            {
                return firstTerminology.getTerm();
            }
            default:
            {
                return firstTerminology.getTerm();
            }
        }
    }

    private T8ConceptTerminology getFirstTerminology(Object conceptID) throws Exception
    {
        List<T8ConceptTerminology> retrieveTerminology = trmApi.retrieveTerminology(Arrays.asList(languageId), Arrays.asList(conceptID.toString()));
        if (retrieveTerminology != null && !retrieveTerminology.isEmpty())
        {
            return retrieveTerminology.get(0);
        }
        else
        {
            return null;
        }
    }

    @Override
    public T8DataEntity getEditorDataEntity()
    {
        return editorEntity;
    }

    @Override
    public boolean commitEditorChanges()
    {
        //Do nothing since we dont actually edit anything, we just display
        return true;
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return definition.getEditorDataEntityIdentifier();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return definition.getParentEditorIdentifier();
    }

}
