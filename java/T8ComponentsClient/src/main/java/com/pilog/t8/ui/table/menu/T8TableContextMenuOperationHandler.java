/**
 * Created on 19 Oct 2015, 10:08:11 AM
 */
package com.pilog.t8.ui.table.menu;

import static com.pilog.t8.definition.ui.table.menu.T8TableContextMenuAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8TableContextMenuOperationHandler extends T8ComponentOperationHandler
{
    private final T8TableContextMenu contextMenu;

    public T8TableContextMenuOperationHandler(T8TableContextMenu component)
    {
        super(component);
        this.contextMenu = component;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        switch (operationIdentifier)
        {
            case OPERATION_SET_COPY_ROWS_VISIBLE:
                this.contextMenu.setVisible(T8TableContextMenu.StandardMenuItem.COPY_ROWS, Boolean.TRUE.equals(operationParameters.get(PARAMETER_VISIBLE)));
                break;
            case OPERATION_SET_EXPORT_VISIBLE:
                this.contextMenu.setVisible(T8TableContextMenu.StandardMenuItem.EXPORT, Boolean.TRUE.equals(operationParameters.get(PARAMETER_VISIBLE)));
                break;
            case OPERATION_SET_FILL_DOWN_VISIBLE:
                this.contextMenu.setVisible(T8TableContextMenu.StandardMenuItem.FILL_DOWN, Boolean.TRUE.equals(operationParameters.get(PARAMETER_VISIBLE)));
                break;
            case OPERATION_SET_MENU_ITEM_VISIBLE:
                this.contextMenu.setVisible((String)operationParameters.get(PARAMETER_MENU_ITEM_IDENTIFIER), Boolean.TRUE.equals(operationParameters.get(PARAMETER_VISIBLE)));
                break;
        }

        return null;
    }
}