package com.pilog.t8.ui.entityeditor.concepttypelabel;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.definition.ui.entityeditor.concepttypelabel.T8ConceptTypeLabelDataEntityEditorDefinition;
import com.pilog.t8.ui.label.T8Label;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ConceptTypeLabelDataEntityEditor extends T8Label implements T8DataEntityEditorComponent
{
    private final T8ConceptTypeLabelDataEntityEditorDefinition definition;
    private final T8ConceptTypeLabelDataEntityOperationHandler operationHandler;
    private T8DataEntity editorEntity;

    public T8ConceptTypeLabelDataEntityEditor(T8ConceptTypeLabelDataEntityEditorDefinition conceptLabelDefinition, T8ComponentController controller)
    {
        super(conceptLabelDefinition, controller);
        this.definition = conceptLabelDefinition;
        this.operationHandler = new T8ConceptTypeLabelDataEntityOperationHandler(this);
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public void setEditorDataEntity(T8DataEntity dataEntity)
    {
        editorEntity = dataEntity;
        if (editorEntity != null)
        {
            String fieldIdentifier;

            fieldIdentifier = definition.getEditorDataEntityFieldIdentifier();
            if (fieldIdentifier != null)
            {
                Object value;

                value = editorEntity.getFieldValue(fieldIdentifier);
                if (value instanceof String)
                {
                    T8OntologyConceptType conceptType;
                    
                    conceptType = T8OntologyConceptType.getConceptType((String)value);
                    setText(conceptType != null ? conceptType.getDisplayName() : null);
                }
                else throw new RuntimeException("Entity field value is not of expected type String: " + value);
            }
            else
            {
                setText(null);
                setToolTipText(definition.getTooltipText());
            }
        }
        else
        {
            setText(null);
            setToolTipText(definition.getTooltipText());
        }
    }

    @Override
    public T8DataEntity getEditorDataEntity()
    {
        return editorEntity;
    }

    @Override
    public boolean commitEditorChanges()
    {
        // No validation: this is not an editor, just used for display purposes.
        return true;
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return definition.getEditorDataEntityIdentifier();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return definition.getParentEditorIdentifier();
    }

}
