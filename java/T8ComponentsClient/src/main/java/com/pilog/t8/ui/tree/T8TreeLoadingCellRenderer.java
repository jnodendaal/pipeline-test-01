package com.pilog.t8.ui.tree;

import com.pilog.t8.ui.busypanel.T8BusyLabelPainter;
import java.awt.Dimension;
import javax.swing.JComponent;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class T8TreeLoadingCellRenderer extends JXPanel
{
    private T8BusyLabelPainter busyPainter;
    
    private static final int BUSY_PAINTER_DIAMETER = 12;
    
    public T8TreeLoadingCellRenderer(JComponent parentComponent)
    {
        setOpaque(false);
        setPreferredSize(new Dimension(300, 12));
        busyPainter = new T8BusyLabelPainter(parentComponent, BUSY_PAINTER_DIAMETER, 8, 3, "Loading Data...", 5, 10);
        setBackgroundPainter(busyPainter);
    }

    public void startAnimation()
    {
        busyPainter.startAnimation();
    }
    
    public void stopAnimation()
    {
        busyPainter.stopAnimation();
    }
}
