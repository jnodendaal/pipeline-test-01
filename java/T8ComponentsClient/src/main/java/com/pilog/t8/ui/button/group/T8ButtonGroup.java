/**
 * Created on 10 Sep 2015, 7:23:25 AM
 */
package com.pilog.t8.ui.button.group;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.button.group.T8ButtonGroupAPIHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jdesktop.swingx.JXRadioGroup;

/**
 * @author Gavin Boshoff
 * @param <B>
 */
public abstract class T8ButtonGroup<B extends T8Component> extends JXRadioGroup<B> implements T8Component
{
    private final T8ButtonGroupOperationHandler operationHandler;
    private final LinkedHashMap<String, B> childComponents;
    private final T8ComponentDefinition definition;
    private final T8ComponentController controller;

    private final ButtonSelectionChangeListener selectionChangeListener;

    public T8ButtonGroup(T8ComponentDefinition buttonGroupDefinition, T8ComponentController controller)
    {
        this.definition = buttonGroupDefinition;
        this.controller = controller;
        this.operationHandler = new T8ButtonGroupOperationHandler(this);
        this.childComponents = new LinkedHashMap<>();
        this.selectionChangeListener = new ButtonSelectionChangeListener();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setOpaque(definition.isOpaque());
    }

    @Override
    public T8ComponentController getController()
    {
        return this.controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return this.definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        if (childComponent instanceof AbstractButton)
        {
            T8ComponentDefinition checkboxDefinition;
            B abstractButton;

            abstractButton = (B)childComponent;
            checkboxDefinition = abstractButton.getComponentDefinition();
            this.childComponents.put(checkboxDefinition.getIdentifier(), abstractButton);

            ((AbstractButton)abstractButton).addChangeListener(this.selectionChangeListener);
            ((javax.swing.JComponent)abstractButton).setOpaque(this.definition.isOpaque());
            add(abstractButton);
        } else throw new IllegalArgumentException("Button group only accepts instances of AbstractButton");
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return this.childComponents.get(childComponentIdentifier);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        B abstractButton;

        abstractButton = (B)getChildComponent(childComponentIdentifier);
        if (abstractButton != null)
        {
            ((AbstractButton)abstractButton).removeChangeListener(this.selectionChangeListener);
            remove((java.awt.Component)abstractButton);
        }

        return abstractButton;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(this.childComponents.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return this.childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
        AbstractButton castButton;

        for (B abstractButton : this.childComponents.values())
        {
            castButton = (AbstractButton)abstractButton;

            castButton.removeChangeListener(this.selectionChangeListener);
            remove(castButton);
        }
        childComponents.clear();
    }

    /**
     * {@inheritDoc}
     * <br/><br/>
     * Sets the opaque level of both the button group, as well as each of the
     * buttons contained therein.
     *
     * @param isOpaque {@code true} if the component should be opaque
     */
    @Override
    public void setOpaque(boolean isOpaque)
    {
        super.setOpaque(isOpaque);
        if (this.childComponents != null)
        {
            this.childComponents.values().forEach((B abstractButton)->((AbstractButton)abstractButton).setOpaque(isOpaque));
        }
    }

    /**
     * Sets the selected button using the {@code String} child component
     * identifier of the button to be selected.
     *
     * @param childComponentIdentifier The identifier for the
     *      {@code AbstractButton} to be set to its selected state
     */
    public void setSelected(String childComponentIdentifier)
    {
        B abstractButton;

        abstractButton = this.childComponents.get(childComponentIdentifier);

        setSelectedValue(abstractButton);
    }

    /**
     * Returns the child component identifier for the selected checkbox in
     * the group.
     *
     * @return The {@code String} component identifier for the selected checkbox
     */
    public String getSelectedIdentifier()
    {
        B selectedAbstractButton;

        selectedAbstractButton = this.getSelectedValue();
        if (selectedAbstractButton != null) return selectedAbstractButton.getComponentDefinition().getIdentifier();
        else return null;
    }

    /**
     * A {@code ChangeListener} implementation which will be triggered when
     * changes occur on the child button components. The component event will
     * only be triggered if the change is the selection state of a child
     * component, and that component is different from the previously selected
     * button.
     */
    private class ButtonSelectionChangeListener implements ChangeListener
    {
        private B lastSelected;

        @Override
        @SuppressWarnings("unchecked")
        public void stateChanged(ChangeEvent e)
        {
            B changedChild;

            changedChild = (B)e.getSource();
            if (changedChild != lastSelected && ((AbstractButton)changedChild).isSelected())
            {
                this.lastSelected = changedChild;
                controller.reportEvent(T8ButtonGroup.this, definition.getComponentEventDefinition(T8ButtonGroupAPIHandler.EVENT_SELECTION_CHANGED), HashMaps.createSingular(T8ButtonGroupAPIHandler.PARAMETER_CHILD_IDENTIFIER, changedChild.getComponentDefinition().getIdentifier()));
            }
        }
    }
}