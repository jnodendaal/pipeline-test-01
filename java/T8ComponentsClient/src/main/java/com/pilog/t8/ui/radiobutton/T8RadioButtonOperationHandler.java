package com.pilog.t8.ui.radiobutton;

import static com.pilog.t8.definition.ui.radiobutton.T8RadioButtonAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8RadioButtonOperationHandler extends T8ComponentOperationHandler
{
    private final T8RadioButton radioButton;

    public T8RadioButtonOperationHandler(T8RadioButton button)
    {
        super(button);
        this.radioButton = button;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(OPERATION_ENABLE_BUTTON))
        {
            radioButton.setEnabled(true);
            return null;
        }
        else if(operationIdentifier.equals(OPERATION_DISABLE_BUTTON))
        {
            radioButton.setEnabled(false);
            return null;
        }
        else if(operationIdentifier.equals(OPERATION_SET_TEXT))
        {
            radioButton.setText((String)operationParameters.get(PARAMETER_TEXT));
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_SET_SELECTED))
        {
            this.radioButton.setSelected(Boolean.TRUE.equals(operationParameters.get(PARAMETER_SELECTED)));
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
