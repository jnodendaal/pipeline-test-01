package com.pilog.t8.ui.list;

import static com.pilog.t8.definition.ui.list.T8ListAPIHandler.*;

import com.pilog.t8.T8Log;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8ListOperationHandler extends T8ComponentOperationHandler
{
    private final T8List list;

    public T8ListOperationHandler(T8List list)
    {
        super(list);
        this.list = list;
    }

    //TODO: GBO - The workaround added here is temporary and should be reverted once issue TECHEIGHT-1535 has been resolved.
    // P.S. This is a terrible thing to do.
    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(OPERATION_ADD_ELEMENT))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                list.addElements(Arrays.asList(operationParameters.get(PARAMETER_ELEMENT)));
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_ADD_ELEMENTS))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                list.addElements((List<Object>)operationParameters.get(PARAMETER_ELEMENT_LIST));
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_REMOVE_ELEMENT))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                list.removeElements(Arrays.asList(operationParameters.get(PARAMETER_ELEMENT)));
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_REMOVE_ELEMENTS))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                list.removeElements((List<Object>)operationParameters.get(PARAMETER_ELEMENT_LIST));
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_REMOVE_ALL_ELEMENTS))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                list.removeAllElements();
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_SET_SELECTED_ELEMENTS))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                list.setSelectedElements((List<Object>)operationParameters.get(PARAMETER_ELEMENT_LIST));
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_GET_ALL_ELEMENTS))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                return HashMaps.createSingular(PARAMETER_ELEMENT_LIST, list.getAllElements());
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_GET_SELECTED_ELEMENTS))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                return HashMaps.createSingular(PARAMETER_ELEMENT_LIST, list.getSelectedElements());
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_CLEAR_SELECTION))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                list.clearSelection();
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_INCREMENT_SELECTED_ELEMENT_INDEX))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                list.incrementSelectedElementIndex();
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if(operationIdentifier.equals(OPERATION_DECREMENT_SELECTED_ELEMENT_INDEX))
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                list.decrementSelectedElementIndex();
            } else return executeOperationOnEDT(operationIdentifier, operationParameters);
        } else return super.executeOperation(operationIdentifier, operationParameters);

        return null;
    }

    public Map<String, Object> executeOperationOnEDT(String operationIdentifier, Map<String, Object> operationParameters)
    {
        ListOperationExecuter operationExecuter;

        operationExecuter = new ListOperationExecuter(operationIdentifier, operationParameters);

        try
        {
            SwingUtilities.invokeAndWait(operationExecuter);
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to execute operation " + operationIdentifier + " on EDT.", ex);
        }

        return operationExecuter.getOperationResult();
    }

    private class ListOperationExecuter implements Runnable
    {
        private final String operationIdentifier;
        private final Map<String, Object> operationParameters;
        private Map<String, Object> operationResult;

        private ListOperationExecuter(String operationIdentifier, Map<String, Object> operationParameters)
        {
            this.operationIdentifier = operationIdentifier;
            this.operationParameters = operationParameters;
        }

        @Override
        public void run()
        {
            if (operationIdentifier.equals(OPERATION_ADD_ELEMENT))
            {
                Object element;

                element = operationParameters.get(PARAMETER_ELEMENT);
                list.addElements(Arrays.asList(element));
            }
            else if (operationIdentifier.equals(OPERATION_ADD_ELEMENTS))
            {
                list.addElements((List<Object>)operationParameters.get(PARAMETER_ELEMENT_LIST));
            }
            else if (operationIdentifier.equals(OPERATION_REMOVE_ELEMENT))
            {
                Object element;

                element = operationParameters.get(PARAMETER_ELEMENT);
                list.removeElements(Arrays.asList(element));
            }
            else if (operationIdentifier.equals(OPERATION_REMOVE_ELEMENTS))
            {
                list.removeElements((List<Object>)operationParameters.get(PARAMETER_ELEMENT_LIST));
            }
            else if (operationIdentifier.equals(OPERATION_REMOVE_ALL_ELEMENTS))
            {
                list.removeAllElements();
            }
            else if (operationIdentifier.equals(OPERATION_SET_SELECTED_ELEMENTS))
            {
                list.setSelectedElements((List<Object>)operationParameters.get(PARAMETER_ELEMENT_LIST));
            }
            else if (operationIdentifier.equals(OPERATION_GET_ALL_ELEMENTS))
            {
                operationResult = HashMaps.createSingular(PARAMETER_ELEMENT_LIST, list.getAllElements());
            }
            else if (operationIdentifier.equals(OPERATION_GET_SELECTED_ELEMENTS))
            {
                operationResult = HashMaps.createSingular(PARAMETER_ELEMENT_LIST, list.getSelectedElements());
            }
            else if (operationIdentifier.equals(OPERATION_CLEAR_SELECTION))
            {
                list.clearSelection();
            }
            else if (operationIdentifier.equals(OPERATION_DECREMENT_SELECTED_ELEMENT_INDEX))
            {
                list.decrementSelectedElementIndex();
            }
            else if (operationIdentifier.equals(OPERATION_INCREMENT_SELECTED_ELEMENT_INDEX))
            {
                list.incrementSelectedElementIndex();
            }
        }

        public Map<String, Object> getOperationResult()
        {
            return operationResult;
        }
    }
}
