package com.pilog.t8.ui.textarea;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.textarea.T8TextAreaDefinition;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.components.border.PulsatingBorder;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JTextArea;

/**
 * @author Bouwer du Preez
 */
public class T8TextArea extends JTextArea implements T8Component
{
    private final T8ComponentController controller;
    private final T8TextAreaDefinition definition;
    private final T8TextAreaOperationHandler operationHandler;

    public T8TextArea(T8TextAreaDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8TextAreaOperationHandler(this);
        this.setMinimumSize(null);
        this.setMaximumSize(null);
        this.setLineWrap(true);
        this.setWrapStyleWord(true);
        this.setRows(definition.getRows());
        this.setColumns(definition.getColumns());
        this.setFont(LAFConstants.CONTENT_FONT);

        // Set the decoration state.
        if (definition.isUndecorated())
        {
           setBorder(null);
           setOpaque(false);
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        // Set the font if available.
        if (definition.getFontDefinition() != null)
        {
            Font font;

            font = definition.getFontDefinition().getNewFontInstance();
            if (font != null) setFont(font);
        }

        // Set the content text and tooltip.
        setText(definition.getText());
        setToolTipText(definition.getTooltipText());

        // Set the editability of the text area.
        setEditable(definition.isEditable());
    }

    @Override
    public void startComponent()
    {
        if (!definition.isUndecorated())
        {
            PulsatingBorder.installWithFocusAnimation(this, new Color(0, 0, 200, 100));
        }
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public String getText()
    {
        return super.getText();
    }

    @Override
    public void setText(String text)
    {
        super.setText(text);
    }

    @Override
    public void setEditable(boolean editable)
    {
        super.setEditable(editable);
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
    }
}
