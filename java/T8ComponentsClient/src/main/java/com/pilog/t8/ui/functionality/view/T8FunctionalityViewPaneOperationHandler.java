package com.pilog.t8.ui.functionality.view;

import com.pilog.t8.definition.ui.functionality.view.T8FunctionalityViewPaneAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityViewPaneOperationHandler extends T8ComponentOperationHandler
{
    private final T8FunctionalityViewPane functionalityView;

    public T8FunctionalityViewPaneOperationHandler(T8FunctionalityViewPane container)
    {
        super(container);
        this.functionalityView = container;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8FunctionalityViewPaneAPIHandler.OPERATION_INVOKE_FUNCTIONALITY))
        {
            Map<String, Object> parameters;
            String functionalityId;

            functionalityId = (String)operationParameters.get(T8FunctionalityViewPaneAPIHandler.PARAMETER_FUNCTIONALITY_IDENTIFIER);
            parameters = (Map<String, Object>) operationParameters.get(T8FunctionalityViewPaneAPIHandler.PARAMETER_FUNCTIONALITY_PARAMETERS);

            try
            {
                functionalityView.accessFunctionality(functionalityId, parameters);
                return null;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while invoking functionality: " + functionalityId, e);
            }
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
