package com.pilog.t8.ui.celleditor.table.combobox;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellEditableComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent.EditParameter;
import com.pilog.t8.definition.ui.celleditor.table.combobox.T8ComboBoxTableCellEditorDefinition;
import com.pilog.t8.ui.combobox.T8ComboBox;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ComboBoxTableCellEditor extends T8ComboBox implements T8TableCellEditorComponent
{
    private T8ComboBoxTableCellEditorDefinition definition;
    private T8ComponentController controller;
    private String editorColumnIdentifier;
    
    public T8ComboBoxTableCellEditor(T8ComboBoxTableCellEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.controller = controller;
        this.definition = definition;
    }
    
    @Override
    public void setEditorData(T8TableCellEditableComponent sourceComponent, Map<String, Object> dataRow, String editorColumnIdentifier, Map<EditParameter, Object> editParameters)
    {
        setSelectedDataValue(dataRow != null ? dataRow.get(definition.getTargetFieldIdentifier()) : null);
    }

    @Override
    public Map<String, Object> getEditorData()
    {
        return HashMaps.newHashMap(definition.getTargetFieldIdentifier(), getSelectedDataValue());
    }

    @Override
    public boolean singleClickEdit()
    {
        return false;
    }
}
