/**
 * Created on 21 Aug 2015, 1:16:59 PM
 */
package com.pilog.t8.ui.radiobutton;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.radiobutton.T8RadioButtonGroupDefinition;
import com.pilog.t8.ui.button.group.T8ButtonGroup;

/**
 * @author Gavin Boshoff
 */
public class T8RadioButtonGroup extends T8ButtonGroup<T8RadioButton>
{
    public T8RadioButtonGroup(T8RadioButtonGroupDefinition buttonGroupDefinition, T8ComponentController controller)
    {
        super(buttonGroupDefinition, controller);
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }
}