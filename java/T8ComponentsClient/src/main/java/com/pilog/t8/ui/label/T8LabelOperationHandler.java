package com.pilog.t8.ui.label;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.label.T8LabelAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;

/**
 * @author Bouwer du Preez
 */
public class T8LabelOperationHandler extends T8ComponentOperationHandler
{
    private final T8Label label;
    private final String projectId;
    private final Map<String, ImageIcon> iconCache;

    public T8LabelOperationHandler(T8Label label)
    {
        super(label);
        this.label = label;
        this.projectId = label.getComponentDefinition().getRootProjectId();
        this.iconCache = new HashMap<>();
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8LabelAPIHandler.OPERATION_SET_TEXT))
        {
            Object text;

            text = operationParameters.get(T8LabelAPIHandler.PARAMETER_TEXT);
            if ((text != null) && (!(text instanceof String))) text = "" + text;
            label.setText((String)text);
            return null;
        }
        else if (operationIdentifier.equals(T8LabelAPIHandler.OPERATION_GET_TEXT))
        {
            return HashMaps.newHashMap(T8LabelAPIHandler.PARAMETER_TEXT, label.getText());
        }
        else if (operationIdentifier.equals(T8LabelAPIHandler.OPERATION_SET_ICON))
        {
            T8DefinitionManager definitionManager;
            String iconId;

            iconId = (String) operationParameters.get(T8LabelAPIHandler.PARAMETER_ICON_IDENTIFIER);
            definitionManager = label.getController().getClientContext().getDefinitionManager();
            if(iconId != null)
            {
                if(!iconCache.containsKey(iconId))
                {
                    T8IconDefinition iconDefinition;

                    try
                    {
                        iconDefinition = (T8IconDefinition) definitionManager.getInitializedDefinition(label.getController().getContext(), projectId, iconId, null);
                        iconCache.put(iconId, iconDefinition.getImage().getImageIcon());
                    }
                    catch (Exception ex)
                    {
                        T8Log.log("Failed to load icon definition " + iconId, ex);
                    }
                }

                label.setIcon(iconCache.get(iconId));
            }

            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
