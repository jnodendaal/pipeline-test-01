package com.pilog.t8.ui.celleditor.table.datepicker;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellEditableComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent.EditParameter;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.celleditor.table.datepicker.T8DatePickerTableCellEditorDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.Strings;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import org.jdesktop.swingx.JXDatePicker;

/**
 * Key bindings (as installed by the UI-Delegate):
 *
 *   - ENTER commits the edited or selected value.
 *   - ESCAPE reverts the edited or selected value.
 *   - alt-DOWN opens the monthView popup.
 *   - shift-F5 if monthView is visible, navigates the monthView to today.
 *   - F5 commits today.
 *
 * @author Bouwer du Preez
 */
public class T8DatePickerTableCellEditor extends JXDatePicker implements T8TableCellEditorComponent
{
    private static final T8Logger logger = T8Log.getLogger(T8DatePickerTableCellEditor.class);

    private final T8DatePickerTableCellEditorDefinition definition;
    private final T8ComponentController controller;
    private final T8SessionContext sessionContext;
    private String editorKey;

    public T8DatePickerTableCellEditor(T8DatePickerTableCellEditorDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.sessionContext = controller.getSessionContext();
        if(!Strings.isNullOrEmpty(definition.getDateFormat())) this.setFormats(new SimpleDateFormat(definition.getDateFormat()));
    }

    @Override
    public void setEditorData(T8TableCellEditableComponent sourceComponent, Map<String, Object> dataRow, String editorKey, Map<EditParameter, Object> editParameters)
    {
        Object dataRowObject;

        this.editorKey = editorKey;
        dataRowObject = dataRow.get(editorKey);
        if (dataRowObject != null)
        {
            switch (definition.getUnderlyingDataType())
            {
                case TIMESTAMP:
                    setDate(new Date(((T8Timestamp) dataRowObject).getMilliseconds()));
                    break;
                case EPOCH_STRING:
                    setDate(new Date(Long.parseLong((String)dataRowObject)));
                    break;
                default:
                    throw new RuntimeException("The supplied underlying type is not supported. Data Value Type: {"+dataRowObject+"}");
            }
        }
        else
        {
            logger.log(T8Logger.Level.DEBUG, "The cell value is NULL. We will set the editor value to the current date and time.");
            setDate(new Date());
        }
    }

    @Override
    public Map<String, Object> getEditorData()
    {
        // Depending on the underlying type for the cell, on the entity, we return
        // an appropriate value.
        switch (this.definition.getUnderlyingDataType())
        {
            case TIMESTAMP:
                return HashMaps.createSingular(editorKey, getDate() == null ? null : new T8Timestamp(getDate().getTime()));
            case EPOCH_STRING:
                return HashMaps.createSingular(editorKey, getDate() == null ? null : Long.toString(getDate().getTime()));
            default:
                throw new RuntimeException("The underlying type for the date picker cell editor is not supported. Cannot determine the appropriate value to return.");
        }

    }

    @Override
    public boolean singleClickEdit()
    {
        return false;
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String string, Map<String, Object> map)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }
}
