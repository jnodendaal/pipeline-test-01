package com.pilog.t8.ui.functionality.view;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewClosedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewListener;
import com.pilog.t8.ui.functionality.event.T8FunctionalityViewUpdatedEvent;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle.T8FunctionalityAccessType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.WrapLayout;
import org.jdesktop.swingx.border.DropShadowBorder;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class FunctionalityViewPane extends JPanel implements T8FunctionalityController
{
    private final T8ComponentController controller;
    private final T8FunctionalityManager functionalityManager;
    private final T8ConfigurationManager configurationManager;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final LinkedHashMap<String, T8FunctionalityView> functionalityViews;
    private final T8DefaultComponentContainer container;
    private T8FunctionalityView currentView;
    private JToolBar jToolBarNavigation;
    private JXPanel navigationPanel;
    private final FunctionalityViewListener viewListener;
    private final Map<String, FunctionalityNavigationButton> navigationButtons;
    private final Map<String, FunctionalityCloseButton> closeButtons;

    private static final Font NAVIGATION_BUTTON_FONT = new java.awt.Font("Tahoma", 0, 13);

    public FunctionalityViewPane(T8ComponentController controller)
    {
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.functionalityManager = clientContext.getFunctionalityManager();
        this.configurationManager = clientContext.getConfigurationManager();
        this.functionalityViews = new LinkedHashMap<>();
        this.navigationButtons = new HashMap<>();
        this.closeButtons = new HashMap<>();
        this.container = new T8DefaultComponentContainer();
        this.viewListener = new FunctionalityViewListener();
        this.currentView = null;
        initComponents();
        setupLayout();
    }

    private void setupLayout()
    {
        GridBagConstraints gridBagConstraints;

        navigationPanel = new JXPanel();
        navigationPanel.setPaintBorderInsets(false);
        navigationPanel.setOpaque(true);
        navigationPanel.setBorder(new DropShadowBorder());
        navigationPanel.setLayout(new BorderLayout());

        jToolBarNavigation = new javax.swing.JToolBar();
        jToolBarNavigation.setFloatable(false);
        jToolBarNavigation.setRollover(true);
        jToolBarNavigation.setOpaque(false);
        jToolBarNavigation.setBorder(null);
        jToolBarNavigation.setBorderPainted(false);
        jToolBarNavigation.setLayout(new WrapLayout(WrapLayout.LEFT, 0, 0));
        navigationPanel.add(jToolBarNavigation, BorderLayout.CENTER);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(navigationPanel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 0);
        add(container, gridBagConstraints);
    }

    public void setBackgroundPainter(Painter painter)
    {
        navigationPanel.setBackgroundPainter(painter);
    }

    public void setViewOpaque(boolean opaque)
    {
        setOpaque(opaque);
        container.setContainerOpaque(opaque);
    }

    @Override
    public T8FunctionalityView getFunctionalityView(String functionalityInstanceIdentifier)
    {
        return functionalityViews.get(functionalityInstanceIdentifier);
    }

    @Override
    public T8ComponentController getFunctionalityComponentController()
    {
        return controller;
    }

    @Override
    public T8FunctionalityAccessHandle accessFunctionality(final String functionalityId, Map<String, Object> parameters) throws Exception
    {
        FunctionalityInitializer functionalityInitializer;
        T8FunctionalityAccessHandle executionHandle;
        long startTime;

        // Make sure this is not the EDT.
        if (SwingUtilities.isEventDispatchThread()) throw new Exception("Method can not be called from EDT.");

        // Lock the UI.
        startTime = System.currentTimeMillis();
        container.setMessage(translate("Accessing Functionality..."));
        container.lock();

        // Load and initialize the functionality instance.
        functionalityInitializer = new FunctionalityInitializer(functionalityId, parameters);

        // Initialize the functionality.
        functionalityInitializer.execute();
        executionHandle = functionalityInitializer.get();
        T8Log.log("Functionality accessed: " + (System.currentTimeMillis() - startTime) + "ms");
        return executionHandle;
    }

    protected String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    private void addFunctionalityView(T8FunctionalityView view)
    {
        if (view != null)
        {
            FunctionalityNavigationButton navigationButton;
            FunctionalityCloseButton closeButton;
            String functionalityInstanceIdentifier;
            T8FunctionalityAccessHandle executionHandle;

            // Get the functionality instance identifier.
            executionHandle = view.getExecutionHandle();
            functionalityInstanceIdentifier = executionHandle.getFunctionalityIid();

            // Add the view to the collection of views.
            functionalityViews.put(functionalityInstanceIdentifier, view);

            // Add the view listener to the view.
            view.addFunctionalityViewListener(viewListener);

            // Add the view to the UI.
            if (!(view instanceof JDialog)) // TODO:  This check is a bit of a hack.  We actually have to define some concrete types for func views e.g. dialogs, panels etc.
            {
                navigationButton = new FunctionalityNavigationButton(executionHandle);
                closeButton = new FunctionalityCloseButton(executionHandle);
                navigationButtons.put(functionalityInstanceIdentifier, navigationButton);
                closeButtons.put(functionalityInstanceIdentifier, closeButton);
                jToolBarNavigation.add(navigationButton);
                jToolBarNavigation.add(closeButton);

                switchToFunctionalityView(functionalityInstanceIdentifier);
            }
        }
    }

    public void setNavigationBarVisible(boolean visible)
    {
        navigationPanel.setVisible(visible);
    }

    private void switchToFunctionalityView(String functionalityInstanceIdentifier)
    {
        T8FunctionalityView view;

        view = functionalityViews.get(functionalityInstanceIdentifier);
        currentView = view;
        container.setComponent((JComponent)view);

        // Set the selected state of all navigation buttons.
        for (FunctionalityNavigationButton navigationButton : navigationButtons.values())
        {
            if (navigationButton.getFunctionalityInstanceIdentifier().equals(functionalityInstanceIdentifier))
            {
                navigationButton.setSelected(true);
            }
            else
            {
                navigationButton.setSelected(false);
            }
        }
    }

    private String findPrecedingView(String instanceIdentifier)
    {
        Iterator<String> instanceIdentifierIterator;
        String precedingInstanceIdentifier;

        precedingInstanceIdentifier = null;
        instanceIdentifierIterator = functionalityViews.keySet().iterator();
        while (instanceIdentifierIterator.hasNext())
        {
            String nextIdentifier;

            nextIdentifier = instanceIdentifierIterator.next();
            if (nextIdentifier.equals(instanceIdentifier))
            {
                return precedingInstanceIdentifier;
            }
            else
            {
                precedingInstanceIdentifier = nextIdentifier;
            }
        }

        return precedingInstanceIdentifier;
    }

    private void closeFunctionalityView(String functionalityIid)
    {
        T8FunctionalityView view;
        String precedingViewInstanceIdentifier;

        // Find the naviation view to close.
        precedingViewInstanceIdentifier = findPrecedingView(functionalityIid);
        view = functionalityViews.remove(functionalityIid);
        if (view != null)
        {
            JComponent navigationButton;
            JComponent closeButton;

            // Remove the view listener from the view.
            view.removeFunctionalityViewListener(viewListener);

            // If we are closing the current view, we need to switch to the preceding view.
            if (view == currentView)
            {
                switchToFunctionalityView(precedingViewInstanceIdentifier);
            }

            // Stop the view and remove it from the container if it is visible.
            view.stopComponent();

            // Remove navigation buttons from the navigation bar.
            T8Log.log("Removing navigation button: " + functionalityIid + " from buttons collection: " + navigationButtons.keySet());
            navigationButton = navigationButtons.remove(functionalityIid);
            closeButton = closeButtons.remove(functionalityIid);
            if (navigationButton != null) jToolBarNavigation.remove(navigationButton);
            if (closeButton != null) jToolBarNavigation.remove(closeButton);
            jToolBarNavigation.revalidate();
            jToolBarNavigation.repaint();

            // Release the functionality.
            if (view.getExecutionHandle().requiresExplicitRelease())
            {
                try
                {
                    functionalityManager.releaseFunctionality(context, functionalityIid);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while releasing functionality: " + functionalityIid, e);
                }
            }
        }
    }

    public void closeAllFunctionalityViews()
    {
        List<String> instanceIdentifiers;

        instanceIdentifiers = new ArrayList<>(functionalityViews.keySet());
        for (String instanceIdentifier : instanceIdentifiers)
        {
            closeFunctionalityView(instanceIdentifier);
        }
    }

    private class FunctionalityNavigationButton extends JToggleButton
    {
        private final T8FunctionalityAccessHandle executionHandle;

        private FunctionalityNavigationButton(T8FunctionalityAccessHandle executionHandle)
        {
            String functionalityDisplayName;

            this.executionHandle = executionHandle;
            functionalityDisplayName = executionHandle.getDisplayName();
            setIcon(executionHandle.getIcon());
            setToolTipText(translate("Navigate to ") + functionalityDisplayName);
            setFocusable(false);
            setFont(NAVIGATION_BUTTON_FONT);
            setText(functionalityDisplayName);
            setHorizontalTextPosition(javax.swing.SwingConstants.TRAILING);
            setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
            setMargin(new java.awt.Insets(0, 0, 0, 0));
            setOpaque(false);
            addActionListener(new FunctionalityNavigationListener());
        }

        public String getFunctionalityInstanceIdentifier()
        {
            return executionHandle.getFunctionalityIid();
        }
    }

    private class FunctionalityCloseButton extends JButton
    {
        private final T8FunctionalityAccessHandle executionHandle;

        private FunctionalityCloseButton(T8FunctionalityAccessHandle executionHandle)
        {
            this.executionHandle = executionHandle;
            setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/crossIcon.png")));
            setToolTipText(translate("Close ") + executionHandle.getDisplayName());
            setFocusable(false);
            setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
            setMargin(new java.awt.Insets(0, 0, 0, 0));
            setOpaque(false);
            setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
            addActionListener(new FunctionalityCloseListener());
        }

        public String getFunctionalityInstanceIdentifier()
        {
            return executionHandle.getFunctionalityIid();
        }
    }

    private class FunctionalityCloseListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            FunctionalityCloseButton closeButton;

            closeButton = (FunctionalityCloseButton)e.getSource();
            closeFunctionalityView(closeButton.getFunctionalityInstanceIdentifier());
        }
    }

    private class FunctionalityNavigationListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            FunctionalityNavigationButton navigationButton;

            navigationButton = (FunctionalityNavigationButton)e.getSource();
            switchToFunctionalityView(navigationButton.getFunctionalityInstanceIdentifier());
        }
    }

    private class FunctionalityInitializer extends SwingWorker<T8FunctionalityAccessHandle, Void>
    {
        private final String functionalityId;
        private final Map<String, Object> parameters;
        private T8FunctionalityAccessHandle functionalityAccessHandle;
        private T8FunctionalityView functionalityView;

        private FunctionalityInitializer(String functionalityId, Map<String, Object> parameters)
        {
            this.functionalityId = functionalityId;
            this.parameters = parameters;
        }

        @Override
        protected T8FunctionalityAccessHandle doInBackground() throws Exception
        {
            // Construct the functionality instance and initialize it.
            functionalityAccessHandle = functionalityManager.accessFunctionality(context, functionalityId, parameters);
            if (functionalityAccessHandle.getAccessType() == T8FunctionalityAccessType.ACCESS)
            {
                // Construct the functionality view and initialize it.
                // TODO:  This method can be improved.  There is no need for the access handle to return a view component.  Instead,
                // we can send the functionality controller to the access handle and allow it to create any view it wants, adding it to the controller if required.
                // This would be useful for handle types than need to show dialogs and don't need to return a view component to be permanently added to the controller.
                functionalityView = functionalityAccessHandle.createViewComponent(controller);
                if (functionalityView != null)
                {
                    // Initialize the functionality view and add it to the UI.
                    functionalityView.initialize();

                    // Add the new view to the UI.
                    addFunctionalityView(functionalityView);
                    functionalityView.startComponent();
                }
            }
            else
            {
                functionalityView = null;
            }

            // Return the functionality execution handler.
            return functionalityAccessHandle;
        }

        @Override
        public void done()
        {
            // Unlock the UI.
            container.unlock();
        }
    }

    private class FunctionalityViewListener implements T8FunctionalityViewListener
    {
        @Override
        public void functionalityViewUpdated(T8FunctionalityViewUpdatedEvent event)
        {
        }

        @Override
        public void functionalityViewClosed(T8FunctionalityViewClosedEvent event)
        {
            T8Log.log("Received signal that functionality view has closed: " + event.getFunctionalityView().getExecutionHandle());
            closeFunctionalityView(event.getFunctionalityView().getExecutionHandle().getFunctionalityIid());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
