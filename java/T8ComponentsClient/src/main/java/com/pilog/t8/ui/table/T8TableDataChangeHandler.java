package com.pilog.t8.ui.table;

import static com.pilog.t8.definition.ui.table.T8TableAPIHandler.*;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.client.T8ClientScriptRunner;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.entity.T8DataEntityValidationError;
import com.pilog.t8.data.entity.T8DataEntityValidationReport;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.script.validation.entity.T8ClientEntityValidationScriptDefinition;
import com.pilog.t8.definition.script.validation.entity.T8EntityValidationScriptDefinition;
import com.pilog.t8.definition.ui.table.T8TableAPIHandler;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.definition.ui.table.T8TableRowChange;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.table.T8DefaultTableModel.ValueChangeListener;
import com.pilog.t8.ui.table.T8DefaultTableModel.ValueChangedEvent;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.strings.Strings;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.JTable;

/**
 * The T8Table model holds the entity for each row in the first column of the
 * model.  This column is a system column and is never altered by the user.
 * When one of the table cells is updated, the change is then first logged and
 * then applied to the row entity.  At all times the entity in the first column
 * must contain the same field values as the table model row that displays its
 * content to the user.
 *
 * @author Bouwer du Preez
 */
public class T8TableDataChangeHandler
{
    private final LinkedHashMap<String, T8TableRowChange> tableInserts;
    private final LinkedHashMap<String, T8TableRowChange> tableUpdates;
    private final LinkedHashMap<String, T8TableRowChange> tableDeletes;
    private final TableValueChangeListener tableChangeListener;
    private final T8TableDefinition tableDefinition;
    private final T8DataEntityDefinition dataEntityDefinition;
    private final List<String> entityFieldIds;
    private T8DefaultTableModel tableModel;
    private final JTable table;
    private final T8Table t8Table;

    public T8TableDataChangeHandler(T8TableDefinition tableDefinition, T8DataEntityDefinition dataEntityDefinition, T8Table t8Table, JTable table)
    {
        this.table = table;
        this.t8Table = t8Table;
        this.tableDefinition = tableDefinition;
        this.dataEntityDefinition = dataEntityDefinition;
        this.entityFieldIds = dataEntityDefinition.getFieldIdentifiers(false);
        this.tableModel = (T8DefaultTableModel)table.getModel();
        this.tableInserts = new LinkedHashMap<>();
        this.tableUpdates = new LinkedHashMap<>();
        this.tableDeletes = new LinkedHashMap<>();
        this.tableChangeListener = new TableValueChangeListener();

        // Add all listeners.
        this.tableModel.addValueChangeListener(tableChangeListener);
        this.table.addPropertyChangeListener(new TablePropertyChangeListener());
    }

    public void clearChanges()
    {
        tableInserts.clear();
        tableUpdates.clear();
        tableDeletes.clear();
    }

    public boolean hasTableChanges()
    {
        if (tableInserts.size() > 0) return true;
        else if (tableUpdates.size() > 0) return true;
        else return (tableDeletes.size() > 0);
    }

    public LinkedHashMap<String, T8TableRowChange> getTableChanges()
    {
        LinkedHashMap<String, T8TableRowChange> tableChanges;

        tableChanges = new LinkedHashMap<>();
        tableChanges.putAll(tableInserts);
        tableChanges.putAll(tableUpdates);
        tableChanges.putAll(tableDeletes);

        return tableChanges;
    }

    public void deleteRows(List<T8DataEntity> entities)
    {
        for (T8DataEntity entity : entities)
        {
            int rowIndex;

            rowIndex = tableModel.getRowIndex(entity);
            if (rowIndex > -1)
            {
                logRowDelete(entity);
                tableModel.removeRow(rowIndex);
            }
            else throw new RuntimeException("Entity to be deleted not found in table model: " + entity);
        }
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    private Vector<Object> copyFromEntity(T8DataEntity entity)
    {
        Vector<Object> dataRow;

        dataRow = new Vector<>();
        dataRow.add(new T8TableRowHeader(tableModel, entity, 0));
        entity.getFieldValues().values().stream().forEach(dataRow::add);

        return dataRow;
    }

    private T8DataEntity copyEntity(T8DataEntity fromEntity, boolean copyNonEditableOnly) throws EPICSyntaxException, EPICRuntimeException
    {
        T8ClientExpressionEvaluator expressionEvaluator;
        Map<String, Object> key;
        T8DataEntity entityCopy;

        entityCopy = fromEntity.copy();
        if (copyNonEditableOnly)
        {
            // Create an expression evaluator for default value expressions.
            expressionEvaluator = new T8ClientExpressionEvaluator(this.t8Table.getController().getContext());

            // The table key values
            key = t8Table.getKey();

            for (T8TableColumnDefinition columnDefinition : this.tableDefinition.getColumnDefinitions())
            {
                // If the column is not editable, we want to keep the copied value for it
                if (!columnDefinition.isEditable()) continue;

                String defaultValueExpression;
                String fieldIdentifier;
                Object defaultValue;

                fieldIdentifier = columnDefinition.getFieldIdentifier();
                defaultValue = (key != null) ? key.get(fieldIdentifier) : null;
                defaultValueExpression = columnDefinition.getDefaultValueExpression();

                if (!Strings.isNullOrEmpty(defaultValueExpression))
                {
                    defaultValue = expressionEvaluator.evaluateExpression(defaultValueExpression, null, null);
                }

                entityCopy.setFieldValue(fieldIdentifier, defaultValue);
            }
        }

        return entityCopy;
    }

    /**
     * Creates copies of the specified entities within the table. The copy count
     * is used to determine the number of copies of each entity to be created.
     * The insertion index has to be specified, since the origin of the entities
     * from which the copies are being made does not necessarily have to be the
     * selected rows, or even a row within the table.<br/>
     * <br/>
     * When copying non-editable values only, key and default values are still
     * populated for editable columns, which could be the value from the
     * originating entity.
     *
     * @param fromEntities The {@code T8DataEntity} list from which the copies
     *      should be made
     * @param copyCount The {@code int} number of copies of each data entity to
     *      be created
     * @param insertionStartIndex The {@code int} row index at which the copied rows
     *      should start being inserted at
     * @param copyHidden {@code false} to exclude the copying of hidden column
     *      values
     * @param copyNonEditableOnly {@code true} to copy only non-editable column
     *      values
     */
    @SuppressWarnings("UseOfObsoleteCollectionType")
    void copyRows(List<T8DataEntity> fromEntities, int copyCount, int insertionStartIndex, boolean copyHidden, boolean copyNonEditableOnly)
    {
        List<T8DataEntity> copiedEntities;
        List<Vector<Object>> copiedRows;
        T8DataEntity copiedEntity;
        Vector<Object> copiedRow;
        int currentInsertIndex;

        try
        {
            // Ensure that all editing has stopped
            stopTableEditing();

            // Create the list of copied entities and rows
            copiedEntities = new ArrayList<>();
            copiedRows = new ArrayList<>();

            for (T8DataEntity fromEntity : fromEntities)
            {
                // Create the initial copies to ensure we don't modify original data
                copiedEntity = copyEntity(fromEntity, copyNonEditableOnly);
                copiedRow = copyFromEntity(copiedEntity);

                if (!copyHidden) {/*TODO: GBO - Remove values from hidden columns, except for key/default values.*/}

                for (int copy = 0; copy < copyCount; copy++)
                {
                    // Copy and add the entity
                    copiedEntity = copiedEntity.copy();
                    copiedEntities.add(copiedEntity);

                    // Create and add the vector row
                    copiedRow = new Vector<>(copiedRow);
                    copiedRow.setElementAt(new T8TableRowHeader(tableModel, copiedEntity, 0), 0);
                    copiedRows.add(copiedRow);
                }
            }

            // Insert the copied rows in the table.
            this.table.getSelectionModel().setValueIsAdjusting(true);
            this.table.clearSelection();

            // Keep track of the start and current insertion index
            currentInsertIndex = insertionStartIndex-1;
            for (T8DataEntity entityCopy : copiedEntities)
            {
                currentInsertIndex++;
                tableModel.addDataEntity(entityCopy, currentInsertIndex);
                logRowInsert(entityCopy);
            }

            // Set the selection based on the selection mode
            switch (this.tableDefinition.getSelectionMode())
            {
                case SINGLE_SELECTION:
                    this.table.getSelectionModel().setSelectionInterval(insertionStartIndex, insertionStartIndex);
                    break;
                default:
                    this.table.getSelectionModel().setSelectionInterval(insertionStartIndex, currentInsertIndex);
            }

            this.table.getSelectionModel().setValueIsAdjusting(false);
        }
        catch (EPICSyntaxException | EPICRuntimeException | RuntimeException ex)
        {
            T8Log.log("Failed to complete the row editing process.", ex);
        }
    }

    void fillDown(T8DataEntity fromEntity, int columnIndex, List<Integer> fillDownRows)
    {
        String fieldIdentifier;
        Object fillDownValue;

        fieldIdentifier = this.tableDefinition.getColumnDefinitions().get(columnIndex).getFieldIdentifier();
        fillDownValue = fromEntity.getFieldValue(fieldIdentifier);

        // This should trigger the table change event which will handle the rest.
        fillDownRows.stream().forEach((rowIndex) -> this.tableModel.setValueAt(fillDownValue, rowIndex, columnIndex+1));
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public void insertNewRow()
    {
        try
        {
            ArrayList<T8TableColumnDefinition> columnDefinitions;
            T8ClientExpressionEvaluator expressionEvaluator;
            Map<String, Object> key;
            T8DataEntity rowEntity;
            Vector newRow;
            int selectedRowIndex;
            int insertionRowIndex;

            // Ensure that all editing has stopped.
            stopTableEditing();

            // Create an expression evaluator for default value expressions.
            expressionEvaluator = new T8ClientExpressionEvaluator(t8Table.getController().getContext());

            // Get some information from the table and its definition.
            columnDefinitions = tableDefinition.getColumnDefinitions();
            selectedRowIndex = table.getSelectedRow();
            insertionRowIndex = selectedRowIndex > -1 ? selectedRowIndex : 0;
            key = t8Table.getKey();

            // Create a new entity and an array to hold the table row data to be added.
            rowEntity = new T8DataEntity(dataEntityDefinition);
            newRow = new Vector();
            newRow.add(new T8TableRowHeader(tableModel, rowEntity, 0));

            // Add all of the default and key values to the new entity and row.
            for (T8TableColumnDefinition columnDefinition : columnDefinitions)
            {
                Object defaultValue;
                String defaultValueExpression;
                String fieldId;

                fieldId = columnDefinition.getFieldIdentifier();
                defaultValue = (key != null) ? key.get(fieldId) : null;
                defaultValueExpression = columnDefinition.getDefaultValueExpression();

                if ((defaultValueExpression != null) && (defaultValueExpression.trim().length() > 0))
                {
                    defaultValue = expressionEvaluator.evaluateExpression(defaultValueExpression, null, null);
                }

                newRow.add(defaultValue);
                rowEntity.setFieldValue(fieldId, defaultValue);
            }

            // Insert the row in the table.
            table.getSelectionModel().setValueIsAdjusting(true);
            table.clearSelection();
            tableModel.addDataEntity(rowEntity, insertionRowIndex);

            logRowInsert(rowEntity);
            table.getSelectionModel().setSelectionInterval(insertionRowIndex, insertionRowIndex);
            table.getSelectionModel().setValueIsAdjusting(false);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while inserting new row.", e);
        }
    }

    public boolean commitTableChanges(T8Context context) throws Exception
    {
        List<T8DataEntityValidationReport> validationReportList;
        HashMap<String, Object> inputParameters;
        Map<String, Object> outputParameters;
        ArrayList<T8TableRowChange> dataChanges;

        // Combine all outstanding table changes in one list.
        dataChanges = new ArrayList<>();
        dataChanges.addAll(tableUpdates.values());
        dataChanges.addAll(tableInserts.values());
        dataChanges.addAll(tableDeletes.values());

        // Create the server operation parameters collection.
        inputParameters = new HashMap<>();
        inputParameters.put(PARAMETER_TABLE_ID, tableDefinition.getPublicIdentifier());
        inputParameters.put(PARAMETER_TABLE_ROW_CHANGE_LIST, dataChanges);

        // Execute the server operation and check for validation errors.
        outputParameters = T8MainServerClient.executeSynchronousOperation(context, SERVER_OPERATION_TABLE_SAVE_DATA_CHANGES, inputParameters);
        validationReportList = outputParameters != null ? (List<T8DataEntityValidationReport>)outputParameters.get(PARAMETER_VALIDATION_REPORT_LIST) : null;
        if (validationReportList == null || validationReportList.isEmpty())
        {
            // Fire the API event to indicate that changes have been saved.
            fireDataChangesSavedEvent(getInsertedEntitiesList(), getDeletedEntitiesList(), getUpdatedOldEntitiesList(), getUpdatedNewEntitiesList());

            // No validation errors, so changes were committed.  Clear existing changes to reflect this fact.
            // We only clear the changes after firing the event to ensure the changes are pushed through to the event
            clearChanges();
            return true;
        }
        else
        {
            for (T8DataEntityValidationReport validationErrorReport : validationReportList)
            {
                String rowGuid;

                rowGuid = validationErrorReport.getEntityGUID();
                if (!Strings.isNullOrEmpty(rowGuid))
                {
                    T8TableRowHeader rowHeader;

                    rowHeader = getRowHeader(rowGuid);
                    if (rowHeader != null)
                    {
                        rowHeader.setValidationErrors(validationErrorReport.getValidationErrors());
                    }
                    else throw new RuntimeException("Validation Report returned an entity GUID that could not be mapped back to the table: " + rowGuid);
                }
                else throw new RuntimeException("Validation Report does not contain the row GUID identifier to map back to table.");
            }

            // Fire the API event to indicate that changes failed to save.
            fireDataChangesSaveFailedEvent(getInsertedEntitiesList(), getDeletedEntitiesList(), getUpdatedOldEntitiesList(), getUpdatedNewEntitiesList(), validationReportList);
            return false;
        }
    }

    public ArrayList<T8DataEntity> getInsertedEntitiesList()
    {
        ArrayList<T8DataEntity> insertedEntities;

        // Create a list of all inserted entities.
        insertedEntities = new ArrayList<>();
        for (T8TableRowChange rowChange : tableInserts.values())
        {
            insertedEntities.add(rowChange.getNewDataEntity());
        }

        return insertedEntities;
    }

    public ArrayList<T8DataEntity> getDeletedEntitiesList()
    {
        ArrayList<T8DataEntity> deletedEntities;

        // Create a list of all deleted entities.
        deletedEntities = new ArrayList<>();
        for (T8TableRowChange rowChange : tableDeletes.values())
        {
            deletedEntities.add(rowChange.getOldDataEntity());
        }

        return deletedEntities;
    }

    public ArrayList<T8DataEntity> getUpdatedOldEntitiesList()
    {
        ArrayList<T8DataEntity> updatedOldEntities;

        // Create a list of all updated entities.
        updatedOldEntities = new ArrayList<>();
        for (T8TableRowChange rowChange : tableUpdates.values())
        {
            updatedOldEntities.add(rowChange.getOldDataEntity());
        }

        return updatedOldEntities;
    }

    public ArrayList<T8DataEntity> getUpdatedNewEntitiesList()
    {
        ArrayList<T8DataEntity> updatedNewEntities;

        // Create a list of all updated entities.
        updatedNewEntities = new ArrayList<>();
        for (T8TableRowChange rowChange : tableUpdates.values())
        {
            updatedNewEntities.add(rowChange.getNewDataEntity());
        }

        return updatedNewEntities;
    }

    public T8TableRowChange getUpdateChange(String rowGUID)
    {
        return tableUpdates.get(rowGUID);
    }

    public boolean isRowUpdated(String rowGUID)
    {
        return tableUpdates.containsKey(rowGUID);
    }

    public boolean isRowInserted(String rowGUID)
    {
        return tableInserts.containsKey(rowGUID);
    }

    private void stopTableEditing()
    {
        // Ensure that all editing has stopped.
        if (table.getCellEditor() != null) table.getCellEditor().stopCellEditing();
    }

    private void printTableChanges()
    {
        T8Log.log("Table Changes");
        T8Log.log("=============");
        T8Log.log("Inserted Rows:");
        for (T8TableRowChange change : tableInserts.values())
        {
            T8Log.log("" + change.getNewDataEntity());
        }
        T8Log.log("Updated Rows:");
        for (T8TableRowChange change : tableUpdates.values())
        {
            T8Log.log(change.getOldDataEntity().getFieldValues() + " -> " + change.getNewDataEntity().getFieldValues());
        }
        T8Log.log("Deleted Rows:");
        for (T8TableRowChange change : tableDeletes.values())
        {
            T8Log.log("" + change.getOldDataEntity());
        }
    }

    private void logRowInsert(T8DataEntity rowData)
    {
        String rowGuid;

        rowGuid = rowData.getGUID();
        if (!tableInserts.containsKey(rowGuid))
        {
            tableInserts.put(rowGuid, new T8TableRowChange(T8TableRowChange.CHANGE_TYPE_INSERT, null, rowData));
        }
        else throw new RuntimeException("Duplicate system row GUID found during row insertion.");

        // Print the table changes.
        printTableChanges();

        // Validate the row.
        validateRow(getRowHeader(rowGuid));
    }

    private void logRowUpdate(int rowIndex, String fieldId, Object oldDataValue, Object newDataValue)
    {
        T8DataEntity newRowEntity;
        String rowGuid;

        newRowEntity = getRowEntity(rowIndex);
        rowGuid = newRowEntity.getGUID();
        if (tableInserts.containsKey(rowGuid)) // The row being updated was previously inserted.
        {
            T8DataEntity rowData;

            rowData = tableInserts.get(rowGuid).getNewDataEntity();
            rowData.setFieldValue(fieldId, newRowEntity.getFieldValue(fieldId));
        }
        else if (tableUpdates.containsKey(rowGuid)) // The row being updated was previously updated.
        {
            T8TableRowChange tableChange;

            // Get the existing table change that holds the updated field values.
            tableChange = tableUpdates.get(rowGuid);

            // If the updated new data entity is again equal to the old data, then the change has been reverted so discard it.
            if (T8DataUtilities.isEqual(newRowEntity, tableChange.getOldDataEntity()))
            {
                tableUpdates.remove(rowGuid);
            }
            else // Get the existing change and add the new change/update to it.
            {
                T8DataEntity rowData;

                rowData = tableChange.getNewDataEntity();
                rowData.setFieldValue(fieldId, newRowEntity.getFieldValue(fieldId));
            }
        }
        else // The row being updated is now newly altered.
        {
            // Compare the old and new values to ensure that an update is only logged if the value actually changed.
            if ((oldDataValue == null) && (newDataValue == null))
            {
                return; // The old and new values are the same, so no real update happened.
            }
            else if ((oldDataValue != null) && (oldDataValue.equals(newDataValue)))
            {
                return; // The old and new values are the same, so no real update happened.
            }
            else // The values differ, so log the update.
            {
                T8DataEntity oldRowEntity;

                // Create an 'old' state entity for the affected row.
                oldRowEntity = newRowEntity.copy();
                oldRowEntity.setGUID(rowGuid);
                oldRowEntity.setFieldValue(fieldId, oldDataValue);
                tableUpdates.put(rowGuid, new T8TableRowChange(T8TableRowChange.CHANGE_TYPE_UPDATE, oldRowEntity, newRowEntity));
            }
        }

        // Print the table changes.
        printTableChanges();

        // Validate the row.
        validateRow(getRowHeader(rowIndex));
    }

    private void logRowDelete(T8DataEntity rowData)
    {
        String rowGuid;

        rowGuid = rowData.getGUID();
        if (tableInserts.containsKey(rowGuid)) // The row being deleted was previously inserted.
        {
            tableInserts.remove(rowGuid);
        }
        else if (tableUpdates.containsKey(rowGuid)) // The row being deleted was previously updated.
        {
            tableUpdates.remove(rowGuid);
            tableDeletes.put(rowGuid, new T8TableRowChange(T8TableRowChange.CHANGE_TYPE_DELETE, rowData, null));
        }
        else // The row being deleted has not been previously inserted or updated.
        {
            tableDeletes.put(rowGuid, new T8TableRowChange(T8TableRowChange.CHANGE_TYPE_DELETE, rowData, null));
        }

        printTableChanges();
    }

    private T8TableRowHeader getRowHeader(String entityGuid)
    {
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            T8TableRowHeader rowHeader;

            rowHeader = getRowHeader(rowIndex);
            if (rowHeader.getDataEntity().getGUID().equals(entityGuid))
            {
                return rowHeader;
            }
        }

        return null;
    }

    private List<T8TableRowHeader> getRowHeaders()
    {
        List<T8TableRowHeader> headers;

        headers = new ArrayList<>();
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
        {
            headers.add(getRowHeader(rowIndex));
        }

        return headers;
    }

    private List<T8TableRowHeader> getChangedRowHeaders()
    {
        List<T8TableRowHeader> headers;

        headers = new ArrayList<>();
        for (String insertedRowGuid : tableInserts.keySet())
        {
            headers.add(getRowHeader(insertedRowGuid));
        }

        for (String updatedRowGuid : tableUpdates.keySet())
        {
            headers.add(getRowHeader(updatedRowGuid));
        }

        return headers;
    }

    private T8TableRowHeader getRowHeader(int rowIndex)
    {
        return (T8TableRowHeader)tableModel.getValueAt(rowIndex, 0);
    }

    private T8DataEntity getRowEntity(int rowIndex)
    {
        return ((T8TableRowHeader)tableModel.getValueAt(rowIndex, 0)).getDataEntity();
    }

    private void fireDataChangesSavedEvent(List<T8DataEntity> insertedEntities, List<T8DataEntity> deletedEntities, List<T8DataEntity> updatedOldEntities, List<T8DataEntity> updatedNewEntities)
    {
        HashMap<String, Object> eventParameters;

        eventParameters = new HashMap<>();
        eventParameters.put(T8TableAPIHandler.PARAMETER_INSERTED_ENTITY_LIST, insertedEntities);
        eventParameters.put(T8TableAPIHandler.PARAMETER_DELETED_ENTITY_LIST, deletedEntities);
        eventParameters.put(T8TableAPIHandler.PARAMETER_UPDATED_OLD_ENTITY_LIST, updatedOldEntities);
        eventParameters.put(T8TableAPIHandler.PARAMETER_UPDATED_NEW_ENTITY_LIST, updatedNewEntities);
        t8Table.getController().reportEvent(t8Table, tableDefinition.getComponentEventDefinition(T8TableAPIHandler.EVENT_DATA_CHANGES_SAVED), eventParameters);
    }

    private void fireDataChangesSaveFailedEvent(ArrayList<T8DataEntity> insertedEntities, ArrayList<T8DataEntity> deletedEntities, ArrayList<T8DataEntity> updatedOldEntities, ArrayList<T8DataEntity> updatedNewEntities, List<T8DataEntityValidationReport> validationErrorList)
    {
        HashMap<String, Object> eventParameters;

        eventParameters = new HashMap<>();
        eventParameters.put(T8TableAPIHandler.PARAMETER_INSERTED_ENTITY_LIST, insertedEntities);
        eventParameters.put(T8TableAPIHandler.PARAMETER_DELETED_ENTITY_LIST, deletedEntities);
        eventParameters.put(T8TableAPIHandler.PARAMETER_UPDATED_OLD_ENTITY_LIST, updatedOldEntities);
        eventParameters.put(T8TableAPIHandler.PARAMETER_UPDATED_NEW_ENTITY_LIST, updatedNewEntities);
        eventParameters.put(T8TableAPIHandler.PARAMETER_VALIDATION_ERROR_LIST, validationErrorList);

        t8Table.getController().reportEvent(t8Table, tableDefinition.getComponentEventDefinition(T8TableAPIHandler.EVENT_DATA_CHANGES_SAVE_FAILED), eventParameters);
    }

    private class TableValueChangeListener implements ValueChangeListener
    {
        @Override
        public void valueChanged(ValueChangedEvent e)
        {
            String fieldId;

            // Get the edited row entity and copy it so that it may be kept as 'old data'.
            fieldId = t8Table.getComponentDefinition().getColumnDefinitions().get(e.getColumnIndex() -1).getFieldIdentifier();

            // Now log the update as a change.
            logRowUpdate(e.getRowIndex(), fieldId, e.getOldValue(), e.getNewValue());

            // Repaint row headers to allow conditional rendering to take effect.
            t8Table.getTableViewHandler().repaintRowHeaders();
        }
    }

    private class TablePropertyChangeListener implements PropertyChangeListener
    {
        @Override
        public void propertyChange(PropertyChangeEvent evt)
        {
            if (evt.getPropertyName().equalsIgnoreCase("model"))
            {
                tableModel.removeValueChangeListener(tableChangeListener);
                tableModel = (T8DefaultTableModel)evt.getNewValue();
                tableModel.addValueChangeListener(tableChangeListener);
            }
        }
    }

    public boolean validateRow(T8TableRowHeader rowHeader)
    {
        return validateRows(ArrayLists.newArrayList(rowHeader));
    }

    public boolean validateAllRows()
    {
        return validateRows(getRowHeaders());
    }

    public boolean validateChangedRows()
    {
        return validateRows(getChangedRowHeaders());
    }

    public boolean validateRows(List<T8TableRowHeader> rowHeaderList)
    {
        T8ClientEntityValidationScriptDefinition clientValidationScriptDefinition;
        List<T8DataEntity> entityList;
        boolean valid;

        // Create a list of entities with indices corresponding to the list of row headers.
        entityList = new ArrayList<>();
        for (T8TableRowHeader rowHeader : rowHeaderList)
        {
            entityList.add(rowHeader.getDataEntity());
        }

        // Do the client-side validation of the rows.
        valid = true;
        clientValidationScriptDefinition = tableDefinition.getClientValidationScriptDefinition();
        if (clientValidationScriptDefinition != null)
        {
            T8ClientScriptRunner scriptRunner;

            scriptRunner = new T8ClientScriptRunner(t8Table.getController());

            try
            {
                scriptRunner.prepareScript(clientValidationScriptDefinition);
                for (T8TableRowHeader rowHeader : rowHeaderList)
                {
                    Map<String, Object> scriptOutputParameters;
                    Map<String, Object> scriptInputParameters;
                    T8DataEntity rowEntity;
                    Boolean validationSuccess;

                    rowEntity = rowHeader.getDataEntity();
                    scriptInputParameters = new HashMap<>();
                    scriptInputParameters.put(T8EntityValidationScriptDefinition.PARAMETER_DATA_ENTITY, rowEntity);

                    scriptOutputParameters = scriptRunner.runScript(scriptInputParameters);
                    validationSuccess = (Boolean)scriptOutputParameters.get(clientValidationScriptDefinition.getNamespace() + T8EntityValidationScriptDefinition.PARAMETER_VALIDATION_SUCCESS);
                    if (validationSuccess != null)
                    {
                        if (!validationSuccess)
                        {
                            List<T8DataEntityValidationError> validationErrors;

                            valid = false;
                            validationErrors = (List<T8DataEntityValidationError>)scriptOutputParameters.get(clientValidationScriptDefinition.getNamespace() + T8EntityValidationScriptDefinition.PARAMETER_VALIDATION_ERROR_LIST);
                            if (validationErrors != null && !validationErrors.isEmpty())
                            {
                                rowHeader.setValidationErrors(validationErrors);
                            }
                            else throw new Exception("Validation script '" + clientValidationScriptDefinition + "' reported validation failure but did not return validation errors: " + scriptOutputParameters);
                        }
                        else
                        {
                            // Clear any existing validation errors.
                            rowHeader.setValidationErrors(null);
                        }
                    }
                    else throw new Exception("Validation script '" + clientValidationScriptDefinition + "' did not return valid success parameter: " + scriptOutputParameters);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exeption while executing client-side entity validation.", e);
            }
            finally
            {
                scriptRunner.finalizeScript();
            }
        }

        // Repaint the table row headers to allow any conditional rendering to take place after validation.
        t8Table.getTableViewHandler().repaintRowHeaders();
        return valid;
    }
}