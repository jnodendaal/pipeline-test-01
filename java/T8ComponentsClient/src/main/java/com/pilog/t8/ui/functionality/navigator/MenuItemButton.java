package com.pilog.t8.ui.functionality.navigator;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class MenuItemButton extends JButton
{
    private Painter backgroundPainter;
    private Painter defaultBackgroundPainter;
    private Painter focusBackgroundPainter;
    private Painter selectedBackgroundPainter;
    private boolean mouseOver;

    public MenuItemButton(Painter backgroundPainter, Painter focusBackgroundPainter, Painter selectedBackgroundPainter)
    {
        this.backgroundPainter = backgroundPainter;
        this.defaultBackgroundPainter = backgroundPainter;
        this.focusBackgroundPainter = focusBackgroundPainter;
        this.selectedBackgroundPainter = selectedBackgroundPainter;
        this.mouseOver = false;
        this.setHorizontalTextPosition(SwingConstants.LEADING);
        this.setHorizontalAlignment(SwingConstants.RIGHT);
        this.setContentAreaFilled(false);
        this.setBorderPainted(false);
        this.setOpaque(false);
        this.setMargin(new Insets(10, 10, 10, 10));
        addMouseListener(new ButtonMouseListener());
    }
    
    private void setBackgroundPainter(Painter painter)
    {
        this.backgroundPainter = painter;
        repaint();
    }

    public void setDefaultBackgroundPainter(Painter painter)
    {
        this.defaultBackgroundPainter = painter;
        if (this.backgroundPainter == null) setBackgroundPainter(defaultBackgroundPainter);
    }

    public void setFocusBackgroundPainter(Painter painter)
    {
        this.focusBackgroundPainter = painter;
        repaint();
    }

    public void setSelectedBackgroundPainter(Painter painter)
    {
        this.selectedBackgroundPainter = painter;
        repaint();
    }
    
    @Override
    public void setText(String text)
    {
        StringBuffer newText;

        newText = new StringBuffer();
        newText.append("<html><p align='right'>");
        newText.append(text);
        newText.append("</p></html>");
        super.setText(newText.toString());
    }

    @Override
    public void setSelected(boolean selected)
    {
        super.setSelected(selected);
        setBackgroundPainter(selected ? selectedBackgroundPainter : mouseOver ? focusBackgroundPainter : defaultBackgroundPainter);
    }
    
    @Override
    public void paint(Graphics g)
    {
        if (backgroundPainter != null)
        {
            Graphics2D g2;
            
            g2 = (Graphics2D)g;
            backgroundPainter.paint(g2, this, this.getWidth(), this.getHeight());
            super.paint(g);
        }
        else
        {
            super.paint(g);
        }
    }

    private class ButtonMouseListener implements MouseListener
    {
        @Override
        public void mouseEntered(MouseEvent m)
        {
            mouseOver = true;
            setBackgroundPainter(focusBackgroundPainter);
        }

        @Override
        public void mouseExited(MouseEvent m)
        {
            Point locationRelativeToButton;
            
            locationRelativeToButton = m.getLocationOnScreen();
            SwingUtilities.convertPointFromScreen(m.getLocationOnScreen(), MenuItemButton.this);
            if (!contains(locationRelativeToButton))
            {
                mouseOver = false;
                setBackgroundPainter(isSelected() ? selectedBackgroundPainter : defaultBackgroundPainter);
            }
        }

        @Override
        public void mouseClicked(MouseEvent e)
        {
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
        }
    }
}
