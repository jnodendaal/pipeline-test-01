/**
 * Created on 10 Sep 2015, 11:03:08 AM
 */
package com.pilog.t8.ui.checkbox;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.checkbox.T8MultiSelectionCheckBoxGroupAPIHandler;
import com.pilog.t8.definition.ui.checkbox.T8MultiSelectionCheckBoxGroupDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author Gavin Boshoff
 */
public class T8MultiSelectionCheckBoxGroup extends JPanel implements T8Component
{
    private static final long serialVersionUID = 4915441187016163546L;

    private final T8MultiSelectionCheckBoxGroupOperationHandler operationHandler;
    private final T8MultiSelectionCheckBoxGroupDefinition definition;
    private final LinkedHashMap<String, T8CheckBox> childComponents;
    private final T8ComponentController controller;

    private final MultiSelectionChangeListener selectionChangeListener;

    public T8MultiSelectionCheckBoxGroup(T8MultiSelectionCheckBoxGroupDefinition checkboxGroupDefinition, T8ComponentController controller)
    {
        this.definition = checkboxGroupDefinition;
        this.controller = controller;
        this.operationHandler = new T8MultiSelectionCheckBoxGroupOperationHandler(this);
        this.childComponents = new LinkedHashMap<>();
        this.selectionChangeListener = new MultiSelectionChangeListener();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setOpaque(definition.isOpaque());
    }

    @Override
    public T8ComponentController getController()
    {
        return this.controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return this.definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        if (childComponent instanceof T8CheckBox)
        {
            T8ComponentDefinition checkboxDefinition;
            T8CheckBox checkbox;

            checkbox = (T8CheckBox)childComponent;
            checkboxDefinition = checkbox.getComponentDefinition();
            this.childComponents.put(checkboxDefinition.getIdentifier(), checkbox);

            checkbox.addChangeListener(this.selectionChangeListener);
            checkbox.setOpaque(this.definition.isOpaque());
            add(checkbox);
        } else throw new IllegalArgumentException("Only " + T8CheckBox.class + " components can be added to Multi Selection Checkbox Group.");
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return this.childComponents.get(childComponentIdentifier);
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        T8CheckBox checkbox;

        checkbox = (T8CheckBox)getChildComponent(childComponentIdentifier);
        if (checkbox != null)
        {
            checkbox.removeChangeListener(this.selectionChangeListener);
            remove(checkbox);
        }

        return checkbox;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(this.childComponents.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return this.childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
        for (T8CheckBox checkbox : this.childComponents.values())
        {
            checkbox.removeChangeListener(this.selectionChangeListener);
            remove(checkbox);
        }
        childComponents.clear();
    }

    /**
     * {@inheritDoc}
     * <br/><br/>
     * Sets the opaque level of both the button group, as well as each of the
     * buttons contained therein.
     *
     * @param isOpaque {@code true} if the component should be opaque
     */
    @Override
    public void setOpaque(boolean isOpaque)
    {
        super.setOpaque(isOpaque);
        if (this.childComponents != null)
        {
            this.childComponents.values().forEach((T8CheckBox checkbox)->checkbox.setOpaque(isOpaque));
        }
    }

    public void setSelected(List<String> selectionIdentifiers)
    {
        for (String selectionIdentifier : selectionIdentifiers)
        {
            this.childComponents.get(selectionIdentifier).setSelected(true);
        }
    }

    public List<String> getSelectedIdentifiers()
    {
        List<String> selectionList;

        selectionList = new ArrayList<>();
        for (Map.Entry<String, T8CheckBox> childComponent : this.childComponents.entrySet())
        {
            if (childComponent.getValue().isSelected()) selectionList.add(childComponent.getKey());
        }

        return selectionList;
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        this.childComponents.values().forEach((T8CheckBox checkbox)->checkbox.setEnabled(enabled));
    }

    /**
     * A {@code ChangeListener} implementation which will be triggered when
     * changes occur on the child button components. The component event will
     * only be triggered if the change is the selection state of a child
     * component, and the set of selected components are different from the
     * previously selected set.
     */
    private class MultiSelectionChangeListener implements ChangeListener
    {
        private final Set<String> lastSelected;

        {
            this.lastSelected = new HashSet<>();
        }

        @Override
        public void stateChanged(ChangeEvent e)
        {
            Set<String> currentSelection;

            currentSelection = new HashSet<>();
            for (Map.Entry<String, T8CheckBox> childComponent : childComponents.entrySet())
            {
                if (childComponent.getValue().isSelected()) currentSelection.add(childComponent.getKey());
            }

            if (!this.lastSelected.containsAll(currentSelection) || !currentSelection.containsAll(this.lastSelected))
            {
                this.lastSelected.clear();
                this.lastSelected.addAll(currentSelection);

                controller.reportEvent(T8MultiSelectionCheckBoxGroup.this, definition.getComponentEventDefinition(T8MultiSelectionCheckBoxGroupAPIHandler.EVENT_SELECTION_CHANGED), HashMaps.createSingular(T8MultiSelectionCheckBoxGroupAPIHandler.PARAMETER_SELECTED_IDENTIFIER_LIST, currentSelection));
            }
        }
    }
}