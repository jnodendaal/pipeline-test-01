package com.pilog.t8.ui.datumeditor.dataentitylist;

import com.pilog.t8.client.T8ClientScriptRunner;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.ui.event.T8DefinitionDatumEditorListener;
import com.pilog.t8.definition.script.T8DatumEditorConfigurationScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.dataentitylist.T8DataEntityListAPIHandler;
import com.pilog.t8.definition.ui.datumeditor.dataentitylist.T8SingleValueDataEntityListDatumEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.dataentitylist.T8DataEntityList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public class T8SingleValueDataEntityListDatumEditor extends JPanel implements T8DefinitionDatumEditor
{
    private final T8SingleValueDataEntityListDatumEditorDefinition editorDefinition;
    private final T8ComponentController controller;
    private final T8DefinitionContext definitionContext;
    private final T8Definition targetDefinition;
    private final T8DefinitionDatumType datumType;
    private T8DataEntityList entityList;

    public T8SingleValueDataEntityListDatumEditor(T8DefinitionContext definitionContext, T8SingleValueDataEntityListDatumEditorDefinition editorDefinition, T8Definition targetDefinition, T8DefinitionDatumType datumType) throws Exception
    {
        this.editorDefinition = editorDefinition;
        this.controller = new T8DefaultComponentController(new T8Context(definitionContext.getClientContext(), definitionContext.getSessionContext()).setProjectId(editorDefinition.getRootProjectId()), editorDefinition.getIdentifier(), false);
        this.controller.addComponentEventListener(new SelectionEventListener());
        this.definitionContext = definitionContext;
        this.targetDefinition = targetDefinition;
        this.datumType = datumType;
        initComponents();
        initEditor();
        configureEditor();
    }

    private void initEditor() throws Exception
    {
        entityList = (T8DataEntityList)editorDefinition.getNewComponentInstance(controller);
        controller.registerComponent(entityList);
        add(entityList, java.awt.BorderLayout.CENTER);
    }

    private void configureEditor()
    {
        T8DatumEditorConfigurationScriptDefinition configurationScriptDefinition;

        configurationScriptDefinition = editorDefinition.getConfigurationScriptDefinition();
        if ((configurationScriptDefinition != null) && (configurationScriptDefinition.getScript() != null))
        {
            try
            {
                T8ClientScriptRunner scriptRunner;
                Map<String, Object> scriptParameters;

                scriptParameters = new HashMap<>();
                scriptParameters.put(T8DatumEditorConfigurationScriptDefinition.PARAMETER_DEFINITION_IDENTIFIER, getDefinition().getIdentifier());
                scriptParameters.put(T8DatumEditorConfigurationScriptDefinition.PARAMETER_DEFINITION, getDefinition());
                scriptParameters.put(T8DatumEditorConfigurationScriptDefinition.PARAMETER_DATUM_IDENTIFIER, getDatumIdentifier());
                scriptParameters.put(T8DatumEditorConfigurationScriptDefinition.PARAMETER_DATUM_TYPE, getDatumType());

                scriptRunner = new T8ClientScriptRunner(controller);
                scriptRunner.runScript(configurationScriptDefinition, scriptParameters);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while executing datum editor configuration script in editor: " + editorDefinition, e);
            }
        }
    }

    protected void setDefinitionDatum(Object value)
    {
        targetDefinition.setDefinitionDatum(datumType.getIdentifier(), value, this);
    }

    protected Object getDefinitionDatum()
    {
        return targetDefinition.getDefinitionDatum(datumType.getIdentifier());
    }

    private void commitValue()
    {
        List<T8DataEntity> dataEntityList;

        dataEntityList = entityList.getDataEntityList();
        if (dataEntityList != null)
        {
            List<Object> values;

            values = new ArrayList<>();
            for (T8DataEntity entity : dataEntityList)
            {
                values.add(entity.getFieldValue(editorDefinition.getValueFieldIdentifier()));
            }

            setDefinitionDatum(values);
        }
        else setDefinitionDatum(null);
    }

    @Override
    public void initializeComponent()
    {
        entityList.initializeComponent(null);
    }

    @Override
    public void startComponent()
    {
        entityList.startComponent();
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
        entityList.stopComponent();
    }

    @Override
    public void commitChanges()
    {
        commitValue();
    }

    @Override
    public void setEditable(boolean editable)
    {
        entityList.setEnabled(editable);
    }

    @Override
    public void refreshEditor()
    {
        List<Object> valueList;

        valueList = (List<Object>)getDefinitionDatum();
        if (valueList != null)
        {
            List<Map<String, Object>> keyList;

            keyList = new ArrayList<>();
            for (Object value : valueList)
            {
                Map<String, Object> key;

                key = new HashMap<>();
                key.put(editorDefinition.getValueFieldIdentifier(), value);
                keyList.add(key);
            }

            // Set the key list on the entity list.
            try
            {
                entityList.setDataEntityListKeys(keyList, true);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while setting datum entity list keys: " + targetDefinition, e);
            }
        }
    }

    @Override
    public T8Context getContext()
    {
        return definitionContext.getContext();
    }

    @Override
    public T8ClientContext getClientContext()
    {
        return definitionContext.getClientContext();
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return definitionContext.getSessionContext();
    }

    @Override
    public T8DefinitionContext getDefinitionContext()
    {
        return definitionContext;
    }

    @Override
    public String getDatumIdentifier()
    {
        return datumType.getIdentifier();
    }

    @Override
    public T8DefinitionDatumType getDatumType()
    {
        return datumType;
    }

    @Override
    public T8Definition getDefinition()
    {
        return targetDefinition;
    }

    @Override
    public void addDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
    }

    @Override
    public void removeDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
    }

    private class SelectionEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;

            eventDefinition = event.getEventDefinition();
            if (eventDefinition.getIdentifier().equals(T8DataEntityListAPIHandler.EVENT_SEQUENCE_CHANGE))
            {
                commitValue();
            }
            else if (eventDefinition.getIdentifier().equals(T8DataEntityListAPIHandler.EVENT_DATA_ENTITIES_ADDED))
            {
                commitValue();
            }
            else if (eventDefinition.getIdentifier().equals(T8DataEntityListAPIHandler.EVENT_DATA_ENTITIES_REMOVED))
            {
                commitValue();
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
