package com.pilog.t8.ui.functionality.menubar;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.ui.functionality.T8FunctionalityController;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.functionality.menubar.T8FunctionalityMenuBarAPIHandler;
import com.pilog.t8.definition.ui.functionality.menubar.T8FunctionalityMenuBarDefinition;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle.T8FunctionalityAccessType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.menu.T8PopupMenuUI;
import com.pilog.t8.ui.menubar.T8MenuBar;
import com.pilog.t8.utilities.components.menu.MenuItem;
import com.pilog.t8.utilities.strings.StringUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingWorker;
import org.jdesktop.swingx.border.DropShadowBorder;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityMenuBar extends T8MenuBar implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8FunctionalityMenuBar.class);

    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8FunctionalityManager functionalityManager;
    private final T8FunctionalityMenuBarDefinition definition;
    private final T8FunctionalityMenuBarOperationHandler operationHandler;
    private final Map<String, T8FunctionalityHandle> functionalities;
    private final FunctionalityActionListener actionListener;
    private final T8ConfigurationManager configurationManager;
    private final String functionalityGroupId;
    private T8FunctionalityGroupHandle mainFunctionalityGroup;
    private String targetObjectId;
    private String targetObjectIid;
    private Painter menuBackgroundPainter;
    private final ReentrantLock lock;
    private MenuLoader loader;
    private final List<JMenu> functionalityMenus;

    public T8FunctionalityMenuBar(T8FunctionalityMenuBarDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.functionalityManager = clientContext.getFunctionalityManager();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.operationHandler = new T8FunctionalityMenuBarOperationHandler(this);
        this.functionalities = new HashMap<>();
        this.actionListener = new FunctionalityActionListener();
        this.functionalityGroupId = definition.getFunctionalityGroupIdentifier();
        this.lock = new ReentrantLock();
        this.functionalityMenus = new ArrayList<>();
        setupPainters();
    }

    private void setupPainters()
    {
        T8PainterDefinition painterDefinition;

        // Initialize painter for painting the background of the popup menu components.
        painterDefinition = definition.getMenuBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            menuBackgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    @Override
    public void startComponent()
    {
        super.startComponent();

        // Load the functionality menu.
        refreshFunctionalityMenu(targetObjectId, targetObjectIid);
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    public void setDataObject(String objectId, String objectIid)
    {
        this.targetObjectId = objectId;
        this.targetObjectIid = objectIid;
        refreshFunctionalityMenu(targetObjectId, targetObjectIid);
    }

    private void refreshFunctionalityMenu(String objectId, String objectIid)
    {
        try
        {
            lock.lock();
            if (loader == null)
            {
                loader = new MenuLoader(new LoadTask(objectId, objectIid));
                loader.execute();
            }
            else
            {
                loader.setNextTask(new LoadTask(objectId, objectIid));
            }

        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing functionality group: " + functionalityGroupId, e);
        }
        finally
        {
            lock.unlock();
        }
    }

    private void clearFunctionalityMenus()
    {
        for (JMenu groupMenu : functionalityMenus)
        {
            this.remove(groupMenu);
        }

        functionalityMenus.clear();
    }

    private void addFunctionalityGroup(JMenu parentMenu, T8FunctionalityGroupHandle functionalityGroup)
    {
        if (functionalityGroup != null)
        {
            JPopupMenu popupMenu;
            JMenu groupMenu;

            // Create the new group menu.
            groupMenu = new JMenu(functionalityGroup.getDisplayName());

            // Set the popup menu border.
            popupMenu = groupMenu.getPopupMenu();
            popupMenu.setOpaque(false);
            popupMenu.setBorder(new DropShadowBorder());
            groupMenu.setIcon(functionalityGroup.getIcon());

            // Set the popup ui.
            popupMenu.setUI(new T8PopupMenuUI(menuBackgroundPainter));

            // Add the new group to the list of functionality menus.
            functionalityMenus.add(groupMenu);

            // Add the new group menu to the UI.
            if (parentMenu == null)
            {
                this.add(groupMenu);
            }
            else
            {
                parentMenu.add(groupMenu);
            }

            // Add all of the sub-groups in the group to a collection.
            for (T8FunctionalityGroupHandle subGroup : functionalityGroup.getSubGroups())
            {
                if (subGroup.containsAvailableFunctionality())
                {
                    addFunctionalityGroup(groupMenu, subGroup);
                }
            }

            // Add all of the functionalities in the group to a collection.
            for (T8FunctionalityHandle functionality : functionalityGroup.getFunctionalities())
            {
                addFunctionality(groupMenu, functionality);
            }
        }
    }

    private void addFunctionality(JMenu parentMenu, T8FunctionalityHandle functionality)
    {
        if (functionality != null)
        {
            JMenuItem newMenuItem;

            // Add the functionality definition to the collection.
            functionalities.put(functionality.getId(), functionality);

            // Create the new menu item.
            newMenuItem = new MenuItem(functionality.getDisplayName(), functionality.getId(), functionality);
            newMenuItem.setToolTipText(functionality.getDescription());
            newMenuItem.setIcon(functionality.getIcon());
            newMenuItem.setEnabled(functionality.isEnabled());
            newMenuItem.setToolTipText(StringUtilities.createToolTip(100, functionality.getDisabledMessage()));

            // Add the action listener on the menu item.
            newMenuItem.addActionListener(actionListener);

            // Add the new menu item to the parent menu.
            parentMenu.add(newMenuItem);
        }
    }

    private void setAllMenuItemsEnabled(boolean enabled)
    {
        for (JMenu groupMenu : functionalityMenus)
        {
            setMenuDescendantsEnabled(groupMenu, enabled);
        }
    }

    private void setMenuDescendantsEnabled(JMenu groupMenu, boolean enabled)
    {
        for (int itemIndex = 0; itemIndex < groupMenu.getItemCount(); itemIndex++)
        {
            JMenuItem menuItem;

            menuItem = groupMenu.getItem(itemIndex);
            menuItem.setEnabled(enabled);
            if (menuItem instanceof JMenu)
            {
                setMenuDescendantsEnabled((JMenu)menuItem, enabled);
            }
        }
    }

    private void accessFunctionality(final T8FunctionalityHandle functionality) throws Exception
    {
        final T8FunctionalityController functionalityController;

        functionalityController = findFunctionalityContext();
        if (functionalityController != null)
        {
            String confirmationMessage;

            // Get the functionality to execute.
            confirmationMessage = functionality.getConfirmationMessage();
            if (!Strings.isNullOrEmpty(confirmationMessage))
            {
                if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), confirmationMessage, translate("Confirmation"), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                {
                    // No confirmation.
                    return;
                }
            }

            // Disable all functionality menu items while access is requested.
            setAllMenuItemsEnabled(false);

            // Invoke the functionality in a new thread (we don't want to block the EDT).
            new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        T8FunctionalityAccessHandle accessHandle;
                        String functionalityId;

                        // Access the functionality.
                        functionalityId = functionality.getId();
                        accessHandle = functionalityController.accessFunctionality(functionalityId, functionality.getPublicParameters());
                        if (accessHandle.getAccessType() == T8FunctionalityAccessType.ACCESS)
                        {
                            Map<String, Object> eventParameters;

                            // Notify listeners that the functionality has been accessed.
                            eventParameters = new HashMap<>();
                            eventParameters.put(T8FunctionalityMenuBarAPIHandler.PARAMETER_FUNCTIONALITY_IDENTIFIER, functionalityId);
                            getController().reportEvent(T8FunctionalityMenuBar.this, definition.getComponentEventDefinition(T8FunctionalityMenuBarAPIHandler.EVENT_FUNCTIONALITY_ACCESSED), eventParameters);
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(clientContext.getParentWindow(), StringUtilities.wrapText(100, accessHandle.getAccessMessage()), translate("Access Denied"), JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while invoking functionality: " + functionality, e);
                        JOptionPane.showMessageDialog(clientContext.getParentWindow(), StringUtilities.wrapText(100, "An unexpected error prevented access to the requested functionality."), translate("Access Denied"), JOptionPane.ERROR_MESSAGE);
                    }
                    finally
                    {
                        // Refresh the functionality menu bar to reflect changes.
                        refreshFunctionalityMenu(targetObjectId, targetObjectIid);
                    }
                }
            }.start();
        }
        else throw new Exception("No Functionality context found for execution of functionality: " + functionality);
    }

    private T8FunctionalityController findFunctionalityContext()
    {
        String contextIdentifier;

        contextIdentifier = definition.getFunctionalityViewIdentifier();
        if (contextIdentifier != null)
        {
            T8FunctionalityController functionalityContext;

            functionalityContext = (T8FunctionalityController)this.getController().findComponent(contextIdentifier);
            if (functionalityContext != null)
            {
                return functionalityContext;
            }
            else throw new RuntimeException("Functionality context not found: " + definition.getFunctionalityViewIdentifier());
        }
        else
        {
            // Return the default functionality controller provided by the component controller responsible for this component.
            return this.getController().getFunctionalityController();
        }
    }

    private class FunctionalityActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            HashMap<String, Object> eventParameters;
            T8FunctionalityHandle handle;
            String functionalityId;
            MenuItem menuItem;

            menuItem = (MenuItem)event.getSource();
            handle = (T8FunctionalityHandle)menuItem.getValue();
            functionalityId = handle.getId();

            try
            {
                accessFunctionality(handle);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while invoking functionality: " + functionalityId, e);
            }

            // Notify the event handlers.
            eventParameters = new HashMap<>();
            eventParameters.put(T8FunctionalityMenuBarAPIHandler.PARAMETER_FUNCTIONALITY_IDENTIFIER, functionalityId);
            getController().reportEvent(T8FunctionalityMenuBar.this, definition.getComponentEventDefinition(T8FunctionalityMenuBarAPIHandler.EVENT_MENU_ITEM_SELECTED), eventParameters);
        }
    }

    private static class LoadTask
    {
        private final String objectId;
        private final String objectIid;

        private LoadTask(String objectId, String objectIid)
        {
            this.objectId = objectId;
            this.objectIid = objectIid;
        }

        public String getObjectId()
        {
            return objectId;
        }

        public String getObjectIid()
        {
            return objectIid;
        }
    }

    private class MenuLoader extends SwingWorker<Void, Void>
    {
        private LoadTask nextTask;

        private MenuLoader(LoadTask task)
        {
            this.nextTask = task;
        }

        public void setNextTask(LoadTask task)
        {
            this.nextTask = task;
        }

        @Override
        protected Void doInBackground() throws Exception
        {
            LoadTask task;

            while (true)
            {
                // Acquire the next load task.
                try
                {
                    lock.lock();
                    if (nextTask != null)
                    {
                        task = nextTask;
                        nextTask = null;
                    }
                    else
                    {
                        loader = null;
                        break;
                    }
                }
                finally
                {
                    lock.unlock();
                }


                // Add all of the sub-groups in the maingroup to the menu bar.
                mainFunctionalityGroup = functionalityManager.getFunctionalityGroupHandle(context, functionalityGroupId, task.getObjectId(), task.getObjectIid());

                // Determine whether the loop should continue.
                try
                {
                    lock.lock();
                    if (nextTask == null)
                    {
                        loader = null;
                        break;
                    }
                }
                finally
                {
                    lock.unlock();
                }
            }

            return null;
        }

        @Override
        public void done()
        {
            // Remove existing components from the menu bar.
            clearFunctionalityMenus();

            // Add the functionality sub-groups to the menu.
            if (mainFunctionalityGroup != null)
            {
                for (T8FunctionalityGroupHandle subGroup : mainFunctionalityGroup.getSubGroups())
                {
                    if (subGroup.containsAvailableFunctionality())
                    {
                        addFunctionalityGroup(null, subGroup);
                    }
                }
            }

            // Revalidate the layout.
            revalidate();
            repaint();
        }
    }
}
