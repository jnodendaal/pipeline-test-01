/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.filter.panel.component.date;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.utilities.components.datetimepicker.JXDateTimePicker;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JComponent;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class DatePickerBetweenComponent extends JXPanel implements DatePickerComponent
{
    private Boolean isSelected = false;
    private JXDateTimePicker startDate;
    private JXDateTimePicker endDate;
    private T8ComponentController controller;

    public DatePickerBetweenComponent()
    {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        startDate = new JXDateTimePicker(yesterday.getTime());
        endDate = new JXDateTimePicker(new Date());
    }

    @Override
    public Boolean isSelected()
    {
        return isSelected;
    }

    @Override
    public T8DataFilterCriteria getFilterCriteria(
            String dataEntityFieldIdentifier)
    {
        T8DataFilterCriteria filterCriteria = new T8DataFilterCriteria();
        if(startDate != null && startDate.getDate() != null)
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND,
                                           dataEntityFieldIdentifier,
                                           T8DataFilterCriterion.DataFilterOperator.GREATER_THAN_OR_EQUAL,
                                           new T8Timestamp(startDate.getDate().getTime()));
        }
        if(endDate != null && endDate.getDate() != null)
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND,
                                           dataEntityFieldIdentifier,
                                           T8DataFilterCriterion.DataFilterOperator.LESS_THAN_OR_EQUAL,
                                           new T8Timestamp(endDate.getDate().getTime()));
        }
        return filterCriteria;
    }

    @Override
    public void initialize(T8ComponentController controller)
    {
        this.controller = controller;
        setOpaque(false);
        setLayout(new GridBagLayout());

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        gridBagConstraints.ipadx = 0;
        gridBagConstraints.ipady = 0;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;

        gridBagConstraints.gridx = 0;
        add(new JXLabel(getTranslatedString("Between")), gridBagConstraints);

        JXPanel spacer = new JXPanel();
        spacer.setOpaque(false);
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.gridx = 1;
        add(spacer,gridBagConstraints);

        JXPanel content = new JXPanel();
        content.setLayout(new GridBagLayout());
        content.setOpaque(false);
        content.setMinimumSize(new Dimension(250, 1));
        content.setPreferredSize(new Dimension(250, 1));


        gridBagConstraints.anchor = GridBagConstraints.CENTER;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.gridx = 1;
        gridBagConstraints.insets = new Insets(0,0,0,0);
        content.add(startDate, gridBagConstraints);
        gridBagConstraints.gridx = 3;
        content.add(endDate,gridBagConstraints);

        gridBagConstraints.gridx = 2;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.insets = new Insets(0,2,0,2);
        content.add(new JXLabel(getTranslatedString("and")), gridBagConstraints);

        content.invalidate();

        gridBagConstraints.insets = new Insets(2,0,2,5);
        gridBagConstraints.gridx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.fill = GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.1;
        add(content,gridBagConstraints);
        invalidate();
    }

    @Override
    public JComponent getComponent()
    {
        return this;
    }

    @Override
    public void setSelected(Boolean selected)
    {
        this.isSelected = selected;
    }

    private String getTranslatedString(String text)
    {
        return controller.getClientContext().getConfigurationManager().getUITranslation(controller.getContext(), text);
    }
}
