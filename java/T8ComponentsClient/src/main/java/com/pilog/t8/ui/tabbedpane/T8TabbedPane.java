package com.pilog.t8.ui.tabbedpane;

import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.tabbedpane.T8TabbedPaneAPIHandler;
import com.pilog.t8.definition.ui.tabbedpane.T8TabbedPaneDefinition;
import com.pilog.t8.definition.ui.tabbedpane.T8TabbedPaneTabDefinition;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Component;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author Bouwer du Preez
 */
public class T8TabbedPane extends JTabbedPane implements T8Component
{
    private final LinkedHashMap<String, T8TabbedPaneTab> childComponents;
    private final Set<String> hiddenTabs;
    private final T8TabbedPaneDefinition definition;
    private final T8ComponentController controller;
    private final T8TabbedPaneOperationHandler operationHandler;
    private final TabChangeListener tabListener;

    public T8TabbedPane(T8TabbedPaneDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.hiddenTabs = new HashSet<>();
        this.childComponents = new LinkedHashMap<>();
        this.operationHandler = new T8TabbedPaneOperationHandler(this);
        UIManager.put("TabbedPane.contentOpaque", definition.isOpaque());
        this.setOpaque(definition.isOpaque());
        this.setBorder(null);
        this.tabListener = new TabChangeListener();
        this.setForeground(LAFConstants.CONTENT_HEADER_TEXT_COLOR);
        this.setBorder(new EmptyBorder(0, 0, 0, 0));
        this.setFont(LAFConstants.CONTENT_HEADER_FONT);
        setupComponent();
    }

    private void setupComponent()
    {
        T8LookAndFeelManager lafManager;
        T8TabbedPaneUI tabbedPaneUI;

        // Get the look and feel manager.
        lafManager = controller.getClientContext().getConfigurationManager().getLookAndFeelManager(controller.getContext());

        // Initialize painter for painting the default background of the tabbed pane.
        tabbedPaneUI = new T8TabbedPaneUI();
        tabbedPaneUI.setBackgroundPainter(lafManager.getContentHeaderBackgroundPainter());
        tabbedPaneUI.setBackgroundSelectedPainter(lafManager.getContentHeaderBackgroundSelectedPainter());
        setUI(tabbedPaneUI);
    }

    /**
     * Gets the identifier of the currently selected tab in the tabbed pane.
     *
     * @return The {@code String} tab identifier. {@code null} if there is none
     *      or the selected tab is not part of the standard child components
     */
    String getSelectedTabIdentifier()
    {
        Component selectedComponent;

        selectedComponent = getSelectedComponent();
        for (Map.Entry<String, T8TabbedPaneTab> tabs : this.childComponents.entrySet())
        {
            if (tabs.getValue() == selectedComponent) return tabs.getKey();
        }

        return null;
    }

    public void setSelectedTab(final String tabIdentifier)
    {
        if (childComponents.containsKey(tabIdentifier))
        {
            setSelectedComponent(childComponents.get(tabIdentifier));
        }
    }

    public void setTabTitle(String tabIdentifier, String text)
    {
        if (childComponents.containsKey(tabIdentifier))
        {
            Component tab = childComponents.get(tabIdentifier);
            setTitleAt(indexOfComponent(tab), translate(text));
        }
    }

    @Override
    public Insets getInsets()
    {
        return new Insets(0, 0, 0, 0);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> hm)
    {
        return operationHandler.executeOperation(operationIdentifier, hm);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
        addChangeListener(tabListener);
    }

    @Override
    public void stopComponent()
    {
        removeChangeListener(tabListener);
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        if (childComponent instanceof T8TabbedPaneTab)
        {
            T8TabbedPaneTab tab;
            T8TabbedPaneTabDefinition tabDefinition;

            tab = (T8TabbedPaneTab)childComponent;
            tabDefinition = (T8TabbedPaneTabDefinition)tab.getComponentDefinition();
            childComponents.put(tabDefinition.getIdentifier(), tab);
            addTab(translate(tabDefinition.getTitle()), null, (Component)tab, tabDefinition.getTooltipText());
        }
        else throw new RuntimeException("Only " + T8TabbedPaneTab.class + " components can be added to a tabbed pane.");
    }

    public void setTabEnabled(String tabIdentifier, boolean enabled)
    {
        T8TabbedPaneTab tab;

        tab = childComponents.get(tabIdentifier);
        if (tab != null)
        {
            int index;

            index = indexOfComponent(tab);
            if (index > -1)
            {
                setEnabledAt(index, enabled);
            }
        }
        else throw new RuntimeException("Tab not found: " + tabIdentifier);
    }

    public void setTabVisible(String tabIdentifier, boolean visible)
    {
        T8TabbedPaneTab tab;

        tab = childComponents.get(tabIdentifier);
        if (tab != null)
        {
            if (visible) hiddenTabs.remove(tabIdentifier);
            else hiddenTabs.add(tabIdentifier);

            rebuildTabs();
        }
        else throw new RuntimeException("Tab not found: " + tabIdentifier);
    }

    private void rebuildTabs()
    {
        removeAll();
        for (T8TabbedPaneTab tab : childComponents.values())
        {
            T8TabbedPaneTabDefinition tabDefinition;

            tabDefinition = (T8TabbedPaneTabDefinition)tab.getComponentDefinition();
            if (!hiddenTabs.contains(tabDefinition.getIdentifier()))
            {
                addTab(translate(tabDefinition.getTitle()), null, (Component)tab, tabDefinition.getTooltipText());
            }
        }
    }

    @Override
    public T8Component getChildComponent(String componentIdentifier)
    {
        return childComponents.get(componentIdentifier);
    }

    @Override
    public T8Component removeChildComponent(String componentIdentifier)
    {
        if (childComponents.containsKey(componentIdentifier))
        {
            remove(childComponents.get(componentIdentifier));
            return childComponents.remove(componentIdentifier);
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(childComponents.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
        childComponents.clear();
    }

    private String translate(String text)
    {
        return controller.translate(text);
    }

    private class TabChangeListener implements ChangeListener
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            final Map<String, Object> eventParameters;
            T8TabbedPaneTab selectedTab;
            String tabIdentifer;

            selectedTab = (T8TabbedPaneTab)getSelectedComponent();
            tabIdentifer = selectedTab != null ? selectedTab.getComponentDefinition().getIdentifier() : null;

            eventParameters = new HashMap<>();
            eventParameters.put(T8TabbedPaneAPIHandler.PARAMETER_TAB_IDENTIFIER, tabIdentifer);
            controller.reportEvent(T8TabbedPane.this, definition.getComponentEventDefinition(T8TabbedPaneAPIHandler.EVENT_TAB_CHANGED), eventParameters);
        }
    }
}
