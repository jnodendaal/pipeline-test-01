package com.pilog.t8.ui.datasearch.combobox;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LinearGradientPaint;
import java.awt.MouseInfo;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Hennie Brink
 */
public class ComboBoxModelRenderer extends JComponent implements ListCellRenderer
{
    private final Icon checkBoxIcon;
    private final Font headerFont;
    private final Font normalFont;

    private Painter focusedPainter;

    private String displayText;
    private boolean isChecked;
    private boolean isHeader;
    private boolean isPagingNode;
    private boolean paintCheckMark;

    public ComboBoxModelRenderer()
    {
        headerFont = new Font("Tahoma", Font.BOLD, 12);
        normalFont = new Font("Tahoma", Font.PLAIN, 11);
        checkBoxIcon = new ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/tick-small.png"));
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected,
                                                  boolean cellHasFocus)
    {
        ComboBoxListModel model;
        ComboBoxModelNode node;

        model = (ComboBoxListModel) list.getModel();
        node = (ComboBoxModelNode) value;

        displayText = node.getDisplayString();
        isChecked = node.isSelected();
        setToolTipText(node.getDescription());

        isHeader = model.isGroupNode(value);
        paintCheckMark = model.isSingleSelectionMode() ? !isHeader : true;

        isPagingNode = node instanceof CombBoxPagingNode;

        return this;
    }

    public void itemClicked(JList<ComboBoxModelNode> list, ComboBoxModelNode item, Point point)
    {
        ComboBoxListModel model;

        model = (ComboBoxListModel) list.getModel();
        //if(point.x < getCheckBoxBounds(getDrawingBounds()).width)

        model.setNodeSelected(item, !item.isSelected());

    }

    @Override
    protected void paintComponent(Graphics g)
    {
        if (displayText == null)
        {
            return; //If there is no display text then exit, there is no point in painting
        }
        Graphics2D g2;
        Rectangle bounds;
        RenderingHints hints;
        boolean isRollover;

        g2 = (Graphics2D) g.create();
        hints = g2.getRenderingHints();   //Cache the rendering hints so we can restore it when neccesarry
        bounds = getDrawingBounds();


        isRollover = isRollover();
        //draw the focus painter if it is configured and the mouse is over the location
        if (focusedPainter != null && isRollover)
        {
            focusedPainter.paint(g2, this, getWidth(), getHeight());

            g2.setRenderingHints(hints);
        }

        //Set the appropriate font
        if (isHeader)
        {
            g2.setFont(headerFont);
        }

        else
        {
            g2.setFont(normalFont);
        }

        int stringY;
        int stringX;

        stringY = bounds.y;
        stringY += bounds.height / 2;
        stringY += g2.getFontMetrics().getHeight() / 2 / 2;

        stringX = bounds.x;
        stringX += checkBoxIcon.getIconWidth();
        stringX += isHeader ? 5 : 10;

        if(isPagingNode)
        {
            stringX = getWidth() / 2;
            stringX -= g2.getFontMetrics().stringWidth(displayText) / 2;
        }

        //Draw the display String, this can be either a header or normal item
        g2.setColor(Color.BLACK);

        g2.drawString(displayText, stringX, stringY);

        //if this is a header, draw a line to indicate that
        if (isHeader)
        {
            g2.setColor(new Color(180, 180, 180));
            g2.drawLine(stringX - 2, bounds.height, bounds.width, bounds.height);
        }

        if(isPagingNode)
        {
            g2.setColor(new Color(100, 100, 100));
            g2.drawLine(bounds.x, bounds.y - 1, bounds.width, bounds.y - 1);
        }

        //Render the checkbox
        if ((isChecked || isRollover) && !isPagingNode && paintCheckMark)
        {
            Rectangle checkBoxBounds;
            BufferedImage image;

            checkBoxBounds = getCheckBoxBounds(bounds);
            image = new BufferedImage(checkBoxBounds.width, checkBoxBounds.height, BufferedImage.TRANSLUCENT);

            checkBoxIcon.paintIcon(this, image.createGraphics(), 0, 0);

            if (!isChecked)
            {
                ColorConvertOp colorConvert;

                colorConvert = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
                colorConvert.filter(image, image);
            }

            g2.drawImage(image, checkBoxBounds.x, checkBoxBounds.y, null);
        }

        g2.dispose();
    }

    @Override
    public void invalidate()
    {
        super.invalidate();

        if (getWidth() > 0 && getHeight() > 0)
        {
            Paint focusPaint;
            int startX;
            int startY;
            int endX;
            int endY;

            startX = 0;
            startY = getHeight() / 2;
            endX = getWidth();
            endY = getHeight() / 2;

            focusPaint = new LinearGradientPaint(startX, startY, endX, endY, new float[]
                                         {
                                             0f, 1f
            }, new Color[]
                                         {
                                             new Color(.80f, .80f, .95f, .7f),
                                             new Color(.80f, .80f, .95f, .7f)
            });

            focusedPainter = new MattePainter(focusPaint);
        }
    }

    @Override
    public Dimension getPreferredSize()
    {
        Dimension prefferedSize;
        FontMetrics fontMetrics;

        if (displayText != null)
        {
            prefferedSize = new Dimension();
            prefferedSize.height = 25;

            fontMetrics = getFontMetrics(isHeader ? headerFont : normalFont);
            prefferedSize.width = 10; //Inset of 5 on each side
            prefferedSize.width += checkBoxIcon.getIconWidth(); // CheckBox size
            prefferedSize.width += fontMetrics.stringWidth(displayText);

            return prefferedSize;
        }
        else
        {
            return new Dimension(0, 0);//We do not want to render anything that has no display text set
        }
    }

    private Rectangle getCheckBoxBounds(Rectangle drawingBounds)
    {
        Rectangle bounds;

        bounds = new Rectangle();

        bounds.x = 1;

        bounds.y = drawingBounds.y;
        bounds.y += drawingBounds.height / 2;
        bounds.y -= checkBoxIcon.getIconHeight() / 2;

        bounds.width = checkBoxIcon.getIconWidth();
        bounds.height = checkBoxIcon.getIconHeight();

        return bounds;
    }

    private Rectangle getDrawingBounds()
    {
        Insets insets;

        if (getBorder() != null)
        {
            insets = getBorder().getBorderInsets(this);
        }
        else
        {
            insets = new Insets(1, 1, 1, 1);
        }

        return new Rectangle(insets.left, insets.top, getWidth() - insets.right, getHeight() - insets.bottom);
    }

    private boolean isRollover()
    {
        Point pointerLocation;

        pointerLocation = MouseInfo.getPointerInfo().getLocation();

        //Check if the pointer is above this component
        SwingUtilities.convertPointFromScreen(pointerLocation, this);

        return new Rectangle(getSize()).contains(pointerLocation);
    }

}
