package com.pilog.t8.ui.passwordfield;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.passwordfield.T8PasswordFieldAPIHandler;
import com.pilog.t8.definition.ui.passwordfield.T8PasswordFieldDefinition;
import com.pilog.t8.utilities.components.border.PulsatingBorder;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPasswordField;

/**
 * @author Bouwer du Preez
 */
public class T8PasswordField extends JPasswordField implements T8Component
{
    private final T8ComponentController controller;
    private final T8PasswordFieldDefinition definition;
    private final T8PasswordFieldOperationHandler operationHandler;

    public T8PasswordField(T8PasswordFieldDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8PasswordFieldOperationHandler(this);
        this.addKeyListener(new PasswordKeyListener());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
        PulsatingBorder.installWithFocusAnimation(this, new Color(0, 0, 200, 200));
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private void fireEnterKeyTypedPressed()
    {
        HashMap<String, Object> eventParameters;

        eventParameters = new HashMap<>();
        controller.reportEvent(this, definition.getComponentEventDefinition(T8PasswordFieldAPIHandler.EVENT_ENTER_KEY_PRESSED), eventParameters);
    }

    private class PasswordKeyListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
            if (e.getKeyCode() == KeyEvent.VK_ENTER)
            {
                fireEnterKeyTypedPressed();
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
        }
    }
}
