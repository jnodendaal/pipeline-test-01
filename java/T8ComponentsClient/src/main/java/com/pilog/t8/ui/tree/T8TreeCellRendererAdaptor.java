package com.pilog.t8.ui.tree;

import com.pilog.t8.T8Log;
import com.pilog.t8.ui.tree.T8TreeNode.NodeType;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeNodeDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8TreeCellRendererAdaptor implements TreeCellRenderer
{
    private final T8TreeCellRenderableComponent renderableComponent;
    private final Map<String, T8TreeCellRendererComponent> rendererComponents;
    private final Map<String, T8TreeCellRendererComponent> groupRendererComponents;
    private final DefaultTreeCellRenderer defaultRenderer;
    private final T8TreeLoadingCellRenderer loadingCellRenderer;
    private final T8TreeDefinition treeDefinition;

    public T8TreeCellRendererAdaptor(T8TreeCellRenderableComponent renderableComponent, T8TreeDefinition treeDefinition)
    {
        this.renderableComponent = renderableComponent;
        this.rendererComponents = new HashMap<>();
        this.groupRendererComponents = new HashMap<>();
        this.defaultRenderer = new DefaultTreeCellRenderer();
        this.loadingCellRenderer = new T8TreeLoadingCellRenderer((JComponent)renderableComponent);
        this.treeDefinition = treeDefinition;
        cacheNodeRenderers(treeDefinition.getRootNodeDefinition());
    }

    private void cacheNodeRenderers(T8TreeNodeDefinition nodeDefinition)
    {
        T8ComponentDefinition rendererDefinition;
        List<T8TreeNodeDefinition> childNodeDefinitions;

        // Get the renderer definition of the node and instantiate a renderer component if available.
        rendererDefinition = nodeDefinition.getRendererComponentDefinition();
        if (rendererDefinition != null)
        {
            try
            {
                T8TreeCellRendererComponent rendererComponent;

                rendererComponent = (T8TreeCellRendererComponent)rendererDefinition.getNewComponentInstance(renderableComponent.getController());
                rendererComponent.initializeComponent(null);
                rendererComponents.put(nodeDefinition.getIdentifier(), rendererComponent);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while instantiating tree cell renderer component: " + rendererDefinition, e);
            }
        }

        // Get the group renderer definition of the node and instantiate a renderer component if available.
        rendererDefinition = nodeDefinition.getGroupRendererComponentDefinition();
        if (rendererDefinition != null)
        {
            try
            {
                T8TreeCellRendererComponent rendererComponent;

                rendererComponent = (T8TreeCellRendererComponent)rendererDefinition.getNewComponentInstance(renderableComponent.getController());
                rendererComponent.initializeComponent(null);
                groupRendererComponents.put(nodeDefinition.getIdentifier(), rendererComponent);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while instantiating tree cell renderer component: " + rendererDefinition, e);
            }
        }

        // Cache the renderers of the descendent nodes.
        childNodeDefinitions = nodeDefinition.getChildNodeDefinitions();
        if (childNodeDefinitions != null)
        {
            for (T8TreeNodeDefinition childNodeDefinition : childNodeDefinitions)
            {
                cacheNodeRenderers(childNodeDefinition);
            }
        }
    }

    public void startAnimation()
    {
        T8Log.log("Starting animation.");
        loadingCellRenderer.startAnimation();
    }

    public void stopAnimation()
    {
        T8Log.log("Stopping animation.");
        loadingCellRenderer.stopAnimation();
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        T8TreeNodeDefinition nodeDefinition;
        T8TreeNode node;

        node = (T8TreeNode)value;
        nodeDefinition = treeDefinition.getNodeDefinition(node.getIdentifier());
        if (nodeDefinition != null)
        {
            String nodeIdentifier;

            // Get the node identifier from its definition.
            nodeIdentifier = nodeDefinition.getIdentifier();

            // Render the node according to its type.
            if (node.getNodeType() == NodeType.GROUP)
            {
                // Find the cached renderer component and use it if available.
                if (groupRendererComponents.containsKey(nodeIdentifier))
                {
                    T8TreeCellRendererComponent rendererComponent;

                    // Set the editor component content and return it.
                    rendererComponent = groupRendererComponents.get(nodeIdentifier);
                    rendererComponent.setRendererNode(renderableComponent, node, selected, hasFocus, true, expanded, leaf, row, node.getChildNodePageNumber(), node.getChildNodePageCount());
                    return (Component)rendererComponent;
                }
                else
                {
                    return defaultRenderer.getTreeCellRendererComponent(tree, nodeDefinition.getGroupDisplayName(), leaf, expanded, leaf, row, hasFocus);
                }
            }
            else
            {
                // Find the cached renderer component and use it if available.
                if (rendererComponents.containsKey(nodeIdentifier))
                {
                    T8TreeCellRendererComponent rendererComponent;

                    // Set the editor component content and return it.
                    rendererComponent = rendererComponents.get(nodeIdentifier);
                    rendererComponent.setRendererNode(renderableComponent, node, selected, hasFocus, true, expanded, leaf, row, node.getChildNodePageNumber(), node.getChildNodePageCount());
                    return (Component)rendererComponent;
                }
                else
                {
                    return defaultRenderer.getTreeCellRendererComponent(tree, node.getDataEntity(), leaf, expanded, leaf, row, hasFocus);
                }
            }
        }
        else if (!Strings.isNullOrEmpty(node.getGroupDisplayName()))
        {
            // The node does not contain a definition, so just render the group display String as its display value.
            return new T8GroupDisplayTreeCellRenderer(node.getGroupDisplayName());
        }
        else
        {
            // This option is used in the event of a loading node, but should not
            //be visible after the tree has fully loaded
            return loadingCellRenderer;
        }
    }
}
