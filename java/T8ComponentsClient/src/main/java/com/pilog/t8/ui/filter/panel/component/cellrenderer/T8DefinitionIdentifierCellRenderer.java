package com.pilog.t8.ui.filter.panel.component.cellrenderer;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.cellrenderer.T8DefinitionIdentifierCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItem;
import com.pilog.t8.ui.filter.panel.component.list.DefaultFilterPanelListComponentItemCellRenderer;
import com.pilog.t8.utilities.cache.LRUCache;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionIdentifierCellRenderer extends DefaultFilterPanelListComponentItemCellRenderer implements ListCellRenderer<T8FilterPanelListComponentItem>, T8Component
{
    private final T8DefinitionIdentifierCellRendererDefinition definition;
    private final T8DefinitionManager definitionManager;
    private final T8ComponentController controller;
    private final T8Context context;
    private ExpressionEvaluator namespaceExpressionEvaluator;
    private ExpressionEvaluator valueExpressionEvaluator;
    private LRUCache<String, T8Definition> definitionCache;

    private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

    public T8DefinitionIdentifierCellRenderer(T8DefinitionIdentifierCellRendererDefinition definition, T8ComponentController controller)
    {
        super();
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.definitionManager = controller.getClientContext().getDefinitionManager();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String string, Map<String, Object> map)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> map)
    {
        String valueExpression;
        String namespaceExpression;
        List<String> cacheDefinitionIdentifiers;
        List<String> cacheDefinitionGroupIdentifiers;
        int cacheSize;

        // Get the cache size and create it.
        cacheSize = definition.getDefinitionCacheSize();
        definitionCache = new LRUCache<>(cacheSize);

        // If a cache group is specified, cache it.
        cacheDefinitionIdentifiers = definition.getCacheDefinitionIdentifiers();
        if ((cacheDefinitionIdentifiers != null) && (cacheDefinitionIdentifiers.size() > 0))
        {
            cacheDefinitions(cacheDefinitionIdentifiers);
        }

        // If a cache group is specified, cache it.
        cacheDefinitionGroupIdentifiers = definition.getCacheDefinitionGroupIdentifiers();
        if ((cacheDefinitionGroupIdentifiers != null) && (cacheDefinitionGroupIdentifiers.size() > 0))
        {
            cacheDefinitionGroups(cacheDefinitionGroupIdentifiers);
        }

        // Get the namespace expression and compile it.
        namespaceExpression = definition.getNamespaceExpression();
        if (namespaceExpression != null)
        {
            try
            {
                namespaceExpressionEvaluator = new ExpressionEvaluator();
                namespaceExpressionEvaluator.compileExpression(namespaceExpression);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while compiling display namespace expression in renderer: " + definition, e);
                namespaceExpressionEvaluator = null;
            }
        }

        // Get the value expression and compile it.
        valueExpression = definition.getDisplayValueExpression();
        if (valueExpression != null)
        {
            try
            {
                valueExpressionEvaluator = new ExpressionEvaluator();
                valueExpressionEvaluator.compileExpression(valueExpression);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while compiling display value expression in renderer: " + definition, e);
                valueExpressionEvaluator = null;
            }
        }
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public JComponent getListCellRendererComponent(JList list, T8FilterPanelListComponentItem value, int index, boolean isSelected, boolean cellHasFocus)
    {
        Map<String, Object> expressionParameters;
        String namespace;

        // Get the namespace of the value in the cell.
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (namespaceExpressionEvaluator != null)
        {
            try
            {
                Object result;

                result = namespaceExpressionEvaluator.evaluateExpression(HashMaps.newHashMap("IDENTIFIER",value.getValue()), null);
                namespace = result != null ? result.toString() : null;
            }
            catch (Exception e)
            {
                T8Log.log("Exception while evaluating display value expression in renderer: " + definition, e);
                namespace = null;
            }
        }
        else namespace = null;

        // Get the display value of the cell.
        try
        {
            String identifier;

            identifier = value == null || value.getValue() == null ? null : value.getValue().toString();
            if (identifier != null)
            {
                T8Definition rendererDefinition;

                rendererDefinition = loadDefinition(namespace != null ? (namespace + identifier) : identifier);
                if (rendererDefinition != null)
                {
                    Object result;

                    expressionParameters = rendererDefinition.getDefinitionDatums();
                    expressionParameters.put("IDENTIFIER", rendererDefinition.getIdentifier());
                    expressionParameters.put("DISPLAY_NAME", rendererDefinition.getMetaDisplayName());
                    result = valueExpressionEvaluator.evaluateExpression(expressionParameters, null);
                    value.setDisplayName(result != null ? result.toString() : null);
                }
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while evaluating display value expression in renderer: " + definition, e);
        }

        // Set the value of the cell on the label.
        label.setText(value == null ? null : value.getDisplayName());
        return this;
    }

    private T8Definition loadDefinition(String identifier)
    {
        String globalIdentifier;
        String localIdentifier;
        T8Definition requestedDefinition;

        // Get the global definition we are looking for.
        globalIdentifier = T8IdentifierUtilities.getGlobalIdentifierPart(identifier);
        requestedDefinition = definitionCache.get(globalIdentifier);
        if (requestedDefinition == null)
        {
            try
            {
                requestedDefinition = definitionManager.getInitializedDefinition(context, definition.getRootProjectId(), globalIdentifier, null);
                definitionCache.put(globalIdentifier, requestedDefinition);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while trying to cache definition: " + globalIdentifier, e);
                return null;
            }
        }

        // If the definition we are looking for is a local one, fetch it.
        localIdentifier = T8IdentifierUtilities.getLocalIdentifierPart(identifier);
        if (localIdentifier != null)
        {
            return requestedDefinition.getLocalDefinition(localIdentifier);
        }
        else return requestedDefinition;
    }

    private void cacheDefinitions(List<String> identifiers)
    {
        for (String identifier : identifiers)
        {
            try
            {
                T8Definition definition;

                definition = definitionManager.getInitializedDefinition(context, this.definition.getRootProjectId(), identifier, null);
                if (definition != null)
                {
                    definitionCache.put(definition.getIdentifier(), definition);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while caching definition group: " + identifier, e);
            }
        }
    }

    private void cacheDefinitionGroups(List<String> groupIds)
    {
        for (String groupId : groupIds)
        {
            try
            {
                List<T8Definition> definitions;

                definitions = definitionManager.getInitializedGroupDefinitions(context, definition.getRootProjectId(), groupId, null);
                if (definitions != null)
                {
                    for (T8Definition retrievedDefinition : definitions)
                    {
                        definitionCache.put(retrievedDefinition.getIdentifier(), retrievedDefinition);
                    }
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while caching definition group: " + groupId, e);
            }
        }
    }
}
