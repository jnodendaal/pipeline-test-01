package com.pilog.t8.ui.label;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.font.T8FontDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.label.T8LabelDefinition;
import com.pilog.t8.definition.ui.label.T8LabelDefinition.HorizontalTextAlignment;
import com.pilog.t8.definition.ui.label.T8LabelDefinition.VerticalTextAlignment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;

/**
 * @author Bouwer du Preez
 */
public class T8Label extends JLabel implements T8Component
{
    private final T8LabelOperationHandler operationHandler;
    private final T8ComponentController controller;
    private final T8LabelDefinition definition;

    public T8Label(T8LabelDefinition labelDefinition, T8ComponentController controller)
    {
        this.definition = labelDefinition;
        this.controller = controller;
        this.operationHandler = new T8LabelOperationHandler(this);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> hm1)
    {
        T8FontDefinition fontDefinition;

        // Set the text font if one is specified.
        fontDefinition = definition.getFontDefinition();
        if (fontDefinition != null) this.setFont(fontDefinition.getNewFontInstance());

        // Set the foreground color.
        this.setForeground(definition.getForegroundColor());

        // Set the horizontal text alignment.
        if (definition.getHorizontalTextAlignment() == HorizontalTextAlignment.CENTER)
        {
            this.setHorizontalAlignment(JLabel.CENTER);
        }
        else if (definition.getHorizontalTextAlignment() == HorizontalTextAlignment.RIGHT)
        {
            this.setHorizontalAlignment(JLabel.RIGHT);
        }
        else this.setHorizontalAlignment(JLabel.LEFT);

        // Set the vertical text alignment.
        if (definition.getVerticalTextAlignment() == VerticalTextAlignment.CENTER)
        {
            this.setVerticalAlignment(JLabel.CENTER);
        }
        else if (definition.getVerticalTextAlignment() == VerticalTextAlignment.TOP)
        {
            this.setVerticalAlignment(JLabel.TOP);
        }
        else this.setVerticalAlignment(JLabel.BOTTOM);

        // Set the label text.
        this.setText(definition.getText());
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }
}
