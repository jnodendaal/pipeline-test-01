package com.pilog.t8.ui.wizard;

import static com.pilog.t8.definition.ui.wizard.T8WizardAPIHandler.*;

import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8WizardOperationHandler
{
    private final T8Wizard wizard;

    public T8WizardOperationHandler(T8Wizard wizard)
    {
        this.wizard = wizard;
    }

    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        switch (operationIdentifier)
        {
            case OPERATION_MOVE_NEXT:
                wizard.goToNextStep();
                break;
            case OPERATION_MOVE_PREVIOUS:
                wizard.goToPreviousStep();
                break;
            case OPERATION_RESET:
                wizard.reset();
                break;
            case OPERATION_SET_PARAMETERS:
                this.wizard.setParameters((Map<String, Object>)operationParameters.get(PARAMETER_WIZARD_PARAMETERS));
                break;
            case OPERATION_SET_STEP_NAME:
//                String stepName = (String)operationParameters.get(T8WizardAPIHandler.PARAMETER_STEP_NAME);
//                String stepIdentifier = (String)operationParameters.get(T8WizardAPIHandler.PARAMETER_STEP_IDENTIFIER);

                //wizard.setStepName(stepIdentifier, stepName);
                break;
            case OPERATION_VALIDATE_CURRENT_STEP:
                wizard.finalizeCurrentStep();
                break;
            case OPERATION_SET_CANCEL_VISIBILITY:
                this.wizard.setCancelVisible(Boolean.TRUE.equals(operationParameters.get(PARAMETER_VISIBLE)));
                break;
            case OPERATION_SET_FINISH_VISIBILITY:
                this.wizard.setFinishVisible(Boolean.TRUE.equals(operationParameters.get(PARAMETER_VISIBLE)));
                break;
            case OPERATION_SET_PREVIOUS_VISIBILITY:
                this.wizard.setPreviousVisible(Boolean.TRUE.equals(operationParameters.get(PARAMETER_VISIBLE)));
                break;
            case OPERATION_SET_NEXT_VISIBILITY:
                this.wizard.setNextVisible(Boolean.TRUE.equals(operationParameters.get(PARAMETER_VISIBLE)));
                break;
            case OPERATION_SET_NAVIGATION_PANEL_VISIBILITY:
                this.wizard.setNavigationPanelVisible(Boolean.TRUE.equals(operationParameters.get(PARAMETER_VISIBLE)));
                break;
        }

        return null;
    }
}
