package com.pilog.t8.ui.dataentitylist;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.ui.dataentitylist.T8DataEntityListDefinition;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityListCellRenderer implements ListCellRenderer
{
    private T8DataEntityListDefinition definition;
    private DefaultListCellRenderer defaultRenderer;
    
    public T8DataEntityListCellRenderer(T8DataEntityListDefinition definition)
    {
        this.definition = definition;
        this.defaultRenderer = new DefaultListCellRenderer();
    }
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        if (value != null)
        {
            T8DataEntity entity;
            Object displayValue;
            JComponent rendererComponent;
            
            entity = (T8DataEntity)value;
            displayValue = entity.getFieldValue(definition.getDisplayFieldIdentifier());
            rendererComponent = (JComponent)defaultRenderer.getListCellRendererComponent(list, displayValue, index, isSelected, cellHasFocus);
            rendererComponent.setOpaque(isSelected);
            return rendererComponent;
        }
        else
        {
            JComponent rendererComponent;
            
            rendererComponent = (JComponent)defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            rendererComponent.setOpaque(isSelected);
            return rendererComponent;
        }
    }
}
