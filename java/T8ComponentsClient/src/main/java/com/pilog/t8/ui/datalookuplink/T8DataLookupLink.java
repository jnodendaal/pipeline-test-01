package com.pilog.t8.ui.datalookuplink;

import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogAPIHandler;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogDefinition;
import com.pilog.t8.definition.ui.datalookupfield.T8DataLookupFieldAPIHandler;
import com.pilog.t8.definition.ui.datalookuplink.T8DataLookupLinkDefinition;
import com.pilog.t8.definition.ui.dialog.T8DialogAPIHandler;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import com.pilog.t8.ui.datalookupdialog.T8DataLookupDialog;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import org.jdesktop.swingx.JXHyperlink;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupLink extends JPanel implements T8Component
{
    private final T8ComponentController parentController;
    private final T8ComponentController controller;
    private final T8DataLookupLinkDefinition definition;
    private final JXHyperlink lookupLink;
    private T8DataLookupDialog dialog;
    private T8DataEntity selectedEntity;
    private final T8DataLookupLinkOperationHandler operationHandler;
    private T8DataEntityDefinition entityDefinition;
    private final Map<String, T8DataFilter> prefilters;
    private MouseListener mouseListener;
    private String nullDisplayString;

    private EntityRetriever entityRetriever;

    public T8DataLookupLink(T8DataLookupLinkDefinition definition, T8ComponentController controller)
    {
        this.parentController = controller;
        this.controller = new T8DefaultComponentController(controller, new T8ProjectContext(controller.getContext(), definition.getRootProjectId()), definition.getIdentifier(), false);
        this.controller.addComponentEventListener(new DialogEventListener());
        this.definition = definition;
        this.operationHandler = new T8DataLookupLinkOperationHandler(this);
        this.prefilters = new HashMap<>();
        this.nullDisplayString = definition.getNullDisplayString();
        initComponents();

        // Create the Lookup Hyperlink.
        if (Strings.isNullOrEmpty(nullDisplayString)) nullDisplayString = "No Value";
        this.lookupLink = new JXHyperlink(new AbstractAction(nullDisplayString)
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                lookup();
            }
        });
        add(lookupLink);

        // Add a mouse listener to listen for right clicks.
        lookupLink.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                if (e.getButton() != MouseEvent.BUTTON1)
                {
                    clear();
                }
            }
        });
    }

    @Override
    public T8ComponentController getController()
    {
        return parentController;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        try
        {
            T8DataLookupDialogDefinition dialogDefinition;

            dialogDefinition = definition.getLookupDialogDefinition();
            dialog = (T8DataLookupDialog)dialogDefinition.getNewComponentInstance(controller);
            dialog.initializeComponent(null);

            for (Map.Entry<String, T8DataFilter> entry : prefilters.entrySet())
            {
                dialog.setPrefilter(entry.getKey(), entry.getValue());
            }

            setToolTipText(definition.getTooltipText());
            lookupLink.setText(nullDisplayString);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while initializing data lookup link: " + definition, e);
        }
    }

    @Override
    public void startComponent()
    {
        if (dialog != null) dialog.startComponent();
    }

    @Override
    public void stopComponent()
    {
        if (entityRetriever != null && !entityRetriever.isDone()) entityRetriever.cancel(false);
        if (dialog != null) dialog.stopComponent();
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public void setPrefilter(String identifier, T8DataFilter prefilter)
    {
        prefilters.put(identifier, prefilter);
        if (dialog != null)
        {
            dialog.setPrefilter(identifier, prefilter);
        }
    }

    private void lookup()
    {
        // Show the dialog.
        dialog.executeOperation(T8DialogAPIHandler.OPERATION_OPEN_DIALOG, new HashMap<>());
    }

    public void clear()
    {
        setSelectedDataEntity(null);
    }

    public void setSelectedDataEntityKey(Map<String, Object> key)
    {
        if ((key != null) && (key.size() > 0))
        {
            // Make sure the entity definition has been retrieved.
            if (entityDefinition == null)
            {
                try
                {
                    entityDefinition = (T8DataEntityDefinition)parentController.getClientContext().getDefinitionManager().getInitializedDefinition(parentController.getContext(), definition.getRootProjectId(), definition.getDataEntityIdentifier(), null);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while loading entity definition: " + definition.getDataEntityIdentifier(), e);
                }
            }

            // Now retrieve the entity using the key values supplied in conjunction with any prefilters that are available.
            if(entityRetriever != null && !entityRetriever.isDone()) entityRetriever.cancel(false);
            entityRetriever = new EntityRetriever(createCombinedFilter(key));
            entityRetriever.execute();
        }
        else setSelectedDataEntity(null);
    }

    public void setSelectedDataEntity(T8DataEntity entity)
    {
        HashMap<String, Object> eventParameters;

        // Set the selected entity and display value.
        selectedEntity = entity;
        if (selectedEntity != null)
        {
            Object displayValue;

            displayValue = selectedEntity.getFieldValue(definition.getDisplayFieldIdentifier());
            if (displayValue == null)
            {
                lookupLink.setText(null);
            }
            else
            {
                lookupLink.setText(displayValue + "");
            }
        }
        else
        {
            lookupLink.setText(nullDisplayString);
        }

        // Report the selection changed event.
        eventParameters = new HashMap<>();
        eventParameters.put(T8DataLookupFieldAPIHandler.PARAMETER_DATA_ENTITY, entity);
        parentController.reportEvent(T8DataLookupLink.this, definition.getComponentEventDefinition(T8DataLookupFieldAPIHandler.EVENT_SELECTION_CHANGED), eventParameters);
    }

    /**
     * Creates and returns a data filter that includes all of the prefilters set
     * along with supplied key filter (if any).
     *
     * @return The combined filter to use when retrieving data to display in the
     * table.
     */
    private T8DataFilter createCombinedFilter(Map<String, Object> keyFilter)
    {
        T8DataFilterCriteria combinedFilter;

        // Create a filter to contain a combination of all applicable filters for data retrieval.
        combinedFilter = new T8DataFilterCriteria();

        // Add all available and valid prefilters.
        for (T8DataFilter prefilter : prefilters.values())
        {
            // Only include filters that have valid filter criteria.
            if ((prefilter != null) && (prefilter.hasFilterCriteria()))
            {
                combinedFilter.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
            }
        }

        // Add the regular user-defined filter if applicable.
        if ((keyFilter != null) && (keyFilter.size() > 0)) combinedFilter.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, new T8DataFilterCriteria(keyFilter));

        // Return a new filter created from the combined criteria.
        return new T8DataFilter(definition.getDataEntityIdentifier(), combinedFilter);
    }

    public T8DataEntity getSelectedDataEntity()
    {
        while(entityRetriever != null && !entityRetriever.isDone())
        {
            synchronized(entityRetriever.getLock())
            {
                try
                {
                    entityRetriever.getLock().wait(1000);
                }
                catch (InterruptedException ex)
                {
                    //Do nothing, just wait again
                }
            }
        }
        return selectedEntity;
    }

    public Object getSelectedDataValue()
    {
        String valueFieldIdentifier;

        valueFieldIdentifier = definition.getValueFieldIdentifier();
        if (valueFieldIdentifier != null)
        {
            return getSelectedDataEntity() != null ? getSelectedDataEntity().getFieldValue(valueFieldIdentifier) : null;
        }
        else throw new RuntimeException("No value field identifier set for definition: " + definition);
    }

    public void refreshData()
    {
        if(dialog != null) dialog.refreshData();
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        lookupLink.setEnabled(enabled);
    }

    public void setEditable(boolean editable)
    {
        lookupLink.setEnabled(editable);
    }

    public boolean isEntityRetrieved()
    {
        return entityRetriever == null ? true : entityRetriever.isDone();
    }

    private class EntityRetriever extends SwingWorker<T8DataEntity, Void>
    {
        private final T8DataFilter retrievalFilter;
        private final Object lock;

        private EntityRetriever(T8DataFilter filter)
        {
            this.lock = new Object();
            retrievalFilter = filter;
        }

        @Override
        protected T8DataEntity doInBackground() throws Exception
        {
            final List<T8DataEntity> retrievedEntities;

            // Retrieve the selected entity.
            retrievedEntities = T8DataManagerOperationHandler.selectDataEntities(parentController.getContext(), entityDefinition.getIdentifier(), retrievalFilter);
            return retrievedEntities.size() > 0 ? retrievedEntities.get(0) : null;
        }

        @Override
        protected void done()
        {
            synchronized(lock)
            {
                try
                {
                    // Set the selected entity.
                    setSelectedDataEntity(get());
                }
                catch(CancellationException c)
                {
                    //Just ignore it if this task was cancelled
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while retrieving entity data: " + entityDefinition, e);
                }


                lock.notifyAll();
            }
        }

        public Object getLock()
        {
            return lock;
        }
    }

    private class DialogEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;
            T8Component component;
            Map<String, Object> eventParameters;

            eventDefinition = event.getEventDefinition();
            component = event.getComponent();
            eventParameters = event.getEventParameters();

            T8Log.log("Data Lookup Dialog Event: " + eventDefinition + " on component: " + component.getComponentDefinition() + " with parameters: " + eventParameters);
            if ((component instanceof T8DataLookupDialog) && (eventDefinition.getIdentifier().equals(T8DataLookupDialogAPIHandler.EVENT_SELECTION_CHANGED)))
            {
                T8DataEntity eventSelectedEntity;

                eventSelectedEntity = (T8DataEntity)eventParameters.get(T8DataLookupDialogAPIHandler.PARAMETER_DATA_ENTITY);
                setSelectedDataEntity(eventSelectedEntity);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setOpaque(false);
        setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
