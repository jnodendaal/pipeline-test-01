package com.pilog.t8.ui.functionality.button;

import com.pilog.t8.definition.ui.functionality.menubar.T8FunctionalityMenuBarAPIHandler;
import com.pilog.t8.ui.button.T8ButtonOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityButtonOperationHandler extends T8ButtonOperationHandler
{
    private final T8FunctionalityButton button;

    public T8FunctionalityButtonOperationHandler(T8FunctionalityButton button)
    {
        super(button);
        this.button = button;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8FunctionalityMenuBarAPIHandler.OPERATION_SET_DATA_OBJECT))
        {
            String objectIid;
            String objectId;

            objectIid = (String)operationParameters.get(T8FunctionalityMenuBarAPIHandler.PARAMETER_DATA_OBJECT_IID);
            objectId = (String)operationParameters.get(T8FunctionalityMenuBarAPIHandler.PARAMETER_DATA_OBJECT_ID);
            button.setDataObject(objectId, objectIid);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
