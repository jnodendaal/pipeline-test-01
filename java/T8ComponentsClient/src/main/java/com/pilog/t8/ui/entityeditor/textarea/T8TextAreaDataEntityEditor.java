package com.pilog.t8.ui.entityeditor.textarea;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.ui.entityeditor.textarea.T8TextAreaDataEntityEditorDefinition;
import com.pilog.t8.ui.textarea.T8TextArea;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextAreaDataEntityEditor extends T8TextArea implements T8DataEntityEditorComponent
{
    private T8TextAreaDataEntityEditorDefinition definition;
    private T8TextAreaDataEntityEditorOperationHandler operationHandler;
    private T8DataEntity editorEntity;
    
    public T8TextAreaDataEntityEditor(T8TextAreaDataEntityEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.operationHandler = new T8TextAreaDataEntityEditorOperationHandler(this);
    }
    
    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }
    
    @Override
    public void setEditorDataEntity(T8DataEntity dataEntity)
    {
        editorEntity = dataEntity;
        if (editorEntity != null)
        {
            String fieldIdentifier;
            
            fieldIdentifier = definition.getEditorDataEntityFieldIdentifier();
            if (fieldIdentifier != null)
            {
                Object value;
                
                value = editorEntity.getFieldValue(fieldIdentifier);
                if (value != null)
                {
                    setText(value.toString());
                }
                else setText(null);
            }
            else setText(null);
        }
        else setText(null);
    }
    
    @Override
    public T8DataEntity getEditorDataEntity()
    {
        return editorEntity;
    }

    @Override
    public boolean commitEditorChanges()
    {
        if (editorEntity != null)
        {
            String fieldIdentifier;

            fieldIdentifier = definition.getEditorDataEntityFieldIdentifier();
            if (fieldIdentifier != null)
            {
                editorEntity.setFieldValue(fieldIdentifier, getText());
                return true;
            }
            else throw new RuntimeException("No field identifier set in entity editor: " + definition);
        }
        else return true;
    }
    
    @Override
    public String getEditorDataEntityIdentifier()
    {
        return definition.getEditorDataEntityIdentifier();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return definition.getParentEditorIdentifier();
    }
}
