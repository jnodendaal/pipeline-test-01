package com.pilog.t8.ui.datarecord.celleditor;

import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.ui.datarecord.DataRecordEditorFactory;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public interface TableCellPropertyResolver
{
    public boolean isColumnEditable(int columnIndex);
    public DataRecord getDataRecord(int rowIndex);
    public String translate(String inputString);
    public TerminologyProvider getTerminologyProvider();
    public DataRecordEditorFactory getCellEditorComponentFactory();
    public PropertyRequirement getViewCellPropertyRequirement(int rowIndex, int columnIndex);
    public boolean isCellEditable(EventObject e);
}
