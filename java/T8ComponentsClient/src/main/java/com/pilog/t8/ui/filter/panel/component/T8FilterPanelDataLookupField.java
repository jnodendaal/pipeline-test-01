package com.pilog.t8.ui.filter.panel.component;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.filter.panel.component.T8FilterPanelDataLookupFieldDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datalookupfield.T8DataLookupField;
import com.pilog.t8.ui.filter.panel.T8FilterPanelComponent;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelDataLookupField extends T8DataLookupField implements T8FilterPanelComponent
{
    private T8FilterPanelDataLookupFieldDefinition definition;
    private String dataEntityIdentifier;
    private List<ChangeListener> changeListeners;

    public T8FilterPanelDataLookupField(T8FilterPanelDataLookupFieldDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.changeListeners = new ArrayList<>();
    }

    @Override
    public T8DataFilterCriteria getFilterCriteria()
    {
        T8DataFilterCriteria filterCriteria = new T8DataFilterCriteria();
        if (getSelectedDataValue() == null)
        {
            return filterCriteria;
        }
        if (!definition.isTagField())
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, definition.getDataEntityKeyFieldIdentifier(), T8DataFilterCriterion.DataFilterOperator.EQUAL, getSelectedDataValue());
        }
        else
        {
            if (getSelectedDataValue().toString() != null)
            {
                filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.OR,
                                               definition.getDataEntityKeyFieldIdentifier(),
                                               T8DataFilterCriterion.DataFilterOperator.EQUAL,
                                               getSelectedDataValue().toString());
            }

        }
        return filterCriteria;
    }

    @Override
    public void initialize(T8Context context) throws Exception
    {
    }

    @Override
    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        this.dataEntityIdentifier = dataEntityIdentifier;
    }

    @Override
    public Component getComponent()
    {
        return this;
    }

    @Override
    public void addChangeListener(ChangeListener changeListener)
    {
        changeListeners.add(changeListener);
    }

    @Override
    public void removeChangeListener(ChangeListener changeListener)
    {
        changeListeners.remove(changeListener);
    }

    @Override
    public void fireStateChanged()
    {
        for (ChangeListener changeListener : changeListeners)
        {
            changeListener.stateChanged(new ChangeEvent(this));
        }
    }

    @Override
    public void setSelectedDataEntity(T8DataEntity entity)
    {
        super.setSelectedDataEntity(entity); //To change body of generated methods, choose Tools | Templates.

        fireStateChanged();
    }

    @Override
    public void refresh() throws Exception
    {
        //do nothing
    }
}
