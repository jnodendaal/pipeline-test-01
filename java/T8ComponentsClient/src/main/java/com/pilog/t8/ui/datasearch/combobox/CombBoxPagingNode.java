package com.pilog.t8.ui.datasearch.combobox;

/**
 * @author Hennie Brink
 */
public class CombBoxPagingNode implements ComboBoxModelNode
{
    private final String displayString;
    private final String description;

    public CombBoxPagingNode(String displayString, String description)
    {
        this.displayString = displayString;
        this.description = description;
    }

    @Override
    public String getDisplayString()
    {
        return displayString;
    }

    @Override
    public String getDescription()
    {
        return description;
    }

    @Override
    public boolean isSelected()
    {
        return false;
    }

    @Override
    public void setSelected(boolean selected)
    {

    }

}
