package com.pilog.t8.ui.table.object;

import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.script.T8ClientContextScript;
import com.pilog.t8.definition.ui.table.object.T8TableDataObjectProviderScriptDefinition;
import com.pilog.t8.definition.ui.table.object.T8TableScriptObjectProviderDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8Table;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TableScriptDataObjectProvider implements T8TableDataObjectProvider
{
    private final T8TableScriptObjectProviderDefinition definition;
    private final T8Context context;
    private T8Table table;

    public T8TableScriptDataObjectProvider(T8ComponentController controller, T8TableScriptObjectProviderDefinition definition)
    {
        this.context = controller.getContext();
        this.definition = definition;
    }

    @Override
    public void setTable(T8Table table)
    {
        this.table = table;
    }

    @Override
    public boolean providesDataObject(String objectIdentifier)
    {
        return true;
    }

    @Override
    public T8DataObject getDataObject(String objectIdentifier)
    {
        if (providesDataObject(objectIdentifier))
        {
            try
            {
                T8TableDataObjectProviderScriptDefinition scriptDefinition;
                Map<String, Object> inputParameters;
                T8ClientContextScript script;
                Object outputObject;

                // Create the script input parameter map.
                inputParameters = new HashMap<>();
                inputParameters.put(definition.getNamespace() + T8TableDataObjectProviderScriptDefinition.PARAMETER_DATA_OBJECT_IDENTIFIER, objectIdentifier);
                inputParameters.put(definition.getNamespace() + T8TableDataObjectProviderScriptDefinition.PARAMETER_DATA_ENTITY, table.getSelectedDataEntity());

                // Execute the script.
                scriptDefinition = definition.getScriptDefinition();
                script = scriptDefinition.getNewScriptInstance(context);
                outputObject = script.executeScript(inputParameters);

                // Process the script output.
                if (outputObject == null)
                {
                    return null;
                }
                else if (outputObject instanceof Map)
                {
                    Map<String, Object> outputParameters;

                    outputParameters =((Map<String, Object>)outputObject);
                    return (T8DataObject)outputParameters.get(definition.getNamespace() + T8TableDataObjectProviderScriptDefinition.PARAMETER_DATA_OBJECT);
                }
                else throw new RuntimeException("Invalid script output object type: " + outputObject.getClass());
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while generating data object '" + objectIdentifier + "' from script: " + definition, e);
            }
        }
        else return null;
    }
}
