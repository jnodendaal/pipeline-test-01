package com.pilog.t8.ui.entitymenu;

import com.pilog.t8.definition.ui.entitymenu.T8DataEntityMenuItemAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityMenuItemOperationHandler extends T8ComponentOperationHandler
{
    private T8DataEntityMenuItem menuItem;

    public T8DataEntityMenuItemOperationHandler(T8DataEntityMenuItem list)
    {
        super(list);
        this.menuItem = list;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if(T8DataEntityMenuItemAPIHandler.OPERATION_DISABLE_MENU_ITEM.equals(operationIdentifier))
        {
            menuItem.setEnabled(false);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
