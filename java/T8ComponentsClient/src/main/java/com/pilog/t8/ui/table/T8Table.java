package com.pilog.t8.ui.table;

import com.pilog.t8.client.T8ClientScriptRunner;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.groupby.T8GroupByDataFilter;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.table.T8TableAPIHandler;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.definition.ui.table.script.T8TableOnDeleteScriptDefinition;
import com.pilog.t8.definition.ui.table.export.T8TableExporter;
import com.pilog.t8.definition.ui.table.object.T8TableDataObjectProviderDefinition;
import com.pilog.t8.ui.busypanel.T8BusyPanel;
import com.pilog.t8.ui.celleditor.T8TableCellEditorAdaptor;
import com.pilog.t8.ui.cellrenderer.T8TableCellRendererAdaptor;
import com.pilog.t8.ui.optionpane.T8OptionPane;
import com.pilog.t8.ui.table.dialog.T8CopyTableRowsOptions;
import com.pilog.t8.ui.table.dialog.T8TableFillDownOptions;
import com.pilog.t8.ui.table.menu.T8TableContextMenuHandler;
import com.pilog.t8.ui.table.object.T8TableDataObjectProvider;
import com.pilog.t8.ui.table.orderby.T8PresetOrderingPanelPopup;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellEditor;
import com.pilog.t8.data.object.T8ComponentDataObjectProvider;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8Table extends JPanel implements T8Component, T8ComponentDataObjectProvider, T8TableCellEditableComponent, T8TableCellRenderableComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8Table.class);

    private final T8TableDefinition definition;
    private final T8ConfigurationManager configurationManager;
    private final T8ComponentController controller;
    private final T8TableFilterPanel filterPanel;
    private final T8TableDataHandler dataHandler;
    private final T8TableDataChangeHandler changeHandler;
    private final T8TableModelHandler modelHandler;
    private final T8TableViewHandler viewHandler;
    private final T8TableOperationHandler operationHandler;
    private final Map<String, T8DataFilter> prefilters;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8BusyPanel busyPanel;
    private final T8TableContextMenuHandler contextMenuHandler;
    private T8DataFilter userDefinedFilter;
    private Map<String, Object> key; // The values used as filter values and which will also be inserted by default into any newly added entity.
    private LinkedHashMap<String, OrderMethod> fieldOrdering;
    private final List<T8TableDataObjectProvider> objectProviders;
    private TableColumnAdjuster tableColumnAdjuster;
    private DataLoader lastDataLoader;
    private DataCounter lastDataCounter;
    private boolean outdated; // Indicates whether or not the data currently contained by the table can be considered outdated.

    public static final String ROW_HEADER_COLUMN_NAME = "T8_TABLE_ROW_HEADER";
    public static final Border UNSELECTED_BORDER = new EmptyBorder(1, 1, 1, 1);
    public static final Border SELECTED_BORDER = new LineBorder(Color.BLUE, 1);
    public static final Border FOCUS_BORDER = UIManager.getBorder("Table.focusCellHighlightBorder");
    public static final Border FOCUS_SELECTED_BORDER = new CompoundBorder(SELECTED_BORDER, FOCUS_BORDER);

    public T8Table(T8TableDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();

        // Components have to be initialized after the configuration manager is set otherwise NPE will be caused by all translation attemps.
        initComponents();
        this.jTableData.setModel(new T8DefaultTableModel(definition, new ArrayList<>(), 0));
        this.filterPanel = new T8TableFilterPanel(this);
        this.changeHandler = new T8TableDataChangeHandler(definition, definition.getEntityDefinition(), this, jTableData);
        this.dataHandler = new T8TableDataHandler(this, definition);
        this.modelHandler = new T8TableModelHandler(this, definition);
        this.viewHandler = new T8TableViewHandler(definition, this, jScrollPaneTable, jTableData);
        this.operationHandler = new T8TableOperationHandler(this);
        this.busyPanel = new T8BusyPanel("Loading Data...");
        this.jPanelProcessingView.add(busyPanel, java.awt.BorderLayout.CENTER);
        this.prefilters = new HashMap<>();
        this.fieldOrdering = new LinkedHashMap<>();
        this.jLabelRecordCount.setVisible(false);
        this.objectProviders = new ArrayList<>();
        this.outdated = true;

        this.contextMenuHandler = new T8TableContextMenuHandler(this);

        // Configure the row header resizer.
        //new T8TableRowHeaderResizer(jScrollPaneTable).setEnabled(true);
        configureDeveloperShortcuts();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8TableDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        // Initialize all the available prefilters.
        for (T8DataFilterDefinition prefilterDefinition : definition.getPrefilterDefinitions())
        {
            prefilters.put(prefilterDefinition.getIdentifier(), prefilterDefinition.getNewDataFilterInstance(context, inputParameters));
        }

        // Add the filter panel.
        add(filterPanel, "FILTER_VIEW");
        jTableData.getSelectionModel().addListSelectionListener(new TableSelectionListener());
        jTableData.addMouseListener(new TableMouseListener());

        // Configure the table view according to the table definition.
        viewHandler.configureTable();

        // Enabled the required functionality.
        jButtonInsertNewEntity.setVisible(definition.isInsertEnabled());
        jButtonDeleteSelectedEntities.setVisible(definition.isDeleteEnabled());
        jButtonCommitChanges.setVisible(definition.isCommitEnabled());
        jButtonFilter.setVisible(definition.isUserFilterEnabled());
        jButtonRefresh.setVisible(definition.isUserRefreshEnabled());
        jButtonOptions.setVisible(definition.isOptionsEnabled());
        jXButtonPopup.setVisible(definition.isPopOutEnabled());
        jXButtonExport.setVisible(definition.isUserExportEnabled());

        // Set the visibility of the tool bar.
        jToolBarMain.setVisible(definition.isToolBarVisible());
        jToolBarPageControls.setVisible(definition.isToolBarVisible());
        setToolTipText(definition.getTooltipText());

        // Initialize the selection mode of the table.
        switch (definition.getSelectionMode())
        {
            case SINGLE_SELECTION:
                jTableData.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                break;
            case SINGLE_INTERVAL_SELECTION:
                jTableData.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
                break;
            case MULTIPLE_INTERVAL_SELECTION:
                jTableData.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
                break;
            default:
                break;//Do nothing and leave table on default selection mode
        }

        /*
         * Because we are changing the the column sizes, we cannot be restrained by the other resize modes that try to fit the entire table into the current panel,
         * we need the table to be able to grow to it the data/columns.
         */
        if (definition.getAutoResizeMode() == T8TableDefinition.AutoResizeMode.OFF && (definition.isResizeToColumnHeader() || definition.isResizeToColumnData()))
        {
            tableColumnAdjuster = new TableColumnAdjuster(jTableData);
            tableColumnAdjuster.setColumnHeaderIncluded(definition.isResizeToColumnHeader());
            tableColumnAdjuster.setColumnDataIncluded(definition.isResizeToColumnData());
            tableColumnAdjuster.setOnlyAdjustLarger(false);
        }

        // Initialize object providers.
        objectProviders.clear();
        for (T8TableDataObjectProviderDefinition objectProviderDefinition : definition.getDataObjectProviderDefinitions())
        {
            try
            {
                T8TableDataObjectProvider objectProvider;

                objectProvider = (T8TableDataObjectProvider)objectProviderDefinition.getNewDataObjectProviderInstance(controller);
                objectProvider.setTable(this);
                objectProviders.add(objectProvider);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while initializing object provider: " + objectProviderDefinition, e);
            }
        }

        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_1, KeyEvent.CTRL_DOWN_MASK), "SHOW_TABLE_REFERENCE");
        getActionMap().put("SHOW_TABLE_REFERENCE", new AbstractAction("SHOW_TABLE_REFERENCE")
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                showTableReferencePopup();
            }
        });
    }

    @Override
    public T8DataObject getDataObject(String objectIdentifier) throws Exception
    {
        for (T8TableDataObjectProvider objectProvider : objectProviders)
        {
            T8DataObject object;

            object = objectProvider.getDataObject(objectIdentifier);
            if (object != null) return object;
        }

        return null;
    }

    @Override
    public void startComponent()
    {
        // Reset the paging.
        resetPaging();
    }

    @Override
    public void stopComponent()
    {
        this.contextMenuHandler.deconstructMenu();

        if (lastDataLoader != null)
        {
            lastDataLoader.cancel(false);
        }
        if (lastDataCounter != null)
        {
            lastDataCounter.cancel(false);
        }
        busyPanel.setBusy(false);

        viewHandler.terminateRendererDataLoader();
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object layoutConstraints)
    {
        if (childComponent instanceof T8TableCellEditorComponent)
        {
            T8ComponentDefinition editorDefinition;

            editorDefinition = childComponent.getComponentDefinition();
            for (T8TableColumnDefinition columnDefinition : definition.getColumnDefinitions())
            {
                //Only set editors for visible columns, because the column would not be on the table otherwise
                if (columnDefinition.isVisible())
                {
                    // Set the column editor if it is available.
                    if (editorDefinition == columnDefinition.getEditorComponentDefinition())
                    {
                        T8TableCellEditorAdaptor editAdaptor;
                        int modelColumnIndex;

                        editAdaptor = new T8TableCellEditorAdaptor(this, (T8TableCellEditorComponent) childComponent);
                        modelColumnIndex = modelHandler.getColumnIndex(columnDefinition.getIdentifier());
                        viewHandler.setColumnEditor(viewHandler.convertModelToViewIndex(modelColumnIndex), editAdaptor);
                    }
                }
            }
        }
        else if (childComponent instanceof T8TableCellRendererComponent)
        {
            T8ComponentDefinition rendererDefinition;

            rendererDefinition = childComponent.getComponentDefinition();
            for (T8TableColumnDefinition columnDefinition : definition.getColumnDefinitions())
            {
                //Only set renderers for visible columns, because the column would not be on the table otherwise
                if (columnDefinition.isVisible())
                {
                    // Set the column renderer if it is available.
                    if (rendererDefinition == columnDefinition.getRendererComponentDefinition())
                    {
                        T8TableCellRendererAdaptor renderAdaptor;
                        int modelColumnIndex;
                        int viewColumnIndex;

                        renderAdaptor = new T8TableCellRendererAdaptor(this, (T8TableCellRendererComponent) childComponent);
                        modelColumnIndex = modelHandler.getColumnIndex(columnDefinition.getIdentifier());
                        viewColumnIndex = viewHandler.convertModelToViewIndex(modelColumnIndex);
                        //Check that the column is actually on the table before trying to set the renderer
                        if (viewColumnIndex != -1)
                        {
                            viewHandler.setColumnRenderer(viewColumnIndex, renderAdaptor);
                        }
                    }
                }
            }
        }
        else
        {
            throw new RuntimeException("Non-cell editor/renderer component cannot be added to " + this.getClass().getName());
        }
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public String getTranslatedString(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    public void showFilterView()
    {
        busyPanel.setBusy(false);
        ((CardLayout) this.getLayout()).show(this, "FILTER_VIEW");
    }

    public void showDataView()
    {
        busyPanel.setBusy(false);
        ((CardLayout) this.getLayout()).show(this, "DATA_VIEW");
    }

    public void showProcessingView(String message)
    {
        busyPanel.setMessage(message);
        busyPanel.setBusy(true);
        ((CardLayout) this.getLayout()).show(this, "PROCESSING_VIEW");
    }

    public Map<String, Object> getKey()
    {
        return key;
    }

    public void setKey(Map<String, Object> fieldValues)
    {
        key = fieldValues;
    }

    void clearUserDefinedFilter(boolean refresh)
    {
        this.userDefinedFilter = null;
        this.controller.reportEvent(this, this.definition.getComponentEventDefinition(T8TableAPIHandler.EVENT_FILTER_UPDATED), null);
        if (refresh) refreshData();
    }

    public T8DataFilter getUserDefinedFilter()
    {
        return userDefinedFilter;
    }

    public void setUserDefinedFilter(T8DataFilter filter)
    {
        this.userDefinedFilter = filter;
        this.dataHandler.resetPageOffset();
        controller.reportEvent(this, definition.getComponentEventDefinition(T8TableAPIHandler.EVENT_FILTER_UPDATED), null);
    }

    public void clearPrefilters(boolean refresh)
    {
        // Reset the paging because any change to the filters will affect them.
        resetPaging();

        this.prefilters.clear();
        this.dataHandler.resetPageOffset();
        if (refresh) refreshData();
    }

    public void setPrefilter(String identifier, T8DataFilter filter)
    {
        // Reset the paging becuase any change to the filters will affect them.
        resetPaging();

        // Update the filters.
        prefilters.put(identifier, filter);
        this.dataHandler.resetPageOffset();
        controller.reportEvent(this, definition.getComponentEventDefinition(T8TableAPIHandler.EVENT_FILTER_UPDATED), null);
    }

    public T8DataFilter getPrefilter(String identifier)
    {
        return prefilters.get(identifier);
    }

    public void setFieldOrdering(Map<String, OrderMethod> fieldOrdering)
    {
        this.fieldOrdering.clear();
        if (fieldOrdering != null)
        {
            this.fieldOrdering.putAll(fieldOrdering);
        }

        //Refresh the data
        refreshData();
    }

    public void setColumnVisibility(List<String> columnIdentifierList, boolean visible, boolean applyInverse)
    {
        viewHandler.setColumnVisibility(columnIdentifierList, visible, applyInverse);
    }

    public void quickOrderOnField(String fieldIdentifier)
    {
        if (fieldIdentifier != null)
        {
            OrderMethod orderMethod;

            orderMethod = fieldOrdering.get(fieldIdentifier); // Get existing ordering for the field (if set).
            fieldOrdering.clear(); // Clear pre-existing ordering.
            if (orderMethod == null) // Ordering was not set on the field, so set it to ascending.
            {
                fieldOrdering.put(fieldIdentifier, OrderMethod.ASCENDING);
            }
            else if (orderMethod == OrderMethod.ASCENDING) // Ordering was set to descending, so change it to ascending.
            {
                fieldOrdering.put(fieldIdentifier, OrderMethod.DESCENDING);
            }
            else // Ordering was set to descending, so change it to ascending.
            {
                fieldOrdering.put(fieldIdentifier, OrderMethod.ASCENDING);
            }

            // Refresh the data.
            refreshData();
        }
    }

    public void clearSelection()
    {
        viewHandler.clearSelection();
    }

    public void clearData()
    {

    }

    public boolean isOutdated()
    {
        return outdated;
    }

    public void setOutdated(boolean outdated)
    {
        this.outdated = outdated;
    }

    public void refreshData()
    {
        //Stop all previously executing loaders
        if (lastDataLoader != null)
        {
            lastDataLoader.cancel(false);
        }
        if (lastDataCounter != null)
        {
            lastDataCounter.cancel(false);
        }

        // Create a new loader.
        lastDataLoader = new DataLoader();

        //This is to fix a bug where the edited cell will keep its value after the data has been refreshed
        stopCellEditing();

        // Run the loader in a new Thread.
        lastDataLoader.execute();

        try
        {
            // Wait a while and if the loader still has not completed, display the processing panel.
            Thread.sleep(100);
            if (!lastDataLoader.isDone())
            {
                showProcessingView("Loading data...");
            }
        }
        catch (InterruptedException e)
        {
            LOGGER.log(e);
        }
    }

    public void refreshRecordCount(boolean lockUI)
    {
        // Cancel the last counter (if there is one).
        if (lastDataCounter != null)
        {
            lastDataCounter.cancel(false);
        }

        // Create a new loader.
        lastDataCounter = new DataCounter();

        // Run the loader in a new Thread.
        lastDataCounter.execute();

        // If we need to lock the UI while counting, do it.
        if (lockUI)
        {
            try
            {
                // Wait a while and if the loader still has not completed, display the processing panel.
                Thread.sleep(100);
                if (!lastDataCounter.isDone())
                {
                    showProcessingView("Counting data...");
                }
            }
            catch (InterruptedException e)
            {
                LOGGER.log(e);
            }
        }
    }

    private void resetPaging()
    {
        dataHandler.resetPageOffset();
        dataHandler.resetRowCount();
        jToolBarPageControls.setEnabled(false);
        jButtonPreviousPage.setEnabled(false);
        jButtonNextPage.setEnabled(false);
        jButtonGoToRow.setEnabled(false);
        jLabelRecordCount.setText("(?)");
        jLabelRecordCount.setVisible(true);
        jLabelPageStartOffset.setText(null);
        jLabelPageEndOffset.setText(null);
    }

    @Override
    public void cellDataEdited(Map<String, Object> dataRow, int rowIndex)
    {
        modelHandler.setRowDataValues(dataRow, rowIndex);
    }

    @Override
    public Map<String, Object> getRowEntityFieldValues(int row)
    {
        return modelHandler.getDataEntity(row).getFieldValues();
    }

    private void retrievePreviousPage()
    {
        dataHandler.decrementPage();
        refreshData();
    }

    private void retrieveNextPage()
    {
        dataHandler.incrementPage();
        refreshData();
    }

    private void retrieveSpecificPage()
    {
        String row;

        row = T8OptionPane.showInputDialog(this, getTranslatedString("Go to page containing this row:"), getTranslatedString("Go To Page"), T8OptionPane.PLAIN_MESSAGE, controller);
        if (Strings.isInteger(row))
        {
            //The user will use the display values for rows, but we need to work from a zero based index
            int parseInt = Integer.parseInt(row) - 1;
            if (parseInt > dataHandler.getRowCount() || parseInt < 0)
            {
                Toast.makeText(controller.getClientContext().getParentWindow(), getTranslatedString("Row number must be between 1 and ") + dataHandler.getRowCount(), Toast.Style.ERROR).display();
            }
            else
            {
                dataHandler.goToPage(dataHandler.findRowPage(parseInt));
                refreshData();
            }
        }
        else
        {
            Toast.makeText(controller.getClientContext().getParentWindow(), getTranslatedString("Only integer numbers accepted"), Toast.Style.ERROR).display();
        }
    }

    public void export()
    {
        try
        {
            T8TableExporter tabelExporter;

            tabelExporter = T8Reflections.getFastFailInstance("com.pilog.t8.ui.table.export.T8TableExportConfigurator", new Class<?>[]{T8ComponentController.class, T8DataFilter.class, T8TableDefinition.class}, this.controller, getCombinedFilter(), this.definition);
            tabelExporter.export();
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to load the class to perform the export", ex);
        }
    }

    private void deleteSelectedRows()
    {
        T8TableOnDeleteScriptDefinition onDeletionScripDefinition;

        // If a on-delete script is defined, execute it.
        onDeletionScripDefinition = definition.getOnDeleteScriptDefinition();
        if (onDeletionScripDefinition != null)
        {
            T8ClientScriptRunner scriptRunner;

            scriptRunner = new T8ClientScriptRunner(controller);

            try
            {
                Map<String, Object> scriptOutputParameters;
                Map<String, Object> scriptInputParameters;
                List<T8DataEntity> inputEntityList;
                List<T8DataEntity> outputEntityList;

                // Get the selected entities and prepare the script input parameters.
                inputEntityList = getSelectedDataEntities();
                scriptRunner.prepareScript(onDeletionScripDefinition);
                scriptInputParameters = new HashMap<>();
                scriptInputParameters.put(onDeletionScripDefinition.getNamespace() + T8TableOnDeleteScriptDefinition.PARAMETER_INPUT_ENTITY_LIST, inputEntityList);

                // Execute the script.
                scriptOutputParameters = scriptRunner.runScript(scriptInputParameters);

                // Get the output entity list and if it is not empty, delete the entities.
                outputEntityList = (List<T8DataEntity>)scriptOutputParameters.get(onDeletionScripDefinition.getNamespace() + T8TableOnDeleteScriptDefinition.PARAMETER_OUTPUT_ENTITY_LIST);
                if ((outputEntityList != null) && (outputEntityList.size() > 0))
                {
                    changeHandler.deleteRows(outputEntityList);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exeption while executing client-side on-delete script.", e);
            }
            finally
            {
                scriptRunner.finalizeScript();
            }
        }
        else
        {
            // No On-deletion script so just do the deletion straight away.
            changeHandler.deleteRows(getSelectedDataEntities());
        }
    }

    /**
     * Displays a dialog for options with regards to copying rows in the table.
     * The current row index is the result of some table action performed which
     * includes a row which may be different than that of the selected row.<br/>
     * <br/>
     * The current row index is not required and can be default to -1. This will
     * however disable the option of copying that index.
     *
     * @param currentRowIndex The {@code int} current row index. {@code -1} if
     *      no current row is/should be available
     */
    public void copyRows(int currentRowIndex)
    {
        T8CopyTableRowsOptions copyOptionsDialog;

        copyOptionsDialog = new T8CopyTableRowsOptions(context);

        // Will only ever be true if copy button is clicked on dialog.
        if (copyOptionsDialog.showDialog(currentRowIndex != -1))
        {
            List<T8DataEntity> rowsToCopy;
            boolean nonEditableOnly;
            boolean copyHidden;
            int insertionIndex;
            int numberOfCopies;

            nonEditableOnly = copyOptionsDialog.copyNonEditableOnly();
            copyHidden = copyOptionsDialog.copyHidden();
            numberOfCopies = copyOptionsDialog.getNumberOfCopies();

            rowsToCopy = new ArrayList<>();

            // Execute copy process, using settings, based on copy type
            switch (copyOptionsDialog.getCopyType())
            {
                case T8CopyTableRowsOptions.CURRENT_ROW:
                    rowsToCopy.add(this.modelHandler.getDataEntity(currentRowIndex));
                    insertionIndex = currentRowIndex+1;
                    break;
                case T8CopyTableRowsOptions.SELECTED_ROWS:
                    rowsToCopy.addAll(getSelectedDataEntities());
                    {
                        int[] selectedRows;

                        selectedRows = getTable().getSelectedRows();
                        insertionIndex = selectedRows.length == 0 ? 0 : selectedRows[selectedRows.length-1]+1;
                    }
                    break;
                case T8CopyTableRowsOptions.FIRST_SELECTED_ROW:
                    rowsToCopy.add(getSelectedDataEntity());
                    insertionIndex = getTable().getSelectedRow()+1;
                    break;
                case T8CopyTableRowsOptions.DISPLAYED_ROWS:
                    rowsToCopy.addAll(getDataEntities());
                    insertionIndex = 0;
                    break;
                default: throw new IllegalStateException("Unsupported Copy Type received. Type: " + copyOptionsDialog.getCopyType());
            }

            this.changeHandler.copyRows(rowsToCopy, numberOfCopies, insertionIndex, copyHidden, nonEditableOnly);
        }
    }

    public void fillDown(int currentRowIndex, int columnIndex)
    {
        T8TableFillDownOptions fillDownOptionsDialog;

        fillDownOptionsDialog = new T8TableFillDownOptions(context);

        // Will only ever be true if fill down button is clicked on dialog
        if (fillDownOptionsDialog.showDialog(currentRowIndex != -1))
        {
            List<Integer> fillDownRows;
            T8DataEntity fromEntity;
            int fromEntityIndex;

            switch (fillDownOptionsDialog.getFromAction())
            {
                case T8TableFillDownOptions.FILL_FROM_CURRENT_ROW:
                    fromEntity = getTableModelHandler().getDataEntity(currentRowIndex);
                    fromEntityIndex = currentRowIndex;
                    break;
                case T8TableFillDownOptions.FILL_FROM_FIRST_SELECTED:
                    fromEntity = getSelectedDataEntity();
                    fromEntityIndex = getTable().getSelectedRow();
                    break;
                case T8TableFillDownOptions.FILL_FROM_TOP_ROW:
                    fromEntity = getTableModelHandler().getDataEntity(0);
                    fromEntityIndex = 0;
                    break;
                default: throw new IllegalStateException("Unsupported Fill Down From Action received. Action: " + fillDownOptionsDialog.getFromAction());
            }

            fillDownRows = new ArrayList<>();
            switch (fillDownOptionsDialog.getToAction())
            {
                case T8TableFillDownOptions.FILL_TO_DISPLAYED:
                    for (int rowIdx = 0; rowIdx < getTable().getRowCount(); rowIdx++)
                    {
                        if (fromEntityIndex == rowIdx) continue; // We want to skip the row from which we're filling down
                        fillDownRows.add(rowIdx);
                    }
                    break;
                case T8TableFillDownOptions.FILL_TO_SELECTED:
                    for (int selectedRow : getTable().getSelectedRows())
                    {
                        if (fromEntityIndex == selectedRow) continue; // We want to skip the row from which we're filling down
                        fillDownRows.add(selectedRow);
                    }
                    break;
                case T8TableFillDownOptions.FILL_TO_ROW_COUNT:
                    fromEntityIndex++;
                    for (int rowIdx = fromEntityIndex; (rowIdx < getTable().getRowCount() && rowIdx < (fromEntityIndex + fillDownOptionsDialog.getFillDownRowCount())); rowIdx++)
                    {
                        fillDownRows.add(rowIdx);
                    }
                    break;
                default: throw new IllegalStateException("Unsupported Fill Down To Action received. Action: " + fillDownOptionsDialog.getToAction());
            }

            this.changeHandler.fillDown(fromEntity, columnIndex, fillDownRows);
        }
    }

    private void showTableReferencePopup()
    {
        //Create a new Table definition that is read only with now actions
        T8TableDefinition referenceDefinition;
        final T8Table referenceTable;
        JFrame referenceFrame;

        referenceDefinition = new T8TableDefinition(definition.getIdentifier());
        referenceDefinition.setAutoResizeMode(definition.getAutoResizeMode());
        referenceDefinition.setAutoSelectOnRetrieve(false);
        referenceDefinition.setColumnDefinitions(definition.getColumnDefinitions());
        referenceDefinition.setCommitEnabled(false);
        referenceDefinition.setDataEntityIdentifier(definition.getDataEntityIdentifier());
        referenceDefinition.setDeleteEnabled(false);
        referenceDefinition.setEntityDefinition(definition.getEntityDefinition());
        referenceDefinition.setInsertEnabled(false);
        referenceDefinition.setOptionsEnabled(false);
        referenceDefinition.setOrderingPresetDefinitions(definition.getOrderingPresetDefinitions());
        referenceDefinition.setPageSize(definition.getPageSize());
        referenceDefinition.setResizeToColumnData(definition.isResizeToColumnData());
        referenceDefinition.setResizeToColumnHeader(definition.isResizeToColumnHeader());
        referenceDefinition.setRowHeight(definition.getRowHeight());
        referenceDefinition.setSelectionMode(T8TableDefinition.SelectionMode.SINGLE_SELECTION);
        referenceDefinition.setToolBarVisible(true);
        referenceDefinition.setUserExportEnabled(false);
        referenceDefinition.setUserFilterEnabled(true);
        referenceDefinition.setUserRefreshEnabled(true);

        referenceTable = new T8Table(referenceDefinition, controller);
        for (String key : prefilters.keySet())
        {
            referenceTable.setPrefilter(key, prefilters.get(key));
        }
        referenceTable.setUserDefinedFilter(getUserDefinedFilter());
        referenceTable.setKey(key);
        referenceFrame = new JFrame();
        referenceFrame.add(referenceTable);
        referenceFrame.setSize(getWidth(), getHeight());
        referenceFrame.addWindowListener(new WindowAdapter()
        {

            @Override
            public void windowOpened(WindowEvent e)
            {
                referenceTable.initializeComponent(null);
                referenceTable.startComponent();
            }

            @Override
            public void windowClosing(WindowEvent e)
            {
                referenceTable.stopComponent();
            }
        });
        referenceFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        referenceFrame.setVisible(true);
    }

    public List<Map<String, Object>> getSelectedDataRows()
    {
        stopCellEditing();
        return modelHandler.getDataRows(jTableData.getSelectedRows());
    }

    /**
     * Gets all of the data entities which are currently being displayed.
     *
     * @return The list of currently displayed {@code T8DataEntity} objects
     */
    public List<T8DataEntity> getDataEntities()
    {
        stopCellEditing();
        return modelHandler.getDataEntities();
    }

    /**
     * All of the selected data entities.
     *
     * @return The list of selected {@code T8DataEntity} objects
     */
    public List<T8DataEntity> getSelectedDataEntities()
    {
        stopCellEditing();
        return modelHandler.getDataEntities(jTableData.getSelectedRows());
    }

    /**
     * Returns all data entities marked with a tick in the corresponding row header.
     *
     * @return The list of ticked {@code T8DataEntity} objects.
     */
    public List<T8DataEntity> getTickedDataEntities()
    {
        stopCellEditing();
        return modelHandler.getTickedDataEntities();
    }

    /**
     * Returns the first selected data entity.
     *
     * @return The first selected {@code T8DataEntity}
     */
    public T8DataEntity getSelectedDataEntity()
    {
        int[] selectedRows;

        stopCellEditing();
        selectedRows = jTableData.getSelectedRows();
        if (selectedRows.length > 0)
        {
            return modelHandler.getDataEntity(selectedRows[0]);
        }
        else return null;
    }

    public boolean areAllRowsTicked()
    {
        return modelHandler.areAllRowsTicked();
    }

    public void setAllRowsTicked(boolean ticked)
    {
        stopCellEditing();
        modelHandler.setAllRowsTicked(ticked);
    }

    public int getDataEntityCount()
    {
        return dataHandler.getRowCount();
    }

    public boolean commitTableChanges()
    {
        // Do some client-side validation before committing changes.
        stopCellEditing();
        if (changeHandler.validateChangedRows())
        {
            try
            {
                if (changeHandler.commitTableChanges(controller.getContext()))
                {
                    Toast.makeText(controller.getClientContext().getParentWindow(), getTranslatedString("Changes Saved Successfully"), Toast.Style.SUCCESS).display();
                    viewHandler.repaint();
                    return true;
                }
                else
                {
                    Toast.makeText(controller.getClientContext().getParentWindow(), getTranslatedString("Data Validation Failure"), Toast.Style.ERROR).display();
                    viewHandler.repaint();
                    return false;
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while committing table changes.", e);
                JOptionPane.showMessageDialog(this, "An unexpected error prevented the successful completion of the operation.", "Data Persistence Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        else
        {
            Toast.makeText(controller.getClientContext().getParentWindow(), getTranslatedString("Data Validation Failure"), Toast.Style.ERROR).display();
            viewHandler.repaint();
            return false;
        }
    }

    public void stopCellEditing()
    {
        TableCellEditor cellEditor;

        cellEditor = viewHandler.getRowHeaderTable().getCellEditor();
        if (cellEditor != null)
        {
            cellEditor.stopCellEditing();
        }

        cellEditor = jTableData.getCellEditor();
        if (cellEditor != null)
        {
            cellEditor.stopCellEditing();
        }
    }

    public JTable getTable()
    {
        return this.jTableData;
    }

    public JViewport getViewport()
    {
        return jScrollPaneTable.getViewport();
    }

    @Override
    public Font getCellFont()
    {
        return jTableData.getFont();
    }

    @Override
    public Color getSelectedCellBackgroundColor()
    {
        return jTableData.getBackground(); // Do not color the background when cell is selected.  The border indicates selection.
    }

    @Override
    public Color getSelectedCellForegroundColor()
    {
        return jTableData.getForeground();
    }

    @Override
    public Color getUnselectedCellBackgroundColor()
    {
        return jTableData.getBackground();
    }

    @Override
    public Color getUnselectedCellForegroundColor()
    {
        return jTableData.getForeground();
    }

    @Override
    public Border getSelectedBorder()
    {
        return SELECTED_BORDER;
    }

    @Override
    public Border getUnselectedBorder()
    {
        return UNSELECTED_BORDER;
    }

    @Override
    public Border getFocusedBorder()
    {
        return FOCUS_BORDER;
    }

    @Override
    public Border getFocusedSelectedBorder()
    {
        return FOCUS_SELECTED_BORDER;
    }

    T8TableDataChangeHandler getTableDataChangeHandler()
    {
        return changeHandler;
    }

    T8TableViewHandler getTableViewHandler()
    {
        return viewHandler;
    }

    T8TableModelHandler getTableModelHandler()
    {
        return modelHandler;
    }

    /**
     * Creates and returns a data filter that includes all of the prefilters set
     * on the table along with the user-set filter criteria and ordering.
     *
     * @return The combined filter to use when retrieving data to display in the
     *         table.
     */
    public T8DataFilter getCombinedFilter()
    {
        T8DataFilterCriteria combinedFilterCriteria;
        T8DataFilter combinedFilter = null;

        // Create a filter to contain a combination of all applicable filters for data retrieval.
        combinedFilterCriteria = new T8DataFilterCriteria();

        // Add the key filter values if necessary.
        if ((key != null) && (key.size() > 0))
        {
            combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, new T8DataFilterCriteria(new HashMap<>(key)));
        }

        // Add all available and valid prefilters.
        for (T8DataFilter prefilter : prefilters.values())
        {
            // Only include filters that have valid filter criteria.
            if ((prefilter != null) && (prefilter.hasFilterCriteria()))
            {
                combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria().copy());
            }
        }

        // If field ordering is null or empty check if one of the pre-filters have an ordering.
        if (fieldOrdering == null || fieldOrdering.isEmpty())
        {
            for (T8DataFilter prefilter : prefilters.values())
            {
                // Only include filters that have valid filter criteria.
                if ((prefilter != null) && (prefilter.hasFieldOrdering()))
                {
                    fieldOrdering = prefilter.getFieldOrdering();
                }
            }
        }

        // Add the regular user-defined filter if applicable.
        if ((userDefinedFilter != null) && (userDefinedFilter.hasFilterCriteria()))
        {
            combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, userDefinedFilter.getFilterCriteria().copy());
        }

        /*
         Create the combined filter from the filter criteria.
         Check if there exists a group by filter in the pre filters, if that is the case
         then create a group by filter as the root, so that the filters work
         correctly.
         */
        for (T8DataFilter t8DataFilter : prefilters.values())
        {
            if (t8DataFilter instanceof T8GroupByDataFilter)
            {
                combinedFilter = t8DataFilter.copy();
                combinedFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, combinedFilterCriteria);
                break;
            }
        }
        if (combinedFilter == null)
        {
            combinedFilter = new T8DataFilter(definition.getDataEntityIdentifier(), combinedFilterCriteria);
        }

        // Add the user-defined ordering if applicable.
        combinedFilter.setFieldOrdering(new LinkedHashMap<>(fieldOrdering));

        // Return a new filter created from the combined criteria.
        return combinedFilter;
    }

    private void configureDeveloperShortcuts()
    {
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("alt shift 1"), "Entity Identifier");
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("alt shift 2"), "Entity Fields");
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("alt shift 3"), "Key Fields");
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("alt shift 4"), "All Fields");

        getActionMap().put("Entity Identifier", new AbstractAction("Entity Identifier")
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                LOGGER.log("\nTable Identifier: " + definition.getPublicIdentifier() + ", Entity Identifier: " + definition.getDataEntityIdentifier() + "\n");
            }
        });

        getActionMap().put("Entity Fields", new AbstractAction("Entity Fields")
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                LOGGER.log(() -> "\nTable Identifier: " + definition.getPublicIdentifier() + ", Entity Identifier: " + definition.getDataEntityIdentifier());
                for (T8TableColumnDefinition columnDefinition : definition.getColumnDefinitions())
                {
                    LOGGER.log("\tField Identifier: " + columnDefinition.getFieldIdentifier());
                }
            }
        });

        getActionMap().put("Key Fields", new AbstractAction("Key Fields")
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                List<T8DataEntity> dataEntitys;

                dataEntitys = getSelectedDataEntities();
                if(!dataEntitys.isEmpty())
                {
                    for (T8DataEntity t8DataEntity : dataEntitys)
                    {
                        LOGGER.log("\n");
                        for (Map.Entry<String, Object> entry : t8DataEntity.getKeyFieldValues().entrySet())
                        {
                            LOGGER.log("\tField Identifier: " + entry.getKey() + ", Field Value: " + entry.getValue());
                        }
                    }

                } else LOGGER.log("No rows selected");
            }
        });

        getActionMap().put("All Fields", new AbstractAction("All Fields")
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                List<T8DataEntity> dataEntitys;

                dataEntitys = getSelectedDataEntities();
                if(!dataEntitys.isEmpty())
                {
                    for (T8DataEntity t8DataEntity : dataEntitys)
                    {
                        LOGGER.log("\n");
                        for (Map.Entry<String, Object> entry : t8DataEntity.getFieldValues().entrySet())
                        {
                            LOGGER.log("\tField Identifier: " + entry.getKey() + ", Field Value: " + entry.getValue());
                        }
                    }
                } else LOGGER.log("No rows selected");
            }
        });
    }

    @Override
    public Object getAdditionalRenderData(int row, String fieldIdentifier)
    {
        return modelHandler.getAdditionalRenderData(row, fieldIdentifier);
    }

    private class DataLoader extends SwingWorker<T8DefaultTableModel, Void>
    {
        @Override
        protected T8DefaultTableModel doInBackground() throws Exception
        {
            T8DefaultTableModel tableModel;

            //Notify listeners that we will be loading new data
            controller.reportEvent(T8Table.this, definition.getComponentEventDefinition(T8TableAPIHandler.EVENT_DATA_LOADING), null);

            // Use the combined filter to retrieve the filtered data and to construct a table model.
            tableModel = dataHandler.loadTableData(controller.getContext(), getCombinedFilter());

            // After the table data is retrieved start refreshing the table count
            if (definition.isAutoRowCount())
            {
                refreshRecordCount(false);
            }

            // Set the outdated flag to false, since the latest data has now been retrieved.
            outdated = false;

            // Return the table model.
            return tableModel;
        }

        @Override
        protected void done()
        {
            if (!isCancelled())
            {
                T8DefaultTableModel tableModel;
                try
                {
                    tableModel = get();
                }
                catch (InterruptedException | ExecutionException ex)
                {
                    LOGGER.log(ex);
                    tableModel = new T8DefaultTableModel(definition, new ArrayList<>(), dataHandler.getPageOffset());
                }

                // Clear all previous table changes.
                changeHandler.clearChanges();

                // Set the new model.
                modelHandler.setModel(tableModel);
                viewHandler.setModel(tableModel);

                // Set the page number label starting at index 1.
                jLabelPageStartOffset.setText("" + (dataHandler.getPageOffset() + 1));
                if (dataHandler.getRowCount() > 0)
                {
                    jLabelPageEndOffset.setText("" + Math.min(dataHandler.getPageOffset() + dataHandler.getPageSize(), dataHandler.getRowCount()));
                }
                else
                {
                    jLabelPageEndOffset.setText("" + (dataHandler.getPageOffset() + dataHandler.getPageSize()));
                }

                // Adjust columns.
                if (tableColumnAdjuster != null)
                {
                    tableColumnAdjuster.adjustColumns();
                }

                // Set the state of the paging controls.
                jButtonPreviousPage.setEnabled(dataHandler.getPageOffset() > 0);
                jButtonNextPage.setEnabled((dataHandler.getPageOffset() + dataHandler.getPageSize()) < dataHandler.getRowCount());
                jButtonGoToRow.setEnabled(dataHandler.getRowCount() > 0);
                jToolBarPageControls.setEnabled(true);

                // Switch back to the data view.
                showDataView();

                if (definition.isAutoSelectOnRetrieve() && jTableData.getRowCount() > 0)
                {
                    jTableData.setColumnSelectionInterval(0, jTableData.getColumnCount()-1);
                    jTableData.setRowSelectionInterval(0, 0);
                }

                //Notify listeners that we the table has new data loaded
                controller.reportEvent(T8Table.this, definition.getComponentEventDefinition(T8TableAPIHandler.EVENT_DATA_LOADED), null);
            }
        }
    }

    private class DataCounter extends SwingWorker<Integer, Void>
    {
        @Override
        protected Integer doInBackground() throws Exception
        {
            return dataHandler.countTableData(controller.getContext(), getCombinedFilter());
        }

        @Override
        protected void done()
        {
            if (!isCancelled())
            {
                try
                {
                    // Get the Record Count, if that failed the exception will be rethrown here
                    Integer recordCount = get();

                    // Update the UI to reflect the record count.
                    jLabelRecordCount.setText("(" + recordCount + ")");
                    jLabelRecordCount.setVisible(true);

                    // Hide the paging buttons if no paging can be performed.
                    jButtonPreviousPage.setEnabled(dataHandler.getPageOffset() > 0);
                    jButtonNextPage.setEnabled((dataHandler.getPageOffset() + dataHandler.getPageSize()) < dataHandler.getRowCount());
                    jButtonGoToRow.setEnabled(dataHandler.getRowCount() > 0);
                    jToolBarPageControls.setEnabled(true);

                    // Switch back to the data view unlocking the UI.
                    showDataView();
                }
                catch (InterruptedException | ExecutionException e)
                {
                    LOGGER.log(e);
                    resetPaging();
                }
            }
        }
    }

    private class TableSelectionListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            if (!e.getValueIsAdjusting())
            {
                HashMap<String, Object> eventParameters;

                eventParameters = new HashMap<>();
                eventParameters.put(T8TableAPIHandler.PARAMETER_DATA_ENTITY_LIST, getSelectedDataEntities());
                controller.reportEvent(T8Table.this, definition.getComponentEventDefinition(T8TableAPIHandler.EVENT_ROW_SELECTION_CHANGED), eventParameters);
            }
        }
    }

    private class TableMouseListener extends MouseAdapter
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.getClickCount() == 2)
            {
                HashMap<String, Object> eventParameters;

                eventParameters = new HashMap<>();
                eventParameters.put(T8TableAPIHandler.PARAMETER_DATA_ENTITY_LIST, getSelectedDataEntities());
                controller.reportEvent(T8Table.this, definition.getComponentEventDefinition(T8TableAPIHandler.EVENT_DOUBLE_CLICKED), eventParameters);
            }
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            showPopupIfAvailable(e);
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            showPopupIfAvailable(e);
        }

        /**
         * <b>Note:</b> Popup menus are triggered differently on different
         * systems. Therefore, {@code isPopupTrigger} should be checked in both
         * {@code mousePressed} and {@code mouseReleased} for proper
         * cross-platform functionality.
         *
         * @param e The {@code MouseEvent} which was triggered
         */
        private void showPopupIfAvailable(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                T8DataEntity currentRow;
                JPopupMenu contextMenu;
                Point mousePoint;
                int rowIndex;

                // Get the point where the mouse action occurred
                mousePoint = new Point(e.getX(), e.getY());

                // We don't want an error if the click did not occur on an actual row
                rowIndex = jTableData.rowAtPoint(mousePoint);
                currentRow = rowIndex != -1 ? modelHandler.getDataEntity(rowIndex) : null;

                // We request the menu from the menu handler, and show it if there is one
                contextMenu = contextMenuHandler.getMenu(currentRow, rowIndex, jTableData.columnAtPoint(mousePoint));
                if (contextMenu != null)
                {
                    contextMenu.show(jTableData, e.getX(), e.getY());
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelDataView = new javax.swing.JPanel();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonCommitChanges = new javax.swing.JButton();
        jButtonRefresh = new javax.swing.JButton();
        jButtonFilter = new javax.swing.JButton();
        jButtonInsertNewEntity = new javax.swing.JButton();
        jButtonDeleteSelectedEntities = new javax.swing.JButton();
        jButtonOptions = new javax.swing.JButton();
        jXButtonExport = new org.jdesktop.swingx.JXButton();
        jXButtonOrderby = new org.jdesktop.swingx.JXButton();
        jXButtonPopup = new org.jdesktop.swingx.JXButton();
        jToolBarPageControls = new javax.swing.JToolBar();
        jButtonPreviousPage = new javax.swing.JButton();
        jLabelPageRow = new javax.swing.JLabel();
        jLabelPageStartOffset = new javax.swing.JLabel();
        jLabelPageRangeDash = new javax.swing.JLabel();
        jLabelPageEndOffset = new javax.swing.JLabel();
        jButtonNextPage = new javax.swing.JButton();
        jButtonGoToRow = new javax.swing.JButton();
        jButtonRowCount = new javax.swing.JButton();
        jLabelRecordCount = new javax.swing.JLabel();
        jScrollPaneTable = new javax.swing.JScrollPane();
        jTableData = new javax.swing.JTable();
        jPanelProcessingView = new javax.swing.JPanel();

        setOpaque(false);
        setLayout(new java.awt.CardLayout());

        jPanelDataView.setOpaque(false);
        jPanelDataView.setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);
        jToolBarMain.setOpaque(false);

        jButtonCommitChanges.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/commitIcon.png"))); // NOI18N
        jButtonCommitChanges.setText(configurationManager.getUITranslation(context, "Commit"));
        jButtonCommitChanges.setToolTipText("Commit Changes");
        jButtonCommitChanges.setFocusable(false);
        jButtonCommitChanges.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonCommitChanges.setOpaque(false);
        jButtonCommitChanges.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCommitChanges.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCommitChangesActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCommitChanges);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setText(configurationManager.getUITranslation(context, "Refresh"));
        jButtonRefresh.setToolTipText("Refresh Data");
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRefresh.setOpaque(false);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefresh);

        jButtonFilter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/filterIcon.png"))); // NOI18N
        jButtonFilter.setText(configurationManager.getUITranslation(context, "Filter"));
        jButtonFilter.setFocusable(false);
        jButtonFilter.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonFilter.setOpaque(false);
        jButtonFilter.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonFilter.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonFilterActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonFilter);

        jButtonInsertNewEntity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/tableRowInsertIcon.png"))); // NOI18N
        jButtonInsertNewEntity.setText(configurationManager.getUITranslation(context, "Insert"));
        jButtonInsertNewEntity.setToolTipText("Insert New Row");
        jButtonInsertNewEntity.setFocusable(false);
        jButtonInsertNewEntity.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonInsertNewEntity.setOpaque(false);
        jButtonInsertNewEntity.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonInsertNewEntity.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonInsertNewEntityActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonInsertNewEntity);

        jButtonDeleteSelectedEntities.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/tableRowDeleteIcon.png"))); // NOI18N
        jButtonDeleteSelectedEntities.setText(configurationManager.getUITranslation(context, "Delete"));
        jButtonDeleteSelectedEntities.setToolTipText("Delete Selected Rows");
        jButtonDeleteSelectedEntities.setFocusable(false);
        jButtonDeleteSelectedEntities.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonDeleteSelectedEntities.setOpaque(false);
        jButtonDeleteSelectedEntities.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDeleteSelectedEntities.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDeleteSelectedEntitiesActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDeleteSelectedEntities);

        jButtonOptions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/gearIcon.png"))); // NOI18N
        jButtonOptions.setText(configurationManager.getUITranslation(context, "Options"));
        jButtonOptions.setToolTipText("Show the table options menu");
        jButtonOptions.setFocusable(false);
        jButtonOptions.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonOptions.setOpaque(false);
        jButtonOptions.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBarMain.add(jButtonOptions);

        jXButtonExport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/export.png"))); // NOI18N
        jXButtonExport.setText(configurationManager.getUITranslation(context, "Export"));
        jXButtonExport.setFocusable(false);
        jXButtonExport.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jXButtonExport.setOpaque(false);
        jXButtonExport.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jXButtonExport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonExportActionPerformed(evt);
            }
        });
        jToolBarMain.add(jXButtonExport);

        jXButtonOrderby.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/sort.png"))); // NOI18N
        jXButtonOrderby.setText(configurationManager.getUITranslation(context, "Ordering"));
        jXButtonOrderby.setFocusable(false);
        jXButtonOrderby.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jXButtonOrderby.setOpaque(false);
        jXButtonOrderby.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jXButtonOrderby.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonOrderbyActionPerformed(evt);
            }
        });
        jToolBarMain.add(jXButtonOrderby);

        jXButtonPopup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/tableReferencePopup.png"))); // NOI18N
        jXButtonPopup.setText(configurationManager.getUITranslation(context, "Pop-up"));
        jXButtonPopup.setFocusable(false);
        jXButtonPopup.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jXButtonPopup.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jXButtonPopup.setOpaque(false);
        jXButtonPopup.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jXButtonPopup.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonPopupActionPerformed(evt);
            }
        });
        jToolBarMain.add(jXButtonPopup);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelDataView.add(jToolBarMain, gridBagConstraints);

        jToolBarPageControls.setFloatable(false);
        jToolBarPageControls.setRollover(true);
        jToolBarPageControls.setOpaque(false);

        jButtonPreviousPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pagePreviousIcon.png"))); // NOI18N
        jButtonPreviousPage.setToolTipText(getTranslatedString("Previous Page"));
        jButtonPreviousPage.setFocusable(false);
        jButtonPreviousPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonPreviousPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonPreviousPage.setOpaque(false);
        jButtonPreviousPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPreviousPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPreviousPageActionPerformed(evt);
            }
        });
        jToolBarPageControls.add(jButtonPreviousPage);

        jLabelPageRow.setText(getTranslatedString("Row: "));
        jToolBarPageControls.add(jLabelPageRow);
        jToolBarPageControls.add(jLabelPageStartOffset);

        jLabelPageRangeDash.setText("-");
        jToolBarPageControls.add(jLabelPageRangeDash);
        jToolBarPageControls.add(jLabelPageEndOffset);

        jButtonNextPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pageNextIcon.png"))); // NOI18N
        jButtonNextPage.setToolTipText(getTranslatedString("Next Page"));
        jButtonNextPage.setFocusable(false);
        jButtonNextPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonNextPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonNextPage.setOpaque(false);
        jButtonNextPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonNextPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonNextPageActionPerformed(evt);
            }
        });
        jToolBarPageControls.add(jButtonNextPage);

        jButtonGoToRow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/control-cursor.png"))); // NOI18N
        jButtonGoToRow.setFocusable(false);
        jButtonGoToRow.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonGoToRow.setOpaque(false);
        jButtonGoToRow.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonGoToRowActionPerformed(evt);
            }
        });
        jToolBarPageControls.add(jButtonGoToRow);

        jButtonRowCount.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/calculatorIcon.png"))); // NOI18N
        jButtonRowCount.setToolTipText("Count Rows (Enable Paging)");
        jButtonRowCount.setFocusable(false);
        jButtonRowCount.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRowCount.setOpaque(false);
        jButtonRowCount.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRowCount.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRowCountActionPerformed(evt);
            }
        });
        jToolBarPageControls.add(jButtonRowCount);

        jLabelRecordCount.setText("(0)");
        jToolBarPageControls.add(jLabelRecordCount);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelDataView.add(jToolBarPageControls, gridBagConstraints);

        jTableData.setAutoCreateColumnsFromModel(false);
        jTableData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        jTableData.setGridColor(new java.awt.Color(204, 204, 204));
        jTableData.setRowHeight(20);
        jScrollPaneTable.setViewportView(jTableData);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelDataView.add(jScrollPaneTable, gridBagConstraints);

        add(jPanelDataView, "DATA_VIEW");

        jPanelProcessingView.setLayout(new java.awt.BorderLayout());
        add(jPanelProcessingView, "PROCESSING_VIEW");
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        refreshData();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jButtonPreviousPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPreviousPageActionPerformed
    {//GEN-HEADEREND:event_jButtonPreviousPageActionPerformed
        retrievePreviousPage();
    }//GEN-LAST:event_jButtonPreviousPageActionPerformed

    private void jButtonNextPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonNextPageActionPerformed
    {//GEN-HEADEREND:event_jButtonNextPageActionPerformed
        retrieveNextPage();
    }//GEN-LAST:event_jButtonNextPageActionPerformed

    private void jButtonInsertNewEntityActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonInsertNewEntityActionPerformed
    {//GEN-HEADEREND:event_jButtonInsertNewEntityActionPerformed
        changeHandler.insertNewRow();
    }//GEN-LAST:event_jButtonInsertNewEntityActionPerformed

    private void jButtonDeleteSelectedEntitiesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteSelectedEntitiesActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteSelectedEntitiesActionPerformed
        deleteSelectedRows();
    }//GEN-LAST:event_jButtonDeleteSelectedEntitiesActionPerformed

    private void jButtonCommitChangesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCommitChangesActionPerformed
    {//GEN-HEADEREND:event_jButtonCommitChangesActionPerformed
        commitTableChanges();
    }//GEN-LAST:event_jButtonCommitChangesActionPerformed

    private void jButtonFilterActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonFilterActionPerformed
    {//GEN-HEADEREND:event_jButtonFilterActionPerformed
        showFilterView();
    }//GEN-LAST:event_jButtonFilterActionPerformed

    private void jButtonGoToRowActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonGoToRowActionPerformed
    {//GEN-HEADEREND:event_jButtonGoToRowActionPerformed
        retrieveSpecificPage();
    }//GEN-LAST:event_jButtonGoToRowActionPerformed

    private void jXButtonExportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonExportActionPerformed
    {//GEN-HEADEREND:event_jXButtonExportActionPerformed
        export();
    }//GEN-LAST:event_jXButtonExportActionPerformed

    private void jXButtonOrderbyActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonOrderbyActionPerformed
    {//GEN-HEADEREND:event_jXButtonOrderbyActionPerformed
        T8PresetOrderingPanelPopup.showOrderingPopup(jXButtonOrderby, this, fieldOrdering);
    }//GEN-LAST:event_jXButtonOrderbyActionPerformed

    private void jXButtonPopupActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonPopupActionPerformed
    {//GEN-HEADEREND:event_jXButtonPopupActionPerformed
        showTableReferencePopup();
    }//GEN-LAST:event_jXButtonPopupActionPerformed

    private void jButtonRowCountActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRowCountActionPerformed
    {//GEN-HEADEREND:event_jButtonRowCountActionPerformed
        refreshRecordCount(true);
    }//GEN-LAST:event_jButtonRowCountActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCommitChanges;
    private javax.swing.JButton jButtonDeleteSelectedEntities;
    private javax.swing.JButton jButtonFilter;
    private javax.swing.JButton jButtonGoToRow;
    private javax.swing.JButton jButtonInsertNewEntity;
    private javax.swing.JButton jButtonNextPage;
    private javax.swing.JButton jButtonOptions;
    private javax.swing.JButton jButtonPreviousPage;
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JButton jButtonRowCount;
    private javax.swing.JLabel jLabelPageEndOffset;
    private javax.swing.JLabel jLabelPageRangeDash;
    private javax.swing.JLabel jLabelPageRow;
    private javax.swing.JLabel jLabelPageStartOffset;
    private javax.swing.JLabel jLabelRecordCount;
    private javax.swing.JPanel jPanelDataView;
    private javax.swing.JPanel jPanelProcessingView;
    private javax.swing.JScrollPane jScrollPaneTable;
    private javax.swing.JTable jTableData;
    private javax.swing.JToolBar jToolBarMain;
    private javax.swing.JToolBar jToolBarPageControls;
    private org.jdesktop.swingx.JXButton jXButtonExport;
    private org.jdesktop.swingx.JXButton jXButtonOrderby;
    private org.jdesktop.swingx.JXButton jXButtonPopup;
    // End of variables declaration//GEN-END:variables
}
