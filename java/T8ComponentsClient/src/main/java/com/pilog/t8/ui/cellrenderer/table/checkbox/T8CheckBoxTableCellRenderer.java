package com.pilog.t8.ui.cellrenderer.table.checkbox;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellRenderableComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent.RenderParameter;
import com.pilog.t8.definition.ui.cellrenderer.table.checkbox.T8CheckBoxTableCellRendererDefinition;
import com.pilog.t8.ui.checkbox.T8CheckBox;
import java.util.Map;
import java.util.Objects;
import javax.swing.SwingConstants;

/**
 * @author Bouwer du Preez
 */
public class T8CheckBoxTableCellRenderer extends T8CheckBox implements T8TableCellRendererComponent
{
    private static final T8Logger logger = T8Log.getLogger(T8CheckBoxTableCellRenderer.class);

    private final T8CheckBoxTableCellRendererDefinition definition;

    private Object selectedValue;
    private Object deselectedValue;

    public T8CheckBoxTableCellRenderer(T8CheckBoxTableCellRendererDefinition definition, T8ComponentController controller)
    {
        super(definition, controller, false);
        this.setHorizontalAlignment(SwingConstants.CENTER);
        this.setBorderPaintedFlat(true);
        this.definition = definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        try
        {
            String selectedValueExpression;
            String deselectedValueExpression;
            ExpressionEvaluator evaluator;

            // Create an expression evaluator.
            evaluator = new ExpressionEvaluator();

            // Evaluate the selected value expression.
            selectedValueExpression = definition.getSelectedValueExpression();
            if (selectedValueExpression != null)
            {
                selectedValue = evaluator.evaluateExpression(selectedValueExpression, inputParameters, null);
            }
            else throw new RuntimeException("No selected value expression set in " + definition);

            // Evaluator the deselected value expression.
            deselectedValueExpression = definition.getDeselectedValueExpression();
            if (deselectedValueExpression != null)
            {
                deselectedValue = evaluator.evaluateExpression(deselectedValueExpression, inputParameters, null);
            }
            else throw new RuntimeException("No deselected value expression set in " + definition);
        }
        catch (EPICSyntaxException | EPICRuntimeException | RuntimeException e)
        {
            logger.log("Exception while initializing component: " + definition, e);
        }
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void setRendererData(T8TableCellRenderableComponent renderableComponent, Map<String, Object> dataRow, String string, Map<RenderParameter, Object> renderParameters)
    {
        Object value;

        value = dataRow != null ? dataRow.get(definition.getTargetFieldIdentifier()) : null;
        if (Objects.equals(value, selectedValue))
        {
            setSelected(true);
        }
        else
        {
            setSelected(false);
        }
    }

    @Override
    public Object getAdditionalRenderData(Map<String, Object> dataRow, String fieldIdentifier)
    {
        return null;
    }
}
