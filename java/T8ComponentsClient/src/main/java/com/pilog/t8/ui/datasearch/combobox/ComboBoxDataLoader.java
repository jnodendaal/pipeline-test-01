package com.pilog.t8.ui.datasearch.combobox;

import static com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxAPIHandler.*;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingWorker;

/**
 * @author Hennie Brink
 */
public class ComboBoxDataLoader extends SwingWorker<ComboBoxListModel, Void>
{
    private final ComboBoxPopup owner;
    private final String dataSourceId;
    private final T8Context context;
    private int dataSetSize;

    private T8DataFilter dataFilter;
    private String searchQuery;

    public ComboBoxDataLoader(ComboBoxPopup owner, T8Context context, String dataSourceId, int dataSetSize)
    {
        this.owner = owner;
        this.dataSourceId = dataSourceId;
        this.context = context;
        this.dataSetSize = dataSetSize;
    }

    public void increaseDataSetSize(int amount)
    {
        dataSetSize += amount;
    }

    @Override
    protected ComboBoxListModel doInBackground() throws Exception
    {
        owner.showLoadingView();
        //Simulate Data Loading
        ComboBoxModelRootNode rootNode;
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DATA_SOURCE_IDENTIFIER, this.dataSourceId);
        operationParameters.put(PARAMETER_SEARCH_QUERY, this.searchQuery);
        operationParameters.put(PARAMETER_RETRIEVAL_SIZE, this.dataSetSize);
        operationParameters.put(PARAMETER_DATA_FILTER, this.dataFilter);

        operationParameters = T8MainServerClient.executeSynchronousOperation(context, SERVER_OPERATION_RETRIEVE_FILTER_DATA, T8IdentifierUtilities.prependNamespace(SERVER_OPERATION_RETRIEVE_FILTER_DATA, operationParameters));
        operationParameters = T8IdentifierUtilities.stripNamespace(operationParameters);

        rootNode = (ComboBoxModelRootNode) operationParameters.get(PARAMETER_DATA_MODEL_ROOT_NODE);

        return new ComboBoxListModel(rootNode);
    }

    @Override
    protected void done()
    {
        try
        {
            owner.showDataView(get());
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to retrieve combo box data", ex);
        }
    }

    public String getSearchQuery()
    {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery)
    {
        this.searchQuery = searchQuery;
    }

    void setDataSourceFilter(T8DataFilter dataFilter)
    {
        this.dataFilter = dataFilter;
    }
}
