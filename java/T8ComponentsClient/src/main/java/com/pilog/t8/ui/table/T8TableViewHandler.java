package com.pilog.t8.ui.table;

import com.pilog.t8.client.T8ClientUtilities;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition.AutoResizeMode;
import com.pilog.t8.ui.cellrenderer.T8TableCellRendererAdaptor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class T8TableViewHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8TableViewHandler.class);

    private final T8TableDefinition definition;
    private final T8Table t8Table;
    private final JTable table;
    private final JScrollPane scrollPane;
    private final T8RowHeaderTable rowHeaderTable;
    private final TableCellRenderer defaultRenderer;
    private final TableCellRenderer tableHeaderRenderer;
    private final T8TableCornerCellRenderer cornerCellRenderer;
    private final T8TableHeaderMouseListener headerMouseListener;
    private final T8TableCopyActionListener copyActionListener;
    private final Map<String, Boolean> columnVisibility;

    private final KeyStroke copyKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK, false);
    private final KeyStroke pasteKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK, false);

    private RendererDataLoader rendererDataLoader;

    public T8TableViewHandler(T8TableDefinition definition, T8Table t8Table, JScrollPane scrollPane, JTable table)
    {
        this.definition = definition;
        this.t8Table = t8Table;
        this.table = table;
        this.scrollPane = scrollPane;
        this.defaultRenderer = new T8TableDefaultCellRenderer(t8Table);
        this.tableHeaderRenderer = new T8TableColumnHeaderCellRenderer(t8Table);
        this.cornerCellRenderer = new T8TableCornerCellRenderer(t8Table, isCornerCellTickEnabled());
        this.rowHeaderTable = new T8RowHeaderTable(t8Table, table);
        this.headerMouseListener = new T8TableHeaderMouseListener();
        this.copyActionListener = new T8TableCopyActionListener();
        this.columnVisibility = new HashMap<>();
        setInitialColumnVisibility();
    }

    /**
     * The corner cell tick can only be enabled iff:<br>
     * <ol>
     * <li>Tick selections are enabled in general</li>
     * <li>Tick all option is enabled</li>
     * <li>A tick selection mode is used that allows multiple ticks</li>
     * </ol>
     *
     * @return {@code true} if the corner cell tick is enabled and should be
     *      made available
     */
    private boolean isCornerCellTickEnabled()
    {
        return definition.isTickSelectionEnabled()
                && definition.isTickAllEnabled()
                && definition.getTickSelectionMode() != T8TableDefinition.TickSelectionMode.SINGLE_SELECTION;
    }

    public JTable getRowHeaderTable()
    {
        return rowHeaderTable;
    }

    private void setInitialColumnVisibility()
    {
        ArrayList<T8TableColumnDefinition> columnDefinitions;

        columnDefinitions = definition.getColumnDefinitions();
        for (T8TableColumnDefinition columnDefinition : columnDefinitions)
        {
            columnVisibility.put(columnDefinition.getIdentifier(), columnDefinition.isVisible());
        }
    }

    public void configureTable()
    {
        AutoResizeMode autoResizeMode;

        // Set the row height of the table.
        table.setRowHeight(definition.getRowHeight());

        // Set the table's auto-resize mode.
        autoResizeMode = definition.getAutoResizeMode();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        if (autoResizeMode == AutoResizeMode.SUBSEQUENT_COLUMNS) table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        else if(autoResizeMode == AutoResizeMode.ALL_COLUMNS) table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        else if(autoResizeMode == AutoResizeMode.NEXT_COLUMN) table.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
        else if(autoResizeMode == AutoResizeMode.LAST_COLUMN) table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        // Configure all of the columns according to their definitions.
        configureColumnModel();

        // Configure the row header column.
        configureRowHeaderColumnModel();

        // Make sure to only have one header mouse listener added.
        table.getTableHeader().removeMouseListener(headerMouseListener);
        table.getTableHeader().addMouseListener(headerMouseListener);

        // Override copy & paste actions.
        table.registerKeyboardAction(copyActionListener, "Copy", copyKeyStroke, JComponent.WHEN_FOCUSED);
    }

    public void repaint()
    {
        rowHeaderTable.repaint();
        table.repaint();
    }

    public void terminateRendererDataLoader()
    {
        if(rendererDataLoader != null)
            rendererDataLoader.terminate();
    }

    public void setColumnVisibility(List<String> columnIdentifierList, boolean visible, boolean applyInverse)
    {
        ArrayList<T8TableColumnDefinition> columnDefinitions;

        // Set the visibility of the columns as specified.
        columnDefinitions = definition.getColumnDefinitions();
        for (T8TableColumnDefinition columnDefinition : columnDefinitions)
        {
            String columnIdentifier;

            columnIdentifier = columnDefinition.getIdentifier();
            if (columnIdentifierList.contains(columnIdentifier))
            {
                columnVisibility.put(columnIdentifier, visible);
            }
            else if (applyInverse)
            {
                columnVisibility.put(columnIdentifier, !visible);
            }
        }

        // Reconfigure the column model after visibility has been altered.
        configureColumnModel();
    }

    private void configureColumnModel()
    {
        ArrayList<T8TableColumnDefinition> columnDefinitions;
        TableColumnModel columnModel;
        int modelIndex;

        columnDefinitions = definition.getColumnDefinitions();

        // Remove any current columns.
        columnModel = table.getColumnModel();

        while (columnModel.getColumnCount() > 0)
        {
            columnModel.removeColumn(columnModel.getColumn(0));
        }

        // Create new columns from the table definition.
        modelIndex = 1; // The model index starts at 1, so that the first system column (row guid) is not displayed.
        for (T8TableColumnDefinition columnDefinition : columnDefinitions)
        {
            // Only add the column to the model if it is visible.
            if (columnVisibility.get(columnDefinition.getIdentifier()))
            {
                TableColumn newColumn;

                newColumn = new TableColumn(modelIndex++, columnDefinition.getWidth());
                newColumn.setHeaderValue(columnDefinition.getColumnName());
                newColumn.setIdentifier(columnDefinition.getIdentifier());
                newColumn.setCellRenderer(defaultRenderer);
                newColumn.setHeaderRenderer(tableHeaderRenderer);
                if(columnDefinition.getMaxWidth() != null) newColumn.setMaxWidth(columnDefinition.getMaxWidth());
                if(columnDefinition.getMinWidth() != null) newColumn.setMinWidth(columnDefinition.getMinWidth());
                table.addColumn(newColumn);
            }
            else modelIndex++;
        }
    }

    private void configureRowHeaderColumnModel()
    {
        TableColumnModel columnModel;
        TableColumn newColumn;

        // Remove any current columns.
        columnModel = rowHeaderTable.getColumnModel();
        while (columnModel.getColumnCount() > 0)
        {
            columnModel.removeColumn(columnModel.getColumn(0));
        }

        newColumn = new TableColumn(0, 25);
        newColumn.setHeaderValue(" ");
        rowHeaderTable.addColumn(newColumn);

        newColumn.setCellRenderer(new T8TableRowHeaderCellRenderer(t8Table.getTableDataChangeHandler(), definition.isTickSelectionEnabled()));
        newColumn.setCellEditor(new T8TableRowHeaderCellEditor(new T8TableRowHeaderCellRenderer(t8Table.getTableDataChangeHandler(), definition.isTickSelectionEnabled())));
    }

    public void setColumnRenderer(int columnIndex, TableCellRenderer renderer)
    {
        table.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
    }

    public void setColumnEditor(int columnIndex, TableCellEditor editor)
    {
        table.getColumnModel().getColumn(columnIndex).setCellEditor(editor);
    }

    public int convertModelToViewIndex(int modelIndex)
    {
        for (int columnIndex = 0; columnIndex < table.getColumnCount(); columnIndex++)
        {
            TableColumn column;

            column = table.getColumnModel().getColumn(columnIndex);
            if (column.getModelIndex() == modelIndex)
            {
                return columnIndex;
            }
        }

        return -1;
    }

    public int convertViewToModelIndex(int viewIndex)
    {
        if (viewIndex >= table.getColumnCount())
        {
            return -1;
        }
        else return table.getColumnModel().getColumn(viewIndex).getModelIndex();
    }

    public void setModel(final TableModel tableModel)
    {
        //First make sure to stop the render data loader thread first to stop processing as soon as possible
        terminateRendererDataLoader();

        Runnable updateModel = () ->
        {
            JTableHeader corner;
            Dimension d;

            // Set the new model on both the content table as well as the row header table.
            table.setModel(tableModel);
            rowHeaderTable.setModel(tableModel);

            // Auto-size the row header table so that all row numbers fit.
            T8ClientUtilities.autoSizeTableColumn(rowHeaderTable, 0, 10);

            d = rowHeaderTable.getPreferredScrollableViewportSize();
            d.width = rowHeaderTable.getPreferredSize().width;
            rowHeaderTable.setPreferredScrollableViewportSize(d);
            rowHeaderTable.setRowHeight(table.getRowHeight());

            scrollPane.setRowHeaderView(rowHeaderTable);

            corner = rowHeaderTable.getTableHeader();
            corner.setReorderingAllowed(false);
            corner.setResizingAllowed(false);
            corner.setDefaultRenderer(cornerCellRenderer);
            corner.addMouseListener(new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent event)
                {
                    t8Table.setAllRowsTicked(!t8Table.areAllRowsTicked());
                    t8Table.repaint();
                }
            });

            scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, corner);
        };

        if (SwingUtilities.isEventDispatchThread()) updateModel.run();
        else
        {
            try
            {
                SwingUtilities.invokeAndWait(updateModel);
            }
            catch (InterruptedException | InvocationTargetException ex)
            {
                LOGGER.log("Exception while setting new table model", ex);
            }
        }

        //Now start a new renderer data loader to load additional rendering data
        if(tableModel instanceof T8DefaultTableModel)
        {
            rendererDataLoader = new RendererDataLoader((T8DefaultTableModel) tableModel);
            rendererDataLoader.start();
        }
    }

    public void clearSelection()
    {
        table.getSelectionModel().clearSelection();
    }

    public void repaintRowHeaders()
    {
        rowHeaderTable.repaint();
    }

    private void doFocusedCellCopy()
    {
        int column;
        int row;

        // Get the focused column and row index.
        row = table.getSelectionModel().getLeadSelectionIndex();
        column = table.getColumnModel().getSelectionModel().getLeadSelectionIndex();
        if ((column != -1) && (row != -1))
        {
            Object value;
            String data;
            StringSelection selection;
            Clipboard clipboard;

            value = table.getValueAt(row, column);
            data = value == null ? "" : value.toString();

            selection = new StringSelection(data);
            clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(selection, selection);
        }
    }

    private class T8TableHeaderMouseListener implements MouseListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            int columnIndex;

            columnIndex = table.convertColumnIndexToModel(table.columnAtPoint(e.getPoint()));
            if (columnIndex > -1)
            {
                String columnIdentifier;

                columnIdentifier = table.getModel().getColumnName(columnIndex);
                t8Table.quickOrderOnField(columnIdentifier);
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    private class T8TableCopyActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            doFocusedCellCopy();
        }
    };

    private class RendererDataLoader extends Thread
    {
        private final T8DefaultTableModel tableModel;
        private boolean stopFlag = false;

        private RendererDataLoader(T8DefaultTableModel tableModel)
        {
            this.tableModel = tableModel;
        }

        @Override
        public void run()
        {
            int rowCount;
            int processedCount;

            rowCount = table.getModel().getRowCount();
            processedCount = 0;

            while(!stopFlag && rowCount > processedCount)
            {
                for (int i = 0; i < definition.getColumnDefinitions().size(); i++)
                {
                    T8TableColumnDefinition columnDefinition = definition.getColumnDefinitions().get(i);

                    if(columnDefinition.getRendererComponentDefinition() != null)
                    {
                        String fieldIdentifier;
                        TableCellRenderer renderAdaptor;
                        Map<String, Object> fieldValues;
                        T8TableCellRendererComponent rendererComponent;

                        fieldIdentifier = columnDefinition.getFieldIdentifier();
                        fieldValues = t8Table.getRowEntityFieldValues(processedCount);

                        if(columnDefinition.isVisible())
                        {
                            try
                            {
                                renderAdaptor = table.getColumn(columnDefinition.getIdentifier()).getCellRenderer();

                                if(renderAdaptor != null && (renderAdaptor instanceof T8TableCellRendererAdaptor))
                                {
                                    final AdditionalRenderData additionalRenderData;

                                    rendererComponent = ((T8TableCellRendererAdaptor)renderAdaptor).getRendererComponent();
                                    additionalRenderData = new AdditionalRenderData(processedCount, fieldIdentifier, rendererComponent.getAdditionalRenderData(fieldValues, fieldIdentifier));

                                    if(additionalRenderData.getData() != null)
                                    {
                                        SwingUtilities.invokeLater(() ->
                                        {
                                            tableModel.setAdditionalRenderData(additionalRenderData.getRow(), additionalRenderData.getFieldIdentifier(), additionalRenderData.getData());
                                        });
                                    }
                                }
                            }
                            catch (IllegalArgumentException e)
                            {
                                LOGGER.log("Could not find column " + columnDefinition.getIdentifier() + " on table.");
                            }
                        }
                    }
                }

                processedCount++;
            }
        }

        private void terminate()
        {
            stopFlag = true;
        }
    }

    private static class AdditionalRenderData
    {
        private final int row;
        private final String fieldIdentifier;
        private final Object data;

        private AdditionalRenderData(int row, String fieldIdentifier, Object data)
        {
            this.row = row;
            this.fieldIdentifier = fieldIdentifier;
            this.data = data;
        }

        public int getRow()
        {
            return row;
        }

        public String getFieldIdentifier()
        {
            return fieldIdentifier;
        }

        public Object getData()
        {
            return data;
        }
    }
}
