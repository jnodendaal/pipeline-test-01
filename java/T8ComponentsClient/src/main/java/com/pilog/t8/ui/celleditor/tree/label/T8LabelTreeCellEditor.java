package com.pilog.t8.ui.celleditor.tree.label;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.tree.T8TreeCellEditableComponent;
import com.pilog.t8.ui.tree.T8TreeCellEditorComponent;
import com.pilog.t8.ui.tree.T8TreeNode;
import com.pilog.t8.ui.tree.T8TreeNodeEditorEvent;
import com.pilog.t8.ui.tree.T8TreeNodeEditorListener;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.celleditor.tree.label.T8LabelTreeCellEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.celleditor.tree.paging.PageSelectionWindow;
import com.pilog.t8.ui.cellrenderer.tree.label.T8LabelTreeCellRenderer;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public class T8LabelTreeCellEditor extends JPanel implements AncestorListener, T8TreeCellEditorComponent
{
    private final T8LabelTreeCellEditorDefinition definition;
    private final T8LabelTreeCellRenderer renderer;
    private final T8ComponentController controller;
    private final T8ConfigurationManager configurationManager;
    private final T8Context context;
    private final EventListenerList listeners;
    private T8TreeNode editorNode;
    private PageSelectionWindow pageSelectionWindow;
    private int currentPage;
    private int pageCount;

    public T8LabelTreeCellEditor(T8LabelTreeCellEditorDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.renderer = new T8LabelTreeCellRenderer(definition, controller);
        this.renderer.setPagingDetailsVisible(false);
        this.listeners = new EventListenerList();
        this.setOpaque(false);
        initComponents();
        jPanelRenderer.add(renderer, java.awt.BorderLayout.CENTER);
        addAncestorListener(this);
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }

    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String string, Map<String, Object> map)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> map)
    {
        renderer.initializeComponent(map);
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
        renderer.startComponent();
    }

    @Override
    public void stopComponent()
    {
        renderer.stopComponent();
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public boolean singleClickEdit()
    {
        return true;
    }

    @Override
    public void addNodeEditorListener(T8TreeNodeEditorListener editorListener)
    {
        listeners.add(T8TreeNodeEditorListener.class, editorListener);
    }

    @Override
    public void removeNodeEditorListener(T8TreeNodeEditorListener editorListener)
    {
        listeners.remove(T8TreeNodeEditorListener.class, editorListener);
    }

    @Override
    public void setEditorNode(T8TreeCellEditableComponent editableComponent, T8TreeNode node, boolean selected, boolean focused, boolean editable, boolean expanded, boolean leaf, int row, int currentPage, int pageCount)
    {
        //Allow the user to page (again)
        this.enablePagingControls();

        this.currentPage = currentPage;
        this.pageCount = pageCount;
        this.editorNode = node;
        this.jLabelPageNumber.setText("" + (currentPage + 1));
        this.jLabelPageCount.setText("" + pageCount);
        this.jToolBarPagingControls.setVisible(pageCount > 1); // Only show the paging details if necessary.
        this.jToolBarFilterControls.setVisible((node.getUserDefinedChildNodeFilters() != null) && (node.getUserDefinedChildNodeFilters().size() > 0));
        this.renderer.setRendererNode(editableComponent, node, selected, focused, editable, expanded, leaf, row, currentPage, pageCount);
        this.renderer.revalidate();
        this.validate();
    }

    @Override
    public T8TreeNode getEditorNode()
    {
        // This editor does not change any of the input data, so just return the orginally supplied collection.
        return editorNode;
    }

    private void firePageDecrementEvent()
    {
        T8TreeNodeEditorEvent event;

        event = new T8TreeNodeEditorEvent(editorNode);
        for (T8TreeNodeEditorListener listener : listeners.getListeners(T8TreeNodeEditorListener.class))
        {
            listener.pageDecremented(event);
        }
    }

    private void firePageIncrementEvent()
    {
        //Prevent the user from paging again
        disablePagingControls();

        T8TreeNodeEditorEvent event;

        event = new T8TreeNodeEditorEvent(editorNode);
        for (T8TreeNodeEditorListener listener : listeners.getListeners(T8TreeNodeEditorListener.class))
        {
            listener.pageIncremented(event);
        }
    }

    private void fireFiltersRemovedEvent()
    {
        //Prevent the user from paging again
        disablePagingControls();

        T8TreeNodeEditorEvent event;

        event = new T8TreeNodeEditorEvent(editorNode);
        for (T8TreeNodeEditorListener listener : listeners.getListeners(T8TreeNodeEditorListener.class))
        {
            listener.filtersRemoved(event);
        }
    }

    private void fireGoToPageEvent(int pageNumber)
    {
        //Prevent the user from paging again
        disablePagingControls();

        T8TreeNodeEditorEvent event;

        event = new T8TreeNodeEditorEvent(editorNode);
        for (T8TreeNodeEditorListener listener : listeners.getListeners(T8TreeNodeEditorListener.class))
        {
            listener.goToPage(event, pageNumber);
        }
    }

    private void showGotoPageDialog()
    {
        if(pageSelectionWindow == null)
        {
            pageSelectionWindow = new PageSelectionWindow(getFrame(null), this);
            pageSelectionWindow.addWindowFocusListener(new WindowAdapter()
                            {
                                @Override
                                public void windowLostFocus(WindowEvent evt)
                                {
                                    pageSelectionWindow.setVisible(false);
                                    fireGoToPageEvent(pageSelectionWindow.getSelectedValue()-1);
                                }
                            });
        }
        pageSelectionWindow.setMaximum(pageCount);
        pageSelectionWindow.setSelectedValue(currentPage);
        Point pt = this.getLocationOnScreen();
        pt.translate(0, this.getHeight());
        pageSelectionWindow.setLocation(pt);
        pageSelectionWindow.toFront();
        pageSelectionWindow.setVisible(true);
        pageSelectionWindow.requestFocusInWindow();
        pageSelectionWindow.invalidate();
    }

    protected Frame getFrame(Component comp)
    {
        if (comp == null)
        {

            comp = this;
        }
        if (comp.getParent() instanceof Frame)
        {
            return (Frame) comp.getParent();
        }
        return getFrame(comp.getParent());
    }

    private void disablePagingControls()
    {
        jButtonNextPage.setEnabled(false);
        jButtonPreviousPage.setEnabled(false);
        jButtonRemoveFilters.setEnabled(false);
        jXButtonGoToPage.setEnabled(false);
    }

    private void enablePagingControls()
    {
        jButtonNextPage.setEnabled(true);
        jButtonPreviousPage.setEnabled(true);
        jButtonRemoveFilters.setEnabled(true);
        jXButtonGoToPage.setEnabled(true);
    }

    @Override
    public void ancestorAdded(AncestorEvent event)
    {
        hidePopup();
    }

    @Override
    public void ancestorRemoved(AncestorEvent event)
    {
        hidePopup();
    }

    @Override
    public void ancestorMoved(AncestorEvent event)
    {
        if (event.getSource() != pageSelectionWindow)
        {
            hidePopup();
        }
    }

    public void hidePopup()
    {
        if (pageSelectionWindow != null && pageSelectionWindow.isVisible())
        {
            pageSelectionWindow.setVisible(false);
        }
    }

    private String getTranslation(String text)
    {
        return getController().getClientContext().getConfigurationManager().getUITranslation(context, text);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelRenderer = new javax.swing.JPanel();
        jToolBarPagingControls = new javax.swing.JToolBar();
        jButtonPreviousPage = new javax.swing.JButton();
        jLabelPage = new javax.swing.JLabel();
        jLabelPageNumber = new javax.swing.JLabel();
        jLabelPageSlash = new javax.swing.JLabel();
        jLabelPageCount = new javax.swing.JLabel();
        jButtonNextPage = new javax.swing.JButton();
        jXButtonGoToPage = new org.jdesktop.swingx.JXButton();
        jToolBarFilterControls = new javax.swing.JToolBar();
        jButtonRemoveFilters = new javax.swing.JButton();

        setMaximumSize(null);
        setMinimumSize(null);
        setName(""); // NOI18N
        setOpaque(false);
        setPreferredSize(null);
        setLayout(new java.awt.GridBagLayout());

        jPanelRenderer.setMinimumSize(null);
        jPanelRenderer.setName(""); // NOI18N
        jPanelRenderer.setOpaque(false);
        jPanelRenderer.setPreferredSize(null);
        jPanelRenderer.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        add(jPanelRenderer, gridBagConstraints);

        jToolBarPagingControls.setFloatable(false);
        jToolBarPagingControls.setRollover(true);
        jToolBarPagingControls.setOpaque(false);
        jToolBarPagingControls.setPreferredSize(new java.awt.Dimension(114, 20));

        jButtonPreviousPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pagePreviousIcon.png"))); // NOI18N
        jButtonPreviousPage.setToolTipText(translate("Previous Page"));
        jButtonPreviousPage.setFocusable(false);
        jButtonPreviousPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonPreviousPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonPreviousPage.setOpaque(false);
        jButtonPreviousPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonPreviousPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonPreviousPageActionPerformed(evt);
            }
        });
        jToolBarPagingControls.add(jButtonPreviousPage);

        jLabelPage.setText(translate("Page:"));
        jToolBarPagingControls.add(jLabelPage);
        jToolBarPagingControls.add(jLabelPageNumber);

        jLabelPageSlash.setText("/");
        jToolBarPagingControls.add(jLabelPageSlash);
        jToolBarPagingControls.add(jLabelPageCount);

        jButtonNextPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/pageNextIcon.png"))); // NOI18N
        jButtonNextPage.setToolTipText(translate("Next Page"));
        jButtonNextPage.setFocusable(false);
        jButtonNextPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonNextPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonNextPage.setOpaque(false);
        jButtonNextPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonNextPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonNextPageActionPerformed(evt);
            }
        });
        jToolBarPagingControls.add(jButtonNextPage);

        jXButtonGoToPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/control-cursor.png"))); // NOI18N
        jXButtonGoToPage.setToolTipText(translate("Go To Page"));
        jXButtonGoToPage.setFocusable(false);
        jXButtonGoToPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jXButtonGoToPage.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jXButtonGoToPage.setName(""); // NOI18N
        jXButtonGoToPage.setOpaque(false);
        jXButtonGoToPage.setPaintBorderInsets(false);
        jXButtonGoToPage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonGoToPageActionPerformed(evt);
            }
        });
        jToolBarPagingControls.add(jXButtonGoToPage);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jToolBarPagingControls, gridBagConstraints);

        jToolBarFilterControls.setFloatable(false);
        jToolBarFilterControls.setRollover(true);
        jToolBarFilterControls.setOpaque(false);

        jButtonRemoveFilters.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/filterMinusIcon.png"))); // NOI18N
        jButtonRemoveFilters.setToolTipText(translate("Remove Filters"));
        jButtonRemoveFilters.setFocusable(false);
        jButtonRemoveFilters.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRemoveFilters.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonRemoveFilters.setMaximumSize(null);
        jButtonRemoveFilters.setMinimumSize(null);
        jButtonRemoveFilters.setOpaque(false);
        jButtonRemoveFilters.setPreferredSize(null);
        jButtonRemoveFilters.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRemoveFilters.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRemoveFiltersActionPerformed(evt);
            }
        });
        jToolBarFilterControls.add(jButtonRemoveFilters);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        add(jToolBarFilterControls, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonPreviousPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonPreviousPageActionPerformed
    {//GEN-HEADEREND:event_jButtonPreviousPageActionPerformed
        firePageDecrementEvent();
    }//GEN-LAST:event_jButtonPreviousPageActionPerformed

    private void jButtonNextPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonNextPageActionPerformed
    {//GEN-HEADEREND:event_jButtonNextPageActionPerformed
        firePageIncrementEvent();
    }//GEN-LAST:event_jButtonNextPageActionPerformed

    private void jButtonRemoveFiltersActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRemoveFiltersActionPerformed
    {//GEN-HEADEREND:event_jButtonRemoveFiltersActionPerformed
        fireFiltersRemovedEvent();
    }//GEN-LAST:event_jButtonRemoveFiltersActionPerformed

    private void jXButtonGoToPageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonGoToPageActionPerformed
    {//GEN-HEADEREND:event_jXButtonGoToPageActionPerformed
        showGotoPageDialog();
    }//GEN-LAST:event_jXButtonGoToPageActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonNextPage;
    private javax.swing.JButton jButtonPreviousPage;
    private javax.swing.JButton jButtonRemoveFilters;
    private javax.swing.JLabel jLabelPage;
    private javax.swing.JLabel jLabelPageCount;
    private javax.swing.JLabel jLabelPageNumber;
    private javax.swing.JLabel jLabelPageSlash;
    private javax.swing.JPanel jPanelRenderer;
    private javax.swing.JToolBar jToolBarFilterControls;
    private javax.swing.JToolBar jToolBarPagingControls;
    private org.jdesktop.swingx.JXButton jXButtonGoToPage;
    // End of variables declaration//GEN-END:variables
}
