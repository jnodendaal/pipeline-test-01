package com.pilog.t8.ui.toolbar;

import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.toolbar.T8ToolBarDefinition;
import com.pilog.t8.definition.ui.toolbar.T8ToolBarDefinition.FlowAlignment;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.swing.JToolBar;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8ToolBar extends JToolBar implements T8Component
{
    private final T8ToolBarDefinition definition;
    private final T8ComponentController controller;
    private final List<ToolBarComponent> childComponents;
    private Painter backgroundPainter;

    public T8ToolBar(T8ToolBarDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.childComponents = new ArrayList<>();
        this.setFloatable(false);
        this.setOpaque(definition.isOpaque());
        setupLayout();
        setupPainters();
    }

    private void setupLayout()
    {
        FlowLayout layout;
        FlowAlignment alignment;

        alignment = definition.getAlignment();

        layout = new FlowLayout();
        switch (alignment)
        {
            case LEFT:
                layout.setAlignment(FlowLayout.LEFT);
                break;
            case RIGHT:
                layout.setAlignment(FlowLayout.RIGHT);
                break;
            default:
                layout.setAlignment(FlowLayout.CENTER);
        }

        layout.setHgap(definition.getHorizontalInset());
        layout.setVgap(definition.getVerticalInset());
        setLayout(layout);
    }

    private void setupPainters()
    {
        T8PainterDefinition backgroundPainterDefinition;
        T8LookAndFeelManager lafManager;
        T8Painter painter;

        // Get the look and feel manager.
        lafManager = controller.getClientContext().getConfigurationManager().getLookAndFeelManager(controller.getContext());
        backgroundPainterDefinition = definition.getBackgroundPainterDefinition();
        if (backgroundPainterDefinition != null)
        {
            backgroundPainter = new T8PainterAdapter(backgroundPainterDefinition.getNewPainterInstance());
        }
        else
        {
            painter = lafManager.getToolBarBackgroundPainter();
            if (painter != null) backgroundPainter = new T8PainterAdapter(painter);
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        ToolBarComponent toolBarComponent;

        // Create the component wrapper
        toolBarComponent = new ToolBarComponent(component);

        // Add the child component.
        childComponents.add(toolBarComponent);
        add(toolBarComponent.getComponent());
        add(toolBarComponent.getSeparator());

        // If we have more than 1 component, check the separators and visibility
        if (this.childComponents.size() > 0) checkSeparators();
    }

    private void checkSeparators()
    {
        ToolBarComponent nextVisibleComponent;
        ToolBarComponent toolBarComponent;

        nextVisibleComponent = getNextVisibleComponent(0);
        toolBarComponent = null;
        while (nextVisibleComponent != null)
        {
            // Set the previous component's separator visible
            if (toolBarComponent != null) toolBarComponent.setSeparatorVisible(true);

            toolBarComponent = nextVisibleComponent;
            nextVisibleComponent = getNextVisibleComponent(this.childComponents.indexOf(toolBarComponent)+1);
        }
    }

    private ToolBarComponent getNextVisibleComponent(int startIdx)
    {
        ToolBarComponent childComponent;

        for (int compIdx = startIdx; compIdx < this.childComponents.size(); compIdx++)
        {
            if ((childComponent = this.childComponents.get(compIdx)).isComponentVisible())
            {
                return childComponent;
            } else childComponent.setSeparatorVisible(false);
        }

        return null;
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        for (ToolBarComponent childComponent : childComponents)
        {
            if (childComponent.isComponent(identifier))
            {
                return childComponent.getT8Component();
            }
        }
        return null;
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        for (ToolBarComponent childComponent : childComponents)
        {
            if (childComponent.isComponent(identifier))
            {
                this.remove(childComponent.getComponent());
                return childComponent.getT8Component();
            }
        }

        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return this.childComponents.stream().map(ToolBarComponent::getT8Component).collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
        childComponents.clear();
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        Graphics2D g2;

        g2 = (Graphics2D)g;
        if (isOpaque())
        {
            super.paintComponent(g2);
        }

        if (backgroundPainter != null)
        {
            backgroundPainter.paint(g2, this, getWidth(), getHeight());
        }
    }

    private class ToolBarComponent
    {
        private final String componentIdentifier;
        private final Component childComponent;
        private final T8Component component;
        private final Separator separator;

        private ToolBarComponent(T8Component component)
        {
            this.childComponent = (Component)component;
            this.separator = new Separator();
            this.component = component;

            this.componentIdentifier = component.getComponentDefinition().getIdentifier();

            this.separator.setVisible(false);
            this.childComponent.addComponentListener(new VisibilityListener());
        }

        private boolean isComponentVisible()
        {
            return this.childComponent.isVisible();
        }

        private void setSeparatorVisible(boolean visible)
        {
            this.separator.setVisible(visible);
        }

        private Separator getSeparator()
        {
            return this.separator;
        }

        private boolean isComponent(String identifier)
        {
            return this.componentIdentifier.equals(identifier);
        }

        private Component getComponent()
        {
            return this.childComponent;
        }

        private T8Component getT8Component()
        {
            return this.component;
        }

        @Override
        public String toString()
        {
            return this.componentIdentifier;
        }
    }

    private class VisibilityListener extends ComponentAdapter
    {
        private VisibilityListener() {}

        @Override
        public void componentHidden(ComponentEvent e)
        {
            checkSeparators();
        }

        @Override
        public void componentShown(ComponentEvent e)
        {
            checkSeparators();
        }
    }
}
