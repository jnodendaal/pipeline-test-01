package com.pilog.t8.ui.tabbedpane;

import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.gfx.T8GraphicsUtilities;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import org.jdesktop.swingx.painter.AbstractLayoutPainter;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.painter.Painter;
import org.jdesktop.swingx.painter.TextPainter;

import static com.pilog.t8.gfx.T8GraphicsUtilities.createTopToBottomLinearGradient;
import static com.pilog.t8.ui.laf.LAFConstants.CONTENT_HEADER_TEXT_COLOR;

/**
 * @author bouwe_000
 */
public class T8TabbedPaneUI extends BasicTabbedPaneUI
{
    private final static int HEIGHT = 25;
    private final static int TAB_PADDING_X = 10;
    private final TextPainter textPainter;
    private Painter backgroundPainter;
    private Painter backgroundSelectedPainter;

    private final Painter DEFAULT_BACKGROUND_PAINTER = new MattePainter(T8GraphicsUtilities.createTopToBottomLinearGradient(LAFConstants.CONTENT_HEADER_HIGH_COLOR, LAFConstants.CONTENT_HEADER_LOW_COLOR), true);
    private final Painter DEFAULT_BACKGROUND_SELECTED_PAINTER = new MattePainter(createTopToBottomLinearGradient(LAFConstants.CONTENT_HEADER_SELECTED_HIGH_COLOR, LAFConstants.CONTENT_HEADER_SELECTED_LOW_COLOR), true);

    public T8TabbedPaneUI()
    {
        backgroundPainter = DEFAULT_BACKGROUND_PAINTER;
        backgroundSelectedPainter = DEFAULT_BACKGROUND_SELECTED_PAINTER;
        textPainter = new TextPainter(null, LAFConstants.CONTENT_HEADER_FONT, LAFConstants.CONTENT_HEADER_TEXT_COLOR);
        textPainter.setHorizontalAlignment(AbstractLayoutPainter.HorizontalAlignment.LEFT);
        textPainter.setInsets(new Insets(0, 0, 0, 0));
        tabAreaInsets = new Insets(0, 0, 0, 0);
        contentBorderInsets = new Insets(0, 0, 0, 0);
    }

    public void setBackgroundPainter(T8Painter painter)
    {
        if (painter != null)
        {
            backgroundPainter = new T8PainterAdapter(painter);
        }
        else
        {
            backgroundPainter = DEFAULT_BACKGROUND_PAINTER;
        }
    }

    public void setBackgroundSelectedPainter(T8Painter painter)
    {
        if (painter != null)
        {
            backgroundSelectedPainter = new T8PainterAdapter(painter);
        }
        else
        {
            backgroundSelectedPainter = DEFAULT_BACKGROUND_SELECTED_PAINTER;
        }
    }

    @Override
    protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex)
    {
        Graphics2D g2;

        g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setColor(LAFConstants.CONTENT_HEADER_BORDER_COLOR);
        g2.drawRect(0, 0, tabPane.getWidth(), tabPane.getHeight());
    }

    @Override
    protected void paintTabArea(Graphics g, int tabPlacement, int selectedIndex)
    {
        Graphics2D g2;

        g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        backgroundPainter.paint(g2, tabPane, tabPane.getWidth(), HEIGHT);
        g2.setColor(LAFConstants.CONTENT_HEADER_BORDER_COLOR);
        g2.drawRect(0, 0, tabPane.getWidth()-1, tabPane.getHeight()-1);
        super.paintTabArea(g, tabPlacement, selectedIndex);
    }

    @Override
    protected void paintTab(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect)
    {
        Rectangle tabRect;
        int selectedIndex;
        boolean isSelected;

        tabRect = rects[tabIndex];
        selectedIndex = tabPane.getSelectedIndex();
        isSelected = selectedIndex == tabIndex;
        paintTabBackground(g, tabPlacement, tabIndex, tabRect.x, tabRect.y, tabRect.width, tabRect.height, isSelected);
        super.paintTab(g, tabPlacement, rects, tabIndex, iconRect, textRect);
    }

    @Override
    protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected)
    {
        Graphics2D g2;

        g2 = (Graphics2D)g.create(x, y, w, h);
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        if (isSelected)
        {
            backgroundSelectedPainter.paint(g2, null, w, h);
        }
        else
        {
            backgroundPainter.paint(g2, null, w, h);
        }

        g2.dispose();
    }

    @Override
    protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics, int tabIndex, String title, Rectangle textRect, boolean isSelected)
    {
        Graphics2D g2;

        g2 = (Graphics2D)g.create(textRect.x, textRect.y, textRect.width, textRect.height);
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        textPainter.setText(title);
        textPainter.setFillPaint(tabPane.isEnabledAt(tabIndex) ? CONTENT_HEADER_TEXT_COLOR : CONTENT_HEADER_TEXT_COLOR.brighter().brighter());
        textPainter.paint(g2, tabPane, textRect.width, textRect.height);
        g2.dispose();
    }

    @Override
    protected FontMetrics getFontMetrics()
    {
        return tabPane.getFontMetrics(LAFConstants.CONTENT_HEADER_FONT);
    }

    @Override
    protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics)
    {
        return super.calculateTabWidth(tabPlacement, tabIndex, metrics) + TAB_PADDING_X;
    }

    @Override
    protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight)
    {
        return HEIGHT;
    }

    @Override
    protected Insets getTabAreaInsets(int tabPlacement)
    {
        return new Insets(0, 0, 0, 0);
    }

    @Override
    protected int getTabLabelShiftX(int tabPlacement, int tabIndex, boolean isSelected)
    {
        return 0;
    }

    @Override
    protected int getTabLabelShiftY(int tabPlacement, int tabIndex, boolean isSelected)
    {
        return 0;
    }

    @Override
    protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected)
    {
        Graphics2D g2;

        g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setColor(LAFConstants.CONTENT_HEADER_BORDER_COLOR);
        g2.drawRect(x, y, w-1, h-1);
    }

    @Override
    protected int getTabRunOverlay(int tabPlacement)
    {
        return 0;
    }

    @Override
    protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected)
    {
        super.paintFocusIndicator(g, tabPlacement, rects, tabIndex, iconRect, textRect, isSelected);
    }

    @Override
    protected Insets getTabInsets(int tabPlacement, int tabIndex)
    {
        return new Insets(0, 0, 0, 0);
    }

    @Override
    protected Insets getSelectedTabPadInsets(int tabPlacement)
    {
        return new Insets(0, 0, 0, 0);
    }

    @Override
    protected Insets getContentBorderInsets(int tabPlacement)
    {
        return new Insets(0, 1, 1, 1);
    }
}
