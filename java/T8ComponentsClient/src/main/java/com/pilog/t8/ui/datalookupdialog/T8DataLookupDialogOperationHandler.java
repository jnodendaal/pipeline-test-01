package com.pilog.t8.ui.datalookupdialog;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.datalookupdialog.T8DataLookupDialogAPIHandler;
import com.pilog.t8.ui.dialog.T8DialogOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupDialogOperationHandler extends T8DialogOperationHandler
{
    private final T8DataLookupDialog lookupDialog;

    public T8DataLookupDialogOperationHandler(T8DataLookupDialog dialog)
    {
        super(dialog);
        this.lookupDialog = dialog;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8DataLookupDialogAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8DataLookupDialogAPIHandler.PARAMETER_DATA_ENTITY, lookupDialog.getSelectedDataEntity());
        }
        else if (operationIdentifier.equals(T8DataLookupDialogAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITIES))
        {
            return HashMaps.newHashMap(T8DataLookupDialogAPIHandler.PARAMETER_DATA_ENTITY_LIST, lookupDialog.getSelectedDataEntities());
        }
        else if (operationIdentifier.equals(T8DataLookupDialogAPIHandler.OPERATION_SET_PREFILTER))
        {
            T8DataFilter dataFilter;
            Boolean refresh;
            String filterIdentifier;

            dataFilter = (T8DataFilter)operationParameters.get(T8DataLookupDialogAPIHandler.PARAMETER_DATA_FILTER);
            refresh = (Boolean)operationParameters.get(T8DataLookupDialogAPIHandler.PARAMETER_REFRESH_DATA);
            filterIdentifier = (String)operationParameters.get(T8DataLookupDialogAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
            if (refresh == null) refresh = true;

            lookupDialog.setPrefilter(filterIdentifier, dataFilter);
            if (refresh) lookupDialog.refreshData();
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
