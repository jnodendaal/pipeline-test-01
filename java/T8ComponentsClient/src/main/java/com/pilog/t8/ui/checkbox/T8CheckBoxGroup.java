/**
 * Created on 02 Sep 2015, 9:48:17 AM
 */
package com.pilog.t8.ui.checkbox;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.checkbox.T8CheckBoxGroupDefinition;
import com.pilog.t8.ui.button.group.T8ButtonGroup;

/**
 * @author Gavin Boshoff
 */
//TODO: GBO - This and the T8RadioButtonGroup should extend from the same abstract class
public class T8CheckBoxGroup extends T8ButtonGroup<T8CheckBox>
{
    private static final long serialVersionUID = 7735175430396636859L;

    public T8CheckBoxGroup(T8CheckBoxGroupDefinition buttonGroupDefinition, T8ComponentController controller)
    {
        super(buttonGroupDefinition, controller);
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }
}