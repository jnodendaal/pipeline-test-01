package com.pilog.t8.ui.table;

import static com.pilog.t8.definition.ui.table.T8TableAPIHandler.*;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TableOperationHandler extends T8ComponentOperationHandler
{
    private final T8Table table;

    public T8TableOperationHandler(T8Table table)
    {
        super(table);
        this.table = table;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(OPERATION_GET_DATA_OBJECT))
        {
            String objectIdentifier;

            objectIdentifier = (String)operationParameters.get(PARAMETER_DATA_OBJECT_IDENTIFIER);
            try
            {
                return HashMaps.createSingular(PARAMETER_DATA_OBJECT, table.getDataObject(objectIdentifier));
            }
            catch (Exception e)
            {
                T8Log.log("Exception while fetching data object from table: " + objectIdentifier, e);
                return null;
            }
        }
        else if (operationIdentifier.equals(OPERATION_SET_KEY))
        {
            Map<String, Object> keyMap;
            Boolean refresh;

            keyMap = (Map<String, Object>)operationParameters.get(PARAMETER_KEY_MAP);
            refresh = (Boolean)operationParameters.get(PARAMETER_REFRESH_DATA);
            if (refresh == null) refresh = true;

            table.setKey(keyMap);
            if (refresh) table.refreshData();
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_GET_KEY))
        {
            return HashMaps.createSingular(PARAMETER_KEY_MAP, table.getKey());
        }
        else if (operationIdentifier.equals(OPERATION_PREFILTER_BY_KEY))
        {
            Map<String, Object> filterValues;
            Boolean refresh;

            filterValues = (Map<String, Object>)operationParameters.get(PARAMETER_KEY_MAP);
            refresh = (Boolean)operationParameters.get(PARAMETER_REFRESH_DATA);
            if (refresh == null) refresh = true;

            if (filterValues != null)
            {
                table.setPrefilter(null, new T8DataFilter(table.getComponentDefinition().getDataEntityIdentifier(), filterValues));
                if (refresh) table.refreshData();
                return null;
            }
            else
            {
                table.setPrefilter(null, null);
                if (refresh) table.refreshData();
                return null;
            }
        }
        else if (operationIdentifier.equals(OPERATION_GET_COMBINED_FILTER))
        {
            return HashMaps.createSingular(PARAMETER_DATA_FILTER, table.getCombinedFilter());
        }
        else if (operationIdentifier.equals(OPERATION_CLEAR_USER_DEFINED_FILTER))
        {
            this.table.clearUserDefinedFilter(Boolean.TRUE.equals(operationParameters.get(PARAMETER_REFRESH_DATA)));
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_GET_USER_DEFINED_FILTER))
        {
            return HashMaps.createSingular(PARAMETER_DATA_FILTER, table.getUserDefinedFilter());
        }
        else if (operationIdentifier.equals(OPERATION_GET_PREFILTER))
        {
            String filterIdentifier;

            filterIdentifier = (String)operationParameters.get(PARAMETER_DATA_FILTER_IDENTIFIER);
            return HashMaps.createSingular(PARAMETER_DATA_FILTER, table.getPrefilter(filterIdentifier));
        }
        else if (operationIdentifier.equals(OPERATION_SET_PREFILTER))
        {
            T8DataFilter dataFilter;
            Boolean refresh;
            String filterIdentifier;

            dataFilter = (T8DataFilter)operationParameters.get(PARAMETER_DATA_FILTER);
            refresh = (Boolean)operationParameters.get(PARAMETER_REFRESH_DATA);
            filterIdentifier = (String)operationParameters.get(PARAMETER_DATA_FILTER_IDENTIFIER);
            if (refresh == null) refresh = true;

            table.setPrefilter(filterIdentifier, dataFilter);
            if (refresh) table.refreshData();
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_CLEAR_PREFILTERS))
        {
            this.table.clearPrefilters(Boolean.TRUE.equals(operationParameters.get(PARAMETER_REFRESH_DATA)));
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_SET_COLUMN_VISIBILITY))
        {
            List<String> columnIdentifierList;
            Boolean applyInverse;
            Boolean visible;

            visible = (Boolean)operationParameters.get(PARAMETER_VISIBLE);
            applyInverse = (Boolean)operationParameters.get(PARAMETER_APPLY_INVERSE);
            columnIdentifierList = (List<String>)operationParameters.get(PARAMETER_COLUMN_IDENTIFIER_LIST);

            if (visible == null) applyInverse = false;
            if (applyInverse == null) applyInverse = false;
            if (columnIdentifierList == null) columnIdentifierList = new ArrayList<>();

            table.setColumnVisibility(columnIdentifierList, true, true);
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_GET_DATA_ENTITIES))
        {
            return HashMaps.createSingular(PARAMETER_DATA_ENTITY_LIST, table.getDataEntities());
        }
        else if (operationIdentifier.equals(OPERATION_GET_SELECTED_DATA_ENTITIES))
        {
            return HashMaps.createSingular(PARAMETER_DATA_ENTITY_LIST, table.getSelectedDataEntities());
        }
        else if (operationIdentifier.equals(OPERATION_GET_TICKED_DATA_ENTITIES))
        {
            return HashMaps.createSingular(PARAMETER_DATA_ENTITY_LIST, table.getTickedDataEntities());
        }
        else if (operationIdentifier.equals(OPERATION_GET_SELECTED_DATA_ROWS))
        {
            return HashMaps.createSingular(PARAMETER_DATA_ROWS, table.getSelectedDataRows());
        }
        else if (operationIdentifier.equals(OPERATION_GET_FIRST_SELECTED_DATA_ROW))
        {
            List<Map<String, Object>> selectedRows;

            selectedRows = table.getSelectedDataRows();
            if ((selectedRows != null) && (selectedRows.size() > 0))
            {
                return HashMaps.createSingular(PARAMETER_DATA_ROW, selectedRows.get(0));
            }
            else return null;
        }
        else if (operationIdentifier.equals(OPERATION_CLEAR_SELECTION))
        {
            table.clearSelection();
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_CLEAR_DATA))
        {
            table.clearData();
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_REFRESH))
        {
            Boolean checkOutdated;

            checkOutdated = (Boolean)operationParameters.get(PARAMETER_CHECK_OUTDATED);
            if ((checkOutdated != null) && (checkOutdated))
            {
                // The flag is set, so only refresh the table if it is outdated.
                if (table.isOutdated())
                {
                    table.refreshData();
                    return null;
                }
                else return null;
            }
            else
            {
                table.refreshData();
                return null;
            }
        }
        else if (operationIdentifier.equals(OPERATION_SAVE_DATA_CHANGES))
        {
            return HashMaps.createSingular(PARAMETER_SUCCESS, table.commitTableChanges());
        }
        else if (operationIdentifier.equals(OPERATION_SET_OUTDATED))
        {
            Boolean outdated;

            outdated = (Boolean)operationParameters.get(PARAMETER_OUTDATED);
            table.setOutdated(outdated != null ? outdated : true);
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_IS_OUTDATED))
        {
            return HashMaps.createSingular(PARAMETER_OUTDATED, table.isOutdated());
        }
        else if (operationIdentifier.equals(OPERATION_HAS_DATA_CHANGES))
        {
            return HashMaps.createSingular(PARAMETER_HAS_DATA_CHANGES, table.getTableDataChangeHandler().hasTableChanges());
        }
        else if (operationIdentifier.equals(OPERATION_GET_DATA_CHANGES))
        {
            HashMap<String, Object> operationOutputParameters;

            operationOutputParameters = new HashMap<>();
            table.stopCellEditing();

            operationOutputParameters.put(PARAMETER_INSERTED_ENTITY_LIST, table.getTableDataChangeHandler().getInsertedEntitiesList());
            operationOutputParameters.put(PARAMETER_DELETED_ENTITY_LIST, table.getTableDataChangeHandler().getDeletedEntitiesList());
            operationOutputParameters.put(PARAMETER_UPDATED_OLD_ENTITY_LIST, table.getTableDataChangeHandler().getUpdatedOldEntitiesList());
            operationOutputParameters.put(PARAMETER_UPDATED_NEW_ENTITY_LIST, table.getTableDataChangeHandler().getUpdatedNewEntitiesList());

            return operationOutputParameters;
        }
        else if (operationIdentifier.equals(OPERATION_GET_DATA_ENTITY_COUNT))
        {
            return HashMaps.createSingular(PARAMETER_DATA_ENTITY_COUNT, table.getDataEntityCount());
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
