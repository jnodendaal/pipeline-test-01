package com.pilog.t8.ui.panel;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelDefinition.BorderType;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.ui.laf.LAFConstants;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.synth.SynthLookAndFeel;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.ScrollableSizeHint;
import org.jdesktop.swingx.border.DropShadowBorder;

/**
 * @author Bouwer du Preez
 */
public class T8Panel extends JXTitledPanel implements T8Component
{
    private static final T8Logger logger = T8Log.getLogger(T8Panel.class);

    protected T8PanelDefinition definition;
    protected HashMap<String, T8Component> childComponents;
    protected T8ComponentController controller;
    protected T8PanelOperationHandler operationHandler;
    protected JXPanel contentPanel;
    private T8LookAndFeelManager lafManager;
    private T8ContentHeaderBorder contentHeaderBorder;
    private ActionListener borderActionListener;

    public T8Panel(T8PanelDefinition definition, T8ComponentController controller)
    {
        setTitleFont(LAFConstants.MAIN_HEADER_FONT);
        setTitleForeground(LAFConstants.MAIN_HEADER_TEXT_COLOR);

        // From SwingX 1.6.5 it has become necessary to set the content container.  This has to be done before the rest of the components are initialized.
        this.contentPanel = new JXPanel();
        this.contentPanel.setScrollableHeightHint(ScrollableSizeHint.PREFERRED_STRETCH);
        this.contentPanel.setScrollableWidthHint(ScrollableSizeHint.PREFERRED_STRETCH);
        this.contentPanel.setOpaque(false); // The content panel is always non-opaque to allow the background painter to shine through.
        this.contentPanel.setLayout(new GridBagLayout());
        this.setContentContainer(contentPanel);
        this.setOpaque(definition.isOpaque());
        this.setScrollableHeightHint(ScrollableSizeHint.PREFERRED_STRETCH);
        this.setScrollableWidthHint(ScrollableSizeHint.PREFERRED_STRETCH);
        // Initialize the rest of the components on the panel.
        this.definition = definition;
        this.controller = controller;
        this.childComponents = new HashMap<>();
        this.operationHandler = new T8PanelOperationHandler(this);
        this.setPaintBorderInsets(false);

        setupPanel();
    }

    private void setupPanel()
    {
        T8PainterDefinition backgroundPainterDefinition;

        backgroundPainterDefinition = definition.getBackgroundPainterDefinition();
        if (backgroundPainterDefinition != null)
        {
            this.setBackgroundPainter(new T8PainterAdapter(backgroundPainterDefinition.getNewPainterInstance()));
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        BorderType borderType;

        // Get the L&F Manager.
        lafManager = controller.getClientContext().getConfigurationManager().getLookAndFeelManager(controller.getContext());

        borderType = definition.getBorderType();
        if (borderType == BorderType.BEVEL)
        {
            setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        }
        else if (borderType == BorderType.ETCHED)
        {
            setBorder(javax.swing.BorderFactory.createEtchedBorder());
        }
        else if (borderType == BorderType.LINE)
        {
            setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        }
        else if ((borderType == BorderType.TITLED) && (!definition.isHeaderVisible()))
        {
            setBorder(javax.swing.BorderFactory.createTitledBorder(definition.getTitle()));
        }
        else if ((borderType == BorderType.CONTENT) && (!definition.isHeaderVisible()))
        {
            T8ContentHeaderBorder border;

            border = new T8ContentHeaderBorder(definition.getTitle());
            border.setBackgroundPainter(lafManager.getContentHeaderBackgroundPainter());
            setBorder(border);
        }
        else if (borderType == BorderType.SHADOW)
        {
            setBorder(new DropShadowBorder());
        }
        else
        {
            setBorder(new EmptyBorder(0, 0, 0, 0));
        }

        // Hide the header if it is not required.
        if (!definition.isHeaderVisible())
        {
            this.getUI().getTitleBar().setVisible(false);
        }
        else
        {
            if (inputParameters != null) addHeaderHelpButton(inputParameters);
            this.setTitle(definition.getTitle());
        }
    }

    private void addHeaderHelpButton(Map<String, Object> inputParameters)
    {
        final String helpID;
        JXButton helpButton;

        logger.log(T8Logger.Level.DEBUG, "Looking for Header Help link ID using : " + this.definition.getNamespace() + "$P_HELP_ID");

        helpID = (String)inputParameters.get(this.definition.getNamespace() + "$P_HELP_ID");
        if (helpID != null)
        {
            helpButton = new JXButton(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/question-frame.png")));
            helpButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    controller.getClientContext().dispayHelp(helpID);
                }
            });

            //Change the look of the button
            helpButton.setBorderPainted(false);
            helpButton.setBorder(null);
            helpButton.setMargin(new Insets(0, 0, 0, 0));
            helpButton.setContentAreaFilled(false);
            helpButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            //Set the button next to the title
            setRightDecoration(helpButton);
        }
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        if (childComponent instanceof T8PanelSlot)
        {
            T8PanelSlotDefinition slotDefinition;
            T8PanelSlot panelSlot;
            GridBagConstraints componentConstraints;
            String componentIdentifier;
            Dimension minimumDimensions;
            Dimension preferredDimensions;

            panelSlot = (T8PanelSlot)childComponent;
            slotDefinition = (T8PanelSlotDefinition)panelSlot.getComponentDefinition();
            componentIdentifier = childComponent.getComponentDefinition().getIdentifier();
            componentConstraints = slotDefinition.getGridBagConstraints();
            minimumDimensions = slotDefinition.getMinimumDimensions();
            preferredDimensions = slotDefinition.getPreferredDimensions();

            // If still no layout constraints were found, use the default settings.
            if (componentConstraints == null)
            {
                componentConstraints = new GridBagConstraints();
                componentConstraints.weightx = 0.1;
                componentConstraints.weighty = 0.1;
                componentConstraints.fill = GridBagConstraints.BOTH;
            }

            childComponents.put(componentIdentifier, childComponent);
            if ((minimumDimensions != null) && (minimumDimensions.height > 0) && (minimumDimensions.width > 0))
            {
                ((Component)childComponent).setMinimumSize(minimumDimensions);
            }
            else
            {
                ((Component)childComponent).setMinimumSize(null);
            }

            if ((preferredDimensions != null) && (preferredDimensions.height > 0) && (preferredDimensions.width > 0))
            {
                ((Component)childComponent).setPreferredSize(preferredDimensions);
            }
            else
            {
                ((Component)childComponent).setPreferredSize(null);
            }

            getContentContainer().add((Component)childComponent, componentConstraints);
            validate();
        }
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return childComponents.get(childComponentIdentifier);
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        if (childComponents.containsKey(childComponentIdentifier))
        {
            T8Component childComponent;

            childComponent = childComponents.get(childComponentIdentifier);
            getContentContainer().remove((Component)childComponent);
            childComponents.remove(childComponentIdentifier);
            return childComponent;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(childComponents.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
        getContentContainer().removeAll();
        childComponents.clear();
    }

    public void setHeaderText(String headerText)
    {
        // Hide the header if it is not required.
        if (!definition.isHeaderVisible())
        {
            this.getUI().getTitleBar().setVisible(false);
        }
        else
        {
            this.setTitle(headerText);
        }

        switch (definition.getBorderType())
        {
            case TITLED:
                setBorder(javax.swing.BorderFactory.createTitledBorder(headerText));
                break;
            case CONTENT:
                contentHeaderBorder = new T8ContentHeaderBorder(this, headerText);
                contentHeaderBorder.setCollapsible(definition.isCollapsible());
                borderActionListener = new BorderActionListener();
                contentHeaderBorder.addActionListener(borderActionListener);
                setBorder(contentHeaderBorder);
                break;
        }
    }

    /**
     * SwingX screwed up the translation of the graphics context when border
     * insets are required, so this method overrides their default
     * implementation in JXPanel to fix the issue.  The method has been copied
     * exactly from JXPanel with a small change added to the background painter
     * usage.
     * @param g Graphics context for paint operation.
     */
    @Override
    protected void paintComponent(Graphics g)
    {
        if (isPatch())
        {
            paintComponentPatch(g);
            return;
        }
        else
        {
            Graphics2D g2 = (Graphics2D) g.create();

            try
            {
                // we should be painting the background behind the painter if we have one
                // this prevents issues with buffer reuse where visual artifacts sneak in
                if (isOpaque() || UIManager.getLookAndFeel() instanceof SynthLookAndFeel)
                {
                    //this will paint the foreground if a JXPanel subclass is
                    //unfortunate enough to have one
                    super.paintComponent(g2);
                }
                else
                {
                    if (getAlpha() < 1f)
                    {
                        g.setColor(getBackground());
                        g.fillRect(0, 0, getWidth(), getHeight());
                    }
                }

                // Apply the background painter if one is set.
                if (getBackgroundPainter() != null)
                {
                    if (isPaintBorderInsets())
                    {
                        getBackgroundPainter().paint(g2, this, getWidth(), getHeight());
                    } else
                    {
                        Insets insets = getInsets();
                        g2.translate(insets.left, insets.top);
                        getBackgroundPainter().paint(g2, this, getWidth() - insets.left - insets.right, getHeight() - insets.top - insets.bottom);
                        g2.translate(-insets.left, -insets.top);
                    }
                }

                //force the foreground to paint again...workaround for folks that
                //incorrectly extend JXPanel instead of JComponent
                getUI().paint(g2, this);
            }
            finally
            {
                g2.dispose();
            }
        }
    }

    public void setPanelSlotHeaderText(String panelSlotIdentifier, String headerText)
    {
        T8PanelSlot panelSlot;

        panelSlot = (T8PanelSlot) getChildComponent(panelSlotIdentifier);
        // Hide the header if it is not required.
        panelSlot.setTitle(headerText);
    }

    private void toggleCollapsedState()
    {
        if (definition.isCollapsible())
        {
            contentPanel.setVisible(!contentPanel.isVisible());
            contentHeaderBorder.setCollapsed(!contentPanel.isVisible());
        }
    }

    private class BorderActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            toggleCollapsedState();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
