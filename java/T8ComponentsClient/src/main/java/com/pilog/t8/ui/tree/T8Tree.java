package com.pilog.t8.ui.tree;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.tree.T8TreeNode.NodeType;
import com.pilog.t8.definition.ui.entitymenu.T8DataEntityMenuItemDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeAPIHandler;
import com.pilog.t8.definition.ui.tree.T8TreeDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeNodeDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.componentcontainer.T8DefaultComponentContainer;
import com.pilog.t8.ui.entitymenu.T8DataEntityMenuItem;
import com.pilog.t8.ui.tree.T8TreeDataHandler.ChildNodePage;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class T8Tree extends javax.swing.JPanel implements T8Component, T8TreeCellEditableComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8Tree.class);

    private final T8TreeDefinition definition;
    private final T8ConfigurationManager configurationManager;
    private final T8ComponentController controller;
    private T8TreeDataHandler dataHandler;
    private T8TreeOperationHandler operationHandler;
    private T8TreeCellRendererAdaptor rendererAdaptor;
    private T8TreeCellEditorAdaptor editorAdaptor;
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8DefaultComponentContainer treeContainer;
    private JTree jTreeData;
    private boolean expansionTipShown = false;
    final private List<NodeLoader> nodeLoaderList;
    final private List<TreeExpansionController> treeExpansionControllers;

    public static final Border UNSELECTED_BORDER = new EmptyBorder(1, 1, 1, 1);
    public static final Border SELECTED_BORDER = new LineBorder(Color.BLUE, 1);
    public static final Border FOCUS_BORDER = UIManager.getBorder("Table.focusCellHighlightBorder");
    public static final Border FOCUS_SELECTED_BORDER = new CompoundBorder(SELECTED_BORDER, FOCUS_BORDER);

    public T8Tree(T8TreeDefinition definition, T8ComponentController controller)
    {
        this.controller = controller;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.nodeLoaderList = Collections.synchronizedList(new ArrayList<NodeLoader>());
        this.treeExpansionControllers = Collections.synchronizedList(new ArrayList<TreeExpansionController>());

        // Components have to be initialized after the configuration manager is set otherwise NPE will be caused by all translation attemps.
        initComponents();

        this.treeContainer = new T8DefaultComponentContainer();
        this.add(treeContainer, java.awt.BorderLayout.CENTER);
        this.treeContainer.setComponent(jPanelDataView);
        this.definition = definition;
    }

    public JTree getTree()
    {
        return jTreeData;
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8TreeDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.dataHandler = new T8TreeDataHandler(context, definition);
        this.operationHandler = new T8TreeOperationHandler(this);
        this.rendererAdaptor = new T8TreeCellRendererAdaptor(this, definition);
        this.editorAdaptor = new T8TreeCellEditorAdaptor(this, definition, new T8TreeNodeListener());

        // Initialize the tree.
        this.jTreeData = new JTree();
        this.jTreeData.setModel(null);
        this.jTreeData.setOpaque(false);
        this.jScrollPaneTree.getViewport().setOpaque(false);
        this.jScrollPaneTree.setViewportView(jTreeData);
        this.jTreeData.setCellRenderer(rendererAdaptor);
        this.jTreeData.setCellEditor(editorAdaptor);
        this.jTreeData.setRowHeight(0); // Force tree to get row height from renderer.
        this.jTreeData.setEditable(true);

        // Set the visibility of the tool bar.
        this.jButtonExpandAll.setVisible(definition.allowExpandAll());
        jToolBarMain.setVisible(definition.isToolBarVisible());
        setToolTipText(definition.getTooltipText());

        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "CANCEL_NODE_EXPANSION");
        this.getActionMap().put("CANCEL_NODE_EXPANSION", new AbstractAction("CANCEL_NODE_EXPANSION")
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                synchronized (treeExpansionControllers)
                {
                    for (TreeExpansionController treeExpansionController : treeExpansionControllers)
                    {
                        treeExpansionController.stopExpansion();
                    }
                }
            }
        });
    }

    @Override
    public void startComponent()
    {
        rebuildTree();
        jTreeData.addTreeExpansionListener(new T8TreeExpansionListener());
        jTreeData.addMouseListener(new T8TreeMouseListener());
        jTreeData.addTreeSelectionListener(new T8TreeSelectionListener());
    }

    @Override
    public void stopComponent()
    {
        // We gotta stop the animation timers or else they will keep running on the EDT.
        rendererAdaptor.stopAnimation();
        editorAdaptor.stopAnimation();
        treeContainer.unlock();
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object layoutConstraints)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public boolean containsNode(T8TreeNode node)
    {
        T8TreeNode rootNode;

        rootNode = getRootNode();
        if (rootNode == null) return false;
        if (rootNode == node) return true;
        else return getDescendantNodes(rootNode).contains(node);
    }

    public boolean areNodesBeingLoaded()
    {
        return nodeLoaderList.size() > 0;
    }

    public List<T8TreeNode> getNodes(String nodeIdentifier)
    {
        List<T8TreeNode> nodes;
        Iterator<T8TreeNode> nodeIterator;

        nodes = getAllNodes();
        nodeIterator = nodes.iterator();
        while (nodeIterator.hasNext())
        {
            if (!Objects.equals(nodeIterator.next().getIdentifier(), nodeIdentifier))
            {
                nodeIterator.remove();
            }
        }

        return nodes;
    }

    public List<T8TreeNode> getAllNodes()
    {
        List<T8TreeNode> allNodes;
        T8TreeNode rootNode;

        allNodes = new ArrayList<>();
        rootNode = getRootNode();
        if (rootNode != null)
        {
            allNodes.add(rootNode);
            allNodes.addAll(rootNode.getDescendantNodes());
        }

        return allNodes;
    }

    public void rebuildSelectedNode()
    {
        T8TreeNode selectedNode;

        selectedNode = getSelectedNode();
        if (selectedNode != null) loadNodes(selectedNode);
    }

    public void rebuildSelectedNodeParent()
    {
        T8TreeNode selectedNode;

        selectedNode = getSelectedNode();
        if (selectedNode != null)
        {
            T8TreeNode parentNode;

            parentNode = (T8TreeNode)selectedNode.getParent();
            if (parentNode != null) loadNodes(parentNode);
        }
    }

    /**
     * Rebuilds all nodes that have one or more child nodes of the specified
     * type.
     * @param nodeIdentifier The type of node for which parents will be found
     * and rebuilt.
     */
    public void rebuildNodeParents(String nodeIdentifier)
    {
        Set<T8TreeNode> parentNodes;

        // Create a set of all affected parent nodes.
        parentNodes = new HashSet<>();
        for (T8TreeNode childNode : getNodes(nodeIdentifier))
        {
            T8TreeNode parentNode;

            parentNode = (T8TreeNode)childNode.getParent();
            if (parentNode != null)
            {
                parentNodes.add(parentNode);
            }
            else // Ok so this node is the root of the tree and we only need to rebuild this one node.
            {
                parentNodes.clear();
                parentNodes.add(parentNode);
                break;
            }
        }

        // Rebuild all parent nodes.
        for (T8TreeNode parentNode : parentNodes)
        {
            loadNodes(parentNode);
        }
    }

    public void rebuildTree()
    {
        try
        {
            List<T8TreeNode> rootNodes;

            // Load the child nodes and add them to the parent.
            rootNodes = dataHandler.loadNodes(controller.getContext(), definition.getRootNodeDefinition(), null);
            if (rootNodes.size() > 1)
            {
                T8TreeNode rootGroupNode;

                // Create a fake root node, so that all the proper root nodes can be added to it.
                rootGroupNode = new T8TreeNode(definition.getRootNodeDefinition().getGroupDisplayName(), 0);
                for (T8TreeNode rootNode : rootNodes)
                {
                    rootGroupNode.add(rootNode);
                }

                // Set the new model.
                jTreeData.setModel(new DefaultTreeModel(rootGroupNode));
            }
            else if (rootNodes.size() == 1)
            {
                T8TreeNode rootNode;

                rootNode = rootNodes.get(0);

                // Set the new model.
                jTreeData.setModel(new DefaultTreeModel(rootNode));

                // Load the first level children.
                loadNodes(rootNode);
            }
            else jTreeData.setModel(null);

            // Auto select a node, if required
            autoSelectFirstDataNode();
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while loading tree root nodes using definition:" + definition.getRootNodeDefinition(), e);
        }
    }

    /**
     * If the definition specifies for auto selection of a node, the following
     * conditions are applied:<br/>
     * <ol>
     * <li>The tree model cannot be {@code null}.</li>
     * <li>The tree root node cannot be {@code null}.</li>
     * <li>There should be no existing selection of a tree node.</li>
     * <li>Only the root node and its immediate children are taken into
     * consideration.</li>
     * <li>The first data node will be selected.</li>
     * </ol>
     */
    private void autoSelectFirstDataNode()
    {
        if (definition.isAutoSelectOnRetrieve() && jTreeData.getModel() != null)
        {
            LOGGER.log(() -> "Setting Selected Node...");
            T8TreeNode rootNode;

            rootNode = getRootNode();
            // We only want to set a selection if there is none
            if (rootNode != null && getSelectedNode() == null)
            {
                // We want to select the first data node on the tree, but we don't
                // want to traverse the entire tree, so we only check the root node
                // and its direct children
                if (rootNode.getNodeType() == NodeType.DATA)
                {
                    jTreeData.setSelectionRow(0);
                }
                else
                {
                    for (int childIdx = 0; childIdx < rootNode.getChildCount(); childIdx++)
                    {
                        if (((T8TreeNode)rootNode.getChildAt(childIdx)).getNodeType() == NodeType.DATA)
                        {
                            // The root node children will have an index less than the tree
                            jTreeData.setSelectionRow(childIdx+1);
                            break;
                        }
                    }
                }
            }
        }
    }

    public List<T8TreeNode> getDescendantNodes(T8TreeNode startNode)
    {
        List<T8TreeNode> nodes;

        // Add all of the descendants of the start node.
        nodes = new ArrayList<>();
        for (int childIndex = 0; childIndex < startNode.getChildCount(); childIndex++)
        {
            T8TreeNode childNode;

            childNode = (T8TreeNode)startNode.getChildAt(childIndex);
            nodes.add(childNode);
            nodes.addAll(getDescendantNodes(childNode));
        }

        return nodes;
    }

    public T8TreeNode getRootNode()
    {
        TreeModel model;

        model = jTreeData.getModel();
        return model != null ? (T8TreeNode)model.getRoot() : null;
    }

    public T8TreeNode getSelectedNode()
    {
        TreePath selectedPath;

        selectedPath = jTreeData.getSelectionPath();
        if (selectedPath != null)
        {
            return (T8TreeNode)selectedPath.getLastPathComponent();
        }
        else return null;
    }

    public List<T8TreeNode> getSelectedTreeNodes()
    {
        List<T8TreeNode> treeNodes;
        TreePath[] selectedPaths;

        treeNodes = new ArrayList<>();
        selectedPaths = jTreeData.getSelectionPaths();
        if (selectedPaths != null)
        {
            for (TreePath selectedPath : jTreeData.getSelectionPaths())
            {
                treeNodes.add((T8TreeNode)selectedPath.getLastPathComponent());
            }
        }

        return treeNodes;
    }

    public List<Map<String, Object>> getSelectedDataRows()
    {
        return null;
    }

    public List<T8DataEntity> getSelectedDataEntities()
    {
        List<T8TreeNode> selectedNodes;
        List<T8DataEntity> selectedEntities;

        selectedNodes = getSelectedTreeNodes();
        selectedEntities = new ArrayList<>();
        for (T8TreeNode selectedNode : selectedNodes)
        {
            selectedEntities.add(selectedNode.getDataEntity());
        }

        return selectedEntities;
    }

    @Override
    public Font getCellFont()
    {
        return jTreeData.getFont();
    }

    @Override
    public Color getSelectedCellBackgroundColor()
    {
        return jTreeData.getBackground();
    }

    @Override
    public Color getSelectedCellForegroundColor()
    {
        return jTreeData.getForeground();
    }

    @Override
    public Color getUnselectedCellBackgroundColor()
    {
        return jTreeData.getBackground();
    }

    @Override
    public Color getUnselectedCellForegroundColor()
    {
        return jTreeData.getForeground();
    }

    @Override
    public Border getSelectedBorder()
    {
        return SELECTED_BORDER;
    }

    @Override
    public Border getUnselectedBorder()
    {
        return UNSELECTED_BORDER;
    }

    @Override
    public Border getFocusedBorder()
    {
        return FOCUS_BORDER;
    }

    @Override
    public Border getFocusedSelectedBorder()
    {
        return FOCUS_SELECTED_BORDER;
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    /**
     * Returns the closes T8TreeNode to the location specified.
     *
     * @param location Any location on the tree.
     * @return The nearest T8TreeNode to the specified location.
     */
    public T8TreeNode getNodeAtLocation(Point location)
    {
        TreePath path;

        path = jTreeData.getPathForLocation(location.x, location.y);
        if (path != null)
        {
            return (T8TreeNode)path.getLastPathComponent();
        }
        else return null;
    }

    private void showQuickSearchPopupMenu(Point location, T8TreeNode selectedNode, String childNodeIdentifier)
    {
        String searchText;

        searchText = T8TreeQuickSearchDialog.getSearchText(controller.getClientContext().getParentWindow(), location, translate("Quick Search"));
        if (searchText != null)
        {
            T8TreeNodeDefinition nodeDefinition;
            T8TreeNodeDefinition childNodeDefinition;
            T8DataFilter quickSearchFilter;

            // Get the selected node definition and the definition of the child node that will be filtered.
            nodeDefinition = definition.getNodeDefinition(selectedNode.getIdentifier());
            childNodeDefinition = nodeDefinition.getChildNodeDefinition(childNodeIdentifier);
            quickSearchFilter = childNodeDefinition.getQuickSearchFilterDefinition().getNewDataFilterInstance(context, null);
            setQuickSearchTextInFilter(quickSearchFilter, searchText);
            selectedNode.setUserDefinedChildNodeFilter(childNodeIdentifier, quickSearchFilter);
            selectedNode.setChildNodePageOffset(0);

            // Refresh the selected node's children.
            loadNodes(selectedNode);
        }
    }

    public void setNodePrefilter(String nodeIdentifier, String filterIdentifier, T8DataFilter filter)
    {
        dataHandler.setNodePrefilter(nodeIdentifier, filterIdentifier, filter);
    }

    private void setQuickSearchTextInFilter(T8DataFilter filter, String text)
    {
        T8DataFilterCriteria filterCriteria;

        filterCriteria = filter.getFilterCriteria();
        for (T8DataFilterClause filterClause : filterCriteria.getFilterClauses().keySet())
        {
            if (filterClause instanceof T8DataFilterCriterion)
            {
                T8DataFilterCriterion filterCriterion;

                filterCriterion = (T8DataFilterCriterion)filterClause;
                if (filterCriterion.getFilterValue() == null)
                {
                    filterCriterion.setFilterValue(text);
                }
            }
        }
    }

    private void showTreePopupMenu(final Point location)
    {
        final T8TreeNode selectedNode;

        selectedNode = getNodeAtLocation(location);
        if (selectedNode != null)
        {
            T8TreeNodeDefinition nodeDefinition;
            JPopupMenu popupMenu;
            JMenuItem menuItem;
            JMenu menu;

            // Get the node definition.
            nodeDefinition = definition.getNodeDefinition(selectedNode.getIdentifier());

            // Create the new popup menu.
            popupMenu = new JPopupMenu();

            // Add all the menu items defined for the node definition.
            for (T8DataEntityMenuItemDefinition menuItemDefinition : nodeDefinition.getMenuItemDefinitions())
            {
                T8DataEntityMenuItem entityMenuItem;

                entityMenuItem = (T8DataEntityMenuItem)menuItemDefinition.getNewComponentInstance(controller);
                entityMenuItem.initializeComponent(null);
                entityMenuItem.setDataEntity(selectedNode.getDataEntity());
                popupMenu.add(entityMenuItem);
            }

            // Create action listener for popup selection events.
            ActionListener menuListener = new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    String optionText;

                    optionText = ((JMenuItem)event.getSource()).getName();
                    if (optionText.startsWith("QUICK_SEARCH_"))
                    {
                        String childNodeIdentifier;
                        Point quickSearchLocation;

                        quickSearchLocation = new Point(location);
                        SwingUtilities.convertPointToScreen(quickSearchLocation, jTreeData);
                        childNodeIdentifier = optionText.substring(13);
                        showQuickSearchPopupMenu(quickSearchLocation, selectedNode, childNodeIdentifier);
                    } else if (optionText.equals("REFRESH"))
                    {
                        loadNodes(selectedNode);
                    } else if (optionText.equals("EXPAND_DESCENDANTS"))
                    {
                        expandNodeDescendants(selectedNode, true);
                    }
                    else if (optionText.equals("COLLAPSE_DESCENDANTS"))
                    {
                        collapseNodeDescendants(selectedNode);
                    }
                }
            };

            // Create the search menu.
            menu = new JMenu(translate("Quick Search"));
            for (T8TreeNodeDefinition childNodeDefinition : nodeDefinition.getChildNodeDefinitions())
            {
                // We only add the quick search option if a quick search filter is defined.
                if (childNodeDefinition.getQuickSearchFilterDefinition() != null)
                {
                    menuItem = new JMenuItem(childNodeDefinition.getGroupDisplayName());
                    menuItem.setName("QUICK_SEARCH_" + childNodeDefinition.getIdentifier());
                    menuItem.addActionListener(menuListener);
                    menu.add(menuItem);
                }
            }

            if (menu.getMenuComponentCount() > 0) popupMenu.add(menu);

            // Create the refresh menu.
            menuItem = new JMenuItem(translate("Refresh"));
            menuItem.setName("REFRESH");
            menuItem.addActionListener(menuListener);
            popupMenu.add(menuItem);

            //Create the expand descendants menu
            if(definition.allowExpandAll())
            {
                menuItem = new JMenuItem(translate("Expand Descendants"));
                menuItem.setName("EXPAND_DESCENDANTS");
                menuItem.addActionListener(menuListener);
                popupMenu.add(menuItem);
            }

            //Create the collapse descendants menu
            if(definition.allowExpandAll())
            {
                menuItem = new JMenuItem(translate("Collapse Descendants"));
                menuItem.setName("COLLAPSE_DESCENDANTS");
                menuItem.addActionListener(menuListener);
                popupMenu.add(menuItem);
            }

            // Show the popup menu.
            popupMenu.show(jTreeData, location.x, location.y);
        }
        else
        {
            jTreeData.clearSelection();
        }
    }

    void collapseAll()
    {
        TreeExpansionController treeExpansionController = new TreeExpansionController(getRootNode(), true, false);
        treeExpansionControllers.add(treeExpansionController);
        treeExpansionController.start();
    }

    void expandAll()
    {
        if(!expansionTipShown)
        {
            expansionTipShown = true;
            Toast.show(translate("Tip: You can cancel the expansion by pressing the 'ESC' key."));
        }
        TreeExpansionController treeExpansionController = new TreeExpansionController(getRootNode(), true, true);
        treeExpansionControllers.add(treeExpansionController);
        treeExpansionController.start();
    }

    void expandNodeDescendants(T8TreeNode node, boolean loadChildren)
    {
        if(!expansionTipShown)
        {
            expansionTipShown = true;
            Toast.show(translate("Tip: You can cancel the expansion by pressing the 'ESC' key."));
        }
        TreeExpansionController treeExpansionController = new TreeExpansionController(node, loadChildren, true);
        treeExpansionControllers.add(treeExpansionController);
        treeExpansionController.start();
    }

    void collapseNodeDescendants(T8TreeNode node)
    {
        TreeExpansionController treeExpansionController = new TreeExpansionController(node, false, false);
        treeExpansionControllers.add(treeExpansionController);
        treeExpansionController.start();
    }

    private void loadNodes(T8TreeNode parentNode)
    {
        NodeLoader newLoader;

        newLoader = new NodeLoader(parentNode);
        nodeLoaderList.add(newLoader);
        newLoader.execute();
    }

    private class NodeLoader extends SwingWorker<Void, Void>
    {
        private final MutableTreeNode dummyNode;
        private final T8TreeNode parentNode;
        private boolean treeInEditState;
        private TreePath editingPath;

        private NodeLoader(T8TreeNode parentNode)
        {
            this.parentNode = parentNode;
            this.dummyNode = new T8TreeNode(null, 0);
        }

        public T8TreeNode getParentNode()
        {
            return this.parentNode;
        }

        @Override
        protected Void doInBackground() throws Exception
        {
            T8TreeNodeDefinition parentNodeDefinition;

            // Start the animation of the renderers.
            rendererAdaptor.startAnimation();
            editorAdaptor.startAnimation();

            // Save the editing state of the tree.
            treeInEditState = jTreeData.isEditing();
            editingPath = jTreeData.getEditingPath();

            // Remove all existing nodes from the parent and then add the new ones.
            parentNode.removeAllChildren();
            parentNode.add(dummyNode);

            // Get the parent node definition.
            parentNodeDefinition = definition.getNodeDefinition(parentNode.getIdentifier());

            //Refresh the tree to show the dummy loading node
            SwingUtilities.invokeAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    // Refresh the parent node's descendents.
                    ((DefaultTreeModel)jTreeData.getModel()).nodeStructureChanged(parentNode);
                }
            });

            // Load all possible child nodes.
            try
            {
                List<T8TreeNode> childNodes;
                ChildNodePage nodePage;

                // Load the child nodes and add them to the parent.
                if (parentNode.getNodeType() == NodeType.GROUP)
                {
                    T8TreeNode groupParent;
                    T8TreeNodeDefinition groupParentNodeDefinition;

                    groupParent = (T8TreeNode)parentNode.getParent();
                    if (groupParent != null)
                    {
                        groupParentNodeDefinition = definition.getNodeDefinition(groupParent.getIdentifier());
                        nodePage = dataHandler.loadNodes(controller.getContext(), groupParentNodeDefinition, groupParent.getDataEntity(), parentNode.getIdentifier(), parentNode.getUserDefinedChildNodeFilters(), parentNode.getChildNodePageOffset(), groupParentNodeDefinition.getPageSize());
                        setLoadedNodes(nodePage.getNodeList(), nodePage.getNodeCount());
                    }
                    else //The group node was added as a dummy since multiple actual root nodes exist.
                    {
                        childNodes = dataHandler.loadNodes(controller.getContext(), definition.getRootNodeDefinition(), null);
                        setLoadedNodes(childNodes, childNodes.size());
                    }
                }
                else
                {
                    nodePage = dataHandler.loadNodes(controller.getContext(), parentNodeDefinition, parentNode.getDataEntity(), null, parentNode.getUserDefinedChildNodeFilters(), parentNode.getChildNodePageOffset(), parentNode.getChildNodePageSize());
                    setLoadedNodes(nodePage.getNodeList(), nodePage.getNodeCount());
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading child nodes into node: " + parentNodeDefinition, e);
            }

            // Remove this loader from the list of active loaders and deactivate renderer animation if we no longer have any active loaders.
            synchronized (nodeLoaderList)
            {
                nodeLoaderList.remove(this);
                if (nodeLoaderList.isEmpty())
                {
                    // This part is very important.  If we do not deactivate the animation, the renderer will force the tree to be redrawn unnecessarily.
                    rendererAdaptor.stopAnimation();
                    editorAdaptor.stopAnimation();
                }
                nodeLoaderList.notifyAll();
            }

            // Return.
            return null;
        }

        private void setLoadedNodes(List<T8TreeNode> childNodes, int childNodeCount)
        {
            // Remove all existing nodes from the parent and then add the new ones.
            parentNode.removeAllChildren();
            for (T8TreeNode childNode : childNodes)
            {
                parentNode.add(childNode);
            }

            // Set the child node count on the parent node.
            parentNode.setChildNodeCount(childNodeCount);

            // Set the completed flag.
            parentNode.setChildrenLoaded(true);
        }

        @Override
        protected void done()
        {
            // Refresh the parent node's descendents.
            ((DefaultTreeModel)jTreeData.getModel()).nodeStructureChanged(parentNode);

            // Expand the node, showing its newly created children.
            jTreeData.expandPath(new TreePath(parentNode.getPath()));

            // If the tree was being edited before the children were loaded, resume the editing process.
            if (treeInEditState)
            {
                T8TreeNode editedNode;

                editedNode = (T8TreeNode)editingPath.getLastPathComponent();
                if (containsNode(editedNode))
                {
                    jTreeData.startEditingAtPath(editingPath);
                }
            }

            // Refresh the parent node.
            ((DefaultTreeModel)jTreeData.getModel()).nodeChanged(parentNode);
        }
    }

    private class T8TreeMouseListener implements MouseListener
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                showTreePopupMenu(e.getPoint());
            } else if(e.getClickCount() > 0 && getSelectedNode() != null)
            {
                HashMap<String, Object> eventParameters;
                String identifier;
                T8TreeNode selectedNode;
                T8DataEntity selectedEntity;

                selectedNode = getSelectedNode();
                selectedEntity = selectedNode.getDataEntity();
                identifier = selectedNode.getIdentifier();

                // Create the event parameters collection and fire the event.
                eventParameters = new HashMap<>();
                eventParameters.put(T8TreeAPIHandler.PARAMETER_TREE_NODE_IDENTIFIER, identifier);
                eventParameters.put(T8TreeAPIHandler.PARAMETER_TREE_NODE, selectedNode);
                eventParameters.put(T8TreeAPIHandler.PARAMETER_DATA_ENTITY, selectedEntity);
                if(e.getClickCount() == 1)
                    controller.reportEvent(T8Tree.this, definition.getComponentEventDefinition(T8TreeAPIHandler.EVENT_TREE_NODE_CLICKED), eventParameters);
                else if(e.getClickCount() == 2)
                    controller.reportEvent(T8Tree.this, definition.getComponentEventDefinition(T8TreeAPIHandler.EVENT_TREE_NODE_DOUBLE_CLICKED), eventParameters);
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                showTreePopupMenu(e.getPoint());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                showTreePopupMenu(e.getPoint());
            }
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    private class T8TreeExpansionListener implements TreeExpansionListener
    {
        @Override
        public void treeExpanded(TreeExpansionEvent event)
        {
            TreePath expandPath;
            T8TreeNode expandNode;

            expandPath = event.getPath();
            expandNode = (T8TreeNode)expandPath.getLastPathComponent();
            if ((expandNode != null) && (!expandNode.areChildrenLoaded()))
            {
                loadNodes(expandNode);
            }
        }

        @Override
        public void treeCollapsed(TreeExpansionEvent event)
        {
        }
    }

    private class T8TreeSelectionListener implements TreeSelectionListener
    {
        @Override
        public void valueChanged(TreeSelectionEvent e)
        {
            HashMap<String, Object> eventParameters;
            List<T8TreeNode> selectedNodes;
            List<T8DataEntity> selectedEntities;
            List<String> identifierList;

            identifierList = new ArrayList<>();
            selectedNodes = getSelectedTreeNodes();
            selectedEntities = new ArrayList<>();
            for (T8TreeNode selectedNode : selectedNodes)
            {
                String identifier;

                identifier = selectedNode.getIdentifier();
                if (!identifierList.contains(identifier)) identifierList.add(identifier);

                selectedEntities.add(selectedNode.getDataEntity());
            }

            // Create the event parameters collection and fire the event.
            eventParameters = new HashMap<>();
            eventParameters.put(T8TreeAPIHandler.PARAMETER_TREE_NODE_IDENTIFIER_LIST, identifierList);
            eventParameters.put(T8TreeAPIHandler.PARAMETER_TREE_NODE_LIST, selectedNodes);
            eventParameters.put(T8TreeAPIHandler.PARAMETER_DATA_ENTITY_LIST, selectedEntities);
            controller.reportEvent(T8Tree.this, definition.getComponentEventDefinition(T8TreeAPIHandler.EVENT_TREE_NODE_SELECTION_CHANGED), eventParameters);
        }
    }

    private class T8TreeNodeListener implements T8TreeNodeEditorListener
    {

        @Override
        public void pageIncremented(T8TreeNodeEditorEvent event)
        {
            T8TreeNode pagedNode;

            pagedNode = event.getTreeNode();
            if (pagedNode != null)
            {
                pagedNode.incrementChildNodePage();
                loadNodes(pagedNode);
            }
        }

        @Override
        public void pageDecremented(T8TreeNodeEditorEvent event)
        {
            T8TreeNode pagedNode;

            pagedNode = event.getTreeNode();
            if (pagedNode != null)
            {
                pagedNode.decrementChildNodePage();
                loadNodes(pagedNode);
            }
        }

        @Override
        public void filtersRemoved(T8TreeNodeEditorEvent event)
        {
            T8TreeNode targetNode;

            targetNode = event.getTreeNode();
            if (targetNode != null)
            {
                targetNode.clearUserDefinedChildNodeFilters();
                loadNodes(targetNode);
            }
        }

        @Override
        public void goToPage(T8TreeNodeEditorEvent event, int pageNumber)
        {
            T8TreeNode pagedNode;

            pagedNode = event.getTreeNode();
            if (pagedNode != null)
            {
                pagedNode.goToChildNodePage(pageNumber);
                loadNodes(pagedNode);
            }
        }
    }

    class TreeExpansionController extends Thread
    {
        private final T8TreeNode node;
        private final boolean loadChildren;
        private final boolean expand;
        private boolean stopFlag;

        TreeExpansionController(T8TreeNode node, boolean loadChildren, boolean expand)
        {
            this.node = node;
            this.loadChildren = loadChildren;
            this.expand = expand;
        }

        void stopExpansion()
        {
            stopFlag = true;
        }

        private void alterDescendantsExpansionState(final T8TreeNode parentNode, boolean loadChildren, boolean expand)
        {
            if(SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Expand Descendants not allowed to be called from EDT.");

            if(!stopFlag)
            {
                if(parentNode.areChildrenLoaded())
                {
                    for (T8TreeNode t8TreeNode : parentNode.getChildNodes())
                    {
                        alterDescendantsExpansionState(t8TreeNode, loadChildren, expand);
                    }

                    if(expand)
                    {
                        SwingUtilities.invokeLater(new Runnable()
                        {

                            @Override
                            public void run()
                            {
                                jTreeData.expandPath(new TreePath(parentNode.getPath()));
                            }
                        });
                    } else
                    {
                        SwingUtilities.invokeLater(new Runnable()
                        {

                            @Override
                            public void run()
                            {
                                TreePath treePath = new TreePath(parentNode.getPath());
                                if(jTreeData.isExpanded(treePath)) jTreeData.collapsePath(treePath);
                            }
                        });
                    }
                }
                else if(loadChildren && expand)
                {
                    //First load the child nodes, and then call the expand method again
                    loadNodes(parentNode);
                    synchronized(nodeLoaderList)
                    {
                        while(!nodeLoaderList.isEmpty())
                        {
                            try
                            {
                                nodeLoaderList.wait(1000);
                            }
                            catch (InterruptedException ex)
                            {
                                LOGGER.log("Interrupted while waiting for the nodes to be loaded", ex);
                            }
                        }
                    }
                    alterDescendantsExpansionState(parentNode, loadChildren, expand);
                }
            }
        }

        @Override
        public void run()
        {
            stopFlag = false;
            alterDescendantsExpansionState(node, loadChildren, expand);
            stopFlag = true;

            synchronized(treeExpansionControllers)
            {
                treeExpansionControllers.remove(this);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelDataView = new javax.swing.JPanel();
        jToolBarMain = new javax.swing.JToolBar();
        jButtonExpandAll = new javax.swing.JButton();
        jButtonCollapseAll = new javax.swing.JButton();
        jScrollPaneTree = new javax.swing.JScrollPane();

        jPanelDataView.setOpaque(false);
        jPanelDataView.setLayout(new java.awt.GridBagLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);
        jToolBarMain.setOpaque(false);

        jButtonExpandAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/nodeSelectChildIcon.png"))); // NOI18N
        jButtonExpandAll.setFocusable(false);
        jButtonExpandAll.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonExpandAll.setOpaque(false);
        jButtonExpandAll.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonExpandAll.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonExpandAllActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonExpandAll);

        jButtonCollapseAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/nodeInsertChildIcon.png"))); // NOI18N
        jButtonCollapseAll.setFocusable(false);
        jButtonCollapseAll.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonCollapseAll.setOpaque(false);
        jButtonCollapseAll.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonCollapseAll.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonCollapseAllActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonCollapseAll);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelDataView.add(jToolBarMain, gridBagConstraints);

        jScrollPaneTree.setBorder(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jPanelDataView.add(jScrollPaneTree, gridBagConstraints);

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonExpandAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExpandAllActionPerformed
    {//GEN-HEADEREND:event_jButtonExpandAllActionPerformed
        expandAll();
    }//GEN-LAST:event_jButtonExpandAllActionPerformed

    private void jButtonCollapseAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCollapseAllActionPerformed
    {//GEN-HEADEREND:event_jButtonCollapseAllActionPerformed
        collapseAll();
    }//GEN-LAST:event_jButtonCollapseAllActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCollapseAll;
    private javax.swing.JButton jButtonExpandAll;
    private javax.swing.JPanel jPanelDataView;
    private javax.swing.JScrollPane jScrollPaneTree;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
