/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.filter.panel.component.date;

import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.ui.T8ComponentController;
import javax.swing.JComponent;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public interface DatePickerComponent
{
    public Boolean isSelected();

    public void setSelected(Boolean selected);

    public T8DataFilterCriteria getFilterCriteria(String dataEntityFieldIdentifier);

    public void initialize(T8ComponentController controller);

    public JComponent getComponent();
}
