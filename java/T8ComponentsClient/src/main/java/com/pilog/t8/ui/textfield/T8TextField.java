package com.pilog.t8.ui.textfield;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.textfield.T8TextFieldAPIHandler;
import com.pilog.t8.definition.ui.textfield.T8TextFieldDefinition;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.components.border.PulsatingBorder;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.swing.JFormattedTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.InternationalFormatter;
import javax.swing.text.NumberFormatter;

/**
 * @author Bouwer du Preez
 */
public class T8TextField extends JFormattedTextField implements T8Component
{
    private static final T8Logger logger = T8Log.getLogger(T8TextField.class);

    private final T8ComponentController controller;
    private final T8TextFieldDefinition definition;

    private RegexValidationDocumentListener regexValidationListener;
    private T8TextFieldOperationHandler operationHandler;

    public T8TextField(T8TextFieldDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;

        if(definition.isUndecorated())
        {
            logger.log(T8Logger.Level.WARNING, "The text field 'Undecorated' attribute should no longer be used and will be removed in future release. Replace with Opaque=false & Border=NONE.");
            this.setOpaque(false);
            this.setBorder(null);
        }

        this.addFocusListener(new TextFieldFocusListener());

        if(definition.getFormatter() != null)
        {
            setFormatterFactory(getDefaultFormatterFactory(definition.getFormatter().getFormat()));
        }

        // We want the regular expression and the formatter to work independently
        // of one another, so we use a document listener for the regular
        // expression if it is available
        if (definition.getRegexValidationPattern() != null)
        {
            applyRegexValidation(definition.getRegexValidationPattern());
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.operationHandler = new T8TextFieldOperationHandler(this);
        setToolTipText(definition.getTooltipText());
        setEditable(definition.isEditable());
        if (!this.definition.isUndecorated()) // TODO: GBO - Remove when undecorated datum is removed
        {
            this.setOpaque(this.definition.isOpaque());
            switch (this.definition.getBorderType())
            {
                case NONE:
                    setBorder(null);
                    break;
            }
        }
    }

    @Override
    public void startComponent()
    {
        if (!definition.isUndecorated()) // TODO: GBO - Remove when undecorated datum is removed
        {
            // This border should only be added when the component is started
            if (this.definition.getBorderType() == T8TextFieldDefinition.TFBorderType.PULSATING)
            {
                PulsatingBorder.installWithFocusAnimation(this, new Color(0, 0, 200, 200));
            }
        }
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }
//</editor-fold>

    private AbstractFormatterFactory getDefaultFormatterFactory(Object type)
    {
        if (type instanceof DateFormat)
        {
            return new DefaultFormatterFactory(new DateFormatter((DateFormat)type));
        }
        if (type instanceof NumberFormat)
        {
            return new DefaultFormatterFactory(new NumberFormatter((NumberFormat)type));
        }
        if (type instanceof Format)
        {
            return new DefaultFormatterFactory(new InternationalFormatter((Format)type));
        }
        if (type instanceof Date)
        {
            return new DefaultFormatterFactory(new DateFormatter());
        }
        if (type instanceof Number)
        {
            AbstractFormatter displayFormatter = new NumberFormatter();
            ((DefaultFormatter)displayFormatter).setValueClass(type.getClass());
            AbstractFormatter editFormatter = new NumberFormatter(new DecimalFormat("#.#"));
            ((DefaultFormatter)editFormatter).setValueClass(type.getClass());

            return new DefaultFormatterFactory(displayFormatter, displayFormatter, editFormatter);
        }

        return new DefaultFormatterFactory(new DefaultFormatter());
    }

    void applyRegexValidation(String regexValidationPattern)
    {
        // We only want 1 regular expression applied at a time
        if (this.regexValidationListener != null)
        {
            getDocument().removeDocumentListener(this.regexValidationListener);
        }

        this.regexValidationListener = new RegexValidationDocumentListener(regexValidationPattern);
        getDocument().addDocumentListener(this.regexValidationListener);
    }

    /**
     * Checks whether or not the last changes made were successfully validated
     * by the regular expression listener applied. If none is applied, the
     * result will always be {@code true}.
     *
     * @return {@code true} iff a regular expression validation is applied and
     *      the changes have been validated according to that
     */
    boolean isRegexValidContent()
    {
        return (this.regexValidationListener == null || this.regexValidationListener.isContentValid());
    }

    private class TextFieldFocusListener implements FocusListener
    {
        @Override
        public void focusGained(FocusEvent e)
        {
            Map<String, Object> parameters;

            parameters = new HashMap<>();
            parameters.put(T8TextFieldAPIHandler.PARAMETER_TEXT, T8TextField.this.getText());
            parameters.put(T8TextFieldAPIHandler.PARAMETER_VALUE, T8TextField.this.getValue());
            controller.reportEvent(T8TextField.this, definition.getComponentEventDefinition(T8TextFieldAPIHandler.EVENT_FOCUS_GAINED), parameters);
        }

        @Override
        public void focusLost(FocusEvent e)
        {
            Map<String, Object> parameters;

            parameters = new HashMap<>();
            parameters.put(T8TextFieldAPIHandler.PARAMETER_TEXT, T8TextField.this.getText());
            parameters.put(T8TextFieldAPIHandler.PARAMETER_VALUE, T8TextField.this.getValue());
            controller.reportEvent(T8TextField.this, definition.getComponentEventDefinition(T8TextFieldAPIHandler.EVENT_FOCUS_LOST), parameters);
        }
    }

    private class RegexValidationDocumentListener implements DocumentListener
    {
        private final Pattern validationPattern;
        /**
         * This value is used to gain access to the regex validation state
         * through scripting operations.
         */
        private boolean validContent;

        private RegexValidationDocumentListener(String regexValidationPattern)
        {
            this.validationPattern = Pattern.compile(regexValidationPattern);
            this.validContent = true;
        }

        @Override
        public void insertUpdate(DocumentEvent e)
        {
            validateByPattern(e);
        }

        @Override
        public void removeUpdate(DocumentEvent e)
        {
            validateByPattern(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e)
        {
            validateByPattern(e);
        }

        private boolean isContentValid()
        {
            return this.validContent;
        }

        private void validateByPattern(DocumentEvent e)
        {
            String text;

            try
            {
                text = e.getDocument().getText(0, e.getDocument().getLength());
                if (!text.isEmpty())
                {
                    this.validContent = validationPattern.matcher(text).matches();
                    applyBackground();
                }
                else
                {
                    this.validContent = true;
                    applyBackground();
                }
            }
            catch (BadLocationException ex)
            {
                logger.log("Invalid Document.", ex);
            }
        }

        private void applyBackground()
        {
            setBackground(this.validContent ? Color.WHITE : LAFConstants.INVALID_PINK);
        }
    }
}
