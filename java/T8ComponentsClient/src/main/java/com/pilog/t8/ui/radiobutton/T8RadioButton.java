package com.pilog.t8.ui.radiobutton;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.radiobutton.T8RadioButtonAPIHandler;
import com.pilog.t8.definition.ui.radiobutton.T8RadioButtonDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JRadioButton;

/**
 * @author Bouwer du Preez
 */
public class T8RadioButton extends JRadioButton implements T8Component
{
    private final T8RadioButtonDefinition definition;
    private final T8ComponentController controller;
    private final T8RadioButtonOperationHandler operationHandler;
    private final ButtonActionListener buttonActionListener;

    public T8RadioButton(T8RadioButtonDefinition buttonDefinition, T8ComponentController controller)
    {
        this.definition = buttonDefinition;
        this.controller = controller;
        this.operationHandler = new T8RadioButtonOperationHandler(this);
        this.buttonActionListener = new ButtonActionListener();
        this.setOpaque(definition.isOpaque());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8IconDefinition iconDefinition;

        iconDefinition = definition.getIconDefinition();
        if (iconDefinition != null)
        {
            this.setIcon(iconDefinition.getImage().getImageIcon());
        }

        setText(definition.getText());
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
        String buttonGroupIdentifier;

        addActionListener(buttonActionListener);

        buttonGroupIdentifier = definition.getButtonGroupIdentifier();
        if (!Strings.isNullOrEmpty(buttonGroupIdentifier))
        {
            controller.addToButtonGroup(this, buttonGroupIdentifier);
        }
    }

    @Override
    public void stopComponent()
    {
        String buttonGroupIdentifier;

        removeActionListener(buttonActionListener);

        buttonGroupIdentifier = definition.getButtonGroupIdentifier();
        if (!Strings.isNullOrEmpty(buttonGroupIdentifier))
        {
            controller.removeFromButtonGroup(this, buttonGroupIdentifier);
        }
    }

    @Override
    public void addChildComponent(T8Component T8c, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private class ButtonActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            controller.reportEvent(T8RadioButton.this, definition.getComponentEventDefinition(T8RadioButtonAPIHandler.EVENT_BUTTON_SELECTED), new HashMap<>());
        }
    }
}
