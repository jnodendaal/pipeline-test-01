package com.pilog.t8.ui.datasearch.combobox;

import com.pilog.t8.data.filter.T8DataFilter;
import static com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8DataSearchComboBoxOperationHandler extends T8ComponentOperationHandler
{
    private final T8DataSearchComboBox comboBox;

    public T8DataSearchComboBoxOperationHandler(T8DataSearchComboBox comboBox)
    {
        super(comboBox);
        this.comboBox = comboBox;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier == null) return super.executeOperation(operationIdentifier, operationParameters);

        switch(operationIdentifier)
        {
            case COMPONENT_OPERATION_CLEAR_DATA_SOURCE_FILTER:
                this.comboBox.setDataSourceFilter(null, Boolean.TRUE.equals(operationParameters.get(PARAMETER_REFRESH_DATA_SOURCE)));
                return null;
            case COMPONENT_OPERATION_SET_DATA_SOURCE_FILTER:
                this.comboBox.setDataSourceFilter((T8DataFilter)operationParameters.get(PARAMETER_DATA_FILTER), Boolean.TRUE.equals(operationParameters.get(PARAMETER_REFRESH_DATA_SOURCE)));
                return null;
            case COMPONENT_OPERATION_GET_DATA_FILTER:
                return HashMaps.createSingular(PARAMETER_DATA_FILTER, comboBox.getDataFilter());
            case COMPONENT_OPERATION_CLEAR_DATA_FILTER:
                comboBox.clearDataFilter(Boolean.TRUE.equals(operationParameters.getOrDefault(PARAMETER_APPLY_FILTER, Boolean.TRUE)));
                return null;
            case COMPONENT_OPERATION_REFRESH_DATA_SOURCE:
                comboBox.refreshDataSource();
                return null;
            case COMPONENT_OPERATION_SET_SELECTED_VALUES:
                return HashMaps.createSingular(PARAMETER_DATA_FILTER, this.comboBox.setSelectedValues((List<Object>)operationParameters.get(PARAMETER_VALUE_LIST), Boolean.TRUE.equals(operationParameters.get(PARAMETER_CLEAR_SELECTION))));
            case COMPONENT_OPERATION_GET_SELECTED_VALUES:
                return HashMaps.createSingular(PARAMETER_VALUE_LIST, this.comboBox.getSelectedValues());
            default:
                return super.executeOperation(operationIdentifier, operationParameters);
        }
    }
}
