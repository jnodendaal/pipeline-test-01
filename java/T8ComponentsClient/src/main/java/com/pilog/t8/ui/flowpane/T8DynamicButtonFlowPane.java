package com.pilog.t8.ui.flowpane;

import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.definition.ui.flowpane.T8DynamicButtonFlowPaneAPIHandler;
import com.pilog.t8.definition.ui.flowpane.T8DynamicButtonFlowPaneDefinition;
import com.pilog.t8.ui.laf.T8ButtonUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.jdesktop.swingx.JXButton;

/**
 * @author Bouwer du Preez
 */
public class T8DynamicButtonFlowPane extends T8FlowPane implements T8Component
{
    private final T8DynamicButtonFlowPaneDefinition definition;
    private final T8ComponentController controller;
    private final Map<String, JXButton> dynamicButtons;
    private final DynamicButtonListener dynamicButtonListener;
    private final T8DynamicButtonFlowPaneOperationHandler operationHandler;
    private final T8LookAndFeelManager lafManager;
    private final T8ButtonUI buttonUI;

    public T8DynamicButtonFlowPane(T8DynamicButtonFlowPaneDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.controller = controller;
        this.dynamicButtonListener = new DynamicButtonListener();
        this.dynamicButtons = new HashMap<>();
        this.operationHandler = new T8DynamicButtonFlowPaneOperationHandler(this);
        this.lafManager = controller.getClientContext().getConfigurationManager().getLookAndFeelManager(controller.getContext());
        this.buttonUI = new T8ButtonUI(lafManager);
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    public void clearButtons()
    {
        dynamicButtons.clear();
        removeAll();
        revalidate();
        repaint();
    }

    public void addButton(String identifier, String text, String tooltipText, String iconIdentifier)
    {
        if (!dynamicButtons.containsKey(identifier))
        {
            JXButton newButton;

            newButton = new JXButton(text);
            newButton.setToolTipText(tooltipText);
            newButton.setName(identifier);
            newButton.addActionListener(dynamicButtonListener);
            newButton.setUI(buttonUI);

            // Add the new button to the collection of dynamic buttons.
            dynamicButtons.put(identifier, newButton);

            // Add the new button to the UI.
            add(newButton);
            revalidate();
            repaint();
        }
        else throw new RuntimeException("Dynamic button with identifier '" + identifier + "' already exists.  The identifier cannot be used again.");
    }

    public void removeButton(String identifier)
    {
        JXButton buttonToRemove;

        buttonToRemove = dynamicButtons.remove(identifier);
        if (buttonToRemove != null)
        {
            remove(buttonToRemove);
            revalidate();
            repaint();
        }
    }

    private class DynamicButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            HashMap<String, Object> eventParameters;
            JXButton buttonClicked;

            buttonClicked = (JXButton)e.getSource();

            eventParameters = new HashMap<>();
            eventParameters.put(T8DynamicButtonFlowPaneAPIHandler.PARAMETER_BUTTON_IDENTIFIER, buttonClicked.getName());
            controller.reportEvent(T8DynamicButtonFlowPane.this, definition.getComponentEventDefinition(T8DynamicButtonFlowPaneAPIHandler.EVENT_BUTTON_CLICKED), eventParameters);
        }
    }
}
