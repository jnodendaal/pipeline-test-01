package com.pilog.t8.ui.cellrenderer.tree.label;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.tree.T8TreeCellRenderableComponent;
import com.pilog.t8.ui.tree.T8TreeCellRendererComponent;
import com.pilog.t8.ui.tree.T8TreeNode;
import com.pilog.t8.ui.tree.T8TreeNode.NodeType;
import com.pilog.t8.definition.gfx.font.T8FontDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8ConditionalCellFormattingDefinition;
import com.pilog.t8.definition.ui.cellrenderer.tree.label.T8LabelTreeCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Bouwer.duPreez
 */
public class T8LabelTreeCellRenderer extends javax.swing.JPanel implements T8TreeCellRendererComponent
{
    private final T8LabelTreeCellRendererDefinition definition;
    private final T8ComponentController controller;
    private final T8ConfigurationManager configurationManager;
    private final T8Context context;
    private final ExpressionEvaluator formattingExpressionEvaluator;
    private ExpressionEvaluator valueExpressionEvaluator;
    private boolean pagingDetailsVisible;

    private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

    public T8LabelTreeCellRenderer(T8LabelTreeCellRendererDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.context = controller.getContext();
        this.formattingExpressionEvaluator = new ExpressionEvaluator();
        this.pagingDetailsVisible = true;
        this.setOpaque(true);
        initComponents();
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(context, inputString);
    }

    public void setPagingDetailsVisible(boolean visible)
    {
        pagingDetailsVisible = visible;
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }

    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String string, Map<String, Object> map)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> map)
    {
        String valueExpression;

        valueExpression = definition.getDisplayValueExpression();
        if ((valueExpression != null) && (valueExpression.trim().length() > 0))
        {
            try
            {
                valueExpressionEvaluator = new ExpressionEvaluator();
                valueExpressionEvaluator.compileExpression(valueExpression);
            }
            catch (Exception e)
            {
                T8Log.log("Exception while compiling display value expression in renderer: " + definition, e);
                valueExpressionEvaluator = null;
            }
        }

        // Set the filter details text.
        jLabelFilterDetails.setText("(" + translate("Filtered") + ")");
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public void setRendererNode(T8TreeCellRenderableComponent renderableComponent, T8TreeNode node, boolean isSelected, boolean hasFocus, boolean editable, boolean expanded, boolean leaf, int row, int currentPage, int pageCount)
    {
        HashMap<String, Object> expressionParameters;
        T8DataEntity dataEntity;
        Color foregroundColor;
        Color backgroundColor;
        Border cellBorder;
        Font cellFont;
        Icon icon;
        Object value;

        icon = null;
        // Determine the node value.
        if (node.getNodeType() == NodeType.GROUP)
        {
            value = node.getGroupDisplayName();
            dataEntity = null;
        }
        else
        {
            // Get the data entity in the node.
            dataEntity = node.getDataEntity();

            // Get the display value of the cell.
            if (valueExpressionEvaluator != null)
            {
                try
                {
                    value = valueExpressionEvaluator.evaluateExpression(dataEntity.getFieldValues(), null);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while evaluating display value expression in renderer: " + definition, e);
                    value = "Error";
                }
            }
            else
            {
                value = dataEntity.toString();
            }
        }

        // Get the default formatting values.
        cellFont = renderableComponent.getCellFont();
        if (isSelected)
        {
            foregroundColor = renderableComponent.getSelectedCellForegroundColor();
            backgroundColor = renderableComponent.getSelectedCellBackgroundColor();
            cellBorder = renderableComponent.getSelectedBorder();
        }
        else
        {
            foregroundColor = renderableComponent.getUnselectedCellForegroundColor();
            backgroundColor = renderableComponent.getUnselectedCellBackgroundColor();
            cellBorder = renderableComponent.getUnselectedBorder();
        }

        // Create a collection of parameters as input to the conditional formatting expressions.
        expressionParameters = new HashMap<>();
        if (dataEntity != null) expressionParameters.putAll(dataEntity.getFieldValues());

        // Evaluate each of the conditional formatting expressions and apply the first one for which the result is true.
        for (T8ConditionalCellFormattingDefinition formattingDefinition : definition.getConditionalFormattingDefinitions())
        {
            try
            {
                String conditionExpression;

                conditionExpression = formattingDefinition.getConditionExpression();
                if ((conditionExpression == null) || (conditionExpression.trim().length() == 0) || (formattingExpressionEvaluator.evaluateBooleanExpression(conditionExpression, expressionParameters, null)))
                {
                    Color bgColor;
                    Color bgsColor;
                    T8FontDefinition fontDefinition;

                    bgColor = formattingDefinition.getBackgroundColor();
                    bgsColor = formattingDefinition.getSelectedBackgroundColor();
                    fontDefinition = formattingDefinition.getFontDefinition();

                    if (isSelected)
                    {
                        if (bgsColor != null) backgroundColor = bgsColor;
                    }
                    else if (bgColor != null)
                    {
                        backgroundColor = bgColor;
                    }

                    // Set the cell font if the conditional formatting overrides it.
                    if (fontDefinition != null)
                    {
                        cellFont = fontDefinition.getNewFontInstance();
                    }

                    T8IconDefinition iconDefinition;

                    iconDefinition = formattingDefinition.getIconDefinition();
                    if (iconDefinition != null)
                    {
                        icon = iconDefinition.getImage().getImageIcon();
                    }
                    break;
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while evaluating condition expression for formatting definition: " + formattingDefinition, e);
            }
        }

        // Set the formatting.
        jLabelValue.setFont(cellFont);
        jLabelValue.setForeground(foregroundColor);
        jLabelValue.setBackground(backgroundColor);
        jLabelValue.setBorder(cellBorder);
        jLabelValue.setIcon(icon);

        // Set the value of the cell on the label.
        jLabelValue.setText(value + "");
        jLabelPagingDetails.setText(translate("Page:") + " " + (currentPage + 1) + "/" + pageCount);
        jLabelPagingDetails.setVisible((pageCount > 1) && (pagingDetailsVisible)); // Only show the paging details if necessary.
        jLabelFilterDetails.setVisible((node.getUserDefinedChildNodeFilters() != null) && (node.getUserDefinedChildNodeFilters().size() > 0));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelValue = new javax.swing.JLabel();
        jLabelPagingDetails = new javax.swing.JLabel();
        jLabelFilterDetails = new javax.swing.JLabel();

        setMaximumSize(null);
        setMinimumSize(null);
        setName(""); // NOI18N
        setOpaque(false);
        setPreferredSize(null);
        setLayout(new java.awt.GridBagLayout());

        jLabelValue.setText("Value");
        jLabelValue.setMaximumSize(null);
        jLabelValue.setMinimumSize(null);
        jLabelValue.setName(""); // NOI18N
        jLabelValue.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 5, 2);
        add(jLabelValue, gridBagConstraints);

        jLabelPagingDetails.setText("Page Details");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 0);
        add(jLabelPagingDetails, gridBagConstraints);

        jLabelFilterDetails.setText("Filter Details");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 2);
        add(jLabelFilterDetails, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelFilterDetails;
    private javax.swing.JLabel jLabelPagingDetails;
    private javax.swing.JLabel jLabelValue;
    // End of variables declaration//GEN-END:variables
}
