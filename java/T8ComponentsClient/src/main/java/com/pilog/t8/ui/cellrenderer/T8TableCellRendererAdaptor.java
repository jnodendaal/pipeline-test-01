package com.pilog.t8.ui.cellrenderer;

import com.pilog.t8.ui.table.T8TableCellRenderableComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent.RenderParameter;
import java.awt.Component;
import java.util.HashMap;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class T8TableCellRendererAdaptor implements TableCellRenderer
{
    private final T8TableCellRenderableComponent renderableComponent;
    private final T8TableCellRendererComponent rendererComponent;
    private String rendererCellIdentifier;
    private int rowIndex;

    public T8TableCellRendererAdaptor(T8TableCellRenderableComponent editableComponent, T8TableCellRendererComponent editorComponent)
    {
        this.renderableComponent = editableComponent;
        this.rendererComponent = editorComponent;
    }

    public T8TableCellRendererComponent getRendererComponent()
    {
        return this.rendererComponent;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        HashMap<RenderParameter, Object> renderParameters;
        TableModel tableModel;

        // Create the render parameter map.
        renderParameters = new HashMap<>();
        renderParameters.put(RenderParameter.SELECTED, isSelected);
        renderParameters.put(RenderParameter.FOCUSED, hasFocus);
        renderParameters.put(RenderParameter.ROW, row);
        renderParameters.put(RenderParameter.COLUMN, column);

        // Get the row of data from the source table.
        tableModel = table.getModel();

        // Record some meta data about the cell being edited.
        rowIndex = row;
        rendererCellIdentifier = tableModel.getColumnName(table.getColumnModel().getColumn(column).getModelIndex());

        // Set the editor component content and return it.
        rendererComponent.setRendererData(renderableComponent, renderableComponent.getRowEntityFieldValues(row), rendererCellIdentifier, renderParameters);
        return (Component)rendererComponent;
    }
}
