package com.pilog.t8.ui.filter.panel.component.list;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.filter.panel.component.list.T8FilterPanelListComponentAPIHandler;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponent;
import com.pilog.t8.ui.filter.panel.component.T8FilterPanelComponentOperationHandler;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8FilterPanelListComponentOperationHandler extends T8FilterPanelComponentOperationHandler
{
    private final T8FilterPanelListComponent component;
    public T8FilterPanelListComponentOperationHandler(
                                                      T8FilterPanelListComponent component)
    {
        super(component);
        this.component = component;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier,
                                                Map<String, Object> operationParameters)
    {
        if(T8FilterPanelListComponentAPIHandler.OPERATION_SET_DATA_SOURCE_FILTER.equals(operationIdentifier))
        {
            T8DataFilter dataFilter;
            Boolean refresh;
            try
            {
                dataFilter = (T8DataFilter) operationParameters.get(T8FilterPanelListComponentAPIHandler.PARAMETER_DATA_FILTER);
                refresh = (Boolean) operationParameters.get(T8FilterPanelListComponentAPIHandler.PARAMETER_REFRESH);

                component.setNewDataSourceFilter(dataFilter);

                if(refresh != null && refresh)
                {
                    component.refresh();
                }
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to refresh filter panel component.", ex);
            }
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
