package com.pilog.t8.ui.menu;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicPopupMenuUI;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8PopupMenuUI extends BasicPopupMenuUI
{
    private final Painter backgroundPainter;

    public T8PopupMenuUI(Painter backgroundPainter)
    {
        this.backgroundPainter = backgroundPainter;
    }

    /**
     * Overridden to provide Painter support.  Calls backgroundPainter.paint()
     * if it is not null, else calls super.paintComponent().
     */
    @Override
    public void paint(Graphics g, JComponent c)
    {
        if (backgroundPainter != null)
        {
            Graphics2D g2;
            Insets borderInsets;
            int contentWidth;
            int contentHeight;

            // Create a graphics context for the painter to use and discard it afterwards.
            borderInsets = c.getBorder().getBorderInsets(c);
            contentWidth = c.getWidth() - borderInsets.left - borderInsets.right;
            contentHeight = c.getHeight() - borderInsets.top - borderInsets.bottom;
            g2 = (Graphics2D)g.create(borderInsets.left, borderInsets.top, contentWidth, contentHeight);
            backgroundPainter.paint(g2, c, contentWidth, contentHeight);
            g2.dispose();
        }
    }

    /**
     * Overridden to prevent the component background from being filled
     * (set to opaque).
     */
    @Override
    public void update(Graphics g, JComponent c)
    {
	paint(g, c);
    }
}
