package com.pilog.t8.ui.passwordfield;

import com.pilog.t8.definition.ui.passwordfield.T8PasswordFieldAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PasswordFieldOperationHandler extends T8ComponentOperationHandler
{
    private T8PasswordField passwordField;

    public T8PasswordFieldOperationHandler(T8PasswordField passwordField)
    {
        super(passwordField);
        this.passwordField = passwordField;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8PasswordFieldAPIHandler.OPERATION_GET_PASSWORD))
        {
            return HashMaps.newHashMap(T8PasswordFieldAPIHandler.PARAMETER_PASSWORD, passwordField.getPassword());
        }
        else if(operationIdentifier.equals(T8PasswordFieldAPIHandler.OPERATION_SET_PASSWORD))
        {
            char[] password;
            
            password = (char[])operationParameters.get(T8PasswordFieldAPIHandler.PARAMETER_PASSWORD);
            passwordField.setText(password != null ? password.toString() : null);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
