package com.pilog.t8.ui.multisplitpane;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8MultiSplitPaneOperationHandler extends T8ComponentOperationHandler
{
    private T8MultiSplitPane splitPane;

    public T8MultiSplitPaneOperationHandler(T8MultiSplitPane splitPane)
    {
        super(splitPane);
        this.splitPane = splitPane;
    }
    
    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return super.executeOperation(operationIdentifier, operationParameters);
    }
}
