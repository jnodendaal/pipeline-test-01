package com.pilog.t8.ui.table.orderby;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.definition.ui.table.T8TableOrderingPresetDefinition;
import com.pilog.t8.utilities.collections.MapEntry;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.TransferHandler;
import org.jdesktop.swingx.JXComboBox;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.StringValue;

/**
 * @author Hennie Brink
 */
public class T8PresetOrderingPanel extends JPanel
{
    private static final T8Logger logger = T8Log.getLogger(T8PresetOrderingPanel.class);

    private final T8TableDefinition tableDefinition;
    private final List<FieldOrderingPanel> fieldOrderingPanels;
    private final Collection<T8TableOrderingPresetDefinition> orderingPresetDefinitions;

    private final LinkedHashMap<String, OrderMethod> ordering;

    public T8PresetOrderingPanel(T8TableDefinition tableDefinition, Map<String, T8DataFilter.OrderMethod> existingOrdering)
    {
        this.tableDefinition = tableDefinition;
        this.orderingPresetDefinitions = tableDefinition.getOrderingPresetDefinitions();
        this.ordering = new LinkedHashMap<>();

        initComponents();
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        setOpaque(false);

        this.fieldOrderingPanels = new ArrayList<>();

        createUI();

        updateOrderingPanels(existingOrdering);
    }

    private void createUI()
    {
        if (orderingPresetDefinitions != null && !orderingPresetDefinitions.isEmpty())
        {
            JXComboBox orderingPresets;
            DefaultComboBoxModel<T8TableOrderingPresetDefinition> comboBoxModel;

            comboBoxModel = new DefaultComboBoxModel<>();

            comboBoxModel.addElement(null);

            for (T8TableOrderingPresetDefinition t8TableOrderingPresetDefinition : orderingPresetDefinitions)
            {
                comboBoxModel.addElement(t8TableOrderingPresetDefinition);
            }

            orderingPresets = new JXComboBox(comboBoxModel);
            orderingPresets.setRenderer(new DefaultListRenderer(new StringValue()
            {

                @Override
                public String getString(Object value)
                {
                    return value == null ? null : ((T8TableOrderingPresetDefinition) value).getPresetName();
                }
            }));

            orderingPresets.addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    T8TableOrderingPresetDefinition selectedItem;
                    JXComboBox orderingPresets;

                    orderingPresets = (JXComboBox) e.getSource();
                    selectedItem = (T8TableOrderingPresetDefinition) orderingPresets.getSelectedItem();
                    if (selectedItem != null)
                    {
                        updateOrderingPanels(selectedItem.getFieldOrderingMap());
                    }
                }
            });

            add(orderingPresets);
        }

        int index = 1;
        for (T8TableColumnDefinition t8TableColumnDefinition : tableDefinition.getColumnDefinitions())
        {
            if (t8TableColumnDefinition.isVisible())
            {
                FieldOrderingPanel orderingPanel;

                orderingPanel = new FieldOrderingPanel(t8TableColumnDefinition, index++);

                add(orderingPanel);
                fieldOrderingPanels.add(orderingPanel);
            }
        }

        revalidate();
    }

    private void updateOrderingPanels(
            Map<String, T8DataFilter.OrderMethod> fieldOrdering)
    {
        if (fieldOrdering == null)
        {
            return;
        }
        for (FieldOrderingPanel fieldOrderingPanel : fieldOrderingPanels)
        {
            fieldOrderingPanel.setPriority(-1);
            fieldOrderingPanel.setOrderingMethod(null);
        }

        int fieldIndex = 1;
        for (String fieldIdentifier : fieldOrdering.keySet())
        {
            for (FieldOrderingPanel fieldOrderingPanel : fieldOrderingPanels)
            {
                if (fieldOrderingPanel.getFieldIdentifier().equals(fieldIdentifier))
                {
                    //Update the ordering methods and priority of the panel
                    fieldOrderingPanel.setOrderingMethod(fieldOrdering.get(fieldIdentifier));
                    fieldOrderingPanel.setPriority(fieldIndex++);
                    break;
                }
            }
        }

        fieldIndex = fieldOrdering.size()+1;

        //Move the other fields to a lower priority
        for (FieldOrderingPanel fieldOrderingPanel : fieldOrderingPanels)
        {
            if(fieldOrderingPanel.getPriority() == -1)
                fieldOrderingPanel.setPriority(fieldIndex++);
        }

        ordering.clear();
        ordering.putAll(fieldOrdering);

        updateOrderingPanels();
    }

    private void updateOrderingPanels()
    {
        Collections.sort(fieldOrderingPanels);

        for (FieldOrderingPanel fieldOrderingPanel : fieldOrderingPanels)
        {
            //Remove the panel from the display so we can insert it at a new position
            remove(fieldOrderingPanel);

            //insert the panel at the new position
            add(fieldOrderingPanel, fieldOrderingPanel.getPriority());
        }
        revalidate();
    }

    public Map<String, T8DataFilter.OrderMethod> getNewOrdering()
    {
        Collections.sort(fieldOrderingPanels);
        LinkedHashMap<String, T8DataFilter.OrderMethod> newOrdering;

        newOrdering = new LinkedHashMap<>(ordering.size());

        for (FieldOrderingPanel fieldOrderingPanel : fieldOrderingPanels)
        {
            MapEntry<String, T8DataFilter.OrderMethod> orderingMethod = fieldOrderingPanel.getOrderingMethod();
            if (orderingMethod != null)
            {
                newOrdering.put(orderingMethod.getKey(), orderingMethod.getValue());
            }
        }

        //If the new map does not contain any of the hidden fields, add them last
        for (String oldKey : ordering.keySet())
        {
           if(!newOrdering.containsKey(oldKey)) newOrdering.put(oldKey, ordering.get(oldKey));
        }

        return newOrdering;
    }

    public void clearOrdering()
    {
        ordering.clear();
        fieldOrderingPanels.clear();
        removeAll();
        createUI();
    }

    protected class FieldOrderingPanel extends JXPanel implements Comparable<FieldOrderingPanel>
    {
        private final T8TableColumnDefinition columnDefinition;
        private Integer priority;

        private JXComboBox orderingComboBox;

        FieldOrderingPanel(T8TableColumnDefinition columnDefinition,
                           int priority)
        {
            this.columnDefinition = columnDefinition;
            this.priority = priority;
            initialize();
            setOpaque(false);
        }

        private void initialize()
        {
            JXLabel fieldNameLabel;

            fieldNameLabel = new JXLabel(columnDefinition.getColumnName());
            fieldNameLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 3));
            fieldNameLabel.setTransferHandler(new TransferHandler()
            {

                @Override
                public int getSourceActions(JComponent c)
                {
                    return TransferHandler.COPY_OR_MOVE;
                }

                @Override
                public boolean importData(
                        TransferHandler.TransferSupport support)
                {
                    if (!support.isDataFlavorSupported(DataFlavor.stringFlavor))
                    {
                        return false;
                    }

                    String fieldIdentifier;
                    FieldOrderingPanel panel = null;
                    try
                    {
                        fieldIdentifier = (String) support.getTransferable().getTransferData(DataFlavor.stringFlavor);

                        for (FieldOrderingPanel fieldOrderingPanel : fieldOrderingPanels)
                        {
                            if (fieldOrderingPanel.getFieldIdentifier().equals(fieldIdentifier))
                            {
                                panel = fieldOrderingPanel;
                                break;
                            }
                        }

                        if(panel == null) return false;

                        int newPriority;
                        int oldPriority;

                        oldPriority = getPriority();
                        newPriority = panel.getPriority();

                        setPriority(newPriority);
                        panel.setPriority(oldPriority);

                        updateOrderingPanels();
                        return true;
                    }
                    catch (UnsupportedFlavorException | IOException ex)
                    {
                        logger.log(ex);
                    }
                    return false;
                }

                @Override
                public boolean canImport(TransferHandler.TransferSupport support)
                {
                    return support.isDataFlavorSupported(DataFlavor.stringFlavor);
                }

                @Override
                protected Transferable createTransferable(JComponent c)
                {
                    return new StringSelection(FieldOrderingPanel.this.getFieldIdentifier());
                }
            });
            orderingComboBox = new JXComboBox(new String[]
            {
                null,
                T8DataFilter.OrderMethod.ASCENDING.toString(),
                T8DataFilter.OrderMethod.DESCENDING.toString()
            });
            orderingComboBox.setRenderer(new DefaultListRenderer(new StringValue()
            {

                @Override
                public String getString(Object value)
                {
                    return value == null ? " " : value.toString();
                }
            }));

            setLayout(new GridLayout(1, 2));

            add(fieldNameLabel);
            add(orderingComboBox);

            fieldNameLabel.addMouseListener(new MouseAdapter()
            {
                @Override
                public void mousePressed(MouseEvent e)
                {
                    JComponent c = (JComponent) e.getSource();
                    TransferHandler handler = c.getTransferHandler();
                    handler.exportAsDrag(c, e, TransferHandler.COPY);
                }

                @Override
                public void mouseEntered(MouseEvent e)
                {
                    super.mouseEntered(e);
                    setCursor(new Cursor(Cursor.MOVE_CURSOR));
                }

                @Override
                public void mouseExited(MouseEvent e)
                {
                    super.mouseExited(e);
                    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }

            });

            setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        }

        public MapEntry<String, T8DataFilter.OrderMethod> getOrderingMethod()
        {
            String selectedOrdering;

            selectedOrdering = (String) orderingComboBox.getSelectedItem();

            if (selectedOrdering == null)
            {
                return null;
            }

            return new MapEntry<>(columnDefinition.getFieldIdentifier(), T8DataFilter.OrderMethod.valueOf(selectedOrdering));
        }

        public String getFieldIdentifier()
        {
            return columnDefinition.getFieldIdentifier();
        }

        public void setOrderingMethod(T8DataFilter.OrderMethod method)
        {
            orderingComboBox.setSelectedItem(method == null ? null
                                             : method.toString());
        }

        public void setPriority(int priority)
        {
            this.priority = priority;
        }

        public Integer getPriority()
        {
            return priority;
        }

        @Override
        public int compareTo(FieldOrderingPanel o)
        {
            return o == null ? -1 : getPriority().compareTo(o.getPriority());
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setName(""); // NOI18N
        setPreferredSize(null);
        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.Y_AXIS));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
