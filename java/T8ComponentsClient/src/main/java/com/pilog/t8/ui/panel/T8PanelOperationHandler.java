package com.pilog.t8.ui.panel;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.HashMap;
import java.util.Map;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8PanelOperationHandler extends T8ComponentOperationHandler
{
    private final Map<String, Painter> painterCache;
    private final T8Panel panel;

    public T8PanelOperationHandler(T8Panel panel)
    {
        super(panel);
        this.panel = panel;
        this.painterCache = new HashMap<>();
    }

    @Override
    public Map<String, Object> executeOperation(String operationId, Map<String, Object> operationParameters)
    {
        if (T8PanelAPIHandler.OPERATION_SET_HEADER_TEXT.equals(operationId))
        {
            String text;

            text = (String) operationParameters.get(T8PanelAPIHandler.PARAMETER_TEXT);
            panel.setHeaderText(text);
            return null;
        }
        else if(T8PanelAPIHandler.OPERATION_SET_PANEL_SLOT_HEADER_TEXT.equals(operationId))
        {
            String panelSlotIdentifier;
            String text;

            panelSlotIdentifier = (String) operationParameters.get(T8PanelAPIHandler.PARAMETER_PANEL_SLOT_IDENTIFIER);
            text = (String) operationParameters.get(T8PanelAPIHandler.PARAMETER_TEXT);
            panel.setPanelSlotHeaderText(panelSlotIdentifier, text);
            return null;
        }
        else if(T8PanelAPIHandler.OPERATION_SET_BACKGROUND_PAINTER.equals(operationId))
        {
            T8DefinitionManager definitionManager;
            String painterId;

            painterId = (String)operationParameters.get(T8PanelAPIHandler.PARAMETER_BACKGROUND_PAINTER_IDENTIFIER);
            definitionManager = panel.getController().getClientContext().getDefinitionManager();

            if(!painterCache.containsKey(painterId))
            {
                T8PainterDefinition paintDefinition;

                try
                {
                    String projectId;

                    projectId = panel.getComponentDefinition().getRootProjectId();
                    paintDefinition = (T8PainterDefinition) definitionManager.getInitializedDefinition(panel.getController().getContext(), projectId, painterId, null);
                    painterCache.put(painterId, new T8PainterAdapter(paintDefinition.getNewPainterInstance()));
                }
                catch (Exception ex)
                {
                    T8Log.log("Failed to load painter definition", ex);
                }
            }

            panel.setBackgroundPainter(painterCache.get(painterId));
            return null;
        }
        return super.executeOperation(operationId, operationParameters);
    }
}
