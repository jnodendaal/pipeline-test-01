package com.pilog.t8.ui.entityeditor.panel;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.entityeditor.panel.T8PanelDataEntityEditorAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PanelDataEntityEditorOperationHandler extends T8ComponentOperationHandler
{
    private T8PanelDataEntityEditor panel;

    public T8PanelDataEntityEditorOperationHandler(T8PanelDataEntityEditor panel)
    {
        super(panel);
        this.panel = panel;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_SET_KEY))
        {
            Map<String, Object> keyMap;
            Boolean refresh;

            keyMap = (Map<String, Object>)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_KEY_MAP);
            refresh = (Boolean)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_REFRESH_DATA);
            if (refresh == null) refresh = true;

            panel.setKey(keyMap);
            if (refresh) panel.refreshData();
            return null;
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_SET_DATA_ENTITY_FIELD_VALUES))
        {
            Map<String, Object> valueMap;
            Boolean refresh;

            valueMap = (Map<String, Object>)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_VALUE_MAP);
            refresh = (Boolean)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_REFRESH_DATA);
            if (refresh == null) refresh = true;

            panel.setEditorDataEntityFieldValues(valueMap);
            return null;
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_GET_KEY))
        {
            return HashMaps.newHashMap(T8PanelDataEntityEditorAPIHandler.PARAMETER_KEY_MAP, panel.getKey());
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_PREFILTER_BY_KEY))
        {
            Map<String, Object> filterValues;
            Boolean refresh;

            filterValues = (Map<String, Object>)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_KEY_MAP);
            refresh = (Boolean)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_REFRESH_DATA);
            if (refresh == null) refresh = true;

            if (filterValues != null)
            {
                panel.setPrefilter(null, new T8DataFilter(panel.getComponentDefinition().getEditorDataEntityIdentifier(), filterValues));
                if (refresh) panel.refreshData();
                return null;
            }
            else
            {
                panel.setPrefilter(null, null);
                if (refresh) panel.refreshData();
                return null;
            }
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_GET_COMBINED_FILTER))
        {
            return HashMaps.newHashMap(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_FILTER, panel.getCombinedFilter());
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_GET_USER_DEFINED_FILTER))
        {
            return HashMaps.newHashMap(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_FILTER, panel.getUserDefinedFilter());
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_GET_PREFILTER))
        {
            String filterIdentifier;

            filterIdentifier = (String)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
            return HashMaps.newHashMap(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_FILTER, panel.getPrefilter(filterIdentifier));
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_SET_PREFILTER))
        {
            T8DataFilter dataFilter;
            Boolean refresh;
            String filterIdentifier;

            dataFilter = (T8DataFilter)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_FILTER);
            refresh = (Boolean)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_REFRESH_DATA);
            filterIdentifier = (String)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
            if (refresh == null) refresh = true;

            panel.setPrefilter(filterIdentifier, dataFilter);
            if (refresh) panel.refreshData();
            return null;
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_COMMIT_CHANGES))
        {
            panel.commitEditorChanges();
            return null;
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_SET_DATA_ENTITY))
        {
            panel.setEditorDataEntity((T8DataEntity)operationParameters.get(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY));
            return null;
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_GET_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8PanelDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY, panel.getEditorDataEntity());
        }
        else if (operationIdentifier.equals(T8PanelDataEntityEditorAPIHandler.OPERATION_REFRESH_DATA))
        {
            panel.refreshData();
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
