package com.pilog.t8.ui.list;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.list.T8ListAPIHandler;
import com.pilog.t8.definition.ui.list.T8ListDefinition;
import com.pilog.t8.definition.ui.list.T8ListDefinition.ListSelectionMode;
import com.pilog.t8.ui.cellrenderer.list.T8CustomListCellRenderer;
import com.pilog.t8.utilities.components.scrollpane.FadingScrollPane;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class T8List extends JXPanel implements T8Component
{
    private final DisabledSelectionModel disabledSelectionModel;
    private final T8ListOperationHandler operationHandler;
    private final T8ListMouseListener mouseListener;
    private final T8ComponentController controller;
    private final T8ListDefinition definition;
    private final JXList list;

    private ListSelectionModel selectionModel;
    private DefaultListModel listModel;
    private JScrollPane scrollPane;
    private boolean enabled;

    public T8List(T8ListDefinition listDefinition, T8ComponentController controller)
    {
        this.definition = listDefinition;
        this.controller = controller;

        this.disabledSelectionModel = new DisabledSelectionModel();
        this.operationHandler = new T8ListOperationHandler(this);
        this.mouseListener = new T8ListMouseListener();
        this.setLayout(new BorderLayout());
        this.setOpaque(definition.isOpaque());
        this.enabled = true;

        // Create a list and wrap it in a scroll pane if required.
        list = new JXList();
        list.setOpaque(definition.isOpaque());
        list.addMouseListener(mouseListener);
        if (definition.isScrollable())
        {
            scrollPane = new FadingScrollPane(list);
            scrollPane.setOpaque(false);
            scrollPane.getViewport().setOpaque(false);
            add(scrollPane, BorderLayout.CENTER);
        }
        else
        {
            add(list, BorderLayout.CENTER);
        }

        // Set the list selection mode.
        if (definition.getSelectionMode() == ListSelectionMode.SINGLE_INTERVAL_SELECTION)
        {
            list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        }
        else if (definition.getSelectionMode() == ListSelectionMode.MULTIPLE_SELECTION)
        {
            list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        }
        else
        {
            list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        listModel = new DefaultListModel();
        list.setModel(listModel);

        this.selectionModel = this.list.getSelectionModel();
        this.selectionModel.addListSelectionListener(new T8ListSelectionListener());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        if (childComponent instanceof T8CustomListCellRenderer)
        {
            list.setCellRenderer((ListCellRenderer) childComponent);
        }
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        this.list.setEnabled(enabled); // This doesn't actually do anything at the moment, but left here in case implemented
        this.scrollPane.setEnabled(enabled);

        // We need to change the actual selection model.
        // The selection listener is bound to the active selection model, so it
        // gets added back when the original selection model is re-applied.
        if (enabled)
        {
            this.list.setSelectionModel(this.selectionModel);
        }
        else
        {
            this.disabledSelectionModel.setPreviousSelection(this.selectionModel, this.listModel.size());
            this.list.setSelectionModel(this.disabledSelectionModel);
        }

        super.setEnabled(enabled);
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled()
    {
        return this.enabled;
    }

    public void addElements(List<Object> elements)
    {
        if (elements != null)
        {
            for (Object element : elements)
            {
                listModel.addElement(element);
            }
        }
    }

    public void removeElements(List<Object> elements)
    {
        if (elements != null)
        {
            for (Object element : elements)
            {
                listModel.removeElement(element);
            }
        }
    }

    public void removeAllElements()
    {
        listModel.removeAllElements();
    }

    public List<Object> getAllElements()
    {
        ArrayList<Object> values;
        ListModel model;

        model = list.getModel();
        values = new ArrayList<>();
        for (int index = 0; index < model.getSize(); index++)
        {
            values.add(model.getElementAt(index));
        }

        return values;
    }

    public List<Object> getSelectedElements()
    {
        // Return an ArrayList (not the java.util.Arrays$ArrayList created by the Arrays.asList() method).
        return new ArrayList<>(Arrays.asList(list.getSelectedValues()));
    }

    public void decrementSelectedElementIndex()
    {
        List<Object> selectedElements;
        int[] selectedIndices;

        selectedIndices = list.getSelectedIndices();
        selectedElements = getSelectedElements();
        if (selectedElements.size() > 0)
        {
            DefaultListModel model;

            model = (DefaultListModel)list.getModel();
            for (Object selectedElement : selectedElements)
            {
                int elementIndex;

                elementIndex = model.indexOf(selectedElement);
                if (elementIndex > 0)
                {
                    model.removeElement(selectedElement);
                    model.add(elementIndex-1, selectedElement);
                }
            }

            // Decrement all selected indices (because they've been moved up).
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]-1);
            }

            // Set the new selected indices.
            list.setSelectedIndices(selectedIndices);
        }
    }

    public void incrementSelectedElementIndex()
    {
        List<Object> selectedElements;
        int[] selectedIndices;

        selectedIndices = list.getSelectedIndices();
        selectedElements = getSelectedElements();
        if (selectedElements.size() > 0)
        {
            DefaultListModel model;

            model = (DefaultListModel)list.getModel();
            for (Object selectedElement : selectedElements)
            {
                int entityIndex;

                entityIndex = model.indexOf(selectedElement);
                if (entityIndex < model.getSize() -1)
                {
                    model.removeElement(selectedElement);
                    model.add(entityIndex+1, selectedElement);
                }
            }

            // Increment all selected indices (because they've been moved down).
            for (int indexIndex = 0; indexIndex < selectedIndices.length; indexIndex++)
            {
                selectedIndices[indexIndex] = (selectedIndices[indexIndex]+1);
            }

            // Set the new selected indices.
            list.setSelectedIndices(selectedIndices);
        }
    }

    public void clearSelection()
    {
        list.clearSelection();
    }

    public void setSelectedElements(List<Object> elements)
    {
        list.clearSelection();
        if (elements != null)
        {
            ListSelectionModel selectionModel;

            selectionModel = list.getSelectionModel();
            selectionModel.setValueIsAdjusting(true);
            for (Object element : elements)
            {
                int index;

                index = listModel.indexOf(element);
                if (index > -1)
                {
                    selectionModel.addSelectionInterval(index, index);
                }
            }
            selectionModel.setValueIsAdjusting(false);
        }
    }

    private class T8ListMouseListener extends MouseAdapter
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.getClickCount() == 2)
            {
                HashMap<String, Object> eventParameters;

                eventParameters = new HashMap<>();
                eventParameters.put(T8ListAPIHandler.PARAMETER_SELECTED_ELEMENT_LIST, getSelectedElements());
                controller.reportEvent(T8List.this, definition.getComponentEventDefinition(T8ListAPIHandler.EVENT_DOUBLE_CLICKED), eventParameters);
            }
        }
    }

    private class T8ListSelectionListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            if (!e.getValueIsAdjusting())
            {
                HashMap<String, Object> eventParameters;

                eventParameters = new HashMap<>();
                eventParameters.put(T8ListAPIHandler.PARAMETER_SELECTED_ELEMENT_LIST, getSelectedElements());
                controller.reportEvent(T8List.this, definition.getComponentEventDefinition(T8ListAPIHandler.EVENT_SELECTION_CHANGED), eventParameters);
            }
        }
    }

    private class DisabledSelectionModel extends DefaultListSelectionModel
    {
        private boolean emptySelectionInterval;
        private List<Integer> selection;

        private DisabledSelectionModel()
        {
            this.emptySelectionInterval = true;
        }

        @Override
        public void setSelectionInterval(int index0, int index1)
        {
            if (this.emptySelectionInterval) super.setSelectionInterval(-1, -1);
            else
            {
                this.selection.stream().filter(selectIdx -> !super.isSelectedIndex(selectIdx)).forEach(selectIdx -> super.addSelectionInterval(selectIdx, selectIdx));
            }
        }

        private void setPreviousSelection(ListSelectionModel currentSelectionModel, int listSize)
        {
            this.selection = new ArrayList<>();

            if (currentSelectionModel.isSelectionEmpty())
            {
                this.emptySelectionInterval = true;
            }
            else
            {
                for (int listIdx = 0; listIdx < listSize; listIdx++)
                {
                    if (currentSelectionModel.isSelectedIndex(listIdx))
                    {
                        this.selection.add(listIdx);
                    }
                }
            }
        }
    }
}
