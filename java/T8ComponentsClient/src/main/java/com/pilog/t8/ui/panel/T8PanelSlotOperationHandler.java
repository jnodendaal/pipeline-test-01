package com.pilog.t8.ui.panel;

import com.pilog.t8.definition.ui.panel.T8PanelSlotAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8PanelSlotOperationHandler extends T8ComponentOperationHandler
{
    private final T8PanelSlot panelSlot;

    public T8PanelSlotOperationHandler(T8PanelSlot panel)
    {
        super(panel);
        this.panelSlot = panel;
    }
    
    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (T8PanelSlotAPIHandler.OPERATION_SET_HEADER_TEXT.equals(operationIdentifier))
        {
            String text;

            text = (String)operationParameters.get(T8PanelSlotAPIHandler.PARAMETER_TEXT);
            panelSlot.setTitle(text);
            return null;
        } 
        else if (T8PanelSlotAPIHandler.OPERATION_SET_SLOT_COMPONENT.equals(operationIdentifier))
        {
            String componentIdentifier;

            componentIdentifier = (String)operationParameters.get(T8PanelSlotAPIHandler.PARAMETER_COMPONENT_IDENTIFIER);
            panelSlot.setSlotComponent(componentIdentifier);
            return null;
        } 
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
