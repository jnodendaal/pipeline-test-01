package com.pilog.t8.ui.datalookuplink;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.datalookuplink.T8DataLookupLinkAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupLinkOperationHandler extends T8ComponentOperationHandler
{
    private final T8DataLookupLink lookupLink;

    public T8DataLookupLinkOperationHandler(T8DataLookupLink lookupLink)
    {
        super(lookupLink);
        this.lookupLink = lookupLink;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8DataLookupLinkAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8DataLookupLinkAPIHandler.PARAMETER_DATA_ENTITY, lookupLink.getSelectedDataEntity());
        }
        else if (operationIdentifier.equals(T8DataLookupLinkAPIHandler.OPERATION_CLEAR))
        {
            lookupLink.clear();
            return null;
        }
        else if (operationIdentifier.equals(T8DataLookupLinkAPIHandler.OPERATION_GET_SELECTED_DATA_VALUE))
        {
            return HashMaps.newHashMap(T8DataLookupLinkAPIHandler.PARAMETER_DATA_VALUE, lookupLink.getSelectedDataValue());
        }
        else if (operationIdentifier.equals(T8DataLookupLinkAPIHandler.OPERATION_SET_SELECTED_DATA_ENTITY_KEY))
        {
            Map<String, Object> keyMap;

            keyMap = (Map<String, Object>)operationParameters.get(T8DataLookupLinkAPIHandler.PARAMETER_DATA_VALUE_MAP);
            lookupLink.setSelectedDataEntityKey(keyMap);
            return null;
        }
        else if (operationIdentifier.equals(T8DataLookupLinkAPIHandler.OPERATION_SET_PREFILTER))
        {
            T8DataFilter dataFilter;
            Boolean refresh;
            String filterIdentifier;

            dataFilter = (T8DataFilter)operationParameters.get(T8DataLookupLinkAPIHandler.PARAMETER_DATA_FILTER);
            refresh = (Boolean)operationParameters.get(T8DataLookupLinkAPIHandler.PARAMETER_REFRESH_DATA);
            filterIdentifier = (String)operationParameters.get(T8DataLookupLinkAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
            if (refresh == null) refresh = true;

            lookupLink.setPrefilter(filterIdentifier, dataFilter);
            if (refresh) lookupLink.refreshData();
            return null;
        }
        else if (operationIdentifier.equals(T8DataLookupLinkAPIHandler.OPERATION_SET_EDITABLE))
        {
            Boolean editable;

            editable = (Boolean)operationParameters.get(T8DataLookupLinkAPIHandler.PARAMETER_EDITABLE);
            if (editable == null) editable = true;

            lookupLink.setEditable(editable);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
