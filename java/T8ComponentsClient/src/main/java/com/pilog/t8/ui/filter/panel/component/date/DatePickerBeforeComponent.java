/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.filter.panel.component.date;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.time.T8Timestamp;
import java.util.Date;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class DatePickerBeforeComponent extends DatePickerSingleDateComponent
{
    public DatePickerBeforeComponent()
    {
        super("Before");
    }

    @Override
    public T8DataFilterCriteria getFilterCriteria(
            Date datePicker, String dataEntityFieldIdentifier)
    {
        T8DataFilterCriteria filterCriteria = new T8DataFilterCriteria();
        if (datePicker != null)
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND,
                                           dataEntityFieldIdentifier,
                                           T8DataFilterCriterion.DataFilterOperator.LESS_THAN_OR_EQUAL,
                                           new T8Timestamp(datePicker.getTime()));
        }
        return filterCriteria;
    }
}
