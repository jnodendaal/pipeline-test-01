package com.pilog.t8.ui.entityeditor.textfield;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.ui.entityeditor.textfield.T8TextFieldDataEntityEditorDefinition;
import com.pilog.t8.ui.textfield.T8TextField;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextFieldDataEntityEditor extends T8TextField implements T8DataEntityEditorComponent
{
    private T8TextFieldDataEntityEditorDefinition definition;
    private T8TextFieldDataEntityEditorOperationHandler operationHandler;
    private T8DataEntity editorEntity;
    
    public T8TextFieldDataEntityEditor(T8TextFieldDataEntityEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.operationHandler = new T8TextFieldDataEntityEditorOperationHandler(this);
    }
    
    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }
    
    @Override
    public void setEditorDataEntity(T8DataEntity dataEntity)
    {
        editorEntity = dataEntity;
        if (editorEntity != null)
        {
            String fieldIdentifier;
            
            fieldIdentifier = definition.getEditorDataEntityFieldIdentifier();
            if (fieldIdentifier != null)
            {
                Object value;
                
                value = editorEntity.getFieldValue(fieldIdentifier);
                if (value != null)
                {
                    setText(value.toString());
                }
                else setText(null);
            }
            else setText(null);
        }
        else setText(null);
    }
    
    @Override
    public T8DataEntity getEditorDataEntity()
    {
        return editorEntity;
    }

    @Override
    public boolean commitEditorChanges()
    {
        if (editorEntity != null)
        {
            String fieldIdentifier;

            fieldIdentifier = definition.getEditorDataEntityFieldIdentifier();
            if (fieldIdentifier != null)
            {
                editorEntity.setFieldValue(fieldIdentifier, getText());
                return true;
            }
            else throw new RuntimeException("No field identifier set in entity editor: " + definition);
        }
        else return true;
    }
    
    @Override
    public String getEditorDataEntityIdentifier()
    {
        return definition.getEditorDataEntityIdentifier();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return definition.getParentEditorIdentifier();
    }
}
