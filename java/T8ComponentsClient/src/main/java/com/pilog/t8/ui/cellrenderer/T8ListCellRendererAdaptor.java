package com.pilog.t8.ui.cellrenderer;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.ui.list.T8ListCellRenderableComponent;
import com.pilog.t8.ui.list.T8ListCellRendererComponent;
import com.pilog.t8.ui.list.T8ListCellRendererComponent.RenderParameter;
import java.awt.Component;
import java.util.HashMap;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

/**
 * @author Bouwer du Preez
 */
public class T8ListCellRendererAdaptor implements ListCellRenderer
{
    private T8ListCellRenderableComponent renderableComponent;
    private T8ListCellRendererComponent rendererComponent;
    
    public T8ListCellRendererAdaptor(T8ListCellRenderableComponent renderableComponent, T8ListCellRendererComponent rendererComponent)
    {
        this.renderableComponent = renderableComponent;
        this.rendererComponent = rendererComponent;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        HashMap<String, Object> dataRow;
        HashMap<RenderParameter, Object> renderParameters;
        ListModel listModel;
        
        // Create the render parameter map.
        renderParameters = new HashMap<>();
        renderParameters.put(RenderParameter.SELECTED, isSelected);
        renderParameters.put(RenderParameter.FOCUSED, cellHasFocus);
        renderParameters.put(RenderParameter.INDEX, index);
        
        // Get the row of data from the source table.
        listModel = list.getModel();
        dataRow = new HashMap<>(((T8DataEntity)listModel.getElementAt(index)).getFieldValues());
        
        // Set the editor component content and return it.
        rendererComponent.setRendererData(renderableComponent, dataRow, renderableComponent.getDisplayFieldIdentifier(), renderParameters);
        return (Component)rendererComponent;
    }
}
