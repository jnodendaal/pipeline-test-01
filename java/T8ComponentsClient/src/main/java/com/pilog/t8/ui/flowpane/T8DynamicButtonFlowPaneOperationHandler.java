package com.pilog.t8.ui.flowpane;

import com.pilog.t8.definition.ui.flowpane.T8DynamicButtonFlowPaneAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DynamicButtonFlowPaneOperationHandler extends T8ComponentOperationHandler
{
    private final T8DynamicButtonFlowPane flowPane;

    public T8DynamicButtonFlowPaneOperationHandler(T8DynamicButtonFlowPane flowPane)
    {
        super(flowPane);
        this.flowPane = flowPane;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8DynamicButtonFlowPaneAPIHandler.OPERATION_ADD_BUTTON))
        {
            String buttonIdentifier;
            String buttonText;
            String buttonToolTip;
            String buttonIconIdentifier;
            Boolean clearButtons;
            
            // Get the required operation parameters.
            buttonIdentifier = (String)operationParameters.get(T8DynamicButtonFlowPaneAPIHandler.PARAMETER_BUTTON_IDENTIFIER);
            buttonText = (String)operationParameters.get(T8DynamicButtonFlowPaneAPIHandler.PARAMETER_BUTTON_TEXT);
            buttonToolTip = (String)operationParameters.get(T8DynamicButtonFlowPaneAPIHandler.PARAMETER_BUTTON_TOOLTIP_TEXT);
            buttonIconIdentifier = (String)operationParameters.get(T8DynamicButtonFlowPaneAPIHandler.PARAMETER_BUTTON_ICON_IDENTIFIER);
            clearButtons = (Boolean)operationParameters.get(T8DynamicButtonFlowPaneAPIHandler.PARAMETER_CLEAR_BUTTONS);
            
            // Clear existing buttons if we need to.
            if ((clearButtons != null) && (clearButtons))
            {
                flowPane.clearButtons();
            }
            
            // Add the new button.
            flowPane.addButton(buttonIdentifier, buttonText, buttonToolTip, buttonIconIdentifier);
            return null;
        }
        else if (operationIdentifier.equals(T8DynamicButtonFlowPaneAPIHandler.OPERATION_REMOVE_BUTTON))
        {
            String buttonIdentifier;
            
            buttonIdentifier = (String)operationParameters.get(T8DynamicButtonFlowPaneAPIHandler.PARAMETER_BUTTON_IDENTIFIER);
            flowPane.removeButton(buttonIdentifier);
            return null;
        }
        else if (operationIdentifier.equals(T8DynamicButtonFlowPaneAPIHandler.OPERATION_CLEAR_BUTTONS))
        {
            flowPane.clearButtons();
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
