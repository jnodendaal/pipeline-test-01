package com.pilog.t8.ui.entityeditor.checkbox;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.ui.entityeditor.checkbox.T8CheckBoxDataEntityEditorDefinition;
import com.pilog.t8.ui.checkbox.T8CheckBox;
import java.util.Map;
import java.util.Objects;

/**
 * @author Hennie Brink
 */
public class T8CheckBoxDataEntityEditor extends T8CheckBox implements T8DataEntityEditorComponent
{
    private T8CheckBoxDataEntityEditorDefinition definition;
    private T8CheckBoxDataEntityEditorOperationHandler operationHandler;
    private T8DataEntity editorEntity;

    private Object selectedValue;
    private Object deselectedValue;
    private Object defaultValue;

    public T8CheckBoxDataEntityEditor(T8CheckBoxDataEntityEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.operationHandler = new T8CheckBoxDataEntityEditorOperationHandler(this);
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        super.initializeComponent(inputParameters);
        try
        {
            String selectedValueExpression;
            String deselectedValueExpression;
            String defaultValueExpression;
            ExpressionEvaluator evaluator;

            // Create an expression evaluator.
            evaluator = new ExpressionEvaluator();

            // Evaluate the selected value expression.
            selectedValueExpression = definition.getSelectedValueExpression();
            if (selectedValueExpression != null)
            {
                selectedValue = evaluator.evaluateExpression(selectedValueExpression, inputParameters, null);
            }
            else throw new RuntimeException("No selected value expression set in " + definition);

            // Evaluator the deselected value expression.
            deselectedValueExpression = definition.getDeselectedValueExpression();
            if (deselectedValueExpression != null)
            {
                deselectedValue = evaluator.evaluateExpression(deselectedValueExpression, inputParameters, null);
            }
            else throw new RuntimeException("No deselected value expression set in " + definition);

            // Evaluator the default value expression.
            defaultValueExpression = definition.getDefaultValueExpression();
            if (defaultValueExpression != null)
            {
                defaultValue = evaluator.evaluateExpression(defaultValueExpression, inputParameters, null);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while initializing component: " + definition, e);
        }
    }

    @Override
    public void setEditorDataEntity(T8DataEntity dataEntity)
    {
        editorEntity = dataEntity;
        if (editorEntity != null)
        {
            String fieldIdentifier;

            fieldIdentifier = definition.getEditorDataEntityFieldIdentifier();
            if (fieldIdentifier != null)
            {
                Object value;

                value = editorEntity.getFieldValue(fieldIdentifier);

                //First check if we should use a default value
                if(value == null && defaultValue != null) value = defaultValue;

                if (Objects.equals(value, selectedValue))
                {
                    setSelected(true);
                }
                else if (Objects.equals(value, deselectedValue))
                {
                    setSelected(false);
                }
                else
                {
                    throw new RuntimeException("Invalid data value encountered in checkbox editor: " + value);
                }
            }
            else throw new RuntimeException("Invalid field identifier specified for component " + definition.getIdentifier() + ": " + fieldIdentifier);
        }
        else setSelected(false);
    }

    @Override
    public T8DataEntity getEditorDataEntity()
    {
        return editorEntity;
    }

    @Override
    public boolean commitEditorChanges()
    {
        if (editorEntity != null)
        {
            String fieldIdentifier;

            fieldIdentifier = definition.getEditorDataEntityFieldIdentifier();
            if (fieldIdentifier != null)
            {
                editorEntity.setFieldValue(fieldIdentifier, isSelected());
                return true;
            }
            else throw new RuntimeException("No field identifier set in entity editor: " + definition);
        }
        else return true;
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return definition.getEditorDataEntityIdentifier();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return definition.getParentEditorIdentifier();
    }
}
