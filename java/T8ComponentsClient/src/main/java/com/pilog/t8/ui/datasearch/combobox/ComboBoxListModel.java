package com.pilog.t8.ui.datasearch.combobox;

import com.pilog.t8.definition.ui.datasearch.combobox.T8DataSearchComboBoxDataSourceDefinition;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * @author Hennie Brink
 */
public class ComboBoxListModel implements ListModel<ComboBoxModelNode>
{
    private final ComboBoxModelRootNode rootNode;
    private final List<ListDataListener> listDataListeners;
    private final CombBoxPagingNode pagingNode;

    private String searchQuery;
    private boolean onlyShowSelected;

    public ComboBoxListModel(ComboBoxModelRootNode rootNode)
    {
        this.rootNode = rootNode;
        this.listDataListeners = new ArrayList<>();
        this.onlyShowSelected = false;
        this.pagingNode = new CombBoxPagingNode("Get More Results...", "Retrieve additional data from the server");
    }

    public boolean isGroupNode(Object node)
    {
        return rootNode.getGroupNodes().contains(node);
    }

    public boolean isSingleSelectionMode()
    {
        return this.rootNode.getSelectionMode() == T8DataSearchComboBoxDataSourceDefinition.SelectionMode.SINGLE_SELECTION;
    }

    public boolean allowInstantSearch()
    {
        return this.rootNode.isCompleteDataSet();
    }

    public boolean isSearchVisible()
    {
        return this.rootNode.isSearchVisible();
    }

    public boolean isOptionsVisible()
    {
        return this.rootNode.isOptionsVisible();
    }

    public T8DataSearchComboBoxDataSourceDefinition.SelectionMode getSelectionMode()
    {
        return this.rootNode.getSelectionMode();
    }

    public void setSearchQuery(String searchQuery)
    {
        this.searchQuery = searchQuery;

        notifyListUpdated(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, getSize()));
    }

    public boolean isOnlyShowSelected()
    {
        return onlyShowSelected;
    }

    public void setOnlyShowSelected(boolean onlyShowSelected)
    {
        this.onlyShowSelected = onlyShowSelected;

        notifyListUpdated(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, getSize()));
    }

    public void clearCurrentSelections()
    {
        for (ComboBoxModelGroupNode groupNode : rootNode.getGroupNodes())
        {
            groupNode.setSelected(false);
        }
    }

    public void setNodeSelected(ComboBoxModelNode node, boolean selected)
    {
        switch(this.rootNode.getSelectionMode())
        {
            case MULTI_SELECTION:
                node.setSelected(selected);
                break;
            case SINGLE_SELECTION:
                clearCurrentSelections();

                if(!isGroupNode(node))
                    node.setSelected(selected);
                break;
            case CATEGORY_SELECTION:
                clearCurrentSelections();

                if(isGroupNode(node))
                {
                    node.setSelected(selected);
                }
                else
                {
                    groupNode:
                    for (ComboBoxModelGroupNode groupNode : rootNode.getGroupNodes())
                    {
                        for (ComboBoxModelItemNode node1 : groupNode.getNodes())
                        {
                            if(Objects.equals(node1, node))
                            {
                                groupNode.setSelected(selected);
                                break groupNode;
                            }
                        }
                    }
                }

                break;
            default: throw new AssertionError("Invalid selection mode");
        }


        if(onlyShowSelected)
            notifyListUpdated(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, getSize()));
    }

    public Set<Object> getSelectedValues()
    {
        Set<Object> selectedValues;

        selectedValues = new HashSet<>();

        for (ComboBoxModelGroupNode groupNode : rootNode.getGroupNodes())
        {
            for (ComboBoxModelItemNode node : groupNode.getNodes())
            {
                if(node.isSelected())
                {
                    selectedValues.add(node.getItemValue());
                }
            }
        }

        return selectedValues;
    }

    void setSelectedValues(List<Object> valueList)
    {
        if (valueList == null || valueList.isEmpty()) return;

        for (ComboBoxModelGroupNode groupNode : rootNode.getGroupNodes())
        {
            for (ComboBoxModelItemNode node : groupNode.getNodes())
            {
                if (valueList.contains(node.getItemValue())) setNodeSelected(node, true);
            }
        }
    }

    public int getNodesSize()
    {
        int size;

        size = 0;
        for (ComboBoxModelGroupNode groupNode : rootNode.getGroupNodes())
        {
            size += groupNode.getNodes().size();
        }

        return size;
    }

    @Override
    public int getSize()
    {
        int size;

        size = 0;
        for (ComboBoxModelGroupNode groupNode : rootNode.getGroupNodes())
        {
            boolean hasSearchMatch;

            hasSearchMatch = false;

            for (ComboBoxModelItemNode node : groupNode.getNodes())
            {
                if (searchQuery == null || node.getDisplayString().toUpperCase().contains(searchQuery.toUpperCase()))
                {
                    if(onlyShowSelected && !node.isSelected())
                        continue;

                    size++;
                    hasSearchMatch = true;
                }
            }

            if (hasSearchMatch)
            {
                size++;
            }
        }

        if(!rootNode.isCompleteDataSet() && !onlyShowSelected && getNodesSize() < 1000)
            size++;

        return size;
    }

    @Override
    public ComboBoxModelNode getElementAt(int index)
    {
        ComboBoxModelNode lastMatch = null; //Store the last match because we need to traverse all of the nodes to get the correct index
        int currentIndex;

        currentIndex = -1;
        for (ComboBoxModelGroupNode groupNode : rootNode.getGroupNodes())
        {
            boolean hasSearchMatch;
            currentIndex++;

            hasSearchMatch = false;
            if (currentIndex == index)
            {
                lastMatch = groupNode;
            }

            for (ComboBoxModelItemNode node : groupNode.getNodes())
            {
                if (searchQuery == null || node.getDisplayString().toUpperCase().contains(searchQuery.toUpperCase()))
                {
                    if(onlyShowSelected && !node.isSelected())
                        continue;

                    hasSearchMatch = true;
                    currentIndex++;
                    if (currentIndex == index)
                    {
                        lastMatch = node;
                    }
                }
            }

            if(!hasSearchMatch) currentIndex--;
        }

        if(!rootNode.isCompleteDataSet() && !onlyShowSelected && getNodesSize() < 1000)
        {
            currentIndex++;

            if(currentIndex == index)
                lastMatch = pagingNode;
        }

        return lastMatch;
    }

    @Override
    public void addListDataListener(ListDataListener l)
    {
        listDataListeners.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l)
    {
        listDataListeners.remove(l);
    }

    private void notifyListUpdated(ListDataEvent event)
    {
        for (int i = listDataListeners.size() - 1; i >= 0; i--)
        {
            listDataListeners.get(i).contentsChanged(event);
        }
    }
}
