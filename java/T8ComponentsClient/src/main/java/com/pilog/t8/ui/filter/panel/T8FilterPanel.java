package com.pilog.t8.ui.filter.panel;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.filter.panel.T8FilterPanelAPIHandler;
import com.pilog.t8.definition.ui.filter.panel.T8FilterPanelDefinition;
import com.pilog.t8.definition.ui.panel.T8PanelSlotDefinition;
import com.pilog.t8.ui.panel.T8Panel;
import com.pilog.t8.ui.panel.T8PanelSlot;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.components.layout.WrapLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.util.HashMap;
import java.util.Map;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanel extends T8Panel implements ChangeListener
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8FilterPanel.class.getName());
    T8FilterPanelDefinition componentDefinition;
    private T8DataFilter preFilter;

    public T8FilterPanel(T8FilterPanelDefinition componentDefinition, T8ComponentController controller)
    {
        super(componentDefinition, controller);
        this.componentDefinition = componentDefinition;

        setLayoutManager();
    }

    private void setLayoutManager()
    {
        switch(componentDefinition.getLayoutManager())
        {
            case WRAPPED_FLOW:
                getContentContainer().setLayout(new WrapLayout(WrapLayout.LEFT));
                break;
            default:
                getContentContainer().setLayout(new GridBagLayout());
        }
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (T8FilterPanelAPIHandler.OPERATION_CREATE_FILTER.toString().equals(operationIdentifier))
        {
            T8DataFilter filter = createFilter();
            return HashMaps.newHashMap(T8FilterPanelAPIHandler.PARAMETER_FILTER, filter);
        }
        else if (T8FilterPanelAPIHandler.OPERATION_SET_PRE_FILTER.toString().equals(operationIdentifier))
        {
            Object newFilter = operationParameters.get(T8FilterPanelAPIHandler.PARAMETER_FILTER);
            if (newFilter != null && newFilter instanceof T8DataFilter)
            {
                preFilter = (T8DataFilter) newFilter;
            }
            else
            {
                LOGGER.log(T8Logger.Level.ERROR, "Invalid object sent to with operation " + T8FilterPanelAPIHandler.OPERATION_SET_PRE_FILTER + ". Expecting object of type T8DataFilter but found " + newFilter);
            }
            return null;
        }
        else if (T8FilterPanelAPIHandler.OPERATION_ADD_PRE_FILTER_CRITERIA.toString().equals(operationIdentifier))
        {
            Object filterCriteria = operationParameters.get(T8FilterPanelAPIHandler.PARAMETER_FILTER_CRITERIA);
            Object filter = operationParameters.get(T8FilterPanelAPIHandler.PARAMETER_FILTER);
            if (!((filter != null) ^ (filterCriteria != null)))
            {
                LOGGER.log(T8Logger.Level.ERROR, "Invalid Parameters received on operation " + T8FilterPanelAPIHandler.OPERATION_ADD_PRE_FILTER_CRITERIA + "."
                                                 + "Only the criteria or the filter can be provided and not both, and atleast one must be provided.");
                return null;
            }

            if (filterCriteria != null && filterCriteria instanceof T8DataFilterCriteria)
            {
                preFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, (T8DataFilterCriteria) filterCriteria);
            }
            else
            {
                if(filter == null)
                    LOGGER.log(T8Logger.Level.ERROR, "Invalid object sent to with operation " + T8FilterPanelAPIHandler.OPERATION_ADD_PRE_FILTER_CRITERIA + ". Expecting object of type T8DataFilter or T8DataFilterCriteria but found " + filterCriteria);
            }


            if (filter != null && filter instanceof T8DataFilter)
            {
                preFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, (T8DataFilter) filter);
            }
            else
            {
                if(filter == null)
                    LOGGER.log(T8Logger.Level.ERROR, "Invalid object sent to with operation " + T8FilterPanelAPIHandler.OPERATION_ADD_PRE_FILTER_CRITERIA + ". Expecting object of type T8DataFilter or T8DataFilter but found " + filterCriteria);
            }

            return null;
        }
        else if (T8FilterPanelAPIHandler.OPERATION_REFRESH.equals(operationIdentifier))
        {
            refreshFilterComponents();
            return null;
        }
        else
        {
            return super.executeOperation(operationIdentifier, operationParameters != null ? operationParameters : new HashMap<>());
        }
    }

    @Override
    public void initializeComponent(
            Map<String, Object> inputParameters)
    {
        super.initializeComponent(inputParameters);

        if (componentDefinition.getPreFilter() != null)
        {
            preFilter = componentDefinition.getPreFilter().getNewDataFilterInstance(controller.getContext(), null);
        }
        else
        {
            preFilter = new T8DataFilter(componentDefinition.getDataEntityDefinitionIdentifier());
        }
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        String componentIdentifier;

        componentIdentifier = childComponent.getComponentDefinition().getIdentifier();

        if(childComponent instanceof T8PanelSlot)
        {
            T8PanelSlotDefinition slotDefinition;
            Dimension minimumDimensions;
            Dimension preferredDimensions;

            slotDefinition = (T8PanelSlotDefinition)childComponent.getComponentDefinition();
            minimumDimensions = slotDefinition.getMinimumDimensions();
            preferredDimensions = slotDefinition.getPreferredDimensions();

            switch(componentDefinition.getLayoutManager())
            {
                case WRAPPED_FLOW:
                    childComponents.put(componentIdentifier, childComponent);
                    if ((minimumDimensions != null) && (minimumDimensions.height > 0) && (minimumDimensions.width > 0))
                    {
                        ((Component)childComponent).setMinimumSize(minimumDimensions);
                    }
                    else
                    {
                        ((Component)childComponent).setMinimumSize(null);
                    }

                    if ((preferredDimensions != null) && (preferredDimensions.height > 0) && (preferredDimensions.width > 0))
                    {
                        ((Component)childComponent).setPreferredSize(preferredDimensions);
                    }
                    else
                    {
                        ((Component)childComponent).setPreferredSize(null);
                    }

                    getContentContainer().add((Component)childComponent);
                    validate();
                    break;

                default:
                    super.addChildComponent(childComponent, constraints);
            }
        }
    }


    private void refreshFilterComponents()
    {
        for (T8Component t8Component : getChildComponents())
        {
            if (t8Component instanceof T8PanelSlot)
            {
                T8PanelSlot slot = (T8PanelSlot) t8Component;
                if (slot.getChildComponentCount() > 0)
                {
                    for (T8Component slotComponent : slot.getChildComponents())
                    {
                        if (slotComponent instanceof T8FilterPanelComponent)
                        {
                            T8FilterPanelComponent component = (T8FilterPanelComponent) slotComponent;
                            try
                            {
                                component.refresh();
                            }
                            catch (Exception ex)
                            {
                                LOGGER.log("Failed to refresh component " + component, ex);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void startComponent()
    {
        super.startComponent(); //To change body of generated methods, choose Tools | Templates.
        //Add state listeners on sub components
        for (T8Component t8Component : getChildComponents())
        {
            if (t8Component instanceof T8PanelSlot)
            {
                T8PanelSlot slot = (T8PanelSlot) t8Component;
                if (slot.getChildComponentCount() > 0)
                {
                    for (T8Component slotComponent : slot.getChildComponents())
                    {
                        if (slotComponent instanceof T8FilterPanelComponent)
                        {
                            T8FilterPanelComponent component = (T8FilterPanelComponent) slotComponent;
                            component.addChangeListener(this);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void stopComponent()
    {
        super.stopComponent();
        //Remove state listeners on sub components
        for (T8Component t8Component : getChildComponents())
        {
            if (t8Component instanceof T8PanelSlot)
            {
                T8PanelSlot slot = (T8PanelSlot) t8Component;
                if (slot.getChildComponentCount() > 0)
                {
                    for (T8Component slotComponent : slot.getChildComponents())
                    {
                        if (slotComponent instanceof T8FilterPanelComponent)
                        {
                            T8FilterPanelComponent component = (T8FilterPanelComponent) slotComponent;
                            component.removeChangeListener(this);
                        }
                    }
                }
            }
        }
    }

    public T8DataFilter createFilter()
    {
        T8DataFilter filterCopy = preFilter.copy();
        for (T8Component t8Component : getChildComponents())
        {
            if (t8Component instanceof T8PanelSlot)
            {
                T8PanelSlot slot = (T8PanelSlot) t8Component;
                if (slot.getChildComponentCount() > 0)
                {
                    for (T8Component slotComponent : slot.getChildComponents())
                    {
                        if (slotComponent instanceof T8FilterPanelComponent)
                        {
                            T8FilterPanelComponent component = (T8FilterPanelComponent) slotComponent;
                            T8DataFilterCriteria componentCriteria = component.getFilterCriteria();
                            if (componentCriteria != null && componentCriteria.hasCriteria())
                            {
                                filterCopy.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, componentCriteria);
                            }
                        }
                    }
                }
            }
        }
        return filterCopy;
    }

    @Override
    public void stateChanged(ChangeEvent e)
    {
        Map<String, Object> eventParams = new HashMap<>(1);
        eventParams.put(T8FilterPanelAPIHandler.PARAMETER_FILTER, createFilter());
        this.controller.reportEvent(this, this.componentDefinition.getComponentEventDefinition(T8FilterPanelAPIHandler.EVENT_FILTER_CREATED), eventParams);

    }
}
