package com.pilog.t8.ui.filter.panel.datasource;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.filter.panel.datasource.T8FilterPanelListDataSourceDefinition;
import com.pilog.t8.definition.ui.filter.panel.datasource.T8FilterPanelListItemDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItem;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItemDataSource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelListDataSource implements T8FilterPanelListComponentItemDataSource
{
    List<T8FilterPanelListItemDefinition> itemsList;
    Iterator<T8FilterPanelListItemDefinition> itemsIterator;
    T8FilterPanelListDataSourceDefinition definition;

    public T8FilterPanelListDataSource(T8FilterPanelListDataSourceDefinition definition)
    {
        this.definition = definition;
    }

    @Override
    public T8FilterPanelListComponentItem next()
    {
        T8FilterPanelListItemDefinition nextItemObject = itemsIterator.next();
        if(nextItemObject == null)
            return new T8FilterPanelListComponentItem(definition.getNullValueDisplayName()==null?" ":definition.getNullValueDisplayName(), null, definition.getNullValueIconDefinition());
        return new T8FilterPanelListComponentItem(nextItemObject);
    }

    @Override
    public boolean hasNext()
    {
        return itemsIterator.hasNext();
    }

    @Override
    public void initialize(T8Context context, Map<String, Object> inputParameters)
    {
        this.itemsList = new ArrayList<>(0);
        if(definition.isAddNullValueOn())
            itemsList.add(null);
        if(definition.getItems() != null)
            itemsList.addAll(definition.getItems());
        this.itemsIterator = itemsList.iterator();
    }

    @Override
    public void setNewDataSourceFilter(T8DataFilter dataFilter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported on the List data source.");
    }

}
