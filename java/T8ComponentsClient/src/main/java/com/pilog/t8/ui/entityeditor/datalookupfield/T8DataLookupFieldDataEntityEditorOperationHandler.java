package com.pilog.t8.ui.entityeditor.datalookupfield;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.ui.entityeditor.textfield.T8TextFieldDataEntityEditorAPIHandler;
import com.pilog.t8.ui.datalookupfield.T8DataLookupFieldOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupFieldDataEntityEditorOperationHandler extends T8DataLookupFieldOperationHandler
{
    private T8DataLookupFieldDataEntityEditor lookupField;

    public T8DataLookupFieldDataEntityEditorOperationHandler(T8DataLookupFieldDataEntityEditor lookupField)
    {
        super(lookupField);
        this.lookupField = lookupField;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_COMMIT_CHANGES))
        {
            lookupField.commitEditorChanges();
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_SET_DATA_ENTITY))
        {
            lookupField.setEditorDataEntity((T8DataEntity)operationParameters.get(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY));
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_GET_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY, lookupField.getEditorDataEntity());
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
