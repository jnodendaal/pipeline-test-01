package com.pilog.t8.ui.functionality.view;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.ui.functionality.T8FunctionalityController;
import com.pilog.t8.ui.functionality.T8FunctionalityView;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.functionality.view.T8FunctionalityViewPaneDefinition;
import com.pilog.t8.security.T8Context.T8ProjectContext;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityViewPane extends JComponent implements T8Component, T8FunctionalityController
{
    protected T8FunctionalityViewPaneDefinition definition;
    protected T8FunctionalityViewPaneOperationHandler operationHandler;
    protected FunctionalityViewPane viewPane;
    protected T8ComponentController parentController;
    protected T8DefaultComponentController controller;

    public T8FunctionalityViewPane(T8FunctionalityViewPaneDefinition definition, T8ComponentController parentController)
    {
        this.definition = definition;
        this.parentController = parentController;
        this.controller = new T8DefaultComponentController(parentController, new T8ProjectContext(controller.getContext(), definition.getRootProjectId()), definition.getIdentifier(), false);
        this.controller.setFunctionalityController(this);
        this.operationHandler = new T8FunctionalityViewPaneOperationHandler(this);
        this.viewPane = new FunctionalityViewPane(controller);
        setOpaque(false);
        setLayout(new BorderLayout());
        setupPainters();
        add(viewPane, BorderLayout.CENTER);
    }

    private void setupPainters()
    {
        T8PainterDefinition navBarBackgroundPainterDefinition;

        navBarBackgroundPainterDefinition = definition.getNavigationBarBackgroundPainterDefinition();
        if (navBarBackgroundPainterDefinition != null)
        {
            viewPane.setBackgroundPainter(new T8PainterAdapter(navBarBackgroundPainterDefinition.getNewPainterInstance()));
        }
    }

    @Override
    public T8FunctionalityView getFunctionalityView(String functionalityIid)
    {
        return viewPane.getFunctionalityView(functionalityIid);
    }

    @Override
    public T8FunctionalityAccessHandle accessFunctionality(String functionalityId, Map<String, Object> parameters) throws Exception
    {
        return viewPane.accessFunctionality(functionalityId, parameters);
    }

    @Override
    public T8ComponentController getController()
    {
        return parentController;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        viewPane.closeAllFunctionalityViews();
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public T8ComponentController getFunctionalityComponentController()
    {
        return controller;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
