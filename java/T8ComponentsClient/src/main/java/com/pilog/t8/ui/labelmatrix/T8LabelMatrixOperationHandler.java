package com.pilog.t8.ui.labelmatrix;

import com.pilog.t8.definition.ui.labelmatrix.T8LabelMatrixAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8LabelMatrixOperationHandler extends T8ComponentOperationHandler
{
    private T8LabelMatrix matrix;

    public T8LabelMatrixOperationHandler(T8LabelMatrix matrix)
    {
        super(matrix);
        this.matrix = matrix;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8LabelMatrixAPIHandler.OPERATION_CLEAR))
        {
            matrix.clear();
            return null;
        }
        else if (operationIdentifier.equals(T8LabelMatrixAPIHandler.OPERATION_ADD_LABEL_COLUMN))
        {
            matrix.addColumn(convertToStringList((List<Object>)operationParameters.get(T8LabelMatrixAPIHandler.PARAMETER_TEXT_LIST)));
            return null;
        }
        else if(operationIdentifier.equals(T8LabelMatrixAPIHandler.OPERATION_ADD_LABEL_ROW))
        {
            matrix.addRow(convertToStringList((List<Object>)operationParameters.get(T8LabelMatrixAPIHandler.PARAMETER_TEXT_LIST)));
            return null;
        }
        else if(operationIdentifier.equals(T8LabelMatrixAPIHandler.OPERATION_GET_TEXT))
        {
            String text;
            
            text = matrix.getText((Integer)operationParameters.get(T8LabelMatrixAPIHandler.PARAMETER_ROW_INDEX), (Integer)operationParameters.get(T8LabelMatrixAPIHandler.PARAMETER_COLUMN_INDEX));
            return HashMaps.newHashMap(T8LabelMatrixAPIHandler.PARAMETER_TEXT, text);
        }
        else if(operationIdentifier.equals(T8LabelMatrixAPIHandler.OPERATION_SET_TEXT))
        {
            Object text;
            
            text = (Integer)operationParameters.get(T8LabelMatrixAPIHandler.PARAMETER_ROW_INDEX);
            if ((text != null) && !(text instanceof String)) text = text.toString();
            matrix.setText((Integer)operationParameters.get(T8LabelMatrixAPIHandler.PARAMETER_ROW_INDEX), (Integer)operationParameters.get(T8LabelMatrixAPIHandler.PARAMETER_COLUMN_INDEX), (String)text);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
    
    private List<String> convertToStringList(List<Object> objectList)
    {
        ArrayList<String> stringList;
        
        stringList = new ArrayList<>();
        if (objectList != null)
        {
            for (Object object : objectList)
            {
                stringList.add(object != null ? object.toString() : null);
            }
        }
        
        return stringList;
    }
}
