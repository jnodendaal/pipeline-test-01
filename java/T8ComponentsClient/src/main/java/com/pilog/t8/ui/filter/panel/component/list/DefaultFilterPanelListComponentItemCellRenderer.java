/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.filter.panel.component.list;

import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItem;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class DefaultFilterPanelListComponentItemCellRenderer extends JXPanel implements ListCellRenderer<T8FilterPanelListComponentItem>
{
    private ListSelectionModel selectionModel;
    protected JXLabel label;
    private JCheckBox checkBox;

    public DefaultFilterPanelListComponentItemCellRenderer(
            ListSelectionModel selectionModel)
    {
        setLayout(new GridBagLayout());
        this.selectionModel = selectionModel;
        label = new JXLabel();
        checkBox = new JCheckBox();
        label.setTextAlignment(JXLabel.TextAlignment.LEFT);
        checkBox.setOpaque(false);
        label.setOpaque(false);

        add(checkBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(1, 1, 1, 1), 0, 0));
        add(label, new GridBagConstraints(2, 0, 1, 1, 0.1, 0.1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(1, 1, 1, 1), 0, 0));
    }

    public DefaultFilterPanelListComponentItemCellRenderer()
    {
        this(null);
    }

    public void setSelectionModel(ListSelectionModel selectionModel)
    {
        this.selectionModel = selectionModel;
    }

    @Override
    public JComponent getListCellRendererComponent(JList list,
                                                   T8FilterPanelListComponentItem value,
                                                   int index,
                                                   boolean isSelected,
                                                   boolean cellHasFocus)
    {
        if(value == null)
        {
            setVisible(false);
            return this;
        }

        if (isSelected)
        {
            setBackground(new Color(200, 200, 200, 50));
            setOpaque(true);
        }
        else
        {
            setOpaque(false);
        }

        label.setText(value.getDisplayName());
        if(selectionModel != null)
            checkBox.setSelected(selectionModel.isSelectedIndex(index));

        setVisible(value.getVisible());
        label.setVisible(value.getVisible());
        checkBox.setVisible(value.getVisible() && selectionModel != null);

        if (value.getIcon() != null)
        {
            label.setIcon(value.getIcon().getImage().getImageIcon());
            label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        }
        else
        {
            label.setIcon(null);
            label.setBorder(BorderFactory.createEmptyBorder(0, 16 + label.getIconTextGap(), 0, 0));
        }
        return this;
    }
}