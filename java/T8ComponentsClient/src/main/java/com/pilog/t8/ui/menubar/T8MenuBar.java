package com.pilog.t8.ui.menubar;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.menubar.T8MenuBarDefinition;
import com.pilog.t8.definition.ui.menubar.T8MenuBarDefinition.BorderType;
import com.pilog.t8.ui.menu.T8Menu;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.border.EmptyBorder;
import org.jdesktop.swingx.border.DropShadowBorder;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8MenuBar extends JMenuBar implements T8Component
{
    private final T8MenuBarDefinition definition;
    private final T8ComponentController controller;
    private final T8MenuBarOperationHandler operationHandler;
    private Painter backgroundPainter;

    public T8MenuBar(T8MenuBarDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8MenuBarOperationHandler(this);
        this.setOpaque(definition.isOpaque());
        setupComponent();
        setupPainters();
    }

    private void setupComponent()
    {
        int height;

        height = definition.getHeight();
        if (height > 0)
        {
            Dimension preferredSize;

            preferredSize = this.getPreferredSize();
            preferredSize.height = height;
            setPreferredSize(preferredSize);
        }
    }

    private void setupPainters()
    {
        T8PainterDefinition painterDefinition;

        // Initialize painter for painting the background of the entire component.
        painterDefinition = definition.getBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            backgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        BorderType borderType;

        borderType = definition.getBorderType();
        if (borderType == BorderType.BEVEL)
        {
            setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        }
        else if (borderType == BorderType.ETCHED)
        {
            setBorder(javax.swing.BorderFactory.createEtchedBorder());
        }
        else if (borderType == BorderType.LINE)
        {
            setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        }
        else if (borderType == BorderType.SHADOW)
        {
            setBorder(new DropShadowBorder());
        }
        else
        {
            setBorder(new EmptyBorder(0, 0, 0, 0));
        }
        setToolTipText(definition.getTooltipText());

        if (definition.getLayoutManagerDefinition() != null)
        {
            try
            {
                setLayout(definition.getLayoutManagerDefinition().constructLayoutManager());
            }
            catch(Exception ex)
            {
                T8Log.log("Failed to construct layout manager " + definition.getPublicIdentifier(), ex);
            }
        }
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        if (component instanceof T8Menu)
        {
            T8Menu menu;

            menu = (T8Menu)component;
            add(menu);
        }
        else if (component instanceof JMenu)
        {
            add((JMenu)component);
        }
        else throw new RuntimeException("Invalid child component type: " + component.getClass());
    }

    @Override
    public T8Component getChildComponent(String componentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String componentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public Dimension getMinimumSize()
    {
        return getPreferredSize();
    }

    /**
     * Overridden to provide Painter support.  Calls backgroundPainter.paint()
     * if it is not null, else calls super.paintComponent().
     */
    @Override
    protected void paintComponent(Graphics g)
    {
        if (backgroundPainter != null)
        {
            Graphics2D g2;
            Insets borderInsets;
            int contentWidth;
            int contentHeight;

            if (isOpaque())
            {
                super.paintComponent(g);
            }

            // Create a graphics context for the painter to use and discard it afterwards.
            borderInsets = this.getBorder().getBorderInsets(this);
            contentWidth = this.getWidth() - borderInsets.left - borderInsets.right;
            contentHeight = this.getHeight() - borderInsets.top - borderInsets.bottom;
            g2 = (Graphics2D)g.create(borderInsets.left, borderInsets.top, contentWidth, contentHeight);
            backgroundPainter.paint(g2, this, contentWidth, contentHeight);
            g2.dispose();
        }
        else
        {
            super.paintComponent(g);
        }
    }
}
