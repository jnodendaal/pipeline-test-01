package com.pilog.t8.ui.menuitem;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.menuitem.T8MenuItemAPIHandler;
import com.pilog.t8.definition.ui.menuitem.T8MenuItemDefinition;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JMenuItem;

/**
 * @author Bouwer du Preez
 */
public class T8MenuItem extends JMenuItem implements T8Component
{
    private final T8ComponentController controller;
    private final T8MenuItemDefinition definition;
    private final T8MenuItemOperationHandler operationHandler;

    public T8MenuItem(T8MenuItemDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8MenuItemOperationHandler(this);
        this.addActionListener(new MenuItemActionListener());
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8IconDefinition iconDefinition;

        iconDefinition = definition.getIconDefinition();
        if (iconDefinition != null)
        {
            this.setIcon(iconDefinition.getImage().getImageIcon());
        }

        this.setText(definition.getText());
        this.setOpaque(this.definition.isOpaque());
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public boolean addSeperator()
    {
        return definition.addSeperator();
    }

    private class MenuItemActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            HashMap<String, Object> eventParameters;

            eventParameters = new HashMap<>();
            eventParameters.put(T8MenuItemAPIHandler.PARAMETER_MENU_ITEM_IDENTIFIER, definition.getIdentifier());
            controller.reportEvent(T8MenuItem.this, definition.getComponentEventDefinition(T8MenuItemAPIHandler.EVENT_MENU_ITEM_SELECTED), eventParameters);
        }
    }
}
