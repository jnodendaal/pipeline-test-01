package com.pilog.t8.ui.table.object;

import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataObjectClientApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.definition.ui.table.object.T8TableEntityMappedObjectProviderDefinition;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8Table;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8TableEntityMappedObjectProvider implements T8TableDataObjectProvider
{
    private final T8TableEntityMappedObjectProviderDefinition definition;
    private final T8ComponentController controller;
    private final String keyFieldId;
    private T8Table table;

    public T8TableEntityMappedObjectProvider(T8TableEntityMappedObjectProviderDefinition definition, T8ComponentController controller)
    {
        this.controller = controller;
        this.definition = definition;
        this.keyFieldId = definition.getKeyFieldId();
    }

    @Override
    public void setTable(T8Table table)
    {
        this.table = table;
    }

    @Override
    public boolean providesDataObject(String objectId)
    {
        return Objects.equals(objectId, definition.getDataObjectId());
    }

    @Override
    public T8DataObject getDataObject(String objectId)
    {
        if (providesDataObject(objectId))
        {
            T8DataEntity selectedEntity;

            selectedEntity = table.getSelectedDataEntity();
            if (selectedEntity != null)
            {
                String key;

                // Get the data object key from the selected entity.
                key = (String)selectedEntity.getFieldValue(keyFieldId);

                try
                {
                    T8DataObjectClientApi objApi;

                    // Get the object api and retrieve the required object using the key value from the entity.
                    objApi = controller.getApi(T8DataObjectClientApi.API_IDENTIFIER);
                    return objApi.retrieve(objectId, key, true);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while retrieving data object: " + objectId + ":" + key, e);
                    return null;
                }
            }
            else return null;
        }
        else return null;
    }
}
