package com.pilog.t8.ui.datumeditor.datacombobox;

import com.pilog.t8.ui.componentcontroller.T8DefaultComponentController;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.ui.event.T8ComponentEvent;
import com.pilog.t8.ui.event.T8ComponentEventListener;
import com.pilog.t8.ui.event.T8DefinitionDatumEditorListener;
import com.pilog.t8.definition.ui.T8ComponentEventDefinition;
import com.pilog.t8.definition.ui.datacombobox.T8DataComboBoxAPIHandler;
import com.pilog.t8.definition.ui.datumeditor.datacombobox.T8SingleValueDataComboBoxDatumEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datacombobox.T8DataComboBox;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;

/**
 * @author Bouwer du Preez
 */
public class T8SingleValueDataComboBoxDatumEditor extends JPanel implements T8DefinitionDatumEditor
{
    private final T8SingleValueDataComboBoxDatumEditorDefinition editorDefinition;
    private final T8ComponentController controller;
    private final T8DefinitionContext definitionContext;
    private final T8Definition targetDefinition;
    private final T8DefinitionDatumType datumType;
    private T8DataComboBox comboBox;

    public T8SingleValueDataComboBoxDatumEditor(T8DefinitionContext definitionContext, T8SingleValueDataComboBoxDatumEditorDefinition editorDefinition, T8Definition targetDefinition, T8DefinitionDatumType datumType) throws Exception
    {
        this.editorDefinition = editorDefinition;
        this.controller = new T8DefaultComponentController(new T8Context(definitionContext.getClientContext(), definitionContext.getSessionContext()).setProjectId(editorDefinition.getRootProjectId()), editorDefinition.getIdentifier(), false);
        this.controller.addComponentEventListener(new SelectionEventListener());
        this.definitionContext = definitionContext;
        this.targetDefinition = targetDefinition;
        this.datumType = datumType;
        initComponents();
        initEditor();
    }

    private void initEditor() throws Exception
    {
        comboBox = (T8DataComboBox)editorDefinition.getNewComponentInstance(controller);
        controller.registerComponent(comboBox);
        add(comboBox, java.awt.BorderLayout.CENTER);
    }

    protected void setDefinitionDatum(Object value)
    {
        targetDefinition.setDefinitionDatum(datumType.getIdentifier(), value, this);
    }

    protected Object getDefinitionDatum()
    {
        return targetDefinition.getDefinitionDatum(datumType.getIdentifier());
    }

    private void commitValue()
    {
        T8DataEntity selectedEntity;

        selectedEntity = comboBox.getSelectedDataEntity();
        setDefinitionDatum(selectedEntity != null ? selectedEntity.getFieldValue(editorDefinition.getValueFieldIdentifier()) : null);
    }

    @Override
    public void initializeComponent()
    {
        comboBox.initializeComponent(null);
    }

    @Override
    public void startComponent()
    {
        comboBox.startComponent();
        refreshEditor();
    }

    @Override
    public void stopComponent()
    {
        comboBox.stopComponent();
    }

    @Override
    public void commitChanges()
    {
        commitValue();
    }

    @Override
    public void refreshEditor()
    {
        Map<String, Object> key;

        key = new HashMap<>();
        key.put(editorDefinition.getValueFieldIdentifier(), getDefinitionDatum());
        comboBox.setSelectedKey(key);
    }

    @Override
    public void setEditable(boolean editable)
    {
        comboBox.setEnabled(editable);
    }

    @Override
    public T8ClientContext getClientContext()
    {
        return definitionContext.getClientContext();
    }

    @Override
    public T8Context getContext()
    {
        return definitionContext.getContext();
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return definitionContext.getSessionContext();
    }

    @Override
    public T8DefinitionContext getDefinitionContext()
    {
        return definitionContext;
    }

    @Override
    public String getDatumIdentifier()
    {
        return datumType.getIdentifier();
    }

    @Override
    public T8DefinitionDatumType getDatumType()
    {
        return datumType;
    }

    @Override
    public T8Definition getDefinition()
    {
        return targetDefinition;
    }

    @Override
    public void addDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
    }

    @Override
    public void removeDefinitionDatumEditorListener(T8DefinitionDatumEditorListener listener)
    {
    }

    private class SelectionEventListener implements T8ComponentEventListener
    {
        @Override
        public void componentEvent(T8ComponentEvent event)
        {
            T8ComponentEventDefinition eventDefinition;

            eventDefinition = event.getEventDefinition();
            if (eventDefinition.getIdentifier().equals(T8DataComboBoxAPIHandler.EVENT_SELECTION_CHANGE))
            {
                commitValue();
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
