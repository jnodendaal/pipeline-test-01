package com.pilog.t8.ui.tabbedpane;

import com.pilog.t8.definition.ui.tabbedpane.T8TabbedPaneAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8TabbedPaneOperationHandler extends T8ComponentOperationHandler
{
    private final T8TabbedPane tabbedPane;

    public T8TabbedPaneOperationHandler(T8TabbedPane tabbedPane)
    {
        super(tabbedPane);
        this.tabbedPane = tabbedPane;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, final Map<String, Object> operationParameters)
    {
        try
        {
            if (T8TabbedPaneAPIHandler.OPERATION_SET_SELECTED_TAB.equals(operationIdentifier))
            {
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        String tabIdentifier;

                        tabIdentifier = (String)operationParameters.get(T8TabbedPaneAPIHandler.PARAMETER_TAB_IDENTIFIER);
                        tabbedPane.setSelectedTab(tabIdentifier);
                    }
                });

                return null;
            }
            else if (T8TabbedPaneAPIHandler.OPERATION_SET_TAB_VISIBLE.equals(operationIdentifier))
            {
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        String tabIdentifier;
                        Boolean visible;

                        tabIdentifier = (String)operationParameters.get(T8TabbedPaneAPIHandler.PARAMETER_TAB_IDENTIFIER);
                        visible = (Boolean)operationParameters.get(T8TabbedPaneAPIHandler.PARAMETER_VISIBLE);
                        if (visible == null) visible = false;

                        tabbedPane.setTabVisible(tabIdentifier, visible);
                    }
                });

                return null;
            }
            else if (T8TabbedPaneAPIHandler.OPERATION_SET_TAB_ENABLED.equals(operationIdentifier))
            {
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        String tabIdentifier;
                        Boolean enabled;

                        tabIdentifier = (String)operationParameters.get(T8TabbedPaneAPIHandler.PARAMETER_TAB_IDENTIFIER);
                        enabled = (Boolean)operationParameters.get(T8TabbedPaneAPIHandler.PARAMETER_ENABLED);
                        if (enabled == null) enabled = false;

                        tabbedPane.setTabEnabled(tabIdentifier, enabled);
                    }
                });

                return null;
            }
            else if (T8TabbedPaneAPIHandler.OPERATION_SET_TAB_TITLE.equals(operationIdentifier))
            {
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        String tabIdentifier;
                        String tabTitle;

                        tabIdentifier = (String)operationParameters.get(T8TabbedPaneAPIHandler.PARAMETER_TAB_IDENTIFIER);
                        tabTitle = (String)operationParameters.get(T8TabbedPaneAPIHandler.PARAMETER_TEXT);
                        tabbedPane.setTabTitle(tabIdentifier, tabTitle);
                    }
                });

                return null;
            }
            else if (T8TabbedPaneAPIHandler.OPERATION_GET_SELECTED_TAB.equals(operationIdentifier))
            {
                final HashMap<String, Object> result;

                result = HashMaps.createSingular(T8TabbedPaneAPIHandler.PARAMETER_TAB_IDENTIFIER, null);
                SwingUtilities.invokeAndWait(() -> result.put(T8TabbedPaneAPIHandler.PARAMETER_TAB_IDENTIFIER, tabbedPane.getSelectedTabIdentifier()));

                return result;
            }
            else return super.executeOperation(operationIdentifier, operationParameters);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while executing operation: " + operationIdentifier + " using parameters: " + operationParameters, e);
        }
    }
}
