package com.pilog.t8.ui.table;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.client.T8DataManagerOperationHandler;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.table.T8TableAPIHandler;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import java.util.HashMap;
import java.util.List;

import static com.pilog.t8.definition.data.T8DataManagerResource.OPERATION_COUNT_DATA_ENTITIES;
import static com.pilog.t8.definition.data.T8DataManagerResource.PARAMETER_DATA_ENTITY_COUNT;
import static com.pilog.t8.definition.data.T8DataManagerResource.PARAMETER_DATA_ENTITY_IDENTIFIER;
import static com.pilog.t8.definition.data.T8DataManagerResource.PARAMETER_DATA_FILTER;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8TableDataHandler
{
    private final T8TableDefinition tableDefinition;
    private final T8Table t8Table;
    private final int pageSize;
    private int pageOffset;
    private int rowCount;

    public T8TableDataHandler(T8Table parentTable, T8TableDefinition tableDefinition)
    {
        this.t8Table = parentTable;
        this.tableDefinition = tableDefinition;
        this.pageOffset = 0;
        this.pageSize = tableDefinition.getPageSize();
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public void resetPageOffset()
    {
        pageOffset = 0;
    }

    public int getPageOffset()
    {
        return pageOffset;
    }

    public void setPageOffset(int offset)
    {
        pageOffset = offset;
    }

    public int getPageNumber()
    {
        return (pageOffset/pageSize) + 1;
    }

    public void incrementPage()
    {
        if ((pageOffset + pageSize) < rowCount)
        {
           pageOffset += pageSize;
        }
    }

    public void decrementPage()
    {
        if (pageOffset > pageSize)
        {
            pageOffset -= pageSize;
        }
        else resetPageOffset();
    }

    /**
     * Moves to the pageOffset provided
     * @param pageOffset the new page offset
     */
    public void goToPage(int pageOffset)
    {
        if(pageOffset < rowCount && pageOffset >= 0)
            this.pageOffset = pageOffset;
    }

    /**
     * Finds the starting offset of the page on which the provided row can be found. If the row is a negative number of larger than the max row count
     * 0 will be returned.
     * @param row The row to search for.
     * @return The pageOffset of the starting page on which the row can be found.
     */
    public int findRowPage(int row)
    {
        if(row < 0 || row > rowCount)
            return 0;

        int currentPage = 0;
        while(currentPage < rowCount)
        {
            if(row >= currentPage && row < (currentPage + pageSize))
                return currentPage;
            else
                currentPage += pageSize;
        }

        return 0;
    }

    public int getRowCount()
    {
        return rowCount;
    }

    public void resetRowCount()
    {
        rowCount = 0;
    }

    public int countTableData(T8Context context, T8DataFilter dataFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, tableDefinition.getDataEntityIdentifier());
        operationParameters.put(PARAMETER_DATA_FILTER, dataFilter);

        rowCount = (Integer)T8MainServerClient.executeSynchronousOperation(context, OPERATION_COUNT_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_COUNT);
        fireDataEntityCountRefreshedEvent(rowCount);
        return rowCount;
    }

    public T8DefaultTableModel loadTableData(T8Context context, T8DataFilter dataFilter) throws Exception
    {
        List<T8DataEntity> retrievedData;

        retrievedData = T8DataManagerOperationHandler.selectDataEntities(context, tableDefinition.getDataEntityIdentifier(), dataFilter, pageOffset, pageSize);
        return new T8DefaultTableModel(tableDefinition, retrievedData, pageOffset);
    }

    private void fireDataEntityCountRefreshedEvent(int entityCount)
    {
        HashMap<String, Object> eventParameters;

        eventParameters = new HashMap<>();
        eventParameters.put(T8TableAPIHandler.PARAMETER_DATA_ENTITY_COUNT, entityCount);
        t8Table.getController().reportEvent(t8Table, tableDefinition.getComponentEventDefinition(T8TableAPIHandler.EVENT_DATA_ENTITY_COUNT_REFRESHED), eventParameters);
    }
}
