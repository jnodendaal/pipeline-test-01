package com.pilog.t8.ui.tree;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.T8DataManagerResource;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeAPIHandler;
import com.pilog.t8.definition.ui.tree.T8TreeDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeNodeDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.data.T8DataManagerResource.*;
import static com.pilog.t8.definition.ui.tree.T8TreeAPIHandler.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8TreeDataHandler
{
    private final T8Context context;
    private final Map<String, Map<String, T8DataFilter>> nodePrefilters;

    public T8TreeDataHandler(T8Context context, T8TreeDefinition treeDefinition)
    {
        this.context = context;
        this.nodePrefilters = new HashMap<>();
    }

    public void setNodePrefilter(String nodeIdentifier, String filterIdentifier, T8DataFilter filter)
    {
        Map<String, T8DataFilter> prefilters;

        prefilters = nodePrefilters.get(nodeIdentifier);
        if (prefilters == null)
        {
            prefilters = new HashMap<>();
            nodePrefilters.put(nodeIdentifier, prefilters);
        }

        prefilters.put(filterIdentifier, filter);
    }

    public T8DataFilter getNodePrefilter(String nodeIdentifier, String filterIdentifier)
    {
        Map<String, T8DataFilter> prefilters;

        prefilters = nodePrefilters.get(nodeIdentifier);
        if (prefilters != null)
        {
            return prefilters.get(filterIdentifier);
        }
        else return null;
    }

    public Map<String, T8DataFilter> getNodePrefilters(String nodeIdentifier)
    {
        return nodePrefilters.get(nodeIdentifier);
    }

    public List<T8TreeNode> loadNodes(T8Context context, T8TreeNodeDefinition nodeDefinition, T8DataEntity parentEntity) throws Exception
    {
        HashMap<String, Object> operationParameters;
        List<T8DataEntity> retrievedData;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, nodeDefinition.getDataEntityIdentifier());
        operationParameters.put(T8DataManagerResource.PARAMETER_DATA_FILTER, getNodeFilter(nodeDefinition, parentEntity));
        operationParameters.put(PARAMETER_PAGE_OFFSET, 0);
        operationParameters.put(PARAMETER_PAGE_SIZE, 5000);

        retrievedData = (List<T8DataEntity>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_SELECT_DATA_ENTITIES, operationParameters).get(T8DataManagerResource.PARAMETER_DATA_ENTITY_LIST);
        return buildNodes(nodeDefinition, retrievedData);
    }

    public ChildNodePage loadNodes(T8Context context, T8TreeNodeDefinition parentNodeDefinition, T8DataEntity parentDataEntity, String childRestriction, Map<String, T8DataFilter> childNodeFilterMap, int pageOffset, int pageSize) throws Exception
    {
        HashMap<String, Object> operationParameters;
        Map<String, Object> operationOutput;
        List<T8TreeNode> nodeList;
        int nodeCount;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_TREE_NODE_DEFINITION, parentNodeDefinition);
        operationParameters.put(T8TreeAPIHandler.PARAMETER_DATA_ENTITY, parentDataEntity);
        operationParameters.put(PARAMETER_TREE_NODE_IDENTIFIER, childRestriction);
        operationParameters.put(PARAMETER_TREE_NODE_PAGE_OFFSET, pageOffset);
        operationParameters.put(PARAMETER_TREE_NODE_PAGE_SIZE, pageSize);
        operationParameters.put(PARAMETER_TREE_NODE_FILTER_MAP, childNodeFilterMap);

        operationOutput = T8MainServerClient.executeSynchronousOperation(context, SERVER_OPERATION_LOAD_TREE_NODES, operationParameters);
        nodeList = (List<T8TreeNode>)operationOutput.get(PARAMETER_TREE_NODE_LIST);
        nodeCount = (Integer)operationOutput.get(PARAMETER_TREE_NODE_COUNT);
        return new ChildNodePage(nodeList, nodeCount);
    }

    private List<T8TreeNode> buildNodes(T8TreeNodeDefinition definition, List<T8DataEntity> nodeData)
    {
        List<T8TreeNode> treeNodes;

        treeNodes = new ArrayList<>();
        for (T8DataEntity entity : nodeData)
        {
            T8TreeNode node;

            // Create a new tree node.
            node = new T8TreeNode(definition, entity);

            // If required, add a fake node just to enabled node expansion (which will trigger the loading of child nodes to replace the fake node).
            if ((definition.getChildNodeDefinitions().size() > 0) || (definition.getRecursionNodeDefinition() != null))
            {
                node.add(new T8TreeNode(null, 0));
            }

            // Add the completed node to the list.
            treeNodes.add(node);
        }

        return treeNodes;
    }

    private T8DataFilter getNodeFilter(T8TreeNodeDefinition nodeDefinition, T8DataEntity parentEntity)
    {
        T8DataFilterCriteria combinedFilterCriteria;
        Map<String, T8DataFilter> prefilters;
        LinkedHashMap<String, OrderMethod> orderBy;

        // Create a filter to contain a combination of all applicable filters for data retrieval.
        combinedFilterCriteria = new T8DataFilterCriteria();
        orderBy = new LinkedHashMap<>();

        // Get the preset node filters.
        prefilters = getNodePrefilters(nodeDefinition.getIdentifier());
        if (prefilters != null)
        {
            for (T8DataFilter prefilter : prefilters.values())
            {
                // Only include filters that have valid filter criteria.
                if ((prefilter != null) && (prefilter.hasFilterCriteria()))
                {
                    combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
                }
            }
        }

        // Add all available and valid prefilters.
        for (T8DataFilterDefinition prefilterDefinition : nodeDefinition.getPrefilterDefinitions())
        {
            // Only add the filter, if it was not part of the preset filter collection.
            if ((prefilters == null) || (!prefilters.containsKey(prefilterDefinition.getIdentifier())))
            {
                T8DataFilter prefilter;

                // Only include filters that have valid filter criteria.
                prefilter = prefilterDefinition.getNewDataFilterInstance(context, null);
                if ((prefilter != null) && (prefilter.hasFilterCriteria()))
                {
                    combinedFilterCriteria.addFilterClause(T8DataFilterCriteria.DataFilterConjunction.AND, prefilter.getFilterCriteria());
                    if(prefilter.hasFieldOrdering()) orderBy = prefilter.getFieldOrdering();
                }
            }
        }

        // Create a key map to use as filter criteria (only if a parent entity is available).
        if (parentEntity != null)
        {
            Map<String, Object> keyData;

            keyData = parentEntity.getFieldValues();
            keyData = T8IdentifierUtilities.mapParameters(keyData, nodeDefinition.getRelationshipFieldMapping());
            combinedFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriteria(keyData));
        }

        // Return the combined filter.
        T8DataFilter combinedFilter;

        combinedFilter = new T8DataFilter(nodeDefinition.getDataEntityIdentifier(), combinedFilterCriteria);
        combinedFilter.setFieldOrdering(orderBy);
        return combinedFilter;
    }

    public class ChildNodePage
    {
        private int nodeCount;
        private List<T8TreeNode> nodeList;

        public ChildNodePage(List<T8TreeNode> nodeList, int nodeCount)
        {
            this.nodeCount = nodeCount;
            this.nodeList = nodeList;
        }

        public int getNodeCount()
        {
            return nodeCount;
        }

        public void setNodeCount(int nodeCount)
        {
            this.nodeCount = nodeCount;
        }

        public List<T8TreeNode> getNodeList()
        {
            return nodeList;
        }

        public void setNodeList(List<T8TreeNode> nodeList)
        {
            this.nodeList = nodeList;
        }
    }
}
