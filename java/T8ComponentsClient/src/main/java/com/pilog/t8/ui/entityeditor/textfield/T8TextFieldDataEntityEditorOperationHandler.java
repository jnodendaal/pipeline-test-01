package com.pilog.t8.ui.entityeditor.textfield;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.ui.entityeditor.textfield.T8TextFieldDataEntityEditorAPIHandler;
import com.pilog.t8.ui.textfield.T8TextFieldOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextFieldDataEntityEditorOperationHandler extends T8TextFieldOperationHandler
{
    private final T8TextFieldDataEntityEditor textField;

    public T8TextFieldDataEntityEditorOperationHandler(T8TextFieldDataEntityEditor textField)
    {
        super(textField);
        this.textField = textField;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_COMMIT_CHANGES))
        {
            textField.commitEditorChanges();
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_SET_DATA_ENTITY))
        {
            textField.setEditorDataEntity((T8DataEntity)operationParameters.get(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY));
            return null;
        }
        else if (operationIdentifier.equals(T8TextFieldDataEntityEditorAPIHandler.OPERATION_GET_DATA_ENTITY))
        {
            return HashMaps.newHashMap(T8TextFieldDataEntityEditorAPIHandler.PARAMETER_DATA_ENTITY, textField.getEditorDataEntity());
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
