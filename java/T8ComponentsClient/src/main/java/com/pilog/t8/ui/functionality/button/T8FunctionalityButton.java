package com.pilog.t8.ui.functionality.button;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.functionality.T8FunctionalityController;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.definition.ui.functionality.button.T8FunctionalityButtonDefinition;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle.T8FunctionalityAccessType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.button.T8Button;
import com.pilog.t8.utilities.strings.StringUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityButton extends T8Button implements T8Component
{
    private final T8ClientContext clientContext;
    private final T8Context context;
    private final T8FunctionalityManager functionalityManager;
    private final T8FunctionalityButtonDefinition definition;
    private final T8FunctionalityButtonOperationHandler operationHandler;
    private final FunctionalityActionListener buttonListenerListener;
    private final T8ConfigurationManager configurationManager;
    private final String functionalityId;
    private T8FunctionalityHandle functionalityHandle;
    private String targetObjectId;
    private String targetObjectIid;
    private final ReentrantLock lock;
    private ButtonLoader loader;
    private final List<JMenu> functionalityMenus;

    public T8FunctionalityButton(T8FunctionalityButtonDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.clientContext = controller.getClientContext();
        this.context = controller.getContext();
        this.functionalityManager = clientContext.getFunctionalityManager();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.operationHandler = new T8FunctionalityButtonOperationHandler(this);
        this.buttonListenerListener = new FunctionalityActionListener();
        this.functionalityId = definition.getFunctionalityIdentifier();
        this.lock = new ReentrantLock();
        this.functionalityMenus = new ArrayList<>();
        this.addActionListener(buttonListenerListener);
    }

    private String translate(String inputString)
    {
        return controller.translate(inputString);
    }

    @Override
    public void startComponent()
    {
        super.startComponent();

        // Load the functionality.
        refreshFunctionality(targetObjectId, targetObjectIid);
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    public void setDataObject(String objectId, String objectIid)
    {
        this.targetObjectId = objectId;
        this.targetObjectIid = objectIid;
        refreshFunctionality(targetObjectId, targetObjectIid);
    }

    private void refreshFunctionality(String objectId, String objectIid)
    {
        try
        {
            lock.lock();
            if (loader == null)
            {
                loader = new ButtonLoader(new LoadTask(objectId, objectIid));
                loader.execute();
            }
            else
            {
                loader.setNextTask(new LoadTask(objectId, objectIid));
            }

        }
        catch (Exception e)
        {
            T8Log.log("Exception while initializing functionality group: " + functionalityId, e);
        }
        finally
        {
            lock.unlock();
        }
    }

    private void setFunctionality(T8FunctionalityHandle functionality)
    {
        this.functionalityHandle = functionality;
        if (functionality != null)
        {
            this.setText(functionalityHandle.getDisplayName());
            this.setToolTipText(functionalityHandle.getDisabledMessage());
            this.setIcon(functionalityHandle.getIcon());
        }
        else
        {
            // Set the default button text.
            this.setText(definition.getButtonText());
        }
    }

    private void accessFunctionality() throws Exception
    {
        if (functionalityHandle != null)
        {
            final T8FunctionalityController functionalityController;

            functionalityController = findFunctionalityContext();
            if (functionalityController != null)
            {
                String confirmationMessage;

                // Get the functionality to execute.
                confirmationMessage = functionalityHandle.getConfirmationMessage();
                if (!Strings.isNullOrEmpty(confirmationMessage))
                {
                    if (JOptionPane.showConfirmDialog(clientContext.getParentWindow(), confirmationMessage, translate("Confirmation"), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) return;
                }

                // Invoke the functionality in a new thread (we don't want to block the EDT.
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            T8FunctionalityAccessHandle accessHandle;

                            // Access the functionality.
                            accessHandle = functionalityController.accessFunctionality(functionalityHandle.getId(), functionalityHandle.getPublicParameters());
                            if (accessHandle.getAccessType() == T8FunctionalityAccessType.ACCESS)
                            {
                                // Refresh the functionality menu bar to reflect changes.
                                refreshFunctionality(targetObjectId, targetObjectIid);
                            }
                            else
                            {
                                JOptionPane.showMessageDialog(clientContext.getParentWindow(), StringUtilities.wrapText(100, accessHandle.getAccessMessage()), translate("Access Denied"), JOptionPane.ERROR_MESSAGE);
                            }
                        }
                        catch (Exception e)
                        {
                            T8Log.log("Exception while invoking functionality: " + functionalityHandle, e);
                            JOptionPane.showMessageDialog(clientContext.getParentWindow(), StringUtilities.wrapText(100, "An unexpected error prevented execution of the functionality."), translate("Access Denied"), JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }.start();
            }
            else throw new Exception("No Functionality context found for execution of functionality: " + functionalityId);
        }
    }

    private T8FunctionalityController findFunctionalityContext()
    {
        String contextIdentifier;

        contextIdentifier = definition.getFunctionalityControllerIdentifier();
        if (contextIdentifier != null)
        {
            T8FunctionalityController functionalityContext;

            functionalityContext = (T8FunctionalityController)controller.findComponent(contextIdentifier);
            if (functionalityContext != null)
            {
                return functionalityContext;
            }
            else throw new RuntimeException("Functionality context not found: " + definition.getFunctionalityControllerIdentifier());
        }
        else
        {
            // Return the default functionality controller provided by the component controller responsible for this component.
            return controller.getFunctionalityController();
        }
    }

    private class FunctionalityActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            HashMap<String, Object> eventParameters;

            try
            {
                accessFunctionality();
            }
            catch (Exception e)
            {
                T8Log.log("Exception while invoking functionality: " + functionalityId, e);
            }
        }
    }

    private static class LoadTask
    {
        private final String objectIid;
        private final String objectId;

        private LoadTask(String objectId, String objectIid)
        {
            this.objectId = objectId;
            this.objectIid = objectIid;
        }

        public String getObjectId()
        {
            return objectId;
        }

        public String getObjectIid()
        {
            return objectIid;
        }
    }

    private class ButtonLoader extends SwingWorker<Void, Void>
    {
        private LoadTask nextTask;

        private ButtonLoader(LoadTask task)
        {
            this.nextTask = task;
        }

        public void setNextTask(LoadTask task)
        {
            this.nextTask = task;
        }

        @Override
        protected Void doInBackground() throws Exception
        {
            LoadTask task;

            while (true)
            {
                // Acquire the next load task.
                try
                {
                    lock.lock();
                    if (nextTask != null)
                    {
                        task = nextTask;
                        nextTask = null;
                    }
                    else
                    {
                        loader = null;
                        break;
                    }
                }
                finally
                {
                    lock.unlock();
                }


                // Get the functionality handle.
                functionalityHandle = functionalityManager.getFunctionalityHandle(context, functionalityId, task.getObjectId(), task.getObjectIid());

                // Determine whether the loop should continue.
                try
                {
                    lock.lock();
                    if (nextTask == null)
                    {
                        loader = null;
                        break;
                    }
                }
                finally
                {
                    lock.unlock();
                }
            }

            return null;
        }

        @Override
        public void done()
        {
            // Set the new functionality handle.
            setFunctionality(functionalityHandle);

            // Revalidate the layout.
            revalidate();
            repaint();
        }
    }
}
