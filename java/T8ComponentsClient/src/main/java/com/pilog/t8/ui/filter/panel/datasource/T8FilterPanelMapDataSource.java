package com.pilog.t8.ui.filter.panel.datasource;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItem;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItemDataSource;
import java.util.Iterator;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelMapDataSource implements T8FilterPanelListComponentItemDataSource
{
    Map<String,Object> itemsMap;
    Iterator<String> itemsMapKeyIterator;

    public T8FilterPanelMapDataSource(Map<String,Object> itemsMap)
    {
        this.itemsMapKeyIterator = itemsMap.keySet().iterator();
        this.itemsMap = itemsMap;
    }

    @Override
    public T8FilterPanelListComponentItem next()
    {
        String nextKey = itemsMapKeyIterator.next();
        T8FilterPanelListComponentItem nextFilterPanelItem = new T8FilterPanelListComponentItem(nextKey, itemsMap.get(nextKey));
        return nextFilterPanelItem;
    }

    @Override
    public boolean hasNext()
    {
        return itemsMapKeyIterator.hasNext();
    }

    @Override
    public void initialize(T8Context context, Map<String, Object> inputParameters)
    {
    }

    @Override
    public void setNewDataSourceFilter(T8DataFilter dataFilter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported on the MAP data source.");
    }

}
