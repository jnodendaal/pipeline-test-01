package com.pilog.t8.ui.menuitem;

import com.pilog.t8.definition.ui.menuitem.T8MenuItemAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8MenuItemOperationHandler extends T8ComponentOperationHandler
{
    private T8MenuItem menuItem;

    public T8MenuItemOperationHandler(T8MenuItem list)
    {
        super(list);
        this.menuItem = list;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (T8MenuItemAPIHandler.OPERATION_DISABLE_MENU_ITEM.equals(operationIdentifier))
        {
            menuItem.setEnabled(false);
            return null;
        }
        else if (T8MenuItemAPIHandler.OPERATION_SET_TEXT.equals(operationIdentifier))
        {
            String text = (String) operationParameters.get(T8MenuItemAPIHandler.PARAMETER_TEXT);
            menuItem.setText(text);
            return null;
        } 
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
