package com.pilog.t8.ui.userprofilemenu;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.userprofilemenu.T8UserProfileMenuAPIHandler;
import com.pilog.t8.definition.ui.userprofilemenu.T8UserProfileMenuDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.menu.T8PopupMenuUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.jdesktop.swingx.border.DropShadowBorder;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8UserProfileMenu extends JMenu implements T8Component
{
    private final T8ComponentController controller;
    private final T8Context context;
    private final T8UserProfileMenuDefinition definition;
    private List<T8DefinitionMetaData> userProfileMetaData;
    private final UserProfileActionListener profileActionListener;
    private Painter backgroundPainter;

    public T8UserProfileMenu(T8UserProfileMenuDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.profileActionListener = new UserProfileActionListener();
        setupPainters();
    }

    private void setupPainters()
    {
        T8PainterDefinition painterDefinition;

        // Initialize painter for painting the background of the entire component.
        painterDefinition = definition.getMenuBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            backgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        JPopupMenu popupMenu;

        // Set the menu text.
        this.setText(definition.getText());

        // Set the popup menu border.
        popupMenu = this.getPopupMenu();
        popupMenu.setOpaque(false);
        popupMenu.setBorder(new DropShadowBorder());

        // Set the popup ui.
        popupMenu.setUI(new T8PopupMenuUI(backgroundPainter));

        try
        {
            this.userProfileMetaData = controller.getClientContext().getSecurityManager().getAccessibleUserProfileMetaData(context);
            if (userProfileMetaData != null)
            {
                Collections.sort(userProfileMetaData, new UserProfileComparator());
                for (T8DefinitionMetaData definitionMetaData : userProfileMetaData)
                {
                    JMenuItem userProfileItem;

                    userProfileItem = new UserProfileMenuItem(definitionMetaData);
                    userProfileItem.addActionListener(profileActionListener);
                    add(userProfileItem);
                }
                if(userProfileMetaData.size() == 1)
                {
                    this.setVisible(false);
                }
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while trying to retrieve the available user profile meta data for the current session.", e);
        }
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        if (component instanceof JMenuItem)
        {
            add((JMenuItem)component);
        }
        else throw new RuntimeException("Invalid child component type: " + component.getClass());
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    private class UserProfileMenuItem extends JMenuItem
    {
        private final T8DefinitionMetaData profileMetaData;

        private UserProfileMenuItem(T8DefinitionMetaData metaData)
        {
            this.profileMetaData = metaData;
            setupMenuItem();
        }

        public T8DefinitionMetaData getProfileMetaData()
        {
            return profileMetaData;
        }

        private void setupMenuItem()
        {
            String displayName;

            displayName = profileMetaData.getDisplayName();
            if (displayName == null) displayName = profileMetaData.getId();
            setText(displayName);
        }
    }

    private class UserProfileActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            HashMap<String, Object> eventParameters;
            UserProfileMenuItem source;
            String userProfileIdentifier;

            source = (UserProfileMenuItem)e.getSource();
            userProfileIdentifier = source.getProfileMetaData().getId();

            // Report the event to the parent component controller.
            eventParameters = new HashMap<>();
            eventParameters.put(T8UserProfileMenuAPIHandler.PARAMETER_USER_PROFILE_IDENTIFIER, userProfileIdentifier);
            controller.reportEvent(T8UserProfileMenu.this, definition.getComponentEventDefinition(T8UserProfileMenuAPIHandler.EVENT_USER_PROFILE_SELECTED), eventParameters);
        }
    }

    private class UserProfileComparator implements Comparator<T8DefinitionMetaData>
    {
        @Override
        public int compare(T8DefinitionMetaData o1, T8DefinitionMetaData o2)
        {
            if(o1 == null && o2 == null)
                return -1;
            if(o1 == null && o2 != null)
                return 1;
            if(o1 != null && o2 == null)
                return -1;
            if(o1.getDisplayName() == null)
                return 1;
            if(o2.getDisplayName() == null)
                return -1;
            return o1.getDisplayName().compareTo(o2.getDisplayName());
        }
    }
}
