/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.ui.table.filter;

import com.pilog.t8.ui.table.T8TableCellRendererComponent;
import com.pilog.t8.ui.cellrenderer.T8TableCellRendererAdaptor;
import com.pilog.t8.ui.table.T8Table;
import com.pilog.t8.ui.table.T8TableFilterPanel;
import java.awt.Component;
import java.util.HashMap;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelTableCellRendererAdaptor extends DefaultTableCellRenderer
{
    private final T8Table sourceTable;
    private final T8TableFilterPanel renderableComponent;
    private T8TableCellRendererAdaptor rendererAdaptor;
    private String rendererCellIdentifier;

    public T8FilterPanelTableCellRendererAdaptor(T8Table sourceTable, T8TableFilterPanel editableComponent)
    {
        this.sourceTable = sourceTable;
        this.renderableComponent = editableComponent;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        TableCellRenderer sourceRenderer = sourceTable.getTable().getColumnModel().getColumn(row).getCellRenderer();

        if(!(sourceRenderer instanceof T8TableCellRendererAdaptor))
        {
            rendererAdaptor = null;
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        rendererAdaptor = ((T8TableCellRendererAdaptor)sourceRenderer);

        HashMap<String, Object> dataRow;
        HashMap<T8TableCellRendererComponent.RenderParameter, Object> renderParameters;
        TableModel tableModel;

        // Create the render parameter map.
        renderParameters = new HashMap<>();
        renderParameters.put(T8TableCellRendererComponent.RenderParameter.SELECTED, isSelected);
        renderParameters.put(T8TableCellRendererComponent.RenderParameter.FOCUSED, hasFocus);
        renderParameters.put(T8TableCellRendererComponent.RenderParameter.ROW, row);
        renderParameters.put(T8TableCellRendererComponent.RenderParameter.COLUMN, column);

        // Get the row of data from the source table.
        tableModel = table.getModel();
        dataRow = new HashMap<>();
        if(tableModel.getRowCount() > 0)
        {
            for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
            {
                dataRow.put(renderableComponent.getColumnDefinition(rowIndex).getFieldIdentifier(), tableModel.getValueAt(rowIndex, 2));
            }
        }

        // Record some meta data about the cell being edited.
        rendererCellIdentifier = renderableComponent.getColumnDefinition(row).getFieldIdentifier();

        // Set the editor component content and return it.
        T8TableCellRendererComponent rendererComponent = rendererAdaptor.getRendererComponent();
        rendererComponent.setRendererData(renderableComponent, dataRow, rendererCellIdentifier, renderParameters);
        return (Component)rendererComponent;
    }

}
