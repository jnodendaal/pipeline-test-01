package com.pilog.t8.ui.textfield;

import static com.pilog.t8.definition.ui.textfield.T8TextFieldAPIHandler.*;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextFieldOperationHandler extends T8ComponentOperationHandler
{
    private final T8TextField textField;

    public T8TextFieldOperationHandler(T8TextField textField)
    {
        super(textField);
        this.textField = textField;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(OPERATION_GET_TEXT))
        {
            return HashMaps.createSingular(PARAMETER_TEXT, textField.getText());
        }
        else if (operationIdentifier.equals(OPERATION_GET_VALUE))
        {
            return HashMaps.createSingular(PARAMETER_VALUE, textField.getValue());
        }
        else if(operationIdentifier.equals(OPERATION_SET_TEXT))
        {
            textField.setText((String)operationParameters.get(PARAMETER_TEXT));
            return null;
        }
        else if(operationIdentifier.equals(OPERATION_SET_VALUE))
        {
            textField.setValue(operationParameters.get(PARAMETER_VALUE));
            return null;
        }
        else if(operationIdentifier.equals(OPERATION_SET_EDITABLE))
        {
            textField.setEditable((Boolean)operationParameters.get(PARAMETER_EDITABLE));
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_SET_REGEX_PATTERN))
        {
            textField.applyRegexValidation((String)operationParameters.get(PARAMETER_REGEX_PATTERN));
            return null;
        }
        else if (operationIdentifier.equals(OPERATION_IS_VALID_CONTENT))
        {
            return HashMaps.createSingular(PARAMETER_VALID, this.textField.isRegexValidContent());
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
