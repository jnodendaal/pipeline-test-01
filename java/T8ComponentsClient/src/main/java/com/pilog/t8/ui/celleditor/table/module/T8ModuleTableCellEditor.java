package com.pilog.t8.ui.celleditor.table.module;

import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellEditableComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent.EditParameter;
import com.pilog.t8.definition.ui.celleditor.table.module.T8ModuleTableCellEditorDefinition;
import com.pilog.t8.ui.module.T8Module;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ModuleTableCellEditor extends T8Module implements T8TableCellEditorComponent
{
    private T8ModuleTableCellEditorDefinition definition;
    private T8ComponentController controller;
    private String editorKey;

    public T8ModuleTableCellEditor(T8ModuleTableCellEditorDefinition definition, T8ComponentController controller)
    {
        super(controller, definition);
        this.controller = controller;
        this.definition = definition;
    }

    @Override
    public void setEditorData(T8TableCellEditableComponent sourceComponent, Map<String, Object> dataRow, String editorKey, Map<EditParameter, Object> editParameters)
    {
        HashMap<String, Object> operationParameters;

        this.editorKey = editorKey;

        operationParameters = new HashMap<>();
        operationParameters.put("dataRow", dataRow);
        operationParameters.put("editorKey", editorKey);
        operationParameters.put("editParameters", editParameters);
        executeOperation("$SET_EDITOR_DATA", operationParameters);
    }

    @Override
    public Map<String, Object> getEditorData()
    {
        Object result;

        result = executeOperation("$GET_EDITOR_DATA", new HashMap<>());
        if (result instanceof Map)
        {
            return (Map<String, Object>)result;
        }
        else
        {
            HashMap<String, Object> editorData;

            editorData = new HashMap<>();
            editorData.put(editorKey, result);
            return editorData;
        }
    }

    @Override
    public boolean singleClickEdit()
    {
        return false;
    }
}
