package com.pilog.t8.ui.celleditor;

import com.pilog.t8.ui.table.T8TableCellEditableComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent.EditParameter;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class T8TableCellEditorAdaptor extends AbstractCellEditor implements TableCellEditor
{
    private T8TableCellEditorComponent editorComponent;
    private T8TableCellEditableComponent editableComponent;
    private String editorCellIdentifier;
    private int rowIndex;

    public T8TableCellEditorAdaptor(T8TableCellEditableComponent editableComponent, T8TableCellEditorComponent editorComponent)
    {
        this.editableComponent = editableComponent;
        this.editorComponent = editorComponent;
    }

    public T8TableCellEditorComponent getEditorComponent()
    {
        return this.editorComponent;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent)
    {
        if (!editorComponent.singleClickEdit())
        {
            // Make sure the mouse is clicked twice before allowing the edit.  This
            // is default behaviour but is not properly implemented by AbstractCellEditor.
            if (anEvent instanceof MouseEvent)
            {
                return ((MouseEvent)anEvent).getClickCount() >= 2;
            }
            else return true;
        }
        else return true;
    }

    @Override
    public Object getCellEditorValue()
    {
        Map<String, Object> editedDataRow;

        editedDataRow = editorComponent.getEditorData();
        if (editedDataRow != null)
        {
            Map<String, Object> additionalColumnData; // Columns apart from the one being edited.

            additionalColumnData = new HashMap<>(editedDataRow);
            additionalColumnData.remove(editorCellIdentifier); // Remove the column being edited.

            // First set the additional columns values on the editable component, then return the edited column value so it can also be set (automatically).
            if (additionalColumnData.size() > 0) editableComponent.cellDataEdited(additionalColumnData, rowIndex);
            return editedDataRow.get(editorCellIdentifier);
        }
        else return null;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        HashMap<String, Object> dataRow;
        HashMap<EditParameter, Object> editParameters;
        TableModel tableModel;

        // Create the edit parameter list.
        editParameters = new HashMap<>();
        editParameters.put(EditParameter.SELECTED, isSelected);
        editParameters.put(EditParameter.ROW, row);
        editParameters.put(EditParameter.COLUMN, column);

        // Get the row of data from the source table.
        tableModel = table.getModel();
        dataRow = new HashMap<>();
        if(tableModel.getRowCount() > 0)
        for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++)
        {
            dataRow.put(tableModel.getColumnName(columnIndex), tableModel.getValueAt(row, columnIndex));
        }

        // Record some meta data about the cell being edited.
        rowIndex = row;
        editorCellIdentifier = tableModel.getColumnName(table.getColumnModel().getColumn(column).getModelIndex());

        // Set the editor component content and return it.
        editorComponent.setEditorData(editableComponent, dataRow, editorCellIdentifier, editParameters);
        return (Component)editorComponent;
    }
}
