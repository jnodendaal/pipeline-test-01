package com.pilog.t8.ui.wizard;

import static com.pilog.t8.definition.ui.wizard.T8WizardAPIHandler.*;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.ui.T8Painter;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ComponentControllerScriptDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.wizard.T8WizardDefinition;
import com.pilog.t8.definition.ui.wizard.T8WizardInitializationScriptDefinition;
import com.pilog.t8.definition.ui.wizard.T8WizardStep;
import com.pilog.t8.definition.ui.wizard.T8WizardStepDefinition;
import com.pilog.t8.definition.ui.wizard.T8WizardStepFinalizationScriptDefinition;
import com.pilog.t8.ui.laf.T8ButtonUI;
import com.pilog.t8.utilities.collections.HashMaps;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class T8Wizard extends JXPanel implements T8Component
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8Wizard.class.getName());

    private final T8WizardDefinition definition;
    private final T8ComponentController controller;
    private final T8WizardOperationHandler operationHandler;
    private final WizardContentPanel contentPanel;
    private final T8WizardStep steps;
    private final Map<String, Object> wizardParameters;
    private final Map<String, Object> inputParameters;
    private T8WizardStep currentStep;

    public T8Wizard(T8WizardDefinition definition, T8ComponentController controller)
    {
        this.controller = controller;
        this.definition = definition;
        this.operationHandler = new T8WizardOperationHandler(this);
        this.steps = definition.getWizardSteps(controller);
        initComponents();
        this.contentPanel = new WizardContentPanel(this, steps);
        this.jXPanelContent.add(contentPanel, BorderLayout.CENTER);
        this.wizardParameters = new HashMap<>();
        this.inputParameters = new HashMap<>();
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8LookAndFeelManager lookAndFeelManager;
        T8ButtonUI buttonUI;

        // Store the input parameters.
        if (inputParameters != null)
        {
            this.inputParameters.putAll(inputParameters);
        }

        // Initialize the header background painter.
        if (definition.getHeaderBackgroundPainterDefinition() != null)
        {
            T8Painter newPainterInstance = definition.getHeaderBackgroundPainterDefinition().getNewPainterInstance();
            jXHeader.setBackgroundPainter(new T8PainterAdapter(newPainterInstance));
        }

        // Initialize the navigator background painter.
        if (definition.getNavigatorBackgroundPainterDefinition() != null)
        {
            T8Painter newPainterInstance = definition.getNavigatorBackgroundPainterDefinition().getNewPainterInstance();
            contentPanel.setNavigatorBackgroundPainter(new T8PainterAdapter(newPainterInstance));
        }

        // Initialize the control background painter.
        if (definition.getControlBackgroundPainterDefinition() != null)
        {
            T8Painter newPainterInstance = definition.getControlBackgroundPainterDefinition().getNewPainterInstance();
            jXPanelControls.setBackgroundPainter(new T8PainterAdapter(newPainterInstance));
        }

        // Set the button UI.
        lookAndFeelManager = controller.getClientContext().getConfigurationManager().getLookAndFeelManager(controller.getContext());
        buttonUI = new T8ButtonUI(lookAndFeelManager);
        jXButtonCancel.setUI(buttonUI);
        jXButtonFinish.setUI(buttonUI);
        jXButtonNext.setUI(buttonUI);
        jXButtonPrevious.setUI(buttonUI);
        jXButtonPrevious.setVisible(false);
    }

    @Override
    public void startComponent()
    {
        T8WizardInitializationScriptDefinition initializationScriptDefinition;

        // Execute the initialization script (if any).
        initializationScriptDefinition = definition.getInitializationScriptDefinition();
        if (initializationScriptDefinition != null)
        {
            try
            {
                Map<String, Object> outputParameters;

                // Execute the script.
                controller.addScript(initializationScriptDefinition);
                outputParameters = controller.executeScript(initializationScriptDefinition.getIdentifier(), inputParameters);
                if (outputParameters != null)
                {
                    // Add all of the valid script output parameters to the wizard parameter collection.
                    for (T8DataParameterDefinition parameterDefinition : definition.getWizardParameterDefinitions())
                    {
                        String parameterId;

                        parameterId = parameterDefinition.getIdentifier();
                        if (outputParameters.containsKey(parameterId))
                        {
                            wizardParameters.put(parameterId, outputParameters.get(parameterId));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while executing wizard initialization script: " + initializationScriptDefinition, e);
            }
        }

        // Add the step scripts to the controller.
        for (T8WizardStepDefinition stepDefinition : definition.getAllStepDefinitions())
        {
            T8ComponentControllerScriptDefinition scriptDefinition;

            // Add the step initialization script.
            scriptDefinition = stepDefinition.getInitializationScriptDefinition();
            if (scriptDefinition != null) controller.addScript(scriptDefinition);

            // Add the step finalization script.
            scriptDefinition = stepDefinition.getFinalizationScriptDefinition();
            if (scriptDefinition != null) controller.addScript(scriptDefinition);
        }

        // Initialize the first step.
        goToNextStep();
    }

    @Override
    public void stopComponent()
    {
        T8WizardInitializationScriptDefinition initializationScriptDefinition;

        // Remove the initialization script (if any).
        initializationScriptDefinition = definition.getInitializationScriptDefinition();
        if (initializationScriptDefinition != null) controller.removeScript(initializationScriptDefinition.getIdentifier());

        // Remove all the step scripts from the controller.
        for (T8WizardStepDefinition stepDefinition : definition.getAllStepDefinitions())
        {
            T8ComponentControllerScriptDefinition scriptDefinition;

            // Remove the step initialization script.
            scriptDefinition = stepDefinition.getInitializationScriptDefinition();
            if (scriptDefinition != null) controller.removeScript(scriptDefinition.getIdentifier());

            // Remove the step finalization script.
            scriptDefinition = stepDefinition.getFinalizationScriptDefinition();
            if (scriptDefinition != null) controller.removeScript(scriptDefinition.getIdentifier());
        }
    }

    /**
     * Navigates the the specified step, which will always be some previous
     * step. This is a wrapper for the {@link #goToStep(java.lang.String)} which
     * will raise an event to be used.
     *
     * @param stepId The {@code String} step identifier for the step
     *      which will/should be navigated to
     */
    void navigateToStep(String stepId)
    {
        HashMap<String, Object> eventParameters;
        String previousStepId;

        previousStepId = (this.currentStep != null) ? this.currentStep.getIdentifier() : null;
        goToStep(stepId);

        // Fire the event.
        eventParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_STEP_IDENTIFIER, PARAMETER_PREVIOUS_STEP_IDENTIFIER}, new Object[]{this.currentStep.getIdentifier(), previousStepId});
        controller.reportEvent(this, definition.getComponentEventDefinition(EVENT_WIZARD_PREVIOUS), eventParameters);
    }

    /**
     * This method will finalize the currently displayed step and, if valid,
     * will switch to the requested step unless a different step has been
     * determined from the output of the finalization.
     *
     * @param stepId The identifier of the step to switch to, unless
     * overridden.
     */
    public void goToStep(String stepId)
    {
        // Set the current step as completed.
        if (currentStep != null)
        {
            currentStep.setSelected(false);
        }

        // Switch to the next step.
        currentStep = steps.getStep(stepId);
        if (currentStep != null)
        {
            String initializationScriptId;

            // Set the new step selected.
            currentStep.setSelected(true);

            // Set the header information.
            jXHeader.setTitle(currentStep.getName());
            jXHeader.setDescription(currentStep.getDescription());
            jXHeader.setIcon(currentStep.getIcon());

            // Set all subsequent steps incomplete.
            for (T8WizardStep subsequentStep : currentStep.getSubsequentSteps())
            {
                subsequentStep.setCompleted(false);
            }

            // Set the selected step on the content panel.
            contentPanel.setSelectedStep(stepId);

            // Set the state of the action buttons.
            jXButtonFinish.setVisible(currentStep.isFinishAction());
            jXButtonNext.setVisible(currentStep.hasSelectableNextStep());
            jXButtonPrevious.setVisible(currentStep.hasSelectablePreviousStep() && currentStep.isBackAction());
            jXButtonCancel.setVisible(currentStep.isCancelAction());

            // Run the step initialization script.
            initializationScriptId = currentStep.getInitializationScriptIdentifier();
            if (initializationScriptId != null)
            {
                try
                {
                    controller.executeScript(initializationScriptId, wizardParameters);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while executing wizard step initialization script: " + initializationScriptId, e);
                }
            }
        }
        else throw new IllegalArgumentException("Step not found: " + stepId);
    }

    public void goToPreviousStep()
    {
        // If we have a current step, find the previous one and go to it.
        if (currentStep != null)
        {
            HashMap<String, Object> eventParameters;
            String previousStepId;
            T8WizardStep previousStep;

            previousStepId = this.currentStep.getIdentifier();
            previousStep = currentStep.getPreviousCompletedStep();
            if (previousStep != null)
            {
                goToStep(previousStep.getIdentifier());

                // Fire the event.
                eventParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_STEP_IDENTIFIER, PARAMETER_PREVIOUS_STEP_IDENTIFIER}, new Object[]{this.currentStep.getIdentifier(), previousStepId});
                controller.reportEvent(this, definition.getComponentEventDefinition(EVENT_WIZARD_PREVIOUS), eventParameters);
            }
        }
    }

    public void goToNextStep()
    {
        HashMap<String, Object> eventParameters;
        String previousStepId = null;

        // If we have a current step, find the next one and go to it.
        if (currentStep != null)
        {
            Pair<Boolean, String> finalizationResult;

            // Only go to the resquested step if the current step is valid.
            finalizationResult = finalizeCurrentStep();
            if (finalizationResult.getValue1())
            {
                String nextStepId;

                // Get the identifier for the current step for the event later
                previousStepId = this.currentStep.getIdentifier();

                // Set the current step as completed.
                currentStep.setCompleted(true);

                // Determine what the next step will be and go to it.
                nextStepId = finalizationResult.getValue2();
                if (nextStepId != null)
                {
                    goToStep(nextStepId);
                }
                else
                {
                    T8WizardStep nextStep;

                    nextStep = currentStep.getNextSelectableStep();
                    if (nextStep != null)
                    {
                        goToStep(nextStep.getIdentifier());
                    }
                }
            }
        }
        else // No current step, so go to the first step.
        {
            T8WizardStep firstStep;

            firstStep = steps.getNextSelectableStep();
            if (firstStep != null)
            {
                goToStep(firstStep.getIdentifier());
            }
        }

        // Fire the event.
        eventParameters = HashMaps.createTypeSafeMap(new String[]{PARAMETER_STEP_IDENTIFIER, PARAMETER_PREVIOUS_STEP_IDENTIFIER}, new Object[]{this.currentStep.getIdentifier(), previousStepId});
        controller.reportEvent(this, definition.getComponentEventDefinition(EVENT_WIZARD_NEXT), eventParameters);
    }

    public Pair<Boolean, String> finalizeCurrentStep()
    {
        if (currentStep != null)
        {
            String finalizationScriptId;

            // Get the identifier of the finalization script.
            finalizationScriptId = currentStep.getFinalizationScriptIdentifier();
            if (finalizationScriptId != null)
            {
                try
                {
                    Map<String, Object> outputParameters;

                    // Execute the finalization script.
                    outputParameters = controller.executeScript(finalizationScriptId, wizardParameters);
                    outputParameters = T8IdentifierUtilities.stripNamespace(outputParameters);
                    if (outputParameters != null)
                    {
                        Boolean continueIndicator;

                        // Get the continue indicator.
                        continueIndicator = (Boolean)outputParameters.get(T8WizardStepFinalizationScriptDefinition.P_CONTINUE);
                        if ((continueIndicator != null) && (continueIndicator))
                        {
                            String nextStepIdentifier;

                            // Add all of the valid script output parameters to the wizard parameter collection.
                            for (T8DataParameterDefinition parameterDefinition : definition.getWizardParameterDefinitions())
                            {
                                String parameterIdentifier;

                                parameterIdentifier = parameterDefinition.getIdentifier();
                                if (outputParameters.containsKey(parameterIdentifier))
                                {
                                    wizardParameters.put(parameterIdentifier, outputParameters.get(parameterIdentifier));
                                }
                            }

                            // Get the next step identifier (if any).
                            nextStepIdentifier = (String)outputParameters.get(T8WizardStepFinalizationScriptDefinition.P_NEXT_STEP_IDENTIFIER);

                            // Return valid indicator.
                            return new Pair<Boolean, String>(true, nextStepIdentifier);
                        }
                        else return new Pair<Boolean, String>(false, null);
                    }
                    else return new Pair<Boolean, String>(false, null);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while executing wizard step finalization script: " + finalizationScriptId, e);
                    return new Pair<Boolean, String>(false, null);
                }
            }
            else return new Pair<Boolean, String>(true, null);
        }
        else return new Pair<Boolean, String>(true, null);
    }

    public void reset()
    {
        // Clear existing parameters.
        wizardParameters.clear();

        // Set the current step to null.
        currentStep = null;

        // Go to the first step.
        goToNextStep();
    }

    public void setParameters(Map<String, Object> parameters)
    {
        for (T8DataParameterDefinition parameterDefinition : definition.getWizardParameterDefinitions())
        {
            String parameterIdentifier;

            parameterIdentifier = parameterDefinition.getIdentifier();
            if (parameters.containsKey(parameterIdentifier))
            {
                wizardParameters.put(parameterIdentifier, parameters.get(parameterIdentifier));
            }
        }
    }

    private void finishWizard()
    {
        Map<String, Object> eventParameters;

        // Fire the event.
        eventParameters = new HashMap<>();
        eventParameters.put(PARAMETER_WIZARD_PARAMETERS, new HashMap<>(wizardParameters));
        controller.reportEvent(this, definition.getComponentEventDefinition(EVENT_WIZARD_FINISHED), eventParameters);

        // Clear the wizard parameters.
        wizardParameters.clear();
    }

    private void canceled()
    {
        // Fire the event.
        controller.reportEvent(this, definition.getComponentEventDefinition(EVENT_WIZARD_CANCELED), null);

        // Clear the wizard parameters.
        wizardParameters.clear();
    }

    void setFinishVisible(boolean visible)
    {
        jXButtonFinish.setVisible(visible);
    }

    void setCancelVisible(boolean visible)
    {
        jXButtonCancel.setVisible(visible);
    }

    void setPreviousVisible(boolean visible)
    {
        jXButtonPrevious.setVisible(currentStep.hasSelectablePreviousStep() && visible);
    }

    void setNextVisible(boolean visible)
    {
        jXButtonNext.setVisible(currentStep.hasSelectableNextStep() && visible);
    }

    void setNavigationPanelVisible(boolean visible)
    {
        jXPanelControls.setVisible(visible);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        if (childComponent.getComponentDefinition().getParentDefinition() instanceof T8WizardStepDefinition)
        {
            contentPanel.addStepComponent(childComponent.getComponentDefinition().getParentDefinition().getIdentifier(), childComponent);
        }
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public String translate(String textToTranslate)
    {
        return controller.getClientContext().getConfigurationManager().getUITranslation(controller.getContext(), textToTranslate);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jXHeader = new org.jdesktop.swingx.JXHeader();
        jXPanelWizardContent = new org.jdesktop.swingx.JXPanel();
        jXPanelContent = new org.jdesktop.swingx.JXPanel();
        jXPanelControls = new org.jdesktop.swingx.JXPanel();
        jXButtonPrevious = new org.jdesktop.swingx.JXButton();
        jXButtonNext = new org.jdesktop.swingx.JXButton();
        jXButtonFinish = new org.jdesktop.swingx.JXButton();
        jXButtonCancel = new org.jdesktop.swingx.JXButton();
        jXPanelControlsSpacer = new org.jdesktop.swingx.JXPanel();

        setLayout(new java.awt.GridBagLayout());

        jXHeader.setMaximumSize(null);
        jXHeader.setMinimumSize(null);
        jXHeader.setName(""); // NOI18N
        jXHeader.setPreferredSize(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(jXHeader, gridBagConstraints);

        jXPanelWizardContent.setLayout(new java.awt.GridBagLayout());

        jXPanelContent.setOpaque(false);
        jXPanelContent.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jXPanelWizardContent.add(jXPanelContent, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jXPanelWizardContent, gridBagConstraints);

        jXPanelControls.setOpaque(false);
        jXPanelControls.setLayout(new java.awt.GridBagLayout());

        jXButtonPrevious.setText(translate("Previous")
        );
        jXButtonPrevious.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonPreviousActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jXPanelControls.add(jXButtonPrevious, gridBagConstraints);

        jXButtonNext.setText(translate("Next"));
        jXButtonNext.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonNextActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jXPanelControls.add(jXButtonNext, gridBagConstraints);

        jXButtonFinish.setText(translate("Finish")
        );
        jXButtonFinish.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonFinishActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jXPanelControls.add(jXButtonFinish, gridBagConstraints);

        jXButtonCancel.setText(translate("Cancel")
        );
        jXButtonCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jXButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jXPanelControls.add(jXButtonCancel, gridBagConstraints);

        jXPanelControlsSpacer.setMaximumSize(null);
        jXPanelControlsSpacer.setOpaque(false);

        javax.swing.GroupLayout jXPanelControlsSpacerLayout = new javax.swing.GroupLayout(jXPanelControlsSpacer);
        jXPanelControlsSpacer.setLayout(jXPanelControlsSpacerLayout);
        jXPanelControlsSpacerLayout.setHorizontalGroup(
            jXPanelControlsSpacerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jXPanelControlsSpacerLayout.setVerticalGroup(
            jXPanelControlsSpacerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        jXPanelControls.add(jXPanelControlsSpacer, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(jXPanelControls, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jXButtonNextActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonNextActionPerformed
    {//GEN-HEADEREND:event_jXButtonNextActionPerformed
        jXButtonNext.setEnabled(false);
        goToNextStep();
        jXButtonNext.setEnabled(true);
    }//GEN-LAST:event_jXButtonNextActionPerformed

    private void jXButtonPreviousActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonPreviousActionPerformed
    {//GEN-HEADEREND:event_jXButtonPreviousActionPerformed
        jXButtonPrevious.setEnabled(false);
        goToPreviousStep();
        jXButtonPrevious.setEnabled(true);
    }//GEN-LAST:event_jXButtonPreviousActionPerformed

    private void jXButtonFinishActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonFinishActionPerformed
    {//GEN-HEADEREND:event_jXButtonFinishActionPerformed
        jXButtonFinish.setEnabled(false);
        finishWizard();
        jXButtonFinish.setEnabled(true);
    }//GEN-LAST:event_jXButtonFinishActionPerformed

    private void jXButtonCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jXButtonCancelActionPerformed
    {//GEN-HEADEREND:event_jXButtonCancelActionPerformed
        jXButtonCancel.setEnabled(false);
        canceled();
        jXButtonCancel.setEnabled(true);
    }//GEN-LAST:event_jXButtonCancelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.jdesktop.swingx.JXButton jXButtonCancel;
    private org.jdesktop.swingx.JXButton jXButtonFinish;
    private org.jdesktop.swingx.JXButton jXButtonNext;
    private org.jdesktop.swingx.JXButton jXButtonPrevious;
    private org.jdesktop.swingx.JXHeader jXHeader;
    private org.jdesktop.swingx.JXPanel jXPanelContent;
    private org.jdesktop.swingx.JXPanel jXPanelControls;
    private org.jdesktop.swingx.JXPanel jXPanelControlsSpacer;
    private org.jdesktop.swingx.JXPanel jXPanelWizardContent;
    // End of variables declaration//GEN-END:variables
}
