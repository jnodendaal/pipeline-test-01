package com.pilog.t8.ui.dataentitylist;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.dataentitylist.T8DataEntityListAPIHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityListOperationHandler extends T8ComponentOperationHandler
{
    private T8DataEntityList list;

    public T8DataEntityListOperationHandler(T8DataEntityList list)
    {
        super(list);
        this.list = list;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITY))
        {
            return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITIES))
        {
            return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_GET_DATA_ENTITY_LIST))
        {
            return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_CLEAR))
        {
            return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_REFRESH))
        {
            return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_ADD_DATA_ENTITIES))
        {
            return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_REMOVE_SELECTED_DATA_ENTITIES))
        {
            return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_SET_LOOKUP_PREFILTER))
        {
            return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_SET_LIST_PREFILTER))
        {
            return executeOperationOnEDT(operationIdentifier, operationParameters);
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }

    public Map<String, Object> executeOperationOnEDT(String operationIdentifier, Map<String, Object> operationParameters)
    {
        ListOperationExecuter operationExecuter;

        operationExecuter = new ListOperationExecuter(operationIdentifier, operationParameters);

        try
        {
            SwingUtilities.invokeAndWait(operationExecuter);
        }
        catch (Exception ex)
        {
            T8Log.log("Failed to execute operation " + operationIdentifier + " on EDT.", ex);
        }

        return operationExecuter.getOperationResult();
    }

    private class ListOperationExecuter implements Runnable
    {
        private final String operationIdentifier;
        private final Map<String, Object> operationParameters;
        private Map<String, Object> operationResult;

        private ListOperationExecuter(String operationIdentifier, Map<String, Object> operationParameters)
        {
            this.operationIdentifier = operationIdentifier;
            this.operationParameters = operationParameters;
        }

        @Override
        public void run()
        {
            if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITY))
            {
                operationResult = HashMaps.newHashMap(T8DataEntityListAPIHandler.PARAMETER_DATA_ENTITY, list.getSelectedDataEntity());
            }
            else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_GET_SELECTED_DATA_ENTITIES))
            {
                operationResult = HashMaps.newHashMap(T8DataEntityListAPIHandler.PARAMETER_DATA_ENTITY_LIST, list.getSelectedDataEntities());
            }
            else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_GET_DATA_ENTITY_LIST))
            {
                operationResult = HashMaps.newHashMap(T8DataEntityListAPIHandler.PARAMETER_DATA_ENTITY_LIST, list.getDataEntityList());
            }
            else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_CLEAR))
            {
                list.clearListData();
            }
            else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_REFRESH))
            {
                list.refreshListData();
            }
            else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_ADD_DATA_ENTITIES))
            {
                List<T8DataEntity> entityList;

                entityList = (List<T8DataEntity>)operationParameters.get(T8DataEntityListAPIHandler.PARAMETER_DATA_ENTITY_LIST);
                list.addDataEntityList(entityList);
            }
            else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_REMOVE_SELECTED_DATA_ENTITIES))
            {
                list.removeSelectedDataEntities();
            }
            else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_SET_LOOKUP_PREFILTER))
            {
                T8DataFilter prefilter;
                String filterIdentifier;
                Boolean refresh;

                prefilter = (T8DataFilter)operationParameters.get(T8DataEntityListAPIHandler.PARAMETER_DATA_FILTER);
                filterIdentifier = (String)operationParameters.get(T8DataEntityListAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
                refresh = (Boolean)operationParameters.get(T8DataEntityListAPIHandler.PARAMETER_REFRESH_DATA);
                if (refresh == null) refresh = true;

                list.setLookupPrefilter(filterIdentifier, prefilter);
                if (refresh) list.refreshLookupData();
            }
            else if (operationIdentifier.equals(T8DataEntityListAPIHandler.OPERATION_SET_LIST_PREFILTER))
            {
                T8DataFilter prefilter;
                String filterIdentifier;
                Boolean refresh;

                prefilter = (T8DataFilter)operationParameters.get(T8DataEntityListAPIHandler.PARAMETER_DATA_FILTER);
                filterIdentifier = (String)operationParameters.get(T8DataEntityListAPIHandler.PARAMETER_DATA_FILTER_IDENTIFIER);
                refresh = (Boolean)operationParameters.get(T8DataEntityListAPIHandler.PARAMETER_REFRESH_DATA);
                if (refresh == null) refresh = true;

                list.setListPrefilter(filterIdentifier, prefilter);
                if (refresh) list.refreshListData();
            }
        }

        public Map<String, Object> getOperationResult()
        {
            return operationResult;
        }
    }
}
