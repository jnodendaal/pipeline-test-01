package com.pilog.t8.ui.datarecord;

import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class RecordPropertyTableColumnDefinition implements Serializable
{
    private PropertyRequirement propertyRequirement;
    private String columnName;
    private boolean visible;
    private int width;
    
    public RecordPropertyTableColumnDefinition()
    {
    }
    
    public RecordPropertyTableColumnDefinition(PropertyRequirement propertyRequirement, String columnName, boolean visible, int width)
    {
        this.propertyRequirement = propertyRequirement;
        this.columnName = columnName;
        this.visible = visible;
        this.width = width;
    }

    public PropertyRequirement getPropertyRequirement()
    {
        return propertyRequirement;
    }

    public void setPropertyRequirement(PropertyRequirement propertyRequirement)
    {
        this.propertyRequirement = propertyRequirement;
    }

    public String getColumnName()
    {
        return columnName;
    }

    public void setColumnName(String columnName)
    {
        this.columnName = columnName;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }
}
