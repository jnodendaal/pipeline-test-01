package com.pilog.t8.ui.celleditor.table.checkbox;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellEditableComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent.EditParameter;
import com.pilog.t8.definition.ui.celleditor.table.checkbox.T8CheckBoxTableCellEditorDefinition;
import com.pilog.t8.ui.checkbox.T8CheckBox;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;
import java.util.Objects;
import javax.swing.SwingConstants;

/**
 * @author Bouwer du Preez
 */
public class T8CheckBoxTableCellEditor extends T8CheckBox implements T8TableCellEditorComponent
{
    private static final T8Logger logger = T8Log.getLogger(T8CheckBoxTableCellEditor.class);

    private final T8CheckBoxTableCellEditorDefinition definition;

    private Object selectedValue;
    private Object deselectedValue;

    public T8CheckBoxTableCellEditor(T8CheckBoxTableCellEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.setHorizontalAlignment(SwingConstants.CENTER);
        this.setBorderPaintedFlat(true);
        this.definition = definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        try
        {
            String selectedValueExpression;
            String deselectedValueExpression;
            ExpressionEvaluator evaluator;

            // Create an expression evaluator.
            evaluator = new ExpressionEvaluator();

            // Evaluate the selected value expression.
            selectedValueExpression = definition.getSelectedValueExpression();
            if (selectedValueExpression != null)
            {
                selectedValue = evaluator.evaluateExpression(selectedValueExpression, inputParameters, null);
            }
            else throw new RuntimeException("No selected value expression set in " + definition);

            // Evaluator the deselected value expression.
            deselectedValueExpression = definition.getDeselectedValueExpression();
            if (deselectedValueExpression != null)
            {
                deselectedValue = evaluator.evaluateExpression(deselectedValueExpression, inputParameters, null);
            }
            else throw new RuntimeException("No deselected value expression set in " + definition);
        }
        catch (EPICSyntaxException | EPICRuntimeException | RuntimeException e)
        {
            logger.log("Exception while initializing component: " + definition, e);
        }
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void setEditorData(T8TableCellEditableComponent sourceComponent, Map<String, Object> dataRow, String editorColumnIdentifier, Map<EditParameter, Object> editParameters)
    {
        Object value;

        value = dataRow != null ? dataRow.get(definition.getTargetFieldIdentifier()) : null;
        if (Objects.equals(value, selectedValue))
        {
            setSelected(true);
        }
        else
        {
            setSelected(false);
        }
    }

    @Override
    public Map<String, Object> getEditorData()
    {
        return HashMaps.newHashMap(definition.getTargetFieldIdentifier(), isSelected() ? selectedValue : deselectedValue);
    }

    @Override
    public boolean singleClickEdit()
    {
        return true;
    }
}
