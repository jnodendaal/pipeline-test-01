/**
 * Created on 16 Oct 2015, 2:46:14 PM
 */
package com.pilog.t8.ui.table.menu;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.menu.T8TableContextMenuDefinition;
import com.pilog.t8.definition.ui.table.menu.T8TableMenuDisplayEvalScriptDefinition;
import com.pilog.t8.ui.table.T8Table;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8TableContextMenuHandler
{
    private static final T8Logger logger = T8Log.getLogger(T8TableContextMenuHandler.class);

    private final T8TableContextMenuDefinition menuDefinition;
    private final String displayEvaluationScriptIdentifier;
    private final T8TableContextMenu contextMenu;
    private final T8Table originTable;

    public T8TableContextMenuHandler(T8Table originTable)
    {
        this.originTable = originTable;
        this.menuDefinition = this.originTable.getComponentDefinition().getContextMenuDefinition();

        if (this.menuDefinition != null)
        {
            this.contextMenu = createMenu();
            this.displayEvaluationScriptIdentifier = setDisplayEvaluationScript();
        }
        else
        {
            this.contextMenu = null;
            this.displayEvaluationScriptIdentifier = null;
        }
    }

    private T8TableContextMenu createMenu()
    {
        T8TableContextMenu menu;

        try
        {
            menu = this.menuDefinition.getNewComponentInstance(originTable.getController());
            menu.initializeComponent(HashMaps.createSingular("$P_TABLE", this.originTable));

            return menu;
        }
        catch (Exception ex)
        {
            logger.log("Failed to load the required table context menu.", ex);
            throw new RuntimeException("Table context menu could not be loaded: " + ex.getMessage(), ex);
        }
    }

    private String setDisplayEvaluationScript()
    {
        T8TableMenuDisplayEvalScriptDefinition displayEvaluationScriptDefinition;

        // Add the evaluation script to the controller
        displayEvaluationScriptDefinition = this.menuDefinition.getDisplayEvaluationScriptDefinition();

        if (displayEvaluationScriptDefinition != null)
        {
            this.originTable.getController().addScript(displayEvaluationScriptDefinition);
            return displayEvaluationScriptDefinition.getIdentifier();
        } else return null;
    }

    private void executeDisplayEvaluationScript(String columnIdentifier, T8DataEntity currentRowEntity)
    {
        if (this.displayEvaluationScriptIdentifier != null)
        {
            Map<String, Object> scriptParameters;

            scriptParameters = HashMaps.createTypeSafeMap(new String[]{T8TableContextMenuDefinition.P_COLUMN_IDENTIFIER, T8TableContextMenuDefinition.P_CURRENT_ROW_ENTITY}, new Object[]{columnIdentifier, currentRowEntity});

            try
            {
                this.originTable.getController().executeScript(this.displayEvaluationScriptIdentifier, scriptParameters);
            }
            catch (Exception e)
            {
                logger.log("Failed to complete the execution of the display evaluation script: " + this.displayEvaluationScriptIdentifier, e);
            }
        }
    }

    public T8TableContextMenu getMenu(T8DataEntity currentRowEntity, int rowIndex, int columnIndex)
    {
        if (this.contextMenu == null) return null;

        T8TableColumnDefinition columnDefinition;
        String columnIdentifier;

        columnDefinition = this.originTable.getComponentDefinition().getColumnDefinitions().get(columnIndex);
        // Check if the menu should only be shown on editable fields
        if (this.menuDefinition.isShowOnEditableFieldsOnly() && !columnDefinition.isEditable()) return null;

        // Execute the display evaluation script with standard parameters. Additional
        // required data can be retrieved using component operations
        columnIdentifier = columnDefinition.getIdentifier();
        executeDisplayEvaluationScript(columnIdentifier, currentRowEntity);

        // Set the current row index for the menu, as some actions will require it
        this.contextMenu.setCurrentRowIndex(rowIndex);
        this.contextMenu.setCurrentColumnIndex(columnIndex);

        // If there are no visible items on the context menu, return null to prevent it from being displayed
        return this.contextMenu;
    }

    public void deconstructMenu()
    {
        if (this.contextMenu == null) return;

        // We need to remove the display script from the component controller here
        if (this.displayEvaluationScriptIdentifier != null) this.originTable.getController().removeScript(this.displayEvaluationScriptIdentifier);

        this.contextMenu.clearChildComponents();
        this.contextMenu.stopComponent();
    }
}