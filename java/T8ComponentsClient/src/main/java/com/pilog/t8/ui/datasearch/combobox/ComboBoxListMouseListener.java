package com.pilog.t8.ui.datasearch.combobox;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JList;

/**
 * @author Hennie Brink
 */
public class ComboBoxListMouseListener extends MouseAdapter
{
    private final ComboBoxPopup owner;
    private final JList<ComboBoxModelNode> list;
    private int mousePressedIndex;

    public ComboBoxListMouseListener(ComboBoxPopup owner, JList<ComboBoxModelNode> list)
    {
        this.owner = owner;
        this.list = list;
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        mousePressedIndex = list.locationToIndex(e.getPoint());
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        int selectionIndex;

        selectionIndex = list.locationToIndex(e.getPoint());
        if(mousePressedIndex == selectionIndex)
        {
            ComboBoxModelNode node;
            ComboBoxModelRenderer renderer;

            if (selectionIndex != -1)
            {
                node = list.getModel().getElementAt(selectionIndex);
                renderer = (ComboBoxModelRenderer) list.getCellRenderer().getListCellRendererComponent(list, node, selectionIndex, false, false);

                if (node instanceof CombBoxPagingNode)
                {
                    owner.loadAdditionalData();
                }
                else
                {
                    renderer.itemClicked(list, node, e.getPoint());

                    list.repaint();
                }
            }
        }
    }
}
