/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pilog.t8.ui.entityeditor.conceptlabel;

import com.pilog.t8.ui.label.T8LabelOperationHandler;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8ConceptLabelDataEntityOperationHandler extends T8LabelOperationHandler
{
    private T8ConceptLabelDataEntityEditor conceptLabel;
    public T8ConceptLabelDataEntityOperationHandler(T8ConceptLabelDataEntityEditor label)
    {
        super(label);
        this.conceptLabel = label;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier,
                                                Map<String, Object> operationParameters)
    {
        return super.executeOperation(operationIdentifier, operationParameters);
    }

}
