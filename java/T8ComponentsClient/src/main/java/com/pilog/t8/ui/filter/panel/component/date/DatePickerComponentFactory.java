/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.ui.filter.panel.component.date;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class DatePickerComponentFactory
{
    List<DatePickerComponent> datePickerComponents;

    public DatePickerComponentFactory()
    {
        datePickerComponents = new ArrayList<>();
        datePickerComponents.add(new DatePickerOnComponent());
        datePickerComponents.add(new DatePickerBeforeComponent());
        datePickerComponents.add(new DatePickerAfterComponent());
        datePickerComponents.add(new DatePickerBetweenComponent());
    }

    public List<DatePickerComponent> getDatePickerComponents()
    {
        return datePickerComponents;
    }
}
