package com.pilog.t8.ui.menu;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.menu.T8MenuDefinition;
import com.pilog.t8.ui.menuitem.T8MenuItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import org.jdesktop.swingx.border.DropShadowBorder;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8Menu extends JMenu implements T8Component
{
    private final T8ComponentController controller;
    private final T8MenuDefinition definition;
    private Painter backgroundPainter;
    private final T8MenuItemOperationHandler operationHandler;

    public T8Menu(T8MenuDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8MenuItemOperationHandler(this);
        setupPainters();
    }

    private void setupPainters()
    {
        T8PainterDefinition painterDefinition;

        // Initialize painter for painting the background of the entire component.
        painterDefinition = definition.getMenuBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            backgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8IconDefinition iconDefinition;
        JPopupMenu popupMenu;

        // Set the meny icon.
        iconDefinition = definition.getIconDefinition();
        if (iconDefinition != null)
        {
            this.setIcon(iconDefinition.getImage().getImageIcon());
        }

        // Set the menu text.
        this.setText(definition.getText());

        // Set the popup menu border.
        popupMenu = this.getPopupMenu();
        popupMenu.setOpaque(this.definition.isOpaque());
        popupMenu.setBorder(new DropShadowBorder());

        // Set the popup ui.
        popupMenu.setUI(new T8PopupMenuUI(backgroundPainter));
        setToolTipText(definition.getTooltipText());

        if(definition.addSeperator()) addSeparator();
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        if (component instanceof T8MenuItem)
        {
            T8MenuItem menuItem;

            menuItem = (T8MenuItem)component;

            add(menuItem);
            if(menuItem.addSeperator()) add(new JSeparator());

        } else if(component instanceof JMenuItem)
        {
            add((JMenuItem) component);
        }
        else throw new RuntimeException("Invalid child component type: " + component.getClass());
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }
}
