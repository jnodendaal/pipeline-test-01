package com.pilog.t8.ui.functionality.navigator;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.functionality.navigator.T8FunctionalityNavigatorDefinition;
import com.pilog.t8.functionality.T8FunctionalityGroupHandle;
import com.pilog.t8.functionality.T8FunctionalityHandle;
import com.pilog.t8.security.T8Context;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.jdesktop.swingx.painter.Painter;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityNavigatorMenuPanel extends JXPanel
{
    private final T8DefinitionManager definitionManager;
    private final T8Context context;
    private final JXTaskPaneContainer menuContainer;
    private Painter menuButtonBackgroundPainter;
    private Painter menuButtonFocusBackgroundPainter;
    private Painter menuButtonSelectedBackgroundPainter;
    private T8FunctionalityGroupHandle functionalityGroup;
    private final T8FunctionalityNavigator navigator;
    private final List<MenuItemButton> menuItemButtons;

    private static final Font MENU_BUTTON_FONT = new java.awt.Font("Tahoma", 0, 11);

    public T8FunctionalityNavigatorMenuPanel(T8FunctionalityNavigator menuView)
    {
        this.navigator = menuView;
        this.definitionManager = menuView.getController().getClientContext().getDefinitionManager();
        this.context = menuView.getController().getContext();
        initComponents();
        this.menuItemButtons = new ArrayList<>();
        this.menuContainer = new JXTaskPaneContainer();
        this.menuContainer.setOpaque(false);
        this.menuContainer.setBackgroundPainter(null);
        this.jScrollPaneMenu.setOpaque(false);
        this.jScrollPaneMenu.getViewport().setOpaque(false);
        this.jScrollPaneMenu.setViewportView(menuContainer);
        this.jScrollPaneMenu.setBorder(null);
        setupPainters(menuView.getComponentDefinition());
    }

    private void setupPainters(T8FunctionalityNavigatorDefinition definition)
    {
        T8PainterDefinition painterDefinition;

        // Initialize painter for painting the background of the menu.
        painterDefinition = definition.getMenuBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            setBackgroundPainter(new T8PainterAdapter(painterDefinition.getNewPainterInstance()));
        }

        // Initialize the painter for painting the background of menu item buttons.
        painterDefinition = definition.getMenuButtonBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            menuButtonBackgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }

        // Initialize the painter for painting the background of task type buttons when focused.
        painterDefinition = definition.getMenuButtonFocusBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            menuButtonFocusBackgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }

        // Initialize the painter for painting the background of task type buttons when selected.
        painterDefinition = definition.getMenuButtonSelectedBackgroundPainterDefinition();
        if (painterDefinition != null)
        {
            menuButtonSelectedBackgroundPainter = new T8PainterAdapter(painterDefinition.getNewPainterInstance());
        }
    }

    private String translate(String inputString)
    {
        return navigator.translate(inputString);
    }

    public void setMenuButtonBackgroundPainter(Painter painter)
    {
        this.menuButtonBackgroundPainter = painter;
        for (MenuItemButton taskTypeButton : menuItemButtons)
        {
            taskTypeButton.setDefaultBackgroundPainter(painter);
        }
    }

    public void setMenuButtonFocusBackgroundPainter(Painter painter)
    {
        this.menuButtonFocusBackgroundPainter = painter;
        for (MenuItemButton taskTypeButton : menuItemButtons)
        {
            taskTypeButton.setFocusBackgroundPainter(painter);
        }
    }

    public void setMenuButtonSelectedBackgroundPainter(Painter painter)
    {
        this.menuButtonSelectedBackgroundPainter = painter;
        for (MenuItemButton taskTypeButton : menuItemButtons)
        {
            taskTypeButton.setSelectedBackgroundPainter(painter);
        }
    }

    public void clearMenu()
    {
        menuContainer.removeAll();
    }

    public T8FunctionalityGroupHandle getFunctionalityGroup()
    {
        return functionalityGroup;
    }

    public void setFunctionalityGroup(T8FunctionalityGroupHandle functionalityGroup)
    {
        this.functionalityGroup = functionalityGroup;
        refresh();
    }

    public void refresh()
    {
        // If a valid group was supplied, use it to populate the menu, otherwise clear the menu.
        if (functionalityGroup != null)
        {
            // Set the header.
            jLabelHeader.setText(functionalityGroup.getDisplayName());
            jLabelHeader.setIcon(functionalityGroup.getIcon());

            // Clear the menu container.
            menuContainer.removeAll();

            //Clear the existing buttons.
            menuItemButtons.clear();

            // Add all of the functionality sub-group menu item buttons.
            for (T8FunctionalityGroupHandle subGroup : functionalityGroup.getSubGroups())
            {
                MenuItemButton menuItemButton;

                // Create a new button.
                menuItemButton = createMenuItemButton(subGroup, false);

                // Add the new button to the collection of buttons.
                menuItemButtons.add(menuItemButton);

                // Add the new button to the layout.
                menuContainer.add(menuItemButton);
            }

            // Add all of the functionality menu item buttons.
            for (T8FunctionalityHandle functionality : functionalityGroup.getFunctionalities())
            {
                MenuItemButton menuItemButton;

                // Create a new button.
                menuItemButton = createMenuItemButton(functionality);

                // Add the new button to the collection of buttons.
                menuItemButtons.add(menuItemButton);

                // Add the new button to the layout.
                menuContainer.add(menuItemButton);
            }

            // Add a back-to-parent-group button if applicable.
            if (functionalityGroup.getParentGroup() != null)
            {
                MenuItemButton menuItemButton;

                // Create a new button.
                menuItemButton = createMenuItemButton(functionalityGroup.getParentGroup(), true);

                // Add the new button to the collection of buttons.
                menuItemButtons.add(menuItemButton);

                // Add the new button to the layout.
                menuContainer.add(menuItemButton);
            }

            // Revalidate the containers.
            menuContainer.validate();
        }
        else
        {
            // Clear the menu container.
            menuContainer.removeAll();

            // Revalidate the containers.
            menuContainer.validate();
        }
    }

    private void deselectAllTaskMenuButtons()
    {
        for (Component component : menuContainer.getComponents())
        {
            if (component instanceof MenuItemButton)
            {
                ((MenuItemButton)component).setSelected(false);
            }
        }
    }

    private MenuItemButton createMenuItemButton(final T8FunctionalityGroupHandle functionalityGroup, boolean createAsBackButton)
    {
        final MenuItemButton newButton;
        Icon menuItemIcon;
        String displayName;

        menuItemIcon = functionalityGroup.getIcon();

        newButton = new MenuItemButton(menuButtonBackgroundPainter, menuButtonFocusBackgroundPainter, menuButtonSelectedBackgroundPainter);
        newButton.setIcon(menuItemIcon);
        newButton.setFont(MENU_BUTTON_FONT);
        newButton.setForeground(Color.DARK_GRAY);
        newButton.setFocusPainted(false); // Focus painting will be handled by the background painters.
        newButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                deselectAllTaskMenuButtons();
                newButton.setSelected(true);
                navigator.setSelectedFunctionalityGroup(functionalityGroup, true);
            }
        });

        // Evaluate the display name expression.
        displayName = functionalityGroup.getDisplayName();
        if (createAsBackButton)
        {
            newButton.setText(translate("Back to") + " " + displayName);
            newButton.setToolTipText(translate("Return to the parent menu:") + " " + displayName);
        }
        else
        {
            newButton.setText(displayName);
            newButton.setToolTipText(functionalityGroup.getDescription());
        }

        return newButton;
    }

    private MenuItemButton createMenuItemButton(final T8FunctionalityHandle functionality)
    {
        final MenuItemButton newButton;
        Icon menuItemIcon;
        String displayName;

        menuItemIcon = functionality.getIcon();

        newButton = new MenuItemButton(menuButtonBackgroundPainter, menuButtonFocusBackgroundPainter, menuButtonSelectedBackgroundPainter);
        newButton.setIcon(menuItemIcon);
        newButton.setFont(MENU_BUTTON_FONT);
        newButton.setForeground(Color.DARK_GRAY);
        newButton.setFocusPainted(false); // Focus painting will be handled by the background painters.
        newButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                deselectAllTaskMenuButtons();
                newButton.setSelected(true);
                navigator.handleFunctionalitySelection(functionality);
            }
        });

        // Evaluate the display name expression.
        displayName = functionality.getDisplayName();
        newButton.setText(displayName);
        newButton.setToolTipText(functionality.getDescription());

        return newButton;
    }

    /**
     * Sets the selected {@code MenuItemButton} according to the index passed.
     * Method will not throw an {@code IndexOutOfBoundsException} if the index
     * specified is invalid.
     *
     * @param menuItemIndex The index of the {@code MenuItemButton} to set as
     *      being selected
     */
    void setSelectedMenuItemIndex(int menuItemIndex)
    {
        if (menuItemIndex != -1 && menuItemIndex < this.menuItemButtons.size())
        {
            this.menuItemButtons.get(menuItemIndex).setSelected(true);
        }
    }

    /**
     * Returns the currently selected {@code MenuItemButton} index.
     *
     * @return The currently selected {@code MenuItemButton} index
     */
    int getSelectedMenuItemIndex()
    {
        MenuItemButton menuItemButton;

        for (int btnIndex = 0; btnIndex < menuItemButtons.size(); btnIndex++)
        {
            menuItemButton = menuItemButtons.get(btnIndex);
            if (menuItemButton.isSelected()) return btnIndex;
        }

        return -1;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelHeader = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();
        jToolBarControls = new javax.swing.JToolBar();
        jButtonRefresh = new javax.swing.JButton();
        jScrollPaneMenu = new javax.swing.JScrollPane();

        setMinimumSize(new java.awt.Dimension(250, 300));
        setName(""); // NOI18N
        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(250, 300));
        setLayout(new java.awt.BorderLayout());

        jPanelHeader.setOpaque(false);
        jPanelHeader.setLayout(new java.awt.GridBagLayout());

        jLabelHeader.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/playIcon.png"))); // NOI18N
        jLabelHeader.setText("Header");
        jLabelHeader.setBorder(javax.swing.BorderFactory.createEmptyBorder(3, 2, 2, 2));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanelHeader.add(jLabelHeader, gridBagConstraints);

        jToolBarControls.setFloatable(false);
        jToolBarControls.setRollover(true);
        jToolBarControls.setOpaque(false);

        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/icons/refreshIcon.png"))); // NOI18N
        jButtonRefresh.setFocusable(false);
        jButtonRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRefresh.setOpaque(false);
        jButtonRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshActionPerformed(evt);
            }
        });
        jToolBarControls.add(jButtonRefresh);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelHeader.add(jToolBarControls, gridBagConstraints);

        add(jPanelHeader, java.awt.BorderLayout.PAGE_START);

        jScrollPaneMenu.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        add(jScrollPaneMenu, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshActionPerformed
        navigator.loadFunctionalityGroup(functionalityGroup.getParentGroup() == null ? functionalityGroup.getIdentifier() : functionalityGroup.getParentGroup().getIdentifier(), functionalityGroup.getIdentifier(), false);
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRefresh;
    private javax.swing.JLabel jLabelHeader;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JScrollPane jScrollPaneMenu;
    private javax.swing.JToolBar jToolBarControls;
    // End of variables declaration//GEN-END:variables
}
