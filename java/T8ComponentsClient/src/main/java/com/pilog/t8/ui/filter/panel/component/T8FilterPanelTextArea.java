package com.pilog.t8.ui.filter.panel.component;

import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.filter.panel.component.T8FilterPanelTextAreaDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.filter.panel.T8FilterPanelComponent;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jdesktop.swingx.JXTextField;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelTextArea extends JXTextField implements T8FilterPanelComponent, T8Component, FocusListener, KeyListener
{
    private final T8FilterPanelTextAreaDefinition definition;
    private final T8ComponentController controller;
    private final List<ChangeListener> changeListeners;
    private final T8FilterPanelComponentOperationHandler operationHandler;
    private final String dataEntityValueFieldIdentifier;
    private String dataEntityIdentifier;
    private T8DataFilterCriteria lastCriteria;

    public T8FilterPanelTextArea(T8FilterPanelTextAreaDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.dataEntityValueFieldIdentifier = definition.getDataEntityKeyFieldIdentifier();
        this.controller = controller;
        this.changeListeners = new ArrayList<>();
        this.operationHandler = new T8FilterPanelComponentOperationHandler(this);
        addFocusListener(this);
        addKeyListener(this);
        lastCriteria = new T8DataFilterCriteria();

        // We must be non-opaque since we won't fill all pixels.
        // This will also stop the UI from filling our background.
        setOpaque(false);

        // Add an empty border around us to compensate for
        // the rounded corners.
        setBorder(BorderFactory.createEmptyBorder(1, 3, 1, 4));
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        int width = getWidth() - 3;
        int height = getHeight() - 2;

        // Paint a rounded rectangle in the background.
        g.setColor(getBackground());

        g.fillRoundRect(getX() + 1, getY() + 1, width, height, 5, 5);
        g.setColor(Color.gray);
        g.drawRoundRect(getX() + 1, getY() + 1, width, height, 5, 5);

        // Now call the superclass behavior to paint the foreground.
        super.paintComponent(g);
    }

    @Override
    public T8DataFilterCriteria getFilterCriteria()
    {
        T8DataFilterCriteria filterCriteria = new T8DataFilterCriteria();
        if (Strings.isNullOrEmpty(getText()))
        {
            return filterCriteria;
        }
        if (!definition.isTagField())
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, dataEntityValueFieldIdentifier, T8DataFilterCriterion.DataFilterOperator.LIKE, this.getText());
        }
        else
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.OR,
                                           dataEntityValueFieldIdentifier,
                                           T8DataFilterCriterion.DataFilterOperator.EQUAL,
                                           this.getText());

        }
        return filterCriteria;
    }

//<editor-fold defaultstate="collapsed" desc="Boiler Plate">

    @Override
    public void refresh() throws Exception
    {
        //do nothing
    }

    @Override
    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        this.dataEntityIdentifier = dataEntityIdentifier;
    }

    @Override
    public Component getComponent()
    {
        return this;
    }

    @Override
    public void initialize(T8Context context)
    {
    }

    @Override
    public String toString()
    {
        return this.definition.getIdentifier();
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier,
                                                Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
    }

    @Override
    public T8Component getChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String childComponentIdentifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public void addChangeListener(ChangeListener changeListener)
    {
        changeListeners.add(changeListener);
    }

    @Override
    public void removeChangeListener(ChangeListener changeListener)
    {
        changeListeners.remove(changeListener);
    }

    @Override
    public void focusGained(FocusEvent e)
    {
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
    }
//</editor-fold>

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.setPrompt(definition.getDisplayLabel());
        this.setAutoscrolls(true);
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void fireStateChanged()
    {
        for (ChangeListener changeListener : changeListeners)
        {
            changeListener.stateChanged(new ChangeEvent(this));
        }
    }

    @Override
    public void focusLost(FocusEvent e)
    {
        T8DataFilterCriteria newCriteria = getFilterCriteria();
        if (newCriteria != null && !newCriteria.equalsFilterCriteria(lastCriteria))
        {
            lastCriteria = newCriteria;
            fireStateChanged();
        }
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        if (e.getKeyCode() == e.VK_ENTER)
        {
            T8DataFilterCriteria newCriteria = getFilterCriteria();
            if (newCriteria != null && !newCriteria.equalsFilterCriteria(lastCriteria))
            {
                lastCriteria = newCriteria;
                fireStateChanged();
            }
        }
    }
}
