package com.pilog.t8.ui.file.uploadpanel;

import com.pilog.t8.definition.ui.file.uploadpanel.T8FileUploadPanelApiHandler;
import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FileUploadPanelOperationHandler extends T8ComponentOperationHandler
{
    private final T8FileUploadPanel uploadPanel;

    public T8FileUploadPanelOperationHandler(T8FileUploadPanel panel)
    {
        super(panel);
        this.uploadPanel = panel;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        if (operationIdentifier.equals(T8FileUploadPanelApiHandler.OPERATION_SET_UPLOAD_CONTEXT))
        {
            String fileContextIid;
            String filePath;

            fileContextIid = (String)operationParameters.get(T8FileUploadPanelApiHandler.PARAMETER_FILE_CONTEXT_IID);
            filePath = (String)operationParameters.get(T8FileUploadPanelApiHandler.PARAMETER_FILE_PATH);

            uploadPanel.setUploadContext(null, fileContextIid, filePath);
            return null;
        }
        else if (operationIdentifier.equals(T8FileUploadPanelApiHandler.OPERATION_SET_UPLOAD_FILE_PATH))
        {
            String filePath;

            filePath = (String)operationParameters.get(T8FileUploadPanelApiHandler.PARAMETER_FILE_PATH);

            uploadPanel.setUploadFilePath(filePath);
            return null;
        }
        else return super.executeOperation(operationIdentifier, operationParameters);
    }
}
