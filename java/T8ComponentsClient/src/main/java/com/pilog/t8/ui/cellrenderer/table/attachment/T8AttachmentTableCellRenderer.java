package com.pilog.t8.ui.cellrenderer.table.attachment;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellRenderableComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent;
import com.pilog.t8.definition.ui.cellrenderer.table.attachment.T8AttachmentTableCellRendererDefinition;
import com.pilog.t8.ui.attachment.T8AttachmentViewer;
import com.pilog.t8.security.T8Context;
import java.util.Map;
import java.util.HashMap;

import static com.pilog.t8.definition.ui.cellrenderer.table.attachment.T8AttachmentTableCellRendererAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8AttachmentTableCellRenderer extends T8AttachmentViewer implements T8TableCellRendererComponent
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8AttachmentTableCellRenderer.class);

    private final T8Context context;
    private final T8AttachmentTableCellRendererDefinition definition;
    private final int thumbnailWidth;
    private final int thumbnailHeight;

    public T8AttachmentTableCellRenderer(T8ComponentController controller, T8AttachmentTableCellRendererDefinition definition)
    {
        super(controller);
        this.context = controller.getContext();
        this.definition = definition;
        this.thumbnailWidth = definition.getThumbnailWidth();
        this.thumbnailHeight = definition.getThumbnailHeight();
    }

    @Override
    public T8AttachmentTableCellRendererDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void setRendererData(T8TableCellRenderableComponent sourceComponent, Map<String, Object> dataRow, String fieldIdentifier, Map<RenderParameter, Object> renderParameters)
    {
        T8AttachmentThumbnail thumbnail;
        int rowIndex;

        rowIndex = (int)renderParameters.get(RenderParameter.ROW);
        thumbnail = (T8AttachmentThumbnail)sourceComponent.getAdditionalRenderData(rowIndex, fieldIdentifier);
        setAttachmentThumbnail(thumbnail);
        setSelected((boolean) renderParameters.get(RenderParameter.SELECTED));
        setFocused((boolean) renderParameters.get(RenderParameter.FOCUSED));
        setLoading(false);
        setBorder(sourceComponent.getUnselectedBorder());
        setFont(sourceComponent.getCellFont());

        if (isFocused()) setBorder(sourceComponent.getFocusedBorder());
        if (isSelected()) setBorder(sourceComponent.getSelectedBorder());
        if (isFocused() && isSelected()) setBorder(sourceComponent.getFocusedSelectedBorder());
    }

    @Override
    public T8AttachmentThumbnail getAdditionalRenderData(Map<String, Object> dataRow, String fieldId)
    {
        String attachmentId;

        attachmentId = (String)dataRow.get(fieldId);
        if (T8IdentifierUtilities.isConceptID(attachmentId))
        {
            try
            {
                Map<String, Object> inputParameters;
                Map<String, Object> outputParameters;

                inputParameters = new HashMap<>();
                inputParameters.put(PARAMETER_ATTACHMENT_ID, attachmentId);
                inputParameters.put(PARAMETER_WIDTH, thumbnailWidth);
                inputParameters.put(PARAMETER_HEIGHT, thumbnailHeight);
                outputParameters = T8MainServerClient.executeSynchronousOperation(context, O_REC_RETRIEVE_ATTACHMENT_TUMBNAIL, inputParameters);
                return (T8AttachmentThumbnail)outputParameters.get(PARAMETER_ATTACHMENT_TUMBNAIL);
            }
            catch (Exception ex)
            {
                LOGGER.log("Failed to retrieve attachment details for attachment " + attachmentId, ex);
                return null;
            }
        }
        else return null;
    }
}
