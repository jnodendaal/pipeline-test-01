package com.pilog.t8.ui.entityeditor.combobox;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.ui.entityeditor.combobox.T8ComboBoxDataEntityEditorDefinition;
import com.pilog.t8.ui.combobox.T8ComboBox;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8ComboBoxDataEntityEditor extends T8ComboBox implements T8DataEntityEditorComponent
{
    private T8ComboBoxDataEntityEditorDefinition definition;
    private T8ComboBoxDataEntityEditorOperationHandler operationHandler;
    private T8DataEntity editorEntity;

    private static final String COMBO_BOX_CONTENT_FILTER_IDENTIFIER = "$FILTER_COMBO_BOX_CONTENT";

    public T8ComboBoxDataEntityEditor(T8ComboBoxDataEntityEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.operationHandler = new T8ComboBoxDataEntityEditorOperationHandler(this);
        this.addItemListener(new SelectionChangeListener());
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public void setEditorDataEntity(T8DataEntity dataEntity)
    {
        editorEntity = dataEntity;
        if(dataEntity != null)
        {
            setSelectedDataValue(dataEntity.getFieldValue(definition.getEditorDataEntityFieldIdentifier()));
        } else setSelectedDataValue(null);
    }

    @Override
    public T8DataEntity getEditorDataEntity()
    {
        return editorEntity;
    }

    @Override
    public boolean commitEditorChanges()
    {
        if (editorEntity != null)
        {
            editorEntity.setFieldValue(definition.getEditorDataEntityFieldIdentifier(), getSelectedDataValue());
            return true;
        }
        else return true;
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return definition.getEditorDataEntityIdentifier();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return definition.getParentEditorIdentifier();
    }

    private class SelectionChangeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                // In future we may have to check the definition to see if we need to commit changes immediately.
                commitEditorChanges();
            }
        }
    }
}
