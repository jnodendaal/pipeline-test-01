package com.pilog.t8.ui.celleditor.table.datalookupfield;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellEditableComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent;
import com.pilog.t8.ui.table.T8TableCellEditorComponent.EditParameter;
import com.pilog.t8.definition.ui.celleditor.table.datalookupfield.T8DataLookupFieldTableCellEditorDefinition;
import com.pilog.t8.ui.datalookupfield.T8DataLookupField;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupFieldTableCellEditor extends T8DataLookupField implements T8TableCellEditorComponent
{
    private final T8DataLookupFieldTableCellEditorDefinition definition;

    public T8DataLookupFieldTableCellEditor(T8DataLookupFieldTableCellEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
    }

    @Override
    public void setEditorData(T8TableCellEditableComponent sourceComponent, Map<String, Object> dataRow, String editorColumnIdentifier, Map<EditParameter, Object> editParameters)
    {
        Map<String, String> inputFieldMapping;
        Map<String, Object> inputData;

        inputFieldMapping = definition.getEditorInputFieldMapping();
        inputData = T8IdentifierUtilities.mapParameters(dataRow, inputFieldMapping);
        setSelectedDataEntityKey(inputData);
    }

    @Override
    public Map<String, Object> getEditorData()
    {
        T8DataEntity selectedEntity;

        selectedEntity = getSelectedDataEntity();
        if (selectedEntity != null)
        {
            Map<String, String> outputFieldMapping;

            outputFieldMapping = definition.getEditorOutputFieldMapping();
            return T8IdentifierUtilities.mapParameters(selectedEntity.getFieldValues(), outputFieldMapping);
        }
        else return null;
    }

    @Override
    public boolean singleClickEdit()
    {
        return false;
    }
}
