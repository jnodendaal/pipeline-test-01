package com.pilog.t8.ui.filter.panel.datasource;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.T8ClientDataManagerScriptFacade;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.filter.panel.datasource.T8FilterPanelDataEntityIconDefinition;
import com.pilog.t8.definition.ui.filter.panel.datasource.T8FilterPanelEntityDataSourceDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItem;
import com.pilog.t8.ui.filter.panel.T8FilterPanelListComponentItemDataSource;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8FilterPanelEntityDataSource implements T8FilterPanelListComponentItemDataSource
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8FilterPanelEntityDataSource.class.getName());

    private enum ResultsType
    {
        DataEntityResults,
        ListResults,
        NoResults
    }

    private String nameFieldId;
    private String keyFieldId;
    private T8DataEntityResults _entityResults;
    private List<T8DataEntity> _entityListResults;
    private ResultsType resultSetType;
    private T8FilterPanelEntityDataSourceDefinition definition;
    private T8DataFilter dataFilter;
    private ExpressionEvaluator expressionEvaluator;
    private boolean expressionFailure;
    private Map<T8FilterPanelDataEntityIconDefinition, ExpressionEvaluator> iconExpressionEvaluators;
    private int currentListIndex = -1;

    public T8FilterPanelEntityDataSource(
            T8FilterPanelEntityDataSourceDefinition definition)
    {
        this(definition.getDisplayField(), definition.getValueField());
        this.definition = definition;
    }

    private T8FilterPanelEntityDataSource(String nameFieldIdentifier,
                                                String keyFieldIdentifier)
    {
        this.keyFieldId = keyFieldIdentifier;
        this.nameFieldId = nameFieldIdentifier;
    }

    public T8FilterPanelEntityDataSource(T8DataEntityResults entityResults,
                                               String nameFieldIdentifier,
                                               String keyFieldIdentifier)
    {
        this(nameFieldIdentifier, keyFieldIdentifier);
        this._entityResults = entityResults;
        this.resultSetType = ResultsType.DataEntityResults;
    }

    public T8FilterPanelEntityDataSource(List<T8DataEntity> entityResults,
                                               String nameFieldIdentifier,
                                               String keyFieldIdentifier)
    {
        this(nameFieldIdentifier, keyFieldIdentifier);
        this._entityListResults = entityResults;
        this.resultSetType = ResultsType.ListResults;
    }

    @Override
    public synchronized T8FilterPanelListComponentItem next()
    {
        T8FilterPanelListComponentItem listItem;
        switch (resultSetType)
        {
            case ListResults:
            {
                T8DataEntity entity = _entityListResults.get(currentListIndex);
                if (entity != null)
                {
                    listItem = new T8FilterPanelListComponentItem(getDisplayString(entity), entity.getFieldValue(entity.getIdentifier() + keyFieldId), getIconDefinition(entity));
                }
                else
                {
                    listItem = new T8FilterPanelListComponentItem(definition.getNullValueDisplayName() == null
                                                                  ? " "
                                                                  : definition.getNullValueDisplayName(), null, definition.getNullValueIconDefinition());
                }
                return listItem;
            }
            case DataEntityResults:
            {
                T8DataEntity entity = _entityResults.get();
                listItem = new T8FilterPanelListComponentItem(getDisplayString(entity), entity.getFieldValue(entity.getIdentifier() + keyFieldId), getIconDefinition(entity));
                return listItem;
            }
            default:
            {
                return null;
            }
        }
    }

    private T8IconDefinition getIconDefinition(T8DataEntity dataEntity)
    {
        if (definition.getIcons() == null || definition.getIcons().isEmpty())
        {
            return null;
        }
        for (T8FilterPanelDataEntityIconDefinition t8FilterPanelDataEntityIconDefinition : definition.getIcons())
        {
            if (!Strings.isNullOrEmpty(t8FilterPanelDataEntityIconDefinition.getConditionExpression()))
            {
                if (iconExpressionEvaluators == null)
                {
                    iconExpressionEvaluators = new HashMap<>();
                }
                if (!iconExpressionEvaluators.containsKey(t8FilterPanelDataEntityIconDefinition))
                {
                    ExpressionEvaluator iconExpressionEvaluator = new ExpressionEvaluator();
                    iconExpressionEvaluators.put(t8FilterPanelDataEntityIconDefinition, iconExpressionEvaluator);
                    try
                    {
                        iconExpressionEvaluator.compileExpression(t8FilterPanelDataEntityIconDefinition.getConditionExpression());
                    }
                    catch (EPICSyntaxException ex)
                    {
                        iconExpressionEvaluators.put(t8FilterPanelDataEntityIconDefinition, null);
                        LOGGER.log("Failed to parse expression " + t8FilterPanelDataEntityIconDefinition.getConditionExpression(), ex);
                    }
                }
                try
                {
                    if (iconExpressionEvaluators.get(t8FilterPanelDataEntityIconDefinition) != null)
                    {
                        if (iconExpressionEvaluators.get(t8FilterPanelDataEntityIconDefinition).evaluateBooleanExpression(dataEntity.getFieldValues(), null))
                        {
                            return t8FilterPanelDataEntityIconDefinition.getIconDefinition();
                        }
                    }
                }
                catch (EPICRuntimeException ex)
                {
                    LOGGER.log("Failed to evaluate expression " + t8FilterPanelDataEntityIconDefinition.getConditionExpression(), ex);

                }
            }
        }
        return null;
    }

    private String getDisplayString(T8DataEntity dataEntity)
    {
        if (Strings.isNullOrEmpty(definition.getDisplayFieldExpression()) || expressionFailure)
        {
            Object fieldValue = dataEntity.getFieldValue(dataEntity.getIdentifier() + nameFieldId);
            return fieldValue == null ? "" : fieldValue.toString();
        }
        if (expressionEvaluator == null)
        {
            expressionEvaluator = new ExpressionEvaluator();
            try
            {
                expressionEvaluator.compileExpression(definition.getDisplayFieldExpression());
                expressionFailure = false;
            }
            catch (EPICSyntaxException ex)
            {
                expressionFailure = true;
                LOGGER.log("Failed to parse expression " + definition.getDisplayFieldExpression(), ex);
            }
        }
        Object evaluated = null;
        try
        {
            evaluated = expressionEvaluator.evaluateExpression(dataEntity.getFieldValues(), null);
        }
        catch (EPICRuntimeException ex)
        {
            LOGGER.log("Failed to evaluate expression " + definition.getDisplayFieldExpression(), ex);
        }
        if (evaluated == null)
        {
            return dataEntity.getFieldValue(dataEntity.getIdentifier() + nameFieldId).toString();
        }
        return evaluated.toString();
    }

    @Override
    public synchronized boolean hasNext() throws Exception
    {
        switch (resultSetType)
        {
            case DataEntityResults:
            {
                return _entityResults.next();
            }
            case ListResults:
            {
                return _entityListResults.size() > ++currentListIndex;
            }
            default:
            {
                return false;
            }
        }
    }

    @Override
    public void initialize(T8Context context, Map<String, Object> inputParameters)
    {
        List<T8DataEntity> dataEntity;
        T8DataFilter dataFilter;

        this.resultSetType = ResultsType.NoResults;
        this.currentListIndex = -1;
        dataFilter = this.dataFilter;
        if(dataFilter == null && definition.getDataFilter() != null)
        {
            dataFilter = definition.getDataFilter().getNewDataFilterInstance(context, inputParameters);
        }
        try
        {
            dataEntity = new T8ClientDataManagerScriptFacade(context).select(definition.getDataEntityIdentifier(), dataFilter, 0, 200);
            if (dataEntity != null)
            {
                if(dataEntity.size() > 100) LOGGER.log("The data source " + definition.getPublicIdentifier() + " returned a suspicious amount of data: " + dataEntity.size());

                this._entityListResults = dataEntity;
                if (definition.isAddNullValueOn() != null && definition.isAddNullValueOn())
                {
                    this._entityListResults.add(0, null);
                }
                this.resultSetType = ResultsType.ListResults;
            }
        }
        catch (Exception ex)
        {
            LOGGER.log(ex);
        }
    }

    @Override
    public void setNewDataSourceFilter(T8DataFilter dataFilter) throws Exception
    {
        this.dataFilter = dataFilter;
    }
}
