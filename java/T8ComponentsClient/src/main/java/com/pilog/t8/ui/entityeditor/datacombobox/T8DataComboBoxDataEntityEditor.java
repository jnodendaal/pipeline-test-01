package com.pilog.t8.ui.entityeditor.datacombobox;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.ui.entityeditor.datacombobox.T8DataComboBoxDataEntityEditorDefinition;
import com.pilog.t8.ui.datacombobox.T8DataComboBox;
import com.pilog.t8.utilities.collections.Maps;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataComboBoxDataEntityEditor extends T8DataComboBox implements T8DataEntityEditorComponent
{
    private T8DataComboBoxDataEntityEditorDefinition definition;
    private T8DataComboBoxDataEntityEditorOperationHandler operationHandler;
    private Map<String, String> editorToComboMapping;
    private Map<String, String> editorToComboFilterMapping;
    private Map<String, String> comboToEditorMapping;
    private T8DataEntity editorEntity;

    private static final String COMBO_BOX_CONTENT_FILTER_IDENTIFIER = "$FILTER_COMBO_BOX_CONTENT";

    public T8DataComboBoxDataEntityEditor(T8DataComboBoxDataEntityEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.operationHandler = new T8DataComboBoxDataEntityEditorOperationHandler(this);
        this.editorToComboMapping = definition.getEditorFieldMapping();
        this.editorToComboFilterMapping = definition.getEditorFilterFieldMapping();
        this.comboToEditorMapping = T8IdentifierUtilities.createReverseIdentifierMap(editorToComboMapping);
        this.addItemListener(new SelectionChangeListener());
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public void setEditorDataEntity(T8DataEntity dataEntity)
    {
        editorEntity = dataEntity;
        if (editorEntity != null)
        {
            Map<String, Object> fieldValues;

            // No filter the content of the combobox if required.
            if (!Maps.isNullOrEmpty(editorToComboFilterMapping))
            {
                Map<String, Object> filterFieldValues;

                filterFieldValues = T8IdentifierUtilities.mapParameters(editorEntity.getFieldValues(), editorToComboFilterMapping);
                setPrefilter(COMBO_BOX_CONTENT_FILTER_IDENTIFIER, new T8DataFilter(definition.getDataEntityIdentifier(), filterFieldValues));
                refreshData();
            }

            // Set the selected key on the combobox.
            fieldValues = T8IdentifierUtilities.mapParameters(editorEntity.getFieldValues(), editorToComboMapping);
            setSelectedKey(fieldValues);
        }
        else setSelectedDataEntity(null);
    }

    @Override
    public T8DataEntity getEditorDataEntity()
    {
        return editorEntity;
    }

    @Override
    public boolean commitEditorChanges()
    {
        if (editorEntity != null)
        {
            Map<String, String> fieldMappings;

            fieldMappings = definition.getEditorFieldMapping();
            if (fieldMappings != null)
            {
                T8DataEntity selectedEntity;

                selectedEntity = getSelectedDataEntity();
                if (selectedEntity != null)
                {
                    Map<String, Object> fieldValues;

                    fieldValues = T8IdentifierUtilities.mapParameters(selectedEntity.getFieldValues(), comboToEditorMapping);
                    editorEntity.setFieldValues(fieldValues);
                    return true;
                }
                else
                {
                    //If the selected Data Entity is null, then the field values for the
                    for (String fieldIdentifier : comboToEditorMapping.values())
                    {
                        editorEntity.setFieldValue(fieldIdentifier, null);
                    }
                    return true;
                }
            }
            else throw new RuntimeException("No field mappings set in entity editor: " + definition);
        }
        else return true;
    }

    @Override
    public String getEditorDataEntityIdentifier()
    {
        return definition.getDataEntityIdentifier();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return definition.getParentEditorIdentifier();
    }

    private class SelectionChangeListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
                // In future we may have to check the definition to see if we need to commit changes immediately.
                commitEditorChanges();
            }
        }
    }
}
