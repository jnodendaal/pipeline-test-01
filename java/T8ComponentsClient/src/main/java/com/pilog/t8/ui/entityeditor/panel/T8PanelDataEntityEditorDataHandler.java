package com.pilog.t8.ui.entityeditor.panel;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.ui.entityeditor.panel.T8PanelDataEntityEditorDefinition;
import java.util.HashMap;
import java.util.List;

import static com.pilog.t8.definition.data.T8DataManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8PanelDataEntityEditorDataHandler
{
    private final T8PanelDataEntityEditorDefinition editorDefinition;
    private int pageOffset;
    private int pageSize;
    private int rowCount;

    public T8PanelDataEntityEditorDataHandler(T8PanelDataEntityEditorDefinition editorDefinition)
    {
        this.editorDefinition = editorDefinition;
        this.pageOffset = 0;
        this.pageSize = 1;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public void resetPageOffset()
    {
        pageOffset = 0;
    }

    public int getPageOffset()
    {
        return pageOffset;
    }

    public void setPageOffset(int offset)
    {
        pageOffset = offset;
    }

    public int getPageNumber()
    {
        return (pageOffset/pageSize) + 1;
    }

    public void incrementPage()
    {
        if((pageOffset + pageSize) < rowCount)
           pageOffset += pageSize;
    }

    public void decrementPage()
    {
        if (pageOffset > pageSize)
        {
            pageOffset -= pageSize;
        }
        else pageOffset = 0;
    }

    /**
     * Moves to the pageOffset provided
     * @param pageOffset the new page offset
     */
    public void goToPage(int pageOffset)
    {
        if(pageOffset < rowCount && pageOffset >= 0)
            this.pageOffset = pageOffset;
    }

    /**
     * Finds the starting offset of the page on which the provided row can be found. If the row is a negative number of larger than the max row count
     * 0 will be returned.
     * @param row The row to search for.
     * @return The pageOffset of the starting page on which the row can be found.
     */
    public int findRowPage(int row)
    {
        if(row < 0 || row > rowCount)
            return 0;

        int currentPage = 0;
        while(currentPage < rowCount)
        {
            if(row >= currentPage && row < (currentPage + pageSize))
                return currentPage;
            else
                currentPage += pageSize;
        }

        return 0;
    }

    public int countDataEntities(T8Context context, T8DataFilter dataFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, editorDefinition.getEditorDataEntityIdentifier());
        operationParameters.put(PARAMETER_DATA_FILTER, dataFilter);

        rowCount = (Integer)T8MainServerClient.executeSynchronousOperation(context, OPERATION_COUNT_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_COUNT);
        return rowCount;
    }

    public T8DataEntity loadDataEntity(T8Context context, T8DataFilter dataFilter) throws Exception
    {
        HashMap<String, Object> operationParameters;
        List<T8DataEntity> retrievedData;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DATA_ENTITY_IDENTIFIER, editorDefinition.getEditorDataEntityIdentifier());
        operationParameters.put(PARAMETER_DATA_FILTER, dataFilter);
        operationParameters.put(PARAMETER_PAGE_OFFSET, pageOffset);
        operationParameters.put(PARAMETER_PAGE_SIZE, pageSize);

        retrievedData = (List<T8DataEntity>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_SELECT_DATA_ENTITIES, operationParameters).get(PARAMETER_DATA_ENTITY_LIST);
        return retrievedData.size() > 0 ? retrievedData.get(0) : null;
    }

    public void saveDataEntity(T8Context context, T8DataEntity dataEntity) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_DATA_ENTITY, dataEntity);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_UPDATE_DATA_ENTITY, operationParameters);
    }
}
