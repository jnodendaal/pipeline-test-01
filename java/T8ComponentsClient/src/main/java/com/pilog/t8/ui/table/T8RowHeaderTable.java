package com.pilog.t8.ui.table;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.entity.T8DataEntityValidationError;
import com.pilog.t8.definition.ui.table.T8TableColumnDefinition;
import com.pilog.t8.definition.ui.table.T8TableRowChange;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.List;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * @author Bouwer du Preez
 */
public class T8RowHeaderTable extends JTable
{
    private final T8Table t8Table;
    private final JTable table;
    private final T8TableModelHandler modelHandler;
    private final T8TableDataChangeHandler changeHandler;
    private int currentToolTipRowIndex; // Holds a row index for which tooltip text is currently set up.
    private boolean selectionUpdateFromTable; // A boolean value that is set when the selection of row headers is being updated from the target table.
    private boolean selectionUpdateFromHeader; // A boolean value that is set when the selection of table rows is being updated from the row header table.

    public T8RowHeaderTable(T8Table t8Table, JTable table)
    {
        this.t8Table = t8Table;
        this.table = table;
        this.modelHandler = t8Table.getTableModelHandler();
        this.changeHandler = t8Table.getTableDataChangeHandler();
        this.setAutoCreateColumnsFromModel(false);
        this.setGridColor(Color.WHITE);
        this.addMouseMotionListener(new RowHeaderMouseListener());
        this.setRowSelectionAllowed(false);
        this.table.getSelectionModel().addListSelectionListener(new TableSelectionListener());
        this.currentToolTipRowIndex = -1;
        this.selectionUpdateFromTable = false;
        this.selectionUpdateFromHeader = false;
    }

    private void updateTooltip(Point mouseLocation)
    {
        int rowIndex;

        rowIndex = this.rowAtPoint(mouseLocation);
        if ((rowIndex > -1) && (rowIndex != currentToolTipRowIndex))
        {
            T8TableRowHeader rowHeader;

            rowHeader = modelHandler.getRowHeader(rowIndex);
            setToolTipText(createTooltipText(rowHeader));
            currentToolTipRowIndex = rowIndex;
        }
    }

    private String createTooltipText(T8TableRowHeader rowHeader)
    {
        List<T8DataEntityValidationError> validationErrors;
        String rowGUID;
        boolean rowUpdated;

        // Get the row GUID.
        rowGUID = rowHeader.getDataEntity().getGUID();
        rowUpdated = changeHandler.isRowUpdated(rowGUID);
        validationErrors = rowHeader.getValidationErrors();

        if ((validationErrors != null) && (validationErrors.size() > 0) || rowUpdated)
        {
            StringBuffer text;

            text = new StringBuffer();
            text.append("<html>\n");

            // Append the updated data information if applicable.
            if (rowUpdated)
            {
                T8TableRowChange change;
                T8DataEntity oldEntity;
                T8DataEntity newEntity;

                change = changeHandler.getUpdateChange(rowGUID);
                text.append("<h4>Row Updates</h4>\n");
                text.append("<table>");
                text.append("<tr><td><b>Field</b></td><td width=100><b>Old Value</b></td><td width=200><b>New Value</b></td></tr>");

                oldEntity = change.getOldDataEntity();
                newEntity = change.getNewDataEntity();
                for (T8TableColumnDefinition columnDefinition : t8Table.getComponentDefinition().getColumnDefinitions())
                {
                    String fieldIdentifier;

                    fieldIdentifier = columnDefinition.getFieldIdentifier();
                    if ((columnDefinition.isVisible()) && (newEntity.getFieldValues().containsKey(fieldIdentifier)))
                    {

                        text.append("<tr><td>");
                        text.append(columnDefinition.getColumnName());
                        text.append("</td><td>");
                        text.append(oldEntity.getFieldValue(fieldIdentifier));
                        text.append("</td><td>");
                        text.append(newEntity.getFieldValue(fieldIdentifier));
                        text.append("</td></tr>");
                    }
                }

                text.append("</table>");
            }

            // Append the validation error information if applicable.
            if (validationErrors != null)
            {
                List<T8DataEntityValidationError> errorList;

                text.append("<h4>Validation Errors</h4>\n");
                text.append("<table>");
                text.append("<tr><td><b>Field</b></td><td><b>Validation Error Message</b></td></tr>");

                errorList = rowHeader.getValidationErrors();
                for (T8DataEntityValidationError error : errorList)
                {
                    String fieldIdentifier;
                    String columnName;

                    columnName = error.getEntityFieldIdentifier();
                    fieldIdentifier = error.getEntityFieldIdentifier();
                    for (T8TableColumnDefinition columnDefinition : t8Table.getComponentDefinition().getColumnDefinitions())
                    {
                        if (columnDefinition.getFieldIdentifier().equals(fieldIdentifier))
                        {
                            columnName = columnDefinition.getColumnName();
                        }
                    }

                    text.append("<tr><td>");
                    text.append(columnName);
                    text.append("</td><td>");
                    text.append(error.getErrorMessage());
                    text.append("</td></tr>");
                }

                text.append("</table>");
            }

            text.append("</html>");
            return text.toString();
        }
        else return null;
    }

    private class RowHeaderMouseListener implements MouseMotionListener
    {
        @Override
        public void mouseDragged(MouseEvent e)
        {
        }

        @Override
        public void mouseMoved(MouseEvent e)
        {
            updateTooltip(e.getPoint());
        }
    }

    private class TableSelectionListener implements ListSelectionListener
    {
        @Override
        public void valueChanged(ListSelectionEvent e)
        {
            if (!selectionUpdateFromHeader)
            {
                int[] selectedIndices;

                selectionUpdateFromTable = true;

                clearSelection();
                selectedIndices = table.getSelectedRows();
                for (int selectedIndex : selectedIndices)
                {
                    getSelectionModel().addSelectionInterval(selectedIndex, selectedIndex);
                }

                selectionUpdateFromTable = false;
            }
        }
    }
}
