package com.pilog.t8.ui.datarecord.celleditor;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class T8RecordTableCellRenderer extends JLabel implements TableCellRenderer
{
    private final TableCellPropertyResolver propertyResolver;
    private final TerminologyProvider terminologyProvider;
    private final T8DataRecordValueStringGenerator valueStringGenerator;
    private DisplayMode displayMode;
    private final Border focusSelectedBorder;
    private final Border focusBorder;
    private final Border selectedBorder;
    private final Border deselectedBorder;

    public static final Color CHILD_ROW_COLOR = new Color(200, 200, 255);
    public static final Border DESELECTED_BORDER = new EmptyBorder(1, 1, 1, 1);
    public static final Border SELECTED_BORDER = new LineBorder(Color.BLUE, 1);
    public static final Border FOCUS_BORDER = UIManager.getBorder("Table.focusCellHighlightBorder");
    public static final Border FOCUS_SELECTED_BORDER = new CompoundBorder(SELECTED_BORDER, FOCUS_BORDER);

    public enum DisplayMode {PROPERTY_VALUE, PROPERTY_FFT};

    public T8RecordTableCellRenderer(TableCellPropertyResolver propertyResolver)
    {
        this.propertyResolver = propertyResolver;
        this.terminologyProvider = propertyResolver.getTerminologyProvider();
        this.valueStringGenerator = new T8DataRecordValueStringGenerator();
        this.valueStringGenerator.setIncludePropertyFFT(false);
        this.valueStringGenerator.setTerminologyProvider(terminologyProvider);
        this.valueStringGenerator.setValueConceptRenderType(T8DataRecordValueStringGenerator.ConceptRenderType.TERM);
        this.displayMode = DisplayMode.PROPERTY_VALUE;
        this.focusSelectedBorder = FOCUS_SELECTED_BORDER;
        this.focusBorder = FOCUS_BORDER;
        this.selectedBorder = SELECTED_BORDER;
        this.deselectedBorder = DESELECTED_BORDER;
    }

    public void setDisplayMode(DisplayMode mode)
    {
        displayMode = mode;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        JTable.DropLocation dropLocation;

        // If this row is a drop location, select it.
        dropLocation = table.getDropLocation();
        if ((dropLocation) != null && (!dropLocation.isInsertRow()) && (!dropLocation.isInsertColumn()) && (dropLocation.getRow() == row) && (dropLocation.getColumn() == column))
        {
            isSelected = true;
        }

        // Set the cell colors.
        setForeground(table.getForeground());
        setBackground(table.getBackground());
        setFont(table.getFont());

        // Set the border according to the focus state.
        if (hasFocus)
        {
            if (isSelected)
            {
                setBorder(focusSelectedBorder);
            }
            else
            {
                setBorder(focusBorder);
            }
        }
        else if (isSelected)
        {
            setBorder(selectedBorder);
        }
        else
        {
            setBorder(deselectedBorder);
        }

        // Set the cell's text value.
        setValue((RecordProperty)value);
        return this;
    }

    private void setValue(RecordProperty recordProperty)
    {
        if (recordProperty != null)
        {
            if (displayMode == DisplayMode.PROPERTY_VALUE)
            {
                RecordValue recordValue;

                recordValue = recordProperty.getRecordValue();
                if (recordValue != null)
                {
                    RequirementType requirementType;

                    requirementType = recordValue.getValueRequirement().getRequirementType();
                    if (RequirementType.DOCUMENT_REFERENCE.equals(requirementType))
                    {
                        setText(recordValue.getSubValues().size() + " " + propertyResolver.translate("Referenced Documents"));
                    }
                    else
                    {
                        StringBuilder valueString;

                        valueString = valueStringGenerator.generateValueString(recordValue);
                        setText(valueString.toString());
                    }
                }
                else
                {
                    setText(null);
                }
            }
            else // FFT Display Mode.
            {
                setText(recordProperty.getFFT());
            }
        }
        else setText(null);
    }
}
