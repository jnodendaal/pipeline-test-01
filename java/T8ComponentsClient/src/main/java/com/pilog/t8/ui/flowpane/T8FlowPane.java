package com.pilog.t8.ui.flowpane;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.flowpane.T8FlowPaneDefinition;
import com.pilog.t8.definition.ui.flowpane.T8FlowPaneDefinition.FlowAlignment;
import java.awt.Component;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.jdesktop.swingx.JXPanel;

/**
 * @author Bouwer du Preez
 */
public class T8FlowPane extends JXPanel implements T8Component
{
    private final T8FlowPaneDefinition definition;
    private final T8ComponentController controller;
    private final LinkedHashMap<String, T8Component> childComponents;

    public T8FlowPane(T8FlowPaneDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.childComponents = new LinkedHashMap<>();
        this.setOpaque(definition.isOpaque());
        setupLayout();
    }
    
    private void setupLayout()
    {
        FlowLayout layout;
        FlowAlignment alignment;
        
        alignment = definition.getAlignment();
        
        layout = new FlowLayout();
        if (alignment == FlowAlignment.LEFT)
        {
            layout.setAlignment(FlowLayout.LEFT);
        }
        else if (alignment == FlowAlignment.RIGHT)
        {
            layout.setAlignment(FlowLayout.RIGHT);
        }
        else
        {
            layout.setAlignment(FlowLayout.CENTER);
        }
        
        layout.setHgap(definition.getHorizontalGap());
        layout.setVgap(definition.getVerticalGap());
        setLayout(layout);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component component, Object constraints)
    {
        String componentIdentifier;
        
        componentIdentifier = component.getComponentDefinition().getIdentifier();
        childComponents.put(componentIdentifier, component);
        add((Component)component, componentIdentifier);
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        return childComponents.get(identifier);
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        T8Component childComponent;
        
        childComponent = childComponents.get(identifier);
        if (childComponent != null)
        {
            this.remove((Component)childComponent);
            return childComponent;
        }
        else return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return new ArrayList<>(childComponents.values());
    }

    @Override
    public int getChildComponentCount()
    {
        return childComponents.size();
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
        childComponents.clear();
    }
}
