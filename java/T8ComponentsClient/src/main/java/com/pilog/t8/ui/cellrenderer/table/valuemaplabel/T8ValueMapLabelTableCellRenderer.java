package com.pilog.t8.ui.cellrenderer.table.valuemaplabel;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.script.T8ClientExpressionEvaluator;
import com.pilog.t8.T8Log;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.table.T8TableCellRenderableComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent;
import com.pilog.t8.ui.table.T8TableCellRendererComponent.RenderParameter;
import com.pilog.t8.definition.gfx.font.T8FontDefinition;
import com.pilog.t8.definition.gfx.image.T8IconDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.cellrenderer.T8ConditionalCellFormattingDefinition;
import com.pilog.t8.definition.ui.cellrenderer.table.valuemaplabel.T8ValueMapLabelTableCellRendererDefinition;
import com.pilog.t8.security.T8Context;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.jdesktop.swingx.JXLabel;

/**
 * @author Bouwer du Preez
 */
public class T8ValueMapLabelTableCellRenderer extends JXLabel implements T8TableCellRendererComponent
{
    private final T8ValueMapLabelTableCellRendererDefinition definition;
    private final T8ComponentController controller;
    private final T8Context context;
    private final ExpressionEvaluator formattingExpressionEvaluator;
    private final Map<Object, String> valueMap;

    private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

    public T8ValueMapLabelTableCellRenderer(T8ValueMapLabelTableCellRendererDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.formattingExpressionEvaluator = new ExpressionEvaluator();
        this.valueMap = new HashMap<>();
        this.setOpaque(true);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }

    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String string, Map<String, Object> map)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> map)
    {
        ExpressionEvaluator valueExpressionEvaluator;
        Map<String, String> valueMapping;

        // Compute the value map.
        valueExpressionEvaluator = new T8ClientExpressionEvaluator(context);
        valueMapping = definition.getValueMapping();
        if (valueMapping != null)
        {
            for (String valueExpression : valueMapping.keySet())
            {
                String displayString;
                Object value;

                // Get the display String matching the value expression.
                displayString = valueMapping.get(valueExpression);

                // Evaluate the value expression to determine the value.
                try
                {
                    value = valueExpressionEvaluator.evaluateExpression(valueExpression, map, null);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while compiling display value expression in renderer: " + definition, e);
                    value = null;
                }

                // Add the value and its display string to the value mapping.
                valueMap.put(value, displayString);
            }
        }

        // Set the tooltip.
        setToolTipText(definition.getTooltipText());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    @Override
    public void setRendererData(T8TableCellRenderableComponent renderableComponent, Map<String, Object> dataRow, String rendererFieldIdentifier, Map<RenderParameter, Object> renderParameters)
    {
        HashMap<String, Object> expressionParameters;
        Color foregroundColor;
        Color backgroundColor;
        Border cellBorder;
        Font cellFont;
        Icon icon;
        boolean isSelected;
        boolean hasFocus;
        boolean editable;
        Object displayValue;

        icon = null;
        displayValue = dataRow.get(rendererFieldIdentifier);
        if (valueMap.containsKey(displayValue)) displayValue = valueMap.get(displayValue);

        // Get the selected and focused parameters.
        isSelected = (renderParameters.get(RenderParameter.SELECTED) != null ? (Boolean)renderParameters.get(RenderParameter.SELECTED) : false);
        hasFocus = (renderParameters.get(RenderParameter.FOCUSED) != null ? (Boolean)renderParameters.get(RenderParameter.FOCUSED) : false);
        editable = (renderParameters.get(RenderParameter.EDITABLE) != null ? (Boolean)renderParameters.get(RenderParameter.EDITABLE) : false);

        // Get the default formatting values.
        cellFont = renderableComponent.getCellFont();
        if (isSelected)
        {
            foregroundColor = renderableComponent.getSelectedCellForegroundColor();
            backgroundColor = renderableComponent.getSelectedCellBackgroundColor();
            cellBorder = hasFocus ? renderableComponent.getFocusedSelectedBorder() : renderableComponent.getSelectedBorder();
        }
        else
        {
            foregroundColor = renderableComponent.getUnselectedCellForegroundColor();
            backgroundColor = renderableComponent.getUnselectedCellBackgroundColor();
            cellBorder = hasFocus ? renderableComponent.getFocusedBorder() : renderableComponent.getUnselectedBorder();
        }

        // Create a collection of parameters as input to the conditional formatting expressions.
        expressionParameters = new HashMap<>();
        if (dataRow != null) expressionParameters.putAll(dataRow);
        if (renderParameters != null)
        {
            for (RenderParameter parameter : renderParameters.keySet())
            {
                expressionParameters.put(parameter.toString(), renderParameters.get(parameter));
            }
        }

        // Evaluate each of the conditional formatting expressions and apply the first one for which the result is true.
        for (T8ConditionalCellFormattingDefinition formattingDefinition : definition.getConditionalFormattingDefinitions())
        {
            try
            {
                String conditionExpression;

                conditionExpression = formattingDefinition.getConditionExpression();
                if ((conditionExpression == null) || (formattingExpressionEvaluator.evaluateBooleanExpression(conditionExpression, expressionParameters, null)))
                {
                    Color bgColor;
                    Color bgsColor;
                    T8FontDefinition fontDefinition;

                    bgColor = formattingDefinition.getBackgroundColor();
                    bgsColor = formattingDefinition.getSelectedBackgroundColor();
                    fontDefinition = formattingDefinition.getFontDefinition();

                    if (isSelected)
                    {
                        if (bgsColor != null) backgroundColor = bgsColor;
                    }
                    else if (bgColor != null)
                    {
                        backgroundColor = bgColor;
                    }

                    // Set the cell font if the conditional formatting overrides it.
                    if (fontDefinition != null)
                    {
                        cellFont = fontDefinition.getNewFontInstance();
                    }

                    T8IconDefinition iconDefinition;

                    iconDefinition = formattingDefinition.getIconDefinition();
                    if (iconDefinition != null)
                    {
                        icon = iconDefinition.getImage().getImageIcon();
                    }
                    break;
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while evaluating condition expression for formatting definition: " + formattingDefinition, e);
            }
        }

        // Set the formatting.
        setFont(cellFont);
        setForeground(foregroundColor);
        setBackground(backgroundColor);
        setBorder(cellBorder);
        setIcon(icon);

        // Set the value of the cell on the label.
        setText(displayValue + "");
    }

    @Override
    public Object getAdditionalRenderData(Map<String, Object> dataRow, String fieldIdentifier)
    {
        return null;
    }
}
