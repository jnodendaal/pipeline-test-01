package com.pilog.t8.ui.datetimepicker;

import static com.pilog.t8.definition.ui.datetimepicker.T8DateTimePickerAPIHandler.*;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.datetimepicker.T8DateTimePickerDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.components.datetimepicker.JXDateTimePicker;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import org.jdesktop.swingx.calendar.DatePickerFormatter;
import org.jdesktop.swingx.plaf.basic.BasicDatePickerUI;

/**
 * @author Bouwer du Preez
 */
public class T8DateTimePicker extends JXDateTimePicker implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DateTimePicker.class);

    private static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    private final T8ComponentController controller;
    private final T8DateTimePickerDefinition definition;
    private final T8DateTimePickerOperationHandler operationHandler;

    public T8DateTimePicker(T8DateTimePickerDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.operationHandler = new T8DateTimePickerOperationHandler(this);
        this.setFormats(new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT));
        this.setTimeFormat(null);
        this.setUI(new DatePickerUI());
        this.addActionListener(new DateSelectionListener());
        this.addFocusListener(new DatePickerFocusListener());
        //This is required for the year spinner to be shown
        getMonthView().setZoomable(true);

    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        setToolTipText(definition.getTooltipText());
        setEditable(definition.isEditable());
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component tc, Object o)
    {
    }

    @Override
    public T8Component getChildComponent(String string)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String string)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    public Long getValue()
    {
        Date date;

        date = getDate();
        return date != null ? date.getTime() : null;
    }

    public void setValue(Long value)
    {
        if (value != null) setDate(new Date(value));
        else setDate(null);
    }

    private class DateSelectionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            controller.reportEvent(T8DateTimePicker.this, definition.getComponentEventDefinition(EVENT_VALUE_CHANGED), HashMaps.createSingular(PARAMETER_VALUE, getValue()));
        }
    }

    /**
     * A simple focus listener that will delegate the events to the controller
     * to ensure the events are reported to the component container.
     */
    private class DatePickerFocusListener implements FocusListener
    {
        private DatePickerFocusListener() {}

        @Override
        public void focusGained(FocusEvent e)
        {
            controller.reportEvent(T8DateTimePicker.this, definition.getComponentEventDefinition(EVENT_FOCUS_GAINED), HashMaps.createSingular(PARAMETER_VALUE, getValue()));
        }

        @Override
        public void focusLost(FocusEvent e)
        {
            controller.reportEvent(T8DateTimePicker.this, definition.getComponentEventDefinition(EVENT_FOCUS_LOST), HashMaps.createSingular(PARAMETER_VALUE, getValue()));
        }
    }

    /**
     * We override the two main components created by the basic UI in order to
     * change the appearance slightly.  We add a custom icon on the date picker
     * and also use a normal formatted text field since the one provided by the
     * default UI fiddles with the preferred size of the editor.
     */
    private static class DatePickerUI extends BasicDatePickerUI
    {
        @Override
        protected JFormattedTextField createEditor()
        {
            JFormattedTextField f = new JFormattedTextField(new DatePickerFormatter.DatePickerFormatterUIResource(datePicker.getLocale()));
            f.setName("dateField");
            return f;
        }

        @Override
        protected JButton createPopupButton()
        {
            JButton popupButton;
            popupButton = super.createPopupButton();
            popupButton.setIcon(new ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarecordeditor/icons/calendarSelectIcon.png")));
            popupButton.setMargin(new Insets(0, 0, 0, 0));
            return popupButton;
        }
    }
}
