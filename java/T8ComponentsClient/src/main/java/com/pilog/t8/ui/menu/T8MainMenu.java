package com.pilog.t8.ui.menu;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.system.T8ClientEnvironmentStatus;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.ui.menu.T8MainMenuDefinition;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import java.text.DecimalFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8MainMenu extends T8Menu
{
    private final T8ClientContext clientContext;
    private int latencyThreshold;
    private final double serverLoadThreshold;
    private double serverActivityThreshold;
    private ScheduledExecutorService executor;
    private StatusUpdateThread updateThread;
    private final DecimalFormat serverLoadFormat;
    private final DecimalFormat throughputFormat;

    private static final long UPDATE_INTERVAL = 30000;
    private static final Icon NORMAL_ICON = new javax.swing.ImageIcon(LAFConstants.class.getResource("/com/pilog/t8/ui/icons/globeIcon.png"));
    private static final Icon ALERT_ICON = new javax.swing.ImageIcon(LAFConstants.class.getResource("/com/pilog/t8/ui/icons/globeAlertIcon.png"));

    public T8MainMenu(T8MainMenuDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.clientContext = controller.getClientContext();
        this.latencyThreshold = 100;
        this.serverLoadThreshold = 80;
        this.serverActivityThreshold = 50;
        this.serverLoadFormat = new DecimalFormat("#0.00");
        this.throughputFormat = new DecimalFormat("#0.00");
    }

    public int getLatencyThreshold()
    {
        return latencyThreshold;
    }

    public void setLatencyThreshold(int latencyThreshold)
    {
        this.latencyThreshold = latencyThreshold;
    }

    public double getServerActivityThreshold()
    {
        return serverActivityThreshold;
    }

    public void setServerActivityThreshold(double serverActivityThreshold)
    {
        this.serverActivityThreshold = serverActivityThreshold;
    }

    @Override
    public void startComponent()
    {
        // Invoke the default behavior.
        super.startComponent();

        // Start the update thread.
        executor = Executors.newSingleThreadScheduledExecutor(new NameableThreadFactory("T8ClientEnvironmentStatusPanel"));
        updateThread = new StatusUpdateThread(this);
        executor.scheduleWithFixedDelay(updateThread, UPDATE_INTERVAL, UPDATE_INTERVAL, TimeUnit.MILLISECONDS);
    }

    @Override
    public void stopComponent()
    {
        // Invoke the default behavior.
        super.stopComponent();

        // Terminate the update thread.
        try
        {
            executor.shutdown();
            if (!executor.awaitTermination(10, TimeUnit.SECONDS))
            {
                throw new RuntimeException("Status Update thread executor could not be terminated.");
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while stopping T8ClientEnvironmentStatusPanel", e);
        }
    }

    public void refreshEnvironmentStatus()
    {
        setEnvironmentStatus(clientContext.getEnvironmentStatus());
    }

    private static class StatusUpdateThread extends Thread
    {
        private final T8MainMenu menu;

        private StatusUpdateThread(T8MainMenu menu)
        {
            this.menu = menu;
        }

        @Override
        public void run()
        {
            menu.refreshEnvironmentStatus();
        }
    }

    public void setEnvironmentStatus(T8ClientEnvironmentStatus status)
    {
        StringBuilder tooltip;
        String latencyRating;
        String throughputRating;
        String serverActivityRating;
        String serverLoadRating;
        boolean alert;
        int networkLatency;
        double networkThroughput;
        double serverActivity;
        double serverLoad;

        // Get the metrics to display.
        networkLatency = status.getNetworkLatency();
        networkThroughput = status.getNetworkThroughput() / 1000.00; // Convert bit/s to kbit/s.
        serverActivity = status.getServerActivity();
        serverLoad = status.getServerLoad();

        // Determine the latency rating.
        alert = false;
        if (status.getNetworkLatency() > latencyThreshold)
        {
            latencyRating = "High";
            alert = true;
        }
        else
        {
            latencyRating = "Normal";
        }

        // Determine the throughput rating.
        if (networkThroughput < 1.0)
        {
            throughputRating = "Not Measurable";
        }
        else if (networkThroughput < 10.0)
        {
            throughputRating = "Low";
            alert = true;
        }
        else
        {
            throughputRating = "Normal";
        }

        // Determine the server activity rating.
        if (status.getServerActivity() > serverActivityThreshold)
        {
            serverActivityRating = "High";
            alert = true;
        }
        else
        {
            serverActivityRating = "Normal";
        }

        // Determine the server load rating.
        if (serverLoad > serverLoadThreshold)
        {
            serverLoadRating = "High";
            alert = true;
        }
        else
        {
            serverLoadRating = "Normal";
        }

        // Set the status icon.
        setIcon(alert ? ALERT_ICON : NORMAL_ICON);

        // Create the new tooltip.
        tooltip = new StringBuilder();
        tooltip.append("<html>");
        tooltip.append("Latency: ");
        tooltip.append(networkLatency);
        tooltip.append(" ms (");
        tooltip.append(latencyRating);
        tooltip.append(")<br>");
        tooltip.append("Throughput: ");
        tooltip.append(throughputFormat.format(networkThroughput));
        tooltip.append(" kbit/s (");
        tooltip.append(throughputRating);
        tooltip.append(")<br>");
        tooltip.append("Server Activity: ");
        tooltip.append(serverActivity);
        tooltip.append(" (");
        tooltip.append(serverActivityRating);
        tooltip.append(")<br>");
        tooltip.append("Server Load: ");
        tooltip.append(serverLoadFormat.format(serverLoad));
        tooltip.append("% (");
        tooltip.append(serverLoadRating);
        tooltip.append(")</html>");

        // Set the updated tooltip on the label.
        setToolTipText(tooltip.toString());
    }
}
