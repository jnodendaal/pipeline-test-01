package com.pilog.t8.ui.menubar;

import com.pilog.t8.ui.T8ComponentOperationHandler;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8MenuBarOperationHandler extends T8ComponentOperationHandler
{
    private T8MenuBar menuBar;

    public T8MenuBarOperationHandler(T8MenuBar menuBar)
    {
        super(menuBar);
        this.menuBar = menuBar;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return super.executeOperation(operationIdentifier, operationParameters);
    }
}
