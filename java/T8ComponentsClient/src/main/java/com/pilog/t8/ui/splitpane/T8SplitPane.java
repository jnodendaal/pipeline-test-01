package com.pilog.t8.ui.splitpane;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8PainterAdapter;
import com.pilog.t8.definition.gfx.painter.T8PainterDefinition;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.splitpane.T8SplitPaneDefinition;
import com.pilog.t8.definition.ui.splitpane.T8SplitPaneDefinition.Orientation;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.jdesktop.swingx.JXMultiSplitPane;
import org.jdesktop.swingx.MultiSplitLayout;
import org.jdesktop.swingx.MultiSplitLayout.Divider;
import org.jdesktop.swingx.MultiSplitLayout.Leaf;
import org.jdesktop.swingx.MultiSplitLayout.Split;

/**
 * @author Bouwer du Preez
 */
public class T8SplitPane extends JXMultiSplitPane implements T8Component
{
    private T8SplitPaneDefinition definition;
    private T8ComponentController controller;
    private T8Component childComponent1;
    private T8Component childComponent2;
    
    private static final String COMPONENT_1_ID = "LEAF1";
    private static final String COMPONENT_2_ID = "LEAF2";

    public T8SplitPane(T8SplitPaneDefinition definition, T8ComponentController controller)
    {
        this.setOpaque(false);
        this.definition = definition;
        this.controller = controller;
        setupSplitPane();
    }
    
    private void setupSplitPane()
    {
        MultiSplitLayout layout;
        Leaf leaf1;
        Leaf leaf2;
        Split split;
        double weight;
        
        weight = definition.getDividerLocation();
        if (weight > 1) weight = 0.5;
        
        leaf1 = new Leaf(COMPONENT_1_ID);
        leaf1.setWeight(weight);
        leaf2 = new Leaf(COMPONENT_2_ID);
        leaf2.setWeight(1 - weight);

        split = new Split();
        split.setChildren(leaf1, new Divider(), leaf2);
        split.setRowLayout(definition.getOrientation() == Orientation.VERTICAL_DIVIDER);
        
        layout = new MultiSplitLayout(split);
        layout.setLayoutMode(MultiSplitLayout.NO_MIN_SIZE_LAYOUT);
        layout.setLayoutByWeight(true);
        setLayout(layout);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public HashMap<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> hm)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        T8PainterDefinition backgroundPainterDefinition;
        
        backgroundPainterDefinition = null; // TODO:  Add optional background painter datum to definition.
        if (backgroundPainterDefinition != null)
        {
            this.setBackgroundPainter(new T8PainterAdapter(backgroundPainterDefinition.getNewPainterInstance()));
        }
    }

    @Override
    public void startComponent()
    {
    }
    
    @Override
    public void stopComponent()
    {
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object constraints)
    {
        if (childComponent1 == null)
        {
            childComponent1 = childComponent;
            add((Component)childComponent1, COMPONENT_1_ID); 
        }
        else if (childComponent2 == null)
        {
            childComponent2 = childComponent;
            add((Component)childComponent2, COMPONENT_2_ID); 
        }
        else throw new RuntimeException("Attempt to add more than 2 child components to a T8SplitPane.");
    }

    @Override
    public T8Component getChildComponent(String componentIdentifier)
    {
        if ((childComponent1 != null) && (childComponent1.getComponentDefinition().getIdentifier().equalsIgnoreCase(componentIdentifier))) return childComponent1;
        else if((childComponent2 != null) && (childComponent2.getComponentDefinition().getIdentifier().equalsIgnoreCase(componentIdentifier))) return childComponent2;
        else return null;
    }

    @Override
    public T8Component removeChildComponent(String componentIdentifier)
    {
        if (childComponent1 != null)
        {
            if (childComponent1.getComponentDefinition().getIdentifier().equalsIgnoreCase(componentIdentifier))
            {
                T8Component removedComponent;

                removedComponent = childComponent1;
                remove((Component)removedComponent);
                childComponent1 = null;
                return removedComponent;
            }
        }

        if (childComponent2 != null)
        {
            if (childComponent2.getComponentDefinition().getIdentifier().equalsIgnoreCase(componentIdentifier))
            {
                T8Component removedComponent;

                removedComponent = childComponent2;
                remove((Component)removedComponent);
                childComponent2 = null;
                return removedComponent;
            }
        }

        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        ArrayList<T8Component> childComponents;

        childComponents = new ArrayList<>();
        if (childComponent1 != null) childComponents.add(childComponent1);
        if (childComponent2 != null) childComponents.add(childComponent2);
        return childComponents;
    }

    @Override
    public int getChildComponentCount()
    {
        if ((childComponent1 != null) && (childComponent2 != null)) return 2;
        else if ((childComponent1 != null) || (childComponent2 != null)) return 1;
        else return 0;
    }

    @Override
    public void clearChildComponents()
    {
        removeAll();
        childComponent1 = null;
        childComponent2 = null;
    }
}
