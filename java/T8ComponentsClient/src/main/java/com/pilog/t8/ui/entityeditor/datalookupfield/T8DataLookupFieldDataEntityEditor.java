package com.pilog.t8.ui.entityeditor.datalookupfield;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8DataEntityEditorComponent;
import com.pilog.t8.definition.ui.entityeditor.datalookupfield.T8DataLookupFieldDataEntityEditorDefinition;
import com.pilog.t8.ui.datalookupfield.T8DataLookupField;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataLookupFieldDataEntityEditor extends T8DataLookupField implements T8DataEntityEditorComponent
{
    private T8DataLookupFieldDataEntityEditorDefinition definition;
    private T8DataLookupFieldDataEntityEditorOperationHandler operationHandler;
    private Map<String, String> editorToComboMapping;
    private Map<String, String> comboToEditorMapping;
    private T8DataEntity editorEntity;
    
    public T8DataLookupFieldDataEntityEditor(T8DataLookupFieldDataEntityEditorDefinition definition, T8ComponentController controller)
    {
        super(definition, controller);
        this.definition = definition;
        this.operationHandler = new T8DataLookupFieldDataEntityEditorOperationHandler(this);
        this.editorToComboMapping = definition.getEditorFieldMapping();
        this.comboToEditorMapping = T8IdentifierUtilities.createReverseIdentifierMap(editorToComboMapping);
    }
    
    @Override
    public Map<String, Object> executeOperation(String operationIdentifier, Map<String, Object> operationParameters)
    {
        return operationHandler.executeOperation(operationIdentifier, operationParameters);
    }
    
    @Override
    public void setEditorDataEntity(T8DataEntity dataEntity)
    {
        editorEntity = dataEntity;
        if (editorEntity != null)
        {
            Map<String, Object> fieldValues;
            
            fieldValues = T8IdentifierUtilities.mapParameters(editorEntity.getFieldValues(), editorToComboMapping);
            setSelectedDataEntityKey(fieldValues);
        }
        else setSelectedDataEntity(null);
    }
    
    @Override
    public T8DataEntity getEditorDataEntity()
    {
        return editorEntity;
    }

    @Override
    public boolean commitEditorChanges()
    {
        if (editorEntity != null)
        {
            Map<String, String> fieldMappings;

            fieldMappings = definition.getEditorFieldMapping();
            if (fieldMappings != null)
            {
                T8DataEntity selectedEntity;
                
                selectedEntity = getSelectedDataEntity();
                if (selectedEntity != null)
                {
                    Map<String, Object> fieldValues;
                    
                    fieldValues = T8IdentifierUtilities.mapParameters(selectedEntity.getFieldValues(), comboToEditorMapping);
                    editorEntity.setFieldValues(fieldValues);
                    return true;
                }
                else return true;
            }
            else throw new RuntimeException("No field mappings set in entity editor: " + definition);
        }
        else return true;
    }
    
    @Override
    public String getEditorDataEntityIdentifier()
    {
        return definition.getDataEntityIdentifier();
    }

    @Override
    public String getParentEditorIdentifier()
    {
        return definition.getParentEditorIdentifier();
    }
}
