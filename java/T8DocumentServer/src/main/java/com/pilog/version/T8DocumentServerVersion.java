package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8DocumentServerVersion
{
    public final static String VERSION = "1355";
}
