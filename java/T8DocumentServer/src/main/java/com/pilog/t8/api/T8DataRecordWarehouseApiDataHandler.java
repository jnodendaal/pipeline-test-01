package com.pilog.t8.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordWarehouseApiDataHandler
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;

    public T8DataRecordWarehouseApiDataHandler(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
    }
}