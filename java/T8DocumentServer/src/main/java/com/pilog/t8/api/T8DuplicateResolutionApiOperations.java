package com.pilog.t8.api;

import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

import static com.pilog.t8.definition.data.org.T8OrganizationDuplicateResolutionAPIResources.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DuplicateResolutionApiOperations
{
    public static class ApiOperationSubmitResolution extends T8DefaultServerOperation
    {
        public ApiOperationSubmitResolution(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DuplicateResolutionApi pdrApi;
            String resolutionID;
            String familyID;
            String matchInstanceID;
            String dataObjectIdentifier;
            String resolutionStateIdentifier;

            // Get the operation parameters we need.
            matchInstanceID = (String)operationParameters.get(PARAMETER_MATCH_INSTANCE_ID);
            familyID = (String)operationParameters.get(PARAMETER_FAMILY_ID);
            dataObjectIdentifier = (String)operationParameters.get(PARAMETER_DATA_OBJECT_IDENTIFIER);
            resolutionStateIdentifier = (String)operationParameters.get(PARAMETER_RESOLUTION_STATE_IDENTIFIER);

            // Perform the API operation.
            pdrApi = tx.getApi(T8DuplicateResolutionApi.API_IDENTIFIER);
            resolutionID = pdrApi.submitResolution(matchInstanceID, familyID, dataObjectIdentifier, resolutionStateIdentifier);
            return HashMaps.createSingular(PARAMETER_RESOLUTION_ID, resolutionID);
        }
    }

    public static class ApiOperationCancelResolution extends T8DefaultServerOperation
    {
        public ApiOperationCancelResolution(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DuplicateResolutionApi pdrApi;
            String resolutionId;

            // Get the operation parameters we need.
            resolutionId = (String)operationParameters.get(PARAMETER_RESOLUTION_ID);

            // Perform the API operation.
            pdrApi = tx.getApi(T8DuplicateResolutionApi.API_IDENTIFIER);
            pdrApi.cancelResolution(resolutionId);
            return null;
        }
    }

    public static class ApiOperationFinalizeResolution extends T8DefaultServerOperation
    {
        public ApiOperationFinalizeResolution(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DuplicateResolutionApi pdrApi;
            String resolutionId;
            String dataObjectId;
            String masterStateId;
            String nonDuplicateStateId;
            String supersededStateId;
            String mergerId;

            // Get the operation parameters we need.
            mergerId = (String)operationParameters.get(PARAMETER_DATA_RECORD_MERGER_IDENTIFIER);
            resolutionId = (String)operationParameters.get(PARAMETER_RESOLUTION_ID);
            dataObjectId = (String)operationParameters.get(PARAMETER_DATA_OBJECT_IDENTIFIER);
            masterStateId = (String)operationParameters.get(PARAMETER_MASTER_STATE_IDENTIFIER);
            nonDuplicateStateId = (String)operationParameters.get(PARAMETER_NON_DUPLICATE_STATE_IDENTIFIER);
            supersededStateId = (String)operationParameters.get(PARAMETER_SUPERSEDED_STATE_IDENTIFIER);

            // Perform the API operation.
            pdrApi = tx.getApi(T8DuplicateResolutionApi.API_IDENTIFIER);
            pdrApi.finalizeResolution(mergerId, resolutionId, dataObjectId, masterStateId, nonDuplicateStateId, supersededStateId);
            return null;
        }
    }

    public static class ApiOperationSetMaster extends T8DefaultServerOperation
    {
        public ApiOperationSetMaster(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DuplicateResolutionApi pdrApi;
            String familyId;
            String recordId;

            // Get the operation parameters we need.
            familyId = (String)operationParameters.get(PARAMETER_FAMILY_ID);
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);

            // Perform the API operation.
            pdrApi = tx.getApi(T8DuplicateResolutionApi.API_IDENTIFIER);
            pdrApi.setMaster(familyId, recordId);
            return null;
        }
    }

    public static class ApiOperationSetExcluded extends T8DefaultServerOperation
    {
        public ApiOperationSetExcluded(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DuplicateResolutionApi pdrApi;
            String familyId;
            String recordId;

            // Get the operation parameters we need.
            familyId = (String)operationParameters.get(PARAMETER_FAMILY_ID);
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);

            // Perform the API operation.
            pdrApi = tx.getApi(T8DuplicateResolutionApi.API_IDENTIFIER);
            pdrApi.setExcluded(familyId, recordId);
            return null;
        }
    }

    public static class ApiOperationSetSuperseded extends T8DefaultServerOperation
    {
        public ApiOperationSetSuperseded(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DuplicateResolutionApi resAPI;
            String familyId;
            String recordId;

            // Get the operation parameters we need.
            familyId = (String)operationParameters.get(PARAMETER_FAMILY_ID);
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);

            // Perform the API operation.
            resAPI = tx.getApi(T8DuplicateResolutionApi.API_IDENTIFIER);
            resAPI.setSuperseded(familyId, recordId);
            return null;
        }
    }

    public static class ApiOperationSetPending extends T8DefaultServerOperation
    {
        public ApiOperationSetPending(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DuplicateResolutionApi resAPI;
            String familyId;
            String recordId;

            // Get the operation parameters we need.
            familyId = (String)operationParameters.get(PARAMETER_FAMILY_ID);
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);

            // Perform the API operation.
            resAPI = tx.getApi(T8DuplicateResolutionApi.API_IDENTIFIER);
            resAPI.setPending(familyId, recordId);
            return null;
        }
    }
}
