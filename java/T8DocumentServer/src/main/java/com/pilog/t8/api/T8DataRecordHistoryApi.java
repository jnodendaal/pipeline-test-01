package com.pilog.t8.api;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.T8DocumentSerializer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.history.HistoryEntry;
import com.pilog.t8.data.source.datarecord.T8DataRecordPersistenceHandler.AuditingType;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.datarecord.RecordAttribute;
import com.pilog.t8.data.document.datarecord.RecordContent;
import com.pilog.t8.data.document.history.RecordHistoryData;
import com.pilog.t8.data.source.datarecord.T8RecordDataHandler;

import static com.pilog.t8.definition.api.T8DataRecordHistoryApiResource.*;
import static com.pilog.t8.definition.data.document.history.T8DocumentHistoryAPIHandler.*;

public class T8DataRecordHistoryApi implements T8Api, T8PerformanceStatisticsProvider
{
    public static final String API_IDENTIFIER = "@API_DATA_RECORD_HISTORY"; // Renamed from @API_SERVER_DATA_RECORD_HISTORY.

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8DataRecordHistoryApiDataHandler dataHandler;
    private T8PerformanceStatistics stats;

    public T8DataRecordHistoryApi(T8DataTransaction tx)
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.serverContext = context.getServerContext();
        this.dataHandler = new T8DataRecordHistoryApiDataHandler(tx);
        this.stats = tx.getPerformanceStatistics(); // Default behviour of all API's is to use the performance statistics object of its parent transaction.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    /**
     * Retrieves history snapshots of the specified data file.  Each entry in the resulting map contains the transaction IID as the key,
     * and the {@code RecordContent} as it existed at the point in time when the transaction was committed as value.
     * The transactions are ordered chronologically from oldest to newest.  This means that the first entry in the resulting map will always be the
     * insertion transaction of the initial record creation.
     *
     * @param rootRecordId The {@code String} root record ID for which the transaction history is to be retrieved.
     * @param transactionIdList Optional. The {@code List} of transaction IID's which will be used for filtering.
     * @return The {@code Map} containing the transaction history, ordered from oldest to latest.
     * @throws Exception If there is any unforseen error during the retrieval of the transaction history snapshot.
     */
    public Map<String, RecordContent> retrieveFileHistorySnapshots(String rootRecordId, List<String> transactionIdList) throws Exception
    {
        Map<String, RecordContent> snapshots;
        List<T8DataEntity> docEntities;
        List<T8DataEntity> propertyEntities;
        List<T8DataEntity> valueEntities;
        List<T8DataEntity> attributeEntities;
        List<T8DataEntity> attachmentEntities;
        RecordHistoryData historyData;
        T8DataFilter dataFilter;
        String currentTxIid;

        // We don't want to check for null and empty, so we make sure we have an empty list
        if (transactionIdList == null) transactionIdList = new ArrayList<>();

        // Make sure we have a record id - it's absolutely required.
        if (rootRecordId == null) throw new IllegalArgumentException("No root record id supplied.");

        // Now create a map to hold a version of the requested data file for each requested transaction in its history.
        snapshots = new LinkedHashMap<>();

        // Fetch the doc history data.
        dataFilter = new T8DataFilter(HISTORY_DATA_RECORD_DOC_DEID);
        dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, HISTORY_DATA_RECORD_DOC_DEID + EF_ROOT_RECORD_ID, DataFilterOperator.EQUAL, rootRecordId, false);
        dataFilter.addFieldOrdering(HISTORY_DATA_RECORD_DOC_DEID + EF_TIME, T8DataFilter.OrderMethod.ASCENDING);
        docEntities = tx.select(HISTORY_DATA_RECORD_DOC_DEID, dataFilter);

        // Fetch the property history data.
        dataFilter = new T8DataFilter(HISTORY_DATA_RECORD_PROPERTY_DEID);
        dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, HISTORY_DATA_RECORD_PROPERTY_DEID + EF_ROOT_RECORD_ID, DataFilterOperator.EQUAL, rootRecordId, false);
        dataFilter.addFieldOrdering(HISTORY_DATA_RECORD_PROPERTY_DEID + EF_TIME, T8DataFilter.OrderMethod.ASCENDING);
        propertyEntities = tx.select(HISTORY_DATA_RECORD_PROPERTY_DEID, dataFilter);

        // Fetch the value history data.
        dataFilter = new T8DataFilter(HISTORY_DATA_RECORD_VALUE_DEID);
        dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, HISTORY_DATA_RECORD_VALUE_DEID + EF_ROOT_RECORD_ID, DataFilterOperator.EQUAL, rootRecordId, false);
        dataFilter.addFieldOrdering(HISTORY_DATA_RECORD_VALUE_DEID + EF_TIME, T8DataFilter.OrderMethod.ASCENDING);
        valueEntities = tx.select(HISTORY_DATA_RECORD_VALUE_DEID, dataFilter);

        // Fetch the attribute history data.
        dataFilter = new T8DataFilter(HISTORY_DATA_RECORD_ATTRIBUTE_DEID);
        dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, HISTORY_DATA_RECORD_ATTRIBUTE_DEID + EF_ROOT_RECORD_ID, DataFilterOperator.EQUAL, rootRecordId, false);
        dataFilter.addFieldOrdering(HISTORY_DATA_RECORD_ATTRIBUTE_DEID + EF_TIME, T8DataFilter.OrderMethod.ASCENDING);
        attributeEntities = tx.select(HISTORY_DATA_RECORD_ATTRIBUTE_DEID, dataFilter);

        // Fetch the attachment history data.
        dataFilter = new T8DataFilter(HISTORY_DATA_RECORD_ATTACHMENT_DEID);
        dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, HISTORY_DATA_RECORD_ATTACHMENT_DEID + EF_ROOT_RECORD_ID, DataFilterOperator.EQUAL, rootRecordId, false);
        dataFilter.addFieldOrdering(HISTORY_DATA_RECORD_ATTACHMENT_DEID + EF_TIME, T8DataFilter.OrderMethod.ASCENDING);
        attachmentEntities = tx.select(HISTORY_DATA_RECORD_ATTACHMENT_DEID, dataFilter);

        // Iterate over the data, add each history entry in sequence to the set so that progressive updates are applied in the correct order.
        currentTxIid = null;
        historyData = new RecordHistoryData();
        while (!docEntities.isEmpty())
        {
            Map<String, Object> filterValues;
            List<T8DataEntity> entities;
            T8DataEntity docEntity;
            T8Timestamp timestamp;
            String txIid;

            // Get the transaction iid.
            docEntity = docEntities.get(0);
            txIid = (String)docEntity.getFieldValue(HISTORY_DATA_RECORD_DOC_DEID + EF_TX_IID);
            timestamp = (T8Timestamp)docEntity.getFieldValue(HISTORY_DATA_RECORD_DOC_DEID + EF_TIME);
            if (currentTxIid == null) currentTxIid = txIid;
            else if (!Objects.equals(currentTxIid, txIid))
            {
                // If this transaction was requested, add it to the snapshot map.
                if (transactionIdList.isEmpty() || transactionIdList.contains(currentTxIid))
                {
                    // Add the current version of the data file to the map using the transaction id as key.
                    snapshots.put(currentTxIid, historyData.constructFile());
                }

                // Set the new transaction.
                currentTxIid = txIid;
            }

            // Add the doc data to the collection.
            filterValues = HashMaps.newHashMap(HISTORY_DATA_RECORD_DOC_DEID + EF_TX_IID, txIid, HISTORY_DATA_RECORD_DOC_DEID + EF_TIME, timestamp);
            entities = T8DataUtilities.getFilteredDataEntities(docEntities, filterValues);
            historyData.addDocData(entities);
            docEntities.removeAll(entities);

            // Add the property data to the collection.
            filterValues = HashMaps.newHashMap(HISTORY_DATA_RECORD_PROPERTY_DEID + EF_TX_IID, txIid, HISTORY_DATA_RECORD_PROPERTY_DEID + EF_TIME, timestamp);
            entities = T8DataUtilities.getFilteredDataEntities(propertyEntities, filterValues);
            historyData.addPropertyData(entities);
            propertyEntities.removeAll(entities);

            // Add the value data to the collection.
            filterValues = HashMaps.newHashMap(HISTORY_DATA_RECORD_VALUE_DEID + EF_TX_IID, txIid, HISTORY_DATA_RECORD_VALUE_DEID + EF_TIME, timestamp);
            entities = T8DataUtilities.getFilteredDataEntities(valueEntities, filterValues);
            historyData.addValueData(entities);
            valueEntities.removeAll(entities);

            // Add the attribute data to the collection.
            filterValues = HashMaps.newHashMap(HISTORY_DATA_RECORD_ATTRIBUTE_DEID + EF_TX_IID, txIid, HISTORY_DATA_RECORD_ATTRIBUTE_DEID + EF_TIME, timestamp);
            entities = T8DataUtilities.getFilteredDataEntities(attributeEntities, filterValues);
            historyData.addAttributeData(entities);
            attributeEntities.removeAll(entities);

            // Add the attachment data to the collection.
            filterValues = HashMaps.newHashMap(HISTORY_DATA_RECORD_ATTACHMENT_DEID + EF_TX_IID, txIid, HISTORY_DATA_RECORD_ATTACHMENT_DEID + EF_TIME, timestamp);
            entities = T8DataUtilities.getFilteredDataEntities(attachmentEntities, filterValues);
            historyData.addAttachmentData(entities);
            attachmentEntities.removeAll(entities);
        }

        // Add the last processed transaction, if required, to the snapshot map.
        if (transactionIdList.isEmpty() || transactionIdList.contains(currentTxIid))
        {
            snapshots.put(currentTxIid, historyData.constructFile());
        }

        // Return the final map of sequencial history snapshots.
        return snapshots;
    }

    public void convertJson(String rootRecordId) throws Exception
    {
        List<T8DataEntity> historyEntities;
        List<HistoryEntry> historyEntries;
        T8DataFilter dataFilter;

        // We create the filter for the specific root record history
        dataFilter = new T8DataFilter(DE_HISTORY_DATA_RECORD);
        dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, DE_HISTORY_DATA_RECORD + EF_ROOT_RECORD_ID, DataFilterOperator.EQUAL, rootRecordId, false);

        // We want to rebuild each historic change to the record, so we order chronologically.
        dataFilter.addFieldOrdering(DE_HISTORY_DATA_RECORD + EF_TIME, T8DataFilter.OrderMethod.ASCENDING);
        historyEntities = tx.select(DE_HISTORY_DATA_RECORD, dataFilter);

        // Convert all entities to entries.
        historyEntries = new ArrayList<>();
        for (T8DataEntity historyEntity : historyEntities)
        {
            HistoryEntry historyEntry;

            historyEntry = new HistoryEntry();
            historyEntry.setDataRecord(T8DocumentSerializer.deserializeDataRecord((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD+EF_RECORD_DATA)));
            historyEntry.setAuditingType(AuditingType.valueOf((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_EVENT)));
            historyEntry.setRootRecordId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_ROOT_RECORD_ID));
            historyEntry.setParentRecordId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_PARENT_RECORD_ID));
            historyEntry.setTransactionIid((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_TRANSACTION_IID));
            historyEntry.setTimestamp((T8Timestamp)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_TIME));
            historyEntry.setSessionId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_SESSION_ID));
            historyEntry.setUserId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_USER_ID));
            historyEntry.setAgentId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_AGENT_ID));
            historyEntry.setAgentIid((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_AGENT_IID));
            historyEntry.setFlowId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_FLOW_ID));
            historyEntry.setFlowIid((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_FLOW_IID));
            historyEntry.setTaskId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_TASK_ID));
            historyEntry.setTaskIid((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_TASK_IID));
            historyEntry.setOperationId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_OPERATION_ID));
            historyEntry.setOperationIid((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_OPERATION_IID));
            historyEntry.setProcessId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_PROCESS_ID));
            historyEntry.setProcessIid((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_PROCESS_IID));
            historyEntry.setFunctionalityId((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_FUNCTIONALITY_ID));
            historyEntry.setFunctionalityIid((String)historyEntity.getFieldValue(DE_HISTORY_DATA_RECORD + EF_FUNCTIONALITY_IID));
            historyEntries.add(historyEntry);
        }

        // Process each entry.
        for (HistoryEntry entry : historyEntries)
        {
            if (entry.isValidEntry())
            {
                switch (entry.getAuditingType())
                {
                    case INSERT:
                        convertJsonInsert(entry);
                        break;
                    case UPDATE:
                        convertJsonUpdate(historyEntries, entry);
                        break;
                    case DELETE:
                        convertJsonDelete(entry);
                        break;
                }
            }
        }
    }

    private void convertJsonInsert(HistoryEntry entry) throws Exception
    {
        List<Map<String, Object>> docData;
        List<Map<String, Object>> propertyData;
        List<Map<String, Object>> valueData;
        List<Map<String, Object>> attributeData;
        List<Map<String, Object>> attachmentData;
        DataRecord dataRecord;

        dataRecord = entry.getDataRecord();
        docData = T8RecordDataHandler.getDataRecordDataRows(dataRecord);
        attributeData = T8RecordDataHandler.getDataRecordAttributeDataRows(dataRecord);
        propertyData = T8RecordDataHandler.getRecordPropertyDataRows(dataRecord);
        valueData = T8RecordDataHandler.getRecordValueDataRows(dataRecord);
        attachmentData = T8RecordDataHandler.getRecordAttachmentDataRows(dataRecord);

        addEntryData(docData, entry);
        addEntryData(attributeData, entry);
        addEntryData(propertyData, entry);
        addEntryData(valueData, entry);
        addEntryData(attachmentData, entry);

        dataHandler.insertHistoryDataRecordEntry(entry, docData, null);
        dataHandler.insertHistoryDataRecordPropertyEntry(entry.getTransactionIid(), AuditingType.INSERT, entry.getTimestamp(), propertyData, null);
        dataHandler.insertHistoryDataRecordValueEntry(entry.getTransactionIid(), AuditingType.INSERT, entry.getTimestamp(), valueData, null);
        dataHandler.insertHistoryDataRecordAttributeEntry(entry.getTransactionIid(), AuditingType.INSERT, entry.getTimestamp(), attributeData, null);
        dataHandler.insertHistoryDataRecordAttachmentEntry(entry.getTransactionIid(), AuditingType.INSERT, entry.getTimestamp(), attachmentData);
    }

    private void convertJsonDelete(HistoryEntry entry) throws Exception
    {
        List<Map<String, Object>> docData;
        List<Map<String, Object>> propertyData;
        List<Map<String, Object>> valueData;
        List<Map<String, Object>> attributeData;
        List<Map<String, Object>> attachmentData;
        DataRecord dataRecord;

        dataRecord = entry.getDataRecord();
        docData = T8RecordDataHandler.getDataRecordDataRows(dataRecord);
        attributeData = T8RecordDataHandler.getDataRecordAttributeDataRows(dataRecord);
        propertyData = T8RecordDataHandler.getRecordPropertyDataRows(dataRecord);
        valueData = T8RecordDataHandler.getRecordValueDataRows(dataRecord);
        attachmentData = T8RecordDataHandler.getRecordAttachmentDataRows(dataRecord);

        addEntryData(docData, entry);
        addEntryData(attributeData, entry);
        addEntryData(propertyData, entry);
        addEntryData(valueData, entry);
        addEntryData(attachmentData, entry);

        dataHandler.insertHistoryDataRecordEntry(entry, docData, null);
        dataHandler.insertHistoryDataRecordPropertyEntry(entry.getTransactionIid(), AuditingType.DELETE, entry.getTimestamp(), propertyData, null);
        dataHandler.insertHistoryDataRecordValueEntry(entry.getTransactionIid(), AuditingType.DELETE, entry.getTimestamp(), valueData, null);
        dataHandler.insertHistoryDataRecordAttributeEntry(entry.getTransactionIid(), AuditingType.DELETE, entry.getTimestamp(), attributeData, null);
        dataHandler.insertHistoryDataRecordAttachmentEntry(entry.getTransactionIid(), AuditingType.DELETE, entry.getTimestamp(), attachmentData);
    }

    private void convertJsonUpdate(List<HistoryEntry> entries, HistoryEntry toEntry) throws Exception
    {
        HistoryEntry fromEntry;

        fromEntry = getFromEntry(entries, toEntry);
        if (fromEntry != null)
        {
            List<Map<String, Object>> toDocData;
            List<Map<String, Object>> toPropertyData;
            List<Map<String, Object>> toValueData;
            List<Map<String, Object>> toAttributeData;
            List<Map<String, Object>> toAttachmentData;
            List<Map<String, Object>> fromDocData;
            List<Map<String, Object>> fromPropertyData;
            List<Map<String, Object>> fromValueData;
            List<Map<String, Object>> fromAttributeData;
            List<Map<String, Object>> fromAttachmentData;
            DataRecord fromRecord;
            DataRecord toRecord;
            T8Timestamp timestamp;
            String txIid;

            txIid = toEntry.getTransactionIid();
            timestamp = toEntry.getTimestamp();

            toRecord = toEntry.getDataRecord();
            toDocData = T8RecordDataHandler.getDataRecordDataRows(toRecord);
            toAttributeData = T8RecordDataHandler.getDataRecordAttributeDataRows(toRecord);
            toPropertyData = T8RecordDataHandler.getRecordPropertyDataRows(toRecord);
            toValueData = T8RecordDataHandler.getRecordValueDataRows(toRecord);
            toAttachmentData = T8RecordDataHandler.getRecordAttachmentDataRows(toRecord);

            addEntryData(toDocData, toEntry);
            addEntryData(toAttributeData, toEntry);
            addEntryData(toPropertyData, toEntry);
            addEntryData(toValueData, toEntry);
            addEntryData(toAttachmentData, toEntry);

            fromRecord = fromEntry.getDataRecord();
            fromDocData = T8RecordDataHandler.getDataRecordDataRows(fromRecord);
            fromAttributeData = T8RecordDataHandler.getDataRecordAttributeDataRows(fromRecord);
            fromPropertyData = T8RecordDataHandler.getRecordPropertyDataRows(fromRecord);
            fromValueData = T8RecordDataHandler.getRecordValueDataRows(fromRecord);
            fromAttachmentData = T8RecordDataHandler.getRecordAttachmentDataRows(fromRecord);

            addEntryData(fromDocData, fromEntry);
            addEntryData(fromAttributeData, fromEntry);
            addEntryData(fromPropertyData, fromEntry);
            addEntryData(fromValueData, fromEntry);
            addEntryData(fromAttachmentData, fromEntry);

            dataHandler.insertHistoryDataRecordEntry(toEntry, toDocData, fromDocData);
            dataHandler.insertHistoryDataRecordPropertyEntry(txIid, AuditingType.INSERT, timestamp, T8RecordDataHandler.getInsertedPropertyData(fromPropertyData, toPropertyData), fromPropertyData);
            dataHandler.insertHistoryDataRecordPropertyEntry(txIid, AuditingType.UPDATE, timestamp, T8RecordDataHandler.getUpdatedPropertyData(fromPropertyData, toPropertyData), fromPropertyData);
            dataHandler.insertHistoryDataRecordPropertyEntry(txIid, AuditingType.DELETE, timestamp, T8RecordDataHandler.getDeletedPropertyData(fromPropertyData, toPropertyData), fromPropertyData);
            dataHandler.insertHistoryDataRecordValueEntry(txIid, AuditingType.INSERT, timestamp, T8RecordDataHandler.getInsertedValueData(fromValueData, toValueData), fromValueData);
            dataHandler.insertHistoryDataRecordValueEntry(txIid, AuditingType.UPDATE, timestamp, T8RecordDataHandler.getUpdatedValueData(fromValueData, toValueData), fromValueData);
            dataHandler.insertHistoryDataRecordValueEntry(txIid, AuditingType.DELETE, timestamp, T8RecordDataHandler.getDeletedValueData(fromValueData, toValueData), fromValueData);
            dataHandler.insertHistoryDataRecordAttributeEntry(txIid, AuditingType.INSERT, timestamp, T8RecordDataHandler.getInsertedAttributeData(fromAttributeData, toAttributeData), fromAttributeData);
            dataHandler.insertHistoryDataRecordAttributeEntry(txIid, AuditingType.UPDATE, timestamp, T8RecordDataHandler.getUpdatedAttributeData(fromAttributeData, toAttributeData), fromAttributeData);
            dataHandler.insertHistoryDataRecordAttributeEntry(txIid, AuditingType.DELETE, timestamp, T8RecordDataHandler.getDeletedAttributeData(fromAttributeData, toAttributeData), fromAttributeData);
            dataHandler.insertHistoryDataRecordAttachmentEntry(txIid, AuditingType.INSERT, timestamp, T8RecordDataHandler.getInsertedAttachmentData(fromAttachmentData, toAttachmentData));
            dataHandler.insertHistoryDataRecordAttachmentEntry(txIid, AuditingType.DELETE, timestamp, T8RecordDataHandler.getDeletedAttachmentData(fromAttachmentData, toAttachmentData));
        }
        else
        {
            T8Log.log("WARNING: No previous state found for updated record history entry: " + toEntry);
        }
    }

    private void addEntryData(List<Map<String, Object>> dataRows, HistoryEntry entry)
    {
        for (Map<String, Object> dataRow : dataRows)
        {
            dataRow.put("ROOT_RECORD_ID", entry.getRootRecordId());
            dataRow.put("PARENT_RECORD_ID", entry.getParentRecordId());
        }
    }

    private HistoryEntry getFromEntry(List<HistoryEntry> entries, HistoryEntry toEntry)
    {
        HistoryEntry fromEntry;
        String toRecordId;
        long toTime; // The time of the toEntry.
        long maxTime;// The current maximum time of all applicable entries evaluated.

        maxTime = 0;
        toTime = toEntry.getTimestamp().getMilliseconds();
        fromEntry = null;
        toRecordId = toEntry.getRecordId();
        for (HistoryEntry entry : entries)
        {
            // Proceed if this entry is not the same as the toEntry but is applicable to the same record.
            if ((entry != toEntry) && (entry.getRecordId().equals(toRecordId)))
            {
                long entryTime;

                // Proceed if this entry has a timestamp larger than the maximum found so far, but also less than or equal to the time of the toEntry.
                entryTime = entry.getTimestamp().getMilliseconds();
                if ((entryTime > maxTime) && (entryTime <= toTime))
                {
                    maxTime = entryTime;
                    fromEntry = entry;
                }
            }
        }

        // Return the entry found, or null if not found.
        return fromEntry;
    }

    public void insertAttributeUpdate(T8Timestamp timestamp, RecordAttribute oldAttribute, RecordAttribute newAttribute) throws Exception
    {
        dataHandler.insertHistoryDataRecordAttributeEntry(tx.getIdentifier(), AuditingType.UPDATE, timestamp, oldAttribute, newAttribute);
    }

    public void insertAttributeInsert(T8Timestamp timestamp, RecordAttribute newAttribute) throws Exception
    {
        dataHandler.insertHistoryDataRecordAttributeEntry(tx.getIdentifier(), AuditingType.INSERT, timestamp, null, newAttribute);
    }

    public void insertAttributeDeletion(T8Timestamp timestamp, RecordAttribute attribute) throws Exception
    {
        dataHandler.insertHistoryDataRecordAttributeEntry(tx.getIdentifier(), AuditingType.DELETE, timestamp, null, attribute);
    }
}