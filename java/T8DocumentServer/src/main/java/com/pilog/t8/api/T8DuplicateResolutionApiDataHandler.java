package com.pilog.t8.api;

import com.pilog.t8.api.T8DuplicateResolutionApi.ResolvedState;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatch;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.object.T8DataObjectState;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger;
import com.pilog.t8.definition.data.document.T8DocumentResources;
import java.util.Collection;
import java.util.List;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8DuplicateResolutionApiDataHandler
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;

    public static final String MASTER_RECORD_ID_ATTRIBUTE_ID = "MASTER_RECORD_ID";

    public T8DuplicateResolutionApiDataHandler(T8ServerContext serverContext, T8SessionContext sessionContext) throws Exception
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
    }

    /**
     * Retrieves all of the specified records matches.
     * @param tx The transaction to use for this operation.
     * @param matchInstanceId The match instance from which record matches will
     * be retrieved.
     * @param familyId The family for which record matches will be retrieved.
     * @return The list of records match retrieved from the specified family and
     * the specified match instance.
     * @throws Exception
     */
    public List<T8RecordMatch> retrieveRecordMatches(T8DataTransaction tx, String matchInstanceId, String familyId) throws Exception
    {
        T8DataFilter matchFilter;
        T8DataFilterCriteria matchFilterCriteria;
        T8DataEntityResults matchResults = null;
        String entityId;
        List<T8RecordMatch> matches;

        // Get the entity identifier.
        entityId = T8DocumentResources.DATA_RECORD_MATCH_DE_IDENTIFIER;

        // Create a data filter to use for retrieving the match results that must be submitted
        matchFilterCriteria = new T8DataFilterCriteria();
        matchFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_INSTANCE_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, matchInstanceId));
        matchFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_FAMILY_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, familyId));
        matchFilter = new T8DataFilter(entityId, matchFilterCriteria);

        // Process all of the match results.
        matches = new ArrayList<>();
        try
        {
            matchResults = tx.scroll(entityId, matchFilter);
            while (matchResults.next())
            {
                T8DataEntity matchEntity;
                String instanceId;
                String criteriaId;
                String caseId;
                String recordId1;
                String recordId2;

                matchEntity = matchResults.get();
                recordId1 = (String)matchEntity.getFieldValue(entityId + EF_RECORD_ID1);
                recordId2 = (String)matchEntity.getFieldValue(entityId + EF_RECORD_ID2);
                instanceId = (String)matchEntity.getFieldValue(entityId + EF_INSTANCE_ID);
                criteriaId = (String)matchEntity.getFieldValue(entityId + EF_CRITERIA_ID);
                caseId = (String)matchEntity.getFieldValue(entityId + EF_CASE_ID);

                matches.add(new T8RecordMatch(instanceId, criteriaId, caseId, familyId, null, recordId1, recordId2));
            }

            return matches;
        }
        finally
        {
            if (matchResults != null) matchResults.close();
        }
    }

    /**
     * Retrieves family master records for all families in the specified
     * resolution batch and returns the results in the form of a map containing
     * <familyID:masterRecordID> pairs.
     * @param tx The transaction to use for this operation.
     * @param resolutionID The resolution ID for which to retrieve the family
     * master records.
     * @return A map of the family master records belonging to the specified
     * resolution batch.
     * @throws Exception
     */
    private Map<String, String> retrieveResolutionFamilyMasters(T8DataTransaction tx, String resolutionID) throws Exception
    {
        T8DataFilterCriteria masterFilterCriteria;
        List<T8DataEntity> masterEntities;
        Map<String, String> familyMasters;
        T8DataFilter masterFilter;
        String entityId;

        // Get the entity identifier.
        entityId = T8DocumentResources.DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;

        // Create a data filter to use for the retrieval of family masters.
        masterFilterCriteria = new T8DataFilterCriteria();
        masterFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RESOLUTION_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, resolutionID));
        masterFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RESOLVED_STATE, T8DataFilterCriterion.DataFilterOperator.EQUAL, ResolvedState.MASTER.toString()));
        masterFilter = new T8DataFilter(entityId, masterFilterCriteria);
        masterEntities = tx.select(entityId, masterFilter);

        // Pack the query results into a map <familyID:masterRecordID>.
        familyMasters = new HashMap<>();
        for (T8DataEntity masterEntity : masterEntities)
        {
            String recordId;
            String familyId;

            familyId = (String)masterEntity.getFieldValue(entityId + EF_FAMILY_ID);
            recordId = (String)masterEntity.getFieldValue(entityId + EF_RECORD_ID);
            familyMasters.put(familyId, recordId);
        }

        // Return the results.
        return familyMasters;
    }

    /**
     * This method performs the validation required before a set of records in a newly detected family can be
     * submitted for resolution.  Validation includes;
     * - None of the records in the family to be submitted may be part of a family currently pending resolution (already submitted).
     * - None of the records in the family to be submitted may have a previously resolved state of superseded.
     * Currently this method throws Exception on validation failure but a future
     * update should return a proper validation report.
     * @param tx The tran
     * @param familyId
     * @param recordIdSet
     * @throws Exception
     */
    public void doSubmissionValidation(T8DataTransaction tx, String familyId, Collection<String> recordIdSet) throws Exception
    {
        String entityId;

        entityId = T8DocumentResources.DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;
        for (String recordId : recordIdSet)
        {
            T8DataFilterCriteria resolvedFilterCriteria;
            List<T8DataEntity> resolvedResults;
            T8DataFilter resolvedFilter;

            // Find the record id in the resolution table.
            resolvedFilterCriteria = new T8DataFilterCriteria();
            resolvedFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RECORD_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, recordId));
            resolvedFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RESOLVED_STATE, T8DataFilterCriterion.DataFilterOperator.NOT_EQUAL, ResolvedState.EXCLUDED.toString()));
            resolvedFilter = new T8DataFilter(entityId, resolvedFilterCriteria);
            resolvedResults = tx.select(entityId, resolvedFilter);
            for (T8DataEntity resolvedEntity : resolvedResults)
            {
                String resolvedFamilyId;
                ResolvedState resolvedState;
                boolean finalized;

                // Get the entity data.
                resolvedFamilyId = (String)resolvedEntity.getFieldValue(entityId + EF_FAMILY_ID);
                finalized = "Y".equals((String)resolvedEntity.getFieldValue(entityId + EF_FINALIZED_INDICATOR));
                resolvedState = ResolvedState.valueOf((String)resolvedEntity.getFieldValue(entityId + EF_RESOLVED_STATE));

                // If the entry is not finalized it means we have a family in progress, so we have to reject this submission.
                if (!finalized)
                {
                    throw new Exception("Family '" + familyId + "' cannot be submitted for resolution as some of its records are already involved in a pending resolution of family: " + resolvedFamilyId);
                }
                else if (resolvedState == ResolvedState.SUPERSEDED) // If the resolved state of the record is SUPERSEDED is means this record has already been resolved.
                {
                    throw new Exception("Family '" + familyId + "' cannot be submitted for resolution as some of its records have already been resolved as part of family: " + resolvedFamilyId);
                }
            }
        }
    }

    /**
     * Creates a resolution group for the specified family and the listed records.
     * @param tx The transaction to use for the operation.
     * @param matchInstanceId The match instance id to specify (if any).
     * @param familyId The id of the family to which the records that will be added to the group belong.
     * @param recordIds The records id's to add to the resolution group.
     * @return The id of the newly created resolution.
     * @throws Exception
     */
    public String createResolution(T8DataTransaction tx, String matchInstanceId, String familyId, Collection<String> recordIds) throws Exception
    {
        Timestamp submittedAt;
        String submittedBy;
        String resolutionId;
        String entityId;

        // Generate an ID to identify this resolution submission and a new Family ID.
        resolutionId = T8IdentifierUtilities.createNewGUID();

        // Get the entity identifier.
        entityId = T8DocumentResources.DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;

        // Record auditing information.
        submittedBy = sessionContext != null ? sessionContext.getUserIdentifier() : Thread.currentThread().getName();
        submittedAt = new Timestamp(System.currentTimeMillis());

        // Insert all entries into the resolution enitity.
        for (String recordId : recordIds)
        {
            T8DataEntity resolvedEntity;

            // Insert the record resolution entity.
            resolvedEntity = tx.create(entityId, null);
            resolvedEntity.setFieldValue(entityId + EF_FAMILY_ID, familyId);
            resolvedEntity.setFieldValue(entityId + EF_RECORD_ID, recordId);
            resolvedEntity.setFieldValue(entityId + EF_RESOLVED_STATE, ResolvedState.PENDING_RESOLUTION.toString());
            resolvedEntity.setFieldValue(entityId + EF_FINALIZED_INDICATOR, "N");
            resolvedEntity.setFieldValue(entityId + EF_RESOLUTION_ID, resolutionId);
            resolvedEntity.setFieldValue(entityId + EF_SUBMITTED_BY, submittedBy);
            resolvedEntity.setFieldValue(entityId + EF_SUBMITTED_AT, submittedAt);
            resolvedEntity.setFieldValue(entityId + EF_MATCH_INSTANCE_ID, matchInstanceId);
            tx.insert(resolvedEntity);
        }

        // Return the resolution id.
        return resolutionId;
    }

    /**
     * Finalizes the specified resolution batch and all families it contains.
     * For each family contained by the resolution batch, this record will merge
     * the superseded records in the family with the designated master record.
     * This operation will also mark all of the records as finalized and will
     * records auditing information regarding the user responsible for- and the
     * time of resolution finalization.
     * @param tx The transaction to use for this operation.
     * @param mergerId The identifier of the merger to use.  If null, a
     * default configuration will be used.
     * @param resolutionId The ID of the resolution batch to finalize.
     * @param dataObjectId The identifier of the data object that
     * represents the records being finalized.
     * @param masterStateId The state to which master records will be
     * set.
     * @param nonDuplicateStateId The state to which the non-duplicate
     * records will be set.
     * @param supersededStateId The state to which the superseded records will be set.
     * @throws Exception
     */
    public void finalizeResolution(T8DataTransaction tx, String mergerId, String resolutionId, String dataObjectId, String masterStateId, String nonDuplicateStateId, String supersededStateId) throws Exception
    {
        T8DataFilterCriteria finalizationFilterCriteria;
        T8DataFilterCriteria checkFilterCriteria;
        T8DataEntityResults finalizationResults;
        Map<String, String> familyMasters;
        T8DataFilter finalizationFilter;
        T8DataFilter checkFilter;
        T8DataRecordApi recApi;
        T8DataObjectApi objApi;
        Timestamp finalizedAt;
        String finalizedBy;
        String entityId;

        // Get the record and object apis.
        recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
        objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);

        // Get the entity identifier.
        entityId = T8DocumentResources.DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;

        // Record auditing information.
        finalizedBy = sessionContext != null ? sessionContext.getUserIdentifier() : Thread.currentThread().getName();
        finalizedAt = new Timestamp(System.currentTimeMillis());

        // Create a data filter to use for the validation checking.
        checkFilterCriteria = new T8DataFilterCriteria();
        checkFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RESOLUTION_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, resolutionId));
        checkFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RESOLVED_STATE, T8DataFilterCriterion.DataFilterOperator.EQUAL, ResolvedState.PENDING_RESOLUTION.toString()));
        checkFilter = new T8DataFilter(entityId, checkFilterCriteria);
        if (tx.count(entityId, checkFilter) > 0)
        {
            throw new Exception("Cannot finalize a resolution batch containing records in pending state: " + resolutionId);
        }

        // Retrieve the master record ID's for the resolution batch.
        familyMasters = retrieveResolutionFamilyMasters(tx, resolutionId);

        // Create a data filter to use for the finalization.
        finalizationFilterCriteria = new T8DataFilterCriteria();
        finalizationFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RESOLUTION_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, resolutionId));
        finalizationFilter = new T8DataFilter(entityId, finalizationFilterCriteria);
        finalizationResults = tx.scroll(entityId, finalizationFilter);

        // Loop through all finalization entries, merging superseded records with the family master and setting all entries as finalized.
        try
        {
            while (finalizationResults.next())
            {
                T8DataEntity resolvedEntity;
                ResolvedState resolvedState;
                String familyId;
                String recordId;

                // Get the data from the next entity in the finalization results.
                resolvedEntity = finalizationResults.get();
                familyId = (String)resolvedEntity.getFieldValue(entityId + EF_FAMILY_ID);
                recordId = (String)resolvedEntity.getFieldValue(entityId + EF_RECORD_ID);
                resolvedState = ResolvedState.valueOf((String)resolvedEntity.getFieldValue(entityId + EF_RESOLVED_STATE));

                // If this record is superseded by the master, merge it with the master record.
                if (resolvedState == ResolvedState.SUPERSEDED)
                {
                    String masterRecordId;

                    // Get the family master record id.
                    masterRecordId = familyMasters.get(familyId);
                    if (masterRecordId != null)
                    {
                        // If a merger is specified, use it to merge superseded data into the master record.
                        if (mergerId != null)
                        {
                            Map<String, String> attachmentIdentifierMapping;
                            Map<String, String> mergedRecordIdMapping;
                            T8DataRecordMerger merger;
                            DataRecord sourceFile;
                            DataRecord destinationFile;
                            String mergeFileContextIid;

                            // Generate an ID for the merge file context instance.
                            mergeFileContextIid = T8IdentifierUtilities.createNewGUID();

                            // Get the merger to use for this operation.
                            merger = recApi.getMerger(mergerId);

                            // Get the source data entity and record.
                            sourceFile = recApi.retrieveFile(recordId, null, true, false, false, true, false, true);
                            destinationFile = recApi.retrieveFile(masterRecordId, null, true, false, false, true, false, true);

                            // If we found an existing destination document, merge the source with it, else copy the source.
                            merger.mergeDataRecords(destinationFile, sourceFile);
                            attachmentIdentifierMapping = merger.getAttachmentIdentifierMapping();

                            // If merging of attachments is required export the source attachments so that they are available for import in the destination.
                            recApi.exportAttachments(mergeFileContextIid, attachmentIdentifierMapping, destinationFile);

                            // Save the destination data record and return it.
                            recApi.saveFileStateChange(destinationFile);

                            // Set the state of all records that have been newly added to the master via the merge operation.
                            mergedRecordIdMapping = merger.getRecordIdMapping();
                            for (String sourceRecordId : mergedRecordIdMapping.keySet())
                            {
                                T8DataObjectState state;

                                // Try to get the data object for the source record.  If found, transfer the state.
                                state = objApi.retrieveState(sourceRecordId);
                                if (state != null)
                                {
                                    String mappedRecordId;

                                    // Get the id of the mapped record (created during the merge) and set its state to that of the source object from which it was copied.
                                    mappedRecordId = mergedRecordIdMapping.get(sourceRecordId);
                                    objApi.saveState(dataObjectId, mappedRecordId, state.getId());
                                }
                            }
                        }

                        // Set the master record id attribute on the superseded record.
                        recApi.saveRecordAttribute(recordId, MASTER_RECORD_ID_ATTRIBUTE_ID, masterRecordId);

                        // Set the superseded state of the source record.
                        if (supersededStateId != null)
                        {
                            objApi.saveState(dataObjectId, recordId, supersededStateId);
                        }
                    }
                    else throw new Exception("No master set for family: " + familyId);
                }
                else if (resolvedState == ResolvedState.MASTER)
                {
                    // Set the object state.
                    if (masterStateId != null)
                    {
                        objApi.saveState(dataObjectId, recordId, masterStateId);
                    }
                }
                else
                {
                    // Set the object state.
                    if (nonDuplicateStateId != null)
                    {
                        objApi.saveState(dataObjectId, recordId, nonDuplicateStateId);
                    }
                }

                // Update the finalization indicator and auditing information.
                resolvedEntity.setFieldValue(entityId + EF_FINALIZED_INDICATOR, "Y");
                resolvedEntity.setFieldValue(entityId + EF_FINALIZED_BY, finalizedBy);
                resolvedEntity.setFieldValue(entityId + EF_FINALIZED_AT, finalizedAt);
                tx.update(resolvedEntity);
            }
        }
        finally
        {
            if (finalizationResults != null) finalizationResults.close();
        }
    }

    /**
     * Cancels and deletes the specified in-progress resolution batch.
     * @param tx The transaction to use for the operation.
     * @param resolutionId The ID of the resolution batch to cancel.
     * @throws Exception
     */
    public void cancelResolution(T8DataTransaction tx, String resolutionId) throws Exception
    {
        T8DataFilterCriteria updateFilterCriteria;
        T8DataFilterCriteria checkFilterCriteria;
        T8DataFilter deletionFilter;
        T8DataFilter checkFilter;
        String entityId;

        // Get the entity identifier.
        entityId = T8DocumentResources.DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;

        // Create a data filter to use for the validation checking.
        checkFilterCriteria = new T8DataFilterCriteria();
        checkFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RESOLUTION_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, resolutionId));
        checkFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_FINALIZED_INDICATOR, T8DataFilterCriterion.DataFilterOperator.EQUAL, "Y"));
        checkFilter = new T8DataFilter(entityId, checkFilterCriteria);
        if (tx.count(entityId, checkFilter) > 0)
        {
            throw new Exception("Cannot cancel already finalized resolution batch: " + resolutionId);
        }

        // Create a data filter to use for the update.
        updateFilterCriteria = new T8DataFilterCriteria();
        updateFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RESOLUTION_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, resolutionId));
        deletionFilter = new T8DataFilter(entityId, updateFilterCriteria);
        if (tx.delete(entityId, deletionFilter) == 0)
        {
            throw new Exception("Resolution batch not found: " + resolutionId);
        }
    }

    /**
     * Sets the specified record as the master of the family.  All other records
     * in the family that have not been excluded, will be set to superseded.
     * @param tx The transaction to use for the operation.
     * @param familyId The family in which the record will be set as the master.
     * @param recordId The record to set as the master of the specified family.
     * @throws Exception
     */
    public void setMaster(T8DataTransaction tx, String familyId, String recordId) throws Exception
    {
        T8DataFilterCriteria updateFilterCriteria;
        T8DataEntityResults familyResults = null;
        T8DataFilter updateFilter;
        boolean masterSet;
        String entityId;

        // Get the entity identifier.
        entityId = T8DocumentResources.DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;

        // Create a data filter to use for the update.
        updateFilterCriteria = new T8DataFilterCriteria();
        updateFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_FAMILY_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, familyId));
        updateFilter = new T8DataFilter(entityId, updateFilterCriteria);

        try
        {
            masterSet = false;
            familyResults = tx.scroll(entityId, updateFilter);
            while (familyResults.next())
            {
                T8DataEntity familyEntity;
                ResolvedState resolvedState;
                String familyRecordId;
                boolean finalized;

                familyEntity = familyResults.get();
                familyRecordId = (String)familyEntity.getFieldValue(entityId + EF_RECORD_ID);
                resolvedState = ResolvedState.valueOf((String)familyEntity.getFieldValue(entityId + EF_RESOLVED_STATE));
                finalized = "Y".equals((String)familyEntity.getFieldValue(entityId + EF_FINALIZED_INDICATOR));
                if (finalized)
                {
                    throw new Exception("Family already finalized: " + familyId);
                }
                else if (familyRecordId.equals(recordId))
                {
                    familyEntity.setFieldValue(entityId + EF_RESOLVED_STATE, ResolvedState.MASTER.toString());
                    tx.update(familyEntity);
                    masterSet = true;
                }
                else if (resolvedState != ResolvedState.EXCLUDED)
                {
                    familyEntity.setFieldValue(entityId + EF_RESOLVED_STATE, ResolvedState.SUPERSEDED.toString());
                    tx.update(familyEntity);
                }
            }

            // Make sure we actually found the record specified as the new master.
            if (!masterSet) throw new Exception("Record '" + recordId + "' not found in family: " + familyId);
        }
        finally
        {
            if (familyResults != null) familyResults.close();
        }
    }

    /**
     * Sets the specified records as excluded from the family.  If the record
     * was previously marked as the master of the family, this method will reset
     * all currently superseded records in the family back to the pending state.
     * @param tx The transaction to use for the operation.
     * @param familyId The ID of the family in which the record will be set to
     * excluded.
     * @param recordId The record to set as excluded from the family.
     * @throws Exception
     */
    public void setExcluded(T8DataTransaction tx, String familyId, String recordId) throws Exception
    {
        T8DataEntity resolvedEntity;
        ResolvedState resolvedState;
        boolean finalized;
        String entityId;

        // Get the identifier of the entity to use.
        entityId = T8DocumentResources.DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;

        // Retrieve the entity required.
        resolvedEntity = tx.retrieve(entityId, HashMaps.newHashMap(entityId + EF_FAMILY_ID, familyId, entityId + EF_RECORD_ID, recordId));
        resolvedState = ResolvedState.valueOf((String)resolvedEntity.getFieldValue(entityId + EF_RESOLVED_STATE));
        finalized = "Y".equals((String)resolvedEntity.getFieldValue(entityId + EF_FINALIZED_INDICATOR));
        if (finalized)
        {
            throw new Exception("Family already finalized: " + familyId);
        }
        else if (resolvedState == ResolvedState.MASTER)
        {
            T8DataFilterCriteria updateFilterCriteria;
            T8DataEntityResults familyResults = null;
            T8DataFilter updateFilter;

            // Create a data filter to use for the update.
            updateFilterCriteria = new T8DataFilterCriteria();
            updateFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_FAMILY_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, familyId));
            updateFilter = new T8DataFilter(entityId, updateFilterCriteria);

            // The record was the master, so update its resolved state but also set all superseded records in the family back to pending.
            try
            {
                familyResults = tx.scroll(entityId, updateFilter);
                while (familyResults.next())
                {
                    T8DataEntity familyEntity;
                    String familyRecordId;

                    familyEntity = familyResults.get();
                    familyRecordId = (String)familyEntity.getFieldValue(entityId + EF_RECORD_ID);
                    resolvedState = ResolvedState.valueOf((String)familyEntity.getFieldValue(entityId + EF_RESOLVED_STATE));
                    if (familyRecordId.equals(recordId))
                    {
                        familyEntity.setFieldValue(entityId + EF_RESOLVED_STATE, ResolvedState.EXCLUDED.toString());
                        tx.update(familyEntity);
                    }
                    else if (resolvedState != ResolvedState.EXCLUDED)
                    {
                        familyEntity.setFieldValue(entityId + EF_RESOLVED_STATE, ResolvedState.PENDING_RESOLUTION.toString());
                        tx.update(familyEntity);
                    }
                }
            }
            finally
            {
                if (familyResults != null) familyResults.close();
            }
        }
        else
        {
            // Just update the resolved state.
            resolvedEntity.setFieldValue(entityId + EF_RESOLVED_STATE, ResolvedState.EXCLUDED.toString());
            tx.update(resolvedEntity);
        }
    }

    /**
     * Sets the specified record as superseded.  This operation will only
     * succeed if there is currently a master set in the family.
     * @param tx The transaction to use for the operation.
     * @param familyId The ID of the family to which the records belongs that
     * will be set to superseded.  This family must contain a record currently
     * set to master for the operation to succeed.
     * @param recordId The record to set as superseded by the current master of
     * the family.
     * @throws Exception
     */
    public void setSuperseded(T8DataTransaction tx, String familyId, String recordId) throws Exception
    {
        T8DataEntity masterEntity;
        String entityId;

        // Get the identifier of the entity to use.
        entityId = T8DocumentResources.DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;

        // Retrieve the entity required.
        masterEntity = tx.retrieve(entityId, HashMaps.newHashMap(entityId + EF_FAMILY_ID, familyId, entityId + EF_RESOLVED_STATE, ResolvedState.MASTER.toString()));
        if (masterEntity == null) throw new Exception("No master set for family: " + familyId);
        else
        {
            String resolvedRecordId;
            boolean finalized;

            finalized = "Y".equals((String)masterEntity.getFieldValue(entityId + EF_FINALIZED_INDICATOR));
            resolvedRecordId = (String)masterEntity.getFieldValue(entityId + EF_RECORD_ID);
            if (finalized)
            {
                throw new Exception("Family already finalized: " + familyId);
            }
            else if (resolvedRecordId.equals(recordId))
            {
                throw new Exception("Cannot set the Master of family '" + familyId + "' to state: " + ResolvedState.SUPERSEDED);
            }
            else
            {
                T8DataEntity supersededEntity;

                // Just update the resolved state.
                supersededEntity = masterEntity.copy();
                supersededEntity.setFieldValue(entityId + EF_RECORD_ID, recordId);
                supersededEntity.setFieldValue(entityId + EF_RESOLVED_STATE, ResolvedState.SUPERSEDED.toString());
                tx.update(supersededEntity);
            }
        }
    }

    /**
     * Resets the specified record back to a pending status indicating that a
     * decision regarding its status in the family has not been made yet.  If
     * the specified record is the current master of the family, all
     * non-excluded records in the family will be set back to the pending state
     * as well.
     * @param tx The transaction to use for the operation.
     * @param familyId The ID of the family to which the record belongs that
     * will be set to a pending state.
     * @param recordId The record to set as pending.
     * @throws Exception
     */
    public void setPending(T8DataTransaction tx, String familyId, String recordId) throws Exception
    {
        T8DataEntity resolvedEntity;
        ResolvedState resolvedState;
        boolean finalized;
        String entityId;

        // Get the identifier of the entity to use.
        entityId = T8DocumentResources.DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;

        // Retrieve the entity required.
        resolvedEntity = tx.retrieve(entityId, HashMaps.newHashMap(entityId + EF_FAMILY_ID, familyId, entityId + EF_RECORD_ID, recordId));
        resolvedState = ResolvedState.valueOf((String)resolvedEntity.getFieldValue(entityId + EF_RESOLVED_STATE));
        finalized = "Y".equals((String)resolvedEntity.getFieldValue(entityId + EF_FINALIZED_INDICATOR));
        if (finalized)
        {
            throw new Exception("Family already finalized: " + familyId);
        }
        else if (resolvedState == ResolvedState.MASTER)
        {
            T8DataFilterCriteria updateFilterCriteria;
            T8DataEntityResults familyResults = null;
            T8DataFilter updateFilter;

            // Create a data filter to use for the update.
            updateFilterCriteria = new T8DataFilterCriteria();
            updateFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_FAMILY_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, familyId));
            updateFilter = new T8DataFilter(entityId, updateFilterCriteria);

            // The record was the master, so update its resolved state but also set all superseded records in the family back to pending.
            try
            {
                familyResults = tx.scroll(entityId, updateFilter);
                while (familyResults.next())
                {
                    T8DataEntity familyEntity;

                    familyEntity = familyResults.get();
                    resolvedState = ResolvedState.valueOf((String)familyEntity.getFieldValue(entityId + EF_RESOLVED_STATE));
                    if (resolvedState != ResolvedState.EXCLUDED)
                    {
                        familyEntity.setFieldValue(entityId + EF_RESOLVED_STATE, ResolvedState.PENDING_RESOLUTION.toString());
                        tx.update(familyEntity);
                    }
                }
            }
            finally
            {
                if (familyResults != null) familyResults.close();
            }
        }
        else
        {
            // Just update the resolved state.
            resolvedEntity.setFieldValue(entityId + EF_RESOLVED_STATE, ResolvedState.PENDING_RESOLUTION.toString());
            tx.update(resolvedEntity);
        }
    }
}
