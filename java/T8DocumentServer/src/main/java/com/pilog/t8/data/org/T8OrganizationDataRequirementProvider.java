package com.pilog.t8.data.org;

import com.pilog.t8.api.T8DataRequirementApi;
import com.pilog.t8.api.T8OrganizationApi;
import com.pilog.t8.data.document.DataRequirementProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationDataRequirementProvider implements DataRequirementProvider
{
    private final T8DataTransaction tx;
    private final T8DataRequirementApi drApi;
    private final T8OrganizationApi orgApi;

    public T8OrganizationDataRequirementProvider(T8DataTransaction tx)
    {
        this.tx = tx;
        this.drApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
        this.orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
    }

    @Override
    public DataRequirement getDataRequirement(String drId, String languageId, boolean includeOntology, boolean includeTerminology)
    {
        try
        {
            return drApi.retrieveDataRequirement(drId, orgApi.getDefaultContentLanguageId(), includeOntology, includeTerminology);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving Data Requirement: " + drId, e);
        }
    }
}
