package com.pilog.t8.api;

import com.pilog.t8.api.datarecordeditor.T8DataFileUpdate;
import com.pilog.t8.data.document.datafile.T8DataFileSaveResult;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.ontology.T8OntologyConcept.T8ConceptElementType;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRecordEditorApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordEditorApiOperations
{
    public static class ApiOpenFile extends T8DefaultServerOperation
    {
        public ApiOpenFile(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordEditorApi redApi;
            DataRecord dataFile;
            String fileId;
            String editSessionId;

            // Get the operation parameters we need.
            editSessionId = (String)operationParameters.get(PARAMETER_EDITOR_SESSION_ID);
            fileId = (String)operationParameters.get(PARAMETER_FILE_ID);

            // Perform the API operation.
            redApi = tx.getApi(T8DataRecordEditorApi.API_IDENTIFIER);
            dataFile = redApi.openFile(editSessionId, fileId);
            return HashMaps.createSingular(PARAMETER_FILE, dataFile);
        }
    }

    public static class ApiUpdateFile extends T8DefaultServerOperation
    {
        public ApiUpdateFile(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordEditorApi redApi;
            T8DataFileUpdate inputFileUpdate;
            T8DataFileUpdate outputFileUpdate;
            String editSessionId;

            // Get the operation parameters we need.
            editSessionId = (String)operationParameters.get(PARAMETER_EDITOR_SESSION_ID);
            inputFileUpdate = (T8DataFileUpdate)operationParameters.get(PARAMETER_FILE_UPDATE);

            // Perform the API operation.
            redApi = tx.getApi(T8DataRecordEditorApi.API_IDENTIFIER);
            outputFileUpdate = redApi.updateFile(editSessionId, inputFileUpdate);
            return HashMaps.createSingular(PARAMETER_FILE_UPDATE, outputFileUpdate);
        }
    }

    public static class ApiSaveFile extends T8DefaultServerOperation
    {
        public ApiSaveFile(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordEditorApi redApi;
            T8DataFileSaveResult saveResult;
            String fileId;
            String editSessionId;

            // Get the operation parameters we need.
            editSessionId = (String)operationParameters.get(PARAMETER_EDITOR_SESSION_ID);
            fileId = (String)operationParameters.get(PARAMETER_FILE_ID);

            // Perform the API operation.
            redApi = tx.getApi(T8DataRecordEditorApi.API_IDENTIFIER);
            saveResult = redApi.saveFile(editSessionId, fileId);
            return HashMaps.createSingular(PARAMETER_SAVE_RESULT, saveResult);
        }
    }

    public static class ApiCloseFile extends T8DefaultServerOperation
    {
        public ApiCloseFile(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordEditorApi redApi;
            T8DataFileSaveResult saveResult;
            String fileId;
            String editSessionId;
            Boolean saveChanges;

            // Get the operation parameters we need.
            editSessionId = (String)operationParameters.get(PARAMETER_EDITOR_SESSION_ID);
            fileId = (String)operationParameters.get(PARAMETER_FILE_ID);
            saveChanges = (Boolean)operationParameters.get(PARAMETER_SAVE_CHANGES);

            // Set default values.
            if (saveChanges == null) saveChanges = false;

            // Perform the API operation.
            redApi = tx.getApi(T8DataRecordEditorApi.API_IDENTIFIER);
            saveResult = redApi.closeFile(editSessionId, fileId, saveChanges);
            return HashMaps.createSingular(PARAMETER_SAVE_RESULT, saveResult);
        }
    }

    public static class ApiRetrieveValueSuggestions extends T8DefaultServerOperation
    {
        public ApiRetrieveValueSuggestions(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<RecordValue>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<RecordValue> recordValues;
            T8DataRecordEditorApi redApi;
            String fileId;
            String editSessionId;
            String targetRecordId;
            String targetPropertyId;
            String targetFieldId;
            String targetDataTypeId;
            String searchPrefix;
            Integer pageOffset;
            Integer pageSize;

            // Get the operation parameters we need.
            editSessionId = (String)operationParameters.get(PARAMETER_EDITOR_SESSION_ID);
            fileId = (String)operationParameters.get(PARAMETER_FILE_ID);
            targetRecordId = (String)operationParameters.get(PARAMETER_RECORD_ID);
            targetPropertyId = (String)operationParameters.get(PARAMETER_PROPERTY_ID);
            targetFieldId = (String)operationParameters.get(PARAMETER_FIELD_ID);
            targetDataTypeId = (String)operationParameters.get(PARAMETER_DATA_TYPE_ID);
            searchPrefix = (String)operationParameters.get(PARAMETER_SEARCH_PREFIX);
            pageOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);

            // Set default values for unreceived parameters.
            if (pageOffset == null) pageOffset = 0;
            if (pageSize == null) pageSize = 20;

            // Perform the API operation.
            redApi = tx.getApi(T8DataRecordEditorApi.API_IDENTIFIER);
            recordValues = redApi.retrieveValueSuggestions(editSessionId, fileId, targetRecordId, targetPropertyId, targetFieldId, targetDataTypeId, searchPrefix, pageOffset, pageSize);
            return HashMaps.createSingular(PARAMETER_RECORD_VALUES, recordValues);
        }
    }

    public static class ApiRetrieveValueOptions extends T8DefaultServerOperation
    {
        public ApiRetrieveValueOptions(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<RecordValue>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<RecordValue> recordValues;
            T8DataRecordEditorApi redApi;
            List<T8ConceptElementType> searchElements;
            String fileId;
            String editSessionId;
            String targetRecordId;
            String targetPropertyId;
            String targetFieldId;
            String targetDataTypeId;
            String searchString;
            Integer pageOffset;
            Integer pageSize;

            // Get the operation parameters we need.
            editSessionId = (String)operationParameters.get(PARAMETER_EDITOR_SESSION_ID);
            fileId = (String)operationParameters.get(PARAMETER_FILE_ID);
            targetRecordId = (String)operationParameters.get(PARAMETER_RECORD_ID);
            targetPropertyId = (String)operationParameters.get(PARAMETER_PROPERTY_ID);
            targetFieldId = (String)operationParameters.get(PARAMETER_FIELD_ID);
            targetDataTypeId = (String)operationParameters.get(PARAMETER_DATA_TYPE_ID);
            searchString = (String)operationParameters.get(PARAMETER_SEARCH_STRING);
            pageOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);

            // Set default values for unreceived parameters.
            if (pageOffset == null) pageOffset = 0;
            if (pageSize == null) pageSize = 20;
            searchElements = Arrays.asList(T8ConceptElementType.values());

            // Perform the API operation.
            redApi = tx.getApi(T8DataRecordEditorApi.API_IDENTIFIER);
            recordValues = redApi.retrieveValueOptions(editSessionId, fileId, targetRecordId, targetPropertyId, targetFieldId, targetDataTypeId, searchString, searchElements, pageOffset, pageSize);
            return HashMaps.createSingular(PARAMETER_RECORD_VALUES, recordValues);
        }
    }
}
