package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator.ConceptRenderType;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordFieldMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordInstanceMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordPropertyMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordValueMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DefaultDataRecordMergerDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordMerger implements T8DataRecordMerger
{
    private final T8DataRecordMergeContext mergeContext;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8ExpressionEvaluator expressionEvaluator;
    private final Set<DataRecord> newDataRecords;
    private final Set<DataRecord> updatedDataRecords;
    private final List<T8DataRecordInstanceMerger> instanceMergers;
    private final List<T8DataRecordPropertyMerger> propertyMergers;
    private final List<T8DataRecordValueMerger> valueMergers;
    private final List<T8DataRecordFieldMerger> fieldMergers;
    private final Map<String, String> recordIdMapping;
    private final Map<String, String> attachmentIdMapping;
    private final Map<String, String> particleSettingsIdMapping;
    private final boolean copyOntologyCodes;
    private final boolean copyAssignNewAttachmentIdentifiers;
    private final boolean copyAssignNewParticleSettingsIds;
    private final boolean copyAssignNewRecordIdentifiers;
    private final boolean copyTagSourceToCopiedRecord;
    private final boolean copyDescendantRecords;
    private final boolean copyTags;
    private TerminologyProvider terminologyProvider;

    public T8DefaultDataRecordMerger(T8DataRecordMergeContext mergeContext, T8DataRecordMergeConfiguration config)
    {
        this.mergeContext = mergeContext;
        this.context = mergeContext.getContext();
        this.serverContext = context.getServerContext();
        this.terminologyProvider = mergeContext.getTerminologyProvider();
        this.newDataRecords = new HashSet<DataRecord>();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.instanceMergers = new LinkedList<T8DataRecordInstanceMerger>();
        this.propertyMergers = new LinkedList<T8DataRecordPropertyMerger>();
        this.fieldMergers = new LinkedList<T8DataRecordFieldMerger>();
        this.valueMergers = new LinkedList<T8DataRecordValueMerger>();
        this.recordIdMapping = new HashMap<>();
        this.attachmentIdMapping = new HashMap<>();
        this.particleSettingsIdMapping = new HashMap<>();
        this.expressionEvaluator = serverContext.getExpressionEvaluator(context);
        this.copyAssignNewAttachmentIdentifiers = config.isCopyAssignNewAttachmentIdentifiers();
        this.copyAssignNewParticleSettingsIds = true;
        this.copyAssignNewRecordIdentifiers = config.isCopyAssignNewRecordIdentifiers();
        this.copyOntologyCodes = config.isCopyOntologyCodes();
        this.copyTagSourceToCopiedRecord = config.isCopyTagSourceToCopiedRecord();
        this.copyDescendantRecords = config.isCopyDescendantRecords();
        this.copyTags = config.isCopyTags();
        initializeDefaultMergers(config);
    }

    private void initializeDefaultMergers(T8DataRecordMergeConfiguration config)
    {
        // Add the default mergers to be used when merger is constructed without a definition.
        instanceMergers.add(new T8DefaultDataRecordInstanceMerger(T8DefaultDataRecordMerger.this, config));
        propertyMergers.add(new T8DefaultDataRecordPropertyMerger(T8DefaultDataRecordMerger.this, config));
        fieldMergers.add(new T8DefaultDataRecordFieldMerger(T8DefaultDataRecordMerger.this, config));
        valueMergers.add(new T8DataRecordReferenceMerger(T8DefaultDataRecordMerger.this, config, "DEFAULT_REFERENCE_MERGER"));
        valueMergers.add(new T8DataRecordIndependentReferenceMerger(T8DefaultDataRecordMerger.this, config, "DEFAULT_INDEPENDENT_REFERENCE_MERGER"));
        valueMergers.add(new T8DataRecordAttachmentMerger(T8DefaultDataRecordMerger.this, config, "DEFAULT_ATTACHMENT_MERGER"));
        valueMergers.add(new T8DefaultDataRecordValueMerger(T8DefaultDataRecordMerger.this, config));
    }

    public T8DefaultDataRecordMerger(T8DataRecordMergeContext mergeContext, T8DefaultDataRecordMergerDefinition definition)
    {
        this.mergeContext = mergeContext;
        this.context = mergeContext.getContext();
        this.serverContext = context.getServerContext();
        this.terminologyProvider = mergeContext.getTerminologyProvider();
        this.newDataRecords = new HashSet<DataRecord>();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.instanceMergers = new LinkedList<T8DataRecordInstanceMerger>();
        this.propertyMergers = new LinkedList<T8DataRecordPropertyMerger>();
        this.fieldMergers = new LinkedList<T8DataRecordFieldMerger>();
        this.valueMergers = new LinkedList<T8DataRecordValueMerger>();
        this.recordIdMapping = new HashMap<String, String>();
        this.attachmentIdMapping = new HashMap<String, String>();
        this.particleSettingsIdMapping = new HashMap<String, String>();
        this.expressionEvaluator = serverContext.getExpressionEvaluator(context);
        this.copyAssignNewAttachmentIdentifiers = definition.isCopyAssignNewAttachmentIdentifiers();
        this.copyAssignNewParticleSettingsIds = true;
        this.copyAssignNewRecordIdentifiers = definition.isCopyAssignNewRecordIdentifiers();
        this.copyOntologyCodes = definition.isCopyOntologyCodes();
        this.copyTagSourceToCopiedRecord = definition.isCopyTagSourceToCopiedRecord();
        this.copyDescendantRecords = definition.isCopyDescendantRecords();
        this.copyTags = definition.isCopyTags();

        // Create all the instance mergers.
        for (T8DataRecordInstanceMergerDefinition instanceMergerDefinition : definition.getInstanceMergerDefinitions())
        {
            instanceMergers.add(instanceMergerDefinition.getNewMergerInstance(T8DefaultDataRecordMerger.this));
        }

        // Create property mergers.
        for (T8DataRecordPropertyMergerDefinition propertyMergerDefinition : definition.getPropertyMergerDefinitions())
        {
            propertyMergers.add(propertyMergerDefinition.getNewMergerInstance(T8DefaultDataRecordMerger.this));
        }

        // Create all the field mergers.
        for (T8DataRecordFieldMergerDefinition fieldMergerDefinition : definition.getFieldMergerDefinitions())
        {
            fieldMergers.add(fieldMergerDefinition.getNewMergerInstance(T8DefaultDataRecordMerger.this));
        }

        // Create all the value mergers.
        for (T8DataRecordValueMergerDefinition valueMergerDefinition : definition.getValueMergerDefinitions())
        {
            valueMergers.add(valueMergerDefinition.getNewMergerInstance(T8DefaultDataRecordMerger.this));
        }
    }

    @Override
    public T8DataTransaction getTransaction()
    {
        return mergeContext.getTransaction();
    }

    @Override
    public T8Context getContext()
    {
        return context;
    }

    @Override
    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        return expressionEvaluator;
    }

    @Override
    public String getDefaultContentLanguageID()
    {
        return mergeContext.getDefaultContentLanguageID();
    }

    @Override
    public TerminologyProvider getTerminologyProvider()
    {
        return mergeContext.getTerminologyProvider();
    }

    @Override
    public OntologyProvider getOntologyProvider()
    {
        return mergeContext.getOntologyProvider();
    }

    @Override
    public DataRequirementInstanceProvider getDRInstanceProvider()
    {
        return mergeContext.getDRInstanceProvider();
    }

    @Override
    public void reset()
    {
        newDataRecords.clear();
        updatedDataRecords.clear();
        recordIdMapping.clear();
        attachmentIdMapping.clear();
        particleSettingsIdMapping.clear();

        // Reset all the instance mergers.
        for (T8DataRecordInstanceMerger instanceMerger : instanceMergers)
        {
            instanceMerger.reset();
        }

        // Reset all property mergers.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            propertyMerger.reset();
        }

        // Reset the value mergers.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            valueMerger.reset();
        }

        // Reset the field mergers.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            fieldMerger.reset();
        }
    }

    @Override
    public Map<String, String> getRecordIdMapping()
    {
        Map<String, String> recordIdMap;

        // Create a map of all record identifier changes.
        recordIdMap = new HashMap<String, String>(recordIdMapping);

        // Add all instance merger mappings.
        for (T8DataRecordInstanceMerger instanceMerger : instanceMergers)
        {
            recordIdMap.putAll(instanceMerger.getRecordIdMapping());
        }

        // Add all property merger mappings.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            recordIdMap.putAll(propertyMerger.getRecordIdMapping());
        }

        // Add all value merger mappings.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            recordIdMap.putAll(valueMerger.getRecordIdMapping());
        }

        // Add all field merger mappings.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            recordIdMap.putAll(fieldMerger.getRecordIdMapping());
        }

        return recordIdMap;
    }

    @Override
    public Map<String, String> getAttachmentIdentifierMapping()
    {
        Map<String, String> attachmentIdentifierMap;

        // Create a map of all attachment identifier changes.
        attachmentIdentifierMap = new HashMap<String, String>(attachmentIdMapping);

        // Add all instance merger mappings.
        for (T8DataRecordInstanceMerger instanceMerger : instanceMergers)
        {
            attachmentIdentifierMap.putAll(instanceMerger.getAttachmentIdentifierMapping());
        }

        // Add all property merger mappings.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            attachmentIdentifierMap.putAll(propertyMerger.getAttachmentIdentifierMapping());
        }

        // Add all value merger mappings.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            attachmentIdentifierMap.putAll(valueMerger.getAttachmentIdentifierMapping());
        }

        // Add all field merger mappings.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            attachmentIdentifierMap.putAll(fieldMerger.getAttachmentIdentifierMapping());
        }

        return attachmentIdentifierMap;
    }

    @Override
    public Set<DataRecord> getNewDataRecordSet()
    {
        Set<DataRecord> newRecordSet;

        // Create a set of all new records added during the merge operation.
        newRecordSet = new HashSet<DataRecord>(newDataRecords);

        // Add all instance merger new records.
        for (T8DataRecordInstanceMerger instanceMerger : instanceMergers)
        {
            newRecordSet.addAll(instanceMerger.getNewDataRecordSet());
        }

        // Add all property merger new records.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            newRecordSet.addAll(propertyMerger.getNewDataRecordSet());
        }

        // Add all value merger new records.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            newRecordSet.addAll(valueMerger.getNewDataRecordSet());
        }

        // Add all field merger new records.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            newRecordSet.addAll(fieldMerger.getNewDataRecordSet());
        }

        return newRecordSet;
    }

    @Override
    public Set<DataRecord> getUpdatedDataRecordSet()
    {
        Set<DataRecord> updatedRecordSet;

        // Create a set of all records updated during the merge operation.
        updatedRecordSet = new HashSet<DataRecord>(updatedDataRecords);

        // Add all instance merger updated records.
        for (T8DataRecordInstanceMerger instanceMerger : instanceMergers)
        {
            updatedRecordSet.addAll(instanceMerger.getUpdatedDataRecordSet());
        }

        // Add all property merger updated records.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            updatedRecordSet.addAll(propertyMerger.getUpdatedDataRecordSet());
        }

        // Add all value merger updated records.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            updatedRecordSet.addAll(valueMerger.getUpdatedDataRecordSet());
        }

        // Add all field merger updated records.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            updatedRecordSet.addAll(fieldMerger.getUpdatedDataRecordSet());
        }

        return updatedRecordSet;
    }

    @Override
    public DataRecord copyDataRecord(DataRecord sourceRecord)
    {
        DataRecord copiedRootRecord;

        // Create a new copy of the source record.
        copiedRootRecord = sourceRecord.copy(true, false, copyTags, false, true, copyDescendantRecords);
        for (DataRecord copiedRecord : copiedRootRecord.getDataRecords())
        {
            T8OntologyConcept copiedOntology;
            String oldRecordId;

            // Store the old record ID.
            oldRecordId = copiedRecord.getID();

            // Clear terms and definitions from the copied ontology.
            copiedOntology = copiedRecord.getOntology();
            copiedOntology.clearTerms();
            copiedOntology.clearDefinitions();

            // Assign new code ID's or clear the codes if they do not need to be copied.
            if (copyOntologyCodes)
            {
                copiedOntology.assignNewCodeIDs();
            }
            else copiedOntology.clearCodes();

            // Assign new record identifiers if required.
            if (copyAssignNewRecordIdentifiers)
            {
                recordIdMapping.putAll(copiedRecord.assignNewRecordIdentifiers(false));
            }

            // Assign new attachment identifiers if required.
            if (copyAssignNewAttachmentIdentifiers)
            {
                attachmentIdMapping.putAll(copiedRecord.assignNewAttachmentIdentifiers(false));
            }

            // Assign new data string id's if required.
            if (copyAssignNewParticleSettingsIds)
            {
                particleSettingsIdMapping.putAll(copiedRecord.assignNewDataStringParticleSettingsIds(false));
            }

            // Tag the destination record if required.
            if (copyTagSourceToCopiedRecord)
            {
                copiedRecord.setAttribute(T8DataRecordMerger.SOURCE_RECORD_ID_ATTRIBUTE_IDENTIFIER, oldRecordId);
            }
        }

        // Return the copied root record.
        return copiedRootRecord;
    }

    @Override
    public void mergeDataRecords(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        T8DataRecordInstanceMerger instanceMerger;

        // Try to find a specific applicable merger for the records.
        instanceMerger = getInstanceMerger(destinationRecord, sourceRecord);
        if (instanceMerger != null)
        {
            instanceMerger.mergeDataRecords(destinationRecord, sourceRecord);
        }
        else
        {
            // No applicable merger found.
            throw new RuntimeException("No applicable instance merger found for record: " + destinationRecord);
        }
    }

    @Override
    public DataRecord synchronizeDataRecords(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        T8DataRecordSynchronizer synchronizer;
        DataRecord syncronizedRecord;

        // Synchronizer the data records.
        synchronizer = new T8DataRecordSynchronizer();
        syncronizedRecord = synchronizer.synchronizeDataRecords(destinationRecord, sourceRecord);

        // Fetched the data changes from the synchronizer.
        newDataRecords.addAll(synchronizer.getNewDataRecordSet());
        updatedDataRecords.addAll(synchronizer.getUpdatedDataRecordSet());
        attachmentIdMapping.putAll(synchronizer.getAttachmentIdentifierMapping());

        return syncronizedRecord;
    }

    @Override
    public T8DataRecordInstanceMerger getInstanceMerger(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        // Try to find a specific applicable merger for the records.
        for (T8DataRecordInstanceMerger instanceMerger : instanceMergers)
        {
            if (instanceMerger.isApplicable(destinationRecord))
            {
                return instanceMerger;
            }
        }

        // No applicable merger found.
        return null;
    }

    @Override
    public T8DataRecordPropertyMerger getPropertyMerger(RecordProperty destinationProperty, RecordProperty sourceProperty)
    {
        // Try to find a specific applicable merger for the properties.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            if (propertyMerger.isApplicable(destinationProperty))
            {
                return propertyMerger;
            }
        }

        // No applicable merger found.
        return null;
    }

    @Override
    public T8DataRecordFieldMerger getFieldMerger(RecordValue destinationField, RecordValue sourceField)
    {
        // Try to find a specific applicable merger for the fields.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            if (fieldMerger.isApplicable(destinationField, sourceField))
            {
                return fieldMerger;
            }
        }

        // No applicable merger found.
        return null;
    }

    @Override
    public T8DataRecordValueMerger getValueMerger(RecordValue destinationValue, RecordValue sourceValue)
    {
        // Try to find a specific applicable merger for the values.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            if (valueMerger.isApplicable(destinationValue, sourceValue))
            {
                return valueMerger;
            }
        }

        // No applicable merger found.
        return null;
    }

    @Override
    public T8DataRecordValueStringGenerator getFFTGenerator()
    {
        T8DataRecordValueStringGenerator generator;

        generator = new T8DataRecordValueStringGenerator();
        generator.setIncludeSubDocumentValueStrings(true);
        generator.setDataRecordProvider(null);
        generator.setTerminologyProvider(terminologyProvider);
        generator.setIncludeRecordFFT(true);
        generator.setIncludeSectionConcepts(true);
        generator.setSectionConceptRenderType(ConceptRenderType.TERM);
        generator.setIncludePropertyFFT(true);
        generator.setIncludePropertyConcepts(true);
        generator.setPropertyConceptRenderType(ConceptRenderType.TERM);
        generator.setValueConceptRenderType(ConceptRenderType.TERM);
        generator.setLanguageID(null);
        return generator;
    }
}
