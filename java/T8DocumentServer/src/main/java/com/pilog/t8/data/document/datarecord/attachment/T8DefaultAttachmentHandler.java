package com.pilog.t8.data.document.datarecord.attachment;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceDefinition;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultAttachmentHandler extends T8AbstractAttachmentHandler
{
    private boolean databaseStorageEnabled;
    private String fileContextID;

    public T8DefaultAttachmentHandler(T8ServerContext serverContext, T8DataTransaction tx, String dataRecordDEID)
    {
        super(tx.getContext(), getDataConnection(tx, dataRecordDEID), serverContext.getFileManager(), getAttachmentTableName(tx, dataRecordDEID));
    }

    private static String getAttachmentTableName(T8DataTransaction tx, String dataRecordDEID)
    {
        try
        {
            T8DataEntityDefinition entityDefinition;
            T8DataRecordDataSourceDefinition dataSourceDefinition;

            entityDefinition = tx.getDataEntityDefinition(dataRecordDEID);
            dataSourceDefinition = (T8DataRecordDataSourceDefinition)tx.getDataSourceDefinition(entityDefinition.getDataSourceIdentifier());
            return dataSourceDefinition.getAttachmentTableName();
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching attachment table name for entity: " + dataRecordDEID, e);
        }
    }

    private static T8DataConnection getDataConnection(T8DataTransaction tx, String dataRecordDEID)
    {
        try
        {
            T8DataEntityDefinition entityDefinition;
            T8DataRecordDataSourceDefinition dataSourceDefinition;

            entityDefinition = tx.getDataEntityDefinition(dataRecordDEID);
            dataSourceDefinition = (T8DataRecordDataSourceDefinition)tx.getDataSourceDefinition(entityDefinition.getDataSourceIdentifier());
            return tx.getDataConnection(dataSourceDefinition.getConnectionIdentifier());
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching attachment table name for entity: " + dataRecordDEID, e);
        }
    }

    public void setDatabaseStorageEnabled(boolean databaseStorageEnabled)
    {
        this.databaseStorageEnabled = databaseStorageEnabled;
    }

    public void setFileContextID(String fileContextID)
    {
        this.fileContextID = fileContextID;
    }

    @Override
    public boolean isDatabaseStorageEnabled(String drInstanceID) throws Exception
    {
        return databaseStorageEnabled;
    }

    @Override
    public String getFileContextID(String drInstanceID) throws Exception
    {
        return fileContextID;
    }
}
