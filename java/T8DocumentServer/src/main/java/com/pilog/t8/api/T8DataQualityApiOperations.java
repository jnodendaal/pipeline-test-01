package com.pilog.t8.api;

import com.pilog.t8.data.document.dataquality.T8GroupedDataQualityReport;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataQualityApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataQualityApiOperations
{
    public static class RetrieveRootDataQualityReport extends T8DefaultServerOperation
    {
        public RetrieveRootDataQualityReport(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8GroupedDataQualityReport> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8GroupedDataQualityReport dqReport;
            List<String> rootDrInstanceIdList;
            T8DataQualityApi dqApi;

            // Get the operation parameters we need.
            rootDrInstanceIdList = (List<String>)operationParameters.get(PARAMETER_ROOT_DR_INSTANCE_ID_LIST);

            // Perform the API operation.
            dqApi = tx.getApi(T8DataQualityApi.API_IDENTIFIER);
            dqReport = dqApi.retrieveRootDataQualityReport(rootDrInstanceIdList);
            return HashMaps.createSingular(PARAMETER_DATA_QUALITY_REPORT, dqReport);
        }
    }

    public static class RetrieveDataQualityReportGroupedByDrInstance extends T8DefaultServerOperation
    {
        public RetrieveDataQualityReportGroupedByDrInstance(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8GroupedDataQualityReport> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8GroupedDataQualityReport dqReport;
            String rootDrInstanceId;
            T8DataQualityApi dqApi;

            // Get the operation parameters we need.
            rootDrInstanceId = (String)operationParameters.get(PARAMETER_ROOT_DR_INSTANCE_ID);

            // Perform the API operation.
            dqApi = tx.getApi(T8DataQualityApi.API_IDENTIFIER);
            dqReport = dqApi.retrieveDataQualityReportGroupedByDrInstance(rootDrInstanceId, null);
            return HashMaps.createSingular(PARAMETER_DATA_QUALITY_REPORT, dqReport);
        }
    }

    public static class RetrieveRootDataQualityReportGroupedByDrInstance extends T8DefaultServerOperation
    {
        public RetrieveRootDataQualityReportGroupedByDrInstance(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8GroupedDataQualityReport> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8GroupedDataQualityReport dqReport;
            String groupByTargetPropertyId;
            String groupByTargetDrInstanceId;
            String rootDrInstanceId;
            T8DataQualityApi dqApi;

            // Get the operation parameters we need.
            rootDrInstanceId = (String)operationParameters.get(PARAMETER_ROOT_DR_INSTANCE_ID);
            groupByTargetDrInstanceId = (String)operationParameters.get(PARAMETER_GROUP_BY_TARGET_DR_INSTANCE_ID);
            groupByTargetPropertyId = (String)operationParameters.get(PARAMETER_GROUP_BY_TARGET_PROPERTY_ID);

            // Perform the API operation.
            dqApi = tx.getApi(T8DataQualityApi.API_IDENTIFIER);
            dqReport = dqApi.retrieveRootDataQualityReportGroupedByDrInstance(rootDrInstanceId, groupByTargetDrInstanceId, groupByTargetPropertyId, null);
            return HashMaps.createSingular(PARAMETER_DATA_QUALITY_REPORT, dqReport);
        }
    }

    public static class RetrieveRootDataQualityReportGroupedByValueConcept extends T8DefaultServerOperation
    {
        public RetrieveRootDataQualityReportGroupedByValueConcept(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8GroupedDataQualityReport> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8GroupedDataQualityReport dqReport;
            String groupByTargetPropertyId;
            String groupByTargetDrInstanceId;
            String rootDrInstanceId;
            T8DataQualityApi dqApi;

            // Get the operation parameters we need.
            rootDrInstanceId = (String)operationParameters.get(PARAMETER_ROOT_DR_INSTANCE_ID);
            groupByTargetDrInstanceId = (String)operationParameters.get(PARAMETER_GROUP_BY_TARGET_DR_INSTANCE_ID);
            groupByTargetPropertyId = (String)operationParameters.get(PARAMETER_GROUP_BY_TARGET_PROPERTY_ID);

            // Perform the API operation.
            dqApi = tx.getApi(T8DataQualityApi.API_IDENTIFIER);
            dqReport = dqApi.retrieveRootDataQualityReportGroupedByValueConcept(rootDrInstanceId, groupByTargetDrInstanceId, groupByTargetPropertyId, null);
            return HashMaps.createSingular(PARAMETER_DATA_QUALITY_REPORT, dqReport);
        }
    }
}