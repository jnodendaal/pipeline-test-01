package com.pilog.t8.data.document.datastring.transformation;

/**
 * @author Bouwer du Preez
 */
public class T8CharacterStripTransformation
{
    public void applyTransformation(StringBuffer string) throws Exception
    {
        String stripCharacterString;

        stripCharacterString = "TEST";
        if (stripCharacterString != null)
        {
            int charIndex;

            charIndex = 0;
            while (charIndex < string.length())
            {
                char character;

                character = string.charAt(charIndex);
                if (stripCharacterString.indexOf(character) > -1)
                {
                    string.deleteCharAt(charIndex);
                }
                else charIndex++;
            }
        }
    }
}
