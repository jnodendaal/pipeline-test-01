package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceChecker;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceError;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.security.T8Context;
import java.util.Collection;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordRequirementValidator
{
    private T8DataFileValidationReport validationReport;
    private final DataRecordProvider recordProvider;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8DataRecordValueStringGenerator fftValueStringGenerator;

    public T8DataRecordRequirementValidator(T8Context context, DataRecordProvider recordProvider, TerminologyProvider terminologyProvider)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.recordProvider = recordProvider;
        this.fftValueStringGenerator = new T8DataRecordValueStringGenerator();
        this.fftValueStringGenerator.setTerminologyProvider(terminologyProvider);
    }

    public void setValidationReport(T8DataFileValidationReport validationReport)
    {
        this.validationReport = validationReport;
    }

    public void moveInvalidDataToFft(Collection<DataRecord> dataRecords)
    {
        T8DataRecordConformanceChecker conformanceChecker;

        conformanceChecker = new T8DataRecordConformanceChecker(fftValueStringGenerator);
        for (DataRecord dataRecord : dataRecords)
        {
            conformanceChecker.moveInvalidDataToFFT(dataRecord);
        }
    }

    public void validateRecords(Collection<DataRecord> dataRecords) throws Exception
    {
        T8DataRecordConformanceChecker conformanceChecker;

        conformanceChecker = new T8DataRecordConformanceChecker(fftValueStringGenerator);
        for (DataRecord dataRecord : dataRecords)
        {
            for (RecordProperty recordProperty : dataRecord.getRecordProperties())
            {
                boolean unique;

                // Do uniqueness check.
                unique = recordProperty.getPropertyRequirement().isUnique();
                if (unique)
                {
                    // TODO:  Check that the property value is unique.
                }

                // Do the conformance check.
                for (T8DataRecordConformanceError error : conformanceChecker.validateRecordProperty(recordProperty))
                {
                    String propertyID;

                    propertyID = recordProperty.getPropertyID();
                    validationReport.addRecordValueError(dataRecord, propertyID, null, error.getErrorMessage());
                }
            }
        }
    }
}
