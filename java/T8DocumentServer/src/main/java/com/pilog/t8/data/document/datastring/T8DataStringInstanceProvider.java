package com.pilog.t8.data.document.datastring;

import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringInstanceProvider
{
    private final OntologyProvider ontologyProvider;
    private final TerminologyProvider terminologyProvider;
    private final T8ExpressionEvaluator expressionEvaluator;
    private final List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> dsInstanceLinks; // Used to cache data string instances linked to specific data requirement instances.

    public T8DataStringInstanceProvider(OntologyProvider ontologyProvider, TerminologyProvider terminologyProvider, List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> dsInstanceLinks, T8ExpressionEvaluator expressionEvaluator)
    {
        this.ontologyProvider = ontologyProvider;
        this.terminologyProvider = terminologyProvider;
        this.dsInstanceLinks = new ArrayList<>();
        this.expressionEvaluator = expressionEvaluator;
        setDataStringInstanceLinks(dsInstanceLinks);
    }

    public final void setDataStringInstanceLinks(List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> newLinks)
    {
        dsInstanceLinks.clear();
        if (newLinks != null)
        {
            dsInstanceLinks.addAll(newLinks);
        }
    }

    public Pair<T8DataStringInstanceLink, T8DataStringInstance> getDataStringInstance(DataRecord dataRecord, String dsInstanceId)
    {
        List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> links;

        links = getDataStringInstances(dataRecord, dsInstanceId, null, null);
        return links.size() > 0 ? links.get(0) : null;
    }

    public Pair<T8DataStringInstanceLink, T8DataStringInstance> getDataStringInstance(DataRecord dataRecord, String languageId, String typeId)
    {
        List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> links;

        links = getDataStringInstances(dataRecord, null, typeId, languageId);
        return links.size() > 0 ? links.get(0) : null;
    }

    public List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> getDataStringInstances(DataRecord dataRecord)
    {
        return getDataStringInstances(dataRecord, null, null, null);
    }

    private List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> getDataStringInstances(DataRecord dataRecord, String dsInstanceId, String dsTypeId, String languageId)
    {
        List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> links;
        Set<String> addedDsInstanceIdSet;
        String drInstanceId;

        // Get the Data Requirement Instance id of the record we are using.
        drInstanceId = dataRecord.getDataRequirementInstanceID();

        // We use a set for the instances to ensure that only one of each is returned.
        links = new ArrayList<>();
        addedDsInstanceIdSet = new HashSet<>();
        for (Pair<T8DataStringInstanceLink, T8DataStringInstance> pair : dsInstanceLinks)
        {
            T8DataStringInstanceLink dsInstanceLink;
            T8DataStringInstance dsInstance;

            // Get the details of the link.
            dsInstanceLink = pair.getValue1();
            dsInstance = pair.getValue2();

            // If we've already identified this instance using a preceding link, do not process it again (the first link in the list will be used as it has higher priority/lower sequence).
            if (!addedDsInstanceIdSet.contains(dsInstance.getInstanceID()))
            {
                // Check the Data Requirement Instance id of the link against that of the record.
                if (dsInstanceLink.getDrInstanceId().equals(drInstanceId))
                {
                    // Check the Data String Instance id filter, if specified.
                    if ((dsInstanceId == null) || (dsInstance.getInstanceID().equals(dsInstanceId)))
                    {
                        // Check the Data String Type id filter, if specified.
                        if ((dsTypeId == null) || (dsInstance.getTypeID().equals(dsTypeId)))
                        {
                            // Check the Language id filter, if specified.
                            if ((languageId == null) || (dsInstance.getLanguageID().equals(languageId)))
                            {
                                String conditionExpression;

                                // Get the link condition expression and evaluate it if one is specified.
                                conditionExpression = dsInstanceLink.getConditionExpression();
                                if (!Strings.isNullOrEmpty(conditionExpression))
                                {
                                    try
                                    {
                                        Object result;

                                        // Evaluate the condition expression.
                                        result = expressionEvaluator.evaluateExpression(conditionExpression, null, dataRecord);
                                        if (result != null)
                                        {
                                            // Process the result according to its type.
                                            if (result instanceof Boolean)
                                            {
                                                if ((Boolean)result)
                                                {
                                                    addedDsInstanceIdSet.add(dsInstance.getInstanceID());
                                                    links.add(pair);
                                                }
                                            }
                                            else if (result instanceof Collection)
                                            {
                                                if (!((Collection)result).isEmpty())
                                                {
                                                    // If the result object is a non-empty collection add the instances linked.
                                                    addedDsInstanceIdSet.add(dsInstance.getInstanceID());
                                                    links.add(pair);
                                                }
                                            }
                                            else
                                            {
                                                // If the result object is not a collection, the result is adequate for inclusion.
                                                addedDsInstanceIdSet.add(dsInstance.getInstanceID());
                                                links.add(pair);
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        throw new RuntimeException("Exception while evaluating Data String Instance Link condition expression: " + conditionExpression, e);
                                    }
                                }
                                else
                                {
                                    // If there is no condition expression always add the instance.
                                    addedDsInstanceIdSet.add(dsInstance.getInstanceID());
                                    links.add(pair);
                                }
                            }
                        }
                    }
                }
            }
        }

        // Return the result as a list.
        return links;
    }
}
