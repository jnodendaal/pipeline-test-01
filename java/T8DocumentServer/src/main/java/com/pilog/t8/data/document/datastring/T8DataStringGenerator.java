package com.pilog.t8.data.document.datastring;

import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.api.T8DataStringApi;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8OrganizationApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.T8LRUCachedDataRecordProvider;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datastring.T8DataStringFilter.T8DataStringFilterBuilder;
import com.pilog.t8.data.org.T8OrganizationDataRecordProvider;
import com.pilog.t8.data.org.T8OrganizationTerminologyProvider;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.data.org.T8OrganizationStructure.OrganizationSetType;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringGenerator implements T8PerformanceStatisticsProvider
{
    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8OrganizationApi orgApi;
    private final T8OntologyApi ontApi;
    private final T8DataStringApi dsApi;
    private final T8DataRecordApi recApi;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final OntologyProvider ontologyProvider;
    private final T8LRUCachedTerminologyProvider terminologyProvider;
    private final T8LRUCachedDataRecordProvider dataRecordProvider;
    private final T8DataStringDataProvider dataProvider;
    private final T8OrganizationStructure orgStructure;
    private final T8DataStringInstanceProvider dsInstanceProvider;
    private final T8ExpressionEvaluator expressionEvaluator;
    private final T8DataStringFormatProvider formatProvider;
    private Set<String> organizationLanguageIds;
    private T8PerformanceStatistics stats;
    private T8DataStringGeneratorContext generatorContext;

    public static final String PC_DATA_STRING_GENERATION = "DATA_STRING_GENERATION";
    public static final String PC_RECORD_ONTOLOGY_GENERATION = "ONTOLOGY_GENERATION";

    public static final String PE_RECORD_ONTOLOGY_GENERATION = "RECORD_ONTOLOGY_GENERATION";
    public static final String PE_RECORD_DATA_STRING_GENERATION = "RECORD_DATA_STRING_GENERATION";
    public static final String PE_DATA_STRING_INSTANCE_GENERATION = "DATA_STRING_INSTANCE_GENERATION";
    public static final String PE_DATA_STRING_FORMAT_COMPILATION = "DATA_STRING_FORMAT_COMPILATION";
    public static final String PE_DATA_STRING_PERSISTENCE = "DATA_STRING_PERSISTENCE";
    public static final String PE_DATA_STRING_GENERATION = "DATA_STRING_GENERATION";
    public static final String PE_RECORD_TERMINOLOGY_CACHING = "RECORD_TERMINOLOGY_CACHING";
    public static final String PE_RECORD_DEFINITION_UPDATE = "RECORD_DEFINITION_UPDATE";
    public static final String PE_RECORD_DEFINITION_INSERT = "RECORD_DEFINITION_INSERT";
    public static final String PE_RECORD_TERMINOLOGY_SAVE = "RECORD_TERMINOLOGY_SAVE";

    public T8DataStringGenerator(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        this.ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        this.dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
        this.recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
        this.orgStructure = orgApi.getOrganizationStructure();
        this.ontologyProvider = ontApi.getOntologyProvider();
        this.formatProvider = new T8ServerDataStringFormatProvider(dsApi, tx, sessionContext.getRootOrganizationIdentifier(), 500);
        this.expressionEvaluator = new T8ServerExpressionEvaluator(context);
        this.terminologyProvider = new T8LRUCachedTerminologyProvider(new T8OrganizationTerminologyProvider(tx.getApi(T8TerminologyApi.API_IDENTIFIER)), 400);
        this.dataRecordProvider = new T8LRUCachedDataRecordProvider(new T8OrganizationDataRecordProvider(recApi), 20);
        this.dataProvider = new T8DataStringDataProvider(terminologyProvider, dataRecordProvider, orgApi.getDefaultContentLanguageId());
        this.dsInstanceProvider = new T8DataStringInstanceProvider(ontologyProvider, terminologyProvider, dsApi.retrieveDataStringInstanceLinkPairs(null), expressionEvaluator);
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        createGeneratorContext(tx);
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    private void createGeneratorContext(T8DataTransaction tx) throws Exception
    {
        // Construct the generator context.
        generatorContext = new T8DataStringGeneratorContext();
        generatorContext.setDataStringFormatProvider(formatProvider);
        generatorContext.setExpressionEvaluator(expressionEvaluator);
        generatorContext.setOrganizationStructure(orgApi.getOrganizationStructure());
    }

    public T8DataString generateDataString(String recordId, String dsInstanceId) throws Exception
    {
        DataRecord dataRecord;

        dataRecord = dataProvider.getDataFile(recordId);
        if (dataRecord != null)
        {
            Pair<T8DataStringInstanceLink, T8DataStringInstance> dsInstanceLink;

            dsInstanceLink = dsInstanceProvider.getDataStringInstance(dataRecord, dsInstanceId);
            if (dsInstanceLink != null)
            {
                return generateDataString(dataRecord, dsInstanceLink.getValue1(), dsInstanceLink.getValue2());
            }
            else throw new Exception("Data String Instance not found: " + dsInstanceId);
        }
        else throw new Exception("Data Record not found: " + recordId);
    }

    public T8DataString generateDataString(String recordId, String languageId, String typeId) throws Exception
    {
        DataRecord dataRecord;

        dataRecord = dataProvider.getDataFile(recordId);
        if (dataRecord != null)
        {
            Pair<T8DataStringInstanceLink, T8DataStringInstance> dsInstanceLink;

            dsInstanceLink = dsInstanceProvider.getDataStringInstance(dataRecord, languageId, typeId);
            if (dsInstanceLink != null)
            {
                return generateDataString(dataRecord, dsInstanceLink.getValue1(), dsInstanceLink.getValue2());
            }
            else throw new Exception("Data String Instance not found for DR Instance: " + dataRecord.getDataRequirementInstanceID() + " Language: " + languageId + " Type: " + typeId);
        }
        else throw new Exception("Data Record not found: " + recordId);
    }

    public T8DataString generateDataString(DataRecord dataRecord, String dsInstanceId) throws Exception
    {
        Pair<T8DataStringInstanceLink, T8DataStringInstance> dsInstanceLink;

        dsInstanceLink = dsInstanceProvider.getDataStringInstance(dataRecord, dsInstanceId);
        if (dsInstanceLink != null)
        {
            return generateDataString(dataRecord, dsInstanceLink.getValue1(), dsInstanceLink.getValue2());
        }
        else throw new Exception("Data String Instance not found: " + dsInstanceId);
    }

    public void refreshDataStrings(DataRecord record, List<String> dsInstanceIds, List<String> dsTypeIds, List<String> languageIds) throws Exception
    {
        List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> dsInstanceLinks;

        // Now get the description instances to generate.
        dsInstanceLinks = dsInstanceProvider.getDataStringInstances(record);
        if (dsInstanceLinks != null)
        {
            Iterator<Pair<T8DataStringInstanceLink, T8DataStringInstance>> linkIterator;

            // Make sure that we have the correct languages as input.
            if (CollectionUtilities.isNullOrEmpty(languageIds)) languageIds = new ArrayList<>(getOrganizationLanguageIdSet());

            // Set the language caching hint on the data provider.
            dataProvider.setCacheLanguageIDList(languageIds);

            // Remove instances that are not marked for auto-generations.
            linkIterator = dsInstanceLinks.iterator();
            while (linkIterator.hasNext())
            {
                Pair<T8DataStringInstanceLink, T8DataStringInstance> dsInstanceLink;
                T8DataStringInstance dsInstance;

                // Remove all description instances from the list that we are not interested in.
                dsInstanceLink = linkIterator.next();
                dsInstance = dsInstanceLink.getValue2();
                if (!dsInstance.isAutoGenerate()) linkIterator.remove();
                else if ((dsInstanceIds != null) && (!dsInstanceIds.contains(dsInstance.getInstanceID()))) linkIterator.remove();
                else if ((dsTypeIds != null) && (!dsTypeIds.contains(dsInstance.getTypeID()))) linkIterator.remove();
                else if ((languageIds != null) && (!languageIds.contains(dsInstance.getLanguageID()))) linkIterator.remove();
            }

            // Refresh the remaining data string instances (after those not required have been removed).
            for (Pair<T8DataStringInstanceLink, T8DataStringInstance> dsInstanceLink : dsInstanceLinks)
            {
                T8DataStringInstance dsInstance;
                T8DataString dataString;

                // Log statistics.
                stats.logExecutionStart(PE_DATA_STRING_INSTANCE_GENERATION);

                // Now refreshDataStrings the description using the description format.
                dsInstance = dsInstanceLink.getValue2();
                dataString = generateDataString(record, dsInstanceLink.getValue1(), dsInstanceLink.getValue2());

                // Persist the description to the data source.
                stats.logExecutionStart(PE_DATA_STRING_PERSISTENCE);
                dsApi.saveDataString(record.getID(), dsInstance.getInstanceID(), dataString.getString());
                stats.logExecutionEnd(PE_DATA_STRING_PERSISTENCE);

                // Log statistics.
                stats.logExecutionEnd(PE_DATA_STRING_INSTANCE_GENERATION);
            }
        }
    }

    public void refreshDataStrings(List<T8DataStringFilter> renderingFilters) throws Exception
    {
        // Loop through all supplied filters.
        for (T8DataStringFilter renderingFilter : renderingFilters)
        {
            refreshDataStrings(renderingFilter);
        }
    }

    public void refreshDataStrings(String recordId) throws Exception
    {
        T8DataStringFilter filter;

        filter = new T8DataStringFilterBuilder().record(recordId).build();
        refreshDataStrings(filter);
    }

    public void refreshDataStrings(T8DataStringFilter renderingFilter) throws Exception
    {
        // Loop through the Record ID list provider for the filter (this list is not optional).
        for (String recordId : renderingFilter.getRecordIDList())
        {
            DataRecord record;

            // Log statistics.
            stats.logExecutionStart(PE_RECORD_DATA_STRING_GENERATION);

            // Retrieve the next record for which to refreshDataStrings descriptions.
            //record = dataRecordProvider.getDataRecord(recordID, null, null, false, false, false, false, false, false, true);
            record = recApi.retrieveRecord(recordId, null, false, false, false, false, false, false, true);
            if (record != null)
            {
                List<String> instanceIdList;
                List<String> typeIdList;
                List<String> languageIdList;

                // Get the criteria lists that will be used to filter the description instances that we refreshDataStrings.
                instanceIdList = renderingFilter.getInstanceIDList();
                typeIdList = renderingFilter.getTypeIDList();
                languageIdList = renderingFilter.getLanguageIDList();
                if (CollectionUtilities.isNullOrEmpty(languageIdList)) languageIdList = new ArrayList<>(getOrganizationLanguageIdSet());

                // Generate the data strings.
                refreshDataStrings(record, instanceIdList, typeIdList, languageIdList);
            }
            else throw new Exception("Data Record not found: " + recordId);

            // Log statistics.
            stats.logExecutionEnd(PE_RECORD_DATA_STRING_GENERATION);
        }
    }

    private T8DataString generateDataString(DataRecord record, T8DataStringInstanceLink dsInstanceLink, T8DataStringInstance dsInstance) throws Exception
    {
        T8DataStringParticle particle;
        StringBuilder particleString;
        T8DataString dataString;
        DataRecord inputRecord;
        Set<String> orgIdSet;
        String dataKeyId;

        // Create a set to hold all of the target organization id's.
        orgIdSet = new HashSet<>();
        for (String dsOrgId : dsInstance.getOrganizationLinks())
        {
            orgIdSet.addAll(orgStructure.getOrganizationIdSet(OrganizationSetType.LINEAGE, dsOrgId));
        }

        // Create an input record that only contains the data linked to the applicable organizations.
        inputRecord = record.copy(true, true, true, true, true, true, true);
        for (DataRecord subRecord : inputRecord.getDataRecords())
        {
            List<T8OntologyLink> links;
            boolean found;

            found = false;
            links = ontologyProvider.getOntologyLinks(subRecord.getDataRequirementInstanceID());
            for (T8OntologyLink link : links)
            {
                if (orgIdSet.contains(link.getOrganizationID()))
                {
                    found = true;
                    break;
                }
            }

            // If an organization link was not found, remove the record so that it is not available for use during data string generation.
            if (!found)
            {
                subRecord.removeFromReferrerRecords();
            }
        }

        // Evaluate the data key to be used for this dsInstance->record combination.
        dataKeyId = getDataKeyId(dsInstanceLink, record);

        // Render the description using the supplied format.
        stats.logExecutionStart(PE_DATA_STRING_GENERATION);
        terminologyProvider.setLanguage(dsInstance.getLanguageID());
        expressionEvaluator.setLanguage(dsInstance.getLanguageID());
        particle = generatorContext.generateDataString(dsInstance, inputRecord, dataKeyId);
        particleString = particle != null ? particle.generateDataString() : null;
        stats.logExecutionEnd(PE_DATA_STRING_GENERATION);

        // Return the rendered Data String.
        dataString = new T8DataString(dsInstance.getInstanceID(), dsInstance.getTypeID(), dsInstance.getLanguageID(), inputRecord.getID(), particleString != null ? particleString.toString() : null);
        dataString.setFormats(generatorContext.getLastGenerationFormats());
        dataString.setOrganizationLinks(dsInstance.getOrganizationLinkSet());

        // Add the generated data string to the input record.
        record.removeDataString(dataString.getInstanceId());
        record.addDataString(dataString);

        // Return the generated data string as output.
        return dataString;
    }

    public void setCacheLanguageIDList(List<String> languageIdList)
    {
        dataProvider.setCacheLanguageIDList(languageIdList);
    }

    private Set<String> getOrganizationLanguageIdSet() throws Exception
    {
        // If we haven't retrieved the organization's language ID's we do it noe.
        if (organizationLanguageIds == null)
        {
            organizationLanguageIds = orgApi.getLanguageIdSet();
            return organizationLanguageIds;
        }
        else return organizationLanguageIds;
    }

    private String getDataKeyId(T8DataStringInstanceLink dsInstanceLink, DataRecord dataFile)
    {
        String expression;

        // We have the matching link but first make sure the expression is not empty.
        expression = dsInstanceLink.getDataKeyExpression();
        if (!Strings.isNullOrEmpty(expression))
        {
            try
            {
                Object result;

                // Evaluate the DocPath expression and check the result.
                result = expressionEvaluator.evaluateExpression(expression, null, dataFile);
                if (result != null)
                {
                    // Make sure the result is a String.
                    if (result instanceof String)
                    {
                        // Return the result.
                        return (String)result;
                    }
                    else throw new RuntimeException("Data Key Expression '" + dsInstanceLink + "' returned an invalid (non-String) result: " + result + " when executed on data file: " + dataFile);
                }
                else return null;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while evaluating Data String Instance Link data key expression: " + expression, e);
            }
        }
        else return null;
    }
}
