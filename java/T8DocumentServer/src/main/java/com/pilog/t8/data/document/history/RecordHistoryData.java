package com.pilog.t8.data.document.history;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.datarecord.PropertyContent;
import com.pilog.t8.data.document.datarecord.RecordAttribute;
import com.pilog.t8.data.document.datarecord.RecordContent;
import com.pilog.t8.data.document.datarecord.ValueContent;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.value.AttachmentListContent;
import com.pilog.t8.data.document.datarecord.value.BooleanValueContent;
import com.pilog.t8.data.document.datarecord.value.CompositeContent;
import com.pilog.t8.data.document.datarecord.value.ControlledConceptContent;
import com.pilog.t8.data.document.datarecord.value.DateTimeValueContent;
import com.pilog.t8.data.document.datarecord.value.DateValueContent;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceListContent;
import com.pilog.t8.data.document.datarecord.value.FieldContent;
import com.pilog.t8.data.document.datarecord.value.LowerBoundNumberContent;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumberContent;
import com.pilog.t8.data.document.datarecord.value.MeasuredRangeContent;
import com.pilog.t8.data.document.datarecord.value.NumberContent;
import com.pilog.t8.data.document.datarecord.value.QualifierOfMeasureContent;
import com.pilog.t8.data.document.datarecord.value.StringValueContent;
import com.pilog.t8.data.document.datarecord.value.TextValueContent;
import com.pilog.t8.data.document.datarecord.value.TimeValueContent;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasureContent;
import com.pilog.t8.data.document.datarecord.value.UpperBoundNumberContent;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.source.datarecord.T8DataRecordPersistenceHandler.AuditingType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRecordHistoryApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class RecordHistoryData
{
    private final Map<String, T8DataEntity> docData;
    private final Map<String, T8DataEntity> propertyData;
    private final Map<String, T8DataEntity> valueData;
    private final Map<String, T8DataEntity> attachmentData;
    private final Map<String, T8DataEntity> attributeData;
    private final Map<String, T8Timestamp> docInsertTimes;
    private final Map<String, T8Timestamp> docUpdateTimes;

    public RecordHistoryData()
    {
        this.docData = new HashMap<>();
        this.propertyData = new HashMap<>();
        this.valueData = new HashMap<>();
        this.attachmentData = new HashMap<>();
        this.attributeData = new HashMap<>();
        this.docInsertTimes = new HashMap<>();
        this.docUpdateTimes = new HashMap<>();
    }

    public void addDocData(List<T8DataEntity> entities)
    {
        for (T8DataEntity entity : entities)
        {
            T8Timestamp timestamp;
            AuditingType event;
            String recordId;
            String key;

            recordId = (String)entity.getFieldValue(HISTORY_DATA_RECORD_DOC_DEID + EF_RECORD_ID);
            timestamp = (T8Timestamp)entity.getFieldValue(HISTORY_DATA_RECORD_DOC_DEID + EF_TIME);

            key = recordId;
            event = AuditingType.valueOf((String)entity.getFieldValue(HISTORY_DATA_RECORD_DOC_DEID + EF_EVENT));
            switch (event)
            {
                case INSERT:
                    docData.put(key, entity);
                    docInsertTimes.put(recordId, timestamp);
                    break;
                case UPDATE:
                    docData.put(key, entity);
                    docUpdateTimes.put(recordId, timestamp);
                    break;
                case DELETE:
                    docData.remove(key);
                    break;
            }
        }
    }

    public void addPropertyData(List<T8DataEntity> entities)
    {
        for (T8DataEntity entity : entities)
        {
            AuditingType event;
            String key;

            key = (String)entity.getFieldValue(HISTORY_DATA_RECORD_PROPERTY_DEID + EF_RECORD_ID);
            key += (String)entity.getFieldValue(HISTORY_DATA_RECORD_PROPERTY_DEID + EF_PROPERTY_ID);
            event = AuditingType.valueOf((String)entity.getFieldValue(HISTORY_DATA_RECORD_PROPERTY_DEID + EF_EVENT));
            switch (event)
            {
                case INSERT:
                    propertyData.put(key, entity);
                    break;
                case UPDATE:
                    propertyData.put(key, entity);
                    break;
                case DELETE:
                    propertyData.remove(key);
                    break;
            }
        }
    }

    public void addValueData(List<T8DataEntity> entities)
    {
        for (T8DataEntity entity : entities)
        {
            AuditingType event;
            String key;

            key = (String)entity.getFieldValue(HISTORY_DATA_RECORD_VALUE_DEID + EF_RECORD_ID);
            key += (String)entity.getFieldValue(HISTORY_DATA_RECORD_VALUE_DEID + EF_PROPERTY_ID);
            key += (String)entity.getFieldValue(HISTORY_DATA_RECORD_VALUE_DEID + EF_DATA_TYPE_ID);
            key += (Integer)entity.getFieldValue(HISTORY_DATA_RECORD_VALUE_DEID + EF_DATA_TYPE_SEQUENCE);
            key += (Integer)entity.getFieldValue(HISTORY_DATA_RECORD_VALUE_DEID + EF_VALUE_SEQUENCE);
            event = AuditingType.valueOf((String)entity.getFieldValue(HISTORY_DATA_RECORD_VALUE_DEID + EF_EVENT));
            switch (event)
            {
                case INSERT:
                    valueData.put(key, entity);
                    break;
                case UPDATE:
                    valueData.put(key, entity);
                    break;
                case DELETE:
                    valueData.remove(key);
                    break;
            }
        }
    }

    public void addAttributeData(List<T8DataEntity> entities)
    {
        for (T8DataEntity entity : entities)
        {
            AuditingType event;
            String key;

            key = (String)entity.getFieldValue(HISTORY_DATA_RECORD_ATTRIBUTE_DEID + EF_RECORD_ID);
            key += (String)entity.getFieldValue(HISTORY_DATA_RECORD_ATTRIBUTE_DEID + EF_ATTRIBUTE_ID);
            event = AuditingType.valueOf((String)entity.getFieldValue(HISTORY_DATA_RECORD_ATTRIBUTE_DEID + EF_EVENT));
            switch (event)
            {
                case INSERT:
                    attributeData.put(key, entity);
                    break;
                case UPDATE:
                    attributeData.put(key, entity);
                    break;
                case DELETE:
                    attributeData.remove(key);
                    break;
            }
        }
    }

    public void addAttachmentData(List<T8DataEntity> entities)
    {
        for (T8DataEntity entity : entities)
        {
            AuditingType event;
            String key;

            key = (String)entity.getFieldValue(HISTORY_DATA_RECORD_ATTACHMENT_DEID + EF_ATTACHMENT_ID);
            event = AuditingType.valueOf((String)entity.getFieldValue(HISTORY_DATA_RECORD_ATTACHMENT_DEID + EF_EVENT));
            switch (event)
            {
                case INSERT:
                    attachmentData.put(key, entity);
                    break;
                case UPDATE:
                    attachmentData.put(key, entity);
                    break;
                case DELETE:
                    attachmentData.remove(key);
                    break;
            }
        }
    }

    public RecordContent constructFile()
    {
        List<RecordContent> contentRecords;
        RecordContent root;

        // Construct all of the records in the file.
        contentRecords = new ArrayList<>();
        for (T8DataEntity docEntity : docData.values())
        {
            RecordContent contentRecord;

            contentRecord = constructRecord((String)docEntity.getFieldValue(EF_RECORD_ID));
            contentRecords.add(contentRecord);
        }

        // Organize the file structure.
        root = null;
        for (RecordContent childRecord : contentRecords)
        {
            String childRecordId;
            boolean parentFound;

            parentFound = false;
            childRecordId = childRecord.getId();
            for (RecordContent targetRecord : contentRecords)
            {
                if (targetRecord != childRecord)
                {
                    RecordContent parentRecord;

                    parentRecord = targetRecord.getParent(childRecordId);
                    if (parentRecord != null)
                    {
                        parentRecord.addSubRecord(childRecord);
                        parentFound = true;
                        break;
                    }
                }
            }

            // If no parent was found, then we've found the root.
            if (!parentFound)
            {
                if (root == null) root = childRecord;
                else
                {
                    throw new IllegalStateException("Two records without a root encountered: " + childRecord + " and: " + root);
                }
            }
        }

        // All content records are now linked to their parents, so return the final root.
        return root;
    }

    private RecordContent constructRecord(String recordId)
    {
        List<T8DataEntity> docEntities;

        docEntities = T8DataUtilities.getFilteredDataEntities(new ArrayList<>(docData.values()), HISTORY_DATA_RECORD_DOC_DEID + EF_RECORD_ID, recordId);
        if (docEntities.size() == 1)
        {
            List<T8DataEntity> attributeEntities;
            List<T8DataEntity> propertyEntities;
            RecordContent newRecord;
            T8DataEntity docEntity;

            // Get the entity from which to construct the record.
            docEntity = docEntities.get(0);

            // Create the new record content object.
            newRecord = new RecordContent();
            newRecord.setId((String)docEntity.getFieldValue(EF_RECORD_ID));
            newRecord.setDrInstanceId((String)docEntity.getFieldValue(EF_DR_INSTANCE_ID));
            newRecord.setFft((String)docEntity.getFieldValue(EF_FFT));
            newRecord.setInsertedAt(docInsertTimes.get(recordId));
            newRecord.setUpdatedAt(docUpdateTimes.get(recordId));

            // Construct the record attributes.
            attributeEntities = T8DataUtilities.getFilteredDataEntities(new ArrayList<>(attributeData.values()), HISTORY_DATA_RECORD_ATTRIBUTE_DEID + EF_RECORD_ID, recordId);
            for (T8DataEntity attributeEntity : attributeEntities)
            {
                String attributeId;
                String type;
                String value;

                attributeId = (String)attributeEntity.getFieldValue(EF_ATTRIBUTE_ID);
                value = (String)attributeEntity.getFieldValue(EF_VALUE);
                type = (String)attributeEntity.getFieldValue(EF_TYPE);

                newRecord.setAttribute(attributeId, type != null ? RecordAttribute.getValue(type, value) : value);
            }

            // Construct the record properties.
            propertyEntities = T8DataUtilities.getFilteredDataEntities(new ArrayList<>(propertyData.values()), HISTORY_DATA_RECORD_PROPERTY_DEID + EF_RECORD_ID, recordId);
            for (T8DataEntity propertyEntity : propertyEntities)
            {
                PropertyContent propertyContent;

                propertyContent = constructProperty(propertyEntity);
                if (propertyContent != null)
                {
                    newRecord.addProperty(propertyContent);
                }
            }

            // Return the complete record.
            return newRecord;
        }
        else throw new RuntimeException("Unexpected number of document entities for id " + recordId + ": " + docEntities.size());
    }

    private PropertyContent constructProperty(T8DataEntity propertyEntity)
    {
        ValueContent value;
        PropertyContent property;
        String propertyId;
        String recordId;

        recordId = (String)propertyEntity.getFieldValue(EF_RECORD_ID);
        propertyId = (String)propertyEntity.getFieldValue(EF_PROPERTY_ID);
        property = new PropertyContent();
        property.setId(propertyId);
        property.setFft((String)propertyEntity.getFieldValue(EF_FFT));

        value = constructValue(recordId, propertyId, null, null);
        property.setValue(value);

        return property;
    }

    public ValueContent constructValue(String recordId, String propertyId, Integer parentValueSequence, RequirementType specificType)
    {
        HashMap<String, Object> filterValues;
        List<T8DataEntity> valueEntities;

        filterValues = new HashMap<>();
        filterValues.put(HISTORY_DATA_RECORD_VALUE_DEID + EF_RECORD_ID, recordId);
        filterValues.put(HISTORY_DATA_RECORD_VALUE_DEID + EF_PROPERTY_ID, propertyId);
        filterValues.put(HISTORY_DATA_RECORD_VALUE_DEID + EF_PARENT_VALUE_SEQUENCE, parentValueSequence);
        valueEntities = T8DataUtilities.getFilteredDataEntities(new ArrayList<>(valueData.values()), filterValues);
        if (valueEntities.size() > 0)
        {
            T8DataEntity valueEntity;
            RequirementType type;

            valueEntity = valueEntities.get(0);
            type = RequirementType.valueOf((String)valueEntity.getFieldValue(EF_DATA_TYPE_ID));
            if ((specificType == null) || (specificType == type))
            {
                switch (type)
                {
                    case COMPOSITE_TYPE:
                        CompositeContent composite;

                        composite = new CompositeContent();
                        for (T8DataEntity fieldEntity : valueEntities)
                        {
                            composite.addField((FieldContent)constructValue(recordId, propertyId, (Integer)fieldEntity.getFieldValue(EF_VALUE_SEQUENCE), RequirementType.FIELD_TYPE));
                        }
                        return composite;
                    case FIELD_TYPE:
                        FieldContent field;

                        field = new FieldContent((String)valueEntity.getFieldValue(EF_VALUE_CONCEPT_ID));
                        field.setValue(constructValue(recordId, propertyId, (Integer)valueEntity.getFieldValue(EF_VALUE_SEQUENCE), null));
                        return field;
                    case CONTROLLED_CONCEPT:
                        ControlledConceptContent concept;

                        concept = new ControlledConceptContent();
                        concept.setConceptId((String)valueEntity.getFieldValue(EF_VALUE_CONCEPT_ID));
                        return concept;
                    case UNIT_OF_MEASURE:
                        UnitOfMeasureContent uom;

                        uom = new UnitOfMeasureContent();
                        uom.setConceptId((String)valueEntity.getFieldValue(EF_VALUE_CONCEPT_ID));
                        return uom;
                    case QUALIFIER_OF_MEASURE:
                        QualifierOfMeasureContent qom;

                        qom = new QualifierOfMeasureContent();
                        qom.setConceptId((String)valueEntity.getFieldValue(EF_VALUE_CONCEPT_ID));
                        return qom;
                    case DOCUMENT_REFERENCE:
                        DocumentReferenceListContent referenceList;

                        referenceList = new DocumentReferenceListContent();
                        for (T8DataEntity referenceEntity : valueEntities)
                        {
                            referenceList.addReference((String)referenceEntity.getFieldValue(EF_VALUE_CONCEPT_ID));
                        }
                        return referenceList;
                    case ATTACHMENT:
                        AttachmentListContent attachmentList;

                        attachmentList = new AttachmentListContent();
                        for (T8DataEntity attachmentIdEntity : valueEntities)
                        {
                            T8AttachmentDetails attachmentDetails;
                            String attachmentId;

                            attachmentId = (String)attachmentIdEntity.getFieldValue(EF_VALUE_CONCEPT_ID);
                            attachmentList.addAttachment(attachmentId);

                            attachmentDetails = getAttachmentDetails(attachmentId);
                            if (attachmentDetails != null)
                            {
                                attachmentList.addAttachmentDetails(attachmentId, attachmentDetails);
                            }
                        }
                        return attachmentList;
                    case MEASURED_NUMBER:
                        MeasuredNumberContent mnContent;

                        mnContent = new MeasuredNumberContent();
                        mnContent.setNumber((NumberContent)constructValue(recordId, propertyId, (Integer)valueEntity.getFieldValue(EF_VALUE_SEQUENCE), RequirementType.NUMBER));
                        mnContent.setUnitOfMeasure((UnitOfMeasureContent)constructValue(recordId, propertyId, (Integer)valueEntity.getFieldValue(EF_VALUE_SEQUENCE), RequirementType.UNIT_OF_MEASURE));
                        mnContent.setQualifierOfMeasure((QualifierOfMeasureContent)constructValue(recordId, propertyId, (Integer)valueEntity.getFieldValue(EF_VALUE_SEQUENCE), RequirementType.QUALIFIER_OF_MEASURE));
                        return mnContent;
                    case MEASURED_RANGE:
                        MeasuredRangeContent rangeContent;

                        rangeContent = new MeasuredRangeContent();
                        rangeContent.setLowerBoundNumber((LowerBoundNumberContent)constructValue(recordId, propertyId, (Integer)valueEntity.getFieldValue(EF_VALUE_SEQUENCE), RequirementType.LOWER_BOUND_NUMBER));
                        rangeContent.setUpperBoundNumber((UpperBoundNumberContent)constructValue(recordId, propertyId, (Integer)valueEntity.getFieldValue(EF_VALUE_SEQUENCE), RequirementType.UPPER_BOUND_NUMBER));
                        rangeContent.setUnitOfMeasure((UnitOfMeasureContent)constructValue(recordId, propertyId, (Integer)valueEntity.getFieldValue(EF_VALUE_SEQUENCE), RequirementType.UNIT_OF_MEASURE));
                        rangeContent.setQualifierOfMeasure((QualifierOfMeasureContent)constructValue(recordId, propertyId, (Integer)valueEntity.getFieldValue(EF_VALUE_SEQUENCE), RequirementType.QUALIFIER_OF_MEASURE));
                        return rangeContent;
                    case REAL_TYPE:
                        NumberContent real;

                        real = new NumberContent();
                        real.setValue((String)valueEntity.getFieldValue(EF_VALUE));
                        return real;
                    case INTEGER_TYPE:
                        NumberContent integer;

                        integer = new NumberContent();
                        integer.setValue((String)valueEntity.getFieldValue(EF_VALUE));
                        return integer;
                    case NUMBER:
                        NumberContent number;

                        number = new NumberContent();
                        number.setValue((String)valueEntity.getFieldValue(EF_VALUE));
                        return number;
                    case LOWER_BOUND_NUMBER:
                        LowerBoundNumberContent lowerBoundNumber;

                        lowerBoundNumber = new LowerBoundNumberContent();
                        lowerBoundNumber.setValue((String)valueEntity.getFieldValue(EF_VALUE));
                        return lowerBoundNumber;
                    case UPPER_BOUND_NUMBER:
                        UpperBoundNumberContent upperBoundNumber;

                        upperBoundNumber = new UpperBoundNumberContent();
                        upperBoundNumber.setValue((String)valueEntity.getFieldValue(EF_VALUE));
                        return upperBoundNumber;
                    case STRING_TYPE:
                        StringValueContent string;

                        string = new StringValueContent();
                        string.setString((String)valueEntity.getFieldValue(EF_VALUE));
                        return string;
                    case TEXT_TYPE:
                        TextValueContent textContent;

                        textContent = new TextValueContent();
                        textContent.setText((String)valueEntity.getFieldValue(EF_TEXT));
                        return textContent;
                    case DATE_TYPE:
                        DateValueContent date;

                        date = new DateValueContent();
                        date.setDate((String)valueEntity.getFieldValue(EF_VALUE));
                        return date;
                    case DATE_TIME_TYPE:
                        DateTimeValueContent dateTime;

                        dateTime = new DateTimeValueContent();
                        dateTime.setDateTime((String)valueEntity.getFieldValue(EF_VALUE));
                        return dateTime;
                    case TIME_TYPE:
                        TimeValueContent time;

                        time = new TimeValueContent();
                        time.setTime((String)valueEntity.getFieldValue(EF_VALUE));
                        return time;
                    case BOOLEAN_TYPE:
                        BooleanValueContent booleanValue;

                        booleanValue = new BooleanValueContent();
                        booleanValue.setValue((String)valueEntity.getFieldValue(EF_VALUE));
                        return booleanValue;
                    default:
                        throw new IllegalStateException("Unsupported type: " + type);
                }
            }
            else return null;
        }
        else return null;
    }

    private T8AttachmentDetails getAttachmentDetails(String attachmentId)
    {
        List<T8DataEntity> attachmentEntities;

        attachmentEntities = T8DataUtilities.getFilteredDataEntities(new ArrayList<>(attachmentData.values()), HISTORY_DATA_RECORD_ATTACHMENT_DEID + EF_ATTACHMENT_ID, attachmentId);
        if (attachmentEntities.size() == 1)
        {
            T8DataEntity attachmentEntity;
            T8AttachmentDetails fileDetails;

            attachmentEntity = attachmentEntities.get(0);

            fileDetails = new T8AttachmentDetails();
            fileDetails.setContextIid(attachmentId);
            fileDetails.setOriginalFileName((String)attachmentEntity.getFieldValue(EF_FILE_NAME));
            fileDetails.setFilePath(attachmentId); // Attachment stored using id as filename.
            fileDetails.setFileSize((Long)attachmentEntity.getFieldValue(EF_FILE_SIZE));
            fileDetails.setMD5Checksum((String)attachmentEntity.getFieldValue(EF_MD5_CHECKSUM));
            fileDetails.setMediaType((String)attachmentEntity.getFieldValue(EF_MEDIA_TYPE));
            return fileDetails;
        }
        else throw new IllegalStateException("Multiple data entries found for attachment: " + attachmentId);
    }
}
