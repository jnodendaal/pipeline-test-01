package com.pilog.t8.data.org;

import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.document.OntologyProvider;
import java.util.List;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.utilities.collections.ArrayLists;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationOntologyProvider implements OntologyProvider
{
    private final T8OntologyApi ontApi;
    private final T8OntologyStructure ontologyStructure;

    public T8OrganizationOntologyProvider(T8OntologyApi ontApi)
    {
        this.ontApi = ontApi;
        this.ontologyStructure = ontApi.getOntologyStructure();
    }

    @Override
    public T8DataIterator<String> getConceptIDIterator(String ocId, boolean includeDescendantGroups)
    {
        try
        {
            return ontApi.getConceptIdIterator(ocId, includeDescendantGroups);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving organization ontology concept ID iterator.", e);
        }
    }

    @Override
    public List<T8OntologyLink> getOntologyLinks(String conceptId)
    {
        try
        {
            return ontApi.retrieveOntologyLinks(ArrayLists.newArrayList(ontologyStructure.getID()), conceptId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving organization ontology links for concept: " + conceptId, e);
        }
    }

    @Override
    public List<T8OntologyLink> getOntologyLinks(List<String> osIdList, String conceptId)
    {
        try
        {
            return ontApi.retrieveOntologyLinks(osIdList, conceptId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving organization ontology links for concept: " + conceptId, e);
        }
    }

    @Override
    public T8OntologyConcept getOntologyConcept(String conceptId)
    {
        try
        {
            return ontApi.retrieveConcept(conceptId, true, true, true, true, true);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving concept ontology: " + conceptId, e);
        }
    }

    @Override
    public List<T8OntologyCode> getOntologyCodes(String conceptId)
    {
        try
        {
            return ontApi.retrieveConceptCodes(conceptId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving concept ontology codes: " + conceptId, e);
        }
    }
}
