package com.pilog.t8.api;

import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.definition.data.org.T8OrganizationDuplicateResolutionAPIResources;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatch;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class server as the API for all server-side code access to the duplicate
 * resolution process within the context of an organization.
 *
 * @author Bouwer du Preez
 */
public class T8DuplicateResolutionApi implements T8Api, T8PerformanceStatisticsProvider
{
    public static final String API_IDENTIFIER = "@API_DUPLICATE_RESOLUTION";

    private final T8DataTransaction tx;
    private final T8SessionContext sessionContext;
    private final T8ServerContext serverContext;
    private final T8FunctionalityManager functionalityManager;
    private final T8DuplicateResolutionApiDataHandler dataHandler;
    private T8PerformanceStatistics stats;

    public enum ResolvedState {PENDING_RESOLUTION, MASTER, EXCLUDED, SUPERSEDED};

    public T8DuplicateResolutionApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.sessionContext = tx.getContext().getSessionContext();
        this.serverContext = tx.getContext().getServerContext();
        this.functionalityManager = serverContext.getFunctionalityManager();
        this.dataHandler = new T8DuplicateResolutionApiDataHandler(serverContext, sessionContext);
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    /**
     * Creates a resolution group for the specified family and the listed records.
     * @param matchInstanceId The match instance id to specify (if any).
     * @param familyId The id of the family to which the records that will be added to the group belong.
     * @param recordIds The records id's to add to the resolution group.
     * @return The id of the newly created resolution.
     * @throws Exception
     */
    public String createResolution(String matchInstanceId, String familyId, Collection<String> recordIds) throws Exception
    {
        String resolutionId;

        // Validate the creation of the new resolution group.
        dataHandler.doSubmissionValidation(tx, familyId, recordIds);

        // Create the resolution group.
        resolutionId = dataHandler.createResolution(tx, matchInstanceId, familyId, recordIds);
        return resolutionId;
    }

    /**
     * Submits the specified match results for resolution.  A new resolution
     * batch is created, containing all of the records linked by the specified
     * match results.
     * @param matchInstanceId The match instance from which results will be
     * submitted for resolution.
     * @param familyId The family for which results will be submitted for
     * resolution.
     * @param dataObjectId The identifier of the data object that
     * represents the data records being submitted.
     * @param resolutionStateId The state to which submitted objects
     * will be set.
     * @return The ID of the newly generated resolution batch.
     * @throws Exception
     */
    public String submitResolution(String matchInstanceId, String familyId, String dataObjectId, String resolutionStateId) throws Exception
    {
        List<T8RecordMatch> recordMatches;
        Set<String> familyRecordIdSet;
        String resolutionId;

        // Log the start of the operation.
        stats.logExecutionStart(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SUBMIT_RESOLUTION);

        // Retrieve the match results.
        recordMatches = dataHandler.retrieveRecordMatches(tx, matchInstanceId, familyId);

        // Add the id's of all records in the family to a set.
        familyRecordIdSet = new HashSet<>();
        for (T8RecordMatch recordMatch : recordMatches)
        {
            familyRecordIdSet.add(recordMatch.getRecordId1());
            familyRecordIdSet.add(recordMatch.getRecordId2());
        }

        // Validate the creation of the new resolution group.
        dataHandler.doSubmissionValidation(tx, familyId, familyRecordIdSet);

        // Create the resolution group.
        resolutionId = dataHandler.createResolution(tx, matchInstanceId, familyId, familyRecordIdSet);

        // Set the state of the record.
        if (resolutionStateId != null)
        {
            T8DataObjectApi objApi;

            objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
            for (String recordId : familyRecordIdSet)
            {
                objApi.saveState(dataObjectId, recordId, resolutionStateId);
            }
        }

        // Now log the completion of the operation.
        stats.logExecutionEnd(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SUBMIT_RESOLUTION);

        // Return the resolution id.
        return resolutionId;
    }

    /**
     * Cancels and deletes the specified in-progress resolution batch.
     * @param resolutionId The ID of the resolution batch to cancel.
     * @throws Exception
     */
    public void cancelResolution(String resolutionId) throws Exception
    {
        // Log the start of the operation.
        stats.logExecutionStart(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_CANCEL_RESOLUTION);

        // Perform the operation.
        dataHandler.cancelResolution(tx, resolutionId);

        // Now log the completion of the operation.
        stats.logExecutionEnd(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_CANCEL_RESOLUTION);
    }

    /**
     * Finalizes the specified resolution batch and all families it contains.
     * For each family contained by the resolution batch, this record will merge
     * the superseded records in the family with the designated master record.
     * This operation will also mark all of the records as finalized and will
     * records auditing information regarding the user responsible for- and the
     * time of resolution finalization.
     * @param mergerId The identifier of the merger to use.  If null, a
     * default configuration will be used.
     * @param resolutionId The ID of the resolution batch to finalize.
     * @param dataObjectId The identifier of the data object that
     * represents the records being finalized.
     * @param masterStateId The state to which master records will be
     * set.
     * @param nonDuplicateStateId The state to which the non-duplicate
     * records will be set.
     * @param supersededStateId The state to which the superseded records will be set.
     * @throws Exception
     */
    public void finalizeResolution(String mergerId, String resolutionId, String dataObjectId, String masterStateId, String nonDuplicateStateId, String supersededStateId) throws Exception
    {
        // Log the start of the operation.
        stats.logExecutionStart(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_FINALIZE_RESOLUTION);

        // Perform the operation.
        dataHandler.finalizeResolution(tx, mergerId, resolutionId, dataObjectId, masterStateId, nonDuplicateStateId, supersededStateId);

        // Now log the completion of the operation.
        stats.logExecutionEnd(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_FINALIZE_RESOLUTION);
    }

    /**
     * Sets the specified record as the master of the family.  All other records
     * in the family that have not been excluded, will be set to superseded.
     * @param familyId The family in which the record will be set as the master.
     * @param recordId The record to set as the master of the specified family.
     * @throws Exception
     */
    public void setMaster(String familyId, String recordId) throws Exception
    {
        // Log the start of the operation.
        stats.logExecutionStart(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SET_MASTER);

        // Perform the operation.
        dataHandler.setMaster(tx, familyId, recordId);

        // Now log the completion of the operation.
        stats.logExecutionEnd(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SET_MASTER);
    }

    /**
     * Sets the specified records as excluded from the family.  If the record
     * was previously marked as the master of the family, this method will reset
     * all currently superseded records in the family back to the pending state.
     * @param familyId The ID of the family in which the record will be set to
     * excluded.
     * @param recordId The record to set as excluded from the family.
     * @throws Exception
     */
    public void setExcluded(String familyId, String recordId) throws Exception
    {
        // Log the start of the operation.
        stats.logExecutionStart(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SET_EXCLUDED);

        // Perform the operation.
        dataHandler.setExcluded(tx, familyId, recordId);

        // Now log the completion of the operation.
        stats.logExecutionEnd(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SET_EXCLUDED);
    }

    /**
     * Sets the specified record as superseded.  This operation will only
     * succeed if there is currently a master set in the family.
     * @param familyId The ID of the family to which the records belongs that
     * will be set to superseded.  This family must contain a record currently
     * set to master for the operation to succeed.
     * @param recordId The record to set as superseded by the current master of
     * the family.
     * @throws Exception
     */
    public void setSuperseded(String familyId, String recordId) throws Exception
    {
        // Log the start of the operation.
        stats.logExecutionStart(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SET_SUPERSEDED);

        // Perform the operation.
        dataHandler.setSuperseded(tx, familyId, recordId);

        // Now log the completion of the operation.
        stats.logExecutionEnd(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SET_SUPERSEDED);
    }

    /**
     * Resets the specified record back to a pending status indicating that a
     * decision regarding its status in the family has not been made yet.  If
     * the specified record is the current master of the family, all
     * non-excluded records in the family will be set back to the pending state
     * as well.
     * @param familyId The ID of the family to which the record belongs that
     * will be set to a pending state.
     * @param recordId The record to set as pending.
     * @throws Exception
     */
    public void setPending(String familyId, String recordId) throws Exception
    {
        // Log the start of the operation.
        stats.logExecutionStart(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SET_PENDING);

        // Perform the operation.
        dataHandler.setPending(tx, familyId, recordId);

        // Now log the completion of the operation.
        stats.logExecutionEnd(T8OrganizationDuplicateResolutionAPIResources.OPERATION_API_ORG_DUP_RES_SET_PENDING);
    }
}
