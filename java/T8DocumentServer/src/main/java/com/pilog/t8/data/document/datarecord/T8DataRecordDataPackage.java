package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.data.document.datastring.T8DataString;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordDataPackage implements Serializable
{
    private List<DataRecord> recordList;
    private List<T8DataString> descriptionHandles;
    private List<T8DataString> subRecordDescriptionHandles;
    private List<T8FileDetails> attachmentDetails;
    private Set<String> conceptIDs;

    public T8DataRecordDataPackage()
    {
        recordList = new ArrayList<>();
        descriptionHandles = new ArrayList<>();
        subRecordDescriptionHandles = new ArrayList<>();
        attachmentDetails = new ArrayList<>();
        conceptIDs = new HashSet<>();
    }

    public T8DataRecordDataPackage(List<DataRecord> recordList, List<T8DataString> descriptionHandles, List<T8DataString> subRecordDescriptionHandles, List<T8FileDetails> attachmentDetails, Set<String> conceptIDs)
    {
        this.recordList = recordList;
        this.descriptionHandles = descriptionHandles;
        this.subRecordDescriptionHandles = subRecordDescriptionHandles;
        this.attachmentDetails = attachmentDetails;
        this.conceptIDs = conceptIDs;
    }

    public void add(T8DataRecordDataPackage dataPackage)
    {
        this.recordList.addAll(dataPackage.getRecordList());
        this.descriptionHandles.addAll(dataPackage.getDescriptionHandles());
        this.subRecordDescriptionHandles.addAll(dataPackage.getSubRecordDescriptionHandles());
        this.attachmentDetails.addAll(dataPackage.getAttachmentDetails());
        this.conceptIDs.addAll(dataPackage.getConceptIDs());
    }

    public List<DataRecord> getRecordList()
    {
        return recordList;
    }

    public void setRecordList(List<DataRecord> recordList)
    {
        this.recordList = recordList;
    }
    
    public void addRecordList(List<DataRecord> recordList)
    {
        this.recordList.addAll(recordList);
    }
    
    public void addDataRecord(DataRecord dataRecord)
    {
        recordList.add(dataRecord);
    }

    public List<T8DataString> getDescriptionHandles()
    {
        return descriptionHandles;
    }

    public void setDescriptionHandles(List<T8DataString> descriptionHandles)
    {
        this.descriptionHandles = descriptionHandles;
    }
    
    public void addDescriptions(List<T8DataString> descriptionHandles)
    {
        this.descriptionHandles.addAll(descriptionHandles);
    }

    public List<T8DataString> getSubRecordDescriptionHandles()
    {
        return subRecordDescriptionHandles;
    }

    public void setSubRecordDescriptionHandles(List<T8DataString> subRecordDescriptionHandles)
    {
        this.subRecordDescriptionHandles = subRecordDescriptionHandles;
    }
    
    public void addSubRecordDescriptionHandles(List<T8DataString> subRecordDescriptionHandles)
    {
        this.subRecordDescriptionHandles.addAll(subRecordDescriptionHandles);
    }

    public List<T8FileDetails> getAttachmentDetails()
    {
        return attachmentDetails;
    }

    public void setAttachmentDetails(List<T8FileDetails> attachmentDetails)
    {
        this.attachmentDetails = attachmentDetails;
    }
    
    public void addAttachmentDetails(List<T8FileDetails> attachmentDetails)
    {
        this.attachmentDetails.addAll(attachmentDetails);
    }

    public Set<String> getConceptIDs()
    {
        return conceptIDs;
    }

    public void setConceptIDs(Set<String> conceptIDs)
    {
        this.conceptIDs = conceptIDs;
    }
    
    public void addConceptIDs(Set<String> conceptIDs)
    {
        this.conceptIDs.addAll(conceptIDs);
    }
}
