package com.pilog.t8.data.document.datarecord.attachment;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.commons.stream.MonitorInputStream;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.data.source.datarecord.T8AttachmentHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.codecs.Base64Codec;
import com.pilog.t8.utilities.data.resultset.extractor.BigDecimalToLongConverter;
import com.pilog.t8.utilities.data.resultset.extractor.GUIDBytesToStringConverter;
import com.pilog.t8.utilities.data.resultset.extractor.GUIDStringConverter;
import com.pilog.t8.utilities.data.resultset.extractor.ResultSetDataExtractor;
import com.pilog.t8.utilities.strings.Strings;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.tika.Tika;

/**
 * @author Bouwer du Preez
 */
public abstract class T8AbstractAttachmentHandler implements T8AttachmentHandler
{
    private static final ResultSetDataExtractor DATA_EXTRACTOR = new ResultSetDataExtractor(new BigDecimalToLongConverter(), new GUIDBytesToStringConverter(), new GUIDStringConverter());

    private final T8Context context;
    private final T8DataConnection connection;
    private final String attachmentTableName;
    private final T8FileManager fileManager;

    public T8AbstractAttachmentHandler(T8Context context, T8DataConnection dataConnection, T8FileManager fileManager, String attachmentTableName)
    {
        this.context = context;
        this.connection = dataConnection;
        this.attachmentTableName = attachmentTableName;
        this.fileManager = fileManager;
    }

    @Override
    public T8FileDetails getTemporaryAttachmentFileDetails(String attachmentId) throws Exception
    {
        List<T8FileDetails> fileDetailList;

        fileDetailList = fileManager.getFileList(context, attachmentId, null);
        if (fileDetailList.size() > 0)
        {
            return fileDetailList.get(0);
        }
        else throw new Exception("Temporary Attachment File not found: " + attachmentId);
    }

    @Override
    public T8AttachmentDetails retrieveAttachmentDetails(String attachmentId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            List<Map<String, Object>> dataRows;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT ATTACHMENT_ID, RECORD_ID, FILE_CONTEXT_ID, FILE_NAME, FILE_SIZE, MD5_CHECKSUM, MEDIA_TYPE FROM " + attachmentTableName + " WHERE ATTACHMENT_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(attachmentId));
            resultSet = selectStatement.executeQuery();
            dataRows = DATA_EXTRACTOR.getResultSetDataRows(resultSet);

            if (dataRows.size() > 0)
            {
                Map<String, Object> dataRow;
                String fileContextId;
                String filePath;
                String originalFileName;
                String mediaType;
                Long fileSize;
                String md5Checksum;

                dataRow = dataRows.get(0);
                filePath = attachmentId; // Attachment files stored using attachment id as filename;
                fileContextId = (String)dataRow.get("FILE_CONTEXT_ID");
                originalFileName = (String)dataRow.get("FILE_NAME");
                fileSize = (Long)dataRow.get("FILE_SIZE");
                mediaType = (String)dataRow.get("MEDIA_TYPE");
                md5Checksum = (String)dataRow.get("MD5_CHECKSUM");

                return new T8AttachmentDetails(attachmentId, null, fileContextId, filePath, originalFileName, fileSize, md5Checksum, mediaType);
            }
            else return null;
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    @Override
    public Map<String, T8AttachmentDetails> retrieveDataRecordAttachmentDetails(String recordId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            Map<String, T8AttachmentDetails> attachmentDetails;
            List<Map<String, Object>> dataRows;
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT ATTACHMENT_ID, RECORD_ID, FILE_CONTEXT_ID, FILE_NAME, FILE_SIZE, MD5_CHECKSUM, MEDIA_TYPE FROM " + attachmentTableName + " WHERE RECORD_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            resultSet = selectStatement.executeQuery();
            dataRows = DATA_EXTRACTOR.getResultSetDataRows(resultSet);

            attachmentDetails = new HashMap<>();
            for (Map<String, Object> dataRow : dataRows)
            {
                String attachmentId;
                String fileContextId;
                String filePath;
                String originalFileName;
                String mediaType;
                Long fileSize;
                String md5Checksum;

                attachmentId = (String)dataRow.get("ATTACHMENT_ID");
                fileContextId = (String)dataRow.get("FILE_CONTEXT_ID");
                originalFileName = (String)dataRow.get("FILE_NAME");
                mediaType = (String)dataRow.get("MEDIA_TYPE");
                fileSize = (Long)dataRow.get("FILE_SIZE");
                md5Checksum = (String)dataRow.get("MD5_CHECKSUM");
                filePath = attachmentId; // Attachment files stored using attachment id as filename;

                attachmentDetails.put(attachmentId, new T8AttachmentDetails(attachmentId, null, fileContextId, filePath, originalFileName, fileSize, md5Checksum, mediaType));
            }

            return attachmentDetails;
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    @Override
    public void importAttachment(String drInstaceId, String attachmentId, T8AttachmentDetails attachmentDetails) throws Exception
    {
        boolean databaseStorageEnabled;
        String sourceFileContextInstanceID;
        String sourceFileContextID;
        String destinationFileContextID;
        boolean dedicatedFileContext = false;

        // Get the storage parameters.
        databaseStorageEnabled = isDatabaseStorageEnabled(drInstaceId);
        destinationFileContextID = getFileContextID(drInstaceId);
        sourceFileContextID = attachmentDetails.getContextId();
        sourceFileContextInstanceID = attachmentDetails.getContextIid();

        // If no storage option is specified for the attachment, throw an exception.
        if ((!databaseStorageEnabled) && (destinationFileContextID == null))
        {
            throw new Exception("No storage configuration found for attachment '" + attachmentId + "' in DR Instance: " + drInstaceId);
        }
        else
        {
            InputStream inputStream = null;

            try
            {
                // Open the file context if it is not open already.
                if (!fileManager.fileContextExists(context, sourceFileContextInstanceID))
                {
                    fileManager.openFileContext(context, sourceFileContextInstanceID, sourceFileContextID, new T8Minute(10));
                    dedicatedFileContext = true;
                }
                else dedicatedFileContext = false;

                // Open an intput stream to the attachment file.
                inputStream = new BufferedInputStream(fileManager.getFileInputStream(context, sourceFileContextInstanceID, attachmentDetails.getFilePath()));

                // Import the attachment file using the input stream.
                importAttachment(attachmentId, destinationFileContextID, databaseStorageEnabled, inputStream);
            }
            finally
            {
                // Make sure the input stream is closed closed.
                if (inputStream != null) inputStream.close();

                // Make sure the file context is closed.
                if ((dedicatedFileContext) && (fileManager.fileContextExists(context, sourceFileContextInstanceID))) fileManager.closeFileContext(context, sourceFileContextInstanceID);
            }
        }
    }

    private void importAttachment(String attachmentID, String fileContextID, boolean databaseStorageEnabled, InputStream inputStream) throws Exception
    {
        OutputStream dbOutputStream = null;
        OutputStream fileOutputStream = null;
        PreparedStatement preparedStatement = null;
        String fileContextInstanceID = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;
            MessageDigest digest;
            Blob blob = null;

            // Create a new message digest for computing MD5 checksum.
            digest = MessageDigest.getInstance("MD5");

            // Prepare a query statement.
            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            preparedStatement = connection.prepareStatement("SELECT FILE_DATA, MD5_CHECKSUM, MEDIA_TYPE, FILE_NAME FROM " + attachmentTableName + " WHERE ATTACHMENT_ID = " + guidParameter, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            preparedStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(attachmentID));

            // Execute the query and fetch the result set that will be updated with attachment specifics.
            resultSet = preparedStatement.executeQuery();

            // Make sure that we found the attachment data row to be updated.
            if (!resultSet.next()) throw new SQLException("Could not retrieve data record attachment entity to update: " + attachmentID);

            // If the attachment needs to be stored in the database, open an output stream to the DB blob.
            if (databaseStorageEnabled)
            {
                blob = (java.sql.Blob)resultSet.getBlob("FILE_DATA");
                dbOutputStream = new BufferedOutputStream(blob.setBinaryStream(1), 1024 * 1024);
            }

            // If the attachment needs to be stored in a file context, open an output stream to the file destination.
            if (fileContextID != null)
            {
                // Open the file context and write the file to it.
                fileContextInstanceID = T8IdentifierUtilities.createNewGUID();
                fileManager.openFileContext(context, fileContextInstanceID, fileContextID, new T8Minute(10));

                // Get the output stream.
                fileOutputStream = new BufferedOutputStream(fileManager.getFileOutputStream(context, fileContextInstanceID, attachmentID, false), 1024 * 1024);
            }

            // Before we start reading the input stream, we want to use it to
            // try and determine the content type. This process reads a little
            // of the input stream and then resets it to where it was before it
            // started.
            resultSet.updateString("MEDIA_TYPE", determineMediaType(inputStream, resultSet.getString("FILE_NAME")));

            // Copy data from the input stream to the applicable output streams.
            if ((dbOutputStream != null) || (fileOutputStream != null))
            {
                int nextByte;

                while ((nextByte = inputStream.read()) != -1)
                {
                    // Update the MD5 digest.
                    digest.update((byte)nextByte);

                    // Write the next byte to the output streams.
                    if (dbOutputStream != null) dbOutputStream.write(nextByte);
                    if (fileOutputStream != null) fileOutputStream.write(nextByte);
                }

                // Flush the output streams.
                if (fileOutputStream != null) fileOutputStream.flush();
                if (dbOutputStream != null) dbOutputStream.flush();
            }

            // Compute the MD5 Hash and set it on the data row.
            resultSet.updateString("MD5_CHECKSUM",  new String(Base64Codec.encode(digest.digest())));

            // Update the blob data if database storage is used.
            if (blob != null)
            {
                // Update blob data.
                resultSet.updateBlob("FILE_DATA", blob);
            }

            // Update the data row.
            resultSet.updateRow();
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (preparedStatement != null) preparedStatement.close();
            if (dbOutputStream != null) dbOutputStream.close();
            if (fileOutputStream != null) fileOutputStream.close();
            if ((fileContextInstanceID != null) && (fileManager.fileContextExists(context, fileContextInstanceID))) fileManager.closeFileContext(context, fileContextInstanceID);
        }
    }

    /**
     * Determines the media type of the file data contained within the specified
     * {@code InputStream}. The input stream should not be at the end and should
     * preferably support {@link InputStream#markSupported()}.<br/>
     * <br/>
     * A small part of the stream is read and then reset to where it was when
     * this method received the stream.<br/>
     * <br/>
     * The {@code String} file name specified is a secondary detection method
     * for the media type.
     *
     * @param inputStream The {@code InputStream} from which the media type will
     *      be detected
     * @param fileName The {@code String} file name which might be considered
     *      for the media type detection
     *
     * @return The {@code String} media type for the file represented by the
     *      specified input stream. {@code null} if the media type could not be
     *      detected
     *
     * @throws IOException If there is an error while determining the media type
     */
    private String determineMediaType(InputStream inputStream, String fileName) throws IOException
    {
        return new Tika().detect(inputStream, fileName);
    }

    @Override
    public void deleteAttachment(String drInstanceID, String attachmentID) throws Exception
    {
        String fileContextID;

        // Get the file context where the attachment is stored and if found, delete the attachement.
        fileContextID = getFileContextID(drInstanceID);
        if (fileContextID != null)
        {
            String instanceContextID;

            // Create a new context instance ID.
            instanceContextID = T8IdentifierUtilities.createNewGUID();

            try
            {
                // If the attachment file exists, delete it.
                fileManager.openFileContext(context, instanceContextID, fileContextID, null);
                if (fileManager.fileExists(context, instanceContextID, attachmentID))
                {
                    fileManager.deleteFile(context, instanceContextID, attachmentID);
                }
            }
            finally
            {
                if (fileManager.fileContextExists(context, instanceContextID)) fileManager.closeFileContext(context, instanceContextID);
            }
        }
    }

    @Override
    public T8AttachmentValidationResult validateAttachment(String attachmentId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8AttachmentValidationResult result;
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            // Create the new result object.
            result = new T8AttachmentValidationResult(attachmentId);

            // Get the attachment details.
            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT RECORD_ID, ATTACHMENT_ID, FILE_CONTEXT_ID, FILE_NAME, FILE_SIZE, FILE_DATA, MD5_CHECKSUM, MEDIA_TYPE FROM " + attachmentTableName + " WHERE ATTACHMENT_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(attachmentId));
            resultSet = selectStatement.executeQuery();
            if (resultSet.next())
            {
                String fileContextId;
                String fileName;
                String mediaType;
                Long fileSize;
                String md5Checksum;

                // Get the data from the selected row.
                fileContextId = resultSet.getString("FILE_CONTEXT_ID");
                fileName = resultSet.getString("FILE_NAME");
                mediaType = resultSet.getString("MEDIA_TYPE");
                fileSize = resultSet.getLong("FILE_SIZE");
                md5Checksum = resultSet.getString("MD5_CHECKSUM");

                // Set the file details on the result object.
                result.setAttachmentDetails(new T8FileDetails(attachmentId, fileContextId, fileName, fileSize, false, md5Checksum, mediaType));

                // If the attachment is stored in a file, do the export from the file.
                if (fileContextId != null)
                {
                    InputStream inputStream = null;
                    String exportSourceContextIID;

                    // Generate a source context instance ID.
                    exportSourceContextIID = T8IdentifierUtilities.createNewGUID();

                    try
                    {
                        long actualFileSize;

                        // Open the file context.
                        fileManager.openFileContext(context, exportSourceContextIID, fileContextId, new T8Minute(2));

                        // Check that the file exists.
                        if (fileManager.fileExists(context, exportSourceContextIID, attachmentId))
                        {
                            // Get the input stream from attachment storage file.
                            inputStream = new BufferedInputStream(fileManager.getFileInputStream(context, exportSourceContextIID, attachmentId));

                            // Check the byte length of the file contents.
                            actualFileSize = 0;
                            while (inputStream.read() != -1)
                            {
                                actualFileSize++;
                            }

                            // Compare the actual file size with the recorded file size.
                            if (actualFileSize != fileSize)
                            {
                                result.addErrors(T8AttachmentValidationResult.AttachmentValidationError.FILE_SIZE_MISMATCH);
                            }
                        }
                        else
                        {
                            // File does not exist, so no input stream.
                            inputStream = null;
                            result.addErrors(T8AttachmentValidationResult.AttachmentValidationError.MISSING_FILE);
                        }

                        // Return the result.
                        return result;
                    }
                    finally
                    {
                        // Make sure to always close any open streams.
                        if (inputStream != null) inputStream.close();

                        // Close the context from which the file was read.
                        if (fileManager.fileContextExists(context, fileContextId)) fileManager.closeFileContext(context, fileContextId);
                    }
                }
                else // No file storage used, so export from the database.
                {
                    InputStream inputStream = null;

                    try
                    {
                        long actualFileSize;
                        java.sql.Blob blob;

                        // Get the input stream from the selected result set row.
                        blob = (java.sql.Blob)resultSet.getBlob("FILE_DATA");
                        inputStream = blob.getBinaryStream();

                        // Check the byte length of the BLOB contents.
                        actualFileSize = 0;
                        while (inputStream.read() != -1)
                        {
                            actualFileSize++;
                        }

                        // Compare the actual file size with the recorded file size.
                        if (actualFileSize != fileSize)
                        {
                            result.addErrors(T8AttachmentValidationResult.AttachmentValidationError.FILE_SIZE_MISMATCH);
                        }

                        // Return the result.
                        return result;
                    }
                    finally
                    {
                        // Make sure to always close any open streams.
                        if (inputStream != null) inputStream.close();
                    }
                }
            }
            else
            {
                result.addErrors(T8AttachmentValidationResult.AttachmentValidationError.ATTACHMENT_NOT_FOUND);
                return result;
            }
        }
        finally
        {
            // Make sure to always close the resultset and statement if it was used.
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    @Override
    public T8AttachmentDetails exportAttachment(String attachmentId, String exportContextIid, String exportContextId, String exportFolderPath, String exportFileName) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT RECORD_ID, ATTACHMENT_ID, FILE_CONTEXT_ID, FILE_NAME, FILE_SIZE, FILE_DATA, MD5_CHECKSUM, MEDIA_TYPE FROM " + attachmentTableName + " WHERE ATTACHMENT_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(attachmentId));
            resultSet = selectStatement.executeQuery();
            if (resultSet.next())
            {
                String fileContextID;
                String originalFileName;
                String mediaType;
                Long fileSize;
                String md5Checksum;
                String completeFilePath;

                // Get the data from the selected row.
                fileContextID = resultSet.getString("FILE_CONTEXT_ID");
                originalFileName = resultSet.getString("FILE_NAME");
                mediaType = resultSet.getString("MEDIA_TYPE");
                fileSize = resultSet.getLong("FILE_SIZE");
                md5Checksum = resultSet.getString("MD5_CHECKSUM");

                // Create the complete filepath to use when exporting the attachment.
                completeFilePath = "";
                if (!Strings.isNullOrEmpty(exportFolderPath)) completeFilePath += exportFolderPath + "/";
                if (!Strings.isNullOrEmpty(exportFileName)) completeFilePath += exportFileName;
                else completeFilePath += originalFileName;

                // If the attachment is stored in a file, do the export from the file.
                if (fileContextID != null)
                {
                    OutputStream outputStream = null;
                    InputStream inputStream = null;
                    String exportSourceContextIID;

                    // Generate a source context instance ID.
                    exportSourceContextIID = T8IdentifierUtilities.createNewGUID();

                    try
                    {
                        int nextByte;

                        // Get the input stream from attachment storage file.
                        fileManager.openFileContext(context, exportSourceContextIID, fileContextID, new T8Minute(2));
                        inputStream = new BufferedInputStream(fileManager.getFileInputStream(context, exportSourceContextIID, attachmentId));

                        // Create the export file context.
                        fileManager.openFileContext(context, exportContextIid, exportContextId, new T8Minute(10));

                        // Stream the BLOB contents to the export file, put the output stream into a buffered stream with a large buffer so we can reduce IO calls
                        outputStream = new BufferedOutputStream(fileManager.getFileOutputStream(context, exportContextIid, completeFilePath, false), 1024 * 1024);
                        while ((nextByte = inputStream.read()) != -1)
                        {
                            outputStream.write(nextByte);
                        }

                        // Flush the output stream.
                        outputStream.flush();

                        // Return the details of the exported file.
                        return new T8AttachmentDetails(attachmentId, exportContextIid, exportContextId, completeFilePath, originalFileName, fileSize, md5Checksum, mediaType);
                    }
                    finally
                    {
                        // Make sure to always close any open streams.
                        if (inputStream != null) inputStream.close();
                        if (outputStream != null) outputStream.close();

                        // Close the context from which the file was exported.
                        if (fileManager.fileContextExists(context, fileContextID)) fileManager.closeFileContext(context, fileContextID);
                    }
                }
                else // No file storage used, so export from the database.
                {
                    OutputStream outputStream = null;
                    InputStream inputStream = null;

                    try
                    {
                        int nextByte;
                        java.sql.Blob blob;

                        // Get the input stream from the selected result set row.
                        blob = (java.sql.Blob)resultSet.getBlob("FILE_DATA");
                        inputStream = blob.getBinaryStream();

                        // Create the export file context.
                        fileManager.openFileContext(context, exportContextIid, exportContextId, new T8Minute(10));

                        // Stream the BLOB contents to the export file, put the output stream into a buffered stream with a large buffer so we can reduce IO calls
                        outputStream = new BufferedOutputStream(fileManager.getFileOutputStream(context, exportContextIid, completeFilePath, false), 1024 * 1024);
                        while ((nextByte = inputStream.read()) != -1)
                        {
                            outputStream.write(nextByte);
                        }

                        // Flush the output stream.
                        outputStream.flush();

                        // Return the details of the exported file.
                        return new T8AttachmentDetails(attachmentId, exportContextIid, exportContextId, completeFilePath, originalFileName, fileSize, md5Checksum, mediaType);
                    }
                    finally
                    {
                        // Make sure to always close any open streams.
                        if (inputStream != null) inputStream.close();
                        if (outputStream != null) outputStream.close();
                    }
                }
            }
            else throw new Exception("Attachment not found for export: " + attachmentId);
        }
        finally
        {
            // Make sure to always close the resultset and statement if it was used.
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    @Override
    public InputStream getFileInputStream(String attachmentID) throws Exception
    {
        final PreparedStatement selectStatement;
        final ResultSet resultSet;
        T8DatabaseAdaptor dbAdaptor;
        String guidParameter;

        dbAdaptor = connection.getDatabaseAdaptor();
        guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
        selectStatement = connection.prepareStatement("SELECT FILE_CONTEXT_ID, FILE_DATA FROM " + attachmentTableName + " WHERE ATTACHMENT_ID = " + guidParameter);
        selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(attachmentID));
        resultSet = selectStatement.executeQuery();
        if (resultSet.next())
        {
            final String fileContextID;

            // Get the data from the selected row.
            fileContextID = resultSet.getString("FILE_CONTEXT_ID");

            // If the attachment is stored in a file, do the export from the file.
            if (fileContextID != null)
            {
                InputStream inputStream;
                MonitorInputStream monitorInputStream;
                final String exportSourceContextIID;

                // Generate a source context instance ID.
                exportSourceContextIID = T8IdentifierUtilities.createNewGUID();

                try
                {
                    // Get the input stream from attachment storage file.
                    fileManager.openFileContext(context, exportSourceContextIID, fileContextID, new T8Minute(2));
                    inputStream = new BufferedInputStream(fileManager.getFileInputStream(context, exportSourceContextIID, attachmentID));
                    monitorInputStream = new MonitorInputStream(inputStream);

                    monitorInputStream.addInputStreamListener(new MonitorInputStream.InputStreamListener()
                    {

                        @Override
                        public void streamClosed()
                        {
                            try
                            {
                                // Close the context from which the file was exported.
                                if (fileManager.fileContextExists(context, exportSourceContextIID)) fileManager.closeFileContext(context, exportSourceContextIID);
                            }
                            catch (Exception ex)
                            {
                                T8Log.log("Failed to close file context " + fileContextID, ex);
                            }
                        }
                    });
                }
                catch(Exception ex)
                {
                    if(fileManager.fileContextExists(context, exportSourceContextIID)) fileManager.closeFileContext(context, exportSourceContextIID);

                    throw ex;
                }

                return monitorInputStream;
            }
            else // No file storage used, so export from the database.
            {
                InputStream inputStream = null;
                MonitorInputStream monitorInputStream;
                java.sql.Blob blob;

                try
                {
                    // Get the input stream from the selected result set row.
                    blob = (java.sql.Blob)resultSet.getBlob("FILE_DATA");
                    inputStream = blob.getBinaryStream();

                    monitorInputStream = new MonitorInputStream(inputStream);
                    monitorInputStream.addInputStreamListener(new MonitorInputStream.InputStreamListener()
                    {

                        @Override
                        public void streamClosed()
                        {
                            try
                            {
                                resultSet.close();
                                selectStatement.close();
                            }
                            catch (SQLException ex)
                            {
                                T8Log.log("Failed to close result set", ex);
                            }

                        }
                    });
                }
                catch(Exception ex)
                {
                    if(inputStream != null) inputStream.close();

                    resultSet.close();
                    selectStatement.close();
                    throw ex;
                }

                return monitorInputStream;
            }
        }
        else throw new Exception("Attachment not found for export: " + attachmentID);

    }


}

