package com.pilog.t8.api;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyDataHandler;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.data.org.T8OrganizationDataHandler;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.data.org.T8OrganizationOntologyProvider;
import com.pilog.t8.definition.data.ontology.T8OntologyStructureCacheDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.Collection;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.ontology.T8ConceptAlteration;
import com.pilog.t8.data.ontology.T8ConceptPair;
import com.pilog.t8.data.ontology.T8OntologyStructureProvider;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyApi implements T8Api, T8PerformanceStatisticsProvider, T8OntologyStructureProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8OntologyApi.class);
    public static final String API_IDENTIFIER = "@API_ONTOLOGY";

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8OrganizationApi orgApi;
    private final T8OntologyDataHandler dataHandler;
    private final T8OrganizationDataHandler organizationDataHandler;
    private final T8OrganizationStructure organizationStructure;
    private T8OntologyStructure ontologyStructure;
    private T8PerformanceStatistics stats;

    public T8OntologyApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.serverContext = tx.getContext().getServerContext();
        this.orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        this.dataHandler = new T8OntologyDataHandler(context);
        this.organizationDataHandler = new T8OrganizationDataHandler(serverContext);
        this.organizationStructure = orgApi.getOrganizationStructure();
        this.ontologyStructure = (T8OntologyStructure)serverContext.getDataManager().getCachedData(T8OntologyStructureCacheDefinition.DATA_IDENTIFIER, HashMaps.createSingular(T8OntologyStructureCacheDefinition.DATA_PARAMETER_ODS_ID, organizationStructure.getOntologyStructureID()));
        this.stats = tx.getPerformanceStatistics(); // Default behviour of all API's is to use the performance statistics object of its parent transaction.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public OntologyProvider getOntologyProvider()
    {
        return new T8OrganizationOntologyProvider(this);
    }

    public T8OrganizationOntologyFactory getOntologyFactory()
    {
        T8OrganizationOntologyFactory factory;

        // Construct the factory and configure the default settings.
        factory = new T8OrganizationOntologyFactory(context);
        factory.setLanguageID(orgApi.getDefaultContentLanguageId());
        factory.setDataStructureID(ontologyStructure.getID());
        factory.setConceptORGID(organizationStructure.getRootOrganizationID());
        factory.setTermORGID(organizationStructure.getRootOrganizationID());
        factory.setAbbreviationORGID(organizationStructure.getRootOrganizationID());
        factory.setDefinitionORGID(organizationStructure.getRootOrganizationID());
        factory.setCodeORGID(organizationStructure.getRootOrganizationID());
        factory.setTerminologyORGID(organizationStructure.getRootOrganizationID());

        // Return the factory.
        return factory;
    }

    @Override
    public T8OntologyStructure getOntologyStructure()
    {
        return ontologyStructure;
    }

    public T8OntologyStructure recacheOntologyStructure() throws Exception
    {
        T8DataManager dataManager;

        dataManager = serverContext.getDataManager();
        this.ontologyStructure = (T8OntologyStructure)dataManager.recacheData(T8OntologyStructureCacheDefinition.DATA_IDENTIFIER, HashMaps.createSingular(T8OntologyStructureCacheDefinition.DATA_PARAMETER_ODS_ID, organizationStructure.getOntologyStructureID()));
        return ontologyStructure;
    }

    public void refreshOntologyStructureNestedIndices(List<String> osIDList) throws Exception
    {
        Map<String, T8OntologyStructure> ontologyStructures;

        ontologyStructures = this.organizationDataHandler.retrieveOntologyStructures(tx, osIDList);
        for (String osID : ontologyStructures.keySet())
        {
            T8OntologyStructure structure;

            structure = ontologyStructures.get(osID);
            if (structure != null)
            {
                List<String> nestedList;
                List<String> classIDList;

                nestedList = structure.getNestedIDList();
                classIDList = structure.getClassIDList();
                for (String classID : classIDList)
                {
                    this.dataHandler.updateOntologyClassIndices(this.tx, osID, classID, nestedList.indexOf(classID), nestedList.lastIndexOf(classID));
                }
            }
        }
    }

    public void saveOntologyClass(String parentOcId, String ocId) throws Exception
    {
        String osId;

        // Log the start of the operation.
        stats.logExecutionStart("SAVE_ONTOLOGY_CLASS");

        // Validate the input concept.
        osId = ontologyStructure.getID();

        // Now save the concept to the ontology data structure.
        dataHandler.saveOntologyClass(tx, osId, parentOcId, ocId);

        // Now log the completion of the operation.
        stats.logExecutionEnd("SAVE_ONTOLOGY_CLASS");
    }

    public void saveOntologyClass(String parentOcId, T8OntologyConcept ontologyClassConcept, boolean saveTerminology) throws Exception
    {
        String rootOrgId;
        String odId;
        String ocId;

        // Log the start of the operation.
        stats.logExecutionStart("SAVE_ONTOLOGY_CLASS");

        // Validate the input concept.
        rootOrgId = context.getRootOrganizationId();
        odId = ontologyStructure.getID();
        ontologyClassConcept.clearOntologyLinks();
        ontologyClassConcept.addOntologyLink(new T8OntologyLink(rootOrgId, odId, T8PrimaryOntologyDataType.ONTOLOGY_DATA_TYPES.getDataTypeID(), ontologyClassConcept.getID()));

        // First save the concept.
        ocId = ontologyClassConcept.getID();
        saveConcept(ontologyClassConcept, false, true, true, true, true, saveTerminology);

        // Now save the concept to the ontology data structure.
        dataHandler.saveOntologyClass(tx, odId, parentOcId, ocId);

        // Now log the completion of the operation.
        stats.logExecutionEnd("SAVE_ONTOLOGY_CLASS");
    }

    public void insertOntologyClass(String parentOcId, T8OntologyConcept ontologyClassConcept, boolean insertTerminology) throws Exception
    {
        String rootOrgId;
        String osId;

        // Log the start of the operation.
        stats.logExecutionStart("INSERT_ONTOLOGY_CLASS");

        // Validate the input concept.
        rootOrgId = context.getRootOrganizationId();
        osId = ontologyStructure.getID();
        ontologyClassConcept.clearOntologyLinks();
        ontologyClassConcept.addOntologyLink(new T8OntologyLink(rootOrgId, osId, T8PrimaryOntologyDataType.ONTOLOGY_DATA_TYPES.getDataTypeID(), ontologyClassConcept.getID()));

        // First insert the concept.
        insertConcept(ontologyClassConcept, true, true, true, true, insertTerminology);

        // Now add the new concept to the ontology data structure.
        dataHandler.insertOntologyClass(tx, ontologyStructure.getID(), parentOcId, ontologyClassConcept.getID());
        stats.logExecutionEnd("INSERT_ONTOLOGY_CLASS");
    }

    public boolean deleteOntologyClass(String ocId, boolean deleteOntology) throws Exception
    {
        stats.logExecutionStart("DELETE_ONTOLOGY_CLASS");

        // Make sure we never delete one of the primary data types.
        if (T8PrimaryOntologyDataType.getDataType(ocId) != null) throw new Exception("Cannot delete primary data type: " + ocId);

        // First remove the data type from the organization data structure.
        if (dataHandler.deleteOntologyClass(tx, ontologyStructure.getID(), ocId))
        {
            // Delete the ontology if required.
            if (deleteOntology)
            {
                deleteConcept(ocId);
            }

            // Return the result.
            stats.logExecutionEnd("DELETE_ONTOLOGY_CLASS");
            return true;
        }
        else
        {
            stats.logExecutionEnd("DELETE_ONTOLOGY_CLASS");
            return false;
        }
    }

    public boolean moveOntologyClass(String newParentOcId, String ocId) throws Exception
    {
        boolean result;

        stats.logExecutionStart("MOVE_ONTOLOGY_CLASS");
        result = dataHandler.moveDataTypeToNewParent(tx, ontologyStructure.getID(), ocId, newParentOcId);
        stats.logExecutionEnd("MOVE_ONTOLOGY_CLASS");
        return result;
    }

    public T8OntologyConcept retrieveConcept(String conceptId) throws Exception
    {
        return retrieveConcept(conceptId, true, true, true, true, true);
    }

    public T8OntologyConcept retrieveConcept(String conceptId, boolean includeOntologyLinks, boolean includeTerms, boolean includeAbbreviations, boolean includeDefinitions, boolean includeCodes) throws Exception
    {
        List<T8OntologyConcept> conceptList;

        stats.logExecutionStart("retrieveConcept");
        conceptList = retrieveConcepts(ArrayLists.typeSafeList(conceptId), includeOntologyLinks, includeTerms, includeAbbreviations, includeDefinitions, includeCodes);
        stats.logExecutionEnd("retrieveConcept");
        return conceptList.size() > 0 ? conceptList.get(0) : null;
    }

    public List<T8OntologyConcept> retrieveConcepts(Collection<String> conceptIds, boolean includeOntologyLinks, boolean includeTerms, boolean includeAbbreviations, boolean includeDefinitions, boolean includeCodes) throws Exception
    {
        List<T8OntologyConcept> conceptList;

        // At the moment we are ignoring the orgIDList until the proper API data handlers have been put in place.
        stats.logExecutionStart("retrieveConcepts");

        // Retrieve the concepts.
        stats.logExecutionStart("SELECT_ONTOLOGY_CONCEPT");
        conceptList = dataHandler.retrieveConcepts(tx, conceptIds, false, false, false, false); // Don't retrieve the terms/definitions/codes here, they will be added later.
        stats.logExecutionEnd("SELECT_ONTOLOGY_CONCEPT");

        // Add all organization links if required.
        if (includeOntologyLinks)
        {
            List<T8OntologyLink> links;

            links = retrieveOntologyLinks(null, conceptIds);
            for (T8OntologyConcept concept : conceptList)
            {
                String conceptId;

                conceptId = concept.getID();
                for (T8OntologyLink link : links)
                {
                    if (link.getConceptID().equals(conceptId))
                    {
                        concept.addOntologyLink(link);
                    }
                }
            }
        }

        // Add the concept terms if required.
        if (includeTerms)
        {
            List<T8OntologyTerm> terms;

            terms = retrieveConceptTerms(conceptIds, includeAbbreviations);
            for (T8OntologyConcept concept : conceptList)
            {
                String conceptId;

                conceptId = concept.getID();
                for (T8OntologyTerm term : terms)
                {
                    if (term.getConceptID().equals(conceptId))
                    {
                        concept.addTerm(term);
                    }
                }
            }
        }

        // Add the concept definitions if required.
        if (includeDefinitions)
        {
            List<T8OntologyDefinition> definitions;

            definitions = retrieveConceptDefinitions(conceptIds);
            for (T8OntologyConcept concept : conceptList)
            {
                String conceptId;

                conceptId = concept.getID();
                for (T8OntologyDefinition definition : definitions)
                {
                    if (definition.getConceptID().equals(conceptId))
                    {
                        concept.addDefinition(definition);
                    }
                }
            }
        }

        // Add the concept codes if required.
        if (includeCodes)
        {
            List<T8OntologyCode> codes;

            codes = retrieveConceptCodes(conceptIds);
            for (T8OntologyConcept concept : conceptList)
            {
                String conceptId;

                conceptId = concept.getID();
                for (T8OntologyCode code : codes)
                {
                    if (code.getConceptID().equals(conceptId))
                    {
                        concept.addCode(code);
                    }
                }
            }
        }

        stats.logExecutionEnd("retrieveConcepts");
        return conceptList;
    }

    public T8OntologyTerm retrieveTerm(String termId, boolean includeAbbreviations) throws Exception
    {
        List<T8OntologyTerm> terms;

        terms = retrieveTerms(ArrayLists.typeSafeList(termId), includeAbbreviations);
        return terms.size() > 0 ? terms.get(0) : null;
    }

    public List<T8OntologyTerm> retrieveTerms(List<String> termIdList, boolean includeAbbreviations) throws Exception
    {
        List<T8OntologyTerm> termList;
        List<T8OntologyTerm> orgTermList;

        stats.logExecutionStart("retrieveTerms");

        // At the moment we are ignoring the orgIDList until the proper API data handlers have been put in place.
        orgTermList = new ArrayList<>();
        termList = dataHandler.retrieveTerms(tx, termIdList, false);
        for (T8OntologyTerm term : termList)
        {
            // Add the term abbreviations if required.
            if (includeAbbreviations)
            {
                term.addAbbreviations(retrieveTermAbbreviations(term.getID()));
            }

            orgTermList.add(term);
        }

        stats.logExecutionEnd("retrieveTerms");
        return orgTermList;
    }

    public List<T8OntologyDefinition> retrieveDefinitions(List<String> definitionIdList) throws Exception
    {
        List<T8OntologyDefinition> definitionList;

        stats.logExecutionStart("retrieveDefinitions");

        // At the moment we are ignoring the orgIDList until the proper API data handlers have been put in place.
        definitionList = dataHandler.retrieveDefinitions(tx, definitionIdList);
        stats.logExecutionEnd("retrieveDefinitions");
        return definitionList;
    }

    public List<T8OntologyAbbreviation> retrieveAbbreviations(List<String> abbreviationIdList) throws Exception
    {
        List<T8OntologyAbbreviation> abbreviationList;

        stats.logExecutionStart("retrieveAbbreviations");

        // At the moment we are ignoring the orgIDList until the proper API data handlers have been put in place.
        abbreviationList = dataHandler.retrieveAbbreviations(tx, abbreviationIdList);
        stats.logExecutionEnd("retrieveAbbreviations");
        return abbreviationList;
    }

    public List<T8OntologyCode> retrieveCodes(List<String> codeTypeIdList, String code) throws Exception
    {
        List<String> orgIDList;

        // Add the root organization to the orgIDList as we only want to check codes linked to this organization.
        orgIDList = new ArrayList<>();
        orgIDList.add(organizationStructure.getRootOrganizationID());

        // Retrieve and return the codes.
        return organizationDataHandler.retrieveOntologyCodes(tx, orgIDList, null, codeTypeIdList, code);
    }

    public List<T8OntologyCode> retrieveCodes(List<String> codeIdList) throws Exception
    {
        List<T8OntologyCode> codeList;

        stats.logExecutionStart("retrieveCodes");

        // At the moment we are ignoring the orgIDList until the proper API data handlers have been put in place.
        codeList = dataHandler.retrieveCodes(tx, codeIdList);
        stats.logExecutionEnd("retrieveCodes");
        return codeList;
    }

    public List<T8OntologyTerm> retrieveConceptTerms(String conceptId, boolean includeAbbreviations) throws Exception
    {
        List<T8OntologyTerm> termList;

        stats.logExecutionStart("retrieveConceptTerms");
        termList = dataHandler.retrieveConceptTerms(tx, conceptId, includeAbbreviations);
        stats.logExecutionEnd("retrieveConceptTerms");
        return termList;
    }

    public List<T8OntologyTerm> retrieveConceptTerms(Collection<String> conceptIds, boolean includeAbbreviations) throws Exception
    {
        List<T8OntologyTerm> termList;

        stats.logExecutionStart("retrieveConceptTerms");
        termList = dataHandler.retrieveConceptTerms(tx, conceptIds, includeAbbreviations);
        stats.logExecutionEnd("retrieveConceptTerms");
        return termList;
    }

    public List<T8OntologyDefinition> retrieveConceptDefinitions(String conceptId) throws Exception
    {
        List<T8OntologyDefinition> definitionList;

        stats.logExecutionStart("retrieveConceptDefinitions");

        // At the moment we are ignoring the orgIDList until the proper API data handlers have been put in place.
        definitionList = dataHandler.retrieveConceptDefinitions(tx, conceptId);
        stats.logExecutionEnd("retrieveConceptDefinitions");
        return definitionList;
    }

    public List<T8OntologyDefinition> retrieveConceptDefinitions(Collection<String> conceptIds) throws Exception
    {
        List<T8OntologyDefinition> definitionList;

        stats.logExecutionStart("retrieveConceptDefinitions");

        // At the moment we are ignoring the orgIDList until the proper API data handlers have been put in place.
        definitionList = dataHandler.retrieveConceptDefinitions(tx, conceptIds);
        stats.logExecutionEnd("retrieveConceptDefinitions");
        return definitionList;
    }

    public List<T8OntologyAbbreviation> retrieveTermAbbreviations(String termId) throws Exception
    {
        List<T8OntologyAbbreviation> abbreviationList;

        stats.logExecutionStart("retrieveTermAbbreviations");

        // At the moment we are ignoring the orgIDList until the proper API data handlers have been put in place.
        abbreviationList = dataHandler.retrieveTermAbbreviations(tx, termId);
        stats.logExecutionEnd("retrieveTermAbbreviations");
        return abbreviationList;
    }

    public List<T8OntologyAbbreviation> retrieveTermAbbreviations(List<String> termIds) throws Exception
    {
        List<T8OntologyAbbreviation> abbreviationList;

        stats.logExecutionStart("retrieveTermAbbreviations");

        // At the moment we are ignoring the orgIDList until the proper API data handlers have been put in place.
        abbreviationList = dataHandler.retrieveTermAbbreviations(tx, termIds);
        stats.logExecutionEnd("retrieveTermAbbreviations");
        return abbreviationList;
    }

    public List<T8OntologyCode> retrieveConceptCodes(String conceptId) throws Exception
    {
        List<T8OntologyCode> codeList;

        stats.logExecutionStart("retrieveConceptCodes");
        codeList = dataHandler.retrieveConceptCodes(tx, conceptId);
        stats.logExecutionEnd("retrieveConceptCodes");
        return codeList;
    }

    public List<T8OntologyCode> retrieveConceptCodes(Collection<String> conceptIds) throws Exception
    {
        List<T8OntologyCode> codeList;

        stats.logExecutionStart("retrieveConceptCodes");
        codeList = dataHandler.retrieveConceptCodes(tx, conceptIds);
        stats.logExecutionEnd("retrieveConceptCodes");
        return codeList;
    }

    public void removeExistingConcepts(Collection<T8OntologyConcept> conceptsToCheck) throws Exception
    {
        Iterator<T8OntologyConcept> conceptIterator;
        Set<String> conceptIds;

        conceptIds = new HashSet<>();
        for (T8OntologyConcept concept : conceptsToCheck)
        {
            conceptIds.add(concept.getID());
        }

        conceptIds = retrieveExistingConceptIds(conceptIds);
        conceptIterator = conceptsToCheck.iterator();
        while (conceptIterator.hasNext())
        {
            if (conceptIds.contains(conceptIterator.next().getID()))
            {
                conceptIterator.remove();
            }
        }
    }

    public Set<String> retrieveExistingConceptIds(Collection<String> conceptIdsToCheck) throws Exception
    {
        List<T8OntologyConcept> existingConcepts;
        Set<String> existingConceptIds;

        // Retrieve all existing concepts
        existingConcepts = retrieveConcepts(conceptIdsToCheck, false, false, false, false, false);
        existingConceptIds = new HashSet<>();
        for (T8OntologyConcept existingConcept : existingConcepts)
        {
            existingConceptIds.add(existingConcept.getID());
        }

        // Return the subset of concept ids from the input set that exist.
        return existingConceptIds;
    }

    public void insertConcepts(Collection<T8OntologyConcept> concepts, boolean insertTerms, boolean insertAbbreviations, boolean insertDefinitions, boolean insertCodes, boolean insertTerminology) throws Exception
    {
        for (T8OntologyConcept concept : concepts)
        {
            insertConcept(concept, insertTerms, insertAbbreviations, insertDefinitions, insertCodes, insertTerminology);
        }
    }

    public void insertConcept(T8OntologyConcept concept, boolean insertTerms, boolean insertAbbreviations, boolean insertDefinitions, boolean insertCodes, boolean insertTerminology) throws Exception
    {
        // First insert the concept.
        dataHandler.insertConcept(tx, concept, false, false, false, false);

        // Insert the terms if required.
        if (insertTerms)
        {
            for (T8OntologyTerm term : concept.getTerms())
            {
                insertTerm(term, insertAbbreviations);
            }
        }

        // Insert the definitions if required.
        if (insertDefinitions)
        {
            for (T8OntologyDefinition definition : concept.getDefinitions())
            {
                insertDefinition(definition);
            }
        }

        // Insert the codes if required.
        if (insertCodes)
        {
            for (T8OntologyCode code : concept.getCodes())
            {
                insertCode(code);
            }
        }

        // Insert all of the concept links.
        if (concept.getOntologyLinks().isEmpty())
        {
            throw new Exception("Cannot insert new concept without at least one organization link.");
        }
        else
        {
            insertOntologyLinks(concept.getOntologyLinks());
        }

        // Insert terminology if required.
        if (insertTerminology)
        {
            T8TerminologyApi trmApi;

            trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            for (T8ConceptTerminology terminology : concept.createTerminologies(organizationStructure.getRootOrganizationID()))
            {
                trmApi.insertTerminology(terminology);
            }
        }
    }

    public void insertTerm(T8OntologyTerm term, boolean insertAbbreviations) throws Exception
    {
        // First insert the term.
        dataHandler.insertTerm(tx, term, false);

        // Insert the abbreviations if required.
        if (insertAbbreviations)
        {
            for (T8OntologyAbbreviation abbreviation : term.getAbbreviations())
            {
                insertAbbreviation(abbreviation);
            }
        }
    }

    public void insertDefinition(T8OntologyDefinition definition) throws Exception
    {
        // First insert the definition.
        dataHandler.insertDefinition(tx, definition);
    }

    public void insertAbbreviation(T8OntologyAbbreviation abbreviation) throws Exception
    {
        // First insert the abbreviation.
        dataHandler.insertAbbreviation(tx, abbreviation);
    }

    public void insertCode(T8OntologyCode code) throws Exception
    {
        // First insert the code.
        dataHandler.insertCode(tx, code);
    }

    public void updateConcepts(Collection<T8OntologyConcept> concepts, boolean updateTerms, boolean updateAbbreviations, boolean updateDefinitions, boolean updateCodes, boolean updateTerminology) throws Exception
    {
        for (T8OntologyConcept concept : concepts)
        {
            updateConcept(concept, updateTerms, updateAbbreviations, updateDefinitions, updateCodes, updateTerminology);
        }
    }

    public void updateConcept(T8OntologyConcept concept, boolean updateTerms, boolean updateAbbreviations, boolean updateDefinitions, boolean updateCodes, boolean updateTerminology) throws Exception
    {
        // First update the concept.
        dataHandler.updateConcept(tx, concept, false, false, false, false);

        // Update the terms if required.
        if (updateTerms)
        {
            for (T8OntologyTerm term : concept.getTerms())
            {
                updateTerm(term, updateAbbreviations);
            }
        }

        // Update the definitions if required.
        if (updateDefinitions)
        {
            for (T8OntologyDefinition definition : concept.getDefinitions())
            {
                updateDefinition(definition);
            }
        }

        // Update the codes if required.
        if (updateCodes)
        {
            for (T8OntologyCode code : concept.getCodes())
            {
                updateCode(code);
            }
        }

        // Insert all of the concept links.
        linkConceptsToOrganizations(concept.getOntologyLinks());

        // Update terminology if required.
        if (updateTerminology)
        {
            T8TerminologyApi trmApi;

            trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            for (T8ConceptTerminology terminology : concept.createTerminologies(organizationStructure.getRootOrganizationID()))
            {
                trmApi.updateTerminology(terminology, false);
            }
        }
    }

    public void updateTerm(T8OntologyTerm term, boolean updateAbbreviations) throws Exception
    {
        // First update the term.
        dataHandler.updateTerm(tx, term, false);

        // Update the abbreviations if required.
        if (updateAbbreviations)
        {
            for (T8OntologyAbbreviation abbreviation : term.getAbbreviations())
            {
                updateAbbreviation(abbreviation);
            }
        }
    }

    public void updateDefinition(T8OntologyDefinition definition) throws Exception
    {
        // First update the definition.
        dataHandler.updateDefinition(tx, definition);
    }

    public void updateAbbreviation(T8OntologyAbbreviation abbreviation) throws Exception
    {
        // First update the abbreviation.
        dataHandler.updateAbbreviation(tx, abbreviation);
    }

    public void updateCode(T8OntologyCode code) throws Exception
    {
        // First update the code.
        dataHandler.updateCode(tx, code);
    }

    public void deleteConcept(String conceptId) throws Exception
    {
        T8TerminologyApi trmApi;
        T8OntologyConcept concept;

        // Retrieve the concept so we can delete all related data.
        concept = dataHandler.retrieveConcept(tx, conceptId, true, false, true, true);

        // First delete any terminology.
        trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
        trmApi.deleteTerminology(ArrayLists.typeSafeList(conceptId));

        // Delete all of the concept terms.
        for (T8OntologyTerm term : concept.getTerms())
        {
            deleteTerm(term.getID());
        }

        // Delete all of the concept definitions.
        for (T8OntologyDefinition definition : concept.getDefinitions())
        {
            deleteDefinition(definition.getID());
        }

        // Delete all of the concept codes.
        for (T8OntologyCode code : concept.getCodes())
        {
            deleteCode(code.getID());
        }

        // Remove all organization links to the concept.
        unlinkConceptsFromOrganizations(null, null, ArrayLists.typeSafeList(conceptId));

        // Delete the concept.
        dataHandler.deleteConcept(tx, concept, false, false, false, false);
    }

    public void deleteTerm(String termID) throws Exception
    {
        T8OntologyTerm term;

        // Retrieve the term so we can delete all related data.
        term = dataHandler.retrieveTerm(tx, termID, true);

        // Unlink all of the term abbreviations.
        for (T8OntologyAbbreviation abbreviation : term.getAbbreviations())
        {
            deleteAbbreviation(abbreviation.getID());
        }

        // Delete the term.
        dataHandler.deleteTerm(tx, term, false);
    }

    public void deleteDefinition(String definitionID) throws Exception
    {
        // Delete the definition.
        dataHandler.deleteDefinition(tx, definitionID);
    }

    public void deleteAbbreviation(String abbreviationId) throws Exception
    {
        // Delete the abbreviation.
        dataHandler.deleteAbbreviation(tx, abbreviationId);
    }

    public void deleteCode(String codeId) throws Exception
    {
        // Delete the code.
        dataHandler.deleteCode(tx, codeId);
    }

    public void saveConcepts(Collection<T8OntologyConcept> concepts, boolean deletionEnabled, boolean saveTerms, boolean saveAbbreviations, boolean saveDefinitions, boolean saveCodes, boolean saveTerminology) throws Exception
    {
        for (T8OntologyConcept concept : concepts)
        {
            saveConcept(concept, deletionEnabled, saveTerms, saveAbbreviations, saveDefinitions, saveCodes, saveTerminology);
        }
    }

    /**
     * Saves the supplied {@code T8OntologyConcept} according to the flags specified.
     * This method automatically performs the persistence operations required to ensure that the persisted
     * state of the supplied concept corresponds exactly with the object.  The existing
     * concept is first retrieved in order to determine how deletions/inserts/updates
     * have to be performed.
     *
     * @param concept The concept to be saved.
     * @param deletionEnabled Whether the delete the current concepts elements and save the passed values.
     * @param saveTerms Whether to save the terms given in the concept.
     * @param saveAbbreviations Whether to save the abbreviations given in the concept.
     * @param saveDefinitions Whether to save the definitions given in the concept.
     * @param saveCodes Whether to save the codes given in the concept.
     * @param saveTerminology Whether to create a new terminology entry for the concept.
     * @throws Exception There are multiple data source related {@code Exception}'s that can occur.
     */
    public void saveConcept(T8OntologyConcept concept, boolean deletionEnabled, boolean saveTerms, boolean saveAbbreviations, boolean saveDefinitions, boolean saveCodes, boolean saveTerminology) throws Exception
    {
        T8OntologyConcept existingConcept;

        // Retrieve the existing concept.  We try to include as little data as possible because we need the concept to check for existence only.
        existingConcept = dataHandler.retrieveConcept(tx, concept.getID(), false, false, false, false);

        // If the concept does not yet exist, insert it.
        if (existingConcept == null)
        {
            insertConcept(concept, saveTerms, saveAbbreviations, saveDefinitions, saveCodes, saveTerminology);
        }
        else
        {
            // Now retrieve all of the additional existing concept data so that we can determine which ones to delete.
            existingConcept.addOntologyLinks(retrieveOntologyLinks(null, concept.getID()));
            if (saveTerms) existingConcept.addTerms(retrieveConceptTerms(existingConcept.getID(), saveAbbreviations));
            if (saveDefinitions) existingConcept.addDefinitions(retrieveConceptDefinitions(existingConcept.getID()));
            if (saveCodes) existingConcept.addCodes(retrieveConceptCodes(existingConcept.getID()));

            // Add organization links.
            for (T8OntologyLink link : concept.getOntologyLinks())
            {
                if (!existingConcept.containsEquivalentLink(link))
                {
                    organizationDataHandler.insertOntologyLink(tx, link);
                }
            }

            // Update the concept if needed.
            if (!existingConcept.isEqualTo(concept))
            {
                dataHandler.updateConcept(tx, concept, false, false, false, false);
            }

            // Save the terms if required.
            if (saveTerms)
            {
                for (T8OntologyTerm term : concept.getTerms())
                {
                    T8OntologyTerm existingTerm;

                    existingTerm = existingConcept.getTerm(term.getID());
                    saveTermStateChange(existingTerm, term, saveAbbreviations, false);
                }
            }

            // Save the definitions if required.
            if (saveDefinitions)
            {
                for (T8OntologyDefinition definition : concept.getDefinitions())
                {
                    T8OntologyDefinition existingDefinition;

                    existingDefinition = existingConcept.getDefinition(definition.getID());
                    saveDefinitionStateChange(existingDefinition, definition);
                }
            }

            // Save the codes if required.
            if (saveCodes)
            {
                for (T8OntologyCode code : concept.getCodes())
                {
                    T8OntologyCode existingCode;

                    existingCode = existingConcept.getCode(code.getID());
                    saveCodeStateChange(existingCode, code);
                }
            }

            // Save updated terminology if required.
            if (saveTerminology)
            {
                T8TerminologyApi trmApi;

                trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
                for (T8ConceptTerminology newTerminology : concept.createTerminologies(organizationStructure.getRootOrganizationID()))
                {
                    trmApi.saveTerminology(newTerminology, true);
                }
            }

            // If the deletion flag is enabled, delete all parts of the concept
            if (deletionEnabled)
            {
                // Delete links that are persisted but not present in the input concept.
                for (T8OntologyLink existingOntologyLink : existingConcept.getOntologyLinks())
                {
                    if (!concept.containsEquivalentLink(existingOntologyLink))
                    {
                        organizationDataHandler.deleteOntologyLink(tx, existingOntologyLink);
                    }
                }

                // Delete all terms that are in the existing concept but not in the provided concept.
                for (T8OntologyTerm existingTerm : existingConcept.getTerms())
                {
                    T8OntologyTerm newTerm;

                    newTerm = concept.getTerm(existingTerm.getID());
                    if (newTerm == null)
                    {
                        deleteTerm(existingTerm.getID());
                    }
                    else
                    {
                        for (T8OntologyAbbreviation existingAbbreviation : existingTerm.getAbbreviations())
                        {
                            T8OntologyAbbreviation newAbbreviation;

                            newAbbreviation = newTerm.getAbbreviation(existingAbbreviation.getID());
                            if (newAbbreviation == null)
                            {
                                deleteAbbreviation(existingAbbreviation.getID());
                            }
                        }
                    }
                }

                // Delete all definitions that are in the existing concept but not in the provided concept.
                for (T8OntologyDefinition definition : existingConcept.getDefinitions())
                {
                    if (!concept.containsDefinitionWithID(definition.getID()))
                    {
                        deleteDefinition(definition.getID());
                    }
                }

                // Delete all codes that are in the existing concept but not in the provided concept.
                for (T8OntologyCode code : existingConcept.getCodes())
                {
                    if (!concept.containsCodeWithID(code.getID()))
                    {
                        deleteCode(code.getID());
                    }
                }
            }
        }
    }

    public void saveConceptStateChange(T8OntologyConcept fromStateConcept, T8OntologyConcept toStateConcept) throws Exception
    {
        // If the concept does not yet exist, insert it.
        if (fromStateConcept == null)
        {
            insertConcept(toStateConcept, true, true, true, true, true);
        }
        else
        {
            T8TerminologyApi trmApi;

            // Add organization links.
            for (T8OntologyLink link : toStateConcept.getOntologyLinks())
            {
                if (!fromStateConcept.containsEquivalentLink(link))
                {
                    organizationDataHandler.insertOntologyLink(tx, link);
                }
            }

            // Update the concept if needed.
            if (!fromStateConcept.isEqualTo(toStateConcept))
            {
                dataHandler.updateConcept(tx, toStateConcept, false, false, false, false);
            }

            // Save the terms if required.
            for (T8OntologyTerm term : toStateConcept.getTerms())
            {
                T8OntologyTerm existingTerm;

                existingTerm = fromStateConcept.getTerm(term.getID());
                saveTermStateChange(existingTerm, term, true, false);
            }

            // Save the definitions if required.
            for (T8OntologyDefinition definition : toStateConcept.getDefinitions())
            {
                T8OntologyDefinition existingDefinition;

                existingDefinition = fromStateConcept.getDefinition(definition.getID());
                saveDefinitionStateChange(existingDefinition, definition);
            }

            // Save the codes if required.
            for (T8OntologyCode code : toStateConcept.getCodes())
            {
                T8OntologyCode existingCode;

                existingCode = fromStateConcept.getCode(code.getID());
                saveCodeStateChange(existingCode, code);
            }

            // Save updated terminology.
            trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            for (T8ConceptTerminology newTerminology : toStateConcept.createTerminologies(organizationStructure.getRootOrganizationID()))
            {
                trmApi.saveTerminology(newTerminology, true);
            }

            // Delete links that are persisted but not present in the input concept.
            for (T8OntologyLink existingOntologyLink : fromStateConcept.getOntologyLinks())
            {
                if (!toStateConcept.containsEquivalentLink(existingOntologyLink))
                {
                    organizationDataHandler.deleteOntologyLink(tx, existingOntologyLink);
                }
            }

            // Delete all terms that are in the existing concept but not in the provided concept.
            for (T8OntologyTerm existingTerm : fromStateConcept.getTerms())
            {
                T8OntologyTerm newTerm;

                newTerm = toStateConcept.getTerm(existingTerm.getID());
                if (newTerm == null)
                {
                    deleteTerm(existingTerm.getID());
                }
                else
                {
                    for (T8OntologyAbbreviation existingAbbreviation : existingTerm.getAbbreviations())
                    {
                        T8OntologyAbbreviation newAbbreviation;

                        newAbbreviation = newTerm.getAbbreviation(existingAbbreviation.getID());
                        if (newAbbreviation == null)
                        {
                            deleteAbbreviation(existingAbbreviation.getID());
                        }
                    }
                }
            }

            // Delete all definitions that are in the existing concept but not in the provided concept.
            for (T8OntologyDefinition definition : fromStateConcept.getDefinitions())
            {
                if (!toStateConcept.containsDefinitionWithID(definition.getID()))
                {
                    deleteDefinition(definition.getID());
                }
            }

            // Delete all codes that are in the existing concept but not in the provided concept.
            for (T8OntologyCode code : fromStateConcept.getCodes())
            {
                if (!toStateConcept.containsCodeWithID(code.getID()))
                {
                    deleteCode(code.getID());
                }
            }
        }
    }

    public void saveTerm(T8OntologyTerm term, boolean deletionEnabled, boolean saveAbbreviations) throws Exception
    {
        // Save the definition.
        dataHandler.saveTerm(tx, term, false, false);

        // Save the abbreviations if required.
        if (saveAbbreviations)
        {
            for (T8OntologyAbbreviation abbreviation : term.getAbbreviations())
            {
                saveAbbreviation(abbreviation);
            }
        }

        // Delete old terms if required.
        if (deletionEnabled)
        {
            T8OntologyTerm existingTerm;

            // Retrieve the existing term.
            // TODO: GBO - Shouldn't the existing term be retrieved before the term or abbreviations are saved at all?
            existingTerm = retrieveTerm(term.getID(), true);

            // Delete all abbreviations that are linked to the existing term, but not to the new term.
            for (T8OntologyAbbreviation abbreviation : existingTerm.getAbbreviations())
            {
                if (!term.containsAbbreviationWithID(abbreviation.getID()))
                {
                    deleteAbbreviation(abbreviation.getID());
                }
            }
        }
    }

    public void saveTermStateChange(T8OntologyTerm existingTerm, T8OntologyTerm newTerm, boolean saveAbbreviations, boolean deleteAbbreviations) throws Exception
    {
        if (existingTerm == null)
        {
            // The term does not yet exist, so just do a straight insert of all component parts including links and abbreviations.
            insertTerm(newTerm, saveAbbreviations);
        }
        else if (newTerm == null)
        {
            // There is no new term so delete the existing one (if applicable).
            deleteTerm(existingTerm.getID());
        }
        else
        {
            // Only update the term if it is different from the existing term.
            if (!existingTerm.isEqualTo(newTerm))
            {
                // Don't update abbreviations here, we need to establish which ones need inserting first.
                dataHandler.updateTerm(tx, newTerm, false);
            }

            // Handle the abbreviations inserts/updates.
            if (saveAbbreviations)
            {
                for (T8OntologyAbbreviation newAbbreviation : newTerm.getAbbreviations())
                {
                    T8OntologyAbbreviation existingAbbreviation;

                    existingAbbreviation = existingTerm.getAbbreviation(newAbbreviation.getID());
                    saveAbbreviationStateChange(existingAbbreviation, newAbbreviation);
                }
            }

            // Handle the abbreviations inserts/updates.
            if (deleteAbbreviations)
            {
                for (T8OntologyAbbreviation existingAbbreviation : existingTerm.getAbbreviations())
                {
                    T8OntologyAbbreviation newAbbreviation;

                    newAbbreviation = newTerm.getAbbreviation(existingAbbreviation.getID());
                    if (newAbbreviation == null)
                    {
                        deleteAbbreviation(existingAbbreviation.getID());
                    }
                }
            }
        }
    }

    public void saveDefinition(T8OntologyDefinition definition) throws Exception
    {
        // Save the definition.
        dataHandler.saveDefinition(tx, definition);
    }

    public void saveDefinitionStateChange(T8OntologyDefinition existingDefinition, T8OntologyDefinition newDefinition) throws Exception
    {
        if (existingDefinition == null)
        {
            // The abbreviation does not yet exist, so just do a straight insert of all component parts including links.
            insertDefinition(newDefinition);
        }
        else if (newDefinition == null)
        {
            // There is no new abbreviation so delete the existing one (if applicable).
            deleteDefinition(existingDefinition.getID());
        }
        else if (!existingDefinition.isEqualTo(newDefinition)) // Only update the abbreviation if it is different from the existing abbreviation.
        {
            dataHandler.updateDefinition(tx, newDefinition);
        }
    }

    public void saveAbbreviation(T8OntologyAbbreviation abbreviation) throws Exception
    {
        // Save the abbreviation.
        dataHandler.saveAbbreviation(tx, abbreviation);
    }

    public void saveAbbreviationStateChange(T8OntologyAbbreviation existingAbbreviation, T8OntologyAbbreviation newAbbreviation) throws Exception
    {
        if (existingAbbreviation == null)
        {
            // The abbreviation does not yet exist, so just do a straight insert of all component parts including links.
            insertAbbreviation(newAbbreviation);
        }
        else if (newAbbreviation == null)
        {
            // There is no new abbreviation so delete the existing one (if applicable).
            deleteAbbreviation(existingAbbreviation.getID());
        }
        else if (!existingAbbreviation.isEqualTo(newAbbreviation)) // Only update the abbreviation if it is different from the existing abbreviation.
        {
            dataHandler.updateAbbreviation(tx, newAbbreviation);
        }
    }

    public void saveCode(T8OntologyCode code) throws Exception
    {
        // Save the code.
        dataHandler.saveCode(tx, code);
    }

    public void saveCodeStateChange(T8OntologyCode existingCode, T8OntologyCode newCode) throws Exception
    {
        if (existingCode == null)
        {
            // The code does not yet exist, so just do a straight insert of all component parts including links.
            insertCode(newCode);
        }
        else if (newCode == null)
        {
            // There is no new code so delete the existing one (if applicable).
            deleteCode(existingCode.getID());
        }
        else if (!existingCode.isEqualTo(newCode)) // Only update the code if it is different from the existing code.
        {
            dataHandler.updateCode(tx, newCode);
        }
    }

    public void saveConceptAlteration(T8ConceptAlteration alteration, boolean inserts, boolean updates, boolean deletes) throws Exception
    {
        if (inserts)
        {
            stats.logExecutionStart("insertConcept");
            for (T8OntologyConcept concept : alteration.getInsertedConcepts())
            {
                dataHandler.insertConcept(tx, concept, false, false, false, false);
            }
            stats.logExecutionEnd("insertConcept");

            stats.logExecutionStart("insertOntologyLink");
            for (T8OntologyLink link : alteration.getInsertedOntologyLinks())
            {
                organizationDataHandler.insertOntologyLink(tx, link);
            }
            stats.logExecutionEnd("insertOntologyLink");

            stats.logExecutionStart("insertTerm");
            for (T8OntologyTerm term : alteration.getInsertedTerms())
            {
                dataHandler.insertTerm(tx, term, false);
            }
            stats.logExecutionEnd("insertTerm");

            stats.logExecutionStart("insertAbbreviation");
            for (T8OntologyAbbreviation abbreviation : alteration.getInsertedAbbreviations())
            {
                dataHandler.insertAbbreviation(tx, abbreviation);
            }
            stats.logExecutionEnd("insertAbbreviation");

            stats.logExecutionStart("insertDefinition");
            for (T8OntologyDefinition definition : alteration.getInsertedDefinitions())
            {
                dataHandler.insertDefinition(tx, definition);
            }
            stats.logExecutionEnd("insertDefinition");

            stats.logExecutionStart("insertCode");
            for (T8OntologyCode code : alteration.getInsertedCodes())
            {
                dataHandler.insertCode(tx, code);
            }
            stats.logExecutionEnd("insertCode");
        }

        if (updates)
        {
            stats.logExecutionStart("updateConcept");
            for (T8OntologyConcept concept : alteration.getUpdatedConcepts())
            {
                dataHandler.updateConcept(tx, concept, false, false, false, false);
            }
            stats.logExecutionEnd("updateConcept");

            stats.logExecutionStart("updateTerm");
            for (T8OntologyTerm term : alteration.getUpdatedTerms())
            {
                dataHandler.updateTerm(tx, term, false);
            }
            stats.logExecutionEnd("updateTerm");

            stats.logExecutionStart("updateAbbreviation");
            for (T8OntologyAbbreviation abbreviation : alteration.getUpdatedAbbreviations())
            {
                dataHandler.updateAbbreviation(tx, abbreviation);
            }
            stats.logExecutionEnd("updateAbbreviation");

            stats.logExecutionStart("updateDefinition");
            for (T8OntologyDefinition definition : alteration.getUpdatedDefinitions())
            {
                dataHandler.updateDefinition(tx, definition);
            }
            stats.logExecutionEnd("updateDefinition");

            stats.logExecutionStart("updateCode");
            for (T8OntologyCode code : alteration.getUpdatedCodes())
            {
                dataHandler.updateCode(tx, code);
            }
            stats.logExecutionEnd("updateCode");
        }

        if (deletes)
        {
            stats.logExecutionStart("deleteCode");
            for (T8OntologyCode code : alteration.getDeletedCodes())
            {
                dataHandler.deleteCode(tx, code);
            }
            stats.logExecutionEnd("deleteCode");

            stats.logExecutionStart("deleteDefinition");
            for (T8OntologyDefinition definition : alteration.getDeletedDefinitions())
            {
                dataHandler.deleteDefinition(tx, definition);
            }
            stats.logExecutionEnd("deleteDefinition");

            stats.logExecutionStart("deleteAbbreviation");
            for (T8OntologyAbbreviation abbreviation : alteration.getDeletedAbbreviations())
            {
                dataHandler.deleteAbbreviation(tx, abbreviation);
            }
            stats.logExecutionEnd("deleteAbbreviation");

            stats.logExecutionStart("deleteTerm");
            for (T8OntologyTerm term : alteration.getDeletedTerms())
            {
                dataHandler.deleteTerm(tx, term, false);
            }
            stats.logExecutionEnd("deleteTerm");

            stats.logExecutionStart("deleteOntologyLink");
            for (T8OntologyLink link : alteration.getDeletedOntologyLinks())
            {
                organizationDataHandler.deleteOntologyLink(tx, link);
            }
            stats.logExecutionEnd("deleteOntologyLink");

            stats.logExecutionStart("deleteConcept");
            for (T8OntologyConcept concept : alteration.getDeletedConcepts())
            {
                dataHandler.deleteConcept(tx, concept, false, false, false, false);
            }
            stats.logExecutionEnd("deleteConcept");
        }
    }

    /**
     * Returns an iterator that will scroll over all of the concept ID's
     * belonging to the specified data type and optionally all its descendant
     * groups.  This iterator will only return concept ID's that are accessible
     * by the current organization i.e. linked to one of its parents or to one
     * of its descendants but not any of its siblings or their descendants.
     *
     * @param ocId The root ODT from which concept ID's will be fetched.
     * @param includeDescendantGroups A boolean flag to indicate whether or not
     * the iterator should include concepts linked to the descendant groups of
     * the specified root ODT.
     * @return A data iterator that returns Data Requirement Instances ID's
     * belonging to the specified group or it's descendants and to the current
     * organization.
     * @throws Exception
     */
    public T8DataIterator<String> getConceptIdIterator(String ocId, boolean includeDescendantGroups) throws Exception
    {
        List<String> ocIds;
        List<String> orgIds;

        // Get the list of ORG_ID's that we can use in the filter.
        orgIds = organizationStructure.getOrganizationLineageIDList(context.getOrganizationId());

        // Get the list of ODT_ID's that we can use in the filter.
        ocIds = new ArrayList<>();
        ocIds.add(ocId); // Add the ODT_ID to the list of its descendants.
        if (includeDescendantGroups) ocIds.addAll(ontologyStructure.getDescendantClassIDList(ocId));

        return organizationDataHandler.getConceptIDIterator(tx, orgIds, ocIds);
    }

    /**
     * Returns all data type ID's in the organization's ontology that belong to
     * the specified parent data type and that match the specified term literal.
     *
     * @param parentOcId The parent data type to which the retrieved data types
     * must belong.
     * @param term The term to match.
     * @param includeDescendantGroups A boolean flag to indicate whether or not
     * to include descendants of the specified parent data type and not just its
     * direct child types.
     * @return All data types ID's that match the specified criteria.
     * @throws java.lang.Exception
     */
    public List<String> retrieveOntologyClassIdsByTerm(String parentOcId, String term, boolean includeDescendantGroups) throws Exception
    {
        List<String> odtIDList;
        List<String> matchingConceptIDList;
        List<String> allowedConceptIDList;

        // Add the ODT_ID for data types to the list.
        odtIDList = new ArrayList<>();
        odtIDList.add(T8PrimaryOntologyDataType.ONTOLOGY_DATA_TYPES.getDataTypeID());

        // Retrieve the list of data type ID's that match the term.
        matchingConceptIDList = organizationDataHandler.retrieveConceptIDsByTerm(tx, organizationStructure.getRootOrganizationID(), null, odtIDList, term);

        // Remove all data type ID's that are not applicable.
        allowedConceptIDList = includeDescendantGroups ? ontologyStructure.getDescendantClassIDList(parentOcId) : ontologyStructure.getChildClassIDList(parentOcId);
        matchingConceptIDList.retainAll(allowedConceptIDList);
        return matchingConceptIDList;
    }

    /**
     * Returns all data type ID's in the organization's ontology that belong to
     * the specified parent data type and that match the specified term literal.
     *
     * @param parentOcId The parent data type to which the retrieved data types
     * must belong.
     * @param code The code to match.
     * @param includeDescendantGroups A boolean flag to indicate whether or not
     * to include descendants of the specified parent data type and not just its
     * direct child types.
     * @return All data types ID's that match the specified criteria.
     * @throws java.lang.Exception
     */
    public List<String> retrieveOntologyClassIdsByCode(String parentOcId, String code, boolean includeDescendantGroups) throws Exception
    {
        String trimmedCode;

        trimmedCode = Strings.trimToNull(code);
        if (trimmedCode != null)
        {
            List<String> odtIDList;
            List<String> matchingConceptIdList;
            List<String> allowedConceptIdList;

            // Add the ODT_ID for data types to the list.
            odtIDList = new ArrayList<>();
            odtIDList.add(T8PrimaryOntologyDataType.ONTOLOGY_DATA_TYPES.getDataTypeID());

            // Retrieve the list of data type ID's that match the code.
            matchingConceptIdList = organizationDataHandler.retrieveConceptIDsByCode(tx, organizationStructure.getRootOrganizationID(), null, odtIDList, code);

            // Remove all data type ID's that are not applicable.
            allowedConceptIdList = includeDescendantGroups ? ontologyStructure.getDescendantClassIDList(parentOcId) : ontologyStructure.getChildClassIDList(parentOcId);
            matchingConceptIdList.retainAll(allowedConceptIdList);
            return matchingConceptIdList;
        }
        else return null;
    }

    /**
     * Returns the first concept id from the specified ontology class that matches the
     * specified code and is linked to the specified organization (optional).
     *
     * @param orgId The organization to which the concept must be linked in order to
     * be included in the code match.
     * @param ocId The id of the ontology class to which the search will be restricted.
     * @param code The code to match.
     * @return The first concept id that matches the specified criteria.
     * @throws java.lang.Exception
     */
    public String retrieveConceptIdByCode(String orgId, String ocId, String code) throws Exception
    {
        String trimmedCode;

        trimmedCode = Strings.trimToNull(code);
        if (trimmedCode != null)
        {
            List<String> ocIdList;
            List<String> conceptIds;

            // Get the list of ODT_ID's that we can use in the filter.
            if (ocId != null)
            {
                ocIdList = new ArrayList<>();
                ocIdList.addAll(ontologyStructure.getDescendantClassIDList(ocId));
                ocIdList.add(ocId); // Add the ODT_ID to the list of its descendants.
            }
            else ocIdList = null;

            // Return the first retrieved concept id.
            conceptIds = organizationDataHandler.retrieveConceptIDsByCode(tx, organizationStructure.getRootOrganizationID(), null, ocIdList, code);
            return conceptIds.size() > 0 ? conceptIds.get(0) : null;
        }
        else return null;
    }

    /**
     * Returns all concept ID's in the organization's ontology that match the
     * specified code.  If the concept ODT_ID is supplied only concepts from the
     * specified group will be returned.
     *
     * @param ocId The (optional) ODT_ID to which the search will be
     * restricted.  If null, all groups will be searched.
     * @param code The code to match.
     * @param includeDescendantGroups A boolean flag to indicate whether or not
     * to include descendants of the specified group when fetching the concept.
     * @return All concept ID's with a code that matches the specified
     * criteria.
     * @throws java.lang.Exception
     */
    public List<String> retrieveConceptIdsByCode(String ocId, String code, boolean includeDescendantGroups) throws Exception
    {
        String trimmedCode;

        trimmedCode = Strings.trimToNull(code);
        if (trimmedCode != null)
        {
            List<String> ocIdList;

            // Get the list of ODT_ID's that we can use in the filter.
            if (ocId != null)
            {
                ocIdList = new ArrayList<>();
                if (includeDescendantGroups) ocIdList.addAll(ontologyStructure.getDescendantClassIDList(ocId));
                ocIdList.add(ocId); // Add the ODT_ID to the list of its descendants.
            }
            else ocIdList = null;

            // Return the retrieved concept ID list.
            return organizationDataHandler.retrieveConceptIDsByCode(tx, organizationStructure.getRootOrganizationID(), null, ocIdList, code);
        }
        else return null;
    }

    /**
     * Returns the first concept id from the specified ontology class that matches the
     * specified term and is linked to the specified organization (optional).
     *
     * @param orgId The organization to which the concept must be linked in order to
     * be included in the term match.
     * @param ocId The id of the ontology class to which the search will be restricted.
     * @param term The term to match.
     * @return The first concept id that matches the specified criteria.
     * @throws java.lang.Exception
     */
    public String retrieveConceptIdByTerm(String orgId, String ocId, String term) throws Exception
    {
        String trimmedTerm;

        trimmedTerm = Strings.trimToNull(term);
        if (trimmedTerm != null)
        {
            List<String> ocIds;
            List<String> conceptIds;

            // Get the list of ODT_ID's that we can use in the filter.
            if (ocId != null)
            {
                ocIds = new ArrayList<>();
                ocIds.addAll(ontologyStructure.getDescendantClassIDList(ocId));
                ocIds.add(ocId); // Add the ODT_ID to the list of its descendants.
            }
            else ocIds = null;

            // Return the first retrieved concept id.
            conceptIds = organizationDataHandler.retrieveConceptIDsByTerm(tx, organizationStructure.getRootOrganizationID(), null, ocIds, term);
            return conceptIds.size() > 0 ? conceptIds.get(0) : null;
        }
        else return null;
    }

    /**
     * Returns all concept ID's in the organization's ontology that match the
     * specified term literal.  If the concept ODT_ID is supplied only concepts
     * from the specified group will be returned.
     *
     * @param ocId The ontology class to which the search will be restricted.
     * @param term The term to match.
     * @param includeDescendantGroups A boolean flag to indicate whether or not
     * to include descendants of the specified group when fetching the concept.
     * @return All concept ID's with a term that matches the specified
     * criteria.
     * @throws java.lang.Exception
     */
    public List<String> retrieveConceptIdsByTerm(String ocId, String term, boolean includeDescendantGroups) throws Exception
    {
        String trimmedTerm;

        trimmedTerm = Strings.trimToNull(term);
        if (trimmedTerm != null)
        {
            List<String> ocIds;

            // Get the list of ODT_ID's that we can use in the filter.
            if (ocId != null)
            {
                ocIds = new ArrayList<>();
                if (includeDescendantGroups) ocIds.addAll(ontologyStructure.getDescendantClassIDList(ocId));
                ocIds.add(ocId); // Add the ODT_ID to the list of its descendants.
            }
            else ocIds = null;

            // Return the retrieved concept ID list.
            return organizationDataHandler.retrieveConceptIDsByTerm(tx, organizationStructure.getRootOrganizationID(), null, ocIds, term);
        }
        else return null;
    }

    /**
     * Returns all concept ID's in the organization's ontology that match the
     * specified definition literal.  If the concept ODT_ID is supplied only
     * concepts from the specified group will be returned.
     *
     * @param ocId The (optional) ODT_ID to which the search will be
     * restricted.  If null, all groups will be searched.
     * @param definition The definition to match.
     * @param includeDescendantGroups A boolean flag to indicate whether or not
     * to include descendants of the specified group when fetching the concept.
     * @return All concept ID's with a definition that matches the specified
     * criteria.
     * @throws java.lang.Exception
     */
    public List<String> retrieveConceptIdsByDefinition(String ocId, String definition, boolean includeDescendantGroups) throws Exception
    {
        List<String> ocIds;

        // Get the list of ODT_ID's that we can use in the filter.
        if (ocId != null)
        {
            ocIds = new ArrayList<>();
            if (includeDescendantGroups) ocIds.addAll(ontologyStructure.getDescendantClassIDList(ocId));
            ocIds.add(ocId); // Add the ODT_ID to the list of its descendants.
        }
        else ocIds = null;

        // Return the retrieved concept ID list.
        return organizationDataHandler.retrieveConceptIDsByDefinition(tx, organizationStructure.getRootOrganizationID(), null, ocIds, definition);
    }

    /**
     * Inserts the supplied ontology links.
     * @param ontologyLinks The ontology links to insert.
     * @throws Exception
     */
    public void insertOntologyLinks(Collection<T8OntologyLink> ontologyLinks) throws Exception
    {
        List<String> orgIdList;

        orgIdList = organizationStructure.getOrganizationIDList();
        for (T8OntologyLink link : ontologyLinks)
        {
            // Make sure all of the links belong to this organization structure.
            if (orgIdList.contains(link.getOrganizationID()))
            {
                organizationDataHandler.insertOntologyLink(tx, link);
            }
            else throw new Exception("Cannot insert ontology link " + link + " that does not belong to organization structure.");
        }
    }

    /**
     * Retrieves the list of Organization Concept links for the specified
     * concept.
     * @param osIds The id's of the ontology structure to include.
     * If this parameter is null, all ontology structures will be included.
     * @param conceptId The concept ID for which to retrieve the links.
     * @return The list of ontology links for the specified concept.
     * @throws Exception
     */
    public List<T8OntologyLink> retrieveOntologyLinks(Collection<String> osIds, String conceptId) throws Exception
    {
        return organizationDataHandler.retrieveOntologyLinks(tx, organizationStructure.getRootOrganizationID(), null, conceptId);
    }

    public List<T8OntologyLink> retrieveOntologyLinks(Collection<String> osIds, Collection<String> conceptIds) throws Exception
    {
        return organizationDataHandler.retrieveOntologyLinks(tx, organizationStructure.getRootOrganizationID(), null, conceptIds);
    }

    public void linkConceptToOrganization(T8OntologyLink conceptLink) throws Exception
    {
        organizationDataHandler.linkConceptsToOrganization(tx, conceptLink.getOrganizationID(), organizationStructure.getOntologyStructureID(), conceptLink.getOntologyClassID(), ArrayLists.typeSafeList(conceptLink.getConceptID()));
    }

    public void linkConceptsToOrganizations(List<T8OntologyLink> conceptLinks) throws Exception
    {
        for (T8OntologyLink conceptLink : conceptLinks)
        {
            organizationDataHandler.linkConceptsToOrganization(tx, conceptLink.getOrganizationID(), organizationStructure.getOntologyStructureID(), conceptLink.getOntologyClassID(), ArrayLists.typeSafeList(conceptLink.getConceptID()));
        }
    }

    public void linkConceptsToOrganization(String orgId, String ocId, List<String> conceptIds) throws Exception
    {
        organizationDataHandler.linkConceptsToOrganization(tx, orgId, organizationStructure.getOntologyStructureID(), ocId, conceptIds);
    }

    public int unlinkConceptsFromOrganizations(List<String> orgIds, List<String> ocIds, List<String> conceptIds) throws Exception
    {
        return organizationDataHandler.unlinkConceptsFromOrganizations(tx, orgIds, ocIds, conceptIds);
    }

    public void setConceptActive(List<String> orgIDList, List<String> odtIDList, String conceptID, boolean active) throws Exception
    {
        organizationDataHandler.setConceptActive(tx, orgIDList, odtIDList, conceptID, active);
    }

    public Map<String, String> retrieveConceptIdsByIrdi(List<String> irdis) throws Exception
    {
        return dataHandler.retrieveConceptIdsByIrdi(tx, irdis);
    }

    public Map<String, String> retrieveTermIdsByIrdi(List<String> irdis) throws Exception
    {
        return dataHandler.retrieveTermIdsByIrdi(tx, irdis);
    }

    public Map<String, String> retrieveAbbreviationIdsByIrdi(List<String> irdis) throws Exception
    {
        return dataHandler.retrieveAbbreviationIdsByIrdi(tx, irdis);
    }

    public Map<String, String> retrieveDefinitionIdsByIrdi(List<String> irdis) throws Exception
    {
        return dataHandler.retrieveDefinitionIdsByIrdi(tx, irdis);
    }

    public void generateAbbreviations(T8OntologyConcept concept) throws Exception
    {
        for (T8OntologyTerm term : concept.getTerms())
        {
            generateAbbreviation(term);
        }
    }

    public void generateAbbreviation(T8OntologyTerm ontologyTerm) throws Exception
    {
        Map<String, String> phraseAbbreviations;
        List<String> words;
        String abbreviation;
        String term;

        // Get the term and use the words it contains, to retrieve potential phrase abbreviations.
        term = ontologyTerm.getTerm();
        words = Strings.splitTerms(term);
        phraseAbbreviations = dataHandler.retrievePhraseAbbreviations(tx, ontologyTerm.getLanguageID(), words);

        // Start with the regular term and apply all retrieved abbreviations.
        abbreviation = term;
        for (String phrase : phraseAbbreviations.keySet())
        {
            String phraseAbbreviation;

            phraseAbbreviation = phraseAbbreviations.get(phrase);
            abbreviation = abbreviation.replace(phrase, phraseAbbreviation);
        }

        // Set the final abbreviation for the term if the abbreviation is any any way different from the term.
        if (!abbreviation.equals(term))
        {
            ontologyTerm.addAbbreviation(new T8OntologyAbbreviation(T8IdentifierUtilities.createNewGUID(), ontologyTerm.getID(), abbreviation, false, true));
        }
    }

    public List<T8ConceptPair> retrieveConceptRelationPath(T8DataTransaction tx, List<String> relationIds, String conceptId, String languageId, boolean headToTail, boolean includeOntology, boolean includeTerminology) throws Exception
    {
        List<T8ConceptPair> edges;

        // Retrieve the path edges.
        edges = dataHandler.retrieveConceptRelationPath(tx, relationIds, conceptId, headToTail);

        // Add terminology if required.
        if (includeTerminology)
        {
            T8ConceptTerminologyList terminologyList;
            T8TerminologyApi trmApi;
            Set<String> conceptIds;

            // Create a set of all concepts used by the path edges.
            conceptIds = new HashSet<String>();
            for (T8ConceptPair edge : edges)
            {
                conceptIds.add(edge.getRelationId());
                conceptIds.add(edge.getRightConceptId());
                conceptIds.add(edge.getLeftConceptId());
            }

            // Retrieve the terminology.
            trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            terminologyList = trmApi.retrieveTerminology(ArrayLists.typeSafeList(languageId), new ArrayList<>(conceptIds));

            // Add the terminology to the concept paths.
            for (T8ConceptPair edge : edges)
            {
                edge.addTerminology(terminologyList.getTerminology(edge.getRelationId()));
                edge.addTerminology(terminologyList.getTerminology(edge.getRightConceptId()));
                edge.addTerminology(terminologyList.getTerminology(edge.getLeftConceptId()));
            }
        }

        // Return the edge list.
        return edges;
    }

    public void insertConceptRelations(List<T8ConceptPair> pairList) throws Exception
    {
        for (T8ConceptPair pair : pairList)
        {
            dataHandler.insertConceptRelation(tx, pair.getRelationId(), pair.getLeftConceptId(), pair.getRightConceptId());
        }
    }

    public void saveConceptRelations(List<T8ConceptPair> pairList) throws Exception
    {
        for (T8ConceptPair pair : pairList)
        {
            dataHandler.saveConceptRelation(tx, pair.getRelationId(), pair.getLeftConceptId(), pair.getRightConceptId());
        }
    }

    public void deleteConceptRelations(List<T8ConceptPair> pairList) throws Exception
    {
        for (T8ConceptPair pair : pairList)
        {
            dataHandler.deleteConceptRelation(tx, pair.getRelationId(), pair.getLeftConceptId(), pair.getRightConceptId());
        }
    }

    public List<T8ConceptPair> validateConceptRelations(List<T8ConceptPair> pairList) throws Exception
    {
        return dataHandler.validateConceptRelations(tx, pairList);
    }
}
