package com.pilog.t8.data.document;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.tag.T8TagSet;
import com.pilog.t8.data.tag.T8TagUtilities;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.entity.T8DataEntityFieldIterator;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceDefinition;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementDataSourceDefinition;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementInstanceDataSourceDefinition;
import com.pilog.t8.data.document.datastring.T8DataString;
import com.pilog.t8.data.document.datastring.T8DataStringFilter;
import com.pilog.t8.data.document.datastring.T8DataStringInstance;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.data.document.datastring.T8DataStringInstanceLink;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.datarequirement.T8DataRequirementComment;
import com.pilog.t8.datastructures.tuples.Pair;
import static com.pilog.t8.definition.api.T8OntologyApiResource.E_DATA_RECORD_CODE;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DocumentDataHandler implements T8PerformanceStatisticsProvider
{
    private final T8Context context;
    private T8PerformanceStatistics stats;

    public T8DocumentDataHandler(T8Context context) throws Exception
    {
        this.context = context;
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is always false.  Other clients of the API can set this variable to their own instance of stats.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public DataRequirement retrieveDataRequirement(T8DataTransaction tx, String drID) throws Exception
    {
        Map<String, Object> keyMap;
        String entityIdentifier;
        T8DataEntity drEntity;

        // Construct the key map.
        entityIdentifier = DATA_REQUIREMENT_DOCUMENT_DE_IDENTIFIER;
        keyMap = new HashMap<>();
        keyMap.put(entityIdentifier + T8DataRequirementDataSourceDefinition.KEY_FIELD_IDENTIFIER, drID);

        // Retrieve the entity and return the document it contains.
        drEntity = tx.retrieve(entityIdentifier, keyMap);
        if (drEntity != null)
        {
            return (DataRequirement)drEntity.getFieldValue(entityIdentifier + T8DataRequirementDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);
        }
        else return null;
    }

    public DataRequirementInstance retrieveDataRequirementInstance(T8DataTransaction tx, String drInstanceID) throws Exception
    {
        Map<String, Object> keyMap;
        String entityIdentifier;
        T8DataEntity drInstanceEntity;

        // Construct the key map.
        entityIdentifier = DATA_REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER;
        keyMap = new HashMap<>();
        keyMap.put(entityIdentifier + T8DataRequirementInstanceDataSourceDefinition.KEY_FIELD_IDENTIFIER, drInstanceID);

        // Retrieve the entity and return the document it contains.
        drInstanceEntity = tx.retrieve(entityIdentifier, keyMap);
        if (drInstanceEntity != null)
        {
            return (DataRequirementInstance)drInstanceEntity.getFieldValue(entityIdentifier + T8DataRequirementInstanceDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);
        }
        else return null;
    }

    public List<T8DataRequirementComment> retrieveDataRequirementComments(T8DataTransaction tx, Collection<String> orgIds, Collection<String> languageIds, Collection<String> drIds, Collection<String> drInstanceIds, Collection<String> commentTypeIds) throws Exception
    {
        List<T8DataRequirementComment> commentList;
        List<T8DataEntity> commentEntities;
        T8DataFilter commentFilter;
        T8DataFilterCriteria filterCriteria;
        String entityId;

        // Get the entity identifier.
        entityId =ORG_DR_COMMENT_DE_IDENTIFIER;

        // Create the filter that will be used to fetch all comments required.
        commentFilter = new T8DataFilter(entityId);

        // Add organization criteria.
        if (CollectionUtilities.hasContent(orgIds)) commentFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_ORG_ID, DataFilterOperator.IN, orgIds, false);

        // Add language criteria.
        if (CollectionUtilities.hasContent(languageIds)) commentFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_LANGUAGE_ID, DataFilterOperator.IN, languageIds, false);

        // Add DR Instance criteria.
        if (CollectionUtilities.hasContent(drInstanceIds))
        {
            filterCriteria = new T8DataFilterCriteria();
            filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DR_INSTANCE_ID, DataFilterOperator.IN, drInstanceIds, false);
            filterCriteria.addFilterClause(DataFilterConjunction.OR, entityId + EF_DR_INSTANCE_ID, DataFilterOperator.IS_NULL, drInstanceIds, false);
            commentFilter.addFilterCriteria(DataFilterConjunction.AND, filterCriteria);
        }

        // Add DR criteria.
        commentFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_DR_ID, DataFilterOperator.IN, drIds, false);

        // Add comment type criteria.
        if (CollectionUtilities.hasContent(commentTypeIds)) commentFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_COMMENT_TYPE_ID, DataFilterOperator.IN, commentTypeIds, false);

        // Retrieve the comments and create a comment object of each.
        commentEntities = tx.select(entityId, commentFilter);
        commentList = new ArrayList<>();
        for (T8DataEntity commentEntity : commentEntities)
        {
            T8DataRequirementComment comment;

            comment = new T8DataRequirementComment();
            comment.setId((String)commentEntity.getFieldValue(entityId + EF_COMMENT_ID));
            comment.setTypeId((String)commentEntity.getFieldValue(entityId + EF_COMMENT_TYPE_ID));
            comment.setDrId((String)commentEntity.getFieldValue(entityId + EF_DR_ID));
            comment.setDrInstanceId((String)commentEntity.getFieldValue(entityId + EF_DR_INSTANCE_ID));
            comment.setLanguageId((String)commentEntity.getFieldValue(entityId + EF_LANGUAGE_ID));
            comment.setPropertyId((String)commentEntity.getFieldValue(entityId + EF_PROPERTY_ID));
            comment.setFieldId((String)commentEntity.getFieldValue(entityId + EF_FIELD_ID));
            comment.setComment((String)commentEntity.getFieldValue(entityId + EF_COMMENT));

            commentList.add(comment);
        }

        // Return the list of comments.
        return commentList;
    }

    public DataRecord retrieveDataRecord(T8DataTransaction tx, String recordID, boolean includeDescendants) throws Exception
    {
        Map<String, Object> keyMap;
        String entityIdentifier;
        T8DataEntity recordEntity;

        // Construct the key map.
        entityIdentifier = DATA_RECORD_DOCUMENT_DE_IDENTIFIER;
        keyMap = new HashMap<>();
        keyMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER, recordID);

        // Retrieve the entity and return the document it contains.
        recordEntity = tx.retrieve(entityIdentifier, keyMap);
        if (recordEntity != null)
        {
            DataRecord dataRecord;

            // Get the retrieved document.
            dataRecord = (DataRecord)recordEntity.getFieldValue(entityIdentifier + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);

            // Add all descendants if required.
            if ((dataRecord != null) && (includeDescendants))
            {
                for (String subRecordID : dataRecord.getSubRecordReferenceIDSet(true, false))
                {
                    DataRecord subRecord;

                    subRecord = retrieveDataRecord(tx, subRecordID, true);
                    if (subRecord != null) dataRecord.addSubRecord(subRecord);
                }
            }

            // Return the retrieved document.
            return dataRecord;
        }
        else return null;
    }

    public DataRecord retrieveDataFileByContentRecord(T8DataTransaction tx, String recordID) throws Exception
    {
        Map<String, Object> keyMap;
        String entityIdentifier;
        T8DataEntity recordEntity;

        // Construct the key map.
        entityIdentifier = DATA_RECORD_DE_IDENTIFIER;
        keyMap = new HashMap<>();
        keyMap.put(entityIdentifier + EF_RECORD_ID, recordID);

        // Retrieve the entity and return the data file containing the specified record.
        recordEntity = tx.retrieve(entityIdentifier, keyMap);
        if (recordEntity != null)
        {
            String rootRecordID;

            rootRecordID = (String)recordEntity.getFieldValue(entityIdentifier + EF_ROOT_RECORD_ID);
            return retrieveDataFile(tx, rootRecordID);
        }
        else return null;
    }

    public DataRecord retrieveDataFile(T8DataTransaction tx, String fileID) throws Exception
    {
        Map<String, Object> keyMap;
        String entityIdentifier;
        T8DataEntity recordEntity;

        // Construct the key map.
        entityIdentifier = DATA_RECORD_DOCUMENT_ROOT_DE_IDENTIFIER;
        keyMap = new HashMap<>();
        keyMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER, fileID);

        // Retrieve the entity and return the document it contains.
        recordEntity = tx.retrieve(entityIdentifier, keyMap);
        if (recordEntity != null)
        {
            DataRecord dataRecord;

            // Get the retrieved document.
            dataRecord = (DataRecord)recordEntity.getFieldValue(entityIdentifier + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);

            // Return the retrieved document.
            return dataRecord;
        }
        else return null;
    }

    public Set<String> retrieveDataFileRecordIDSet(T8DataTransaction tx, String fileID) throws Exception
    {
        List<T8DataEntity> recordEntities;
        Map<String, Object> keyMap;
        String entityIdentifier;
        Set<String> recordIDSet;

        // Construct the key map.
        entityIdentifier = DATA_RECORD_DE_IDENTIFIER;
        keyMap = new HashMap<>();
        keyMap.put(entityIdentifier + EF_ROOT_RECORD_ID, fileID);

        // Retrieve the entity and return the document it contains.
        recordIDSet = new HashSet<String>();
        recordEntities = tx.select(entityIdentifier, keyMap);
        for (T8DataEntity recordEntity : recordEntities)
        {
            String recordID;

            // Get the retrieved document record ID and add it to the set.
            recordID = (String)recordEntity.getFieldValue(entityIdentifier + EF_RECORD_ID);
            recordIDSet.add(recordID);
        }

        // Return the ID set.
        return recordIDSet;
    }

    public T8AttachmentDetails retrieveAttachmentDetails(T8DataTransaction tx, String attachmentId) throws Exception
    {
        T8DataFilter dataFilter;
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        String entityId;

        // Get the entity identifier.
        entityId = DATA_RECORD_ATTACHMENT_DETAILS_DE_IDENTIFIER;

        // Create a data filter to use for retrieving the attachment data.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_ATTACHMENT_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, attachmentId));
        dataFilter = new T8DataFilter(entityId, filterCriteria);

        // Get the data session and retrieve the entities required.
        entityList = tx.select(entityId, dataFilter);
        if (entityList.size() > 0)
        {
            T8DataEntity attachmentEntity;
            String fileContextId;
            String filePath;
            String originalFileName;
            String mediaType;
            String checksum;
            long fileSize;

            attachmentEntity = entityList.get(0);
            fileContextId = (String)attachmentEntity.getFieldValue(entityId + EF_FILE_CONTEXT_ID);
            originalFileName = (String)attachmentEntity.getFieldValue(entityId + EF_FILE_NAME);
            mediaType = (String)attachmentEntity.getFieldValue(entityId + EF_MEDIA_TYPE);
            checksum = (String)attachmentEntity.getFieldValue(entityId + EF_MD5_CHECKSUM);
            fileSize = (Long)attachmentEntity.getFieldValue(entityId + EF_FILE_SIZE);
            filePath = attachmentId; // Attachments stored using id as filename;

            return new T8AttachmentDetails(attachmentId, null, fileContextId, filePath, originalFileName, fileSize, checksum, mediaType);
        }
        else return null;
    }

    public Map<String, T8AttachmentDetails> retrieveDataRecordAttachmentDetails(T8DataTransaction tx, String recordId) throws Exception
    {
        Map<String, T8AttachmentDetails> attachmentDetails;
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        T8DataFilter dataFilter;
        String entityId;

        // Get the entity identifier.
        entityId = DATA_RECORD_ATTACHMENT_DETAILS_DE_IDENTIFIER;

        // Create a data filter to use for retrieving the attachment data.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RECORD_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, recordId));
        dataFilter = new T8DataFilter(entityId, filterCriteria);

        // Get the data session and retrieve the entities required.
        attachmentDetails = new HashMap<>();
        entityList = tx.select(entityId, dataFilter);
        for (T8DataEntity entity : entityList)
        {
            String attachmentId;
            String fileContextId;
            String originalFileName;
            String mediaType;
            String checksum;
            String filePath;
            long fileSize;

            attachmentId = (String)entity.getFieldValue(entityId + EF_ATTACHMENT_ID);
            fileContextId = (String)entity.getFieldValue(entityId + EF_FILE_CONTEXT_ID);
            originalFileName = (String)entity.getFieldValue(entityId + EF_FILE_NAME);
            mediaType = (String)entity.getFieldValue(entityId + EF_MEDIA_TYPE);
            checksum = (String)entity.getFieldValue(entityId + EF_MD5_CHECKSUM);
            fileSize = (Long)entity.getFieldValue(entityId + EF_FILE_SIZE);
            filePath = attachmentId; // Attachment stored using id as filename.

            attachmentDetails.put(attachmentId, new T8AttachmentDetails(attachmentId, null, fileContextId, filePath, originalFileName, fileSize, checksum, mediaType));
        }

        return attachmentDetails;
    }

    public List<T8DataString> retrieveDataStrings(T8DataTransaction tx, Collection<T8DataStringFilter> dataStringFilters) throws Exception
    {
        T8DataFilter dataFilter;
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        List<T8DataString> descriptionHandles;
        String entityIdentifier;

        // Get the entity to use.
        entityIdentifier = DATA_STRING_DETAILS_DE_IDENTIFIER;

        // Use the description filters to add 1 or more data filter criteria objects.
        filterCriteria = new T8DataFilterCriteria();
        for (T8DataStringFilter descriptionFilter : dataStringFilters)
        {
            T8DataFilterCriteria groupCriteria;

            groupCriteria = new T8DataFilterCriteria();
            if (CollectionUtilities.hasContent(descriptionFilter.getRecordIDList())) groupCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + EF_RECORD_ID, T8DataFilterCriterion.DataFilterOperator.IN, descriptionFilter.getRecordIDList()));
            if (CollectionUtilities.hasContent(descriptionFilter.getLanguageIDList())) groupCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + EF_LANGUAGE_ID, T8DataFilterCriterion.DataFilterOperator.IN, descriptionFilter.getLanguageIDList()));
            if (CollectionUtilities.hasContent(descriptionFilter.getTypeIDList())) groupCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + EF_DATA_STRING_TYPE_ID, T8DataFilterCriterion.DataFilterOperator.IN, descriptionFilter.getTypeIDList()));
            if (CollectionUtilities.hasContent(descriptionFilter.getInstanceIDList())) groupCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + EF_DATA_STRING_INSTANCE_ID, T8DataFilterCriterion.DataFilterOperator.IN, descriptionFilter.getInstanceIDList()));
            filterCriteria.addFilterClause(DataFilterConjunction.OR, groupCriteria);
        }

        // Construct the data filter.
        dataFilter = new T8DataFilter(entityIdentifier, filterCriteria);

        // Get the selected data and package it in a list of description handles.
        descriptionHandles = new ArrayList<>();
        entityList = tx.select(entityIdentifier, dataFilter);
        for (T8DataEntity entity : entityList)
        {
            T8DataString descriptionHandle;
            String description;
            String recordID;
            String typeID;
            String instanceID;
            String languageID;

            recordID = (String)entity.getFieldValue(entityIdentifier + EF_RECORD_ID);
            instanceID = (String)entity.getFieldValue(entityIdentifier + EF_DATA_STRING_INSTANCE_ID);
            typeID = (String)entity.getFieldValue(entityIdentifier + EF_DATA_STRING_TYPE_ID);
            languageID = (String)entity.getFieldValue(entityIdentifier + EF_LANGUAGE_ID);
            description = (String)entity.getFieldValue(entityIdentifier + EF_DATA_STRING);

            descriptionHandle = new T8DataString(recordID, description);
            descriptionHandle.setInstanceID(instanceID);
            descriptionHandle.setTypeID(typeID);
            descriptionHandle.setLanguageID(languageID);
            descriptionHandles.add(descriptionHandle);
        }

        return descriptionHandles;
    }

    public List<T8DataRecordValueString> retrieveValueStrings(T8DataTransaction tx, Collection<String> recordIDList) throws Exception
    {
        T8DataFilter dataFilter;
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        List<T8DataRecordValueString> valueStrings;
        String recordDetailsEntityID;

        // Get the entity identifier.
        recordDetailsEntityID = DATA_RECORD_DE_IDENTIFIER;

        // Create a data filter to use for retrieving the value Strings.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(recordDetailsEntityID + EF_RECORD_ID, DataFilterOperator.IN, (List)recordIDList));
        dataFilter = new T8DataFilter(recordDetailsEntityID, filterCriteria);

        // Get the data session and retrieve the entities required.
        valueStrings = new ArrayList<>();
        entityList = tx.select(recordDetailsEntityID, dataFilter);
        for (T8DataEntity entity : entityList)
        {
            T8DataRecordValueString recordValueString;
            String recordID;
            String valueString;

            recordID = (String)entity.getFieldValue(recordDetailsEntityID + EF_RECORD_ID);
            valueString = (String)entity.getFieldValue(recordDetailsEntityID + EF_VALUE_STRING);

            recordValueString = new T8DataRecordValueString(recordID, valueString);
            valueStrings.add(recordValueString);
        }

        return valueStrings;
    }

    public List<String> retrieveRecordIdByCode(T8DataTransaction tx, String rootRecordId, String dataRequirementId, String dataRequirementInstanceId, String codeTypeId, String code) throws Exception
    {
        List<T8DataEntity> entities;
        T8DataFilter dataFilter;
        List<String> recordIdList;

        // Check if the required values are supplied
        // Required field:
        //  * code
        // At least one of the following is required:
        //  * rootRecordId
        //  * dataRequirementId
        //  * dataRequirementInstanceId
        // Optional field:
        //  * codeTypeId
        if (code == null || (rootRecordId == null && dataRequirementId == null && dataRequirementInstanceId == null))
        {
            throw new IllegalArgumentException("The code value is required, and at least on of the following is required:\nRoot Record Id\nData Requirement Id\nData Requiremnet Instance Id");
        }

        // Construct the filter.
        dataFilter = new T8DataFilter(E_DATA_RECORD_CODE);
        dataFilter.addFilterCriterion(E_DATA_RECORD_CODE + EF_CODE, code);
        if (rootRecordId != null) dataFilter.addFilterCriterion(E_DATA_RECORD_CODE + EF_ROOT_RECORD_ID, rootRecordId);
        if (dataRequirementId != null) dataFilter.addFilterCriterion(E_DATA_RECORD_CODE + EF_DR_ID, dataRequirementId);
        if (dataRequirementInstanceId != null) dataFilter.addFilterCriterion(E_DATA_RECORD_CODE + EF_DR_INSTANCE_ID, dataRequirementInstanceId);
        if (codeTypeId != null) dataFilter.addFilterCriterion(E_DATA_RECORD_CODE + EF_CODE_TYPE_ID, codeTypeId);

        // Construct the entity
        recordIdList = new ArrayList<>();
        entities = tx.select(E_DATA_RECORD_CODE, dataFilter);

        for (T8DataEntity entity : entities)
        {
            recordIdList.add((String)entity.getFieldValue(E_DATA_RECORD_CODE + EF_RECORD_ID));
        }

        return recordIdList;
    }

    public String retrieveDataRecordDRInstanceID(T8DataTransaction tx, String recordID) throws Exception
    {
        T8DataEntity recordDetailsEntity;
        String recordDetailsEntityID;

        // Get the entity identifier.
        recordDetailsEntityID = DATA_RECORD_DE_IDENTIFIER;

        // Get the data session and retrieve the entities required.
        recordDetailsEntity = tx.retrieve(recordDetailsEntityID, HashMaps.newHashMap(recordDetailsEntityID + EF_RECORD_ID, recordID));
        if (recordDetailsEntity != null)
        {
            return (String)recordDetailsEntity.getFieldValue(recordDetailsEntityID + EF_DR_INSTANCE_ID);
        }
        else throw new RuntimeException("Data Record not found: " + recordID);
    }

    public Set<String> retrieveDataRecordDescendantIDSet(T8DataTransaction tx, String rootRecordID) throws Exception
    {
        List<T8DataEntity> recordEntities;
        String recordDetailsEntityID;
        Set<String> recordIDSet;

        // Get the entity identifier.
        recordDetailsEntityID = DATA_RECORD_DE_IDENTIFIER;

        // Retrieve the records and add all descendant record ID's to the set.
        recordIDSet = new HashSet<>();
        recordEntities = tx.select(recordDetailsEntityID, HashMaps.newHashMap(recordDetailsEntityID + EF_ROOT_RECORD_ID, rootRecordID));
        for (T8DataEntity recordEntity : recordEntities)
        {
            String descendantRecordID;

            descendantRecordID = (String)recordEntity.getFieldValue(recordDetailsEntityID + EF_RECORD_ID);
            if (descendantRecordID != null) recordIDSet.add(descendantRecordID);
        }

        // Return the set of descendant record ID's.
        return recordIDSet;
    }

    public Set<String> retrieveDataRecordAncestorIDSet(T8DataTransaction tx, String recordID) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Retrieves the Data String Instance Links for the specified DR Instances.
     * This method returns a map with entries consisting of a Data String Instance Link
     * object as key and the corresponding Data String Instance as value.  The
     * {@code T8DataStringInstance} value objects may be shared among multiple entries if
     * more than one link points to the same Data String Instance.
     * @param tx The transaction to be used for this operation.
     * @param rootOrgId The root organization for which to retrieve the links.
     * @param drInstanceIds The DR Instances for which to retrieve links.  If null, all links to all DR Instances will be retrieved.
     * @return The map of retrieved Data String Instance Links.
     * @throws Exception
     */
    public List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> retrieveDataStringInstanceLinkPairs(T8DataTransaction tx, String rootOrgId, Collection<String> drInstanceIds) throws Exception
    {
        List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> instanceLinks;
        Map<String, T8DataStringInstance> dsInstances;
        T8DataFilter filter;
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = DATA_STRING_INSTANCE_LINK_DETAILS_DE_IDENTIFIER;

        // Create a map to filter out only the required instances.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + EF_ROOT_ORG_ID, DataFilterOperator.EQUAL, rootOrgId));
        if (CollectionUtilities.hasContent(drInstanceIds)) filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + EF_DR_INSTANCE_ID, DataFilterOperator.IN, drInstanceIds));
        filter = new T8DataFilter(entityIdentifier, filterCriteria);

        // Make sure to order by the sequence value.
        filter.addFieldOrdering(entityIdentifier + EF_SEQUENCE, T8DataFilter.OrderMethod.ASCENDING);

        // Select the instances and pack them all into the result map.
        dsInstances = new HashMap<>();
        instanceLinks = new ArrayList<>();
        entityList = tx.select(entityIdentifier, filter);
        for (T8DataEntity entity : entityList)
        {
            T8DataStringInstanceLink dsInstanceLink;
            T8DataStringInstance dsInstance;
            String orgId;
            String drInstanceId;
            String languageId;
            String dsTypeId;
            String dsInstanceId;
            String autoRenderFlag;
            String conditionExpression;
            String dataKeyExpression;
            Integer sequence;

            // Get the data from the entity.
            orgId = (String)entity.getFieldValue(entityIdentifier + EF_ORG_ID);
            drInstanceId = (String)entity.getFieldValue(entityIdentifier + EF_DR_INSTANCE_ID);
            languageId = (String)entity.getFieldValue(entityIdentifier + EF_LANGUAGE_ID);
            dsInstanceId = (String)entity.getFieldValue(entityIdentifier + EF_DATA_STRING_INSTANCE_ID);
            dsTypeId = (String)entity.getFieldValue(entityIdentifier + EF_DATA_STRING_TYPE_ID);
            autoRenderFlag = (String)entity.getFieldValue(entityIdentifier + EF_AUTO_RENDER);
            conditionExpression = (String)entity.getFieldValue(entityIdentifier + EF_CONDITION_EXPRESSION);
            dataKeyExpression = (String)entity.getFieldValue(entityIdentifier + EF_DATA_KEY_EXPRESSION);
            sequence = (Integer)entity.getFieldValue(entityIdentifier + EF_SEQUENCE);

            // Create the Data String Instance link object.
            dsInstanceLink = new T8DataStringInstanceLink(drInstanceId);
            dsInstanceLink.setDsInstanceId(dsInstanceId);
            dsInstanceLink.setConditionExpression(conditionExpression);
            dsInstanceLink.setDataKeyExpression(dataKeyExpression);
            dsInstanceLink.setSequence(sequence != null ? sequence : 0);

            // Get the Data String instance to which the link refers (create it if it does not yet exist).
            dsInstance = dsInstances.get(dsInstanceId);
            if (dsInstance == null)
            {
                dsInstance = new T8DataStringInstance(dsInstanceId, dsTypeId, languageId, ("Y".equalsIgnoreCase(autoRenderFlag)));
            }

            // Add the organization to the instance.
            dsInstance.addOrganizationLink(orgId);

            // Add link to result list.
            instanceLinks.add(new Pair<>(dsInstanceLink, dsInstance));
        }

        // Return the complete ist of links.
        return instanceLinks;
    }

    public void saveDataRequirement(T8DataTransaction tx, DataRequirement dataRequirement) throws Exception
    {
        Map<String, Object> fieldMap;
        String entityIdentifier;
        T8DataEntity drEntity;

        // Construct the key map.
        entityIdentifier = DATA_REQUIREMENT_DOCUMENT_DE_IDENTIFIER;
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + T8DataRequirementDataSourceDefinition.KEY_FIELD_IDENTIFIER, dataRequirement.getRequirementType());
        fieldMap.put(entityIdentifier + T8DataRequirementDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER, dataRequirement);

        // Save the entity (Documents can be updated even if they do not exist - the source will insert/update where necessary).
        drEntity = tx.create(entityIdentifier, fieldMap);
        tx.update(drEntity);
    }

    public void saveDataRequirementInstance(T8DataTransaction tx, DataRequirementInstance drInstance) throws Exception
    {
        Map<String, Object> fieldMap;
        String entityIdentifier;
        T8DataEntity drInstanceEntity;

        // Construct the key map.
        entityIdentifier = DATA_REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER;
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + T8DataRequirementInstanceDataSourceDefinition.KEY_FIELD_IDENTIFIER, drInstance.getRequirementType());
        fieldMap.put(entityIdentifier + T8DataRequirementInstanceDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER, drInstance);

        // Save the entity (Documents can be updated even if they do not exist - the source will insert/update where necessary).
        drInstanceEntity = tx.create(entityIdentifier, fieldMap);
        tx.update(drInstanceEntity);
    }

    public void insertDataRecord(T8DataTransaction tx, DataRecord dataRecord) throws Exception
    {
        Map<String, Object> fieldMap;
        String entityIdentifier;
        T8DataEntity recordEntity;

        // Construct the key map.
        entityIdentifier = DATA_RECORD_DOCUMENT_DE_IDENTIFIER;
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER, dataRecord.getID());
        fieldMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER, dataRecord);

        // Save the entity (Documents can be updated even if they do not exist - the source will insert/update where necessary).
        recordEntity = tx.create(entityIdentifier, fieldMap);
        tx.insert(recordEntity);
    }

    public void saveDataRecord(T8DataTransaction tx, DataRecord dataRecord) throws Exception
    {
        Map<String, Object> fieldMap;
        String entityIdentifier;
        T8DataEntity recordEntity;

        // Construct the key map.
        entityIdentifier = DATA_RECORD_DOCUMENT_DE_IDENTIFIER;
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER, dataRecord.getID());
        fieldMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER, dataRecord);

        // Save the entity (Documents can be updated even if they do not exist - the source will insert/update where necessary).
        recordEntity = tx.create(entityIdentifier, fieldMap);
        tx.update(recordEntity);
    }

    public boolean deleteDataRequirement(T8DataTransaction tx, String drID) throws Exception
    {
        Map<String, Object> fieldMap;
        String entityIdentifier;
        T8DataEntity drEntity;

        // Construct the key map.
        entityIdentifier = DATA_REQUIREMENT_DOCUMENT_DE_IDENTIFIER;
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + T8DataRequirementDataSourceDefinition.KEY_FIELD_IDENTIFIER, drID);

        // Delete the entity.
        drEntity = tx.create(entityIdentifier, fieldMap);
        return tx.delete(drEntity);
    }

    public boolean deleteDataRequirementInstance(T8DataTransaction tx, String drInstanceID) throws Exception
    {
        Map<String, Object> fieldMap;
        String entityIdentifier;
        T8DataEntity drInstanceEntity;

        // Construct the key map.
        entityIdentifier = DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER;
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + EF_DR_INSTANCE_ID, drInstanceID);

        // Delete the entity.
        drInstanceEntity = tx.create(entityIdentifier, fieldMap);
        return tx.delete(drInstanceEntity);
    }

    public boolean deleteDataRecord(T8DataTransaction tx, String recordID) throws Exception
    {
        Map<String, Object> fieldMap;
        String entityIdentifier;
        T8DataEntity recordEntity;

        // Construct the key map.
        entityIdentifier = DATA_RECORD_DOCUMENT_DE_IDENTIFIER;
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER, recordID);

        // Delete the entity.
        recordEntity = tx.create(entityIdentifier, fieldMap);
        return tx.delete(recordEntity);
    }

    public int deleteDataStrings(T8DataTransaction tx, String recordID) throws Exception
    {
        String entityIdentifier;
        T8DataFilter dataFilter;

        // Get the entity to use.
        entityIdentifier = DATA_STRING_DE_IDENTIFIER;

        // Construct the data filter.
        dataFilter = new T8DataFilter(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_RECORD_ID, recordID));

        // Get the selected data and package it in a list of description handles.
        return tx.delete(entityIdentifier, dataFilter);
    }

    public void insertDataRequirementInstance(T8DataTransaction tx, String drInstanceID, String drID, boolean independent) throws Exception
    {
        Map<String, Object> fieldMap;
        String entityIdentifier;
        T8DataEntity drInstanceEntity;

        // Construct the key map.
        entityIdentifier = DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER;
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + EF_DR_INSTANCE_ID, drInstanceID);
        fieldMap.put(entityIdentifier + EF_DR_ID, drID);
        fieldMap.put(entityIdentifier + EF_INDEPENDENT_INDICATOR, independent ? "Y" : "N");

        // Delete the entity.
        drInstanceEntity = tx.create(entityIdentifier, fieldMap);
        tx.insert(drInstanceEntity);
    }

    public boolean updateDataRequirementInstance(T8DataTransaction tx, String drInstanceID, String drID, boolean independent) throws Exception
    {
        Map<String, Object> fieldMap;
        String entityIdentifier;
        T8DataEntity drInstanceEntity;

        // Construct the key map.
        entityIdentifier = DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER;
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + EF_DR_INSTANCE_ID, drInstanceID);
        fieldMap.put(entityIdentifier + EF_DR_ID, drID);
        fieldMap.put(entityIdentifier + EF_INDEPENDENT_INDICATOR, independent ? "Y" : "N");

        // Delete the entity.
        drInstanceEntity = tx.create(entityIdentifier, fieldMap);
        return tx.update(drInstanceEntity);
    }

    public void insertDataString(T8DataTransaction tx, String recordID, String dataStringInstanceID, String dataString) throws Exception
    {
        T8DataEntity descriptionEntity;
        String entityIdentifier;

        // Create an entity.
        entityIdentifier = DATA_STRING_DE_IDENTIFIER;
        descriptionEntity = tx.create(entityIdentifier, null);
        descriptionEntity.setFieldValue(entityIdentifier + EF_RECORD_ID, recordID);
        descriptionEntity.setFieldValue(entityIdentifier + EF_DATA_STRING_INSTANCE_ID, dataStringInstanceID);
        descriptionEntity.setFieldValue(entityIdentifier + EF_DATA_STRING, dataString);

        // Insert the entity.
        tx.insert(descriptionEntity);
    }

    public void saveDataString(T8DataTransaction tx, String recordID, String dataStringInstanceID, String dataString) throws Exception
    {
        T8DataEntity dataStringEntity;
        String entityIdentifier;

        // Create an entity.
        entityIdentifier = DATA_STRING_DE_IDENTIFIER;
        dataStringEntity = tx.create(entityIdentifier, null);
        dataStringEntity.setFieldValue(entityIdentifier + EF_RECORD_ID, recordID);
        dataStringEntity.setFieldValue(entityIdentifier + EF_DATA_STRING_INSTANCE_ID, dataStringInstanceID);
        dataStringEntity.setFieldValue(entityIdentifier + EF_DATA_STRING, dataString);

        // Save the entity.
        if (!tx.update(dataStringEntity))
        {
            tx.insert(dataStringEntity);
        }
    }

    public void saveDataRequirementInstance(T8DataTransaction tx, String drInstanceID, String drID, boolean independent) throws Exception
    {
        String entityIdentifier;
        T8DataEntity drInstanceEntity;

        // Construct the key map.
        entityIdentifier = DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER;
        drInstanceEntity = tx.create(entityIdentifier, null);
        drInstanceEntity.setFieldValue(entityIdentifier + EF_DR_INSTANCE_ID, drInstanceID);
        drInstanceEntity.setFieldValue(entityIdentifier + EF_DR_ID, drID);
        drInstanceEntity.setFieldValue(entityIdentifier + EF_INDEPENDENT_INDICATOR, independent ? "Y" : "N");

        // Save the entity.
        if (!tx.update(drInstanceEntity))
        {
            tx.insert(drInstanceEntity);
        }
    }

    public List<T8TagSet> retrieveDataRecordTagSets(T8DataTransaction tx, Collection<String> recordIDList) throws Exception
    {
        T8DataFilter filter;
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        List<T8TagSet> tagSetList;
        String entityIdentifier;

        // Get the entity identifier from the model definition.
        entityIdentifier = DATA_RECORD_DE_IDENTIFIER;

        // Create a map to filter out only the required records.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + EF_RECORD_ID, DataFilterOperator.IN, recordIDList));
        filter = new T8DataFilter(entityIdentifier, filterCriteria);

        // Select the entities and pack all tag sets in the result list.
        tagSetList = new ArrayList<>();
        entityList = tx.select(entityIdentifier, filter);
        for (T8DataEntity entity : entityList)
        {
            tagSetList.add(T8TagUtilities.parseTags((String)entity.getFieldValue(entityIdentifier + EF_TAGS)));
        }

        // Return the list of tag sets.
        return tagSetList;
    }

    public int countDataRecords(T8DataTransaction tx, String drInstanceID, String drID, List<PropertyValueStringCriterion> criteria) throws Exception
    {
        T8DataRecordFilterConstructor filterConstructor;
        T8DataFilter matchFilter;
        T8DataFilterCriteria matchFilterCriteria;
        String matchEntityIdentifier;

        // Get the entity identifier.
        matchEntityIdentifier = DATA_RECORD_DE_IDENTIFIER;

        // Create a data filter to use for retrieving the match results.
        filterConstructor = new T8DataRecordFilterConstructor();
        matchFilterCriteria = new T8DataFilterCriteria();
        matchFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(matchEntityIdentifier + EF_DR_INSTANCE_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, drInstanceID));
        matchFilter = new T8DataFilter(matchEntityIdentifier, matchFilterCriteria);
        filterConstructor.addPropertySubDataFilter(DataFilterConjunction.AND, matchFilter, matchEntityIdentifier + EF_RECORD_ID, drID, criteria);

        // Return the record count.
        return tx.count(matchEntityIdentifier, matchFilter);
    }

    public T8DataIterator<String> getDataRecordIDIterator(T8DataTransaction tx, String drInstanceID, String drID, List<PropertyValueStringCriterion> criteria) throws Exception
    {
        T8DataRecordFilterConstructor filterConstructor;
        T8DataFilter matchFilter;
        T8DataFilterCriteria matchFilterCriteria;
        String matchEntityIdentifier;

        // Get the entity identifier.
        matchEntityIdentifier = DATA_RECORD_DE_IDENTIFIER;

        // Create a data filter to use for retrieving the match results.
        filterConstructor = new T8DataRecordFilterConstructor();
        matchFilterCriteria = new T8DataFilterCriteria();
        matchFilterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(matchEntityIdentifier + EF_DR_INSTANCE_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, drInstanceID));
        matchFilter = new T8DataFilter(matchEntityIdentifier, matchFilterCriteria);
        filterConstructor.addPropertySubDataFilter(DataFilterConjunction.AND, matchFilter, matchEntityIdentifier + EF_RECORD_ID, drID, criteria);

        // Return a Record ID iterator.
        return new T8DataEntityFieldIterator(tx, tx.scroll(matchEntityIdentifier, matchFilter), matchEntityIdentifier + EF_RECORD_ID);
    }

    public int flagAsyncOntologyUpdateByRecordId(T8DataTransaction tx, Collection<String> recordIdList) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        T8DataFilter filter;
        String entityId;

        // Set the entity id to use.
        entityId = DATA_RECORD_DE_IDENTIFIER;

        // Create a map to filter out only the target records.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_RECORD_ID, DataFilterOperator.IN, recordIdList));
        filter = new T8DataFilter(entityId, filterCriteria);

        // Update the entities and return the update count;
        return tx.update(entityId, HashMaps.newHashMap(entityId + EF_ASYNC_ONT_UPD_TIME, new T8Timestamp(System.currentTimeMillis())), filter);
    }

    public int flagAsyncOntologyUpdateByDrId(T8DataTransaction tx, Collection<String> drIdList) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        String entityId;
        T8DataFilter filter;

        // Set the entity id to use.
        entityId = DATA_RECORD_DE_IDENTIFIER;

        // Create a map to filter out only the target records.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_DR_ID, DataFilterOperator.IN, drIdList));
        filter = new T8DataFilter(entityId, filterCriteria);

        // Update the entities and return the update count;
        return tx.update(entityId, HashMaps.newHashMap(entityId + EF_ASYNC_ONT_UPD_TIME, new T8Timestamp(System.currentTimeMillis())), filter);
    }

    public int flagAsyncOntologyUpdateByDrInstanceId(T8DataTransaction tx, Collection<String> drInstanceIdList) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        String entityId;
        T8DataFilter filter;

        // Set the entity id to use.
        entityId = DATA_RECORD_DE_IDENTIFIER;

        // Create a map to filter out only the target records.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_DR_INSTANCE_ID, DataFilterOperator.IN, drInstanceIdList));
        filter = new T8DataFilter(entityId, filterCriteria);

        // Update the entities and return the update count;
        return tx.update(entityId, HashMaps.newHashMap(entityId + EF_ASYNC_ONT_UPD_TIME, new T8Timestamp(System.currentTimeMillis())), filter);
    }
}
