package com.pilog.t8.data.document.datarecord.view;

import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.definition.data.document.datarecord.view.T8DocPathViewConstructorDefinition;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DocPathDataRecordViewConstructor implements T8DataRecordViewConstructor
{
    private final T8DataTransaction tx;
    private final TerminologyProvider terminologyProvider;
    private final OntologyProvider ontologyProvider;
    private final String identifier;
    private final DocPathExpressionEvaluator expressionEvaluator;

    public T8DocPathDataRecordViewConstructor(T8DataTransaction tx, T8DocPathViewConstructorDefinition definition)
    {
        this.tx = tx;
        this.terminologyProvider = ((T8TerminologyApi)tx.getApi(T8TerminologyApi.API_IDENTIFIER)).getTerminologyProvider();
        this.ontologyProvider = ((T8OntologyApi)tx.getApi(T8OntologyApi.API_IDENTIFIER)).getOntologyProvider();
        this.identifier = definition.getIdentifier();
        this.expressionEvaluator = new DocPathExpressionEvaluator();
        this.expressionEvaluator.parseExpression(definition.getDocPathExpression());
    }

    @Override
    public Map<String, Object> constructViewData(DataRecord inputRecord, T8DataFileAlteration alteration, String languageID)
    {
        Map<String, Object> dataValues;
        Object result;

        // Set the terminology provider to use when evaluating the expression.
        terminologyProvider.setLanguage(languageID);
        expressionEvaluator.setTerminologyProvider(terminologyProvider);

        // Set the ontology provider to use when evaluating the expression.
        expressionEvaluator.setOntologyProvider(ontologyProvider);

        // Create the result map.
        dataValues = new HashMap<String, Object>();

        // Evaluate the expression and add the result to the map.
        result = expressionEvaluator.evaluateExpression(inputRecord);
        if (result instanceof List)
        {
            throw new RuntimeException("DocPath for data extractor '" + identifier + "' returns list instead of expected single value: " + result);
        }
        else
        {
            dataValues.put(identifier, result);
            return dataValues;
        }
    }
}
