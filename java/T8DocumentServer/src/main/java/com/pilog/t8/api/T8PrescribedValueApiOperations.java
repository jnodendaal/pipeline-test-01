package com.pilog.t8.api;

import com.pilog.t8.data.document.datarequirement.PrescribedValue;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8PrescribedValueApiResource.*;

/**
 * @author Andre Scheepers
 */
public class T8PrescribedValueApiOperations
{
    public static class ApiReplacePrescribedValueReferences extends T8DefaultServerOperation
    {
        public ApiReplacePrescribedValueReferences(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Integer> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<String> valueIds;
            String replacementValueID;
            T8PrescribedValueApi pvApi;
            Integer referencesReplaced;

            // Get the operation parameters we need.
            valueIds = (List<String>)operationParameters.get(PARAMETER_VALUE_ID_LIST);
            replacementValueID = (String) operationParameters.get(PARAMETER_REPLACEMENT_VALUE_ID);

            // Perform the API operation.
            pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);
            referencesReplaced = pvApi.replacePrescribedValueReferences(valueIds, replacementValueID);
            return HashMaps.createSingular(PARAMETER_REFERENCE_UPDATE_COUNT, referencesReplaced);
        }
    }

    public static class ApiReplacePrescribedValues extends T8DefaultServerOperation
    {
        public ApiReplacePrescribedValues(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Integer> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8PrescribedValueApi pvApi;
            List<String> valueIds;
            Integer referencesReplaced;
            String replacementValueId;

            // Get the operation parameters we need.
            valueIds = (List<String>)operationParameters.get(PARAMETER_VALUE_ID_LIST);
            replacementValueId = (String) operationParameters.get(PARAMETER_REPLACEMENT_VALUE_ID);

            // Perform the API operation.
            pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);
            referencesReplaced = pvApi.replacePrescribedValues(valueIds, replacementValueId);
            return HashMaps.createSingular(PARAMETER_REFERENCE_UPDATE_COUNT, referencesReplaced);
        }
    }

    public static class ApiSavePrescribedValues extends T8DefaultServerOperation
    {
        public ApiSavePrescribedValues(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<PrescribedValue> prescribedValues;
            T8PrescribedValueApi pvApi;

            // Get the operation parameters we need.
            prescribedValues = (List<PrescribedValue>)operationParameters.get(PARAMETER_PRESCRIBED_VALUE_LIST);

            // Perform the API operation.
            pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);
            pvApi.savePrescribedValues(prescribedValues);
            return null;
        }
    }

    public static class ApiInsertPrescribedValues extends T8DefaultServerOperation
    {
        public ApiInsertPrescribedValues(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<PrescribedValue> prescribedValues;
            T8PrescribedValueApi pvApi;

            // Get the operation parameters we need.
            prescribedValues = (List<PrescribedValue>)operationParameters.get(PARAMETER_PRESCRIBED_VALUE_LIST);

            // Perform the API operation.
            pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);
            pvApi.insertPrescribedValues(prescribedValues);
            return null;
        }
    }

    public static class ApiUpdatePrescribedValues extends T8DefaultServerOperation
    {
        public ApiUpdatePrescribedValues(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<PrescribedValue> prescribedValues;
            T8PrescribedValueApi pvApi;

            // Get the operation parameters we need.
            prescribedValues = (List<PrescribedValue>)operationParameters.get(PARAMETER_PRESCRIBED_VALUE_LIST);

            // Perform the API operation.
            pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);
            pvApi.updatePrescribedValues(prescribedValues);
            return null;
        }
    }

    public static class ApiDeletePrescribedValues extends T8DefaultServerOperation
    {
        public ApiDeletePrescribedValues(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8PrescribedValueApi pvApi;
            List valueIds;

            //Get the operation parameter we need
            valueIds = (List) operationParameters.get(PARAMETER_VALUE_ID_LIST);

            //Perform the API operation.
            pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);
            pvApi.deletePrescribedValues(valueIds);
            return null;
        }
    }

    public static class ApiSetStandard extends T8DefaultServerOperation
    {
        public ApiSetStandard(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8PrescribedValueApi pvApi;
            boolean standardIndicator;
            String valueId;

            // Get the operation parameters we need.
            valueId = (String) operationParameters.get(PARAMETER_VALUE_ID);
            standardIndicator = (Boolean) operationParameters.get(PARAMETER_STANDARD_INDICATOR);

            // Perform the API operation.
            pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);
            pvApi.setStandard(valueId, standardIndicator);
            return null;
        }
    }

    public static class ApiSetApproved extends T8DefaultServerOperation
    {
        public ApiSetApproved(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8PrescribedValueApi pvApi;
            boolean approvedIndicator;
            String valueId;

            // Get the operation parameters we need.
            valueId = (String) operationParameters.get(PARAMETER_VALUE_ID);
            approvedIndicator = (Boolean) operationParameters.get(PARAMETER_APPROVED_INDICATOR);

            // Perform the API operation.
            pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);
            pvApi.setApproved(valueId, approvedIndicator);
            return null;
        }
    }
}
