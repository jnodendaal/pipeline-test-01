package com.pilog.t8.data.document.datarecord.match;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCase.T8RecordMatchCaseType;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.utilities.codecs.HexCodec;
import com.google.common.base.Strings;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordHashCodeGenerator
{
    private final Map<String, DocPathExpressionEvaluator> checkEvaluatorCache; // A cache of check evaluators used during matching.
    private final Map<String, DocPathExpressionEvaluator> caseEvaluatorCache; // A cache of case evaluators used during matching.
    private T8RecordMatchCriteria criteria; // The criteria to use when comparing records to eachother.
    private String matchInstanceId; // A unique ID identifying a match operation.  A new one is generated every time records are matched.

    public T8DataRecordHashCodeGenerator()
    {
        this.checkEvaluatorCache = new HashMap<String, DocPathExpressionEvaluator>();
        this.caseEvaluatorCache = new HashMap<String, DocPathExpressionEvaluator>();
    }

    public void setCriteria(T8RecordMatchCriteria criteria)
    {
        this.criteria = criteria;
        this.checkEvaluatorCache.clear();
        this.caseEvaluatorCache.clear();
    }

    public String getMatchInstanceId()
    {
        return matchInstanceId;
    }

    public List<T8DataRecordHashCode> generateHashCodes(DataRecord record)
    {
        List<T8DataRecordHashCode> hashCodes;

        // Generate a new instance ID.
        matchInstanceId = T8IdentifierUtilities.createNewGUID();

        // Make sure record is filled to ensure that no empty properties mess up the sequence of values to compare.
        record.fill(true);

        // Loop through all cases in the criteria.
        hashCodes = new ArrayList<>();
        for (T8RecordMatchCase matchCase : criteria.getCases())
        {
            T8RecordMatchCaseType caseType;

            // We can only generate hash codes from match cases with the correct type 'HASH'.
            caseType = matchCase.getType();
            if (caseType == T8RecordMatchCaseType.HASH)
            {
                List<Object> checkInputList;

                // Create a list to hold the objects to be used as inputs to check expression.
                checkInputList = new ArrayList<>();
                if (Strings.isNullOrEmpty(matchCase.getDataExpression()))
                {
                    // No data expression is specified for the match case, so we simply add the input record to the list.
                    checkInputList.add(record);
                }
                else
                {
                    DocPathExpressionEvaluator caseEvaluator;
                    Object caseResult;

                    // Get the evaluator to use for evaluation of the case data expression.
                    caseEvaluator = getEvaluator(matchCase);
                    caseResult = caseEvaluator.evaluateExpression(record);
                    if (caseResult instanceof List)
                    {
                        checkInputList.addAll((List)caseResult);
                    }
                    else if (caseResult instanceof Value)
                    {
                        checkInputList.add((Value)caseResult);
                    }
                }

                // For each of the objects in the check input list, execute the check list and generate a hash code.
                for (Object checkInput : checkInputList)
                {
                    List<Object> values;

                    // Create the lists that will hold the values from which the hash codes will be generated.
                    values = new ArrayList<>();

                    // Loop through all expressions, evaluate each one on both records and add the values the comparison lists.
                    for (T8RecordMatchCheck matchCheck : matchCase.getChecks())
                    {
                        DocPathExpressionEvaluator checkEvaluator;
                        Object checkResult;

                        // Get the evaluator to use.
                        checkEvaluator = getEvaluator(matchCheck);

                        // Evaluate the expression on record2.
                        checkResult = checkEvaluator.evaluateExpression(checkInput);
                        if (checkResult instanceof List)
                        {
                            values.addAll((List)checkResult);
                        }
                        else values.add(checkResult);
                    }

                    // Only generate a hash code if all values were found (hash codes can only be used for 100% match on checked results).
                    if (!values.contains(null))
                    {
                        T8DataRecordHashCode hashCode;

                        // Generate a hash code from the list of values.
                        hashCode =  generateHashCode(matchInstanceId, matchCase, record, values);
                        if (hashCode != null)
                        {
                            hashCodes.add(hashCode);
                        }
                    }
                }
            }
        }

        // Return the complete list of hash codes generated for the record.
        return hashCodes;
    }

    private DocPathExpressionEvaluator getEvaluator(T8RecordMatchCase expression)
    {
        DocPathExpressionEvaluator evaluator;
        String id;

        id = expression.getId();
        evaluator = caseEvaluatorCache.get(id);
        if (evaluator != null)
        {
            return evaluator;
        }
        else
        {
            evaluator = new DocPathExpressionEvaluator();
            evaluator.parseExpression(expression.getDataExpression());
            caseEvaluatorCache.put(id, evaluator);
            return evaluator;
        }
    }

    private DocPathExpressionEvaluator getEvaluator(T8RecordMatchCheck expression)
    {
        DocPathExpressionEvaluator evaluator;
        String id;

        id = expression.getId();
        evaluator = checkEvaluatorCache.get(id);
        if (evaluator != null)
        {
            return evaluator;
        }
        else
        {
            evaluator = new DocPathExpressionEvaluator();
            evaluator.parseExpression(expression.getDataExpression());
            checkEvaluatorCache.put(id, evaluator);
            return evaluator;
        }
    }

    private T8DataRecordHashCode generateHashCode(String matchInstanceId, T8RecordMatchCase matchCase, DataRecord dataRecord, List<Object> values)
    {
        MessageDigest messageDigest;
        StringBuilder buffer;
        String valueString;

        // Concatenate all values to form a string.
        buffer = new StringBuilder();
        for (Object value : values)
        {
            buffer.append(value);
        }

        // Generate and MD5 hash code from the string.
        valueString = buffer.toString();
        if (valueString.length() > 0)
        {
            try
            {
                T8DataRecordHashCode hashCode;

                // Generate and MD5 hash code from the string.
                messageDigest = MessageDigest.getInstance("MD5");
                messageDigest.update(valueString.getBytes("UTF-8"));

                // Create the hash code object to record the details of this operation and return the result.
                hashCode = new T8DataRecordHashCode();
                hashCode.setMatchInstanceId(matchInstanceId);
                hashCode.setMatchCriteriaId(matchCase.getMatchCriteria().getId());
                hashCode.setMatchCaseId(matchCase.getId());
                hashCode.setRecordId(dataRecord.getID());
                hashCode.setValueString(valueString);
                hashCode.setHash(HexCodec.convertBytesToHexString(messageDigest.digest()));
                return hashCode;
            }
            catch (NoSuchAlgorithmException | UnsupportedEncodingException e)
            {
                throw new RuntimeException("Exception while generating MD5 hash code using match criteria '" + matchCase.getMatchCriteria() + "' for record: " + dataRecord, e);
            }
        }
        else return null;
    }
}
