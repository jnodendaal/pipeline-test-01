package com.pilog.t8.data.source.datarecord;

import static com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceDefinition.*;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.source.T8CommonStatementHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceDefinition;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordDataSource implements T8DataSource
{
    private final T8Context context;
    private final T8DataTransaction tx;
    private final T8DataRecordDataSourceDefinition definition;
    private T8DataConnection connection;
    private final List<T8DataEntityResults> openEntityResults;
    private final T8DataRecordPersistenceHandler persistenceHandler;
    private final String pubKeyFieldIdentifier;
    private final String pubDocumentFieldIdentifier;
    private T8PerformanceStatistics stats;

    public T8DataRecordDataSource(T8DataRecordDataSourceDefinition definition, T8DataTransaction tx)
    {
        this.definition = definition;
        this.tx = tx;
        this.context = tx.getContext();
        this.openEntityResults = new ArrayList<>();
        this.pubKeyFieldIdentifier = definition.getIdentifier() + KEY_FIELD_IDENTIFIER;
        this.pubDocumentFieldIdentifier = definition.getIdentifier() + DOCUMENT_FIELD_IDENTIFIER;
        this.persistenceHandler = new T8DataRecordPersistenceHandler(tx, definition.getDocumentTableName(), "DAT_REC_ATR", definition.getPropertyTableName(), definition.getValueTableName(), definition.getAttachmentTableName(), definition.getAttachmentHandlerIdentifier());
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.

        // Do a validity check.
        if (context == null) throw new RuntimeException("Null session context obtained from data session.");
    }

    @Override
    public T8DataSourceDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
        this.persistenceHandler.setPerformanceStatistics(statistics);
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8DataConnection getConnection()
    {
        return connection;
    }

    @Override
    public void open() throws Exception
    {
        connection = tx.getDataConnection(definition.getConnectionIdentifier());
    }

    @Override
    public void close() throws Exception
    {
        // Close any open entity results.
        for (T8DataEntityResults results : openEntityResults)
        {
            results.close();
        }

        // Clear the collection.
        openEntityResults.clear();
    }

    @Override
    public void commit() throws Exception
    {
        if (connection != null) connection.commit();
    }

    @Override
    public void rollback() throws Exception
    {
        if (connection != null) connection.rollback();
    }

    @Override
    public void setParameters(Map<String, Object> parameters)
    {
    }

    @Override
    public List<T8DataSourceFieldDefinition> retrieveFieldDefinitions() throws Exception
    {
        ArrayList<T8DataSourceFieldDefinition> definitionList;

        definitionList = new ArrayList<>();
        // The field definitions are constants and do not need to be retrieved.
        definitionList.add(new T8DataSourceFieldDefinition(KEY_FIELD_IDENTIFIER, KEY_FIELD_SOURCE_IDENTIFIER, true, true, T8DataType.GUID));
        definitionList.add(new T8DataSourceFieldDefinition(DOCUMENT_FIELD_IDENTIFIER, DOCUMENT_FIELD_SOURCE_IDENTIFIER, false, true, T8DataType.CUSTOM_OBJECT));
        definitionList.add(new T8DataSourceFieldDefinition(FILTER_FIELD_RECORD_VALUE_STRING, FILTER_SOURCE_RECORD_VALUE_STRING, false, true, T8DataType.STRING));
        definitionList.add(new T8DataSourceFieldDefinition(FILTER_FIELD_FFT, FILTER_SOURCE_FFT, false, true, T8DataType.STRING));
        definitionList.add(new T8DataSourceFieldDefinition(FILTER_FIELD_DR_ID, FILTER_SOURCE_DR_ID, false, true, T8DataType.GUID));
        definitionList.add(new T8DataSourceFieldDefinition(FILTER_FIELD_DR_INSTANCE_ID, FILTER_SOURCE_DR_INSTANCE_ID, false, true, T8DataType.GUID));
        // The field values are included in the eventual data record. Fields used for ordering only.
        definitionList.add(new T8DataSourceFieldDefinition(ORDER_FIELD_INSERTED_AT, ORDER_SOURCE_INSERTED_AT, false, false, T8DataType.TIMESTAMP));
        definitionList.add(new T8DataSourceFieldDefinition(ORDER_FIELD_UPDATED_AT, ORDER_SOURCE_UPDATED_AT, false, false, T8DataType.TIMESTAMP));

        return definitionList;
    }

    @Override
    public int count(T8DataFilter filter) throws Exception
    {
        if (filter == null) throw new IllegalArgumentException("Filter cannot be null when counting data records.");
        T8DataEntityDefinition entityDefinition;
        ParameterizedString queryString;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(filter.getEntityIdentifier());

        // Create and execute the query.
        queryString = new ParameterizedString("-- ");
        queryString.append(definition.getIdentifier());
        queryString.append("\nSELECT COUNT(*) AS RECORD_COUNT FROM DATA_RECORD_DOC ");
        if (filter.hasFilterCriteria())
        {
            queryString.append(filter.getWhereClause(tx, "DATA_RECORD_DOC"));
            queryString.setParameterValues(filter.getWhereClauseParameters(tx));
        }

        return T8CommonStatementHandler.executeCountQuery(context, connection, queryString, definition, entityDefinition, 0);
    }

    @Override
    public boolean exists(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return count(new T8DataFilter(entityIdentifier, keyMap)) > 0;
    }

    @Override
    public void insert(T8DataEntity dataEntity) throws Exception
    {
        DataRecord dataRecord;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = dataEntity.getDefinition();

        dataRecord = (DataRecord)dataEntity.getFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubDocumentFieldIdentifier));
        persistenceHandler.insertDataRecord(tx, connection, dataRecord);
    }

    @Override
    public void insert(List<T8DataEntity> dataEntities) throws Exception
    {
        for (T8DataEntity t8DataEntity : dataEntities)
        {
            insert(t8DataEntity);
        }
    }

    @Override
    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        String key;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);
        key = (String)keyMap.get(entityDefinition.mapSourceToFieldIdentifier(pubKeyFieldIdentifier));
        return retrieve(entityDefinition, key);
    }

    public T8DataEntity retrieve(T8DataEntityDefinition entityDefinition, String recordID) throws Exception
    {
        DataRecord dataRecord;

        dataRecord = persistenceHandler.retrieveDataRecord(connection, recordID, definition.isRootLevelSource());
        if (dataRecord != null)
        {
            T8DataEntity dataEntity;

            dataEntity = entityDefinition.getNewDataEntityInstance();
            dataEntity.setFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubKeyFieldIdentifier), recordID);
            dataEntity.setFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubDocumentFieldIdentifier), dataRecord);
            return dataEntity;
        }
        else return null;
    }

    @Override
    public boolean update(T8DataEntity dataEntity) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        Set<String> removedReferences;
        DataRecord dataRecord;

        // Update the data record document.
        entityDefinition = dataEntity.getDefinition();
        dataRecord = (DataRecord)dataEntity.getFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubDocumentFieldIdentifier));
        removedReferences = persistenceHandler.updateDataRecord(tx, connection, dataRecord);

        // Deleted all removed dependent references.
        for (String removedReference : removedReferences)
        {
            T8DataEntity entityToDelete;

            entityToDelete = entityDefinition.getNewDataEntityInstance();
            entityToDelete.setFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubKeyFieldIdentifier), removedReference);
            tx.delete(entityToDelete);
        }

        return true;
    }

    @Override
    public boolean update(List<T8DataEntity> dataEntities) throws Exception
    {
        for (T8DataEntity entity : dataEntities)
        {
            update(entity);
        }
        return true;
    }

    @Override
    public boolean delete(T8DataEntity dataEntity) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        String key;

        // Get the entity definition and the entity key.
        entityDefinition = dataEntity.getDefinition();
        key = (String)dataEntity.getFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubKeyFieldIdentifier));

        // Delete the record.
        persistenceHandler.deleteDataRecord(tx, connection, definition, entityDefinition, key);

        // Return a result indicating that the record was deleted.
        return true;
    }

    @Override
    public int delete(List<T8DataEntity> dataEntities) throws Exception
    {
        int result;

        // This method of deletion can be optimizes to use a batch statement.
        result = 0;
        for (T8DataEntity dataEntity : dataEntities)
        {
            if (delete(dataEntity)) result++;
        }

        return result;
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return select(entityIdentifier, keyMap != null ? new T8DataFilter(entityIdentifier, keyMap) : null);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        return select(entityIdentifier, filter, 0, 50000);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap, int offset, int pageSize) throws Exception
    {
        return select(entityIdentifier, keyMap != null ? new T8DataFilter(entityIdentifier, keyMap) : null, offset, pageSize);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter, int offset, int pageSize) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        ParameterizedString queryString;
        List<T8DataEntity> idEntities;
        List<T8DataEntity> recordEntities;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        // Create the query String using the specified data filter (if any).
        queryString = new ParameterizedString("-- ");
        queryString.append(definition.getIdentifier());
        queryString.append("\nSELECT * FROM DATA_RECORD_DOC");
        queryString.append(" ");
        if ((filter != null) && (filter.hasFilterCriteria()))
        {
            queryString.append(filter.getWhereClause(tx, "DATA_RECORD_DOC"));
            queryString.setParameterValues(filter.getWhereClauseParameters(tx));
        }

        // Retrieve the data record entities using the created query.
        recordEntities = new ArrayList<>();
        idEntities = T8CommonStatementHandler.executeQuery(context, connection, queryString, definition, entityDefinition, offset, pageSize, 0);
        for (T8DataEntity idEntity : idEntities) // For each record entity, retrieve the full record and return the total list of documents.
        {
            T8DataEntity dataRecordEntity;
            String recordID;

            // Get the record ID from the entity and then use it to retrieve the full document.
            recordID = (String)idEntity.getFieldValue(entityIdentifier + KEY_FIELD_IDENTIFIER);
            dataRecordEntity = retrieve(entityDefinition, recordID);
            if (dataRecordEntity != null)
            {
                recordEntities.add(dataRecordEntity);
            }
        }

        return recordEntities;
    }

    @Override
    public T8DataEntityResults scroll(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        ParameterizedString queryString;
        T8DataEntityResults idEntityResults;
        T8DataEntityResults documentEntityResults;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        // Create the query String using the specified data filter (if any).
        queryString = new ParameterizedString("-- ");
        queryString.append(definition.getIdentifier());
        queryString.append("\nSELECT * FROM DATA_RECORD_DOC");
        queryString.append(" ");
        if (filter != null)
        {
            // Add on the WHERE clause, if available
            if (filter.hasFilterCriteria())
            {
                queryString.append(filter.getWhereClause(tx, "DATA_RECORD_DOC"));
                queryString.setParameterValues(filter.getWhereClauseParameters(tx));
            }
            // Add on the ORDER BY clause, if available
            if (filter.hasFieldOrdering())
            {
                queryString.append(" ");
                queryString.append(filter.getOrderByClause(connection, entityDefinition, definition));
            }
        }

        // Create a scrollable entity results object for the record ID's and then wrap in in a scrollable entity results object that retrieves the document itself.
        idEntityResults = T8CommonStatementHandler.getEntityResults(context, connection, queryString, definition, entityDefinition);
        documentEntityResults = new T8ScrollableDataRecordEntityResults(this, entityDefinition, idEntityResults);
        openEntityResults.add(documentEntityResults);
        return documentEntityResults;
    }

    @Override
    public int delete(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int update(String entityIdentifier, Map<String, Object> updatedValues, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
