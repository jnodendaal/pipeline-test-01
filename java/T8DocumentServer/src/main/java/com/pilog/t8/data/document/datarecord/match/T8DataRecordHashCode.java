package com.pilog.t8.data.document.datarecord.match;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordHashCode implements Serializable
{
    private String recordId;
    private String matchInstanceId;
    private String matchCriteriaId;
    private String matchCaseId;
    private String hashCode;
    private String valueString;

    public T8DataRecordHashCode()
    {
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getMatchInstanceId()
    {
        return matchInstanceId;
    }

    public void setMatchInstanceId(String matchInstanceId)
    {
        this.matchInstanceId = matchInstanceId;
    }

    public String getMatchCriteriaId()
    {
        return matchCriteriaId;
    }

    public void setMatchCriteriaId(String matchCriteriaId)
    {
        this.matchCriteriaId = matchCriteriaId;
    }

    public String getMatchCaseId()
    {
        return matchCaseId;
    }

    public void setMatchCaseId(String matchCaseId)
    {
        this.matchCaseId = matchCaseId;
    }

    public String getValueString()
    {
        return valueString;
    }

    public void setValueString(String valueString)
    {
        this.valueString = valueString;
    }

    public String getHash()
    {
        return hashCode;
    }

    public void setHash(String hashCode)
    {
        this.hashCode = hashCode;
    }
}
