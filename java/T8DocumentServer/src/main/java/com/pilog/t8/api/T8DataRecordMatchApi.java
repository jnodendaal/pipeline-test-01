package com.pilog.t8.api;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.match.T8DataRecordHashCode;
import com.pilog.t8.data.document.datarecord.match.T8DataRecordHashCodeGenerator;
import com.pilog.t8.data.document.datarecord.match.T8DataRecordMatcher;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatch;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCriteria;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.datarecord.match.T8DataRecordHashCodeMatcher;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordMatchApi implements T8Api, T8PerformanceStatisticsProvider
{
    public static final String API_IDENTIFIER = "@API_DATA_RECORD_MATCH";

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8SessionContext sessionContext;
    private final T8ServerContext serverContext;
    private final T8DataManager dataManager;
    private final T8DataRecordMatchApiDataHandler dataHandler;
    private final Map<String, List<T8RecordMatchCriteria>> drInstanceCriteriaCache;
    private T8PerformanceStatistics stats;

    public enum T8RecordMatchEvent {CRITERIA_UPDATE, CRITERIA_DELETE, RECORD_MATCH, HASH_CODE_GENERATION};

    public T8DataRecordMatchApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.dataManager = serverContext.getDataManager();
        this.dataHandler = new T8DataRecordMatchApiDataHandler(serverContext, sessionContext);

        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
        this.drInstanceCriteriaCache = new HashMap<>();
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    private List<T8RecordMatchCriteria> getDrInstanceMatchCriteria(String drInstanceId) throws Exception
    {
        List<T8RecordMatchCriteria> criteriaList;

        criteriaList = drInstanceCriteriaCache.get(drInstanceId);
        if (criteriaList != null)
        {
            return criteriaList;
        }
        else
        {
            List<String> criteriaIdList;

            // Retrieve the requested criteria from the persistence store.
            criteriaIdList = dataHandler.retrieveDrInstanceMatchCriteria(tx, drInstanceId);
            if (!criteriaIdList.isEmpty())
            {
                criteriaList = dataHandler.retrieveMatchCriteria(tx, criteriaIdList);

                // Cache the retrieved criteria and then return it as output.
                drInstanceCriteriaCache.put(drInstanceId, criteriaList);
                return criteriaList;
            }
            else
            {
                // Cache an empty list to prevent subsequent retrievals.
                criteriaList = new ArrayList<>();
                drInstanceCriteriaCache.put(drInstanceId, criteriaList);
                return criteriaList;
            }
        }
    }

    public void insertMatchCriteriaLink(String drInstanceId, String criteriaId) throws Exception
    {
        dataHandler.insertMatchCriteriaLink(tx, drInstanceId, criteriaId);
    }

    public boolean deleteMatchCriteriaLink(String drInstanceId, String criteriaId) throws Exception
    {
        return dataHandler.deleteMatchCriteriaLink(tx, drInstanceId, criteriaId);
    }

    public int deleteMatchCriteriaLinks(String criteriaId) throws Exception
    {
        return dataHandler.deleteMatchCriteriaLinks(tx, criteriaId);
    }

    public int deleteDrInstanceMatchCriteriaLinks(String drInstanceId) throws Exception
    {
        return dataHandler.deleteDrInstanceMatchCriteriaLinks(tx, drInstanceId);
    }

    public T8RecordMatchCriteria retrieveMatchCriteria(String criteriaId) throws Exception
    {
        // Retrieve the criteria and return the result.
        return dataHandler.retrieveMatchCriteria(tx, criteriaId);
    }

    public void saveMatchCriteria(T8RecordMatchCriteria criteria) throws Exception
    {
        // Save the match criteria.
        dataHandler.saveMatchCriteria(tx, criteria);

        // Log the event.
        dataHandler.logRecordMatchEvent(tx, T8RecordMatchEvent.CRITERIA_UPDATE, criteria, null);
    }

    public void deleteMatchCriteria(String criteriaId) throws Exception
    {
        T8RecordMatchCriteria criteria;

        // First retrieve the criteria to be deleted.
        criteria = dataHandler.retrieveMatchCriteria(tx, criteriaId);
        if (criteria != null)
        {
            // Delete the criteria links.
            dataHandler.deleteMatchCriteriaLinks(tx, criteriaId);

            // Delete the criteria.
            dataHandler.deleteMatchCriteria(tx, criteria);

            // Log the event.
            dataHandler.logRecordMatchEvent(tx, T8RecordMatchEvent.CRITERIA_DELETE, criteria, null);
        }
        else throw new Exception("Match criteria not found: " + criteriaId);
    }

    /**
     * Uses the match instance identifiers in the list to remove matches from
     * the Data Record Match table if they have not been used to resolve a duplicate.
     * @param matchInstanceIdList A list of match instance IDs that will be deleted
     * @throws Exception
     */
     public void deleteMatchResults(List matchInstanceIdList) throws Exception
    {
        // Ensure the match instance id list is not empty, then proceed to delete
        if (matchInstanceIdList != null)
        {
            // Delete the match results.
            dataHandler.deleteMatchResults(tx, matchInstanceIdList);
        }
        else throw new Exception("Match instance id list is empty.");
    }

    /**
     * Uses the specified match criteria to match all data files identified by the specified filter with each other.
     * All file matches identified are then persisted and the match instance ID is returned.
     * @param criteriaId The ID of the criteria to use when matching files.
     * @param fileFilter The filter to use for retrieval of data files to be matched.
     * @param fileIdEntityFieldId The ID of the field in the entities retrieved using the specified filter, from which the data file ID will be fetched.
     * @return The ID of this match instance.  All record matches identified during the matching operation will be persisted using this ID as reference.
     * @throws Exception
     */
    public String matchFiles(String criteriaId, T8DataFilter fileFilter, String fileIdEntityFieldId) throws Exception
    {
        T8RecordMatchCriteria criteria;

        // Retrieve the criteria to be used for the matching operation.
        criteria = retrieveMatchCriteria(criteriaId);
        if (criteria != null)
        {
            return matchFiles(criteria, fileFilter, fileIdEntityFieldId);
        }
        else throw new Exception("Criteria not found: " + criteriaId);
    }

    /**
     * Uses the specified match criteria to match all data files identified by the specified filter with each other.
     * All file matches identified are then persisted and the match instance id is returned.
     * @param criteria The criteria to use when matching files.
     * @param fileFilter The filter to use for retrieval of data files to be matched.
     * @param fileIdEntityFieldId The id of the field in the entities retrieved using the specified filter, from which the data file id will be fetched.
     * @return The id of this match instance.  All record matches identified during the matching operation will be persisted using this id as reference.
     * @throws Exception
     */
    public String matchFiles(T8RecordMatchCriteria criteria, T8DataFilter fileFilter, String fileIdEntityFieldId) throws Exception
    {
        Map<String, String> processedFamilyCodes;
        T8DataRecordMatcher matcher;
        String entityIdentifier;
        T8DataRecordApi recApi;

        // Get the Api for this operation.
        recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

        // Create the matcher to use for this operation.
        matcher = new T8DataRecordMatcher();
        matcher.setCriteria(criteria);

        // Load all of the records to be matched.
        processedFamilyCodes = new HashMap<String, String>();
        entityIdentifier = fileFilter.getEntityIdentifier();
        for (T8DataEntity entity : tx.select(entityIdentifier, fileFilter))
        {
            DataRecord dataFile;
            String fileId;

            // Retrieve the data file and add it to the matcher.
            fileId = (String)entity.getFieldValue(fileIdEntityFieldId);
            dataFile = recApi.retrieveFile(fileId, null, true, false, false, false, false, false);
            if (dataFile != null)
            {
                matcher.addRecord(dataFile);
            }
        }

        // Perform the record match. Depending on the number of records added to the matcher, this step can take a few minutes.
        matcher.matchRecords();

        // Persist the results of the matching operation.
        for (T8RecordMatch match : matcher.getRecordMatches())
        {
            // First make sure that the match has not been previously excluded by the user.
            if (!dataHandler.isRecordMatchExcluded(tx, match))
            {
                String familyId;
                String familyCode;

                // Get the family ID of the match.
                familyId = match.getFamilyId();
                familyCode = processedFamilyCodes.get(familyId);
                if (familyCode == null)
                {
                    // Generate a new family code.
                    familyCode = dataManager.generateKey("@KEY_FAMILY_CODE");

                    // Add the generated code to the collection so that this family is not processed again.
                    processedFamilyCodes.put(familyId, familyCode);
                    dataHandler.insertRecordMatchFamily(tx, matcher.getMatchInstanceId(), familyId, familyCode);
                }

                // Now persist the match.
                dataHandler.insertRecordMatch(tx, match);
            }
        }

        // Log the event.
        dataHandler.logRecordMatchEvent(tx, T8RecordMatchEvent.RECORD_MATCH, criteria, matcher.getMatchInstanceId());

        // Return the match instance ID.
        return matcher.getMatchInstanceId();
    }

    /**
     * Uses the supplied data filter to retrieve data rows, containing record id's and corresponding hash codes which are
     * then used to match the records.
     * All file matches identified are then persisted and the match instance id is returned.
     * @param fileFilter The filter to use for retrieval of data files to be matched.
     * The minimum requirements for this filter are as follows:
     * - An entity id must be specified.
     * - The entity specified must contain the field $RECORD_ID from which the record id of a data file to be matched, will be retrieved.
     * - The entity specified must contain the field $HASH_CODE from which the hash code generated for the corresponding data file to be matched, will be retrieved.
     * - The entity specified must contain the field $MATCH_CRITERIA_ID from which the criteria id used to generate the corresponding hash code, will be retrieved.
     * - The entity specified must contain the field $MATCH_CASE_ID from which the case id used to generate the corresponding hash code, will be retrieved.
     * @return The id of this match instance.  All record matches identified during the matching operation will be persisted using this id as reference.
     * @throws Exception
     */
    public String matchFileHashCodes(T8DataFilter fileFilter) throws Exception
    {
        Map<String, String> processedFamilyCodes;
        T8DataRecordHashCodeMatcher matcher;
        T8RecordMatchCriteria criteria;

        // Create the matcher to use for this operation.
        matcher = new T8DataRecordHashCodeMatcher(serverContext, sessionContext);
        matcher.setSourceFilter(fileFilter);

        // Load all of the records to be matched.
        processedFamilyCodes = new HashMap<String, String>();

        // Perform the record match. Depending on the number of records added to the matcher, this step can take a few minutes.
        matcher.matchRecords();

        // Persist the results of the matching operation.
        for (T8RecordMatch match : matcher.getRecordMatches())
        {
            // First make sure that the match has not been previously excluded by the user.
            if (!dataHandler.isRecordMatchExcluded(tx, match))
            {
                String familyId;
                String familyCode;

                // Get the family id of the match.
                familyId = match.getFamilyId();
                familyCode = processedFamilyCodes.get(familyId);
                if (familyCode == null)
                {
                    // Generate a new family code.
                    familyCode = dataManager.generateKey("@KEY_FAMILY_CODE");

                    // Add the generated code to the collection so that this family is not processed again.
                    processedFamilyCodes.put(familyId, familyCode);
                    dataHandler.insertRecordMatchFamily(tx, matcher.getMatchInstanceId(), familyId, familyCode);
                }

                // Now persist the match.
                dataHandler.insertRecordMatch(tx, match);
            }
        }

        // Retrieve the criteria from which the hash codes used for the matching operation, were generated.
        criteria = retrieveMatchCriteria(matcher.getMatchCriteriaId());

        // Log the event.
        dataHandler.logRecordMatchEvent(tx, T8RecordMatchEvent.RECORD_MATCH, criteria, matcher.getMatchInstanceId());

        // Return the match instance ID.
        return matcher.getMatchInstanceId();
    }

    public String createRecordFamily(String matchInstanceId) throws Exception
    {
        String familyCode;
        String familyId;

        familyId = T8IdentifierUtilities.createNewGUID();
        familyCode = dataManager.generateKey("@KEY_FAMILY_CODE");
        dataHandler.insertRecordMatchFamily(tx, matchInstanceId, familyId, familyCode);
        return familyId;
    }

    public List<T8DataRecordHashCode> generateFileHashCodes(String criteriaId, DataRecord dataFile) throws Exception
    {
        T8RecordMatchCriteria criteria;

        // Retrieve the criteria to be used for the matching operation.
        criteria = retrieveMatchCriteria(criteriaId);
        if (criteria != null)
        {
            return generateFileHashCodes(criteria, dataFile);
        }
        else throw new Exception("Criteria not found: " + criteriaId);
    }

    public List<T8DataRecordHashCode> generateFileHashCodes(T8RecordMatchCriteria criteria, DataRecord dataFile) throws Exception
    {
        T8DataRecordHashCodeGenerator hashCodeGenerator;

        // Create the matcher to use for this operation.
        hashCodeGenerator = new T8DataRecordHashCodeGenerator();
        hashCodeGenerator.setCriteria(criteria);

        // Generate checksums for the data file and then save them.
        return hashCodeGenerator.generateHashCodes(dataFile);
    }

    public List<T8DataRecordHashCode> refreshFileHashCodes(String criteriaId, T8DataFilter fileFilter, String fileIdEntityFieldId) throws Exception
    {
        T8RecordMatchCriteria criteria;

        // Retrieve the criteria to be used for the matching operation.
        criteria = retrieveMatchCriteria(criteriaId);
        if (criteria != null)
        {
            return refreshFileHashCodes(criteria, fileFilter, fileIdEntityFieldId);
        }
        else throw new Exception("Criteria not found: " + criteriaId);
    }

    public List<T8DataRecordHashCode> refreshFileHashCodes(T8RecordMatchCriteria criteria, T8DataFilter fileFilter, String fileIdFieldId) throws Exception
    {
        T8DataRecordHashCodeGenerator hashCodeGenerator;
        List<T8DataRecordHashCode> resultList;
        String entityIdentifier;
        T8DataRecordApi recApi;
        String criteriaId;

        // Get the Api for this operation.
        recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

        // Create the matcher to use for this operation.
        hashCodeGenerator = new T8DataRecordHashCodeGenerator();
        hashCodeGenerator.setCriteria(criteria);
        criteriaId = criteria.getId();

        // Load all of the records to be matched.
        resultList = new ArrayList<>();
        entityIdentifier = fileFilter.getEntityIdentifier();
        for (T8DataEntity entity : tx.select(entityIdentifier, fileFilter))
        {
            DataRecord dataFile;
            String fileId;

            // Retrieve the data file and add it to the matcher.
            fileId = (String)entity.getFieldValue(fileIdFieldId);
            dataFile = recApi.retrieveFile(fileId, null, true, false, false, false, false, false);
            if (dataFile != null)
            {
                List<T8DataRecordHashCode> hashCodes;

                // Generate checksums for the data file and then save them.
                hashCodes = hashCodeGenerator.generateHashCodes(dataFile);
                resultList.addAll(hashCodes);

                // Delete the existing checksums for the data file.
                dataHandler.deleteHashCodes(tx, fileId, criteriaId);

                // Inser the newly generated checksums.
                dataHandler.insertHashCodes(tx, hashCodes);
            }
        }

        // Log the event.
        dataHandler.logRecordMatchEvent(tx, T8RecordMatchEvent.HASH_CODE_GENERATION, criteria, hashCodeGenerator.getMatchInstanceId());

        // Return all generated hash codes.
        return resultList;
    }

    /**
     * Refreshes all hash codes applicable to the input data file in the database.
     * @param dataFile The DataFile for which to refresh hash codes.
     * @return The list of hash codes generated by this record.
     * @throws Exception
     */
    public List<T8DataRecordHashCode> refreshFileHashCodes(DataRecord dataFile) throws Exception
    {
        List<T8RecordMatchCriteria> criteriaList;
        List<T8DataRecordHashCode> resultList;

        // Get all of the match criteria linked to the input file's data requirement instance.
        resultList = new ArrayList<>();
        criteriaList = getDrInstanceMatchCriteria(dataFile.getDataRequirementInstanceID());
        for (T8RecordMatchCriteria criteria : criteriaList)
        {
            T8DataRecordHashCodeGenerator hashCodeGenerator;
            List<T8DataRecordHashCode> hashCodes;

            // Create the matcher to use for this operation.
            hashCodeGenerator = new T8DataRecordHashCodeGenerator();
            hashCodeGenerator.setCriteria(criteria);

            // Generate checksums for the data file and then save them.
            hashCodes = hashCodeGenerator.generateHashCodes(dataFile);
            resultList.addAll(hashCodes);

            // Delete the existing checksums for the data file.
            dataHandler.deleteHashCodes(tx, dataFile.getID(), criteria.getId());

            // Inser the newly generated checksums.
            dataHandler.insertHashCodes(tx, hashCodes);
        }

        // Return all generated hash codes.
        return resultList;
    }

    /**
     * Refreshes the persisted hash codes for the supplied data file and match criteria combination.
     * @param criteria The match criteria to use for the operation.
     * @param dataFile The data file to use as input for hash generation.
     * @return The list of hash codes generated by this method.
     * @throws Exception
     */
    public List<T8DataRecordHashCode> refreshFileHashCodes(T8RecordMatchCriteria criteria, DataRecord dataFile) throws Exception
    {
        T8DataRecordHashCodeGenerator hashCodeGenerator;
        List<T8DataRecordHashCode> hashCodes;

        // Create the matcher to use for this operation.
        hashCodeGenerator = new T8DataRecordHashCodeGenerator();
        hashCodeGenerator.setCriteria(criteria);

        // Generate checksums for the data file and then save them.
        hashCodes = hashCodeGenerator.generateHashCodes(dataFile);

        // Delete the existing checksums for the data file.
        dataHandler.deleteHashCodes(tx, dataFile.getID(), criteria.getId());

        // Inser the newly generated checksums.
        dataHandler.insertHashCodes(tx, hashCodes);

        // Log the event.
        dataHandler.logRecordMatchEvent(tx, T8RecordMatchEvent.HASH_CODE_GENERATION, criteria, hashCodeGenerator.getMatchInstanceId());

        // Return the generated hash codes.
        return hashCodes;
    }
}
