package com.pilog.t8.data.document.history;

import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.source.datarecord.T8DataRecordPersistenceHandler.AuditingType;

/**
 * @author Bouwer du Preez
 */
public class HistoryEntry
{
    private AuditingType auditingType;
    private T8Timestamp timestamp;
    private String rootRecordId;
    private String parentRecordId;
    private String transactionIid;
    private DataRecord dataRecord;
    private String agentIid;
    private String agentId;
    private String processId;
    private String processIid;
    private String operationId;
    private String operationIid;
    private String flowId;
    private String flowIid;
    private String functionalityId;
    private String functionalityIid;
    private String taskId;
    private String taskIid;
    private String sessionId;
    private String userId;

    public HistoryEntry()
    {
    }

    public AuditingType getAuditingType()
    {
        return auditingType;
    }

    public void setAuditingType(AuditingType auditingType)
    {
        this.auditingType = auditingType;
    }

    public T8Timestamp getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(T8Timestamp timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getRecordId()
    {
        return dataRecord.getID();
    }

    public String getRootRecordId()
    {
        return rootRecordId;
    }

    public void setRootRecordId(String rootRecordId)
    {
        this.rootRecordId = rootRecordId;
    }

    public String getParentRecordId()
    {
        return parentRecordId;
    }

    public void setParentRecordId(String parentRecordId)
    {
        this.parentRecordId = parentRecordId;
    }

    public String getTransactionIid()
    {
        return transactionIid;
    }

    public void setTransactionIid(String transactionIid)
    {
        this.transactionIid = transactionIid;
    }

    public DataRecord getDataRecord()
    {
        return dataRecord;
    }

    public void setDataRecord(DataRecord dataRecord)
    {
        this.dataRecord = dataRecord;
    }

    public boolean isValidEntry()
    {
        return dataRecord != null;
    }

    public boolean hasUserId()
    {
        return this.userId != null;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }

    public String getAgentId()
    {
        return agentId;
    }

    public void setAgentId(String agentId)
    {
        this.agentId = agentId;
    }

    public void setAgentIid(String agentIid)
    {
        this.agentIid = agentIid;
    }

    public String getAgentIid()
    {
        return this.agentIid;
    }

    public String getProcessId()
    {
        return processId;
    }

    public void setProcessId(String processId)
    {
        this.processId = processId;
    }

    public String getProcessIid()
    {
        return processIid;
    }

    public void setProcessIid(String processIid)
    {
        this.processIid = processIid;
    }

    public String getOperationId()
    {
        return operationId;
    }

    public void setOperationId(String operationId)
    {
        this.operationId = operationId;
    }

    public String getOperationIid()
    {
        return operationIid;
    }

    public void setOperationIid(String operationIid)
    {
        this.operationIid = operationIid;
    }

    public String getFlowId()
    {
        return flowId;
    }

    public void setFlowId(String flowId)
    {
        this.flowId = flowId;
    }

    public String getFlowIid()
    {
        return flowIid;
    }

    public void setFlowIid(String flowIid)
    {
        this.flowIid = flowIid;
    }

    public String getFunctionalityId()
    {
        return functionalityId;
    }

    public void setFunctionalityId(String functionalityId)
    {
        this.functionalityId = functionalityId;
    }

    public String getFunctionalityIid()
    {
        return functionalityIid;
    }

    public void setFunctionalityIid(String functionalityIid)
    {
        this.functionalityIid = functionalityIid;
    }

    public String getTaskId()
    {
        return taskId;
    }

    public void setTaskId(String taskId)
    {
        this.taskId = taskId;
    }

    public String getTaskIid()
    {
        return taskIid;
    }

    public void setTaskIid(String taskIid)
    {
        this.taskIid = taskIid;
    }

    public String getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    @Override
    public String toString()
    {
        return "HistoryEntry{" + "auditingType=" + auditingType + ", timestamp=" + timestamp + ", parentRecordId=" + parentRecordId + ", transactionIid=" + transactionIid + ", recordId=" + dataRecord.getID() + ", agentIid=" + agentIid + ", agentId=" + agentId + ", processId=" + processId + ", processIid=" + processIid + ", operationid=" + operationId + ", operationIid=" + operationIid + ", flowId=" + flowId + ", flowIid=" + flowIid + ", functionalityId=" + functionalityId + ", functionalityIid=" + functionalityIid + ", taskId=" + taskId + ", taskIid=" + taskIid + ", sessionId=" + sessionId + ", userId=" + userId + '}';
    }
}
