package com.pilog.t8.data.org;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8TerminologyDataHandler
{
    private final T8DataTransaction dataAccessProvider;

    public T8TerminologyDataHandler(T8DataTransaction dataAccessProvider)
    {
        this.dataAccessProvider = dataAccessProvider;
    }

    /**
     * Retrieves terminology in the requested languages for all of the concepts
     * in the supplied list.
     * @param entityId The terminology entity from which to retrieve the
     * data.
     * @param orgIdList The ID of the organization that will be used when retrieving
     * terminology.
     * @param languageIdList The list of terminology languages to fetch.
     * @param conceptIds
     * @return A list of the sets of terminology retrieved.
     * @throws Exception
     */
    public T8ConceptTerminologyList loadConceptTerminology(String entityId, Collection<String> orgIdList, Collection<String> languageIdList, Collection<String> conceptIds) throws Exception
    {
        T8ConceptTerminologyList terminologyList;
        List<String> conceptIdList;

        conceptIdList = new ArrayList<>(conceptIds);
        terminologyList = new T8ConceptTerminologyList();
        for (int conceptBatchIndex = 0; conceptBatchIndex < conceptIdList.size(); conceptBatchIndex += 1000)
        {
            T8DataFilterCriteria criteria;
            List <T8DataEntity> entities;
            List<String> conceptBatch;
            int batchEnd;

            batchEnd = conceptBatchIndex + 1000;
            if (batchEnd >= conceptIdList.size()) batchEnd = conceptIdList.size();
            conceptBatch = conceptIdList.subList(conceptBatchIndex, batchEnd);

            criteria = new T8DataFilterCriteria();

            // Add the organization ID's to the filter.
            if (orgIdList.size() > 1) criteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_ORG_ID, T8DataFilterCriterion.DataFilterOperator.IN, orgIdList);
            else criteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_ORG_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, orgIdList.iterator().next());

            // Add the language to the filter.
            if (languageIdList.size() > 1) criteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_LANGUAGE_ID, T8DataFilterCriterion.DataFilterOperator.IN, languageIdList);
            else criteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_LANGUAGE_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, languageIdList.iterator().next());

            // Add the concept ID's to the filter.
            if (conceptBatch.size() > 1) criteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_CONCEPT_ID, T8DataFilterCriterion.DataFilterOperator.IN, conceptBatch);
            else criteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_CONCEPT_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, conceptBatch.get(0));

            // Retrieve the entities.
            entities = dataAccessProvider.select(entityId, new T8DataFilter(entityId, criteria));

            // Read the entities and create a list of concept terminology.
            for (T8DataEntity entity : entities)
            {
                T8ConceptTerminology conceptTerminology;

                // Construct the terminology and add it to the result list.
                conceptTerminology = constructTerminology(entity);
                terminologyList.add(conceptTerminology);
            }
        }

        // Return the complete list of retrieved terminologies.
        return terminologyList;
    }

    public List<T8ConceptTerminology> loadConceptTerminology(String entityId, String orgId, String languageId, ArrayList<String> conceptIds) throws Exception
    {
        return loadConceptTerminology(entityId, ArrayLists.newArrayList(orgId), ArrayLists.newArrayList(languageId), conceptIds);
    }

    public T8ConceptTerminology loadConceptTerminology(String entityId, String orgId, String languageId, String conceptId) throws Exception
    {
        List<T8ConceptTerminology> terminology;

        terminology = loadConceptTerminology(entityId, ArrayLists.newArrayList(orgId), ArrayLists.newArrayList(languageId), ArrayLists.newArrayList(conceptId));
        return terminology.size() > 0 ? terminology.get(0) : null;
    }

    public static T8ConceptTerminology constructTerminology(T8DataEntity entity)
    {
        T8ConceptTerminology conceptTerminology;
        String conceptId;
        String conceptTypeId;
        String languageId;
        String term;
        String termId;
        String definition;
        String definitionId;
        String abbreviation;
        String abbreviationId;
        String code;
        String codeId;
        String orgId;

        orgId = (String)entity.getFieldValue(EF_ORG_ID);
        conceptTypeId = (String)entity.getFieldValue(EF_CONCEPT_TYPE_ID);
        conceptId = (String)entity.getFieldValue(EF_CONCEPT_ID);
        languageId = (String)entity.getFieldValue(EF_LANGUAGE_ID);
        term = (String)entity.getFieldValue(EF_TERM);
        termId = (String)entity.getFieldValue(EF_TERM_ID);
        definition = (String)entity.getFieldValue(EF_DEFINITION);
        definitionId = (String)entity.getFieldValue(EF_DEFINITION_ID);
        abbreviation = (String)entity.getFieldValue(EF_ABBREVIATION);
        abbreviationId = (String)entity.getFieldValue(EF_ABBREVIATION_ID);
        code = (String)entity.getFieldValue(EF_CODE);
        codeId = (String)entity.getFieldValue(EF_CODE_ID);

        conceptTerminology = new T8ConceptTerminology(T8OntologyConceptType.getConceptType(conceptTypeId), conceptId);
        conceptTerminology.setTerm(term);
        conceptTerminology.setTermId(termId);
        conceptTerminology.setDefinition(definition);
        conceptTerminology.setDefinitionId(definitionId);
        conceptTerminology.setAbbreviation(abbreviation);
        conceptTerminology.setAbbreviationId(abbreviationId);
        conceptTerminology.setCode(code);
        conceptTerminology.setCodeId(codeId);
        conceptTerminology.setLanguageId(languageId);
        conceptTerminology.setOrganizationId(orgId);
        return conceptTerminology;
    }
}
