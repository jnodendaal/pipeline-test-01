package com.pilog.t8.data.ontology.commission;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.api.T8Api;

/**
 * This class server as the API for all server-side code access to the data
 * within the context of a specific organization structure.  This class uses a
 * selection of data handler objects to access and mutate data and applies the
 * necessary business logic required by the organization context.
 *
 * @author Bouwer du Preez
 */
public class T8ServerOntologyCommissioningAPI implements T8Api, T8PerformanceStatisticsProvider
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private T8PerformanceStatistics stats;

    public static final String API_IDENTIFIER = "@API_SERVER_ONTOLOGY_COMMISSIONING";

    public T8ServerOntologyCommissioningAPI(T8ServerContext serverContext, T8SessionContext sessionContext) throws Exception
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }
    
    public T8CommissioningImpactReport determineDRCommissioningImpact(T8DataTransaction tx, T8DataRequirementChangeRequest changeRequest)
    {
        // TODO:  Implement DR Commissioning Impact Report generation.
        return null;
    }
    
    public T8CommissioningImpactReport determineOntologyCommissioningImpact(T8DataTransaction tx, T8OntologyChangeRequest changeRequest)
    {
        // TODO:  Implement Ontology Commissioning Impact Report generation.
        return null;
    }
}
