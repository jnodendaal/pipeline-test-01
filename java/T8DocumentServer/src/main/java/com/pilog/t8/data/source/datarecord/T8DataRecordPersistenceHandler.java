package com.pilog.t8.data.source.datarecord;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.cache.T8DataRecordCache;
import com.pilog.t8.cache.T8DataRecordCache.RecordData;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.structure.DataRecordStructure;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.source.datarequirement.T8RequirementPersistenceHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordAttachmentHandlerDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceDefinition;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.data.resultset.extractor.BigDecimalToIntegerConverter;
import com.pilog.t8.utilities.data.resultset.extractor.GUIDBytesToStringConverter;
import com.pilog.t8.utilities.data.resultset.extractor.GUIDStringConverter;
import com.pilog.t8.utilities.data.resultset.extractor.ResultSetDataExtractor;
import com.pilog.t8.utilities.strings.Strings;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.definition.data.document.datarecord.view.T8DataRecordObjectResource;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.data.resultset.extractor.BigDecimalToLongConverter;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordPersistenceHandler implements T8PerformanceStatisticsProvider
{
    public enum AuditingType {INSERT, UPDATE, DELETE};

    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordPersistenceHandler.class);
    private static final ResultSetDataExtractor DATA_EXTRACTOR = new ResultSetDataExtractor
        (
                new BigDecimalToLongConverter("FILE_SIZE"),
                new BigDecimalToIntegerConverter(),
                new GUIDBytesToStringConverter(),
                new GUIDStringConverter()
        );

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8DataRecordCache recordCache;
    private final String documentTableName;
    private final String documentHistoryTableName;
    private final String documentAttributeTableName;
    private final String recordAttributeHistoryTableName;
    private final String propertyTableName;
    private final String propertyHistoryTableName;
    private final String valueTableName;
    private final String valueHistoryTableName;
    private final String attachmentTableName;
    private final String attachmentHistoryTableName;
    private final String objectTableName;
    private final String attachmentHandlerId;
    private T8AttachmentHandler attachmentHandler;
    private T8PerformanceStatistics stats;

    private static final List<String> AUDITING_COLUMN_NAMES = ArrayLists.typeSafeList("UPDATED_BY_ID", "UPDATED_BY_IID", "UPDATED_BY_TX_IID", "UPDATED_AT", "INSERTED_BY_ID", "INSERTED_BY_IID", "INSERTED_BY_TX_IID", "INSERTED_AT");

    private static final String PE_RETRIEVE_FILE = "RETRIEVE_FILE";
    private static final String PE_INSERT_RECORD = "INSERT_RECORD";
    private static final String PE_UPDATE_RECORD = "UPDATE_RECORD";
    private static final String PE_DELETE_RECORD = "DELETE_RECORD";
    private static final String PE_GET_EXISTING_DATA = "GET_EXISTING_DATA";
    private static final String PE_DR_INSTANCE_RETRIEVE = "DR_INSTANCE_RETRIEVE";
    private static final String PE_INSERT_DOC = "INSERT_DOC";
    private static final String PE_UPDATE_DOC = "UPDATE_DOC";
    private static final String PE_DELETE_DOC = "DELETE_DOC";
    private static final String PE_INSERT_DOC_HISTORY = "INSERT_DOC_HISTORY";
    private static final String PE_INSERT_PROPERTY = "INSERT_PROPERTY";
    private static final String PE_UPDATE_PROPERTY = "UPDATE_PROPERTY";
    private static final String PE_DELETE_PROPERTY = "DELETE_PROPERTY";
    private static final String PE_INSERT_PROPERTY_HISTORY = "INSERT_PROPERTY_HISTORY";
    private static final String PE_INSERT_VALUE = "INSERT_VALUE";
    private static final String PE_UPDATE_VALUE = "UPDATE_VALUE";
    private static final String PE_DELETE_VALUE = "DELETE_VALUE";
    private static final String PE_INSERT_VALUE_HISTORY = "INSERT_VALUE_HISTORY";
    private static final String PE_INSERT_DOC_ATTRIBUTE = "INSERT_DOC_ATTRIBUTE";
    private static final String PE_UPDATE_DOC_ATTRIBUTE = "UPDATE_DOC_ATTRIBUTE";
    private static final String PE_DELETE_DOC_ATTRIBUTE = "DELETE_DOC_ATTRIBUTE";
    private static final String PE_INSERT_DOC_ATTRIBUTE_HISTORY = "INSERT_RECORD_ATTRIBUTE_HISTORY";
    private static final String PE_INSERT_ATTACHMENT = "INSERT_ATTACHMENT";
    private static final String PE_DELETE_ATTACHMENT = "DELETE_ATTACHMENT";
    private static final String PE_INSERT_ATTACHMENT_HISTORY = "INSERT_ATTACHMENT_HISTORY";

    public T8DataRecordPersistenceHandler(T8DataTransaction tx, String documentTableName, String documentAttributeTableName, String propertyTableName, String valueTableName, String attachmentTableName, String attachmentHandlerIdentifier)
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.recordCache = tx.getDataRecordCache();
        this.documentTableName = documentTableName;
        this.documentHistoryTableName = "HIS_DAT_REC";
        this.recordAttributeHistoryTableName = "HIS_DAT_REC_ATR";
        this.propertyHistoryTableName = "HIS_DAT_REC_PRP";
        this.attachmentHistoryTableName = "HIS_DAT_REC_ATC";
        this.documentAttributeTableName = documentAttributeTableName;
        this.propertyTableName = propertyTableName;
        this.valueTableName = valueTableName;
        this.valueHistoryTableName = "HIS_DAT_REC_VAL";
        this.attachmentTableName = attachmentTableName;
        this.objectTableName = T8DataRecordObjectResource.OBJECT_DATA_RECORD_TABLE_NAME;
        this.attachmentHandlerId = attachmentHandlerIdentifier;
        this.stats = tx.getPerformanceStatistics();
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    private void createAttachmentHandler() throws Exception
    {
        T8DefinitionManager definitionManager;

        definitionManager = tx.getDataSession().getDefinitionManager();
        if (attachmentHandlerId != null)
        {
            T8DataRecordAttachmentHandlerDefinition attachmentHandlerDefinition;

            attachmentHandlerDefinition = (T8DataRecordAttachmentHandlerDefinition)definitionManager.getInitializedDefinition(context, null, attachmentHandlerId, null);
            if (attachmentHandlerDefinition != null)
            {
                attachmentHandler = attachmentHandlerDefinition.getNewAttachmentHandlerInstance(tx);
            }
            else throw new Exception("Attachment handler not found: " + attachmentHandlerId);
        }
        else
        {
            T8OrganizationSetupDefinition organizationSetupDefinition;

            organizationSetupDefinition = (T8OrganizationSetupDefinition)definitionManager.getResolvedDefinition(null, T8OrganizationSetupDefinition.TYPE_IDENTIFIER, null);
            if (organizationSetupDefinition != null)
            {
                String attachmentHandlerId;

                attachmentHandlerId = organizationSetupDefinition.getAttachmentHandlerIdentifier();
                if (!Strings.isNullOrEmpty(attachmentHandlerId))
                {
                    T8DataRecordAttachmentHandlerDefinition attachmentHandlerDefinition;

                    attachmentHandlerDefinition = (T8DataRecordAttachmentHandlerDefinition)definitionManager.getInitializedDefinition(context, null, attachmentHandlerId, null);
                    if (attachmentHandlerDefinition != null)
                    {
                        attachmentHandler = attachmentHandlerDefinition.getNewAttachmentHandlerInstance(tx);
                    }
                    else throw new Exception("Attachment handler could not be created because no definition was found in organization setup: " + organizationSetupDefinition);
                }
                else throw new Exception("No Attachment handler specified in organization setup: " + organizationSetupDefinition);
            }
            else throw new Exception("Attachment handler could not be created because no organization setup could be resolved.");
        }
    }

    public final DataRecord retrieveDataRecord(T8DataConnection connection, String recordID, boolean completeRetrieval) throws Exception
    {
        if (completeRetrieval) return retrieveCompleteDataRecord(connection, recordID);
        else return retrieveDataRecord(connection, recordID, null);
    }

    public final DataRecord retrieveDataRecord(T8DataConnection connection, String recordId, DataRequirementInstance inputDataRequirementInstance) throws Exception
    {
        List<Map<String, Object>> recordData;
        List<Map<String, Object>> attributeData;
        List<Map<String, Object>> propertyData;
        List<Map<String, Object>> valueData;
        List<Map<String, Object>> attachmentData;
        RecordData cachedData;

        // Try to find the required data in the cache.
        cachedData = (RecordData)recordCache.get(recordId);
        if (cachedData != null)
        {
            // Get all existing data from the database.
            recordData = cachedData.getDocumentData();
            attributeData = cachedData.getAttributeData();
            propertyData = cachedData.getPropertyData();
            valueData = cachedData.getValueData();
            attachmentData = cachedData.getAttachmentData();
        }
        else
        {
            // Get all existing data from the database.
            recordData = (List)selectDocData(connection, recordId);
            attributeData = (List)selectDocAttributeData(connection, recordId);
            propertyData = (List)selectPropertyData(connection, recordId);
            valueData = (List)selectValueData(connection, recordId);
            attachmentData = (List)selectAttachmentData(connection, recordId);

            // Cache the retrieved data.
            recordCache.put(new RecordData(recordId, recordData, propertyData, valueData, attributeData, attachmentData));
        }

        if (recordData.size() > 0)
        {
            DataRequirementInstance dataRequirementInstance;
            DataRecord retrievedRecord;

            // Create a new data record by either using the supplied data requirement or retrieving the applicable document.
            if (inputDataRequirementInstance == null)
            {
                T8RequirementPersistenceHandler requirementPersistenceHandler;
                String drInstanceId;

                // Retrieve the required Data Requirement Instance.
                stats.logExecutionStart(PE_DR_INSTANCE_RETRIEVE);
                drInstanceId = (String)recordData.get(0).get("DR_INSTANCE_ID");
                requirementPersistenceHandler = new T8RequirementPersistenceHandler(tx);
                dataRequirementInstance = requirementPersistenceHandler.retrieveDataRequirementInstance(connection, drInstanceId);
                stats.logExecutionEnd(PE_DR_INSTANCE_RETRIEVE);
                if (dataRequirementInstance == null) throw new Exception("Data Requirement Instance not found: " + drInstanceId);
            }
            else
            {
                dataRequirementInstance =  inputDataRequirementInstance;
            }

            // Build the data record, cache it and return it.
            retrievedRecord = T8RecordDataHandler.buildDataRecord(dataRequirementInstance, recordData.get(0), attributeData, propertyData, valueData);
            return retrievedRecord;
        }
        else
        {
            LOGGER.log("Record not found: " + recordId);
            return null;
        }
    }

    public final DataRecord retrieveCompleteDataRecord(T8DataConnection connection, String rootRecordId) throws Exception
    {
        Map<String, List<Map<String, Object>>> completeRecordData;
        Map<String, List<Map<String, Object>>> completeAttributeData;
        Map<String, List<Map<String, Object>>> completePropertyData;
        Map<String, List<Map<String, Object>>> completeValueData;
        Map<String, List<Map<String, Object>>> completeRecordAttachmentData;
        List<DataRecord> constructedRecords;

        // Get all existing data from the database.
        stats.logExecutionStart(PE_RETRIEVE_FILE);
        completeRecordData = selectCompleteDocData(connection, rootRecordId);
        completeAttributeData = selectCompleteDocAttributeData(connection, rootRecordId);
        completePropertyData = selectCompletePropertyData(connection, rootRecordId);
        completeValueData = selectCompleteValueData(connection, rootRecordId);
        completeRecordAttachmentData = selectCompleteAttachmentData(connection, rootRecordId);
        stats.logExecutionEnd(PE_RETRIEVE_FILE);

        // Construct the individual records from the complete collection of data.
        if (completeRecordData.size() > 0)
        {
            DataRecordStructure structure;
            DataRecord rootRecord;

            constructedRecords = new ArrayList<>();
            for (String recordId : completeRecordData.keySet())
            {
                T8RequirementPersistenceHandler requirementPersistenceHandler;
                DataRequirementInstance dataRequirementInstance;
                List<Map<String, Object>> recordData;
                List<Map<String, Object>> attributeData;
                List<Map<String, Object>> attachmentData;
                List<Map<String, Object>> propertyData;
                List<Map<String, Object>> valueData;
                String drInstanceId;

                // Get the record data from the compelte collections.
                recordData = completeRecordData.get(recordId);
                attributeData = completeAttributeData.get(recordId);
                attachmentData = completeRecordAttachmentData.get(recordId);
                propertyData = completePropertyData.get(recordId);
                valueData = completeValueData.get(recordId);

                // Retrieve the required Data Requirement Instance.
                stats.logExecutionStart(PE_DR_INSTANCE_RETRIEVE);
                drInstanceId = (String)recordData.get(0).get("DR_INSTANCE_ID");
                requirementPersistenceHandler = new T8RequirementPersistenceHandler(tx);
                dataRequirementInstance = requirementPersistenceHandler.retrieveDataRequirementInstance(connection, drInstanceId);
                stats.logExecutionEnd(PE_DR_INSTANCE_RETRIEVE);
                if (dataRequirementInstance == null) throw new Exception("Data Requirement Instance not found: " + drInstanceId);

                // Build the data record and return it.
                constructedRecords.add(T8RecordDataHandler.buildDataRecord(dataRequirementInstance, recordData.get(0), attributeData, propertyData, valueData));
                recordCache.put(new RecordData(recordId, recordData, propertyData, valueData, attributeData, attachmentData));
            }

            // Arrange the records into the property hierarchy.
            structure = new DataRecordStructure();
            structure.setRecords(rootRecordId, constructedRecords);
            structure.applyStructureToRecords();

            // Return the root record.
            rootRecord = structure.getRootNode().getDataRecord();
            return rootRecord;
        }
        else
        {
            LOGGER.log("Record not found: " + rootRecordId);
            return null;
        }
    }

    public final void insertDataRecord(T8DataTransaction tx, T8DataConnection connection, DataRecord dataRecord) throws Exception
    {
        List<Map<String, Object>> newRecordData;
        List<Map<String, Object>> newAttributeData;
        List<Map<String, Object>> newPropertyData;
        List<Map<String, Object>> newValueData;
        List<Map<String, Object>> newAttachmentData;
        T8SessionContext sessionContext;
        T8Timestamp timestamp;
        String agentId;
        String agentIid;

        // Log the start of the operation.
        stats.logExecutionStart(PE_INSERT_RECORD);

        // Record the formal timestamp to use for this operation.
        timestamp = new T8Timestamp(System.currentTimeMillis());

        // Determine the agent responsible for the operation.
        sessionContext = context.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            agentId = sessionContext.getSystemAgentIdentifier();
            agentIid = sessionContext.getSystemAgentInstanceIdentifier();
        }
        else
        {
            agentId = sessionContext.getUserIdentifier();
            agentIid = null;
        }

        // Normalize the data record to remove redundant (empty) parts.
        dataRecord.normalize();

        // Update the auditing values on the record.
        dataRecord.setInsertedAt(timestamp);
        dataRecord.setInsertedByID(agentId);
        dataRecord.setInsertedByIid(agentIid);

        // Get all the new data from the data requirement.
        newRecordData = T8RecordDataHandler.getDataRecordDataRows(dataRecord);
        newAttributeData = T8RecordDataHandler.getDataRecordAttributeDataRows(dataRecord);
        newPropertyData = T8RecordDataHandler.getRecordPropertyDataRows(dataRecord);
        newValueData = T8RecordDataHandler.getRecordValueDataRows(dataRecord);
        newAttachmentData = T8RecordDataHandler.getRecordAttachmentDataRows(dataRecord);

        // Insert the new record data.
        stats.logExecutionStart(PE_INSERT_DOC);
        insertDocData(connection, newRecordData, agentId, agentIid, timestamp);
        stats.logExecutionEnd(PE_INSERT_DOC);

        // Insert the record history data.
        stats.logExecutionStart(PE_INSERT_DOC_HISTORY);
        insertDocHistoryData(connection, newRecordData, null, null, null, agentId, agentIid, timestamp);
        stats.logExecutionEnd(PE_INSERT_DOC_HISTORY);

        // Insert the new record attribute data.
        stats.logExecutionStart(PE_INSERT_DOC_ATTRIBUTE);
        insertDocAttributeData(connection, newAttributeData, agentId, agentIid, timestamp);
        stats.logExecutionEnd(PE_INSERT_DOC_ATTRIBUTE);

        // Insert the record attribute history data.
        stats.logExecutionStart(PE_INSERT_DOC_ATTRIBUTE_HISTORY);
        insertDocAttributeHistoryData(connection, newAttributeData, null, null, null, agentId, agentIid, timestamp);
        stats.logExecutionEnd(PE_INSERT_DOC_ATTRIBUTE_HISTORY);

        // Insert the new property data.
        stats.logExecutionStart(PE_INSERT_PROPERTY);
        insertPropertyData(connection, newPropertyData);
        stats.logExecutionEnd(PE_INSERT_PROPERTY);

        // Insert the property history data.
        stats.logExecutionStart(PE_INSERT_PROPERTY_HISTORY);
        insertPropertyHistoryData(connection, newPropertyData, null, null, null, agentId, agentIid, timestamp);
        stats.logExecutionEnd(PE_INSERT_PROPERTY_HISTORY);

        // Insert the new value data.
        stats.logExecutionStart(PE_INSERT_VALUE);
        insertValueData(connection, newValueData, agentId, agentIid, timestamp);
        stats.logExecutionEnd(PE_INSERT_VALUE);

        // Insert the value history data.
        stats.logExecutionStart(PE_INSERT_VALUE_HISTORY);
        insertValueHistoryData(connection, newValueData, null, null, null, agentId, agentIid, timestamp);
        stats.logExecutionEnd(PE_INSERT_VALUE_HISTORY);

        // Insert the new attachment data.
        stats.logExecutionStart(PE_INSERT_ATTACHMENT);
        insertAttachmentData(dataRecord, connection, newAttachmentData);
        stats.logExecutionEnd(PE_INSERT_ATTACHMENT);

        // Insert the attachment history data.
        stats.logExecutionStart(PE_INSERT_ATTACHMENT_HISTORY);
        insertAttachmentHistoryData(connection, newAttachmentData, null, agentId, agentIid, timestamp);
        stats.logExecutionEnd(PE_INSERT_ATTACHMENT_HISTORY);

        // Cache the new record.
        recordCache.put(new RecordData(dataRecord.getID(), newRecordData, newPropertyData, newValueData, newAttributeData, newAttachmentData));

        // Log the end of the operation.
        stats.logExecutionEnd(PE_INSERT_RECORD);
    }

    public final Set<String> updateDataRecord(T8DataTransaction tx, T8DataConnection connection, DataRecord dataRecord) throws Exception
    {
        List<Map<String, Object>> oldDocData;
        List<Map<String, Object>> oldDocAttributeData;
        List<Map<String, Object>> oldPropertyData;
        List<Map<String, Object>> oldValueData;
        List<Map<String, Object>> oldAttachmentData;
        List<Map<String, Object>> newDocData;
        List<Map<String, Object>> newDocAttributeData;
        List<Map<String, Object>> newPropertyData;
        List<Map<String, Object>> newValueData;
        List<Map<String, Object>> newAttachmentData;
        List<Map<String, Object>> insertedDocData;
        List<Map<String, Object>> updatedDocData;
        List<Map<String, Object>> insertedPropertyData;
        List<Map<String, Object>> updatedPropertyData;
        List<Map<String, Object>> deletedPropertyData;
        List<Map<String, Object>> insertedValueData;
        List<Map<String, Object>> updatedValueData;
        List<Map<String, Object>> deletedValueData;
        List<Map<String, Object>> insertedAttachmentData;
        List<Map<String, Object>> deletedAttachmentData;
        List<Map<String, Object>> insertedDocAttributeData;
        List<Map<String, Object>> updatedDocAttributeData;
        List<Map<String, Object>> deletedDocAttributeData;
        T8SessionContext sessionContext;
        Set<String> oldSubRecordIdSet;
        Set<String> newSubRecordIdSet;
        RecordData existingData;
        T8Timestamp timestamp;
        String agentId;
        String agentIid;

        // Log the start of the operation.
        stats.logExecutionStart(PE_UPDATE_RECORD);

        // Record the formal timestamp to use for this operation.
        timestamp = new T8Timestamp(System.currentTimeMillis());

        // Determine the agent responsible for the operation.
        sessionContext = context.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            agentId = sessionContext.getSystemAgentIdentifier();
            agentIid = sessionContext.getSystemAgentInstanceIdentifier();
        }
        else
        {
            agentId = sessionContext.getUserIdentifier();
            agentIid = null;
        }

        // Normalize the data record to remove redundant (empty) parts.
        dataRecord.normalize();

        // Get all existing data from the database.
        stats.logExecutionStart(PE_GET_EXISTING_DATA);
        existingData = recordCache.get(dataRecord.getID());
        if (existingData != null)
        {
            oldDocData = existingData.getDocumentData();
            oldPropertyData = existingData.getPropertyData();
            oldValueData = existingData.getValueData();
            oldAttachmentData = existingData.getAttachmentData();
            oldDocAttributeData = existingData.getAttributeData();
        }
        else
        {
            oldDocData = selectDocData(connection, dataRecord.getID());
            oldPropertyData = selectPropertyData(connection, dataRecord.getID());
            oldValueData = selectValueData(connection, dataRecord.getID());
            oldAttachmentData = selectAttachmentData(connection, dataRecord.getID());
            oldDocAttributeData = selectDocAttributeData(connection, dataRecord.getID());
        }
        stats.logExecutionEnd(PE_GET_EXISTING_DATA);

        // Determine the old sub-record ID set.
        if (oldDocData.size() > 0)
        {
            DataRequirement oldDataRequirement;
            String oldDrId;

            // Get the Data Requirement from the record we are saving and compare it with the DR referenced by the old record data.
            oldDataRequirement = dataRecord.getDataRequirement();
            oldDrId = (String)oldDocData.get(0).get("DR_ID");
            if (!oldDataRequirement.getConceptID().equals(oldDrId))
            {
                T8RequirementPersistenceHandler requirementPersistenceHandler;

                // The old record data was based on a different DR (this will only happen if a record's DR Instance is changed) and so we need to retrieve it.
                stats.logExecutionStart(PE_DR_INSTANCE_RETRIEVE);
                requirementPersistenceHandler = new T8RequirementPersistenceHandler(tx);
                oldDataRequirement = requirementPersistenceHandler.retrieveDataRequirement(connection, oldDrId);
                stats.logExecutionEnd(PE_DR_INSTANCE_RETRIEVE);
            }

            // Determine the sub-record ID set.
            oldSubRecordIdSet = getSubRecordIdSet(oldDataRequirement, oldValueData, true, false);
        }
        else
        {
            // Determine the sub-record ID set.
            oldSubRecordIdSet = new HashSet<>();
        }

        // Get all the new data from the data requirement.
        newDocData = T8RecordDataHandler.getDataRecordDataRows(dataRecord);
        newPropertyData = T8RecordDataHandler.getRecordPropertyDataRows(dataRecord);
        newValueData = T8RecordDataHandler.getRecordValueDataRows(dataRecord);
        newAttachmentData = T8RecordDataHandler.getRecordAttachmentDataRows(dataRecord);
        newDocAttributeData = T8RecordDataHandler.getDataRecordAttributeDataRows(dataRecord);
        newSubRecordIdSet = dataRecord.getSubRecordReferenceIDSet(true, false);

        // Remove auditing data retrieved as part of the old and new collections so that it does not interfere with the comparison to follow (which determines what data has been updated/inserted/deleted).
        CollectionUtilities.removeKeys(newPropertyData, AUDITING_COLUMN_NAMES);
        CollectionUtilities.removeKeys(newValueData, AUDITING_COLUMN_NAMES);
        CollectionUtilities.removeKeys(newDocAttributeData, AUDITING_COLUMN_NAMES);
        CollectionUtilities.removeKeys(oldPropertyData, AUDITING_COLUMN_NAMES);
        CollectionUtilities.removeKeys(oldValueData, AUDITING_COLUMN_NAMES);
        CollectionUtilities.removeKeys(oldDocAttributeData, AUDITING_COLUMN_NAMES);

        // Update the record data row.
        if (oldDocData.size() > 0)
        {
            // Update the auditing values of the record.
            dataRecord.setUpdatedAt(timestamp);
            dataRecord.setUpdatedByID(agentId);
            dataRecord.setUpdatedByIid(agentIid);

            // Update document data.
            stats.logExecutionStart(PE_UPDATE_DOC);
            insertedDocData = null;
            updatedDocData = newDocData;
            updateDocData(connection, updatedDocData, timestamp);
            stats.logExecutionEnd(PE_UPDATE_DOC);
        }
        else //  No record to update, so insert the data.
        {
            // Update the auditing values of the record.
            dataRecord.setInsertedAt(timestamp);
            dataRecord.setInsertedByID(agentId);
            dataRecord.setInsertedByIid(agentIid);

            // Insert the new record data.
            stats.logExecutionStart(PE_INSERT_DOC);
            updatedDocData = null;
            insertedDocData = newDocData;
            insertDocData(connection, insertedDocData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_DOC);
        }

        // Update all new and existing property rows.
        {
            // Find the new rows by comparing old and new data maps.  If any of the rows in the new collection do no exist in the old collection, the row will be inserted.
            insertedPropertyData = T8RecordDataHandler.getInsertedPropertyData(oldPropertyData, newPropertyData);
            stats.logExecutionStart(PE_INSERT_PROPERTY);
            insertPropertyData(connection, insertedPropertyData);
            stats.logExecutionEnd(PE_INSERT_PROPERTY);

            // Find the updated rows by comparing old and new data maps.  If any of the non-key field values in the two maps differ, the row will be updated.
            updatedPropertyData = T8RecordDataHandler.getUpdatedPropertyData(oldPropertyData, newPropertyData);
            stats.logExecutionStart(PE_UPDATE_PROPERTY);
            updatePropertyData(connection, updatedPropertyData);
            stats.logExecutionEnd(PE_UPDATE_PROPERTY);
        }

        // Update all new value rows.
        {
            // Find the new rows by comparing old and new data maps.  If any of the rows in the new collection do no exist in the old collection, the row will be inserted.
            insertedValueData = T8RecordDataHandler.getInsertedValueData(oldValueData, newValueData);
            stats.logExecutionStart(PE_INSERT_VALUE);
            insertValueData(connection, insertedValueData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_VALUE);

            // Find the updated rows by comparing old and new data maps.  If any of the non-key field values in the two maps differ, the row will be updated.
            updatedValueData = T8RecordDataHandler.getUpdatedValueData(oldValueData, newValueData);
            stats.logExecutionStart(PE_UPDATE_VALUE);
            updateValueData(connection, updatedValueData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_UPDATE_VALUE);
        }

        // Insert all new attachment rows.
        insertedAttachmentData = T8RecordDataHandler.getInsertedAttachmentData(oldAttachmentData, newAttachmentData);
        stats.logExecutionStart(PE_INSERT_ATTACHMENT);
        insertAttachmentData(dataRecord, connection, insertedAttachmentData);
        stats.logExecutionEnd(PE_INSERT_ATTACHMENT);

        // Insert all new and existing record attribute rows.
        {
            insertedDocAttributeData = T8RecordDataHandler.getInsertedAttributeData(oldDocAttributeData, newDocAttributeData);
            stats.logExecutionStart(PE_INSERT_DOC_ATTRIBUTE);
            insertDocAttributeData(connection, insertedDocAttributeData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_DOC_ATTRIBUTE);

            updatedDocAttributeData = T8RecordDataHandler.getUpdatedAttributeData(oldDocAttributeData, newDocAttributeData);
            stats.logExecutionStart(PE_UPDATE_DOC_ATTRIBUTE);
            updateDocAttributeData(connection, updatedDocAttributeData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_UPDATE_DOC_ATTRIBUTE);
        }

        // Remove all deleted record attribute rows.
        deletedDocAttributeData = T8RecordDataHandler.getDeletedAttributeData(oldDocAttributeData, newDocAttributeData);
        stats.logExecutionStart(PE_DELETE_DOC_ATTRIBUTE);
        deleteDocAttributeData(connection, deletedDocAttributeData);
        stats.logExecutionEnd(PE_DELETE_DOC_ATTRIBUTE);

        // Remove all deleted attachment rows.
        deletedAttachmentData = T8RecordDataHandler.getDeletedAttachmentData(oldAttachmentData, newAttachmentData);
        deleteAttachmentData(connection, deletedAttachmentData);

        // Remove all deleted value rows.
        deletedValueData = T8RecordDataHandler.getDeletedValueData(oldValueData, newValueData);
        CollectionUtilities.sortMaps(deletedValueData, false, "VALUE_SEQUENCE");
        stats.logExecutionStart(PE_DELETE_VALUE);
        deleteValueData(connection, deletedValueData);
        stats.logExecutionEnd(PE_DELETE_VALUE);

        // Remove all deleted property rows.
        deletedPropertyData = T8RecordDataHandler.getDeletedPropertyData(oldPropertyData, newPropertyData);
        stats.logExecutionStart(PE_DELETE_PROPERTY);
        deletePropertyData(connection, deletedPropertyData);
        stats.logExecutionEnd(PE_DELETE_PROPERTY);

        // Insert the history data.
        if (CollectionUtilities.sumSize(
                insertedPropertyData,
                updatedPropertyData,
                deletedPropertyData,
                insertedValueData,
                updatedValueData,
                deletedValueData,
                insertedAttachmentData,
                deletedAttachmentData,
                insertedDocAttributeData,
                updatedDocAttributeData,
                deletedDocAttributeData) > 0) // Only insert history data if something actually changed.
        {
            // Insert the history data.
            stats.logExecutionStart(PE_INSERT_DOC_HISTORY);
            insertDocHistoryData(connection, insertedDocData, updatedDocData, null, oldDocData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_DOC_HISTORY);
            stats.logExecutionStart(PE_INSERT_PROPERTY_HISTORY);
            insertPropertyHistoryData(connection, insertedPropertyData, updatedPropertyData, deletedPropertyData, oldPropertyData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_PROPERTY_HISTORY);
            stats.logExecutionStart(PE_INSERT_VALUE_HISTORY);
            insertValueHistoryData(connection, insertedValueData, updatedValueData, deletedValueData, oldValueData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_VALUE_HISTORY);
            stats.logExecutionStart(PE_INSERT_DOC_ATTRIBUTE_HISTORY);
            insertDocAttributeHistoryData(connection, insertedDocAttributeData, updatedDocAttributeData, deletedDocAttributeData, oldDocAttributeData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_DOC_ATTRIBUTE_HISTORY);
            stats.logExecutionStart(PE_INSERT_ATTACHMENT_HISTORY);
            insertAttachmentHistoryData(connection, insertedAttachmentData, deletedAttachmentData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_ATTACHMENT_HISTORY);
        }

        // Cache the new record.
        recordCache.put(new RecordData(dataRecord.getID(), newDocData, newPropertyData, newValueData, newDocAttributeData, newAttachmentData));

        // Remove all deleted sub-records references.
        {
            Set<String> removedReferences;

            removedReferences = oldSubRecordIdSet;
            removedReferences.removeAll(newSubRecordIdSet);

            // Log the end of the operation.
            stats.logExecutionEnd(PE_UPDATE_RECORD);
            return removedReferences;
        }
    }

    public final Set<String> deleteDataRecord(T8DataTransaction tx, T8DataConnection connection, T8DataSourceDefinition sourceDefinition, T8DataEntityDefinition entityDefinition, String recordId) throws Exception
    {
        List<Map<String, Object>> dataRecordData;

        // Get the record data to be deleted.
        dataRecordData = selectDocData(connection, recordId);
        if (dataRecordData.size() > 0)
        {
            T8RequirementPersistenceHandler requirementPersistenceHandler;
            T8SessionContext sessionContext;
            List<Map<String, Object>> docData;
            List<Map<String, Object>> attributeData;
            List<Map<String, Object>> propertyData;
            List<Map<String, Object>> valueData;
            List<Map<String, Object>> attachmentData;
            String pubKeyFieldIdentifier;
            DataRequirementInstance dataRequirementInstance;
            Set<String> removedReferences;
            RecordData existingData;
            String drInstanceId;
            T8Timestamp timestamp;
            String agentId;
            String agentIid;

            // Record the formal timestamp to use for this operation.
            timestamp = new T8Timestamp(System.currentTimeMillis());

            // Determine the agent responsible for the operation.
            sessionContext = context.getSessionContext();
            if (sessionContext.isSystemSession())
            {
                agentId = sessionContext.getSystemAgentIdentifier();
                agentIid = sessionContext.getSystemAgentInstanceIdentifier();
            }
            else
            {
                agentId = sessionContext.getUserIdentifier();
                agentIid = null;
            }

            // Get all existing data from the database/cache.
            stats.logExecutionStart(PE_GET_EXISTING_DATA);
            existingData = recordCache.get(recordId);
            if (existingData != null)
            {
                docData = existingData.getDocumentData();
                attributeData = existingData.getAttributeData();
                propertyData = existingData.getPropertyData();
                valueData = existingData.getValueData();
                attachmentData = existingData.getAttachmentData();
            }
            else
            {
                docData = selectDocData(connection, recordId);
                attributeData = selectDocAttributeData(connection, recordId);
                propertyData = selectPropertyData(connection, recordId);
                valueData = selectValueData(connection, recordId);
                attachmentData = selectAttachmentData(connection, recordId);
            }
            stats.logExecutionEnd(PE_GET_EXISTING_DATA);

            // Get the DR for the record to be deleted.  We need this to find all dependent references so they can also be deleted.
            stats.logExecutionStart(PE_DR_INSTANCE_RETRIEVE);
            drInstanceId = (String)dataRecordData.get(0).get("DR_INSTANCE_ID");
            requirementPersistenceHandler =  new T8RequirementPersistenceHandler(tx);
            dataRequirementInstance = requirementPersistenceHandler.retrieveDataRequirementInstance(connection, drInstanceId);
            stats.logExecutionEnd(PE_DR_INSTANCE_RETRIEVE);

            // Return the set of references from this document to sub-records.
            pubKeyFieldIdentifier = sourceDefinition.getIdentifier() + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER;
            removedReferences = getSubRecordIdSet(dataRequirementInstance.getDataRequirement(), valueData, true, false);
            for (String removedReference : removedReferences)
            {
                T8DataEntity entityToDelete;

                entityToDelete = entityDefinition.getNewDataEntityInstance();
                entityToDelete.setFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubKeyFieldIdentifier), removedReference);
                tx.delete(entityToDelete);
            }

            // Delete all view data.
            deleteRecordRelatedData(connection, recordId);

            // Insert the attachment history data.
            stats.logExecutionStart(PE_INSERT_ATTACHMENT_HISTORY);
            insertAttachmentHistoryData(connection, null, attachmentData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_ATTACHMENT_HISTORY);

            // Delete all attachments.
            deleteAttachmentData(connection, attachmentData);

            // Insert the value history data.
            stats.logExecutionStart(PE_INSERT_VALUE_HISTORY);
            insertValueHistoryData(connection, null, null, valueData, null, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_VALUE_HISTORY);

            // Delete all values.
            CollectionUtilities.sortMaps(valueData, false, "VALUE_SEQUENCE");
            deleteValueData(connection, valueData);

            // Insert the property history data.
            stats.logExecutionStart(PE_INSERT_PROPERTY_HISTORY);
            insertPropertyHistoryData(connection, null, null, propertyData, null, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_PROPERTY_HISTORY);

            // Delete all properties.
            deletePropertyData(connection, propertyData);

            // Insert the record attribute history data.
            stats.logExecutionStart(PE_INSERT_DOC_ATTRIBUTE_HISTORY);
            insertDocAttributeHistoryData(connection, null, null, attributeData, null, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_DOC_ATTRIBUTE_HISTORY);

            // Delete the record attributes.
            deleteDataRecordAttributeData(connection, recordId);

            // Insert the record history data.
            stats.logExecutionStart(PE_INSERT_DOC_HISTORY);
            insertDocHistoryData(connection, null, null, docData, docData, agentId, agentIid, timestamp);
            stats.logExecutionEnd(PE_INSERT_DOC_HISTORY);

            // Delete the record data.
            deleteDocData(connection, recordId);

            // Delete concept.
            deleteRecordConcept(connection, recordId);

            // Remove the record data from the cache.
            recordCache.remove(recordId);

            // Return the list of referenced record ID's that have been deleted.
            return removedReferences;
        }
        else return new HashSet<>();
    }

    private List<Map<String, Object>> selectDocData(T8DataConnection connection, String recordId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM " + documentTableName + " WHERE RECORD_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            resultSet = selectStatement.executeQuery();
            return DATA_EXTRACTOR.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private Map<String, List<Map<String, Object>>> selectCompleteDocData(T8DataConnection connection, String rootRecordId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM " + documentTableName + " WHERE ROOT_RECORD_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(rootRecordId));
            resultSet = selectStatement.executeQuery();
            return CollectionUtilities.groupDataRows((List)DATA_EXTRACTOR.getResultSetDataRows(resultSet), "RECORD_ID");
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private Map<String, List<Map<String, Object>>> selectCompleteDocAttributeData(T8DataConnection connection, String rootRecordID) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM " + documentAttributeTableName + " WHERE ROOT_RECORD_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(rootRecordID));
            resultSet = selectStatement.executeQuery();
            return CollectionUtilities.groupDataRows((List)DATA_EXTRACTOR.getResultSetDataRows(resultSet), "RECORD_ID");
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectPropertyData(T8DataConnection connection, String recordId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT ROOT_RECORD_ID, RECORD_ID, DR_ID, SECTION_ID, PROPERTY_ID, FFT, VALIDITY_INDICATOR FROM " + propertyTableName + " WHERE RECORD_ID = " + guidParameter + " ORDER BY PROPERTY_ID");
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            resultSet = selectStatement.executeQuery();
            return DATA_EXTRACTOR.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private Map<String, List<Map<String, Object>>> selectCompletePropertyData(T8DataConnection connection, String rootRecordId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT ROOT_RECORD_ID, RECORD_ID, DR_ID, SECTION_ID, PROPERTY_ID, FFT, VALIDITY_INDICATOR FROM " + propertyTableName + " WHERE ROOT_RECORD_ID = " + guidParameter + " ORDER BY RECORD_ID, PROPERTY_ID");
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(rootRecordId));
            resultSet = selectStatement.executeQuery();
            return CollectionUtilities.groupDataRows((List)DATA_EXTRACTOR.getResultSetDataRows(resultSet),"RECORD_ID");
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectValueData(T8DataConnection connection, String recordId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM " + valueTableName + " WHERE RECORD_ID = " + guidParameter + " ORDER BY VALUE_SEQUENCE");
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            resultSet = selectStatement.executeQuery();
            return DATA_EXTRACTOR.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private Map<String, List<Map<String, Object>>> selectCompleteValueData(T8DataConnection connection, String rootRecordID) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM " + valueTableName + " WHERE ROOT_RECORD_ID = " + guidParameter + " ORDER BY RECORD_ID, VALUE_SEQUENCE");
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(rootRecordID));
            resultSet = selectStatement.executeQuery();
            return CollectionUtilities.groupDataRows((List)DATA_EXTRACTOR.getResultSetDataRows(resultSet), "RECORD_ID");
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectAttachmentData(T8DataConnection connection, String recordId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT ATTACHMENT_ID, RECORD_ID, FILE_CONTEXT_ID, FILE_NAME, FILE_SIZE, FILE_DATA, MD5_CHECKSUM, MEDIA_TYPE, ROOT_RECORD_ID FROM " + attachmentTableName + " WHERE RECORD_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            resultSet = selectStatement.executeQuery();
            return DATA_EXTRACTOR.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private Map<String, List<Map<String, Object>>> selectCompleteAttachmentData(T8DataConnection connection, String rootRecordId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM " + attachmentTableName + " WHERE ROOT_RECORD_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(rootRecordId));
            resultSet = selectStatement.executeQuery();
            return CollectionUtilities.groupDataRows((List)DATA_EXTRACTOR.getResultSetDataRows(resultSet), "RECORD_ID");
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectDocAttributeData(T8DataConnection connection, String recordId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM DAT_REC_ATR WHERE RECORD_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            resultSet = selectStatement.executeQuery();
            return DATA_EXTRACTOR.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private void insertDocData(T8DataConnection connection, List<Map<String, Object>> dataRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                // Construct the statement and execute it.
                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                statement = connection.prepareStatement("INSERT INTO " + documentTableName + " (RECORD_ID, ROOT_RECORD_ID, DR_ID, DR_INSTANCE_ID, PARENT_RECORD_ID, FFT, TAGS, INSERTED_BY_ID, INSERTED_BY_IID, INSERTED_AT, PROPERTY_COUNT, PROPERTY_DATA_COUNT, PROPERTY_VALID_COUNT, PROPERTY_VERIFICATION_COUNT, PROPERTY_ACCURATE_COUNT, CHRCTRSTC_COUNT, CHRCTRSTC_DATA_COUNT, CHRCTRSTC_VALID_COUNT, CHRCTRSTC_VERIFICATION_COUNT, CHRCTRSTC_ACCURATE_COUNT) VALUES (" + guidParameter + " ," + guidParameter + " ," + guidParameter + " ," + guidParameter + ", " + guidParameter + ", ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                for (Map<String, Object> dataRow : dataRows)
                {
                    statement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    statement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                    statement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                    statement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_INSTANCE_ID")));
                    statement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PARENT_RECORD_ID")));
                    statement.setString(6, (String)dataRow.get("FFT"));
                    statement.setString(7, (String)dataRow.get("TAGS"));
                    statement.setString(8, agentId);
                    statement.setString(9, agentIid);
                    statement.setTimestamp(10, new Timestamp(timestamp.getMilliseconds()));
                    statement.setInt(11, (int)dataRow.get("PROPERTY_COUNT"));
                    statement.setInt(12, (int)dataRow.get("PROPERTY_DATA_COUNT"));
                    statement.setInt(13, (int)dataRow.get("PROPERTY_VALID_COUNT"));
                    statement.setInt(14, (int)dataRow.get("PROPERTY_VERIFICATION_COUNT"));
                    statement.setInt(15, (int)dataRow.get("PROPERTY_ACCURATE_COUNT"));
                    statement.setInt(16, (int)dataRow.get("CHRCTRSTC_COUNT"));
                    statement.setInt(17, (int)dataRow.get("CHRCTRSTC_DATA_COUNT"));
                    statement.setInt(18, (int)dataRow.get("CHRCTRSTC_VALID_COUNT"));
                    statement.setInt(19, (int)dataRow.get("CHRCTRSTC_VERIFICATION_COUNT"));
                    statement.setInt(20, (int)dataRow.get("CHRCTRSTC_ACCURATE_COUNT"));
                    statement.addBatch();
                }
                statement.executeBatch();
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void insertDocHistoryData(T8DataConnection connection, List<Map<String, Object>> insertedRows, List<Map<String, Object>> updatedRows, List<Map<String, Object>> deletedRows, List<Map<String, Object>> oldRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(insertedRows) || !CollectionUtilities.isNullOrEmpty(updatedRows) || !CollectionUtilities.isNullOrEmpty(deletedRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                // Construct the statement and execute it.
                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();

                // Insert auditing data.
                statement = connection.prepareStatement("INSERT INTO " + documentHistoryTableName + " (TX_IID, TIME, EVENT, RECORD_ID, ROOT_RECORD_ID, DR_ID, DR_INSTANCE_ID, PARENT_RECORD_ID, FFT, OLD_FFT, AGENT_ID, AGENT_IID, USER_ID, SESSION_ID, OPERATION_ID, OPERATION_IID, FLOW_ID, FLOW_IID, TASK_ID, TASK_IID, FUNCTIONALITY_ID, FUNCTIONALITY_IID, PROCESS_ID, PROCESS_IID) VALUES (" + guidParameter + ",?,?," + guidParameter + "," + guidParameter + "," + guidParameter + "," + guidParameter + ",?,?,?,?," + guidParameter + ",?," + guidParameter + ",?," + guidParameter + ",?," + guidParameter + ",?," + guidParameter + ",?," + guidParameter + ",?," + guidParameter + ")");
                setStatementDocHistoryData(dbAdaptor, statement, insertedRows, oldRows, AuditingType.INSERT, agentId, agentIid, timestamp);
                setStatementDocHistoryData(dbAdaptor, statement, updatedRows, oldRows, AuditingType.UPDATE, agentId, agentIid, timestamp);
                setStatementDocHistoryData(dbAdaptor, statement, deletedRows, oldRows, AuditingType.DELETE, agentId, agentIid, timestamp);
                statement.executeBatch();
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void setStatementDocHistoryData(T8DatabaseAdaptor dbAdaptor, PreparedStatement statement, List<Map<String, Object>> dataRows, List<Map<String, Object>> oldRows, AuditingType type, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (dataRows != null)
        {
            for (Map<String, Object> dataRow : dataRows)
            {
                Map<String, Object> oldRow;

                oldRow = oldRows != null ? T8RecordDataHandler.getOldRecordRow(oldRows, dataRow) : null;
                statement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(tx.getIdentifier()));
                statement.setTimestamp(2, new Timestamp(timestamp.getMilliseconds()));
                statement.setString(3, type.toString());
                statement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                statement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                statement.setString(6, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                statement.setString(7, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_INSTANCE_ID")));
                statement.setString(8, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PARENT_RECORD_ID")));
                statement.setString(9, (String)dataRow.get("FFT"));
                statement.setString(10, oldRow != null ? (String)oldRow.get("FFT") : null);
                statement.setString(11, agentId);
                statement.setString(12, dbAdaptor.convertHexStringToSQLGUIDString(agentIid));
                statement.setString(13, context.getUserId());
                statement.setString(14, dbAdaptor.convertHexStringToSQLGUIDString(context.getSessionId()));
                statement.setString(15, context.getOperationId());
                statement.setString(16, dbAdaptor.convertHexStringToSQLGUIDString(context.getOperationIid()));
                statement.setString(17, context.getFlowId());
                statement.setString(18, dbAdaptor.convertHexStringToSQLGUIDString(context.getFlowIid()));
                statement.setString(19, context.getTaskId());
                statement.setString(20, dbAdaptor.convertHexStringToSQLGUIDString(context.getTaskIid()));
                statement.setString(21, context.getFunctionalityId());
                statement.setString(22, dbAdaptor.convertHexStringToSQLGUIDString(context.getFunctionalityIid()));
                statement.setString(23, context.getProcessId());
                statement.setString(24, dbAdaptor.convertHexStringToSQLGUIDString(context.getProcessIid()));
                statement.addBatch();
            }
        }
    }

    private void updateDocData(T8DataConnection connection, List<Map<String, Object>> dataRows, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                // Create the statement and execute it.
                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                statement = connection.prepareStatement("UPDATE " + documentTableName + " SET ROOT_RECORD_ID = " + guidParameter + ", PARENT_RECORD_ID = " + guidParameter + ", DR_ID = " + guidParameter + ", DR_INSTANCE_ID = " + guidParameter + ", FFT = ?, TAGS = ?, UPDATED_BY_ID = ?, UPDATED_BY_IID = ?, UPDATED_AT = ?, PROPERTY_COUNT = ?, PROPERTY_DATA_COUNT = ?, PROPERTY_VALID_COUNT = ?, PROPERTY_VERIFICATION_COUNT = ?, PROPERTY_ACCURATE_COUNT = ?, CHRCTRSTC_COUNT = ?, CHRCTRSTC_DATA_COUNT = ?, CHRCTRSTC_VALID_COUNT = ?, CHRCTRSTC_VERIFICATION_COUNT = ?, CHRCTRSTC_ACCURATE_COUNT = ? WHERE RECORD_ID = " + guidParameter);
                for (Map<String, Object> dataRow : dataRows)
                {
                    statement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                    statement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PARENT_RECORD_ID")));
                    statement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                    statement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_INSTANCE_ID")));
                    statement.setString(5, (String)dataRow.get("FFT"));
                    statement.setString(6, (String)dataRow.get("TAGS"));
                    statement.setString(7, (String)dataRow.get("UPDATED_BY_ID"));
                    statement.setString(8, (String)dataRow.get("UPDATED_BY_IID"));
                    statement.setTimestamp(9, new Timestamp(timestamp.getMilliseconds()));
                    statement.setInt(10, (int)dataRow.get("PROPERTY_COUNT"));
                    statement.setInt(11, (int)dataRow.get("PROPERTY_DATA_COUNT"));
                    statement.setInt(12, (int)dataRow.get("PROPERTY_VALID_COUNT"));
                    statement.setInt(13, (int)dataRow.get("PROPERTY_VERIFICATION_COUNT"));
                    statement.setInt(14, (int)dataRow.get("PROPERTY_ACCURATE_COUNT"));
                    statement.setInt(15, (int)dataRow.get("CHRCTRSTC_COUNT"));
                    statement.setInt(16, (int)dataRow.get("CHRCTRSTC_DATA_COUNT"));
                    statement.setInt(17, (int)dataRow.get("CHRCTRSTC_VALID_COUNT"));
                    statement.setInt(18, (int)dataRow.get("CHRCTRSTC_VERIFICATION_COUNT"));
                    statement.setInt(19, (int)dataRow.get("CHRCTRSTC_ACCURATE_COUNT"));
                    statement.setString(20, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    statement.addBatch();
                }
                statement.executeBatch();
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void deleteDocData(T8DataConnection connection, String recordId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REC WHERE RECORD_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.execute();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void deleteDataRecordAttributeData(T8DataConnection connection, String recordId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REC_ATR WHERE RECORD_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.execute();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void insertDocAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                statement = connection.prepareStatement("INSERT INTO DAT_REC_ATR (RECORD_ID, ATTRIBUTE_ID, VALUE, TYPE, ROOT_RECORD_ID, INSERTED_BY_ID, INSERTED_BY_IID, INSERTED_AT) VALUES (" + guidParameter + ", ?, ?, ?, " + guidParameter + ", ?, " + guidParameter + ", ?)");
                for (Map<String, Object> dataRow : dataRows)
                {
                    statement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    statement.setString(2, (String)dataRow.get("ATTRIBUTE_ID"));
                    statement.setString(3, (String)dataRow.get("VALUE"));
                    statement.setString(4, (String)dataRow.get("TYPE"));
                    statement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                    statement.setString(6, agentId);
                    statement.setString(7, dbAdaptor.convertHexStringToSQLGUIDString(agentIid));
                    statement.setTimestamp(8, new Timestamp(timestamp.getMilliseconds()));
                    statement.addBatch();
                }
                statement.executeBatch();
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void insertDocAttributeHistoryData(T8DataConnection connection, List<Map<String, Object>> insertedRows, List<Map<String, Object>> updatedRows, List<Map<String, Object>> deletedRows, List<Map<String, Object>> oldRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(insertedRows) || !CollectionUtilities.isNullOrEmpty(updatedRows) || !CollectionUtilities.isNullOrEmpty(deletedRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                statement = connection.prepareStatement("INSERT INTO " + recordAttributeHistoryTableName + " (TX_IID, TIME, EVENT, RECORD_ID, ATTRIBUTE_ID, VALUE, OLD_VALUE, TYPE, ROOT_RECORD_ID) VALUES (" + guidParameter + ",?,?," + guidParameter + ",?,?,?,?," + guidParameter + ")");
                setStatementDocAttributeHistoryData(dbAdaptor, statement, insertedRows, oldRows, AuditingType.INSERT, agentId, agentIid, timestamp);
                setStatementDocAttributeHistoryData(dbAdaptor, statement, updatedRows, oldRows, AuditingType.UPDATE, agentId, agentIid, timestamp);
                setStatementDocAttributeHistoryData(dbAdaptor, statement, deletedRows, oldRows, AuditingType.DELETE, agentId, agentIid, timestamp);
                statement.executeBatch();
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void setStatementDocAttributeHistoryData(T8DatabaseAdaptor dbAdaptor, PreparedStatement statement, List<Map<String, Object>> dataRows, List<Map<String, Object>> oldRows, AuditingType type, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (dataRows != null)
        {
            for (Map<String, Object> dataRow : dataRows)
            {
                Map<String, Object> oldRow;

                oldRow = oldRows != null ? T8RecordDataHandler.getOldAttributeRow(oldRows, dataRow) : null;
                statement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(tx.getIdentifier()));
                statement.setTimestamp(2, new Timestamp(timestamp.getMilliseconds()));
                statement.setString(3, type.toString());
                statement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                statement.setString(5, (String)dataRow.get("ATTRIBUTE_ID"));
                statement.setString(6, (String)dataRow.get("VALUE"));
                statement.setString(7, oldRow != null ? (String)oldRow.get("VALUE") : null);
                statement.setString(8, (String)dataRow.get("TYPE"));
                statement.setString(9, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                statement.addBatch();
            }
        }
    }

    private void updateDocAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement updateStatement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                updateStatement = connection.prepareStatement("UPDATE DAT_REC_ATR SET VALUE = ?, TYPE = ?, ROOT_RECORD_ID = " + guidParameter + ", UPDATED_BY_ID = ?, UPDATED_BY_IID = " + guidParameter + ", UPDATED_AT = ? WHERE RECORD_ID = " + guidParameter + " AND ATTRIBUTE_ID = ?");
                for (Map<String, Object> dataRow : dataRows)
                {
                    updateStatement.setString(1, (String)dataRow.get("VALUE"));
                    updateStatement.setString(2, (String)dataRow.get("TYPE"));
                    updateStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                    updateStatement.setString(4, agentId);
                    updateStatement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString(agentIid));
                    updateStatement.setTimestamp(6, new Timestamp(timestamp.getMilliseconds()));
                    updateStatement.setString(7, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    updateStatement.setString(8, (String)dataRow.get("ATTRIBUTE_ID"));
                    updateStatement.execute();
                }
            }
            finally
            {
                if (updateStatement != null) updateStatement.close();
            }
        }
    }

    private void deleteDocAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement deleteStatement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                deleteStatement = connection.prepareStatement("DELETE FROM DAT_REC_ATR WHERE RECORD_ID = " + guidParameter + " AND ATTRIBUTE_ID = ?");
                for (Map<String, Object> dataRow : dataRows)
                {
                    deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    deleteStatement.setString(2, (String)dataRow.get("ATTRIBUTE_ID"));
                    deleteStatement.execute();
                }
            }
            finally
            {
                if (deleteStatement != null) deleteStatement.close();
            }
        }
    }

    private void insertPropertyData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement insertStatement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                insertStatement = connection.prepareStatement("INSERT INTO " + propertyTableName + " (RECORD_ID, ROOT_RECORD_ID, DR_ID, SECTION_ID, PROPERTY_ID, FFT, VALUE_STRING) VALUES (" + guidParameter + ", " + guidParameter + ", " + guidParameter + ", " + guidParameter + ", " + guidParameter + ", ?, ?)");
                for (Map<String, Object> dataRow : dataRows)
                {
                    insertStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    insertStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                    insertStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                    insertStatement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                    insertStatement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                    insertStatement.setString(6, (String)dataRow.get("FFT"));
                    insertStatement.setString(7, (String)dataRow.get("VALUE_STRING"));
                    insertStatement.addBatch();
                }
                insertStatement.executeBatch();
            }
            finally
            {
                if (insertStatement != null) insertStatement.close();
            }
        }
    }

    private void insertPropertyHistoryData(T8DataConnection connection, List<Map<String, Object>> insertedRows, List<Map<String, Object>> updatedRows, List<Map<String, Object>> deletedRows, List<Map<String, Object>> oldRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(insertedRows) || !CollectionUtilities.isNullOrEmpty(updatedRows) || !CollectionUtilities.isNullOrEmpty(deletedRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                statement = connection.prepareStatement("INSERT INTO " + propertyHistoryTableName + " (TX_IID, TIME, EVENT, RECORD_ID, ROOT_RECORD_ID, DR_ID, SECTION_ID, PROPERTY_ID, FFT, OLD_FFT) VALUES (" + guidParameter + ",?,?," + guidParameter + "," + guidParameter + "," + guidParameter + "," + guidParameter + "," + guidParameter + ",?,?)");
                setStatementPropertyHistoryData(dbAdaptor, statement, insertedRows, oldRows, AuditingType.INSERT, agentId, agentIid, timestamp);
                setStatementPropertyHistoryData(dbAdaptor, statement, updatedRows, oldRows, AuditingType.UPDATE, agentId, agentIid, timestamp);
                setStatementPropertyHistoryData(dbAdaptor, statement, deletedRows, oldRows, AuditingType.DELETE, agentId, agentIid, timestamp);
                statement.executeBatch();
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void setStatementPropertyHistoryData(T8DatabaseAdaptor dbAdaptor, PreparedStatement statement, List<Map<String, Object>> dataRows, List<Map<String, Object>> oldRows, AuditingType type, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (dataRows != null)
        {
            for (Map<String, Object> dataRow : dataRows)
            {
                Map<String, Object> oldRow;

                oldRow = oldRows != null ? T8RecordDataHandler.getOldPropertyRow(oldRows, dataRow) : null;
                statement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(tx.getIdentifier()));
                statement.setTimestamp(2, new Timestamp(timestamp.getMilliseconds()));
                statement.setString(3, type.toString());
                statement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                statement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                statement.setString(6, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                statement.setString(7, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                statement.setString(8, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                statement.setString(9, (String)dataRow.get("FFT"));
                statement.setString(10, oldRow != null ? (String)oldRow.get("FFT") : null);
                statement.addBatch();
            }
        }
    }

    private void updatePropertyData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement updateStatement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                updateStatement = connection.prepareStatement("UPDATE " + propertyTableName + " SET ROOT_RECORD_ID = " + guidParameter + ", DR_ID = " + guidParameter + ", SECTION_ID = " + guidParameter + ", FFT = ?, VALUE_STRING = ? WHERE RECORD_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter);
                for (Map<String, Object> dataRow : dataRows)
                {
                    updateStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                    updateStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                    updateStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                    updateStatement.setString(4, (String)dataRow.get("FFT"));
                    updateStatement.setString(5, (String)dataRow.get("VALUE_STRING"));
                    updateStatement.setString(6, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    updateStatement.setString(7, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                    updateStatement.addBatch();
                }
                updateStatement.executeBatch();
            }
            finally
            {
                if (updateStatement != null) updateStatement.close();
            }
        }
    }

    private void deletePropertyData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement deleteStatement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                deleteStatement = connection.prepareStatement("DELETE FROM " + propertyTableName + " WHERE RECORD_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter);
                for (Map<String, Object> dataRow : dataRows)
                {
                    deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    deleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                    deleteStatement.addBatch();
                }
                deleteStatement.executeBatch();
            }
            finally
            {
                if (deleteStatement != null) deleteStatement.close();
            }
        }
    }

    private void insertValueData(T8DataConnection connection, List<Map<String, Object>> dataRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement insertStatement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                insertStatement = connection.prepareStatement("INSERT INTO " + valueTableName + " (RECORD_ID, ROOT_RECORD_ID, DR_ID, SECTION_ID, PROPERTY_ID, DATA_TYPE_ID, DATA_TYPE_SEQUENCE, VALUE_SEQUENCE, PARENT_VALUE_SEQUENCE, VALUE, TEXT, FFT, VALUE_ID, VALUE_CONCEPT_ID, FIELD_ID, INSERTED_BY_ID, INSERTED_BY_IID, INSERTED_BY_TX_IID, INSERTED_AT) VALUES (" + guidParameter + ", " + guidParameter + ", " + guidParameter + ", " + guidParameter + ", " + guidParameter + ", ?, ?, ?, ?, ?, ?, ?, " + guidParameter + ", " + guidParameter + ", " + guidParameter + ", ?, " + guidParameter + ", " + guidParameter + ", ?)");
                for (Map<String, Object> dataRow : dataRows)
                {
                    insertStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    insertStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                    insertStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                    insertStatement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                    insertStatement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                    insertStatement.setString(6, (String)dataRow.get("DATA_TYPE_ID"));
                    insertStatement.setInt(7, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                    insertStatement.setInt(8, (Integer)dataRow.get("VALUE_SEQUENCE"));
                    insertStatement.setObject(9, dataRow.get("PARENT_VALUE_SEQUENCE")); //Can be null, so don't use setInt
                    insertStatement.setString(10, (String)dataRow.get("VALUE"));
                    insertStatement.setString(11, (String)dataRow.get("TEXT"));
                    insertStatement.setString(12, (String)dataRow.get("FFT"));
                    insertStatement.setString(13, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("VALUE_ID")));
                    insertStatement.setString(14, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("VALUE_CONCEPT_ID")));
                    insertStatement.setString(15, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("FIELD_ID")));
                    insertStatement.setString(16, agentId);
                    insertStatement.setString(17, dbAdaptor.convertHexStringToSQLGUIDString(agentIid));
                    insertStatement.setString(18, dbAdaptor.convertHexStringToSQLGUIDString(tx.getIdentifier()));
                    insertStatement.setTimestamp(19, new Timestamp(timestamp.getMilliseconds()));
                    insertStatement.addBatch();
                }
                insertStatement.executeBatch();
            }
            finally
            {
                if (insertStatement != null) insertStatement.close();
            }
        }
    }

    private void insertValueHistoryData(T8DataConnection connection, List<Map<String, Object>> insertedRows, List<Map<String, Object>> updatedRows, List<Map<String, Object>> deletedRows, List<Map<String, Object>> oldRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(insertedRows) || !CollectionUtilities.isNullOrEmpty(updatedRows) || !CollectionUtilities.isNullOrEmpty(deletedRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                statement = connection.prepareStatement("INSERT INTO " + valueHistoryTableName + " (TX_IID, TIME, EVENT, RECORD_ID, ROOT_RECORD_ID, DR_ID, SECTION_ID, PROPERTY_ID, DATA_TYPE_ID, DATA_TYPE_SEQUENCE, VALUE_SEQUENCE, PARENT_VALUE_SEQUENCE, VALUE, TEXT, FFT, VALUE_ID, VALUE_CONCEPT_ID, FIELD_ID, OLD_VALUE, OLD_TEXT, OLD_FFT, OLD_VALUE_ID, OLD_VALUE_CONCEPT_ID) VALUES (" + guidParameter + ",?,?," + guidParameter + "," + guidParameter + "," + guidParameter + "," + guidParameter + "," + guidParameter + ",?,?,?,?,?,?,?," + guidParameter + "," + guidParameter + "," + guidParameter + ",?,?,?," + guidParameter + "," + guidParameter + ")");
                setStatementValueHistoryData(dbAdaptor, statement, insertedRows, oldRows, AuditingType.INSERT, agentId, agentIid, timestamp);
                setStatementValueHistoryData(dbAdaptor, statement, updatedRows, oldRows, AuditingType.UPDATE, agentId, agentIid, timestamp);
                setStatementValueHistoryData(dbAdaptor, statement, deletedRows, oldRows, AuditingType.DELETE, agentId, agentIid, timestamp);
                statement.executeBatch();
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void setStatementValueHistoryData(T8DatabaseAdaptor dbAdaptor, PreparedStatement statement, List<Map<String, Object>> dataRows, List<Map<String, Object>> oldRows, AuditingType type, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (dataRows != null)
        {
            for (Map<String, Object> dataRow : dataRows)
            {
                Map<String, Object> oldRow;

                oldRow = oldRows != null ? T8RecordDataHandler.getOldValueRow(oldRows, dataRow) : null;
                statement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(tx.getIdentifier()));
                statement.setTimestamp(2, new Timestamp(timestamp.getMilliseconds()));
                statement.setString(3, type.toString());
                statement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                statement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                statement.setString(6, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                statement.setString(7, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                statement.setString(8, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                statement.setString(9, (String)dataRow.get("DATA_TYPE_ID"));
                statement.setInt(10, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                statement.setInt(11, (Integer)dataRow.get("VALUE_SEQUENCE"));
                statement.setObject(12, dataRow.get("PARENT_VALUE_SEQUENCE")); //Can be null, so don't use setInt
                statement.setString(13, (String)dataRow.get("VALUE"));
                statement.setString(14, (String)dataRow.get("TEXT"));
                statement.setString(15, (String)dataRow.get("FFT"));
                statement.setString(16, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("VALUE_ID")));
                statement.setString(17, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("VALUE_CONCEPT_ID")));
                statement.setString(18, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("FIELD_ID")));
                statement.setString(19, oldRow != null ? (String)oldRow.get("VALUE") : null);
                statement.setString(20, oldRow != null ? (String)oldRow.get("TEXT") : null);
                statement.setString(21, oldRow != null ? (String)oldRow.get("FFT") : null);
                statement.setString(22, oldRow != null ? dbAdaptor.convertHexStringToSQLGUIDString((String)oldRow.get("VALUE_ID")) : null);
                statement.setString(23, oldRow != null ? dbAdaptor.convertHexStringToSQLGUIDString((String)oldRow.get("VALUE_CONCEPT_ID")) : null);
                statement.addBatch();
            }
        }
    }

    private void updateValueData(T8DataConnection connection, List<Map<String, Object>> dataRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                statement = connection.prepareStatement("UPDATE " + valueTableName + " SET VALUE = ?, TEXT = ?, ROOT_RECORD_ID = " + guidParameter + ", DR_ID = " + guidParameter + ", SECTION_ID = " + guidParameter + ", VALUE_ID = " + guidParameter + ", FFT = ?, PARENT_VALUE_SEQUENCE = ?, VALUE_CONCEPT_ID = " + guidParameter + ", FIELD_ID = " + guidParameter + ", UPDATED_BY_ID = ?, UPDATED_BY_IID = " + guidParameter + ", UPDATED_BY_TX_IID = " + guidParameter + ", UPDATED_AT = ? WHERE RECORD_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter + " AND DATA_TYPE_ID = ? AND DATA_TYPE_SEQUENCE = ? AND VALUE_SEQUENCE = ?");
                for (Map<String, Object> dataRow : dataRows)
                {
                    statement.setString(1, (String)dataRow.get("VALUE"));
                    statement.setString(2, (String)dataRow.get("TEXT"));
                    statement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                    statement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                    statement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                    statement.setString(6, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("VALUE_ID")));
                    statement.setString(7, (String)dataRow.get("FFT"));
                    statement.setObject(8, dataRow.get("PARENT_VALUE_SEQUENCE")); // Can be null so do not use setInt.
                    statement.setObject(9, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("VALUE_CONCEPT_ID")));
                    statement.setObject(10, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("FIELD_ID")));
                    statement.setString(11, agentId);
                    statement.setString(12, dbAdaptor.convertHexStringToSQLGUIDString(agentIid));
                    statement.setString(13, dbAdaptor.convertHexStringToSQLGUIDString(tx.getIdentifier()));
                    statement.setTimestamp(14, new Timestamp(timestamp.getMilliseconds()));
                    statement.setString(15, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    statement.setString(16, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                    statement.setString(17, (String)dataRow.get("DATA_TYPE_ID"));
                    statement.setInt(18, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                    statement.setInt(19, (Integer)dataRow.get("VALUE_SEQUENCE"));
                    statement.addBatch();
                }
                statement.executeBatch();
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void insertAttachmentData(DataRecord dataRecord, T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                if (attachmentHandler == null) createAttachmentHandler();
                statement = connection.prepareStatement("INSERT INTO " + attachmentTableName + " (ROOT_RECORD_ID, RECORD_ID, ATTACHMENT_ID, FILE_CONTEXT_ID, FILE_NAME, FILE_SIZE, FILE_DATA, MD5_CHECKSUM, MEDIA_TYPE) VALUES (" + guidParameter + ", " + guidParameter + ", " + guidParameter + ", ?, ?, ?, ?, ?, ?)");
                for (Map<String, Object> dataRow : dataRows)
                {
                    T8AttachmentDetails attachmentDetails;
                    String drInstanceId;
                    String attachmentId;
                    String fileContextId;

                    // Get the attachment ID and storage File Context ID.
                    attachmentId = (String)dataRow.get("ATTACHMENT_ID");
                    drInstanceId = (String)dataRow.get("DR_INSTANCE_ID");
                    fileContextId = attachmentHandler.getFileContextID(drInstanceId);
                    attachmentDetails = dataRecord.getAttachmentDetails(attachmentId);
                    if (attachmentDetails != null)
                    {
                        // Insert the attachment registry data.
                        statement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                        statement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                        statement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString(attachmentId));
                        statement.setString(4, fileContextId);
                        statement.setString(5, attachmentDetails.getOriginalFileName());
                        statement.setLong(6, attachmentDetails.getFileSize());
                        statement.setBytes(7, new byte[]{0});
                        statement.setString(8, "0"); // This is just an initial value.  The Checksum will be updated when the attachment is imported.
                        statement.setString(9, attachmentDetails.getMediaType());
                        statement.execute();

                        // Import the attachment to the proper storage location.
                        attachmentHandler.importAttachment(drInstanceId, attachmentId, attachmentDetails);
                    }
                    else throw new Exception("Attachment '" + attachmentId + "' source file details not found in Data Record: " + dataRecord);
                }
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void insertAttachmentHistoryData(T8DataConnection connection, List<Map<String, Object>> insertedRows, List<Map<String, Object>> deletedRows, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(insertedRows) || !CollectionUtilities.isNullOrEmpty(deletedRows))
        {
            PreparedStatement statement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                if (attachmentHandler == null) createAttachmentHandler();
                statement = connection.prepareStatement("INSERT INTO " + attachmentHistoryTableName + " (TX_IID, TIME, EVENT, ROOT_RECORD_ID, RECORD_ID, ATTACHMENT_ID, FILE_CONTEXT_ID, FILE_NAME, FILE_SIZE, MD5_CHECKSUM, MEDIA_TYPE) VALUES (" + guidParameter + ",?,?," + guidParameter + ", " + guidParameter + ", " + guidParameter + ", ?, ?, ?, ?, ?)");
                setStatementAttachmentHistoryData(dbAdaptor, statement, insertedRows, AuditingType.INSERT, agentId, agentIid, timestamp);
                setStatementAttachmentHistoryData(dbAdaptor, statement, deletedRows, AuditingType.DELETE, agentId, agentIid, timestamp);
                statement.executeBatch();
            }
            finally
            {
                if (statement != null) statement.close();
            }
        }
    }

    private void setStatementAttachmentHistoryData(T8DatabaseAdaptor dbAdaptor, PreparedStatement statement, List<Map<String, Object>> dataRows, AuditingType type, String agentId, String agentIid, T8Timestamp timestamp) throws Exception
    {
        if (dataRows != null)
        {
            for (Map<String, Object> dataRow : dataRows)
            {
                statement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(tx.getIdentifier()));
                statement.setTimestamp(2, new Timestamp(timestamp.getMilliseconds()));
                statement.setString(3, type.toString());
                statement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ROOT_RECORD_ID")));
                statement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                statement.setString(6, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("ATTACHMENT_ID")));
                statement.setString(7, (String)dataRow.get("CONTEXT_ID"));
                statement.setString(8, (String)dataRow.get("FILE_NAME"));
                statement.setLong(9, (Long)dataRow.get("FILE_SIZE"));
                statement.setString(10, (String)dataRow.get("MD5_CHECKSUM"));
                statement.setString(11, (String)dataRow.get("MEDIA_TYPE"));
                statement.addBatch();
            }
        }
    }

    private void deleteAttachmentData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement deleteStatement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                if (attachmentHandler == null) createAttachmentHandler();
                deleteStatement = connection.prepareStatement("DELETE FROM " + attachmentTableName + " WHERE ATTACHMENT_ID = " + guidParameter);
                for (Map<String, Object> dataRow : dataRows)
                {
                    String drInstanceID;
                    String attachmentID;

                    // Get the attachment ID and storage Context ID.
                    attachmentID = (String)dataRow.get("ATTACHMENT_ID");
                    drInstanceID = (String)dataRow.get("DR_INSTANCE_ID");

                    // Delete the attachment data from its storage location.
                    attachmentHandler.deleteAttachment(drInstanceID, attachmentID);

                    // Delete the attachment registry data.
                    deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(attachmentID));
                    deleteStatement.execute();
                }
            }
            finally
            {
                if (deleteStatement != null) deleteStatement.close();
            }
        }
    }

    private void deleteValueData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement deleteStatement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                deleteStatement = connection.prepareStatement("DELETE FROM " + valueTableName + " WHERE RECORD_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter + " AND DATA_TYPE_ID = ? AND DATA_TYPE_SEQUENCE = ? AND VALUE_SEQUENCE = ?");
                for (Map<String, Object> dataRow : dataRows)
                {
                    deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    deleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                    deleteStatement.setString(3, (String)dataRow.get("DATA_TYPE_ID"));
                    deleteStatement.setInt(4, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                    deleteStatement.setInt(5, (Integer)dataRow.get("VALUE_SEQUENCE"));
                    deleteStatement.addBatch();
                }
                deleteStatement.executeBatch();
            }
            finally
            {
                if (deleteStatement != null) deleteStatement.close();
            }
        }
    }

    private void deleteRecordView(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        if (!CollectionUtilities.isNullOrEmpty(dataRows))
        {
            PreparedStatement deleteStatement = null;

            try
            {
                T8DatabaseAdaptor dbAdaptor;
                String guidParameter;

                dbAdaptor = connection.getDatabaseAdaptor();
                guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
                deleteStatement = connection.prepareStatement("DELETE FROM " + propertyTableName + " WHERE RECORD_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter);
                for (Map<String, Object> dataRow : dataRows)
                {
                    deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_ID")));
                    deleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                    deleteStatement.addBatch();
                }
                deleteStatement.executeBatch();
            }
            finally
            {
                if (deleteStatement != null) deleteStatement.close();
            }
        }
    }

    private void deleteRecordRelatedData(T8DataConnection connection, String recordId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();

            // Delete Views.
            deleteStatement = connection.prepareStatement("DELETE " + objectTableName + " WHERE RECORD_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();

            // Delete Data String Particle Settings.
            deleteStatement = connection.prepareStatement("DELETE DAT_STR_PRT WHERE RECORD_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();

            // Delete Data Strings.
            deleteStatement = connection.prepareStatement("DELETE DAT_STR WHERE RECORD_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();
            deleteStatement.close();

            // Delete Checksums.
            deleteStatement = connection.prepareStatement("DELETE DAT_REC_MTC_HSH WHERE RECORD_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();
            deleteStatement.close();

            // Delete record matches.
            deleteStatement = connection.prepareStatement("DELETE DAT_REC_MTC WHERE RECORD_ID1 = " + guidParameter + " OR RECORD_ID2 = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();
            deleteStatement.close();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void deleteRecordConcept(T8DataConnection connection, String recordId) throws Exception
    {
        PreparedStatement deleteStatement = null;
        T8DatabaseAdaptor dbAdaptor;
        String guidParameter;

        dbAdaptor = connection.getDatabaseAdaptor();
        guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();

        try
        {
            // Delete Terminology.
            deleteStatement = connection.prepareStatement("DELETE ORG_TERMINOLOGY WHERE CONCEPT_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();

            // Delete codes.
            deleteStatement = connection.prepareStatement("DELETE ONTOLOGY_CODE WHERE CONCEPT_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();
            deleteStatement.close();

            // Delete terms.
            deleteStatement = connection.prepareStatement("DELETE ONTOLOGY_TERM WHERE CONCEPT_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();
            deleteStatement.close();

            // Delete definitions.
            deleteStatement = connection.prepareStatement("DELETE ONTOLOGY_DEFINITION WHERE CONCEPT_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();
            deleteStatement.close();

            // Delete concept.
            deleteStatement = connection.prepareStatement("DELETE ORG_ONTOLOGY WHERE CONCEPT_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();
            deleteStatement.close();
            deleteStatement = connection.prepareStatement("DELETE ONTOLOGY_CONCEPT WHERE CONCEPT_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(recordId));
            deleteStatement.executeUpdate();
            deleteStatement.close();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private Set<String> getSubRecordIdSet(DataRequirement dataRequirement, List<Map<String, Object>> dataRows, boolean includeDependentReferences, boolean includeIndependentReferences)
    {
        Set<String> subRecordIdSet;

        subRecordIdSet = new HashSet<>();
        if (dataRows != null)
        {
            for (Map<String, Object> dataRow : dataRows)
            {
                PropertyRequirement propertyRequirement;
                String propertyID;
                String dataTypeID;

                propertyID = (String)dataRow.get("PROPERTY_ID");
                dataTypeID = (String)dataRow.get("DATA_TYPE_ID");

                if (RequirementType.DOCUMENT_REFERENCE.getID().equals(dataTypeID))
                {
                    ValueRequirement valueRequirement;
                    Boolean independent;
                    String subRecordID;

                    propertyRequirement = dataRequirement.getPropertyRequirement(propertyID);
                    valueRequirement = propertyRequirement.getValueRequirement();
                    independent = Boolean.TRUE.equals(valueRequirement.getAttribute(RequirementAttribute.INDEPENDENT.toString()));
                    if ((independent && includeIndependentReferences) || (!independent && includeDependentReferences))
                    {
                        subRecordID = (String)dataRow.get("VALUE");
                        if (subRecordID != null) subRecordIdSet.add(subRecordID);
                    }
                }
            }
        }

        return subRecordIdSet;
    }
}
