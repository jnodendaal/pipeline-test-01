package com.pilog.t8.data.source.datarecord;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResultListener;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ScrollableDataRecordEntityResults implements T8DataEntityResults
{
    private final List<T8DataEntityResultListener> listeners;
    private final T8DataEntityResults idEntityResults;
    private final T8DataRecordDataSource dataSource;
    private final T8DataEntityDefinition entityDefinition;
    private T8DataEntity nextEntity;

    public T8ScrollableDataRecordEntityResults(T8DataRecordDataSource dataSource, T8DataEntityDefinition entityDefinition, T8DataEntityResults idEntityResults)
    {
        this.dataSource = dataSource;
        this.entityDefinition = entityDefinition;
        this.idEntityResults = idEntityResults;
        this.listeners = new ArrayList<>();
    }

    @Override
    public void close()
    {
        this.idEntityResults.close();

        // Fire the closed event
        this.listeners.forEach(T8DataEntityResultListener::entityResultsClosed);

        // Clear the listeners as they will no longer be used
        this.listeners.clear();
    }

    @Override
    public boolean next() throws Exception
    {
        if (!idEntityResults.next())
        {
            return false;
        }
        else
        {
            T8DataEntity nextIDEntity;
            String recordID;

            nextIDEntity = idEntityResults.get();
            recordID = (String)nextIDEntity.getFieldValue(entityDefinition.getIdentifier() + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER);
            // Fetch the actual record/entity
            nextEntity = dataSource.retrieve(entityDefinition, recordID);

            // Fire the retrieved event
            this.listeners.forEach(T8DataEntityResultListener::entityRetrieved);

            // Return the result of the entity retrieval
            return nextEntity != null;
        }
    }

    @Override
    public T8DataEntity get()
    {
        return nextEntity;
    }

    @Override
    public void setStreamType(String fieldIdentifier, StreamType streamType)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getEntityIdentifier()
    {
        return entityDefinition.getIdentifier();
    }

    @Override
    public void addListener(T8DataEntityResultListener listener)
    {
        this.listeners.add(listener);
    }

    @Override
    public void removeListener(T8DataEntityResultListener listener)
    {
        this.listeners.remove(listener);
    }
}
