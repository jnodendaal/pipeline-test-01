package com.pilog.t8.data.document.datarecord.match;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRecordMatchApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordHashCodeMatcher
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final List<T8RecordMatch> recordMatches; // The list of matches identified by this matcher.
    private final Map<String, String> hashCodeFamilies; // Stores hash codes as keys, each of which is linked to the family id assigned to records shared the hash code.
    private final Map<String, List<String>> hashCodeRecords; // Stores hash codes as keys, each of which is linked to the list of records sharing the hash code.
    private String matchInstanceId; // A unique id identifying a match operation.  A new one is generated every time records are matched.
    private String matchCriteriaId; // The unique id of the match criteria used during the record match operation (dependent on source filter i.e. if source filter contains multiple criteria id's, the first one will be recorded).
    private T8DataFilter dataFilter;

    public T8DataRecordHashCodeMatcher(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.recordMatches = new ArrayList<>();
        this.hashCodeFamilies = new HashMap<>();
        this.hashCodeRecords = new HashMap<>();
    }

    public void setSourceFilter(T8DataFilter filter)
    {
        this.dataFilter = filter;
    }

    public void clear()
    {
        hashCodeFamilies.clear();
        hashCodeRecords.clear();
        recordMatches.clear();
        matchInstanceId = null;
        matchCriteriaId = null;
    }

    public List<T8RecordMatch> getRecordMatches()
    {
        return new ArrayList<>(recordMatches);
    }

    public String getMatchCriteriaId()
    {
        return matchCriteriaId;
    }

    public String getMatchInstanceId()
    {
        return matchInstanceId;
    }

    public void matchRecords() throws Exception
    {
        List<T8DataEntity> sourceEntities;
        T8DataTransaction tx;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().getTransaction();

        // Generate a new instance ID.
        matchInstanceId = T8IdentifierUtilities.createNewGUID();

        // Clear existing match collections.
        recordMatches.clear();
        hashCodeFamilies.clear();

        // Loop through all records in order to compare each one to all the others.
        sourceEntities = tx.select(dataFilter.getEntityIdentifier(), dataFilter);
        for (T8DataEntity sourceEntity : sourceEntities)
        {
            List<String> hashCodeRecordIds;
            String criteriaId;
            String caseId;
            String recordId;
            String hashCode;

            // Get the required data from the next entity.
            criteriaId = (String)sourceEntity.getFieldValue(F_MATCH_CRITERIA_ID);
            caseId = (String)sourceEntity.getFieldValue(F_MATCH_CASE_ID);
            recordId = (String)sourceEntity.getFieldValue(F_RECORD_ID);
            hashCode = (String)sourceEntity.getFieldValue(F_HASH_CODE);

            // Record the match criteria id.
            if (matchCriteriaId == null) matchCriteriaId = criteriaId;

            // Get all records that have the same hash code as the retrieved record.
            hashCodeRecordIds = hashCodeRecords.get(hashCode);
            if (hashCodeRecordIds == null)
            {
                hashCodeRecordIds = new ArrayList<>(2);
                hashCodeRecordIds.add(recordId);
                hashCodeRecords.put(hashCode, hashCodeRecordIds);
                hashCodeFamilies.put(hashCode, T8IdentifierUtilities.createNewGUID()); // Generate a new Family id for this hash code.
            }
            else if (!hashCodeRecordIds.contains(recordId)) // Only proceed to process this record if it has not already been done.
            {
                String familyId;

                // Get the family id associated with this hash code.  There has to be one at this stage, since we found records id's associated to the hash code.
                familyId = hashCodeFamilies.get(hashCode);
                for (String matchingRecordId : hashCodeRecordIds)
                {
                    T8RecordMatch match;

                    // Add the new match to the list of record matches.
                    match = new T8RecordMatch(matchInstanceId, criteriaId, caseId, familyId, null, recordId, matchingRecordId);
                    recordMatches.add(match);
                }

                // Lastly, add this record id to the list linked to the hash code.
                hashCodeRecordIds.add(recordId);
            }
        }
    }
}
