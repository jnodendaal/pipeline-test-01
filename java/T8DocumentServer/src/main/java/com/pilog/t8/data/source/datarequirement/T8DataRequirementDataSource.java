package com.pilog.t8.data.source.datarequirement;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.source.T8CommonStatementHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementDataSourceDefinition;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementDataSource implements T8DataSource
{
    private final T8Context context;
    private final T8DataTransaction tx;
    private final T8DataRequirementDataSourceDefinition definition;
    private final String pubKeyFieldIdentifier;
    private final String pubDocumentFieldIdentifier;
    private final T8RequirementPersistenceHandler persistenceHandler;
    private T8DataConnection connection;
    protected T8PerformanceStatistics stats;

    public T8DataRequirementDataSource(T8DataRequirementDataSourceDefinition definition, T8DataTransaction tx)
    {
        this.definition = definition;
        this.tx = tx;
        this.context = tx.getContext();
        this.pubKeyFieldIdentifier = definition.getIdentifier() + T8DataRequirementDataSourceDefinition.KEY_FIELD_IDENTIFIER;
        this.pubDocumentFieldIdentifier = definition.getIdentifier() + T8DataRequirementDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER;
        this.persistenceHandler = new T8RequirementPersistenceHandler(tx);
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.

        // Do a validity check.
        if (context == null) throw new RuntimeException("Null session context obtained from data session.");
    }

    @Override
    public T8DataSourceDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    @Override
    public void open() throws Exception
    {
        connection = tx.getDataConnection(definition.getConnectionIdentifier());
    }

    @Override
    public void close() throws Exception
    {
    }

    @Override
    public void commit() throws Exception
    {
        if (connection != null) connection.commit();
    }

    @Override
    public void rollback() throws Exception
    {
        if (connection != null) connection.rollback();
    }

    @Override
    public void setParameters(Map<String, Object> parameters)
    {
    }

    @Override
    public List<T8DataSourceFieldDefinition> retrieveFieldDefinitions() throws Exception
    {
        ArrayList<T8DataSourceFieldDefinition> definitionList;

        // The field definitions are constants and do not need to be retrieved.
        definitionList = new ArrayList<>();
        definitionList.add(new T8DataSourceFieldDefinition(T8DataRequirementDataSourceDefinition.KEY_FIELD_IDENTIFIER, T8DataRequirementDataSourceDefinition.KEY_FIELD_SOURCE_IDENTIFIER, true, true, T8DataType.GUID));
        definitionList.add(new T8DataSourceFieldDefinition(T8DataRequirementDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER, T8DataRequirementDataSourceDefinition.DOCUMENT_FIELD_SOURCE_IDENTIFIER, false, true, T8DataType.CUSTOM_OBJECT));
        return definitionList;
    }

    @Override
    public int count(T8DataFilter filter) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(filter.getEntityIdentifier());

        queryString = new ParameterizedString("SELECT COUNT(*) AS RECORD_COUNT FROM DATA_REQUIREMENT_DOC ");
        if (filter.hasFilterCriteria())
        {
            queryString.append(filter.getWhereClause(tx, "DATA_REQUIREMENT_DOC"));
            queryString.setParameterValues(filter.getWhereClauseParameters(tx));
        }

        return T8CommonStatementHandler.executeCountQuery(context, connection, queryString, definition, entityDefinition, 0);
    }

    @Override
    public boolean exists(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return count(new T8DataFilter(entityIdentifier, keyMap)) > 0;
    }

    @Override
    public void insert(T8DataEntity dataEntity) throws Exception
    {
        DataRequirement dataRequirement;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = dataEntity.getDefinition();

        dataRequirement = (DataRequirement)dataEntity.getFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubDocumentFieldIdentifier));
        persistenceHandler.insertDataRequirement(tx, connection, dataRequirement);
    }

    @Override
    public void insert(List<T8DataEntity> dataEntities) throws Exception
    {
        for (T8DataEntity t8DataEntity : dataEntities)
        {
            insert(t8DataEntity);
        }
    }

    @Override
    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        DataRequirement dataRequirement;
        T8DataEntityDefinition entityDefinition;
        String key;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        key = (String)keyMap.get(entityDefinition.mapSourceToFieldIdentifier(pubKeyFieldIdentifier));
        dataRequirement = persistenceHandler.retrieveDataRequirement(connection, key);
        if (dataRequirement != null)
        {
            T8DataEntity dataEntity;

            dataEntity = entityDefinition.getNewDataEntityInstance();
            dataEntity.setFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubKeyFieldIdentifier), key);
            dataEntity.setFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubDocumentFieldIdentifier), dataRequirement);
            return dataEntity;
        }
        else return null;
    }

    @Override
    public boolean update(T8DataEntity dataEntity) throws Exception
    {
        DataRequirement dataRequirement;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = dataEntity.getDefinition();

        dataRequirement = (DataRequirement)dataEntity.getFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubDocumentFieldIdentifier));
        persistenceHandler.updateDataRequirement(tx, connection, dataRequirement);
        return true;
    }

    @Override
    public boolean update(List<T8DataEntity> dataEntities) throws Exception
    {
        for (T8DataEntity t8DataEntity : dataEntities)
        {
            update(t8DataEntity);
        }
        return true;
    }

    @Override
    public boolean delete(T8DataEntity dataEntity) throws Exception
    {
        String key;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = dataEntity.getDefinition();

        key = (String)dataEntity.getFieldValue(entityDefinition.mapSourceToFieldIdentifier(pubKeyFieldIdentifier));
        persistenceHandler.deleteDataRequirement(connection, key);
        return true;
    }

    @Override
    public int delete(List<T8DataEntity> dataEntities) throws Exception
    {
        int result;

        // This method of deletion can be optimizes to use a batch statement.
        result = 0;
        for (T8DataEntity dataEntity : dataEntities)
        {
            if (delete(dataEntity)) result++;
        }

        return result;
    }

    @Override
    public int delete(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        return null;
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter, int startOffset, int pageSize) throws Exception
    {
        return null;
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> map) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> map, int i, int i1) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8DataEntityResults scroll(String string, T8DataFilter tdf) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int update(String entityIdentifier, Map<String, Object> updatedValues, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
