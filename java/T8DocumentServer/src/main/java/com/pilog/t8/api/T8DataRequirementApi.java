package com.pilog.t8.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.T8DocumentDataHandler;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.T8DataRequirementComment;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.T8LRUCachedDataRequirementInstanceProvider;
import com.pilog.t8.data.org.T8OrganizationDataRequirementInstanceProvider;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.CollectionUtilities;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementApi implements T8Api, T8PerformanceStatisticsProvider
{
    public static final String API_IDENTIFIER = "@API_DATA_REQUIREMENT";

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8SessionContext sessionContext;
    private final T8ServerContext serverContext;
    private final T8DocumentDataHandler documentDataHandler;
    private T8PerformanceStatistics stats;

    public T8DataRequirementApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.documentDataHandler = new T8DocumentDataHandler(context);
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public DataRequirementInstanceProvider getDrInstanceProvider()
    {
        return new T8LRUCachedDataRequirementInstanceProvider(new T8OrganizationDataRequirementInstanceProvider(this), 100);
    }

    /**
     * Retrieves the specified Data Requirement comments.
     * @param orgIds The list of organization ID's for which to fetch linked
     * comments.
     * @param languageIds The list of language ID's for which to fetch linked
     * comments.
     * @param drIds The list of Data Requirement ID's for which to fetch
     * linked comments.
     * @param drInstanceIds The list of Data Requirement Instance ID's for
     * which to fetch linked comments.
     * @param commentTypeIds The list of comment type to retrieve.
     * @return A list containing the retrieved Data Requirement comments.
     * @throws java.lang.Exception
     */
    public List<T8DataRequirementComment> retrieveDataRequirementComments(Collection<String> orgIds, Collection<String> languageIds, Collection<String> drIds, Collection<String> drInstanceIds, Collection<String> commentTypeIds) throws Exception
    {
        if ((CollectionUtilities.hasContent(drIds)) || (CollectionUtilities.hasContent(drInstanceIds)))
        {
            return documentDataHandler.retrieveDataRequirementComments(tx, orgIds, languageIds, drIds, drInstanceIds, commentTypeIds);
        }
        else throw new IllegalArgumentException("Cannot retrieve Data Requirement comments without at least one DR or DR Instance id.");
    }

    /**
     * Retrieves the specified Data Requirement and includes the ontology and
     * terminology used by the document as requested.
     * @param drId The ID of the Data Requirement to retrieve.
     * @param languageId The language ID to use in case ontology/terminology is
     * requested.
     * @param includeOntology A boolean flag to indicate whether or not the
     * Data Requirement ontology should be retrieved and included.
     * @param includeTerminology A boolean flag to indicate whether or not the
     * terminology of the Data Requirement and the concepts it contains should
     * be retrieved and included.
     * @return The retrieved Data Requirement Document.
     * @throws Exception
     */
    public DataRequirement retrieveDataRequirement(String drId, String languageId, boolean includeOntology, boolean includeTerminology) throws Exception
    {
        DataRequirement dataRequirement;

        // Retrieve the document.
        dataRequirement = documentDataHandler.retrieveDataRequirement(tx, drId);
        if (dataRequirement != null)
        {
            // Add the ontology if required.
            if (includeOntology)
            {
                T8OntologyApi ontApi;

                ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
                dataRequirement.setOntology(ontApi.retrieveConcept(drId));
            }

            // Add the terminology if required.
            if (includeTerminology)
            {
                T8TerminologyApi trmApi;

                trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
                dataRequirement.setContentTerminology(trmApi.getTerminologyProvider());
            }

            // Return the Data Requirement.
            return dataRequirement;
        }
        else return null;
    }

    /**
     * Retrieves the specified Data Requirement Instances and includes the
     * ontology and terminology used by the document as requested.
     * @param drInstanceIds The list of ID's of the Data Requirement Instances to retrieve.
     * @param languageId The language ID to use in case ontology/terminology is
     * requested.
     * @param includeOntology A boolean flag to indicate whether or not the
     * Data Requirement Instance ontology should be retrieved and included.
     * @param includeTerminology A boolean flag to indicate whether or not the
     * terminology of the Data Requirement Instance and the concepts it contains
     * should be retrieved and included.
     * @return The list of retrieved Data Requirement Instance Documents.
     * @throws Exception
     */
    public List<DataRequirementInstance> retrieveDataRequirementInstances(List<String> drInstanceIds, String languageId, boolean includeOntology, boolean includeTerminology) throws Exception
    {
        List<DataRequirementInstance> drInstanceList;

        drInstanceList = new ArrayList<>();
        for (String drInstanceId : drInstanceIds)
        {
            DataRequirementInstance drInstance;

            drInstance = retrieveDataRequirementInstance(drInstanceId, languageId, includeOntology, includeTerminology);
            drInstanceList.add(drInstance);
        }

        return drInstanceList;
    }

    /**
     * Retrieves the specified Data Requirement Instance and includes the
     * ontology and terminology used by the document as requested.
     * @param drInstanceId The ID of the Data Requirement Instance to retrieve.
     * @param languageId The language ID to use in case ontology/terminology is
     * requested.
     * @param includeOntology A boolean flag to indicate whether or not the
     * Data Requirement Instance ontology should be retrieved and included.
     * @param includeTerminology A boolean flag to indicate whether or not the
     * terminology of the Data Requirement Instance and the concepts it contains
     * should be retrieved and included.
     * @return The retrieved Data Requirement Instance Document.
     * @throws Exception
     */
    public DataRequirementInstance retrieveDataRequirementInstance(String drInstanceId, String languageId, boolean includeOntology, boolean includeTerminology) throws Exception
    {
        DataRequirementInstance drInstance;

        // Retrieve the document.
        drInstance = documentDataHandler.retrieveDataRequirementInstance(tx, drInstanceId);
        if (drInstance != null)
        {
            // Add the ontology if required.
            if (includeOntology)
            {
                T8OntologyApi ontApi;

                ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
                drInstance.setOntology(ontApi.retrieveConcept(drInstanceId));
            }

            // Add the terminology if required.
            if (includeTerminology)
            {
                T8TerminologyApi trmApi;

                trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
                drInstance.setContentTerminology(trmApi.getTerminologyProvider());
            }

            // Return the Data Requirement.
            return drInstance;
        }
        else return null;
    }

    public void insertDataRequirementInstance(T8OntologyConcept concept, String drId, boolean independent) throws Exception
    {
        T8OntologyApi ontApi;

        // Insert the ontology.
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        ontApi.insertConcept(concept, true, true, true, true, true);

        // Insert the DR Instance entry.
        documentDataHandler.insertDataRequirementInstance(tx, concept.getID(), drId, independent);
    }

    public void updateDataRequirementInstance(T8OntologyConcept concept, String drId, boolean independent) throws Exception
    {
        T8OntologyApi ontApi;

        // Update the ontology.
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        ontApi.updateConcept(concept, true, true, true, true, true);

        // Insert the DR Instance entry.
        documentDataHandler.updateDataRequirementInstance(tx, concept.getID(), drId, independent);
    }

    public void saveDataRequirementInstance(String drInstanceId, String drId, boolean independent) throws Exception
    {
        // Save the DR Instance entry.
        documentDataHandler.saveDataRequirementInstance(tx, drInstanceId, drId, independent);
    }

    public void saveDataRequirementInstance(T8OntologyConcept concept, String drId, boolean independent) throws Exception
    {
        T8OntologyApi ontApi;

        // Update the ontology.
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        ontApi.saveConcept(concept, false, true, true, true, true, true);

        // Save the DR Instance entry.
        documentDataHandler.saveDataRequirementInstance(tx, concept.getID(), drId, independent);
    }

    public void saveDataRequirement(DataRequirement dr, boolean saveOntology) throws Exception
    {
        // Save the Data Requirement Ontology if required.
        if (saveOntology)
        {
            T8OntologyConcept ontology;

            // Get the Data Requirement ontology.
            ontology = dr.getOntology();

            // Save the ontology.
            if (ontology != null)
            {
                T8OntologyApi ontApi;

                ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
                ontApi.saveConcept(ontology, false, true, true, true, true, true);
            }
        }

        // Save the Data Requirement
         documentDataHandler.saveDataRequirement(tx, dr);
    }

    public void saveDataRequirementInstance(DataRequirementInstance drInstance, boolean saveOntology) throws Exception
    {
        // Save the Data Requirement Instance Ontology if required.
        if (saveOntology)
        {
            T8OntologyConcept ontology;

            // Get the Data Requirement ontology.
            ontology = drInstance.getOntology();

            // Save the ontology.
            if (ontology != null)
            {
                T8OntologyApi ontApi;

                ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
                ontApi.saveConcept(ontology, false, true, true, true, true, true);
            }
        }

        // Save the Data Requirement Instance.
        documentDataHandler.saveDataRequirementInstance(tx, drInstance);
    }

    public void deleteDataRequirement(String drId, boolean deleteOntology) throws Exception
    {
        // Delete the Data Requirement Document.
        documentDataHandler.deleteDataRequirement(tx, drId);

        // Delete the ontology if required.
        if (deleteOntology)
        {
            T8OntologyApi ontApi;

            // Now delete the ontology.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.deleteConcept(drId);
        }
    }

    public void deleteDataRequirementInstance(String drIntanceId, boolean deleteOntology) throws Exception
    {
        // Delete the Data Requirement Document.
        documentDataHandler.deleteDataRequirementInstance(tx, drIntanceId);

        // Delete the ontology if required.
        if (deleteOntology)
        {
            T8OntologyApi ontApi;

            // Now delete the ontology.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.deleteConcept(drIntanceId);
        }
    }
}
