package com.pilog.t8.data.org;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.Composite;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarequirement.PrescribedValue;
import com.pilog.t8.data.document.datarequirement.PrescribedValueRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.prescribedvalue.T8PrescribedValueDataHandler;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.StringValue;
import com.pilog.t8.data.document.datarequirement.OldPrescribedValue;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.api.T8OntologyApi;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import static com.pilog.t8.data.document.requirementtype.RequirementType.CONTROLLED_CONCEPT;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 * @deprecated This class is not longer in use but remains temporarily in order for migration of legacy methods as
 * they become required.
 */
@Deprecated
public class T8ServerPrescribedValueAPI implements T8Api
{
    public static final String API_IDENTIFIER = "@API_SERVER_PRESCRIBED_VALUE";

    private final T8Context context;
    private final String orgId;
    private T8PerformanceStatistics stats;
    private T8OrganizationSetupDefinition setupDefinition;
    private T8PrescribedValueDataHandler prescribedValueDataHandler;
    private final Map<String, List<PrescribedValue>> drPrescribedValueCache; // Caches prescribed values for data requirements <DR_ID, List<OldPrescribedValue>>.

    public T8ServerPrescribedValueAPI(T8Context context)
    {
        this(context, context.getSessionContext().getRootOrganizationIdentifier());
    }

    public T8ServerPrescribedValueAPI(T8Context context, String orgId)
    {
        this.context = context;
        this.orgId = orgId;
        this.drPrescribedValueCache = new HashMap<>();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
        createDataHandlers();
    }

    private void createDataHandlers()
    {
        try
        {
            this.setupDefinition = T8OrganizationSetupDefinition.getOrganizationSetupDefinition(context);
            this.prescribedValueDataHandler = new T8PrescribedValueDataHandler(context);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while creating data handlers.", e);
        }
    }

    private List<PrescribedValue> getDataRequirementPrescribedValues(T8DataTransaction tx, String drId) throws Exception
    {
        List<PrescribedValue> prescribedValues;

        prescribedValues = drPrescribedValueCache.get(drId);
        if (prescribedValues != null)
        {
            return prescribedValues;
        }
        else
        {
            prescribedValues = prescribedValueDataHandler.retrieveDataRequirementPrescribedValues(tx, drId);
            drPrescribedValueCache.put(drId, prescribedValues);
            return prescribedValues;
        }
    }

    public void replaceOldPrescribedValues(T8DataTransaction tx, Collection<DataRecord> records) throws Exception
    {
        // Loop through all of the input records.
        for (DataRecord dataRecord : records)
        {
            replaceOldPrescribedValues(tx, dataRecord.getAllPropertyValues());
        }
    }

    private void replaceOldPrescribedValues(T8DataTransaction tx, List<RecordValue> recordValues) throws Exception
    {
        for (RecordValue recordValue : recordValues)
        {
            if (recordValue.getValueRequirement().isPrescribed())
            {
                replaceOldPrescribedValues(tx, recordValue);
            }
            else replaceOldPrescribedValues(tx, recordValue.getSubValues());
        }
    }

    private void replaceOldPrescribedValues(T8DataTransaction tx, RecordValue value) throws Exception
    {
        List<PrescribedValue> prescribedValues;
        RequirementType requirementType;
        String drId;

        requirementType = value.getRequirementType();
        drId = value.getDataRequirementID();
        prescribedValues = getDataRequirementPrescribedValues(tx, drId);
        for (PrescribedValue prescribedValue : prescribedValues)
        {
            if ((Objects.equals(prescribedValue.getPropertyID(), value.getPropertyID())) && (Objects.equals(prescribedValue.getFieldID(), value.getFieldID())) && (prescribedValue.getRequirementType() == requirementType))
            {
                switch (requirementType)
                {
                    case STRING_TYPE:
                        StringValue stringValue;
                        String currentString;

                        stringValue = (StringValue)value;
                        currentString = Strings.trimToNull(stringValue.getString());
                        if (currentString != null)
                        {
                            for (OldPrescribedValue oldValue : prescribedValue.getOldValues())
                            {
                                if (currentString.equalsIgnoreCase(oldValue.getValue()))
                                {
                                    stringValue.setPrescribedValueID(prescribedValue.getValueID());
                                    stringValue.setString(prescribedValue.getValue());
                                    break;
                                }
                            }
                        }
                        break;
                    case CONTROLLED_CONCEPT:
                        ControlledConcept controlledConcept;
                        String currentConceptId;
                        String currentTerm;

                        controlledConcept = (ControlledConcept)value;
                        currentConceptId = controlledConcept.getConceptId();
                        currentTerm = Strings.trimToNull(controlledConcept.getTerm());

                        if (currentConceptId != null)
                        {
                            for (OldPrescribedValue oldValue : prescribedValue.getOldValues())
                            {
                                if (currentConceptId.equalsIgnoreCase(oldValue.getConceptId()))
                                {
                                    controlledConcept.setPrescribedValueID(prescribedValue.getValueID());
                                    controlledConcept.setConceptId(prescribedValue.getConceptID(), false);
                                    controlledConcept.setTerm(null);
                                    break;
                                }
                            }
                        }
                        else if (currentTerm != null)
                        {
                            for (OldPrescribedValue oldValue : prescribedValue.getOldValues())
                            {
                                if (currentTerm.equalsIgnoreCase(oldValue.getConceptTerm()))
                                {
                                    controlledConcept.setPrescribedValueID(prescribedValue.getValueID());
                                    controlledConcept.setConceptId(prescribedValue.getConceptID(), false);
                                    controlledConcept.setTerm(null);
                                    break;
                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    public void savePrescribedValues(T8DataTransaction tx, DataRecord dataFile) throws Exception
    {
        for (DataRecord dataRecord : dataFile.getDataRecords())
        {
            List<RecordProperty> recordProperties;

            // Check each of the properties.
            recordProperties = dataRecord.getRecordProperties();
            for (RecordProperty recordProperty : recordProperties)
            {
                RecordValue recordValue;

                // Get the property value and if it is not null, save any potential standard values.
                recordValue = recordProperty.getRecordValue();
                if (recordValue != null)
                {
                    savePrescribedValue(tx, recordValue);
                }
            }
        }
    }

    private void savePrescribedValue(T8DataTransaction tx, RecordValue recordValue) throws Exception
    {
        ValueRequirement valueRequirement;

        valueRequirement = recordValue.getValueRequirement();
        if (valueRequirement.getRequirementType() == RequirementType.COMPOSITE_TYPE)
        {
            // Process each field value individually.
            for (Field field : ((Composite)recordValue).getFields())
            {
                RecordValue fieldValue;

                fieldValue = field.getFirstSubValue();
                if (fieldValue != null) savePrescribedValue(tx, fieldValue);
            }
        }

        if (valueRequirement instanceof PrescribedValueRequirement)
        {
            // Process the top level requirement first
            if (((PrescribedValueRequirement)valueRequirement).isPrescribed())
            {
                setDataRequirementValueId(tx, recordValue);
            }
            else // Process the sub-values if the top level is not standardized
            {
                for (RecordValue subValue : recordValue.getSubValues())
                {
                    savePrescribedValue(tx, subValue);
                }
            }
        }
        else
        {
            // Assume that there might be top level types which consist of sub-types where
            // the top level is not standardizable, but all, or some of the sub-types are
            for (RecordValue subValue : recordValue.getSubValues())
            {
                savePrescribedValue(tx, subValue);
            }
        }
    }

    /**
     * Saves the specified data requirement value. If the value already exists,
     * it is simply updated. Otherwise a new entry is created.<br/>
     * <br/>
     *
     * @param tx The {@code T8DataTransaction} with an open connection to the
     *      database
     * @param prescribedValue The {@code PrescribedValue} which
     *      defines the details of the data requirement value to be saved
     *
     * @throws Exception If there is an error during the update or insertion of
     *      the data requirement value
     */
    public void savePrescribedValue(T8DataTransaction tx, PrescribedValue prescribedValue) throws Exception
    {
        this.prescribedValueDataHandler.savePrescribedValue(tx, prescribedValue);
    }

    /**
     * Saves the specified data requirement value. If the value already exists,
     * it is simply updated. Otherwise a new entry is created.<br/>
     * <br/>
     *
     * @param tx The {@code T8DataTransaction} with an open connection to the
     *      database
     * @param prescribedValues The {@code PrescribedValue} which
     *      defines the details of the data requirement value to be saved
     *
     * @throws Exception If there is an error during the update or insertion of
     *      the data requirement value
     */
    public void savePrescribedValues(T8DataTransaction tx, Collection<PrescribedValue> prescribedValues) throws Exception
    {
        for (PrescribedValue prescribedValue : prescribedValues)
        {
            String prescribedValueOrgId;

            prescribedValueOrgId = prescribedValue.getOrganizationID();
            if (orgId.equals(prescribedValueOrgId))
            {
                prescribedValueDataHandler.savePrescribedValue(tx, prescribedValue);
            }
            else throw new IllegalArgumentException("Cannot save prescribed values belonging to different organization: " + prescribedValueOrgId);
        }
    }

    public void insertPrescribedValues(T8DataTransaction tx, Collection<PrescribedValue> prescribedValues) throws Exception
    {
        for (PrescribedValue prescribedValue : prescribedValues)
        {
            String prescribedValueOrgId;

            prescribedValueOrgId = prescribedValue.getOrganizationID();
            if (orgId.equals(prescribedValueOrgId))
            {
                prescribedValueDataHandler.insertPrescribedValue(tx, prescribedValue);
            }
            else throw new IllegalArgumentException("Cannot insert prescribed values belonging to different organization: " + prescribedValueOrgId);
        }
    }

    public void updatePrescribedValues(T8DataTransaction tx, Collection<PrescribedValue> prescribedValues) throws Exception
    {
        for (PrescribedValue prescribedValue : prescribedValues)
        {
            String prescribedValueOrgId;

            prescribedValueOrgId = prescribedValue.getOrganizationID();
            if (orgId.equals(prescribedValueOrgId))
            {
                prescribedValueDataHandler.updatePrescribedValue(tx, prescribedValue);
            }
            else throw new IllegalArgumentException("Cannot update prescribed values belonging to different organization: " + prescribedValueOrgId);
        }
    }

    /**
     * Removes the specified prescribed values. If the value is in use, it is
     * not deleted.
     * </br>The value ID of a existing prescribed value, that is not in use,
     * needs to be passed.<br/>
     *
     * @param tx The {@code T8DataTransaction} with an open connection to the
     *      database
     * @param svIdentifiersList A list of the Prescribed Value ID's that are to
     *      be deleted.
     *
     * @throws Exception If there is an error during the deletion of the data
     *      requirement value
     */
    public void deletePrescribedValues(T8DataTransaction tx, List<String> svIdentifiersList) throws Exception
    {
        //If no prescribed value ID's are given
        if (svIdentifiersList == null) throw new IllegalArgumentException("The Prescribed Value ID List is required.");

        //Delete the values with no usages
        prescribedValueDataHandler.deletePrescribedValues(tx, svIdentifiersList);
    }

    /**
     * Checks the prescribed values list for the combination of the data
     * requirement, property and field identifiers. If a prescribed value
     * matching the input RecordValue is found, the input RecordValue's
     * prescribed Value Id is updated to correspond to the matched value.
     * If not matching prescribed value is found, a new T8PrescribedValue
     * is created and inserted and its value Id is set on the input RecordValue.
     *
     * @param tx The {@code T8DataTransaction} which gives the current
     *      process access to the database.
     * @param recordValue The specific value to be matched or added for
     *      standardization
     */
    private void setDataRequirementValueId(T8DataTransaction tx, RecordValue recordValue) throws Exception
    {
        List<PrescribedValue> prescribedValues;
        PrescribedValue recordValueAsPrescribedValue;

        // Make sure we remove the original standard value ID, if there was one.
        recordValue.setPrescribedValueID(null);

        // Get the data entities for the specific DR_ID, P_ID, F_ID combination.
        prescribedValues = prescribedValueDataHandler.retrievePrescribedValues(tx, recordValue.getValueRequirement());

        // Create a PrescribedValue object from the record value.
        recordValueAsPrescribedValue = PrescribedValue.fromRecordValue(recordValue, this.orgId);

        // If the recordValueAsPrescribedValue is null the dataEntitry should be skipped and not saved.
        if (recordValueAsPrescribedValue != null)
        {

            // If one of the prescribed values matches the RecordValue we received as input, set the RecordValue's value ID accordingly.
            for (PrescribedValue prescribedValue : prescribedValues)
            {
                if (prescribedValue.isEquivalentValue(recordValueAsPrescribedValue))
                {
                    recordValue.setPrescribedValueID(prescribedValue.getValueID());
                    return; // We found a matching Prescribed Value.
                }
            }

            // Insert the new prescribed value.
            recordValueAsPrescribedValue.setValueID(T8IdentifierUtilities.createNewGUID());
            prescribedValueDataHandler.insertPrescribedValue(tx, recordValueAsPrescribedValue);

            // Set the RecordValue's value Id to the new prescribed value's Id.
            recordValue.setPrescribedValueID(recordValueAsPrescribedValue.getValueID());
        }
    }

    /**
     * Retrieves a set of suggested values based on the {@code ValueRequirement}
     * and search prefix string which is specified. The suggestions are returned
     * in a {@code RecordValue} format which enables using the actual object,
     * rather than only the {@code String} value.
     *
     * @param tx The {@code T8DataTransaction} which allows access to
     *      query the required database
     * @param valueRequirement The {@code ValueRequirement} which defines the
     *      value type
     * @param searchPrefx The {@code String} which serves as a prefix for the
     *      suggested value search
     *
     * @return The set of {@code RecordValue} objects which define the set of
     *      suggested values
     *
     * @throws Exception If there is any error during the retrieval of the set
     *      of suggested values
     */
    public List<RecordValue> retrievePrescribedValueSuggestions(T8DataTransaction tx, ValueRequirement valueRequirement, String searchPrefx) throws Exception
    {
        if (valueRequirement == null) throw new IllegalArgumentException("The Value Requirement is a required value.");
        if (searchPrefx == null) searchPrefx = "%";

        return prescribedValueDataHandler.retrievePrescribedValueSuggestions(tx, ArrayLists.typeSafeList(orgId), valueRequirement, searchPrefx);
    }

    /**
     * Replaces all the prescribed value references specified by the supplied
     * filter with the supplied prescribed value.  This method does not affect
     * the prescribed value collection at all, it simply updates record data to
     * use a specified prescribed value in place of others.
     *
     * @param tx The {@code T8DataTransaction} transaction to use for this
     *      operation.
     * @param prescribedValueIDList The prescribed value ID list used to
     *      identify the prescribed values to be replaced.
     * @param replacementValueID The {@code String} prescribed value to replace
     *      the filtered values.
     *
     * @return The {@code Integer} number of references which have been replaced
     *      with the specified prescribed value.
     *
     * @throws Exception If there is any error while updating the prescribed
     *      value references with the new value.
     */
    public Integer replacePrescribedValueReferences(T8DataTransaction tx, List<String> prescribedValueIDList, String replacementValueID) throws Exception
    {
        return this.prescribedValueDataHandler.replacePrescribedValueReferences(tx, prescribedValueIDList, replacementValueID);
    }

    /**
     * Replaces all the prescribed values and references specified by the
     * supplied filter with the supplied prescribed value.  This method replaces
     * the filtered prescribed values in the base list as well as in all record
     * data using the prescribed values to be replaced.
     *
     * @param tx The {@code T8DataTransaction} transaction to use for this
     *      operation.
     * @param prescribedValueIDList The prescribed value ID list used to
     *      identify the prescribed values to be replaced.
     * @param replacementValueID The {@code String} prescribed value to replace
     *      the filtered values.
     *
     * @return The {@code Integer} number of references which have been replaced
     *      with the specified prescribed value.
     *
     * @throws Exception If there is any error while updating the prescribed
     *      value references with the new value.
     */
    public Integer replacePrescribedValues(T8DataTransaction tx, List<String> prescribedValueIDList, String replacementValueID) throws Exception
    {
        return this.prescribedValueDataHandler.replacePrescribedValues(tx, prescribedValueIDList, replacementValueID);
    }

    /**
     * Saves the specified concept and sets the old value accordingly for all affected prescribed values.
     * @param tx The transaction to use.
     * @param concept The new concept to save, updating affected prescribed values.
     * @throws Exception
     */
    public void savePrescribedConcept(T8DataTransaction tx, T8OntologyConcept concept) throws Exception
    {
         T8OntologyApi ontApi;
         T8OntologyConcept oldConcept;
         String drvEntityIdentifier;
         T8DataFilter filter;
         List<T8DataEntity> conceptUsages;

        drvEntityIdentifier = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;

        // API for saving and using concept.
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);

        // Get the old concept before saving the new concept.
        oldConcept = ontApi.retrieveConcept(concept.getID(), false, true, false, false, false);

        // Perform the API operation (Save updated concept).
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        // Create a filter to find the valueId's that used the concept.
        filter = new T8DataFilter(drvEntityIdentifier);
        filter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, drvEntityIdentifier + EF_CONCEPT_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, concept.getID(), false);

        // Get the valueId's that use the concept
        conceptUsages = tx.select(drvEntityIdentifier, filter);

        //If the term of the concept did change then save the old values
        for (T8OntologyTerm oldTerm : oldConcept.getTerms())
        {
            T8OntologyTerm newTerm;

            newTerm = concept.getTerm(oldTerm.getID());

            if (!Objects.equals(oldTerm.getTerm(), newTerm.getTerm()))
            {
                for(T8DataEntity currentValue : conceptUsages)
                {
                    OldPrescribedValue oldValue;

                    //Build up oldPrescribedValue to save
                    oldValue = new OldPrescribedValue(CONTROLLED_CONCEPT, T8IdentifierUtilities.createNewGUID());
                    oldValue.setConceptId(oldConcept.getID());
                    oldValue.setConceptTerm(oldTerm.getTerm());
                    oldValue.setValueId(currentValue.getFieldValue(drvEntityIdentifier+EF_VALUE_ID).toString());

                    //Save old value concept
                    prescribedValueDataHandler.insertOldPrescribedValue(tx, oldValue);
                }
            }
        }
    }

    /**
     * Sets the standard flag for a specific data requirement value. The
     * flag passed is set for the specified standard value ID.<br/>
     * <br/>
     *
     * @param tx The {@code T8DataTransaction} with an open connection to the
     *      database
     * @param prescribedValueId The {@code String} ID of the standard value to be
     *      approved
     * @param standardIndicator {@code true} to flag as standardized. {@code false}
     *      to remove
     *
     * @throws Exception If any error occurs while setting the standardized flag
     *      for the specified value
     */
    public void setStandard(T8DataTransaction tx, String prescribedValueId, boolean standardIndicator) throws Exception
    {
        this.prescribedValueDataHandler.setStandard(tx, prescribedValueId, standardIndicator);
    }

    /**
     * Sets the approved flag for a specific data requirement value. The
     * flag passed is set for the specified standard value ID.<br/>
     * <br/>
     *
     * @param tx The {@code T8DataTransaction} with an open connection to the
     *      database
     * @param standardValueID The {@code String} ID of the standard value to be
     *      approved
     * @param approvedIndicator {@code true} to flag as standardized. {@code false}
     *      to remove
     *
     * @throws Exception If any error occurs while setting the standardized flag
     *      for the specified value
     */
    public void setApproved(T8DataTransaction tx, String standardValueID, boolean approvedIndicator) throws Exception
    {
        this.prescribedValueDataHandler.setApproved(tx, standardValueID, approvedIndicator);
    }
}
