package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordDataSourceDefinition;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordPersistenceUtilities
{
    public static void saveDataRecord(T8DataTransaction tx, String entityIdentifier, DataRecord dataRecord) throws Exception
    {
        Map<String, Object> fieldMap;
        T8DataEntity recordEntity;

        // Construct the key map.
        fieldMap = new HashMap<>();
        fieldMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER, dataRecord.getID());
        fieldMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER, dataRecord);

        // Save the entity (Documents can be updated even if they do not exist - the source will insert/update where necessary).
        recordEntity = tx.create(entityIdentifier, fieldMap);
        tx.update(recordEntity);
    }
    
    public static DataRecord retrieveDataRecord(T8DataTransaction tx, String entityIdentifier, String recordID) throws Exception
    {
        Map<String, Object> keyMap;
        T8DataEntity recordEntity;

        // Construct the key map.
        keyMap = new HashMap<>();
        keyMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER, recordID);

        // Retrieve the entity and return the document it contains.
        recordEntity = tx.retrieve(entityIdentifier, keyMap);
        if (recordEntity != null)
        {
            DataRecord dataRecord;
            
            // Get the retrieved document.
            dataRecord = (DataRecord)recordEntity.getFieldValue(entityIdentifier + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);
            
            // Return the retrieved document.
            return dataRecord;
        }
        else return null;
    }
    
    public static void saveDataRecordTree(T8DataTransaction tx, String entityIdentifier, DataRecord dataRecord) throws Exception
    {
        for (DataRecord treeDataRecord : dataRecord.getDataRecords())
        {
            saveDataRecord(tx, entityIdentifier, treeDataRecord);
        }
    }
    
    public static DataRecord retrieveDataRecordTree(T8DataTransaction tx, String entityIdentifier, String recordID) throws Exception
    {
        Map<String, Object> keyMap;
        T8DataEntity recordEntity;

        // Construct the key map.
        keyMap = new HashMap<>();
        keyMap.put(entityIdentifier + T8DataRecordDataSourceDefinition.KEY_FIELD_IDENTIFIER, recordID);

        // Retrieve the entity and return the document it contains.
        recordEntity = tx.retrieve(entityIdentifier, keyMap);
        if (recordEntity != null)
        {
            DataRecord dataRecord;
            
            // Get the retrieved document.
            dataRecord = (DataRecord)recordEntity.getFieldValue(entityIdentifier + T8DataRecordDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);
            
            // Add all sub-records if required.
            if (dataRecord != null)
            {
                for (String subRecordID : dataRecord.getSubRecordReferenceIDSet(true, true))
                {
                    DataRecord subRecord;
                    
                    subRecord = T8DataRecordPersistenceUtilities.retrieveDataRecordTree(tx, entityIdentifier, subRecordID);
                    if (subRecord != null) dataRecord.addSubRecord(subRecord);
                }
            }
            
            // Return the retrieved document.
            return dataRecord;
        }
        else return null;
    }
}
