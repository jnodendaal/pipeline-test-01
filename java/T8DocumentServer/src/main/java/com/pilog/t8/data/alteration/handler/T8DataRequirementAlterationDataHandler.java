package com.pilog.t8.data.alteration.handler;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.data.resultset.extractor.BigDecimalToIntegerConverter;
import com.pilog.t8.utilities.data.resultset.extractor.GUIDBytesToStringConverter;
import com.pilog.t8.utilities.data.resultset.extractor.GUIDStringConverter;
import com.pilog.t8.utilities.data.resultset.extractor.ResultSetDataExtractor;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.*;
import com.pilog.t8.data.source.datarequirement.T8RequirementDataHandler;
import com.pilog.t8.utilities.collections.HashMaps;

import static com.pilog.t8.definition.api.T8DataAlterationApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementAlterationDataHandler implements T8PerformanceStatisticsProvider
{
    private final T8DataTransaction tx;
    private T8PerformanceStatistics stats;

    private static final ResultSetDataExtractor dataExtractor = new ResultSetDataExtractor(new BigDecimalToIntegerConverter(), new GUIDBytesToStringConverter(), new GUIDStringConverter());

    public T8DataRequirementAlterationDataHandler(T8DataTransaction tx)
    {
        this.tx = tx;
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public final DataRequirementInstance retrieveDataRequirementInstance(String packageIid, int step) throws Exception
    {
        List<T8DataEntity> dataRequirementInstanceData;

        // Get all existing data from the database.
        dataRequirementInstanceData = tx.select(ALT_DR_INSTANCE_DE_IDENTIFIER, new T8DataFilter(ALT_DR_INSTANCE_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_INSTANCE_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_INSTANCE_DE_IDENTIFIER + EF_STEP, step)));
        if (dataRequirementInstanceData.size() > 0)
        {
            DataRequirement dataRequirement;
            String independentIndicator;
            String drID;

            drID = (String)dataRequirementInstanceData.get(0).getFieldValue(ALT_DR_INSTANCE_DE_IDENTIFIER + EF_DR_ID);
            independentIndicator = (String)dataRequirementInstanceData.get(0).getFieldValue(ALT_DR_INSTANCE_DE_IDENTIFIER + EF_INDEPENDENT_INDICATOR);
            dataRequirement = retrieveDataRequirement(packageIid, step);
            if (dataRequirement != null)
            {
                DataRequirementInstance drInstance;

                drInstance = new DataRequirementInstance((String) dataRequirementInstanceData.get(0).getFieldValue(ALT_DR_INSTANCE_DE_IDENTIFIER + EF_DR_INSTANCE_ID), dataRequirement);
                drInstance.setIndependent(independentIndicator != null ? independentIndicator.equalsIgnoreCase("Y") : true);

                return drInstance;
            }
            else return null;
        }
        else
        {
            T8Log.log("Data Requirement Instance not found.");
            return null;
        }
    }

    /**
     * @param tx The transaction to use for this operation.
     * @param packageIid The unique instance id of the alteration package to retrieve.
     * @param step
     * @return The specified package concept method.
     * @throws Exception
     */
    public String retrieveDataRequirementAlterationMethod(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        T8DataEntity entity;
        String entityId;
        Map<String, Object> filterMap;
        String method;

        // Get the entity identifier from the model definition.
        entityId = ALT_DR_DOC_DE_IDENTIFIER;

        // Create a map to filter out only the required records.
        filterMap = new HashMap<>();
        filterMap.put(entityId + EF_PACKAGE_IID, packageIid);
        filterMap.put(entityId + EF_STEP, step);

        // Retreive the package details.
        entity = tx.retrieve(entityId, filterMap);

        // Retrieve the method from the entity.
        method = (String) entity.getFieldValue(entityId + EF_METHOD);

        // Return the Tconcept method.
        return method;
    }

    public final void insertDataRequirementInstance(String packageIid, int step, String method, DataRequirementInstance dataRequirementInstance) throws Exception
    {
        List<T8DataEntity> newDRInstanceData;
        List<Map<String, Object>> rows;

        rows = T8RequirementDataHandler.getDataRequirementInstanceDataRows(dataRequirementInstance);
        rows.get(0).put("PACKAGE_IID", packageIid);
        rows.get(0).put("STEP", step);
        rows.get(0).put("METHOD", method);

        // Get all the new data from the data requirement instance.
        newDRInstanceData = createEntities(tx, rows, ALT_DR_INSTANCE_DE_IDENTIFIER);

        // Insert the data requirement data rows.
        tx.insert(newDRInstanceData);
    }

    public final void updateDataRequirementInstance(String packageIid, int step, String method,DataRequirementInstance drInstance) throws Exception
    {
        List<T8DataEntity> oldDRInstanceData;
        List<T8DataEntity> newDRInstanceData;
        String drInstanceId;
        List<Map<String, Object>> rows;

        // Get all existing data from the database.
        drInstanceId = drInstance.getConceptID();
        oldDRInstanceData = tx.select(ALT_DR_INSTANCE_DE_IDENTIFIER, new T8DataFilter(ALT_DR_INSTANCE_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_INSTANCE_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_INSTANCE_DE_IDENTIFIER + EF_STEP, step, ALT_DR_INSTANCE_DE_IDENTIFIER + EF_DR_INSTANCE_ID, drInstanceId)));

        // Get all the new data from the data requirement instance.
        rows = T8RequirementDataHandler.getDataRequirementInstanceDataRows(drInstance);
        rows.get(0).put("PACKAGE_IID", packageIid);
        rows.get(0).put("STEP", step);
        rows.get(0).put("METHOD", method);
        newDRInstanceData = createEntities(tx, rows, ALT_DR_INSTANCE_DE_IDENTIFIER);

        // Update the data requirement data row.
        if (oldDRInstanceData.size() > 0)
        {
            tx.update(newDRInstanceData);
        }
        else // No data requirement to update, so insert the data.
        {
            tx.insert(newDRInstanceData);
        }
    }

    public final void deleteDataRequirementInstance(String packageIid, int step) throws Exception
    {
        tx.delete(ALT_DR_INSTANCE_DE_IDENTIFIER, new T8DataFilter(ALT_DR_INSTANCE_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_INSTANCE_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_INSTANCE_DE_IDENTIFIER + EF_STEP, step)));
    }

    public final DataRequirement retrieveDataRequirement(String packageIid, int step) throws Exception
    {
        List<T8DataEntity> dataRequirementData;
        List<T8DataEntity> dataRequirementAttributeData;
        List<T8DataEntity> propertyRequirementData;
        List<T8DataEntity> propertyRequirementAttributeData;
        List<T8DataEntity> valueRequirementData;
        List<T8DataEntity> valueRequirementAttributeData;
        T8DataFilter drFilter;
        T8DataFilter drAttributeFilter;
        T8DataFilter propertyFilter;
        T8DataFilter propertyAttributeFilter;
        T8DataFilter dataTypeFilter;
        T8DataFilter dataTypeAttributeFilter;

        // Create filters for data retrieval.
        drFilter = new T8DataFilter(ALT_DR_DOC_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DOC_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DOC_DE_IDENTIFIER + EF_STEP, step));
        drAttributeFilter = new T8DataFilter(ALT_DR_DOC_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_STEP, step));
        propertyFilter = new T8DataFilter(ALT_DR_PROPERTY_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_PROPERTY_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_PROPERTY_DE_IDENTIFIER + EF_STEP, step));
        propertyFilter.addFieldOrdering(ALT_DR_PROPERTY_DE_IDENTIFIER + EF_PROPERTY_SEQUENCE, OrderMethod.ASCENDING);
        propertyAttributeFilter = new T8DataFilter(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_STEP, step));
        dataTypeFilter = new T8DataFilter(ALT_DR_DATA_TYPE_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_STEP, step));
        dataTypeFilter.addFieldOrdering(ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_DATA_TYPE_SEQUENCE, OrderMethod.ASCENDING);
        dataTypeAttributeFilter = new T8DataFilter(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_STEP, step));

        // Get all existing data from the database.
        dataRequirementData = tx.select(ALT_DR_DOC_DE_IDENTIFIER, drFilter);
        dataRequirementAttributeData = tx.select(ALT_DR_DOC_ATR_DE_IDENTIFIER, drAttributeFilter);
        propertyRequirementData = tx.select(ALT_DR_PROPERTY_DE_IDENTIFIER, propertyFilter);
        valueRequirementData = tx.select(ALT_DR_DATA_TYPE_DE_IDENTIFIER, dataTypeFilter);
        propertyRequirementAttributeData = tx.select(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER, propertyAttributeFilter);
        valueRequirementAttributeData = tx.select(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER, dataTypeAttributeFilter);

        if (dataRequirementData.size() > 0)
        {
            DataRequirement dataRequirement;
            T8Timestamp insertedAt;
            T8Timestamp updatedAt;

            // Check auditing fields.
            insertedAt = (T8Timestamp) dataRequirementData.get(0).getFieldValue(ALT_DR_DOC_DE_IDENTIFIER + EF_INSERTED_AT);
            updatedAt = (T8Timestamp)dataRequirementData.get(0).getFieldValue(ALT_DR_DOC_DE_IDENTIFIER + EF_UPDATED_AT);

            // Create the new Data Requirement.
            dataRequirement = new DataRequirement();
            dataRequirement.setConceptID((String) dataRequirementData.get(0).getFieldValue(ALT_DR_DOC_DE_IDENTIFIER + EF_DR_ID));
            dataRequirement.setClassConceptID((String)dataRequirementData.get(0).getFieldValue(ALT_DR_DOC_DE_IDENTIFIER + EF_CLASS_ID));
            dataRequirement.setInsertedByID((String)dataRequirementData.get(0).getFieldValue(ALT_DR_DOC_DE_IDENTIFIER + EF_INSERTED_BY_ID));
            dataRequirement.setInsertedAt(insertedAt != null ? insertedAt : null);
            dataRequirement.setUpdatedByID((String)dataRequirementData.get(0).getFieldValue(ALT_DR_DOC_DE_IDENTIFIER + EF_UPDATED_BY_ID));
            dataRequirement.setUpdatedAt(updatedAt != null ? updatedAt : null);

            // Add all Property Requirements.
            for (T8DataEntity propertyDataRow : propertyRequirementData)
            {
                PropertyRequirement propertyRequirement;
                SectionRequirement sectionRequirement;
                String sectionId;
                String propertyId;
                boolean characteristic;

                // Get the required parameters from the data row.
                sectionId = (String)propertyDataRow.getFieldValue(ALT_DR_PROPERTY_DE_IDENTIFIER + EF_SECTION_ID);
                propertyId = (String)propertyDataRow.getFieldValue(ALT_DR_PROPERTY_DE_IDENTIFIER + EF_PROPERTY_ID);
                characteristic = "Y".equalsIgnoreCase((String)propertyDataRow.getFieldValue(ALT_DR_PROPERTY_DE_IDENTIFIER + EF_CHARACTERISTIC));

                // Add the Section Requirement to the Data Requirement if it does not already exist.
                sectionRequirement = dataRequirement.getSectionRequirement(sectionId);
                if (sectionRequirement == null)
                {
                    sectionRequirement = new SectionRequirement();
                    sectionRequirement.setConceptID(sectionId);
                    dataRequirement.addSectionRequirement(sectionRequirement);
                }

                // Construct the section property.
                propertyRequirement = new PropertyRequirement();
                propertyRequirement.setConceptID(propertyId);
                propertyRequirement.setCharacteristic(characteristic);

                // Add the Property Requirement attributes.
                for (T8DataEntity propertyAttributeDataRow : propertyRequirementAttributeData)
                {
                    String attributeID;
                    String type;
                    String value;
                    String atrPropertyId;

                    atrPropertyId = (String)propertyAttributeDataRow.getFieldValue(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_PROPERTY_ID);

                    if (atrPropertyId != null && atrPropertyId.equals(propertyId))
                    {
                        attributeID = (String)propertyAttributeDataRow.getFieldValue(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_ATTRIBUTE_ID);
                        value = (String)propertyAttributeDataRow.getFieldValue(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_VALUE);
                        type = (String)propertyAttributeDataRow.getFieldValue(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_TYPE);

                        if (type == null)
                        {
                            propertyRequirement.setAttribute(attributeID, value);
                        }
                        else
                        {
                            switch (type)
                            {
                                case "NUMBER":
                                    propertyRequirement.setAttribute(attributeID, new BigDecimal(value));
                                    break;
                                case "BOOLEAN":
                                    propertyRequirement.setAttribute(attributeID, Boolean.parseBoolean(value));
                                    break;
                                default:
                                    propertyRequirement.setAttribute(attributeID, value);
                            }
                        }
                    }
                }
                sectionRequirement.addPropertyRequirement(propertyRequirement);

                // Construct the property value.
                propertyRequirement.setValueRequirement(buildPropertyValueRequirement(valueRequirementData,valueRequirementAttributeData, propertyRequirement));
            }

            // Add the Data Requirement attributes.
            for (T8DataEntity attributeDataRow : dataRequirementAttributeData)
            {
                String attributeID;
                String type;
                String value;

                attributeID = (String)attributeDataRow.getFieldValue(ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_ATTRIBUTE_ID);
                value = (String)attributeDataRow.getFieldValue(ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_VALUE);
                type = (String)attributeDataRow.getFieldValue(ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_TYPE);

                if (type == null)
                {
                    dataRequirement.setAttribute(attributeID, value);
                }
                else
                {
                    switch (type)
                    {
                        case "NUMBER":
                            dataRequirement.setAttribute(attributeID, new BigDecimal(value));
                            break;
                        case "BOOLEAN":
                            dataRequirement.setAttribute(attributeID, Boolean.parseBoolean(value));
                            break;
                        default:
                            dataRequirement.setAttribute(attributeID, value);
                    }
                }
            }

            // Return the result.
            return dataRequirement;
        }
        else
        {
            T8Log.log("Data Requirement not found.");
            return null;
        }
    }

    private ValueRequirement buildPropertyValueRequirement(List<T8DataEntity> valueRequirementData, List<T8DataEntity> valueRequirementAttributeData, PropertyRequirement propertyRequirement)
    {
        ArrayList<T8DataEntity> dataRows;
        ArrayList<ValueRequirement> valueRequirements;
        Map<String, Object> filterValues;

        filterValues = new HashMap<>();
        filterValues.put(ALT_DR_DATA_TYPE_DE_IDENTIFIER +  EF_SECTION_ID, propertyRequirement.getParentSectionRequirement().getConceptID());
        filterValues.put(ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_PROPERTY_ID, propertyRequirement.getConceptID());
        dataRows = T8DataUtilities.getFilteredDataEntities(valueRequirementData, filterValues);

        valueRequirements = buildValueRequirements(dataRows,valueRequirementAttributeData, null, null);
        if (valueRequirements.size() > 0)
        {
            return valueRequirements.get(0);
        }
        else return null;
    }

    private ArrayList<ValueRequirement> buildValueRequirements(List<T8DataEntity> valueRequirementData,List<T8DataEntity> valueRequirementAttributeData, RequirementType parentRequirementType, Integer parentDataTypeSequence)
    {
        ArrayList<ValueRequirement> valueRequirements;
        ArrayList<T8DataEntity> dataRows;
        HashMap<String, Object> filterValues;

        filterValues = new HashMap<>();
        filterValues.put(ALT_DR_DATA_TYPE_DE_IDENTIFIER +  EF_PARENT_DATA_TYPE_ID, parentRequirementType != null ? parentRequirementType.getID() : null);
        filterValues.put(ALT_DR_DATA_TYPE_DE_IDENTIFIER +  EF_PARENT_DATA_TYPE_SEQ, parentDataTypeSequence);
        dataRows = T8DataUtilities.getFilteredDataEntities(valueRequirementData, filterValues);

        valueRequirements = new ArrayList<>();
        for (T8DataEntity dataRow : dataRows)
        {
            ValueRequirement valueRequirement;
            String requirementTypeID;
            RequirementType requirementType;
            int dataTypeSequence;
            String value;
            String propertyId;

            requirementTypeID = (String)dataRow.getFieldValue(dataRow.getIdentifier() + EF_DATA_TYPE_ID);
            dataTypeSequence = (Integer)dataRow.getFieldValue(dataRow.getIdentifier() + EF_DATA_TYPE_SEQUENCE);
            value = (String)dataRow.getFieldValue(dataRow.getIdentifier() + EF_VALUE);
            propertyId = (String)dataRow.getFieldValue(dataRow.getIdentifier() + EF_PROPERTY_ID);

            requirementType = RequirementType.getRequirementType(requirementTypeID);
            valueRequirement = ValueRequirement.createValueRequirement(requirementType);
            valueRequirement.setValue(value);

            // Add the Value Requirement attributes.
            if (parentRequirementType == null)
            {
                for (T8DataEntity valueAttributeDataRow : valueRequirementAttributeData)
                {
                    String attributeID;

                    String type;
                    String atrValue;
                    String atrPropertyId;

                    atrPropertyId = (String)valueAttributeDataRow.getFieldValue(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_PROPERTY_ID);

                    if (atrPropertyId != null && atrPropertyId.equals(propertyId))
                    {
                        attributeID = (String)valueAttributeDataRow.getFieldValue(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_ATTRIBUTE_ID);
                        atrValue = (String)valueAttributeDataRow.getFieldValue(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_VALUE);
                        type = (String)valueAttributeDataRow.getFieldValue(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_TYPE);

                        if (type == null)
                        {
                            valueRequirement.setAttribute(attributeID, atrValue);
                        }
                        else
                        {
                            switch (type)
                            {
                                case "NUMBER":
                                    valueRequirement.setAttribute(attributeID, new BigDecimal(atrValue));
                                    break;
                                case "BOOLEAN":
                                    valueRequirement.setAttribute(attributeID, Boolean.parseBoolean(atrValue));
                                    break;
                                default:
                                    valueRequirement.setAttribute(attributeID, atrValue);
                            }
                        }
                    }
                }
            }

            valueRequirements.add(valueRequirement);
            valueRequirement.addSubRequirements(buildValueRequirements(valueRequirementData,valueRequirementAttributeData, requirementType, dataTypeSequence));
        }

        return valueRequirements;
    }

    public final void insertDataRequirement(String packageIid, int step, String method, DataRequirement dataRequirement) throws Exception
    {
        List<T8DataEntity> newDRData;
        List<T8DataEntity> newDRAttributeData;
        List<T8DataEntity> newPropertyData;
        List<T8DataEntity> newPropertyAttributeData;
        List<T8DataEntity> newValueData;
        List<T8DataEntity> newValueAttributeData;
        List<Map<String, Object>> drRows;
        List<Map<String, Object>> drAtrRows;
        List<Map<String, Object>> drPropRows;
        List<Map<String, Object>> drPropAtrRows;
        List<Map<String, Object>> drDataTypeRows;
        List<Map<String, Object>> drDataTypeAtrRows;

        // Create the rows to insert in the different tables.
        drRows = T8RequirementDataHandler.getDataRequirementDataRows(dataRequirement);
        drRows.get(0).put("PACKAGE_IID", packageIid);
        drRows.get(0).put("STEP", step);
        drRows.get(0).put("METHOD", method);

        // Add the default fields to the attributes.
        drAtrRows = T8RequirementDataHandler.getDataRequirementAttributeDataRows(dataRequirement);
        for (Map<String, Object> drAtrRow : drAtrRows)
        {
            drAtrRow.put("PACKAGE_IID", packageIid);
            drAtrRow.put("STEP", step);
            drAtrRow.put("METHOD", method);
        }

        // Add the default fields to the properties.
        drPropRows = T8RequirementDataHandler.getPropertyRequirementDataRows(dataRequirement);
        for (Map<String, Object> drPropRow : drPropRows)
        {
            drPropRow.put("PACKAGE_IID", packageIid);
            drPropRow.put("STEP", step);
            drPropRow.put("METHOD", method);
        }

        // Add the default fields to the property attributes.
        drPropAtrRows = T8RequirementDataHandler.getPropertyRequirementAttributeDataRows(dataRequirement);
        for (Map<String, Object> drPropAtrRow : drPropAtrRows)
        {
            drPropAtrRow.put("PACKAGE_IID", packageIid);
            drPropAtrRow.put("STEP", step);
            drPropAtrRow.put("METHOD", method);
        }

        // Add the default fields to the property data types.
        drDataTypeRows = T8RequirementDataHandler.getValueRequirementDataRows(dataRequirement);
        for (Map<String, Object> drDataTypeRow : drDataTypeRows)
        {
            drDataTypeRow.put("PACKAGE_IID", packageIid);
            drDataTypeRow.put("STEP", step);
            drDataTypeRow.put("METHOD", method);
        }

        // Add the default fields to the property data type attributes.
        drDataTypeAtrRows = T8RequirementDataHandler.getValueRequirementAttributeDataRows(dataRequirement);
        for (Map<String, Object> drDataTypeAtrRow : drDataTypeAtrRows)
        {
            drDataTypeAtrRow.put("PACKAGE_IID", packageIid);
            drDataTypeAtrRow.put("STEP", step);
            drDataTypeAtrRow.put("METHOD", method);
        }

        // Get all the new data from the data requirement.
        newDRData = createEntities(tx, drRows, ALT_DR_DOC_DE_IDENTIFIER);
        newDRAttributeData = createEntities(tx, drAtrRows, ALT_DR_DOC_ATR_DE_IDENTIFIER);
        newPropertyData = createEntities(tx, drPropRows, ALT_DR_PROPERTY_DE_IDENTIFIER);
        newPropertyAttributeData = createEntities(tx, drPropAtrRows, ALT_DR_PROPERTY_ATR_DE_IDENTIFIER);
        newValueData = createEntities(tx, drDataTypeRows, ALT_DR_DATA_TYPE_DE_IDENTIFIER);
        newValueAttributeData = createEntities(tx, drDataTypeAtrRows, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER);

        // Insert the data requirement data rows.
        tx.insert(newDRData);
        tx.insert(newDRAttributeData);

        // Insert all the new property requirement data rows.
        tx.insert(newPropertyData);
        tx.insert(newPropertyAttributeData);

        // Insert all the new value requirement data rows.
        tx.insert(newValueData);
        tx.insert(newValueAttributeData);
    }

    public final void updateDataRequirement(String packageIid, int step, String method, DataRequirement dr) throws Exception
    {
        List<T8DataEntity> oldDRData;
        List<T8DataEntity> oldDRAttributeData;
        List<T8DataEntity> oldPropertyData;
        List<T8DataEntity> oldPropertyAttributeData;
        List<T8DataEntity> oldDataTypeData;
        List<T8DataEntity> oldDataTypeAttributeData;
        List<T8DataEntity> newDRData;
        List<T8DataEntity> newDRAttributeData;
        List<T8DataEntity> newPropertyData;
        List<T8DataEntity> newPropertyAttributeData;
        List<T8DataEntity> newValueData;
        List<T8DataEntity> newValueAttributeData;
        String drId;
        List<Map<String, Object>> drRows;
        List<Map<String, Object>> drAtrRows;
        List<Map<String, Object>> drPropRows;
        List<Map<String, Object>> drPropAtrRows;
        List<Map<String, Object>> drDataTypeRows;
        List<Map<String, Object>> drDataTypeAtrRows;

        // Create the rows to insert in the different tables.
        drRows = T8RequirementDataHandler.getDataRequirementDataRows(dr);
        drRows.get(0).put("PACKAGE_IID", packageIid);
        drRows.get(0).put("STEP", step);
        drRows.get(0).put("METHOD", method);

        // Add the default fields to the attributes.
        drAtrRows = T8RequirementDataHandler.getDataRequirementAttributeDataRows(dr);
        for (Map<String, Object> drAtrRow : drAtrRows)
        {
            // WARNING: TO BE REMOVED
            drAtrRow.remove("TYPE");

            drAtrRow.put("PACKAGE_IID", packageIid);
            drAtrRow.put("STEP", step);
            drAtrRow.put("METHOD", method);
        }

        // Add the default fields to the properties.
        drPropRows = T8RequirementDataHandler.getPropertyRequirementDataRows(dr);
        for (Map<String, Object> drPropRow : drPropRows)
        {
            drPropRow.put("PACKAGE_IID", packageIid);
            drPropRow.put("STEP", step);
            drPropRow.put("METHOD", method);
        }

        // Add the default fields to the property attributes.
        drPropAtrRows = T8RequirementDataHandler.getPropertyRequirementAttributeDataRows(dr);
        for (Map<String, Object> drPropAtrRow : drPropAtrRows)
        {
            // WARNING: TO BE REMOVED
            drPropAtrRow.remove("TYPE");

            drPropAtrRow.put("PACKAGE_IID", packageIid);
            drPropAtrRow.put("STEP", step);
            drPropAtrRow.put("METHOD", method);
        }

        // Add the default fields to the property data types.
        drDataTypeRows = T8RequirementDataHandler.getValueRequirementDataRows(dr);
        for (Map<String, Object> drDataTypeRow : drDataTypeRows)
        {
            drDataTypeRow.put("PACKAGE_IID", packageIid);
            drDataTypeRow.put("STEP", step);
            drDataTypeRow.put("METHOD", method);
        }

        // Add the default fields to the property data type attributes.
        drDataTypeAtrRows = T8RequirementDataHandler.getValueRequirementAttributeDataRows(dr);
        for (Map<String, Object> drDataTypeAtrRow : drDataTypeAtrRows)
        {
            // WARNING: TO BE REMOVED
            drDataTypeAtrRow.remove("TYPE");

            drDataTypeAtrRow.put("PACKAGE_IID", packageIid);
            drDataTypeAtrRow.put("STEP", step);
            drDataTypeAtrRow.put("METHOD", method);
        }

        // Get all existing data from the database.
        drId = dr.getConceptID();
        oldDRData = tx.select(ALT_DR_DOC_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DOC_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DOC_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DOC_DE_IDENTIFIER + EF_STEP, step)));
        oldDRAttributeData = tx.select(ALT_DR_DOC_ATR_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DOC_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_STEP, step)));
        oldPropertyData = tx.select(ALT_DR_PROPERTY_DE_IDENTIFIER, new T8DataFilter(ALT_DR_PROPERTY_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_PROPERTY_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_PROPERTY_DE_IDENTIFIER + EF_STEP, step)));
        oldPropertyAttributeData = tx.select(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER, new T8DataFilter(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_STEP, step)));
        oldDataTypeData = tx.select(ALT_DR_DATA_TYPE_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DATA_TYPE_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_STEP, step)));
        oldDataTypeAttributeData = tx.select(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_STEP, step)));

        // Get all the new data from the data requirement.
        newDRData = createEntities(tx, drRows, ALT_DR_DOC_DE_IDENTIFIER);
        newDRAttributeData = createEntities(tx, drAtrRows, ALT_DR_DOC_ATR_DE_IDENTIFIER);
        newPropertyData = createEntities(tx, drPropRows, ALT_DR_PROPERTY_DE_IDENTIFIER);
        newPropertyAttributeData = createEntities(tx, drPropAtrRows, ALT_DR_PROPERTY_ATR_DE_IDENTIFIER);
        newValueData = createEntities(tx, drDataTypeRows, ALT_DR_DATA_TYPE_DE_IDENTIFIER);
        newValueAttributeData = createEntities(tx, drDataTypeAtrRows, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER);

        // Update the data requirement data row.
        if (oldDRData.size() > 0)
        {
            tx.update(newDRData);
        }
        else // No data requirement to update, so insert the data.
        {
            tx.insert(newDRData);
        }

        // Remove all deleted value requirement attribute rows.
        {
            ArrayList<T8DataEntity> removedEntities;

            removedEntities = T8DataUtilities.getAddedEntities(newValueAttributeData, oldDataTypeAttributeData, new String[]{ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_PROPERTY_ID, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_DATA_TYPE_ID, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_DATA_TYPE_SEQUENCE, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_ATTRIBUTE_ID});
            tx.delete(removedEntities);
        }

        // Remove all deleted value requirement rows.
        {
            ArrayList<T8DataEntity> removedEntities;
            Map<String, OrderMethod> fieldOrdering;

            fieldOrdering = new HashMap<String, OrderMethod>();
            fieldOrdering.put(ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_DATA_TYPE_SEQUENCE,OrderMethod.DESCENDING);

            removedEntities = T8DataUtilities.getAddedEntities(newValueData, oldDataTypeData, new String[]{ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_SECTION_ID, ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_PROPERTY_ID, ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_DATA_TYPE_ID, ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_DATA_TYPE_SEQUENCE});
            T8DataUtilities.sort(removedEntities, fieldOrdering);
            tx.delete(removedEntities);
        }

        // Remove all deleted property requirement attribute rows.
        {
            ArrayList<T8DataEntity> removedEntities;

            removedEntities = T8DataUtilities.getAddedEntities(newPropertyAttributeData, oldPropertyAttributeData, new String[]{ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_PROPERTY_ID, ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_ATTRIBUTE_ID});
            tx.delete(removedEntities);
        }

        // Remove all deleted property requirement rows.
        {
            ArrayList<T8DataEntity> removedEntities;

            removedEntities = T8DataUtilities.getAddedEntities(newPropertyData, oldPropertyData, new String[]{ALT_DR_PROPERTY_DE_IDENTIFIER + EF_SECTION_ID, ALT_DR_PROPERTY_DE_IDENTIFIER + EF_PROPERTY_ID});
            tx.delete(removedEntities);
        }

        // Remove all deleted data requirement attribute rows.
        {
            ArrayList<T8DataEntity> removedEntities;

            removedEntities = T8DataUtilities.getAddedEntities(newDRAttributeData, oldDRAttributeData, new String[]{ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_ATTRIBUTE_ID});
            tx.delete(removedEntities);
        }

        // Insert all new and existing data requirement attribute rows.
        {
            ArrayList<T8DataEntity> newEntities;
            ArrayList<T8DataEntity> updatedEntities;

            newEntities = T8DataUtilities.getAddedEntities(oldDRAttributeData, newDRAttributeData, new String[]{ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_ATTRIBUTE_ID});
            tx.insert(newEntities);

            updatedEntities = new ArrayList<>(newDRAttributeData);
            updatedEntities.removeAll(newEntities);
            tx.update(updatedEntities);
        }

        // Insert all new and existing property requirement rows.
        {
            ArrayList<T8DataEntity> newEntities;
            ArrayList<T8DataEntity> updatedEntities;

            newEntities = T8DataUtilities.getAddedEntities(oldPropertyData, newPropertyData, new String[]{ALT_DR_PROPERTY_DE_IDENTIFIER + EF_SECTION_ID, ALT_DR_PROPERTY_DE_IDENTIFIER + EF_PROPERTY_ID});
            tx.insert(newEntities);

            updatedEntities = new ArrayList<>(newPropertyData);
            updatedEntities.removeAll(newEntities);
            tx.update(updatedEntities);
        }

        // Insert all new and existing property requirement attribute rows.
        {
            ArrayList<T8DataEntity> newEntities;
            ArrayList<T8DataEntity> updatedEntities;

            newEntities = T8DataUtilities.getAddedEntities(oldPropertyAttributeData, newPropertyAttributeData, new String[]{ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_PROPERTY_ID, ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_ATTRIBUTE_ID});
            tx.insert(newEntities);

            updatedEntities = new ArrayList<>(newPropertyAttributeData);
            updatedEntities.removeAll(newEntities);
            tx.update(updatedEntities);
        }

        // Insert all new value requirement rows.
        {
            ArrayList<T8DataEntity> newEntities;
            ArrayList<T8DataEntity> updatedEntities;

            newEntities = T8DataUtilities.getAddedEntities(oldDataTypeData, newValueData, new String[]{ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_SECTION_ID, ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_PROPERTY_ID, ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_DATA_TYPE_ID, ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_DATA_TYPE_SEQUENCE});
            tx.insert(newEntities);

            updatedEntities = new ArrayList<>(newValueData);
            updatedEntities.removeAll(newEntities);
            tx.update(updatedEntities);
        }

        // Insert all new value requirement attribute rows.
        {
            ArrayList<T8DataEntity> newEntities;
            ArrayList<T8DataEntity> updatedEntities;

            newEntities = T8DataUtilities.getAddedEntities(oldDataTypeAttributeData, newValueAttributeData, new String[]{ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_PROPERTY_ID, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_DATA_TYPE_ID, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_DATA_TYPE_SEQUENCE, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_ATTRIBUTE_ID});
            tx.insert(newEntities);

            updatedEntities = new ArrayList<>(newValueAttributeData);
            updatedEntities.removeAll(newEntities);
            tx.update(updatedEntities);
        }
    }

    public final void deleteDataRequirement(String packageIid, int step) throws Exception
    {
        // Delete without the step in the filter when step is smaller than 0.
        if (step < 0)
        {
            tx.delete(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid)));
            tx.delete(ALT_DR_DATA_TYPE_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DATA_TYPE_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid)));

            // Delete all property requirements.
            tx.delete(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER, new T8DataFilter(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid)));
            tx.delete(ALT_DR_PROPERTY_DE_IDENTIFIER, new T8DataFilter(ALT_DR_PROPERTY_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_PROPERTY_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid)));

            // Delete the data requirement instance entries.
            tx.delete(ALT_DR_INSTANCE_DE_IDENTIFIER, new T8DataFilter(ALT_DR_INSTANCE_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_INSTANCE_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid)));

            // Delete the data requirement entry.
            tx.delete(ALT_DR_DOC_ATR_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DOC_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid)));
            tx.delete(ALT_DR_DOC_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DOC_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DOC_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid)));
        }
        else
        {
            // Delete all value requirements in the correct sequence (hierarchical delete).
            tx.delete(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DATA_TYPE_ATR_DE_IDENTIFIER + EF_STEP, step)));
            tx.delete(ALT_DR_DATA_TYPE_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DATA_TYPE_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DATA_TYPE_DE_IDENTIFIER + EF_STEP, step)));

            // Delete all property requirements.
            tx.delete(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER, new T8DataFilter(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_PROPERTY_ATR_DE_IDENTIFIER + EF_STEP, step)));
            tx.delete(ALT_DR_PROPERTY_DE_IDENTIFIER, new T8DataFilter(ALT_DR_PROPERTY_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_PROPERTY_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_PROPERTY_DE_IDENTIFIER + EF_STEP, step)));

            // Delete the data requirement instance entries.
            tx.delete(ALT_DR_INSTANCE_DE_IDENTIFIER, new T8DataFilter(ALT_DR_INSTANCE_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_INSTANCE_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_INSTANCE_DE_IDENTIFIER + EF_STEP, step)));

            // Delete the data requirement entry.
            tx.delete(ALT_DR_DOC_ATR_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DOC_ATR_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DOC_ATR_DE_IDENTIFIER + EF_STEP, step)));
            tx.delete(ALT_DR_DOC_DE_IDENTIFIER, new T8DataFilter(ALT_DR_DOC_DE_IDENTIFIER, HashMaps.newHashMap(ALT_DR_DOC_DE_IDENTIFIER + EF_PACKAGE_IID, packageIid, ALT_DR_DOC_DE_IDENTIFIER + EF_STEP, step)));
        }
    }

    private List<T8DataEntity> createEntities(T8DataTransaction tx, List<Map<String, Object>> rows, String entityId) throws Exception
    {
        List<T8DataEntity> entityList;
        Object keyValue;

        entityList = new ArrayList<T8DataEntity>();
        for (Map<String, Object> row : rows)
        {
            T8DataEntity entity;

            entity = tx.create(entityId, null);
            for (String key : row.keySet())
            {
                // WARNING: TO BE REMOVED.
                if (!key.equals("ATTRIBUTES") && !key.equals("PRESCRIBED_INDICATOR"))
                {
                    keyValue =  row.get(key);

                    if (key.equals("REQUIRED"))
                    {
                        key = "CHARACTERISTIC";
                    }
                    entity.setFieldValue(entityId + "$" + key, keyValue);
                }
            }

            entityList.add(entity);
        }

        return entityList;
    }
}
