package com.pilog.t8.org;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionResolver;
import com.pilog.t8.definition.resolver.T8JavaDefinitionResolverDefinition;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationSetupDefinitionResolver implements T8DefinitionResolver
{
    private T8ServerContext serverContext;
    private T8DefinitionManager definitionManager;
    private T8JavaDefinitionResolverDefinition definition;

    public T8OrganizationSetupDefinitionResolver(T8Context context, T8JavaDefinitionResolverDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.definitionManager = serverContext.getDefinitionManager();
        this.definition = definition;
    }

    @Override
    public void init() throws Exception
    {
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public void reset()
    {
    }

    @Override
    public boolean resolvesType(String typeIdentifier)
    {
        return T8OrganizationSetupDefinition.TYPE_IDENTIFIER.equals(typeIdentifier);
    }

    @Override
    public T8Definition getResolvedDefinition(T8Context context, String typeId, Map<String, Object> inputParameters) throws Exception
    {
        List<T8Definition> setupDefinitions;

        setupDefinitions = definitionManager.getRawGroupDefinitions(context, null, T8OrganizationSetupDefinition.GROUP_IDENTIFIER);
        if (setupDefinitions.size() > 0)
        {
            // TODO:  This resolver needs to actually check all of the
            // definitions in this list and then create a cached mapping of
            // organization ID -> organization setup identifier so that
            // subsequent requests can be handled without retrieving the entire
            // group of definitions again.
            return setupDefinitions.get(0);
        }
        else return null;
    }
}
