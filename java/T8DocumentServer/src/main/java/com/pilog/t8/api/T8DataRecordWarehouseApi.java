package com.pilog.t8.api;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.sql.T8DataRecordEntityGenerator;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceResourceDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordWarehouseApi implements T8Api, T8PerformanceStatisticsProvider
{
    public static final String API_IDENTIFIER = "@API_DATA_RECORD_WAREHOUSE";

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8SessionContext sessionContext;
    private final T8ServerContext serverContext;
    private final T8DataRecordWarehouseApiDataHandler dataHandler;
    private TerminologyProvider terminologyProvider;
    private T8PerformanceStatistics stats;

    public T8DataRecordWarehouseApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.dataHandler = new T8DataRecordWarehouseApiDataHandler(serverContext, sessionContext);
        this.stats = tx.getPerformanceStatistics();
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8DataRecordEntityGenerator getEntityGenerator()
    {
        return new T8DataRecordEntityGenerator(serverContext, tx);
    }

    private void checkTableStructure(String connectionId, String tableName, DataRecord record) throws Exception
    {
        T8TableDataSourceResourceDefinition sourceDefinition;
        T8DataSourceDefinition existingSourceDefinition;
        T8DataEntityDefinition entityDefinition;
        String dataSourceId;
        String entityId;

        // Set the id's of the data source and entity.
        dataSourceId = getDataSourceId(tableName);
        entityId = getEntityId(tableName);

        // Create the temporary Data Source definition.
        sourceDefinition = new T8TableDataSourceResourceDefinition(dataSourceId);
        sourceDefinition.setConnectionIdentifier(connectionId);
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$RECORD_ID", "RECORD_ID", true, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DR_INSTANCE_ID", "DR_INSTANCE_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$DR_ID", "DR_ID", false, true, T8DataType.GUID));
        for (PropertyRequirement propertyRequirement : record.getDataRequirement().getPropertyRequirements())
        {
            ValueRequirement valueRequirement;

            valueRequirement = propertyRequirement.getValueRequirement();
            if (valueRequirement.getRequirementType() != RequirementType.DOCUMENT_REFERENCE)
            {
                String propertyTerm;

                propertyTerm = terminologyProvider.getTerm(null, propertyRequirement.getConceptID());
                sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition("$" + propertyTerm, propertyTerm, false, false, T8DataType.STRING));
            }
        }

        // Create the temporary Data Entity definition.
        entityDefinition = new T8DataEntityResourceDefinition(entityId);
        entityDefinition.setDataSourceIdentifier(dataSourceId);
        for (T8DataSourceFieldDefinition fieldDefinition : sourceDefinition.getFieldDefinitions())
        {
            String fieldId;

            fieldId = fieldDefinition.getIdentifier();
            entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(fieldId, dataSourceId + fieldId, fieldDefinition.isKey(), fieldDefinition.isRequired(), fieldDefinition.getDataType()));
        }

        // Compare the data source created, to the one already existing and only update the table structure if a descrepency is found.
        existingSourceDefinition = tx.getDataSourceDefinition(dataSourceId);
        if ((existingSourceDefinition == null) || (existingSourceDefinition.getFieldDefinitions().size() != sourceDefinition.getFieldDefinitions().size()))
        {
            T8DefinitionManager definitionManager;

            // Initialize the temporary definitions.
            definitionManager = serverContext.getDefinitionManager();
            definitionManager.initializeDefinition(context, sourceDefinition, null);
            definitionManager.initializeDefinition(context, entityDefinition, null);

            // Set the temporary data source and entity on this transaction.
            tx.setDataSourceDefinition(sourceDefinition);
            tx.setDataEntityDefinition(entityDefinition);


        }
    }

    private String getEntityId(String tableName)
    {
        return "@E_TMP_" + tableName;
    }

    private String getDataSourceId(String tableName)
    {
        return "@DS_TMP_" + tableName;
    }

    private String getTableName(PropertyRequirement propertyRequirement)
    {
        String propertyTerm;

        propertyTerm = terminologyProvider.getTerm(null, propertyRequirement.getId());
        if (propertyTerm != null)
        {
            return "WH_" + propertyTerm;
        }
        else throw new RuntimeException("Cannot create table name for property: " + propertyRequirement + "  as no term was found.");
    }

    public void exportFile(String connectionId, String tableName, String fileId) throws Exception
    {
        T8TerminologyApi trmApi;
        T8DataRecordApi recApi;
        DataRecord file;

        recApi = tx.getApi((T8DataRecordApi.API_IDENTIFIER));
        trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
        terminologyProvider = trmApi.getTerminologyProvider();
        file = recApi.retrieveFile(fileId, null, false, false, false, false, false, false);
        exportRecord(connectionId, tableName, file);
    }

    private void exportRecord(String connectionId, String tableName, DataRecord record) throws Exception
    {
        T8DataRecordValueStringGenerator valueStringGenerator;
        Map<String, List<DataRecord>> subRecordsToExport;
        T8DataEntity entity;
        String entityId;

        // Make sure the table structure is in the correct state to accommodate the record data.
        entityId = getEntityId(tableName);
        checkTableStructure(connectionId, tableName, record);

        // Create a value string generator to use when values need to be formatted into a single string representation.
        valueStringGenerator = new T8DataRecordValueStringGenerator();
        valueStringGenerator.setTerminologyProvider(terminologyProvider);
        subRecordsToExport = new HashMap<>();

        // Create the entity to save.
        entity = tx.create(entityId, null);
        entity.setFieldValue(entityId + "$RECORD_ID", record.getID());
        entity.setFieldValue(entityId + "$DR_INSTANCE_ID", record.getID());
        entity.setFieldValue(entityId + "$DR_ID", record.getID());
        for (PropertyRequirement propertyRequirement : record.getDataRequirement().getPropertyRequirements())
        {
            RecordProperty property;
            String propertyId;
            String propertyTerm;

            // Get the property corresponding to the requirement of the current iteration.
            propertyId = propertyRequirement.getConceptID();
            propertyTerm = terminologyProvider.getTerm(null, propertyId);
            property = record.getRecordProperty(propertyId);
            if (property != null)
            {
                RecordValue propertyValue;

                // Get the property value and process it if not null.
                propertyValue = property.getRecordValue();
                if (propertyValue != null)
                {
                    // If the property value is a document reference list, we need to export all referenced records to a separate table.
                    if (propertyValue instanceof DocumentReferenceList)
                    {
                        DocumentReferenceList referenceList;
                        List<DataRecord> referencedRecords;
                        String referenceTableName;

                        // Create a list of all sub-records referenced from this property.
                        referenceList = (DocumentReferenceList)propertyValue;
                        referencedRecords = new ArrayList<>();
                        for (String reference : referenceList.getReferenceIds())
                        {
                            DataRecord referencedRecord;

                            referencedRecord = record.findDataRecord(reference);
                            if (referencedRecord != null) referencedRecords.add(referencedRecord);
                        }

                        // Add the referenced record list to the map of sub-records to be exported.
                        referenceTableName = getTableName(propertyRequirement);
                        subRecordsToExport.put(referenceTableName, referencedRecords);
                    }
                    else
                    {
                        // Set the value to the generated string representation.
                        entity.setFieldValue(entityId + propertyTerm, valueStringGenerator.generateValueString(propertyValue));
                    }
                }
                else
                {
                    // No property value, so set value to null.
                    entity.setFieldValue(entityId + propertyTerm, null);
                }
            }
            else
            {
                // No record property, so set value to null.
                entity.setFieldValue(entityId + propertyTerm, null);
            }
        }

        // Write the constructed entity to the database.
        if (existsInRegistry(record))
        {
            tx.update(entity);
        }
        else
        {
            tx.insert(entity);
            addToRegistry(record);
        }

        // Export all sub-records.
        for (String subRecordTableName : subRecordsToExport.keySet())
        {
            List<DataRecord> subRecords;

            subRecords = subRecordsToExport.get(tableName);
            for (DataRecord subRecord : subRecords)
            {
                exportRecord(connectionId, subRecordTableName, subRecord);
            }
        }
    }

    private void addToRegistry(DataRecord record)
    {
        String fileId;

        fileId = record.getDataFileID();

    }

    private boolean existsInRegistry(DataRecord record)
    {
        return false;
    }
}
