package com.pilog.t8.data.org;

import com.pilog.t8.api.T8DataRequirementApi;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationDataRequirementInstanceProvider implements DataRequirementInstanceProvider
{
    private final T8DataRequirementApi drApi;

    public T8OrganizationDataRequirementInstanceProvider(T8DataRequirementApi drApi)
    {
        this.drApi = drApi;
    }

    @Override
    public DataRequirementInstance getDataRequirementInstance(String drInstanceId, String languageId, boolean includeOntology, boolean includeTerminology)
    {
        try
        {
            return drApi.retrieveDataRequirementInstance(drInstanceId, languageId, includeOntology, includeTerminology);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving Data Requirement Instance: " + drInstanceId, e);
        }
    }
}
