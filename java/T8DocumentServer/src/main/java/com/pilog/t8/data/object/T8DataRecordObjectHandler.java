package com.pilog.t8.data.object;

import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.api.T8DataObjectApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.filter.T8DataFilterExpressionParser;
import com.pilog.t8.data.object.filter.T8ObjectFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.object.T8DataRecordObjectDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.definition.data.object.T8DataObjectConstructorScriptDefinition;
import com.pilog.t8.definition.data.object.T8DataRecordObjectDefinition.RefreshTrigger;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.definition.data.document.datarecord.view.T8DataRecordObjectResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordObjectHandler implements T8DataObjectHandler
{
    private final T8DataRecordObjectDefinition definition;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8DataTransaction tx;
    private final List<String> languageIds;
    private final String objectId;
    private final String orgId;
    private final Map<String, String> entityFieldMapping;
    private final String entityId;
    private final RefreshTrigger refreshTrigger;
    private final List<String> dataFileDrIds;
    private final List<String> dataFileDrIids;
    private final List<String> drIds;
    private final List<String> drIids;
    private final T8ServerContextScript script;

    public T8DataRecordObjectHandler(T8DataRecordObjectDefinition definition, T8DataTransaction tx) throws Exception
    {
        this.definition = definition;
        this.tx = tx;
        this.context = tx.getContext();
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.languageIds = definition.getLanguageIds();
        this.orgId = sessionContext.getRootOrganizationIdentifier();
        this.objectId = definition.getIdentifier();
        this.refreshTrigger = definition.getViewRefreshTrigger();
        this.dataFileDrIds = definition.getDataFileDrIds();
        this.dataFileDrIids = definition.getDataFileDrIids();
        this.drIds = definition.getDrIds();
        this.drIids = definition.getDrIids();
        this.entityFieldMapping = definition.getEntityFieldMapping();
        this.entityId = definition.getDataEntityId();
        this.script = definition.getConstructorScript() != null ? definition.getConstructorScript().getNewScriptInstance(context) : null;

        // Do some initialization checks to ensure settings are valid.
        if (script == null) throw new Exception("No constructor script defined for data record object: " + objectId);
        if ((languageIds == null) || (languageIds.isEmpty())) throw new Exception("No language ids specified for data record obejct: " + objectId);
    }

    @Override
    public String getDataObjectId()
    {
        return definition.getIdentifier();
    }

    @Override
    public T8DataObject retrieve(String objectIid) throws Exception
    {
        T8DataEntity entity;

        // Retrieve the entity from the view using the specified record id and the current transaction language.
        entity = tx.retrieve(entityId, HashMaps.newHashMap(entityId + F_RECORD_ID, objectIid, entityId + F_LANGUAGE_ID, sessionContext.getContentLanguageIdentifier()));
        if (entity != null)
        {
            // Create an object from the entity and return the result.
            return mapObject(entity);
        }
        else return null;
    }

    @Override
    public List<T8DataObject> search(T8ObjectFilter objectFilter, String searchString, int pageOffset, int pageSize) throws Exception
    {
        T8DataFilterExpressionParser parser;
        List<T8DataEntity> entities;
        List<T8DataObject> objects;
        T8DataFilter filter;

        // Parse the search expression and create a filter from it.
        parser = new T8DataFilterExpressionParser(entityId, ArrayLists.newArrayList(entityId + F_SEARCH_TERMS));
        parser.setWhitespaceConjunction(T8DataFilterClause.DataFilterConjunction.AND);
        filter = parser.parseExpression(searchString, false);
        filter.addFilterCriterion(entityId + F_OBJECT_ID, DataFilterOperator.EQUAL, objectId);
        filter.addFilterCriterion(entityId + F_LANGUAGE_ID, DataFilterOperator.EQUAL, sessionContext.getContentLanguageIdentifier());

        // Select entities from the view using the search filter.
        objects = new ArrayList<>();
        entities = tx.select(entityId, filter, pageOffset, pageSize);
        for (T8DataEntity entity : entities)
        {
            // Map each entity to a new data object and add it to the result.
            objects.add(mapObject(entity));
        }

        // Return the final list of objects, found using the search filter.
        return objects;
    }

    private T8DataObject mapObject(T8DataEntity entity)
    {
        T8DataObject object;

        object = new T8DataObject(definition, null);
        object.setFieldValues(T8IdentifierUtilities.mapParameters(entity.getFieldValues(), T8IdentifierUtilities.createReverseIdentifierMap(entityFieldMapping)));
        return object;
    }

    private T8DataObject create(DataRecord sourceRecord, String languageId)
    {
        Map<String, Object> inputParameters;
        DataRecord dataRecord;

        // Get the source record.
        dataRecord = (DataRecord)sourceRecord;

        // Create the input parameter collection.
        inputParameters = new HashMap<String, Object>();
        inputParameters.put(T8DataObjectConstructorScriptDefinition.PARAMETER_SOURCE, dataRecord);
        inputParameters.put(T8DataObjectConstructorScriptDefinition.PARAMETER_ORG_ID, orgId);
        inputParameters.put(T8DataObjectConstructorScriptDefinition.PARAMETER_LANGUAGE_ID, languageId);

        // Create the result map.
        try
        {
            Map<String, Object> outputParameters;
            T8DataObject newObject;

            // Execute the script.
            outputParameters = script.executeScript(inputParameters);
            outputParameters = T8IdentifierUtilities.stripNamespace(outputParameters);
            outputParameters.put(F_RECORD_ID, dataRecord.getID());
            outputParameters.put(F_ROOT_RECORD_ID, dataRecord.getDataFileID());
            outputParameters.put(F_DR_ID, dataRecord.getDataRequirementID());
            outputParameters.put(F_DR_IID, dataRecord.getDataRequirementInstanceID());
            outputParameters.put(F_LANGUAGE_ID, languageId);
            outputParameters.put(F_ROOT_ORG_ID, orgId);

            // Create the new object from the output field values.
            newObject = new T8DataObject(definition, null);
            newObject.setFieldValues(outputParameters);
            return newObject;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing new data object: " + objectId, e);
        }
    }

    public void insert(T8DataObject dataObject) throws Exception
    {
        Map<String, Object> fieldValues;
        T8DataEntity objectEntity;

        // Create the object entity.
        fieldValues = T8IdentifierUtilities.stripNamespace(dataObject.getFieldValues());
        fieldValues.put("$ID", dataObject.getId());
        objectEntity = tx.create(entityId, T8IdentifierUtilities.mapParameters(fieldValues, entityFieldMapping));
        objectEntity.setFieldValue(entityId + F_ROW_ID, T8IdentifierUtilities.createNewGUID());

        // Insert the entity if it exists, else insert.
        tx.insert(objectEntity);
    }

    public void save(T8DataObject dataObject) throws Exception
    {
        Map<String, Object> fieldValues;
        T8DataEntity objectEntity;

        // Create the object entity.
        fieldValues = T8IdentifierUtilities.stripNamespace(dataObject.getFieldValues());
        fieldValues.put("$ID", dataObject.getId());
        objectEntity = tx.create(entityId, T8IdentifierUtilities.mapParameters(fieldValues, entityFieldMapping));
        objectEntity.setFieldValue(entityId + F_ROW_ID, T8IdentifierUtilities.createNewGUID());

        // Update the entity if it exists, else insert.
        if (!tx.update(objectEntity))
        {
            tx.insert(objectEntity);
        }
    }

    public void recordUpdated(DataRecord dataRecord, T8DataFileAlteration alteration) throws Exception
    {
        // Make sure the data record is applicable to this object and that changes have been made that actually affect the object.
        if (checkRefresh(dataRecord, alteration))
        {
            String recordId;

            // For each language, we have to extract data from the file and update the persisted view of the data object.
            for (String languageId : languageIds)
            {
                T8DataObject object;

                // Create a new object from the source record.
                object = create(dataRecord, languageId);

                // Update the persisted object.
                try
                {
                    save(object);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while updating data object: " + object, e);
                }
            }

            // If the record is an insert, we need to set the initial state of new record and also attach it to the creator functionality state.
            recordId = dataRecord.getId();
            if (alteration.isInsert(recordId))
            {
                String functionalityIid;

                // If this object is not stateless, save the initial state of the object.
                if (definition.isStateless())
                {
                    T8DataObjectApi objApi;
                    
                    objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
                    objApi.saveInitialState(objectId, recordId);
                }

                // Access the data object.
                functionalityIid = context.getFunctionalityIid();
                if (functionalityIid != null)
                {
                    T8FunctionalityManager functionalityManager;

                    // Attach the new object to its creator functionality's state.
                    functionalityManager = serverContext.getFunctionalityManager();
                    functionalityManager.accessDataObject(tx, functionalityIid, objectId, recordId);
                }
            }
        }
    }

    private boolean checkRefresh(DataRecord dataRecord, T8DataFileAlteration alteration)
    {
        Set<String> changedRecordIdSet;
        DataRecord dataFile;

        // Firstly; make sure that the input record is included in one of the filter lists.
        if (!drIds.contains(dataRecord.getDataRequirementID()))
        {
            if (!drIids.contains(dataRecord.getDataRequirementInstanceID()))
            {
                return false;
            }
        }

        // Secondly; if any DataFile filters are specified, make sure that the input record's data file is included in one of the filter lists.
        dataFile = dataRecord.getDataFile();
        if (!dataFileDrIds.isEmpty() && !dataFileDrIds.contains(dataFile.getDataRequirementID()))
        {
            return false;
        }
        else if (!dataFileDrIids.isEmpty() && !dataFileDrIids.contains(dataFile.getDataRequirementInstanceID()))
        {
            return false;
        }

        // Now check the refresh trigger against the altered record list to find out if we need to refresh this view.
        changedRecordIdSet = alteration.getOntologyUpdatedRecordIdSet();
        changedRecordIdSet.addAll(alteration.getInsertedAndUpdatedRecordIdList());
        switch (refreshTrigger)
        {
            case FILE:
                // Return true if any part of the file has been changed.
                return !changedRecordIdSet.isEmpty();
            case CONTENT:
                // Return true if this record has been altered.
                return changedRecordIdSet.contains(dataRecord.getID());
            case CONTENT_OR_DESCENDANT:
                // Return true if this record or any of its descendants have been altered.
                return changedRecordIdSet.contains(dataRecord.getID()) || CollectionUtilities.containsAny(changedRecordIdSet, dataRecord.getDescendantRecordIDList());
            case LINEAGE:
                // Return true if this record or any of its descendants have been altered.
                return CollectionUtilities.containsAny(changedRecordIdSet, dataRecord.getDescendantRecordIDList()) || CollectionUtilities.containsAny(changedRecordIdSet, dataRecord.getPathRecordIDList());
            default:
                throw new RuntimeException("Invalid refresh trigger: " + refreshTrigger);
        }
    }
}
