package com.pilog.t8.api;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.datarecord.RecordAttribute;
import com.pilog.t8.data.document.history.HistoryEntry;
import com.pilog.t8.data.source.datarecord.T8DataRecordPersistenceHandler.AuditingType;
import com.pilog.t8.data.source.datarecord.T8RecordDataHandler;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRecordHistoryApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordHistoryApiDataHandler
{
    private T8DataTransaction tx;

    public T8DataRecordHistoryApiDataHandler(T8DataTransaction tx)
    {
        this.tx = tx;
    }

    public void insertHistoryDataRecordEntry(HistoryEntry entry, List<Map<String, Object>> dataRows, List<Map<String, Object>> oldRows) throws Exception
    {
        for (Map<String, Object> dataRow : dataRows)
        {
            Map<String, Object> oldRow;
            T8DataEntity entity;
            String entityId;

            entityId = HISTORY_DATA_RECORD_DOC_DEID;
            oldRow = oldRows != null ? T8RecordDataHandler.getOldRecordRow(oldRows, dataRow) : null;
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + EF_TX_IID, entry.getTransactionIid());
            entity.setFieldValue(entityId + EF_TIME, entry.getTimestamp());
            entity.setFieldValue(entityId + EF_EVENT, entry.getAuditingType().toString());
            entity.setFieldValue(entityId + EF_RECORD_ID, dataRow.get("RECORD_ID"));
            entity.setFieldValue(entityId + EF_ROOT_RECORD_ID, entry.getRootRecordId());
            entity.setFieldValue(entityId + EF_PARENT_RECORD_ID, dataRow.get("PARENT_RECORD_ID"));
            entity.setFieldValue(entityId + EF_DR_ID, dataRow.get("DR_ID"));
            entity.setFieldValue(entityId + EF_DR_INSTANCE_ID, dataRow.get("DR_INSTANCE_ID"));
            entity.setFieldValue(entityId + EF_FFT, dataRow.get("FFT"));
            entity.setFieldValue(entityId + EF_OLD_FFT, oldRow != null ? oldRow.get("FFT") : null);
            entity.setFieldValue(entityId + EF_AGENT_ID, entry.getAgentId());
            entity.setFieldValue(entityId + EF_AGENT_IID, entry.getAgentIid());
            entity.setFieldValue(entityId + EF_USER_ID, entry.getUserId());
            entity.setFieldValue(entityId + EF_SESSION_ID, entry.getSessionId());
            entity.setFieldValue(entityId + EF_OPERATION_ID, entry.getOperationId());
            entity.setFieldValue(entityId + EF_OPERATION_IID, entry.getOperationIid());
            entity.setFieldValue(entityId + EF_FLOW_ID, entry.getFlowId());
            entity.setFieldValue(entityId + EF_FLOW_IID, entry.getFlowIid());
            entity.setFieldValue(entityId + EF_TASK_ID, entry.getTaskId());
            entity.setFieldValue(entityId + EF_TASK_IID, entry.getTaskIid());
            entity.setFieldValue(entityId + EF_FUNCTIONALITY_ID, entry.getFunctionalityId());
            entity.setFieldValue(entityId + EF_FUNCTIONALITY_IID, entry.getFunctionalityIid());
            entity.setFieldValue(entityId + EF_PROCESS_ID, entry.getProcessId());
            entity.setFieldValue(entityId + EF_PROCESS_IID, entry.getProcessIid());
            tx.insert(entity);
        }
    }

    public void insertHistoryDataRecordPropertyEntry(String txIid, AuditingType type, T8Timestamp timestamp, List<Map<String, Object>> dataRows, List<Map<String, Object>> oldRows) throws Exception
    {
        for (Map<String, Object> dataRow : dataRows)
        {
            Map<String, Object> oldRow;
            T8DataEntity entity;
            String entityId;

            entityId = HISTORY_DATA_RECORD_PROPERTY_DEID;
            oldRow = oldRows != null ? T8RecordDataHandler.getOldPropertyRow(oldRows, dataRow) : null;
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + EF_TX_IID, txIid);
            entity.setFieldValue(entityId + EF_TIME, timestamp);
            entity.setFieldValue(entityId + EF_EVENT, type.toString());
            entity.setFieldValue(entityId + EF_RECORD_ID, dataRow.get("RECORD_ID"));
            entity.setFieldValue(entityId + EF_ROOT_RECORD_ID, dataRow.get("ROOT_RECORD_ID"));
            entity.setFieldValue(entityId + EF_PROPERTY_ID, dataRow.get("PROPERTY_ID"));
            entity.setFieldValue(entityId + EF_DR_ID, dataRow.get("DR_ID"));
            entity.setFieldValue(entityId + EF_SECTION_ID, dataRow.get("SECTION_ID"));
            entity.setFieldValue(entityId + EF_FFT, dataRow.get("FFT"));
            entity.setFieldValue(entityId + EF_OLD_FFT, oldRow != null ? oldRow.get("FFT") : null);
            tx.insert(entity);
        }
    }

    public void insertHistoryDataRecordValueEntry(String txIid, AuditingType type, T8Timestamp timestamp, List<Map<String, Object>> dataRows, List<Map<String, Object>> oldRows) throws Exception
    {
        for (Map<String, Object> dataRow : dataRows)
        {
            Map<String, Object> oldRow;
            T8DataEntity entity;
            String entityId;

            entityId = HISTORY_DATA_RECORD_VALUE_DEID;
            oldRow = oldRows != null ? T8RecordDataHandler.getOldValueRow(oldRows, dataRow) : null;
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + EF_TX_IID, txIid);
            entity.setFieldValue(entityId + EF_TIME, timestamp);
            entity.setFieldValue(entityId + EF_EVENT, type.toString());
            entity.setFieldValue(entityId + EF_RECORD_ID, dataRow.get("RECORD_ID"));
            entity.setFieldValue(entityId + EF_ROOT_RECORD_ID, dataRow.get("ROOT_RECORD_ID"));
            entity.setFieldValue(entityId + EF_PROPERTY_ID, dataRow.get("PROPERTY_ID"));
            entity.setFieldValue(entityId + EF_DR_ID, dataRow.get("DR_ID"));
            entity.setFieldValue(entityId + EF_SECTION_ID, dataRow.get("SECTION_ID"));
            entity.setFieldValue(entityId + EF_DATA_TYPE_ID, dataRow.get("DATA_TYPE_ID"));
            entity.setFieldValue(entityId + EF_DATA_TYPE_SEQUENCE, dataRow.get("DATA_TYPE_SEQUENCE"));
            entity.setFieldValue(entityId + EF_VALUE_SEQUENCE, dataRow.get("VALUE_SEQUENCE"));
            entity.setFieldValue(entityId + EF_PARENT_VALUE_SEQUENCE, dataRow.get("PARENT_VALUE_SEQUENCE"));
            entity.setFieldValue(entityId + EF_FIELD_ID, dataRow.get("FIELD_ID"));
            entity.setFieldValue(entityId + EF_VALUE, dataRow.get("VALUE"));
            entity.setFieldValue(entityId + EF_FFT, dataRow.get("FFT"));
            entity.setFieldValue(entityId + EF_VALUE_ID, dataRow.get("VALUE_ID"));
            entity.setFieldValue(entityId + EF_VALUE_CONCEPT_ID, dataRow.get("VALUE_CONCEPT_ID"));
            entity.setFieldValue(entityId + EF_TEXT, dataRow.get("TEXT"));
            entity.setFieldValue(entityId + EF_OLD_VALUE, oldRow != null ? oldRow.get("VALUE") : null);
            entity.setFieldValue(entityId + EF_OLD_FFT, oldRow != null ? oldRow.get("FFT") : null);
            entity.setFieldValue(entityId + EF_OLD_VALUE_ID, oldRow != null ? oldRow.get("VALUE_ID") : null);
            entity.setFieldValue(entityId + EF_OLD_VALUE_CONCEPT_ID, oldRow != null ? oldRow.get("VALUE_CONCEPT_ID") : null);
            entity.setFieldValue(entityId + EF_OLD_TEXT, oldRow != null ? oldRow.get("TEXT") : null);
            tx.insert(entity);
        }
    }

    public void insertHistoryDataRecordAttachmentEntry(String txIid, AuditingType type, T8Timestamp timestamp, List<Map<String, Object>> dataRows) throws Exception
    {
        for (Map<String, Object> dataRow : dataRows)
        {
            T8DataEntity entity;
            String entityId;

            entityId = HISTORY_DATA_RECORD_ATTACHMENT_DEID;
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + EF_TX_IID, txIid);
            entity.setFieldValue(entityId + EF_TIME, timestamp);
            entity.setFieldValue(entityId + EF_EVENT, type.toString());
            entity.setFieldValue(entityId + EF_RECORD_ID, dataRow.get("RECORD_ID"));
            entity.setFieldValue(entityId + EF_ROOT_RECORD_ID, dataRow.get("ROOT_RECORD_ID"));
            entity.setFieldValue(entityId + EF_ATTACHMENT_ID, dataRow.get("ATTACHMENT_ID"));
            entity.setFieldValue(entityId + EF_FILE_CONTEXT_ID, dataRow.get("FILE_CONTEXT_ID"));
            entity.setFieldValue(entityId + EF_FILE_NAME, dataRow.get("FILE_NAME"));
            entity.setFieldValue(entityId + EF_FILE_SIZE, dataRow.get("FILE_SIZE"));
            entity.setFieldValue(entityId + EF_MD5_CHECKSUM, dataRow.get("MD5_CHECKSUM"));
            entity.setFieldValue(entityId + EF_MEDIA_TYPE, dataRow.get("MEDIA_TYPE"));
            tx.insert(entity);
        }
    }

    public void insertHistoryDataRecordAttributeEntry(String txIid, AuditingType type, T8Timestamp timestamp, List<Map<String, Object>> dataRows, List<Map<String, Object>> oldRows) throws Exception
    {
        for (Map<String, Object> dataRow : dataRows)
        {
            Map<String, Object> oldRow;
            T8DataEntity entity;
            String entityId;

            entityId = HISTORY_DATA_RECORD_ATTRIBUTE_DEID;
            oldRow = oldRows != null ? T8RecordDataHandler.getOldAttributeRow(oldRows, dataRow) : null;
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + EF_TX_IID, txIid);
            entity.setFieldValue(entityId + EF_TIME, timestamp);
            entity.setFieldValue(entityId + EF_EVENT, type.toString());
            entity.setFieldValue(entityId + EF_RECORD_ID, dataRow.get("RECORD_ID"));
            entity.setFieldValue(entityId + EF_ROOT_RECORD_ID, dataRow.get("ROOT_RECORD_ID"));
            entity.setFieldValue(entityId + EF_ATTRIBUTE_ID, dataRow.get("ATTRIBUTE_ID"));
            entity.setFieldValue(entityId + EF_TYPE, dataRow.get("TYPE"));
            entity.setFieldValue(entityId + EF_VALUE, dataRow.get("VALUE"));
            entity.setFieldValue(entityId + EF_OLD_VALUE, oldRow != null ? dataRow.get("VALUE") : null);
            tx.insert(entity);
        }
    }

    public void insertHistoryDataRecordAttributeEntry(String txIid, AuditingType type, T8Timestamp timestamp, RecordAttribute oldAttribute, RecordAttribute attribute) throws Exception
    {
        T8DataEntity entity;
        String entityId;

        entityId = HISTORY_DATA_RECORD_ATTRIBUTE_DEID;
        entity = tx.create(entityId, null);
        entity.setFieldValue(entityId + EF_TX_IID, txIid);
        entity.setFieldValue(entityId + EF_TIME, timestamp);
        entity.setFieldValue(entityId + EF_EVENT, type.toString());
        entity.setFieldValue(entityId + EF_RECORD_ID, attribute.getRecordId());
        entity.setFieldValue(entityId + EF_ROOT_RECORD_ID, attribute.getRootRecordId());
        entity.setFieldValue(entityId + EF_ATTRIBUTE_ID, attribute.getAttributeId());
        entity.setFieldValue(entityId + EF_TYPE, attribute.getValueTypeString());
        entity.setFieldValue(entityId + EF_VALUE, attribute.getValueString());
        entity.setFieldValue(entityId + EF_OLD_VALUE, oldAttribute != null ? oldAttribute.getValueString() : null);
        tx.insert(entity);
    }
}
