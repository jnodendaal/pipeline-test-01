package com.pilog.t8.data.document.datarecord.view;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.definition.data.document.datarecord.view.T8EPICViewConstructorDefinition;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8EPICDataRecordViewConstructor implements T8DataRecordViewConstructor
{
    private final String globalIdentifier;
    private final String identifier;
    private final T8ServerContextScript script;

    public T8EPICDataRecordViewConstructor(T8DataTransaction tx, T8EPICViewConstructorDefinition definition)
    {
        this.globalIdentifier = definition.getPublicIdentifier();
        this.identifier = definition.getIdentifier();

        // Construct the script.
        try
        {
            this.script = definition.getNewScriptInstance(tx.getContext());
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while creating script: " + definition, e);
        }
    }

    @Override
    public Map<String, Object> constructViewData(DataRecord inputRecord, T8DataFileAlteration alteration, String languageID)
    {
        Map<String, Object> inputParameters;

        // Create the input parameter collection.
        inputParameters = new HashMap<String, Object>();
        inputParameters.put(T8EPICViewConstructorDefinition.PARAMETER_DATA_RECORD, inputRecord);
        inputParameters.put(T8EPICViewConstructorDefinition.PARAMETER_ALTERATION, alteration);
        inputParameters.put(T8EPICViewConstructorDefinition.PARAMETER_LANGUAGE_ID, languageID);

        // Create the result map.
        try
        {
            Map<String, Object> outputParameters;

            // Execute the script.
            outputParameters = script.executeScript(inputParameters);
            return T8IdentifierUtilities.stripNamespace(outputParameters);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing data record view: " + this.globalIdentifier, e);
        }
    }
}
