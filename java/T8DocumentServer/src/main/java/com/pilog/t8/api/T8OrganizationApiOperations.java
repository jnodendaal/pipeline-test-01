package com.pilog.t8.api;

import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

import static com.pilog.t8.definition.api.T8OrganizationApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationApiOperations
{
    public static class ApiRefreshOrganizationStructureNestedIndices extends T8DefaultServerOperation
    {
        public ApiRefreshOrganizationStructureNestedIndices(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OrganizationApi orgAPI;

            // Perform the api operation.
            orgAPI = this.tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            orgAPI.refreshOrganizationStructureNestedIndices();
            return null;
        }
    }

    public static class ApiGetOranizationStructure extends T8DefaultServerOperation
    {
        public ApiGetOranizationStructure(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OrganizationStructure orgStructure;
            T8OrganizationApi orgAPI;

            // Perform the api operation.
            orgAPI = this.tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            orgStructure = orgAPI.getOrganizationStructure();
            return HashMaps.createSingular(PARAMETER_ORGANIZATION_STRUCTURE, orgStructure);
        }
    }

    public static class ApiSaveOranization extends T8DefaultServerOperation
    {
        public ApiSaveOranization(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OrganizationApi orgApi;
            T8OntologyConcept organizationConcept;
            String parentOrgId;
            String orgTypeId;
            boolean includeTerminology;

            // Get the nessacary input paramaters.
            organizationConcept = (T8OntologyConcept)operationParameters.get(PARAMETER_CONCEPT);
            parentOrgId = (String)operationParameters.get(PARAMETER_PARENT_ORG_ID);
            orgTypeId = (String)operationParameters.get(PARAMETER_ORG_TYPE_ID);
            includeTerminology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY);

            // Perform the api operation.
            orgApi = this.tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            orgApi.saveOrganization(parentOrgId, orgTypeId, organizationConcept, includeTerminology);
            return null;
        }
    }

    public static class ApiMoveOranization extends T8DefaultServerOperation
    {
        public ApiMoveOranization(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OrganizationApi orgApi;
            String parentOrgId;
            String orgId;
            Boolean success;

            // Get the nessacary input paramaters.
            orgId = (String)operationParameters.get(PARAMETER_ORG_ID);
            parentOrgId = (String)operationParameters.get(PARAMETER_PARENT_ORG_ID);

            // Perform the api operation.
            orgApi = this.tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            success = orgApi.moveOrganization(orgId, parentOrgId);

            // Return the result.
            return HashMaps.createSingular(PARAMETER_SUCCESS, success);
        }
    }

    public static class ApiRecacheOranizationStructure extends T8DefaultServerOperation
    {
        public ApiRecacheOranizationStructure(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8OrganizationStructure> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OrganizationApi orgApi;
            T8OrganizationStructure organizationStructure;

            // Perform the API operation.
            orgApi = this.tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            organizationStructure = orgApi.recacheOrganizationStructure();

            // Return the result.
            return HashMaps.createSingular(PARAMETER_ORGANIZATION_STRUCTURE, organizationStructure);
        }
    }
}