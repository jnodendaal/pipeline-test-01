package com.pilog.t8.data.document.datastring;

import com.pilog.t8.api.T8DataStringApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datastring.T8DataStringFormat.Type;
import com.pilog.t8.utilities.cache.LRUCache;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static com.pilog.t8.definition.api.T8DataStringApiResource.*;

/**
 * This class provides rendering formats to be used during rendering processes.
 * An internal cache is maintained to ensure that formats most recently used are
 * not re-retrieved unnecessarily.  Rendering formats are divided into three
 * parts:
 *  - DataString Format:  The container for one or more rendering elements that
 * are grouped together to form a set of related rendering rules.
 *  - DataString Element:  A set of rules to use for rendering one distinct type
 * of data particle identified by the element expression.
 *  - DataString Particle:  The smallest unit of data to be included in a
 * rendering.
 *
 * @author Bouwer du Preez
 */
public class T8ServerDataStringFormatProvider implements T8DataStringFormatProvider
{
    private final Map<String, String> generalFormatLinkCache; // Caches general data string format links (i.e. links that are not DR, DR Instance or Record specific).
    private final Map<String, Map<String, String>> fileFormatLinkCache; // Caches data string format links that are Data File-specific.
    private final Map<String, Map<String, String>> keyFormatLinkCache; // Caches data string format links that are key-specific.
    private final Map<String, Map<String, String>> drInstanceFormatLinkCache; // Caches data string format links that are DR Instance-specific.
    private final Map<String, Map<String, String>> drFormatLinkCache; // Caches data string format links that are DR-specific.
    private final Map<String, T8DataStringFormat> dataStringFormatCache; // Caches data string formats.
    private final Map<String, T8DataStringParticleSettingsList> recordParticleCache; // Caches data string particles that are record-specific.
    private final Map<String, T8DataStringParticleSettingsList> drInstanceParticleCache; // Caches data string particles that are DR Instance-specific.
    private final Map<String, T8DataStringParticleSettingsList> drParticleCache; // Caches data string particles that are DR-specific.
    private final T8DataStringApi dsApi;
    private final T8DataTransaction tx;
    private final String rootOrgId;

    public T8ServerDataStringFormatProvider(T8DataStringApi dsApi, T8DataTransaction tx, String rootOrgId, int cacheSize) throws Exception
    {
        this.dsApi = dsApi;
        this.tx = tx;
        this.rootOrgId = rootOrgId;
        this.dataStringFormatCache = new LRUCache<>(cacheSize);
        this.generalFormatLinkCache = new HashMap<>(); // These format links are not cleared as with LRU caches.  They remain available throughout the process.
        this.keyFormatLinkCache = new LRUCache<>(cacheSize);
        this.fileFormatLinkCache = new LRUCache<>(cacheSize);
        this.drInstanceFormatLinkCache = new LRUCache<>(cacheSize);
        this.drFormatLinkCache = new LRUCache<>(cacheSize);
        this.recordParticleCache = new LRUCache<>(cacheSize);
        this.drInstanceParticleCache = new LRUCache<>(cacheSize);
        this.drParticleCache = new LRUCache<>(cacheSize);
        cacheGeneralFormatLinks(rootOrgId);
    }

    /**
     * Retrieves all rendering formats for the specified root organization that
     * are not DR, DR Instance or Record specific and adds them to the general
     * format cache.
     * @param rootOrgID The root organization ID for which to cache rendering
     * formats.
     * @throws Exception
     */
    private void cacheGeneralFormatLinks(String rootOrgID) throws Exception
    {
        generalFormatLinkCache.clear();
        generalFormatLinkCache.putAll(retrieveRootOrganizationFormatLinks(rootOrgID));
    }

    /**
     * Returns the rendering format that best fits the key specified.  If no
     * rendering format is found for the specified key, null is returned.
     * @param key The rendering format key for which to fetch a rendering
     * format.
     * @return The rendering format that best fits the specified key or null if
     * no rendering format was found.
     */
    @Override
    public T8DataStringFormat getDataStringFormat(T8DataStringKey key)
    {
        try
        {
            Map<String, String> recordFormatLinks;
            Map<String, String> keyFormatLinks;
            Map<String, String> drInstanceFormatLinks;
            Map<String, String> drFormatLinks;
            String rootRecordId;
            String drInstanceId;
            String dataKeyId;
            String drId;

            // Get some values from the key to use for format retrieval and caching.
            drId = key.getDrId();
            drInstanceId = key.getDrInstanceId();
            dataKeyId = key.getDataKeyId();
            rootRecordId = key.getRootRecordId();

            // Fetch the DR Instance format links applicable to this key.
            drInstanceFormatLinks = drInstanceFormatLinkCache.get(drInstanceId);
            if (drInstanceFormatLinks == null)
            {
                drInstanceFormatLinks = retrieveDrInstanceFormatLinks(rootOrgId, drInstanceId);
                drInstanceFormatLinkCache.put(drInstanceId, drInstanceFormatLinks);
            }

            // Fetch the DR format links applicable to this key.
            drFormatLinks = drFormatLinkCache.get(drId);
            if (drFormatLinks == null)
            {
                drFormatLinks = retrieveDrFormatLinks(rootOrgId, drId);
                drFormatLinkCache.put(drId, drFormatLinks);
            }

            // Fetch the record format links applicable to this key.
            recordFormatLinks = fileFormatLinkCache.get(rootRecordId);
            if (recordFormatLinks == null)
            {
                recordFormatLinks = retrieveRootRecordFormatLinks(rootOrgId, rootRecordId);
                fileFormatLinkCache.put(rootRecordId, recordFormatLinks);
            }

            // Fetch the key links applicable to this key.
            if (dataKeyId != null)
            {
                keyFormatLinks = keyFormatLinkCache.get(dataKeyId);
                if (keyFormatLinks == null)
                {
                    keyFormatLinks = retrieveDataKeyFormatLinks(rootOrgId, dataKeyId);
                    keyFormatLinkCache.put(dataKeyId, keyFormatLinks);
                }
            }
            else keyFormatLinks = null;

            // Now find the most applicable format from those we have available.
            for (String keyString : key.getPrecedencyKeyStrings())
            {
                String dsFormatId;

                // Try to find the link from the record-specific collection.
                if (recordFormatLinks != null)
                {
                    dsFormatId = recordFormatLinks.get(keyString);
                    if (dsFormatId != null) return getDataStringFormat(dsFormatId);
                }

                // Try to find the link from the key-specific collection.
                if (keyFormatLinks != null)
                {
                    dsFormatId = keyFormatLinks.get(keyString);
                    if (dsFormatId != null) return getDataStringFormat(dsFormatId);
                }

                // Try to find the link from the DR Instance-specific collection.
                if (drInstanceFormatLinks != null)
                {
                    dsFormatId = drInstanceFormatLinks.get(keyString);
                    if (dsFormatId != null) return getDataStringFormat(dsFormatId);
                }

                // Try to find the link from the DR-specific collection.
                if (drFormatLinks != null)
                {
                    dsFormatId = drFormatLinks.get(keyString);
                    if (dsFormatId != null) return getDataStringFormat(dsFormatId);
                }

                // Try to find the link from the general collection.
                dsFormatId = generalFormatLinkCache.get(keyString);
                if (dsFormatId != null) return getDataStringFormat(dsFormatId);
            }

            // No applicable rendering format could be found.
            return null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving rendering format for key: " + key, e);
        }
    }

    /**
     * Retrieves the rendering format links identified by the supplied filter
     * criteria.
     * @param rootOrgId The root Organization ID to use for filtering.
     * @param rootRecordId The root record ID to use for filtering.
     * @return The map of rendering format links retrieved.  The map contains
     * key-value pairs in the form of <KEY_STRING, DATA_STRING_FORMAT_ID>.
     * @throws Exception
     */
    public Map<String, String> retrieveRootRecordFormatLinks(String rootOrgId, String rootRecordId) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        String entityId;
        T8DataFilter filter;

        // Retrieve the entity.
        entityId = DATA_STRING_FORMAT_LINK_DE_IDENTIFIER;

        // Create the new filter object.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityId, filterCriteria);

        // Add the root organization filter criterion.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_ROOT_ORG_ID, DataFilterOperator.EQUAL, rootOrgId));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_ROOT_RECORD_ID, DataFilterOperator.EQUAL, rootRecordId));

        // Select the entities that match the link criteria.
        entityList = tx.select(entityId, filter);
        return createDataStringFormatLinkMap(entityList);
    }

    /**
     * Retrieves the rendering format links identified by the supplied filter
     * criteria.
     * @param rootOrgId The root Organization ID to use for filtering.
     * @param dataKeyId The data key to use for filtering.
     * @return The map of rendering format links retrieved.  The map contains
     * key-value pairs in the form of <KEY_STRING, DATA_STRING_FORMAT_ID>.
     * @throws Exception
     */
    public Map<String, String> retrieveDataKeyFormatLinks(String rootOrgId, String dataKeyId) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        String entityId;
        T8DataFilter filter;

        // Retrieve the entity.
        entityId = DATA_STRING_FORMAT_LINK_DE_IDENTIFIER;

        // Create the new filter object.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityId, filterCriteria);

        // Add the root organization filter criterion.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_ROOT_ORG_ID, DataFilterOperator.EQUAL, rootOrgId));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_DATA_KEY_ID, DataFilterOperator.EQUAL, dataKeyId));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + F_ROOT_RECORD_ID, DataFilterOperator.IS_NULL, null));

        // Select the entities that match the link criteria.
        entityList = tx.select(entityId, filter);
        return createDataStringFormatLinkMap(entityList);
    }

    /**
     * Retrieves the rendering format links identified by the supplied filter
     * criteria.
     * @param rootOrgId The root Organization ID to use for filtering.
     * @param drInstanceId The DR Instance ID to use for filtering.
     * @return The map of rendering format links retrieved.  The map contains
     * key-value pairs in the form of <KEY_STRING, DATA_STRING_FORMAT_ID>.
     * @throws Exception
     */
    public Map<String, String> retrieveDrInstanceFormatLinks(String rootOrgId, String drInstanceId) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        String entityIdentifier;
        T8DataFilter filter;

        // Retrieve the entity.
        entityIdentifier = DATA_STRING_FORMAT_LINK_DE_IDENTIFIER;

        // Create the new filter object.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityIdentifier, filterCriteria);

        // Add the root organization filter criterion.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_ROOT_ORG_ID, DataFilterOperator.EQUAL, rootOrgId));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DR_INSTANCE_ID, DataFilterOperator.EQUAL, drInstanceId));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DATA_KEY_ID, DataFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_ROOT_RECORD_ID, DataFilterOperator.IS_NULL, null));

        // Select the entities that match the link criteria.
        entityList = tx.select(entityIdentifier, filter);
        return createDataStringFormatLinkMap(entityList);
    }

    /**
     * Retrieves the rendering format links identified by the supplied filter
     * criteria.
     * @param rootOrgId The root Organization ID to use for filtering.
     * @param drId The DR ID to use for filtering.
     * @return The map of rendering format links retrieved.  The map contains
     * key-value pairs in the form of <KEY_STRING, DATA_STRING_FORMAT_ID>.
     * @throws Exception
     */
    public Map<String, String> retrieveDrFormatLinks(String rootOrgId, String drId) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        String entityIdentifier;
        T8DataFilter filter;

        // Retrieve the entity.
        entityIdentifier = DATA_STRING_FORMAT_LINK_DE_IDENTIFIER;

        // Create the new filter object.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityIdentifier, filterCriteria);

        // Add the root organization filter criterion.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_ROOT_ORG_ID, DataFilterOperator.EQUAL, rootOrgId));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DR_ID, DataFilterOperator.EQUAL, drId));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DR_INSTANCE_ID, DataFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DATA_KEY_ID, DataFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_ROOT_RECORD_ID, DataFilterOperator.IS_NULL, null));

        // Select the entities that match the link criteria.
        entityList = tx.select(entityIdentifier, filter);
        return createDataStringFormatLinkMap(entityList);
    }

    /**
     * Retrieves the rendering format links identified by the supplied filter
     * criteria.
     * @param rootOrgId The root Organization ID to use for filtering.
     * @return The map of rendering format links retrieved.  The map contains
     * key-value pairs in the form of <KEY_STRING, DATA_STRING_FORMAT_ID>.
     * @throws Exception
     */
    public Map<String, String> retrieveRootOrganizationFormatLinks(String rootOrgId) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        String entityIdentifier;
        T8DataFilter filter;

        // Retrieve the entity.
        entityIdentifier = DATA_STRING_FORMAT_LINK_DE_IDENTIFIER;

        // Create the new filter object.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityIdentifier, filterCriteria);

        // Add the root organization filter criterion.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_ROOT_ORG_ID, DataFilterOperator.EQUAL, rootOrgId));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DR_ID, DataFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DR_INSTANCE_ID, DataFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DATA_KEY_ID, DataFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_ROOT_RECORD_ID, DataFilterOperator.IS_NULL, null));

        // Select the entities that match the link criteria.
        entityList = tx.select(entityIdentifier, filter);
        return createDataStringFormatLinkMap(entityList);
    }

    private Map<String, String> createDataStringFormatLinkMap(List<T8DataEntity> entityList)
    {
        Map<String, String> renderingFormatLinks;

        renderingFormatLinks = new HashMap<>();
        for (T8DataEntity linkEntity : entityList)
        {
            T8DataStringKey retrievedKey;
            String dsFormatId;

            // Construct a rendering format key from the retrieved data.
            retrievedKey = new T8DataStringKey(Type.valueOf((String)linkEntity.getFieldValue(F_TYPE)));
            retrievedKey.setDrId((String)linkEntity.getFieldValue(F_DR_ID));
            retrievedKey.setDrInstanceId((String)linkEntity.getFieldValue(F_DR_INSTANCE_ID));
            retrievedKey.setDataKeyId((String)linkEntity.getFieldValue(F_DATA_KEY_ID));
            retrievedKey.setRootRecordId((String)linkEntity.getFieldValue(F_ROOT_RECORD_ID));
            retrievedKey.setPropertyId((String)linkEntity.getFieldValue(F_PROPERTY_ID));
            retrievedKey.setFieldId((String)linkEntity.getFieldValue(F_FIELD_ID));
            retrievedKey.setDataTypeId((String)linkEntity.getFieldValue(F_DATA_TYPE_ID));
            retrievedKey.setLanguageId((String)linkEntity.getFieldValue(F_LANGUAGE_ID));
            retrievedKey.setDsTypeId((String)linkEntity.getFieldValue(F_DATA_STRING_TYPE_ID));
            retrievedKey.setOrgId((String)linkEntity.getFieldValue(F_ORG_ID));

            // Add the retrieved link to the map.
            dsFormatId = (String)linkEntity.getFieldValue(F_DATA_STRING_FORMAT_ID);
            renderingFormatLinks.put(retrievedKey.getKeyString(), dsFormatId);
        }

        // Return the complete set of links retrieved for the supplied rendering format key.
        return renderingFormatLinks;
    }

    /**
     * Returns the specified rendering format from cache.  If the format is not
     * found in cache, it is retrieved and added.
     * @param dsFormatId The ID of the data string format to fetch.
     * @return The fetched rendering format or null if the format was not found
     * in the cache, nor in the persistence store.
     * @throws Exception
     */
    private T8DataStringFormat getDataStringFormat(String dsFormatId) throws Exception
    {
        T8DataStringFormat dataStringFormat;

        dataStringFormat = dataStringFormatCache.get(dsFormatId);
        if (dataStringFormat == null)
        {
            dataStringFormat = dsApi.retrieveDataStringFormat(dsFormatId);
            dataStringFormatCache.put(dsFormatId, dataStringFormat);
        }

        return dataStringFormat;
    }

    /**
     * Fetches all rendering particles matching the supplied list of rendering
     * keys.
     * @param keys The rendering keys for which to retrieve rendering particles.
     * @return A rendering particle list containing all of the particles that
     * were found matching the supplied keys.
     */
    @Override
    public T8DataStringParticleSettingsList getDataStringParticleSettings(List<T8DataStringKey> keys)
    {
        T8DataStringParticleSettingsList particleList;
        Set<String> recordIdSet;
        Set<String> drInstanceIdSet;
        Set<String> drIdSet;

        // Create sets to hold ID's of particle lists that need to be retrieved.
        recordIdSet = new HashSet<>();
        drInstanceIdSet = new HashSet<>();
        drIdSet = new HashSet<>();

        // Create a list to hold all particles matching the rendering key.
        particleList = new T8DataStringParticleSettingsList();
        for (T8DataStringKey key : keys)
        {
            T8DataStringParticleSettingsList recordParticleList;
            T8DataStringParticleSettingsList drInstanceParticleList;
            T8DataStringParticleSettingsList drParticleList;
            String recordID;
            String drInstanceID;
            String drID;

            // Get the key data level ID's.
            recordID = key.getRecordId();
            drInstanceID = key.getDrInstanceId();
            drID = key.getDrId();

            // Fetch cached particle lists.
            recordParticleList = recordParticleCache.get(recordID);
            drInstanceParticleList = drInstanceParticleCache.get(drInstanceID);
            drParticleList = drParticleCache.get(drID);

            // If we could not find the record particles in the cache, add it to the list of ID's that need to be retrieved.
            if (recordParticleList == null)
            {
                recordIdSet.add(recordID);
            }
            else particleList.addParticleSettings(recordParticleList);

            // If we could not find the DR Instance particles in the cache, add it to the list of ID's that need to be retrieved.
            if (drInstanceParticleList == null)
            {
                drInstanceIdSet.add(drInstanceID);
            }
            else particleList.addParticleSettings(drInstanceParticleList);

            // If we could not find the DR Instance particles in the cache, add it to the list of ID's that need to be retrieved.
            if (drParticleList == null)
            {
                drIdSet.add(drID);
            }
            else particleList.addParticleSettings(drParticleList);
        }

        // Retrieve record particles that were not found in cache.
        if (recordIdSet.size() > 0)
        {
            try
            {
                T8DataStringParticleSettingsList recordParticleList;

                recordParticleList = dsApi.retrieveDataStringParticleSettings(null, null, recordIdSet);
                particleList.addParticleSettings(recordParticleList);

                // Add the retrieved rendering particles to the cache.
                for (String recordID : recordIdSet)
                {
                    recordParticleCache.put(recordID, recordParticleList.getRecordParticleSettingsList(recordID));
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while retrieving rendering particles for records: " + recordIdSet, e);
            }
        }

        // Retrieve DR Instance particles that were not found in cache.
        if (drIdSet.size() > 0)
        {
            try
            {
                T8DataStringParticleSettingsList drInstanceParticleList;

                drInstanceParticleList = dsApi.retrieveDataStringParticleSettings(null, drInstanceIdSet, null);
                particleList.addParticleSettings(drInstanceParticleList);

                // Add the retrieved rendering particles to the cache.
                for (String drInstanceID : drInstanceIdSet)
                {
                    drInstanceParticleCache.put(drInstanceID, drInstanceParticleList.getDrInstanceParticleSettingsList(drInstanceID));
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while retrieving rendering particles for DR Instances: " + recordIdSet, e);
            }
        }

        // Retrieve DR particles that were not found in cache.
        if (drIdSet.size() > 0)
        {
            try
            {
                T8DataStringParticleSettingsList drParticleList;

                drParticleList = dsApi.retrieveDataStringParticleSettings(drIdSet, null, null);
                particleList.addParticleSettings(drParticleList);

                // Add the retrieved rendering particles to the cache.
                for (String drId : drIdSet)
                {
                    drParticleCache.put(drId, drParticleList.getDrParticleSettingsList(drId));
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while retrieving rendering particles for DR's: " + recordIdSet, e);
            }
        }

        // Return all fetched particles.
        return particleList;
    }
}
