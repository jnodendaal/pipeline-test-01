package com.pilog.t8.data.alteration.handler;

import com.pilog.json.JsonObject;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.alteration.T8DataAlterationHandler;
import com.pilog.t8.data.alteration.T8DataAlterationImpact;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyCodeAlterationHandler implements T8DataAlterationHandler
{
    @Override
    public String getAlterationId()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int deleteAlteration(T8DataTransaction tx, String alterationID, int step) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlterationImpact applyAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlteration retrieveAlteration(T8DataTransaction tx, String alterationID, int step) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void adjustSteps(T8DataTransaction tx, String alterationID, int startStep, int endStep, int increment) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlterationImpact getAlterationImpact(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlteration deserializeAlteration(JsonObject jsonValue)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
