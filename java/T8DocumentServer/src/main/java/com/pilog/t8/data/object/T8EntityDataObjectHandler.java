package com.pilog.t8.data.object;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.filter.T8DataFilterExpressionParser;
import com.pilog.t8.data.object.filter.T8ObjectFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.object.T8EntityDataObjectDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EntityDataObjectHandler implements T8DataObjectHandler
{
    private final T8EntityDataObjectDefinition definition;
    private final T8Context context;
    private final T8SessionContext sessionContext;
    private final T8DataTransaction tx;
    private final String objectId;
    private final String orgId;
    private final String keyEntityFieldId;
    private final String languageEntityFieldId;
    private final String organizationEntityFieldId;
    private final String searchTermsFieldId;
    private final Map<String, String> entityFieldMapping;
    private final String entityId;
    private final String projectId;

    public T8EntityDataObjectHandler(T8EntityDataObjectDefinition definition, T8DataTransaction tx) throws Exception
    {
        this.definition = definition;
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.orgId = sessionContext.getRootOrganizationIdentifier();
        this.objectId = definition.getIdentifier();
        this.searchTermsFieldId = definition.getSearchTermsEntityFieldId();
        this.entityFieldMapping = definition.getEntityFieldMapping();
        this.entityId = definition.getDataEntityId();
        this.projectId = definition.getRootProjectId();
        this.keyEntityFieldId = entityFieldMapping.get(definition.getKeyFieldId());
        this.languageEntityFieldId = definition.getLanguageEntityFieldId();
        this.organizationEntityFieldId = definition.getOrganizationEntityFieldId();
    }

    @Override
    public String getDataObjectId()
    {
        return definition.getIdentifier();
    }

    @Override
    public T8DataObject retrieve(String objectIid) throws Exception
    {
        T8DataEntity entity;

        // Retrieve the entity from the view using the specified record id and the current transaction language.
        entity = tx.retrieve(entityId, HashMaps.newHashMap(keyEntityFieldId, objectIid, languageEntityFieldId, sessionContext.getContentLanguageIdentifier(), organizationEntityFieldId, orgId));
        if (entity != null)
        {
            // Create an object from the entity and return the result.
            return mapObject(entity);
        }
        else return null;
    }

    @Override
    public List<T8DataObject> search(T8ObjectFilter objectFilter, String searchString, int pageOffset, int pageSize) throws Exception
    {
        T8DataFilterExpressionParser parser;
        List<T8DataEntity> entities;
        List<T8DataObject> objects;
        T8DataFilter filter;

        // Parse the search expression and create a filter from it.
        parser = new T8DataFilterExpressionParser(entityId, ArrayLists.newArrayList(searchTermsFieldId));
        parser.setWhitespaceConjunction(T8DataFilterClause.DataFilterConjunction.AND);
        filter = parser.parseExpression(searchString, false);
        if (organizationEntityFieldId != null) filter.addFilterCriterion(organizationEntityFieldId, DataFilterOperator.EQUAL, orgId);
        if (languageEntityFieldId != null) filter.addFilterCriterion(languageEntityFieldId, DataFilterOperator.EQUAL, sessionContext.getContentLanguageIdentifier());

        // Select entities from the view using the search filter.
        objects = new ArrayList<>();
        entities = tx.select(entityId, filter, pageOffset, pageSize);
        for (T8DataEntity entity : entities)
        {
            // Map each entity to a new data object and add it to the result.
            objects.add(mapObject(entity));
        }

        // Return the final list of objects, found using the search filter.
        return objects;
    }

    private T8DataObject mapObject(T8DataEntity entity)
    {
        T8DataObject object;

        object = new T8DataObject(definition, null);
        object.setFieldValues(T8IdentifierUtilities.mapParameters(entity.getFieldValues(), T8IdentifierUtilities.createReverseIdentifierMap(entityFieldMapping)));
        return object;
    }
}
