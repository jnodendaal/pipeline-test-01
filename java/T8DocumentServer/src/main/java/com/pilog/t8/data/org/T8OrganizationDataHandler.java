package com.pilog.t8.data.org;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.cache.T8TerminologyCache;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.entity.T8DataEntityFieldIterator;
import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.data.ontology.T8OntologyClass;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.definition.data.document.T8DocumentResources;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.collections.HashSets;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationDataHandler implements T8PerformanceStatisticsProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8OrganizationDataHandler.class);

    private final T8ServerContext serverContext;
    private T8PerformanceStatistics stats;

    public T8OrganizationDataHandler(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is always false.  Other clients of the API can set this variable to their own instance of stats.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public void insertOrganization(T8DataTransaction tx, String parentORGID, String orgID, String orgTypeID, String rootOrgID, int orgLevel) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the organization structure and entity identifiers.
        entityIdentifier = ORG_STRUCTURE_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ORG_ID, orgID);
        entity.setFieldValue(entityIdentifier + EF_PARENT_ORG_ID, parentORGID);
        entity.setFieldValue(entityIdentifier + EF_ORG_TYPE_ID, orgTypeID);
        entity.setFieldValue(entityIdentifier + EF_ROOT_ORG_ID, rootOrgID);
        entity.setFieldValue(entityIdentifier + EF_ORG_LEVEL, orgLevel);

        // Insert the organization.
        tx.insert(entity);
    }

    public void saveOrganization(T8DataTransaction tx, String parentORGID, String orgID, String orgTypeID, String rootOrgID, int orgLevel) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the organization structure and entity identifiers.
        entityIdentifier = ORG_STRUCTURE_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ORG_ID, orgID);
        entity.setFieldValue(entityIdentifier + EF_PARENT_ORG_ID, parentORGID);
        entity.setFieldValue(entityIdentifier + EF_ORG_TYPE_ID, orgTypeID);
        entity.setFieldValue(entityIdentifier + EF_ROOT_ORG_ID, rootOrgID);
        entity.setFieldValue(entityIdentifier + EF_ORG_LEVEL, orgLevel);

        // Save the organization.
        if (!tx.update(entity))
        {
            tx.insert(entity);
        }
    }

    public boolean deleteOrganization(T8DataTransaction tx, String orgID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the organization structure and entity identifiers.
        entityIdentifier = ORG_STRUCTURE_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ORG_ID, orgID);

        // Delete the organization.
        return tx.delete(entity);
    }

    public boolean updateOrganizationType(T8DataTransaction tx, String orgID, String newOrgTypeID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the organization structure and entity identifiers.
        entityIdentifier = ORG_STRUCTURE_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ORG_ID, orgID);
        entity.setFieldValue(entityIdentifier + EF_ORG_TYPE_ID, newOrgTypeID);

        // Update the organization.
        return tx.update(entity);
    }

    public boolean moveOrganizationToNewParent(T8DataTransaction tx, String orgID, String newParentOrgID, String newRootOrgID, int newOrgLevel) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the organization structure and entity identifiers.
        entityIdentifier = ORG_STRUCTURE_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ORG_ID, orgID);
        entity.setFieldValue(entityIdentifier + EF_PARENT_ORG_ID, newParentOrgID);
        entity.setFieldValue(entityIdentifier + EF_ROOT_ORG_ID, newRootOrgID);
        entity.setFieldValue(entityIdentifier + EF_ORG_LEVEL, newOrgLevel);

        // Update the organization.
        return tx.update(entity);
    }

    public String retrieveRootOrganizationID(T8DataTransaction tx, String orgID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ORG_STRUCTURE_DE_IDENTIFIER;

        // Retrieve the organization entity and fetch the org level.
        entity = tx.retrieve(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_ORG_ID, orgID));
        if (entity != null)
        {
            return (String)entity.getFieldValue(entityIdentifier + EF_ROOT_ORG_ID);
        }
        else throw new Exception("Organization not found: " + orgID);
    }

    public String retrieveParentOrganizationID(T8DataTransaction tx, String orgID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ORG_STRUCTURE_DE_IDENTIFIER;

        // Retrieve the organization entity and fetch the org level.
        entity = tx.retrieve(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_ORG_ID, orgID));
        if (entity != null)
        {
            return (String)entity.getFieldValue(entityIdentifier + EF_PARENT_ORG_ID);
        }
        else throw new Exception("Organization not found: " + orgID);
    }

    public int retrieveOrganizationLevel(T8DataTransaction tx, String orgID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ORG_STRUCTURE_DE_IDENTIFIER;

        // Retrieve the organization entity and fetch the org level.
        entity = tx.retrieve(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_ORG_ID, orgID));
        if (entity != null)
        {
            return (Integer)entity.getFieldValue(entityIdentifier + EF_ORG_LEVEL);
        }
        else throw new Exception("Organization not found: " + orgID);
    }

    public void updateOrganizationIndices(T8DataTransaction tx, String orgID, int leftIndex, int rightIndex) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ORG_STRUCTURE_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ORG_ID, orgID);
        entity.setFieldValue(entityIdentifier + EF_LFT, leftIndex);
        entity.setFieldValue(entityIdentifier + EF_RGT, rightIndex);

        // Update the organization.
        tx.update(entity);
    }

    public Map<String, T8OrganizationStructure> retrieveOrganizationStructures(T8DataTransaction tx, Set<String> orgIDSet) throws Exception
    {
        Map<String, T8OrganizationStructure> organizationStructures;
        List<T8DataEntity> orgStructureEntityList;
        List<T8DataEntity> orgOntologyStructureEntityList;
        T8DataFilter orgStructureFilter;
        LinkedList<T8DataEntity> entityQueue;
        String orgStructureEID;
        String orgOntologyStructureEID;

        // Get the entity identifier an retrieve all organization structure entities.
        orgStructureEID = ORG_STRUCTURE_DE_IDENTIFIER;
        orgOntologyStructureEID = ORG_ONTOLOGY_STRUCTURE_DE_IDENTIFIER;
        organizationStructures = new HashMap<>();

        // Retrieve all required ORG_STRUCTURE data.
        orgStructureFilter = new T8DataFilter(ORG_STRUCTURE_DE_IDENTIFIER);
        if (!CollectionUtilities.isNullOrEmpty(orgIDSet)) orgStructureFilter.addFilterCriteria(DataFilterConjunction.AND, ORG_STRUCTURE_DE_IDENTIFIER + EF_ROOT_ORG_ID, orgIDSet);
        orgStructureEntityList = new LinkedList<>(tx.select(orgStructureEID, orgStructureFilter));

        // Retrieve all required TABLE_ORG_ONTOLOGY_STRUCTURE data.
        orgOntologyStructureEntityList = tx.select(orgOntologyStructureEID, (T8DataFilter)null);

        // Get all root entities and add them to the queue.
        entityQueue = new LinkedList<>(T8DataUtilities.findDataEntities(orgStructureEntityList, orgStructureEID + EF_PARENT_ORG_ID, null));

        // Continue to process all entities in the queue.
        while (entityQueue.size() > 0)
        {
            T8DataEntity nextEntity;
            String rootOrgID;
            String parentOrgID;
            String orgID;
            String term;
            String orgTypeID;

            // Get the next entity and process it.
            nextEntity = entityQueue.pop();
            rootOrgID = (String)nextEntity.getFieldValue(orgStructureEID + EF_ROOT_ORG_ID);
            parentOrgID = (String)nextEntity.getFieldValue(orgStructureEID + EF_PARENT_ORG_ID);
            orgID = (String)nextEntity.getFieldValue(orgStructureEID + EF_ORG_ID);
            orgTypeID = (String)nextEntity.getFieldValue(orgStructureEID + EF_ORG_TYPE_ID);
            term = (String)nextEntity.getFieldValue(orgStructureEID + EF_TERM);

            // No parent ID means this is the root of a new structure.
            if (parentOrgID == null)
            {
                T8Organization newNode;
                List<T8DataEntity> orgOntologyStructureEntities;
                String dataStructureID;

                // Retrieve the base data structure ID for the organization.
                dataStructureID = retrieveOrganizationBaseDataStructureID(tx, orgID);

                // Add the new node.
                newNode = new T8Organization(orgID, term, orgTypeID);
                organizationStructures.put(orgID, new T8OrganizationStructure(newNode, dataStructureID));

                // Set the ontology structures linked to this organization node.
                orgOntologyStructureEntities = T8DataUtilities.findDataEntities(orgOntologyStructureEntityList, orgOntologyStructureEID + EF_ORG_ID, orgID);
                for (T8DataEntity orgOntologyStructureEntity : orgOntologyStructureEntities)
                {
                    newNode.addOntologyStructureID((String)orgOntologyStructureEntity.getFieldValue(orgOntologyStructureEID + EF_ODS_ID));
                }

                // Add all of the node's child entities to the queue.
                entityQueue.addAll(T8DataUtilities.findDataEntities(orgStructureEntityList, orgStructureEID + EF_PARENT_ORG_ID, orgID));
            }
            else if (Objects.equals(parentOrgID, orgID))
            {
                // Invalid data found.
                LOGGER.log("Invalid data found:  ORG_ID and PARENT_ORG_ID are the same: " + parentOrgID);
            }
            else // The new node has a parent, so we need to find the parent and add its new child.
            {
                T8OrganizationStructure organizationStructure;

                // Find the organization structure to which the new node will be added.
                organizationStructure = organizationStructures.get(rootOrgID);

                // If we find the parent structure, add the new node to it otherwise just add the entity back in the queue.
                if (organizationStructure != null)
                {
                    if (organizationStructure.getNode(orgID) != null)
                    {
                        // Invalid data found.
                        LOGGER.log("Cyclic reference found:  ORG_ID '" + orgID + "' already added to root: " + organizationStructure.getRootOrganizationID());
                    }
                    else
                    {
                        T8Organization newNode;
                        T8Organization parentNode;
                        List<T8DataEntity> orgOntologyStructureEntities;

                        // Add the new node.
                        parentNode = organizationStructure.getNode(parentOrgID);
                        newNode = new T8Organization(orgID, term, orgTypeID);
                        parentNode.addChildNode(newNode);

                        // Set the ontology structures linked to this organization node.
                        orgOntologyStructureEntities = T8DataUtilities.findDataEntities(orgOntologyStructureEntityList, orgOntologyStructureEID + EF_ORG_ID, orgID);
                        for (T8DataEntity orgOntologyStructureEntity : orgOntologyStructureEntities)
                        {
                            newNode.addOntologyStructureID((String)orgOntologyStructureEntity.getFieldValue(orgOntologyStructureEID + EF_ODS_ID));
                        }

                        // Add all of the node's child entities to the queue.
                        entityQueue.addAll(T8DataUtilities.findDataEntities(orgStructureEntityList, orgStructureEID + EF_PARENT_ORG_ID, orgID));
                    }
                }
                else throw new Exception("ORG Parent not found: " + parentOrgID);
            }
        }

        // Return the result.
        return organizationStructures;
    }

    private String retrieveOrganizationBaseDataStructureID(T8DataTransaction tx, String orgID) throws Exception
    {
        T8DataEntity rootEntity;
        String entityIdentifier;

        entityIdentifier = T8DocumentResources.ORG_ROOT_DE_IDENTIFIER;
        rootEntity = tx.retrieve(entityIdentifier, HashMaps.createSingular(entityIdentifier + EF_ORG_ID, orgID));
        if (rootEntity != null)
        {
            return (String)rootEntity.getFieldValue(entityIdentifier + EF_ODS_ID);
        }
        else return null;
    }

    public T8OntologyStructure retrieveOntologyStructures(T8DataTransaction tx, String osID) throws Exception
    {
        Map<String, T8OntologyStructure> ontologyStructures;

        ontologyStructures = retrieveOntologyStructures(tx, HashSets.buildSet(osID));
        return ontologyStructures.get(osID);
    }

    public Map<String, T8OntologyStructure> retrieveOntologyStructures(T8DataTransaction tx, Collection<String> osIDSet) throws Exception
    {
        Map<String, T8OntologyStructure> ontologyStructures;
        List<T8DataEntity> dataEntityList;
        LinkedList<T8DataEntity> entityQueue;
        T8DataFilter ontologyStructureFilter;
        String entityIdentifier;

        // Get the entity identifier an retrieve all Ontology Data Structure entities.
        entityIdentifier = T8DocumentResources.ONTOLOGY_CLASS_DE_IDENTIFIER;

        // Create the filter to use.
        ontologyStructureFilter = new T8DataFilter(entityIdentifier);
        ontologyStructureFilter.addFilterCriteria(DataFilterConjunction.AND, entityIdentifier + EF_ODS_ID, osIDSet);

        // Get all structure entities.
        dataEntityList = new LinkedList<>(tx.select(entityIdentifier, ontologyStructureFilter));

        // Get all root entities and add them to the queue.
        entityQueue = new LinkedList<>(T8DataUtilities.findDataEntities(dataEntityList, entityIdentifier + EF_PARENT_ODT_ID, null));

        // Continue to process all entities in the queue.
        ontologyStructures = new HashMap<>();
        while (entityQueue.size() > 0)
        {
            T8DataEntity nextEntity;
            String dataStructureID;
            String dataTypeID;
            String parentID;

            // Get the next entity and process it.
            nextEntity = entityQueue.pop();
            parentID = (String)nextEntity.getFieldValue(entityIdentifier + EF_PARENT_ODT_ID);
            dataTypeID = (String)nextEntity.getFieldValue(entityIdentifier + EF_ODT_ID);
            dataStructureID = (String)nextEntity.getFieldValue(entityIdentifier + EF_ODS_ID);

            // No parent ID means this is the root of a new structure.
            if (parentID == null)
            {
                if (!ontologyStructures.containsKey(dataStructureID))
                {
                    T8OntologyClass newNode;

                    // Add the new node.
                    newNode = new T8OntologyClass(dataTypeID, null);
                    ontologyStructures.put(dataStructureID, new T8OntologyStructure(dataStructureID, newNode));

                    // Add all of the node's child entities to the queue.
                    entityQueue.addAll(T8DataUtilities.findDataEntities(dataEntityList, entityIdentifier + EF_PARENT_ODT_ID, dataTypeID));
                }
                else throw new Exception("Ontology Data Structure '" + dataStructureID + "' cannot be loaded as it contains multiple root nodes.");
            }
            else if (Objects.equals(parentID, dataTypeID))
            {
                // Invalid data found.
                throw new Exception("Invalid data found:  ODT_ID and PARENT_ODT_ID are the same: " + parentID);
            }
            else // The new node has a parent, so we need to find the parent and add its new child.
            {
                T8OntologyStructure organizationDataStructure;

                // Find the organization structure that contains the required parent.
                organizationDataStructure = ontologyStructures.get(dataStructureID);

                // If we find the parent structure, add the new node to it otherwise just add the entity back in the queue.
                if (organizationDataStructure != null)
                {
                    if (organizationDataStructure.getNode(dataTypeID) != null)
                    {
                        // Invalid data found.
                        LOGGER.log("Cyclic reference found:  ODT_ID already added to structure: " + dataStructureID);
                    }
                    else
                    {
                        T8OntologyClass parentNode;

                        // If we find the parent node, add the new node to it otherwise just add the entity back in the queue.
                        parentNode = organizationDataStructure.getNode(parentID);
                        if (parentNode != null)
                        {
                            T8OntologyClass newNode;

                            // Add the new node.
                            newNode = new T8OntologyClass(dataTypeID, null);
                            parentNode.addChildNode(newNode);

                            // Add all of the node's child entities to the queue.
                            entityQueue.addAll(T8DataUtilities.findDataEntities(dataEntityList, entityIdentifier + EF_PARENT_ODT_ID, dataTypeID));
                        }
                        else throw new Exception("ODT Parent not found: " + parentID);
                    }
                }
                else throw new Exception("Data Structure not found: " + dataStructureID);
            }
        }

        // Return the result.
        return ontologyStructures;
    }

    public void insertOntologyLink(T8DataTransaction tx, T8OntologyLink link) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ORG_ONTOLOGY_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ORG_ID, link.getOrganizationID());
        entity.setFieldValue(entityIdentifier + EF_ODS_ID, link.getOntologyStructureID());
        entity.setFieldValue(entityIdentifier + EF_ODT_ID, link.getOntologyClassID());
        entity.setFieldValue(entityIdentifier + EF_CONCEPT_ID, link.getConceptID());
        entity.setFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR, link.isActive() ? "Y" : "N");

        // Insert the link.
        tx.insert(entity);
    }

    public boolean deleteOntologyLink(T8DataTransaction tx, T8OntologyLink link) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ORG_ONTOLOGY_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ORG_ID, link.getOrganizationID());
        entity.setFieldValue(entityIdentifier + EF_ODS_ID, link.getOntologyStructureID());
        entity.setFieldValue(entityIdentifier + EF_ODT_ID, link.getOntologyClassID());
        entity.setFieldValue(entityIdentifier + EF_CONCEPT_ID, link.getConceptID());

        // Insert the link.
        return tx.delete(entity);
    }

    public void linkConceptsToOrganization(T8DataTransaction tx, String orgID, String osId, String ocId, Collection<String> conceptIDList) throws Exception
    {
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = ORG_ONTOLOGY_DE_IDENTIFIER;
        for (String conceptId : conceptIDList)
        {
            T8DataEntity entity;

            // Construct the entity.
            entity = tx.create(entityIdentifier, null);
            entity.setFieldValue(entityIdentifier + EF_ORG_ID, orgID);
            entity.setFieldValue(entityIdentifier + EF_ODS_ID, osId);
            entity.setFieldValue(entityIdentifier + EF_ODT_ID, ocId);
            entity.setFieldValue(entityIdentifier + EF_CONCEPT_ID, conceptId);

            // Insert the link.
            if (tx.retrieve(entityIdentifier, entity.getFieldValues()) == null)
            {
                tx.insert(entity);
            }
        }
    }

    public int unlinkConceptsFromOrganizations(T8DataTransaction tx, Collection<String> orgIDList, Collection<String> odtIDList, Collection<String> conceptIDList) throws Exception
    {
        T8DataFilter linkFilter;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = ORG_ONTOLOGY_DE_IDENTIFIER;

        // Create the filter that will be used to delete all of the specified links.
        linkFilter = new T8DataFilter(entityIdentifier);
        if (CollectionUtilities.hasContent(orgIDList)) linkFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_ORG_ID, DataFilterOperator.IN, orgIDList, false);
        if (CollectionUtilities.hasContent(odtIDList)) linkFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_ODT_ID, DataFilterOperator.IN, odtIDList, false);
        if (CollectionUtilities.hasContent(conceptIDList)) linkFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_ID, DataFilterOperator.IN, conceptIDList, false);

        // Delete all links identified by the filter.
        return tx.delete(entityIdentifier, linkFilter);
    }

    public void setConceptActive(T8DataTransaction tx, Collection<String> orgIDList, Collection<String> odtIDList, String conceptID, boolean active) throws Exception
    {
        List<T8DataEntity> linkEntities;
        T8DataFilter linkFilter;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = ORG_ONTOLOGY_DE_IDENTIFIER;

        // Create the filter that will be used to fetch all links to be updated.
        linkFilter = new T8DataFilter(entityIdentifier);
        if (CollectionUtilities.hasContent(orgIDList)) linkFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_ORG_ID, DataFilterOperator.IN, orgIDList, false);
        if (CollectionUtilities.hasContent(odtIDList)) linkFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_ODT_ID, DataFilterOperator.IN, odtIDList, false);
        linkFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_ID, DataFilterOperator.EQUAL, conceptID, false);
        linkFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_ACTIVE_INDICATOR, DataFilterOperator.EQUAL, active ? "N" : "Y", false); // Only retrieve the rows that are of the opposite state so we only update the necessary rows.

        // Update all links identified by the filter to the required state.
        linkEntities = tx.select(entityIdentifier, linkFilter);
        for (T8DataEntity linkEntity : linkEntities)
        {
            linkEntity.setFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR, active ? "Y" : "N");
            tx.update(linkEntity);
        }
    }

    /**
     * Retrieves all ontology codes using the code itself instead of the code ID
     * as filter argument along with the organizations, groups and code type.
     * @param tx The transaction to use for the operation.
     * @param orgIDList The list of organizations (to which the code is linked)
     * to include in the retrieval.  If null, this filter argument will be
     * ignored.
     * @param odtIDList The list of groups (to which the code is linked) to
     * include in the retrieval.  If null, this filter argument will be ignored.
     * @param codeTypeIDList The list of code types to include in the retrieval.
     * If null, this filter argument will be ignored.
     * @param code The actual code to use as filter argument.  This parameter
     * can not be null.
     * @return The list of codes retrieved using the specified retrieval
     * criteria.
     * @throws Exception
     */
    public List<T8OntologyCode> retrieveOntologyCodes(T8DataTransaction tx, Collection<String> orgIDList, Collection<String> odtIDList, Collection<String> codeTypeIDList, String code) throws Exception
    {
        String terminologyEntityID;
        List<T8DataEntity> dataEntities;
        List<T8OntologyCode> ontologyCodes;
        T8DataFilter filter;

        // Get the entity identifier.
        terminologyEntityID = ORG_ONTOLOGY_CONCEPT_CODE_DE_IDENTIFIER;

        // Create the filter to retrieve all DR Instance ID's belonging to the specified group and its descendants.
        filter = new T8DataFilter(terminologyEntityID);
        if (odtIDList != null) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ODT_ID, DataFilterOperator.IN, odtIDList, false);
        if (codeTypeIDList != null) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_CODE_TYPE_ID, DataFilterOperator.IN, codeTypeIDList, false);
        filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_CODE, DataFilterOperator.EQUAL, code, false);

        // Select the entities using the filter.
        dataEntities = tx.select(terminologyEntityID, filter);

        // Get the selected concept ID's.
        ontologyCodes = new ArrayList<>();
        for (T8DataEntity entity : dataEntities)
        {
            String codeID;
            String conceptID;
            String codeTypeID;

            // Get the field values from the entity.
            codeID = (String)entity.getFieldValue(terminologyEntityID + EF_CODE_ID);
            conceptID = (String)entity.getFieldValue(terminologyEntityID + EF_CONCEPT_ID);
            codeTypeID = (String)entity.getFieldValue(terminologyEntityID + EF_CODE_TYPE_ID);

            // Add a newly constructed ontology code object to the list that will be returned.
            ontologyCodes.add(new T8OntologyCode(codeID, conceptID, codeTypeID, code, true, true));
        }

        return ontologyCodes;
    }


    /**
     * Returns an iterator that will scroll over all of the concept ID's
     * belonging to the specified data types and organizations.
     * @param tx The transaction to use.
     * @param orgIDList The list of organizations for which data will be
     * included in the iterator.
     * @param odtIDList The list of data types to include in the iterator.
     * @throws Exception
     */
    public T8DataIterator<String> getConceptIDIterator(T8DataTransaction tx, List<String> orgIDList, List<String> odtIDList) throws Exception
    {
        T8DataEntityFieldIterator<String> iterator;
        T8DataEntityResults conceptIDResults;
        T8DataFilter conceptIDFilter;
        String orgOntologyEntityIdentifier;

        // Get the entity identifier from which the Concept links to the organization data structure will be retrieved.
        orgOntologyEntityIdentifier = ORG_ONTOLOGY_DE_IDENTIFIER;

        // Create the filter to retrieve all Concept ID's belonging to the specified group and its descendants.
        conceptIDFilter = new T8DataFilter(orgOntologyEntityIdentifier);
        conceptIDFilter.addFilterCriterion(DataFilterConjunction.AND, orgOntologyEntityIdentifier + EF_ORG_ID, DataFilterOperator.IN, orgIDList, false);
        conceptIDFilter.addFilterCriterion(DataFilterConjunction.AND, orgOntologyEntityIdentifier + EF_ODT_ID, DataFilterOperator.IN, odtIDList, false);

        // Get the entity results that will scroll over the results retrieve using the filter.
        conceptIDResults = tx.scroll(orgOntologyEntityIdentifier, conceptIDFilter);
        iterator = new T8DataEntityFieldIterator<>(tx, conceptIDResults, orgOntologyEntityIdentifier + EF_CONCEPT_ID);
        return iterator;
    }

    public Set<String> retrieveConceptIDSet(T8DataTransaction tx, Collection<String> orgIDList, Collection<String> odtIDList) throws Exception
    {
        List<T8DataEntity> conceptEntityList;
        T8DataFilter conceptIDFilter;
        String orgOntologyEntityIdentifier;

        // Get the entity identifier from which the Concept links to the organization data structure will be retrieved.
        orgOntologyEntityIdentifier = ORG_ONTOLOGY_DE_IDENTIFIER;

        // Create the filter to retrieve all Concept ID's belonging to the specified group and its descendants.
        conceptIDFilter = new T8DataFilter(orgOntologyEntityIdentifier);
        conceptIDFilter.addFilterCriterion(DataFilterConjunction.AND, orgOntologyEntityIdentifier + EF_ORG_ID, DataFilterOperator.IN, orgIDList, false);
        conceptIDFilter.addFilterCriterion(DataFilterConjunction.AND, orgOntologyEntityIdentifier + EF_ODT_ID, DataFilterOperator.IN, odtIDList, false);

        // Get the entity results that will scroll over the results retrieve using the filter.
        conceptEntityList = tx.select(orgOntologyEntityIdentifier, conceptIDFilter, 0, 10000); // We set a hard limit of 10 000 to ensure that we don't retrieve too much data.
        if (conceptEntityList.size() == 10000) LOGGER.log("Warning:  The retrieved result set has a size equal to the hard limit.  There may be more data that has not been retrieved.");
        return T8DataUtilities.<String>getEntityFieldValueSet(conceptEntityList, orgOntologyEntityIdentifier + EF_CONCEPT_ID);
    }

    /**
     * This method retrieves the concept terminology for the requested concepts
     * in the requested languages.
     *
     * @param tx
     * @param orgIDList
     * @param languageIDList The list of terminology languages to retrieve.
     * @param conceptIDList The concept ID's for which to retrieve the terminology.
     * @return The list of concept terminology retrieved.
     * @throws Exception
     */
    public T8ConceptTerminologyList retrieveTerminology(T8DataTransaction tx, Collection<String> orgIDList, Collection<String> languageIDList, Collection<String> conceptIDList) throws Exception
    {
        T8TerminologyDataHandler terminologyHandler;

        // Retrieve the terminology for all organizations.
        terminologyHandler = new T8TerminologyDataHandler(tx);
        return terminologyHandler.loadConceptTerminology(ORG_TERMINOLOGY_DE_IDENTIFIER, orgIDList, languageIDList, conceptIDList);
    }

    public void insertTerminology(T8DataTransaction tx, T8ConceptTerminology terminology) throws Exception
    {
        T8DataEntity entity;

        // Create the new terminology entity and persist it.
        entity = constructTerminologyDataEntity(tx, terminology, false);
        tx.insert(entity);

        // Update the terminology cache.
        tx.getTerminologyCache().put(terminology);
    }

    public void updateTerminology(T8DataTransaction tx, T8ConceptTerminology terminology, boolean deletionEnabled) throws Exception
    {
        T8DataEntity entity;

        // Create the new terminology entity and persist it.
        entity = constructTerminologyDataEntity(tx, terminology, deletionEnabled);
        tx.update(entity);

        // Update the terminology cache.
        cacheTerminology(tx, terminology, deletionEnabled);
    }

    public void saveTerminology(T8DataTransaction tx, T8ConceptTerminology terminology, boolean deletionEnabled) throws Exception
    {
        T8DataEntity entity;

        // Create the new terminology entity.
        entity = constructTerminologyDataEntity(tx, terminology, deletionEnabled);

        // Try to update the entity.  If it cannot be updated it does not exist so insert a new entry.
        if (!tx.update(entity))
        {
            tx.insert(entity);
        }

        // Update the terminology cache.
        cacheTerminology(tx, terminology, deletionEnabled);
    }

    private void cacheTerminology(T8DataTransaction tx, T8ConceptTerminology terminology, boolean deletionEnabled)
    {
        T8TerminologyCache cache;

        // Get the cache.
        cache = tx.getTerminologyCache();
        if (deletionEnabled)
        {
            // If deletion is enabled, just add the supplied terminology to the cache, replacing the existing entry (if any).
            cache.put(terminology);
        }
        else // If deletion is not enabled, we have to only add the parts of the terminology available in the input parameter, to the cache.
        {
            T8ConceptTerminology cachedTerminology;

            // Get the cached terminology.
            cachedTerminology = (T8ConceptTerminology)cache.get(terminology.getLanguageId(), terminology.getConceptId());
            if (cachedTerminology == null)
            {
                // If no cached terminology existed, just add the input.
                cache.put(terminology);
            }
            else
            {

                // Add the term data if available.
                if (!Strings.isNullOrEmpty(terminology.getTermId()))
                {
                    cachedTerminology.setTermId(terminology.getTermId());
                    cachedTerminology.setTerm(terminology.getTerm());
                }

                // Add the definition data if available.
                if (!Strings.isNullOrEmpty(terminology.getDefinitionId()))
                {
                    cachedTerminology.setDefinitionId(terminology.getDefinitionId());
                    cachedTerminology.setDefinition(terminology.getDefinition());
                }

                // Add the abbreviation data if available.
                if (!Strings.isNullOrEmpty(terminology.getAbbreviationId()))
                {
                    cachedTerminology.setAbbreviationId(terminology.getAbbreviationId());
                    cachedTerminology.setAbbreviation(terminology.getAbbreviation());
                }

                // Add the code data if available.
                if (!Strings.isNullOrEmpty(terminology.getCodeId()))
                {
                    cachedTerminology.setCodeId(terminology.getCodeId());
                    cachedTerminology.setCode(terminology.getCode());
                }

                // Reset the enty in the cache.
                cache.put(cachedTerminology);
            }
        }
    }

    public void deleteTerminology(T8DataTransaction tx, T8ConceptTerminology terminology) throws Exception
    {
        T8DataEntity entity;

        // Create the new terminology entity.
        entity = constructTerminologyDataEntity(tx, terminology, false);

        // Try to retrieve the entity.  If it exists, delete it.
        tx.delete(entity);

        // Update the terminology cache.
        tx.getTerminologyCache().remove(terminology);
    }

    public void deleteTerminology(T8DataTransaction tx, Collection<String> orgIDList, Collection<String> languageIDList, Collection<String> conceptIDList) throws Exception
    {
        String entityId;
        T8TerminologyCache terminologyCache;
        T8DataFilter filter;

        // Get the entity identifier.
        entityId = ORG_TERMINOLOGY_DE_IDENTIFIER;

        // Create the filter to identify the collection of terminology to delete.
        filter = new T8DataFilter(entityId);
        if (CollectionUtilities.hasContent(orgIDList)) filter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_ORG_ID, DataFilterOperator.IN, orgIDList, false);
        if (CollectionUtilities.hasContent(languageIDList)) filter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_LANGUAGE_ID, DataFilterOperator.IN, languageIDList, false);
        if (CollectionUtilities.hasContent(conceptIDList)) filter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_CONCEPT_ID, DataFilterOperator.IN, conceptIDList, false);

        // Delete the entities using the filter.
        tx.delete(entityId, filter);

        // Update the terminology cache.
        terminologyCache = tx.getTerminologyCache();
        if (!CollectionUtilities.isNullOrEmpty(languageIDList))
        {
            for (String languageID : languageIDList)
            {
                for (String conceptID : conceptIDList)
                {
                    terminologyCache.remove(languageID, conceptID);
                }
            }
        }
        else throw new Exception("Cannot delete terminology if no language ID's are specified.");
    }

    public void clearTerminologyCode(T8DataTransaction tx, Collection<String> orgIDList, Collection<String> odtIDList, Collection<String> conceptIDList) throws Exception
    {
        List<T8DataEntity> linkEntities;
        String terminologyEntityID;
        T8DataFilter filter;

        // Get the Organization Terminology entity identifier.
        terminologyEntityID = ORG_TERMINOLOGY_DE_IDENTIFIER;

        // Create the filter to identify the collection of terminology to clear the codes.
        filter = new T8DataFilter(terminologyEntityID);
        if (CollectionUtilities.hasContent(orgIDList)) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ORG_ID, DataFilterOperator.IN, orgIDList, false);
        if (CollectionUtilities.hasContent(conceptIDList)) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_CONCEPT_ID, DataFilterOperator.IN, conceptIDList, false);

        // Update all links identified by the filter to the terminology row.
        linkEntities = tx.select(terminologyEntityID, filter);
        for (T8DataEntity linkEntity : linkEntities)
        {
            linkEntity.setFieldValue(terminologyEntityID + EF_CODE_ID, null);
            linkEntity.setFieldValue(terminologyEntityID + EF_CODE, null);
            tx.update(linkEntity);
        }
    }

    /**
     * Returns all concept ID's in the specified organization's ontology that
     * match the specified code.  If the concept ODT_ID is supplied only
     * concepts from the specified group will be returned.
     *
     * @param tx The data transaction to use.
     * @param rootOrgId The ID of the root organization for which codes
     * will be searched.
     * @param orgId The organization to which the retrieval will be limited i.e.
     * only concepts linked to this organization will be matched using the
     * specified code.  If null, all concepts will be taken into account.
     * @param ocIdList The (optional) list of concept ODT_ID's to which the
     * search will be restricted.  If null, all groups will be searched.
     * @param code The code to match.
     * @return All concept ID's with a code that matches the specified
     * criteria.
     * @throws java.lang.Exception
     */
    public List<String> retrieveConceptIDsByCode(T8DataTransaction tx, String rootOrgId, String orgId, Collection<String> ocIdList, String code) throws Exception
    {
        String terminologyEntityID;
        List<T8DataEntity> dataEntities;
        List<String> conceptIDs;
        T8DataFilter filter;

        // Get the entity identifier.
        terminologyEntityID = ORG_ONTOLOGY_CONCEPT_CODE_DE_IDENTIFIER;

        // Create the filter to retrieve all DR Instance ID's belonging to the specified group and its descendants.
        filter = new T8DataFilter(terminologyEntityID);
        filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ROOT_ORG_ID, DataFilterOperator.EQUAL, rootOrgId, false);
        if (orgId != null) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ORG_ID, DataFilterOperator.EQUAL, orgId, false);
        if (ocIdList != null) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ODT_ID, DataFilterOperator.IN, ocIdList, false);
        filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_CODE, DataFilterOperator.EQUAL, code, false);

        // Select the entities using the filter.
        dataEntities = tx.select(terminologyEntityID, filter);

        // Get the selected concept ID's.
        conceptIDs = new ArrayList<>();
        for (T8DataEntity entity : dataEntities)
        {
            conceptIDs.add((String)entity.getFieldValue(terminologyEntityID + EF_CONCEPT_ID));
        }

        return conceptIDs;
    }

    /**
     * Returns all concept ID's in the specified organization's ontology that
     * match the specified term literal.  If the concept ODT_ID is supplied only
     * concepts from the specified group will be returned.
     *
     * @param tx The data transaction to use.
     * @param rootOrganizationID The ID of the root organization for which terms
     * will be searched.
     * @param orgID The organization to which the retrieval will be limited i.e.
     * only concepts linked to this organization will be matched using the
     * specified term.  If null, all concepts will be taken into account.
     * @param odtIDList The (optional) list of concept ODT_ID's to which the
     * search will be restricted.  If null, all groups will be searched.
     * @param term The term to match.
     * @return All concept ID's with a term that matches the specified
     * criteria.
     * @throws java.lang.Exception
     */
    public List<String> retrieveConceptIDsByTerm(T8DataTransaction tx, String rootOrganizationID, String orgID, Collection<String> odtIDList, String term) throws Exception
    {
        String terminologyEntityID;
        List<T8DataEntity> dataEntities;
        List<String> conceptIDs;
        T8DataFilter filter;

        // Get the entity identifier.
        terminologyEntityID = ORG_ONTOLOGY_CONCEPT_TERM_DE_IDENTIFIER;

        // Create the filter to retrieve all DR Instance ID's belonging to the specified group and its descendants.
        filter = new T8DataFilter(terminologyEntityID);
        filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ROOT_ORG_ID, DataFilterOperator.EQUAL, rootOrganizationID, false);
        if (orgID != null) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ORG_ID, DataFilterOperator.EQUAL, orgID, false);
        if (odtIDList != null) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ODT_ID, DataFilterOperator.IN, odtIDList, false);
        filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_TERM, DataFilterOperator.EQUAL, term, false);

        // Select the entities using the filter.
        dataEntities = tx.select(terminologyEntityID, filter);

        // Get the selected concept ID's.
        conceptIDs = new ArrayList<>();
        for (T8DataEntity entity : dataEntities)
        {
            conceptIDs.add((String)entity.getFieldValue(terminologyEntityID + EF_CONCEPT_ID));
        }

        return conceptIDs;
    }

    /**
     * Returns all concept ID's in the specified organization's ontology that
     * match the specified definition literal.  If the concept ODT_ID is
     * supplied only concepts from the specified group will be returned.
     *
     * @param tx The data transaction to use.
     * @param rootOrganizationID The ID of the root organization for which
     * definitions will be searched.
     * @param orgID The organization to which the retrieval will be limited i.e.
     * only concepts linked to this organization will be matched using the
     * specified definition.  If null, all concepts will be taken into account.
     * @param odtIDList The (optional) list of concept ODT_ID's to which the
     * search will be restricted.  If null, all groups will be searched.
     * @param conceptDefinition The definition to match.
     * @return All concept ID's with a definition that matches the specified
     * criteria.
     * @throws java.lang.Exception
     */
    public List<String> retrieveConceptIDsByDefinition(T8DataTransaction tx, String rootOrganizationID, String orgID, Collection<String> odtIDList, String conceptDefinition) throws Exception
    {
        String terminologyEntityID;
        List<T8DataEntity> dataEntities;
        List<String> conceptIDs;
        T8DataFilter filter;

        // Get the entity identifier.
        terminologyEntityID = ORG_ONTOLOGY_CONCEPT_DEFINITION_DE_IDENTIFIER;

        // Create the filter to retrieve all DR Instance ID's belonging to the specified group and its descendants.
        filter = new T8DataFilter(terminologyEntityID);
        filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ROOT_ORG_ID, DataFilterOperator.EQUAL, rootOrganizationID, false);
        if (orgID != null) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ORG_ID, DataFilterOperator.EQUAL, orgID, false);
        if (odtIDList != null) filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_ODT_ID, DataFilterOperator.IN, odtIDList, false);
        filter.addFilterCriterion(DataFilterConjunction.AND, terminologyEntityID + EF_DEFINITION, DataFilterOperator.EQUAL, conceptDefinition, false);

        // Select the entities using the filter.
        dataEntities = tx.select(terminologyEntityID, filter);

        // Get the selected concept ID's.
        conceptIDs = new ArrayList<>();
        for (T8DataEntity entity : dataEntities)
        {
            conceptIDs.add((String)entity.getFieldValue(terminologyEntityID + EF_CONCEPT_ID));
        }

        return conceptIDs;
    }

    public List<T8OntologyLink> retrieveOntologyLinks(T8DataTransaction tx, String rootOrgId, String osId, String conceptId) throws Exception
    {
        List<T8OntologyLink> linkList;
        List<T8DataEntity> linkEntities;
        Map<String, Object> filterMap;
        String entityIdentifier;

        // Create the filter and retrieve the link entities.
        linkList = new ArrayList<>();
        entityIdentifier = ORG_ONTOLOGY_DE_IDENTIFIER;
        filterMap = new HashMap<String, Object>();
        // This columns is not yet available in ORG_ONTOLOGY but will be in future.  filterMap.put(entityIdentifier + EF_ROOT_ORG_ID, filterRootOrgId);
        if (osId != null) filterMap.put(entityIdentifier + EF_ODS_ID, osId);
        filterMap.put(entityIdentifier + EF_CONCEPT_ID, conceptId);
        linkEntities = tx.select(entityIdentifier, filterMap);
        for (T8DataEntity linkEntity : linkEntities)
        {
            String linkedConceptID;
            String linkOrgId;
            String linkOsId;
            String linkOcId;

            // Get the field values from the entity.
            linkOrgId = (String)linkEntity.getFieldValue(entityIdentifier + EF_ORG_ID);
            linkOsId = (String)linkEntity.getFieldValue(entityIdentifier + EF_ODS_ID);
            linkOcId = (String)linkEntity.getFieldValue(entityIdentifier + EF_ODT_ID);
            linkedConceptID = (String)linkEntity.getFieldValue(entityIdentifier + EF_CONCEPT_ID);

            // Add a newly constructed link object to the list that will be returned.
            linkList.add(new T8OntologyLink(linkOrgId, linkOsId, linkOcId, linkedConceptID));
        }

        // Return the final list of concept links.
        return linkList;
    }

    public List<T8OntologyLink> retrieveOntologyLinks(T8DataTransaction tx, String rootOrgId, String osId, Collection<String> conceptIds) throws Exception
    {
        List<T8OntologyLink> linkList;
        List<T8DataEntity> linkEntities;
        T8DataFilter filter;
        String entityId;

        // Create the filter and retrieve the link entities.
        linkList = new ArrayList<>();
        entityId = ORG_ONTOLOGY_DE_IDENTIFIER;
        filter = new T8DataFilter(entityId);
        if (osId != null) filter.addFilterCriterion(entityId + EF_ODS_ID, osId);
        filter.addFilterCriteria(DataFilterConjunction.AND, entityId + EF_CONCEPT_ID, conceptIds);
        linkEntities = tx.select(entityId, filter);
        for (T8DataEntity linkEntity : linkEntities)
        {
            String linkedConceptID;
            String linkOrgId;
            String linkOsId;
            String linkOcId;

            // Get the field values from the entity.
            linkOrgId = (String)linkEntity.getFieldValue(entityId + EF_ORG_ID);
            linkOsId = (String)linkEntity.getFieldValue(entityId + EF_ODS_ID);
            linkOcId = (String)linkEntity.getFieldValue(entityId + EF_ODT_ID);
            linkedConceptID = (String)linkEntity.getFieldValue(entityId + EF_CONCEPT_ID);

            // Add a newly constructed link object to the list that will be returned.
            linkList.add(new T8OntologyLink(linkOrgId, linkOsId, linkOcId, linkedConceptID));
        }

        // Return the final list of concept links.
        return linkList;
    }

    public List<T8OntologyConcept> retrieveConcepts(T8DataTransaction tx, String rootOrganizationID, Collection<String> conceptIDList, Collection<String> languageIDList, boolean includeOrganizationLinks, boolean includeTerms, boolean includeAbbreviations, boolean includeDefinitions, boolean includeCodes) throws Exception
    {
        List<T8OntologyConcept> ontologyConcepts;
        List<T8DataEntity> concepEntities;
        String entityIdentifier;
        T8DataFilter filter;

        // Create the filter that will be used to identify all concept to be retrieved.
        entityIdentifier = ONTOLOGY_CONCEPT_DE_IDENTIFIER;
        filter = new T8DataFilter(entityIdentifier);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_ID, DataFilterOperator.IN, conceptIDList, false);

        // Retrieve the concept data and construct the result list from it.
        stats.logExecutionStart("CONCEPT_RETRIEVAL");
        ontologyConcepts = new ArrayList<>();
        concepEntities = tx.select(entityIdentifier, filter);
        for (T8DataEntity conceptEntity : concepEntities)
        {
            T8OntologyConcept ontologyConcept;

            // Construct the concept object.
            ontologyConcept = constructOntologyConcept(conceptEntity);

            // Add the concept to the retrieved collection.
            ontologyConcepts.add(ontologyConcept);
        }
        stats.logExecutionEnd("CONCEPT_RETRIEVAL");

        // Add the organization links if required.
        if (includeOrganizationLinks)
        {
            List<T8OntologyLink> organizationLinks;

            // Retrieve the links and then add them to the correct concepts.
            organizationLinks = retrieveOrganizationConceptLinks(tx, conceptIDList);
            for (T8OntologyLink organizationLink : organizationLinks)
            {
                for (T8OntologyConcept ontologyConcept : ontologyConcepts)
                {
                    if (organizationLink.getConceptID().equals(ontologyConcept.getID()))
                    {
                        ontologyConcept.addOntologyLink(organizationLink);
                        break;
                    }
                }
            }
        }

        // Add the terms if required.
        if (includeTerms)
        {
            List<T8OntologyTerm> ontologyTerms;

            // Retrieve the terms and then add them to the correct concepts.
            ontologyTerms = retrieveConceptTerms(tx, rootOrganizationID, conceptIDList, languageIDList, includeAbbreviations);
            for (T8OntologyTerm ontologyTerm : ontologyTerms)
            {
                for (T8OntologyConcept ontologyConcept : ontologyConcepts)
                {
                    if (ontologyTerm.getConceptID().equals(ontologyConcept.getID()))
                    {
                        ontologyConcept.addTerm(ontologyTerm);
                        break;
                    }
                }
            }
        }

        // Add the definitions if required.
        if (includeDefinitions)
        {
            List<T8OntologyDefinition> ontologyDefinitions;

            // Retrieve the definitions and then add them to the correct concepts.
            ontologyDefinitions = retrieveConceptDefinitions(tx, rootOrganizationID, conceptIDList, languageIDList);
            for (T8OntologyDefinition ontologyDefinition : ontologyDefinitions)
            {
                for (T8OntologyConcept ontologyConcept : ontologyConcepts)
                {
                    if (ontologyDefinition.getConceptID().equals(ontologyConcept.getID()))
                    {
                        ontologyConcept.addDefinition(ontologyDefinition);
                        break;
                    }
                }
            }
        }

        // Add the codes if required.
        if (includeCodes)
        {
            List<T8OntologyCode> ontologyCodes;

            // Retrieve the codes and then add them to the correct concepts.
            ontologyCodes = retrieveConceptCodes(tx, rootOrganizationID, conceptIDList);
            for (T8OntologyCode ontologyCode : ontologyCodes)
            {
                for (T8OntologyConcept ontologyConcept : ontologyConcepts)
                {
                    if (ontologyCode.getConceptID().equals(ontologyConcept.getID()))
                    {
                        ontologyConcept.addCode(ontologyCode);
                        break;
                    }
                }
            }
        }

        // Return the retrieved collection.
        return ontologyConcepts;
    }

    public List<T8OntologyLink> retrieveOrganizationConceptLinks(T8DataTransaction tx, Collection<String> conceptIDList) throws Exception
    {
        List<T8OntologyLink> linkList;
        List<T8DataEntity> linkEntities;
        String entityIdentifier;
        T8DataFilter filter;

        // Create the filter that will be used to identify the concept link to retrieve.
        entityIdentifier = ORG_ONTOLOGY_DE_IDENTIFIER;
        filter = new T8DataFilter(entityIdentifier);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_ID, DataFilterOperator.IN, conceptIDList, false);
        // TODO:  Add filter on ROOT_ORG_ID as soon as it has been added to ORG_ONTOLOGY table.

        // Create the filter and retrieve the link entities.
        stats.logExecutionStart("ORGANIZATION_LINK_RETRIEVAL");
        linkList = new ArrayList<>();
        linkEntities = tx.select(entityIdentifier, filter);
        for (T8DataEntity linkEntity : linkEntities)
        {
            String orgID;
            String odsID;
            String odtID;
            String linkedConceptID;

            // Get the field values from the entity.
            orgID = (String)linkEntity.getFieldValue(entityIdentifier + EF_ORG_ID);
            odsID = (String)linkEntity.getFieldValue(entityIdentifier + EF_ODS_ID);
            odtID = (String)linkEntity.getFieldValue(entityIdentifier + EF_ODT_ID);
            linkedConceptID = (String)linkEntity.getFieldValue(entityIdentifier + EF_CONCEPT_ID);

            // Add a newly constructed link object to the list that will be returned.
            linkList.add(new T8OntologyLink(orgID, odsID, odtID, linkedConceptID));
        }
        stats.logExecutionEnd("ORGANIZATION_LINK_RETRIEVAL");

        // Return the final list of concept links.
        return linkList;
    }

    public List<T8OntologyTerm> retrieveConceptTerms(T8DataTransaction tx, String rootOrganizationID, Collection<String> conceptIDList, Collection<String> languageIDList, boolean includeAbbreviations) throws Exception
    {
        List<T8OntologyTerm> ontologyTerms;
        List<T8DataEntity> termEntities;
        List<String> termIDList;
        String entityIdentifier;
        T8DataFilter filter;

        // Create the filter that will be used to identify all relationships to be deleted.
        entityIdentifier = ONTOLOGY_TERM_DE_IDENTIFIER;
        filter = new T8DataFilter(entityIdentifier);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_ID, DataFilterOperator.IN, conceptIDList, false);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_LANGUAGE_ID, DataFilterOperator.IN, languageIDList, false);

        // Retrieve the term data and construct the result list from it.
        stats.logExecutionStart("TERM_RETRIEVAL");
        termIDList = new ArrayList<String>();
        ontologyTerms = new ArrayList<>();
        termEntities = tx.select(entityIdentifier, filter);
        for (T8DataEntity termEntity : termEntities)
        {
            T8OntologyTerm ontologyTerm;

            // Construct the term object.
            ontologyTerm = constructOntologyTerm(termEntity);

            // Add the term to the retrieved collection.
            ontologyTerms.add(ontologyTerm);
            termIDList.add(ontologyTerm.getID());
        }
        stats.logExecutionEnd("TERM_RETRIEVAL");

        // Add the term abbreviations if required.
        if (includeAbbreviations)
        {
            List<T8OntologyAbbreviation> ontologyAbbreviations;

            // Retrieve the abbreviations and then add them to the correct terms.
            ontologyAbbreviations = retrieveTermAbbreviations(tx, rootOrganizationID, termIDList);
            for (T8OntologyAbbreviation ontologyAbbreviation : ontologyAbbreviations)
            {
                for (T8OntologyTerm ontologyTerm : ontologyTerms)
                {
                    if (ontologyAbbreviation.getTermID().equals(ontologyTerm.getID()))
                    {
                        ontologyTerm.addAbbreviation(ontologyAbbreviation);
                        break;
                    }
                }
            }
        }

        // Return the retrieved collection.
        return ontologyTerms;
    }

    public List<T8OntologyDefinition> retrieveConceptDefinitions(T8DataTransaction tx, String rootOrganizationID, Collection<String> conceptIDList, Collection<String> languageIDList) throws Exception
    {
        List<T8OntologyDefinition> ontologyDefinitions;
        List<T8DataEntity> definitionEntities;
        String entityIdentifier;
        T8DataFilter filter;

        // Create the filter that will be used to identify all definitions to be retrieved.
        entityIdentifier = ONTOLOGY_DEFINITION_DE_IDENTIFIER;
        filter = new T8DataFilter(entityIdentifier);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_ID, DataFilterOperator.IN, conceptIDList, false);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_LANGUAGE_ID, DataFilterOperator.IN, languageIDList, false);

        // Retrieve the definition data and construct the result list from it.
        stats.logExecutionStart("DEFINITION_RETRIEVAL");
        ontologyDefinitions = new ArrayList<>();
        definitionEntities = tx.select(entityIdentifier, filter);
        for (T8DataEntity definitionEntity : definitionEntities)
        {
            T8OntologyDefinition ontologyDefinition;

            // Construct the definition object.
            ontologyDefinition = constructOntologyDefinition(definitionEntity);

            // Add the definition to the retrieved collection.
            ontologyDefinitions.add(ontologyDefinition);
        }
        stats.logExecutionEnd("DEFINITION_RETRIEVAL");

        // Return the retrieved collection.
        return ontologyDefinitions;
    }

    public List<T8OntologyAbbreviation> retrieveTermAbbreviations(T8DataTransaction tx, String rootOrganizationID, Collection<String> termIDList) throws Exception
    {
        List<T8DataEntity> abbreviationEntities;
        List<T8OntologyAbbreviation> ontologyAbbreviations;
        String entityIdentifier;
        T8DataFilter filter;

        // Create the filter that will be used to identify the abbreviations to be retrieved.
        entityIdentifier = ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;
        filter = new T8DataFilter(entityIdentifier);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_TERM_ID, DataFilterOperator.IN, termIDList, false);

        // Retrieve the abbreviation data and construc the object list from it.
        stats.logExecutionStart("ABBREVIATION_RETRIEVAL");
        ontologyAbbreviations = new ArrayList<>();
        abbreviationEntities = tx.select(entityIdentifier, filter);
        for (T8DataEntity abbreviationEntity : abbreviationEntities)
        {
            T8OntologyAbbreviation ontologyAbbreviation;

            // Construct the abbreviation object.
            ontologyAbbreviation = constructOntologyAbbreviation(abbreviationEntity);

            // Add the abbreviation to the retrieved collection.
            ontologyAbbreviations.add(ontologyAbbreviation);
        }
        stats.logExecutionEnd("ABBREVIATION_RETRIEVAL");

        // Return the retrieved collection.
        return ontologyAbbreviations;
    }

    public List<T8OntologyCode> retrieveConceptCodes(T8DataTransaction tx, String rootOrganizationID, Collection<String> conceptIDList) throws Exception
    {
        List<T8OntologyCode> ontologyCodes;
        List<T8DataEntity> codeEntities;
        String entityIdentifier;
        T8DataFilter filter;

        // Create the filter that will be used to identify all codes to be retrieved.
        entityIdentifier = ONTOLOGY_CODE_DE_IDENTIFIER;
        filter = new T8DataFilter(entityIdentifier);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_ID, DataFilterOperator.IN, conceptIDList, false);

        // Retrieve the definition data and construct the result list from it.
        stats.logExecutionStart("CODE_RETRIEVAL");
        ontologyCodes = new ArrayList<>();
        codeEntities = tx.select(entityIdentifier, filter);
        for (T8DataEntity codeEntity : codeEntities)
        {
            T8OntologyCode ontologyCode;

            // Construct the code object.
            ontologyCode = constructOntologyCode(codeEntity);

            // Add the code to the retrieved collection.
            ontologyCodes.add(ontologyCode);
        }
        stats.logExecutionEnd("CODE_RETRIEVAL");

        // Return the retrieved collection.
        return ontologyCodes;
    }

    private T8OntologyConcept constructOntologyConcept(T8DataEntity conceptEntity)
    {
        String entityIdentifier;
        String conceptID;
        String conceptTypeID;
        boolean standardIndicator;
        boolean activeIndicator;

        // Get required the entity field values.
        entityIdentifier = conceptEntity.getIdentifier();
        conceptID = (String)conceptEntity.getFieldValue(entityIdentifier + EF_CONCEPT_ID);
        conceptTypeID = (String)conceptEntity.getFieldValue(entityIdentifier + EF_CONCEPT_TYPE_ID);
        activeIndicator = "Y".equalsIgnoreCase((String)conceptEntity.getFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR));
        standardIndicator = "Y".equalsIgnoreCase((String)conceptEntity.getFieldValue(entityIdentifier + EF_STANDARD_INDICATOR));

        // Construct the concept object and return it.
        return new T8OntologyConcept(conceptID, T8OntologyConceptType.getConceptType(conceptTypeID), standardIndicator, activeIndicator);
    }

    private T8OntologyTerm constructOntologyTerm(T8DataEntity termEntity)
    {
        T8OntologyTerm ontologyTerm;
        String entityIdentifier;
        String conceptID;
        String termID;
        String languageID;
        String term;
        boolean standardIndicator;
        boolean activeIndicator;

        // Get required the entity field values.
        entityIdentifier = termEntity.getIdentifier();
        termID = (String)termEntity.getFieldValue(entityIdentifier + EF_TERM_ID);
        conceptID = (String)termEntity.getFieldValue(entityIdentifier + EF_CONCEPT_ID);
        languageID = (String)termEntity.getFieldValue(entityIdentifier + EF_LANGUAGE_ID);
        term = (String)termEntity.getFieldValue(entityIdentifier + EF_TERM);
        activeIndicator = "Y".equalsIgnoreCase((String)termEntity.getFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR));
        standardIndicator = "Y".equalsIgnoreCase((String)termEntity.getFieldValue(entityIdentifier + EF_STANDARD_INDICATOR));

        // Return the newly constructed term object.
        ontologyTerm = new T8OntologyTerm(termID, conceptID, languageID, term, standardIndicator, activeIndicator);
        return ontologyTerm;
    }

    private T8OntologyDefinition constructOntologyDefinition(T8DataEntity definitionEntity)
    {
        T8OntologyDefinition ontologyDefinition;
        String entityId;
        String definitionId;
        String conceptId;
        String languageId;
        String definition;
        boolean standardIndicator;
        boolean activeIndicator;

        // Get required the entity field values.
        entityId = definitionEntity.getIdentifier();
        definitionId = (String)definitionEntity.getFieldValue(entityId + EF_DEFINITION_ID);
        conceptId = (String)definitionEntity.getFieldValue(entityId + EF_CONCEPT_ID);
        definition = (String)definitionEntity.getFieldValue(entityId + EF_DEFINITION);
        languageId = (String)definitionEntity.getFieldValue(entityId + EF_LANGUAGE_ID);
        activeIndicator = "Y".equalsIgnoreCase((String)definitionEntity.getFieldValue(entityId + EF_ACTIVE_INDICATOR));
        standardIndicator = "Y".equalsIgnoreCase((String)definitionEntity.getFieldValue(entityId + EF_STANDARD_INDICATOR));

        // Construct the definition object.
        ontologyDefinition = new T8OntologyDefinition(definitionId, conceptId, languageId, definition, standardIndicator, activeIndicator);
        return ontologyDefinition;
    }

    private T8OntologyAbbreviation constructOntologyAbbreviation(T8DataEntity abbreviationEntity)
    {
        T8OntologyAbbreviation ontologyAbbreviation;
        String entityIdentifier;
        String abbreviationId;
        String termId;
        String abbreviation;
        boolean standardIndicator;
        boolean activeIndicator;

        // Get required the entity field values.
        entityIdentifier = abbreviationEntity.getIdentifier();
        abbreviationId = (String)abbreviationEntity.getFieldValue(entityIdentifier + EF_ABBREVIATION_ID);
        termId = (String)abbreviationEntity.getFieldValue(entityIdentifier + EF_TERM_ID);
        abbreviation = (String)abbreviationEntity.getFieldValue(entityIdentifier + EF_ABBREVIATION);
        activeIndicator = "Y".equalsIgnoreCase((String)abbreviationEntity.getFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR));
        standardIndicator = "Y".equalsIgnoreCase((String)abbreviationEntity.getFieldValue(entityIdentifier + EF_STANDARD_INDICATOR));

        // Construct the abbreviation object.
        ontologyAbbreviation = new T8OntologyAbbreviation(abbreviationId, termId, abbreviation, standardIndicator, activeIndicator);
        return ontologyAbbreviation;
    }

    private T8OntologyCode constructOntologyCode(T8DataEntity codeEntity)
    {
        T8OntologyCode ontologyCode;
        String entityIdentifier;
        String codeId;
        String codeTypeId;
        String conceptId;
        String code;
        boolean standardIndicator;
        boolean activeIndicator;

        // Get required the entity field values.
        entityIdentifier = codeEntity.getIdentifier();
        codeId = (String)codeEntity.getFieldValue(entityIdentifier + EF_CODE_ID);
        codeTypeId = (String)codeEntity.getFieldValue(entityIdentifier + EF_CODE_TYPE_ID);
        conceptId = (String)codeEntity.getFieldValue(entityIdentifier + EF_CONCEPT_ID);
        code = (String)codeEntity.getFieldValue(entityIdentifier + EF_CODE);
        activeIndicator = "Y".equalsIgnoreCase((String)codeEntity.getFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR));
        standardIndicator = "Y".equalsIgnoreCase((String)codeEntity.getFieldValue(entityIdentifier + EF_STANDARD_INDICATOR));

        // Construct the code object.
        ontologyCode = new T8OntologyCode(codeId, conceptId, codeTypeId, code, standardIndicator, activeIndicator);
        return ontologyCode;
    }

    private T8DataEntity constructTerminologyDataEntity(T8DataTransaction tx, T8ConceptTerminology terminology, boolean deletionEnabled) throws Exception
    {
        T8DataEntity entity;
        String entityIdentifier;
        String orgId;

        // Get the required information to construct the entity.
        orgId = terminology.getOrganizationId();
        entityIdentifier = ORG_TERMINOLOGY_DE_IDENTIFIER;

        // Create the new terminology entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ORG_ID, orgId);
        entity.setFieldValue(entityIdentifier + EF_LANGUAGE_ID, terminology.getLanguageId());
        entity.setFieldValue(entityIdentifier + EF_CONCEPT_ID, terminology.getConceptId());
        entity.setFieldValue(entityIdentifier + EF_CONCEPT_TYPE_ID, terminology.getConceptType().getConceptTypeID());

        // Add the term data if available.
        if ((deletionEnabled) || (!Strings.isNullOrEmpty(terminology.getTermId())))
        {
            entity.setFieldValue(entityIdentifier + EF_TERM_ID, terminology.getTermId());
            entity.setFieldValue(entityIdentifier + EF_TERM, terminology.getTerm());
        }

        // Add the definition data if available.
        if ((deletionEnabled) || (!Strings.isNullOrEmpty(terminology.getDefinitionId())))
        {
            entity.setFieldValue(entityIdentifier + EF_DEFINITION_ID, terminology.getDefinitionId());
            entity.setFieldValue(entityIdentifier + EF_DEFINITION, terminology.getDefinition());
        }

        // Add the abbreviation data if available.
        if ((deletionEnabled) || (!Strings.isNullOrEmpty(terminology.getAbbreviationId())))
        {
            entity.setFieldValue(entityIdentifier + EF_ABBREVIATION_ID, terminology.getAbbreviationId());
            entity.setFieldValue(entityIdentifier + EF_ABBREVIATION, terminology.getAbbreviation());
        }

        // Add the code data if available.
        if ((deletionEnabled) || (!Strings.isNullOrEmpty(terminology.getCodeId())))
        {
            entity.setFieldValue(entityIdentifier + EF_CODE_ID, terminology.getCodeId());
            entity.setFieldValue(entityIdentifier + EF_CODE, terminology.getCode());
        }

        return entity;
    }
}
