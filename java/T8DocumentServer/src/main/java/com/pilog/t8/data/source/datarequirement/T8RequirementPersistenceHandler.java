package com.pilog.t8.data.source.datarequirement;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.cache.T8DataRequirementCache;
import com.pilog.t8.cache.T8DataRequirementInstanceCache;
import com.pilog.t8.cache.T8DataRequirementInstanceCache.DataRequirementInstanceData;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.data.resultset.extractor.BigDecimalToIntegerConverter;
import com.pilog.t8.utilities.data.resultset.extractor.GUIDBytesToStringConverter;
import com.pilog.t8.utilities.data.resultset.extractor.GUIDStringConverter;
import com.pilog.t8.utilities.data.resultset.extractor.ResultSetDataExtractor;
import com.pilog.t8.utilities.strings.Strings;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8RequirementPersistenceHandler implements T8PerformanceStatisticsProvider
{
    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8DataRequirementCache drCache;
    private final T8DataRequirementInstanceCache drInstanceCache;
    private T8PerformanceStatistics stats;

    private static final ResultSetDataExtractor dataExtractor = new ResultSetDataExtractor(new BigDecimalToIntegerConverter(), new GUIDBytesToStringConverter(), new GUIDStringConverter());

    public T8RequirementPersistenceHandler(T8DataTransaction tx)
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.drCache = tx.getDataRequirementCache();
        this.drInstanceCache = tx.getDataRequirementInstanceCache();
        this.stats = tx.getPerformanceStatistics();
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public final DataRequirementInstance retrieveDataRequirementInstance(T8DataConnection connection, String drInstanceId) throws Exception
    {
        List<Map<String, Object>> dataRequirementInstanceData;
        DataRequirementInstanceData drInstanceData;

        // Fetch the data requirement instance data.
        stats.logExecutionStart("SELECT_DATA_REQUIREMENT_INSTANCE");
        drInstanceData = drInstanceCache.get(drInstanceId);
        if (drInstanceData == null)
        {
            // Retrieve the data from the database.
            dataRequirementInstanceData = selectDataRequirementInstanceData(connection, drInstanceId);
            if (dataRequirementInstanceData.size() > 0)
            {
                String independentIndicator;
                String recordOcId;
                String drId;

                drId = (String)dataRequirementInstanceData.get(0).get("DR_ID");
                recordOcId = (String)dataRequirementInstanceData.get(0).get("RECORD_OC_ID");
                independentIndicator = (String)dataRequirementInstanceData.get(0).get("INDEPENDENT_INDICATOR");
                drInstanceData = new DataRequirementInstanceData(drInstanceId, drId, "Y".equalsIgnoreCase(independentIndicator), recordOcId);
                drInstanceCache.put(drInstanceData);
            }
        }
        stats.logExecutionEnd("SELECT_DATA_REQUIREMENT_INSTANCE");

        // Use the retrieved data to create a Data Requirement Instance object.
        if (drInstanceData != null)
        {
            DataRequirement dataRequirement;

            stats.logExecutionStart("RETRIEVE_DATA_REQUIREMENT");
            dataRequirement = retrieveDataRequirement(connection, drInstanceData.getDrId());
            stats.logExecutionEnd("RETRIEVE_DATA_REQUIREMENT");
            if (dataRequirement != null)
            {
                DataRequirementInstance drInstance;

                drInstance = new DataRequirementInstance(drInstanceId, dataRequirement);
                drInstance.setIndependent(drInstanceData.isIndependentIndicator());
                drInstance.setRecordOcId(drInstanceData.getRecordOntologyClass());

                return drInstance;
            }
            else return null;
        }
        else
        {
            T8Log.log("Data Requirement Instance not found: " + drInstanceId);
            return null;
        }
    }

    public final void insertDataRequirementInstance(T8DataTransaction tx, T8DataConnection connection, DataRequirementInstance drInstance) throws Exception
    {
        List<Map<String, Object>> newDRInstanceData;
        DataRequirementInstanceData drInstanceData;

        // Get all the new data from the data requirement instance.
        newDRInstanceData = T8RequirementDataHandler.getDataRequirementInstanceDataRows(drInstance);

        // Insert the data requirement data rows.
        insertDataRequirementInstanceData(tx, connection, newDRInstanceData);

        // Cache the new data.
        drInstanceData = new DataRequirementInstanceData(drInstance.getId(), drInstance.getDataRequirement().getConceptID(), drInstance.isIndependent(), drInstance.getRecordOcId());
        drInstanceCache.put(drInstanceData);
    }

    public final void updateDataRequirementInstance(T8DataTransaction dataAccessProvider, T8DataConnection connection, DataRequirementInstance drInstance) throws Exception
    {
        List<Map<String, Object>> oldDRInstanceData;
        List<Map<String, Object>> newDRInstanceData;
        DataRequirementInstanceData drInstanceData;

        // Get all existing data from the database.
        oldDRInstanceData = selectDataRequirementInstanceData(connection, drInstance.getConceptID());

        // Get all the new data from the data requirement instance.
        newDRInstanceData = T8RequirementDataHandler.getDataRequirementInstanceDataRows(drInstance);

        // Update the data requirement data row.
        if (oldDRInstanceData.size() > 0)
        {
            updateDataRequirementInstanceData(dataAccessProvider, connection, newDRInstanceData);
        }
        else // No data requirement to update, so insert the data.
        {
            insertDataRequirementInstanceData(dataAccessProvider, connection, newDRInstanceData);
        }

        // Cache the new data.
        drInstanceData = new DataRequirementInstanceData(drInstance.getId(), drInstance.getDataRequirement().getConceptID(), drInstance.isIndependent(), drInstance.getRecordOcId());
        drInstanceCache.put(drInstanceData);
    }

    public final void deleteDataRequirementInstance(T8DataConnection connection, String drInstanceId) throws Exception
    {
        List<Map<String, Object>> drInstanceData;

        // Get all existing value data from the database.
        drInstanceData = selectDataRequirementInstanceData(connection, drInstanceId);
        if (drInstanceData.size() > 0)
        {
            deleteDataRequirementInstanceData(connection, drInstanceData);
            drInstanceCache.remove(drInstanceId);
        }
    }

    public final DataRequirement retrieveDataRequirement(T8DataConnection connection, String requirementID) throws Exception
    {
        if (drCache.containsKey(requirementID))
        {
            return (DataRequirement)drCache.get(requirementID);
        }
        else
        {
            List<Map<String, Object>> dataRequirementData;
            List<Map<String, Object>> dataRequirementAttributeData;
            List<Map<String, Object>> propertyRequirementData;
            List<Map<String, Object>> valueRequirementData;

            // Get all existing data from the database.
            dataRequirementData = selectDataRequirementData(connection, requirementID);
            dataRequirementAttributeData = selectDataRequirementAttributeData(connection, requirementID);
            propertyRequirementData = selectPropertyRequirementData(connection, requirementID);
            valueRequirementData = selectValueRequirementData(connection, requirementID);

            if (dataRequirementData.size() > 0)
            {
                DataRequirement dataRequirement;
                Date insertedAt;
                Date updatedAt;

                // Check auditing fields.
                insertedAt = (Date)dataRequirementData.get(0).get("INSERTED_AT");
                updatedAt = (Date)dataRequirementData.get(0).get("UPDATED_AT");

                // Create the new Data Requirement.
                dataRequirement = new DataRequirement();
                dataRequirement.setConceptID(requirementID);
                dataRequirement.setClassConceptID((String)dataRequirementData.get(0).get("CLASS_ID"));
                dataRequirement.setInsertedByID((String)dataRequirementData.get(0).get("INSERTED_BY_ID"));
                dataRequirement.setInsertedAt(insertedAt != null ? new T8Timestamp(insertedAt.getTime()) : null);
                dataRequirement.setUpdatedByID((String)dataRequirementData.get(0).get("UPDATED_BY_ID"));
                dataRequirement.setUpdatedAt(updatedAt != null ? new T8Timestamp(updatedAt.getTime()) : null);

                // Add all Property Requirements.
                for (Map<String, Object> propertyDataRow : propertyRequirementData)
                {
                    PropertyRequirement propertyRequirement;
                    SectionRequirement sectionRequirement;
                    String attributeString;
                    String sectionID;
                    String propertyID;
                    boolean characteristic;

                    // Get the required parameters from the data row.
                    sectionID = (String)propertyDataRow.get("SECTION_ID");
                    propertyID = (String)propertyDataRow.get("PROPERTY_ID");
                    characteristic = "Y".equalsIgnoreCase((String)propertyDataRow.get("REQUIRED"));
                    attributeString = (String)propertyDataRow.get("ATTRIBUTES");

                    // Add the Section Requirement to the Data Requirement if it does not already exist.
                    sectionRequirement = dataRequirement.getSectionRequirement(sectionID);
                    if (sectionRequirement == null)
                    {
                        sectionRequirement = new SectionRequirement();
                        sectionRequirement.setConceptID(sectionID);
                        dataRequirement.addSectionRequirement(sectionRequirement);
                    }

                    // Construct the section property.
                    propertyRequirement = new PropertyRequirement();
                    propertyRequirement.setConceptID(propertyID);
                    propertyRequirement.setCharacteristic(characteristic);
                    propertyRequirement.setAttributes(Strings.isNullOrEmpty(attributeString) ? null : T8RequirementDataHandler.getAttributeMap(attributeString));
                    sectionRequirement.addPropertyRequirement(propertyRequirement);

                    // Construct the property value.
                    propertyRequirement.setValueRequirement(buildPropertyValueRequirement(valueRequirementData, propertyRequirement));
                }

                // Add the Data Requirement attributes.
                for (Map<String, Object> attributeDataRow : dataRequirementAttributeData)
                {
                    String attributeID;
                    String type;
                    String value;

                    attributeID = (String)attributeDataRow.get("ATTRIBUTE_ID");
                    value = (String)attributeDataRow.get("VALUE");
                    type = (String)attributeDataRow.get("TYPE");

                    if (type == null)
                    {
                        dataRequirement.setAttribute(attributeID, value);
                    }
                    else
                    {
                        switch (type)
                        {
                            case "NUMBER":
                                dataRequirement.setAttribute(attributeID, new BigDecimal(value));
                                break;
                            case "BOOLEAN":
                                dataRequirement.setAttribute(attributeID, Boolean.parseBoolean(value));
                                break;
                            default:
                                dataRequirement.setAttribute(attributeID, value);
                        }
                    }
                }

                // Update the cache.
                drCache.put(dataRequirement);

                // Return the result.
                return dataRequirement;
            }
            else
            {
                T8Log.log("Data Requirement not found: " + requirementID);
                return null;
            }
        }
    }

    private ValueRequirement buildPropertyValueRequirement(List<Map<String, Object>> valueRequirementData, PropertyRequirement propertyRequirement)
    {
        ArrayList<Map<String, Object>> dataRows;
        ArrayList<ValueRequirement> valueRequirements;
        HashMap<String, Object> filterValues;

        filterValues = new HashMap<>();
        filterValues.put("SECTION_ID", propertyRequirement.getParentSectionRequirement().getConceptID());
        filterValues.put("PROPERTY_ID", propertyRequirement.getConceptID());
        dataRows = CollectionUtilities.getFilteredDataRows((List)valueRequirementData, filterValues);

        valueRequirements = buildValueRequirements(dataRows, null, null);
        if (valueRequirements.size() > 0)
        {
            return valueRequirements.get(0);
        }
        else return null;
    }

    private ArrayList<ValueRequirement> buildValueRequirements(List<Map<String, Object>> valueRequirementData, RequirementType parentRequirementType, Integer parentDataTypeSequence)
    {
        ArrayList<ValueRequirement> valueRequirements;
        ArrayList<Map<String, Object>> dataRows;
        HashMap<String, Object> filterValues;

        filterValues = new HashMap<>();
        filterValues.put("PARENT_DATA_TYPE_ID", parentRequirementType != null ? parentRequirementType.getID() : null);
        filterValues.put("PARENT_DATA_TYPE_SEQ", parentDataTypeSequence);
        dataRows = CollectionUtilities.getFilteredDataRows(valueRequirementData, filterValues);

        valueRequirements = new ArrayList<>();
        for (Map<String, Object> dataRow : dataRows)
        {
            ValueRequirement valueRequirement;
            String requirementTypeID;
            RequirementType requirementType;
            int dataTypeSequence;
            String attributeString;
            String value;

            requirementTypeID = (String)dataRow.get("DATA_TYPE_ID");
            dataTypeSequence = (Integer)dataRow.get("DATA_TYPE_SEQUENCE");
            value = (String)dataRow.get("VALUE");
            attributeString = (String)dataRow.get("ATTRIBUTES");

            requirementType = RequirementType.getRequirementType(requirementTypeID);
            valueRequirement = ValueRequirement.createValueRequirement(requirementType);
            valueRequirement.setValue(value);
            valueRequirement.setAttributes(Strings.isNullOrEmpty(attributeString) ? null : T8RequirementDataHandler.getAttributeMap(attributeString));

            valueRequirements.add(valueRequirement);
            valueRequirement.addSubRequirements(buildValueRequirements(valueRequirementData, requirementType, dataTypeSequence));
        }

        return valueRequirements;
    }

    public final void insertDataRequirement(T8DataTransaction dataAccessProvider, T8DataConnection connection, DataRequirement dataRequirement) throws Exception
    {
        List<Map<String, Object>> newDRData;
        List<Map<String, Object>> newDRAttributeData;
        List<Map<String, Object>> newPropertyData;
        List<Map<String, Object>> newPropertyAttributeData;
        List<Map<String, Object>> newValueData;
        List<Map<String, Object>> newValueAttributeData;

        // Get all the new data from the data requirement.
        newDRData = T8RequirementDataHandler.getDataRequirementDataRows(dataRequirement);
        newDRAttributeData = T8RequirementDataHandler.getDataRequirementAttributeDataRows(dataRequirement);
        newPropertyData = T8RequirementDataHandler.getPropertyRequirementDataRows(dataRequirement);
        newPropertyAttributeData = T8RequirementDataHandler.getPropertyRequirementAttributeDataRows(dataRequirement);
        newValueData = T8RequirementDataHandler.getValueRequirementDataRows(dataRequirement);
        newValueAttributeData = T8RequirementDataHandler.getValueRequirementAttributeDataRows(dataRequirement);

        // Insert the data requirement data rows.
        insertDataRequirementData(dataAccessProvider, connection, newDRData);
        insertDataRequirementAttributeData(connection, newDRAttributeData);

        // Insert all the new property requirement data rows.
        insertPropertyRequirementData(connection, newPropertyData);
        insertPropertyRequirementAttributeData(connection, newPropertyAttributeData);

        // Insert all the new value requirement data rows.
        insertValueRequirementData(connection, newValueData);
        insertValueRequirementAttributeData(connection, newValueAttributeData);
    }

    public final void updateDataRequirement(T8DataTransaction dataAccessProvider, T8DataConnection connection, DataRequirement dataRequirement) throws Exception
    {
        List<Map<String, Object>> oldDRData;
        List<Map<String, Object>> oldDRAttributeData;
        List<Map<String, Object>> oldPropertyData;
        List<Map<String, Object>> oldPropertyAttributeData;
        List<Map<String, Object>> oldValueData;
        List<Map<String, Object>> oldValueAttributeData;
        List<Map<String, Object>> newDRData;
        List<Map<String, Object>> newDRAttributeData;
        List<Map<String, Object>> newPropertyData;
        List<Map<String, Object>> newPropertyAttributeData;
        List<Map<String, Object>> newValueData;
        List<Map<String, Object>> newValueAttributeData;

        // Get all existing data from the database.
        oldDRData = selectDataRequirementData(connection, dataRequirement.getConceptID());
        oldDRAttributeData = selectDataRequirementAttributeData(connection, dataRequirement.getConceptID());
        oldPropertyData = selectPropertyRequirementData(connection, dataRequirement.getConceptID());
        oldPropertyAttributeData = selectPropertyRequirementAttributeData(connection, dataRequirement.getConceptID());
        oldValueData = selectValueRequirementData(connection, dataRequirement.getConceptID());
        oldValueAttributeData = selectValueRequirementAttributeData(connection, dataRequirement.getConceptID());

        // Get all the new data from the data requirement.
        newDRData = T8RequirementDataHandler.getDataRequirementDataRows(dataRequirement);
        newDRAttributeData = T8RequirementDataHandler.getDataRequirementAttributeDataRows(dataRequirement);
        newPropertyData = T8RequirementDataHandler.getPropertyRequirementDataRows(dataRequirement);
        newPropertyAttributeData = T8RequirementDataHandler.getPropertyRequirementAttributeDataRows(dataRequirement);
        newValueData = T8RequirementDataHandler.getValueRequirementDataRows(dataRequirement);
        newValueAttributeData = T8RequirementDataHandler.getValueRequirementAttributeDataRows(dataRequirement);

        // Update the data requirement data row.
        if (oldDRData.size() > 0)
        {
            updateDataRequirementData(dataAccessProvider, connection, newDRData);
        }
        else // No data requirement to update, so insert the data.
        {
            insertDataRequirementData(dataAccessProvider, connection, newDRData);
        }

        // Remove all deleted value requirement attribute rows.
        {
            List<Map<String, Object>> removedRows;

            removedRows = CollectionUtilities.getAddedRows(newValueAttributeData, oldValueAttributeData, new String[]{"PROPERTY_ID", "DATA_TYPE_ID", "DATA_TYPE_SEQUENCE", "ATTRIBUTE_ID"});
            deleteValueRequirementAttributeData(connection, removedRows);
        }

        // Remove all deleted value requirement rows.
        {
            List<Map<String, Object>> removedRows;

            removedRows = CollectionUtilities.getAddedRows(newValueData, oldValueData, new String[]{"PROPERTY_ID", "DATA_TYPE_ID", "DATA_TYPE_SEQUENCE"});
            CollectionUtilities.sortMaps(removedRows, false, "DATA_TYPE_SEQUENCE");
            deleteValueRequirementData(connection, removedRows);
        }

        // Remove all deleted property requirement attribute rows.
        {
            List<Map<String, Object>> removedRows;

            removedRows = CollectionUtilities.getAddedRows(newPropertyAttributeData, oldPropertyAttributeData, new String[]{"PROPERTY_ID", "ATTRIBUTE_ID"});
            deletePropertyRequirementAttributeData(connection, removedRows);
        }

        // Remove all deleted property requirement rows.
        {
            List<Map<String, Object>> removedRows;

            removedRows = CollectionUtilities.getAddedRows(newPropertyData, oldPropertyData, new String[]{"PROPERTY_ID"});
            deletePropertyRequirementData(connection, removedRows);
        }

        // Remove all deleted data requirement attribute rows.
        {
            List<Map<String, Object>> removedRows;

            removedRows = CollectionUtilities.getAddedRows(newDRAttributeData, oldDRAttributeData, new String[]{"ATTRIBUTE_ID"});
            deleteDataRequirementAttributeData(connection, removedRows);
        }

        // Insert all new and existing data requirement attribute rows.
        {
            List<Map<String, Object>> newRows;
            List<Map<String, Object>> updatedRows;

            newRows = CollectionUtilities.getAddedRows(oldDRAttributeData, newDRAttributeData, new String[]{"ATTRIBUTE_ID"});
            insertDataRequirementAttributeData(connection, newRows);

            updatedRows = new ArrayList<>(newDRAttributeData);
            updatedRows.removeAll(newRows);
            updateDataRequirementAttributeData(connection, updatedRows);
        }

        // Insert all new and existing property requirement rows.
        {
            List<Map<String, Object>> newRows;
            List<Map<String, Object>> updatedRows;

            newRows = CollectionUtilities.getAddedRows(oldPropertyData, newPropertyData, new String[]{"PROPERTY_ID"});
            insertPropertyRequirementData(connection, newRows);

            updatedRows = new ArrayList<>(newPropertyData);
            updatedRows.removeAll(newRows);
            updatePropertyRequirementData(connection, updatedRows);
        }

        // Insert all new and existing property requirement attribute rows.
        {
            List<Map<String, Object>> newRows;
            List<Map<String, Object>> updatedRows;

            newRows = CollectionUtilities.getAddedRows(oldPropertyAttributeData, newPropertyAttributeData, new String[]{"PROPERTY_ID", "ATTRIBUTE_ID"});
            insertPropertyRequirementAttributeData(connection, newRows);

            updatedRows = new ArrayList<>(newPropertyAttributeData);
            updatedRows.removeAll(newRows);
            updatePropertyRequirementAttributeData(connection, updatedRows);
        }

        // Insert all new value requirement rows.
        {
            List<Map<String, Object>> newRows;
            List<Map<String, Object>> updatedRows;

            newRows = CollectionUtilities.getAddedRows(oldValueData, newValueData, new String[]{"PROPERTY_ID", "DATA_TYPE_ID", "DATA_TYPE_SEQUENCE"});
            insertValueRequirementData(connection, newRows);

            updatedRows = new ArrayList<>(newValueData);
            updatedRows.removeAll(newRows);
            updateValueRequirementData(connection, updatedRows);
        }

        // Insert all new value requirement attribute rows.
        {
            List<Map<String, Object>> newRows;
            List<Map<String, Object>> updatedRows;

            newRows = CollectionUtilities.getAddedRows(oldValueAttributeData, newValueAttributeData, new String[]{"PROPERTY_ID", "DATA_TYPE_ID", "DATA_TYPE_SEQUENCE", "ATTRIBUTE_ID"});
            insertValueRequirementAttributeData(connection, newRows);

            updatedRows = new ArrayList<>(newValueAttributeData);
            updatedRows.removeAll(newRows);
            updateValueRequirementAttributeData(connection, updatedRows);
        }

        // Update the cache.
        drCache.put(dataRequirement);
    }

    public final void deleteDataRequirement(T8DataConnection connection, String drId) throws Exception
    {
        List<Map<String, Object>> valueRequirementData;

        // Get all existing value data from the database.
        valueRequirementData = selectValueRequirementData(connection, drId);

        // Delete data in other tables related to the Data Requirement.
        deleteDataRequirementRelatedData(connection, drId);

        // Delete all value requirements in the correct sequence (hierarchical delete).
        deleteValueRequirementAttributeData(connection, drId);
        CollectionUtilities.sortMaps(valueRequirementData, false, "DATA_TYPE_SEQUENCE");
        deleteValueRequirementData(connection, valueRequirementData);

        // Delete all property requirements.
        deletePropertyRequirementAttributeData(connection, drId);
        deletePropertyRequirementData(connection, drId);

        // Delete the data requirement instance entries.
        deleteDataRequirementInstanceDataByDR(connection, drId);

        // Delete the data requirement entry.
        deleteDataRequirementAttributeData(connection, drId);
        deleteDataRequirementData(connection, drId);

        // Update the cache.
        drCache.remove(drId);
    }

    private List<Map<String, Object>> selectDataRequirementInstanceData(T8DataConnection connection, String drInstanceId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM DAT_REQ_INS WHERE DR_INSTANCE_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drInstanceId));
            resultSet = selectStatement.executeQuery();
            return dataExtractor.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectDataRequirementData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM DAT_REQ WHERE DR_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            resultSet = selectStatement.executeQuery();
            return dataExtractor.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectDataRequirementAttributeData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM DAT_REQ_ATR WHERE DR_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            resultSet = selectStatement.executeQuery();
            return dataExtractor.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectPropertyRequirementData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM DAT_REQ_PRP WHERE DR_ID = " + guidParameter + " ORDER BY PROPERTY_SEQUENCE");
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            resultSet = selectStatement.executeQuery();
            return dataExtractor.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectPropertyRequirementAttributeData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM DAT_REQ_PRP_ATR WHERE DR_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            resultSet = selectStatement.executeQuery();
            return dataExtractor.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectValueRequirementData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM DAT_REQ_TYP WHERE DR_ID = " + guidParameter + " ORDER BY DATA_TYPE_SEQUENCE");
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            resultSet = selectStatement.executeQuery();
            return dataExtractor.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private List<Map<String, Object>> selectValueRequirementAttributeData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement selectStatement = null;
        ResultSet resultSet = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            selectStatement = connection.prepareStatement("SELECT * FROM DAT_REQ_TYP_ATR WHERE DR_ID = " + guidParameter);
            selectStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            resultSet = selectStatement.executeQuery();
            return dataExtractor.getResultSetDataRows(resultSet);
        }
        finally
        {
            if (resultSet != null) resultSet.close();
            if (selectStatement != null) selectStatement.close();
        }
    }

    private void insertDataRequirementInstanceData(T8DataTransaction tx, T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement insertStatement = null;

        try
        {
            T8SessionContext sessionContext;
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;
            String insertedByID;
            String insertedByIID;
            Timestamp insertedAt;

            // Get auditing values.
            sessionContext = tx.getContext().getSessionContext();
            if (sessionContext.isSystemSession())
            {
                insertedByID = sessionContext.getSystemAgentIdentifier();
                insertedByIID = sessionContext.getSystemAgentInstanceIdentifier();
                insertedAt = new Timestamp(System.currentTimeMillis());
            }
            else
            {
                insertedByID = sessionContext.getUserIdentifier();
                insertedByIID = null;
                insertedAt = new Timestamp(System.currentTimeMillis());
            }


            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            insertStatement = connection.prepareStatement("INSERT INTO DAT_REQ_INS (DR_INSTANCE_ID, DR_ID, INDEPENDENT_INDICATOR, RECORD_OC_ID, INSERTED_BY_ID, INSERTED_BY_IID, INSERTED_AT) VALUES (" + guidParameter + ", " + guidParameter + ", ?, " + guidParameter + ", ?, ?, ?)");
            for (Map<String, Object> dataRow : dataRows)
            {
                insertStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_INSTANCE_ID")));
                insertStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                insertStatement.setString(3, (String)dataRow.get("INDEPENDENT_INDICATOR"));
                insertStatement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_OC_ID")));
                insertStatement.setString(5, insertedByID);
                insertStatement.setString(6, insertedByIID);
                insertStatement.setTimestamp(7, insertedAt);
                insertStatement.execute();
            }
        }
        finally
        {
            if (insertStatement != null) insertStatement.close();
        }
    }

    private void updateDataRequirementInstanceData(T8DataTransaction tx, T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement updateStatement = null;

        try
        {
            T8SessionContext sessionContext;
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;
            String updatedByID;
            String updatedByIID;
            Timestamp updatedAt;

            // Get auditing values.
            sessionContext = tx.getContext().getSessionContext();
            if (sessionContext.isSystemSession())
            {
                updatedByID = sessionContext.getSystemAgentIdentifier();
                updatedByIID = sessionContext.getSystemAgentInstanceIdentifier();
                updatedAt = new Timestamp(System.currentTimeMillis());
            }
            else
            {
                updatedByID = sessionContext.getUserIdentifier();
                updatedByIID = null;
                updatedAt = new Timestamp(System.currentTimeMillis());
            }

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            updateStatement = connection.prepareStatement("UPDATE DAT_REQ_INS SET DR_ID = " + guidParameter + ", INDEPENDENT_INDICATOR = ?, RECORD_OC_ID = " + guidParameter + ", UPDATED_BY_ID = ?, UPDATED_BY_IID = ?, UPDATED_AT = ? WHERE DR_INSTANCE_ID = " + guidParameter);
            for (Map<String, Object> dataRow : dataRows)
            {
                updateStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                updateStatement.setString(2, (String)dataRow.get("INDEPENDENT_INDICATOR"));
                updateStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("RECORD_OC_ID")));
                updateStatement.setString(4, updatedByID);
                updateStatement.setString(5, updatedByIID);
                updateStatement.setTimestamp(6, updatedAt);
                updateStatement.setString(7, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_INSTANCE_ID")));
                updateStatement.execute();
            }
        }
        finally
        {
            if (updateStatement != null) updateStatement.close();
        }
    }

    private void deleteDataRequirementInstanceDataByDR(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_INS WHERE DR_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            deleteStatement.execute();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void deleteDataRequirementInstanceData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_INS WHERE DR_INSTANCE_ID = " + guidParameter);
            for (Map<String, Object> dataRow : dataRows)
            {
                deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_INSTANCE_ID")));
                deleteStatement.execute();
            }
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void insertDataRequirementData(T8DataTransaction tx, T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement insertStatement = null;

        try
        {
            T8SessionContext sessionContext;
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;
            String insertedByID;
            String insertedByIID;
            Timestamp insertedAt;

            // Get auditing values.
            sessionContext = tx.getContext().getSessionContext();
            if (sessionContext.isSystemSession())
            {
                insertedByID = sessionContext.getSystemAgentIdentifier();
                insertedByIID = sessionContext.getSystemAgentInstanceIdentifier();
                insertedAt = new Timestamp(System.currentTimeMillis());
            }
            else
            {
                insertedByID = sessionContext.getUserIdentifier();
                insertedByIID = null;
                insertedAt = new Timestamp(System.currentTimeMillis());
            }

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            insertStatement = connection.prepareStatement("INSERT INTO DAT_REQ (DR_ID, CLASS_ID, INSERTED_BY_ID, INSERTED_BY_IID, INSERTED_AT) VALUES (" + guidParameter + ", " + guidParameter + ", ?, ?, ?)");
            for (Map<String, Object> dataRow : dataRows)
            {
                insertStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                insertStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("CLASS_ID")));
                insertStatement.setString(3, insertedByID);
                insertStatement.setString(4, insertedByIID);
                insertStatement.setTimestamp(5, insertedAt);
                insertStatement.execute();
            }
        }
        finally
        {
            if (insertStatement != null) insertStatement.close();
        }
    }

    private void updateDataRequirementData(T8DataTransaction tx, T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement updateStatement = null;

        try
        {
            T8SessionContext sessionContext;
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;
            String updatedByID;
            String updatedByIID;
            Timestamp updatedAt;

            // Get auditing values.
            sessionContext = tx.getContext().getSessionContext();
            if (sessionContext.isSystemSession())
            {
                updatedByID = sessionContext.getSystemAgentIdentifier();
                updatedByIID = sessionContext.getSystemAgentInstanceIdentifier();
                updatedAt = new Timestamp(System.currentTimeMillis());
            }
            else
            {
                updatedByID = sessionContext.getUserIdentifier();
                updatedByIID = null;
                updatedAt = new Timestamp(System.currentTimeMillis());
            }

            // Create the statement and execute it.
            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            updateStatement = connection.prepareStatement("UPDATE DAT_REQ SET CLASS_ID = " + guidParameter + ", UPDATED_BY_ID = ?, UPDATED_BY_IID = ?, UPDATED_AT = ? WHERE DR_ID = " + guidParameter);
            for (Map<String, Object> dataRow : dataRows)
            {
                updateStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("CLASS_ID")));
                updateStatement.setString(2, updatedByID);
                updateStatement.setString(3, updatedByIID);
                updateStatement.setTimestamp(4, updatedAt);
                updateStatement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                updateStatement.execute();
            }
        }
        finally
        {
            if (updateStatement != null) updateStatement.close();
        }
    }

    private void deleteDataRequirementRelatedData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();

            // Delete Views.
            deleteStatement = connection.prepareStatement("DELETE DAT_REQ_VAL WHERE DR_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            deleteStatement.executeUpdate();

            // Delete Data String Particle Settings.
            deleteStatement = connection.prepareStatement("DELETE DAT_STR_PRT WHERE DR_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            deleteStatement.executeUpdate();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void deleteDataRequirementData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ WHERE DR_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            deleteStatement.execute();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void deleteDataRequirementAttributeData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_ATR WHERE DR_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            deleteStatement.execute();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void insertDataRequirementAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement insertStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            insertStatement = connection.prepareStatement("INSERT INTO DAT_REQ_ATR (DR_ID, ATTRIBUTE_ID, VALUE, TYPE) VALUES (" + guidParameter + ", ?, ?, ?)");
            for (Map<String, Object> dataRow : dataRows)
            {
                insertStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                insertStatement.setString(2, (String)dataRow.get("ATTRIBUTE_ID"));
                insertStatement.setString(3, (String)dataRow.get("VALUE"));
                insertStatement.setString(4, (String)dataRow.get("TYPE"));
                insertStatement.execute();
            }
        }
        finally
        {
            if (insertStatement != null) insertStatement.close();
        }
    }

    private void updateDataRequirementAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement updateStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            updateStatement = connection.prepareStatement("UPDATE DAT_REQ_ATR SET VALUE = ?, TYPE = ? WHERE DR_ID = " + guidParameter + " AND ATTRIBUTE_ID = ?");
            for (Map<String, Object> dataRow : dataRows)
            {
                updateStatement.setString(1, (String)dataRow.get("VALUE"));
                updateStatement.setString(2, (String)dataRow.get("TYPE"));
                updateStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                updateStatement.setString(4, (String)dataRow.get("ATTRIBUTE_ID"));
                updateStatement.execute();
            }
        }
        finally
        {
            if (updateStatement != null) updateStatement.close();
        }
    }

    private void deleteDataRequirementAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_ATR WHERE DR_ID = " + guidParameter + " AND ATTRIBUTE_ID = ?");
            for (Map<String, Object> dataRow : dataRows)
            {
                deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                deleteStatement.setString(2, (String)dataRow.get("ATTRIBUTE_ID"));
                deleteStatement.execute();
            }
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void insertPropertyRequirementData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement insertStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            insertStatement = connection.prepareStatement("INSERT INTO DAT_REQ_PRP (DR_ID, SECTION_ID, PROPERTY_ID, REQUIRED, PROPERTY_SEQUENCE, ATTRIBUTES) VALUES (" + guidParameter + ", " + guidParameter + ", " + guidParameter + ", ?, ?, ?)");
            for (Map<String, Object> dataRow : dataRows)
            {
                insertStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                insertStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                insertStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                insertStatement.setString(4, (String)dataRow.get("REQUIRED"));
                insertStatement.setInt(5, (Integer)dataRow.get("PROPERTY_SEQUENCE"));
                insertStatement.setString(6, (String)dataRow.get("ATTRIBUTES"));
                insertStatement.execute();
            }
        }
        finally
        {
            if (insertStatement != null) insertStatement.close();
        }
    }

    private void updatePropertyRequirementData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement updateStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            updateStatement = connection.prepareStatement("UPDATE DAT_REQ_PRP SET REQUIRED = ?, PROPERTY_SEQUENCE = ?, ATTRIBUTES = ? WHERE DR_ID = " + guidParameter + " AND SECTION_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter);
            for (Map<String, Object> dataRow : dataRows)
            {
                updateStatement.setString(1, (String)dataRow.get("REQUIRED"));
                updateStatement.setInt(2, (Integer)dataRow.get("PROPERTY_SEQUENCE"));
                updateStatement.setString(3, (String)dataRow.get("ATTRIBUTES"));
                updateStatement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                updateStatement.setString(5, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                updateStatement.setString(6, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                updateStatement.execute();
            }
        }
        finally
        {
            if (updateStatement != null) updateStatement.close();
        }
    }

    private void deletePropertyRequirementData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_PRP WHERE DR_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            deleteStatement.execute();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void deletePropertyRequirementData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement drPropertyDeleteStatement = null;
        PreparedStatement dsParticlePropertyDeleteStatement = null;
        PreparedStatement drValuePropertyDeleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();

            drPropertyDeleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_PRP WHERE DR_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter + "");
            dsParticlePropertyDeleteStatement = connection.prepareStatement("DELETE DAT_STR_PRP WHERE DR_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter);
            drValuePropertyDeleteStatement = connection.prepareStatement("DELETE DAT_REQ_VAL WHERE DR_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter);

            for (Map<String, Object> dataRow : dataRows)
            {
                dsParticlePropertyDeleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                dsParticlePropertyDeleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                dsParticlePropertyDeleteStatement.execute();

                drValuePropertyDeleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                drValuePropertyDeleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                drValuePropertyDeleteStatement.execute();

                drPropertyDeleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                drPropertyDeleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                drPropertyDeleteStatement.execute();
            }
        }
        finally
        {
            if (drPropertyDeleteStatement != null) drPropertyDeleteStatement.close();
            if (dsParticlePropertyDeleteStatement != null) dsParticlePropertyDeleteStatement.close();
            if (drValuePropertyDeleteStatement != null) drValuePropertyDeleteStatement.close();
        }
    }

    private void insertPropertyRequirementAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement insertStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            insertStatement = connection.prepareStatement("INSERT INTO DAT_REQ_PRP_ATR (DR_ID, PROPERTY_ID, ATTRIBUTE_ID, VALUE, TYPE) VALUES (" + guidParameter + ", " + guidParameter + ", ?, ?, ?)");
            for (Map<String, Object> dataRow : dataRows)
            {
                insertStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                insertStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                insertStatement.setString(3, (String)dataRow.get("ATTRIBUTE_ID"));
                insertStatement.setString(4, (String)dataRow.get("VALUE"));
                insertStatement.setString(5, (String)dataRow.get("TYPE"));
                insertStatement.execute();
            }
        }
        finally
        {
            if (insertStatement != null) insertStatement.close();
        }
    }

    private void updatePropertyRequirementAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement updateStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            updateStatement = connection.prepareStatement("UPDATE DAT_REQ_PRP_ATR SET VALUE = ?, TYPE = ? WHERE DR_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter + " AND ATTRIBUTE_ID = ?");
            for (Map<String, Object> dataRow : dataRows)
            {
                updateStatement.setString(1, (String)dataRow.get("VALUE"));
                updateStatement.setString(2, (String)dataRow.get("TYPE"));
                updateStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                updateStatement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                updateStatement.setString(5, (String)dataRow.get("ATTRIBUTE_ID"));
                updateStatement.execute();
            }
        }
        finally
        {
            if (updateStatement != null) updateStatement.close();
        }
    }

    private void deletePropertyRequirementAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_PRP_ATR WHERE DR_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter + " AND ATTRIBUTE_ID = ?");
            for (Map<String, Object> dataRow : dataRows)
            {
                deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                deleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                deleteStatement.setString(3, (String)dataRow.get("ATTRIBUTE_ID"));
                deleteStatement.execute();
            }
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void deletePropertyRequirementAttributeData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_PRP_ATR WHERE DR_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            deleteStatement.execute();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void insertValueRequirementData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement insertStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            insertStatement = connection.prepareStatement("INSERT INTO DAT_REQ_TYP (DR_ID, SECTION_ID, PROPERTY_ID, DATA_TYPE_ID, DATA_TYPE_SEQUENCE, PARENT_DATA_TYPE_ID, PARENT_DATA_TYPE_SEQ, VALUE, ATTRIBUTES, PRESCRIBED_INDICATOR) VALUES (" + guidParameter + ", " + guidParameter + ", " + guidParameter + ", ?, ?, ?, ?, ?, ?, ?)");
            for (Map<String, Object> dataRow : dataRows)
            {
                insertStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                insertStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                insertStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                insertStatement.setString(4, (String)dataRow.get("DATA_TYPE_ID"));
                insertStatement.setInt(5, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                insertStatement.setString(6, (String)dataRow.get("PARENT_DATA_TYPE_ID"));
                insertStatement.setObject(7, (Integer)dataRow.get("PARENT_DATA_TYPE_SEQ"));
                insertStatement.setString(8, (String)dataRow.get("VALUE"));
                insertStatement.setString(9, (String)dataRow.get("ATTRIBUTES"));
                insertStatement.setString(10, (String)dataRow.get("PRESCRIBED_INDICATOR"));
                insertStatement.execute();
            }
        }
        finally
        {
            if (insertStatement != null) insertStatement.close();
        }
    }

    private void updateValueRequirementData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement updateStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            updateStatement = connection.prepareStatement("UPDATE DAT_REQ_TYP SET PARENT_DATA_TYPE_ID = ?, PARENT_DATA_TYPE_SEQ = ?, VALUE = ?, ATTRIBUTES = ?, PRESCRIBED_INDICATOR = ? WHERE DR_ID = " + guidParameter + " AND SECTION_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter + " AND DATA_TYPE_ID = ? AND DATA_TYPE_SEQUENCE = ?");
            for (Map<String, Object> dataRow : dataRows)
            {
                updateStatement.setString(1, (String)dataRow.get("PARENT_DATA_TYPE_ID"));
                updateStatement.setObject(2, (Integer)dataRow.get("PARENT_DATA_TYPE_SEQ"));
                updateStatement.setString(3, (String)dataRow.get("VALUE"));
                updateStatement.setString(4, (String)dataRow.get("ATTRIBUTES"));
                updateStatement.setString(5, (String)dataRow.get("PRESCRIBED_INDICATOR"));
                updateStatement.setString(6, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                updateStatement.setString(7, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                updateStatement.setString(8, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                updateStatement.setString(9, (String)dataRow.get("DATA_TYPE_ID"));
                updateStatement.setInt(10, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                updateStatement.execute();
            }
        }
        finally
        {
            if (updateStatement != null) updateStatement.close();
        }
    }

    private void deleteValueRequirementData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_TYP WHERE DR_ID = " + guidParameter + " AND SECTION_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter + " AND DATA_TYPE_ID = ? AND DATA_TYPE_SEQUENCE = ?");
            for (Map<String, Object> dataRow : dataRows)
            {
                deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                deleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("SECTION_ID")));
                deleteStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                deleteStatement.setString(4, (String)dataRow.get("DATA_TYPE_ID"));
                deleteStatement.setInt(5, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                deleteStatement.execute();
            }
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void insertValueRequirementAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement insertStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            insertStatement = connection.prepareStatement("INSERT INTO DAT_REQ_TYP_ATR (DR_ID, PROPERTY_ID, DATA_TYPE_ID, DATA_TYPE_SEQUENCE, ATTRIBUTE_ID, VALUE, TYPE) VALUES (" + guidParameter + ", " + guidParameter + ", ?, ?, ?, ?, ?)");
            for (Map<String, Object> dataRow : dataRows)
            {
                insertStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                insertStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                insertStatement.setString(3, (String)dataRow.get("DATA_TYPE_ID"));
                insertStatement.setInt(4, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                insertStatement.setString(5, (String)dataRow.get("ATTRIBUTE_ID"));
                insertStatement.setString(6, (String)dataRow.get("VALUE"));
                insertStatement.setString(7, (String)dataRow.get("TYPE"));
                insertStatement.execute();
            }
        }
        finally
        {
            if (insertStatement != null) insertStatement.close();
        }
    }

    private void updateValueRequirementAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement updateStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            updateStatement = connection.prepareStatement("UPDATE DAT_REQ_TYP_ATR SET VALUE = ?, TYPE = ? WHERE DR_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter + " AND DATA_TYPE_ID = ? AND DATA_TYPE_SEQUENCE = ? AND ATTRIBUTE_ID = ?");
            for (Map<String, Object> dataRow : dataRows)
            {
                updateStatement.setString(1, (String)dataRow.get("VALUE"));
                updateStatement.setString(2, (String)dataRow.get("TYPE"));
                updateStatement.setString(3, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                updateStatement.setString(4, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                updateStatement.setString(5, (String)dataRow.get("DATA_TYPE_ID"));
                updateStatement.setInt(6, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                updateStatement.setString(7, (String)dataRow.get("ATTRIBUTE_ID"));
                updateStatement.execute();
            }
        }
        finally
        {
            if (updateStatement != null) updateStatement.close();
        }
    }

    private void deleteValueRequirementAttributeData(T8DataConnection connection, List<Map<String, Object>> dataRows) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_TYP_ATR WHERE DR_ID = " + guidParameter + " AND PROPERTY_ID = " + guidParameter + " AND DATA_TYPE_ID = ? AND DATA_TYPE_SEQUENCE = ? AND ATTRIBUTE_ID = ?");
            for (Map<String, Object> dataRow : dataRows)
            {
                deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("DR_ID")));
                deleteStatement.setString(2, dbAdaptor.convertHexStringToSQLGUIDString((String)dataRow.get("PROPERTY_ID")));
                deleteStatement.setString(3, (String)dataRow.get("DATA_TYPE_ID"));
                deleteStatement.setInt(4, (Integer)dataRow.get("DATA_TYPE_SEQUENCE"));
                deleteStatement.setString(5, (String)dataRow.get("ATTRIBUTE_ID"));
                deleteStatement.execute();
            }
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }

    private void deleteValueRequirementAttributeData(T8DataConnection connection, String drId) throws Exception
    {
        PreparedStatement deleteStatement = null;

        try
        {
            T8DatabaseAdaptor dbAdaptor;
            String guidParameter;

            dbAdaptor = connection.getDatabaseAdaptor();
            guidParameter = dbAdaptor.getSQLParameterizedHexToGUID();
            deleteStatement = connection.prepareStatement("DELETE FROM DAT_REQ_TYP_ATR WHERE DR_ID = " + guidParameter);
            deleteStatement.setString(1, dbAdaptor.convertHexStringToSQLGUIDString(drId));
            deleteStatement.execute();
        }
        finally
        {
            if (deleteStatement != null) deleteStatement.close();
        }
    }
}
