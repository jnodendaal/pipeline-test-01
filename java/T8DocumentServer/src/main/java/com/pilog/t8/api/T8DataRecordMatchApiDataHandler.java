package com.pilog.t8.api;

import com.pilog.t8.api.T8DataRecordMatchApi.T8RecordMatchEvent;
import com.pilog.t8.api.T8DuplicateResolutionApi.ResolvedState;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatch;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCase;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCriteria;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCheck;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCheck.T8RecordMatchCheckType;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCase.T8RecordMatchCaseType;
import com.pilog.t8.data.document.datarecord.match.T8DataRecordHashCode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.CollectionUtilities;

import static com.pilog.t8.definition.api.T8DataRecordMatchApiResource.*;
import static com.pilog.t8.api.T8DuplicateResolutionApi.ResolvedState.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordMatchApiDataHandler
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;

    public T8DataRecordMatchApiDataHandler(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
    }

    public List<String> retrieveDrInstanceMatchCriteria(T8DataTransaction tx, String drInstanceId) throws Exception
    {
        List<T8DataEntity> entities;
        List<String> criteriaIdList;
        T8DataFilter filter;
        String entityId;

        // Set the entity identifier.
        entityId = DATA_RECORD_MATCH_LINK_DE_IDENTIFIER;

        // Create new filter object.
        filter = new T8DataFilter(entityId);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_ROOT_ORG_ID, DataFilterOperator.EQUAL, sessionContext.getRootOrganizationIdentifier(), false);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_DR_INSTANCE_ID, DataFilterOperator.EQUAL, drInstanceId, false);

        criteriaIdList = new ArrayList<>();
        entities = tx.select(entityId, filter);
        for (T8DataEntity entity : entities)
        {
            String criteriaId;

            criteriaId = (String)entity.getFieldValue(entityId + F_MATCH_CRITERIA_ID);
            criteriaIdList.add(criteriaId);
        }

        return criteriaIdList;
    }

    public T8RecordMatchCriteria retrieveMatchCriteria(T8DataTransaction tx, String criteriaId) throws Exception
    {
        List<T8RecordMatchCriteria> criteria;

        criteria = retrieveMatchCriteria(tx, ArrayLists.newArrayList(criteriaId));
        return criteria.size() > 0 ? criteria.get(0) : null;
    }

    public List<T8RecordMatchCriteria> retrieveMatchCriteria(T8DataTransaction tx, List<String> criteriaIdList) throws Exception
    {
        List<T8RecordMatchCriteria> criteriaList;
        T8DataFilter matchCriteriaFilter;
        List<T8DataEntity> entityList;
        String entityId;

        // Set entity identifier.
        entityId = DATA_RECORD_MATCH_CRITERIA_DE_IDENTIFIER;

        // Create new filter object.
        matchCriteriaFilter = new T8DataFilter(entityId);
        matchCriteriaFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_ORG_ID, DataFilterOperator.EQUAL, sessionContext.getRootOrganizationIdentifier(), false);
        matchCriteriaFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_MATCH_CRITERIA_ID, DataFilterOperator.IN, criteriaIdList, false);

        // Select the entities using the filter.
        criteriaList = new ArrayList<>();
        entityList = tx.select(entityId, matchCriteriaFilter);
        for (T8DataEntity matchCriteriaEntity : entityList)
        {
            T8RecordMatchCriteria matchCriteria;
            String criteriaName;
            String criteriaDescription;
            String criteriaId;
            String orgId;

            // Get the entity field values.
            criteriaId = (String)matchCriteriaEntity.getFieldValue(entityId + F_MATCH_CRITERIA_ID);
            criteriaName = (String)matchCriteriaEntity.getFieldValue(entityId + F_NAME);
            criteriaDescription = (String)matchCriteriaEntity.getFieldValue(entityId + F_DESCRIPTION);
            orgId = (String)matchCriteriaEntity.getFieldValue(entityId + F_ORG_ID);

            // Create the match criteria object.
            matchCriteria = new T8RecordMatchCriteria(criteriaId);
            matchCriteria.setDescription(criteriaDescription);
            matchCriteria.setName(criteriaName);
            matchCriteria.setOrgId(orgId);

            // Retrieve the match criteria match cases.
            retrieveMatchCases(tx, matchCriteria);

            // Add the completed criteria object to the result list.
            criteriaList.add(matchCriteria);
        }

        // Return retrieved list of match criteria.
        return criteriaList;
    }

    private void retrieveMatchCases(T8DataTransaction tx, T8RecordMatchCriteria criteria) throws Exception
    {
        List<T8DataEntity> matchCaseEntities;
        T8DataFilter matchCaseFilter;
        String entityId;

        // Set the entity identifier.
        entityId = DATA_RECORD_MATCH_CASE_DE_IDENTIFIER;

        // Create new filter object.
        matchCaseFilter = new T8DataFilter(entityId);
        matchCaseFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_MATCH_CRITERIA_ID, DataFilterOperator.EQUAL, criteria.getId(), false);
        matchCaseFilter.addFieldOrdering(entityId + F_SEQUENCE, T8DataFilter.OrderMethod.ASCENDING);

        matchCaseEntities = tx.select(entityId, matchCaseFilter);
        for (T8DataEntity matchCaseEntity : matchCaseEntities)
        {
            T8RecordMatchCase matchCase;

            matchCase = new T8RecordMatchCase((String)matchCaseEntity.getFieldValue(entityId + F_MATCH_CASE_ID));
            matchCase.setName((String)matchCaseEntity.getFieldValue(entityId + F_NAME)) ;
            matchCase.setDescription((String)matchCaseEntity.getFieldValue(entityId + F_DESCRIPTION));
            matchCase.setDataExpression((String)matchCaseEntity.getFieldValue(entityId + F_DATA_EXPRESSION));
            matchCase.setType(T8RecordMatchCaseType.valueOf((String)matchCaseEntity.getFieldValue(entityId + F_TYPE)));
            criteria.addCase(matchCase);
        }

        retrieveMatchChecks(tx, criteria);
    }

    private void retrieveMatchChecks(T8DataTransaction tx, T8RecordMatchCriteria criteria) throws Exception
    {
        List<T8DataEntity> matchCheckEntities;
        List<T8RecordMatchCase> matchCaseList;
        T8RecordMatchCheck matchCheck;
        T8DataFilter matchCheckFilter;
        String entityId;
        String dataExpression;

        // Set the entityIdentifier.
        entityId = DATA_RECORD_MATCH_CHECK_DE_IDENTIFIER;

        // Create new filter object.
        matchCheckFilter = new T8DataFilter(entityId);
        matchCheckFilter.addFilterCriterion(DataFilterConjunction.AND, entityId+F_MATCH_CRITERIA_ID, DataFilterOperator.EQUAL, criteria.getId(), false);
        matchCheckFilter.addFieldOrdering(entityId + F_SEQUENCE, T8DataFilter.OrderMethod.ASCENDING);

        // Get the match cases for the criteria.
        matchCaseList = criteria.getCases();
        matchCheckEntities = tx.select(entityId, matchCheckFilter);
        for(T8DataEntity  matchCheckEntity : matchCheckEntities)
        {
            String matchCheckId;
            String matchCaseId;

            matchCheckId = (String)matchCheckEntity.getFieldValue(entityId + F_MATCH_CHECK_ID);
            matchCaseId = (String)matchCheckEntity.getFieldValue(entityId + F_MATCH_CASE_ID);
            dataExpression = (String)matchCheckEntity.getFieldValue(entityId + F_DATA_EXPRESSION);

            matchCheck = new T8RecordMatchCheck(matchCheckId);
            matchCheck.setDataExpression(dataExpression);
            matchCheck.setName((String)matchCheckEntity.getFieldValue(entityId + F_NAME));
            matchCheck.setDescription((String)matchCheckEntity.getFieldValue(entityId + F_DESCRIPTION));
            matchCheck.setPercentage((double)matchCheckEntity.getFieldValue(entityId + F_PERCENTAGE));
            matchCheck.setType(T8RecordMatchCheckType.valueOf((String)matchCheckEntity.getFieldValue(entityId + F_TYPE)));

            // For each match case add the appropriate checks.
            for (T8RecordMatchCase matchCase : matchCaseList)
            {
                if (matchCase.getId().equals(matchCaseId))
                {
                    matchCase.addCheck(matchCheck);
                }
            }
        }
    }

    public void insertMatchCriteria(T8DataTransaction tx, T8RecordMatchCriteria criteria) throws Exception
    {
        T8DataEntity matchCriteriaEntity;
        String entityId;

        // Create an entity.
        entityId = DATA_RECORD_MATCH_CRITERIA_DE_IDENTIFIER;
        matchCriteriaEntity = tx.create(entityId, null);
        matchCriteriaEntity.setFieldValue(entityId + F_MATCH_CRITERIA_ID, criteria.getId());
        matchCriteriaEntity.setFieldValue(entityId + F_ORG_ID,  criteria.getOrgId());
        matchCriteriaEntity.setFieldValue(entityId + F_NAME, criteria.getName());
        matchCriteriaEntity.setFieldValue(entityId + F_DESCRIPTION, criteria.getDescription());

        // Insert the entity.
        tx.insert(matchCriteriaEntity);
    }

    public void insertMatchCase(T8DataTransaction tx, T8RecordMatchCase matchCase) throws Exception
    {
        T8DataEntity matchCaseEntity;
        String entityId;

        // Create an entity.
        entityId = DATA_RECORD_MATCH_CASE_DE_IDENTIFIER;
        matchCaseEntity = tx.create(entityId, null);
        matchCaseEntity.setFieldValue(entityId + F_MATCH_CRITERIA_ID, matchCase.getMatchCriteria().getId());
        matchCaseEntity.setFieldValue(entityId + F_MATCH_CASE_ID,  matchCase.getId());
        matchCaseEntity.setFieldValue(entityId + F_NAME, matchCase.getName());
        matchCaseEntity.setFieldValue(entityId + F_TYPE, matchCase.getType().toString());
        matchCaseEntity.setFieldValue(entityId + F_DESCRIPTION, matchCase.getDescription());
        matchCaseEntity.setFieldValue(entityId + F_SEQUENCE,  matchCase.getIndex());
        matchCaseEntity.setFieldValue(entityId + F_DATA_EXPRESSION,  matchCase.getDataExpression());

        // Insert the entity.
        tx.insert(matchCaseEntity);
    }

    public void insertMatchCheck(T8DataTransaction tx, T8RecordMatchCheck matchCheck) throws Exception
    {
        T8DataEntity matchCheckEntity;
        T8RecordMatchCase matchCase;
        String entityId;

        entityId = DATA_RECORD_MATCH_CHECK_DE_IDENTIFIER;
        matchCheckEntity = tx.create(entityId, null);
        matchCase = matchCheck.getMatchCase();

        // Set all the entity value.
        matchCheckEntity.setFieldValue(entityId + F_MATCH_CRITERIA_ID, matchCase.getMatchCriteria().getId());
        matchCheckEntity.setFieldValue(entityId + F_MATCH_CASE_ID, matchCase.getId());
        matchCheckEntity.setFieldValue(entityId + F_MATCH_CHECK_ID, matchCheck.getId());
        matchCheckEntity.setFieldValue(entityId + F_NAME, matchCheck.getName());
        matchCheckEntity.setFieldValue(entityId + F_DESCRIPTION, matchCheck.getDescription());
        matchCheckEntity.setFieldValue(entityId + F_SEQUENCE, matchCheck.getIndex());
        matchCheckEntity.setFieldValue(entityId + F_TYPE, matchCheck.getType().toString());
        matchCheckEntity.setFieldValue(entityId + F_PERCENTAGE, matchCheck.getPercentage());
        matchCheckEntity.setFieldValue(entityId + F_DATA_EXPRESSION, matchCheck.getDataExpression());

        // Insert the entity.
        tx.insert(matchCheckEntity);
    }

    public boolean updateMatchCriteria(T8DataTransaction tx, T8RecordMatchCriteria matchCriteria) throws Exception
    {
        T8DataEntity matchCriteriaEntity;
        String entityId;

        // Create an entity.
        entityId = DATA_RECORD_MATCH_CRITERIA_DE_IDENTIFIER;
        matchCriteriaEntity = tx.create(entityId, null);
        matchCriteriaEntity.setFieldValue(entityId + F_MATCH_CRITERIA_ID, matchCriteria.getId());
        matchCriteriaEntity.setFieldValue(entityId + F_ORG_ID,  matchCriteria.getOrgId());
        matchCriteriaEntity.setFieldValue(entityId + F_NAME, matchCriteria.getName());
        matchCriteriaEntity.setFieldValue(entityId + F_DESCRIPTION, matchCriteria.getDescription());

        // Update the match criteria the entity.
        return tx.update(matchCriteriaEntity);
    }

    public boolean updateMatchCase(T8DataTransaction tx, T8RecordMatchCase matchCase) throws Exception
    {
        String entityId;
        T8DataEntity entity;

        // Set the entity identifier.
        entityId = DATA_RECORD_MATCH_CASE_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityId, null);
        entity.setFieldValue(entityId + F_MATCH_CASE_ID, matchCase.getId());
        entity.setFieldValue(entityId + F_NAME, matchCase.getName());
        entity.setFieldValue(entityId + F_DESCRIPTION, matchCase.getDescription());
        entity.setFieldValue(entityId + F_MATCH_CRITERIA_ID, matchCase.getMatchCriteria().getId());
        entity.setFieldValue(entityId + F_SEQUENCE, matchCase.getIndex());
        entity.setFieldValue(entityId + F_DATA_EXPRESSION, matchCase.getDataExpression());
        entity.setFieldValue(entityId + F_TYPE, matchCase.getType().toString());
        // Update the Match Case.
        return tx.update(entity);
    }

    public boolean updateMatchCheck(T8DataTransaction tx, T8RecordMatchCheck matchCheck) throws Exception
    {
        T8DataEntity matchCheckEntity;
        T8RecordMatchCase matchCase;
        String entityId;

        entityId = DATA_RECORD_MATCH_CHECK_DE_IDENTIFIER;
        matchCheckEntity = tx.create(entityId, null);
        matchCase = matchCheck.getMatchCase();

        //set all the entity value
        matchCheckEntity.setFieldValue(entityId + F_MATCH_CRITERIA_ID, matchCase.getMatchCriteria().getId());
        matchCheckEntity.setFieldValue(entityId + F_MATCH_CASE_ID, matchCase.getId());
        matchCheckEntity.setFieldValue(entityId + F_MATCH_CHECK_ID, matchCheck.getId());
        matchCheckEntity.setFieldValue(entityId + F_NAME, matchCheck.getName());
        matchCheckEntity.setFieldValue(entityId + F_DESCRIPTION, matchCheck.getDescription());
        matchCheckEntity.setFieldValue(entityId + F_SEQUENCE, matchCheck.getIndex());
        matchCheckEntity.setFieldValue(entityId + F_DATA_EXPRESSION, matchCheck.getDataExpression());
        matchCheckEntity.setFieldValue(entityId + F_TYPE, matchCheck.getType().toString());
        matchCheckEntity.setFieldValue(entityId + F_PERCENTAGE, matchCheck.getPercentage());

        //update the value
        return tx.update(matchCheckEntity);
    }

    public void saveMatchCriteria(T8DataTransaction tx, T8RecordMatchCriteria newCriteria) throws Exception
    {
        T8RecordMatchCriteria existingCriteria;

        existingCriteria = retrieveMatchCriteria(tx, newCriteria.getId());
        if (existingCriteria == null)
        {
            // Insert the match criteria as it is new.
            insertMatchCriteria(tx, newCriteria);

            // Save the match cases for the criteria.
            for(T8RecordMatchCase matchCase : newCriteria.getCases())
            {
                saveMatchCase(tx, matchCase);
            }
        }
        else
        {
            updateMatchCriteria(tx, newCriteria);

            // The criteria already exists, so we need to do selective updates/inserts/deletions.
            for (T8RecordMatchCase existingCase : existingCriteria.getCases())
            {
                T8RecordMatchCase newCase;

                newCase = newCriteria.getCase(existingCase.getId());
                if (newCase == null)
                {
                    // This means the case has been deleted so delete it from the DB.
                    // First delete the match case checks.
                    for ( T8RecordMatchCheck check : existingCase.getChecks())
                    {
                        deleteMatchCheck(tx, check.getId());
                    }
                    //now Delete the match case.
                    deleteMatchCase(tx, existingCase.getId());
                }
                else
                {
                    // Update existing case.
                    updateMatchCase(tx, newCase);

                    // Also do check comparisons.
                    for (T8RecordMatchCheck existingCheck : existingCase.getChecks())
                    {
                        T8RecordMatchCheck newCheck;

                        newCheck = newCase.getCheck(existingCheck.getId());
                        if (newCheck == null)
                        {
                            // This is check deleted.
                            deleteMatchCheck(tx, existingCheck.getId());
                        }
                        else
                        {
                            // Check updated.
                            updateMatchCheck(tx, newCheck);
                        }
                    }
                }
            }

            // Now loop through the new cases to find insertions.
            for (T8RecordMatchCase newCase : newCriteria.getCases())
            {
                T8RecordMatchCase existingCase;

                existingCase = existingCriteria.getCase(newCase.getId());
                if (existingCase == null)
                {
                    // This means the case has been inserted in the new object.
                    insertMatchCase(tx, newCase);
                }
                else
                {
                    // Also do check comparisons.
                    for (T8RecordMatchCheck newCheck : newCase.getChecks())
                    {
                        T8RecordMatchCheck existingCheck;

                        existingCheck = existingCase.getCheck(newCheck.getId());
                        if (existingCheck == null)
                        {
                            // This means the check has been inserted.
                            insertMatchCheck(tx, newCheck);
                        }
                    }
                }
            }
        }
    }

    public void saveMatchCase(T8DataTransaction tx, T8RecordMatchCase matchCase) throws Exception
    {
       T8RecordMatchCase existingMatchCase;
       T8RecordMatchCriteria criteria;

       criteria = retrieveMatchCriteria(tx, matchCase.getMatchCriteria().getId());

       existingMatchCase = criteria.getCase(matchCase.getId());
       if (existingMatchCase == null)
       {
           insertMatchCase(tx, matchCase);

           //Save each match check for the match case.
           for(T8RecordMatchCheck matchCheck : matchCase.getChecks())
           {
               saveMatchCheck(tx, matchCheck);
           }
       }
       else
       {
            updateMatchCase(tx, matchCase);

            // The match case already exists so do selective updates/inserts/deletions.
            for (T8RecordMatchCheck existingCheck : existingMatchCase.getChecks())
            {
                T8RecordMatchCheck newCheck;

                newCheck = matchCase.getCheck(existingCheck.getId());
                if (newCheck == null)
                {
                    // If the match exist in the in the DB and not on the object delete it from the DB.
                    deleteMatchCheck(tx, existingCheck.getId());
                }
                else
                {
                    // Check updated.
                    updateMatchCheck(tx, newCheck);
                }
            }

            // Also do check comparisons.
            for (T8RecordMatchCheck newCheck : matchCase.getChecks())
            {
                T8RecordMatchCheck existingCheck;

                existingCheck = existingMatchCase.getCheck(newCheck.getId());
                if (existingCheck == null)
                {
                    // This means the check has been inserted.
                    insertMatchCheck(tx, newCheck);
                }
            }
       }
    }

    public void saveMatchCheck(T8DataTransaction tx, T8RecordMatchCheck matchCheck) throws Exception
    {
        T8RecordMatchCase matchCase;
        T8RecordMatchCase existingCase;
        T8RecordMatchCheck existingCheck;
        T8RecordMatchCriteria criteria;

        //Get match check match case.
        matchCase = matchCheck.getMatchCase();
        criteria =  retrieveMatchCriteria(tx, matchCase.getMatchCriteria().getId());
        existingCase = criteria.getCase(matchCase.getId());
        existingCheck = existingCase.getCheck(matchCheck.getId());

        if(existingCheck == null)
        {
            insertMatchCheck(tx, matchCheck);
        }
        else
        {
            updateMatchCheck(tx, matchCheck);
        }
    }

    public void  deleteMatchCriteria(T8DataTransaction tx, T8RecordMatchCriteria criteria) throws Exception
    {
        if (criteria != null)
        {
            for (T8RecordMatchCase matchCase : criteria.getCases())
            {
                for (T8RecordMatchCheck check : matchCase.getChecks())
                {
                    deleteMatchCheck(tx, check.getId());
                }
                deleteMatchCase(tx, matchCase.getId());
            }
            deleteMatchCriteria(tx, criteria.getId());
        }
    }

    public int deleteMatchCriteriaLinks(T8DataTransaction tx, String criteriaId) throws Exception
    {
        T8DataFilter filter;
        String entityId;

        // Get the entity to use.
        entityId = DATA_RECORD_MATCH_LINK_DE_IDENTIFIER;

        // Construct the data filter.
        filter = new T8DataFilter(entityId);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_ROOT_ORG_ID, DataFilterOperator.EQUAL, sessionContext.getRootOrganizationIdentifier(), false);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_MATCH_CRITERIA_ID, DataFilterOperator.EQUAL, criteriaId, false);

        // Delete the Match Criteria links.
        return tx.delete(entityId, filter);
    }

    public boolean deleteMatchCriteriaLink(T8DataTransaction tx, String drInstanceId, String criteriaId) throws Exception
    {
        T8DataEntity linkEntity;
        String entityId;

        // Create the link entity.
        entityId = DATA_RECORD_MATCH_LINK_DE_IDENTIFIER;
        linkEntity = tx.create(entityId, null);
        linkEntity.setFieldValue(entityId + F_ROOT_ORG_ID, sessionContext.getRootOrganizationIdentifier());
        linkEntity.setFieldValue(entityId + F_MATCH_CRITERIA_ID, criteriaId);
        linkEntity.setFieldValue(entityId + F_DR_INSTANCE_ID, criteriaId);

        // Delete the link.
        return tx.delete(linkEntity);
    }

    public int deleteDrInstanceMatchCriteriaLinks(T8DataTransaction tx, String drInstanceId) throws Exception
    {
        T8DataFilter filter;
        String entityId;

        // Create the link entity.
        entityId = DATA_RECORD_MATCH_LINK_DE_IDENTIFIER;
        filter = new T8DataFilter(entityId);
        filter.addFilterCriterion(entityId + F_ROOT_ORG_ID, sessionContext.getRootOrganizationIdentifier());
        filter.addFilterCriterion(entityId + F_DR_INSTANCE_ID, drInstanceId);

        // Delete the links.
        return tx.delete(entityId, filter);
    }

    public int deleteMatchCriteria(T8DataTransaction tx, String criteriaId) throws Exception
    {
        T8DataFilter filter;
        String entityId;

        // Get the entity to use.
        entityId = DATA_RECORD_MATCH_CRITERIA_DE_IDENTIFIER;

        // Construct the data filter.
        filter = new T8DataFilter(entityId);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_MATCH_CRITERIA_ID, DataFilterOperator.EQUAL, criteriaId, false);

        // Delete the Match Criteria.
        return tx.delete(entityId, filter);
    }

    public int deleteMatchCase(T8DataTransaction tx, String caseID) throws Exception
    {
        String entityId;
        T8DataFilter matchCaseDeleteFilter;

        // Get the entity to use.
        entityId = DATA_RECORD_MATCH_CASE_DE_IDENTIFIER;

        // Construct the data filter.
        matchCaseDeleteFilter = new T8DataFilter(entityId);
        matchCaseDeleteFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_MATCH_CASE_ID, DataFilterOperator.EQUAL, caseID, false);

        // Delete the Match Case.
        return tx.delete(entityId, matchCaseDeleteFilter);
    }

    public int deleteMatchCases(T8DataTransaction tx, String criteriaID) throws Exception
    {
        String entityId;
        T8DataFilter matchCaseDeleteFilter;

        // Get the entity to use.
        entityId = DATA_RECORD_MATCH_CASE_DE_IDENTIFIER;

        // Construct the data filter.
        matchCaseDeleteFilter = new T8DataFilter(entityId);
        matchCaseDeleteFilter.addFilterCriterion(DataFilterConjunction.AND, entityId+F_MATCH_CRITERIA_ID, DataFilterOperator.EQUAL, criteriaID, false);

        // Delete the Match Case.
        return tx.delete(entityId, matchCaseDeleteFilter);
    }

    public int deleteMatchChecks(T8DataTransaction tx, String criteriaID) throws Exception
    {
        String entityIdentifier;
        T8DataFilter matchCheckExpresionDeleteFilter;

        // Get the entity to use.
        entityIdentifier = DATA_RECORD_MATCH_CHECK_DE_IDENTIFIER;

        // Construct the data filter.
        matchCheckExpresionDeleteFilter = new T8DataFilter(entityIdentifier);
        matchCheckExpresionDeleteFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + F_MATCH_CRITERIA_ID, DataFilterOperator.EQUAL, criteriaID, false);

        // Delete the Match Criteria.
        return tx.delete(entityIdentifier, matchCheckExpresionDeleteFilter);
    }

    private int deleteMatchCheck(T8DataTransaction tx, String checkID) throws Exception
    {
        String entityId;
        T8DataFilter matchCheckeDeleteFilter;

        // Get the entity to use.
        entityId = DATA_RECORD_MATCH_CHECK_DE_IDENTIFIER;

        // Construct the data filter.
        matchCheckeDeleteFilter = new T8DataFilter(entityId);
        matchCheckeDeleteFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_MATCH_CHECK_ID, DataFilterOperator.EQUAL, checkID, false);

        // Delete the Match Check.
        return tx.delete(entityId, matchCheckeDeleteFilter);
    }

    public void insertMatchCriteriaLink(T8DataTransaction tx, String drInstanceId, String criteriaId) throws Exception
    {
        T8DataEntity entity;
        String entityId;

        // Create entity.
        entityId = DATA_RECORD_MATCH_LINK_DE_IDENTIFIER;
        entity =  tx.create(entityId, null);
        entity.setFieldValue(entityId + F_ROOT_ORG_ID, sessionContext.getRootOrganizationIdentifier());
        entity.setFieldValue(entityId + F_DR_INSTANCE_ID, drInstanceId);
        entity.setFieldValue(entityId + F_MATCH_CRITERIA_ID, criteriaId);

        // Insert the entity.
        tx.insert(entity);
    }

    public void insertRecordMatchFamily(T8DataTransaction tx, String matchInstanceId, String familyId, String familyCode) throws Exception
    {
        // TODO: Insert family.
        T8DataEntity recordMatchfamilyEntity;
        String entityId;
        Timestamp insertedAt;
        String insertedById;

        //Get auditing values.
        insertedById =  sessionContext.getUserIdentifier();
        insertedAt = new Timestamp(System.currentTimeMillis());
        entityId = DATA_RECORD_MATCH_FAMILY_DE_IDENTIFIER;

        recordMatchfamilyEntity =  tx.create(entityId, null);
        recordMatchfamilyEntity.setFieldValue(entityId + F_MATCH_INSTANCE_ID, matchInstanceId);
        recordMatchfamilyEntity.setFieldValue(entityId + F_FAMILY_ID, familyId);
        recordMatchfamilyEntity.setFieldValue(entityId + F_FAMILY_CODE, familyCode);
        recordMatchfamilyEntity.setFieldValue(entityId + F_INSERTED_BY_ID, insertedById);
        recordMatchfamilyEntity.setFieldValue(entityId + F_INSERTED_AT, insertedAt);

        //Insert the entity.
        tx.insert(recordMatchfamilyEntity);
    }

    public void insertRecordMatch(T8DataTransaction tx, T8RecordMatch match) throws Exception
    {
        T8DataEntity recordMatchEntity;
        String entityId;
        Timestamp insertedAt;
        String insertedById;

        // Get auditing values.
        insertedById =  sessionContext.getUserIdentifier();
        insertedAt = new Timestamp(System.currentTimeMillis());
        entityId = DATA_RECORD_MATCH_DE_IDENTIFIER;

        // Create the entity.
        recordMatchEntity = tx.create(entityId, null);
        recordMatchEntity.setFieldValue(entityId + F_MATCH_INSTANCE_ID, match.getInstanceId());
        recordMatchEntity.setFieldValue(entityId + F_RECORD_ID1, match.getRecordId1());
        recordMatchEntity.setFieldValue(entityId + F_RECORD_ID2,  match.getRecordId2());
        recordMatchEntity.setFieldValue(entityId + F_MATCH_PLAN_ID, match.getCriteriaId());
        recordMatchEntity.setFieldValue(entityId + F_FAMILY_ID, match.getFamilyId());
        recordMatchEntity.setFieldValue(entityId + F_INSERTED_AT, insertedAt);
        recordMatchEntity.setFieldValue(entityId + F_INSERTED_BY_ID, insertedById);
        recordMatchEntity.setFieldValue(entityId + F_UPDATED_BY_ID, sessionContext.getUserIdentifier());
        recordMatchEntity.setFieldValue(entityId + F_UPDATED_AT, new Timestamp(System.currentTimeMillis()));

        // Insert the entity.
        tx.insert(recordMatchEntity);
    }

    public void insertHistoryRecordMatch(T8DataTransaction tx, String eventIID, T8RecordMatchEvent event, String matchIid) throws Exception
    {
        T8DataEntity recordMatchEntity;
        String entityId;
        String initiatorIid;

        if (sessionContext.isSystemSession())
        {
            initiatorIid =  sessionContext.getSystemAgentInstanceIdentifier();
        }
        else
        {
            initiatorIid = null;
        }

        entityId = HISTORY_RECORD_MATCH_DE_IDENTIFIER;

        // Create the entity.
        recordMatchEntity = tx.create(entityId, null);
        recordMatchEntity.setFieldValue(entityId + F_MATCH_INSTANCE_ID, matchIid);
        recordMatchEntity.setFieldValue(entityId + F_EVENT_IID, eventIID);
        recordMatchEntity.setFieldValue(entityId + F_EVENT,  event.toString());
        recordMatchEntity.setFieldValue(entityId + "$RECORD_COUNT" , 0);
        recordMatchEntity.setFieldValue(entityId + "$TIME",  new Timestamp(System.currentTimeMillis()));
        recordMatchEntity.setFieldValue(entityId + "$INITIATOR_ID", sessionContext.getUserIdentifier());
        recordMatchEntity.setFieldValue(entityId + "$INITIATOR_IID", initiatorIid);
        recordMatchEntity.setFieldValue(entityId + "$INITIATOR_ORG_ID", sessionContext.getOrganizationIdentifier());

        // Insert the entity.
        tx.insert(recordMatchEntity);
    }

    public boolean isRecordMatchExcluded(T8DataTransaction tx, T8RecordMatch match) throws Exception
    {
        List<T8DataEntity> matchEntities;
        T8DataFilter filter;
        String entityId;

        // Create a filter that will retrieve all matches that have been previously resolved between the two records.
        entityId =  DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;
        filter =  new T8DataFilter(entityId);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_RECORD_ID1, DataFilterOperator.EQUAL, match.getRecordId1() , false);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_RECORD_ID2, DataFilterOperator.EQUAL, match.getRecordId2() , false);

        // Retrieve all resolved matches between the records in question.
        matchEntities = tx.select(entityId, filter);
        if (matchEntities ==  null)
        {
            return true;
        }
        else
        {
            // Check each of the matches we retrieved, to see if the two records in question have been previously resolved as non-duplicates.
            for (T8DataEntity entity : matchEntities)
            {
                ResolvedState record1ResolvedState;
                ResolvedState record2ResolvedState;

                // Get the resolved state of each record.
                record1ResolvedState = ResolvedState.valueOf((String)entity.getFieldValue(entityId + F_RECORD1_RESOLVED_STATE));
                record2ResolvedState = ResolvedState.valueOf((String)entity.getFieldValue(entityId + F_RECORD2_RESOLVED_STATE));

                // If the match between the records is marked as excluded we can return the result immediately.
                if ((record1ResolvedState == EXCLUDED) || (record2ResolvedState ==  EXCLUDED))
                {
                    return true;
                }
            }

            return false;
        }
    }

    public void logRecordMatchEvent(T8DataTransaction tx, T8RecordMatchEvent event, T8RecordMatchCriteria criteria, String matchInstanceID) throws Exception
    {
        String eventIid;

        eventIid = T8IdentifierUtilities.createNewGUID();

        // Insert history of the record match.
        insertHistoryRecordMatch(tx, eventIid, event, matchInstanceID);
        insertHistoryMatchCriteria(tx, criteria, eventIid , matchInstanceID);

        // Insert match criteria match cases.
        for (T8RecordMatchCase matchCase : criteria.getCases())
        {
            insertHistoryMatchCase(tx, matchCase,eventIid , matchInstanceID);
            for ( T8RecordMatchCheck check : matchCase.getChecks())
            {
                insertHistoryMatchCheck(tx, check,eventIid, matchInstanceID);
            }
        }
    }

    private void insertHistoryMatchCriteria(T8DataTransaction tx, T8RecordMatchCriteria matchCriteria, String eventIID , String matchInstanceID) throws Exception
    {
        T8DataEntity historyMatchCriteria;
        String entityId;

        // Create an entity.
        entityId = HISTORY_RECORD_MATCH_CRITERIA_DE_IDENTIFIER;
        historyMatchCriteria = tx.create(entityId, null);
        historyMatchCriteria.setFieldValue(entityId + F_MATCH_CRITERIA_ID, matchCriteria.getId());
        historyMatchCriteria.setFieldValue(entityId + F_ORG_ID, matchCriteria.getOrgId());
        historyMatchCriteria.setFieldValue(entityId + F_NAME, matchCriteria.getName());
        historyMatchCriteria.setFieldValue(entityId + F_DESCRIPTION, matchCriteria.getDescription());
        historyMatchCriteria.setFieldValue(entityId + F_EVENT_IID, eventIID);
        historyMatchCriteria.setFieldValue(entityId + F_MATCH_INSTANCE_ID, matchInstanceID);

        // Insert the entity.
        tx.insert(historyMatchCriteria);
    }

    private void insertHistoryMatchCase(T8DataTransaction tx, T8RecordMatchCase matchCase, String eventIID, String matchInstanceID) throws Exception
    {
        T8DataEntity historyMatchCase;
        String entityId;

        // Create an entity.
        entityId = HISTORY_RECORD_MATCH_CASE_DE_IDENTIFIER;
        historyMatchCase = tx.create(entityId, null);
        historyMatchCase.setFieldValue(entityId + F_MATCH_CASE_ID, matchCase.getId());
        historyMatchCase.setFieldValue(entityId + F_EVENT_IID, eventIID);
        historyMatchCase.setFieldValue(entityId + F_MATCH_INSTANCE_ID, matchInstanceID);
        historyMatchCase.setFieldValue(entityId + F_MATCH_CRITERIA_ID, matchCase.getMatchCriteria().getId());
        historyMatchCase.setFieldValue(entityId + F_NAME, matchCase.getName());
        historyMatchCase.setFieldValue(entityId + F_DESCRIPTION, matchCase.getDescription());
        historyMatchCase.setFieldValue(entityId + F_SEQUENCE, matchCase.getIndex());
        historyMatchCase.setFieldValue(entityId + F_CONDITION_EXPRESSION, "//");
        historyMatchCase.setFieldValue(entityId + F_DATA_EXPRESSION, matchCase.getDataExpression());
        historyMatchCase.setFieldValue(entityId + F_TYPE, matchCase.getType().toString());

        // Insert the entity.
        tx.insert(historyMatchCase);
    }

    private void insertHistoryMatchCheck(T8DataTransaction tx, T8RecordMatchCheck matchCheck, String eventIID, String matchInstanceID) throws Exception
    {
        T8DataEntity historyMatchCheck;
        String entityId;

        // Create an entity.
        entityId = HISTORY_RECORD_MATCH_CHECK_DE_IDENTIFIER;
        historyMatchCheck = tx.create(entityId, null);
        historyMatchCheck.setFieldValue(entityId + F_EVENT_IID, eventIID);
        historyMatchCheck.setFieldValue(entityId + F_MATCH_CHECK_ID, matchCheck.getId());
        historyMatchCheck.setFieldValue(entityId + F_MATCH_INSTANCE_ID, matchInstanceID);
        historyMatchCheck.setFieldValue(entityId + F_MATCH_CASE_ID, matchCheck.getMatchCase().getId());
        historyMatchCheck.setFieldValue(entityId + F_MATCH_CRITERIA_ID, matchCheck.getMatchCase().getMatchCriteria().getId());
        historyMatchCheck.setFieldValue(entityId + F_NAME, matchCheck.getName());
        historyMatchCheck.setFieldValue(entityId + F_DESCRIPTION, matchCheck.getDescription());
        historyMatchCheck.setFieldValue(entityId + F_SEQUENCE, matchCheck.getIndex());
        historyMatchCheck.setFieldValue(entityId + F_TYPE, matchCheck.getType().toString());
        historyMatchCheck.setFieldValue(entityId + F_PERCENTAGE, matchCheck.getPercentage());
        historyMatchCheck.setFieldValue(entityId + F_DATA_EXPRESSION, matchCheck.getDataExpression());

        // Insert the entity.
        tx.insert(historyMatchCheck);
    }

    public void deleteMatchResults(T8DataTransaction tx, List matchInstanceIdList) throws Exception
    {
        String entityId;
        String usedEntityId;
        T8DataFilter filter;
        T8DataFilter usageFilter;
        List<T8DataEntity> usageResults;

        entityId = DATA_RECORD_MATCH_DE_IDENTIFIER;
        usedEntityId = DATA_RECORD_MATCH_RESOLVED_DE_IDENTIFIER;

        // Create a filter to see if any of the Match Instance ids have been used to resolve duplicates.
        usageFilter = new T8DataFilter(usedEntityId);
        usageFilter.addFilterCriterion(DataFilterConjunction.AND, usedEntityId + F_MATCH_INSTANCE_ID, DataFilterOperator.IN, matchInstanceIdList, false);

        // Check to ensure no Match Instance Id's have been used to resolve duplicates.
        usageResults = tx.select(usedEntityId, usageFilter);

        // If no usages are found proceed with delete, else throw exception.
        if(usageResults.isEmpty())
        {
            // Create a filter for the match instances to be deleted.
            filter = new T8DataFilter(entityId);
            filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_MATCH_INSTANCE_ID, DataFilterOperator.IN, matchInstanceIdList, false);

            // Delete match result using filter.
            tx.delete(entityId, filter);
        }
        else
        {
            throw new Exception("Cannot delete, match instance identifier has been used to resolve duplicates.");
        }
    }

    public void insertHashCodes(T8DataTransaction tx, List<T8DataRecordHashCode> hashCodes) throws Exception
    {
        List<T8DataEntity> insertEntities;
        String entityId;

        entityId = DATA_RECORD_MATCH_HASH_DE_IDENTIFIER;

        insertEntities = new ArrayList<>();
        for (T8DataRecordHashCode hashCode : hashCodes)
        {
            T8DataEntity hashEntity;

            // Create an entity.
            hashEntity = tx.create(entityId, null);
            hashEntity.setFieldValue(entityId + F_HASH_CODE_ID, T8IdentifierUtilities.createNewGUID());
            hashEntity.setFieldValue(entityId + F_MATCH_INSTANCE_ID, hashCode.getMatchInstanceId());
            hashEntity.setFieldValue(entityId + F_MATCH_CRITERIA_ID, hashCode.getMatchCriteriaId());
            hashEntity.setFieldValue(entityId + F_MATCH_CASE_ID, hashCode.getMatchCaseId());
            hashEntity.setFieldValue(entityId + F_RECORD_ID, hashCode.getRecordId());
            hashEntity.setFieldValue(entityId + F_HASH_CODE, hashCode.getHash());
            insertEntities.add(hashEntity);
        }

        // Insert the entity.
        tx.insert(insertEntities);
    }

    public void deleteHashCodes(T8DataTransaction tx, String recordId, String matchCriteriaId) throws Exception
    {
        deleteHashCodes(tx, ArrayLists.newArrayList(recordId), ArrayLists.newArrayList(matchCriteriaId));
    }

    public void deleteHashCodes(T8DataTransaction tx, List<String> recordIdList, List<String> matchCriteriaIdList) throws Exception
    {
        T8DataFilter filter;
        String entityId;

        // Get the entity to use.
        entityId = DATA_RECORD_MATCH_HASH_DE_IDENTIFIER;

        // Construct the data filter.
        filter = new T8DataFilter(entityId);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_RECORD_ID, DataFilterOperator.IN, recordIdList, false);
        if (!CollectionUtilities.isNullOrEmpty(matchCriteriaIdList)) filter.addFilterCriterion(DataFilterConjunction.AND, entityId + F_MATCH_CRITERIA_ID, DataFilterOperator.IN, matchCriteriaIdList, false);

        // Delete the Checksums
        tx.delete(entityId, filter);
    }
}
