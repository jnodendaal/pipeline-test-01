package com.pilog.t8.data.document.datarecord.sql;

import com.pilog.t8.api.T8DataRequirementApi;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.database.T8DatabaseTableGenerator;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.CompositeRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.source.T8CommonStatementHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordEntityGenerator
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordEntityGenerator.class);

    private final List<TableColumn> columns;
    private final List<String> drInstanceIds;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8DataTransaction tx;
    private final T8TerminologyApi trmApi;
    private final T8OntologyApi ontApi;
    private final TerminologyProvider terminologyProvider;
    private String connectionId;
    private String projectId;
    private String tableName;
    private String sourceTablePrefix;
    private int maximumColumnNameLength;
    private boolean logSqlStatement;

    private static final String ID_SUFFIX = "_ID";
    private static final String CODE_SUFFIX = "_CDE";
    private static final String TERM_SUFFIX = "_TRM";
    private static final String FFT_SUFFIX = "_FFT";

    public T8DataRecordEntityGenerator(T8ServerContext serverContext, T8DataTransaction tx)
    {
        this.serverContext = serverContext;
        this.context = tx.getContext();
        this.columns = new ArrayList<>();
        this.drInstanceIds = new ArrayList<>();
        this.tx = tx;
        this.ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        this.trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
        this.terminologyProvider = trmApi.getTerminologyProvider();
        this.maximumColumnNameLength = 30;
        this.logSqlStatement = false;
    }

    public void reset()
    {
        this.columns.clear();
        this.drInstanceIds.clear();
    }

    public String getConnectionId()
    {
        return connectionId;
    }

    public void setConnectionId(String connectionId)
    {
        this.connectionId = connectionId;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getSourceTablePrefix()
    {
        return sourceTablePrefix;
    }

    public void setSourceTablePrefix(String sourceTablePrefix)
    {
        this.sourceTablePrefix = sourceTablePrefix;
    }

    public int getMaximumColumnNameLength()
    {
        return maximumColumnNameLength;
    }

    public void setMaximumColumnNameLength(int maximumColumnNameLength)
    {
        this.maximumColumnNameLength = maximumColumnNameLength;
    }

    public boolean isLogSqlStatement()
    {
        return logSqlStatement;
    }

    public void setLogSqlStatement(boolean logSqlStatement)
    {
        this.logSqlStatement = logSqlStatement;
    }

    public void addInstances(T8DataFilter filter, String drInstanceIdFieldId) throws Exception
    {
        List<T8DataEntity> drInstanceEntities;
        Set<String> instanceIds;

        drInstanceEntities = tx.select(filter.getEntityIdentifier(), filter);
        instanceIds = T8DataUtilities.getEntityFieldValueSet(drInstanceEntities, drInstanceIdFieldId);
        addInstances(instanceIds);
    }

    public void addInstances(Collection<String> drInstanceIds) throws Exception
    {
        T8DataRequirementApi drApi;

        drApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
        for (String drInstanceId : drInstanceIds)
        {
            DataRequirementInstance drInstance;

            drInstance = drApi.retrieveDataRequirementInstance(drInstanceId, null, false, false);
            if (drInstance != null)
            {
                addInstance(drInstance);
            }
        }
    }

    public void addInstance(DataRequirementInstance drInstance)
    {
        DataRequirement dr;

        this.drInstanceIds.add(drInstance.getConceptID());

        dr = drInstance.getDataRequirement();
        for (PropertyRequirement propertyRequirement : dr.getPropertyRequirements())
        {
            ValueRequirement valueRequirement;

            // If no value requirement is defined for the property, do not include it.
            valueRequirement = propertyRequirement.getValueRequirement();
            if (valueRequirement != null)
            {
                RequirementType dataType;

                dataType = valueRequirement.getRequirementType();
                switch (dataType)
                {
                    case DOCUMENT_REFERENCE: continue; // Document references not added.
                    case ATTACHMENT: continue; // Attachments not added.
                    case COMPOSITE_TYPE:
                        CompositeRequirement compositeRequirement;

                        compositeRequirement = (CompositeRequirement)valueRequirement;
                        for (FieldRequirement fieldRequirement : compositeRequirement.getFieldRequirements())
                        {
                            ValueRequirement fieldValueRequirement;

                            fieldValueRequirement = fieldRequirement.getFirstSubRequirement();
                            if (fieldValueRequirement != null)
                            {
                                RequirementType fieldDataType;
                                TableColumn column;
                                String propertyId;
                                String fieldId;

                                propertyId = propertyRequirement.getConceptID();
                                fieldId = fieldRequirement.getFieldID();
                                fieldDataType = fieldValueRequirement.getRequirementType();
                                column = getTableColumn(propertyId, fieldId);
                                if (column == null)
                                {
                                    column = new TableColumn(propertyId, fieldId, generateColumnName(propertyId, fieldId, maximumColumnNameLength - 4), fieldDataType); // Allow 4 additional character suffix for _ID, _TRM, _CDE and _FFT.
                                    columns.add(column);
                                }
                            }
                        }

                        break;
                    default:
                        TableColumn column;
                        String propertyId;

                        propertyId = propertyRequirement.getConceptID();
                        column = getTableColumn(propertyId, null);
                        if (column == null)
                        {
                            column = new TableColumn(propertyId, null, generateColumnName(propertyId, null, maximumColumnNameLength - 4), dataType); // Allow 4 additional character suffix for _FFT.
                            columns.add(column);
                        }
                        break;
                }
            }
        }
    }

    private TableColumn getTableColumn(String propertyId, String fieldId)
    {
        for (TableColumn column : columns)
        {
            if (column.getPropertyId().equals(propertyId))
            {
                if ((fieldId == null) || (fieldId.equals(column.getFieldId())))
                {
                    return column;
                }
            }
        }

        return null;
    }

    private String generateColumnName(String propertyId, String fieldId, int maxLength)
    {
        if (fieldId != null)
        {
            String propertyTerm;
            String fieldTerm;
            String columnName;

            propertyTerm = terminologyProvider.getTerm(null, propertyId);
            if (propertyTerm == null) propertyTerm = propertyId;

            fieldTerm = terminologyProvider.getTerm(null, fieldId);
            if (fieldTerm == null) fieldTerm = fieldId;

            columnName = propertyTerm  + "_" + fieldTerm;
            return formatColumnName(columnName, maxLength);
        }
        else
        {
            String columnName;

            columnName = terminologyProvider.getTerm(null, propertyId);
            if (columnName == null) return propertyId;
            return formatColumnName(columnName, maxLength);
        }
    }

    private String formatColumnName(String identifier, int maxLength)
    {
        String columnName;

        // Do a few replacements and transformations on the string.
        columnName = identifier;
        columnName = columnName.replace('/', '_');
        columnName = columnName.replace(':', '_');

        // Create an identifier from the string.
        columnName = T8IdentifierUtilities.createIdentifierString(columnName);
        if (columnName.length() <= maxLength)
        {
            return columnName;
        }
        else
        {
            columnName = T8IdentifierUtilities.stripVowels(columnName, true);
            if (columnName.length() <= maxLength)
            {
                columnName = T8IdentifierUtilities.shortenIdentifier(columnName, maxLength);
                return columnName;
            }
            else return columnName.substring(0, maxLength);
        }
    }

    public int generateEntity() throws Exception
    {
        T8TableDataSourceDefinition sourceDefinition;
        T8DataEntityDefinition entityDefinition;
        T8DefinitionManager definitionManager;
        T8DatabaseTableGenerator tableGenerator;
        ParameterizedString inStatement;

        // Generate the new entity definition.
        definitionManager = serverContext.getDefinitionManager();
        sourceDefinition = generateDataSourceDefinition();
        entityDefinition = generateEntityDefinition(sourceDefinition);
        definitionManager.saveDefinition(context, sourceDefinition, null);
        definitionManager.saveDefinition(context, entityDefinition, null);

        // Initialize the new definitions.
        definitionManager.initializeDefinition(context, sourceDefinition, null);
        definitionManager.initializeDefinition(context, entityDefinition, null);

        // Use the new entity definition to generate the destination table.
        tableGenerator = new T8DatabaseTableGenerator(tx);
        tableGenerator.generateTable(sourceDefinition, T8DatabaseTableGenerator.TableGenerationType.DROP_EXISTING);

        // Generate the SQL statement to populate the new table.
        inStatement = generateInsertStatement();
        System.out.println(inStatement);

        // Execute the insert statement.
        return T8CommonStatementHandler.executeUpdate(context, tx.getDataConnection(connectionId), inStatement, sourceDefinition);
    }

    public T8DataEntityDefinition generateEntityDefinition(T8DataSourceDefinition sourceDefinition)
    {
        T8DataEntityDefinition entityDefinition;
        String entityId;

        entityId = T8Definition.getPublicIdPrefix() + T8DataEntityDefinition.IDENTIFIER_PREFIX + tableName;
        entityId = entityId.toUpperCase();
        entityDefinition = new T8DataEntityDefinition(entityId);
        entityDefinition.setProjectIdentifier(projectId);
        entityDefinition.setDataSourceIdentifier(sourceDefinition.getIdentifier());
        for (T8DataSourceFieldDefinition sourceFieldDefinition : sourceDefinition.getFieldDefinitions())
        {
            String type;
            boolean key;
            boolean required;

            key = sourceFieldDefinition.isKey();
            required = sourceFieldDefinition.isRequired();
            type = sourceFieldDefinition.getDataTypeString();
            entityDefinition.addFieldDefinition(new T8DataEntityFieldDefinition(sourceFieldDefinition.getIdentifier(), sourceFieldDefinition.getPublicIdentifier(), key, required, type));
        }

        return entityDefinition;
    }

    public T8TableDataSourceDefinition generateDataSourceDefinition()
    {
        T8TableDataSourceDefinition sourceDefinition;
        String dsId;

        dsId = T8Definition.getPublicIdPrefix() + T8DataSourceDefinition.IDENTIFIER_PREFIX + tableName;
        dsId = dsId.toUpperCase();
        sourceDefinition = new T8TableDataSourceDefinition(dsId);
        sourceDefinition.setProjectIdentifier(projectId);
        sourceDefinition.setConnectionIdentifier(connectionId);
        sourceDefinition.setTableName(tableName);
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$ROOT_RECORD_ID", "ROOT_RECORD_ID", false, false, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$PARENT_RECORD_ID", "PARENT_RECORD_ID", false, false, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$RECORD_ID", "RECORD_ID", true, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$RECORD_CDE", "RECORD_CDE", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$DR_ID", "DR_ID", false, false, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$DR_INSTANCE_ID", "DR_INSTANCE_ID", false, false, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$DR_INSTANCE_TRM", "DR_INSTANCE_TRM", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$DR_INSTANCE_CDE", "DR_INSTANCE_CDE", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$ORG_ID", "ORG_ID", false, false, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$ORG_TRM", "ORG_TRM", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$ORG_CDE", "ORG_CDE", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$LANGUAGE_ID", "LANGUAGE_ID", true, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$LANGUAGE_CDE", "LANGUAGE_CDE", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$LANGUAGE_TRM", "LANGUAGE_TRM", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$INSERTED_AT", "INSERTED_AT", false, false, T8DataType.TIMESTAMP));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$INSERTED_BY_ID", "INSERTED_BY_ID", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$INSERTED_BY_IID", "INSERTED_BY_IID", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$UPDATED_AT", "UPDATED_AT", false, false, T8DataType.TIMESTAMP));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$UPDATED_BY_ID", "UPDATED_BY_ID", false, false, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$UPDATED_BY_IID", "UPDATED_BY_IID", false, false, T8DataType.STRING));

        for (TableColumn column : columns)
        {
            String columnName;

            columnName = column.getColumnName();
            switch (column.getType())
            {
                case DATE_TYPE:
                case DATE_TIME_TYPE:
                    sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$" + columnName, columnName, false, false, T8DataType.TIMESTAMP));
                    break;
                case CONTROLLED_CONCEPT:
                    sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$" + columnName + ID_SUFFIX, columnName + ID_SUFFIX, false, false, T8DataType.GUID));
                    sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$" + columnName + CODE_SUFFIX, columnName + CODE_SUFFIX, false, false, T8DataType.STRING));
                    sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$" + columnName + TERM_SUFFIX, columnName + TERM_SUFFIX, false, false, T8DataType.STRING));
                    break;
                default:
                    sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$" + columnName, columnName, false, false, T8DataType.STRING));
                    break;
            }

            // Add the fft field for the column.
            sourceDefinition.addFieldDefinition(new T8DataSourceFieldDefinition("$" + columnName + FFT_SUFFIX, columnName + FFT_SUFFIX, false, false, T8DataType.STRING));
        }

        return sourceDefinition;
    }

    public ParameterizedString generateInsertStatement()
    {
        T8SessionContext sessionContext;
        ParameterizedString sql;
        T8DatabaseAdaptor dbAdaptor;
        String languageId;
        String orgId;
        String osId;

        // Get the ontology id required.
        sessionContext = context.getSessionContext();
        osId = ontApi.getOntologyStructure().getID();
        orgId = sessionContext.getRootOrganizationIdentifier();
        languageId = sessionContext.getContentLanguageIdentifier();

        // Get the database adaptor.
        dbAdaptor = tx.getDataConnection(connectionId).getDatabaseAdaptor();

        // Create the WITH clause that selects all record values.
        sql = new ParameterizedString();
        sql.append("WITH RECORD_VALUES AS (SELECT ");
        sql.append("REC_DOC.RECORD_ID");

        // Add the selection of all table columns.
        for (int index = 0; index < columns.size(); index++)
        {
            TableColumn column;

            column = columns.get(index);
            switch (column.getType())
            {
                case CONTROLLED_CONCEPT:
                case UNIT_OF_MEASURE:
                case QUALIFIER_OF_MEASURE:
                    // Add id selection.
                    sql.append(",MAX(CASE WHEN REC_VAL.PROPERTY_ID=");
                    sql.append(dbAdaptor.getSQLHexToGUID(column.getPropertyId()));
                    if (column.isField())
                    {
                        sql.append(" AND REC_VAL.FIELD_ID=");
                        sql.append(dbAdaptor.getSQLHexToGUID(column.getFieldId()));
                    }
                    sql.append(" AND REC_VAL.DATA_TYPE_ID='" + column.getType() + "'");
                    sql.append(" THEN CAST(REC_VAL.VALUE_CONCEPT_ID AS VARCHAR(36)) ELSE NULL END) AS ");
                    sql.append("\"");
                    sql.append(column.getColumnName() + ID_SUFFIX);
                    sql.append("\"");

                    // Add code selection.
                    sql.append(",MAX(CASE WHEN REC_VAL.PROPERTY_ID=");
                    sql.append(dbAdaptor.getSQLHexToGUID(column.getPropertyId()));
                    if (column.isField())
                    {
                        sql.append(" AND REC_VAL.FIELD_ID=");
                        sql.append(dbAdaptor.getSQLHexToGUID(column.getFieldId()));
                    }
                    sql.append(" AND REC_VAL.DATA_TYPE_ID='" + column.getType() + "'");
                    sql.append(" THEN VAL_TRM.CODE ELSE NULL END) AS ");
                    sql.append("\"");
                    sql.append(column.getColumnName() + CODE_SUFFIX);
                    sql.append("\"");

                    // Add term selection.
                    sql.append(",MAX(CASE WHEN REC_VAL.PROPERTY_ID=");
                    sql.append(dbAdaptor.getSQLHexToGUID(column.getPropertyId()));
                    if (column.isField())
                    {
                        sql.append(" AND REC_VAL.FIELD_ID=");
                        sql.append(dbAdaptor.getSQLHexToGUID(column.getFieldId()));
                    }
                    sql.append(" AND REC_VAL.DATA_TYPE_ID='" + column.getType() + "'");
                    sql.append(" THEN VAL_TRM.TERM ELSE NULL END) AS ");
                    sql.append("\"");
                    sql.append(column.getColumnName() + TERM_SUFFIX);
                    sql.append("\"");
                    break;
                case TEXT_TYPE:
                    sql.append(",MAX(CASE WHEN REC_VAL.PROPERTY_ID=");
                    sql.append(dbAdaptor.getSQLHexToGUID(column.getPropertyId()));
                    if (column.isField())
                    {
                        sql.append(" AND REC_VAL.FIELD_ID=");
                        sql.append(dbAdaptor.getSQLHexToGUID(column.getFieldId()));
                    }
                    sql.append(" AND REC_VAL.DATA_TYPE_ID='" + column.getType() + "'");
                    sql.append(" THEN REC_VAL.TEXT ELSE NULL END) AS ");
                    sql.append("\"");
                    sql.append(column.getColumnName());
                    sql.append("\"");
                    break;
                case DATE_TYPE:
                case DATE_TIME_TYPE:
                    sql.append(",MAX(CASE WHEN REC_VAL.PROPERTY_ID=");
                    sql.append(dbAdaptor.getSQLHexToGUID(column.getPropertyId()));
                    if (column.isField())
                    {
                        sql.append(" AND REC_VAL.FIELD_ID=");
                        sql.append(dbAdaptor.getSQLHexToGUID(column.getFieldId()));
                    }
                    sql.append(" AND REC_VAL.DATA_TYPE_ID='" + column.getType() + "'");
                    sql.append(" THEN ");
                        sql.append(" CASE WHEN (ABS(REC_VAL.VALUE) > 2147483647999)"); // Account for the year 2038 problem.
                        sql.append(" THEN ? ELSE ", new Timestamp(2147483647999L));
                        sql.append(dbAdaptor.getSQLColumnMillisecondsToTimestamp("REC_VAL.VALUE"));
                        sql.append(" END ");
                    sql.append(" ELSE NULL END) AS ");
                    sql.append("\"");
                    sql.append(column.getColumnName());
                    sql.append("\"");
                    break;
                default:
                    sql.append(",MAX(CASE WHEN REC_VAL.PROPERTY_ID=");
                    sql.append(dbAdaptor.getSQLHexToGUID(column.getPropertyId()));
                    if (column.isField())
                    {
                        sql.append(" AND REC_VAL.FIELD_ID=");
                        sql.append(dbAdaptor.getSQLHexToGUID(column.getFieldId()));
                    }
                    sql.append(" AND REC_VAL.DATA_TYPE_ID='" + column.getType() + "'");
                    sql.append(" THEN REC_VAL.VALUE ELSE NULL END) AS ");
                    sql.append("\"");
                    sql.append(column.getColumnName());
                    sql.append("\"");
                    break;
            }

            // Add fft selection (applicable to all properties).
            sql.append(",MAX(CASE WHEN REC_PRP.PROPERTY_ID=");
            sql.append(dbAdaptor.getSQLHexToGUID(column.getPropertyId()));
            sql.append(" THEN REC_PRP.FFT ELSE NULL END) AS ");
            sql.append("\"");
            sql.append(column.getColumnName() + FFT_SUFFIX);
            sql.append("\"");
        }

        // Add the joins for the WITH clause.
        sql.append(" FROM " + sourceTablePrefix + "DATA_RECORD_DOC REC_DOC");
        sql.append(" LEFT JOIN " + sourceTablePrefix + "DATA_RECORD_PROPERTY REC_PRP");
        sql.append(" ON REC_PRP.RECORD_ID = REC_DOC.RECORD_ID");
        sql.append(" LEFT JOIN " + sourceTablePrefix + "DATA_RECORD_VALUE REC_VAL");
        sql.append(" ON REC_VAL.RECORD_ID = REC_DOC.RECORD_ID");
        sql.append(" AND REC_VAL.PROPERTY_ID = REC_PRP.PROPERTY_ID");
        sql.append(" LEFT JOIN " + sourceTablePrefix + "ORG_TERMINOLOGY VAL_TRM ");
        sql.append(" ON VAL_TRM.CONCEPT_ID = REC_VAL.VALUE_CONCEPT_ID");
        sql.append(" AND VAL_TRM.ORG_ID = ");
        sql.append(dbAdaptor.getSQLHexToGUID(orgId));
        sql.append(" AND VAL_TRM.LANGUAGE_ID = ");
        sql.append(dbAdaptor.getSQLHexToGUID(languageId));

        // Append the WITH clause's WHERE clause.
        sql.append(" WHERE REC_DOC.DR_INSTANCE_ID IN (");
        for (int index = 0; index < drInstanceIds.size(); index++)
        {
            if (index > 0) sql.append(",");
            sql.append(dbAdaptor.getSQLHexToGUID(drInstanceIds.get(index)));
        }
        sql.append(") GROUP BY REC_DOC.RECORD_ID)");

        // Create the default columns.
        sql.append("INSERT INTO ");
        sql.append(tableName);
        sql.append(" (");
        sql.append("ORG_ID");
        sql.append(",ORG_TRM");
        sql.append(",ORG_CDE");
        sql.append(",RECORD_ID");
        sql.append(",RECORD_CDE");
        sql.append(",PARENT_RECORD_ID");
        sql.append(",ROOT_RECORD_ID");
        sql.append(",DR_ID");
        sql.append(",DR_INSTANCE_ID");
        sql.append(",DR_INSTANCE_TRM");
        sql.append(",DR_INSTANCE_CDE");
        sql.append(",LANGUAGE_ID");
        sql.append(",LANGUAGE_CDE");
        sql.append(",LANGUAGE_TRM");
        sql.append(",INSERTED_AT");
        sql.append(",INSERTED_BY_ID");
        sql.append(",INSERTED_BY_IID");
        sql.append(",UPDATED_AT");
        sql.append(",UPDATED_BY_ID");
        sql.append(",UPDATED_BY_IID");

        // Add all property column names.
        for (TableColumn column : columns)
        {
            switch (column.getType())
            {
                case CONTROLLED_CONCEPT:
                    sql.append(",\"");
                    sql.append(column.getColumnName() + ID_SUFFIX);
                    sql.append("\",\"");
                    sql.append(column.getColumnName() + CODE_SUFFIX);
                    sql.append("\",\"");
                    sql.append(column.getColumnName() + TERM_SUFFIX);
                    sql.append("\"");
                    break;
                default:
                    sql.append(",\"");
                    sql.append(column.getColumnName());
                    sql.append("\"");
                    break;
            }

            // Add fft column.
            sql.append(",\"");
            sql.append(column.getColumnName() + FFT_SUFFIX);
            sql.append("\"");
        }

        // Add the selection of all default columns
        sql.append(") SELECT ");
        sql.append("ORG_ONT.ORG_ID");
        sql.append(",ORG_TRM.TERM AS ORG_TRM");
        sql.append(",ORG_TRM.CODE AS ORG_CDE");
        sql.append(",REC_DOC.RECORD_ID");
        sql.append(",REC_TRM.CODE AS RECORD_CDE");
        sql.append(",REC_DOC.PARENT_RECORD_ID");
        sql.append(",REC_DOC.ROOT_RECORD_ID");
        sql.append(",REC_DOC.DR_ID");
        sql.append(",REC_DOC.DR_INSTANCE_ID AS DR_INSTANCE_ID");
        sql.append(",INS_TRM.TERM AS DR_INSTANCE_TRM");
        sql.append(",INS_TRM.CODE AS DR_INSTANCE_CDE");
        sql.append(",ORG_TRM.LANGUAGE_ID");
        sql.append(",LAN_TRM.CODE AS LANGUAGE_CDE");
        sql.append(",LAN_TRM.TERM AS LANGUAGE_TRM");
        sql.append(",REC_DOC.INSERTED_AT");
        sql.append(",REC_DOC.INSERTED_BY_ID");
        sql.append(",REC_DOC.INSERTED_BY_IID");
        sql.append(",REC_DOC.UPDATED_AT");
        sql.append(",REC_DOC.UPDATED_BY_ID");
        sql.append(",REC_DOC.UPDATED_BY_IID");

        // Add all property column names.
        for (TableColumn column : columns)
        {
            switch (column.getType())
            {
                case CONTROLLED_CONCEPT:
                    sql.append(",\"");
                    sql.append(column.getColumnName() + ID_SUFFIX);
                    sql.append("\",\"");
                    sql.append(column.getColumnName() + CODE_SUFFIX);
                    sql.append("\",\"");
                    sql.append(column.getColumnName() + TERM_SUFFIX);
                    sql.append("\"");
                    break;
                default:
                    sql.append(",\"");
                    sql.append(column.getColumnName());
                    sql.append("\"");
                    break;
            }

            sql.append(",\"");
            sql.append(column.getColumnName() + FFT_SUFFIX);
            sql.append("\"");
        }

        // Append from clause.
        sql.append(" FROM RECORD_VALUES REC_VAL");
        sql.append(" INNER JOIN " + sourceTablePrefix + "DATA_RECORD_DOC REC_DOC ");
        sql.append(" ON REC_DOC.RECORD_ID = REC_VAL.RECORD_ID");
        sql.append(" LEFT JOIN " + sourceTablePrefix + "ORG_ONTOLOGY ORG_ONT ");
        sql.append(" ON ORG_ONT.CONCEPT_ID = REC_DOC.RECORD_ID");
        sql.append(" AND ORG_ONT.ODS_ID = ");
        sql.append(dbAdaptor.getSQLHexToGUID(osId));
        sql.append(" LEFT JOIN " + sourceTablePrefix + "ORG_TERMINOLOGY ORG_TRM ");
        sql.append(" ON ORG_TRM.CONCEPT_ID = ORG_ONT.ORG_ID");
        sql.append(" LEFT JOIN " + sourceTablePrefix + "ORG_TERMINOLOGY REC_TRM ");
        sql.append(" ON REC_TRM.CONCEPT_ID = REC_DOC.RECORD_ID");
        sql.append(" AND REC_TRM.ORG_ID = ORG_TRM.ORG_ID");
        sql.append(" AND REC_TRM.LANGUAGE_ID = ORG_TRM.LANGUAGE_ID");
        sql.append(" LEFT JOIN " + sourceTablePrefix + "ORG_TERMINOLOGY INS_TRM ");
        sql.append(" ON INS_TRM.CONCEPT_ID = REC_DOC.DR_INSTANCE_ID");
        sql.append(" AND INS_TRM.ORG_ID = ORG_TRM.ORG_ID");
        sql.append(" AND INS_TRM.LANGUAGE_ID = ORG_TRM.LANGUAGE_ID");
        sql.append(" LEFT JOIN " + sourceTablePrefix + "ORG_TERMINOLOGY LAN_TRM ");
        sql.append(" ON LAN_TRM.CONCEPT_ID = ORG_TRM.LANGUAGE_ID");
        sql.append(" AND LAN_TRM.ORG_ID = ORG_TRM.ORG_ID");
        sql.append(" AND LAN_TRM.LANGUAGE_ID = ORG_TRM.LANGUAGE_ID");

        // Append the where clause.
        sql.append(" WHERE ORG_TRM.ORG_ID = ");
        sql.append(dbAdaptor.getSQLHexToGUID(orgId));
        sql.append(" AND ORG_TRM.LANGUAGE_ID = ");
        sql.append(dbAdaptor.getSQLHexToGUID(languageId));

        // Return the final statement.
        if (logSqlStatement) LOGGER.log(sql.toString());
        return sql;
    }

    private class TableColumn
    {
        private String propertyId;
        private String fieldId;
        private String columnName;
        private RequirementType type;

        public TableColumn(String propertyId, String fieldId, String columnName, RequirementType type)
        {
            this.propertyId = propertyId;
            this.fieldId = fieldId;
            this.columnName = columnName;
            this.type = type;
        }

        public boolean isField()
        {
            return fieldId != null;
        }

        public String getPropertyId()
        {
            return propertyId;
        }

        public void setPropertyId(String propertyId)
        {
            this.propertyId = propertyId;
        }

        public String getFieldId()
        {
            return fieldId;
        }

        public void setFieldId(String fieldId)
        {
            this.fieldId = fieldId;
        }

        public String getColumnName()
        {
            return columnName;
        }

        public void setColumnName(String columnName)
        {
            this.columnName = columnName;
        }

        public RequirementType getType()
        {
            return type;
        }

        public void setType(RequirementType type)
        {
            this.type = type;
        }
    }
}
