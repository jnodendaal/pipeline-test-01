package com.pilog.t8.api;

import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.match.T8DataRecordHashCode;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCriteria;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRecordMatchApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordMatchApiOperations
{
    public static class ApiRetrieveMatchCriteria extends T8DefaultServerOperation
    {
        public ApiRetrieveMatchCriteria(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<RecordValue>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordMatchApi matchApi;
            T8RecordMatchCriteria criteria;
            String criteriaID;

            // Get the operation parameters we need.
            criteriaID = (String)operationParameters.get(PARAMETER_CRITERIA_ID);

            // Perform the API operation.
            matchApi = tx.getApi(T8DataRecordMatchApi.API_IDENTIFIER);
            criteria = matchApi.retrieveMatchCriteria(criteriaID);
            return HashMaps.newHashMap(PARAMETER_MATCH_CRITERIA, criteria);
        }
    }

    public static class ApiMatchDataFiles extends T8DefaultServerOperation
    {
        public ApiMatchDataFiles(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8RecordMatchCriteria criteriaObject;
            T8DataRecordMatchApi matchApi;
            T8DataFilter filter;
            String criteriaId;
            String matchIid;

            // Get the operation parameters we need.
            criteriaId = (String)operationParameters.get(PARAMETER_CRITERIA_ID);
            criteriaObject = (T8RecordMatchCriteria)operationParameters.get(PARAMETER_MATCH_CRITERIA);
            filter = (T8DataFilter)operationParameters.get(PARAMETER_FILTER);

            // Get the api.
            matchApi = tx.getApi(T8DataRecordMatchApi.API_IDENTIFIER);

            // Use the Criteria Object Parameter or the Criteria ID
            if (criteriaId != null)
            {
                matchIid = matchApi.matchFiles(criteriaId, filter, F_ROOT_RECORD_ID);
            }
            else
            {
                matchIid = matchApi.matchFiles(criteriaObject, filter, F_ROOT_RECORD_ID);
            }

            // Return the match Instance ID
            return HashMaps.createSingular(PARAMETER_MATCH_INSTANCE_ID, matchIid);
        }
    }

    public static class ApiMatchDataFileHashCodes extends T8DefaultServerOperation
    {
        public ApiMatchDataFileHashCodes(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordMatchApi matchApi;
            T8DataFilter filter;
            String matchIid;

            // Get the operation parameters we need.
            filter = (T8DataFilter)operationParameters.get(PARAMETER_FILTER);

            // Get the api.
            matchApi = tx.getApi(T8DataRecordMatchApi.API_IDENTIFIER);

            // Use the specified filter to match files in the result set using their hash codes.
            matchIid = matchApi.matchFileHashCodes(filter);

            // Return the match Instance ID
            return HashMaps.createSingular(PARAMETER_MATCH_INSTANCE_ID, matchIid);
        }
    }

    public static class ApiDeleteMatchCriteria extends T8DefaultServerOperation
    {
        public ApiDeleteMatchCriteria(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordMatchApi matchApi;
            String criteriaID;

            // Get the operation parameter we need.
            criteriaID = (String)operationParameters.get(PARAMETER_CRITERIA_ID);

            // Perform the API operation.
            matchApi = tx.getApi(T8DataRecordMatchApi.API_IDENTIFIER);
            matchApi.deleteMatchCriteria(criteriaID);

            // Nothing to return.
            return null;
        }
    }

    public static class ApiSaveMatchCriteria extends T8DefaultServerOperation
    {
        public ApiSaveMatchCriteria(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordMatchApi matchApi;
            T8RecordMatchCriteria criteria;

            // Get the operation parameter we need.
            criteria = (T8RecordMatchCriteria)operationParameters.get(PARAMETER_NEW_CRITERIA);

            // Perform the API operation.
            matchApi = tx.getApi(T8DataRecordMatchApi.API_IDENTIFIER);
            matchApi.saveMatchCriteria(criteria);

            // Nothing to return.
            return null;
        }
    }

    public static class ApiGenerateFileHashCodes extends T8DefaultServerOperation
    {
        public ApiGenerateFileHashCodes(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<RecordValue>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8RecordMatchCriteria criteriaObject;
            List<T8DataRecordHashCode> hashCodes;
            T8DataRecordMatchApi matchApi;
            T8DataFilter filter;
            String criteriaId;

            // Get the operation parameters we need.
            criteriaId = (String)operationParameters.get(PARAMETER_CRITERIA_ID);
            criteriaObject = (T8RecordMatchCriteria)operationParameters.get(PARAMETER_MATCH_CRITERIA);
            filter = (T8DataFilter)operationParameters.get(PARAMETER_FILTER);

            // Get the api.
            matchApi = tx.getApi(T8DataRecordMatchApi.API_IDENTIFIER);

            // Use the Criteria Object Parameter or the Criteria ID
            if (criteriaId != null)
            {
                hashCodes = matchApi.refreshFileHashCodes(criteriaId, filter, F_ROOT_RECORD_ID);
            }
            else
            {
                hashCodes = matchApi.refreshFileHashCodes(criteriaObject, filter, F_ROOT_RECORD_ID);
            }

            // Return the match Instance ID
            return HashMaps.newHashMap(PARAMETER_HASH_CODE_LIST, hashCodes);
        }
    }

    public static class ApiDeleteMatchResults extends T8DefaultServerOperation
    {
        public ApiDeleteMatchResults(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordMatchApi matchApi;
            List matchIidList;

            // Get the operation parameter we need.
            matchIidList = (List)operationParameters.get(PARAMETER_MATCH_INSTANCE_ID_LIST);

            // Perform the API operation.
            matchApi = tx.getApi(T8DataRecordMatchApi.API_IDENTIFIER);
            matchApi.deleteMatchResults(matchIidList);

            // Nothing to return.
            return null;
        }
    }
}