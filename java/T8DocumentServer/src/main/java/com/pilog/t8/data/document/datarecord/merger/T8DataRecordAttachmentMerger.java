package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.AttachmentDetailSource;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.AttachmentMatchType;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.definition.data.document.datarecord.merger.T8AttachmentFilenameConversionScriptDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordAttachmentMergerDefinition;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordAttachmentMerger implements T8DataRecordValueMerger
{
    private final String identifier;
    private final Set<DataRecord> newDataRecords;
    private final Set<DataRecord> updatedDataRecords;
    private final Map<String, String> attachmentIdentifierMapping; // Used to store attachment ID's mapped to newly assigned identifiers in destination documents.
    private final T8AttachmentFilenameConversionScriptDefinition filenameConversionScriptDefinition;
    private final boolean assignNewAttachmentIdentifiers;
    private final DataRecordProvider sourceRecordProvider;
    private final DataRecordProvider destinationRecordProvider;
    private final AttachmentMatchType attachmentMatchType;
    private final AttachmentDetailSource attachmentDetailSource;
    private final T8DataRecordMergeContext mergeContext;
    private T8ServerContextScript filenameConversionScript;

    public T8DataRecordAttachmentMerger(T8DataRecordMergeContext mergeContext, T8DataRecordMergeConfiguration config, String identifier)
    {
        this.identifier = identifier;
        this.mergeContext = mergeContext;
        this.attachmentMatchType = config.getAttachmentMatchType();
        this.attachmentDetailSource = config.getAttachmentDetailSource();
        this.assignNewAttachmentIdentifiers = config.isMergeAssignNewAttachmentIdentifiers();
        this.sourceRecordProvider = config.getSourceRecordProvider();
        this.destinationRecordProvider = config.getDestinationRecordProvider();
        this.newDataRecords = new HashSet<DataRecord>();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.attachmentIdentifierMapping = new HashMap<String, String>();
        this.filenameConversionScriptDefinition = null;
        this.filenameConversionScript = null;
    }

    public T8DataRecordAttachmentMerger(T8DataRecordMergeContext mergeContext, T8DataRecordAttachmentMergerDefinition definition)
    {
        this.identifier = definition.getIdentifier();
        this.mergeContext = mergeContext;
        this.attachmentMatchType = definition.getAttachmentMatchType();
        this.attachmentDetailSource = definition.getAttachmentDetailSource();
        this.assignNewAttachmentIdentifiers = definition.isAssignNewAttachmentIdentifiers();
        this.sourceRecordProvider = null;
        this.destinationRecordProvider = null;
        this.newDataRecords = new HashSet<DataRecord>();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.attachmentIdentifierMapping = new HashMap<String, String>();
        this.filenameConversionScriptDefinition = definition.getFilenameConversionScriptDefinition();
    }

    @Override
    public void reset()
    {
        // Clear this merger's collections.
        newDataRecords.clear();
        updatedDataRecords.clear();
        attachmentIdentifierMapping.clear();
    }

    @Override
    public Map<String, String> getRecordIdMapping()
    {
        return new HashMap<>();
    }

    @Override
    public Map<String, String> getAttachmentIdentifierMapping()
    {
        return new HashMap<String, String>(attachmentIdentifierMapping);
    }

    @Override
    public Set<DataRecord> getNewDataRecordSet()
    {
        return new HashSet<DataRecord>(newDataRecords);
    }

    @Override
    public Set<DataRecord> getUpdatedDataRecordSet()
    {
        return new HashSet<DataRecord>(updatedDataRecords);
    }

    @Override
    public boolean isApplicable(RecordValue destinationValue, RecordValue sourceValue)
    {
        return (destinationValue.getValueRequirement().getRequirementType() == RequirementType.ATTACHMENT);
    }

    @Override
    public void mergeRecordValues(RecordValue destinationValue, RecordValue sourceValue)
    {
        RequirementType requirementType;

        // Get the source requirement type.
        requirementType = sourceValue.getValueRequirement().getRequirementType();
        if (requirementType == RequirementType.ATTACHMENT)
        {
            AttachmentList sourceAttachmentList;
            AttachmentList destinationAttachmentList;
            Map<String, T8AttachmentDetails> sourceRecordAttachmentDetailsMap;
            Map<String, T8AttachmentDetails> destinationRecordAttachmentDetails;

            // Get the two attachment lists involved.
            sourceAttachmentList = (AttachmentList)sourceValue;
            destinationAttachmentList = (AttachmentList)destinationValue;

            // If required for the matching type of the attachments, get the source and destination attachment details.
            if (attachmentDetailSource == T8DefaultDataRecordMerger.AttachmentDetailSource.DATA_RECORD_PROVIDER)
            {
                if (sourceRecordProvider == null) throw new RuntimeException("No source record provider set for merge operation.");
                if (destinationRecordProvider == null) throw new RuntimeException("No destination record provider set for merge operation.");
                sourceRecordAttachmentDetailsMap = sourceRecordProvider.getRecordAttachmentDetails(sourceValue.getParentDataRecord().getID());
                destinationRecordAttachmentDetails = destinationRecordProvider.getRecordAttachmentDetails(destinationValue.getParentDataRecord().getID());
            }
            else
            {
                DataRecord sourceRecord;
                DataRecord destinationRecord;

                sourceRecord = sourceValue.getParentDataRecord();
                destinationRecord = destinationValue.getParentDataRecord();
                sourceRecordAttachmentDetailsMap = sourceRecord != null ? sourceRecord.getAttachmentDetails() : new HashMap<String, T8AttachmentDetails>();
                destinationRecordAttachmentDetails = destinationRecord != null ? destinationRecord.getAttachmentDetails() : new HashMap<String, T8AttachmentDetails>();
            }

            // Get all source attachments and for each one try to find a match in the destination.
            for (String sourceAttachmentID : sourceAttachmentList.getAttachmentIds())
            {
                String matchingDestinationAttachmentID;
                T8AttachmentDetails sourceAttachmentDetails;

                // Get the source attachment ID and file details.
                sourceAttachmentDetails = sourceRecordAttachmentDetailsMap.get(sourceAttachmentID);

                // Try to find an attachment in the destination value that matches the source attachment.
                matchingDestinationAttachmentID = null;
                for (String destinationAttachmentID : destinationAttachmentList.getAttachmentIds())
                {
                    T8AttachmentDetails destinationAttachmentDetails;

                    // Get the destination attachment ID and file details.
                    destinationAttachmentDetails = destinationRecordAttachmentDetails.get(destinationAttachmentID);

                    // Check the match depending on the match type.
                    if (attachmentMatchType == T8DefaultDataRecordMerger.AttachmentMatchType.CHECKSUM)
                    {
                        // Compare the attachment MD5 Checksums to see if the files match.
                        if (Objects.equals(sourceAttachmentDetails.getMD5Checksum(), destinationAttachmentDetails.getMD5Checksum()))
                        {
                            matchingDestinationAttachmentID = destinationAttachmentID;
                            break;
                        }
                    }
                    else // Default to Attachment ID matching only.
                    {
                        // Compare the attachment ID's to see if the files match.
                        if (Objects.equals(sourceAttachmentID, destinationAttachmentID))
                        {
                            matchingDestinationAttachmentID = destinationAttachmentID;
                            break;
                        }
                    }
                }

                // If we couldn't find a match, we need to add the attachment to the destination value.
                if (matchingDestinationAttachmentID == null)
                {
                    T8AttachmentDetails newAttachmentDetails;
                    String convertedOriginalFileName;
                    String newAttachmentId;

                    // Add the new attachment to the destination value.
                    newAttachmentId = assignNewAttachmentIdentifiers ? T8IdentifierUtilities.createNewGUID() : sourceAttachmentID;
                    destinationAttachmentList.addAttachment(newAttachmentId);

                    // Add the mapping from the source attachment ID to the new ID created in the destination.
                    attachmentIdentifierMapping.put(sourceAttachmentID, newAttachmentId);

                    // Convert the filename.
                    convertedOriginalFileName = getConvertedFilename(destinationValue, sourceValue, sourceAttachmentDetails.getOriginalFileName());

                    // Add the new attachment details to the destination record.
                    newAttachmentDetails = new T8AttachmentDetails(newAttachmentId, sourceAttachmentDetails.getContextIid(), sourceAttachmentDetails.getContextId(), sourceAttachmentDetails.getFilePath(), convertedOriginalFileName, sourceAttachmentDetails.getFileSize(), sourceAttachmentDetails.getMD5Checksum(), sourceAttachmentDetails.getMediaType());
                    destinationValue.getParentDataRecord().addAttachmentDetails(newAttachmentId, newAttachmentDetails);

                    // Add the document to the updated collection.
                    updatedDataRecords.add(destinationValue.getParentDataRecord());
                }
            }
        }
        else throw new RuntimeException("Invalid requirement type encountered while merging attachment values: " + requirementType);
    }

    private String getConvertedFilename(RecordValue destinationValue, RecordValue sourceValue, String oldFilename)
    {
        if (filenameConversionScriptDefinition == null)
        {
            // No conversion.
            return oldFilename;
        }
        else // Execute the conversion script.
        {
            Map<String, Object> inputParameters;

            // Create an instance of the script if we've not yet done it.
            if (filenameConversionScript == null)
            {
                try
                {
                    filenameConversionScript = filenameConversionScriptDefinition.getNewScriptInstance(mergeContext.getContext());
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while executing filename conversion script: " + filenameConversionScriptDefinition.getPublicIdentifier(), e);
                }
            }

            // Create the input parameter collection.
            inputParameters = new HashMap<String, Object>();
            inputParameters.put(T8AttachmentFilenameConversionScriptDefinition.PARAMETER_FILE_NAME, oldFilename);
            inputParameters.put(T8AttachmentFilenameConversionScriptDefinition.PARAMETER_SOURCE_VALUE, destinationValue);
            inputParameters.put(T8AttachmentFilenameConversionScriptDefinition.PARAMETER_DESTINATION_VALUE, sourceValue);

            // Execute the script and return the new converted filename.
            try
            {
                Map<String, Object> outputParameters;

                outputParameters = filenameConversionScript.executeScript(inputParameters);
                if (outputParameters != null)
                {
                    String newFilename;

                    newFilename = (String)outputParameters.get(filenameConversionScriptDefinition.getNamespace() + T8AttachmentFilenameConversionScriptDefinition.PARAMETER_FILE_NAME);
                    if (newFilename != null)
                    {
                        return newFilename;
                    }
                    else throw new Exception("Filename conversion script returned a null value as output: " + filenameConversionScriptDefinition.getPublicIdentifier());
                }
                else throw new Exception("Filename conversion script did not return any output: " + filenameConversionScriptDefinition.getPublicIdentifier());
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while executing filename conversion script: " + filenameConversionScriptDefinition.getPublicIdentifier(), e);
            }
        }
    }
}
