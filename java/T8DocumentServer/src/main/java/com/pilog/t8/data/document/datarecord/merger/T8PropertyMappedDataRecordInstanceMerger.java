/**
 * Created on Jun 28, 2017, 9:12:30 AM
 */
package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.definition.data.document.datarecord.merger.T8PropertyMappedDataRecordInstanceMergerDefinition;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8PropertyMappedDataRecordInstanceMerger extends T8DefaultDataRecordInstanceMerger
{
    private final Map<String, String> propertyIdMapping;

    public T8PropertyMappedDataRecordInstanceMerger(T8DataRecordMergeContext mergeContext, T8DataRecordMergeConfiguration config)
    {
        super(mergeContext, config);
        this.propertyIdMapping = new HashMap<>();
    }

    public T8PropertyMappedDataRecordInstanceMerger(T8PropertyMappedDataRecordInstanceMergerDefinition definition, T8DataRecordMergeContext mergeContext)
    {
        super(definition, mergeContext);
        this.propertyIdMapping = definition.getPropertyIdMapping();
    }

    @Override
    public void mergeDataRecords(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        // Merge ontology codes if required.
        if (mergeOntologyCodes)
        {
            T8OntologyConcept sourceConcept;

            sourceConcept = sourceRecord.getOntology();
            if (sourceConcept != null)
            {
                T8OntologyConcept destinationConcept;

                // Make a copy of the source concept.
                sourceConcept = sourceConcept.copy(false, false, false, true, true);
                sourceConcept.assignNewCodeIDs();
                sourceConcept.setID(destinationRecord.getID());
                destinationConcept = destinationRecord.getOntology();

                // If the destination concept exists, merge the code or otherwise just copy them.
                if (destinationConcept != null)
                {
                    destinationConcept.mergeCodes(sourceConcept.getCodes());
                }
                else
                {
                    destinationRecord.setOntology(sourceConcept);
                }
            }
        }

        // Merge the record properties.
        for (RecordProperty sourceProperty : sourceRecord.getRecordProperties())
        {
            RecordProperty destinationProperty;
            String propertyId;

            // If a property mapping exists, we use it, otherwise we use the property ID as-is
            propertyId = sourceProperty.getPropertyID();
            propertyId = propertyIdMapping.getOrDefault(propertyId, propertyId);

            // Get the destination property
            destinationProperty = destinationRecord.getRecordProperty(propertyId);

            // If a destination property exists, we can continue
            if (destinationProperty == null)
            {
                PropertyRequirement propertyRequirement;

                // Get the property requirement to use for the construction of the new property.
                propertyRequirement = destinationRecord.getDataRequirement().getPropertyRequirement(propertyId);
                if (propertyRequirement != null)
                {
                    // Create the new property.
                    destinationProperty = new RecordProperty(propertyRequirement);
                    destinationRecord.setRecordProperty(destinationProperty);
                    updatedDataRecords.add(destinationRecord);
                }
                else // So the source property does not existing in the destination data requirement.
                {
                    T8DataRecordValueStringGenerator fftGenerator;
                    StringBuilder newFft;

                    // Append the source property as FFT to the destination record FFT.
                    fftGenerator = mergeContext.getFFTGenerator();
                    newFft = fftGenerator.generateValueString(sourceProperty);
                    if (newFft != null)
                    {
                        destinationRecord.appendFFTChecked(newFft.toString(), ",");
                        updatedDataRecords.add(destinationRecord);
                    }

                    // Conitnue the loop (we don't want to merge the property).
                    continue;
                }
            }

            // Merge the two properties.
            mergeRecordProperties(destinationProperty, sourceProperty);
        }
    }
}