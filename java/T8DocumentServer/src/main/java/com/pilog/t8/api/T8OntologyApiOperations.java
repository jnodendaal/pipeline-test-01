package com.pilog.t8.api;

import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.ontology.T8ConceptPair;
import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8OntologyApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyApiOperations
{
    public static class ApiGetOntologyStructure extends T8DefaultServerOperation
    {
        public ApiGetOntologyStructure(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;

            // Perform the API operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            return HashMaps.createSingular(PARAMETER_ONTOLOGY_STRUCTURE, ontApi.getOntologyStructure());
        }
    }

    public static class ApiValidateConceptRelations extends T8DefaultServerOperation
    {
        public ApiValidateConceptRelations(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8ConceptPair>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            List<T8ConceptPair> pairList;

            // Get the operation parameters we need.
            pairList = (List<T8ConceptPair>)operationParameters.get(PARAMETER_CONCEPT_PAIRS);

            // Make sure to set some defaults.
            if (pairList == null) pairList = new ArrayList<>();

            // Perform the API operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            pairList = ontApi.validateConceptRelations(pairList);
            return HashMaps.createSingular(PARAMETER_CONCEPT_PAIRS, pairList);
        }
    }

    public static class ApiInsertConceptRelations extends T8DefaultServerOperation
    {
        public ApiInsertConceptRelations(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8ConceptPair>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8ConceptPair> pairList;
            T8OntologyApi ontApi;

            // Get the operation parameters we need.
            pairList = (List<T8ConceptPair>)operationParameters.get(PARAMETER_CONCEPT_PAIRS);

            // Make sure to set some defaults.
            if (pairList == null) pairList = new ArrayList<>();

            // Perform the API operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.insertConceptRelations(pairList);
            return null;
        }
    }

    public static class ApiSaveConceptRelations extends T8DefaultServerOperation
    {
        public ApiSaveConceptRelations(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8ConceptPair>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8ConceptPair> pairList;
            T8OntologyApi ontApi;

            // Get the operation parameters we need.
            pairList = (List<T8ConceptPair>)operationParameters.get(PARAMETER_CONCEPT_PAIRS);

            // Make sure to set some defaults.
            if (pairList == null) pairList = new ArrayList<>();

            // Perform the API operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.saveConceptRelations(pairList);
            return null;
        }
    }

    public static class ApiDeleteConceptRelations extends T8DefaultServerOperation
    {
        public ApiDeleteConceptRelations(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8ConceptPair>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8ConceptPair> pairList;
            T8OntologyApi ontApi;

            // Get the operation parameters we need.
            pairList = (List<T8ConceptPair>)operationParameters.get(PARAMETER_CONCEPT_PAIRS);

            // Make sure to set some defaults.
            if (pairList == null) pairList = new ArrayList<>();

            // Perform the API operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.deleteConceptRelations(pairList);
            return null;
        }
    }

    public static class ApiRecacheOntologyStructure extends T8DefaultServerOperation
    {
        public ApiRecacheOntologyStructure(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8OntologyStructure> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyStructure ontologyStructure;

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontologyStructure = ontApi.recacheOntologyStructure();

            // Return the result.
            return HashMaps.createSingular(PARAMETER_ONTOLOGY_STRUCTURE, ontologyStructure);
        }
    }

    public static class ApiDeleteOntologyClass extends T8DefaultServerOperation
    {
        public ApiDeleteOntologyClass(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            String ocId;
            boolean success;
            boolean includeOntology;

            // Get the operation parameters.
            ocId = (String)operationParameters.get(PARAMETER_OC_ID);
            includeOntology = (boolean)operationParameters.get(PARAMETER_INCLUDE_ONTOLOGY);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            success = ontApi.deleteOntologyClass(ocId, includeOntology);

            // Return the result.
            return HashMaps.createSingular(PARAMETER_SUCCESS, success);
        }
    }

    public static class ApiDeleteDefinition extends T8DefaultServerOperation
    {
        public ApiDeleteDefinition(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            String definitionId;

            // Get the operation parameters.
            definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.deleteDefinition(definitionId);

            // Return the result.
            return null;
        }
    }

    public static class ApiDeleteTerm extends T8DefaultServerOperation
    {
        public ApiDeleteTerm(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            String termId;

            // Get the operation parameters.
            termId = (String)operationParameters.get(PARAMETER_TERM_ID);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.deleteTerm(termId);

            // Return the result.
            return null;
        }
    }

    public static class ApiDeleteCode extends T8DefaultServerOperation
    {
        public ApiDeleteCode(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            String codeId;

            // Get the operation parameters.
            codeId = (String)operationParameters.get(PARAMETER_CODE_ID);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.deleteCode(codeId);

            // Return the result.
            return null;
        }
    }

    public static class ApiDeleteConcept extends T8DefaultServerOperation
    {
        public ApiDeleteConcept(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            String conceptId;

            // Get the operation parameters.
            conceptId = (String)operationParameters.get(PARAMETER_CONCEPT_ID);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.deleteConcept(conceptId);

            // Return the result.
            return null;
        }
    }

    public static class ApiDeleteAbbreviation extends T8DefaultServerOperation
    {
        public ApiDeleteAbbreviation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            String abbreviationId;

            // Get the operation parameters.
            abbreviationId = (String)operationParameters.get(PARAMETER_ABBREVIATION_ID);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.deleteAbbreviation(abbreviationId);

            // Return the result.
            return null;
        }
    }

    public static class ApiSaveDefinition extends T8DefaultServerOperation
    {
        public ApiSaveDefinition(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyDefinition ontologyDefinition;

            // Get the operation parameters.
            ontologyDefinition = (T8OntologyDefinition)operationParameters.get(PARAMETER_DEFINITION);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.saveDefinition(ontologyDefinition);

            // Return the result.
            return null;
        }
    }

    public static class ApiSaveAbbreviation extends T8DefaultServerOperation
    {
        public ApiSaveAbbreviation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyAbbreviation ontologyAbbreviation;

            // Get the operation parameters.
            ontologyAbbreviation = (T8OntologyAbbreviation)operationParameters.get(PARAMETER_ABBREVIATION);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.saveAbbreviation(ontologyAbbreviation);

            // Return the result.
            return null;
        }
    }

    public static class ApiSaveConcept extends T8DefaultServerOperation
    {
        public ApiSaveConcept(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyConcept ontologyConcept;
            boolean deletionEnabled;
            boolean includeTerms;
            boolean includeAbbreviations;
            boolean includeDefinitions;
            boolean includeCodes;
            boolean includeTerminology;

            // Get the operation parameters.
            ontologyConcept = (T8OntologyConcept)operationParameters.get(PARAMETER_CONCEPT);
            deletionEnabled = (boolean)operationParameters.get(PARAMETER_DELETION_ENABLED);
            includeTerms = (boolean)operationParameters.get(PARAMETER_INCLUDE_TERMS);
            includeAbbreviations = (boolean)operationParameters.get(PARAMETER_INCLUDE_ABBREVIATIONS);
            includeDefinitions = (boolean)operationParameters.get(PARAMETER_INCLUDE_DEFINITIONS);
            includeCodes = (boolean)operationParameters.get(PARAMETER_INCLUDE_CODES);
            includeTerminology = (boolean)operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.saveConcept(ontologyConcept, deletionEnabled, includeTerms, includeAbbreviations, includeDefinitions, includeCodes, includeTerminology);

            // Return the result.
            return null;
        }
    }

    public static class ApiSaveCode extends T8DefaultServerOperation
    {
        public ApiSaveCode(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyCode ontologyCode;

            // Get the operation parameters.
            ontologyCode = (T8OntologyCode)operationParameters.get(PARAMETER_CODE);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.saveCode(ontologyCode);

            // Return the result.
            return null;
        }
    }

    public static class ApiSaveTerm extends T8DefaultServerOperation
    {
        public ApiSaveTerm(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyTerm ontologyTerm;
            boolean includeAbbreviations;
            boolean deletionEnabled;

            // Get the operation parameters.
            ontologyTerm = (T8OntologyTerm)operationParameters.get(PARAMETER_TERM);
            deletionEnabled = (boolean)operationParameters.get(PARAMETER_DELETION_ENABLED);
            includeAbbreviations = (boolean)operationParameters.get(PARAMETER_INCLUDE_ABBREVIATIONS);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.saveTerm(ontologyTerm, deletionEnabled, includeAbbreviations);

            // Return the result.
            return null;
        }
    }

    public static class ApiInsertDefinition extends T8DefaultServerOperation
    {
        public ApiInsertDefinition(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyDefinition ontologyDefinition;

            // Get the operation parameters.
            ontologyDefinition = (T8OntologyDefinition)operationParameters.get(PARAMETER_DEFINITION);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.insertDefinition(ontologyDefinition);

            // Return the result.
            return null;
        }
    }

    public static class ApiInsertAbbreviation extends T8DefaultServerOperation
    {
        public ApiInsertAbbreviation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyAbbreviation ontologyAbbreviation;

            // Get the operation parameters.
            ontologyAbbreviation = (T8OntologyAbbreviation)operationParameters.get(PARAMETER_ABBREVIATION);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.insertAbbreviation(ontologyAbbreviation);

            // Return the result.
            return null;
        }
    }

    public static class ApiInsertCode extends T8DefaultServerOperation
    {
        public ApiInsertCode(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyCode ontologyCode;

            // Get the operation parameters.
            ontologyCode = (T8OntologyCode)operationParameters.get(PARAMETER_CODE);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.insertCode(ontologyCode);

            // Return the result.
            return null;
        }
    }

    public static class ApiInsertConcept extends T8DefaultServerOperation
    {
        public ApiInsertConcept(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyConcept ontologyConcept;
            boolean includeTerms;
            boolean includeAbbreviations;
            boolean includeDefinitions;
            boolean includeCodes;
            boolean includeTerminology;

            // Get the operation parameters.
            ontologyConcept = (T8OntologyConcept)operationParameters.get(PARAMETER_CONCEPT);
            includeTerms = (boolean)operationParameters.get(PARAMETER_INCLUDE_TERMS);
            includeAbbreviations = (boolean)operationParameters.get(PARAMETER_INCLUDE_ABBREVIATIONS);
            includeDefinitions = (boolean)operationParameters.get(PARAMETER_INCLUDE_DEFINITIONS);
            includeCodes = (boolean)operationParameters.get(PARAMETER_INCLUDE_CODES);
            includeTerminology = (boolean)operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.insertConcept(ontologyConcept, includeTerms, includeAbbreviations, includeDefinitions, includeCodes, includeTerminology);

            // Return the result.
            return null;
        }
    }

    public static class ApiInsertTerm extends T8DefaultServerOperation
    {
        public ApiInsertTerm(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            T8OntologyTerm ontologyTerm;
            boolean includeAbbreviations;

            // Get the operation parameters.
            ontologyTerm = (T8OntologyTerm)operationParameters.get(PARAMETER_TERM);
            includeAbbreviations = (boolean)operationParameters.get(PARAMETER_INCLUDE_ABBREVIATIONS);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.insertTerm(ontologyTerm, includeAbbreviations);

            // Return the result.
            return null;
        }
    }

    public static class ApiRetrieveDefinitions extends T8DefaultServerOperation
    {
        public ApiRetrieveDefinitions(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyDefinition>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            List<String> definitionIdList;
            List<T8OntologyDefinition> ontologyDefinitionList;

            // Get the operation parameters.
            definitionIdList = (List<String>)operationParameters.get(PARAMETER_DEFINITION_ID_LIST);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontologyDefinitionList = ontApi.retrieveDefinitions(definitionIdList);

            // Return the result.
            return HashMaps.createSingular(PARAMETER_DEFINITION_LIST, ontologyDefinitionList);
        }
    }

    public static class ApiRetrieveAbbreviations extends T8DefaultServerOperation
    {
        public ApiRetrieveAbbreviations(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyAbbreviation>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            List<String> abbrevitationIdList;
            List<T8OntologyAbbreviation> abbrevitationList;

            // Get the operation parameters.
            abbrevitationIdList = (List<String>)operationParameters.get(PARAMETER_ABBREVIATION_ID_LIST);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            abbrevitationList = ontApi.retrieveAbbreviations(abbrevitationIdList);

            // Return the result.
            return HashMaps.createSingular(PARAMETER_ABBREVIATION_LIST, abbrevitationList);
        }
    }

    public static class ApiRetrieveTermAbbreviations extends T8DefaultServerOperation
    {
        public ApiRetrieveTermAbbreviations(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyAbbreviation>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            String termId;
            List<T8OntologyAbbreviation> abbrevitationList;

            // Get the operation parameters.
            termId = (String)operationParameters.get(PARAMETER_TERM_ID);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            abbrevitationList = ontApi.retrieveTermAbbreviations(termId);

            // Return the result.
            return HashMaps.createSingular(PARAMETER_ABBREVIATION_LIST, abbrevitationList);
        }
    }

    public static class ApiRetrieveConceptDefinitions extends T8DefaultServerOperation
    {
        public ApiRetrieveConceptDefinitions(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyDefinition>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            String conceptId;
            List<T8OntologyDefinition> ontologyDefinitionList;

            // Get the operation parameters.
            conceptId = (String)operationParameters.get(PARAMETER_CONCEPT_ID);

            // Perform the Api operation.
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontologyDefinitionList = ontApi.retrieveConceptDefinitions(conceptId);

            // Return the result.
            return HashMaps.createSingular(PARAMETER_DEFINITION_LIST, ontologyDefinitionList);
        }
    }

    public static class ApiRetrieveConceptTerms extends T8DefaultServerOperation
    {
        public ApiRetrieveConceptTerms(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyTerm>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8OntologyTerm> terms;
            String conceptId;
            boolean includeAbbreviations;
            T8OntologyApi ontApi;

            conceptId = (String)operationParameters.get(PARAMETER_CONCEPT_ID);
            includeAbbreviations = (boolean)operationParameters.get(PARAMETER_INCLUDE_ABBREVIATIONS);

            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            terms = ontApi.retrieveConceptTerms(conceptId, includeAbbreviations);

            return HashMaps.createSingular(PARAMETER_TERM_LIST, terms);
        }
    }

    public static class ApiRetrieveCodes extends T8DefaultServerOperation
    {
        public ApiRetrieveCodes(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyCode>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8OntologyCode> codes;
            List<String> codeIdList;
            T8OntologyApi ontApi;

            codeIdList = (List<String>)operationParameters.get(PARAMETER_CODE_ID_LIST);

            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            codes = ontApi.retrieveCodes(codeIdList);

            return HashMaps.createSingular(PARAMETER_CODE_LIST, codes);
        }
    }

    public static class ApiRetrieveTerms extends T8DefaultServerOperation
    {
        public ApiRetrieveTerms(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyTerm>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8OntologyTerm> terms;
            List<String> termIdList;
            boolean includeAbbreviations;
            T8OntologyApi ontApi;

            termIdList = (List<String>)operationParameters.get(PARAMETER_TERM_ID_LIST);
            includeAbbreviations = (boolean)operationParameters.get(PARAMETER_INCLUDE_ABBREVIATIONS);

            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            terms = ontApi.retrieveTerms(termIdList, includeAbbreviations);

            return HashMaps.createSingular(PARAMETER_TERM_LIST, terms);
        }
    }

    public static class ApiRetrieveConceptCodes extends T8DefaultServerOperation
    {
        public ApiRetrieveConceptCodes(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyCode>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8OntologyCode> codes;
            T8OntologyApi ontApi;

            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            codes = ontApi.retrieveConceptCodes((String)operationParameters.get(PARAMETER_CONCEPT_ID));

            return HashMaps.createSingular(PARAMETER_CONCEPT_CODES, codes);
        }
    }

    public static class ApiRetrieveConcepts extends T8DefaultServerOperation
    {
        public ApiRetrieveConcepts(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyConcept>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8OntologyConcept> conceptList;
            T8OntologyApi ontApi;

            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            conceptList = ontApi.retrieveConcepts((Collection)operationParameters.getOrDefault(PARAMETER_CONCEPT_ID_LIST, new ArrayList<>()),
                    Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_ONTOLOGY_LINKS)),
                    Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_TERMS)),
                    Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_ABBREVIATIONS)),
                    Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_DEFINITIONS)),
                    Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_CODES)));

            return HashMaps.createSingular(PARAMETER_CONCEPT_LIST, conceptList);
        }

    }

    public static class ApiGetOntologyLinks extends T8DefaultServerOperation
    {
        public ApiGetOntologyLinks(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8OntologyLink>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8OntologyLink> links;
            T8OntologyApi ontApi;

            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            OntologyProvider ontProvider = ontApi.getOntologyProvider();

            if (operationParameters.containsKey(PARAMETER_OS_ID_LIST))
            {
                links = ontProvider.getOntologyLinks((List) operationParameters.get(PARAMETER_OS_ID_LIST), (String) operationParameters.get(PARAMETER_CONCEPT_ID));
            }
            else
            {
                links = ontProvider.getOntologyLinks((String) operationParameters.get(PARAMETER_CONCEPT_ID));
            }

            return HashMaps.createSingular(PARAMETER_ONTOLOGY_LINKS, links);
        }
    }

    public static class ApiRefreshOntologyStructureNestedIndices extends T8DefaultServerOperation
    {
        public ApiRefreshOntologyStructureNestedIndices(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            List<String> osIdList;

            // Get the operation parameters.
            osIdList = (List<String>)operationParameters.get(PARAMETER_OS_ID_LIST);

            // Perform the API operation.
            ontApi = this.tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.refreshOntologyStructureNestedIndices(osIdList);

            return null;
        }
    }

    public static class ApiMoveOntologyClass extends T8DefaultServerOperation
    {
        public ApiMoveOntologyClass(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8OntologyApi ontApi;
            String ocId;
            String newParentOcId;
            boolean success;

            // Get the nessacary input paramaters.
            ocId = (String)operationParameters.get(PARAMETER_OC_ID);
            newParentOcId = (String)operationParameters.get(PARAMETER_PARENT_OC_ID);

            // Perform the api operation.
            ontApi = this.tx.getApi(T8OntologyApi.API_IDENTIFIER);
            success = ontApi.moveOntologyClass(newParentOcId, ocId);

            // Return the result.
            return HashMaps.createSingular(PARAMETER_SUCCESS, success);
        }
    }
}
