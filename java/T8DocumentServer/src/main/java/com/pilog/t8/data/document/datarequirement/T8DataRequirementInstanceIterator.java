package com.pilog.t8.data.document.datarequirement;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementInstanceDataSourceDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.NoSuchElementException;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementInstanceIterator implements T8DataIterator<DataRequirementInstance>
{
    private final T8DataTransaction tx;
    private final T8DataEntityResults drInstanceIDResults;
    private final String drInstanceIDFieldIdentifier;
    private final String drInstanceDocumentEntityIdentifier;
    private DataRequirementInstance nextDRInstance;
    private DataRequirementInstance currentDRInstance;

    public T8DataRequirementInstanceIterator(T8DataTransaction tx, T8DataEntityResults drInstanceIDResults, String drInstanceIDFieldIdentifier, String drInstanceDocumentEntityIdentifier) throws Exception
    {
        this.tx = tx;
        this.drInstanceIDResults = drInstanceIDResults;
        this.drInstanceIDFieldIdentifier = drInstanceIDFieldIdentifier;
        this.drInstanceDocumentEntityIdentifier = drInstanceDocumentEntityIdentifier;
        this.nextDRInstance = fetchNextDRInstance();
    }

    @Override
    public T8DataTransaction getDataAccessProvider()
    {
        return tx;
    }

    @Override
    public void close()
    {
        drInstanceIDResults.close();
    }

    @Override
    public boolean hasNext()
    {
        return nextDRInstance != null;
    }

    @Override
    public DataRequirementInstance next()
    {
        if (hasNext())
        {
            try
            {
                currentDRInstance = nextDRInstance;
                nextDRInstance = fetchNextDRInstance();
                return currentDRInstance;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while fetching next Data Requirement Instance.", e);
            }
        }
        else throw new NoSuchElementException("Cannot fetch next element from exhauseted iterator.");
    }

    @Override
    public void remove()
    {
        if (currentDRInstance != null)
        {
            try
            {
                T8DataEntity drInstanceEntity;

                drInstanceEntity = tx.create(drInstanceDocumentEntityIdentifier, HashMaps.createSingular(drInstanceDocumentEntityIdentifier + T8DataRequirementInstanceDataSourceDefinition.KEY_FIELD_IDENTIFIER, currentDRInstance.getRequirementType()));
                tx.delete(drInstanceEntity);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while removing Data Requirement Instance.", e);
            }
        }
        else throw new RuntimeException("Cannot remove from iterator without current element.");
    }

    private DataRequirementInstance fetchNextDRInstance() throws Exception
    {
        if (drInstanceIDResults.next())
        {
            String nextDRInstanceID;

            nextDRInstanceID = (String)drInstanceIDResults.get().getFieldValue(drInstanceIDResults.getEntityIdentifier() + drInstanceIDFieldIdentifier);
            if (nextDRInstanceID != null)
            {
                T8DataEntity drInstanceEntity;

                drInstanceEntity = tx.retrieve(drInstanceDocumentEntityIdentifier, HashMaps.createSingular(drInstanceDocumentEntityIdentifier + T8DataRequirementInstanceDataSourceDefinition.KEY_FIELD_IDENTIFIER, nextDRInstanceID));
                if (drInstanceEntity != null)
                {
                    return (DataRequirementInstance)drInstanceEntity.getFieldValue(drInstanceDocumentEntityIdentifier + T8DataRequirementInstanceDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);
                }
                else return null;
            }
            else return null;
        }
        else return null;
    }
}
