package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordReferenceMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordReferenceMergerDefinition.ReferenceMergeMethod;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordReferenceMergerDefinition.ReferenceMatchType;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordReferenceMergerDefinition.ReferenceMismatchAction;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordReferenceMerger implements T8DataRecordValueMerger
{
    private final String identifier;
    private final Set<DataRecord> newDataRecords;
    private final Set<DataRecord> updatedDataRecords;
    private final Map<String, String> recordIdMapping;
    private final Map<String, String> attachmentIdMapping; // Used to store attachment ID's mapped to newly assigned identifiers in destination documents.
    private final T8ExpressionEvaluator expressionEvaluator;
    private final String conditionExpression;
    private final String referenceMatchExpression;
    private final ReferenceMatchType referenceMatchType;
    private final ReferenceMergeMethod referenceMergeMethod;
    private final ReferenceMismatchAction referenceMismatchAction;
    private final int maximumReferenceCount;
    private final T8DataRecordMergeContext mergeContext;
    private final boolean includeDRInstanceOntology;

    public T8DataRecordReferenceMerger(T8DataRecordMergeContext mergeContext, T8DataRecordMergeConfiguration config, String identifier)
    {
        this.identifier = identifier;
        this.mergeContext = mergeContext;
        this.newDataRecords = new HashSet<DataRecord>();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.recordIdMapping = new HashMap<String, String>();
        this.attachmentIdMapping = new HashMap<String, String>();
        this.expressionEvaluator = mergeContext.getExpressionEvaluator();
        this.conditionExpression = null;
        this.referenceMatchExpression = null;
        this.referenceMatchType = ReferenceMatchType.DEFINITION;
        this.referenceMergeMethod = ReferenceMergeMethod.MERGE_REFERENCE_CONTENT;
        this.referenceMismatchAction = ReferenceMismatchAction.ADD_REFERENCE;
        this.maximumReferenceCount = 1000000; // Very large number to simulate 'unlimited'.
        this.includeDRInstanceOntology = false;
    }

    public T8DataRecordReferenceMerger(T8DataRecordReferenceMergerDefinition definition, T8DataRecordMergeContext mergeContext)
    {
        this.identifier = definition.getIdentifier();
        this.mergeContext = mergeContext;
        this.newDataRecords = new HashSet<DataRecord>();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.recordIdMapping = new HashMap<String, String>();
        this.attachmentIdMapping = new HashMap<String, String>();
        this.expressionEvaluator = mergeContext.getExpressionEvaluator();
        this.conditionExpression = definition.getConditionExpression();
        this.referenceMatchExpression = definition.getReferenceMatchExpression();
        this.referenceMatchType = definition.getReferenceMatchType();
        this.referenceMergeMethod = definition.getReferenceMergeMethod();
        this.referenceMismatchAction = definition.getReferenceMismatchAction();
        this.maximumReferenceCount = definition.getMaximumReferenceCount() > 0 ? definition.getMaximumReferenceCount() : 1000000; // Very large number to simulate 'unlimited'.
        this.includeDRInstanceOntology = definition.isIncludeDRInstanceOntology();
    }

    @Override
    public void reset()
    {
        newDataRecords.clear();
        updatedDataRecords.clear();
        attachmentIdMapping.clear();
        recordIdMapping.clear();
    }

    @Override
    public Map<String, String> getRecordIdMapping()
    {
        return new HashMap<String, String>(recordIdMapping);
    }

    @Override
    public Map<String, String> getAttachmentIdentifierMapping()
    {
        return new HashMap<String, String>(attachmentIdMapping);
    }

    @Override
    public Set<DataRecord> getNewDataRecordSet()
    {
        return new HashSet<DataRecord>(newDataRecords);
    }

    @Override
    public Set<DataRecord> getUpdatedDataRecordSet()
    {
        return new HashSet<DataRecord>(updatedDataRecords);
    }

    @Override
    public boolean isApplicable(RecordValue destinationValue, RecordValue sourceValue)
    {
        ValueRequirement destinationValueRequirement;

        // Get the destination value requirement.
        destinationValueRequirement = destinationValue.getValueRequirement();
        if (destinationValueRequirement instanceof DocumentReferenceRequirement)
        {
            DocumentReferenceRequirement referenceRequirement;

            // Get the independent indicator of the destination document reference type.
            referenceRequirement = (DocumentReferenceRequirement)destinationValueRequirement;
            if (!referenceRequirement.isIndependent())
            {
                if (!Strings.isNullOrEmpty(conditionExpression))
                {
                    try
                    {
                        Map<String, Object> expressionParameters;

                        expressionParameters = new HashMap<String, Object>();
                        expressionParameters.put("destinationValue", destinationValue);
                        expressionParameters.put("sourceValue", sourceValue);
                        return expressionEvaluator.evaluateBooleanExpression(conditionExpression, expressionParameters, null);
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException("Exception while evaluating condition expression in merger: " + identifier, e);
                    }
                }
                else return true;
            }
            else return false;
        }
        else return false;
    }

    @Override
    public void mergeRecordValues(RecordValue destinationValue, RecordValue sourceValue)
    {
        DocumentReferenceRequirement destinationValueRequirement;
        DocumentReferenceList destinationReferenceList;
        DocumentReferenceList sourceReferenceList;
        DataRecord destinationParentRecord;

        // Get the source requirement type.
        destinationParentRecord = destinationValue.getParentDataRecord();
        destinationReferenceList = (DocumentReferenceList)destinationValue;
        sourceReferenceList = (DocumentReferenceList)sourceValue;

        // Get the destination value requirement.
        destinationValueRequirement = (DocumentReferenceRequirement)destinationReferenceList.getValueRequirement();

        // This merger is only applicable to dependent references.
        if (!destinationReferenceList.isIndependent())
        {
            List<String> destinationReferences;

            // Get the source and destination references to match.
            destinationReferences = destinationReferenceList.getReferenceIds();
            for (String sourceRecordId : sourceReferenceList.getReferenceIds())
            {
                String matchingDestinationRecordID;
                DataRecord sourceParentRecord;
                DataRecord sourceRecord;

                // Try to find a document reference in the destination that matches the referenced source document.
                sourceParentRecord = sourceReferenceList.getParentDataRecord();
                sourceRecord = sourceParentRecord.findDataRecord(sourceRecordId);
                matchingDestinationRecordID = findMatchingReference(destinationParentRecord, sourceParentRecord, sourceRecord, destinationReferences, sourceRecordId, referenceMatchType);

                // Make sure we found the source record (if required).
                if (sourceRecord == null) throw new RuntimeException("Cannot find descendant record: " + sourceRecordId + " referenced from source record: " + sourceParentRecord);

                // If a destination reference matching the source reference exists, perform the merge action.
                if (matchingDestinationRecordID != null)
                {
                    if (referenceMergeMethod == ReferenceMergeMethod.MERGE_REFERENCE_CONTENT)
                    {
                        DataRecord destinationRecord;

                        // Merge the sub-records.
                        destinationRecord = destinationParentRecord.findDataRecord(matchingDestinationRecordID);
                        mergeDataRecords(destinationRecord, sourceRecord);
                    }
                    else if (referenceMergeMethod == ReferenceMergeMethod.CHANGE_DR_INSTANCE_AND_MERGE_REFERENCE_CONTENT)
                    {
                        DataRecord destinationRecord;
                        String destinationDRInstanceID;
                        String sourceDRInstanceID;

                        // Merge the sub-records.
                        destinationRecord = destinationParentRecord.findDataRecord(matchingDestinationRecordID);
                        destinationDRInstanceID = destinationRecord.getDataRequirementInstanceID();
                        sourceDRInstanceID = sourceRecord.getDataRequirementInstanceID();

                        // If necessary, change the destination DR Instance to match the source record.
                        if (!destinationDRInstanceID.equals(sourceDRInstanceID))
                        {
                            try
                            {
                                T8DRInstanceChangeHandler changeHandler;
                                DataRecord destinationDataFile;

                                changeHandler = new T8DRInstanceChangeHandler(mergeContext.getTransaction());
                                destinationDataFile = changeHandler.changeDrInstance(destinationRecord.getDataFile(), matchingDestinationRecordID, sourceDRInstanceID);

                                // Get the new destination record from the data file returned from the DR Instance change process.
                                destinationRecord = destinationDataFile.findDataRecord(matchingDestinationRecordID);
                                if (destinationRecord == null) throw new RuntimeException("Destination record not found after DR Instance change: " + matchingDestinationRecordID);
                            }
                            catch (Exception e)
                            {
                                throw new RuntimeException("Exception while changing record " + matchingDestinationRecordID + " DR instance to " + sourceDRInstanceID + " during merge operation.", e);
                            }
                        }

                        // Now merge the two records.
                        mergeDataRecords(destinationRecord, sourceRecord);
                    }
                    else if (referenceMergeMethod == ReferenceMergeMethod.SUBSTITUTE_REFERENCE)
                    {
                        DataRecord newDestinationRecord;

                        // Copy the source record.
                        newDestinationRecord = mergeContext.copyDataRecord(sourceRecord);
                        destinationReferenceList.replaceReference(matchingDestinationRecordID, newDestinationRecord.getID());
                        destinationParentRecord.addSubRecord(newDestinationRecord);
                        destinationParentRecord.removeSubRecord(matchingDestinationRecordID);
                        updatedDataRecords.add(destinationParentRecord);
                        newDataRecords.add(newDestinationRecord);
                        newDataRecords.addAll(newDestinationRecord.getDescendantRecords());
                    }
                    else if (referenceMergeMethod == ReferenceMergeMethod.SYNCHRONIZE_REFERENCE)
                    {
                        T8DataRecordSynchronizer synchronizer;
                        DataRecord oldDestinationRecord;

                        // Get the old destination record.
                        oldDestinationRecord = destinationParentRecord.findDataRecord(matchingDestinationRecordID);

                        // Copy the source record.
                        synchronizer = new T8DataRecordSynchronizer();
                        synchronizer.synchronizeDataRecords(oldDestinationRecord, sourceRecord);
                        updatedDataRecords.addAll(synchronizer.getUpdatedDataRecordSet());
                        newDataRecords.addAll(synchronizer.getNewDataRecordSet());
                        recordIdMapping.putAll(synchronizer.getRecordIdMapping());
                        attachmentIdMapping.putAll(synchronizer.getAttachmentIdentifierMapping());
                    }
                    else if (referenceMergeMethod == ReferenceMergeMethod.IGNORE)
                    {
                        // Ignore the match.
                    }
                    else throw new RuntimeException("Invalid Reference Merge Method: " + referenceMergeMethod);
                }
                else // If we did not find a matching destination reference, perform the mismatch action.
                {
                    if (referenceMismatchAction == ReferenceMismatchAction.ADD_REFERENCE)
                    {
                        int drReferenceLimit;
                        int referenceLimit;

                        // Determine the maximum number of references allowed.
                        referenceLimit = maximumReferenceCount;
                        drReferenceLimit = destinationValueRequirement.getMaximumReferenceCount();
                        if ((drReferenceLimit > 0) && (drReferenceLimit < maximumReferenceCount)) referenceLimit = drReferenceLimit;

                        // If the number of references is still below the limit, add another.
                        if (destinationReferences.size() < referenceLimit)
                        {
                            DataRecord newDestinationRecord;

                            // Copy the source record.
                            newDestinationRecord = mergeContext.copyDataRecord(sourceRecord);

                            // Add the copied source record as a new destination reference.
                            destinationReferenceList.addReference(newDestinationRecord.getID());
                            destinationReferenceList.getParentDataRecord().addSubRecord(newDestinationRecord);
                            updatedDataRecords.add(destinationReferenceList.getParentDataRecord());
                            newDataRecords.add(newDestinationRecord);
                            newDataRecords.addAll(newDestinationRecord.getDescendantRecords());
                        }
                        else // We've already reached the maximum allowed reference count, so add FFT.
                        {
                            T8DataRecordValueStringGenerator fftGenerator;
                            StringBuilder fftString;

                            fftGenerator = mergeContext.getFFTGenerator();
                            fftString = fftGenerator.generateValueString(sourceRecord);
                            if (fftString != null)
                            {
                                destinationReferenceList.getParentRecordProperty().appendFFTChecked(fftString.toString(), ",");
                            }
                        }
                    }
                    else if (referenceMismatchAction == ReferenceMismatchAction.SUBSTITUTE_ALL_REFERENCES)
                    {
                        DataRecord newDestinationRecord;

                        // Clear all existing references.
                        for (String destinationReference : destinationReferences)
                        {
                            destinationParentRecord.removeSubRecord(destinationReference);
                            destinationReferenceList.removeReference(destinationReference, false);
                        }

                        // Copy the source record.
                        newDestinationRecord = mergeContext.copyDataRecord(sourceRecord);

                        // Add the copied source record as a new destination reference.
                        destinationReferenceList.addReference(newDestinationRecord.getID());
                        destinationParentRecord.addSubRecord(newDestinationRecord);
                        updatedDataRecords.add(destinationReferenceList.getParentDataRecord());
                        newDataRecords.add(newDestinationRecord);
                        newDataRecords.addAll(newDestinationRecord.getDescendantRecords());
                    }
                    else if (referenceMismatchAction == ReferenceMismatchAction.ADD_FFT)
                    {
                        T8DataRecordValueStringGenerator fftGenerator;
                        StringBuilder fftString;

                        fftGenerator = mergeContext.getFFTGenerator();
                        fftString = fftGenerator.generateValueString(sourceRecord);
                        if (fftString != null)
                        {
                            destinationReferenceList.getParentRecordProperty().appendFFTChecked(fftString.toString(), ",");
                        }
                    }
                    else if (referenceMismatchAction == ReferenceMismatchAction.IGNORE)
                    {
                        // Ignore the mismatch.
                    }
                    else throw new RuntimeException("Invalid Reference Mismatch Action: " +  referenceMismatchAction);
                }
            }
        }
        else throw new RuntimeException("Document Reference merger can only be applied to dependent references: " + destinationReferenceList);
    }

    private String findMatchingReference(DataRecord destinationParentRecord, DataRecord sourceParentRecord, DataRecord sourceRecord, List<String> destinationReferences, String sourceReferenceRecordID, ReferenceMatchType matchType)
    {
        // Try to find a document reference in the destination that matches the referenced source document.
        for (String destinationRecordId : destinationReferences)
        {
            DataRecord destinationRecord;

            destinationRecord = destinationParentRecord.findDataRecord(destinationRecordId);
            if (matchType == ReferenceMatchType.RECORD_ID)
            {
                if (Objects.equals(sourceReferenceRecordID, destinationRecordId))
                {
                    return destinationRecordId;
                }
            }
            else if (matchType == ReferenceMatchType.SOURCE_RECORD_ID_TAG)
            {
                String sourceRecordIdAttribute;

                // We will use the source and destination records here, so check that they have been found.
                if (sourceRecord == null) throw new RuntimeException("Cannot find descendant record: " + sourceReferenceRecordID + " referenced from source Record: " + sourceParentRecord);

                // Get the source record ID tag.
                sourceRecordIdAttribute = (String)sourceRecord.getAttribute(T8DataRecordMerger.SOURCE_RECORD_ID_ATTRIBUTE_IDENTIFIER);
                if (sourceRecordIdAttribute != null)
                {
                    if (Objects.equals(sourceRecordIdAttribute, destinationRecordId))
                    {
                        return destinationRecordId;
                    }
                }
            }
            else if (matchType == ReferenceMatchType.DEFINITION)
            {
                T8DataRecordDefinitionGenerator definitionGenerator;
                String sourceRecordDefinition;
                String destinationRecordDefinition;

                // We will use the source and destination records here, so check that they have been found.
                if (sourceRecord == null) throw new RuntimeException("Cannot find descendant record: " + sourceReferenceRecordID + " referenced from source Record: " + sourceParentRecord);
                if (destinationRecord == null) throw new RuntimeException("Cannot find descendant record: " + destinationRecordId + " referenced from destination Record: " + destinationParentRecord);

                // Generate a destination from the source and destination records and compare the two.
                definitionGenerator = new T8DataRecordDefinitionGenerator(null);
                sourceRecordDefinition = definitionGenerator.generateValueString(sourceRecord).toString();
                destinationRecordDefinition = definitionGenerator.generateValueString(destinationRecord).toString();
                if (Objects.equals(sourceRecordDefinition, destinationRecordDefinition))
                {
                    return destinationRecordId;
                }
            }
            else if (matchType == ReferenceMatchType.EXPRESSION)
            {
                Map<String, Object> expressionParameters;

                // Create a map of expression input parameters.
                expressionParameters = new HashMap<String, Object>();
                expressionParameters.put("destinationRecord", destinationRecord);
                expressionParameters.put("sourceRecord", sourceRecord);

                // Evaluate the expression.
                try
                {
                    if (expressionEvaluator.evaluateBooleanExpression(referenceMatchExpression, expressionParameters, null))
                    {
                        return destinationRecordId;
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception during evaluation of match expression in merger: " + identifier, e);
                }
            }
            else throw new RuntimeException("Invalid record match type: " + matchType);
        }

        // No matching reference found.
        return null;
    }

    private void mergeDataRecords(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        T8DataRecordInstanceMerger instanceMerger;

        // Try to find a specific property merger to use.
        instanceMerger = mergeContext.getInstanceMerger(destinationRecord, sourceRecord);
        if (instanceMerger != null)
        {
            instanceMerger.mergeDataRecords(destinationRecord, sourceRecord);
        }
        else
        {
            // No applicable property merger found.
            throw new RuntimeException("No applicable merger found for data record: " + destinationRecord);
        }
    }
}
