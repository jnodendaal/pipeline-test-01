package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordFieldMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordPropertyMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordValueMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DefaultDataRecordInstanceMergerDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordInstanceMerger implements T8DataRecordInstanceMerger
{
    private final LinkedList<T8DataRecordPropertyMerger> propertyMergers;
    private final LinkedList<T8DataRecordValueMerger> valueMergers;
    private final LinkedList<T8DataRecordFieldMerger> fieldMergers;
    private final String applicableDRInstanceID;

    protected final T8DataRecordMergeContext mergeContext;
    protected final Set<DataRecord> updatedDataRecords;
    protected final boolean mergeOntologyCodes;

    public T8DefaultDataRecordInstanceMerger(T8DataRecordMergeContext mergeContext, T8DataRecordMergeConfiguration config)
    {
        this.mergeContext = mergeContext;
        this.updatedDataRecords = new HashSet<>();
        this.propertyMergers = new LinkedList<>();
        this.fieldMergers = new LinkedList<>();
        this.valueMergers = new LinkedList<>();
        this.applicableDRInstanceID = null; // Applicable to all DR Instances.
        this.mergeOntologyCodes = config.isMergeOntologyCodes();
    }

    public T8DefaultDataRecordInstanceMerger(T8DefaultDataRecordInstanceMergerDefinition definition, T8DataRecordMergeContext mergeContext)
    {
        this.mergeContext = mergeContext;
        this.updatedDataRecords = new HashSet<>();
        this.propertyMergers = new LinkedList<>();
        this.fieldMergers = new LinkedList<>();
        this.valueMergers = new LinkedList<>();
        this.applicableDRInstanceID = definition.getDataRequirementInstanceID();
        this.mergeOntologyCodes = definition.isMergeOntologyCodes();

        // Create property mergers.
        for (T8DataRecordPropertyMergerDefinition propertyMergerDefinition : definition.getPropertyMergerDefinitions())
        {
            propertyMergers.push(propertyMergerDefinition.getNewMergerInstance(mergeContext));
        }

        // Create all the value mergers.
        for (T8DataRecordValueMergerDefinition valueMergerDefinition : definition.getValueMergerDefinitions())
        {
            valueMergers.push(valueMergerDefinition.getNewMergerInstance(mergeContext));
        }

        // Create all the field mergers.
        for (T8DataRecordFieldMergerDefinition fieldMergerDefinition : definition.getFieldMergerDefinitions())
        {
            fieldMergers.push(fieldMergerDefinition.getNewMergerInstance(mergeContext));
        }
    }

    @Override
    public T8DataTransaction getTransaction()
    {
        return mergeContext.getTransaction();
    }

    @Override
    public T8Context getContext()
    {
        return mergeContext.getContext();
    }

    @Override
    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        return mergeContext.getExpressionEvaluator();
    }

    @Override
    public T8DataRecordValueStringGenerator getFFTGenerator()
    {
        return mergeContext.getFFTGenerator();
    }

    @Override
    public DataRecord copyDataRecord(DataRecord sourceRecord)
    {
        return mergeContext.copyDataRecord(sourceRecord);
    }

    public void addPropertyMerger(T8DataRecordPropertyMerger propertyMerger)
    {
        propertyMergers.push(propertyMerger);
    }

    public boolean removePropertyMerger(T8DataRecordPropertyMerger propertyMerger)
    {
        return propertyMergers.remove(propertyMerger);
    }

    @Override
    public String getDefaultContentLanguageID()
    {
        return mergeContext.getDefaultContentLanguageID();
    }

    @Override
    public TerminologyProvider getTerminologyProvider()
    {
        return mergeContext.getTerminologyProvider();
    }

    @Override
    public OntologyProvider getOntologyProvider()
    {
        return mergeContext.getOntologyProvider();
    }

    @Override
    public DataRequirementInstanceProvider getDRInstanceProvider()
    {
        return mergeContext.getDRInstanceProvider();
    }

    @Override
    public void reset()
    {
        // Clear this merger's updated records collection.
        updatedDataRecords.clear();

        // Reset all property mergers.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            propertyMerger.reset();
        }

        // Reset the value mergers.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            valueMerger.reset();
        }

        // Reset the field mergers.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            fieldMerger.reset();
        }
    }

    @Override
    public Map<String, String> getRecordIdMapping()
    {
        Map<String, String> recordIdMap;

        // Create a map of all record identifier changes.
        recordIdMap = new HashMap<>();

        // Add all property merger mappings.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            recordIdMap.putAll(propertyMerger.getRecordIdMapping());
        }

        // Add all value merger mappings.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            recordIdMap.putAll(valueMerger.getRecordIdMapping());
        }

        // Add all field merger mappings.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            recordIdMap.putAll(fieldMerger.getRecordIdMapping());
        }

        // Return the result.
        return recordIdMap;
    }

    @Override
    public Map<String, String> getAttachmentIdentifierMapping()
    {
        Map<String, String> attachmentIdentifierMap;

        // Create a map of all attachment identifier changes.
        attachmentIdentifierMap = new HashMap<>();

        // Add all property merger mappings.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            attachmentIdentifierMap.putAll(propertyMerger.getAttachmentIdentifierMapping());
        }

        // Add all value merger mappings.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            attachmentIdentifierMap.putAll(valueMerger.getAttachmentIdentifierMapping());
        }

        // Add all field merger mappings.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            attachmentIdentifierMap.putAll(fieldMerger.getAttachmentIdentifierMapping());
        }

        // Return the result.
        return attachmentIdentifierMap;
    }

    @Override
    public Set<DataRecord> getNewDataRecordSet()
    {
        HashSet<DataRecord> newRecordSet;

        // Create a set of all new records added during the merge operation.
        newRecordSet = new HashSet<>();

        // Add all property merger new records.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            newRecordSet.addAll(propertyMerger.getNewDataRecordSet());
        }

        // Add all value merger new records.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            newRecordSet.addAll(valueMerger.getNewDataRecordSet());
        }

        // Add all field merger new records.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            newRecordSet.addAll(fieldMerger.getNewDataRecordSet());
        }

        return newRecordSet;
    }

    @Override
    public Set<DataRecord> getUpdatedDataRecordSet()
    {
        HashSet<DataRecord> updatedRecordSet;

        // Create a set of all records updated during the merge operation.
        updatedRecordSet = new HashSet<>(updatedDataRecords);

        // Add all property merger updated records.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            updatedRecordSet.addAll(propertyMerger.getUpdatedDataRecordSet());
        }

        // Add all value merger updated records.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            updatedRecordSet.addAll(valueMerger.getUpdatedDataRecordSet());
        }

        // Add all field merger updated records.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            updatedRecordSet.addAll(fieldMerger.getUpdatedDataRecordSet());
        }

        return updatedRecordSet;
    }

    @Override
    public boolean isApplicable(DataRecord destinationRecord)
    {
        return ((applicableDRInstanceID == null) || (applicableDRInstanceID.equals(destinationRecord.getDataRequirementInstance().getConceptID())));
    }

    @Override
    public void mergeDataRecords(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        // Merge ontology codes if required.
        if (mergeOntologyCodes)
        {
            T8OntologyConcept sourceConcept;

            sourceConcept = sourceRecord.getOntology();
            if (sourceConcept != null)
            {
                T8OntologyConcept destinationConcept;

                // Make a copy of the source concept.
                sourceConcept = sourceConcept.copy(false, false, false, true, true);
                sourceConcept.assignNewCodeIDs();
                sourceConcept.setID(destinationRecord.getID());
                destinationConcept = destinationRecord.getOntology();

                // If the destination concept exists, merge the code or otherwise just copy them.
                if (destinationConcept != null)
                {
                    destinationConcept.mergeCodes(sourceConcept.getCodes());
                }
                else
                {
                    destinationRecord.setOntology(sourceConcept);
                }
            }
        }

        // Merge the record properties.
        for (RecordProperty sourceProperty : sourceRecord.getRecordProperties())
        {
            RecordProperty destinationProperty;
            String propertyID;

            // Get/create the destination property.
            propertyID = sourceProperty.getPropertyID();
            destinationProperty = destinationRecord.getRecordProperty(propertyID);
            if (destinationProperty == null)
            {
                PropertyRequirement propertyRequirement;

                // Get the property requirement to use for the construction of the new property.
                propertyRequirement = destinationRecord.getDataRequirement().getPropertyRequirement(propertyID);
                if (propertyRequirement != null)
                {
                    // Create the new property.
                    destinationProperty = new RecordProperty(propertyRequirement);
                    destinationRecord.setRecordProperty(destinationProperty);
                    updatedDataRecords.add(destinationRecord);
                }
                else // So the source property does not existing in the destination data requirement.
                {
                    T8DataRecordValueStringGenerator fftGenerator;
                    StringBuilder newFft;

                    // Append the source property as FFT to the destination record FFT.
                    fftGenerator = mergeContext.getFFTGenerator();
                    newFft = fftGenerator.generateValueString(sourceProperty);
                    if (newFft != null)
                    {
                        destinationRecord.appendFFTChecked(newFft.toString(), ",");
                        updatedDataRecords.add(destinationRecord);
                    }

                    // Conitnue the loop (we don't want to merge the property).
                    continue;
                }
            }

            // Merge the two properties.
            mergeRecordProperties(destinationProperty, sourceProperty);
        }
    }

    protected void mergeRecordProperties(RecordProperty destinationProperty, RecordProperty sourceProperty)
    {
        T8DataRecordPropertyMerger propertyMerger;

        // Try to find a specific property merger to use.
        propertyMerger = getPropertyMerger(destinationProperty, sourceProperty);
        if (propertyMerger != null)
        {
            propertyMerger.mergeRecordProperties(destinationProperty, sourceProperty);
        }
        else
        {
            // No applicable property merger found.
            throw new RuntimeException("No applicable property merger found for property: " + destinationProperty);
        }
    }

    @Override
    public T8DataRecordInstanceMerger getInstanceMerger(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        return mergeContext.getInstanceMerger(destinationRecord, sourceRecord);
    }

    @Override
    public T8DataRecordPropertyMerger getPropertyMerger(RecordProperty destinationProperty, RecordProperty sourceProperty)
    {
        // Try to find a specific applicable merger for the properties.
        for (T8DataRecordPropertyMerger propertyMerger : propertyMergers)
        {
            if (propertyMerger.isApplicable(destinationProperty))
            {
                return propertyMerger;
            }
        }

        // No applicable merger found.
        return mergeContext.getPropertyMerger(destinationProperty, sourceProperty);
    }

    @Override
    public T8DataRecordFieldMerger getFieldMerger(RecordValue destinationField, RecordValue sourceField)
    {
        // Try to find a specific applicable merger for the fields.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            if (fieldMerger.isApplicable(destinationField, sourceField))
            {
                return fieldMerger;
            }
        }

        // No applicable merger found.
        return mergeContext.getFieldMerger(destinationField, sourceField);
    }

    @Override
    public T8DataRecordValueMerger getValueMerger(RecordValue destinationValue, RecordValue sourceValue)
    {
        // Try to find a specific applicable merger for the values.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            if (valueMerger.isApplicable(destinationValue, sourceValue))
            {
                return valueMerger;
            }
        }

        // No applicable merger found.
        return mergeContext.getValueMerger(destinationValue, sourceValue);
    }
}
