package com.pilog.t8.data.document.path;

import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.object.T8DataObjectProvider;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.object.T8ServerDataObjectProvider;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ServerDocPathEvaluatorFactory implements DocPathEvaluatorFactory
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private OntologyProvider ontologyProvider;
    private TerminologyProvider terminologyProvider;
    private DataRecordProvider recordProvider;
    private T8DataObjectProvider dataObjectProvider;
    private String languageId;

    public T8ServerDocPathEvaluatorFactory(T8Context context)
    {
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
    }

    @Override
    public void setLanguage(String languageId)
    {
        this.languageId = languageId;
        if (terminologyProvider != null)
        {
            terminologyProvider.setLanguage(languageId);
        }
    }

    @Override
    public DocPathExpressionEvaluator getEvaluator()
    {
        DocPathExpressionEvaluator evaluator;
        T8DataManager dataManager;
        T8DataTransaction tx;
        T8DataSession ds;

        dataManager = serverContext.getDataManager();
        ds = dataManager.getCurrentSession();
        tx = ds.getTransaction();
        if (tx == null) tx = ds.instantTransaction();

        if (ontologyProvider == null)
        {
            T8OntologyApi ontApi;

            ontApi =  tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontologyProvider = ontApi.getOntologyProvider();
        }

        if (terminologyProvider == null)
        {
            T8TerminologyApi trmApi;

            trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            terminologyProvider = trmApi.getTerminologyProvider();
            terminologyProvider.setLanguage(languageId);
        }

        if (recordProvider == null)
        {
            T8DataRecordApi recApi;

            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            recordProvider = recApi.getRecordProvider();
        }

        if (dataObjectProvider == null)
        {
            dataObjectProvider = new T8ServerDataObjectProvider(tx);
        }

        evaluator = new DocPathExpressionEvaluator();
        evaluator.setOntologyProvider(ontologyProvider);
        evaluator.setTerminologyProvider(terminologyProvider);
        evaluator.setRecordProvider(recordProvider);
        evaluator.setDataObjectProvider(dataObjectProvider);
        evaluator.setLanguage(languageId);
        return evaluator;
    }
}
