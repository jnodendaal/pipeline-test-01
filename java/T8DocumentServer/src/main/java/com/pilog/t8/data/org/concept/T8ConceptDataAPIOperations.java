/**
 * Created on 28 Jul 2015, 7:29:25 AM
 */
package com.pilog.t8.data.org.concept;

import static com.pilog.t8.definition.data.org.concept.T8ConceptDataAPIResources.*;

import com.pilog.t8.definition.data.org.concept.T8ConceptMetaUsages;
import com.pilog.t8.definition.data.org.concept.T8ConceptUsages;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8ConceptDataAPIOperations
{

    public static class APIOperationGetConceptUsageCounts extends T8DefaultServerOperation
    {
        public APIOperationGetConceptUsageCounts(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8ConceptUsages> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ServerConceptDataAPI cdAPI;
            String conceptID;

            // Get the operation parameters we need.
            conceptID = (String) operationParameters.get(PARAMETER_CONCEPT_ID);

            // Get the Concept Data API
            cdAPI = serverContext.getConfigurationManager().getAPI(context, T8ServerConceptDataAPI.API_IDENTIFIER);

            // Perform the API operation and return the result.
            return HashMaps.createSingular(PARAMETER_CONCEPT_USAGES, cdAPI.getConceptUsageCounts(tx, conceptID));
        }
    }

    public static class APIOperationGetConceptMetaUsageCounts extends T8DefaultServerOperation
    {
        public APIOperationGetConceptMetaUsageCounts(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8ConceptMetaUsages> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ServerConceptDataAPI cdAPI;
            String conceptID;

            // Get the operation parameters we need.
            conceptID = (String) operationParameters.get(PARAMETER_CONCEPT_ID);

            // Get the Concept Data API
            cdAPI = serverContext.getConfigurationManager().getAPI(context, T8ServerConceptDataAPI.API_IDENTIFIER);

            // Perform the API operation and return the result.
            return HashMaps.createSingular(PARAMETER_CONCEPT_META_USAGES, cdAPI.getConceptMetaUsageCounts(tx, conceptID));
        }
    }
}
