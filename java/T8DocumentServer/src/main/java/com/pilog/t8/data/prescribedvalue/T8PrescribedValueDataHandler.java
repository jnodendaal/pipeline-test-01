package com.pilog.t8.data.prescribedvalue;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.IntegerValue;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.datarecord.value.QualifierOfMeasure;
import com.pilog.t8.data.document.datarecord.value.RealValue;
import com.pilog.t8.data.document.datarecord.value.StringValue;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasure;
import com.pilog.t8.data.document.datarequirement.PrescribedValue;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.value.DocumentReference.DocumentReferenceType;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.security.T8Context;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarequirement.OldPrescribedValue;

import static com.pilog.t8.data.document.requirementtype.RequirementType.*;
import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import static com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction.*;
import static com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator.*;

/**
 *
 * @author Andre Scheepers
 */
public class T8PrescribedValueDataHandler
{
    private final T8Context context;

    public T8PrescribedValueDataHandler(T8Context context)
    {
        this.context = context;
    }

    /**
     * Retrieves a set of data requirement standard value {@code T8DataEntity}'s
     * which contain the multiple values which are available for the specified
     * combination of identifiers specified.
     *
     * @param tx The {@code T8DataTransaction} which provides the required
     *      access to the database
     * @param valueRequirement The {@code ValueRequirement} which will allow the
     *      values to be filtered accordingly
     *
     * @return The {@code List} of {@code RecordValue} objects generated from
     *      the results retrieved. An empty list if no results were found
     *
     * @throws Exception If there is any error during the retrieval of the
     *      data entities
     */
    public List<PrescribedValue> retrievePrescribedValues(T8DataTransaction tx, ValueRequirement valueRequirement) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        T8DataFilter valuesFilter;
        String entityIdentifier;

        // Get the entity identifier
        entityIdentifier = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;

        // Create the filter that will be used to fetch all Suggested Record Values required.
        valuesFilter = new T8DataFilter(entityIdentifier);

        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityIdentifier + EF_DR_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, valueRequirement.getDataRequirementID(), false);
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityIdentifier + EF_PROPERTY_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, valueRequirement.getPropertyID(), false);
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityIdentifier + EF_DATA_TYPE_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, valueRequirement.getRequirementType().getID(), false);
        if (valueRequirement.getFieldID() != null)
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityIdentifier + EF_FIELD_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, valueRequirement.getFieldID(), false);
        }
        else filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityIdentifier + EF_FIELD_ID, T8DataFilterCriterion.DataFilterOperator.IS_NULL, null, false);

        valuesFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, filterCriteria);
        return retrievePrescribedValues(tx, valuesFilter);
    }

    /**
     * This method retrieves all prescribed values linked to the specified data requirement for the current session organization.
     * All old prescribed values are included for each prescribed value retrieved.  It is important to note that there is a
     * one-to-many relation from prescribed value to its collection of old values and this method takes that into account
     * during retrieval and construction of the result list.
     * @param tx The transaction to use for data retrieval.
     * @param drId The Data Requirement id for which to retrieve the prescribed value list.
     * @return The list of prescribed values (and each one's old values) linked to the specified data requirement.
     * @throws Exception
     */
    public List<PrescribedValue> retrieveDataRequirementPrescribedValues(T8DataTransaction tx, String drId) throws Exception
    {
        List<T8DataEntity> prescribedValueEntities;
        Map<String, PrescribedValue> prescribedValues;
        T8DataFilterCriteria filterCriteria;
        T8DataFilter filter;
        String entityId;

        // Get the entity identifier
        entityId = DATA_REQUIREMENT_VALUE_OLD_AND_NEW_DE_IDENTIFIER;

        // Create the filter that will be used to fetch all prescribed values for the Data Requirement.
        filter = new T8DataFilter(entityId);

        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_ORG_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, context.getSessionContext().getRootOrganizationIdentifier(), false);
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_DR_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, drId, false);
        filter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, filterCriteria);

        // Process the results.
        prescribedValues = new HashMap<>();
        prescribedValueEntities = tx.select(entityId, filter);
        for (T8DataEntity prescribedValueEntity : prescribedValueEntities)
        {
            PrescribedValue prescribedValue;
            String valueId;

            // Get the value id from the next entity and use it to get the applicable prescribed value from the result map.
            // The same value id can appear multiple times in the result set, once for each old value linked to it and
            // we don't want to recreate a new prescribed value for each entity.
            valueId = (String)prescribedValueEntity.getFieldValue(EF_VALUE_ID);
            prescribedValue = prescribedValues.get(valueId);

            // If the prescribed value was not found in the map and therefore has not been created yet, do it now.
            if (prescribedValue == null)
            {
                prescribedValue = createPrescribedValue(prescribedValueEntity);
                prescribedValues.put(valueId, prescribedValue);
            }

            // Now add the old value to the prescribed value.
            prescribedValue.addOldValue(createOldPrescribedValue(prescribedValueEntity));
        }

        // Return the prescribed values as a list.
        return new ArrayList<>(prescribedValues.values());
    }

    public PrescribedValue retrievePrescribedValue(T8DataTransaction tx, String valueId) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        T8DataFilter valuesFilter;
        String entityIdentifier;
        PrescribedValue prescribedValue;
        List <T8DataEntity> prescribedValueEntity;
        RequirementType valueDataType;

        // Get the entity identifier
        entityIdentifier = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;

        // Create the filter that will be used to fetch all Suggested Record Values required.
        valuesFilter = new T8DataFilter(entityIdentifier);

        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, entityIdentifier + EF_VALUE_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, valueId, false);

        valuesFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, filterCriteria);

        //Retrive the value from the DB
        prescribedValueEntity = tx.select(entityIdentifier, valuesFilter);

        valueDataType = getRequirementType((prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_DATA_TYPE_ID)).toString());

        //Build prescribed value object by setting from retrived result
        prescribedValue = new PrescribedValue(valueDataType, valueId);
        prescribedValue.setDataRequirementID(prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_DR_ID).toString());
        prescribedValue.setPropertyID(prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_PROPERTY_ID).toString());
        prescribedValue.setValueID(prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_VALUE_ID).toString());

        switch (valueDataType)
        {
            case CONTROLLED_CONCEPT:
            {
                String conceptId;

                conceptId = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_CONCEPT_ID);

                if (conceptId != null) prescribedValue.setConceptID(conceptId);
                break;
            }
            case QUALIFIER_OF_MEASURE:
            {
                String qom;

                qom = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_QOM_ID);

                if (qom != null) prescribedValue.setQualifierOfMeasureID(qom);
                break;
            }
            case UNIT_OF_MEASURE:
            {
                String uom;

                uom = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_UOM_ID);

                if (uom != null) prescribedValue.setUnitOfMeasureID(uom);
                break;
            }
            case MEASURED_NUMBER:
            {
                String value;
                String uom;
                String qom;

                value = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_VALUE);
                uom = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_UOM_ID);
                qom = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_QOM_ID);

                if (value != null) prescribedValue.setValue(value);
                if (uom != null) prescribedValue.setUnitOfMeasureID(uom);
                if (qom != null) prescribedValue.setQualifierOfMeasureID(qom);
                break;
            }
            case MEASURED_RANGE:
            {
                String lowerBoundValue;
                String upperBoundValue;
                String uom;
                String qom;

                lowerBoundValue = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_LOWER_BOUND_VALUE);
                upperBoundValue = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_UPPER_BOUND_VALUE);
                uom = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_UOM_ID);
                qom = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_QOM_ID);

                if (lowerBoundValue != null) prescribedValue.setLowerBoundValue(lowerBoundValue);
                if (upperBoundValue != null) prescribedValue.setUpperBoundValue(upperBoundValue);
                if (uom != null) prescribedValue.setUnitOfMeasureID(uom);
                if (qom != null) prescribedValue.setQualifierOfMeasureID(qom);
                break;
            }
            case INTEGER_TYPE:
            case REAL_TYPE:
            case STRING_TYPE:
            {
                String value;

                value = (String)prescribedValueEntity.get(0).getFieldValue(entityIdentifier+EF_VALUE);

                if (value != null) prescribedValue.setValue(value);
                break;
            }
            default:
                throw new RuntimeException("Unsupported type: " + valueDataType);
        }

        //Return prescribedValue
        return prescribedValue;
    }

    private List<PrescribedValue> retrievePrescribedValues(T8DataTransaction tx, T8DataFilter filter) throws Exception
    {
        List<T8DataEntity> prescribedValueEntities;

        prescribedValueEntities = tx.select(filter.getEntityIdentifier(), filter);
        return createPrescribedValues(prescribedValueEntities);
    }

    public void savePrescribedValue(T8DataTransaction tx, PrescribedValue prescribedValue) throws Exception
    {
        T8DataEntity entity;
        String entityId;

        // Get the entity identifier
        entityId = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;

        // Update/insert the prescribed value.
        entity = entityFromDataRequirementValue(tx, entityId, prescribedValue);
        if (!tx.update(entity))
        {
            tx.insert(entity);
        }
        else
        {
            // Update the records that use the prescribed value.
            updatePrescribedValueRecordUsages(tx, prescribedValue);
        }
    }

    public void insertPrescribedValue(T8DataTransaction tx, PrescribedValue prescribedValue) throws Exception
    {
        String entityId;

        // Get the entity identifier
        entityId = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;

        // Insert the actual standard value record
        tx.insert(entityFromDataRequirementValue(tx, entityId, prescribedValue));
    }

    public void updatePrescribedValue(T8DataTransaction tx, PrescribedValue prescribedValue) throws Exception
    {
        String entityId;

        // Get the data requirement value entity identifier
        entityId = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;

        // Update the actual standard value record
        tx.update(entityFromDataRequirementValue(tx, entityId, prescribedValue));

        // Update the records that use the prescribed value.
        updatePrescribedValueRecordUsages(tx, prescribedValue);
    }

    private T8DataEntity entityFromDataRequirementValue(T8DataTransaction tx, String entityId, PrescribedValue prescribedValue) throws Exception
    {
        T8DataEntity recordValueEntity;

        recordValueEntity = tx.create(entityId, null);
        recordValueEntity.setFieldValue(entityId+EF_VALUE_ID, prescribedValue.getValueID());
        recordValueEntity.setFieldValue(entityId+EF_ORG_ID, prescribedValue.getOrganizationID());
        recordValueEntity.setFieldValue(entityId+EF_DR_ID, prescribedValue.getDataRequirementID());
        recordValueEntity.setFieldValue(entityId+EF_DR_INSTANCE_ID, prescribedValue.getDataRequirementInstanceID());
        recordValueEntity.setFieldValue(entityId+EF_PROPERTY_ID, prescribedValue.getPropertyID());
        recordValueEntity.setFieldValue(entityId+EF_FIELD_ID, prescribedValue.getFieldID());
        recordValueEntity.setFieldValue(entityId+EF_DATA_TYPE_ID, prescribedValue.getRequirementType().getID());
        recordValueEntity.setFieldValue(entityId+EF_STANDARD_INDICATOR, "N");
        recordValueEntity.setFieldValue(entityId+EF_APPROVED_INDICATOR, "N");

        switch (prescribedValue.getRequirementType())
        {
            case BOOLEAN_TYPE:
            case INTEGER_TYPE:
            case REAL_TYPE:
            case STRING_TYPE:
            case NUMBER:
            case LOWER_BOUND_NUMBER:
            case UPPER_BOUND_NUMBER:
            case TIME_TYPE:
            {
                recordValueEntity.setFieldValue(entityId+EF_VALUE, prescribedValue.getValue());
                break;
            }
            case CONTROLLED_CONCEPT:
            {
                recordValueEntity.setFieldValue(entityId+EF_CONCEPT_ID, prescribedValue.getConceptID());
                break;
            }
            case DOCUMENT_REFERENCE:
            {
                recordValueEntity.setFieldValue(entityId+EF_CONCEPT_ID, prescribedValue.getConceptID());
                break;
            }
            case QUALIFIER_OF_MEASURE:
            case UNIT_OF_MEASURE:
            case MEASURED_NUMBER:
            {
                recordValueEntity.setFieldValue(entityId+EF_VALUE, prescribedValue.getValue());
                if (prescribedValue.getUnitOfMeasureID() != null) recordValueEntity.setFieldValue(entityId+EF_UOM_ID, prescribedValue.getUnitOfMeasureID());
                if (prescribedValue.getQualifierOfMeasureID()!= null) recordValueEntity.setFieldValue(entityId+EF_QOM_ID, prescribedValue.getQualifierOfMeasureID());
                break;
            }
            case MEASURED_RANGE:
            {
                recordValueEntity.setFieldValue(entityId+EF_LOWER_BOUND_VALUE, prescribedValue.getLowerBoundValue());
                recordValueEntity.setFieldValue(entityId+EF_UPPER_BOUND_VALUE, prescribedValue.getUpperBoundValue());
                recordValueEntity.setFieldValue(entityId+EF_UOM_ID, prescribedValue.getUnitOfMeasureID());
                recordValueEntity.setFieldValue(entityId+EF_QOM_ID, prescribedValue.getQualifierOfMeasureID());
                break;
            }
            // No use case supports date and date-time values being standardized
            case DATE_TIME_TYPE: // Can be added to first case if implemented
            case DATE_TYPE: // Can be added to first case if implemented
            default:
            {
                throw new IllegalStateException("Invalid record value requirement type: " + prescribedValue.getRequirementType().getID());
            }
        }
        return recordValueEntity;
    }

    private void updatePrescribedValueRecordUsages(T8DataTransaction tx, PrescribedValue prescribedValue) throws Exception
    {

        String entityId;

        entityId = DATA_RECORD_VALUE_DE_IDENTIFIER;
        switch (prescribedValue.getRequirementType())
        {
            case STRING_TYPE:
            case BOOLEAN_TYPE:
            case INTEGER_TYPE:
            case REAL_TYPE:
            case TIME_TYPE:
            {
                Map<String, Object> updatedValues;
                T8DataFilter valueIdFilter;

                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, prescribedValue.getRequirementType().getID()));

                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getValue());
                tx.update(entityId, updatedValues, valueIdFilter);
                break;
            }
            case CONTROLLED_CONCEPT:
            {
                Map<String, Object> updatedValues;
                T8DataFilter valueIdFilter;

                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, prescribedValue.getRequirementType().getID()));

                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getConceptID());
                updatedValues.put(entityId + EF_VALUE_CONCEPT_ID, prescribedValue.getConceptID());
                tx.update(entityId, updatedValues, valueIdFilter);
                break;
            }
            case QUALIFIER_OF_MEASURE:
            {
                Map<String, Object> updatedValues;
                T8DataFilter valueIdFilter;

                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, prescribedValue.getRequirementType().getID()));

                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getQualifierOfMeasureID());
                updatedValues.put(entityId + EF_VALUE_CONCEPT_ID, prescribedValue.getQualifierOfMeasureID());
                tx.update(entityId, updatedValues, valueIdFilter);
                break;
            }
            case UNIT_OF_MEASURE:
            {
                Map<String, Object> updatedValues;
                T8DataFilter valueIdFilter;

                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, prescribedValue.getRequirementType().getID()));

                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getUnitOfMeasureID());
                updatedValues.put(entityId + EF_VALUE_CONCEPT_ID, prescribedValue.getUnitOfMeasureID());
                tx.update(entityId, updatedValues, valueIdFilter);
                break;
            }
            case MEASURED_NUMBER:
            {
                Map<String, Object> updatedValues;
                T8DataFilter valueIdFilter;

                // Update the numeric value.
                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, RequirementType.NUMBER.toString()));
                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getValue());
                tx.update(entityId, updatedValues, valueIdFilter);

                // Update the UOM.
                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, UNIT_OF_MEASURE.toString()));
                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getUnitOfMeasureID());
                updatedValues.put(entityId + EF_VALUE_CONCEPT_ID, prescribedValue.getUnitOfMeasureID());
                tx.update(entityId, updatedValues, valueIdFilter);

                // Update the QOM.
                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, QUALIFIER_OF_MEASURE.toString()));
                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getQualifierOfMeasureID());
                updatedValues.put(entityId + EF_VALUE_CONCEPT_ID, prescribedValue.getQualifierOfMeasureID());
                tx.update(entityId, updatedValues, valueIdFilter);

                break;
            }
            case MEASURED_RANGE:
            {
                Map<String, Object> updatedValues;
                T8DataFilter valueIdFilter;

                // Update the lower bound numeric value.
                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, RequirementType.LOWER_BOUND_NUMBER.toString()));

                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getLowerBoundValue());
                tx.update(entityId, updatedValues, valueIdFilter);

                // Update the upper bound numeric value.
                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, RequirementType.UPPER_BOUND_NUMBER.toString()));

                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getUpperBoundValue());
                tx.update(entityId, updatedValues, valueIdFilter);

                // Update the UOM.
                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, UNIT_OF_MEASURE.toString()));
                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getUnitOfMeasureID());
                updatedValues.put(entityId + EF_VALUE_CONCEPT_ID, prescribedValue.getUnitOfMeasureID());
                tx.update(entityId, updatedValues, valueIdFilter);

                // Update the QOM.
                valueIdFilter = new T8DataFilter(entityId);
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, EQUAL, prescribedValue.getValueID()));
                valueIdFilter.addFilterCriterion(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, QUALIFIER_OF_MEASURE.toString()));
                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE, prescribedValue.getQualifierOfMeasureID());
                updatedValues.put(entityId + EF_VALUE_CONCEPT_ID, prescribedValue.getQualifierOfMeasureID());
                tx.update(entityId, updatedValues, valueIdFilter);

                break;
            }
            // No use case supports the following types.
            case DATE_TIME_TYPE:
            case DATE_TYPE:
            case DOCUMENT_REFERENCE:
            default:
            {
                throw new IllegalStateException("Invalid record value requirement type: " + prescribedValue.getRequirementType());
            }
        }
    }

    /**
     * Replaces all references of standard values, as defined by the standard
     * values reference filter with the specified standard value identifier.
     * The filter essentially defines the where clause for the standard value
     * references to be updated with the new standard value ID.
     *
     * @param tx The {@code T8DataTransaction} which provides access to the
     *      connected database
     * @param prescribedValueIdList A list of ID's of the prescribed values that
     *      will be replaced across all records by the specified replacement
     *      value.
     * @param replacementValueId The {@code String} standard value ID
     *      to be used as the replacement ID
     * @return The {@code Integer} number of references which have been replaced
     *      with the specified standard value ID
     *
     * @throws Exception If there is any error while updating the standard value
     *      references with the new value
     */
    public Integer replacePrescribedValueReferences(T8DataTransaction tx, List<String> prescribedValueIdList, String replacementValueId) throws Exception
    {
        Map<String, Object> updatedValues;
        String entityId;
        T8DataFilterCriteria filterCriteria;
        T8DataFilter dataFilter;
        PrescribedValue replacementPrescribedValue;
        Integer updatedRecordsCount;

        // Retrieve the replacement prescribed value.
        replacementPrescribedValue = retrievePrescribedValue(tx, replacementValueId);
        entityId = DATA_RECORD_VALUE_DE_IDENTIFIER;

        // Save the old values, before making changes.
        for (String currentOldValueId : prescribedValueIdList)
        {
            PrescribedValue replacedPrescribedValue;
            OldPrescribedValue oldValue;

            replacedPrescribedValue = retrievePrescribedValue(tx, currentOldValueId);

            // Build up and entry for DATA_REQUIREMENT_VALUE_OLD.  This entry contains the replaced prescribed value details, linked against the value id of the replacement.
            oldValue = new OldPrescribedValue(replacedPrescribedValue.getRequirementType(), T8IdentifierUtilities.createNewGUID());
            oldValue.setConceptId(replacedPrescribedValue.getConceptID());
            oldValue.setValueId(replacementPrescribedValue.getValueID());
            oldValue.setValue(replacedPrescribedValue.getValue());
            oldValue.setLowerBoundValue(replacedPrescribedValue.getLowerBoundValue());
            oldValue.setUpperBoundValue(replacedPrescribedValue.getUpperBoundValue());
            oldValue.setQomId(replacedPrescribedValue.getQualifierOfMeasureID());
            oldValue.setUomId(replacedPrescribedValue.getUnitOfMeasureID());

            // Insert the old data in DATA_REQUIREMENT_VALUE_OLD.
            insertOldPrescribedValue(tx, oldValue);
        }

        // Check the type of the replacement value and handle it accordingly.
        switch (replacementPrescribedValue.getRequirementType())
        {
            case CONTROLLED_CONCEPT:
                // Create a map of values to be updated.
                updatedValues = HashMaps.newHashMap(entityId + EF_VALUE_ID, replacementValueId, entityId+EF_VALUE, replacementPrescribedValue.getConceptID(), entityId+EF_VALUE_CONCEPT_ID, replacementPrescribedValue.getConceptID());

                // Create filter criteria for the values to update.
                filterCriteria = new T8DataFilterCriteria();
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_VALUE_ID, DataFilterOperator.IN, prescribedValueIdList, false);
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DATA_TYPE_ID, DataFilterOperator.EQUAL, RequirementType.CONTROLLED_CONCEPT.toString(), false);

                // Create the filter and update the values.
                dataFilter = new T8DataFilter(entityId, filterCriteria);
                updatedRecordsCount = tx.update(entityId, updatedValues, dataFilter);

                break;
            case MEASURED_RANGE:

                // Update the measured range.
                filterCriteria = new T8DataFilterCriteria(entityId);
                filterCriteria.addFilterClause(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, IN, prescribedValueIdList));
                filterCriteria.addFilterClause(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, RequirementType.MEASURED_RANGE.toString()));
                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE_ID, replacementValueId);
                dataFilter = new T8DataFilter(entityId, filterCriteria);
                tx.update(entityId, updatedValues, dataFilter);

                // Update the lower bound numeric value.
                updatedValues = HashMaps.newHashMap(entityId + EF_VALUE_ID, replacementValueId, entityId + EF_VALUE, replacementPrescribedValue.getLowerBoundValue());

                // Create filter criteria for the numeric values to update.
                filterCriteria = new T8DataFilterCriteria();
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DATA_TYPE_ID, DataFilterOperator.EQUAL, RequirementType.LOWER_BOUND_NUMBER.toString(), false);
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_VALUE_ID, DataFilterOperator.IN, prescribedValueIdList, false);

                // Create the filter and update the lower bound numeric value.
                dataFilter = new T8DataFilter(entityId, filterCriteria);
                tx.update(entityId, updatedValues, dataFilter);

                // Update the upper bound numeric value.
                updatedValues = HashMaps.newHashMap(entityId + EF_VALUE_ID, replacementValueId, entityId + EF_VALUE, replacementPrescribedValue.getUpperBoundValue());

                // Create filter criteria for the numeric values to update.
                filterCriteria = new T8DataFilterCriteria();
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DATA_TYPE_ID, DataFilterOperator.EQUAL, RequirementType.UPPER_BOUND_NUMBER.toString(), false);
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_VALUE_ID, DataFilterOperator.IN, prescribedValueIdList, false);

                // Create the filter and update the upper bound numeric value.
                dataFilter = new T8DataFilter(entityId, filterCriteria);
                tx.update(entityId, updatedValues, dataFilter);

                // Create a map of UOM to be updated.
                updatedValues = HashMaps.newHashMap(entityId + EF_VALUE_ID, replacementValueId, entityId+EF_VALUE_CONCEPT_ID, replacementPrescribedValue.getUnitOfMeasureID(), entityId + EF_VALUE, replacementPrescribedValue.getUnitOfMeasureID());

                // Create filter criteria for the uom's to update.
                filterCriteria = new T8DataFilterCriteria();
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_VALUE_ID, DataFilterOperator.IN, prescribedValueIdList, false);
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DATA_TYPE_ID, DataFilterOperator.EQUAL, RequirementType.UNIT_OF_MEASURE.toString(), false);

                // Create the filter and update the UOM ID's.
                dataFilter = new T8DataFilter(entityId, filterCriteria);
                updatedRecordsCount = tx.update(entityId, updatedValues, dataFilter);

                //Check if there is a QOM ID
                if((replacementPrescribedValue.getQualifierOfMeasureID())!= null)
                {
                    // Create a map of QOM to be updated.
                    updatedValues = HashMaps.newHashMap(entityId + EF_VALUE_ID, replacementValueId, entityId+EF_VALUE_CONCEPT_ID, replacementPrescribedValue.getQualifierOfMeasureID(), entityId+EF_VALUE, replacementPrescribedValue.getQualifierOfMeasureID());

                    // Create filter criteria for the qom's to update.
                    filterCriteria = new T8DataFilterCriteria();
                    filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_VALUE_ID, DataFilterOperator.IN, prescribedValueIdList, false);
                    filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DATA_TYPE_ID, DataFilterOperator.EQUAL, RequirementType.QUALIFIER_OF_MEASURE.toString(), false);

                    // Create the filter and update the QOM ID's.
                    dataFilter = new T8DataFilter(entityId, filterCriteria);
                    updatedRecordsCount = tx.update(entityId, updatedValues, dataFilter);
                }

                break;
            case MEASURED_NUMBER:
                // Update the measured range.
                filterCriteria = new T8DataFilterCriteria(entityId);
                filterCriteria.addFilterClause(AND, new T8DataFilterCriterion(entityId + EF_VALUE_ID, IN, prescribedValueIdList));
                filterCriteria.addFilterClause(AND, new T8DataFilterCriterion(entityId + EF_DATA_TYPE_ID, EQUAL, RequirementType.MEASURED_NUMBER.toString()));
                updatedValues = new HashMap<>();
                updatedValues.put(entityId + EF_VALUE_ID, replacementValueId);
                dataFilter = new T8DataFilter(entityId, filterCriteria);
                tx.update(entityId, updatedValues, dataFilter);

                // Create a map of numeric values to be updated.
                updatedValues = HashMaps.newHashMap(entityId + EF_VALUE_ID, replacementValueId, entityId + EF_VALUE, replacementPrescribedValue.getValue());

                // Create filter criteria for the numeric values to update.
                filterCriteria = new T8DataFilterCriteria();
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DATA_TYPE_ID, DataFilterOperator.EQUAL, RequirementType.NUMBER.toString(), false);
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_VALUE_ID, DataFilterOperator.IN, prescribedValueIdList, false);

                // Create the filter and update the numeric values.
                dataFilter = new T8DataFilter(entityId, filterCriteria);
                tx.update(entityId, updatedValues, dataFilter);

                // Create a map of UOM to be updated.
                updatedValues = HashMaps.newHashMap(entityId + EF_VALUE_ID, replacementValueId, entityId+EF_VALUE_CONCEPT_ID, replacementPrescribedValue.getUnitOfMeasureID(), entityId + EF_VALUE, replacementPrescribedValue.getUnitOfMeasureID());

                // Create filter criteria for the uom's to update.
                filterCriteria = new T8DataFilterCriteria();
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_VALUE_ID, DataFilterOperator.IN, prescribedValueIdList, false);
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DATA_TYPE_ID, DataFilterOperator.EQUAL, RequirementType.UNIT_OF_MEASURE.toString(), false);

                // Create the filter and update the UOM ID's.
                dataFilter = new T8DataFilter(entityId, filterCriteria);
                updatedRecordsCount = tx.update(entityId, updatedValues, dataFilter);

                //Check if there is a QOM ID
                if((replacementPrescribedValue.getQualifierOfMeasureID())!= null)
                {
                    // Create a map of QOM to be updated.
                    updatedValues = HashMaps.newHashMap(entityId + EF_VALUE_ID, replacementValueId, entityId+EF_VALUE_CONCEPT_ID, replacementPrescribedValue.getQualifierOfMeasureID(), entityId+EF_VALUE, replacementPrescribedValue.getQualifierOfMeasureID());

                    // Create filter criteria for the qom's to update.
                    filterCriteria = new T8DataFilterCriteria();
                    filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_VALUE_ID, DataFilterOperator.IN, prescribedValueIdList, false);
                    filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DATA_TYPE_ID, DataFilterOperator.EQUAL, RequirementType.QUALIFIER_OF_MEASURE.toString(), false);

                    // Create the filter and update the QOM ID's.
                    dataFilter = new T8DataFilter(entityId, filterCriteria);
                    updatedRecordsCount = tx.update(entityId, updatedValues, dataFilter);
                }

                break;
            case INTEGER_TYPE:
            case REAL_TYPE:
            case STRING_TYPE:
                List<String> dataRequirementTypes;

                dataRequirementTypes = new ArrayList<>();

                dataRequirementTypes.add(RequirementType.STRING_TYPE.toString());
                dataRequirementTypes.add(RequirementType.REAL_TYPE.toString());
                dataRequirementTypes.add(RequirementType.INTEGER_TYPE.toString());

                // Create a map of values to be updated.
                updatedValues = HashMaps.newHashMap(entityId + EF_VALUE_ID, replacementValueId, entityId+EF_VALUE, replacementPrescribedValue.getValue());

                // Create filter criteria for the values to update.
                filterCriteria = new T8DataFilterCriteria();
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_VALUE_ID, DataFilterOperator.IN, prescribedValueIdList, false);
                filterCriteria.addFilterClause(DataFilterConjunction.AND, entityId + EF_DATA_TYPE_ID, DataFilterOperator.IN, dataRequirementTypes, false);

                // Create the filter and update the values.
                dataFilter = new T8DataFilter(entityId, filterCriteria);
                updatedRecordsCount = tx.update(entityId, updatedValues, dataFilter);

                break;
            default:
                throw new RuntimeException("Replacement of type not supported: " + replacementPrescribedValue.getRequirementType());
        }

       return updatedRecordsCount;
    }

    /**
     * Replaces all references of standard values, as defined by the standard
     * values reference filter with the specified standard value identifier &
     * deletes the old value.
     * The filter essentially defines the where clause for the standard value
     * references to be updated with the new standard value ID.
     *
     * @param tx The {@code T8DataTransaction} which provides access to the
     *      connected database
     * @param prescribedValueIDList A list of ID's of the prescribed values that
     *      will be replaced across all records by the specified replacement
     *      value.
     * @param replacementValueID The {@code String} standard value ID
     *      to be used as the replacement ID
     * @return The {@code Integer} number of references which have been replaced
     *      with the specified standard value ID
     *
     * @throws Exception If there is any error while updating the standard value
     *      references with the new value
     */
    public Integer replacePrescribedValues(T8DataTransaction tx, List<String> prescribedValueIDList, String replacementValueID) throws Exception
    {
        Integer updatedRecordsCount;

        // First replace references to the old value id with the new id.
        updatedRecordsCount = replacePrescribedValueReferences(tx, prescribedValueIDList, replacementValueID);

        // Now remove the old prescribed value from DATA_REQUIREMENT_VALUE.
        deletePrescribedValues(tx, prescribedValueIDList);

        // Return the amount of updated records.
        return updatedRecordsCount;
    }

    /**
     * retrievePrescribedValueSuggestions will retrieve the suggested values on
     * the Property/Field.
     *
     * @param tx - Data Connection to be used.
     * @param orgIDList - Organization ID List
     * @param valueRequirement - Consist of the object to get the Data Requirement, Property and Field Concept IDs to filter on.
     * @param searchPrefx - The value prefix that will be used to filter on.
     * @return Collection of RecordValue will be returned.
     * @throws java.lang.Exception
     */
    public List<RecordValue> retrievePrescribedValueSuggestions(T8DataTransaction tx, Collection<String> orgIDList, ValueRequirement valueRequirement, String searchPrefx) throws Exception
    {
        List<T8DataEntity> prescribedValueEntities;
        T8DataFilter recordValuesFilter;
        T8DataFilterCriteria filterCriteria;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;

        // Create the filter that will be used to fetch all Suggested Record Values required.
        recordValuesFilter = new T8DataFilter(entityIdentifier);

        // Create filter criteria for the suggestions to retrieve.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, entityIdentifier + EF_VALUE, DataFilterOperator.STARTS_WITH, searchPrefx, true);
        filterCriteria.addFilterClause(DataFilterConjunction.AND, entityIdentifier + EF_DR_ID, DataFilterOperator.EQUAL, valueRequirement.getDataRequirementID(), false);
        filterCriteria.addFilterClause(DataFilterConjunction.AND, entityIdentifier + EF_PROPERTY_ID, DataFilterOperator.EQUAL, valueRequirement.getPropertyID(), false);
        filterCriteria.addFilterClause(DataFilterConjunction.AND, entityIdentifier + EF_DATA_TYPE_ID, DataFilterOperator.EQUAL, valueRequirement.getRequirementType().getID(), false);
        if (valueRequirement.getFieldID() != null)
        {
            filterCriteria.addFilterClause(DataFilterConjunction.AND, entityIdentifier + EF_FIELD_ID, DataFilterOperator.EQUAL, valueRequirement.getFieldID(), false);
        }
        else filterCriteria.addFilterClause(DataFilterConjunction.AND, entityIdentifier + EF_FIELD_ID, DataFilterOperator.IS_NULL, null, false);

        // Add the filter criteria to the filter.
        recordValuesFilter.addFilterCriteria(DataFilterConjunction.AND, filterCriteria);

        // Add ordering according to the standard indicator and value.
        recordValuesFilter.addFieldOrdering(entityIdentifier + EF_APPROVED_INDICATOR, T8DataFilter.OrderMethod.DESCENDING);
        recordValuesFilter.addFieldOrdering(entityIdentifier + EF_VALUE, T8DataFilter.OrderMethod.ASCENDING);

        // Retrieve the values and return the results.
        prescribedValueEntities = tx.select(recordValuesFilter.getEntityIdentifier(), recordValuesFilter);
        return createPrescribedRecordValues(valueRequirement, prescribedValueEntities);
    }

    private static List<PrescribedValue> createPrescribedValues(List<T8DataEntity> prescribedValueEntities)
    {
        List<PrescribedValue> prescribedValues;

        prescribedValues = new ArrayList<>();
        for (T8DataEntity prescribedValueEntity : prescribedValueEntities)
        {
            prescribedValues.add(createPrescribedValue(prescribedValueEntity));
        }

        return prescribedValues;
    }

    private static PrescribedValue createPrescribedValue(T8DataEntity prescribedValueEntity)
    {
        PrescribedValue prescribedValue;
        String requirementTypeId;
        String valueId;
        String orgId;
        String drId;
        String drInstanceId;
        String propertyId;
        String fieldId;
        String value;
        String lowerBoundValue;
        String upperBoundValue;
        String conceptId;
        String uomId;
        String qomId;

        requirementTypeId = (String)prescribedValueEntity.getFieldValue(EF_DATA_TYPE_ID);
        valueId = (String)prescribedValueEntity.getFieldValue(EF_VALUE_ID);
        orgId = (String)prescribedValueEntity.getFieldValue(EF_ORG_ID);
        drId = (String)prescribedValueEntity.getFieldValue(EF_DR_ID);
        drInstanceId = (String)prescribedValueEntity.getFieldValue(EF_DR_INSTANCE_ID);
        propertyId = (String)prescribedValueEntity.getFieldValue(EF_PROPERTY_ID);
        fieldId = (String)prescribedValueEntity.getFieldValue(EF_FIELD_ID);
        value = (String)prescribedValueEntity.getFieldValue(EF_VALUE);
        lowerBoundValue = (String)prescribedValueEntity.getFieldValue(EF_LOWER_BOUND_VALUE);
        upperBoundValue = (String)prescribedValueEntity.getFieldValue(EF_UPPER_BOUND_VALUE);
        conceptId = (String)prescribedValueEntity.getFieldValue(EF_CONCEPT_ID);
        uomId = (String)prescribedValueEntity.getFieldValue(EF_UOM_ID);
        qomId = (String)prescribedValueEntity.getFieldValue(EF_QOM_ID);

        prescribedValue = new PrescribedValue(RequirementType.valueOf(requirementTypeId), valueId);
        prescribedValue.setOrganizationID(orgId);
        prescribedValue.setDataRequirementID(drId);
        prescribedValue.setDataRequirementInstanceID(drInstanceId);
        prescribedValue.setPropertyID(propertyId);
        prescribedValue.setFieldID(fieldId);
        prescribedValue.setValue(value);
        prescribedValue.setLowerBoundValue(lowerBoundValue);
        prescribedValue.setUpperBoundValue(upperBoundValue);
        prescribedValue.setConceptID(conceptId);
        prescribedValue.setUnitOfMeasureID(uomId);
        prescribedValue.setQualifierOfMeasureID(qomId);

        return prescribedValue;
    }

    private static List<OldPrescribedValue> createOldPrescribedValues(List<T8DataEntity> oldPrescribedValueEntities)
    {
        List<OldPrescribedValue> oldPrescribedValues;

        oldPrescribedValues = new ArrayList<>();
        for (T8DataEntity oldPrescribedValueEntity : oldPrescribedValueEntities)
        {
            oldPrescribedValues.add(createOldPrescribedValue(oldPrescribedValueEntity));
        }

        return oldPrescribedValues;
    }

    private static OldPrescribedValue createOldPrescribedValue(T8DataEntity oldPrescribedValueEntity)
    {
        OldPrescribedValue oldPrescribedValue;
        String requirementTypeId;
        String oldValueId;
        String valueId;
        String orgId;
        String drId;
        String drInstanceId;
        String propertyId;
        String fieldId;
        String value;
        String lowerBoundValue;
        String upperBoundValue;
        String conceptId;
        String conceptTerm;
        String uomId;
        String uomTerm;
        String qomId;
        String qomTerm;

        requirementTypeId = (String)oldPrescribedValueEntity.getFieldValue(EF_DATA_TYPE_ID);
        oldValueId = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_VALUE_ID);
        valueId = (String)oldPrescribedValueEntity.getFieldValue(EF_VALUE_ID);
        orgId = (String)oldPrescribedValueEntity.getFieldValue(EF_ORG_ID);
        drId = (String)oldPrescribedValueEntity.getFieldValue(EF_DR_ID);
        drInstanceId = (String)oldPrescribedValueEntity.getFieldValue(EF_DR_INSTANCE_ID);
        propertyId = (String)oldPrescribedValueEntity.getFieldValue(EF_PROPERTY_ID);
        fieldId = (String)oldPrescribedValueEntity.getFieldValue(EF_FIELD_ID);
        value = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_VALUE);
        lowerBoundValue = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_LOWER_BOUND_VALUE);
        upperBoundValue = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_UPPER_BOUND_VALUE);
        conceptId = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_CONCEPT_ID);
        conceptTerm = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_CONCEPT_TERM);
        uomId = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_UOM_ID);
        uomTerm = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_UOM_TERM);
        qomId = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_QOM_ID);
        qomTerm = (String)oldPrescribedValueEntity.getFieldValue(EF_OLD_QOM_TERM);

        oldPrescribedValue = new OldPrescribedValue(RequirementType.valueOf(requirementTypeId), oldValueId);
        oldPrescribedValue.setValueId(valueId);
        oldPrescribedValue.setOrgId(orgId);
        oldPrescribedValue.setDrId(drId);
        oldPrescribedValue.setDrInstanceId(drInstanceId);
        oldPrescribedValue.setPropertyId(propertyId);
        oldPrescribedValue.setFieldId(fieldId);
        oldPrescribedValue.setValue(value);
        oldPrescribedValue.setLowerBoundValue(lowerBoundValue);
        oldPrescribedValue.setUpperBoundValue(upperBoundValue);
        oldPrescribedValue.setConceptId(conceptId);
        oldPrescribedValue.setConceptTerm(conceptTerm);
        oldPrescribedValue.setUomId(uomId);
        oldPrescribedValue.setUomTerm(uomTerm);
        oldPrescribedValue.setQomId(qomId);
        oldPrescribedValue.setQomTerm(qomTerm);

        return oldPrescribedValue;
    }

    public static List<RecordValue> createPrescribedRecordValues(ValueRequirement valueRequirement, List<T8DataEntity> prescribedValueEntities)
    {
        List<RecordValue> recordValues;

        // Create a RecordValue from each of the entities in the list.
        recordValues = new ArrayList<>();
        for (T8DataEntity dataEntity : prescribedValueEntities)
        {
            RecordValue recordValue;

            // Get the field values from the entity.
            recordValue = createPrescribedRecordValue(valueRequirement, dataEntity);

            // Add to the returning list
            recordValues.add(recordValue);
        }

        return recordValues;
    }

    public static RecordValue createPrescribedRecordValue(ValueRequirement valueRequirement, T8DataEntity prescribedValueEntity)
    {
        String standardIndicator;
        String approvedIndicator;
        String entityId;
        String valueId;
        boolean standard;
        boolean approved;

        // Get the standard value identifier from the entity
        entityId = prescribedValueEntity.getIdentifier();
        valueId = (String)prescribedValueEntity.getFieldValue(entityId + EF_VALUE_ID);
        standardIndicator = (String)prescribedValueEntity.getFieldValue(entityId + EF_STANDARD_INDICATOR);
        approvedIndicator = (String)prescribedValueEntity.getFieldValue(entityId + EF_APPROVED_INDICATOR);
        standard = "Y".equalsIgnoreCase(standardIndicator);
        approved = "Y".equalsIgnoreCase(approvedIndicator);

        switch (valueRequirement.getRequirementType())
        {
            case CONTROLLED_CONCEPT:
            {
                ControlledConcept value;
                String conceptId;

                conceptId = (String) prescribedValueEntity.getFieldValue(entityId + EF_CONCEPT_ID);
                value = new ControlledConcept(valueRequirement);
                value.setConceptId(conceptId, false);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case DOCUMENT_REFERENCE:
            {
                DocumentReferenceRequirement requirement;
                DocumentReferenceList value;
                String conceptId;

                requirement = (DocumentReferenceRequirement)valueRequirement;
                conceptId = (String)prescribedValueEntity.getFieldValue(entityId + EF_CONCEPT_ID);
                value = new DocumentReferenceList(requirement);
                if (requirement.isIndependent())
                {
                    // If this list contains independent references, the concepts will be Record ids.
                    value.addReference(conceptId);
                    value.setPrescribedValueID(valueId);
                    value.setStandard(standard);
                    value.setApproved(approved);
                    return value;
                }
                else
                {
                    // If this list contains dependent references, the concepts will be DR Instance ids.
                    value.addReference(DocumentReferenceType.DR_INSTANCE, conceptId);
                    value.setPrescribedValueID(valueId);
                    value.setStandard(standard);
                    value.setApproved(approved);
                    return value;
                }
            }
            case QUALIFIER_OF_MEASURE:
            {
                QualifierOfMeasure value;
                String qomId;

                qomId = (String) prescribedValueEntity.getFieldValue(entityId + EF_QOM_ID);
                value = new QualifierOfMeasure(valueRequirement);
                value.setValue(qomId);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case UNIT_OF_MEASURE:
            {
                UnitOfMeasure value;
                String uomId;

                uomId = (String) prescribedValueEntity.getFieldValue(entityId + EF_UOM_ID);
                value = new UnitOfMeasure(valueRequirement);
                value.setValue(uomId);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case MEASURED_NUMBER:
            {
                MeasuredNumber value;
                String stringValue;
                String uomID;
                String qomID;

                stringValue = (String) prescribedValueEntity.getFieldValue(entityId + EF_VALUE);
                uomID = (String) prescribedValueEntity.getFieldValue(entityId + EF_UOM_ID);
                qomID = (String) prescribedValueEntity.getFieldValue(entityId + EF_QOM_ID);
                value = new MeasuredNumber(valueRequirement);
                value.setNumber(stringValue);
                value.setUomId(uomID);
                value.setQomId(qomID);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case MEASURED_RANGE:
            {
                MeasuredRange value;
                String lowerBoundValue;
                String upperBoundValue;
                String uomID;
                String qomID;

                lowerBoundValue = (String) prescribedValueEntity.getFieldValue(entityId + EF_LOWER_BOUND_VALUE);
                upperBoundValue = (String) prescribedValueEntity.getFieldValue(entityId + EF_UPPER_BOUND_VALUE);
                uomID = (String) prescribedValueEntity.getFieldValue(entityId + EF_UOM_ID);
                qomID = (String) prescribedValueEntity.getFieldValue(entityId + EF_QOM_ID);
                value = new MeasuredRange(valueRequirement);
                value.setLowerBoundNumber(lowerBoundValue);
                value.setUpperBoundNumber(upperBoundValue);
                value.setUomId(uomID);
                value.setQomId(qomID);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case INTEGER_TYPE:
            {
                IntegerValue value;
                String stringValue;

                stringValue = (String) prescribedValueEntity.getFieldValue(entityId + EF_VALUE);
                value = new IntegerValue(valueRequirement);
                value.setValue(Integer.parseInt(stringValue));
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case REAL_TYPE:
            {
                RealValue value;
                String stringValue;

                stringValue = (String) prescribedValueEntity.getFieldValue(entityId + EF_VALUE);
                value = new RealValue(valueRequirement);
                value.setValue(new BigDecimal(stringValue));
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case STRING_TYPE:
            {
                StringValue value;
                String stringValue;

                stringValue = (String) prescribedValueEntity.getFieldValue(entityId + EF_VALUE);
                value = new StringValue(valueRequirement);
                value.setString(stringValue);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            default:
            {
                throw new IllegalStateException("Invalid record value type: " + valueRequirement.getRequirementType());
            }
        }
    }

    /**
     * Sets the standardized indicator for a specific data requirement value. The
     * flag passed is set for the specified standard value ID.<br/>
     * <br/>
     *
     * @param tx The {@code T8DataTransaction} which provides access to the
     *      connected database
     * @param prescribedValueID The {@code String} ID of the standard value to be
     *      approved
     * @param standardIndicator {@code true} to flag as standardized. {@code false}
     *      to remove
     *
     * @throws Exception If any error occurs while setting the standardized flag.
     */
    public void setStandard(T8DataTransaction tx, String prescribedValueID, boolean standardIndicator) throws Exception
    {
        Map<String, Object> updatedValues;
        String entityIdentifier;
        T8DataFilterCriteria filterCriteria;
        T8DataFilter dataFilter;

        entityIdentifier = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;
        updatedValues = HashMaps.createSingular(entityIdentifier + EF_STANDARD_INDICATOR, (standardIndicator ? "Y" : "N"));

        // Create filter criteria for the suggestions to retrieve.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, entityIdentifier + EF_VALUE_ID, DataFilterOperator.EQUAL, prescribedValueID, false);

        // Create the filter and update the values.
        dataFilter = new T8DataFilter(entityIdentifier, filterCriteria);
        tx.update(entityIdentifier, updatedValues, dataFilter);
    }

    /**
     * Sets the approved indicator for a specific data requirement value. The
     * flag passed is set for the specified standard value ID.<br/>
     * <br/>
     *
     * @param tx The {@code T8DataTransaction} which provides access to the
     *      connected database
     * @param prescribedValueID The {@code String} ID of the standard value to be
     *      approved
     * @param approvedIndicator {@code true} to flag as approved. {@code false}
     *      to remove
     *
     * @throws Exception If any error occurs while setting the approved flag.
     */
    public void setApproved(T8DataTransaction tx, String prescribedValueID, boolean approvedIndicator) throws Exception
    {
        Map<String, Object> updatedValues;
        String entityIdentifier;
        T8DataFilterCriteria filterCriteria;
        T8DataFilter dataFilter;

        entityIdentifier = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;
        updatedValues = HashMaps.createSingular(entityIdentifier + EF_APPROVED_INDICATOR, (approvedIndicator ? "Y" : "N"));

        // Create filter criteria for the suggestions to retrieve.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, entityIdentifier + EF_VALUE_ID, DataFilterOperator.EQUAL, prescribedValueID, false);

        // Create the filter and update the values.
        dataFilter = new T8DataFilter(entityIdentifier, filterCriteria);
        tx.update(entityIdentifier, updatedValues, dataFilter);
    }

    /**
     * Deletes the specified prescribed values. If a value is in use, it is not
     * deleted.
     * @param tx The {@code T8DataTransaction} which provides access to the
     *      connected database.
     * @param valueIDList A list of the prescribed value ID's which are to be
     *      deleted.
     * @throws Exception If any error during the deletion of prescribed values.
     */
    public void deletePrescribedValues(T8DataTransaction tx, List valueIDList) throws Exception
    {
        String entityIdentifier;
        String oldValueEntityIdentifier;
        T8DataFilter filter;
        T8DataFilter oldValueFilter;

        entityIdentifier = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;
        oldValueEntityIdentifier = DATA_REQUIREMENT_VALUE_OLD_DE_IDENTIFIER;

        //Create a filter to delete any value references from DATA_REQUIREMENT_VALUE_OLD.
        oldValueFilter = new T8DataFilter(oldValueEntityIdentifier);
        oldValueFilter.addFilterCriterion(DataFilterConjunction.AND, oldValueEntityIdentifier + EF_VALUE_ID, DataFilterOperator.IN, valueIDList, false);

        // Delete Old Values using filter.
        tx.delete(oldValueEntityIdentifier, oldValueFilter);

        //Create a filter for the values to be deleted from DATA_REQUIREMENT_VALUE.
        filter = new T8DataFilter(entityIdentifier);
        filter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_VALUE_ID, DataFilterOperator.IN, valueIDList, false);

        // Delete Values using filter.
        tx.delete(entityIdentifier, filter);
    }

    public void insertOldPrescribedValue(T8DataTransaction tx, OldPrescribedValue oldValue) throws Exception
    {
        String drvOldEntityIdentifier;
        T8DataEntity historicValueEntity;

        drvOldEntityIdentifier = DATA_REQUIREMENT_VALUE_OLD_DE_IDENTIFIER;

        //Create the entity to be inserted
        historicValueEntity = tx.create(drvOldEntityIdentifier, null);
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_VALUE_ID, oldValue.getOldValueId());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_VALUE_ID, oldValue.getValueId());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_VALUE, oldValue.getValue());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_LOWER_BOUND_VALUE, oldValue.getLowerBoundValue());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_UPPER_BOUND_VALUE, oldValue.getUpperBoundValue());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_CONCEPT_ID, oldValue.getConceptId());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_CONCEPT_TERM, oldValue.getConceptTerm());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_UOM_ID, oldValue.getUomId());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_UOM_TERM, oldValue.getUomTerm());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_QOM_ID, oldValue.getQomId());
        historicValueEntity.setFieldValue(drvOldEntityIdentifier + EF_OLD_QOM_TERM, oldValue.getQomTerm());

        //Insert the value history
        tx.insert(historicValueEntity);
    }
}
