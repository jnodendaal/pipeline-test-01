package com.pilog.t8.data.source.datarequirement;

import com.pilog.t8.data.tag.T8TagUtilities;
import com.pilog.t8.data.document.DocumentHandler;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute.RequirementAttributeType;
import com.pilog.t8.utilities.strings.Strings;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8RequirementDataHandler
{
    public static List<Map<String, Object>> getDataRequirementInstanceDataRows(DataRequirementInstance dataRequirementInstance)
    {
        List<Map<String, Object>> dataRows;
        HashMap<String, Object> dataRow;

        dataRow = new HashMap<>();
        dataRow.put("DR_INSTANCE_ID", dataRequirementInstance.getConceptID());
        dataRow.put("DR_ID", dataRequirementInstance.getDataRequirement().getConceptID());
        dataRow.put("RECORD_OC_ID", dataRequirementInstance.getRecordOcId());
        dataRow.put("INDEPENDENT_INDICATOR", dataRequirementInstance.isIndependent() ? "Y" : "N");

        dataRows = new ArrayList<>();
        dataRows.add(dataRow);
        return dataRows;
    }

    public static List<Map<String, Object>> getDataRequirementDataRows(DataRequirement dataRequirement)
    {
        List<Map<String, Object>> dataRows;
        HashMap<String, Object> dataRow;

        dataRow = new HashMap<>();
        dataRow.put("DR_ID", dataRequirement.getConceptID());
        dataRow.put("CLASS_ID", dataRequirement.getClassConceptID());

        dataRows = new ArrayList<>();
        dataRows.add(dataRow);
        return dataRows;
    }

    public static List<Map<String, Object>> getDataRequirementAttributeDataRows(DataRequirement dataRequirement)
    {
        List<Map<String, Object>> dataRows;
        Map<String, Object> attributes;

        attributes = dataRequirement.getAttributes();
        dataRows = new ArrayList<>();
        for (String attributeID : attributes.keySet())
        {
            HashMap<String, Object> dataRow;
            Object value;

            value = attributes.get(attributeID);

            dataRow = new HashMap<>();
            dataRow.put("DR_ID", dataRequirement.getConceptID());
            dataRow.put("ATTRIBUTE_ID", attributeID);
            dataRow.put("VALUE", value != null ? value.toString() : null);
            if (value instanceof BigDecimal) dataRow.put("TYPE", RequirementAttributeType.NUMBER.toString());
            else if (value instanceof Boolean) dataRow.put("TYPE", RequirementAttributeType.BOOLEAN.toString());
            else dataRow.put("TYPE", RequirementAttributeType.STRING.toString());

            dataRows.add(dataRow);
        }

        return dataRows;
    }

    public static List<Map<String, Object>> getSectionRequirementDataRows(DataRequirement dataRequirement)
    {
        List<Map<String, Object>> dataRows;
        ArrayList<SectionRequirement> sectionRequirements;

        sectionRequirements = dataRequirement.getSectionRequirements();
        dataRows = new ArrayList<>();
        for (SectionRequirement sectionRequirement : sectionRequirements)
        {
            HashMap<String, Object> dataRow;

            dataRow = new HashMap<>();
            dataRow.put("DR_ID", dataRequirement.getConceptID());
            dataRow.put("SECTION_ID", sectionRequirement.getConceptID());

            dataRows.add(dataRow);
        }

        return dataRows;
    }

    public static List<Map<String, Object>> getPropertyRequirementDataRows(DataRequirement dataRequirement)
    {
        List<Map<String, Object>> dataRows;
        ArrayList<PropertyRequirement> propertyRequirements;

        propertyRequirements = dataRequirement.getPropertyRequirements();
        dataRows = new ArrayList<>();
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            HashMap<String, Object> dataRow;
            SectionRequirement sectionRequirement;

            sectionRequirement = propertyRequirement.getParentSectionRequirement();

            dataRow = new HashMap<>();
            dataRow.put("DR_ID", dataRequirement.getConceptID());
            dataRow.put("SECTION_ID", sectionRequirement.getConceptID());
            dataRow.put("PROPERTY_ID", propertyRequirement.getConceptID());
            dataRow.put("REQUIRED", propertyRequirement.isCharacteristic() ? "Y" : "N");
            dataRow.put("PROPERTY_SEQUENCE", dataRequirement.getPropertyRequirementIndex(propertyRequirement.getConceptID()));
            dataRow.put("ATTRIBUTES", getRequirementAttributeString(propertyRequirement.getAttributes()));

            dataRows.add(dataRow);
        }

        return dataRows;
    }

    public static List<Map<String, Object>> getPropertyRequirementAttributeDataRows(DataRequirement dataRequirement)
    {
        ArrayList<PropertyRequirement> propertyRequirements;
        List<Map<String, Object>> dataRows;

        dataRows = new ArrayList<>();
        propertyRequirements = dataRequirement.getPropertyRequirements();
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            Map<String, Object> attributes;

            attributes = propertyRequirement.getAttributes();
            for (String attributeID : attributes.keySet())
            {
                HashMap<String, Object> dataRow;
                Object value;

                value = attributes.get(attributeID);

                dataRow = new HashMap<>();
                dataRow.put("DR_ID", propertyRequirement.getDataRequirementID());
                dataRow.put("PROPERTY_ID", propertyRequirement.getConceptID());
                dataRow.put("ATTRIBUTE_ID", attributeID);
                dataRow.put("VALUE", value != null ? value.toString() : null);
                if (value instanceof BigDecimal) dataRow.put("TYPE", RequirementAttributeType.NUMBER.toString());
                else if (value instanceof Boolean) dataRow.put("TYPE", RequirementAttributeType.BOOLEAN.toString());
                else dataRow.put("TYPE", RequirementAttributeType.STRING.toString());

                dataRows.add(dataRow);
            }
        }

        return dataRows;
    }

    public static List<Map<String, Object>> getValueRequirementDataRows(DataRequirement dataRequirement)
    {
        List<Map<String, Object>> dataRows;
        ArrayList<ValueRequirement> valueRequirements;

        valueRequirements = DocumentHandler.getAllValueRequirements(dataRequirement);
        dataRows = new ArrayList<>();
        for (ValueRequirement valueRequirement : valueRequirements)
        {
            HashMap<String, Object> dataRow;
            SectionRequirement sectionRequirement;
            PropertyRequirement propertyRequirement;
            ValueRequirement parentValueRequirement;

            parentValueRequirement = valueRequirement.getParentValueRequirement();
            propertyRequirement = valueRequirement.getParentPropertyRequirement();
            sectionRequirement = propertyRequirement.getParentSectionRequirement();

            dataRow = new HashMap<>();
            dataRow.put("DR_ID", dataRequirement.getConceptID());
            dataRow.put("SECTION_ID", sectionRequirement.getConceptID());
            dataRow.put("PROPERTY_ID", propertyRequirement.getConceptID());
            dataRow.put("DATA_TYPE_ID", valueRequirement.getRequirementType().getID());
            dataRow.put("DATA_TYPE_SEQUENCE", valueRequirement.getSequence());
            dataRow.put("PARENT_DATA_TYPE_ID", parentValueRequirement != null ? parentValueRequirement.getRequirementType().getID() : null);
            dataRow.put("PARENT_DATA_TYPE_SEQ", parentValueRequirement != null ? parentValueRequirement.getSequence() : null);
            dataRow.put("VALUE", valueRequirement.getValue());
            dataRow.put("ATTRIBUTES", getRequirementAttributeString(valueRequirement.getAttributes()));
            dataRow.put("PRESCRIBED_INDICATOR", valueRequirement.isPrescribed() ? "Y" : "N");

            dataRows.add(dataRow);
        }

        return dataRows;
    }

    public static List<Map<String, Object>> getValueRequirementAttributeDataRows(DataRequirement dataRequirement)
    {
        ArrayList<PropertyRequirement> propertyRequirements;
        List<Map<String, Object>> dataRows;

        dataRows = new ArrayList<>();
        propertyRequirements = dataRequirement.getPropertyRequirements();
        for (PropertyRequirement propertyRequirement : propertyRequirements)
        {
            for (ValueRequirement valueRequirement : propertyRequirement.getAllValueRequirements())
            {
                Map<String, Object> attributes;

                attributes = valueRequirement.getAttributes();
                for (String attributeID : attributes.keySet())
                {
                    HashMap<String, Object> dataRow;
                    Object value;

                    value = attributes.get(attributeID);

                    dataRow = new HashMap<>();
                    dataRow.put("DR_ID", valueRequirement.getDataRequirementID());
                    dataRow.put("PROPERTY_ID", valueRequirement.getPropertyID());
                    dataRow.put("DATA_TYPE_ID", valueRequirement.getRequirementType().getID());
                    dataRow.put("DATA_TYPE_SEQUENCE", valueRequirement.getSequence());
                    dataRow.put("ATTRIBUTE_ID", attributeID);
                    dataRow.put("VALUE", value != null ? value.toString() : null);
                    if (value instanceof BigDecimal) dataRow.put("TYPE", RequirementAttributeType.NUMBER.toString());
                    else if (value instanceof Boolean) dataRow.put("TYPE", RequirementAttributeType.BOOLEAN.toString());
                    else dataRow.put("TYPE", RequirementAttributeType.STRING.toString());

                    dataRows.add(dataRow);
                }
            }
        }

        return dataRows;
    }

    public static final String getRequirementAttributeString(Map<String, Object> attributes)
    {
        LinkedHashMap<String, String> attributeMap;

        attributeMap = new LinkedHashMap<>();
        for (String attributeID : attributes.keySet())
        {
            Object value;

            value = attributes.get(attributeID);
            if (value != null)
            {
                String stringValue;

                stringValue = value.toString();
                if (!Strings.isNullOrEmpty(stringValue))
                {
                    attributeMap.put(attributeID, stringValue);
                }
            }
        }

        return T8TagUtilities.getTagString(attributeMap);
    }

    public static final Map<String, Object> getAttributeMap(String attributeString)
    {
        if (attributeString != null)
        {
            Map<String, String> attributeStringMap;
            Map<String, Object> attributeMap;

            attributeMap = new LinkedHashMap<>();
            attributeStringMap = T8TagUtilities.parseTagMap(attributeString);
            if (attributeStringMap != null)
            {
                for (String attributeID : attributeStringMap.keySet())
                {
                    String stringValue;

                    stringValue = attributeStringMap.get(attributeID);
                    if (!Strings.isNullOrEmpty(stringValue))
                    {
                        RequirementAttribute attribute;

                        attribute = RequirementAttribute.getRequirementAttribute(attributeID);
                        if (attribute != null)
                        {
                            switch (attribute.getType())
                            {
                                case NUMBER:
                                    attributeMap.put(attributeID, new BigDecimal(stringValue));
                                    break;
                                case BOOLEAN:
                                    attributeMap.put(attributeID, Boolean.parseBoolean(stringValue));
                                    break;
                                default:
                                    attributeMap.put(attributeID, stringValue);
                            }
                        }
                        else
                        {
                            attributeMap.put(attributeID, stringValue);
                        }
                    }
                }

                return attributeMap;
            }
            else return null;
        }
        else return null;
    }
}
