package com.pilog.t8.data.alteration.handler;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import com.pilog.json.JsonObject;
import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.api.T8DataRequirementApi;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.alteration.T8DataAlterationHandler;
import com.pilog.t8.data.alteration.T8DataAlterationImpact;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.alteration.T8DataAlteration.T8DataAlterationMethod;
import com.pilog.t8.data.alteration.T8DataAlterationDataHandler;
import com.pilog.t8.data.alteration.type.T8DataRequirementAlteration;
import com.pilog.t8.data.document.T8DocumentSerializer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implements a handler capable of maintaining and applying {@code T8DataRequirementAlteration} objects.
 *
 * @author Bouwer du Preez
 */
public class T8DataRequirementAlterationHandler implements T8DataAlterationHandler
{
    private final T8ServerContext serverContext;

    public T8DataRequirementAlterationHandler(T8Context context)
    {
        this.serverContext = context.getServerContext();
    }

    @Override
    public String getAlterationId()
    {
        return T8DataRequirementAlteration.ALTERATION_ID;
    }

    @Override
    public int deleteAlteration(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        int requirementDeletedCount = 0;
        T8DataRequirementAlterationDataHandler handler;
        T8DataAlterationDataHandler conceptHandler;

        // Set values of the needed variables
        handler = new T8DataRequirementAlterationDataHandler(tx);
        conceptHandler =  new T8DataAlterationDataHandler(serverContext);

        // Delete the data requirement concepts.
        conceptHandler.deleteOntologyConcept(tx, packageIid, step);

        // Delete the data requirement.
        handler.deleteDataRequirement(packageIid, step);

        return requirementDeletedCount;
    }

    @Override
    public void insertAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8DataRequirementAlteration requirementAlteration;
        DataRequirement dataRequirement;
        T8DataRequirementAlterationDataHandler handler;
        T8DataAlterationDataHandler conceptHandler;
        String packageIid;
        int step;
        String method;

        // Set values of the needed variables
        handler = new T8DataRequirementAlterationDataHandler(tx);
        conceptHandler =  new T8DataAlterationDataHandler(serverContext);
        requirementAlteration = (T8DataRequirementAlteration)alteration;
        dataRequirement = requirementAlteration.getDataRequirement();
        packageIid = requirementAlteration.getPackageIid();
        step = requirementAlteration.getStep();
        method = requirementAlteration.getAlterationMethod().toString();

        // Insert the concepts
        for (T8OntologyConcept concept : requirementAlteration.getConcepts())
        {
            conceptHandler.insertOntologyConcept(tx, concept, packageIid, step , method);
        }

        // Insert the data requirement.
        handler.insertDataRequirement(packageIid, step, method, dataRequirement);
    }

    @Override
    public void updateAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8DataRequirementAlteration requirementAlteration;
        DataRequirement dataRequirement;
        T8DataRequirementAlterationDataHandler handler;
        T8DataAlterationDataHandler conceptHandler;
        String packageIid;
        int step;
        String method;

        // Set values of the needed variables
        handler = new T8DataRequirementAlterationDataHandler(tx);
        conceptHandler =  new T8DataAlterationDataHandler(serverContext);
        requirementAlteration = (T8DataRequirementAlteration)alteration;
        dataRequirement = requirementAlteration.getDataRequirement();
        packageIid = requirementAlteration.getPackageIid();
        step = requirementAlteration.getStep();
        method = requirementAlteration.getAlterationMethod().toString();

        // Update concept
        for (T8OntologyConcept concept : requirementAlteration.getConcepts())
        {
            conceptHandler.updateOntologyConcept(tx, concept, packageIid, step , method);
        }

        // Update the data requirement.
        handler.updateDataRequirement(packageIid, step, method, dataRequirement);
    }

    @Override
     public T8DataAlterationImpact applyAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8DataRequirementAlteration dataRequirementAlteration;
        T8DataRecordValueStringGenerator valueStringGenerator;
        T8DataAlterationImpact alterationImpact;
        DataRequirement newDataRequirement;
        DataRequirement oldDataRequirement;
        T8DataAlterationMethod alterationMethod;
        T8DataRequirementApi drApi;
        T8OntologyApi ontApi;
        T8DataRecordApi recApi;
        T8TerminologyApi trmApi;
        String drId;

        // Initialize variables.
        dataRequirementAlteration = (T8DataRequirementAlteration)alteration;
        newDataRequirement = dataRequirementAlteration.getDataRequirement();
        alterationMethod = dataRequirementAlteration.getAlterationMethod();
        drApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
        recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
        drId = newDataRequirement.getConceptID();
        alterationImpact = new T8DataAlterationImpact();

        // Create a value string generator to use during alteration.
        valueStringGenerator = new T8DataRecordValueStringGenerator();
        valueStringGenerator.setTerminologyProvider(trmApi.getTerminologyProvider());

        // Process the alteration depending on the method.
        if (alterationMethod.equals(T8DataAlterationMethod.INSERT))
        {
            List<T8OntologyConcept> alteredConceptList;

            // Get list of ontology concepts
            alteredConceptList = dataRequirementAlteration.getConcepts();

            // Save concepts before saving data requirement
            for (T8OntologyConcept newConcept : alteredConceptList)
            {
                T8OntologyConcept oldConcept;
                String conceptId;

                // Retrieve the existing concept (if any).
                conceptId = newConcept.getID();
                oldConcept = ontApi.retrieveConcept(conceptId);

                // If no existing ontology concept was found, add organization links for the terms, definitions, codes and concepts.
                if (oldConcept == null)
                {
                    // Save the concept.  If the concept is a DR Instance, don't save it now as that will happen later.
                    if (newConcept.getConceptType() != T8OntologyConceptType.DATA_REQUIREMENT_INSTANCE)
                    {
                        ontApi.insertConcept(newConcept, true, true, true, true, true);
                    }
                }
                else
                {
                    // Apply changes to the old concept.
                    // TODO:  Implement this case.
                }
            }

            // Save the DR (which will result in an insert if its a new DR).
            drApi.saveDataRequirement(newDataRequirement, false);

            // Insert the data requirement instances.
            for (T8OntologyConcept newConcept : alteredConceptList)
            {
                if (newConcept.getConceptType().equals(T8OntologyConceptType.DATA_REQUIREMENT_INSTANCE))
                {
                    drApi.insertDataRequirementInstance(newConcept, drId, false);
                }
            }
        }
        else if (alterationMethod.equals(T8DataAlterationMethod.DELETE))
        {
            String entityId;
            T8DataFilter dataFilter;

            // Entity id to be used to count from.
            entityId = DATA_RECORD_DOCUMENT_DE_IDENTIFIER;

            // Create DR_ID data filter.
            dataFilter = new T8DataFilter(entityId, HashMaps.newHashMap(entityId + EF_DR_ID, drId));

            // If no records are found that use this DR, it's safe to delete the DR.
            if (tx.count(entityId, dataFilter) == 0)
            {
                drApi.deleteDataRequirement(drId, true);
            }
            else throw new Exception("Existing records linked to DR. Cannot apply DELETE alteration.");
        }
        else // The alteration method is an update.
        {
            List<String> newDrPropertyIdList;
            List<String> oldDrPropertyIdList;
            Map<String, String> supersededPropertyIdMap;
            Set<String> insertedPropertyIdSet;
            Set<String> deletedPropertyIdSet;
            Set<String> updatedPropertyIdSet;
            Set<String> affectedPropertyIdSet;
            Set<String> affectedRecordIdSet;
            int updatedPropertyCount;
            int deletedPropertyCount;
            List<T8OntologyConcept> alteredConceptList;

            // Get list of ontology concepts.
            alteredConceptList = dataRequirementAlteration.getConcepts();

            // Make sure all required concepts exist and are update to the latest version before proceeding with the data requirement.
            for (T8OntologyConcept newConcept : alteredConceptList)
            {
                ontApi.saveConcept(newConcept, true, true, true, true, true, true);
            }

            // Firstly, retrieve the old data requirement to be altered.
            oldDataRequirement = drApi.retrieveDataRequirement(drId, null, true, false);

            // Get the list of property id's from the old and new data requirements.
            newDrPropertyIdList = newDataRequirement.getPropertyRequirementIDList();
            oldDrPropertyIdList = oldDataRequirement.getPropertyRequirementIDList();

            // Take all the properties in the new DR and remove the corresponding properties from the old DR, remaining properties are those that are specified as inserts.
            insertedPropertyIdSet = new HashSet<>(newDrPropertyIdList);
            insertedPropertyIdSet.removeAll(oldDrPropertyIdList);

            // Get the id's of properties that existing in both the old and the new DR's.  These are the potentially updated properties.
            updatedPropertyIdSet = new HashSet<>(newDrPropertyIdList);
            updatedPropertyIdSet.retainAll(oldDrPropertyIdList);

            // Take all the properties in the old DR and remove the corresponding properties from the new DR, remaining properties are those that are specified as deletions.
            deletedPropertyIdSet = new HashSet<>(oldDrPropertyIdList);
            deletedPropertyIdSet.removeAll(newDrPropertyIdList);

            // Construct the superseded property id map.
            supersededPropertyIdMap = new HashMap<>();
            for (PropertyRequirement newPropertyRequirement : newDataRequirement.getPropertyRequirements())
            {
                String supersededPropertyId;

                supersededPropertyId = (String)newPropertyRequirement.getAttribute(T8DataRequirementAlteration.SUPERSEDED_PROPERTY_ATTRIBUTE_ID);
                if (supersededPropertyId != null)
                {
                    supersededPropertyIdMap.put(supersededPropertyId, newPropertyRequirement.getConceptID());
                }
            }

            // Combine both updated and deleted property lists to get a collection of all properties that will affect records using them.
            affectedPropertyIdSet = new HashSet<>();
            affectedPropertyIdSet.addAll(updatedPropertyIdSet);
            affectedPropertyIdSet.addAll(deletedPropertyIdSet);
            affectedPropertyIdSet.addAll(supersededPropertyIdMap.keySet());

            // Using the set of affected property id's, retrieve all records that contain content for one of more of these properties.
            affectedRecordIdSet = retrieveRecordIdSet(tx, drId, affectedPropertyIdSet, true);
            alterationImpact.setAffectedRecordCount(affectedRecordIdSet.size());

            // Before we apply the alteration to all affected records, we have to save an intermediate version of the new data requirement
            // containing all of the newly added properties.  This is required so that superseded property values can be moved to these
            // newly added properties by the subsequent record update loop.
            if ((insertedPropertyIdSet.size() > 0) && (supersededPropertyIdMap.size() > 0))
            {
                DataRequirement intermediateDataRequirement;

                // Make a copy of the old data requirement (the existing one) and add all newly inserted properties to it.
                intermediateDataRequirement = oldDataRequirement.copy();
                for (String insertedPropertyId : insertedPropertyIdSet)
                {
                    PropertyRequirement newPropertyRequirement;
                    PropertyRequirement intermediatePropertyRequirement;

                    newPropertyRequirement = newDataRequirement.getPropertyRequirement(insertedPropertyId);
                    intermediatePropertyRequirement = intermediateDataRequirement.getOrAddPropertyRequirement(newPropertyRequirement.getSectionId(), newPropertyRequirement.getId());
                    intermediatePropertyRequirement.setValueRequirement(newPropertyRequirement.getValueRequirement().copy());
                }

                // Save the intermediate data requirement.
                drApi.saveDataRequirement(intermediateDataRequirement, true);
            }

            // Loop through all records using the dr-property combination.
            updatedPropertyCount = 0;
            deletedPropertyCount = 0;
            for (String affectedRecordId : affectedRecordIdSet)
            {
                DataRecord dataFile;

                // Retrieve the data file and loop through its content records, applying the DR alteration to each one as required.
                dataFile = recApi.retrieveFile(affectedRecordId, null, true, false, false, false, false, false);
                for (DataRecord record : dataFile.getDataFileRecords())
                {
                    // Only process the record if it is based on the altered data requirement.
                    if (record.getDataRequirementID().equals(drId))
                    {
                        // Process each of the superseded properties.
                        for (String supersededPropertyId : supersededPropertyIdMap.keySet())
                        {
                            RecordProperty supersededProperty;
                            RecordProperty supersedingProperty;
                            String supersedingPropertyId;

                            // Get the superseded property.
                            supersededProperty = record.getRecordProperty(supersededPropertyId);

                            // Get the superseding property.
                            // The current design assumes that the superseding property has been newly added as part of this alteration.
                            supersedingPropertyId = supersededPropertyIdMap.get(supersededPropertyId);
                            supersedingProperty = record.getOrAddRecordProperty(supersedingPropertyId);

                            // Move the superseded property content to the superseding property.
                            supersedingProperty.setContent(supersededProperty.getContent());
                        }

                        // Process each of the updated properties.
                        for (String propertyId : updatedPropertyIdSet)
                        {
                            PropertyRequirement oldPropertyRequirement;
                            PropertyRequirement newPropertyRequirement;
                            ValueRequirement oldValueRequirement;
                            ValueRequirement newValueRequirement;

                            // Get the old and new property requirements.
                            newPropertyRequirement = newDataRequirement.getPropertyRequirement(propertyId);
                            newValueRequirement = newPropertyRequirement.getValueRequirement();
                            oldPropertyRequirement = oldDataRequirement.getPropertyRequirement(propertyId);
                            oldValueRequirement = oldPropertyRequirement.getValueRequirement();

                            // If the data types differ, transform the value or move to FFT.
                            if (!oldValueRequirement.isEquivalentRequirement(newValueRequirement))
                            {
                                RecordProperty property;

                                // Get the old record property.
                                property = record.getRecordProperty(propertyId);
                                if (property != null)
                                {
                                    boolean converted;

                                    converted = convertValue(property, oldValueRequirement, newValueRequirement, valueStringGenerator);
                                    if (converted) updatedPropertyCount++;
                                }
                            }
                        }

                        // Process each of the deleted properties.
                        for (String propertyId : deletedPropertyIdSet)
                        {
                            RecordProperty property;

                            // Get the old record value, create a value string from it.
                            property = record.getRecordProperty(propertyId);
                            if (property != null)
                            {
                                RecordValue oldValue;

                                // Get the old record value, create a value string from it.
                                oldValue = property.getRecordValue();
                                if (oldValue != null)
                                {
                                    StringBuilder valueString;

                                    valueString = valueStringGenerator.generateValueString(oldValue);

                                    // Move property value to record-level FFT.
                                    record.appendFFTChecked(valueString != null ? valueString.toString() : "", " ");

                                    // Remove property value.
                                    record.removeRecordProperty(propertyId);
                                    deletedPropertyCount++;
                                }
                            }
                        }
                    }
                }

                // Save the file after all changes have been applied.
                recApi.saveFileStateChange(dataFile);
            }

            // Save the DR.
            drApi.saveDataRequirement(newDataRequirement, true);

            // Update the impact report.
            alterationImpact.setUpdatedPropertyCount(updatedPropertyCount);
            alterationImpact.setDeletedPropertyCount(deletedPropertyCount);
        }

        // Return the final alteration impact.
        return alterationImpact;
    }

    private boolean convertValue(RecordProperty property, ValueRequirement oldValueRequirement, ValueRequirement newValueRequirement, T8DataRecordValueStringGenerator valueStringGenerator)
    {
        RequirementType oldRequirementType;
        RequirementType newRequirementType;
        RecordValue oldValue;
        String fft;

        // Get the old record value, create a value string from it.
        oldValue = property.getRecordValue();
        fft = Strings.trimToNull(property.getFFT());

        // Get the old and new requirement types to compare.
        oldRequirementType = oldValueRequirement.getRequirementType();
        newRequirementType = newValueRequirement.getRequirementType();
        if (oldRequirementType != newRequirementType)
        {
            // Set the new value requirement on the altered property.
            property.getPropertyRequirement().setValueRequirement(newValueRequirement);

            // If the new requirement is a concept, we have to create a term from the old value.
            if (newRequirementType == RequirementType.CONTROLLED_CONCEPT)
            {
                // If we have an old value, use it as the new term for the new value.
                if ((oldValue != null) && (oldValue.hasContent(false)))
                {
                    StringBuilder valueString;

                    valueString = valueStringGenerator.generateValueString(oldValue);
                    if (valueString != null)
                    {
                        String term;

                        term = Strings.trimToNull(valueString.toString());
                        if (term != null)
                        {
                            ControlledConcept newValue;

                            // Create a new concept using the value string of the previous value as term.
                            newValue = new ControlledConcept(newValueRequirement);
                            newValue.setTerm(term);

                            // Set the new property value.
                            property.setRecordValue(newValue);
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else if (fft != null) // If we don't have an old value but an FFT exists, use it as the term for the new concept.
                {
                    ControlledConcept newValue;

                    // Create a new concept using the value string of the previous value as term.
                    newValue = new ControlledConcept(newValueRequirement);
                    newValue.setTerm(fft);

                    // Set the new property value.
                    property.setRecordValue(newValue);
                    property.setFFT(null);
                    return true;
                }
                else return false; // We have no old value or fft, so no conversion needs to be performed.
            }
            else if ((oldValue != null) && (oldValue.hasContent(false)))
            {
                StringBuilder valueString;

                // Move property value to FFT.
                valueString = valueStringGenerator.generateValueString(oldValue);
                property.appendFFTChecked(valueString != null ? valueString.toString() : "", " ");

                // Remove property value.
                property.setRecordValue(null);
                return true;
            }
            else return false;
        }
        else if ((oldValue != null) && (oldValue.hasContent(false)))
        {
            RecordValue newValue;

            // Create a new value from the content of the old value and add it to the property.
            newValue = RecordValue.createValue(newValueRequirement);
            newValue.setContent(oldValue.getContent());
            property.setRecordValue(newValue);
            return true;
        }
        else return false;
    }

    @Override
    public T8DataAlteration retrieveAlteration(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        T8DataRequirementAlterationDataHandler handler;
        T8DataAlterationDataHandler conceptHandler;
        T8DataAlterationMethod alterationMethod;
        T8DataRequirementAlteration alteration;
        List<T8OntologyConcept> concepts;
        DataRequirement dataRequirement;
        String method;

        // Initialize the data handler.
        handler = new T8DataRequirementAlterationDataHandler(tx);
        conceptHandler = new T8DataAlterationDataHandler(serverContext);

        // Retrieve the alteration method.
        method = handler.retrieveDataRequirementAlterationMethod(tx, packageIid, step);
        alterationMethod = T8DataAlterationMethod.valueOf(method);

        // Retrieve the concepts for this dr alteration.
        concepts = conceptHandler.retrieveConcepts(tx, packageIid, step);

        // Retrieve the data requirement.
        dataRequirement = handler.retrieveDataRequirement(packageIid, step);

        // Create the alteration object.
        alteration = new T8DataRequirementAlteration(packageIid, step, dataRequirement, alterationMethod);
        alteration.setConcepts(concepts);

        // Return the alteration object.
        return alteration;
    }

    @Override
    public void adjustSteps(T8DataTransaction tx, String alterationID, int startStep, int endStep, int increment) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlterationImpact getAlterationImpact(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8DataAlterationImpact alterationImpact;
        T8DataRequirementAlteration dataRequirementAlteration;
        DataRequirement newDataRequirement;
        DataRequirement oldDataRequirement;
        List<String> newDrPropertyIdList;
        List<String> oldDrPropertyIdList;
        Map<String, String> supersededPropertyIdMap;
        Set<String> deletedPropertyIdSet;
        Set<String> updatedPropertyIdSet;
        Set<String> affectedPropertyIdSet;
        Set<String> affectedRecordIdSet;
        T8DataRequirementApi drApi;
        int deletedPropertyCount;
        int updatedPropertyCount;
        String drId;

        // Initialize variables.
        alterationImpact = new T8DataAlterationImpact();
        dataRequirementAlteration = (T8DataRequirementAlteration)alteration;
        newDataRequirement = dataRequirementAlteration.getDataRequirement();
        drId = newDataRequirement.getConceptID();
        drApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);

         // Firstly, retrieve the old data requirement to be altered.
        oldDataRequirement = drApi.retrieveDataRequirement(drId, null, true, false);

         // Get the list of property id's from the old and new data requirements.
        newDrPropertyIdList = newDataRequirement.getPropertyRequirementIDList();
        oldDrPropertyIdList = oldDataRequirement.getPropertyRequirementIDList();

        // Get the id's of properties that existing in both the old and the new DR's.  These are the potentially updated properties.
        updatedPropertyIdSet = new HashSet<>(newDrPropertyIdList);
        updatedPropertyIdSet.retainAll(oldDrPropertyIdList);

        // Take all the properties in the old DR and remove the corresponding properties from the new DR, remaining properties are those that are specified as deletions.
        deletedPropertyIdSet = new HashSet<>(oldDrPropertyIdList);
        deletedPropertyIdSet.removeAll(newDrPropertyIdList);

        // Construct the superseded property id map.
        supersededPropertyIdMap = new HashMap<>();
        for (PropertyRequirement newPropertyRequirement : newDataRequirement.getPropertyRequirements())
        {
            String supersededPropertyId;

            supersededPropertyId = (String)newPropertyRequirement.getAttribute(T8DataRequirementAlteration.SUPERSEDED_PROPERTY_ATTRIBUTE_ID);
            if (supersededPropertyId != null)
            {
                supersededPropertyIdMap.put(supersededPropertyId, newPropertyRequirement.getConceptID());
            }
        }

        // Combine both updated and deleted property lists to get a collection of all properties that will affect records using them.
        affectedPropertyIdSet = new HashSet<>();
        affectedPropertyIdSet.addAll(updatedPropertyIdSet);
        affectedPropertyIdSet.addAll(deletedPropertyIdSet);
        affectedPropertyIdSet.addAll(supersededPropertyIdMap.keySet());

        // Using the set of affected property id's, retrieve all records that contain content for one of more of these properties.
        affectedRecordIdSet = retrieveRecordIdSet(tx, drId, affectedPropertyIdSet, true);

        // Set affected record count
        alterationImpact.setAffectedRecordCount(affectedRecordIdSet.size());

        // Set delted property count
        if (!deletedPropertyIdSet.isEmpty())
        {
            deletedPropertyCount = countRecordIds(tx, drId, deletedPropertyIdSet);
            alterationImpact.setDeletedPropertyCount(deletedPropertyCount);
        }

        // Set updated property count
        if (!updatedPropertyIdSet.isEmpty())
        {
            updatedPropertyCount = countRecordIds(tx, drId, updatedPropertyIdSet);
            alterationImpact.setUpdatedPropertyCount(updatedPropertyCount);
        }

        return alterationImpact;
    }

    @Override
    public T8DataAlteration deserializeAlteration(JsonObject jsonValue) throws Exception
    {
        if (jsonValue instanceof JsonObject)
        {
            JsonObject alterationObject;
            JsonObject drObject;
            String alterationIid;
            String alterationId;
            T8DataAlterationMethod method;
            int step;

            // Extract the required values from the JSON object.
            alterationObject = (JsonObject)jsonValue;
            alterationId = alterationObject.getString("id");
            alterationIid = alterationObject.getString("iid");
            step = alterationObject.getInteger("step");
            method = T8DataAlterationMethod.valueOf(alterationObject.getString("method"));
            drObject = alterationObject.getJsonObject("dataRequirement");

            // Make sure the JSON object was of the correct type and then construct the result.
            if (alterationId.equals(T8DataRequirementAlteration.ALTERATION_ID))
            {
                DataRequirement dataRequirement;

                // Deserialize the Data Requirement object and return the completed alteration object.
                dataRequirement = drObject != null ? T8DocumentSerializer.deserializeDataRequirement(drObject) : null;
                return new T8DataRequirementAlteration(alterationIid, step, dataRequirement, method);
            }
            else throw new IllegalArgumentException("Expected alteration '" + T8DataRequirementAlteration.ALTERATION_ID + "', found: " + alterationId);
        }
        else throw new IllegalArgumentException("Cannot deserialize alteration from JSON value: " + jsonValue);
    }

    /**
     * Retrieve the id's of all data files that have descendant records containing content (value or FFT) for one or more of
     * the specified properties.
     * @param tx The transaction to use.
     * @param drId The Data Requirement to use for filtering.
     * @param affectedPropertyIdSet The set of property id's to use for filtering.
     * @return A set of data file id's.
     * @throws Exception
     */
    private Set<String> retrieveRecordIdSet(T8DataTransaction tx, String drId, Set<String> affectedPropertyIdSet, boolean includeFft) throws Exception
    {
        List<T8DataEntity> entities;
        Set<String> recordIdSet;
        T8DataFilter dataFilter;
        String entityId;

        // Create data filter to filter on DR_ID and list of property Id's
        entityId = includeFft ? DATA_RECORD_PROPERTY_DE_IDENTIFIER : DATA_RECORD_VALUE_DE_IDENTIFIER;
        dataFilter = new T8DataFilter(entityId, HashMaps.newHashMap(entityId + EF_DR_ID, drId));

        // Add empty check to cater for the use case when the existing data requirement has no properties.
        if (affectedPropertyIdSet != null && !affectedPropertyIdSet.isEmpty())
        {
            dataFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_PROPERTY_ID, affectedPropertyIdSet);
        }

        // Retrieve the filtered entities.
        entities = tx.select(entityId, dataFilter);

        // Loop through entities and get the root record id of each of them.
        recordIdSet = new HashSet<>();
        for (T8DataEntity entity : entities)
        {
            recordIdSet.add((String) entity.getFieldValue(entityId + EF_ROOT_RECORD_ID));
        }

        // Return the result list of id's.
        return recordIdSet;
    }

     /**
     * Count the id's of all data files that have descendant records containing value for one or more of
     * the specified properties.
     * @param tx The transaction to use.
     * @param drId The Data Requirement to use for filtering.
     * @param propertyIdSet The set of property id's to use for counting.
     * @return A count of the record id's.
     * @throws Exception
     */
    private int countRecordIds(T8DataTransaction tx, String drId, Set<String> propertyIdSet) throws Exception
    {
        T8DataFilter dataFilter;
        String entityId;
        int recordIdCount;

        // Create data filter to filter on DR_ID and list of property Id's
        entityId = DATA_RECORD_VALUE_DE_IDENTIFIER;
        dataFilter = new T8DataFilter(entityId, HashMaps.newHashMap(entityId + EF_DR_ID, drId));
        dataFilter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_PROPERTY_ID, propertyIdSet);

        recordIdCount = tx.count(entityId, dataFilter);

        return recordIdCount;
    }
}