package com.pilog.t8.api;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.document.dataquality.T8DataQualityMetrics;
import com.pilog.t8.data.document.dataquality.T8DataQualityMetricsGroup;
import com.pilog.t8.data.document.dataquality.T8GroupedDataQualityReport;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldResourceDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityResourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldResourceDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceResourceDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataQualityApiDataHandler
{
    private final T8ServerContext serverContext;
    private final T8Context context;

    private static final String DS_DYN_DQ_REPORT_ID = "@DS_DYN_DQ_REPORT";
    private static final String E_DYN_DQ_REPORT_ID = "@E_DYN_DQ_REPORT";

    private static final String F_ORG_ID = "$ORG_ID";
    private static final String F_LANGUAGE_ID = "$LANGUAGE_ID";
    private static final String F_CODE = "$CODE";
    private static final String F_TERM = "$TERM";
    private static final String F_DEFINITION = "$DEFINITION";
    private static final String F_OS_ID = "$OS_ID";
    private static final String F_OC_ID = "$OC_ID";
    private static final String F_OC_CODE = "$OC_CODE";
    private static final String F_OC_TERM = "$OC_TERM";
    private static final String F_OC_DEFINITION = "$OC_DEFINITION";
    private static final String F_DR_INSTANCE_ID = "$DR_INSTANCE_ID";
    private static final String F_VALUE_CONCEPT_ID = "$VALUE_CONCEPT_ID";
    private static final String F_RECORD_COUNT = "$RECORD_COUNT";
    private static final String F_PROPERTY_COUNT = "$PROPERTY_COUNT";
    private static final String F_PROPERTY_DATA_COUNT = "$PROPERTY_DATA_COUNT";
    private static final String F_PROPERTY_VALID_COUNT = "$PROPERTY_VALID_COUNT";
    private static final String F_CHRCTRSTC_COUNT = "$CHRCTRSTC_COUNT";
    private static final String F_CHRCTRSTC_DATA_COUNT = "$CHRCTRSTC_DATA_COUNT";
    private static final String F_CHRCTRSTC_VALID_COUNT = "$CHRCTRSTC_VALID_COUNT";

    public T8DataQualityApiDataHandler(T8DataTransaction tx)
    {
        this.context = tx.getContext();
        this.serverContext = context.getServerContext();
    }

    /**
     * Returns a data quality report of all root records of a specific type (DR Instance).
     * @param tx
     * @param rootDrInstanceIdList
     * @return
     * @throws Exception
     */
    public T8GroupedDataQualityReport retrieveRootDataQualityReport(T8DataTransaction tx, List<String> rootDrInstanceIdList) throws Exception
    {
        T8SQLDataSourceResourceDefinition sourceDefinition;
        T8DataEntityDefinition entityDefinition;
        T8GroupedDataQualityReport dqReport;
        T8DefinitionManager definitionManager;
        List<T8DataEntity> resultEntities;
        String sqlRootDrInstanceIdString;
        String connectionId;
        T8DatabaseAdaptor dbAdaptor;
        T8DataFilter dataFilter;

        // Get the required connection and database adaptor.
        connectionId = serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId();
        dbAdaptor = tx.getDataConnection(connectionId).getDatabaseAdaptor();

        // Create the list of DR Instance id's to use as operand to the IN operator.
        sqlRootDrInstanceIdString = "";
        for (int drInstanceIdIndex = 0; drInstanceIdIndex < rootDrInstanceIdList.size(); drInstanceIdIndex++)
        {
            String rootDrInstanceId;

            rootDrInstanceId = rootDrInstanceIdList.get(drInstanceIdIndex);
            sqlRootDrInstanceIdString += dbAdaptor.getSQLHexToGUID(rootDrInstanceId);
            if (drInstanceIdIndex < rootDrInstanceIdList.size() -1) sqlRootDrInstanceIdString += ",";
        }

        // Create the dynamic report data source.
        sourceDefinition = new T8SQLDataSourceResourceDefinition(DS_DYN_DQ_REPORT_ID);
        sourceDefinition.setConnectionIdentifier(connectionId);
        sourceDefinition.setSQLWithString
        (
            "WITH COUNTS AS\n" +
            "(\n" +
            "	SELECT \n" +
            "		ROOT.DR_INSTANCE_ID,\n" +
            "		COUNT(CASE WHEN DOC.RECORD_ID = DOC.ROOT_RECORD_ID THEN 1 ELSE NULL END) AS RECORD_COUNT,\n" +
            "		SUM(DOC.PROPERTY_COUNT) AS PROPERTY_COUNT,\n" +
            "		SUM(DOC.PROPERTY_DATA_COUNT) AS PROPERTY_DATA_COUNT,\n" +
            "		SUM(DOC.PROPERTY_VALID_COUNT) AS PROPERTY_VALID_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_COUNT) AS CHRCTRSTC_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_DATA_COUNT) AS CHRCTRSTC_DATA_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_VALID_COUNT) AS CHRCTRSTC_VALID_COUNT\n" +
            "	FROM DATA_RECORD_DOC ROOT\n" +
            "	INNER JOIN DATA_RECORD_DOC DOC\n" +
            "		ON ROOT.RECORD_ID = DOC.ROOT_RECORD_ID\n" +
            "	WHERE \n" +
            "		ROOT.DR_INSTANCE_ID IN (" + sqlRootDrInstanceIdString + ")\n" +
            "	GROUP BY ROOT.DR_INSTANCE_ID\n" +
            ")"
        );
        sourceDefinition.setSQLSelectString
        (
            "SELECT\n" +
            "	TRM.ORG_ID,\n" +
            "	TRM.LANGUAGE_ID,\n" +
            "	TRM.CODE,\n" +
            "	TRM.TERM,\n" +
            "	TRM.DEFINITION,\n" +
            "	CTS.DR_INSTANCE_ID,\n" +
            "	CTS.RECORD_COUNT,\n" +
            "	CTS.PROPERTY_COUNT,\n" +
            "	CTS.PROPERTY_DATA_COUNT,\n" +
            "	CTS.PROPERTY_VALID_COUNT,\n" +
            "	CTS.CHRCTRSTC_COUNT,\n" +
            "	CTS.CHRCTRSTC_DATA_COUNT,\n" +
            "	CTS.CHRCTRSTC_VALID_COUNT\n" +
            "FROM COUNTS CTS\n" +
            "INNER JOIN ORG_TERMINOLOGY TRM\n" +
            "ON CTS.DR_INSTANCE_ID = TRM.CONCEPT_ID"
        );
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CODE, "CODE", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TERM, "TERM", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DEFINITION, "DEFINITION", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RECORD_COUNT, "RECORD_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_COUNT, "PROPERTY_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_DATA_COUNT, "PROPERTY_DATA_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_VALID_COUNT, "PROPERTY_VALID_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_COUNT, "CHRCTRSTC_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_DATA_COUNT, "CHRCTRSTC_DATA_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_VALID_COUNT, "CHRCTRSTC_VALID_COUNT", false, true, T8DataType.INTEGER));

        // Create the dynamic report Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(E_DYN_DQ_REPORT_ID);
        entityDefinition.setDataSourceIdentifier(DS_DYN_DQ_REPORT_ID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DS_DYN_DQ_REPORT_ID + F_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DS_DYN_DQ_REPORT_ID + F_LANGUAGE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CODE, DS_DYN_DQ_REPORT_ID + F_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TERM, DS_DYN_DQ_REPORT_ID + F_TERM, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DEFINITION, DS_DYN_DQ_REPORT_ID + F_DEFINITION, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_INSTANCE_ID, DS_DYN_DQ_REPORT_ID + F_DR_INSTANCE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RECORD_COUNT, DS_DYN_DQ_REPORT_ID + F_RECORD_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_DATA_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_DATA_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_VALID_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_VALID_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_DATA_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_DATA_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_VALID_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_VALID_COUNT, false, true, T8DataType.INTEGER));

        // Initialize the dynamic definitions.
        definitionManager = serverContext.getDefinitionManager();
        definitionManager.initializeDefinition(context, sourceDefinition, null);
        definitionManager.initializeDefinition(context, entityDefinition, null);

        // Set the dynamic data source and entity on this transaction.
        tx.setDataSourceDefinition(sourceDefinition);
        tx.setDataEntityDefinition(entityDefinition);

        // Create a data filter to use for retrieval.
        dataFilter = new T8DataFilter(E_DYN_DQ_REPORT_ID);
        dataFilter.addFilterCriterion(DS_DYN_DQ_REPORT_ID + F_ORG_ID, context.getSessionContext().getRootOrganizationIdentifier());
        dataFilter.addFilterCriterion(DS_DYN_DQ_REPORT_ID + F_LANGUAGE_ID, context.getSessionContext().getContentLanguageIdentifier());

        // Retrieve results from the data entity.
        dqReport = new T8GroupedDataQualityReport();
        resultEntities = tx.select(E_DYN_DQ_REPORT_ID, dataFilter);
        for (T8DataEntity resultEntity : resultEntities)
        {
            T8DataQualityMetricsGroup group;
            T8DataQualityMetrics metrics;
            String drInstanceId;
            String code;
            String term;
            String definition;
            int rootRecordCount;
            int propertyCount;
            int propertyDataCount;
            int propertyValidCount;
            int characteristicCount;
            int characteristicDataCount;
            int characteristicValidCount;

            // Get the details of the result entity.
            drInstanceId = (String)resultEntity.getFieldValue(F_DR_INSTANCE_ID);
            code = (String)resultEntity.getFieldValue(F_CODE);
            term = (String)resultEntity.getFieldValue(F_TERM);
            definition = (String)resultEntity.getFieldValue(F_DEFINITION);
            rootRecordCount = (Integer)resultEntity.getFieldValue(F_RECORD_COUNT);
            propertyCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_COUNT);
            propertyDataCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_DATA_COUNT);
            propertyValidCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_VALID_COUNT);
            characteristicCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_COUNT);
            characteristicDataCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_DATA_COUNT);
            characteristicValidCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_VALID_COUNT);

            // Create a new data quality group.
            group = new T8DataQualityMetricsGroup(drInstanceId);
            group.setCode(code);
            group.setTerm(term);
            group.setDefinition(definition);

            // Create the data quality metrics for the group.
            metrics = new T8DataQualityMetrics();
            metrics.setRecordCount(rootRecordCount);
            metrics.setPropertyCount(propertyCount);
            metrics.setPropertyDataCount(propertyDataCount);
            metrics.setPropertyValidCount(propertyValidCount);
            metrics.setCharacteristicCount(characteristicCount);
            metrics.setCharacteristicDataCount(characteristicDataCount);
            metrics.setCharacteristicValidCount(characteristicValidCount);

            // Add the metrics to the group and the group to the report.
            group.setMetrics(metrics);
            dqReport.addGroup(group);
        }

        // Return the final data quality report.
        return dqReport;
    }

    /**
     * Returns a data quality report of all records belonging to a root record of a specific type (DR Instance).
     * The data quality results are grouped according to the DR Instance of the records included.
     * @param tx
     * @param rootDrInstanceId
     * @param filterDrInstanceIdList
     * @return
     */
    public T8GroupedDataQualityReport retrieveDataQualityReportGroupedByDrInstance(T8DataTransaction tx, String osId, String rootDrInstanceId, List<String> filterDrInstanceIdList) throws Exception
    {
        T8SQLDataSourceResourceDefinition sourceDefinition;
        T8DataEntityDefinition entityDefinition;
        T8GroupedDataQualityReport dqReport;
        T8DefinitionManager definitionManager;
        List<T8DataEntity> resultEntities;
        String sqlRootDrInstanceId;
        String connectionId;
        T8DatabaseAdaptor dbAdaptor;
        T8DataFilter dataFilter;

        // Get the required connection and database adaptor.
        connectionId = serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId();
        dbAdaptor = tx.getDataConnection(connectionId).getDatabaseAdaptor();
        sqlRootDrInstanceId = dbAdaptor.getSQLHexToGUID(rootDrInstanceId);

        // Create the dynamic report data source.
        sourceDefinition = new T8SQLDataSourceResourceDefinition(DS_DYN_DQ_REPORT_ID);
        sourceDefinition.setConnectionIdentifier(connectionId);
        sourceDefinition.setSQLWithString
        (
            "WITH COUNTS AS\n" +
            "(\n" +
            "	SELECT \n" +
            "		DOC.DR_INSTANCE_ID,\n" +
            "		COUNT(DOC.ROOT_RECORD_ID) AS RECORD_COUNT,\n" +
            "		SUM(DOC.PROPERTY_COUNT) AS PROPERTY_COUNT,\n" +
            "		SUM(DOC.PROPERTY_DATA_COUNT) AS PROPERTY_DATA_COUNT,\n" +
            "		SUM(DOC.PROPERTY_VALID_COUNT) AS PROPERTY_VALID_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_COUNT) AS CHRCTRSTC_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_DATA_COUNT) AS CHRCTRSTC_DATA_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_VALID_COUNT) AS CHRCTRSTC_VALID_COUNT\n" +
            "	FROM DATA_RECORD_DOC ROOT\n" +
            "	INNER JOIN DATA_RECORD_DOC DOC\n" +
            "		ON ROOT.RECORD_ID = DOC.ROOT_RECORD_ID\n" +
            "	WHERE \n" +
            "		ROOT.DR_INSTANCE_ID = " + sqlRootDrInstanceId + "\n" +
            "	GROUP BY DOC.DR_INSTANCE_ID\n" +
            ")"
        );
        sourceDefinition.setSQLSelectString
        (
            "SELECT\n" +
            "	TRM.ORG_ID,\n" +
            "	TRM.LANGUAGE_ID,\n" +
            "	TRM.CODE,\n" +
            "	TRM.TERM,\n" +
            "	TRM.DEFINITION,\n" +
            "	OO.ODS_ID AS OS_ID,\n" +
            "	OO.ODT_ID AS OC_ID,\n" +
            "	OCTRM.TERM AS OC_TERM,\n" +
            "	CTS.DR_INSTANCE_ID,\n" +
            "	CTS.RECORD_COUNT,\n" +
            "	CTS.PROPERTY_COUNT,\n" +
            "	CTS.PROPERTY_DATA_COUNT,\n" +
            "	CTS.PROPERTY_VALID_COUNT,\n" +
            "	CTS.CHRCTRSTC_COUNT,\n" +
            "	CTS.CHRCTRSTC_DATA_COUNT,\n" +
            "	CTS.CHRCTRSTC_VALID_COUNT\n" +
            "FROM COUNTS CTS\n" +
            "INNER JOIN ORG_ONTOLOGY OO\n" +
            "ON CTS.DR_INSTANCE_ID = OO.CONCEPT_ID\n" +
            "INNER JOIN ORG_TERMINOLOGY TRM\n" +
            "ON CTS.DR_INSTANCE_ID = TRM.CONCEPT_ID\n" +
            "AND TRM.ORG_ID = OO.ORG_ID\n" +
            "LEFT JOIN ORG_TERMINOLOGY OCTRM\n" +
            "ON OO.ODT_ID = OCTRM.CONCEPT_ID\n" +
            "AND OO.ORG_ID = OCTRM.ORG_ID\n" +
            "AND TRM.LANGUAGE_ID = OCTRM.LANGUAGE_ID"
        );
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CODE, "CODE", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TERM, "TERM", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DEFINITION, "DEFINITION", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_OS_ID, "OS_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_OC_ID, "OC_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_OC_TERM, "OC_TERM", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RECORD_COUNT, "RECORD_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_COUNT, "PROPERTY_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_DATA_COUNT, "PROPERTY_DATA_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_VALID_COUNT, "PROPERTY_VALID_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_COUNT, "CHRCTRSTC_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_DATA_COUNT, "CHRCTRSTC_DATA_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_VALID_COUNT, "CHRCTRSTC_VALID_COUNT", false, true, T8DataType.INTEGER));

        // Create the dynamic report Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(E_DYN_DQ_REPORT_ID);
        entityDefinition.setDataSourceIdentifier(DS_DYN_DQ_REPORT_ID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DS_DYN_DQ_REPORT_ID + F_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DS_DYN_DQ_REPORT_ID + F_LANGUAGE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CODE, DS_DYN_DQ_REPORT_ID + F_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TERM, DS_DYN_DQ_REPORT_ID + F_TERM, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DEFINITION, DS_DYN_DQ_REPORT_ID + F_DEFINITION, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_OS_ID, DS_DYN_DQ_REPORT_ID + F_OS_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_OC_ID, DS_DYN_DQ_REPORT_ID + F_OC_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_OC_TERM, DS_DYN_DQ_REPORT_ID + F_OC_TERM, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_INSTANCE_ID, DS_DYN_DQ_REPORT_ID + F_DR_INSTANCE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RECORD_COUNT, DS_DYN_DQ_REPORT_ID + F_RECORD_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_DATA_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_DATA_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_VALID_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_VALID_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_DATA_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_DATA_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_VALID_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_VALID_COUNT, false, true, T8DataType.INTEGER));

        // Initialize the dynamic definitions.
        definitionManager = serverContext.getDefinitionManager();
        definitionManager.initializeDefinition(context, sourceDefinition, null);
        definitionManager.initializeDefinition(context, entityDefinition, null);

        // Set the dynamic data source and entity on this transaction.
        tx.setDataSourceDefinition(sourceDefinition);
        tx.setDataEntityDefinition(entityDefinition);

        // Create a data filter to use for retrieval.
        dataFilter = new T8DataFilter(E_DYN_DQ_REPORT_ID);
        dataFilter.addFilterCriterion(E_DYN_DQ_REPORT_ID + F_ORG_ID, context.getSessionContext().getRootOrganizationIdentifier());
        dataFilter.addFilterCriterion(E_DYN_DQ_REPORT_ID + F_LANGUAGE_ID, context.getSessionContext().getContentLanguageIdentifier());
        dataFilter.addFilterCriterion(E_DYN_DQ_REPORT_ID + F_OS_ID, osId);
        dataFilter.addFieldOrdering(E_DYN_DQ_REPORT_ID + F_TERM, T8DataFilter.OrderMethod.ASCENDING);

        // Retrieve results from the data entity.
        dqReport = new T8GroupedDataQualityReport();
        resultEntities = tx.select(E_DYN_DQ_REPORT_ID, dataFilter);
        for (T8DataEntity resultEntity : resultEntities)
        {
            T8DataQualityMetricsGroup group;
            T8DataQualityMetrics metrics;
            String drInstanceId;
            String code;
            String term;
            String definition;
            String ocId;
            String ocTerm;
            int recordCount;
            int propertyCount;
            int propertyDataCount;
            int propertyValidCount;
            int characteristicCount;
            int characteristicDataCount;
            int characteristicValidCount;

            // Get the details of the result entity.
            drInstanceId = (String)resultEntity.getFieldValue(F_DR_INSTANCE_ID);
            code = (String)resultEntity.getFieldValue(F_CODE);
            term = (String)resultEntity.getFieldValue(F_TERM);
            definition = (String)resultEntity.getFieldValue(F_DEFINITION);
            ocId = (String)resultEntity.getFieldValue(F_OC_ID);
            ocTerm = (String)resultEntity.getFieldValue(F_OC_TERM);
            recordCount = (Integer)resultEntity.getFieldValue(F_RECORD_COUNT);
            propertyCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_COUNT);
            propertyDataCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_DATA_COUNT);
            propertyValidCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_VALID_COUNT);
            characteristicCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_COUNT);
            characteristicDataCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_DATA_COUNT);
            characteristicValidCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_VALID_COUNT);

            // Create a new data quality group.
            group = new T8DataQualityMetricsGroup(drInstanceId);
            group.setCode(code);
            group.setTerm(term);
            group.setDefinition(definition);
            group.setOcId(ocId);
            group.setOcTerm(ocTerm);

            // Create the data quality metrics for the group.
            metrics = new T8DataQualityMetrics();
            metrics.setRecordCount(recordCount);
            metrics.setPropertyCount(propertyCount);
            metrics.setPropertyDataCount(propertyDataCount);
            metrics.setPropertyValidCount(propertyValidCount);
            metrics.setCharacteristicCount(characteristicCount);
            metrics.setCharacteristicDataCount(characteristicDataCount);
            metrics.setCharacteristicValidCount(characteristicValidCount);

            // Add the metrics to the group and the group to the report.
            group.setMetrics(metrics);
            dqReport.addGroup(group);
        }

        // Return the final data quality report.
        return dqReport;
    }

    /**
     * Returns a data quality report of all root records of a specific type (DR Instance).
     * The data quality results are grouped according to the DR Instance of a specific descendant of the root record.
     * @param tx
     * @param rootDrInstanceId
     * @param groupByTargetDrInstanceId
     * @param groupByTargetPropertyId
     * @param filterDrInstanceIdList
     * @return
     */
    public T8GroupedDataQualityReport retrieveRootDataQualityReportGroupedByDrInstance(T8DataTransaction tx, String rootDrInstanceId, String groupByTargetDrInstanceId, String groupByTargetPropertyId, List<String> filterDrInstanceIdList) throws Exception
    {
        T8SQLDataSourceResourceDefinition sourceDefinition;
        T8DataEntityDefinition entityDefinition;
        T8GroupedDataQualityReport dqReport;
        T8DefinitionManager definitionManager;
        List<T8DataEntity> resultEntities;
        String sqlRootDrInstanceId;
        String sqlTargetDrInstanceId;
        String sqlTargetPropertyId;
        String connectionId;
        T8DatabaseAdaptor dbAdaptor;
        T8DataFilter dataFilter;

        // Get the required connection and database adaptor.
        connectionId = serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId();
        dbAdaptor = tx.getDataConnection(connectionId).getDatabaseAdaptor();
        sqlRootDrInstanceId = dbAdaptor.getSQLHexToGUID(rootDrInstanceId);
        sqlTargetDrInstanceId = dbAdaptor.getSQLHexToGUID(groupByTargetDrInstanceId);
        sqlTargetPropertyId = dbAdaptor.getSQLHexToGUID(groupByTargetPropertyId);

        // Create the dynamic report data source.
        sourceDefinition = new T8SQLDataSourceResourceDefinition(DS_DYN_DQ_REPORT_ID);
        sourceDefinition.setConnectionIdentifier(connectionId);
        sourceDefinition.setSQLWithString
        (
            "WITH COUNTS AS\n" +
            "(\n" +
            "	SELECT \n" +
            "		REF.DR_INSTANCE_ID,\n" +
            "		COUNT(CASE WHEN DOC.RECORD_ID = DOC.ROOT_RECORD_ID THEN 1 ELSE NULL END) AS RECORD_COUNT,\n" +
            "		SUM(DOC.PROPERTY_COUNT) AS PROPERTY_COUNT,\n" +
            "		SUM(DOC.PROPERTY_DATA_COUNT) AS PROPERTY_DATA_COUNT,\n" +
            "		SUM(DOC.PROPERTY_VALID_COUNT) AS PROPERTY_VALID_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_COUNT) AS CHRCTRSTC_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_DATA_COUNT) AS CHRCTRSTC_DATA_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_VALID_COUNT) AS CHRCTRSTC_VALID_COUNT\n" +
            "	FROM DATA_RECORD_DOC ROOT\n" +
            "	INNER JOIN DATA_RECORD_VALUE FIL\n" +
            "		ON ROOT.RECORD_ID = FIL.ROOT_RECORD_ID\n" +
            "	INNER JOIN DATA_RECORD_DOC FILR\n" +
            "		ON FILR.RECORD_ID = FIL.RECORD_ID\n" +
            "	INNER JOIN DATA_RECORD_DOC DOC\n" +
            "		ON DOC.ROOT_RECORD_ID = FIL.ROOT_RECORD_ID\n" +
            "	INNER JOIN DATA_RECORD_DOC REF\n" +
            "		ON FIL.VALUE_CONCEPT_ID = REF.RECORD_ID\n" +
            "	WHERE \n" +
            "		ROOT.DR_INSTANCE_ID = " + sqlRootDrInstanceId + "\n" +
            "		AND FIL.PROPERTY_ID = " + sqlTargetPropertyId + "\n" +
            "		AND FILR.DR_INSTANCE_ID = " + sqlTargetDrInstanceId + "\n" +
            "	GROUP BY REF.DR_INSTANCE_ID\n" +
            ")"
        );
        sourceDefinition.setSQLSelectString
        (
            "SELECT\n" +
            "	TRM.ORG_ID,\n" +
            "	TRM.LANGUAGE_ID,\n" +
            "	TRM.CODE,\n" +
            "	TRM.TERM,\n" +
            "	TRM.DEFINITION,\n" +
            "	CTS.DR_INSTANCE_ID,\n" +
            "	CTS.RECORD_COUNT,\n" +
            "	CTS.PROPERTY_COUNT,\n" +
            "	CTS.PROPERTY_DATA_COUNT,\n" +
            "	CTS.PROPERTY_VALID_COUNT,\n" +
            "	CTS.CHRCTRSTC_COUNT,\n" +
            "	CTS.CHRCTRSTC_DATA_COUNT,\n" +
            "	CTS.CHRCTRSTC_VALID_COUNT\n" +
            "FROM COUNTS CTS\n" +
            "INNER JOIN ORG_TERMINOLOGY TRM\n" +
            "ON CTS.DR_INSTANCE_ID = TRM.CONCEPT_ID"
        );
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CODE, "CODE", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TERM, "TERM", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DEFINITION, "DEFINITION", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DR_INSTANCE_ID, "DR_INSTANCE_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RECORD_COUNT, "RECORD_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_COUNT, "PROPERTY_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_DATA_COUNT, "PROPERTY_DATA_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_VALID_COUNT, "PROPERTY_VALID_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_COUNT, "CHRCTRSTC_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_DATA_COUNT, "CHRCTRSTC_DATA_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_VALID_COUNT, "CHRCTRSTC_VALID_COUNT", false, true, T8DataType.INTEGER));

        // Create the dynamic report Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(E_DYN_DQ_REPORT_ID);
        entityDefinition.setDataSourceIdentifier(DS_DYN_DQ_REPORT_ID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DS_DYN_DQ_REPORT_ID + F_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DS_DYN_DQ_REPORT_ID + F_LANGUAGE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CODE, DS_DYN_DQ_REPORT_ID + F_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TERM, DS_DYN_DQ_REPORT_ID + F_TERM, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DEFINITION, DS_DYN_DQ_REPORT_ID + F_DEFINITION, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DR_INSTANCE_ID, DS_DYN_DQ_REPORT_ID + F_DR_INSTANCE_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RECORD_COUNT, DS_DYN_DQ_REPORT_ID + F_RECORD_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_DATA_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_DATA_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_VALID_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_VALID_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_DATA_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_DATA_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_VALID_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_VALID_COUNT, false, true, T8DataType.INTEGER));

        // Initialize the dynamic definitions.
        definitionManager = serverContext.getDefinitionManager();
        definitionManager.initializeDefinition(context, sourceDefinition, null);
        definitionManager.initializeDefinition(context, entityDefinition, null);

        // Set the dynamic data source and entity on this transaction.
        tx.setDataSourceDefinition(sourceDefinition);
        tx.setDataEntityDefinition(entityDefinition);

        // Create a data filter to use for retrieval.
        dataFilter = new T8DataFilter(E_DYN_DQ_REPORT_ID);
        dataFilter.addFilterCriterion(E_DYN_DQ_REPORT_ID + F_ORG_ID, context.getSessionContext().getRootOrganizationIdentifier());
        dataFilter.addFilterCriterion(E_DYN_DQ_REPORT_ID + F_LANGUAGE_ID, context.getSessionContext().getContentLanguageIdentifier());
        dataFilter.addFieldOrdering(E_DYN_DQ_REPORT_ID + F_TERM, T8DataFilter.OrderMethod.ASCENDING);

        // Retrieve results from the data entity.
        dqReport = new T8GroupedDataQualityReport();
        resultEntities = tx.select(E_DYN_DQ_REPORT_ID, dataFilter);
        for (T8DataEntity resultEntity : resultEntities)
        {
            T8DataQualityMetricsGroup group;
            T8DataQualityMetrics metrics;
            String drInstanceId;
            String code;
            String term;
            String definition;
            int rootRecordCount;
            int propertyCount;
            int propertyDataCount;
            int propertyValidCount;
            int characteristicCount;
            int characteristicDataCount;
            int characteristicValidCount;

            // Get the details of the result entity.
            drInstanceId = (String)resultEntity.getFieldValue(F_DR_INSTANCE_ID);
            code = (String)resultEntity.getFieldValue(F_CODE);
            term = (String)resultEntity.getFieldValue(F_TERM);
            definition = (String)resultEntity.getFieldValue(F_DEFINITION);
            rootRecordCount = (Integer)resultEntity.getFieldValue(F_RECORD_COUNT);
            propertyCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_COUNT);
            propertyDataCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_DATA_COUNT);
            propertyValidCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_VALID_COUNT);
            characteristicCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_COUNT);
            characteristicDataCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_DATA_COUNT);
            characteristicValidCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_VALID_COUNT);

            // Create a new data quality group.
            group = new T8DataQualityMetricsGroup(drInstanceId);
            group.setCode(code);
            group.setTerm(term);
            group.setDefinition(definition);

            // Create the data quality metrics for the group.
            metrics = new T8DataQualityMetrics();
            metrics.setRecordCount(rootRecordCount);
            metrics.setPropertyCount(propertyCount);
            metrics.setPropertyDataCount(propertyDataCount);
            metrics.setPropertyValidCount(propertyValidCount);
            metrics.setCharacteristicCount(characteristicCount);
            metrics.setCharacteristicDataCount(characteristicDataCount);
            metrics.setCharacteristicValidCount(characteristicValidCount);

            // Add the metrics to the group and the group to the report.
            group.setMetrics(metrics);
            dqReport.addGroup(group);
        }

        // Return the final data quality report.
        return dqReport;
    }

    /**
     * Returns a data quality report of all root records of a specific type (DR Instance).
     * The data quality results are grouped according to a specified value concept in one of the descendants of the root record.
     * @param tx
     * @param rootDrInstanceId
     * @param groupByTargetDrInstanceId
     * @param groupByTargetPropertyId
     * @param filterValueConceptIdList
     * @return
     */
    public T8GroupedDataQualityReport retrieveRootDataQualityReportGroupedByValueConcept(T8DataTransaction tx, String rootDrInstanceId, String groupByTargetDrInstanceId, String groupByTargetPropertyId, List<String> filterValueConceptIdList) throws Exception
    {
        T8SQLDataSourceResourceDefinition sourceDefinition;
        T8DataEntityDefinition entityDefinition;
        T8GroupedDataQualityReport dqReport;
        T8DefinitionManager definitionManager;
        List<T8DataEntity> resultEntities;
        String sqlRootDrInstanceId;
        String sqlTargetDrInstanceId;
        String sqlTargetPropertyId;
        String connectionId;
        T8DatabaseAdaptor dbAdaptor;
        T8DataFilter dataFilter;

        // Get the required connection and database adaptor.
        connectionId = serverContext.getDefinitionManager().getSystemDefinition(null).getDefaultConnectionId();
        dbAdaptor = tx.getDataConnection(connectionId).getDatabaseAdaptor();
        sqlRootDrInstanceId = dbAdaptor.getSQLHexToGUID(rootDrInstanceId);
        sqlTargetDrInstanceId = dbAdaptor.getSQLHexToGUID(groupByTargetDrInstanceId);
        sqlTargetPropertyId = dbAdaptor.getSQLHexToGUID(groupByTargetPropertyId);

        // Create the dynamic report data source.
        sourceDefinition = new T8SQLDataSourceResourceDefinition(DS_DYN_DQ_REPORT_ID);
        sourceDefinition.setConnectionIdentifier(connectionId);
        sourceDefinition.setSQLWithString
        (
            "WITH COUNTS AS\n" +
            "(\n" +
            "	SELECT \n" +
            "		FIL.VALUE_CONCEPT_ID,\n" +
            "		COUNT(CASE WHEN DOC.RECORD_ID = DOC.ROOT_RECORD_ID THEN 1 ELSE NULL END) AS RECORD_COUNT,\n" +
            "		SUM(DOC.PROPERTY_COUNT) AS PROPERTY_COUNT,\n" +
            "		SUM(DOC.PROPERTY_DATA_COUNT) AS PROPERTY_DATA_COUNT,\n" +
            "		SUM(DOC.PROPERTY_VALID_COUNT) AS PROPERTY_VALID_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_COUNT) AS CHRCTRSTC_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_DATA_COUNT) AS CHRCTRSTC_DATA_COUNT,\n" +
            "		SUM(DOC.CHRCTRSTC_VALID_COUNT) AS CHRCTRSTC_VALID_COUNT\n" +
            "	FROM DATA_RECORD_DOC ROOT\n" +
            "	INNER JOIN DATA_RECORD_VALUE FIL\n" +
            "	ON ROOT.RECORD_ID = FIL.ROOT_RECORD_ID\n" +
            "	INNER JOIN DATA_RECORD_DOC FILR\n" +
            "	ON FILR.RECORD_ID = FIL.RECORD_ID\n" +
            "	INNER JOIN DATA_RECORD_DOC DOC\n" +
            "	ON DOC.ROOT_RECORD_ID = FIL.ROOT_RECORD_ID\n" +
            "	WHERE \n" +
            "		ROOT.DR_INSTANCE_ID = " + sqlRootDrInstanceId + "\n" +
            "		AND FIL.PROPERTY_ID = " + sqlTargetPropertyId + "\n" +
            "		AND FILR.DR_INSTANCE_ID = " + sqlTargetDrInstanceId + "\n" +
            "	GROUP BY FIL.VALUE_CONCEPT_ID\n" +
            ")"
        );
        sourceDefinition.setSQLSelectString
        (
            "SELECT\n" +
            "	TRM.ORG_ID,\n" +
            "	TRM.LANGUAGE_ID,\n" +
            "	TRM.CODE,\n" +
            "	TRM.TERM,\n" +
            "	TRM.DEFINITION,\n" +
            "	CTS.VALUE_CONCEPT_ID,\n" +
            "	CTS.RECORD_COUNT,\n" +
            "	CTS.PROPERTY_COUNT,\n" +
            "	CTS.PROPERTY_DATA_COUNT,\n" +
            "	CTS.PROPERTY_VALID_COUNT,\n" +
            "	CTS.CHRCTRSTC_COUNT,\n" +
            "	CTS.CHRCTRSTC_DATA_COUNT,\n" +
            "	CTS.CHRCTRSTC_VALID_COUNT\n" +
            "FROM COUNTS CTS\n" +
            "INNER JOIN ORG_TERMINOLOGY TRM\n" +
            "ON CTS.VALUE_CONCEPT_ID = TRM.CONCEPT_ID"
        );
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_ORG_ID, "ORG_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_LANGUAGE_ID, "LANGUAGE_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CODE, "CODE", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_TERM, "TERM", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_DEFINITION, "DEFINITION", false, true, T8DataType.STRING));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_VALUE_CONCEPT_ID, "VALUE_CONCEPT_ID", false, true, T8DataType.GUID));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_RECORD_COUNT, "RECORD_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_COUNT, "PROPERTY_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_DATA_COUNT, "PROPERTY_DATA_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_PROPERTY_VALID_COUNT, "PROPERTY_VALID_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_COUNT, "CHRCTRSTC_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_DATA_COUNT, "CHRCTRSTC_DATA_COUNT", false, true, T8DataType.INTEGER));
        sourceDefinition.addFieldDefinition(new T8DataSourceFieldResourceDefinition(F_CHRCTRSTC_VALID_COUNT, "CHRCTRSTC_VALID_COUNT", false, true, T8DataType.INTEGER));

        // Create the dynamic report Data Entity.
        entityDefinition = new T8DataEntityResourceDefinition(E_DYN_DQ_REPORT_ID);
        entityDefinition.setDataSourceIdentifier(DS_DYN_DQ_REPORT_ID);
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_ORG_ID, DS_DYN_DQ_REPORT_ID + F_ORG_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_LANGUAGE_ID, DS_DYN_DQ_REPORT_ID + F_LANGUAGE_ID, false, false, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CODE, DS_DYN_DQ_REPORT_ID + F_CODE, false, false, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_TERM, DS_DYN_DQ_REPORT_ID + F_TERM, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_DEFINITION, DS_DYN_DQ_REPORT_ID + F_DEFINITION, false, true, T8DataType.STRING));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_VALUE_CONCEPT_ID, DS_DYN_DQ_REPORT_ID + F_VALUE_CONCEPT_ID, false, true, T8DataType.GUID));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_RECORD_COUNT, DS_DYN_DQ_REPORT_ID + F_RECORD_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_DATA_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_DATA_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_PROPERTY_VALID_COUNT, DS_DYN_DQ_REPORT_ID + F_PROPERTY_VALID_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_DATA_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_DATA_COUNT, false, true, T8DataType.INTEGER));
        entityDefinition.addFieldDefinition(new T8DataEntityFieldResourceDefinition(F_CHRCTRSTC_VALID_COUNT, DS_DYN_DQ_REPORT_ID + F_CHRCTRSTC_VALID_COUNT, false, true, T8DataType.INTEGER));

        // Initialize the dynamic definitions.
        definitionManager = serverContext.getDefinitionManager();
        definitionManager.initializeDefinition(context, sourceDefinition, null);
        definitionManager.initializeDefinition(context, entityDefinition, null);

        // Set the dynamic data source and entity on this transaction.
        tx.setDataSourceDefinition(sourceDefinition);
        tx.setDataEntityDefinition(entityDefinition);

        // Create a data filter to use for retrieval.
        dataFilter = new T8DataFilter(E_DYN_DQ_REPORT_ID);
        dataFilter.addFilterCriterion(E_DYN_DQ_REPORT_ID + F_ORG_ID, context.getSessionContext().getRootOrganizationIdentifier());
        dataFilter.addFilterCriterion(E_DYN_DQ_REPORT_ID + F_LANGUAGE_ID, context.getSessionContext().getContentLanguageIdentifier());
        dataFilter.addFieldOrdering(E_DYN_DQ_REPORT_ID + F_TERM, T8DataFilter.OrderMethod.ASCENDING);

        // Retrieve results from the data entity.
        dqReport = new T8GroupedDataQualityReport();
        resultEntities = tx.select(E_DYN_DQ_REPORT_ID, dataFilter);
        for (T8DataEntity resultEntity : resultEntities)
        {
            T8DataQualityMetricsGroup group;
            T8DataQualityMetrics metrics;
            String valueConceptId;
            String code;
            String term;
            String definition;
            int rootRecordCount;
            int propertyCount;
            int propertyDataCount;
            int propertyValidCount;
            int characteristicCount;
            int characteristicDataCount;
            int characteristicValidCount;

            // Get the details of the result entity.
            valueConceptId = (String)resultEntity.getFieldValue(F_VALUE_CONCEPT_ID);
            code = (String)resultEntity.getFieldValue(F_CODE);
            term = (String)resultEntity.getFieldValue(F_TERM);
            definition = (String)resultEntity.getFieldValue(F_DEFINITION);
            rootRecordCount = (Integer)resultEntity.getFieldValue(F_RECORD_COUNT);
            propertyCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_COUNT);
            propertyDataCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_DATA_COUNT);
            propertyValidCount = (Integer)resultEntity.getFieldValue(F_PROPERTY_VALID_COUNT);
            characteristicCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_COUNT);
            characteristicDataCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_DATA_COUNT);
            characteristicValidCount = (Integer)resultEntity.getFieldValue(F_CHRCTRSTC_VALID_COUNT);

            // Create a new data quality group.
            group = new T8DataQualityMetricsGroup(valueConceptId);
            group.setCode(code);
            group.setTerm(term);
            group.setDefinition(definition);

            // Create the data quality metrics for the group.
            metrics = new T8DataQualityMetrics();
            metrics.setRecordCount(rootRecordCount);
            metrics.setPropertyCount(propertyCount);
            metrics.setPropertyDataCount(propertyDataCount);
            metrics.setPropertyValidCount(propertyValidCount);
            metrics.setCharacteristicCount(characteristicCount);
            metrics.setCharacteristicDataCount(characteristicDataCount);
            metrics.setCharacteristicValidCount(characteristicValidCount);

            // Add the metrics to the group and the group to the report.
            group.setMetrics(metrics);
            dqReport.addGroup(group);
        }

        // Return the final data quality report.
        return dqReport;
    }
}
