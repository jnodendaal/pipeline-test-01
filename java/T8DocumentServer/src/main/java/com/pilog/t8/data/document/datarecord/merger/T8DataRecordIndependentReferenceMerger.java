package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordIndependentReferenceMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordIndependentReferenceMergerDefinition.ReferenceMatchType;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordIndependentReferenceMergerDefinition.ReferenceMergeMethod;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordIndependentReferenceMergerDefinition.ReferenceMismatchAction;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordIndependentReferenceMerger implements T8DataRecordValueMerger
{
    private final String identifier;
    private final Set<DataRecord> newDataRecords;
    private final Set<DataRecord> updatedDataRecords;
    private final Map<String, String> attachmentIdentifierMapping; // Used to store attachment ID's mapped to newly assigned identifiers in destination documents.
    private final T8ExpressionEvaluator expressionEvaluator;
    private final String conditionExpression;
    private final String referenceMatchExpression;
    private final ReferenceMatchType referenceMatchType;
    private final ReferenceMergeMethod referenceMergeMethod;
    private final ReferenceMismatchAction referenceMismatchAction;
    private final int maximumReferenceCount;
    private final T8DataRecordMergeContext mergeContext;

    public T8DataRecordIndependentReferenceMerger(T8DataRecordMergeContext mergeContext, T8DataRecordMergeConfiguration config, String identifier)
    {
        this.identifier = identifier;
        this.mergeContext = mergeContext;
        this.newDataRecords = new HashSet<DataRecord>();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.attachmentIdentifierMapping = new HashMap<String, String>();
        this.expressionEvaluator = mergeContext.getExpressionEvaluator();
        this.conditionExpression = null;
        this.referenceMatchExpression = null;
        this.referenceMatchType = ReferenceMatchType.DEFINITION;
        this.referenceMergeMethod = ReferenceMergeMethod.SUBSTITUTE_REFERENCE;
        this.referenceMismatchAction = ReferenceMismatchAction.ADD_REFERENCE;
        this.maximumReferenceCount = 1000000; // Very large number to simulate 'unlimited'.
    }

    public T8DataRecordIndependentReferenceMerger(T8DataRecordIndependentReferenceMergerDefinition definition, T8DataRecordMergeContext mergeContext)
    {
        this.identifier = definition.getIdentifier();
        this.mergeContext = mergeContext;
        this.newDataRecords = new HashSet<DataRecord>();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.attachmentIdentifierMapping = new HashMap<String, String>();
        this.expressionEvaluator = mergeContext.getExpressionEvaluator();
        this.conditionExpression = definition.getConditionExpression();
        this.referenceMatchExpression = definition.getReferenceMatchExpression();
        this.referenceMatchType = definition.getReferenceMatchType();
        this.referenceMergeMethod = definition.getReferenceMergeMethod();
        this.referenceMismatchAction = definition.getReferenceMismatchAction();
        this.maximumReferenceCount = definition.getMaximumReferenceCount() > 0 ? definition.getMaximumReferenceCount() : 1000000; // Very large number to simulate 'unlimited'.
    }

    @Override
    public void reset()
    {
        newDataRecords.clear();
        updatedDataRecords.clear();
        attachmentIdentifierMapping.clear();
    }

    @Override
    public Map<String, String> getRecordIdMapping()
    {
        return new HashMap<>();
    }

    @Override
    public Map<String, String> getAttachmentIdentifierMapping()
    {
        return new HashMap<String, String>(attachmentIdentifierMapping);
    }

    @Override
    public Set<DataRecord> getNewDataRecordSet()
    {
        return new HashSet<DataRecord>(newDataRecords);
    }

    @Override
    public Set<DataRecord> getUpdatedDataRecordSet()
    {
        return new HashSet<DataRecord>(updatedDataRecords);
    }

    @Override
    public boolean isApplicable(RecordValue destinationValue, RecordValue sourceValue)
    {
        ValueRequirement destinationValueRequirement;

        // Get the destination value requirement.
        destinationValueRequirement = destinationValue.getValueRequirement();
        if (destinationValueRequirement instanceof DocumentReferenceRequirement)
        {
            DocumentReferenceRequirement referenceRequirement;

            // Get the independent indicator of the destination document reference type.
            referenceRequirement = (DocumentReferenceRequirement)destinationValueRequirement;
            if (referenceRequirement.isIndependent())
            {
                if (!Strings.isNullOrEmpty(conditionExpression))
                {
                    try
                    {
                        Map<String, Object> expressionParameters;

                        expressionParameters = new HashMap<String, Object>();
                        expressionParameters.put("destinationValue", destinationValue);
                        expressionParameters.put("sourceValue", sourceValue);
                        return expressionEvaluator.evaluateBooleanExpression(conditionExpression, expressionParameters, null);
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException("Exception while evaluating condition expression in merger: " + identifier, e);
                    }
                }
                else return true;
            }
            else return false;
        }
        else return false;
    }

    @Override
    public void mergeRecordValues(RecordValue destinationValue, RecordValue sourceValue)
    {
        DocumentReferenceRequirement destinationValueRequirement;
        DocumentReferenceList destinationReferenceList;
        DocumentReferenceList sourceReferenceList;
        DataRecord destinationParentRecord;

        // Get the source requirement type.
        destinationParentRecord = destinationValue.getParentDataRecord();
        destinationReferenceList = (DocumentReferenceList)destinationValue;
        sourceReferenceList = (DocumentReferenceList)sourceValue;

        // Get the destination value requirement.
        destinationValueRequirement = (DocumentReferenceRequirement)destinationValue.getValueRequirement();

        // This merger is only applicable to independent references.
        if (destinationReferenceList.isIndependent())
        {
            List<String> destinationReferences;

            // Get the source and destination references to match.
            destinationReferences = destinationReferenceList.getReferenceIds();
            for (String sourceRecordID : sourceReferenceList.getReferenceIds())
            {
                String matchingDestinationRecordID;
                DataRecord sourceParentRecord;
                DataRecord sourceRecord;

                // Try to find a document reference in the destination that matches the referenced source document.
                sourceParentRecord = sourceReferenceList.getParentDataRecord();
                sourceRecord = sourceParentRecord.findDataRecord(sourceRecordID);
                matchingDestinationRecordID = findMatchingReference(destinationParentRecord, sourceParentRecord, sourceRecord, destinationReferences, sourceRecordID, referenceMatchType);

                // If a destination reference matching the source reference exists, perform the merge action.
                if (matchingDestinationRecordID != null)
                {
                    if (referenceMergeMethod == ReferenceMergeMethod.SUBSTITUTE_REFERENCE)
                    {
                        // No need to merge descendants, so just substitute the reference.
                        destinationReferenceList.replaceReference(matchingDestinationRecordID, sourceRecordID);
                        updatedDataRecords.add(destinationParentRecord);
                    }
                    else throw new RuntimeException("Invalid Reference Merge Method: " + referenceMergeMethod);
                }
                else // If we did not find a matching destination reference, perform the mismatch action.
                {
                    if (referenceMismatchAction == ReferenceMismatchAction.ADD_REFERENCE)
                    {
                        int drReferenceLimit;
                        int referenceLimit;

                        // Determine the maximum number of references allowed.
                        referenceLimit = maximumReferenceCount;
                        drReferenceLimit = destinationValueRequirement.getMaximumReferenceCount();
                        if ((drReferenceLimit > 0) && (drReferenceLimit < maximumReferenceCount)) referenceLimit = drReferenceLimit;

                        // If the number of references is still below the limit, add another.
                        if (destinationReferences.size() < referenceLimit)
                        {
                            // No need to merge descendants, so just add the reference.
                            destinationReferenceList.addReference(sourceRecordID);
                            updatedDataRecords.add(destinationParentRecord);
                        }
                        else
                        {
                            // TODO:  Implement handling of the scenario where maximum reference count is reached and we need to fetch the source record for FFT generation.
                            throw new RuntimeException("Maximum reference count reach and no alternative resolution yet implemented.");
                        }
                    }
                    else if (referenceMismatchAction == ReferenceMismatchAction.SUBSTITUTE_REFERENCE)
                    {
                        // Remove existing references.
                        destinationReferenceList.removeReference(sourceRecordID, false);

                        // Add the new reference.
                        destinationReferenceList.addReference(sourceRecordID);
                        updatedDataRecords.add(destinationParentRecord);
                    }
                    else if (referenceMismatchAction == ReferenceMismatchAction.ADD_FFT)
                    {
                        // TODO:  Implement retrieval of the source record for FFT generation.
                        throw new RuntimeException("FFT generation of independent reference not yet implemented.");
                    }
                    else if (referenceMismatchAction == ReferenceMismatchAction.IGNORE)
                    {
                        // Ignore the mismatch.
                    }
                    else throw new RuntimeException("Invalid Reference Mismatch Action: " +  referenceMismatchAction);
                }
            }
        }
        else throw new RuntimeException("Independent Document Reference merger can only be applied to independent references: " + destinationValue);
    }

    private String findMatchingReference(DataRecord destinationParentRecord, DataRecord sourceParentRecord, DataRecord sourceRecord, List<String> destinationReferences, String sourceReferenceRecordID, ReferenceMatchType matchType)
    {
        // Try to find a document reference in the destination that matches the referenced source document.
        for (String destinationRecordId : destinationReferences)
        {
            DataRecord destinationRecord;

            destinationRecord = destinationParentRecord.findDataRecord(destinationRecordId);
            if (matchType == ReferenceMatchType.RECORD_ID)
            {
                if (Objects.equals(sourceReferenceRecordID, destinationRecordId))
                {
                    return destinationRecordId;
                }
            }
            else if (matchType == ReferenceMatchType.SOURCE_RECORD_ID_TAG)
            {
                String sourceRecordIdAttribute;

                // We will use the source and destination records here, so check that they have been found.
                if (sourceRecord == null) throw new RuntimeException("Cannot find descendant record: " + sourceReferenceRecordID + " referenced from source Record: " + sourceParentRecord);

                // Get the source record ID tag.
                sourceRecordIdAttribute = (String)sourceRecord.getAttribute(T8DataRecordMerger.SOURCE_RECORD_ID_ATTRIBUTE_IDENTIFIER);
                if (sourceRecordIdAttribute != null)
                {
                    if (Objects.equals(sourceRecordIdAttribute, destinationRecordId))
                    {
                        return destinationRecordId;
                    }
                }
                else throw new RuntimeException("Source record '" + sourceRecord + "' tag not found: " + T8DataRecordMerger.SOURCE_RECORD_ID_ATTRIBUTE_IDENTIFIER);
            }
            else if (matchType == ReferenceMatchType.DEFINITION)
            {
                T8DataRecordDefinitionGenerator definitionGenerator;
                String sourceRecordDefinition;
                String destinationRecordDefinition;

                // We will use the source and destination records here, so check that they have been found.
                if (sourceRecord == null) throw new RuntimeException("Cannot find descendant record: " + sourceReferenceRecordID + " referenced from source Record: " + sourceParentRecord);
                if (destinationRecord == null) throw new RuntimeException("Cannot find descendant record: " + destinationRecordId + " referenced from destination Record: " + destinationParentRecord);

                // Generate a destination from the source and destination records and compare the two.
                definitionGenerator = new T8DataRecordDefinitionGenerator(null);
                sourceRecordDefinition = definitionGenerator.generateValueString(sourceRecord).toString();
                destinationRecordDefinition = definitionGenerator.generateValueString(destinationRecord).toString();
                if (Objects.equals(sourceRecordDefinition, destinationRecordDefinition))
                {
                    return destinationRecordId;
                }
            }
            else if (matchType == ReferenceMatchType.EXPRESSION)
            {
                Map<String, Object> expressionParameters;

                // Create a map of expression input parameters.
                expressionParameters = new HashMap<String, Object>();
                expressionParameters.put("destinationRecord", destinationRecord);
                expressionParameters.put("sourceRecord", sourceRecord);

                // Evaluate the expression.
                try
                {
                    if (expressionEvaluator.evaluateBooleanExpression(referenceMatchExpression, expressionParameters, null))
                    {
                        return destinationRecordId;
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception during evaluation of match expression in merger: " + identifier, e);
                }
            }
            else throw new RuntimeException("Invalid record match type: " + matchType);
        }

        // No matching reference found.
        return null;
    }
}
