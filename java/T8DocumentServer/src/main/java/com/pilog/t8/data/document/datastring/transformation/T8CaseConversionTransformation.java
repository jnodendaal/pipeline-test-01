package com.pilog.t8.data.document.datastring.transformation;

/**
 * @author Bouwer du Preez
 */
public class T8CaseConversionTransformation 
{
    private void applyTitleCaseConversion(StringBuffer inputString)
    {
        char previousCharacter;

        previousCharacter = ' ';
        for (int charIndex = 0; charIndex < inputString.length(); charIndex++)
        {
            char character;

            character = inputString.charAt(charIndex);
            if ((!Character.isLetter(previousCharacter)) && (Character.isLetter(character)))
            {
                inputString.setCharAt(charIndex, Character.toTitleCase(character));
            }

            previousCharacter = character;
        }
    }

    private void applySentenceCaseConversion(StringBuffer inputString)
    {
        char character;

        character = inputString.charAt(0);
        if (Character.isLetter(character)) inputString.setCharAt(0, Character.toTitleCase(character));
    }

    private void applyUpperCaseConversion(StringBuffer inputString)
    {
        for (int characterIndex = 0; characterIndex < inputString.length(); characterIndex++)
        {
            inputString.setCharAt(characterIndex, Character.toUpperCase(inputString.charAt(characterIndex)));
        }
    }    
}
