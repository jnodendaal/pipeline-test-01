package com.pilog.t8.api;

import com.pilog.t8.cache.T8DataRecordCache;
import com.pilog.t8.cache.T8DataRecordCache.RecordData;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.T8DataRecordValueFilterHandler;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.prescribedvalue.T8PrescribedValueDataHandler;
import com.pilog.t8.datastructures.tuples.Pair;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarecord.RecordAttribute;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarecord.value.QualifierOfMeasure;
import com.pilog.t8.data.document.datarecord.value.UnitOfMeasure;
import com.pilog.t8.data.ontology.T8OntologyConcept.T8ConceptElementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.org.T8TerminologyDataHandler;
import java.util.ArrayList;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.security.T8Context;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordApiDataHandler
{
    private final T8Context context;

    public T8DataRecordApiDataHandler(T8Context context)
    {
        this.context = context;
    }

    public List<RecordValue> retrievePrescribedValueSuggestions(T8DataTransaction tx, T8OrganizationStructure organizationStructure, T8OntologyStructure ontologyStructure, ValueRequirement valueRequirement, RequirementType requirementType, String drId, String drInstanceId, String propertyId, String fieldId, List<String> conceptIdList, String searchPrefix, boolean restrictToApprovedValues, int pageOffset, int pageSize) throws Exception
    {
        List<T8DataEntity> prescribedValueEntities;
        T8DataRecordValueFilterHandler filterHandler;
        T8DataFilter dataFilter;

        // Get the entity identifier.
        filterHandler = new T8DataRecordValueFilterHandler(context, organizationStructure, ontologyStructure);
        dataFilter = filterHandler.createPrescribedValueFilter(requirementType, drId, drInstanceId, propertyId, fieldId, conceptIdList, searchPrefix, restrictToApprovedValues);

        // Retrieve the values and return the results.
        prescribedValueEntities = tx.select(dataFilter.getEntityIdentifier(), dataFilter, pageOffset, pageSize);
        return T8PrescribedValueDataHandler.createPrescribedRecordValues(valueRequirement, prescribedValueEntities);
    }

    public List<RecordValue> retrievePrescribedValueOptions(T8DataTransaction tx, T8OrganizationStructure organizationStructure, T8OntologyStructure ontologyStructure, ValueRequirement valueRequirement, RequirementType requirementType, String drId, String drInstanceId, String propertyId, String fieldId, List<String> conceptIdList, boolean restrictToApprovedValues, String searchText, int pageOffset, int pageSize) throws Exception
    {
        List<T8DataEntity> prescribedValueEntities;
        T8DataRecordValueFilterHandler filterHandler;
        T8DataFilter dataFilter;

        // Get the entity identifier.
        filterHandler = new T8DataRecordValueFilterHandler(context, organizationStructure, ontologyStructure);
        dataFilter = filterHandler.createPrescribedValueFilter(requirementType, drId, drInstanceId, propertyId, fieldId, conceptIdList, restrictToApprovedValues);

        // Retrieve the values and return the results.
        prescribedValueEntities = tx.select(dataFilter.getEntityIdentifier(), dataFilter, pageOffset, pageSize);
        return T8PrescribedValueDataHandler.createPrescribedRecordValues(valueRequirement, prescribedValueEntities);
    }

    public List<T8ConceptTerminology> retrievePrescribedDrInstanceOptions(T8DataTransaction tx, T8OrganizationStructure organizationStructure, T8OntologyStructure ontologyStructure, ValueRequirement valueRequirement, RequirementType requirementType, String drId, String drInstanceId, String propertyId, String fieldId, List<String> conceptIdList, boolean restrictToApprovedValues, int pageOffset, int pageSize) throws Exception
    {
        List<T8DataEntity> prescribedValueEntities;
        List<T8ConceptTerminology> drInstanceOptions;
        T8DataRecordValueFilterHandler filterHandler;
        T8DataFilter dataFilter;

        // Get the entity identifier.
        filterHandler = new T8DataRecordValueFilterHandler(context, organizationStructure, ontologyStructure);
        dataFilter = filterHandler.createPrescribedValueFilter(requirementType, drId, drInstanceId, propertyId, fieldId, conceptIdList, restrictToApprovedValues);

        // Retrieve the values and return the results.
        drInstanceOptions = new ArrayList<>();
        prescribedValueEntities = tx.select(dataFilter.getEntityIdentifier(), dataFilter, pageOffset, pageSize);
        for (T8DataEntity entity : prescribedValueEntities)
        {
            T8ConceptTerminology conceptTerminology;
            String conceptId;
            String conceptTypeId;
            String languageId;
            String term;
            String termId;
            String definition;
            String definitionId;
            String abbreviation;
            String abbreviationId;
            String code;
            String codeId;
            String orgId;

            orgId = (String)entity.getFieldValue(EF_ORG_ID);
            conceptTypeId = (String)entity.getFieldValue(EF_CONCEPT_TYPE_ID);
            conceptId = (String)entity.getFieldValue(EF_CONCEPT_ID);
            languageId = (String)entity.getFieldValue(EF_LANGUAGE_ID);
            term = (String)entity.getFieldValue(EF_CONCEPT_TERM);
            termId = (String)entity.getFieldValue(EF_CONCEPT_TERM_ID);
            definition = (String)entity.getFieldValue(EF_CONCEPT_DEFINITION);
            definitionId = (String)entity.getFieldValue(EF_CONCEPT_DEFINITION_ID);
            abbreviation = (String)entity.getFieldValue(EF_CONCEPT_ABBREVIATION);
            abbreviationId = (String)entity.getFieldValue(EF_CONCEPT_ABBREVIATION_ID);
            code = (String)entity.getFieldValue(EF_CONCEPT_CODE);
            codeId = (String)entity.getFieldValue(EF_CONCEPT_CODE_ID);

            conceptTerminology = new T8ConceptTerminology(T8OntologyConceptType.getConceptType(conceptTypeId), conceptId);
            conceptTerminology.setTerm(term);
            conceptTerminology.setTermId(termId);
            conceptTerminology.setDefinition(definition);
            conceptTerminology.setDefinitionId(definitionId);
            conceptTerminology.setAbbreviation(abbreviation);
            conceptTerminology.setAbbreviationId(abbreviationId);
            conceptTerminology.setCode(code);
            conceptTerminology.setCodeId(codeId);
            conceptTerminology.setLanguageId(languageId);
            conceptTerminology.setOrganizationId(orgId);
            drInstanceOptions.add(conceptTerminology);
        }

        return drInstanceOptions;
    }

    public String retrievePrescribedConceptId(T8DataTransaction tx, T8OrganizationStructure organizationStructure, T8OntologyStructure ontologyStructure, ValueRequirement valueRequirement, RequirementType requirementType, String drId, String drInstanceId, String propertyId, String fieldId, List<String> conceptIdList, boolean restrictToApprovedValues, String matchString, List<T8ConceptElementType> elementTypes) throws Exception
    {
        List<T8DataEntity> prescribedValueEntities;
        T8DataRecordValueFilterHandler filterHandler;
        T8DataFilter dataFilter;

        // Get the entity identifier.
        filterHandler = new T8DataRecordValueFilterHandler(context, organizationStructure, ontologyStructure);
        dataFilter = filterHandler.createExactMatchPrescribedValueFilter(requirementType, drId, drInstanceId, propertyId, fieldId, conceptIdList, restrictToApprovedValues, matchString, elementTypes);

        // Retrieve the values and return the results.
        prescribedValueEntities = tx.select(dataFilter.getEntityIdentifier(), dataFilter, 0, 1);
        return prescribedValueEntities.isEmpty() ? null : (String)prescribedValueEntities.get(0).getFieldValue(EF_CONCEPT_ID);
    }

    public String retrieveConceptId(T8DataTransaction tx, T8OrganizationStructure organizationStructure, T8OntologyStructure ontologyStructure, ValueRequirement valueRequirement, RequirementType requirementType, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList, String matchString, List<T8ConceptElementType> elementTypes) throws Exception
    {
        List<T8DataEntity> conceptEntities;
        T8DataRecordValueFilterHandler filterHandler;
        T8DataFilter dataFilter;

        // Get the entity identifier.
        filterHandler = new T8DataRecordValueFilterHandler(context, organizationStructure, ontologyStructure);
        dataFilter = filterHandler.createExactMatchConceptFilter(requirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList, matchString, elementTypes);

        // Retrieve the values and return the results.
        conceptEntities = tx.select(dataFilter.getEntityIdentifier(), dataFilter, 0, 1);
        return conceptEntities.isEmpty() ? null : (String)conceptEntities.get(0).getFieldValue(EF_CONCEPT_ID);
    }

    public List<RecordValue> retrieveConceptValueSuggestions(T8DataTransaction tx, T8OrganizationStructure organizationStructure, T8OntologyStructure ontologyStructure, ValueRequirement valueRequirement, RequirementType requirementType, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList, String searchPrefix, int pageOffset, int pageSize) throws Exception
    {
        List<T8DataEntity> conceptEntities;
        T8DataRecordValueFilterHandler filterHandler;
        T8DataFilter dataFilter;

        // Get the entity identifier.
        filterHandler = new T8DataRecordValueFilterHandler(context, organizationStructure, ontologyStructure);
        dataFilter = filterHandler.createSuggestionConceptFilter(requirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList, searchPrefix);

        // Retrieve the values and return the results.
        conceptEntities = tx.select(dataFilter.getEntityIdentifier(), dataFilter, pageOffset, pageSize);
        return createConceptRecordValues(valueRequirement, conceptEntities);
    }

    public List<RecordValue> retrieveConceptValueOptions(T8DataTransaction tx, T8OrganizationStructure organizationStructure, T8OntologyStructure ontologyStructure, ValueRequirement valueRequirement, RequirementType requirementType, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList, String searchTerms, List<T8ConceptElementType> searchElementTypes, int pageOffset, int pageSize) throws Exception
    {
        List<T8DataEntity> conceptEntities;
        T8DataRecordValueFilterHandler filterHandler;
        T8DataFilter dataFilter;

        // Create either a regular concept filter or a search filter if search terms are specified.
        filterHandler = new T8DataRecordValueFilterHandler(context, organizationStructure, ontologyStructure);
        if (Strings.trimToNull(searchTerms) != null)
        {
            dataFilter = filterHandler.createSearchConceptFilter(requirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList, searchTerms, searchElementTypes);
        }
        else
        {
            dataFilter = filterHandler.createConceptFilter(requirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList);
        }

        // Retrieve the values and return the results.
        conceptEntities = tx.select(dataFilter.getEntityIdentifier(), dataFilter, pageOffset, pageSize);
        return createConceptRecordValues(valueRequirement, conceptEntities);
    }

    public List<T8ConceptTerminology> retrieveDrInstanceOptions(T8DataTransaction tx, T8OrganizationStructure organizationStructure, T8OntologyStructure ontologyStructure, ValueRequirement valueRequirement, RequirementType requirementType, List<String> orgIdList, List<String> ocIdList, List<String> conceptIdList, List<Pair<String, String>> conceptDependencyList, int pageOffset, int pageSize) throws Exception
    {
        List<T8DataEntity> ontologyConceptEntities;
        List<T8ConceptTerminology> drInstanceOptions;
        T8DataRecordValueFilterHandler filterHandler;
        T8DataFilter dataFilter;

        // Get the entity identifier.
        filterHandler = new T8DataRecordValueFilterHandler(context, organizationStructure, ontologyStructure);
        dataFilter = filterHandler.createConceptFilter(requirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList);

        // Retrieve the values and return the results.
        drInstanceOptions = new ArrayList<>();
        ontologyConceptEntities = tx.select(dataFilter.getEntityIdentifier(), dataFilter, pageOffset, pageSize);
        for (T8DataEntity entity : ontologyConceptEntities)
        {
            drInstanceOptions.add(T8TerminologyDataHandler.constructTerminology(entity));
        }

        return drInstanceOptions;
    }

    public List<RecordAttribute> retrieveRecordAttributes(T8DataTransaction tx, String recordId, List<String> attributeIds) throws Exception
    {
        List<T8DataEntity> attributeEntities;
        List<RecordAttribute> recordAttributes;
        T8DataFilter filter;
        String entityId;

        // Get the entity identifier.
        entityId = DATA_RECORD_DOC_ATR_DE_IDENTIFIER;

        // Create the selection filter.
        filter = new T8DataFilter(entityId, HashMaps.createSingular(entityId + EF_RECORD_ID, recordId));
        if (attributeIds != null) filter.addFilterCriteria(DataFilterConjunction.AND, entityId + EF_ATTRIBUTE_ID, attributeIds);

        // Select the entities and convert all of them to attribute objects.
        attributeEntities = tx.select(entityId, filter);
        recordAttributes = new ArrayList<>();
        for (T8DataEntity attributeEntity : attributeEntities)
        {
            RecordAttribute recordAttribute;
            String rootRecordId;
            String value;
            String type;
            String attributeId;

            attributeId = (String)attributeEntity.getFieldValue(EF_ATTRIBUTE_ID);
            value = (String)attributeEntity.getFieldValue(EF_VALUE);
            type = (String)attributeEntity.getFieldValue(EF_TYPE);
            rootRecordId = (String)attributeEntity.getFieldValue(EF_ROOT_RECORD_ID);

            recordAttribute = new RecordAttribute(attributeId, RecordAttribute.getValue(type, value));
            recordAttribute.setRootRecordId(rootRecordId);
            recordAttribute.setRecordId(recordId);
            recordAttributes.add(recordAttribute);
        }

        // Return the final list of attributes.
        return recordAttributes;
    }

    public RecordAttribute retrieveRecordAttribute(T8DataTransaction tx, String recordId, String attributeId) throws Exception
    {
        T8DataEntity attributeEntity;
        String entityId;

        // Get the entity identifier.
        entityId = DATA_RECORD_DOC_ATR_DE_IDENTIFIER;
        attributeEntity = tx.retrieve(entityId, HashMaps.newHashMap(entityId + EF_RECORD_ID, recordId, entityId + EF_ATTRIBUTE_ID, attributeId));
        if (attributeEntity != null)
        {
            RecordAttribute recordAttribute;
            String rootRecordId;
            String value;
            String type;

            attributeId = (String)attributeEntity.getFieldValue(EF_ATTRIBUTE_ID);
            value = (String)attributeEntity.getFieldValue(EF_VALUE);
            type = (String)attributeEntity.getFieldValue(EF_TYPE);
            rootRecordId = (String)attributeEntity.getFieldValue(EF_ROOT_RECORD_ID);

            recordAttribute = new RecordAttribute(attributeId, RecordAttribute.getValue(type, value));
            recordAttribute.setRootRecordId(rootRecordId);
            recordAttribute.setRecordId(recordId);
            return recordAttribute;
        }
        else return null;
    }

    public void insertRecordAttribute(T8DataTransaction tx, String rootRecordId, String recordId, String attributeId, Object attributeValue) throws Exception
    {
        T8DataRecordCache recordCache;
        RecordData cachedData;
        T8DataEntity entity;
        String entityId;
        String type;
        String value;

        // Get the entity identifier.
        entityId = DATA_RECORD_DOC_ATR_DE_IDENTIFIER;
        type = RecordAttribute.getTypeString(attributeValue);
        value = attributeValue != null ? attributeValue.toString() : null;

        // Construct the entity.
        entity = tx.create(entityId, null);
        entity.setFieldValue(entityId + EF_RECORD_ID, recordId);
        entity.setFieldValue(entityId + EF_ATTRIBUTE_ID, attributeId);
        entity.setFieldValue(entityId + EF_TYPE, type);
        entity.setFieldValue(entityId + EF_VALUE, value);
        entity.setFieldValue(entityId + EF_ROOT_RECORD_ID, rootRecordId);

        // Update the cached record data to reflect the changes in the operation tables.
        recordCache = tx.getDataRecordCache();
        cachedData = recordCache.get(recordId);
        if (cachedData != null)
        {
            cachedData.insertAttributeValue(rootRecordId, recordId, attributeId, type, value);
            recordCache.put(cachedData);
        }

        // Insert the attribute entity.
        tx.insert(entity);
    }

    public boolean updateRecordAttribute(T8DataTransaction tx, String rootRecordId, String recordId, String attributeId, Object attributeValue) throws Exception
    {
        T8DataRecordCache recordCache;
        RecordData cachedData;
        T8DataEntity entity;
        String entityId;
        String type;
        String value;

        // Get the entity identifier.
        entityId = DATA_RECORD_DOC_ATR_DE_IDENTIFIER;
        type = RecordAttribute.getTypeString(attributeValue);
        value = attributeValue != null ? attributeValue.toString() : null;

        // Construct the entity.
        entity = tx.create(entityId, null);
        entity.setFieldValue(entityId + EF_RECORD_ID, recordId);
        entity.setFieldValue(entityId + EF_ATTRIBUTE_ID, attributeId);
        entity.setFieldValue(entityId + EF_TYPE, type);
        entity.setFieldValue(entityId + EF_VALUE, value);
        entity.setFieldValue(entityId + EF_ROOT_RECORD_ID, rootRecordId);

        // Update the cached record data to reflect the changes in the operation tables.
        recordCache = tx.getDataRecordCache();
        cachedData = recordCache.get(recordId);
        if (cachedData != null)
        {
            cachedData.updateAttributeValue(attributeId, type, value);
            recordCache.put(cachedData);
        }

        // Update the attribute entity.
        return tx.update(entity);
    }

    public void saveRecordAttribute(T8DataTransaction tx, String recordId, String attributeId, Object attributeValue) throws Exception
    {
        T8DataRecordCache recordCache;
        RecordData cachedData;
        T8DataEntity entity;
        String entityId;
        String rootRecordId;
        String type;
        String value;

        // Get the entity identifier.
        entityId = DATA_RECORD_DOC_ATR_DE_IDENTIFIER;

        // Get the root record id.
        rootRecordId = retrieveRootRecordId(tx, recordId);
        type = RecordAttribute.getTypeString(attributeValue);
        value = attributeValue != null ? attributeValue.toString() : null;

        // Construct the entity.
        entity = tx.create(entityId, null);
        entity.setFieldValue(entityId + EF_RECORD_ID, recordId);
        entity.setFieldValue(entityId + EF_ATTRIBUTE_ID, attributeId);
        entity.setFieldValue(entityId + EF_TYPE, type);
        entity.setFieldValue(entityId + EF_VALUE, value);
        entity.setFieldValue(entityId + EF_ROOT_RECORD_ID, rootRecordId);

        // Get the cached data for the affected record.
        recordCache = tx.getDataRecordCache();
        cachedData = recordCache.get(recordId);

        // Save the attribute entity.
        if (tx.update(entity))
        {
            if (cachedData != null)
            {
                cachedData.updateAttributeValue(attributeId, type, value);
                recordCache.put(cachedData);
            }
        }
        else
        {
            tx.insert(entity);
            if (cachedData != null)
            {
                cachedData.insertAttributeValue(rootRecordId, recordId, attributeId, type, value);
                recordCache.put(cachedData);
            }
        }
    }

    public boolean deleteRecordAttribute(T8DataTransaction tx, String recordId, String attributeId) throws Exception
    {
        T8DataRecordCache recordCache;
        T8DataFilter dataFilter;
        RecordData cachedData;
        String entityId;

        // Get the entity identifier.
        entityId = DATA_RECORD_DOC_ATR_DE_IDENTIFIER;
        dataFilter = new T8DataFilter(entityId);
        dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_RECORD_ID, DataFilterOperator.EQUAL, recordId, false);
        dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_ATTRIBUTE_ID, DataFilterOperator.EQUAL, attributeId, false);

        // Update the cached record data to reflect the changes in the operation tables.
        recordCache = tx.getDataRecordCache();
        cachedData = recordCache.get(recordId);
        if (cachedData != null)
        {
            cachedData.deleteAttributeData(ArrayLists.newArrayList(attributeId));
            recordCache.put(cachedData);
        }

        // Delete the record attribute
        return tx.delete(entityId, dataFilter) > 0;
    }

    public int deleteRecordAttributes(T8DataTransaction tx, String recordId, List<String> attributeIds) throws Exception
    {
        T8DataRecordCache recordCache;
        T8DataFilter dataFilter;
        RecordData cachedData;
        String entityId;

        // Get the entity identifier.
        entityId = DATA_RECORD_DOC_ATR_DE_IDENTIFIER;

        // Create the filter to identify the attribute record to delete.
        dataFilter = new T8DataFilter(entityId);
        dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_RECORD_ID, DataFilterOperator.EQUAL, recordId, false);
        if (CollectionUtilities.hasContent(attributeIds)) dataFilter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, entityId + EF_ATTRIBUTE_ID, DataFilterOperator.IN, attributeIds, false);

        // Update the cached record data to reflect the changes in the operation tables.
        recordCache = tx.getDataRecordCache();
        cachedData = recordCache.get(recordId);
        if (cachedData != null)
        {
            cachedData.deleteAttributeData(attributeIds);
            recordCache.put(cachedData);
        }

        // Delete the record attributes.
        return tx.delete(entityId, dataFilter);
    }

    public String retrieveRootRecordId(T8DataTransaction tx, String recordId) throws Exception
    {
        T8DataEntity entity;

        // Construct the entity.
        entity = tx.retrieve(DATA_RECORD_DE_IDENTIFIER, HashMaps.createSingular(DATA_RECORD_DE_IDENTIFIER + EF_RECORD_ID, recordId));
        return entity != null ? (String)entity.getFieldValue(DATA_RECORD_DE_IDENTIFIER + EF_ROOT_RECORD_ID) : null;
    }

    public List<RecordValue> createConceptRecordValues(ValueRequirement valueRequirement, List<T8DataEntity> conceptEntities)
    {
        List<RecordValue> recordValues;

        // Create a RecordValue from each of the entities in the list.
        recordValues = new ArrayList<>();
        for (T8DataEntity conceptEntity : conceptEntities)
        {
            RecordValue recordValue;

            // Create the value from the entity.
            recordValue = createConceptRecordValue(valueRequirement, conceptEntity);

            // Add to the returning list
            recordValues.add(recordValue);
        }

        return recordValues;
    }

    public RecordValue createConceptRecordValue(ValueRequirement valueRequirement, T8DataEntity conceptEntity)
    {
        String standardIndicator;
        String approvedIndicator;
        String entityId;
        String valueId;
        boolean standard;
        boolean approved;

        // Get the standard value identifier from the entity
        entityId = conceptEntity.getIdentifier();
        valueId = (String)conceptEntity.getFieldValue(entityId + EF_VALUE_ID);
        standardIndicator = (String)conceptEntity.getFieldValue(entityId + EF_STANDARD_INDICATOR);
        approvedIndicator = (String)conceptEntity.getFieldValue(entityId + EF_APPROVED_INDICATOR);
        standard = "Y".equalsIgnoreCase(standardIndicator);
        approved = "Y".equalsIgnoreCase(approvedIndicator);

        switch (valueRequirement.getRequirementType())
        {
            case CONTROLLED_CONCEPT:
            {
                ControlledConcept value;
                String conceptId;

                conceptId = (String) conceptEntity.getFieldValue(entityId + EF_CONCEPT_ID);
                value = new ControlledConcept(valueRequirement);
                value.setConceptId(conceptId, false);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case DOCUMENT_REFERENCE:
            {
                DocumentReferenceList value;
                String conceptId;

                conceptId = (String) conceptEntity.getFieldValue(entityId + EF_CONCEPT_ID);
                value = new DocumentReferenceList(valueRequirement);
                value.addReference(conceptId);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case QUALIFIER_OF_MEASURE:
            {
                QualifierOfMeasure value;
                String conceptId;

                conceptId = (String) conceptEntity.getFieldValue(entityId + EF_CONCEPT_ID);
                value = new QualifierOfMeasure(valueRequirement);
                value.setValue(conceptId);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            case UNIT_OF_MEASURE:
            {
                UnitOfMeasure value;
                String conceptId;

                conceptId = (String) conceptEntity.getFieldValue(entityId + EF_CONCEPT_ID);
                value = new UnitOfMeasure(valueRequirement);
                value.setValue(conceptId);
                value.setPrescribedValueID(valueId);
                value.setStandard(standard);
                value.setApproved(approved);
                return value;
            }
            default:
            {
                throw new IllegalStateException("Invalid record value type: " + valueRequirement.getRequirementType());
            }
        }
    }
}
