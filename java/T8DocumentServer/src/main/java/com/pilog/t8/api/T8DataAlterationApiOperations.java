package com.pilog.t8.api;

import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.alteration.T8DataAlterationImpact;
import com.pilog.t8.data.alteration.T8DataAlterationPackage;
import com.pilog.t8.data.alteration.T8DataAlterationPackageIndex;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataAlterationApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataAlterationApiOperations
{
    public static class ApplyAlterationPackageOperation extends T8DefaultServerOperation
    {
        private double progress;

        public ApplyAlterationPackageOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataAlterationApi daApi;
            T8DataTransaction itx;
            T8DataSession ds;
            long startTime;
            String packageIid;
            int packageSize;
            int stepCount;
            int currentStep;
            int startStep;
            int endStep;

            // Get the operation parameters we need.
            packageIid = (String)operationParameters.get(PARAMETER_PACKAGE_IID);
            startStep = (Integer)operationParameters.get(PARAMETER_START_STEP);
            endStep = (Integer)operationParameters.get(PARAMETER_END_STEP);

            // Set the parameters of the operation.
            daApi = (T8DataAlterationApi)serverContext.getConfigurationManager().getAPI(context, T8DataAlterationApi.API_IDENTIFIER);

            // Use the instance transaction for the initial count and retrieval.
            ds = serverContext.getDataManager().getCurrentSession();
            itx = ds.instantTransaction();
            packageSize = daApi.retrieveAlterationPackageSize(packageIid);
            stepCount = endStep - startStep + 1;

            // Retrieve the first page of data and start the loop.
            progress = 0.0;
            currentStep = startStep;
            startTime = System.currentTimeMillis();
            while ((!stopFlag) && (currentStep <= endStep) && (currentStep < packageSize))
            {
                T8DataTransaction loopTx = null;
                int progressCount;

                try
                {
                    loopTx = ds.beginTransaction();
                    daApi.applyAlteration(packageIid, currentStep);
                    loopTx.commit();
                }
                catch (Exception e)
                {
                    if (loopTx != null) loopTx.rollback();
                    throw e;
                }

                // Update the progress of the operation.
                progressCount = currentStep - startStep;
                if (progressCount > stepCount) progressCount = stepCount;
                progress = (((progressCount * 1.00) / (stepCount * 1.00)) * 100.00);

                // Increment the currentStep.
                currentStep++;
            }

            return null;
        }

        @Override
        public double getProgress(T8Context context)
        {
            return progress;
        }
    }

    public static class ApplyAlterationOperation extends T8DefaultServerOperation
    {
        public ApplyAlterationOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataAlterationImpact altImpact;
            T8DataAlterationApi altApi;
            T8DataAlteration alteration;

            // Get the operation parameters we need.
            alteration = (T8DataAlteration)operationParameters.get(PARAMETER_ALTERATION);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            altImpact = altApi.applyAlteration(alteration);
            return HashMaps.createSingular(PARAMETER_ALTERATION_IMPACT, altImpact);
        }
    }

    public static class InsertAlterationOperation extends T8DefaultServerOperation
    {
        public InsertAlterationOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataAlterationPackageIndex index;
            T8DataAlterationApi altApi;
            T8DataAlteration alteration;

            // Get the operation parameters we need.
            alteration = (T8DataAlteration)operationParameters.get(PARAMETER_ALTERATION);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            index = altApi.insertAlteration(alteration);
            return HashMaps.createSingular(PARAMETER_PACKAGE_INDEX, index);
        }
    }

    public static class InsertAlterationPackageOperation extends T8DefaultServerOperation
    {
        public InsertAlterationPackageOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataAlterationApi altApi;
            T8DataAlterationPackage alterationPackage;

            // Get the operation parameters we need.
            alterationPackage = (T8DataAlterationPackage)operationParameters.get(PARAMETER_ALTERATION_PACKAGE);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            altApi.insertAlterationPackage(alterationPackage);

            // Nothing to return.
            return null;
        }
    }

    public static class DeleteAlterationOperation extends T8DefaultServerOperation
    {
        public DeleteAlterationOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataAlterationPackageIndex index;
            T8DataAlterationApi altApi;
            String alterationID;
            Integer step;

            // Get the operation parameters we need.
            alterationID = (String)operationParameters.get(PARAMETER_ALTERATION_ID);
            step = (Integer)operationParameters.get(PARAMETER_STEP);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            index = altApi.deleteAlteration(alterationID, step);
            return HashMaps.createSingular(PARAMETER_PACKAGE_INDEX, index);
        }
    }

    public static class DeleteAlterationPackageOperation extends T8DefaultServerOperation
    {
        public DeleteAlterationPackageOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataAlterationApi altApi;
            String packageIid;

            // Get the operation parameters we need.
            packageIid = (String) operationParameters.get(PARAMETER_PACKAGE_IID);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            altApi.deleteAlterationPackage(packageIid);

            // Nothing to return.
            return null;
        }
    }

    public static class GetAlterationPackageIndexOperation extends T8DefaultServerOperation
    {
        public GetAlterationPackageIndexOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataAlterationPackageIndex index;
            T8DataAlterationApi altApi;
            String altpackageIiD;

            // Get the operation parameters we need.
            altpackageIiD = (String)operationParameters.get(PARAMETER_PACKAGE_IID);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            index = altApi.getAlterationPackageIndex(altpackageIiD);
            return HashMaps.createSingular(PARAMETER_PACKAGE_INDEX, index);
        }
    }

    public static class RetrieveAlterationPackageOperation extends T8DefaultServerOperation
    {
        public RetrieveAlterationPackageOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String packageIid;
            T8DataAlterationApi altApi;
            T8DataAlterationPackage alterationPackage;

            // Get the operation parameters we need.
            packageIid = (String)operationParameters.get(PARAMETER_PACKAGE_IID);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            alterationPackage = altApi.retrieveAlterationPackage(packageIid);
            return HashMaps.createSingular(PARAMETER_ALTERATION_PACKAGE, alterationPackage);
        }
    }

    public static class RetrieveAlterationOperation extends T8DefaultServerOperation
    {
        public RetrieveAlterationOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String packageIid;
            int step;
            T8DataAlterationApi altApi;
            T8DataAlteration alteration;

            // Get the operation parameters we need.
            packageIid = (String)operationParameters.get(PARAMETER_PACKAGE_IID);
            step = (int)operationParameters.get(PARAMETER_STEP);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            alteration = altApi.retrieveAlteration(packageIid,step);
            return HashMaps.createSingular(PARAMETER_ALTERATION, alteration);
        }
    }

    public static class UpdateAlterationOperation extends T8DefaultServerOperation
    {
        public UpdateAlterationOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataAlterationApi altApi;
            T8DataAlteration alteration;

            // Get the operation parameters we need.
            alteration = (T8DataAlteration)operationParameters.get(PARAMETER_ALTERATION);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            altApi.updateAlteration(alteration);

            // Nothing to return.
            return null;
        }
    }

    public static class UpdateAlterationPackageOperation extends T8DefaultServerOperation
    {
        public UpdateAlterationPackageOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataAlterationApi altApi;
            T8DataAlterationPackage alterationPackage;

            // Get the operation parameters we need.
            alterationPackage = (T8DataAlterationPackage)operationParameters.get(PARAMETER_ALTERATION_PACKAGE);

            // Perform the API operation.
            altApi = tx.getApi(T8DataAlterationApi.API_IDENTIFIER);
            altApi.updateAlterationPackage(alterationPackage);

            // Nothing to return.
            return null;
        }
    }
}