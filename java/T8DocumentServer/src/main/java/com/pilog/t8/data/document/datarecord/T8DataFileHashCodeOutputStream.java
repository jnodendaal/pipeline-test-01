package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.api.T8DataRecordMatchApi;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datafile.T8DataFileOutputStream;
import java.util.Collection;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileHashCodeOutputStream implements T8DataFileOutputStream
{
    private final T8DataRecordMatchApi mtcApi;
    private final T8DataTransaction tx;
    private boolean enabled;

    public T8DataFileHashCodeOutputStream(T8DataTransaction tx)
    {
        this.tx = tx;
        this.mtcApi = tx.getApi(T8DataRecordMatchApi.API_IDENTIFIER);
        this.enabled = true;
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    @Override
    public void write(DataRecord dataFile, T8DataFileAlteration alteration, Collection<RecordMetaDataType> metaDataTypes) throws Exception
    {
        // Generate duplicate detection hashes if required.
        if (enabled)
        {
            if ((metaDataTypes == null)|| (metaDataTypes.contains(RecordMetaDataType.ALL)) || (metaDataTypes.contains(RecordMetaDataType.DUPLICATE_DETECTION_HASH)))
            {
                mtcApi.refreshFileHashCodes(dataFile);
            }
        }
    }
}
