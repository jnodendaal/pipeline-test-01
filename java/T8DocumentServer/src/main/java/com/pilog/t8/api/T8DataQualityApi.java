package com.pilog.t8.api;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.dataquality.T8GroupedDataQualityReport;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataQualityApi implements T8Api, T8PerformanceStatisticsProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataQualityApi.class);

    private final T8DataTransaction tx;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8DataQualityApiDataHandler dqDataHandler;
    private T8PerformanceStatistics stats;

    public static final String API_IDENTIFIER = "@API_DATA_QUALITY";

    public T8DataQualityApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.serverContext = context.getServerContext();
        this.stats = tx.getPerformanceStatistics();
        this.dqDataHandler = new T8DataQualityApiDataHandler(tx);
    }
    
    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    public T8Context getContext()
    {
        return context;
    }

    public T8GroupedDataQualityReport retrieveRootDataQualityReport(List<String> rootDrInstanceIdList) throws Exception
    {
        return dqDataHandler.retrieveRootDataQualityReport(tx, rootDrInstanceIdList);
    }

    public T8GroupedDataQualityReport retrieveDataQualityReportGroupedByDrInstance(String rootDrInstanceId, List<String> filterDrInstanceIdList) throws Exception
    {
        T8OntologyApi ontApi;

        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        return dqDataHandler.retrieveDataQualityReportGroupedByDrInstance(tx, ontApi.getOntologyStructure().getID(), rootDrInstanceId, filterDrInstanceIdList);
    }

    public T8GroupedDataQualityReport retrieveRootDataQualityReportGroupedByDrInstance(String rootDrInstanceId, String groupByTargetDrInstanceId, String groupByTargetPropertyId, List<String> filterDrInstanceIdList) throws Exception
    {
        return dqDataHandler.retrieveRootDataQualityReportGroupedByDrInstance(tx, rootDrInstanceId, groupByTargetDrInstanceId, groupByTargetPropertyId, filterDrInstanceIdList);
    }

    public T8GroupedDataQualityReport retrieveRootDataQualityReportGroupedByValueConcept(String rootDrInstanceId, String groupByTargetDrInstanceId, String groupByTargetPropertyId, List<String> filterDrInstanceIdList) throws Exception
    {
        return dqDataHandler.retrieveRootDataQualityReportGroupedByValueConcept(tx, rootDrInstanceId, groupByTargetDrInstanceId, groupByTargetPropertyId, filterDrInstanceIdList);
    }
}
