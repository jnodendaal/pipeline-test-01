package com.pilog.t8.api;

import com.pilog.t8.api.datarecordeditor.T8DataFileNewAttachment;
import com.pilog.t8.api.datarecordeditor.T8DataFileNewRecord;
import com.pilog.t8.api.datarecordeditor.T8DataFileRecordCreation;
import com.pilog.t8.api.datarecordeditor.T8DataFileUpdate;
import com.pilog.t8.api.datarecordeditor.T8DataFileValueUpdate;
import com.pilog.t8.api.datarecordeditor.T8DataRecordEditorSession;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.api.datarecordeditor.T8DataFilePropertyAccessUpdate;
import com.pilog.t8.api.datarecordeditor.T8DataFileRecordDeletion;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.T8DataRecordAccessUpdate;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.time.T8TimeUnit;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.document.datafile.T8DataFileSaveResult;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.T8DataRecordAlteration;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.SectionAccessLayer;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarecord.value.FieldContent;
import com.pilog.t8.data.ontology.T8OntologyConcept.T8ConceptElementType;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordEditorApi implements T8Api, T8PerformanceStatisticsProvider
{
    public static final String API_IDENTIFIER = "@API_DATA_RECORD_EDITOR";

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private T8PerformanceStatistics stats;

    private static final T8TimeUnit FILE_CONTEXT_EXPIRATION_TIME = new T8Minute(30);

    public T8DataRecordEditorApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    private T8DataRecordEditorSession getEditorSession(String editorSessionId)
    {
        T8DataRecordEditorSession editorSession;

        editorSession = (T8DataRecordEditorSession)sessionContext.getSessionData(editorSessionId);
        if (editorSession != null)
        {
            return editorSession;
        }
        else
        {
            editorSession = new T8DataRecordEditorSession(editorSessionId);
            sessionContext.setSessionData(editorSessionId, editorSession);
            return editorSession;
        }
    }

    private void saveEditorSession(T8DataRecordEditorSession editorSession)
    {
        sessionContext.setSessionData(editorSession.getId(), editorSession);
    }

    public DataRecord openFile(String editorSessionId, String recordId) throws Exception
    {
        T8DataRecordEditorSession editorSession;
        T8DataRecordApi recApi;
        DataRecord outputState;
        DataRecord openedFile;

        // Get the API required by this operation.
        recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

        // Get the specified editor session.
        editorSession = getEditorSession(editorSessionId);

        // Retrieve the record in question and return the result.
        openedFile = recApi.retrieveFile(recordId, sessionContext.getContentLanguageIdentifier(), true, true, true, true, true, true);
        openedFile.fill(true); // We do this to allow easier binding in JS.

        // Create the output state of the file which does not contain hidden data.
        outputState = openedFile.copy();
        removeHiddenData(outputState);

        // Update the editor session.
        editorSession.setFile(openedFile);
        editorSession.setOutputState(outputState);
        saveEditorSession(editorSession);

        // Return the opened file.
        return outputState;
    }

    public T8DataFileSaveResult saveFile(String editorSessionId, String fileId) throws Exception
    {
        T8DataRecordEditorSession editorSession;
        DataRecord openFile;

        // Get the specified editor session and file.
        editorSession = getEditorSession(editorSessionId);
        openFile = editorSession.getFile(fileId);
        if (openFile != null)
        {
            T8DataFileSaveResult saveResult;
            T8DataRecordApi recApi;

            // Get the API required by this operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

            // Save the file in question and return the result.
            saveResult = recApi.saveFileStateChange(openFile);
            return saveResult;
        }
        else throw new Exception("Specified file " + fileId + "  is not open in current editor session: " + editorSessionId);
    }

    public T8DataFileSaveResult closeFile(String editorSessionId, String fileId, boolean saveChanges) throws Exception
    {
        T8DataRecordEditorSession editorSession;
        DataRecord openFile;

        // Get the specified editor session and file.
        editorSession = getEditorSession(editorSessionId);
        openFile = editorSession.getFile(fileId);
        if (openFile != null)
        {
            if (saveChanges)
            {
                T8DataFileSaveResult saveResult;
                T8DataRecordApi recApi;

                // Get the API required by this operation.
                recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

                // Save the file in question and return the result.
                saveResult = recApi.saveFileStateChange(openFile);
                return saveResult;
            }
            else
            {
                editorSession.removeFile(fileId);
                editorSession.removeOutputState(fileId);
                saveEditorSession(editorSession);
                return null;
            }
        }
        else throw new Exception("Specified file " + fileId + "  is not open in current editor session: " + editorSessionId);
    }

    public T8DataFileUpdate updateFile(String editorSessionId, T8DataFileUpdate fileUpdate) throws Exception
    {
        T8DataRecordEditorSession editorSession;
        DataRecord openFile;
        String fileId;

        // Get the specified editor session and file.
        fileId = fileUpdate.getFileId();
        editorSession = getEditorSession(editorSessionId);
        openFile = editorSession.getFile(fileId);
        if (openFile != null)
        {
            T8DataRecordAccessHandler accessHandler;
            T8DataFileUpdate outputUpdate;
            T8DataRecordApi recApi;
            DataRecord previousOutputState;
            DataRecord outputState;

            // Get the API required by this operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

            // Apply the update to the file.
            applyUpdate(editorSessionId, recApi, openFile, fileUpdate);

            // Execute the dynamic access layer (if one is specified) on the open file.
            accessHandler = recApi.getAccessHandler(openFile);
            if (accessHandler != null)
            {
                accessHandler.refreshDynamicState(openFile);
            }

            // Make a copy of the local file and then remove all non-visible (hidden) data from it.
            outputState = openFile.copy();
            removeHiddenData(outputState);

            // Identify the updates that were applied to the output state and return them to the client.
            previousOutputState = editorSession.getOutputState(fileId);
            outputUpdate = getFileUpdate(fileUpdate, previousOutputState, outputState);
            outputUpdate.remove(fileUpdate);

            // Save the current state of the editor session (which contains the updated open file).
            editorSession.setOutputState(outputState);
            saveEditorSession(editorSession);

            // Return the output update (which may be null if not applicable).
            return outputUpdate;
        }
        else throw new Exception("Specified file " + fileId + "  is not open in current editor session: " + editorSessionId);
    }

    public List<RecordValue> retrieveValueSuggestions(String editorSessionId, String fileId, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, String searchPrefix, int pageOffset, int pageSize) throws Exception
    {
        T8DataRecordEditorSession editorSession;
        DataRecord openFile;

        // Get the specified editor session and file.
        editorSession = getEditorSession(editorSessionId);
        openFile = editorSession.getFile(fileId);
        if (openFile != null)
        {
            T8DataRecordApi recApi;

            // Get the API required by this operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

            // Return the suggested values.
            return recApi.retrieveValueSuggestions(openFile, targetRecordId, targetPropertyId, targetFieldId, targetDataTypeId, searchPrefix, pageOffset, pageSize);
        }
        else throw new Exception("Specified file " + fileId + "  is not open in current editor session: " + editorSessionId);
    }

    public List<RecordValue> retrieveValueOptions(String editorSessionId, String fileId, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, String searchTerms, List<T8ConceptElementType> searchElements, int pageOffset, int pageSize) throws Exception
    {
        T8DataRecordEditorSession editorSession;
        DataRecord openFile;

        // Get the specified editor session and file.
        editorSession = getEditorSession(editorSessionId);
        openFile = editorSession.getFile(fileId);
        if (openFile != null)
        {
            T8DataRecordApi recApi;

            // Get the API required by this operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

            // Return the value options.
            return recApi.retrieveValueOptions(openFile, targetRecordId, targetPropertyId, targetFieldId, targetDataTypeId, searchTerms, searchElements, pageOffset, pageSize);
        }
        else throw new Exception("Specified file " + fileId + "  is not open in current editor session: " + editorSessionId);
    }

    private void applyUpdate(String editorSessionId, T8DataRecordApi recApi, DataRecord dataFile, T8DataFileUpdate fileUpdate) throws Exception
    {
        // First apply the value updates.
        for (T8DataFileValueUpdate valueUpdate : fileUpdate.getValueUpdates())
        {
            DataRecord targetRecord;
            String targetRecordId;

            // Get the target record where the update is to be applied.
            targetRecordId = valueUpdate.getRecordId();
            targetRecord = dataFile.findDataRecord(targetRecordId);
            if (targetRecord != null)
            {
                RecordProperty targetProperty;
                String targetPropertyId;
                String targetFieldId;

                // Get the target property and/or field where the update is to be applied.
                targetPropertyId = valueUpdate.getPropertyId();
                targetFieldId = valueUpdate.getFieldId();
                targetProperty = targetRecord.getOrAddRecordProperty(targetPropertyId);
                if (targetFieldId == null)
                {
                    targetProperty.setContent(valueUpdate.getNewValue());
                }
                else
                {
                    FieldContent fieldContent;
                    Field targetField;

                    // Get the current field and its content.
                    targetField = targetProperty.getOrAddField(targetFieldId);
                    fieldContent = targetField.getContent();

                    // Update the field content and set it on the field.
                    fieldContent.setValue(valueUpdate.getNewValue());
                    targetField.setContent(fieldContent);
                }

                // Remove unreferenced sub-records.
                targetRecord.removeUnreferencedSubRecords();
            }
            else throw new RuntimeException("Target record not found: " + targetRecordId);
        }

        // Secondly apply new record creations.
        for (T8DataFileRecordCreation recordCreation : fileUpdate.getRecordCreations())
        {
            String targetRecordId;
            String targetPropertyId;
            String targetFieldId;
            String drInstanceId;
            DataRecord newRecord;

            targetRecordId = recordCreation.getRecordId();
            targetPropertyId = recordCreation.getPropertyId();
            targetFieldId = recordCreation.getFieldId();
            drInstanceId = recordCreation.getDrInstanceId();
            newRecord = recApi.createRecord(dataFile, targetRecordId, targetPropertyId, targetFieldId, drInstanceId, sessionContext.getContentLanguageIdentifier(), true, true);
            newRecord.fill(true); // We do this to allow easier binding in JS.
            recordCreation.setNewRecordId(newRecord.getID());
        }

        // Thirdly apply all new file attachments.
        if (fileUpdate.getNewAttachments().size() > 0)
        {
            T8FileManager fileManager;

            fileManager = serverContext.getFileManager();
            fileManager.openFileContext(context, editorSessionId, null, FILE_CONTEXT_EXPIRATION_TIME);
            for (T8DataFileNewAttachment newAttachment : fileUpdate.getNewAttachments())
            {
                DataRecord targetRecord;
                String newAttachmentId;
                String targetRecordId;
                String targetPropertyId;
                String targetFieldId;
                String temporaryFilepath;
                byte[] fileData;

                // Get the details of the newly added attachment.
                targetRecordId = newAttachment.getRecordId();
                targetPropertyId = newAttachment.getPropertyId();
                targetFieldId = newAttachment.getFieldId();
                fileData = newAttachment.getFileData();
                newAttachmentId = T8IdentifierUtilities.createNewGUID();
                temporaryFilepath = newAttachmentId + "/" + newAttachment.getFilename();

                // Get the target record.
                targetRecord = dataFile.findDataRecord(targetRecordId);
                if (targetRecord != null)
                {
                    T8FileDetails temporaryFileDetails;
                    AttachmentList attachmentList;
                    RecordProperty targetProperty;

                    // Get the target property where the attachment will be added.
                    targetProperty = targetRecord.getOrAddRecordProperty(targetPropertyId);
                    if (targetFieldId != null)
                    {
                        Field targetField;

                        // Get the target field where the attachment will be added.
                        targetField = targetProperty.getOrAddField(targetFieldId);

                        // Get the target list where the attachment will be added or create it if it does not yet exist.
                        attachmentList = (AttachmentList)targetProperty.getRecordValue();
                        if (attachmentList == null)
                        {
                            attachmentList = (AttachmentList)RecordValue.createValue(targetField.getFieldValueRequirement());
                            targetField.setFieldValue(attachmentList);
                        }
                    }
                    else
                    {
                        // Get the target list where the attachment will be added or create it if it does not yet exist.
                        attachmentList = (AttachmentList)targetProperty.getRecordValue();
                        if (attachmentList == null)
                        {
                            attachmentList = (AttachmentList)RecordValue.createValue(targetProperty.getValueRequirement());
                            targetProperty.setRecordValue(attachmentList);
                        }
                    }

                    // Save the new attachment to the temporary file location.
                    fileManager.uploadFileData(context, editorSessionId, temporaryFilepath, fileData);
                    temporaryFileDetails = fileManager.getFileDetails(context, editorSessionId, temporaryFilepath);

                    // Add the details of the attachment (in the temporary location) to the attachment list.
                    attachmentList.addAttachment(newAttachmentId);
                    attachmentList.addAttachmentDetails(newAttachmentId, new T8AttachmentDetails(newAttachmentId, temporaryFileDetails, newAttachment.getFilename()));
                    System.out.println("Temporary attachment details: " + temporaryFileDetails);
                }
            }
        }
    }

    private T8DataFileUpdate getFileUpdate(T8DataFileUpdate inputFileUpdate, DataRecord fromState, DataRecord toState)
    {
        T8DataFileAlteration alteration;
        T8DataFileUpdate outputUpdate;

        // Create the output file update.
        outputUpdate = new T8DataFileUpdate(toState.getID());

        // Determine the alteration that occurred bewteen the two states of the data file.
        alteration = T8DataFileAlteration.getDataFileAlteration(fromState, toState);

        // Add all new records to the output update.
        for (DataRecord newRecord : alteration.getInsertedRecords())
        {
            T8DataFileNewRecord newRecordUpdate;

            // Create the new record update.
            newRecordUpdate = new T8DataFileNewRecord(null, newRecord);
            newRecordUpdate.setRecordId(newRecord.getParentRecordId());
            newRecordUpdate.setPropertyId(newRecord.getParentPropertyId());
            outputUpdate.addNewRecord(newRecordUpdate);

            // Try to find the new record in the input file update so that we can transfer the temporary id.
            for (T8DataFileRecordCreation creation : inputFileUpdate.getRecordCreations())
            {
                if (newRecord.getID().equals(creation.getNewRecordId()))
                {
                    // Transfer the temporary id used by the creation to the output update.
                    newRecordUpdate.setTemporaryId(creation.getTemporaryId());
                    newRecordUpdate.setRecordId(creation.getRecordId());
                    newRecordUpdate.setPropertyId(creation.getPropertyId());
                    newRecordUpdate.setFieldId(creation.getFieldId());
                    break;
                }
            }
        }

        // Add all updated values to the output update.
        for (DataRecord toStateRecord : alteration.getUpdatedRecords())
        {
            T8DataRecordAlteration recordAlteration;
            DataRecord fromStateRecord;

            fromStateRecord = fromState.findDataRecord(toStateRecord.getID());
            recordAlteration = T8DataRecordAlteration.getDataRecordAlteration(fromStateRecord, toStateRecord);

            // Add inserted properties and values.
            for (RecordProperty insertedProperty : recordAlteration.getInsertedProperties())
            {
                T8DataFileValueUpdate valueUpdate;
                RecordValue recordValue;
                String recordId;
                String propertyId;

                recordId = insertedProperty.getRecordID();
                propertyId = insertedProperty.getPropertyID();
                recordValue = insertedProperty.getRecordValue();
                valueUpdate = new T8DataFileValueUpdate(recordId, propertyId, null);
                valueUpdate.setNewValue(recordValue != null ? recordValue.getContent() : null);
                outputUpdate.addValueUpdate(valueUpdate);
            }

            // Add updated properties and values.
            for (RecordProperty updatedProperty : recordAlteration.getUpdatedProperties())
            {
                T8DataFileValueUpdate valueUpdate;
                RecordValue recordValue;
                String recordId;
                String propertyId;

                recordId = updatedProperty.getRecordID();
                propertyId = updatedProperty.getPropertyID();
                recordValue = updatedProperty.getRecordValue();
                valueUpdate = new T8DataFileValueUpdate(recordId, propertyId, null);
                valueUpdate.setNewValue(recordValue != null ? recordValue.getContent() : null);
                outputUpdate.addValueUpdate(valueUpdate);
            }

            // Add deleted properties.  We handle deleted properties as a value update (to null).
            for (RecordProperty deletedProperty : recordAlteration.getDeletedProperties())
            {
                T8DataFileValueUpdate valueUpdate;
                String recordId;
                String propertyId;

                recordId = deletedProperty.getRecordID();
                propertyId = deletedProperty.getPropertyID();
                valueUpdate = new T8DataFileValueUpdate(recordId, propertyId, null);
                valueUpdate.setNewValue(null);
                outputUpdate.addValueUpdate(valueUpdate);
            }
        }

        // Add all deleted records to the output update.
        for (DataRecord deletedRecord : alteration.getDeletedRecords())
        {
            T8DataFileRecordDeletion deletion;
            DataRecord parentRecord;
            String deletedRecordId;

            deletedRecordId = deletedRecord.getId();
            parentRecord = deletedRecord.getParentRecord();
            deletion = new T8DataFileRecordDeletion();
            deletion.setRecordId(parentRecord.getId());
            deletion.setPropertyId(parentRecord.getPropertyContainingReference(deletedRecordId).getId());
            deletion.setSubRecordId(deletedRecordId);
            outputUpdate.addRecordDeletion(deletion);
        }

        // Add all access updates.
        for (DataRecord toStateRecord : toState.getDataFileRecords())
        {
            DataRecord fromStateRecord;
            String recordId;

            recordId = toStateRecord.getId();
            fromStateRecord = fromState.findDataRecord(recordId);
            if (fromStateRecord != null)
            {
                T8DataRecordAccessUpdate recordAccessUpdate;

                // Get the access updates that occurred on the record between the from- and to-states.
                recordAccessUpdate = T8DataRecordAccessUpdate.getAccessUpdate(fromStateRecord.getAccessLayer(), toStateRecord.getAccessLayer());

                // Check for record acccess update.
                if (recordAccessUpdate.isRecordAccessUpdated())
                {
                    // Todo: Add record access update to output file update.
                }

                // Add all property access updates.
                for (PropertyAccessLayer updatedProperty : recordAccessUpdate.getUpdatedPropertyAccess())
                {
                    T8DataFilePropertyAccessUpdate update;

                    update = new T8DataFilePropertyAccessUpdate(recordAccessUpdate.getRecordId(), updatedProperty.getPropertyId());
                    update.setVisible(updatedProperty.isVisible());
                    update.setEditable(updatedProperty.isEditable());
                    update.setFftEditable(updatedProperty.isFftEditable());
                    update.setRequired(updatedProperty.isRequired());
                    update.setInsertEnabled(updatedProperty.isInsertEnabled());
                    update.setUpdateEnabled(updatedProperty.isUpdateEnabled());
                    update.setDeleteEnabled(updatedProperty.isDeleteEnabled());
                    outputUpdate.addPropertyAccessUpdate(update);
                }
            }
        }

        // Return the final output update.
        return outputUpdate;
    }

    private void removeHiddenData(DataRecord dataFile)
    {
        // Check all record visibility.
        for (DataRecord record : dataFile.getDataRecords())
        {
            DataRecordAccessLayer recordAccess;

            recordAccess = record.getAccessLayer();
            if (!recordAccess.isVisible())
            {
                // The record is not visible so remove it entirely from all referrers.
                record.removeFromReferrerRecords(true);
            }
            else
            {
                // Check section visibility in this record.
                for (SectionAccessLayer sectionAccess : recordAccess.getSectionAccessLayers())
                {
                    if (!sectionAccess.isVisible())
                    {
                        // The section is not visible so remove it from the record.
                        record.removeRecordSection(sectionAccess.getSectionID());
                    }
                    else
                    {
                        // Check property visibility in this section.
                        for (PropertyAccessLayer propertyAccess : recordAccess.getPropertyAccessLayers())
                        {
                            if (!propertyAccess.isVisible())
                            {
                                // The property is not visible so remove it from the section.
                                record.removeRecordProperty(propertyAccess.getPropertyId());
                            }
                        }
                    }
                }
            }
        }
    }
}
