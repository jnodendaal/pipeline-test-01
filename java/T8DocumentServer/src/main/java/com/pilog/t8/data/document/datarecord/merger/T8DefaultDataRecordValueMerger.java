package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger.MergeConflictPolicy;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DefaultDataRecordValueMergerDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordValueMerger implements T8DataRecordValueMerger
{
    private final T8DataRecordMergeContext mergeContext;
    private final MergeConflictPolicy conflictPolicy;
    private final Set<DataRecord> updatedDataRecords;

    public T8DefaultDataRecordValueMerger(T8DataRecordMergeContext mergeContext, T8DataRecordMergeConfiguration config)
    {
        this.mergeContext = mergeContext;
        this.conflictPolicy = config.getConflictPolicy();
        this.updatedDataRecords = new HashSet<DataRecord>();
    }

    public T8DefaultDataRecordValueMerger(T8DefaultDataRecordValueMergerDefinition definition, T8DataRecordMergeContext mergeContext)
    {
        this.mergeContext = mergeContext;
        this.conflictPolicy = definition.getMergeConflictPolicy();
        this.updatedDataRecords = new HashSet<DataRecord>();
    }

    @Override
    public void reset()
    {
        updatedDataRecords.clear();
    }

    @Override
    public Map<String, String> getRecordIdMapping()
    {
        return new HashMap<>();
    }

    @Override
    public Map<String, String> getAttachmentIdentifierMapping()
    {
        return new HashMap<String, String>();
    }

    @Override
    public Set<DataRecord> getNewDataRecordSet()
    {
        return new HashSet<DataRecord>();
    }

    @Override
    public Set<DataRecord> getUpdatedDataRecordSet()
    {
        return new HashSet<DataRecord>(updatedDataRecords);
    }

    @Override
    public boolean isApplicable(RecordValue destinationValue, RecordValue sourceValue)
    {
        RequirementType requirementType;

        // Get the destination requirement type.
        requirementType = destinationValue.getValueRequirement().getRequirementType();
        if (requirementType == RequirementType.DOCUMENT_REFERENCE)
        {
            return false;
        }
        else if (requirementType == RequirementType.ATTACHMENT)
        {
            return false;
        }
        else return true;
    }

    @Override
    public void mergeRecordValues(RecordValue destinationValue, RecordValue sourceValue)
    {
        RequirementType requirementType;

        // Get the source requirement type.
        requirementType = sourceValue.getValueRequirement().getRequirementType();
        if (requirementType == RequirementType.DOCUMENT_REFERENCE)
        {
            throw new IllegalArgumentException("Cannot merge document reference value using default value merger: " + destinationValue);
        }
        else if (requirementType == RequirementType.ATTACHMENT)
        {
            throw new IllegalArgumentException("Cannot merge attachment value using default value merger: " + destinationValue);
        }
        else if (requirementType == RequirementType.COMPOSITE_TYPE)
        {
            List<RecordValue> sourceFields;

            sourceFields = sourceValue.getSubValues();
            for (RecordValue sourceField : sourceFields)
            {
                RecordValue matchingDestinationField;
                String sourceFieldID;

                sourceFieldID = sourceField.getValueRequirement().getValue();
                matchingDestinationField = destinationValue.getSubValue(RequirementType.FIELD_TYPE, sourceFieldID, null);
                if (matchingDestinationField == null)
                {
                    ValueRequirement destinationFieldRequirement;

                    destinationFieldRequirement = destinationValue.getValueRequirement().getEquivalentSubRequirement(sourceField.getValueRequirement());
                    matchingDestinationField = new Field(destinationFieldRequirement);
                    destinationValue.setSubValue(matchingDestinationField);

                    // Add the document to the updated collection.
                    updatedDataRecords.add(destinationValue.getParentDataRecord());
                }

                // Now merge the two fields.
                mergeRecordValues(matchingDestinationField, sourceField);
            }
        }
        else if (requirementType == RequirementType.MEASURED_NUMBER)
        {
            MeasuredNumber sourceMeasuredNumber;
            String sourceNumber;
            String sourceUomId;
            String sourceQomId;

            sourceMeasuredNumber = (MeasuredNumber)sourceValue;
            sourceNumber = sourceMeasuredNumber.getNumber();
            sourceUomId = sourceMeasuredNumber.getUomId();
            sourceQomId = sourceMeasuredNumber.getQomId();

            // Only proceed if we have a number (measured numbers are not valid without one).
            if (!Strings.isNullOrEmpty(sourceNumber))
            {
                MeasuredNumber destinationMeasuredNumber;
                String destinationNumber;

                // Get the current destination number.
                destinationMeasuredNumber = (MeasuredNumber)destinationValue;
                destinationNumber = destinationMeasuredNumber.getNumber();
                if (!Strings.isNullOrEmpty(destinationNumber))
                {
                    String destinationUomId;
                    String destinationQomId;

                    // Get the UOM and QOM id's of the destination value.
                    destinationUomId = destinationMeasuredNumber.getUomId();
                    destinationQomId = destinationMeasuredNumber.getQomId();

                    // Only merge source to destination if the source value is different from the destination.
                    if ((!Objects.equals(sourceNumber, destinationNumber)) || (!Objects.equals(sourceUomId, destinationUomId)) || (!Objects.equals(sourceQomId, destinationQomId)))
                    {
                        // Add the document to the updated collection.
                        updatedDataRecords.add(destinationValue.getParentDataRecord());
                        if (conflictPolicy == T8DefaultDataRecordMerger.MergeConflictPolicy.USE_SOURCE_DATA)
                        {
                            // Add the source data to the destination overwriting the existing destination value.
                            destinationMeasuredNumber.setNumber(sourceNumber);
                            destinationMeasuredNumber.setUomId(sourceUomId);
                            destinationMeasuredNumber.setQomId(sourceQomId);
                        }
                        else if (conflictPolicy == T8DefaultDataRecordMerger.MergeConflictPolicy.USE_SOURCE_DATA_ADD_FFT)
                        {
                            T8DataRecordValueStringGenerator fftGenerator;
                            StringBuilder fft;

                            // Add the destination value to the destination FFT and then add the source value to the destination.
                            fftGenerator = mergeContext.getFFTGenerator();
                            fft = fftGenerator.generateValueString(destinationValue);
                            destinationMeasuredNumber.getParentRecordProperty().appendFFT(fft != null ? fft.toString() : null);
                            destinationMeasuredNumber.setNumber(sourceNumber);
                            destinationMeasuredNumber.setUomId(sourceUomId);
                            destinationMeasuredNumber.setQomId(sourceQomId);
                        }
                        else if (conflictPolicy == T8DefaultDataRecordMerger.MergeConflictPolicy.USE_DESTINATION_DATA_ADD_FFT)
                        {
                            T8DataRecordValueStringGenerator fftGenerator;
                            StringBuilder fft;

                            fftGenerator = mergeContext.getFFTGenerator();
                            fft = fftGenerator.generateValueString(sourceValue);
                            destinationMeasuredNumber.getParentRecordProperty().appendFFT(fft != null ? fft.toString() : null);
                        }
                    }
                }
                else
                {
                    // No conflict, so just add all source values to the destination.
                    destinationMeasuredNumber.setNumber(sourceNumber);
                    destinationMeasuredNumber.setUomId(sourceUomId);
                    destinationMeasuredNumber.setQomId(sourceQomId);

                    // Add the document to the updated collection.
                    updatedDataRecords.add(destinationValue.getParentDataRecord());
                }
            }
        }
        else if (requirementType == RequirementType.MEASURED_RANGE)
        {
            MeasuredRange sourceMeasuredRange;
            String sourceLowerBoundNumber;
            String sourceUpperBoundNumber;
            String sourceUomId;
            String sourceQomId;

            sourceMeasuredRange = (MeasuredRange)sourceValue;
            sourceLowerBoundNumber = sourceMeasuredRange.getLowerBoundNumber();
            sourceUpperBoundNumber = sourceMeasuredRange.getUpperBoundNumber();
            sourceUomId = sourceMeasuredRange.getUomId();
            sourceQomId = sourceMeasuredRange.getQomId();

            // Only proceed if we have a lower bound and upper bound number (measured ranges are not valid without both).
            if (!Strings.isNullOrEmpty(sourceLowerBoundNumber) && !Strings.isNullOrEmpty(sourceUpperBoundNumber))
            {
                MeasuredRange destinationMeasuredRange;
                String destinationLowerBoundNumber;
                String destinationUpperBoundNumber;

                // Get the current destination lower and upper bound numbers.
                destinationMeasuredRange = (MeasuredRange)destinationValue;
                destinationLowerBoundNumber = destinationMeasuredRange.getLowerBoundNumber();
                destinationUpperBoundNumber = destinationMeasuredRange.getUpperBoundNumber();
                if (!Strings.isNullOrEmpty(destinationLowerBoundNumber) && !Strings.isNullOrEmpty(destinationUpperBoundNumber))
                {
                    String destinationUomId;
                    String destinationQomId;

                    // Get the UOM and QOM id's of the destination value.
                    destinationUomId = destinationMeasuredRange.getUomId();
                    destinationQomId = destinationMeasuredRange.getQomId();

                    // Only merge source to destination if the source value is different from the destination.
                    if ((!Objects.equals(sourceLowerBoundNumber, destinationLowerBoundNumber)) || (!Objects.equals(sourceUpperBoundNumber, destinationUpperBoundNumber)) || (!Objects.equals(sourceUomId, destinationUomId)) || (!Objects.equals(sourceQomId, destinationQomId)))
                    {
                        // Add the document to the updated collection.
                        updatedDataRecords.add(destinationValue.getParentDataRecord());
                        if (conflictPolicy == T8DefaultDataRecordMerger.MergeConflictPolicy.USE_SOURCE_DATA)
                        {
                            // Add the source data to the destination overwriting the existing destination value.
                            destinationMeasuredRange.setLowerBoundNumber(sourceLowerBoundNumber);
                            destinationMeasuredRange.setUpperBoundNumber(sourceUpperBoundNumber);
                            destinationMeasuredRange.setUomId(sourceUomId);
                            destinationMeasuredRange.setQomId(sourceQomId);
                        }
                        else if (conflictPolicy == T8DefaultDataRecordMerger.MergeConflictPolicy.USE_SOURCE_DATA_ADD_FFT)
                        {
                            T8DataRecordValueStringGenerator fftGenerator;
                            StringBuilder fft;

                            // Add the destination value to the destination FFT and then add the source value to the destination.
                            fftGenerator = mergeContext.getFFTGenerator();
                            fft = fftGenerator.generateValueString(destinationValue);
                            destinationMeasuredRange.getParentRecordProperty().appendFFT(fft != null ? fft.toString() : null);
                            destinationMeasuredRange.setLowerBoundNumber(sourceLowerBoundNumber);
                            destinationMeasuredRange.setUpperBoundNumber(sourceUpperBoundNumber);
                            destinationMeasuredRange.setUomId(sourceUomId);
                            destinationMeasuredRange.setQomId(sourceQomId);
                        }
                        else if (conflictPolicy == T8DefaultDataRecordMerger.MergeConflictPolicy.USE_DESTINATION_DATA_ADD_FFT)
                        {
                            T8DataRecordValueStringGenerator fftGenerator;
                            StringBuilder fft;

                            fftGenerator = mergeContext.getFFTGenerator();
                            fft = fftGenerator.generateValueString(sourceValue);
                            destinationMeasuredRange.getParentRecordProperty().appendFFT(fft != null ? fft.toString() : null);
                        }
                    }
                }
                else
                {
                    // No conflict, so just add all source values to the destination.
                    destinationMeasuredRange.setLowerBoundNumber(sourceLowerBoundNumber);
                    destinationMeasuredRange.setUpperBoundNumber(sourceUpperBoundNumber);
                    destinationMeasuredRange.setUomId(sourceUomId);
                    destinationMeasuredRange.setQomId(sourceQomId);

                    // Add the document to the updated collection.
                    updatedDataRecords.add(destinationValue.getParentDataRecord());
                }
            }
        }
        else // This last part is intended to catch all simple types.  All complex types must be handled in preceding if-clauses.
        {
            String sourceValueString;

            // Copy the value String.
            sourceValueString = sourceValue.getValue();
            if (!Strings.isNullOrEmpty(sourceValueString))
            {
                String destinationValueString;

                // Add the document to the updated collection.
                updatedDataRecords.add(destinationValue.getParentDataRecord());

                destinationValueString = destinationValue.getValue();
                if (!Strings.isNullOrEmpty(destinationValueString))
                {
                    // Only merge source to destination if the source value is different from the destination.
                    if (!Objects.equals(sourceValueString, destinationValueString))
                    {
                        if (conflictPolicy == T8DefaultDataRecordMerger.MergeConflictPolicy.USE_SOURCE_DATA)
                        {
                            // Add the source data to the destination overwriting the existing destination value.
                            destinationValue.setValue(sourceValueString);
                        }
                        else if (conflictPolicy == T8DefaultDataRecordMerger.MergeConflictPolicy.USE_SOURCE_DATA_ADD_FFT)
                        {
                            T8DataRecordValueStringGenerator fftGenerator;
                            StringBuilder fft;

                            // Add the destination value to the destination FFT and then add the source value to the destination.
                            fftGenerator = mergeContext.getFFTGenerator();
                            fft = fftGenerator.generateValueString(destinationValue);
                            destinationValue.getParentRecordProperty().appendFFT(fft != null ? fft.toString() : null);
                            destinationValue.setValue(sourceValueString);
                        }
                        else if (conflictPolicy == T8DefaultDataRecordMerger.MergeConflictPolicy.USE_DESTINATION_DATA_ADD_FFT)
                        {
                            T8DataRecordValueStringGenerator fftGenerator;
                            StringBuilder fft;

                            fftGenerator = mergeContext.getFFTGenerator();
                            fft = fftGenerator.generateValueString(sourceValue);
                            destinationValue.getParentRecordProperty().appendFFT(fft != null ? fft.toString() : null);
                        }
                    }
                }
                else // The destination value is empty, so just add the the source value to it.
                {
                    destinationValue.setValue(sourceValueString);
                }
            }
        }
    }
}
