package com.pilog.t8.data.org;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.cache.T8TerminologyCache;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8CachedOrganizationTerminologyProvider implements TerminologyProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8CachedOrganizationTerminologyProvider.class);

    private final T8TerminologyCache cache;
    private final TerminologyProvider terminologyProvider;
    private final String rootOrgId;
    private String defaultLanguageId;

    public T8CachedOrganizationTerminologyProvider(T8ServerContext serverContext, String rootOrgId, TerminologyProvider terminologyProvider)
    {
        this.rootOrgId = rootOrgId;
        this.cache = serverContext.getDataManager().getTerminologyCache(rootOrgId);
        this.terminologyProvider = terminologyProvider;
    }

    @Override
    public void setLanguage(String languageId)
    {
        defaultLanguageId = languageId;
        terminologyProvider.setLanguage(languageId);
    }

    @Override
    public String getCode(String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(null, conceptID);
        return terminology != null ? terminology.getCode() : null;
    }

    @Override
    public String getTerm(String languageID, String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageID, conceptID);
        return terminology != null ? terminology.getTerm() : null;
    }

    @Override
    public String getAbbreviation(String languageID, String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageID, conceptID);
        return terminology != null ? terminology.getAbbreviation() : null;
    }

    @Override
    public String getDefinition(String languageID, String conceptID)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageID, conceptID);
        return terminology != null ? terminology.getDefinition() : null;
    }

    @Override
    public T8ConceptTerminology getTerminology(String languageID, String conceptID)
    {
        T8ConceptTerminology conceptTerminology;
        String resolvedLanguageID;

        // Make sure the input parameters are valid.
        if (conceptID == null) throw new IllegalArgumentException("Null Concept ID.");

        // Resolve the language ID to use.
        resolvedLanguageID = (languageID != null ? languageID : defaultLanguageId);
        conceptTerminology = (T8ConceptTerminology)cache.get(resolvedLanguageID, conceptID);
        if (conceptTerminology != null)
        {
            return conceptTerminology;
        }
        else
        {
            // Retrieve the terminology, add it to the cache and return it.
            conceptTerminology = terminologyProvider.getTerminology(resolvedLanguageID, conceptID);
            if (conceptTerminology != null)
            {
                cache.put(conceptTerminology);
                return conceptTerminology;
            }
            else
            {
                LOGGER.log("WARNING invalid terminology request: Language: " + languageID + ", Concept: " + conceptID);
                cache.put(createEmptyTerminology(languageID, conceptID));
                return null;
            }
        }
    }

    @Override
    public T8ConceptTerminologyList getTerminology(Collection<String> languageIds, Collection<String> conceptIds)
    {
        T8ConceptTerminologyList resultList;
        Set<String> languageIdSet;

        // First make sure we some valid languages set.
        if (languageIds == null)
        {
            if (defaultLanguageId == null) throw new RuntimeException("No terminology languages specified and no default language set.");
            else
            {
                languageIdSet = new HashSet<String>();
                languageIdSet.add(defaultLanguageId);
            }
        }
        else languageIdSet = new HashSet<String>(languageIds);

        // Cache all required terminology.
        cacheTerminology(languageIdSet, conceptIds);

        // Get all the terminologies we need.
        resultList = new T8ConceptTerminologyList();
        for (String conceptId : conceptIds)
        {
            for (String languageId : languageIdSet)
            {
                resultList.add((T8ConceptTerminology)cache.get(languageId, conceptId));
            }
        }

        // Return the results.
        return resultList;
    }

    private void cacheTerminology(Collection<String> languageIds, Collection<String> conceptIds)
    {
        List<T8ConceptTerminology> terminologyList;
        Set<String> languageIdSet;
        Set<String> conceptsToCache;

        // First make sure we some valid languages set.
        if (languageIds == null)
        {
            if (defaultLanguageId == null) throw new RuntimeException("No languages specified for caching and no default language set.");
            else
            {
                languageIdSet = new HashSet<String>();
                languageIdSet.add(defaultLanguageId);
            }
        }
        else languageIdSet = new HashSet<String>(languageIds);

        // Create a list of concepts for which the required terminology has not already been cached.
        conceptsToCache = new HashSet<String>();
        for (String conceptId : conceptIds)
        {
            for (String languageId : languageIdSet)
            {
                if (!cache.containsKey(languageId, conceptId))
                {
                    conceptsToCache.add(conceptId);
                }
            }
        }

        // Now retrieve the rest.
        terminologyList = terminologyProvider.getTerminology(languageIdSet, conceptsToCache);
        for (T8ConceptTerminology retrievedTerminology : terminologyList)
        {
            // Add the retrieved terminology to the cache.
            cache.put(retrievedTerminology);
        }

        // We have to cache dummy objects for all terminology that we could not retrieve to ensure that the retrieval is not performed again.
        for (String conceptID : conceptIds)
        {
            for (String languageID : languageIdSet)
            {
                if (!cache.containsKey(languageID, conceptID))
                {
                    LOGGER.log("WARNING invalid terminology request: Language: " + languageID + ", Concept: " + conceptID);
                    cache.put(createEmptyTerminology(languageID, conceptID));
                }
            }
        }
    }

    private T8ConceptTerminology createEmptyTerminology(String languageId, String conceptId)
    {
        T8ConceptTerminology emptyTerminology;

        emptyTerminology = new T8ConceptTerminology(T8OntologyConceptType.VALUE, conceptId);
        emptyTerminology.setLanguageId(languageId);
        return emptyTerminology;
    }
}

