package com.pilog.t8.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.alteration.T8DataAlterationPackageIndexChange.IndexChangeType;
import com.pilog.t8.utilities.cache.LRUCache;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.alteration.T8DataAlterationHandler;
import com.pilog.t8.data.alteration.T8DataAlterationHandlerFactory;
import com.pilog.t8.data.alteration.T8DataAlterationImpact;
import com.pilog.t8.data.alteration.T8DataAlterationPackageIndex;
import com.pilog.t8.data.alteration.T8DataAlterationPackageIndexChange;
import com.pilog.t8.data.alteration.T8DataAlterationPackageIndexChanges;
import com.pilog.t8.data.alteration.T8DataAlterationPackageIndexEntry;
import com.pilog.t8.data.alteration.T8DataAlterationDataHandler;
import com.pilog.t8.data.alteration.T8DataAlterationPackage;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class implements the api for all maintenance and applications of {@code T8DataAlteration} instances.
 * @author Bouwer du Preez
 */
public class T8DataAlterationApi implements T8Api, T8PerformanceStatisticsProvider
{
    public static final String API_IDENTIFIER = "@API_DATA_ALTERATION";

    private final T8DataTransaction tx;
    private final T8DataSession ds;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final Map<String, T8DataAlterationPackageIndex> alterationIndexCache; // Use to cache alteration indices since they will be retrieved and used very often.
    private final T8DataAlterationHandlerFactory handlerFactory; // Factory used for construction of required {@code T8DataAlterationHandle} objects.
    private final T8DataAlterationDataHandler alterationDataHandler;
    private T8PerformanceStatistics stats; // Provides support for logging of performance statistics.

    public T8DataAlterationApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.ds = tx.getDataSession();
        this.context = tx.getContext();
        this.serverContext = context.getServerContext();
        this.alterationDataHandler = new T8DataAlterationDataHandler(serverContext);
        this.alterationIndexCache = new LRUCache<String, T8DataAlterationPackageIndex>(10);
        this.handlerFactory = new T8DataAlterationHandlerFactory(context);
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8DataAlterationPackageIndex getAlterationPackageIndex(String packageIid) throws Exception
    {
        if (alterationIndexCache.containsKey(packageIid))
        {
            return alterationIndexCache.get(packageIid);
        }
        else
        {
            T8DataAlterationPackageIndex index;

            index = alterationDataHandler.retrieveDataAlterationIndex(tx, packageIid);
            alterationIndexCache.put(packageIid, index);
            return index;
        }
    }

    /**
     * Returns the {@code T8DataAlterationHandle} applicable to the specified alteration instance step.
     * @param tx The transaction to use for this operation.
     * @param packageIid The alteration instance id for which to fetch a handler.
     * @param step The alteration instance step for which to fetch a handler.
     * @return The {@code T8DataAlterationHandle} capable of handling the specified alteration instance step.
     * @throws Exception
     */
    private T8DataAlterationHandler getAlterationHandler(String packageIid, int step) throws Exception
    {
        T8DataAlterationPackageIndex index;

        index = getAlterationPackageIndex(packageIid);
        if (index != null)
        {
            T8DataAlterationPackageIndexEntry entry;

            entry = index.getEntry(step);
            if (entry != null)
            {
                return handlerFactory.getAlterationHandler(entry.getAlterationId());
            }
            else throw new Exception("Alteration step not found: " + step  + "(Alteration:" + packageIid + ")");
        }
        else throw new Exception("Alteration not found: " + packageIid);
    }

    private void updateAlterationIndex(T8DataAlterationPackageIndexChanges indexChanges) throws Exception
    {
        // Update the alteration index.
        alterationDataHandler.updateAlterationIndex(tx, indexChanges);

        // Update index steps in each affected alteration entry.
        for (T8DataAlterationPackageIndexChange change : indexChanges.getChanges())
        {
            if (change.getChangeType() == IndexChangeType.UPDATE)
            {
                T8DataAlterationPackageIndexEntry entry;
                T8DataAlterationHandler handler;
                String alterationID;
                String dataType;

                entry = change.getEntry();
                alterationID = entry.getPackageIid();
                dataType = entry.getAlterationId();

                // Get the applicable handler and adjust its steps if required.
                if (change.getStepChange() != 0)
                {
                    handler = handlerFactory.getAlterationHandler(dataType);
                    handler.adjustSteps(tx, alterationID, change.getStartStep(), -1, change.getStepChange());
                }
            }
            else if (change.getChangeType() == IndexChangeType.DELETE)
            {
                T8DataAlterationPackageIndexEntry entry;
                T8DataAlterationDataHandler handler;

                entry = change.getEntry();
                handler = new T8DataAlterationDataHandler(serverContext);

                // Delete alteration package index entry.
                handler.deleteAlterationPackageIndex(tx, entry.getPackageIid(), entry.getEntryIid());
            }
        }
    }

    /**
     * Deletes the specified data alteration.
     * @param packageIid The unique ID of the alteration package to delete from.
     * @param step The alteration step to delete.  If the value is negative, all
     * steps in the specified package will be deleted.
     * @return The updated alteration package index.
     * @throws Exception
     */
    public T8DataAlterationPackageIndex deleteAlteration(String packageIid, int step) throws Exception
    {
        T8DataAlterationPackageIndex index;

        // Get the alteration index.
        index = getAlterationPackageIndex(packageIid);
        if (index != null)
        {
            if (step < 0)
            {
                T8DataAlterationPackageIndexChanges changes;

                // Loop over all entries in the index, deleting each one completely.
                changes = new T8DataAlterationPackageIndexChanges();
                for (T8DataAlterationPackageIndexEntry entry : index.getEntries())
                {
                    T8DataAlterationHandler handler;

                    // Get the applicable handler and delete all alterations in the range.
                    handler = handlerFactory.getAlterationHandler(entry.getAlterationId());
                    handler.deleteAlteration(tx, packageIid, -1);

                    // Delete the entry from the index and dd the entry deletion to the change list.
                    changes.addEntryDeletion(entry);
                }

                // Clear all entries from the index.
                index.clearEntries();

                // Update the alteration index to reflect the deletion of all entries.
                updateAlterationIndex(changes);

                // Return a copy of the updated index.
                return index.copy();
            }
            else
            {
                T8DataAlterationPackageIndexEntry entry;

                // Get the entry in the index, from which the specified step will be deleted.
                entry = index.getEntry(step);
                if (entry != null)
                {
                    T8DataAlterationPackageIndexChanges changes;
                    T8DataAlterationHandler handler;

                    // Get the applicable handler and delete the alteration data.
                    handler = handlerFactory.getAlterationHandler(entry.getAlterationId());
                    handler.deleteAlteration(tx, packageIid, step);

                    // Update the alteration index.
                    changes = index.deleteStep(step);
                    updateAlterationIndex(changes);

                    // Return a copy of the updated index.
                    return index.copy();
                }
                else throw new Exception("Alteration step not found: " + step  + " (Alteration:" + packageIid + ")");
            }
        }
        else throw new Exception("Alteration not found: " + packageIid);
    }

    /**
     * Deletes a data alteration package in the applicable alteration tables.
     * This method does not check existing data and performs a straight delete.
     * @param packageIid The data alteration packageIid to delete.
     * @throws Exception
     */
    public void deleteAlterationPackage(String packageIid) throws Exception
    {
        T8DataAlterationDataHandler handler;

        handler = new T8DataAlterationDataHandler(serverContext);

        // Delete the alterations in the package.
        deleteAlteration(packageIid, -1);

        // Delete the package
        handler.deleteAlterationPackage(tx, packageIid);
    }

    /**
     * Inserts the supplied data alteration in the applicable alteration tables.
     * This method does not check existing data and performs a straight insert.
     * @param alteration The data alteration to persist.
     * @return The updated alteration package index.
     * @throws Exception
     */
    public T8DataAlterationPackageIndex insertAlteration(T8DataAlteration alteration) throws Exception
    {
        T8DataAlterationPackageIndexChanges changes;
        T8DataAlterationHandler handler;
        T8DataAlterationPackageIndex index;
        String alterationId;
        String packageIid;
        int step;

        // Get the alteration index.
        step = alteration.getStep();
        alterationId = alteration.getAlterationId();
        packageIid = alteration.getPackageIid();
        index = getAlterationPackageIndex(packageIid);
        if (index == null)
        {
            // This is an entirely new alteration, so we create it from scratch.
            index = new T8DataAlterationPackageIndex(packageIid);
            alterationIndexCache.put(packageIid, index);
        }

        // Add a new entry to the alteration index.
        changes = index.insertStep(alterationId, step);
        updateAlterationIndex(changes);

        // Get the applicable handler and delete the alteration data.
        handler = handlerFactory.getAlterationHandler(alterationId);
        handler.insertAlteration(tx, alteration);

        // Return a copy of the updated index.
        return index.copy();
    }

    /**
     * Inserts a data alteration package in the applicable alteration tables.
     * This method does not check existing data and performs a straight insert.
     * @param alterationPackage The data alteration package to persist.
     * @throws Exception
     */
    public void insertAlterationPackage(T8DataAlterationPackage alterationPackage) throws Exception
    {
        T8DataAlterationDataHandler handler;

        handler = new T8DataAlterationDataHandler(serverContext);

        // Insert the new package.
        handler.insertAlterationPackage(tx, alterationPackage);
    }

    /**
     * Updates the supplied data alteration in the applicable tables.
     * @param alteration The alteration to persist.
     * @throws Exception
     */
    public void updateAlteration(T8DataAlteration alteration) throws Exception
    {
        T8DataAlterationHandler handler;
        String alterationIid;
        int step;

        step = alteration.getStep();
        alterationIid = alteration.getPackageIid();
        handler = getAlterationHandler(alterationIid, step);
        if (handler != null)
        {
            handler.updateAlteration(tx, alteration);
        }
        else throw new Exception("Alteration step not found: " + step  + " (Alteration:" + alterationIid + ")");
    }

    /**
     * Updates a data alteration package in the applicable alteration tables.
     * This method does not check existing data and performs a straight update.
     * @param alterationPackage The data alteration package to persist.
     * @throws Exception
     */
    public void updateAlterationPackage(T8DataAlterationPackage alterationPackage) throws Exception
    {
        T8DataAlterationDataHandler handler;

        handler = new T8DataAlterationDataHandler(serverContext);

        // Update the package.
        handler.updateAlterationPackage(tx, alterationPackage);
    }

    /**
     * Applies the outstanding data alterations deleting, inserting and updating
     * operational data as required for the whole package.  This method returns an impact report
     * indicating the data changes that were applied.
     * @param packageIid The id of the alteration package from which the specified alteration will be fetched for application.
     * @return The impact report, summarizing applied data changes.
     * @throws Exception
     */
    public List<T8DataAlterationImpact> applyAlteration(String packageIid) throws Exception
    {
        T8DataAlterationHandler handler;
        T8DataAlterationPackageIndex alterationIndex;
        List<T8DataAlterationImpact> alterationImpactList;

        alterationImpactList = new ArrayList<>();
        alterationIndex = getAlterationPackageIndex(packageIid);

        if (alterationIndex != null)
        {
            for (T8DataAlterationPackageIndexEntry entry : alterationIndex.getEntries())
            {
                for (int stepIndex = entry.getStep(); stepIndex < entry.getSize(); stepIndex++)
                {
                    handler = getAlterationHandler(packageIid, stepIndex);
                    if (handler != null)
                    {
                        T8DataAlteration alteration;

                        alteration = handler.retrieveAlteration(tx, packageIid, stepIndex);
                        alterationImpactList.add(handler.applyAlteration(tx, alteration));
                    }
                    else throw new Exception("Alteration step not found: " + stepIndex  + " (Package:" + packageIid + ")");
                }
            }
        }
        return alterationImpactList;
    }

    /**
     * Applies the specified data alteration deleting, inserting and updating
     * operational data as required.  This method returns an impact report
     * indicating the data changes that were applied.
     * @param packageIid The id of the alteration package from which the specified alteration will be fetched for application.
     * @param step The step index of the alteration to be applied.
     * @return The impact report, summarizing applied data changes.
     * @throws Exception
     */
    public T8DataAlterationImpact applyAlteration(String packageIid, int step) throws Exception
    {
        T8DataAlterationHandler handler;

        handler = getAlterationHandler(packageIid, step);
        if (handler != null)
        {
            T8DataAlteration alteration;

            alteration = handler.retrieveAlteration(tx, packageIid, step);
            return handler.applyAlteration(tx, alteration);
        }
        else throw new Exception("Alteration step not found: " + step  + " (Package:" + packageIid + ")");
    }

    /**
     * Applies the specified data alteration deleting, inserting and updating
     * operational data as required.  This method returns an impact report
     * indicating the data changes that were applied.
     * @param alteration The data alteration to apply.
     * @return The impact report, summarizing applied data changes.
     * @throws Exception
     */
    public T8DataAlterationImpact applyAlteration(T8DataAlteration alteration) throws Exception
    {
        T8DataAlterationHandler handler;
        String packageIid;
        int step;

        step = alteration.getStep();
        packageIid = alteration.getPackageIid();
        handler = getAlterationHandler(packageIid, step);
        if (handler != null)
        {
            return handler.applyAlteration(tx, alteration);
        }
        else throw new Exception("Alteration step not found: " + step  + " (Package:" + packageIid + ")");
    }

    /**
     * Retrieves the specified alteration from the persistence store.  The
     * alteration step specified will be used as a minimum value i.e. the next
     * alteration to return will have a step value equal to or greater than the
     * step value specified in order to accommodate skips in steps that could
     * potentially occur.
     * @param packageIid The unique instance id of the alteration to retrieve.
     * @param step The minimum step to retrieve.  The alteration returned will
     * have a step value equal to or greater than this value.  If this value is
     * less than or equal to zero, the first step of the alteration will be
     * retrieved.
     * @return The specified alteration retrieved from the persistence store or
     * null if no such alteration exists.
     * @throws Exception
     */
    public T8DataAlteration retrieveAlteration(String packageIid, int step) throws Exception
    {
        T8DataAlterationHandler handler;

        handler = getAlterationHandler(packageIid, step);
        if (handler != null)
        {
            return handler.retrieveAlteration(tx, packageIid, step);
        }
        else throw new Exception("Alteration step not found: " + step  + " (Alteration:" + packageIid + ")");
    }

    /**
     * @param packageIid The unique instance id of the alteration package to retrieve.
     * @return The specified alteration package retrieved from the persistence store or
     * null if no such alteration package exists.
     * @throws Exception
     */
    public T8DataAlterationPackage retrieveAlterationPackage(String packageIid) throws Exception
    {
        T8DataAlterationDataHandler handler;
        T8DataAlterationPackage alterationPackage;

        handler = new T8DataAlterationDataHandler(serverContext);

        // Insert the new package
        alterationPackage = handler.retrieveAlterationPackage(tx, packageIid);

        return alterationPackage;
    }

    /**
     * Retrieves the total number of steps in the specified package.
     * @param packageIid The id of the package for which to retrieve the size.
     * @return The size of- or number of steps in the specified package.
     * @throws Exception
     */
    public int retrieveAlterationPackageSize(String packageIid) throws Exception
    {
        T8DataAlterationPackageIndex index;

        index = getAlterationPackageIndex(packageIid);
        if (index != null)
        {
            return index.getSize();
        }
        else throw new Exception("Package not found: " + packageIid);
    }

    /**
     * Calculate the impact of the specified data alteration step.  If a negative step index is specified, the
     * impact of the entire package is calculated.
     * @param packageIid The id of the alteration package where the specified step is located.
     * @param step The index of the alteration step for which to calculate an impact analysis.  If negative, all steps in the package are included.
     * @return The impact of the specified step.
     */
    public T8DataAlterationImpact calculateDataAlterationImpact(String packageIid, int step) throws Exception
    {
        T8DataAlterationHandler handler;

        handler = getAlterationHandler(packageIid, step);
        if (handler != null)
        {
            T8DataAlteration alteration;

            alteration = handler.retrieveAlteration(tx, packageIid, step);
            return handler.getAlterationImpact(tx, alteration);
        }
        else throw new Exception("Alteration step not found: " + step  + " (Package:" + packageIid + ")");
    }
}
