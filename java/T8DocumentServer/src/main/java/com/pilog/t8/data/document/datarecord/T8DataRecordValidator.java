package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordValidator
{
    private final T8DataTransaction tx;
    private final T8DataRecordApi recApi;
    private final T8TerminologyApi trmApi;
    private final T8Context context;
    private final DataRecordProvider dataRecordProvider;
    private final TerminologyProvider terminologyProvider;
    private T8DataRecordAccessHandler accessHandler;

    public T8DataRecordValidator(T8DataTransaction tx)
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
        this.trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
        this.dataRecordProvider = recApi.getRecordProvider();
        this.terminologyProvider = trmApi.getTerminologyProvider();
    }

    public void setAccessHandler(T8DataRecordAccessHandler accessHandler)
    {
        this.accessHandler = accessHandler;
    }

    /**
     * Validates the records in the supplied data file against the their individual requirements and
     * moves all values that are found to be invalid to the most applicable FFT variable.
     * @param dataFile The DataFile to be processed.  All records in the file will be processed.
     */
    public void moveInvalidDataToFft(DataRecord dataFile)
    {
        T8DataRecordRequirementValidator validator;

        validator = new T8DataRecordRequirementValidator(context, dataRecordProvider, terminologyProvider);
        validator.moveInvalidDataToFft(dataFile.getDataFileRecords());
    }

    /**
     * @deprecated Use {@code validateFile}
     */
    @Deprecated
    public T8DataFileValidationReport validateDataRecords(DataRecord dataFile) throws Exception
    {
        return validateFile(dataFile);
    }

    public T8DataFileValidationReport validateFile(DataRecord dataFile) throws Exception
    {
        T8DataFileValidationReport validationReport;
        T8DataRecordRequirementValidator requirementValidator;

        // Create a new validation report.
        validationReport = new T8DataFileValidationReport();

        // Run the access checks if an access definition is available.
        if (accessHandler != null)
        {
            accessHandler.setStaticState(dataFile);
            accessHandler.refreshDynamicState(dataFile);
            validationReport.addValidationReport(accessHandler.validateState(dataFile));
        }

        // Validate context-sensitive requirements (such as uniqueness of properties).
        requirementValidator = new T8DataRecordRequirementValidator(context, dataRecordProvider, terminologyProvider);
        requirementValidator.setValidationReport(validationReport);
        requirementValidator.validateRecords(dataFile.getDataFileRecords());

        // Return the list of validation reports from the validation context.
        return validationReport;
    }
}
