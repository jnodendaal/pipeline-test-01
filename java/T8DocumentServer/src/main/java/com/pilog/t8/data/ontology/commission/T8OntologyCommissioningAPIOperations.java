package com.pilog.t8.data.ontology.commission;

import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

import static com.pilog.t8.definition.data.ontology.commission.T8OntologyCommissioningAPIResources.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyCommissioningAPIOperations
{
    public static class APIOperationDetermineDRCommissioningImpact extends T8DefaultServerOperation
    {
        public APIOperationDetermineDRCommissioningImpact(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ServerOntologyCommissioningAPI commAPI;
            T8DataRequirementChangeRequest changeRequest;
            T8CommissioningImpactReport impactReport;

            // Get the operation parameters we need.
            changeRequest = (T8DataRequirementChangeRequest)operationParameters.get(PARAMETER_DATA_REQUIREMENT_CHANGE_REQUEST);

            // Perform the API operation.
            commAPI = (T8ServerOntologyCommissioningAPI)serverContext.getConfigurationManager().getAPI(context, T8ServerOntologyCommissioningAPI.API_IDENTIFIER);
            impactReport = commAPI.determineDRCommissioningImpact(tx, changeRequest);
            return HashMaps.newHashMap(PARAMETER_COMMISSIONING_IMPACT_REPORT, impactReport);
        }
    }

    public static class APIOperationDetermineOntologyCommissioningImpact extends T8DefaultServerOperation
    {
        public APIOperationDetermineOntologyCommissioningImpact(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ServerOntologyCommissioningAPI commAPI;
            T8OntologyChangeRequest changeRequest;
            T8CommissioningImpactReport impactReport;

            // Get the operation parameters we need.
            changeRequest = (T8OntologyChangeRequest)operationParameters.get(PARAMETER_ONTOLOGY_CHANGE_REQUEST);

            // Perform the API operation.
            commAPI = (T8ServerOntologyCommissioningAPI)serverContext.getConfigurationManager().getAPI(context, T8ServerOntologyCommissioningAPI.API_IDENTIFIER);
            impactReport = commAPI.determineOntologyCommissioningImpact(tx, changeRequest);
            return HashMaps.newHashMap(PARAMETER_COMMISSIONING_IMPACT_REPORT, impactReport);
        }
    }
}
