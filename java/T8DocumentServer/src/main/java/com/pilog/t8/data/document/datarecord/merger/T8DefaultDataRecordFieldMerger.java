package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordValueMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DefaultDataRecordFieldMergerDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordFieldMerger implements T8DataRecordFieldMerger
{
    private final Set<DataRecord> updatedDataRecords;
    private final List<T8DataRecordValueMerger> valueMergers;
    private final EnumSet<RequirementType> excludedRequirementTypes;
    private final T8DataRecordMergeContext mergeContext;
    private final String applicableFieldID;

    public T8DefaultDataRecordFieldMerger(T8DataRecordMergeContext mergeContext, T8DataRecordMergeConfiguration config)
    {
        this.mergeContext = mergeContext;
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.valueMergers = new ArrayList<T8DataRecordValueMerger>();
        this.applicableFieldID = null; // Applicable to all properties.
        this.excludedRequirementTypes = config.getExcludedRequirementTypes();
    }

    public T8DefaultDataRecordFieldMerger(T8DefaultDataRecordFieldMergerDefinition definition, T8DataRecordMergeContext mergeContext)
    {
        this.mergeContext = mergeContext;
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.valueMergers = new ArrayList<T8DataRecordValueMerger>();
        this.applicableFieldID = definition.getFieldID();
        this.excludedRequirementTypes = EnumSet.noneOf(RequirementType.class);

        // Create all the value mergers.
        for (T8DataRecordValueMergerDefinition valueMergerDefinition : definition.getValueMergerDefinitions())
        {
            valueMergers.add(valueMergerDefinition.getNewMergerInstance(mergeContext));
        }
    }

    @Override
    public T8DataTransaction getTransaction()
    {
        return mergeContext.getTransaction();
    }

    @Override
    public T8Context getContext()
    {
        return mergeContext.getContext();
    }

    @Override
    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        return mergeContext.getExpressionEvaluator();
    }

    @Override
    public T8DataRecordValueStringGenerator getFFTGenerator()
    {
        return mergeContext.getFFTGenerator();
    }

    @Override
    public DataRecord copyDataRecord(DataRecord sourceRecord)
    {
        return mergeContext.copyDataRecord(sourceRecord);
    }

    @Override
    public String getDefaultContentLanguageID()
    {
        return mergeContext.getDefaultContentLanguageID();
    }

    @Override
    public TerminologyProvider getTerminologyProvider()
    {
        return mergeContext.getTerminologyProvider();
    }

    @Override
    public OntologyProvider getOntologyProvider()
    {
        return mergeContext.getOntologyProvider();
    }

    @Override
    public DataRequirementInstanceProvider getDRInstanceProvider()
    {
        return mergeContext.getDRInstanceProvider();
    }

    @Override
    public void reset()
    {
        // Clear the updates records on this merger.
        updatedDataRecords.clear();

        // Reset the value mergers.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            valueMerger.reset();
        }
    }

    @Override
    public Map<String, String> getRecordIdMapping()
    {
        Map<String, String> recordIdMapping;

        // Create a map of all record identifier changes.
        recordIdMapping = new HashMap<String, String>();

        // Add all value merger mappings.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            recordIdMapping.putAll(valueMerger.getRecordIdMapping());
        }

        // Return the result.
        return recordIdMapping;
    }

    @Override
    public Map<String, String> getAttachmentIdentifierMapping()
    {
        Map<String, String> attachmentIdentifierMapping;

        // Create a map of all attachment identifier changes.
        attachmentIdentifierMapping = new HashMap<String, String>();

        // Add all value merger mappings.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            attachmentIdentifierMapping.putAll(valueMerger.getAttachmentIdentifierMapping());
        }

        // Return the result.
        return attachmentIdentifierMapping;
    }

    @Override
    public Set<DataRecord> getNewDataRecordSet()
    {
        HashSet<DataRecord> newRecordSet;

        // Create a set of all new records added during the merge operation.
        newRecordSet = new HashSet<DataRecord>();

        // Add all value merger new records.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            newRecordSet.addAll(valueMerger.getNewDataRecordSet());
        }

        return newRecordSet;
    }

    @Override
    public Set<DataRecord> getUpdatedDataRecordSet()
    {
        HashSet<DataRecord> updatedRecordSet;

        // Create a set of all records updated during the merge operation.
        updatedRecordSet = new HashSet<DataRecord>(updatedDataRecords);

        // Add all value merger updated records.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            updatedRecordSet.addAll(valueMerger.getUpdatedDataRecordSet());
        }

        return updatedRecordSet;
    }

    @Override
    public boolean isApplicable(RecordValue destinationField, RecordValue sourceField)
    {
        if (destinationField.getValueRequirement().getRequirementType() == RequirementType.FIELD_TYPE)
        {
            return ((applicableFieldID == null) || (applicableFieldID.equals(destinationField.getValueRequirement().getValue())));
        }
        else return false;
    }

    @Override
    public void mergeRecordFields(RecordValue destinationField, RecordValue sourceField)
    {
        RecordValue sourceValue;
        RecordValue destinationValue;

        // Get the source and destination values.
        sourceValue = sourceField.getFirstSubValue();
        destinationValue = destinationField.getFirstSubValue();

        // Only proceed if the source value is not null.
        if ((sourceValue != null) && (!excludedRequirementTypes.contains(sourceValue.getValueRequirement().getRequirementType())))
        {
            // Create the destination value if required.
            if (destinationValue == null)
            {
                destinationValue = new Field(sourceValue.getValueRequirement());
                destinationField.setSubValue(destinationValue);
                updatedDataRecords.add(destinationField.getParentDataRecord());
            }

            // Merge the two values.
            mergeRecordValues(destinationValue, sourceValue);
        }
    }

    private void mergeRecordValues(RecordValue destinationValue, RecordValue sourceValue)
    {
        T8DataRecordValueMerger valueMerger;

        // Try to find an applicable value merger.
        valueMerger = getValueMerger(destinationValue, sourceValue);
        if (valueMerger != null)
        {
            valueMerger.mergeRecordValues(destinationValue, sourceValue);
        }
        else
        {
            // No field merger found, so use the defaul.
            throw new RuntimeException("No applicable value merger found for destination value: " + destinationValue);
        }
    }

    @Override
    public T8DataRecordInstanceMerger getInstanceMerger(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        return mergeContext.getInstanceMerger(destinationRecord, sourceRecord);
    }

    @Override
    public T8DataRecordPropertyMerger getPropertyMerger(RecordProperty destinationProperty, RecordProperty sourceProperty)
    {
        return mergeContext.getPropertyMerger(destinationProperty, sourceProperty);
    }

    @Override
    public T8DataRecordFieldMerger getFieldMerger(RecordValue destinationField, RecordValue sourceField)
    {
        return mergeContext.getFieldMerger(destinationField, sourceField);
    }

    @Override
    public T8DataRecordValueMerger getValueMerger(RecordValue destinationValue, RecordValue sourceValue)
    {
        // Try to find a specific applicable merger for the values.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            if (valueMerger.isApplicable(destinationValue, sourceValue))
            {
                return valueMerger;
            }
        }

        // No applicable merger found.
        return mergeContext.getValueMerger(destinationValue, sourceValue);
    }
}
