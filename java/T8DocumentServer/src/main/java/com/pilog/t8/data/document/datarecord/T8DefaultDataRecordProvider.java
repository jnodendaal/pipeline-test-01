package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.datarecord.attachment.T8DefaultAttachmentHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordProvider implements DataRecordProvider
{
    private final T8DataTransaction tx;
    private final String dataRecordDEID;
    private final T8DefaultAttachmentHandler attachmentHandler;

    public T8DefaultDataRecordProvider(T8ServerContext serverContext, T8DataTransaction tx, String dataRecordDEID)
    {
        this.tx = tx;
        this.dataRecordDEID = dataRecordDEID;
        this.attachmentHandler = new T8DefaultAttachmentHandler(serverContext, tx, dataRecordDEID);
    }

    @Override
    public String getRecordDrInstanceId(String recordID)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8AttachmentDetails getAttachmentDetails(String attachmentId)
    {
        try
        {
            return attachmentHandler.retrieveAttachmentDetails(attachmentId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching attachment details '" + attachmentId + "' from entity: " + dataRecordDEID, e);
        }
    }

    @Override
    public Map<String, T8AttachmentDetails> getRecordAttachmentDetails(String recordID)
    {
        try
        {
            return attachmentHandler.retrieveDataRecordAttachmentDetails(recordID);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching attachment details '" + recordID + "' from entity: " + dataRecordDEID, e);
        }
    }

    @Override
    public DataRecord getDataRecord(String recordID, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendentRecords)
    {
        try
        {
            return T8DataRecordPersistenceUtilities.retrieveDataRecord(tx, dataRecordDEID, recordID);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data record '" + recordID + "' from entity: " + dataRecordDEID, e);
        }
    }

    @Override
    public List<DataRecord> getDataRecords(List<String> recordIDList, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendentRecords)
    {
        try
        {
            List<DataRecord> recordList;

            recordList = new ArrayList<>();
            for (String recordID : recordIDList)
            {
                DataRecord record;

                record = T8DataRecordPersistenceUtilities.retrieveDataRecord(tx, dataRecordDEID, recordID);
                if (record != null) recordList.add(record);
            }

            return recordList;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data records '" + recordIDList + "' from entity: " + dataRecordDEID, e);
        }
    }

    @Override
    public DataRecord getDataFile(String recordID, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DataRecord> getDataFiles(List<String> recordIDList, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DataRecord getDataFileByContentRecord(String recordID, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DataRecord> getDataFilesByContentRecord(List<String> recordIDList, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
