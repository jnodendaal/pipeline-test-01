package com.pilog.t8.data.alteration.handler;

import com.pilog.json.JsonObject;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.alteration.T8DataAlterationHandler;
import com.pilog.t8.data.alteration.T8DataAlterationImpact;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.alteration.type.T8CompleteTermAlteration;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8CompleteTermAlterationHandler implements T8DataAlterationHandler
{
    private final T8ServerContext serverContext;

    public T8CompleteTermAlterationHandler(T8Context context)
    {
        this.serverContext = context.getServerContext();
    }

    @Override
    public String getAlterationId()
    {
        return T8CompleteTermAlteration.ALTERATION_ID;
    }

    @Override
    public int deleteAlteration(T8DataTransaction tx, String alterationID, int step) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8CompleteTermAlteration termAlteration;
        T8OntologyTerm alteredTerm;

        termAlteration = (T8CompleteTermAlteration)alteration;
        alteredTerm = termAlteration.getTerm();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8CompleteTermAlteration termAlteration;
        T8OntologyTerm alteredTerm;

        termAlteration = (T8CompleteTermAlteration)alteration;
        alteredTerm = termAlteration.getTerm();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlterationImpact applyAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8CompleteTermAlteration termAlteration;
        T8OntologyTerm alteredTerm;

        termAlteration = (T8CompleteTermAlteration)alteration;
        alteredTerm = termAlteration.getTerm();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlteration retrieveAlteration(T8DataTransaction tx, String alterationID, int step) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void adjustSteps(T8DataTransaction tx, String alterationID, int startStep, int endStep, int increment) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlterationImpact getAlterationImpact(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8CompleteTermAlteration termAlteration;
        T8OntologyTerm alteredTerm;

        termAlteration = (T8CompleteTermAlteration)alteration;
        alteredTerm = termAlteration.getTerm();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlteration deserializeAlteration(JsonObject jsonValue)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
