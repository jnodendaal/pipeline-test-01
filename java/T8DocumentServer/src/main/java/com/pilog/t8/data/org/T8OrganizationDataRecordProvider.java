package com.pilog.t8.data.org;

import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationDataRecordProvider implements DataRecordProvider
{
    private final T8DataRecordApi recApi;

    public T8OrganizationDataRecordProvider(T8DataRecordApi recApi)
    {
        this.recApi = recApi;
    }

    @Override
    public DataRecord getDataRecord(String recordId, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendants)
    {
        try
        {
            return recApi.retrieveRecord(recordId, languageID, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings, includeDescendants);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving organization Data Record.", e);
        }
    }

    @Override
    public List<DataRecord> getDataRecords(List<String> recordIDList, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendants)
    {
        try
        {
            List<DataRecord> recordList;

            recordList = new ArrayList<>();
            for (String recordId : recordIDList)
            {
                DataRecord record;

                record = recApi.retrieveRecord(recordId, languageID, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings, includeDescendants);
                if (record != null) recordList.add(record);
            }

            return recordList;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving organization Data Records: " + recordIDList, e);
        }
    }

    @Override
    public DataRecord getDataFile(String fileId, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        try
        {
            return recApi.retrieveFile(fileId, languageID, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving Data File: " + fileId, e);
        }
    }

    @Override
    public List<DataRecord> getDataFiles(List<String> fileIDList, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        try
        {
            List<DataRecord> recordList;

            recordList = new ArrayList<>();
            for (String recordId : fileIDList)
            {
                DataRecord record;

                record = recApi.retrieveFile(recordId, languageID, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
                if (record != null) recordList.add(record);
            }

            return recordList;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving Data Files: " + fileIDList, e);
        }
    }

    @Override
    public DataRecord getDataFileByContentRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        try
        {
            return recApi.retrieveFileByContentRecord(recordId, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving Data File by content record: " + recordId, e);
        }
    }

    @Override
    public List<DataRecord> getDataFilesByContentRecord(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings)
    {
        try
        {
            List<DataRecord> recordList;

            recordList = new ArrayList<>();
            for (String recordId : recordIdList)
            {
                DataRecord record;

                record = recApi.retrieveFileByContentRecord(recordId, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
                if (record != null) recordList.add(record);
            }

            return recordList;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving Data Files by content record: " + recordIdList, e);
        }
    }

    @Override
    public T8AttachmentDetails getAttachmentDetails(String attachmentId)
    {
        try
        {
            return recApi.retrieveAttachmentDetails(attachmentId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving details for attachment: " + attachmentId, e);
        }
    }

    @Override
    public Map<String, T8AttachmentDetails> getRecordAttachmentDetails(String recordId)
    {
        try
        {
            return recApi.retrieveRecordAttachmentDetails(recordId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving attachment details for Data Record: " + recordId, e);
        }
    }

    @Override
    public String getRecordDrInstanceId(String recordId)
    {
        try
        {
            return recApi.retrieveRecordDrInstanceId(recordId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving Data Record DR Instance ID: " + recordId, e);
        }
    }
}
