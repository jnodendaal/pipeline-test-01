package com.pilog.t8.api;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.document.datarecord.access.layer.FieldAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.access.T8DataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.value.DependencyValue;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarequirement.DataDependency;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.datarequirement.value.QualifierOfMeasureRequirement;
import com.pilog.t8.data.document.datarequirement.value.UnitOfMeasureRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.datastructures.tuples.DefaultPairComparator;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.DocumentHandler;
import com.pilog.t8.data.document.PropertyValueStringCriterion;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.document.T8DocumentDataHandler;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.document.datafile.T8DataFileOutputStream;
import com.pilog.t8.data.document.datafile.T8DataFileSaveResult;
import com.pilog.t8.data.document.datarecord.T8DataFileDataStringOutputStream;
import com.pilog.t8.data.document.datarecord.T8DataRecordObjectOutputStream;
import com.pilog.t8.data.document.datarecord.T8DataRecordOntologyHandler;
import com.pilog.t8.data.document.datarecord.T8DataRecordValidator;
import com.pilog.t8.data.document.datarecord.access.T8DefaultDataRecordAccessHandler;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentValidationReport;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentValidationResult;
import com.pilog.t8.data.document.datarecord.comparator.DataRecordLevelComparator;
import com.pilog.t8.data.document.datarecord.merger.T8DRInstanceChangeHandler;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeContext;
import com.pilog.t8.data.document.datarecord.merger.T8DefaultDataRecordMergeContext;
import com.pilog.t8.data.document.datarequirement.T8DataRequirementComment;
import com.pilog.t8.data.document.datastring.T8DataString;
import com.pilog.t8.data.document.datastring.T8DataStringFilter;
import com.pilog.t8.data.document.datastring.T8DataStringFilter.T8DataStringFilterBuilder;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.org.T8OrganizationDataRecordProvider;
import com.pilog.t8.data.source.datarecord.T8AttachmentHandler;
import com.pilog.t8.definition.data.document.datarecord.access.T8DataRecordAccessDefinition;
import com.pilog.t8.definition.data.org.T8OrganizationDataCacheDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordAttachmentHandlerDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.DataRecordProvider;
import com.pilog.t8.data.document.T8LRUCachedDataRecordProvider;
import com.pilog.t8.data.document.datarecord.RecordAttribute;
import com.pilog.t8.data.document.datarecord.RecordMetaDataType;
import com.pilog.t8.data.document.datarecord.T8DataFileHashCodeOutputStream;
import com.pilog.t8.data.document.datarecord.T8DataFileTerminologyOutputStream;
import com.pilog.t8.data.document.datarecord.T8DataRecordFactory;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeConfiguration;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordSynchronizer;
import com.pilog.t8.data.document.datarecord.merger.T8DefaultDataRecordMerger;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.ontology.T8ConceptAlteration;
import com.pilog.t8.data.ontology.T8OntologyConcept.T8ConceptElementType;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordMergerDefinition;
import com.pilog.t8.utilities.function.MemoizedSupplier;
import com.pilog.t8.utilities.strings.Strings;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.function.Supplier;


/**
 * @author Bouwer du Preez
 */
public class T8DataRecordApi implements T8Api, T8PerformanceStatisticsProvider
{
    public static final String API_IDENTIFIER = "@API_DATA_RECORD";

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8Context accessContext;
    private final T8DataRecordApiDataHandler dataHandler;
    private final T8DocumentDataHandler documentDataHandler;
    private final Map<String, Supplier<T8DataFileOutputStream>> fileOutputStreams;
    private final Map<String, T8DataRecordAccessDefinition> accessDefinitions; // Key: Definition ID.
    private T8PerformanceStatistics stats;

    public T8DataRecordApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.serverContext = context.getServerContext();
        this.accessContext = tx.getContext();
        this.dataHandler = new T8DataRecordApiDataHandler(context);
        this.documentDataHandler = new T8DocumentDataHandler(context);
        this.accessDefinitions = new HashMap<>();
        this.fileOutputStreams = new LinkedHashMap<>();
        this.stats = tx.getPerformanceStatistics(); // Default behviour of all API's is to use the performance statistics object of its parent transaction.
        createFileOutputStreams();
    }

    private void createFileOutputStreams()
    {
        // We use lazy suppliers to ensure that these output streams are not created before they are required (necessary to prevent endless API constructor loop).
        fileOutputStreams.put("TERMINOLOGY", new MemoizedSupplier(() -> new T8DataFileTerminologyOutputStream(tx)));
        fileOutputStreams.put("DATA_STRING", new MemoizedSupplier(() -> new T8DataFileDataStringOutputStream(tx)));
        fileOutputStreams.put("HASH_CODE", new MemoizedSupplier(() -> new T8DataFileHashCodeOutputStream(tx)));
        fileOutputStreams.put("DATA_OBJECT", new MemoizedSupplier(() -> new T8DataRecordObjectOutputStream(tx, null)));
    }

    public T8DataFileOutputStream getFileOutputStream(String streamId)
    {
        return fileOutputStreams.get(streamId).get();
    }

    public void setFileOutputStream(String streamId, T8DataFileOutputStream stream)
    {
        fileOutputStreams.put(streamId, () -> stream);
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public void recacheAccessDefinitions() throws Exception
    {
        accessDefinitions.clear();
    }

    public T8DataRecordFactory getRecordFactory()
    {
        return new T8DataRecordFactory(tx);
    }

    public DataRecordProvider getRecordProvider()
    {
        return new T8LRUCachedDataRecordProvider(new T8OrganizationDataRecordProvider(this), 20);
    }

    /**
     * Returns all ontology links for the specified Data Requirement
     * Instance.
     * @return The list of organization links for the specified DR Instance.
     * @param drInstanceId The concept ID of the DR Instance for which to fetch
     * the links.
     * @throws java.lang.Exception
     */
    private List<T8OntologyLink> getOrganizationDataRequirementInstanceLinks(String drInstanceId) throws Exception
    {
        T8DataManager dataManager;
        Map<String, Object> dataParameters;

        dataParameters = new HashMap<>();
        dataParameters.put(T8OrganizationDataCacheDefinition.DATA_PARAMETER_ORG_ID, context.getSessionContext().getRootOrganizationIdentifier());
        dataParameters.put(T8OrganizationDataCacheDefinition.DATA_PARAMETER_DR_INSTANCE_ID, drInstanceId);
        dataManager = serverContext.getDataManager();
        return (List<T8OntologyLink>)dataManager.getCachedData(T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_DR_INSTANCE_LINKS, dataParameters);
    }

    private T8DataRecordAccessDefinition getAccessDefinition(String accessId)
    {
        if (accessId != null)
        {
            T8DataRecordAccessDefinition accessDefinition;

            accessDefinition = accessDefinitions.get(accessId);
            if (accessDefinition == null)
            {
                try
                {
                    accessDefinition = (T8DataRecordAccessDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), accessId, null);
                    accessDefinitions.put(accessId, accessDefinition);
                    return accessDefinition;
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while retrieving access definition: " + accessId, e);
                }
            }
            else return accessDefinition;
        }
        else return null;
    }

    public T8DataRecordAccessHandler getAccessHandler(DataRecord dataFile)
    {
        String accessId;

        accessId = accessContext.getDataAccessId(dataFile.getDataRequirementInstanceID());
        if (accessId != null)
        {
            T8DataRecordAccessDefinition accessDefinition;

            // Retrieve all of the validation script definitions.
            accessDefinition = getAccessDefinition(accessId);
            if (accessDefinition == null) return null;
            else return accessDefinition.getNewDataRecordAccessHandlerInstance(context);
        }
        else return new T8DefaultDataRecordAccessHandler(null, true);
    }

    public T8DataRecordAccessHandler getAccessHandler()
    {
        String accessId;

        accessId = accessContext.getDataAccessId(null);
        if (accessId != null)
        {
            T8DataRecordAccessDefinition accessDefinition;

            // Retrieve all of the validation script definitions.
            accessDefinition = getAccessDefinition(accessId);
            if (accessDefinition == null) return null;
            else return accessDefinition.getNewDataRecordAccessHandlerInstance(context);
        }
        else return new T8DefaultDataRecordAccessHandler(null, true);
    }

    public T8DataRecordValidator getValidator()
    {
        T8DataRecordValidator validator;

        validator = new T8DataRecordValidator(tx);
        validator.setAccessHandler(getAccessHandler());
        return validator;
    }

    public T8DataRecordMergeConfiguration createMergerConfiguration()
    {
        T8DataRecordMergeConfiguration newConfig;
        DataRecordProvider recordProvider;
        T8TerminologyApi trmApi;

        // Get the api's required for the configuration.
        trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);

        // Create record providers to use for the merge operation.
        recordProvider = new T8LRUCachedDataRecordProvider(new T8OrganizationDataRecordProvider(this), 20);

        // Create the configuration object.
        newConfig = new T8DataRecordMergeConfiguration();
        newConfig.setSourceRecordProvider(recordProvider);
        newConfig.setDestinationRecordProvider(recordProvider);
        newConfig.setTerminologyProvider(trmApi.getTerminologyProvider());
        return newConfig;
    }

    public T8DataRecordMerger createMerger(T8DataRecordMergeConfiguration config)
    {
        T8DataRecordMergeContext mergeContext;
        T8DefaultDataRecordMerger merger;

        // Create the merger and the context for it.
        mergeContext = new T8DefaultDataRecordMergeContext(tx);
        merger = new T8DefaultDataRecordMerger(mergeContext, config);
        return merger;
    }

    public T8DataRecordMerger getMerger(String mergerId) throws Exception
    {
        // Create the merger to use for this operation.
        if (mergerId != null)
        {
            T8DataRecordMergerDefinition mergerDefinition;

            mergerDefinition = (T8DataRecordMergerDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, null, mergerId, null);
            if (mergerDefinition != null)
            {
                T8DataRecordMergeContext mergeContext;

                // Create a new merge context.
                mergeContext = new T8DefaultDataRecordMergeContext(tx);
                return mergerDefinition.getNewMergerInstance(mergeContext);
            }
            else throw new Exception("Merger Definition not found: " + mergerId);
        }
        else throw new Exception("No Merger Definition specified.");
    }

    public int flagAsyncOntologyUpdateByRecordId(String recordId) throws Exception
    {
        return documentDataHandler.flagAsyncOntologyUpdateByRecordId(tx, ArrayLists.typeSafeList(recordId));
    }

    public int flagAsyncOntologyUpdateByRecordId(List<String> recordIdList) throws Exception
    {
        return documentDataHandler.flagAsyncOntologyUpdateByRecordId(tx, recordIdList);
    }

    public int flagAsyncOntologyUpdateByDrId(String drId) throws Exception
    {
        return documentDataHandler.flagAsyncOntologyUpdateByDrId(tx, ArrayLists.typeSafeList(drId));
    }

    public int flagAsyncOntologyUpdateByDrId(List<String> drIdList) throws Exception
    {
        return documentDataHandler.flagAsyncOntologyUpdateByDrId(tx, drIdList);
    }

    public int flagAsyncOntologyUpdateByDrInstanceId(String drInstanceId) throws Exception
    {
        return documentDataHandler.flagAsyncOntologyUpdateByDrInstanceId(tx, ArrayLists.typeSafeList(drInstanceId));
    }

    public int flagAsyncOntologyUpdateByDrInstanceId(List<String> drInstanceIdList) throws Exception
    {
        return documentDataHandler.flagAsyncOntologyUpdateByDrInstanceId(tx, drInstanceIdList);
    }

    public String resolveConceptId(DataRecord dataFile, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, String matchString, List<T8ConceptElementType> elementTypes) throws Exception
    {
        T8DataRecordAccessHandler accessHandler;
        DataRecord targetRecord;
        DataRequirementInstance targetDataRequirementInstance;
        DataRequirement targetDataRequirement;
        PropertyRequirement targetPropertyRequirement;
        FieldRequirement targetFieldRequirement;
        ValueRequirement targetValueRequirement;
        RequirementType targetRequirementType;
        RecordProperty targetProperty;
        String targetMatchString;
        Field targetField;
        RecordValue targetValue;
        String targetDrInstanceId;
        String targetDrId;
        T8OrganizationApi orgApi;
        T8OntologyApi ontApi;

        // Get the required api's.
        orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);

        // Get the access handler to use for the operation.
        accessHandler = getAccessHandler(dataFile);

        // Get the target requirement for which to suggest values.
        targetRecord = dataFile.findDataRecord(targetRecordId);
        targetDataRequirementInstance = targetRecord.getDataRequirementInstance();
        targetDataRequirement = targetRecord.getDataRequirement();
        targetPropertyRequirement = targetDataRequirement.getPropertyRequirement(targetPropertyId);
        targetFieldRequirement = targetPropertyRequirement.getFieldRequirement(targetFieldId);
        targetRequirementType = RequirementType.valueOf(targetDataTypeId);
        targetDrInstanceId = targetRecord.getDataRequirementInstanceID();
        targetDrId = targetRecord.getDataRequirementID();
        targetProperty = targetRecord.getRecordProperty(targetPropertyId);
        targetField = (targetFieldRequirement != null) && (targetProperty != null) ? targetProperty.getField(targetFieldId) : null;
        targetValue = targetField != null ? targetField.getFieldValue() : targetProperty != null ? targetProperty.getRecordValue() : null;
        targetMatchString = Strings.trimToNull(matchString);

        // Get the target value requirement from the target field or if no field is specified, from the target property.
        if (targetFieldRequirement != null)
        {
            targetValueRequirement = targetFieldRequirement.getDescendentRequirement(targetRequirementType, null, null);
        }
        else
        {
            targetValueRequirement = targetPropertyRequirement.getValueRequirement();
            if (targetValueRequirement.getRequirementType() != targetRequirementType) targetValueRequirement = targetValueRequirement.getDescendentRequirement(RequirementType.valueOf(targetDataTypeId), null, null);
        }

        // If the value is prescribed use the precribed value API to retrieve the suggestions.
        if (targetValueRequirement.isPrescribed())
        {
            boolean restrictToApprovedValues;

            restrictToApprovedValues = false;
            return dataHandler.retrievePrescribedConceptId(tx, orgApi.getOrganizationStructure(), ontApi.getOntologyStructure(), targetValueRequirement, targetRequirementType, targetDrId, targetDrInstanceId, targetPropertyId, targetFieldId, null, restrictToApprovedValues, targetMatchString, elementTypes);
        }
        else
        {
            List<String> orgIdList;
            List<String> ocIdList;
            List<String> conceptIdList;
            List<Pair<String, String>> conceptDependencyList;
            PropertyAccessLayer propertyAccessLayer;
            FieldAccessLayer fieldAccessLayer;
            String defaultOcId;

            // Get the access layers of the target property/field.
            propertyAccessLayer = targetRecord.getAccessLayer().getPropertyAccessLayer(targetPropertyId);
            fieldAccessLayer = propertyAccessLayer.getFieldAccessLayer(targetFieldId);

            // Get the default group ot use.
            switch (targetRequirementType)
            {
                case CONTROLLED_CONCEPT:
                    defaultOcId = ((ControlledConceptRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                case UNIT_OF_MEASURE:
                    defaultOcId = ((UnitOfMeasureRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                case QUALIFIER_OF_MEASURE:
                    defaultOcId = ((QualifierOfMeasureRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                default:
                    return null;
            }

            // Create the list of accesible organization levels.
            orgIdList = getFilterOrgIdList(orgApi.getOrganizationStructure(), targetDataRequirementInstance);

            // Create a list of all groups to include (roots and their descendants).
            ocIdList = getFilterOntologyClassIdList(ontApi.getOntologyStructure(), defaultOcId, propertyAccessLayer, fieldAccessLayer);

            // Create a list of all the specific concepts to include in the filter.
            conceptIdList = getFilterConceptIdList(propertyAccessLayer, fieldAccessLayer);

            // Create the list of concept dependencies to use for filter construction.
            conceptDependencyList = getConceptDependencyList(targetValue);
            return dataHandler.retrieveConceptId(tx, orgApi.getOrganizationStructure(), ontApi.getOntologyStructure(), targetValueRequirement, targetRequirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList, targetMatchString, elementTypes);
        }
    }

    public List<RecordValue> retrieveValueSuggestions(DataRecord dataFile, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, String searchPrefix, int pageOffset, int pageSize) throws Exception
    {
        T8DataRecordAccessHandler accessHandler;
        DataRequirementInstance targetDataRequirementInstance;
        DataRequirement targetDataRequirement;
        PropertyRequirement targetPropertyRequirement;
        FieldRequirement targetFieldRequirement;
        ValueRequirement targetValueRequirement;
        RequirementType targetRequirementType;
        RecordProperty targetProperty;
        DataRecord targetRecord;
        Field targetField;
        RecordValue targetValue;
        String targetDRInstanceID;
        String targetDRID;
        T8OrganizationApi orgApi;
        T8OntologyApi ontApi;
        T8TerminologyApi trmApi;

        // Get the required api's.
        orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);

        // Get the access handler to use for the operation.
        accessHandler = getAccessHandler(dataFile);

        // Get the target requirement for which to suggest values.
        targetRecord = dataFile.findDataRecord(targetRecordId);
        targetDataRequirementInstance = targetRecord.getDataRequirementInstance();
        targetDataRequirement = targetRecord.getDataRequirement();
        targetPropertyRequirement = targetDataRequirement.getPropertyRequirement(targetPropertyId);
        targetFieldRequirement = targetPropertyRequirement.getFieldRequirement(targetFieldId);
        targetRequirementType = RequirementType.valueOf(targetDataTypeId);
        targetDRInstanceID = targetRecord.getDataRequirementInstanceID();
        targetDRID = targetRecord.getDataRequirementID();
        targetProperty = targetRecord.getRecordProperty(targetPropertyId);
        targetField = (targetFieldRequirement != null) && (targetProperty != null) ? targetProperty.getField(targetFieldId) : null;
        targetValue = targetField != null ? targetField.getFieldValue() : targetProperty != null ? targetProperty.getRecordValue() : null;

        // Get the target value requirement from the target field or if no field is specified, from the target property.
        if (targetFieldRequirement != null)
        {
            targetValueRequirement = targetFieldRequirement.getDescendentRequirement(targetRequirementType, null, null);
        }
        else
        {
            targetValueRequirement = targetPropertyRequirement.getValueRequirement();
            if (targetValueRequirement.getRequirementType() != targetRequirementType) targetValueRequirement = targetValueRequirement.getDescendentRequirement(RequirementType.valueOf(targetDataTypeId), null, null);
        }

        // If the value is prescribed use the precribed value API to retrieve the suggestions.
        if (targetValueRequirement.isPrescribed())
        {
            TerminologyProvider terminologyProvider;
            List<RecordValue> valueSuggestions;
            boolean restrictToApprovedValues;

            // Retrieve the prescribed value suggestions.
            restrictToApprovedValues = false;
            valueSuggestions = dataHandler.retrievePrescribedValueSuggestions(tx, orgApi.getOrganizationStructure(), ontApi.getOntologyStructure(), targetValueRequirement, targetRequirementType, targetDRID, targetDRInstanceID, targetPropertyId, targetFieldId, null, searchPrefix, restrictToApprovedValues, pageOffset, pageSize);

            // Set terminology on the value suggestions and return them as the output result.
            terminologyProvider = trmApi.getTerminologyProvider();
            for (RecordValue valueSuggestion : valueSuggestions)
            {
                valueSuggestion.setContentTerminology(terminologyProvider);
            }
            return valueSuggestions;
        }
        else
        {
            List<String> orgIdList;
            List<String> ocIdList;
            List<String> conceptIdList;
            List<RecordValue> valueSuggestions;
            List<Pair<String, String>> conceptDependencyList;
            TerminologyProvider terminologyProvider;
            PropertyAccessLayer propertyAccessLayer;
            FieldAccessLayer fieldAccessLayer;
            String defaultOcId;

            // Get the access layers of the target property/field.
            propertyAccessLayer = targetRecord.getAccessLayer().getPropertyAccessLayer(targetPropertyId);
            fieldAccessLayer = propertyAccessLayer.getFieldAccessLayer(targetFieldId);

            // Get the default group ot use.
            switch (targetRequirementType)
            {
                case CONTROLLED_CONCEPT:
                    defaultOcId = ((ControlledConceptRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                case UNIT_OF_MEASURE:
                    defaultOcId = ((UnitOfMeasureRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                case QUALIFIER_OF_MEASURE:
                    defaultOcId = ((QualifierOfMeasureRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                default:
                    return new ArrayList<RecordValue>();
            }

            // Create the list of accesible organization levels.
            orgIdList = getFilterOrgIdList(orgApi.getOrganizationStructure(), targetDataRequirementInstance);

            // Create a list of all groups to include (roots and their descendants).
            ocIdList = getFilterOntologyClassIdList(ontApi.getOntologyStructure(), defaultOcId, propertyAccessLayer, fieldAccessLayer);

            // Create a list of all the specific concepts to include in the filter.
            conceptIdList = getFilterConceptIdList(propertyAccessLayer, fieldAccessLayer);

            // Create the list of concept dependencies to use for filter construction.
            conceptDependencyList = getConceptDependencyList(targetValue);
            valueSuggestions = dataHandler.retrieveConceptValueSuggestions(tx, orgApi.getOrganizationStructure(), ontApi.getOntologyStructure(), targetValueRequirement, targetRequirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList, searchPrefix, pageOffset, pageSize);

            // Set terminology on the value suggestions and return them as the output result.
            terminologyProvider = trmApi.getTerminologyProvider();
            for (RecordValue valueSuggestion : valueSuggestions)
            {
                valueSuggestion.setContentTerminology(terminologyProvider);
            }
            return valueSuggestions;
        }
    }

    public List<RecordValue> retrieveValueOptions(DataRecord dataFile, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, String searchTerms, List<T8ConceptElementType> searchElements, int pageOffset, int pageSize) throws Exception
    {
        T8DataRecordAccessHandler accessHandler;
        DataRecord targetRecord;
        DataRequirementInstance targetDataRequirementInstance;
        DataRequirement targetDataRequirement;
        PropertyRequirement targetPropertyRequirement;
        FieldRequirement targetFieldRequirement;
        ValueRequirement targetValueRequirement;
        RequirementType targetRequirementType;
        RecordProperty targetProperty;
        Field targetField;
        RecordValue targetValue;
        String targetDrInstanceId;
        String targetDrId;
        T8TerminologyApi trmApi;
        T8OrganizationApi orgApi;
        T8OntologyApi ontApi;

        // Get the required api's.
        orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);

        // Get the access handler to use for the operation.
        accessHandler = getAccessHandler(dataFile);

        // Get the target requirement for which to suggest values.
        targetRecord = dataFile.findDataRecord(targetRecordId);
        targetDataRequirementInstance = targetRecord.getDataRequirementInstance();
        targetDataRequirement = targetRecord.getDataRequirement();
        targetPropertyRequirement = targetDataRequirement.getPropertyRequirement(targetPropertyId);
        targetFieldRequirement = targetPropertyRequirement.getFieldRequirement(targetFieldId);
        targetRequirementType = RequirementType.valueOf(targetDataTypeId);
        targetDrInstanceId = targetRecord.getDataRequirementInstanceID();
        targetDrId = targetRecord.getDataRequirementID();
        targetProperty = targetRecord.getRecordProperty(targetPropertyId);
        targetField = (targetFieldRequirement != null) && (targetProperty != null) ? targetProperty.getField(targetFieldId) : null;
        targetValue = targetField != null ? targetField.getFieldValue() : targetProperty != null ? targetProperty.getRecordValue() : null;

        // Get the target value requirement from the target field or if no field is specified, from the target property.
        if (targetFieldRequirement != null)
        {
            targetValueRequirement = targetFieldRequirement.getDescendentRequirement(targetRequirementType, null, null);
        }
        else
        {
            targetValueRequirement = targetPropertyRequirement.getValueRequirement();
            if (targetValueRequirement.getRequirementType() != targetRequirementType) targetValueRequirement = targetValueRequirement.getDescendentRequirement(RequirementType.valueOf(targetDataTypeId), null, null);
        }

        // If the value is prescribed use the precribed value API to retrieve the suggestions.
        if (targetValueRequirement.isPrescribed())
        {
            TerminologyProvider terminologyProvider;
            List<RecordValue> valueOptions;
            boolean restrictToApprovedValues;

            // Retrieve the value options.
            restrictToApprovedValues = false;
            valueOptions = dataHandler.retrievePrescribedValueOptions(tx, orgApi.getOrganizationStructure(), ontApi.getOntologyStructure(), targetValueRequirement, targetRequirementType, targetDrId, targetDrInstanceId, targetPropertyId, targetFieldId, null, restrictToApprovedValues, searchTerms, pageOffset, pageSize);

            // Set terminology on the value options and return them as the output result.
            terminologyProvider = trmApi.getTerminologyProvider();
            for (RecordValue valueOption : valueOptions)
            {
                valueOption.setContentTerminology(terminologyProvider);
            }
            return valueOptions;
        }
        else
        {
            List<Pair<String, String>> conceptDependencyList;
            PropertyAccessLayer propertyAccessLayer;
            TerminologyProvider terminologyProvider;
            FieldAccessLayer fieldAccessLayer;
            List<RecordValue> valueOptions;
            List<String> orgIdList;
            List<String> ocIdList;
            List<String> conceptIdList;
            String defaultOcId;

            // Get the access layers of the target property/field.
            propertyAccessLayer = targetRecord.getAccessLayer().getPropertyAccessLayer(targetPropertyId);
            fieldAccessLayer = propertyAccessLayer.getFieldAccessLayer(targetFieldId);

            // Get the default group to use.
            switch (targetRequirementType)
            {
                case DOCUMENT_REFERENCE:
                    defaultOcId = ((DocumentReferenceRequirement)targetValueRequirement).getOntologyClassId();
                    break;
                case CONTROLLED_CONCEPT:
                    defaultOcId = ((ControlledConceptRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                case UNIT_OF_MEASURE:
                    defaultOcId = ((UnitOfMeasureRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                case QUALIFIER_OF_MEASURE:
                    defaultOcId = ((QualifierOfMeasureRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                default:
                    return new ArrayList<RecordValue>();
            }

            // Create the list of accesible organization levels.
            orgIdList = getFilterOrgIdList(orgApi.getOrganizationStructure(), targetDataRequirementInstance);

            // Create a list of all groups to include (roots and their descendants).
            ocIdList = getFilterOntologyClassIdList(ontApi.getOntologyStructure(), defaultOcId, propertyAccessLayer, fieldAccessLayer);

            // Create a list of all the specific concepts to include in the filter.
            conceptIdList = getFilterConceptIdList(propertyAccessLayer, fieldAccessLayer);

            // Create the list of concept dependencies to use for filter construction.
            conceptDependencyList = getConceptDependencyList(targetValue);
            valueOptions = dataHandler.retrieveConceptValueOptions(tx, orgApi.getOrganizationStructure(), ontApi.getOntologyStructure(), targetValueRequirement, targetRequirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList, searchTerms, searchElements, pageOffset, pageSize);

            // Set terminology on the value options and return them as the output result.
            terminologyProvider = trmApi.getTerminologyProvider();
            for (RecordValue valueOption : valueOptions)
            {
                valueOption.setContentTerminology(terminologyProvider);
            }
            return valueOptions;
        }
    }

    public List<T8ConceptTerminology> retrieveDrInstanceOptions(DataRecord dataFile, String targetRecordId, String targetPropertyId, String targetFieldId, String targetDataTypeId, int pageOffset, int pageSize) throws Exception
    {
        T8DataRecordAccessHandler accessHandler;
        DataRecord targetRecord;
        DataRequirementInstance targetDataRequirementInstance;
        DataRequirement targetDataRequirement;
        PropertyRequirement targetPropertyRequirement;
        FieldRequirement targetFieldRequirement;
        ValueRequirement targetValueRequirement;
        RequirementType targetRequirementType;
        RecordProperty targetProperty;
        Field targetField;
        RecordValue targetValue;
        String targetDrInstanceId;
        String targetDrId;
        T8OrganizationApi orgApi;
        T8OntologyApi ontApi;

        // Get the required api's.
        orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);

        // Get the access handler to use for the operation.
        accessHandler = getAccessHandler(dataFile);

        // Get the target requirement for which to suggest values.
        targetRecord = dataFile.findDataRecord(targetRecordId);
        targetDataRequirementInstance = targetRecord.getDataRequirementInstance();
        targetDataRequirement = targetRecord.getDataRequirement();
        targetPropertyRequirement = targetDataRequirement.getPropertyRequirement(targetPropertyId);
        targetFieldRequirement = targetPropertyRequirement.getFieldRequirement(targetFieldId);
        targetRequirementType = RequirementType.valueOf(targetDataTypeId);
        targetDrInstanceId = targetRecord.getDataRequirementInstanceID();
        targetDrId = targetRecord.getDataRequirementID();
        targetProperty = targetRecord.getRecordProperty(targetPropertyId);
        targetField = (targetFieldRequirement != null) && (targetProperty != null) ? targetProperty.getField(targetFieldId) : null;
        targetValue = targetField != null ? targetField.getFieldValue() : targetProperty != null ? targetProperty.getRecordValue() : null;
        System.out.println("Using filter values: targetRequirementType:" + targetRequirementType + " targetDRID:" + targetDrId + " targetDRInstanceID:" + targetDrInstanceId + " targetPropertyID:" + targetPropertyId + " targetFieldID:" + targetFieldId);

        // Get the target value requirement from the target field or if no field is specified, from the target property.
        if (targetFieldRequirement != null)
        {
            targetValueRequirement = targetFieldRequirement.getDescendentRequirement(targetRequirementType, null, null);
        }
        else
        {
            targetValueRequirement = targetPropertyRequirement.getValueRequirement();
            if (targetValueRequirement.getRequirementType() != targetRequirementType) targetValueRequirement = targetValueRequirement.getDescendentRequirement(RequirementType.valueOf(targetDataTypeId), null, null);
        }

        // If the value is prescribed use the precribed value API to retrieve the suggestions.
        if (targetValueRequirement.isPrescribed())
        {
            boolean restrictToApprovedValues;

            restrictToApprovedValues = false;
            System.out.println("Value is Prescribed...");
            return dataHandler.retrievePrescribedDrInstanceOptions(tx, orgApi.getOrganizationStructure(), ontApi.getOntologyStructure(), targetValueRequirement, targetRequirementType, targetDrId, targetDrInstanceId, targetPropertyId, targetFieldId, null, restrictToApprovedValues, pageOffset, pageSize);
        }
        else
        {
            List<Pair<String, String>> conceptDependencyList;
            List<String> conceptIdList;
            List<String> orgIdList;
            List<String> ocIdList;
            PropertyAccessLayer propertyAccessLayer;
            FieldAccessLayer fieldAccessLayer;
            String defaultOCID;

            // Get the access layers of the target property/field.
            propertyAccessLayer = targetRecord.getAccessLayer().getPropertyAccessLayer(targetPropertyId);
            fieldAccessLayer = propertyAccessLayer.getFieldAccessLayer(targetFieldId);

            // Get the default group ot use.
            switch (targetRequirementType)
            {
                case CONTROLLED_CONCEPT:
                    defaultOCID = ((ControlledConceptRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                case UNIT_OF_MEASURE:
                    defaultOCID = ((UnitOfMeasureRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                case QUALIFIER_OF_MEASURE:
                    defaultOCID = ((QualifierOfMeasureRequirement)targetValueRequirement).getOntologyClassID();
                    break;
                default:
                    return new ArrayList<T8ConceptTerminology>();
            }

            // Create the list of accesible organization levels.
            orgIdList = getFilterOrgIdList(orgApi.getOrganizationStructure(), targetDataRequirementInstance);

            // Create a list of all groups to include (roots and their descendants).
            ocIdList = getFilterOntologyClassIdList(ontApi.getOntologyStructure(), defaultOCID, propertyAccessLayer, fieldAccessLayer);

            // Create a list of all the specific concepts to include in the filter.
            conceptIdList = getFilterConceptIdList(propertyAccessLayer, fieldAccessLayer);

            // Create the list of concept dependencies to use for filter construction.
            conceptDependencyList = getConceptDependencyList(targetValue);
            return dataHandler.retrieveDrInstanceOptions(tx, orgApi.getOrganizationStructure(), ontApi.getOntologyStructure(), targetValueRequirement, targetRequirementType, orgIdList, ocIdList, conceptIdList, conceptDependencyList, pageOffset, pageSize);
        }
    }

    public List<T8OntologyConcept> retrieveContentOntology(DataRecord record, boolean includeDescendants) throws Exception
    {
        Set<String> conceptIds;
        T8OntologyApi ontApi;

        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        conceptIds = DocumentHandler.getValueConceptIds(record);
        if (includeDescendants)
        {
            for (DataRecord descendant : record.getDescendantRecords())
            {
                conceptIds.addAll(DocumentHandler.getValueConceptIds(descendant));
            }
        }

        return ontApi.retrieveConcepts(conceptIds, true, true, true, true, true);
    }

    private List<String> getFilterOrgIdList(T8OrganizationStructure organizationStructure, DataRequirementInstance targetDRInstance)
    {
        List<T8OntologyLink> drInstanceLinks;
        T8OntologyConcept drInstanceOntology;
        List<String> orgIDList;
        String drInstanceOrgID;

        // Get the organization to which the Data Requirement instance is linked.
        drInstanceOntology = (T8OntologyConcept)targetDRInstance.getOntology();
        if (drInstanceOntology == null) throw new RuntimeException("No Ontology Found in DR Instance object: " + targetDRInstance);
        drInstanceLinks = drInstanceOntology.getOntologyLinks();
        if (drInstanceLinks.isEmpty()) throw new RuntimeException("No Organization Links Found in DR Instance Ontology object: " + drInstanceOntology);
        drInstanceOrgID = drInstanceLinks.get(0).getOrganizationID();

        // Create the list of accesible organization levels.
        orgIDList = new ArrayList<String>();
        orgIDList.addAll(organizationStructure.getOrganizationLineageIDList(context.getSessionContext().getOrganizationIdentifier())); // Add a filter to include all concepts linked to the user's session organization lineage.
        orgIDList.retainAll(organizationStructure.getOrganizationLineageIDList(drInstanceOrgID)); // In the list of organizations accessible by the user, retain only those that are also in the lineage of the organization to which the document is linked.
        return orgIDList;
    }

    private List<String> getFilterOntologyClassIdList(T8OntologyStructure ontologyStructure, String defaultOcId, PropertyAccessLayer propertyAccessLayer, FieldAccessLayer fieldAccessLayer)
    {
        Set<String> accessLayerFilterConceptGroupIDSet;
        List<String> ocIdSet;

        // Create the filter ID sets.
        if (fieldAccessLayer != null)
        {
            accessLayerFilterConceptGroupIDSet = fieldAccessLayer.getFilterConceptGroupIDSet();
        }
        else
        {
            accessLayerFilterConceptGroupIDSet = propertyAccessLayer.getFilterConceptGroupIDSet();
        }

        // Create a list of all groups to include (roots and their descendants).
        ocIdSet = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(accessLayerFilterConceptGroupIDSet))
        {
            for (String ocId : accessLayerFilterConceptGroupIDSet)
            {
                // Get the list of ODT_ID's to use for retrieval.
                ocIdSet.add(ocId); // Add the group itself.
                ocIdSet.addAll(ontologyStructure.getDescendantClassIDList(ocId)); // Add the descendants of the group.
            }
        }
        else
        {
            // User the default OC_ID.
            ocIdSet.add(defaultOcId); // Add the group itself.
            ocIdSet.addAll(ontologyStructure.getDescendantClassIDList(defaultOcId)); // Add the descendants of the group.
        }

        // Return the list of groups to filter by.
        return ocIdSet;
    }

    private List<String> getFilterConceptIdList(PropertyAccessLayer propertyAccessLayer, FieldAccessLayer fieldAccessLayer)
    {
        Set<String> accessLayerFilterConceptIDSet;
        List<String> conceptIdList;

        // Create the filter ID sets.
        if (fieldAccessLayer != null)
        {
            accessLayerFilterConceptIDSet = fieldAccessLayer.getFilterConceptIDSet();
        }
        else
        {
            accessLayerFilterConceptIDSet = propertyAccessLayer.getFilterConceptIDSet();
        }

        // Create a list of all the specific concepts to include in the filter.
        conceptIdList = new ArrayList<String>();
        if (!CollectionUtilities.isNullOrEmpty(accessLayerFilterConceptIDSet))
        {
            conceptIdList.addAll(accessLayerFilterConceptIDSet);
        }

        // Return the list of concepts to filter by.
        return conceptIdList;
    }

    private List<Pair<String, String>> getConceptDependencyList(RecordValue targetValue)
    {
        // Make sure the target value is a dependency value type. The current implementation of this check is lacking and therefore uses instanceof.
        if (targetValue instanceof DependencyValue)
        {
            Map<DataDependency, String> requiredConceptIds;
            List<Pair<String, String>> dependencyList;

            // Get the dependency values (we don't specify a record provider, as all required records must be present in the object model).
            requiredConceptIds = ((DependencyValue)targetValue).getDependencyParentConceptIds(null);

            // Create the new concept dependency set.
            dependencyList = new ArrayList<Pair<String, String>>();
            for (DataDependency dependencyRequirement : requiredConceptIds.keySet())
            {
                String dependencyConceptId;
                Pair<String, String> dependency;

                dependencyConceptId = requiredConceptIds.get(dependencyRequirement);
                dependency = new Pair<String, String>(dependencyRequirement.getRelationId(), dependencyConceptId);
                if (!dependencyList.contains(dependency)) dependencyList.add(dependency);
            }

            // Sort the dependency list (this step is important for consistency, since the order of the list depends on keys fetched from a map).
            Collections.sort(dependencyList, new DefaultPairComparator(true, true, false));
            return dependencyList;
        }
        else return new ArrayList<>();
    }

    /**
     * Retrieves the {@code RecordAttribute} for the specified record ID and
     * attribute ID, if it exists. If not, {@code null} is returned.
     *
     * @param recordId The {@code String} record ID for the attribute
     * @param attributeId The {@code String} attribute ID
     *
     * @return The {@code RecordAttribute}, or {@code null} if it does not exist
     *
     * @throws Exception If there is any error during the retrieval of the
     *      attribute
     */
    public RecordAttribute retrieveRecordAttribute(String recordId, String attributeId) throws Exception
    {
        return dataHandler.retrieveRecordAttribute(tx, recordId, attributeId);
    }

    public Map<String, Object> retrieveRecordAttributes(String recordId) throws Exception
    {
        List<RecordAttribute> existingAttributes;
        Map<String, Object> attributeMap;

        // Retrieve the existing attributes to be deleted.
        existingAttributes = dataHandler.retrieveRecordAttributes(tx, recordId, null);
        attributeMap = new HashMap<>();
        for (RecordAttribute attribute : existingAttributes)
        {
            attributeMap.put(attribute.getAttributeId(), attribute.getValue());
        }

        return attributeMap;
    }

    public void saveRecordAttribute(String recordId, String attributeId, Object attributeValue) throws Exception
    {
        RecordAttribute existingAttribute;
        T8DataRecordHistoryApi hisApi;

        // Get the history API.
        hisApi = tx.getApi(T8DataRecordHistoryApi.API_IDENTIFIER);

        // First see if the attribute already exists (if it does, we will need the data for history purposes).
        existingAttribute = dataHandler.retrieveRecordAttribute(tx, recordId, attributeId);
        if (existingAttribute != null)
        {
            RecordAttribute newAttribute;

            newAttribute = new RecordAttribute(attributeId, attributeValue);
            newAttribute.setRecordId(recordId);
            newAttribute.setRootRecordId(existingAttribute.getRootRecordId());

            // Update the existing attribute.
            dataHandler.updateRecordAttribute(tx, existingAttribute.getRootRecordId(), recordId, attributeId, attributeValue);

            // Insert the history entry.
            hisApi.insertAttributeUpdate(new T8Timestamp(System.currentTimeMillis()), existingAttribute, newAttribute);
        }
        else
        {
            RecordAttribute newAttribute;
            String rootRecordId;

            // Get the root record id for the new attribute.
            rootRecordId = dataHandler.retrieveRootRecordId(tx, recordId);
            newAttribute = new RecordAttribute(attributeId, attributeValue);
            newAttribute.setRecordId(recordId);
            newAttribute.setRootRecordId(rootRecordId);

            // Update the existing attribute.
            dataHandler.insertRecordAttribute(tx, rootRecordId, recordId, attributeId, attributeValue);

            // Insert the history entry.
            hisApi.insertAttributeInsert(new T8Timestamp(System.currentTimeMillis()), newAttribute);
        }
    }

    public void saveRecordAttributes(String recordId, Map<String, Object> attributeMap) throws Exception
    {
        for (String attributeId : attributeMap.keySet())
        {
            saveRecordAttribute(recordId, attributeId, attributeMap.get(attributeId));
        }
    }

    public void deleteRecordAttribute(String recordId, String attributeId) throws Exception
    {
        RecordAttribute existingAttribute;

        // First see if the attribute already exists (if it does, we will need the data for history purposes).
        existingAttribute = dataHandler.retrieveRecordAttribute(tx, recordId, attributeId);
        if (existingAttribute != null)
        {
            T8DataRecordHistoryApi hisApi;

            // Get the history API.
            hisApi = tx.getApi(T8DataRecordHistoryApi.API_IDENTIFIER);
            dataHandler.deleteRecordAttribute(tx, recordId, attributeId);
            hisApi.insertAttributeDeletion(new T8Timestamp(System.currentTimeMillis()), existingAttribute);
        }
    }

    public void deleteRecordAttributes(String recordId, List<String> attributeIds) throws Exception
    {
        List<RecordAttribute> existingAttributes;
        T8DataRecordHistoryApi hisApi;
        T8Timestamp timestamp;

        // Get the history API.
        hisApi = tx.getApi(T8DataRecordHistoryApi.API_IDENTIFIER);

        // Retrieve the existing attributes to be deleted.
        existingAttributes = dataHandler.retrieveRecordAttributes(tx, recordId, attributeIds);

        // Delete the existing attributes.
        dataHandler.deleteRecordAttributes(tx, recordId, attributeIds);

        // Add the history entries for the deletions.
        timestamp = new T8Timestamp(System.currentTimeMillis());
        for (RecordAttribute deletedAttribute : existingAttributes)
        {
            hisApi.insertAttributeDeletion(timestamp, deletedAttribute);
        }
    }

    public boolean deleteFile(String fileId) throws Exception
    {
        return documentDataHandler.deleteDataRecord(tx, fileId);
    }

    public T8DataFileSaveResult saveFileStateChange(DataRecord file) throws Exception
    {
        return saveFileStateChange(file.getSavedState(), file);
    }

    /**
     * Saves all changes identified between the input fromState and toState files.
     * This method also normalizes the ontology of all updated records in the input
     * toState file.
     * @param fromState The state specifying the starting point for change identification.
     * @param toState The state specifying the end point for change identification.
     * @return The result of this save operation, indicating the success or failure along
     * with any accompanying information.
     * @throws Exception
     */
    public T8DataFileSaveResult saveFileStateChange(DataRecord fromState, DataRecord toState) throws Exception
    {
        T8DataFileValidationReport validationReport;
        T8DataRecordOntologyHandler ontologyHandler;
        List<DataRecord> toStateChangedRecordList;
        T8DataRecordAccessHandler accessHandler;
        T8DataFileAlteration initialAlteration;
        T8DataRecordValidator recordValidator;
        T8PrescribedValueApi pvApi;

        // Log the start of this operation.
        stats.logExecutionStart("saveFileStateChange");

        // Get the Api's that we need for the operation.
        pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);

        // Get the access handler to use for the operation.
        accessHandler = getAccessHandler(toState);

        // Determine an initial alteration from the input from and to states.
        initialAlteration = T8DataFileAlteration.getNormalizedDataFileAlteration(fromState, toState);
        toStateChangedRecordList = initialAlteration.getInsertedAndUpdatedRecords();

        // Sort the records from root to leaf.
        Collections.sort(toStateChangedRecordList, new DataRecordLevelComparator());

        // Create the record ontology handler.
        ontologyHandler = new T8DataRecordOntologyHandler(tx);

        // Replace old prescribed values with the correct new values.
        stats.logExecutionStart("REPLACE_PRESCRIBED_VALUES");
        pvApi.replaceOldPrescribedValues(toStateChangedRecordList);
        stats.logExecutionEnd("REPLACE_PRESCRIBED_VALUES");

        // Resolve all concepts for which and id is not specified.
        stats.logExecutionStart("RESOLVE_CONCEPTS");
        ontologyHandler.resolveDataRecordConcepts(tx, toStateChangedRecordList);
        stats.logExecutionEnd("RESOLVE_CONCEPTS");

        // Validate the data record.
        stats.logExecutionStart("RECORD_VALIDATION");
        recordValidator = new T8DataRecordValidator(tx);
        recordValidator.setAccessHandler(accessHandler);
        validationReport = recordValidator.validateDataRecords(toState);
        stats.logExecutionEnd("RECORD_VALIDATION");

        // If validation failed, return the result.
        if (validationReport.hasErrors())
        {
            stats.logExecutionEnd("saveFileStateChange");
            return new T8DataFileSaveResult(null, toState, validationReport, false, initialAlteration);
        }
        else // Validation passed, so continue to save the data.
        {
            T8DataFileSaveResult result;

            // Create and save a new alteration that will now take all resolved data from previous steps into account.
            result =  saveFileAlteration(T8DataFileAlteration.getNormalizedDataFileAlteration(fromState, toState));
            stats.logExecutionEnd("saveFileStateChange");
            return result;
        }
    }

    /**
     * This method validates (optional) and saves all of the Data Record changes
     * supplied.  After the changes have been saved the ontology or descriptions
     * for the affected Data Records can be generated if required.  If
     * description/ontology generation is specified but non-synchronous
     * generation is set, the transaction passed to this method will be
     * committed after successful updates to the affected records and BEFORE the
     * description/ontology generation is requested from the asynchronous
     * service.  This is necessary to ensure that the service uses the updated
     * data.  In all other scenarios, the transaction will not be touched.
     * @param alteration The data file alteration to save.  The alteration
     * object specifies insert, updated and deleted records in the file.
     * @return The result of the operation.
     * @throws java.lang.Exception
     */
    private T8DataFileSaveResult saveFileAlteration(T8DataFileAlteration alteration) throws Exception
    {
        T8ConceptAlteration conceptAlteration;
        List<DataRecord> changedRecordList;
        T8PrescribedValueApi pvApi;
        T8DataStringApi dsApi;
        T8OntologyApi ontApi;
        DataRecord dataFile;

        // Log the start of the operation.
        stats.logExecutionStart("saveFileAlteration");

        // Get the api's that we need for the operation.
        dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        pvApi = tx.getApi(T8PrescribedValueApi.API_IDENTIFIER);

        // Get the access handler to use for the operation.
        dataFile = alteration.getDataFile();

        // Create a list of Data Records that were changed (inserts and updates - sequence important)
        changedRecordList = alteration.getInsertedAndUpdatedRecords();

        // Sort the records from root to leaf.
        Collections.sort(changedRecordList, new DataRecordLevelComparator());

        // Handle ontology changes.
        stats.logExecutionStart("SAVE_CONCEPT_ALTERATION");
        conceptAlteration = alteration.getConceptAlteration();
        conceptAlteration.clearInsertedTerms(); // Term and definition updates are handled by a separate output stream.
        conceptAlteration.clearUpdatedTerms(); // Term and definition updates are handled by a separate output stream.
        conceptAlteration.clearInsertedDefinitions(); // Term and definition updates are handled by a separate output stream.
        conceptAlteration.clearUpdatedDefinitions(); // Term and definition updates are handled by a separate output stream.
        ontApi.saveConceptAlteration(conceptAlteration, true, true, false);
        stats.logExecutionEnd("SAVE_CONCEPT_ALTERATION");

        // Save all Data Record changes (inserts and updates).
        for (DataRecord dataRecord : changedRecordList)
        {
            // Save prescribed values contained by the document.
            stats.logExecutionStart("SAVE_PRESCRIBED_VALUES");
            pvApi.savePrescribedValues(dataRecord);
            stats.logExecutionEnd("SAVE_PRESCRIBED_VALUES");

            // Now save the Data Record document.
            // Because of the sequence of the changedRecordList, all inserts are processed before
            // updates.  This is important in order for auditing data be logged in a chronologic order.
            stats.logExecutionStart("RECORD_DOCUMENT_PERSISTENCE");
            if (alteration.isInsert(dataRecord.getID()))
            {
                documentDataHandler.insertDataRecord(tx, dataRecord);
            }
            else
            {
                documentDataHandler.saveDataRecord(tx, dataRecord);
            }
            stats.logExecutionEnd("RECORD_DOCUMENT_PERSISTENCE");
        }

        // Handle ontology deletions.
        stats.logExecutionStart("SAVE_CONCEPT_DELETION");
        ontApi.saveConceptAlteration(conceptAlteration, false, false, true);
        stats.logExecutionEnd("SAVE_CONCEPT_DELETION");

        // Insert new particle settings.
        stats.logExecutionStart("SAVE_DATA_STRING_PARTICLES");
        for (T8DataStringParticleSettings settings : alteration.getInsertedParticleSettings())
        {
            dsApi.insertDataStringParticleSettings(settings);
        }

        // Update particle settings.
        for (T8DataStringParticleSettings settings : alteration.getUpdatedParticleSettings())
        {
            dsApi.updateDataStringParticleSettings(settings);
        }

        // Delete particle settings.
        for (T8DataStringParticleSettings settings : alteration.getDeletedParticleSettings())
        {
            dsApi.deleteDataStringParticleSettings(settings);
        }
        stats.logExecutionEnd("SAVE_DATA_STRING_PARTICLES");

        // Set data object states (if applicable).
        stats.logExecutionStart("RECORD_STATE_UPDATE");
        for (DataRecord insertedRecord : alteration.getInsertedRecords())
        {
            String dataObjectId;
            String stateId;

            dataObjectId = insertedRecord.getAccessLayer().getDataObjectId();
            stateId = insertedRecord.getAccessLayer().getStateId();
            if (stateId != null)
            {
                T8DataObjectApi objApi;

                objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
                objApi.saveState(dataObjectId, insertedRecord.getID(), stateId);
            }
        }
        stats.logExecutionEnd("RECORD_STATE_UPDATE");

        // Process data file output streams.
        stats.logExecutionStart("FILE_OUTPUT_STREAMS");
        for (String streamId : fileOutputStreams.keySet())
        {
            T8DataFileOutputStream outputStream;

            stats.logExecutionStart("FILE_OUTPUT_STREAM:" + streamId);
            outputStream = fileOutputStreams.get(streamId).get();
            outputStream.write(dataFile, alteration, ArrayLists.typeSafeList(RecordMetaDataType.ALL));
            stats.logExecutionEnd("FILE_OUTPUT_STREAM:" + streamId);
        }
        stats.logExecutionEnd("FILE_OUTPUT_STREAMS");

        // Log the end of the operation.
        stats.logExecutionEnd("saveFileAlteration");
        return new T8DataFileSaveResult(null, dataFile, null, true, alteration);
    }

    public T8DataFileValidationReport validateFileStateChange(DataRecord fromState, DataRecord toState) throws Exception
    {
        T8DataRecordValidator recordValidator;
        T8DataRecordAccessHandler accessHandler;

        // Create the validator.
        recordValidator = new T8DataRecordValidator(tx);

        // Set the access identifier.
        accessHandler = getAccessHandler(toState);
        if (accessHandler != null) recordValidator.setAccessHandler(accessHandler);

        // Validate the record and return the results.
        return recordValidator.validateDataRecords(toState);
    }

    public DataRecord synchronizeFiles(String sourceFileId, String destinationFileId) throws Exception
    {
        DataRecord sourceFile;
        DataRecord destinationFile;

        // Get the source and destination files.
        sourceFile = retrieveFile(sourceFileId, null, true, false, false, true, false, true);
        destinationFile = retrieveFile(destinationFileId, null, true, false, false, true, false, true);
        return synchronizeFiles(sourceFile, destinationFile);
    }

    /**
     * Synchronizes the specified destination data file so that it matches the
     * content of the supplied source record EXACTLY.  In order for this method
     * to work properly, the supplied source record must be tagged with the
     * corresponding destination record ID from which its content was originally
     * copied.
     * @param sourceFile The record from which content will be fetched and
     * added/substituted into the destination record.
     * @param destinationFile The record that will be synchronized so that
     * its contents matches the content of the supplied source record.  All
     * existing record ID's will remain.
     * @return The new destination record, containing exactly the content of the
     * supplied source record.
     * @throws Exception
     */
    public DataRecord synchronizeFiles(DataRecord sourceFile, DataRecord destinationFile) throws Exception
    {
        Map<String, String> attachmentIdentifierMapping;
        T8DataRecordSynchronizer synchronizer;
        DataRecord newDestinationFile;
        String mergeFileContextInstanceID;

        // Generate an ID for the merge file context instance.
        mergeFileContextInstanceID = T8IdentifierUtilities.createNewGUID();

        // Synchronize the records.
        synchronizer = new T8DataRecordSynchronizer();
        newDestinationFile = synchronizer.synchronizeDataRecords(destinationFile, sourceFile);
        attachmentIdentifierMapping = synchronizer.getAttachmentIdentifierMapping();

        // Export affected attachments so they can be imported when the destination records are saved.
        exportAttachments(mergeFileContextInstanceID, attachmentIdentifierMapping, newDestinationFile);

        // Save the destination data file and return it.
        saveFileStateChange(destinationFile, newDestinationFile);
        return newDestinationFile;
    }

    public List<DataRecord> retrieveFiles(List<String> recordIDList, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings) throws Exception
    {
        List<DataRecord> dataRecordList;

        dataRecordList = new ArrayList<>();
        for (String recordID : recordIDList)
        {
            DataRecord retrievedRecord;

            retrievedRecord = retrieveFile(recordID, languageID, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
            if (retrievedRecord != null) dataRecordList.add(retrievedRecord);
        }

        return dataRecordList;
    }

    public DataRecord retrieveFile(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings) throws Exception
    {
        DataRecord dataFile;

        // Retrieve the document.
        stats.logExecutionStart("retrieveFile");
        dataFile = documentDataHandler.retrieveDataFile(tx, recordId);
        if (dataFile != null)
        {
            T8DataRecordAccessHandler accessHandler;

            // Construct the access handler to use for the retrieval operation.
            accessHandler = getAccessHandler(dataFile);

            // Add record meta data.
            addRecordData(dataFile, languageId, includeOntology, false, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);

            // Save the retrieved state of the file.  It is crucial that the state is saved at this point and not after access layer application.
            // If access layers alter the record's state, those changes must be detectable from the state of the record as retrieved.
            dataFile.saveState();

            // Set the record static state.
            stats.logExecutionStart("SET_STATIC_STATE");
            accessHandler.setStaticState(dataFile);
            stats.logExecutionEnd("SET_STATIC_STATE");

            // Refresh the record dynamic state.
            stats.logExecutionStart("SET_DYNAMIC_STATE");
            accessHandler.refreshDynamicState(dataFile);
            stats.logExecutionEnd("SET_DYNAMIC_STATE");

            // Add record meta data.
            stats.logExecutionStart("SET_TERMINOLOGY");
            addRecordData(dataFile, languageId, false, includeTerminology, false, false, false, false);
            stats.logExecutionEnd("SET_TERMINOLOGY");

            // Return the root Data Record.
            stats.logExecutionEnd("retrieveFile");
            return dataFile;
        }
        else
        {
            stats.logExecutionEnd("retrieveFile");
            return null;
        }
    }

    public List<DataRecord> retrieveFilesByContentRecord(List<String> recordIdList, String languageID, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings) throws Exception
    {
        List<DataRecord> dataRecordList;

        stats.logExecutionStart("retrieveDataFilesByContentRecord");
        dataRecordList = new ArrayList<>();
        for (String recordId : recordIdList)
        {
            DataRecord retrievedRecord;

            retrievedRecord = retrieveFileByContentRecord(recordId, languageID, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
            if (retrievedRecord != null) dataRecordList.add(retrievedRecord);
        }
        stats.logExecutionEnd("retrieveDataFilesByContentRecord");

        return dataRecordList;
    }

    public DataRecord retrieveFileByContentRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings) throws Exception
    {
        DataRecord dataFile;

        // Retrieve the document.
        stats.logExecutionStart("RECORD_RETRIEVAL");
        dataFile = documentDataHandler.retrieveDataFileByContentRecord(tx, recordId);
        stats.logExecutionEnd("RECORD_RETRIEVAL");

        // Add associated data and execute access layer.
        if (dataFile != null)
        {
            T8DataRecordAccessHandler accessHandler;

            // Add record meta data.
            stats.logExecutionStart("SET_ASSOCIATED_DATA");
            addRecordData(dataFile, languageId, includeOntology, false, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);
            stats.logExecutionEnd("SET_ASSOCIATED_DATA");

            // Save the retrieved state of the file.
            dataFile.saveState();

            // Set the record static state.
            stats.logExecutionStart("SET_STATIC_STATE");
            accessHandler = getAccessHandler(dataFile);
            accessHandler.setStaticState(dataFile);
            stats.logExecutionEnd("SET_STATIC_STATE");

            // Refresh the record dynamic state.
            stats.logExecutionStart("SET_DYNAMIC_STATE");
            accessHandler.refreshDynamicState(dataFile);
            stats.logExecutionEnd("SET_DYNAMIC_STATE");

            // Add record terminology data (after all access layer updates).
            stats.logExecutionStart("SET_TERMINOLOGY");
            addRecordData(dataFile, languageId, false, includeTerminology, false, false, false, false);
            stats.logExecutionEnd("SET_TERMINOLOGY");

            return dataFile;
        }
        else return null;
    }

    public List<DataRecord> retrieveRecords(List<String> recordIdList, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeSubRecords) throws Exception
    {
        List<DataRecord> dataRecordList;

        dataRecordList = new ArrayList<>();
        for (String recordID : recordIdList)
        {
            DataRecord retrievedRecord;

            retrievedRecord = retrieveRecord(recordID, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings, includeSubRecords);
            if (retrievedRecord != null) dataRecordList.add(retrievedRecord);
        }

        return dataRecordList;
    }

    public DataRecord retrieveRecord(String recordId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDRComments, boolean includeParticleSettings, boolean includeDescendants) throws Exception
    {
        DataRecord dataRecord;

        // Retrieve the document.
        dataRecord = documentDataHandler.retrieveDataRecord(tx, recordId, includeDescendants);
        if (dataRecord != null)
        {
            T8DataRecordAccessHandler accessHandler;

            // Add record meta data.
            addRecordData(dataRecord, languageId, includeOntology, false, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings);

            // Set the record static state.
            accessHandler = getAccessHandler(dataRecord);
            stats.logExecutionStart("SET_STATIC_STATE");
            accessHandler.setStaticState(dataRecord);
            stats.logExecutionEnd("SET_STATIC_STATE");

            // Refresh the record dynamic state.
            stats.logExecutionStart("SET_DYNAMIC_STATE");
            accessHandler.refreshDynamicState(dataRecord);
            stats.logExecutionEnd("SET_DYNAMIC_STATE");

            // Add terminology data.
            stats.logExecutionStart("SET_TERMINOLOGY");
            addRecordData(dataRecord, languageId, false, includeTerminology, false, false, false, false);
            stats.logExecutionEnd("SET_TERMINOLOGY");

            // Return the root Data Record.
            return dataRecord;
        }
        else return null;
    }

    public void addRecordData(DataRecord dataFile, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescripions, boolean includeAttachmentDetails, boolean includeDrComments, boolean includeParticleSettings) throws Exception
    {
        List<T8DataRequirementComment> drComments;
        List<DataRecord> dataFileRecords;
        T8DataStringApi dsApi = null;
        T8OntologyApi ontApi = null;
        Set<String> conceptIdSet;

        // Get the Api's that we need for the operation and only when if needed.
        if (includeOntology) ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        if (includeParticleSettings) dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);

        // Create a set to hold all concept ID's.
        conceptIdSet = new HashSet<>();

        // Create a list of all retrieved records.
        dataFileRecords = dataFile.getDescendantRecords();
        dataFileRecords.add(dataFile);

        // Retrieve ontology if required.
        if (includeOntology)
        {
            List<T8OntologyConcept> ontologyList;
            Set<String> ontologyConceptIDSet;

            // Create a set of concepts ID's of all records and DR Instances.
            ontologyConceptIDSet = new HashSet<String>();
            for (DataRecord dataFileRecord : dataFileRecords)
            {
                ontologyConceptIDSet.add(dataFileRecord.getID());
                ontologyConceptIDSet.add(dataFileRecord.getDataRequirementInstanceID());
            }

            // Retrieve the ontology for the set of concepts.
            stats.logExecutionStart("RETRIEVE_ONTOLOGY");
            ontologyList = ontApi.retrieveConcepts(ontologyConceptIDSet, true, true, true, true, true);
            stats.logExecutionEnd("RETRIEVE_ONTOLOGY");

            // Distribute the retrieved ontology to the corresponding documents.
            for (DataRecord retrievedRecord : dataFileRecords)
            {
                for (T8OntologyConcept ontology : ontologyList)
                {
                    String conceptID;

                    conceptID = ontology.getID();
                    if (conceptID.equals(retrievedRecord.getID()))
                    {
                        // The ontology belongs to the record, so set it.
                        retrievedRecord.setOntology(ontology);
                    }
                    else if (conceptID.equals(retrievedRecord.getDataRequirementInstanceID()))
                    {
                        // The ontology belongs to the data requirement instance.
                        retrievedRecord.getDataRequirementInstance().setOntology(ontology);
                    }
                }
            }
        }

        // Retrieve DR Comments if required.
        if (includeDrComments)
        {
            T8DataRequirementApi drApi;
            T8OrganizationApi orgApi;
            List<String> orgIdList;
            List<String> languageIdList;
            Set<String> drInstanceIdSet;
            Set<String> drIdSet;

            // Get the organization ID list to use for DR comment retrieval.
            drApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            orgIdList = orgApi.getOrganizationStructure().getNode(context.getSessionContext().getOrganizationIdentifier()).getOrganizationIDPath();
            languageIdList = ArrayLists.typeSafeList(context.getSessionContext().getContentLanguageIdentifier());
            drInstanceIdSet = dataFile.getDataRequirementInstanceIDSet();
            drIdSet = dataFile.getDataRequirementIDSet();
            stats.logExecutionStart("retrieveDataRequirementComments");
            drComments = drApi.retrieveDataRequirementComments(orgIdList, languageIdList, drIdSet, drInstanceIdSet, null);
            stats.logExecutionEnd("retrieveDataRequirementComments");
        }
        else drComments = null;

        // Loop through each of the data file records and add the associated data requested.
        for (DataRecord dataFileRecord : dataFileRecords)
        {
            // Add the descriptions if required.
            if (includeDescripions)
            {
                List<T8DataStringFilter> dataStringFilters;
                List<T8DataString> dataStrings;

                // Retrieve the descriptions applicable to this record and the requested language.
                stats.logExecutionStart("retrieveDataStrings");
                dataStringFilters = new ArrayList<>();
                dataStringFilters.add(new T8DataStringFilterBuilder().record(dataFileRecord.getID()).language(languageId).build());
                dataStrings = documentDataHandler.retrieveDataStrings(tx, dataStringFilters);
                dataFileRecord.setDataStrings(dataStrings);
                stats.logExecutionEnd("retrieveDataStrings");
            }

            // Add data string particle settings if required.
            if (includeParticleSettings)
            {
                // Retrieve the descriptions applicable to this record and the requested language.
                stats.logExecutionStart("setDataStringParticleSettings");
                dataFileRecord.setDataStringParticleSettings(dsApi.retrieveDataStringParticleSettings(dataFileRecord.getID()));
                stats.logExecutionEnd("setDataStringParticleSettings");
            }

            // Add attachment details if required.
            if (includeAttachmentDetails)
            {
                stats.logExecutionStart("setAttachmentDetails");
                dataFileRecord.setAttachmentDetails(documentDataHandler.retrieveDataRecordAttachmentDetails(tx, dataFileRecord.getID()));
                stats.logExecutionEnd("setAttachmentDetails");
            }

            // Add DR comments if required.
            if (includeDrComments)
            {
                DataRequirementInstance drInstance;

                drInstance = dataFileRecord.getDataRequirementInstance();
                for (T8DataRequirementComment comment : drComments)
                {
                    if (!drInstance.containsComment(comment.getId()))
                    {
                        String commentDRInstanceID;

                        commentDRInstanceID = comment.getDrInstanceId();
                        if (drInstance.getConceptID().equals(commentDRInstanceID))
                        {
                            drInstance.addComment(comment);
                        }
                        else if ((commentDRInstanceID == null) && (drInstance.getDataRequirement().getConceptID().equals(comment.getDrID())))
                        {
                            drInstance.addComment(comment);
                        }
                    }
                }
            }

            // Add the record Concept ID's to the set.
            conceptIdSet.addAll(DocumentHandler.getAllConceptIds(dataFileRecord));
        }

        // Add the terminology if required.
        if (includeTerminology)
        {
            T8ConceptTerminologyList terminologyList;
            TerminologyProvider terminologyProvider;
            T8TerminologyApi trmApi;

            // We retrieve the terminology for all records simultaneously to save on the number of retrievals.
            stats.logExecutionStart("getTerminology");
            trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            terminologyProvider = trmApi.getTerminologyProvider();
            terminologyList = terminologyProvider.getTerminology(ArrayLists.typeSafeList(languageId), new ArrayList<>(conceptIdSet));
            dataFile.setContentTerminology(terminologyList, true);
            stats.logExecutionEnd("getTerminology");
        }
    }

    public T8DataIterator<String> getRecordIdIterator(String drInstanceID, String drID, List<PropertyValueStringCriterion> criteria) throws Exception
    {
        return documentDataHandler.getDataRecordIDIterator(tx, drInstanceID, drID, criteria);
    }

    public List<String> retrieveRecordIdByCode(String rootRecordId, String dataRequirementId, String dataRequirementInstanceId, String codeTypeId, String code) throws Exception
    {
        return documentDataHandler.retrieveRecordIdByCode(tx, rootRecordId, dataRequirementId, dataRequirementInstanceId, codeTypeId, code);
    }

    public String retrieveRecordDrInstanceId(String recordID) throws Exception
    {
        return documentDataHandler.retrieveDataRecordDRInstanceID(tx, recordID);
    }

    public DataRecord createRecord(DataRecord dataFile, String parentRecordId, String propertyId, String fieldId, String drInstanceId, String languageId, boolean includeTerminology, boolean includeDrComments) throws Exception
    {
        DataRequirementInstance dataRequirementInstance;
        T8DataRequirementApi drApi;

        drApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
        dataRequirementInstance = drApi.retrieveDataRequirementInstance(drInstanceId, null, true, false);
        if (dataRequirementInstance != null)
        {
            T8DataRecordOntologyHandler ontologyHandler;
            List<T8OntologyLink> drInstanceLinks;
            T8DataRecordAccessHandler accessHandler;
            Set<String> orgLanguageIdSet;
            T8OntologyConcept ontology;
            T8OrganizationApi orgApi;
            DataRecord newRecord;
            String recordId;

            // Generate the new record id.
            recordId = T8IdentifierUtilities.createNewGUID();

            // Create ontology for the Data Record Concept.
            ontology = new T8OntologyConcept(recordId, T8OntologyConceptType.DATA_RECORD, false, true);

            // Create the new record and persist it (if required).
            newRecord = new DataRecord(dataRequirementInstance);
            newRecord.setID(recordId);
            newRecord.setOntology(ontology);

            // Retrieve the list of organization language ID's so that we know what terminology we need to save.
            orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            orgLanguageIdSet = orgApi.getLanguageIdSet();

            // Now make sure the record is linked according to its Data Requirement Instance.
            drInstanceLinks = dataRequirementInstance.getOntology().getOntologyLinks();
            ontologyHandler = new T8DataRecordOntologyHandler(tx);
            ontologyHandler.normalizeRecordTerminology(newRecord, orgLanguageIdSet);
            ontologyHandler.normalizeRecordOntologyLinks(newRecord, drInstanceLinks);

            // Add record meta data.
            addRecordData(newRecord, languageId, false, false, false, false, includeDrComments, false);

            // Add the new record to the supplied root (if any).
            if (dataFile != null)
            {
                DataRecord parentDataRecord;

                // Find the parent record and add the new reference and sub-record to it.
                parentDataRecord = dataFile.findDataRecord(parentRecordId);
                if (parentDataRecord != null)
                {
                    parentDataRecord.addDocumentReference(propertyId, fieldId, drInstanceId, null, recordId);
                    parentDataRecord.addSubRecord(newRecord);
                }
                else throw new RuntimeException("Parent Record not found: " + parentRecordId);

                // Use the access definition to set initial values.
                accessHandler = getAccessHandler(dataFile);
                stats.logExecutionStart("SET_INITIAL_STATE");
                accessHandler.setInitialState(newRecord);
                stats.logExecutionEnd("SET_INITIAL_STATE");

                stats.logExecutionStart("SET_STATIC_STATE");
                accessHandler.setStaticState(newRecord);
                stats.logExecutionEnd("SET_STATIC_STATE");

                stats.logExecutionStart("SET_DYNAMIC_STATE");
                accessHandler.refreshDynamicState(newRecord);
                stats.logExecutionEnd("SET_DYNAMIC_STATE");

                stats.logExecutionStart("SET_TERMINOLOGY");
                addRecordData(newRecord, languageId, false, includeTerminology, false, false, false, false);
                stats.logExecutionEnd("SET_TERMINOLOGY");
            }
            else
            {
                // Use the access definition to set initial values.
                accessHandler = getAccessHandler(newRecord);
                stats.logExecutionStart("SET_INITIAL_STATE");
                accessHandler.setInitialState(newRecord);
                stats.logExecutionEnd("SET_INITIAL_STATE");

                stats.logExecutionStart("SET_STATIC_STATE");
                accessHandler.setStaticState(newRecord);
                stats.logExecutionEnd("SET_STATIC_STATE");

                stats.logExecutionStart("SET_DYNAMIC_STATE");
                accessHandler.refreshDynamicState(newRecord);
                stats.logExecutionEnd("SET_DYNAMIC_STATE");

                stats.logExecutionStart("SET_TERMINOLOGY");
                addRecordData(newRecord, languageId, false, includeTerminology, false, false, false, false);
                stats.logExecutionEnd("SET_TERMINOLOGY");
            }

            // Return the completed record.
            return newRecord;
        }
        else throw new Exception("Data Requirement Instance not found: " + drInstanceId);
    }

    public DataRecord changeRecordDrInstance(DataRecord dataFile, String targetRecordId, String drInstanceId, String languageId, boolean includeOntology, boolean includeTerminology, boolean includeDescriptions, boolean includeAttachmentDetails, boolean includeDrComments, boolean includeParticleSettings) throws Exception
    {
        T8DRInstanceChangeHandler recordChangeHandler;
        T8DataRecordAccessHandler accessHandler;
        DataRecord updatedFile;
        DataRecord targetRecord;

        // Get the data handler and terminology handler.
        recordChangeHandler = new T8DRInstanceChangeHandler(tx);

        // Do the Data Requirement Instance change and then return all of the retrieved data.
        updatedFile = recordChangeHandler.changeDrInstance(dataFile, targetRecordId, drInstanceId);
        targetRecord = updatedFile.findDataRecord(targetRecordId);

        // Add record meta data (exclude attachment details, as they should already be present on the input record for this operation to work properly).
        addRecordData(targetRecord, languageId, includeOntology, false, includeDescriptions, false, includeDrComments, includeParticleSettings);

        // Use the access definition (if specified) to set initial values.
        accessHandler = getAccessHandler(dataFile);
        stats.logExecutionStart("SET_INITIAL_STATE");
        accessHandler.setInitialState(targetRecord);
        stats.logExecutionEnd("SET_INITIAL_STATE");

        stats.logExecutionStart("SET_STATIC_STATE");
        accessHandler.setStaticState(targetRecord);
        stats.logExecutionEnd("SET_STATIC_STATE");

        stats.logExecutionStart("SET_DYNAMIC_STATE");
        accessHandler.refreshDynamicState(targetRecord);
        stats.logExecutionEnd("SET_DYNAMIC_STATE");

        stats.logExecutionStart("SET_TERMINOLOGY");
        addRecordData(dataFile, languageId, false, includeTerminology, false, false, false, false);
        stats.logExecutionEnd("SET_TERMINOLOGY");

        // Return the updated root Data Record.
        return updatedFile;
    }

    public T8AttachmentDetails retrieveAttachmentDetails(String attachmentId) throws Exception
    {
        return documentDataHandler.retrieveAttachmentDetails(tx, attachmentId);
    }

    public Map<String, T8AttachmentDetails> retrieveRecordAttachmentDetails(String recordId) throws Exception
    {
        return documentDataHandler.retrieveDataRecordAttachmentDetails(tx, recordId);
    }

    public T8AttachmentValidationReport validateFileAttachments(String dataFileId) throws Exception
    {
        return validateRecordAttachments(documentDataHandler.retrieveDataFileRecordIDSet(tx, dataFileId));
    }

    public T8AttachmentValidationReport validateRecordAttachments(Collection<String> recordIds) throws Exception
    {
        T8DataRecordAttachmentHandlerDefinition attachmentHandlerDefinition;
        T8AttachmentValidationReport validationReport;
        T8AttachmentHandler attachmentHandler;
        String attachmentHandlerId;
        T8OrganizationApi orgApi;

        // Get the attachment handler configured for this organization.
        orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        attachmentHandlerId = orgApi.getAttachmentHandlerId();
        attachmentHandlerDefinition = (T8DataRecordAttachmentHandlerDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, null, attachmentHandlerId, null);
        attachmentHandler = attachmentHandlerDefinition.getNewAttachmentHandlerInstance(tx);

        // Loop through all of the records in the file.
        validationReport = new T8AttachmentValidationReport();
        for (String recordId : recordIds)
        {
            Map<String, T8AttachmentDetails> attachmentDetails;

            // Loop through all attachments in this record and validate each one.
            attachmentDetails = retrieveRecordAttachmentDetails(recordId);
            for (String attachmentID : attachmentDetails.keySet())
            {
                T8AttachmentValidationResult result;

                // Validate the attachment and add the result to the report.
                result = attachmentHandler.validateAttachment(attachmentID);
                validationReport.addValidationResult(recordId, result);
            }
        }

        // Return the complete result for all records in the data file.
        return validationReport;
    }

    /**
     * This method assigns a new file context to all attachments in the supplied
     * file that are listed in the specified attachment id collection.
     * @param file The input file containing the attachment details to be updated.
     * @param attachmentIds The id's of the attachment details to be updated.
     * @param newFileContextId The new file context id to set on all targeted attachment details.
     */
    public void setAttachmentContext(DataRecord file, List<String> attachmentIds, String newFileContextId)
    {
        for (DataRecord record : file.getDataRecords())
        {
            for (String newAttachmentId : attachmentIds)
            {
                T8FileDetails attachmentDetails;

                attachmentDetails = record.getAttachmentDetails(newAttachmentId);
                if (attachmentDetails != null)
                {
                    attachmentDetails.setContextId(newFileContextId);
                }
            }
        }
    }

    /**
     * Exports the specified attachment from the local persistence store to the
     * specified file context.
     * @param attachmentId The ID of the attachment to export.
     * @param fileContextIid The file context instance to which the
     * attachment file will be exported.
     * @param fileContextId The file context to which the attachment will be
     * exported.
     * @param directoryPath The directory in the specified file context to which
     * the attachment file will be exported.
     * @param fileName The filename to use when exporting the attachment.  If
     * this parameter is null, the default filename of the attachment (as it was
     * imported) will be used.
     * @return The file details of the exported file.
     * @throws Exception
     */
    public T8AttachmentDetails exportAttachment(String attachmentId, String fileContextIid, String fileContextId, String directoryPath, String fileName) throws Exception
    {
        T8DataRecordAttachmentHandlerDefinition attachmentHandlerDefinition;
        T8AttachmentHandler attachmentHandler;
        String attachmentHandlerId;
        T8OrganizationApi orgApi;

        // Get the organization api.
        orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);

        // Get the attachment handler configured for this organization.
        attachmentHandlerId = orgApi.getAttachmentHandlerId();
        attachmentHandlerDefinition = (T8DataRecordAttachmentHandlerDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, null, attachmentHandlerId, null);
        attachmentHandler = attachmentHandlerDefinition.getNewAttachmentHandlerInstance(tx);
        return attachmentHandler.exportAttachment(attachmentId, fileContextIid, fileContextId, directoryPath, fileName);
    }

    /**
     * This method exports the attachments in the supplied attachment identifier
     * mapping from the local persistence store to the specified file context.
     * Each key in the identifier mapping must be an existing attachment ID in
     * the data base.  Each attachment is exported to the specified file context
     * using its source attachment ID but the exported file detail is then set on
     * the supplied data record using the mapped destination attachment ID from the
     * identifier mapping.  In other words, once this method has completed all
     * of the attachments in the identifier mapping (the keys) will exist as
     * files in the specified context and the corresponding destination ID (the
     * value) from the mapping will have its file details updated to point to
     * the correct exported file.
     * @param fileContextId The file context to which the attachments
     * will be exported.
     * @param attachmentIdMapping  The attachment mapping to export.
     * Each of the keys in this map must be an existing attachment in the
     * database/persistence store and each of the values must be an attachment
     * in the supplied destination data record i.e. an attachment details object
     * must exist for the in the destination record or one of its descendants
     * with a matching ID.
     * @param dataFile  The destination record on which the file details of
     * the existing attachments will be updated to point to the exported files.
     * @throws Exception
     */
    public void exportAttachments(String fileContextId, Map<String, String> attachmentIdMapping, DataRecord dataFile) throws Exception
    {
        // If merging of attachments is required export the source attachments so that they are available for import in the destination.
        if ((attachmentIdMapping != null) && (!attachmentIdMapping.isEmpty()))
        {
            for (String sourceAttachmentId : attachmentIdMapping.keySet())
            {
                String destinationAttachmentId;
                DataRecord destinationRecord;

                // Get the destination attachment ID to which the attachment to be exported is mapped.
                destinationAttachmentId = attachmentIdMapping.get(sourceAttachmentId);

                // Find the data record in the destination data file to which the new attachment belongs.
                destinationRecord = null;
                for (DataRecord destinationDescendant : dataFile.getDataRecords())
                {
                    if (destinationDescendant.getAttachmentDetails(destinationAttachmentId) != null)
                    {
                        destinationRecord = destinationDescendant;
                        break;
                    }
                }

                // Make sure we found the destination record.
                if (destinationRecord != null)
                {
                    T8AttachmentDetails exportedFileDetails;
                    T8AttachmentDetails destinationFileDetails;
                    String destinationFileName;

                    // Get the file details as they exist in the destination record.
                    destinationFileDetails = destinationRecord.getAttachmentDetails(destinationAttachmentId);
                    destinationFileName = destinationFileDetails.getFileName();

                    // Export the attachment from the source to the merge file context instance.
                    exportedFileDetails = exportAttachment(sourceAttachmentId, fileContextId, null, null, destinationFileName);

                    // Now add the details of the exported attachment to the destination record so that the attachment will be found for import when the record is persisted.
                    exportedFileDetails.setAttachmentId(destinationAttachmentId);
                    destinationRecord.addAttachmentDetails(destinationAttachmentId, exportedFileDetails);
                }
                else throw new Exception("Destination record containing attachment '" + destinationAttachmentId + "' could not be found in supplied data file: " + dataFile);
            }
        }
    }

    public void refreshDataObjects(String fileId, List<String> objectIds) throws Exception
    {
        T8DataRecordObjectOutputStream objectOutputStream;
        DataRecord dataFile;

        // Get the view output stream.
        objectOutputStream = new T8DataRecordObjectOutputStream(tx, objectIds);

        // Retrieve the data file.
        dataFile = this.retrieveFile(fileId, null, false, false, false, false, false, false);
        if (dataFile != null)
        {
            T8DataFileAlteration alteration;

            // Create an alteration and add all of the data file records as updates (so all views will get updated).
            alteration = new T8DataFileAlteration(dataFile);
            alteration.addUpdatedRecords(dataFile.getDataFileRecords());

            // Send data file to the view output stream.
            objectOutputStream.write(dataFile, alteration, EnumSet.of(RecordMetaDataType.DATA_OBJECT));
        }
    }
}