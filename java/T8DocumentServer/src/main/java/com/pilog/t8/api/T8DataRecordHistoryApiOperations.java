package com.pilog.t8.api;

import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRecordHistoryApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordHistoryApiOperations
{
    public static class RetrieveFileHistorySnapshots extends T8DefaultServerOperation
    {
        public RetrieveFileHistorySnapshots(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<String> transactionIdList;
            T8DataRecordHistoryApi drhApi;
            String fileId;

            // Get the operation parameters.
            fileId = (String)operationParameters.get(PARAMETER_FILE_ID);
            transactionIdList = (List<String>)operationParameters.get(PARAMETER_TRANSACTION_ID_LIST);

            // Get the history api.
            drhApi = tx.getApi(T8DataRecordHistoryApi.API_IDENTIFIER);

            // Return the result
            return HashMaps.createSingular(PARAMETER_SNAPSHOT_MAP, drhApi.retrieveFileHistorySnapshots(fileId, transactionIdList));
        }
    }
}
