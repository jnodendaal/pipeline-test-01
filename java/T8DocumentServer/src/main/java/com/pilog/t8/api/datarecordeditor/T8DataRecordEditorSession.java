package com.pilog.t8.api.datarecordeditor;

import com.pilog.t8.data.document.datarecord.DataRecord;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordEditorSession implements Serializable
{
    private final String id;
    private final Map<String, DataRecord> dataFiles;
    private final Map<String, DataRecord> outputStates;

    public T8DataRecordEditorSession(String id)
    {
        this.id = id;
        this.dataFiles = new HashMap<>();
        this.outputStates = new HashMap<>();
    }

    public String getId()
    {
        return this.id;
    }

    public DataRecord getFile(String fileId)
    {
        return dataFiles.get(fileId);
    }

    public void setFile(DataRecord file)
    {
        dataFiles.put(file.getId(), file);
    }

    public DataRecord removeFile(String fileId)
    {
        return dataFiles.remove(fileId);
    }

    public DataRecord getOutputState(String fileId)
    {
        return outputStates.get(fileId);
    }

    public void setOutputState(DataRecord file)
    {
        outputStates.put(file.getId(), file);
    }

    public DataRecord removeOutputState(String fileId)
    {
        return outputStates.remove(fileId);
    }
}
