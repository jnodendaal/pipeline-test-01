package com.pilog.t8.data.document.datarecord.match;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.Value;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCase.T8RecordMatchCaseType;
import com.pilog.t8.data.document.datarecord.match.T8RecordMatchCheck.T8RecordMatchCheckType;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordMatcher
{
    private final List<DataRecord> records; // Holds all records that will be compared to each other when matching is performed.
    private final List<T8RecordMatch> recordMatches; // The list of matches identified by this matcher.
    private final Map<String, String> recordFamilies; // The families to which each record belongs.
    private final Map<String, DocPathExpressionEvaluator> checkEvaluatorCache; // A cache of check evaluators used during matching.
    private final Map<String, DocPathExpressionEvaluator> caseEvaluatorCache; // A cache of case evaluators used during matching.
    private T8RecordMatchCriteria criteria; // The criteria to use when comparing records to eachother.
    private String matchInstanceId; // A unique ID identifying a match operation.  A new one is generated every time records are matched.

    public T8DataRecordMatcher()
    {
        this.records = new ArrayList<>();
        this.checkEvaluatorCache = new HashMap<String, DocPathExpressionEvaluator>();
        this.caseEvaluatorCache = new HashMap<String, DocPathExpressionEvaluator>();
        this.recordMatches = new ArrayList<>();
        this.recordFamilies = new HashMap<>();
    }

    public void setCriteria(T8RecordMatchCriteria criteria)
    {
        this.criteria = criteria;
        this.checkEvaluatorCache.clear();
        this.caseEvaluatorCache.clear();
        this.matchInstanceId = null;
    }

    public void addRecord(DataRecord record)
    {
        records.add(record);
    }

    public void addRecords(Collection<DataRecord> records)
    {
        this.records.addAll(records);
    }

    public void clearRecords()
    {
        records.clear();
        recordFamilies.clear();
        recordMatches.clear();
        matchInstanceId = null;
    }

    public List<T8RecordMatch> getRecordMatches()
    {
        return new ArrayList<>(recordMatches);
    }

    public String getMatchInstanceId()
    {
        return matchInstanceId;
    }

    public void matchRecords()
    {
        String criteriaId;

        // Generate a new instance ID.
        matchInstanceId = T8IdentifierUtilities.createNewGUID();

        // Clear existing match collections.
        recordMatches.clear();
        recordFamilies.clear();

        // Loop through all records in order to compare each one to all the others.
        criteriaId = criteria.getId();
        for (int index1 = 0; index1 < records.size(); index1++)
        {
            DataRecord record1;
            String recordId1;

            // Get the  record 1 to compare.
            record1 = records.get(index1);
            recordId1 = record1.getID();

            // Make sure record1 is filled to ensure that no empty properties mess up the sequence of values to compare.
            record1.fill(true);

            // Loop through all records after record 1 so they can be compared to record 1.
            for (int index2 = index1 + 1; index2 < records.size(); index2++)
            {
                DataRecord record2;
                String recordId2;

                // Get record2 to compare to record1.
                record2 = records.get(index2);
                recordId2 = record2.getID();

                // Make sure record2 is filled to ensure that no empty properties mess up the sequence of values to compare.
                record2.fill(true);

                // Loop through all cases in the criteria.
                for (T8RecordMatchCase matchCase : criteria.getCases())
                {
                    T8RecordMatchCaseType caseType;

                    // Get the case type.
                    caseType = matchCase.getType();
                    if (caseType == T8RecordMatchCaseType.VALUE)
                    {
                        List<Object> record1CaseDataList;
                        List<Object> record2CaseDataList;

                        // Create a list to hold the objects to be used as inputs to check expression.
                        record1CaseDataList = new ArrayList<>();
                        if (Strings.isNullOrEmpty(matchCase.getDataExpression()))
                        {
                            // No data expression is specified for the match case, so we simply add the input record to the list.
                            record1CaseDataList.add(record1);
                        }
                        else
                        {
                            DocPathExpressionEvaluator caseEvaluator;
                            Object caseResult;

                            // Get the evaluator to use for evaluation of the case data expression.
                            caseEvaluator = getEvaluator(matchCase);
                            caseResult = caseEvaluator.evaluateExpression(record1);
                            if (caseResult instanceof List)
                            {
                                record1CaseDataList.addAll((List)caseResult);
                            }
                            else if (caseResult instanceof Value)
                            {
                                record1CaseDataList.add((Value)caseResult);
                            }
                        }

                        // Create a list to hold the objects to be used as inputs to check expression.
                        record2CaseDataList = new ArrayList<>();
                        if (Strings.isNullOrEmpty(matchCase.getDataExpression()))
                        {
                            // No data expression is specified for the match case, so we simply add the input record to the list.
                            record2CaseDataList.add(record2);
                        }
                        else
                        {
                            DocPathExpressionEvaluator caseEvaluator;
                            Object caseResult;

                            // Get the evaluator to use for evaluation of the case data expression.
                            caseEvaluator = getEvaluator(matchCase);
                            caseResult = caseEvaluator.evaluateExpression(record2);
                            if (caseResult instanceof List)
                            {
                                record2CaseDataList.addAll((List)caseResult);
                            }
                            else if (caseResult instanceof Value)
                            {
                                record2CaseDataList.add((Value)caseResult);
                            }
                        }

                        // For each of the results obtained from the match case expression executed on record1, do the match checks.
                        for (Object record1CaseDataObject : record1CaseDataList)
                        {
                            boolean caseMatched = false;

                            // For each of the results obtained from the match case expression executed on record 2, do the match checks.
                            for (Object record2CaseDataObject : record2CaseDataList)
                            {
                                // Loop through all checks for this case and evaluate each one.  If any of the checks fail, the case is not a match.
                                caseMatched = true;
                                for (T8RecordMatchCheck matchCheck : matchCase.getChecks())
                                {
                                    DocPathExpressionEvaluator evaluator;
                                    T8RecordMatchCheckType matchType;
                                    List<Object> values1;
                                    List<Object> values2;
                                    boolean checkMathed;
                                    Object result;

                                    // Create the lists that will hold the values from record1 and record2 to be compared.
                                    values1 = new ArrayList<>();
                                    values2 = new ArrayList<>();

                                    // Get the evaluator to use.
                                    evaluator = getEvaluator(matchCheck);
                                    matchType = matchCheck.getType();

                                    // Evaluate the expression on record1.
                                    result = evaluator.evaluateExpression(record1CaseDataObject);
                                    if (result instanceof List)
                                    {
                                        values1.addAll((List)result);
                                    }
                                    else values1.add(result);

                                    // Evaluate the expression on record2.
                                    result = evaluator.evaluateExpression(record2CaseDataObject);
                                    if (result instanceof List)
                                    {
                                        values2.addAll((List)result);
                                    }
                                    else values2.add(result);

                                    // Now compare the values from list1 with those in list2 using the specified match type.
                                    switch (matchType)
                                    {
                                        case CORRESPONDING:
                                           checkMathed = matchCorrespondingPercentage(values1, values2, matchCheck.getPercentage());
                                           break;
                                        case ANY:
                                            checkMathed = matchAny(values1, values2, 1);
                                            break;
                                        default:
                                            throw new RuntimeException("Invalid match type found in " + matchCase + ": " + matchType);
                                    }

                                    // If this check is not a match, the subsequent checks can be skipped as the case has already been resolved.
                                    if (!checkMathed)
                                    {
                                        caseMatched = false;
                                        break;
                                    }
                                }

                                // If the records matched, create the match and assign the family ID.
                                if (caseMatched)
                                {
                                    T8RecordMatch match;
                                    String familyId;

                                    // Try to find an existing family ID for record1.
                                    familyId = recordFamilies.get(recordId1);
                                    if (familyId == null)
                                    {
                                        // Try to find an existing family ID for record2.
                                        familyId = recordFamilies.get(recordId2);
                                    }

                                    // If neither record1 nor record2 belongs to an existing family, create a new one.
                                    if (familyId == null) familyId = T8IdentifierUtilities.createNewGUID();

                                    // Make sure to set the family for both records.
                                    recordFamilies.put(recordId1, familyId);
                                    recordFamilies.put(recordId2, familyId);

                                    // Add the new match to the list of record matches.
                                    match = new T8RecordMatch(matchInstanceId, criteriaId, matchCase.getId(), familyId, null, recordId1, recordId2);
                                    recordMatches.add(match);

                                    // Break the iteration over record2CaseDataList because we don't need to continue checking cases if a match has already been found.
                                    break;
                                }
                            }

                            // Break the iteration over record1CaseDataList because we don't need to continue checking cases if a match has already been found.
                            if (caseMatched)  break;
                        }
                    }
                }
            }
        }
    }

    /**
     * This match type compares all values from list1 with all values from list2, with any match counting towards the minimum match count.
     * The two value lists do not need to be the same size.
     * @param values1 The first list of values to compare to the second list.
     * @param values2 The second list of values to compare to the first list.
     * @return Boolean true if the percentage
     */
    private boolean matchAny(List<Object> values1, List<Object> values2, int minimumMatchCount)
    {
        int matchCount;

        matchCount = 0;
        for (int index1 = 0; index1 < values1.size(); index1++)
        {
            Object value1;

            value1 = values1.get(index1);
            if (value1 != null) // null values cannot be used for matching.
            {
                for (int index2 = 0; index2 < values2.size(); index2++)
                {
                    Object value2;

                    value2 = values2.get(index2);
                    if (value1.equals(value2))
                    {
                        matchCount++;
                        if (matchCount >= minimumMatchCount) return true;
                    }
                }
            }
        }

        // Return the result of the match.
        return (matchCount >= minimumMatchCount);
    }

    /**
     * This match type performs a percentage check i.e. a specified percentage of corresponding values have to match in order for the check to succeed.
     * The two value lists must have equals size.  Values at each index in list1 will be compared to the corresponding
     * value at the same index in list2.
     * If two values do not match and neither one is null, this check immediately fails regardless of the percentage specified.
     * @param values1 The first list of values to compare to the second list.
     * @param values2 The second list of values to compare to the first list.
     * @return Boolean true if at least the specified percentage of values from list1 match those in the corresponding indices in list2.
     */
    private boolean matchCorrespondingPercentage(List<Object> values1, List<Object> values2, double percentage)
    {
        double matchCount;
        double valueCount;

        // Make sure we have two lists of equal size.
        if (values1.size() != values2.size()) throw new RuntimeException("Sizes of value lists do not match during CORRESPONDING match.  List1:" + values1.size() + ", List2:" + values2.size());

        // Initialize the numeric values and then compare the values from the lists.
        matchCount = 0;
        valueCount = values1.size();
        for (int index = 0; index < values1.size(); index++)
        {
            Object value1;
            Object value2;

            value1 = values1.get(index);
            value2 = values2.get(index);
            if (value1 != null) // If the first value is null, it immediately eliminates the possibility of a match.
            {
                if (value1.equals(value2))
                {
                    matchCount++;
                }
                else if (value2 != null)
                {
                    // If two values do not match and neither one is null, this check immediately fails.
                    return false;
                }
            }
        }

        // If the percentage of values matched is larger or equal to the specified percentage the check succeeds.
        return (matchCount/valueCount*100.00) >= percentage;
    }

    private DocPathExpressionEvaluator getEvaluator(T8RecordMatchCase expression)
    {
        DocPathExpressionEvaluator evaluator;
        String id;

        id = expression.getId();
        evaluator = caseEvaluatorCache.get(id);
        if (evaluator != null)
        {
            return evaluator;
        }
        else
        {
            evaluator = new DocPathExpressionEvaluator();
            evaluator.parseExpression(expression.getDataExpression());
            caseEvaluatorCache.put(id, evaluator);
            return evaluator;
        }
    }

    private DocPathExpressionEvaluator getEvaluator(T8RecordMatchCheck expression)
    {
        DocPathExpressionEvaluator evaluator;
        String id;

        id = expression.getId();
        evaluator = checkEvaluatorCache.get(id);
        if (evaluator != null)
        {
            return evaluator;
        }
        else
        {
            evaluator = new DocPathExpressionEvaluator();
            evaluator.parseExpression(expression.getDataExpression());
            checkEvaluatorCache.put(id, evaluator);
            return evaluator;
        }
    }
}
