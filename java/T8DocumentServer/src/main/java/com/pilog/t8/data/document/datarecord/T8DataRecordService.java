package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.service.T8Service;
import com.pilog.t8.definition.service.T8ServiceDefinition;
import com.pilog.t8.definition.data.document.datarecord.T8DataRecordServiceDefinition;
import com.pilog.t8.thread.T8ContextRunnable;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import static com.pilog.t8.definition.data.document.datarecord.T8DataRecordServiceResources.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordService implements T8Service
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordService.class);

    private final T8Context context;
    private final T8DataRecordServiceDefinition definition;
    private final String rootOrgId;
    private ScheduledExecutorService executor;
    private AsyncProcessor asyncProcessor;

    private static final int ASYNC_PROCESSOR_DELAY = 120;
    private static final long ASYNC_PROCESSOR_POLL_INTERVAL = 10; // The interval (in seconds) at which asynchronous updates to records will be processed.
    private static final int ASYNC_PROCESSOR_BATCH_SIZE = 10;

    public T8DataRecordService(T8Context context, T8DataRecordServiceDefinition definition)
    {
        this.rootOrgId = definition.getRootOrgId();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public void init() throws Exception
    {
        // Schedule the polling thread on a fixed interval.
        LOGGER.log("Starting Data Record Service for Organization: " + rootOrgId + "...");
        asyncProcessor = new AsyncProcessor(context, rootOrgId);
        executor = Executors.newScheduledThreadPool(2, new NameableThreadFactory("T8DataRecordService"));
        executor.scheduleWithFixedDelay(asyncProcessor, ASYNC_PROCESSOR_DELAY, ASYNC_PROCESSOR_POLL_INTERVAL, TimeUnit.SECONDS);
    }

    @Override
    public void destroy()
    {
        LOGGER.log("Shutting Down Data Record Service for Organization: " + rootOrgId + "...");
        executor.shutdown();
    }

    @Override
    public T8ServiceDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public Map<String, Object> executeOperation(T8Context context, String operationId, Map<String, Object> operationParameters)
    {
        if (OPERATION_TEST.equals(operationId))
        {
            T8Log.log("Test.");
            return null;
        }
        else throw new RuntimeException("Service operation not found: " + operationId);
    }

    private static class AsyncProcessor extends T8ContextRunnable
    {
        private T8DataFilter pollingFilter;

        private AsyncProcessor(T8Context context, String rootOrgId)
        {
            super(context, "T8DataRecordService-AsyncProcessor:" + rootOrgId);
            createPollingFilter();
        }

        private void createPollingFilter()
        {
            this.pollingFilter = new T8DataFilter(DATA_RECORD_ASYNC_ONT_UPD_DE_ID);
            this.pollingFilter.addFilterCriterion(DataFilterConjunction.OR, DATA_RECORD_ASYNC_ONT_UPD_DE_ID + EF_ASYNC_ONT_UPD_TIME, DataFilterOperator.IS_NOT_NULL, null, true);
            this.pollingFilter.addFieldOrdering(DATA_RECORD_ASYNC_ONT_UPD_DE_ID + EF_ASYNC_ONT_UPD_TIME, T8DataFilter.OrderMethod.ASCENDING);
        }

        @Override
        public void exec()
        {
            try
            {
                List<T8DataEntity> entitiesToProcess;
                List<String> rootRecordIdsToProcess;
                T8DataRecordApi recApi;
                T8DataTransaction tx;
                T8DataSession ds;

                // Get the transaction and API's to use for this operation.
                ds = serverContext.getDataManager().getCurrentSession();
                tx = ds.instantTransaction();
                recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);

                // Retrieve the entities with the oldest async update time.
                entitiesToProcess = tx.select(DATA_RECORD_ASYNC_ONT_UPD_DE_ID, pollingFilter, 0, ASYNC_PROCESSOR_BATCH_SIZE);
                rootRecordIdsToProcess = T8DataUtilities.getDistinctEntityFieldValues(entitiesToProcess, DATA_RECORD_ASYNC_ONT_UPD_DE_ID + EF_ROOT_RECORD_ID);

                // For each of the root record id's retrieved, process the root record and reset its async update times.
                for (String rootRecordId : rootRecordIdsToProcess)
                {
                    T8DataFilter resetFilter;

                    // Log the async update.
                    LOGGER.log("Processing asynchronous ontology update to root record: " + rootRecordId);

                    // Update the record view.
                    recApi.refreshDataObjects(rootRecordId, null);

                    // Now reset all async times on records belonging to the root record that has been processed.
                    resetFilter = new T8DataFilter(DATA_RECORD_DE_IDENTIFIER);
                    resetFilter.addFilterCriterion(DATA_RECORD_DE_IDENTIFIER + EF_ROOT_RECORD_ID, rootRecordId);
                    tx.update(DATA_RECORD_DE_IDENTIFIER, HashMaps.newHashMap(DATA_RECORD_DE_IDENTIFIER + EF_ASYNC_ONT_UPD_TIME, null), resetFilter);
                }
            }
            catch (Throwable e)
            {
                LOGGER.log("Exception while processing async Data Record update.", e);
            }
        }
    }
}
