package com.pilog.t8.api;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.org.T8CachedOrganizationTerminologyProvider;
import com.pilog.t8.data.org.T8OrganizationDataHandler;
import com.pilog.t8.data.org.T8OrganizationTerminologyProvider;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import java.util.Collection;
import java.util.List;
import com.pilog.t8.data.ontology.T8ConceptAlteration;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8TerminologyApi implements T8Api, T8PerformanceStatisticsProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8TerminologyApi.class);

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8OrganizationDataHandler organizationDataHandler;
    private T8PerformanceStatistics stats;

    public static final String API_IDENTIFIER = "@API_TERMINOLOGY";

    public T8TerminologyApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.stats = tx.getPerformanceStatistics(); // Default behviour of all API's is to use the performance statistics object of its parent transaction.
        this.organizationDataHandler = new T8OrganizationDataHandler(serverContext);
        this.organizationDataHandler.setPerformanceStatistics(stats);
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    public TerminologyProvider getTerminologyProvider()
    {
        TerminologyProvider terminologyProvider;

        terminologyProvider = new T8CachedOrganizationTerminologyProvider(serverContext, sessionContext.getRootOrganizationIdentifier(), new T8OrganizationTerminologyProvider(this));
        terminologyProvider.setLanguage(sessionContext.getContentLanguageIdentifier());
        return terminologyProvider;
    }

    public T8ConceptTerminology retrieveTerminology(String languageId, String conceptId) throws Exception
    {
        List<T8ConceptTerminology> terminologyList;

        terminologyList = retrieveTerminology(ArrayLists.typeSafeList(languageId), ArrayLists.typeSafeList(conceptId));
        return terminologyList.size() > 0 ? terminologyList.get(0) : null;
    }

    public T8ConceptTerminologyList retrieveTerminology(Collection<String> languageIds, Collection<String> conceptIds) throws Exception
    {
        T8OrganizationApi orgApi;
        List<String> orgIdList;

        // All terminology is linked to the root organization.
        orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        orgIdList = ArrayLists.newArrayList(sessionContext.getRootOrganizationIdentifier());
        return organizationDataHandler.retrieveTerminology(tx, orgIdList, languageIds != null ? languageIds : orgApi.getLanguageIdSet(), conceptIds);
    }

    public void insertTerminology(T8ConceptTerminology terminology) throws Exception
    {
        organizationDataHandler.insertTerminology(tx, terminology);
    }

    public void insertTerminology(Collection<T8ConceptTerminology> terminologies) throws Exception
    {
        for (T8ConceptTerminology terminology : terminologies)
        {
            organizationDataHandler.insertTerminology(tx, terminology);
        }
    }

    public void updateTerminology(T8ConceptTerminology terminology, boolean deletionEnabled) throws Exception
    {
        organizationDataHandler.updateTerminology(tx, terminology, true);
    }

    public void saveTerminology(T8ConceptTerminology terminology, boolean deletionEnabled) throws Exception
    {
        organizationDataHandler.saveTerminology(tx, terminology, deletionEnabled);
    }

    public void saveTerminology(Collection<T8ConceptTerminology> terminologies) throws Exception
    {
        for (T8ConceptTerminology terminology : terminologies)
        {
            organizationDataHandler.saveTerminology(tx, terminology, true);
        }
    }

    public void saveTerminologyAlteration(T8ConceptAlteration conceptAlteration) throws Exception
    {
        for (T8OntologyConcept insertedConcept : conceptAlteration.getInsertedConcepts())
        {
            insertTerminology(insertedConcept.createTerminologies(sessionContext.getRootOrganizationIdentifier()));
        }

        for (T8OntologyConcept updatedConcept : conceptAlteration.getUpdatedConcepts())
        {
            saveTerminology(updatedConcept.createTerminologies(sessionContext.getRootOrganizationIdentifier()));
        }

        for (T8OntologyConcept deletedConcept : conceptAlteration.getDeletedConcepts())
        {
            deleteTerminology(ArrayLists.typeSafeList(deletedConcept.getID()));
        }
    }

    public void deleteTerminology(T8ConceptTerminology terminology) throws Exception
    {
        organizationDataHandler.deleteTerminology(tx, terminology);
    }

    public void deleteTerminology(List<String> conceptIdList) throws Exception
    {
        deleteTerminology(null, conceptIdList);
    }

    public void deleteTerminology(List<String> languageIdList, List<String> conceptIdList) throws Exception
    {
        if (CollectionUtilities.isNullOrEmpty(languageIdList))
        {
            T8OrganizationApi orgApi;

            orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            organizationDataHandler.deleteTerminology(tx, ArrayLists.typeSafeList(sessionContext.getRootOrganizationIdentifier()), orgApi.getLanguageIdSet(), conceptIdList);
        }
        else
        {
            organizationDataHandler.deleteTerminology(tx, ArrayLists.typeSafeList(sessionContext.getRootOrganizationIdentifier()), languageIdList, conceptIdList);
        }
    }

    public void clearTerminologyCode(List<String> conceptIds) throws Exception
    {
        organizationDataHandler.clearTerminologyCode(tx, ArrayLists.typeSafeList(sessionContext.getRootOrganizationIdentifier()), null, conceptIds);
    }
}
