package com.pilog.t8.data.alteration.handler;

import com.pilog.json.JsonObject;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.data.alteration.T8DataAlteration;
import com.pilog.t8.data.alteration.T8DataAlterationHandler;
import com.pilog.t8.data.alteration.T8DataAlterationImpact;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.alteration.T8DataAlteration.T8DataAlterationMethod;
import com.pilog.t8.data.alteration.T8DataAlterationDataHandler;
import com.pilog.t8.data.alteration.T8DataAlterationPackage;
import com.pilog.t8.data.alteration.type.T8CompleteConceptAlteration;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8CompleteConceptAlterationHandler implements T8DataAlterationHandler
{
    private final T8ServerContext serverContext;
    private T8OntologyApi ontApi;

    public T8CompleteConceptAlterationHandler(T8Context context)
    {
        this.serverContext = context.getServerContext();
    }

    @Override
    public String getAlterationId()
    {
        return T8CompleteConceptAlteration.ALTERATION_ID;
    }

    @Override
    public int deleteAlteration(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        int conceptDeltedCount;
        T8DataAlterationDataHandler handler;

        // Set values of the needed variables
        handler = new T8DataAlterationDataHandler(serverContext);

        // Delete from ontology concept.
        conceptDeltedCount = handler.deleteOntologyConcept(tx, packageIid, step);

        return conceptDeltedCount;
    }

    @Override
    public void insertAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8CompleteConceptAlteration conceptAlteration;
        T8OntologyConcept alteredConcept;
        T8DataAlterationDataHandler handler;
        String packageIid;
        int step;
        String method;

        // Set values of the needed variables
        handler = new T8DataAlterationDataHandler(serverContext);
        conceptAlteration = (T8CompleteConceptAlteration)alteration;
        alteredConcept = conceptAlteration.getConcept();
        packageIid = conceptAlteration.getPackageIid();
        step = conceptAlteration.getStep();
        method = conceptAlteration.getAlterationMethod().toString();

        // Insert the complete concept alteration
        handler.insertOntologyConcept(tx, alteredConcept, packageIid, step , method);
    }

    @Override
    public void updateAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8CompleteConceptAlteration conceptAlteration;
        T8OntologyConcept alteredConcept;
        T8DataAlterationDataHandler handler;
        String packageIid;
        int step;
        String method;

        // Set values of the needed variables
        handler = new T8DataAlterationDataHandler(serverContext);
        conceptAlteration = (T8CompleteConceptAlteration)alteration;
        alteredConcept = conceptAlteration.getConcept();
        packageIid = conceptAlteration.getPackageIid();
        step = conceptAlteration.getStep();
        method = conceptAlteration.getAlterationMethod().toString();

        // Update the complete concept alteration
        handler.updateOntologyConcept(tx, alteredConcept, packageIid, step , method);
    }

    @Override
    public T8DataAlterationImpact applyAlteration(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8CompleteConceptAlteration conceptAlteration;
        T8OntologyConcept alteredConcept;
        T8DataAlterationMethod method;
        T8DataAlterationDataHandler handler;
        T8DataAlterationPackage altPackage;
        int appliedStep;

        // Initialize the data handler.
        handler = new T8DataAlterationDataHandler(serverContext);

        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        conceptAlteration = (T8CompleteConceptAlteration)alteration;
        alteredConcept = conceptAlteration.getConcept();
        method = conceptAlteration.getAlterationMethod();

        // Check what the alteration method is and apply the relevant method.
        if (null != method)
        {
            switch (method)
            {
                case INSERT:
                        ontApi.insertConcept(alteredConcept, true, true, true, true, true);
                     break;
                case UPDATE:
                        ontApi.saveConcept(alteredConcept, true, true, true, true, true, true);
                     break;
                case DELETE:
                        ontApi.deleteConcept(alteredConcept.getID()) ;
                     break;
            }
        }

        // Update the applied step on the package.
        altPackage = handler.retrieveAlterationPackage(tx, conceptAlteration.getPackageIid());

        appliedStep = altPackage.getAppliedStep();
        appliedStep = appliedStep + 1;

        altPackage.setAppliedStep(appliedStep);
        handler.updateAlterationPackage(tx, altPackage);

        return null;
    }

    @Override
    public T8DataAlteration retrieveAlteration(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        T8CompleteConceptAlteration alteration;
        List<T8OntologyConcept> conceptList;
        T8DataAlterationDataHandler handler;
        String method;
        T8DataAlterationMethod alterationMethod;

        // Initialize the data handler.
        handler = new T8DataAlterationDataHandler(serverContext);

        // Retrieve the concept.
        conceptList = handler.retrieveConcepts(tx, packageIid, step);

        // Retrieve the alteration method.
        method = handler.retrieveConceptAlterationMethod(tx, packageIid, step);
        alterationMethod = getAlterationMethod(method);

        // Create the complete concept alteration object.
        alteration = new T8CompleteConceptAlteration(packageIid, step, conceptList.get(0), alterationMethod);

        return alteration;
    }

    @Override
    public void adjustSteps(T8DataTransaction tx, String alterationID, int startStep, int endStep, int increment) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlterationImpact getAlterationImpact(T8DataTransaction tx, T8DataAlteration alteration) throws Exception
    {
        T8CompleteConceptAlteration conceptAlteration;
        T8OntologyConcept alteredConcept;

        conceptAlteration = (T8CompleteConceptAlteration)alteration;
        alteredConcept = conceptAlteration.getConcept();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataAlteration deserializeAlteration(JsonObject jsonValue)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public T8DataAlterationMethod getAlterationMethod(String method)
    {
        T8DataAlterationMethod alterationMethod;

        // Return the correct alteration method.
        switch (method)
        {
            case "INSERT":
                alterationMethod = T8DataAlterationMethod.INSERT;
                break;
            case "UPDATE":
                alterationMethod = T8DataAlterationMethod.UPDATE;
                break;
            case "DELETE":
                alterationMethod = T8DataAlterationMethod.DELETE;
                break;
            default:
                alterationMethod = null;
                break;
        }

        // Return the correct method.
        return alterationMethod;
    }
}
