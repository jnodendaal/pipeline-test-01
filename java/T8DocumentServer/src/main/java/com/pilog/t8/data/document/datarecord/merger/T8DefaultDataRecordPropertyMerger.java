package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordFieldMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DataRecordValueMergerDefinition;
import com.pilog.t8.definition.data.document.datarecord.merger.T8DefaultDataRecordPropertyMergerDefinition;
import com.pilog.t8.security.T8Context;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This is the default merger used for properties.  Any number of value and
 * field mergers can be added to this property merger.  When a merge is
 * performed all of the added mergers will be tested for applicability and only
 * when no applicable merger is found, will the defaults be used.
 *
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordPropertyMerger implements T8DataRecordPropertyMerger
{
    private final Set<DataRecord> updatedDataRecords;
    private final LinkedList<T8DataRecordValueMerger> valueMergers;
    private final LinkedList<T8DataRecordFieldMerger> fieldMergers;
    private final EnumSet<RequirementType> excludedRequirementTypes;
    private final T8DataRecordMergeContext mergeContext;
    private final String applicablePropertyID;

    public T8DefaultDataRecordPropertyMerger(T8DataRecordMergeContext mergeContext, T8DataRecordMergeConfiguration config)
    {
        this.mergeContext = mergeContext;
        this.excludedRequirementTypes = config.getExcludedRequirementTypes();
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.valueMergers = new LinkedList<T8DataRecordValueMerger>();
        this.fieldMergers = new LinkedList<T8DataRecordFieldMerger>();
        this.applicablePropertyID = null; // Applicable to all properties.
    }

    public T8DefaultDataRecordPropertyMerger(T8DefaultDataRecordPropertyMergerDefinition definition, T8DataRecordMergeContext mergeContext)
    {
        this.mergeContext = mergeContext;
        this.updatedDataRecords = new HashSet<DataRecord>();
        this.valueMergers = new LinkedList<T8DataRecordValueMerger>();
        this.fieldMergers = new LinkedList<T8DataRecordFieldMerger>();
        this.applicablePropertyID = definition.getPropertyID();
        this.excludedRequirementTypes = EnumSet.noneOf(RequirementType.class);

        // Create all the value mergers.
        for (T8DataRecordValueMergerDefinition valueMergerDefinition : definition.getValueMergerDefinitions())
        {
            valueMergers.push(valueMergerDefinition.getNewMergerInstance(mergeContext));
        }

        // Create all the field mergers.
        for (T8DataRecordFieldMergerDefinition fieldMergerDefinition : definition.getFieldMergerDefinitions())
        {
            fieldMergers.push(fieldMergerDefinition.getNewMergerInstance(mergeContext));
        }
    }

    @Override
    public T8DataTransaction getTransaction()
    {
        return mergeContext.getTransaction();
    }

    @Override
    public T8Context getContext()
    {
        return mergeContext.getContext();
    }

    @Override
    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        return mergeContext.getExpressionEvaluator();
    }

    @Override
    public T8DataRecordValueStringGenerator getFFTGenerator()
    {
        return mergeContext.getFFTGenerator();
    }

    @Override
    public DataRecord copyDataRecord(DataRecord sourceRecord)
    {
        return mergeContext.copyDataRecord(sourceRecord);
    }

    public void addValueMerger(T8DataRecordValueMerger valueMerger)
    {
        valueMergers.push(valueMerger);
    }

    public boolean removeValueMerger(T8DataRecordValueMerger valueMerger)
    {
        return valueMergers.remove(valueMerger);
    }

    public void addFieldMerger(T8DataRecordFieldMerger fieldMerger)
    {
        fieldMergers.push(fieldMerger);
    }

    public boolean removeFieldMerger(T8DataRecordFieldMerger fieldMerger)
    {
        return fieldMergers.remove(fieldMerger);
    }

    @Override
    public String getDefaultContentLanguageID()
    {
        return mergeContext.getDefaultContentLanguageID();
    }

    @Override
    public TerminologyProvider getTerminologyProvider()
    {
        return mergeContext.getTerminologyProvider();
    }

    @Override
    public OntologyProvider getOntologyProvider()
    {
        return mergeContext.getOntologyProvider();
    }

    @Override
    public DataRequirementInstanceProvider getDRInstanceProvider()
    {
        return mergeContext.getDRInstanceProvider();
    }

    @Override
    public void reset()
    {
        // Clear the updates records on this merger.
        updatedDataRecords.clear();

        // Reset the value mergers.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            valueMerger.reset();
        }

        // Reset the field mergers.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            fieldMerger.reset();
        }
    }

    @Override
    public Map<String, String> getRecordIdMapping()
    {
        Map<String, String> recordIdMapping;

        // Create a map of all record identifier changes.
        recordIdMapping = new HashMap<String, String>();

        // Add all value merger mappings.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            recordIdMapping.putAll(valueMerger.getRecordIdMapping());
        }

        // Add all field merger mappings.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            recordIdMapping.putAll(fieldMerger.getRecordIdMapping());
        }

        // Return the result.
        return recordIdMapping;
    }

    @Override
    public Map<String, String> getAttachmentIdentifierMapping()
    {
        Map<String, String> attachmentIdentifierMapping;

        // Create a map of all attachment identifier changes.
        attachmentIdentifierMapping = new HashMap<String, String>();

        // Add all value merger mappings.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            attachmentIdentifierMapping.putAll(valueMerger.getAttachmentIdentifierMapping());
        }

        // Add all field merger mappings.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            attachmentIdentifierMapping.putAll(fieldMerger.getAttachmentIdentifierMapping());
        }

        // Return the result.
        return attachmentIdentifierMapping;
    }

    @Override
    public Set<DataRecord> getNewDataRecordSet()
    {
        HashSet<DataRecord> newRecordSet;

        // Create a set of all new records added during the merge operation.
        newRecordSet = new HashSet<DataRecord>();

        // Add all value merger new records.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            newRecordSet.addAll(valueMerger.getNewDataRecordSet());
        }

        // Add all field merger new records.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            newRecordSet.addAll(fieldMerger.getNewDataRecordSet());
        }

        return newRecordSet;
    }

    @Override
    public Set<DataRecord> getUpdatedDataRecordSet()
    {
        HashSet<DataRecord> updatedRecordSet;

        // Create a set of all records updated during the merge operation.
        updatedRecordSet = new HashSet<DataRecord>(updatedDataRecords);

        // Add all value merger updated records.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            updatedRecordSet.addAll(valueMerger.getUpdatedDataRecordSet());
        }

        // Add all field merger updated records.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            updatedRecordSet.addAll(fieldMerger.getUpdatedDataRecordSet());
        }

        return updatedRecordSet;
    }

    @Override
    public boolean isApplicable(RecordProperty recordProperty)
    {
        return ((applicablePropertyID == null) || (applicablePropertyID.equals(recordProperty.getPropertyID())));
    }

    @Override
    public void mergeRecordProperties(RecordProperty destinationProperty, RecordProperty sourceProperty)
    {
        RecordValue sourceValue;
        RecordValue destinationValue;

        // Get the source and destination values.
        sourceValue = sourceProperty.getRecordValue();
        destinationValue = destinationProperty.getRecordValue();

        // Only proceed if the source value is not null.
        if ((sourceValue != null) && (!excludedRequirementTypes.contains(sourceValue.getValueRequirement().getRequirementType())))
        {
            // Create the destination value if required.
            if (destinationValue == null)
            {
                destinationValue = RecordValue.createValue(sourceValue.getValueRequirement());
                destinationProperty.setRecordValue(destinationValue);
                updatedDataRecords.add(destinationProperty.getParentDataRecord());
            }

            // Merge the two values.
            mergeRecordValues(destinationValue, sourceValue);
        }
    }

    private void mergeRecordValues(RecordValue destinationValue, RecordValue sourceValue)
    {
        RequirementType requirementType;

        // Get the destination requirement type.
        requirementType = destinationValue.getValueRequirement().getRequirementType();

        // If the destination requirement type is a composite type, use the field mergers available.
        if ((requirementType == RequirementType.COMPOSITE_TYPE) && (fieldMergers.size() > 0))
        {
            List<RecordValue> sourceFields;

            sourceFields = sourceValue.getSubValues();
            for (RecordValue sourceField : sourceFields)
            {
                RecordValue matchingDestinationField;
                String sourceFieldID;

                sourceFieldID = sourceField.getValueRequirement().getValue();
                matchingDestinationField = destinationValue.getSubValue(RequirementType.FIELD_TYPE, sourceFieldID, null);
                if (matchingDestinationField == null)
                {
                    ValueRequirement destinationFieldRequirement;

                    destinationFieldRequirement = destinationValue.getValueRequirement().getEquivalentSubRequirement(sourceField.getValueRequirement());
                    matchingDestinationField = new Field(destinationFieldRequirement);
                    destinationValue.setSubValue(matchingDestinationField);

                    // Add the document to the updated collection.
                    updatedDataRecords.add(destinationValue.getParentDataRecord());
                }

                // Now merge the fields.
                mergeRecordFields(matchingDestinationField, sourceField);
            }
        }
        else // Not a composite type, so merge values directly (no fields).
        {
            T8DataRecordValueMerger valueMerger;

            // Try to find a value merger to merge the values.
            valueMerger = getValueMerger(destinationValue, sourceValue);
            if (valueMerger != null)
            {
                valueMerger.mergeRecordValues(destinationValue, sourceValue);
            }
            else
            {
                // No field merger found, so use the defaul.
                throw new RuntimeException("No applicable value merger found for destination value: " + destinationValue);
            }
        }
    }

    private void mergeRecordFields(RecordValue destinationField, RecordValue sourceField)
    {
        T8DataRecordFieldMerger fieldMerger;

        // Try to find a field merger to merge the fields.
        fieldMerger = getFieldMerger(destinationField, sourceField);
        if (fieldMerger != null)
        {
            fieldMerger.mergeRecordFields(destinationField, sourceField);
        }
        else
        {
            // No field merger found, so use the defaul.
            throw new RuntimeException("No applicable field merger found for destination field: " + destinationField);
        }
    }

    @Override
    public T8DataRecordInstanceMerger getInstanceMerger(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        return mergeContext.getInstanceMerger(destinationRecord, sourceRecord);
    }

    @Override
    public T8DataRecordPropertyMerger getPropertyMerger(RecordProperty destinationProperty, RecordProperty sourceProperty)
    {
        return mergeContext.getPropertyMerger(destinationProperty, sourceProperty);
    }

    @Override
    public T8DataRecordFieldMerger getFieldMerger(RecordValue destinationField, RecordValue sourceField)
    {
        // Try to find a specific applicable merger for the fields.
        for (T8DataRecordFieldMerger fieldMerger : fieldMergers)
        {
            if (fieldMerger.isApplicable(destinationField, sourceField))
            {
                return fieldMerger;
            }
        }

        // No applicable merger found.
        return mergeContext.getFieldMerger(destinationField, sourceField);
    }

    @Override
    public T8DataRecordValueMerger getValueMerger(RecordValue destinationValue, RecordValue sourceValue)
    {
        // Try to find a specific applicable merger for the values.
        for (T8DataRecordValueMerger valueMerger : valueMergers)
        {
            if (valueMerger.isApplicable(destinationValue, sourceValue))
            {
                return valueMerger;
            }
        }

        // No applicable merger found.
        return mergeContext.getValueMerger(destinationValue, sourceValue);
    }
}
