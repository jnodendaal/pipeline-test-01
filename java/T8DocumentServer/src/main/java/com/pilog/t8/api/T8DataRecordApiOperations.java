package com.pilog.t8.api;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datafile.T8DataFileSaveResult;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.ontology.T8OntologyConcept.T8ConceptElementType;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.log.T8Logger;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRecordApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordApiOperations
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordApiOperations.class);

    public static class ApiCreateRecord extends T8DefaultServerOperation
    {
        public ApiCreateRecord(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, DataRecord> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordApi recApi;
            DataRecord createdRecord;
            DataRecord dataFile;
            String drInstanceId;
            String recordId;
            String languageId;
            String propertyId;
            String fieldId;
            boolean includeTerminology;
            boolean includeDrComments;

            // Get the operation parameters we need.
            dataFile = (DataRecord)operationParameters.get(PARAMETER_FILE);
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);
            propertyId = (String)operationParameters.get(PARAMETER_PROPERTY_ID);
            fieldId = (String)operationParameters.get(PARAMETER_FIELD_ID);
            drInstanceId = (String)operationParameters.get(PARAMETER_DR_INSTANCE_ID);
            languageId = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);
            includeTerminology = Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY));
            includeDrComments = Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_DR_COMMENTS));

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            createdRecord = recApi.createRecord(dataFile, recordId, propertyId, fieldId, drInstanceId, languageId, includeTerminology, includeDrComments);

            // Commit the transaction and return the result.
            return HashMaps.createSingular(PARAMETER_RECORD, createdRecord);
        }
    }

    public static class ApiChangeRecordDrInstance extends T8DefaultServerOperation
    {
        public ApiChangeRecordDrInstance(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, DataRecord> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordApi recApi;
            DataRecord dataFile;
            String drInstanceId;
            String recordId;
            String languageId;
            boolean includeOntology;
            boolean includeTerminology;
            boolean includeDescriptions;
            boolean includeAttachmentDetails;
            boolean includeDrComments;
            boolean includeParticleSettings;

            // Get the operation parameters we need.
            dataFile = (DataRecord)operationParameters.get(PARAMETER_FILE);
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);
            drInstanceId = (String)operationParameters.get(PARAMETER_DR_INSTANCE_ID);
            languageId = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);
            includeOntology = Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_ONTOLOGY));
            includeTerminology = Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY));
            includeDescriptions = Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_DESCRIPTIONS));
            includeAttachmentDetails = Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_ATTACHMENT_DETAILS));
            includeDrComments = Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_DR_COMMENTS));
            includeParticleSettings = Boolean.TRUE.equals(operationParameters.get(PARAMETER_INCLUDE_PARTICLE_SETTINGS));

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            recApi.changeRecordDrInstance(dataFile, recordId, drInstanceId, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDrComments, includeParticleSettings);

            // Commit the transaction and return the result.
            return HashMaps.createSingular(PARAMETER_FILE, dataFile);
        }
    }

    public static class ApiSaveFileStateChange extends T8DefaultServerOperation
    {
        public ApiSaveFileStateChange(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataFileSaveResult saveResult;
            DataRecord dataFile;
            T8DataRecordApi recApi;

            // Get the operation parameters we need.
            dataFile = (DataRecord)operationParameters.get(PARAMETER_FILE);

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            saveResult = recApi.saveFileStateChange(dataFile.getSavedState(), dataFile);
            return HashMaps.createSingular(PARAMETER_SAVE_RESULT, saveResult);
        }
    }

    public static class ApiValidateFileStateChange extends T8DefaultServerOperation
    {
        public ApiValidateFileStateChange(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataFileValidationReport validationReport;
            DataRecord dataFile;
            T8DataRecordApi recApi;

            // Get the operation parameters we need.
            dataFile = (DataRecord)operationParameters.get(PARAMETER_FILE);

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            validationReport = recApi.validateFileStateChange(dataFile.getSavedState(), dataFile);
            return HashMaps.createSingular(PARAMETER_VALIDATION_REPORT, validationReport);
        }
    }

    public static class ApiRetrieveFiles extends T8DefaultServerOperation
    {
        public ApiRetrieveFiles(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<DataRecord>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<DataRecord> dataFileList;
            T8DataRecordApi recApi;
            List<String> fileIdList;
            String languageId;
            Boolean includeOntology;
            Boolean includeTerminology;
            Boolean includeDescriptions;
            Boolean includeAttachmentDetails;
            Boolean includeDrComments;
            Boolean includeParticleSettings;

            // Get the operation parameters we need.
            fileIdList = (List<String>)operationParameters.get(PARAMETER_FILE_IDS);
            languageId = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);
            includeOntology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_ONTOLOGY);
            includeTerminology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY);
            includeDescriptions = (Boolean)operationParameters.get(PARAMETER_INCLUDE_DESCRIPTIONS);
            includeAttachmentDetails = (Boolean)operationParameters.get(PARAMETER_INCLUDE_ATTACHMENT_DETAILS);
            includeDrComments = (Boolean)operationParameters.get(PARAMETER_INCLUDE_DR_COMMENTS);
            includeParticleSettings =  (Boolean)operationParameters.get(PARAMETER_INCLUDE_PARTICLE_SETTINGS);

            // Set parameter defaults.
            if (languageId == null) context.getSessionContext().getContentLanguageIdentifier();
            if (includeOntology == null) includeOntology = false;
            if (includeTerminology == null) includeTerminology = false;
            if (includeDescriptions == null) includeDescriptions = false;
            if (includeAttachmentDetails == null) includeAttachmentDetails = false;
            if (includeDrComments == null) includeDrComments = false;
            if (includeParticleSettings == null) includeParticleSettings = false;

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            dataFileList = recApi.retrieveFiles(fileIdList, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDrComments, includeParticleSettings);
            return HashMaps.createSingular(PARAMETER_FILE_LIST, dataFileList);
        }
    }

    public static class ApiRetrieveFilesByContentRecord extends T8DefaultServerOperation
    {
        public ApiRetrieveFilesByContentRecord(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<DataRecord>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<DataRecord> dataFileList;
            T8DataRecordApi recApi;
            List<String> recordIdList;
            String languageId;
            Boolean includeOntology;
            Boolean includeTerminology;
            Boolean includeDescriptions;
            Boolean includeAttachmentDetails;
            Boolean includeDrComments;
            Boolean includeParticleSettings;

            // Get the operation parameters we need.
            recordIdList = (List<String>)operationParameters.get(PARAMETER_RECORD_IDS);
            languageId = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);
            includeOntology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_ONTOLOGY);
            includeTerminology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY);
            includeDescriptions = (Boolean)operationParameters.get(PARAMETER_INCLUDE_DESCRIPTIONS);
            includeAttachmentDetails = (Boolean)operationParameters.get(PARAMETER_INCLUDE_ATTACHMENT_DETAILS);
            includeDrComments = (Boolean)operationParameters.get(PARAMETER_INCLUDE_DR_COMMENTS);
            includeParticleSettings =  (Boolean)operationParameters.get(PARAMETER_INCLUDE_PARTICLE_SETTINGS);

            // Set parameter defaults.
            if (languageId == null) context.getSessionContext().getContentLanguageIdentifier();
            if (includeOntology == null) includeOntology = false;
            if (includeTerminology == null) includeTerminology = false;
            if (includeDescriptions == null) includeDescriptions = false;
            if (includeAttachmentDetails == null) includeAttachmentDetails = false;
            if (includeDrComments == null) includeDrComments = false;
            if (includeParticleSettings == null) includeParticleSettings = false;

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            dataFileList = recApi.retrieveFilesByContentRecord(recordIdList, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDrComments, includeParticleSettings);
            return HashMaps.createSingular(PARAMETER_FILE_LIST, dataFileList);
        }
    }

    public static class ApiRetrieveRecords extends T8DefaultServerOperation
    {
        public ApiRetrieveRecords(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<DataRecord>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<DataRecord> dataRecordList;
            T8DataRecordApi recApi;
            List<String> recordIdList;
            String languageId;
            Boolean includeOntology;
            Boolean includeTerminology;
            Boolean includeDescriptions;
            Boolean includeAttachmentDetails;
            Boolean includeDescendantRecords;
            Boolean includeDRComments;
            Boolean includeParticleSettings;

            // Get the operation parameters we need.
            recordIdList = (List<String>)operationParameters.get(PARAMETER_RECORD_IDS);
            languageId = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);
            includeOntology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_ONTOLOGY);
            includeTerminology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY);
            includeDescriptions = (Boolean)operationParameters.get(PARAMETER_INCLUDE_DESCRIPTIONS);
            includeAttachmentDetails = (Boolean)operationParameters.get(PARAMETER_INCLUDE_ATTACHMENT_DETAILS);
            includeDRComments = (Boolean)operationParameters.get(PARAMETER_INCLUDE_DR_COMMENTS);
            includeParticleSettings =  (Boolean)operationParameters.get(PARAMETER_INCLUDE_PARTICLE_SETTINGS);
            includeDescendantRecords = (Boolean)operationParameters.get(PARAMETER_INCLUDE_DESCENDANT_RECORDS);

            // Set parameter defaults.
            if (languageId == null) context.getSessionContext().getContentLanguageIdentifier();
            if (includeOntology == null) includeOntology = false;
            if (includeTerminology == null) includeTerminology = false;
            if (includeDescriptions == null) includeDescriptions = false;
            if (includeAttachmentDetails == null) includeAttachmentDetails = false;
            if (includeDRComments == null) includeDRComments = false;
            if (includeParticleSettings == null) includeParticleSettings = false;
            if (includeDescendantRecords == null) includeDescendantRecords = false;

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            dataRecordList = recApi.retrieveRecords(recordIdList, languageId, includeOntology, includeTerminology, includeDescriptions, includeAttachmentDetails, includeDRComments, includeParticleSettings, includeDescendantRecords);
            return HashMaps.createSingular(PARAMETER_RECORD_LIST, dataRecordList);
        }
    }

    public static class ApiResolveConceptId extends T8DefaultServerOperation
    {
        public ApiResolveConceptId(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordApi recApi;
            DataRecord contextRecord;
            String targetRecordId;
            String targetPropertyId;
            String targetFieldId;
            String targetDataTypeId;
            String matchString;
            List<T8ConceptElementType> elementTypes;
            String conceptId;

            // Get the operation parameters we need.
            contextRecord = (DataRecord)operationParameters.get(PARAMETER_RECORD);
            targetRecordId = (String)operationParameters.get(PARAMETER_TARGET_RECORD_ID);
            targetPropertyId = (String)operationParameters.get(PARAMETER_TARGET_PROPERTY_ID);
            targetFieldId = (String)operationParameters.get(PARAMETER_TARGET_FIELD_ID);
            targetDataTypeId = (String)operationParameters.get(PARAMETER_TARGET_DATA_TYPE_ID);
            matchString = (String)operationParameters.get(PARAMETER_MATCH_STRING);
            elementTypes = (List<T8ConceptElementType>)operationParameters.get(PARAMETER_CONCEPT_ELEMENT_TYPES);

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            conceptId = recApi.resolveConceptId(contextRecord, targetRecordId, targetPropertyId, targetFieldId, targetDataTypeId, matchString, elementTypes);
            return HashMaps.createSingular(PARAMETER_CONCEPT_ID, conceptId);
        }
    }

    public static class ApiRetrieveValueSuggestions extends T8DefaultServerOperation
    {
        public ApiRetrieveValueSuggestions(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<RecordValue>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordApi recApi;
            DataRecord contextRecord;
            String targetRecordId;
            String targetPropertyId;
            String targetFieldId;
            String targetDataTypeId;
            String searchPrefix;
            List<RecordValue> recordValues;
            Integer pageOffset;
            Integer pageSize;

            // Get the operation parameters we need.
            contextRecord = (DataRecord)operationParameters.get(PARAMETER_RECORD);
            targetRecordId = (String)operationParameters.get(PARAMETER_TARGET_RECORD_ID);
            targetPropertyId = (String)operationParameters.get(PARAMETER_TARGET_PROPERTY_ID);
            targetFieldId = (String)operationParameters.get(PARAMETER_TARGET_FIELD_ID);
            targetDataTypeId = (String)operationParameters.get(PARAMETER_TARGET_DATA_TYPE_ID);
            searchPrefix = (String)operationParameters.get(PARAMETER_SEARCH_PREFIX);
            pageOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);

            // Set default values for unreceived parameters.
            if (pageOffset == null) pageOffset = 0;
            if (pageSize == null) pageSize = 20;

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            recordValues = recApi.retrieveValueSuggestions(contextRecord, targetRecordId, targetPropertyId, targetFieldId, targetDataTypeId, searchPrefix, pageOffset, pageSize);
            return HashMaps.createSingular(PARAMETER_RECORD_VALUES, recordValues);
        }
    }

    public static class ApiRetrieveValueOptions extends T8DefaultServerOperation
    {
        public ApiRetrieveValueOptions(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<RecordValue>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordApi recApi;
            DataRecord contextRecord;
            String targetRecordID;
            String targetPropertyID;
            String targetFieldID;
            String targetDataTypeID;
            List<RecordValue> recordValues;
            Integer pageOffset;
            Integer pageSize;

            // Get the operation parameters we need.
            contextRecord = (DataRecord)operationParameters.get(PARAMETER_RECORD);
            targetRecordID = (String)operationParameters.get(PARAMETER_TARGET_RECORD_ID);
            targetPropertyID = (String)operationParameters.get(PARAMETER_TARGET_PROPERTY_ID);
            targetFieldID = (String)operationParameters.get(PARAMETER_TARGET_FIELD_ID);
            targetDataTypeID = (String)operationParameters.get(PARAMETER_TARGET_DATA_TYPE_ID);
            pageOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);

            // Set default values for parameters not received.
            if (pageOffset == null) pageOffset = 0;
            if (pageSize == null) pageSize = 20;

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            recordValues = recApi.retrieveValueOptions(contextRecord, targetRecordID, targetPropertyID, targetFieldID, targetDataTypeID, null, null, pageOffset, pageSize);
            return HashMaps.createSingular(PARAMETER_RECORD_VALUES, recordValues);
        }
    }

    public static class ApiRetrieveDrInstanceOptions extends T8DefaultServerOperation
    {
        public ApiRetrieveDrInstanceOptions(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<T8ConceptTerminology>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRecordApi recApi;
            DataRecord contextRecord;
            String targetRecordID;
            String targetPropertyID;
            String targetFieldID;
            String targetDataTypeID;
            List<T8ConceptTerminology> terminologyList;
            Integer pageOffset;
            Integer pageSize;

            // Get the operation parameters we need.
            contextRecord = (DataRecord)operationParameters.get(PARAMETER_RECORD);
            targetRecordID = (String)operationParameters.get(PARAMETER_TARGET_RECORD_ID);
            targetPropertyID = (String)operationParameters.get(PARAMETER_TARGET_PROPERTY_ID);
            targetFieldID = (String)operationParameters.get(PARAMETER_TARGET_FIELD_ID);
            targetDataTypeID = (String)operationParameters.get(PARAMETER_TARGET_DATA_TYPE_ID);
            pageOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);

            // Set default values for parameters not received.
            if (pageOffset == null) pageOffset = 0;
            if (pageSize == null) pageSize = 20;

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            terminologyList = recApi.retrieveDrInstanceOptions(contextRecord, targetRecordID, targetPropertyID, targetFieldID, targetDataTypeID, pageOffset, pageSize);
            return HashMaps.createSingular(PARAMETER_CONCEPT_TERMINOLOGY_LIST, terminologyList);
        }
    }

    public static class ApiRetrieveRecordAttachmentDetails extends T8DefaultServerOperation
    {
        public ApiRetrieveRecordAttachmentDetails(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Map<String, T8AttachmentDetails> attachmentDetails;
            T8DataRecordApi recApi;
            String recordId;

            // Get the operation parameters we need.
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            attachmentDetails = recApi.retrieveRecordAttachmentDetails(recordId);
            return HashMaps.createSingular(PARAMETER_ATTACHMENT_DETAILS, attachmentDetails);
        }
    }

    public static class ApiRetrieveAttachmentDetails extends T8DefaultServerOperation
    {
        public ApiRetrieveAttachmentDetails(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FileDetails attachmentDetails;
            T8DataRecordApi recApi;
            String attachmentId;

            // Get the operation parameters we need.
            attachmentId = (String)operationParameters.get(PARAMETER_ATTACHMENT_ID);

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            attachmentDetails = recApi.retrieveAttachmentDetails(attachmentId);
            return HashMaps.createSingular(PARAMETER_ATTACHMENT_DETAILS, attachmentDetails);
        }
    }

    public static class ApiExportAttachment extends T8DefaultServerOperation
    {
        public ApiExportAttachment(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FileDetails attachmentDetails;
            T8DataRecordApi recApi;
            String attachmentId;
            String contextIid;
            String contextId;
            String path;
            String filename;

            // Get the operation parameters we need.
            attachmentId = (String)operationParameters.get(PARAMETER_ATTACHMENT_ID);
            contextIid = (String)operationParameters.get(PARAMETER_FILE_CONTEXT_IID);
            contextId = (String)operationParameters.get(PARAMETER_FILE_CONTEXT_ID);
            path = (String)operationParameters.get(PARAMETER_PATH);
            filename = (String)operationParameters.get(PARAMETER_FILENAME);

            // Perform the API operation.
            recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            attachmentDetails = recApi.exportAttachment(attachmentId, contextIid, contextId, path, filename);
            return HashMaps.createSingular(PARAMETER_ATTACHMENT_DETAILS, attachmentDetails);
        }
    }

    public static class ApiRefreshDataObjects extends T8DefaultServerOperation
    {
        private double progress;

        public ApiRefreshDataObjects(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<String> objectIds;
            List<T8DataEntity> entityList;
            List<String> recordIds;
            List<String> drInstanceIds;
            T8DataFilter dataFilter;
            T8DataSession ds;
            String entityId;
            String recordIdFieldId;
            T8DataTransaction itx;
            long startTime;
            int pageOffset;
            int pageSize;
            int recordCount;

            // Get the operation parameters we need.
            recordIds = (List<String>) operationParameters.get(PARAMETER_RECORD_IDS);
            drInstanceIds = (List<String>) operationParameters.get(PARAMETER_DR_IIDS);
            objectIds = (List<String>) operationParameters.get(PARAMETER_DATA_OBJECT_IDS);

            // Set the parameters of the operation.
            dataFilter = new T8DataFilter("@E_DATA_RECORD_DOC");
            dataFilter.addFilterCriterion(DataFilterConjunction.AND, "@E_DATA_RECORD_DOC$RECORD_ID", DataFilterOperator.EQUAL_TO_FIELD, "@E_DATA_RECORD_DOC$ROOT_RECORD_ID", false);
            if (CollectionUtilities.hasContent(recordIds)) dataFilter.addFilterCriterion(DataFilterConjunction.AND, "@E_DATA_RECORD_DOC$RECORD_ID", DataFilterOperator.IN, recordIds, false);
            if (CollectionUtilities.hasContent(drInstanceIds)) dataFilter.addFilterCriterion(DataFilterConjunction.AND, "@E_DATA_RECORD_DOC$DR_INSTANCE_ID", DataFilterOperator.IN, drInstanceIds, false);
            entityId = "@E_DATA_RECORD_DOC";
            recordIdFieldId = "@E_DATA_RECORD_DOC$RECORD_ID";

            // Use the instance transaction for the initial count and retrieval.
            ds = serverContext.getDataManager().getCurrentSession();
            itx = ds.instantTransaction();

            // Count the total number of records for which checksums will be generated.
            recordCount = itx.count(entityId, dataFilter);

            // Set the batch sizes.
            progress = 0.0;
            pageOffset = 0;
            pageSize = 1000;

            // Retrieve the first page of data and start the loop.
            startTime = System.currentTimeMillis();
            entityList = itx.select(entityId, dataFilter, pageOffset, pageSize);
            while ((!stopFlag) && (!entityList.isEmpty()))
            {
                List<String> retrievedIDList;
                int batchOffset;
                int retrievedIDCount;

                // Get the next record ID list from the retrieved data.
                batchOffset = 0;
                retrievedIDList = T8DataUtilities.getEntityFieldValues(entityList, recordIdFieldId);
                retrievedIDCount = retrievedIDList.size();

                // Loop through the retrieved record ID list and generate checksums in batches for optimization.
                while ((!stopFlag) && (batchOffset < retrievedIDCount))
                {
                    T8DataTransaction loopTx = null;
                    int progressCount;
                    String dataFileID;

                    // Get the next record ID to process.
                    dataFileID = retrievedIDList.get(batchOffset);

                    // Retrieve the data file, then save it again to ensure that the required views are refreshed.
                    try
                    {
                        T8DataRecordApi recApi;

                        loopTx = ds.beginTransaction();
                        recApi = loopTx.getApi(T8DataRecordApi.API_IDENTIFIER);
                        recApi.refreshDataObjects(dataFileID, objectIds);
                        loopTx.commit();
                    }
                    catch (Exception e)
                    {
                        if (loopTx != null) loopTx.rollback();
                        throw e;
                    }

                    // Update the progress of the operation.
                    progressCount = pageOffset + batchOffset;
                    if (progressCount > recordCount) progressCount = recordCount;
                    progress = (((progressCount * 1.00) / (recordCount * 1.00)) * 100.00);

                    // Increment the batch offset.
                    batchOffset++;
                }

                // Retrieve the next page.
                pageOffset += pageSize;
                entityList = itx.select(entityId, dataFilter, pageOffset, pageSize);
                LOGGER.log("Batch from offset: " + pageOffset + " " + entityList.size());
            }

            LOGGER.log("Refreshed views for " + recordCount + " records, in " + (System.currentTimeMillis() - startTime) + "ms");
            return null;
        }

        @Override
        public double getProgress(T8Context context)
        {
            return progress;
        }
    }
}
