package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.api.T8DataStringApi;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import java.util.Collection;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datastring.T8DataStringGenerator;
import com.pilog.t8.data.document.datafile.T8DataFileOutputStream;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileDataStringOutputStream implements T8DataFileOutputStream
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataFileDataStringOutputStream.class);

    private final T8DataStringApi dsApi;
    private final T8DataStringGenerator stringGenerator;
    private boolean enabled;

    public T8DataFileDataStringOutputStream(T8DataTransaction tx)
    {
        this.dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
        this.stringGenerator = dsApi.getDataStringGenerator();
        this.enabled = true;
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    @Override
    public void write(DataRecord dataFile, T8DataFileAlteration alteration, Collection<RecordMetaDataType> metaDataTypes) throws Exception
    {
        // Refresh the record's descriptions if required.
        if (enabled)
        {
            if ((metaDataTypes == null)|| (metaDataTypes.contains(RecordMetaDataType.ALL)) || (metaDataTypes.contains(RecordMetaDataType.DESCRIPTION)))
            {
                // Remove existing data strings from the data file so that only the newly generated ones will remain after this method executes.
                dataFile.clearDataStrings();

                // Now generate the new data strings.
                LOGGER.log("Generating descriptions...");
                stringGenerator.refreshDataStrings(dataFile, null, null, null);
            }
        }
    }
}
