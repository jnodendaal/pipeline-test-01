package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8OrganizationApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import java.util.Collection;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datafile.T8DataFileOutputStream;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileTerminologyOutputStream implements T8DataFileOutputStream
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataFileTerminologyOutputStream.class);

    private final T8DataRecordOntologyHandler ontologyHandler;
    private final T8OrganizationApi orgApi;
    private final T8OntologyApi ontApi;
    private final T8TerminologyApi trmApi;
    private Set<String> orgLanguageIdSet;
    private boolean enabled;

    public T8DataFileTerminologyOutputStream(T8DataTransaction tx)
    {
        this.orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
        this.ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        this.trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
        this.ontologyHandler = new T8DataRecordOntologyHandler(tx);
        this.enabled = true;
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    @Override
    public void write(DataRecord dataFile, T8DataFileAlteration alteration, Collection<RecordMetaDataType> metaDataTypes) throws Exception
    {
        // Refresh the affected record terminology
        if (enabled)
        {
            Set<String> updatedRecordIds;

            // Fetcht the language id set if it has not been fetched already.
            if (this.orgLanguageIdSet == null)
            {
                this.orgLanguageIdSet = orgApi.getLanguageIdSet();
            }

            // Insert terminology for all new records.
            for (DataRecord insertedRecord : alteration.getInsertedRecords())
            {
                T8OntologyConcept insertedConcept;

                // Get the updated record's concept.
                insertedConcept = insertedRecord.getOntology();

                // Normalize the terminology of the updated record.
                ontologyHandler.normalizeRecordTerminology(insertedRecord, orgLanguageIdSet);

                // Save all terms.
                for (T8OntologyTerm term : insertedConcept.getTerms())
                {
                    ontApi.insertTerm(term, false);
                }

                // Save all definitions.
                for (T8OntologyDefinition definition : insertedConcept.getDefinitions())
                {
                    ontApi.insertDefinition(definition);
                }

                // Save terminology.
                trmApi.saveTerminology(insertedConcept.createTerminologies(orgApi.getRootOrgId()));
            }

            // Update the terminology for all affected existing records.
            updatedRecordIds = alteration.getOntologyUpdatedRecordIdSet(); // We have to include records where content did not change but ontology did.
            updatedRecordIds.addAll(alteration.getUpdatedRecordIds());
            for (String updatedRecordId : updatedRecordIds)
            {
                T8OntologyConcept updatedConcept;
                DataRecord updatedRecord;

                // Get the updated record's concept.
                updatedRecord = dataFile.findDataRecord(updatedRecordId);
                updatedConcept = updatedRecord.getOntology();

                // Normalize the terminology of the updated record.
                ontologyHandler.normalizeRecordTerminology(updatedRecord, orgLanguageIdSet);

                // Save all terms.
                for (T8OntologyTerm term : updatedConcept.getTerms())
                {
                    ontApi.saveTerm(term, false, false);
                }

                // Save all definitions.
                for (T8OntologyDefinition definition : updatedConcept.getDefinitions())
                {
                    ontApi.saveDefinition(definition);
                }

                // Save terminology.
                trmApi.saveTerminology(updatedConcept.createTerminologies(orgApi.getRootOrgId()));
            }
        }
    }
}
