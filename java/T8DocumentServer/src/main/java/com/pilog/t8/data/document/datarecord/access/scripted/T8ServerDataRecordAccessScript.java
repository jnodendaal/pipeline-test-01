package com.pilog.t8.data.document.datarecord.access.scripted;

import com.pilog.epic.ParserContext;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8OrganizationApi;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datarecord.access.layer.DataRecordAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.FieldAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.PropertyAccessLayer;
import com.pilog.t8.data.document.datarecord.access.layer.SectionAccessLayer;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.document.validation.T8DataFileValidationReport;
import com.pilog.t8.definition.data.document.datarecord.access.logic.T8DataAccessLogicScriptDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.script.T8DefaultServerContextScript;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ServerDataRecordAccessScript extends T8DefaultServerContextScript implements T8DataRecordAccessScript
{
    private final T8ServerContextScriptDefinition definition;
    private final DocPathExpressionEvaluator docPathEvaluator;
    private DataRecord rootDataRecord;
    private DataRecord focusDataRecord;
    private T8DataFileValidationReport validationReport;

    public T8ServerDataRecordAccessScript(T8Context context, T8ServerContextScriptDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.docPathEvaluator = getDocPathEvaluatorFactory().getEvaluator();

        // Prepare the script so that it does not have to be done every time it is executed.
        try
        {
            this.prepareScript();
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while preparing access logic script: " + definition, e);
        }
    }

    @Override
    public synchronized T8DataFileValidationReport applyScript(DataRecord dataRecord)
    {
        try
        {
            Map<String, Object> inputParameters;
            Map<String, Object> outputParameters;
            T8DataTransaction tx;
            T8OrganizationApi orgApi;
            T8OntologyApi ontApi;

            // Get the api's required.
            tx = serverContext.getDataManager().getCurrentSession().getTransaction();
            if (tx == null) tx =  serverContext.getDataManager().getCurrentSession().instantTransaction();
            orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);

            // Set the local variable on which script methods will be executed.
            this.rootDataRecord = dataRecord.getDataFile();
            this.focusDataRecord = dataRecord;
            this.validationReport = new T8DataFileValidationReport();

            // Create a map of script input variables.
            inputParameters = new HashMap<>();
            inputParameters.put(T8DataAccessLogicScriptDefinition.SCRIPT_PARAMETER_ROOT_DATA_RECORD, rootDataRecord);
            inputParameters.put(T8DataAccessLogicScriptDefinition.SCRIPT_PARAMETER_FOCUS_DATA_RECORD, focusDataRecord);
            inputParameters.put(T8DataAccessLogicScriptDefinition.SCRIPT_PARAMETER_ONTOLOGY_DATA_STRUCTURE, ontApi.getOntologyStructure());
            inputParameters.put(T8DataAccessLogicScriptDefinition.SCRIPT_PARAMETER_ORGANIZATION_STRUCTURE, orgApi.getOrganizationStructure());

            // Execute the script.
            outputParameters = executePreparedScript(inputParameters);
            return validationReport;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while executing access logic: " + definition, e);
        }
    }

    @Override
    protected void addProgramImports(ParserContext parserContext) throws Exception
    {
        // Make sure the default imports are performed.
        super.addProgramImports(parserContext);

        // Method imports.
        parserContext.addMethodImport("setVisible", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setVisible", String.class, boolean.class));
        parserContext.addMethodImport("setVisibleUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setVisible", Object.class, String.class, boolean.class));
        parserContext.addMethodImport("setEditable", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setEditable", String.class, boolean.class));
        parserContext.addMethodImport("setEditableUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setEditable", Object.class, String.class, boolean.class));
        parserContext.addMethodImport("setRequired", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setRequired", String.class, boolean.class));
        parserContext.addMethodImport("setRequiredUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setRequired", Object.class, String.class, boolean.class));
        parserContext.addMethodImport("setFftEditable", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setFFTEditable", String.class, boolean.class));
        parserContext.addMethodImport("setFftEditableUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setFftEditable", Object.class, String.class, boolean.class));
        parserContext.addMethodImport("setInsertEnabled", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setInsertEnabled", String.class, boolean.class));
        parserContext.addMethodImport("setInsertEnabledUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setInsertEnabled", Object.class, String.class, boolean.class));
        parserContext.addMethodImport("setUpdateEnabled", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setUpdateEnabled", String.class, boolean.class));
        parserContext.addMethodImport("setUpdateEnabledUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setUpdateEnabled", Object.class, String.class, boolean.class));
        parserContext.addMethodImport("setDeleteEnabled", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setDeleteEnabled", String.class, boolean.class));
        parserContext.addMethodImport("setDeleteEnabledUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setDeleteEnabled", Object.class, String.class, boolean.class));
        parserContext.addMethodImport("getValue", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("getValue", String.class));
        parserContext.addMethodImport("getValueUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("getValue", Object.class, String.class));
        parserContext.addMethodImport("setValue", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setValue", String.class, String.class, String.class, String.class));
        parserContext.addMethodImport("setValueUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setValue", Object.class, String.class, String.class, String.class, String.class));
        parserContext.addMethodImport("setFilterConceptIdList", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setFilterConceptIDList", String.class, List.class));
        parserContext.addMethodImport("setFilterConceptIdListUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setFilterConceptIdList", Object.class, String.class, List.class));
        parserContext.addMethodImport("setFilterConceptGroupIdList", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setFilterConceptGroupIDList", String.class, List.class));
        parserContext.addMethodImport("setFilterConceptGroupIdListUsing", this, T8ServerDataRecordAccessScript.class.getDeclaredMethod("setFilterConceptGroupIdList", Object.class, String.class, List.class));
        parserContext.addMethodImport("raiseError", this, this.getClass().getDeclaredMethod("raiseError", String.class, String.class, String.class, String.class));
    }

    private Object accessDocPath(Object dataObject, String pathExpression)
    {
        return docPathEvaluator.evaluateExpression(dataObject, pathExpression);
    }

    public void raiseError(String recordID, String propertyID, String fieldID, String message)
    {
        validationReport.addRecordValueError(rootDataRecord.findDataRecord(recordID), propertyID, fieldID, message);
    }

    public void setVisible(String pathExpression, boolean visible)
    {
        setVisible(this.rootDataRecord, pathExpression, visible);
    }

    public void setVisible(Object dataObject, String pathExpression, boolean visible)
    {
        Object result;

        result = accessDocPath(dataObject, pathExpression);
        if (result instanceof List)
        {
            for (Object listObject : (List)result)
            {
                setVisible(pathExpression, listObject, visible);
            }
        }
        else setVisible(pathExpression, result, visible);
    }

    private void setVisible(String pathExpresssion, Object accessLayerObject, boolean visible)
    {
        if (accessLayerObject instanceof DataRecordAccessLayer)
        {
            ((DataRecordAccessLayer)accessLayerObject).setVisible(visible);
        }
        else if (accessLayerObject instanceof SectionAccessLayer)
        {
            ((SectionAccessLayer)accessLayerObject).setVisible(visible);
        }
        else if (accessLayerObject instanceof PropertyAccessLayer)
        {
            ((PropertyAccessLayer)accessLayerObject).setVisible(visible);
        }
        else if (accessLayerObject instanceof FieldAccessLayer)
        {
            ((FieldAccessLayer)accessLayerObject).setVisible(visible);
        }
        else throw new RuntimeException("Document path does not identify an access layer object: " + pathExpresssion);
    }

    public void setEditable(String pathExpression, boolean editable)
    {
        setEditable(this.rootDataRecord, pathExpression, editable);
    }

    public void setEditable(Object dataObject, String pathExpression, boolean editable)
    {
        Object result;

        result = accessDocPath(dataObject, pathExpression);
        if (result instanceof List)
        {
            for (Object listObject : (List)result)
            {
                setEditable(pathExpression, listObject, editable);
            }
        }
        else setEditable(pathExpression, result, editable);
    }

    private void setEditable(String pathExpresssion, Object accessLayerObject, boolean editable)
    {
        if (accessLayerObject instanceof DataRecordAccessLayer)
        {
            ((DataRecordAccessLayer)accessLayerObject).setEditable(editable);
        }
        else if (accessLayerObject instanceof SectionAccessLayer)
        {
            ((SectionAccessLayer)accessLayerObject).setEditable(editable);
        }
        else if (accessLayerObject instanceof PropertyAccessLayer)
        {
            ((PropertyAccessLayer)accessLayerObject).setEditable(editable);
        }
        else if (accessLayerObject instanceof FieldAccessLayer)
        {
            ((FieldAccessLayer)accessLayerObject).setEditable(editable);
        }
        else throw new RuntimeException("Document path does not identify an access layer object: " + pathExpresssion);
    }

    public void setRequired(String pathExpresssion, boolean required)
    {
        setRequired(this.rootDataRecord, pathExpresssion, required);
    }

    public void setRequired(Object dataObject, String pathExpression, boolean required)
    {
        Object result;

        result = accessDocPath(dataObject, pathExpression);
        if (result instanceof List)
        {
            for (Object listObject : (List)result)
            {
                setRequired(pathExpression, listObject, required);
            }
        }
        else setRequired(pathExpression, result, required);
    }

    private void setRequired(String pathExpresssion, Object accessLayerObject, boolean required)
    {
        if (accessLayerObject instanceof PropertyAccessLayer)
        {
            ((PropertyAccessLayer)accessLayerObject).setRequired(required);
        }
        else if (accessLayerObject instanceof FieldAccessLayer)
        {
            ((FieldAccessLayer)accessLayerObject).setRequired(required);
        }
        else throw new RuntimeException("Document path does not identify a Property or Field access layer object: " + pathExpresssion);
    }

    public void setInsertEnabled(String pathExpression, boolean required)
    {
        setInsertEnabled(this.rootDataRecord, pathExpression, required);
    }

    public void setInsertEnabled(Object dataObject, String pathExpresssion, boolean insertEnabled)
    {
        Object result;

        result = accessDocPath(dataObject, pathExpresssion);
        if (result instanceof List)
        {
            for (Object listObject : (List)result)
            {
                setInsertEnabled(pathExpresssion, listObject, insertEnabled);
            }
        }
        else setInsertEnabled(pathExpresssion, result, insertEnabled);
    }

    private void setInsertEnabled(String pathExpresssion, Object accessLayerObject, boolean required)
    {
        if (accessLayerObject instanceof PropertyAccessLayer)
        {
            ((PropertyAccessLayer)accessLayerObject).setInsertEnabled(required);
        }
        else if (accessLayerObject instanceof FieldAccessLayer)
        {
            ((FieldAccessLayer)accessLayerObject).setInsertEnabled(required);
        }
        else throw new RuntimeException("Document path does not identify a Property or Field access layer object: " + pathExpresssion);
    }

    public void setUpdateEnabled(String pathExpression, boolean updateEnabled)
    {
        setUpdateEnabled(this.rootDataRecord, pathExpression, updateEnabled);
    }

    public void setUpdateEnabled(Object dataObject, String pathExpression, boolean updateEnabled)
    {
        Object result;

        result = accessDocPath(dataObject, pathExpression);
        if (result instanceof List)
        {
            for (Object listObject : (List)result)
            {
                setUpdateEnabled(pathExpression, listObject, updateEnabled);
            }
        }
        else setUpdateEnabled(pathExpression, result, updateEnabled);
    }

    private void setUpdateEnabled(String pathExpresssion, Object accessLayerObject, boolean required)
    {
        if (accessLayerObject instanceof PropertyAccessLayer)
        {
            ((PropertyAccessLayer)accessLayerObject).setUpdateEnabled(required);
        }
        else if (accessLayerObject instanceof FieldAccessLayer)
        {
            ((FieldAccessLayer)accessLayerObject).setUpdateEnabled(required);
        }
        else throw new RuntimeException("Document path does not identify a Property or Field access layer object: " + pathExpresssion);
    }

    public void setDeleteEnabled(String pathExpression, boolean deleteEnabled)
    {
        setDeleteEnabled(this.rootDataRecord, pathExpression, deleteEnabled);
    }

    public void setDeleteEnabled(Object dataObject, String pathExpresssion, boolean deleteEnabled)
    {
        Object result;

        result = accessDocPath(dataObject, pathExpresssion);
        if (result instanceof List)
        {
            for (Object listObject : (List)result)
            {
                setDeleteEnabled(pathExpresssion, listObject, deleteEnabled);
            }
        }
        else setDeleteEnabled(pathExpresssion, result, deleteEnabled);
    }

    private void setDeleteEnabled(String pathExpresssion, Object accessLayerObject, boolean required)
    {
        if (accessLayerObject instanceof PropertyAccessLayer)
        {
            ((PropertyAccessLayer)accessLayerObject).setDeleteEnabled(required);
        }
        else if (accessLayerObject instanceof FieldAccessLayer)
        {
            ((FieldAccessLayer)accessLayerObject).setDeleteEnabled(required);
        }
        else throw new RuntimeException("Document path does not identify a Property or Field access layer object: " + pathExpresssion);
    }

    public void setFFTEditable(String pathExpresssion, boolean editable)
    {
        setFftEditable(this.rootDataRecord, pathExpresssion, editable);
    }

    public void setFftEditable(Object dataObject, String pathExpresssion, boolean fftEditable)
    {
        Object result;

        result = accessDocPath(dataObject, pathExpresssion);
        if (result instanceof List)
        {
            for (Object listObject : (List)result)
            {
                setFftEditable(pathExpresssion, listObject, fftEditable);
            }
        }
        else setFftEditable(pathExpresssion, result, fftEditable);
    }

    private void setFftEditable(String pathExpresssion, Object accessLayerObject, boolean editable)
    {
        if (accessLayerObject instanceof DataRecordAccessLayer)
        {
            ((DataRecordAccessLayer)accessLayerObject).setFFTEditable(editable);
        }
        else if (accessLayerObject instanceof PropertyAccessLayer)
        {
            ((PropertyAccessLayer)accessLayerObject).setFFTEditable(editable);
        }
        else throw new RuntimeException("Document path does not identify an access layer object: " + pathExpresssion);
    }

    public void setFilterConceptIDList(String pathExpresssion, List<String> idList)
    {
        setFilterConceptIdList(this.rootDataRecord, pathExpresssion, idList);
    }

    public void setFilterConceptIdList(Object dataObject, String pathExpression, List<String> idList)
    {
        Object result;

        result = accessDocPath(dataObject, pathExpression);
        if (result instanceof List)
        {
            for (Object listObject : (List)result)
            {
                setFilterConceptIdList(pathExpression, listObject, idList);
            }
        }
        else setFilterConceptIdList(pathExpression, result, idList);
    }

    private void setFilterConceptIdList(String pathExpresssion, Object accessLayerObject, List<String> idList)
    {
        if (accessLayerObject instanceof PropertyAccessLayer)
        {
            ((PropertyAccessLayer)accessLayerObject).setFilterConceptIDSet(idList != null ? new HashSet<String>(idList) : null);
        }
        else if (accessLayerObject instanceof FieldAccessLayer)
        {
            ((FieldAccessLayer)accessLayerObject).setFilterConceptIDSet(idList != null ? new HashSet<String>(idList) : null);
        }
        else throw new RuntimeException("Document path does not identify a Property or Field access layer object: " + pathExpresssion);
    }

    public void setFilterConceptGroupIDList(String pathExpression, List<String> idList)
    {
        setFilterConceptGroupIdList(this.rootDataRecord, pathExpression, idList);
    }

    public void setFilterConceptGroupIdList(Object dataObject, String pathExpression, List<String> idList)
    {
        Object result;

        result = accessDocPath(dataObject, pathExpression);
        if (result instanceof List)
        {
            for (Object listObject : (List)result)
            {
                setFilterConceptGroupIdList(pathExpression, listObject, idList);
            }
        }
        else setFilterConceptGroupIdList(pathExpression, result, idList);
    }

    private void setFilterConceptGroupIdList(String pathExpresssion, Object accessLayerObject, List<String> idList)
    {
        if (accessLayerObject instanceof PropertyAccessLayer)
        {
            ((PropertyAccessLayer)accessLayerObject).setFilterConceptGroupIDSet(idList != null ? new HashSet<String>(idList) : null);
        }
        else if (accessLayerObject instanceof FieldAccessLayer)
        {
            ((FieldAccessLayer)accessLayerObject).setFilterConceptGroupIDSet(idList != null ? new HashSet<String>(idList) : null);
        }
        else throw new RuntimeException("Document path does not identify a Property or Field access layer object: " + pathExpresssion);
    }

    public String getValue(String pathExpression)
    {
        return getValue(this.rootDataRecord, pathExpression);
    }

    public String getValue(Object dataObject, String pathExpression)
    {
        Object target;

        target = accessDocPath(dataObject, pathExpression);
        if (target != null)
        {
            if (target instanceof RecordProperty)
            {
                return ((RecordProperty)target).getPlainValue();
            }
            else if (target instanceof RecordValue)
            {
                return ((RecordValue)target).getPlainValue();
            }
            else if (target instanceof List)
            {
                List targetList;

                targetList = (List)target;
                if (targetList.size() > 1) throw new RuntimeException("Target of path expression '" + pathExpression + "' must be a single property or value.");
                else if (targetList.size() > 0)
                {
                    Object targetElement;

                    targetElement = targetList.get(0);
                    if (targetElement instanceof RecordProperty)
                    {
                        return ((RecordProperty)targetElement).getPlainValue();
                    }
                    else if (targetElement instanceof RecordValue)
                    {
                        return ((RecordValue)targetElement).getPlainValue();
                    }
                    else throw new RuntimeException("Target of path expression '" + pathExpression + "' must be a property or value.");
                }
                else return null;
            }
            else throw new RuntimeException("Target of path expression '" + pathExpression + "' must be a property or value.");
        }
        else return null;
    }

    public void setValue(String pathExpression, String propertyId, String fieldId, String plainValue)
    {
        setValue(this.rootDataRecord, pathExpression, propertyId, fieldId, plainValue);
    }

    public void setValue(Object dataObject, String pathExpression, String propertyId, String fieldId, String plainValue)
    {
        Object target;

        target = accessDocPath(dataObject, pathExpression);
        if (target != null)
        {
            if (target instanceof DataRecord)
            {
                ((DataRecord)target).setValue(propertyId, fieldId, plainValue, false);
            }
            else if (target instanceof List)
            {
                for (Object targetElement : (List)target)
                {
                    if (targetElement instanceof DataRecord)
                    {
                        ((DataRecord)targetElement).setValue(propertyId, fieldId, plainValue, false);
                    }
                    else throw new RuntimeException("Target of path expression '" + pathExpression + "' must be a Data Record.");
                }
            }
            else throw new RuntimeException("Target of path expression '" + pathExpression + "' must be a Data Record.");
        }
    }
}

