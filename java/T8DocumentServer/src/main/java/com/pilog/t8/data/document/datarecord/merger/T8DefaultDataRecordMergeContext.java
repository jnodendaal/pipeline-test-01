package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.api.T8DataRequirementApi;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDataRecordMergeContext implements T8DataRecordMergeContext
{
    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final OntologyProvider ontologyProvider;
    private final TerminologyProvider terminologyProvider;
    private final DataRequirementInstanceProvider drInstanceProvider;

    public T8DefaultDataRecordMergeContext(T8DataTransaction tx)
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.ontologyProvider = ((T8OntologyApi)tx.getApi(T8OntologyApi.API_IDENTIFIER)).getOntologyProvider();
        this.terminologyProvider = ((T8TerminologyApi)tx.getApi(T8TerminologyApi.API_IDENTIFIER)).getTerminologyProvider();
        this.drInstanceProvider = ((T8DataRequirementApi)tx.getApi(T8DataRequirementApi.API_IDENTIFIER)).getDrInstanceProvider();
    }

    @Override
    public T8DataTransaction getTransaction()
    {
        return tx;
    }

    @Override
    public DataRecord copyDataRecord(DataRecord sourceRecord)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDefaultContentLanguageID()
    {
        return sessionContext.getContentLanguageIdentifier();
    }

    @Override
    public T8Context getContext()
    {
        return context;
    }

    @Override
    public T8ExpressionEvaluator getExpressionEvaluator()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataRecordValueStringGenerator getFFTGenerator()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TerminologyProvider getTerminologyProvider()
    {
        return terminologyProvider;
    }

    @Override
    public OntologyProvider getOntologyProvider()
    {
        return ontologyProvider;
    }

    @Override
    public DataRequirementInstanceProvider getDRInstanceProvider()
    {
        return drInstanceProvider;
    }

    @Override
    public T8DataRecordInstanceMerger getInstanceMerger(DataRecord destinationRecord, DataRecord sourceRecord)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataRecordPropertyMerger getPropertyMerger(RecordProperty destinationProperty, RecordProperty sourceProperty)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataRecordFieldMerger getFieldMerger(RecordValue destinationField, RecordValue sourceField)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataRecordValueMerger getValueMerger(RecordValue destinationValue, RecordValue sourceValue)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
