package com.pilog.t8.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.value.Composite;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarequirement.PrescribedValue;
import com.pilog.t8.data.document.datarequirement.PrescribedValueRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.prescribedvalue.T8PrescribedValueDataHandler;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.StringValue;
import com.pilog.t8.data.document.datarequirement.OldPrescribedValue;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.utilities.strings.Strings;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import static com.pilog.t8.data.document.requirementtype.RequirementType.CONTROLLED_CONCEPT;

/**
 * @author Bouwer du Preez
 */
public class T8PrescribedValueApi implements T8Api
{
    public static final String API_IDENTIFIER = "@API_PRESCRIBED_VALUE";

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8SessionContext sessionContext;
    private final T8ServerContext serverContext;
    private final T8PerformanceStatistics stats;
    private final T8PrescribedValueDataHandler prescribedValueDataHandler;
    private final Map<String, List<PrescribedValue>> drPrescribedValueCache; // Caches prescribed values for data requirements <DR_ID, List<OldPrescribedValue>>.

    public T8PrescribedValueApi(T8DataTransaction tx)
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.drPrescribedValueCache = new HashMap<>();
        this.prescribedValueDataHandler = new T8PrescribedValueDataHandler(context);
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
    }

    private List<PrescribedValue> getDataRequirementPrescribedValues(String drId) throws Exception
    {
        List<PrescribedValue> prescribedValues;

        prescribedValues = drPrescribedValueCache.get(drId);
        if (prescribedValues != null)
        {
            return prescribedValues;
        }
        else
        {
            prescribedValues = prescribedValueDataHandler.retrieveDataRequirementPrescribedValues(tx, drId);
            drPrescribedValueCache.put(drId, prescribedValues);
            return prescribedValues;
        }
    }

    public void replaceOldPrescribedValues(Collection<DataRecord> records) throws Exception
    {
        // Loop through all of the input records.
        for (DataRecord dataRecord : records)
        {
            replaceOldPrescribedValues(dataRecord.getAllPropertyValues());
        }
    }

    private void replaceOldPrescribedValues(List<RecordValue> recordValues) throws Exception
    {
        for (RecordValue recordValue : recordValues)
        {
            if (recordValue.getValueRequirement().isPrescribed())
            {
                replaceOldPrescribedValues(recordValue);
            }
            else replaceOldPrescribedValues(recordValue.getSubValues());
        }
    }

    private void replaceOldPrescribedValues(RecordValue value) throws Exception
    {
        List<PrescribedValue> prescribedValues;
        RequirementType requirementType;
        String drId;

        requirementType = value.getRequirementType();
        drId = value.getDataRequirementID();
        prescribedValues = getDataRequirementPrescribedValues(drId);
        for (PrescribedValue prescribedValue : prescribedValues)
        {
            if ((Objects.equals(prescribedValue.getPropertyID(), value.getPropertyID())) && (Objects.equals(prescribedValue.getFieldID(), value.getFieldID())) && (prescribedValue.getRequirementType() == requirementType))
            {
                switch (requirementType)
                {
                    case STRING_TYPE:
                        StringValue stringValue;
                        String currentString;

                        stringValue = (StringValue)value;
                        currentString = Strings.trimToNull(stringValue.getString());
                        if (currentString != null)
                        {
                            for (OldPrescribedValue oldValue : prescribedValue.getOldValues())
                            {
                                if (currentString.equalsIgnoreCase(oldValue.getValue()))
                                {
                                    stringValue.setPrescribedValueID(prescribedValue.getValueID());
                                    stringValue.setString(prescribedValue.getValue());
                                    break;
                                }
                            }
                        }
                        break;
                    case CONTROLLED_CONCEPT:
                        ControlledConcept controlledConcept;
                        String currentConceptId;
                        String currentTerm;

                        controlledConcept = (ControlledConcept)value;
                        currentConceptId = controlledConcept.getConceptId();
                        currentTerm = Strings.trimToNull(controlledConcept.getTerm());

                        if (currentConceptId != null)
                        {
                            for (OldPrescribedValue oldValue : prescribedValue.getOldValues())
                            {
                                if (currentConceptId.equalsIgnoreCase(oldValue.getConceptId()))
                                {
                                    controlledConcept.setPrescribedValueID(prescribedValue.getValueID());
                                    controlledConcept.setConceptId(prescribedValue.getConceptID(), false);
                                    controlledConcept.setTerm(null);
                                    break;
                                }
                            }
                        }
                        else if (currentTerm != null)
                        {
                            for (OldPrescribedValue oldValue : prescribedValue.getOldValues())
                            {
                                if (currentTerm.equalsIgnoreCase(oldValue.getConceptTerm()))
                                {
                                    controlledConcept.setPrescribedValueID(prescribedValue.getValueID());
                                    controlledConcept.setConceptId(prescribedValue.getConceptID(), false);
                                    controlledConcept.setTerm(null);
                                    break;
                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    public void savePrescribedValues(DataRecord dataRecord) throws Exception
    {
        List<RecordProperty> recordProperties;

        // Check each of the properties.
        recordProperties = dataRecord.getRecordProperties();
        for (RecordProperty recordProperty : recordProperties)
        {
            RecordValue recordValue;

            // Get the property value and if it is not null, save any potential standard values.
            recordValue = recordProperty.getRecordValue();
            if (recordValue != null)
            {
                savePrescribedValue(recordValue);
            }
        }
    }

    private void savePrescribedValue(RecordValue recordValue) throws Exception
    {
        ValueRequirement valueRequirement;

        valueRequirement = recordValue.getValueRequirement();
        if (valueRequirement.getRequirementType() == RequirementType.COMPOSITE_TYPE)
        {
            // Process each field value individually.
            for (Field field : ((Composite)recordValue).getFields())
            {
                RecordValue fieldValue;

                fieldValue = field.getFirstSubValue();
                if (fieldValue != null) savePrescribedValue(fieldValue);
            }
        }

        if (valueRequirement instanceof PrescribedValueRequirement)
        {
            // Process the top level requirement first
            if (((PrescribedValueRequirement)valueRequirement).isPrescribed())
            {
                setDataRequirementValueId(recordValue);
            }
            else // Process the sub-values if the top level is not standardized
            {
                for (RecordValue subValue : recordValue.getSubValues())
                {
                    savePrescribedValue(subValue);
                }
            }
        }
        else
        {
            // Assume that there might be top level types which consist of sub-types where
            // the top level is not standardizable, but all, or some of the sub-types are
            for (RecordValue subValue : recordValue.getSubValues())
            {
                savePrescribedValue(subValue);
            }
        }
    }

    /**
     * Saves the specified data requirement value. If the value already exists,
     * it is simply updated. Otherwise a new entry is created.<br/>
     * <br/>
     *
     * @param prescribedValue The {@code PrescribedValue} which
     *      defines the details of the data requirement value to be saved
     *
     * @throws Exception If there is an error during the update or insertion of
     *      the data requirement value
     */
    public void savePrescribedValue(PrescribedValue prescribedValue) throws Exception
    {
        this.prescribedValueDataHandler.savePrescribedValue(tx, prescribedValue);
    }

    /**
     * Saves the specified data requirement value. If the value already exists,
     * it is simply updated. Otherwise a new entry is created.<br/>
     * <br/>
     *
     * @param prescribedValues The {@code PrescribedValue} which
     *      defines the details of the data requirement value to be saved
     *
     * @throws Exception If there is an error during the update or insertion of
     *      the data requirement value
     */
    public void savePrescribedValues(Collection<PrescribedValue> prescribedValues) throws Exception
    {
        String rootOrgId;

        rootOrgId = sessionContext.getRootOrganizationIdentifier();
        for (PrescribedValue prescribedValue : prescribedValues)
        {
            String prescribedValueOrgId;

            prescribedValueOrgId = prescribedValue.getOrganizationID();
            if (rootOrgId.equals(prescribedValueOrgId))
            {
                prescribedValueDataHandler.savePrescribedValue(tx, prescribedValue);
            }
            else throw new IllegalArgumentException("Cannot save prescribed values belonging to different organization: " + prescribedValueOrgId);
        }
    }

    public void insertPrescribedValues(Collection<PrescribedValue> prescribedValues) throws Exception
    {
        String rootOrgId;

        rootOrgId = sessionContext.getRootOrganizationIdentifier();
        for (PrescribedValue prescribedValue : prescribedValues)
        {
            String prescribedValueOrgId;

            prescribedValueOrgId = prescribedValue.getOrganizationID();
            if (rootOrgId.equals(prescribedValueOrgId))
            {
                prescribedValueDataHandler.insertPrescribedValue(tx, prescribedValue);
            }
            else throw new IllegalArgumentException("Cannot insert prescribed values belonging to different organization: " + prescribedValueOrgId);
        }
    }

    public void updatePrescribedValues(Collection<PrescribedValue> prescribedValues) throws Exception
    {
        String rootOrgId;

        rootOrgId = sessionContext.getRootOrganizationIdentifier();
        for (PrescribedValue prescribedValue : prescribedValues)
        {
            String prescribedValueOrgId;

            prescribedValueOrgId = prescribedValue.getOrganizationID();
            if (rootOrgId.equals(prescribedValueOrgId))
            {
                prescribedValueDataHandler.updatePrescribedValue(tx, prescribedValue);
            }
            else throw new IllegalArgumentException("Cannot update prescribed values belonging to different organization: " + prescribedValueOrgId);
        }
    }

    /**
     * Removes the specified prescribed values. If the value is in use, it is
     * not deleted.
     * </br>The value ID of a existing prescribed value, that is not in use,
     * needs to be passed.<br/>
     *
     * @param tx The {@code T8DataTransaction} with an open connection to the
     *      database
     * @param svIdentifiersList A list of the Prescribed Value ID's that are to
     *      be deleted.
     *
     * @throws Exception If there is an error during the deletion of the data
     *      requirement value
     */
    public void deletePrescribedValues(List<String> svIdentifiersList) throws Exception
    {
        //If no prescribed value ID's are given
        if (svIdentifiersList == null) throw new IllegalArgumentException("The Prescribed Value ID List is required.");

        //Delete the values with no usages
        prescribedValueDataHandler.deletePrescribedValues(tx, svIdentifiersList);
    }

    /**
     * Checks the prescribed values list for the combination of the data
     * requirement, property and field identifiers. If a prescribed value
     * matching the input RecordValue is found, the input RecordValue's
     * prescribed Value Id is updated to correspond to the matched value.
     * If not matching prescribed value is found, a new T8PrescribedValue
     * is created and inserted and its value Id is set on the input RecordValue.
     *
     * @param tx The {@code T8DataTransaction} which gives the current
     *      process access to the database.
     * @param recordValue The specific value to be matched or added for
     *      standardization
     */
    private void setDataRequirementValueId(RecordValue recordValue) throws Exception
    {
        PrescribedValue recordValueAsPrescribedValue;

        // Make sure we remove the original standard value ID, if there was one.
        recordValue.setPrescribedValueID(null);

        // Create a PrescribedValue object from the record value.
        recordValueAsPrescribedValue = PrescribedValue.fromRecordValue(recordValue, sessionContext.getRootOrganizationIdentifier());

        // If the recordValueAsPrescribedValue is null the dataEntitry should be skipped and not saved.
        if (recordValueAsPrescribedValue != null)
        {
            List<PrescribedValue> prescribedValues;
            String drId;

            // Get the details of the record value.
            drId = recordValue.getDataRequirementID();

            // Get all prescribed values for the data requirement in question.
            prescribedValues = getDataRequirementPrescribedValues(drId);

            // If one of the prescribed values matches the RecordValue we received as input, set the RecordValue's value ID accordingly.
            for (PrescribedValue prescribedValue : prescribedValues)
            {
                if (prescribedValue.isEquivalentValue(recordValueAsPrescribedValue))
                {
                    recordValue.setPrescribedValueID(prescribedValue.getValueID());
                    return; // We found a matching Prescribed Value.
                }
            }

            // Insert the new prescribed value.
            recordValueAsPrescribedValue.setValueID(T8IdentifierUtilities.createNewGUID());
            prescribedValueDataHandler.insertPrescribedValue(tx, recordValueAsPrescribedValue);

            // Set the RecordValue's value Id to the new prescribed value's Id.
            recordValue.setPrescribedValueID(recordValueAsPrescribedValue.getValueID());
        }
    }

    /**
     * Replaces all the prescribed value references specified by the supplied
     * filter with the supplied prescribed value.  This method does not affect
     * the prescribed value collection at all, it simply updates record data to
     * use a specified prescribed value in place of others.
     *
     * @param prescribedValueIDList The prescribed value ID list used to
     *      identify the prescribed values to be replaced.
     * @param replacementValueId The {@code String} prescribed value to replace
     *      the filtered values.
     *
     * @return The {@code Integer} number of references which have been replaced
     *      with the specified prescribed value.
     *
     * @throws Exception If there is any error while updating the prescribed
     *      value references with the new value.
     */
    public Integer replacePrescribedValueReferences(List<String> prescribedValueIDList, String replacementValueId) throws Exception
    {
        return this.prescribedValueDataHandler.replacePrescribedValueReferences(tx, prescribedValueIDList, replacementValueId);
    }

    /**
     * Replaces all the prescribed values and references specified by the
     * supplied filter with the supplied prescribed value.  This method replaces
     * the filtered prescribed values in the base list as well as in all record
     * data using the prescribed values to be replaced.
     *
     * @param prescribedValueIDList The prescribed value ID list used to
     *      identify the prescribed values to be replaced.
     * @param replacementValueID The {@code String} prescribed value to replace
     *      the filtered values.
     *
     * @return The {@code Integer} number of references which have been replaced
     *      with the specified prescribed value.
     *
     * @throws Exception If there is any error while updating the prescribed
     *      value references with the new value.
     */
    public Integer replacePrescribedValues(List<String> prescribedValueIDList, String replacementValueID) throws Exception
    {
        return this.prescribedValueDataHandler.replacePrescribedValues(tx, prescribedValueIDList, replacementValueID);
    }

    /**
     * Saves the specified concept and sets the old value accordingly for all affected prescribed values.
     * @param concept The new concept to save, updating affected prescribed values.
     * @throws Exception
     */
    public void savePrescribedConcept(T8OntologyConcept concept) throws Exception
    {
         T8OntologyApi ontApi;
         T8OntologyConcept oldConcept;
         String drvEntityIdentifier;
         T8DataFilter filter;
         List<T8DataEntity> conceptUsages;

        drvEntityIdentifier = DATA_REQUIREMENT_VALUE_DE_IDENTIFIER;

        // API for saving and using concept.
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);

        // Get the old concept before saving the new concept.
        oldConcept = ontApi.retrieveConcept(concept.getID(), false, true, false, false, false);

        // Perform the API operation (Save updated concept).
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        // Create a filter to find the valueId's that used the concept.
        filter = new T8DataFilter(drvEntityIdentifier);
        filter.addFilterCriterion(T8DataFilterClause.DataFilterConjunction.AND, drvEntityIdentifier + EF_CONCEPT_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, concept.getID(), false);

        // Get the valueId's that use the concept
        conceptUsages = tx.select(drvEntityIdentifier, filter);

        //If the term of the concept did change then save the old values
        for (T8OntologyTerm oldTerm : oldConcept.getTerms())
        {
            T8OntologyTerm newTerm;

            newTerm = concept.getTerm(oldTerm.getID());
            if (!Objects.equals(oldTerm.getTerm(), newTerm.getTerm()))
            {
                for(T8DataEntity currentValue : conceptUsages)
                {
                    OldPrescribedValue oldValue;

                    //Build up oldPrescribedValue to save
                    oldValue = new OldPrescribedValue(CONTROLLED_CONCEPT, T8IdentifierUtilities.createNewGUID());
                    oldValue.setConceptId(oldConcept.getID());
                    oldValue.setConceptTerm(oldTerm.getTerm());
                    oldValue.setValueId(currentValue.getFieldValue(drvEntityIdentifier+EF_VALUE_ID).toString());

                    //Save old value concept
                    prescribedValueDataHandler.insertOldPrescribedValue(tx, oldValue);
                }
            }
        }
    }

    /**
     * Sets the standard flag for a specific data requirement value. The
     * flag passed is set for the specified standard value ID.<br/>
     * <br/>
     *
     * @param prescribedValueId The {@code String} ID of the standard value to be
     *      approved
     * @param standardIndicator {@code true} to flag as standardized. {@code false}
     *      to remove
     *
     * @throws Exception If any error occurs while setting the standardized flag
     *      for the specified value
     */
    public void setStandard(String prescribedValueId, boolean standardIndicator) throws Exception
    {
        this.prescribedValueDataHandler.setStandard(tx, prescribedValueId, standardIndicator);
    }

    /**
     * Sets the approved flag for a specific data requirement value. The
     * flag passed is set for the specified standard value ID.<br/>
     * <br/>
     *
     * @param standardValueID The {@code String} ID of the standard value to be
     *      approved
     * @param approvedIndicator {@code true} to flag as standardized. {@code false}
     *      to remove
     *
     * @throws Exception If any error occurs while setting the standardized flag
     *      for the specified value
     */
    public void setApproved(String standardValueID, boolean approvedIndicator) throws Exception
    {
        this.prescribedValueDataHandler.setApproved(tx, standardValueID, approvedIndicator);
    }
}
