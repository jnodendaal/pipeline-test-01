package com.pilog.t8.data.org.concept;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.definition.data.org.concept.T8ConceptMetaUsages;
import com.pilog.t8.definition.data.org.concept.T8ConceptUsages;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 */
public class T8ServerConceptDataAPI implements T8Api
{
    public static final String API_IDENTIFIER = "@API_SERVER_CONCEPT_DATA";

    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8PerformanceStatistics stats;

    private T8ServerConceptDataHandler conceptDataHandler;

    public T8ServerConceptDataAPI(T8Context context)
    {
        this.serverContext = context.getServerContext();
        this.context = context;

        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.

        createDataHandlers();
    }

    private void createDataHandlers()
    {
        this.conceptDataHandler = new T8ServerConceptDataHandler(serverContext);
    }

    /**
     * Retrieves the concept type for the specified concept ID.
     *
     * @param tx The {@code T8DataTransaction} to be used for accessing
     *      the database
     * @param conceptId The {@code String} concept ID for which the concept type
     *      should be retrieved
     *
     * @return The {@code T8OntologyConceptType} associated with the specified
     *      concept ID
     *
     * @throws Exception If there is any error while retrieving the concept
     *      detail
     * @throws IllegalStateException If there is no concept associated with the
     *      specified concept ID
     */
    private T8OntologyConceptType getConceptType(T8DataTransaction tx, String conceptId) throws Exception
    {
        T8OntologyApi ontApi;
        T8OntologyConcept concept;

        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        concept = ontApi.retrieveConcept(conceptId);

        if (concept == null) throw new IllegalStateException("The Concept ID {"+conceptId+"} does not exist.");

        return concept.getConceptType();
    }

    /**
     * Performs all of the usage count queries associated with the
     * {@code T8OntologyConceptType} of the specified {@code String} concept ID.
     * <br/><br/>
     * The resulting {@code UsageCount} will contain the usage counts for those
     * queries which has been executed, but will also contain {@code 0} for
     * each of the usages which were not applicable to the specific concept
     * type.
     *
     * @param tx The {@code T8DataTransaction} to be used for accessing
     *      the database
     * @param conceptID The {@code String} concept ID for which the usage count
     *      should be retrieved
     *
     * @return The {@code UsageCount} structure containing each of the usage
     *      count values as retrieved
     *
     * @throws Exception If any error occurs while attempting to retrieve the
     *      usage counts for the specified concept ID
     * @throws IllegalStateException If there is no concept associated with the
     *      specified concept ID
     */
    public T8ConceptUsages getConceptUsageCounts(T8DataTransaction tx, String conceptID) throws Exception
    {
        T8OntologyConceptType conceptType;
        T8ConceptUsages usageCounts;

        conceptType = getConceptType(tx, conceptID);
        usageCounts = new T8ConceptUsages(conceptID, conceptType);

        switch (conceptType)
        {
            case CLASS:
                usageCounts.setClassDRUsages(this.conceptDataHandler.getClassDRUsageCount(tx, conceptID));
                usageCounts.setClassRecordUsages(this.conceptDataHandler.getClassRecordUsageCount(tx, conceptID));
                break;
            case CONCEPT_RELATIONSHIP_GRAPH:
            case CONCEPT_TYPE:
                usageCounts.setConceptGraphRelationalUsages(this.conceptDataHandler.getConceptGraphRelationalUsageCount(tx, conceptID));
                break;
            case DATA_REQUIREMENT:
                usageCounts.setDataRequirementDRUsages(this.conceptDataHandler.getDataRequirementDRUsageCount(tx, conceptID));
                usageCounts.setDataRequirementRecordUsages(this.conceptDataHandler.getDataRequirementRecordUsageCount(tx, conceptID));
                break;
            case DATA_REQUIREMENT_INSTANCE:
                usageCounts.setDRInstanceDRUsages(this.conceptDataHandler.getDRInstanceDRUsageCount(tx, conceptID));
                usageCounts.setDRInstanceRecordUsages(this.conceptDataHandler.getDRInstanceRecordUsageCount(tx, conceptID));
                break;
            case ONTOLOGY_CLASS:
                usageCounts.setDataTypeDataStructureUsages(this.conceptDataHandler.getDataTypeDataStructureUsageCount(tx, conceptID));
                break;
            case LANGUAGE:
                usageCounts.setLanguageTerminologyUsages(this.conceptDataHandler.getLanguageTerminologyUsageCount(tx, conceptID));
                usageCounts.setLanguageOntologyTermUsages(this.conceptDataHandler.getLanguageOntologyTermUsageCount(tx, conceptID));
                usageCounts.setLanguageOntologyDefinitionUsages(this.conceptDataHandler.getLanguageOntologyDefinitionUsageCount(tx, conceptID));
                break;
            case ORGANIZATION:
                usageCounts.setOrganizationOrgStructureUsages(this.conceptDataHandler.getOrganizationOrgStructureUsageCount(tx, conceptID));
                break;
            case PROPERTY:
                usageCounts.setPropertyDRUsages(this.conceptDataHandler.getPropertyDRUsageCount(tx, conceptID));
                usageCounts.setPropertyRecordUsages(this.conceptDataHandler.getPropertyRecordUsageCount(tx, conceptID));
                break;
            case QUALIFIER_OF_MEASURE:
                usageCounts.setQOMRecordValueUsages(this.conceptDataHandler.getQOMRecordValueUsageCount(tx, conceptID));
                usageCounts.setQOMSVUsages(this.conceptDataHandler.getQOMSVUsageCount(tx, conceptID));
                break;
            case UNIT_OF_MEASURE:
                usageCounts.setUOMRecordValueUsages(this.conceptDataHandler.getUOMRecordValueUsageCount(tx, conceptID));
                usageCounts.setUOMSVUsages(this.conceptDataHandler.getUOMSVUsageCount(tx, conceptID));
                break;
            case VALUE:
                usageCounts.setValueRecordUsages(this.conceptDataHandler.getValueRecordUsageCount(tx, conceptID));
                usageCounts.setValueSVUsages(this.conceptDataHandler.getValueSVUsageCount(tx, conceptID));
                break;
            case CODE_TYPE:
            case COMMENT_TYPE:
            case CURRENCY:
            case DATA_RECORD:
            case DATA_STRING_FORMAT:
            case DATA_STRING_INSTANCE:
            case DATA_STRING_TYPE:
            case ONTOLOGY_STRUCTURE:
            case ORGANIZATION_TYPE:
            case SECTION:
            case STATE:
            default:
                throw new UnsupportedOperationException("Concept ID {"+conceptID+"} with Concept Type {"+conceptType+"} is not supported for usage checking.");
        }

        return usageCounts;
    }

    public T8ConceptMetaUsages getConceptMetaUsageCounts(T8DataTransaction tx, String conceptId) throws Exception
    {
        T8ConceptMetaUsages metaUsageCounts;
        List<T8DefinitionHandle> definitionHandles;
        T8OntologyConceptType conceptType;

        conceptType = getConceptType(tx, conceptId);
        metaUsageCounts = new T8ConceptMetaUsages(conceptId, conceptType);

        definitionHandles = this.serverContext.getDefinitionManager().findDefinitionsByText(context, conceptId);
        metaUsageCounts.setMetaUsages(definitionHandles);

        return metaUsageCounts;
    }
}