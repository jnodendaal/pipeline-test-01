package com.pilog.t8.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datastring.T8DataStringElement;
import com.pilog.t8.data.document.datastring.T8DataStringFormat;
import com.pilog.t8.data.document.datastring.T8DataStringFormatLink;
import com.pilog.t8.data.document.datastring.T8DataStringInstance;
import com.pilog.t8.data.document.datastring.T8DataStringInstanceLink;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettingsList;
import java.util.Collection;

import static com.pilog.t8.definition.api.T8DataStringApiResource.*;

/**
 * @author Gavin Boshoff
 */
public class T8DataStringApiDataHandler
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;

    public T8DataStringApiDataHandler(T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
    }

    public T8DataStringInstance retrieveDataStringInstance(T8DataTransaction tx, String dsIid) throws Exception
    {
        T8DataEntity dataEntity;

        dataEntity = tx.retrieve(DATA_STRING_INSTANCE_DE_IDENTIFIER, HashMaps.newHashMap(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_DATA_STRING_INSTANCE_ID, dsIid));
        if (dataEntity != null)
        {
            T8DataStringInstance dsInstance;

            dsInstance = new T8DataStringInstance();
            dsInstance.setInstanceID((String)dataEntity.getFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_DATA_STRING_INSTANCE_ID));
            dsInstance.setTypeID((String)dataEntity.getFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_DATA_STRING_TYPE_ID));
            dsInstance.setLanguageID((String)dataEntity.getFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_LANGUAGE_ID));
            dsInstance.setAutoGenerate("Y".equalsIgnoreCase((String)dataEntity.getFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_AUTO_RENDER)));
            return dsInstance;
        }
        else return null;
    }

    public void insertDataStringInstance(T8DataTransaction tx, T8DataStringInstance dsInstance) throws Exception
    {
        T8DataEntity dataEntity;

        dataEntity = tx.create(DATA_STRING_INSTANCE_DE_IDENTIFIER, null);
        dataEntity.setFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_DATA_STRING_INSTANCE_ID, dsInstance.getInstanceID());
        dataEntity.setFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_DATA_STRING_TYPE_ID, dsInstance.getTypeID());
        dataEntity.setFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_LANGUAGE_ID, dsInstance.getLanguageID());
        dataEntity.setFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_AUTO_RENDER, (dsInstance.isAutoGenerate() ? "Y" : "N"));
        tx.insert(dataEntity);
    }

    public void updateDataStringInstance(T8DataTransaction tx, T8DataStringInstance dsInstance) throws Exception
    {
        T8DataEntity dataEntity;

        dataEntity = tx.create(DATA_STRING_INSTANCE_DE_IDENTIFIER, null);
        dataEntity.setFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_DATA_STRING_INSTANCE_ID, dsInstance.getInstanceID());
        dataEntity.setFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_DATA_STRING_TYPE_ID, dsInstance.getTypeID());
        dataEntity.setFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_LANGUAGE_ID, dsInstance.getLanguageID());
        dataEntity.setFieldValue(DATA_STRING_INSTANCE_DE_IDENTIFIER + F_AUTO_RENDER, (dsInstance.isAutoGenerate() ? "Y" : "N"));
        tx.update(dataEntity);
    }

    public boolean isDRInstanceLinked(T8DataTransaction tx, T8SessionContext sessionContext, String dataStringInstanceID, String dataRequirementIID) throws Exception
    {
        T8DataEntity linkEntity;

        linkEntity = tx.retrieve(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, HashMaps.createTypeSafeMap(new String[]{DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER+F_DATA_STRING_INSTANCE_ID, DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER+F_DR_INSTANCE_ID}, new Object[]{dataStringInstanceID, dataRequirementIID}));

        return (linkEntity != null);
    }

    public void insertDataStringInstanceLink(T8DataTransaction tx, String rootOrganizationID, String dataStringInstanceID, String dataRequirementIID) throws Exception
    {
        T8DataEntity instanceLinkEntity;

        instanceLinkEntity = tx.create(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, null);
        instanceLinkEntity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER+F_ROOT_ORG_ID, rootOrganizationID);
        instanceLinkEntity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER+F_DR_INSTANCE_ID, dataRequirementIID);
        instanceLinkEntity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER+F_DATA_STRING_INSTANCE_ID, dataStringInstanceID);
        if (tx.retrieve(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, instanceLinkEntity.getFieldValues()) == null) tx.insert(instanceLinkEntity);
        else tx.update(instanceLinkEntity);
    }

    public void insertDataStringInstanceLink(T8DataTransaction tx, T8DataStringInstanceLink link) throws Exception
    {
        T8DataEntity entity;

        entity = tx.create(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, null);
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_ROOT_ORG_ID, sessionContext.getRootOrganizationIdentifier());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_DR_INSTANCE_ID, link.getDrInstanceId());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_DATA_STRING_INSTANCE_ID, link.getDsInstanceId());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_CONDITION_EXPRESSION, link.getConditionExpression());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_DATA_KEY_EXPRESSION, link.getDataKeyExpression());
        tx.insert(entity);
    }

    public void updateDataStringInstanceLink(T8DataTransaction tx, T8DataStringInstanceLink link) throws Exception
    {
        T8DataEntity entity;

        entity = tx.create(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, null);
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_ROOT_ORG_ID, sessionContext.getRootOrganizationIdentifier());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_DR_INSTANCE_ID, link.getDrInstanceId());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_DATA_STRING_INSTANCE_ID, link.getDsInstanceId());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_CONDITION_EXPRESSION, link.getConditionExpression());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_DATA_KEY_EXPRESSION, link.getDataKeyExpression());
        tx.update(entity);
    }

    public void saveDataStringInstanceLink(T8DataTransaction tx, T8DataStringInstanceLink link) throws Exception
    {
        T8DataEntity entity;

        entity = tx.create(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, null);
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_ROOT_ORG_ID, sessionContext.getRootOrganizationIdentifier());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_DR_INSTANCE_ID, link.getDrInstanceId());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_DATA_STRING_INSTANCE_ID, link.getDsInstanceId());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_CONDITION_EXPRESSION, link.getConditionExpression());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_DATA_KEY_EXPRESSION, link.getDataKeyExpression());
        entity.setFieldValue(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER + F_SEQUENCE, link.getSequence());

        if (!tx.update(entity))
        {
            tx.insert(entity);
        }
    }

    public void deleteDataStringInstanceLink(T8DataTransaction tx, String rootOrganizationID, String dataStringInstanceID, String dataRequirementIID) throws Exception
    {
        Map<String, Object> deletionKey;

        deletionKey = HashMaps.createTypeSafeMap(new String[]{DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER+F_ROOT_ORG_ID, DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER+F_DR_INSTANCE_ID, DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER+F_DATA_STRING_INSTANCE_ID,}, new String[]{rootOrganizationID, dataRequirementIID, dataStringInstanceID});

        tx.delete(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, new T8DataFilter(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, deletionKey));
    }

    public void deleteDataStringInstance(T8DataTransaction tx, String dataStringInstanceID) throws Exception
    {
        // Delete all data string instance links associated with the specified instance
        tx.delete(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, new T8DataFilter(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_INSTANCE_LINK_DE_IDENTIFIER+F_DATA_STRING_INSTANCE_ID, dataStringInstanceID)));

        // Delete the data string instance entry
        tx.delete(DATA_STRING_INSTANCE_DE_IDENTIFIER, new T8DataFilter(DATA_STRING_INSTANCE_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_INSTANCE_DE_IDENTIFIER+F_DATA_STRING_INSTANCE_ID, dataStringInstanceID)));
    }

    public T8DataStringFormatLink retrieveDataStringFormatLink(T8DataTransaction tx, String linkID) throws Exception
    {
        T8DataEntity formatLinkEntity;

        formatLinkEntity = tx.retrieve(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_LINK_ID, linkID));

        return entityToFormatLink(formatLinkEntity);
    }

    public String insertDataStringFormatLink(T8DataTransaction tx, T8DataStringFormatLink formatLink, T8DataStringFormat format) throws Exception
    {
        // Insert the data string format if one is specified
        if (format != null)
        {
            String dataStringFormatID;

            dataStringFormatID = insertDataStringFormat(tx, format);
            formatLink.setDsFormatId(dataStringFormatID);
        }

        // If a link ID is supplied, we use it, otherwise we generate a new one
        if (formatLink.getLinkId() == null) formatLink.setLinkId(T8IdentifierUtilities.createNewGUID());
        // Insert the actual data record into the database
        tx.insert(formatLinkToEntity(tx, formatLink));
        // Return the newly generated ID
        return formatLink.getLinkId();
    }

    public void updateDataStringFormatLink(T8DataTransaction tx, T8DataStringFormatLink formatLink) throws Exception
    {
        // Update the data record in the database
        tx.update(formatLinkToEntity(tx, formatLink));
    }

    public void deleteDataStringFormatLink(T8DataTransaction tx, String linkID) throws Exception
    {
        tx.delete(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER, new T8DataFilter(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_LINK_ID, linkID)));
    }

    public T8DataStringFormat retrieveDataStringFormat(T8DataTransaction tx, String dataStringFormatID) throws Exception
    {
        List<T8DataStringElement> formatElements;
        T8DataStringFormat dataStringFormat;
        T8DataEntity formatEntity;

        formatEntity = tx.retrieve(DATA_STRING_FORMAT_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_FORMAT_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID, dataStringFormatID));
        dataStringFormat = entityToFormat(formatEntity);

        // Retrieve all of the format elements
        formatElements = retrieveFormatElements(tx, dataStringFormatID);

        // Reconstruct the format as the root element and add the retrieved elements as children
        reconstructFormat(dataStringFormat, formatElements);

        return dataStringFormat;
    }

    public String insertDataStringFormat(T8DataTransaction tx, T8DataStringFormat format) throws Exception
    {
        String formatID;

        // If a format ID is supplied, we use it, otherwise we generate a new one
        formatID = format.getDataStringFormatID();
        if (formatID == null)
        {
            formatID = T8IdentifierUtilities.createNewGUID();
        }

        // Insert the new format
        tx.insert(formatToEntity(tx, format, formatID));

        // Now insert the data string elements, starting with the format as the root element
        saveDataStringFormatElements(tx, new HashSet<>(0), ArrayLists.typeSafeList(format), formatID, null);

        return formatID;
    }

    public void updateDataStringFormat(T8DataTransaction tx, T8DataStringFormat format) throws Exception
    {
        Set<String> elementIDs;

        // Update the existing format
        tx.update(formatToEntity(tx, format, format.getDataStringFormatID()));

        // We need a set of the existing element ID's to help us determine whether
        // or not elements need to be inserted or updated
        elementIDs = retrieveFormatElementIDs(tx, format.getDataStringFormatID());

        // Now save the data string elements, starting with the format as the root element
        elementIDs = saveDataStringFormatElements(tx, elementIDs, ArrayLists.typeSafeList(format), format.getDataStringFormatID(), null);

        // Delete any data string elements which aren't part of the saved set
        deleteDataStringFormatElements(tx, retrieveFormatElements(tx, format.getDataStringFormatID()), elementIDs);
    }

    public void deleteDataStringFormat(T8DataTransaction tx, String formatID) throws Exception
    {
        // Delete elements linked to format. We have to delete in hierarchy, regardless of deleting all of them
        deleteDataStringFormatElements(tx, retrieveFormatElements(tx, formatID), new HashSet<>(0));

        // Delete links linked to format
        tx.delete(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER, new T8DataFilter(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID, formatID)));

        // Delete format
        tx.delete(DATA_STRING_FORMAT_DE_IDENTIFIER, new T8DataFilter(DATA_STRING_FORMAT_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID, formatID)));
    }

    public int deleteDataStrings(T8DataTransaction tx, List<String> recordIDList, List<String> dataStringInstanceIDList) throws Exception
    {
        T8DataFilter filter;

        filter = new T8DataFilter(DATA_STRING_DE_IDENTIFIER);
        filter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, DATA_STRING_DE_IDENTIFIER+F_RECORD_ID, recordIDList);
        filter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, DATA_STRING_DE_IDENTIFIER+F_DATA_STRING_INSTANCE_ID, dataStringInstanceIDList);

        return tx.delete(DATA_STRING_DE_IDENTIFIER, filter);
    }

    private Set<String> retrieveFormatElementIDs(T8DataTransaction tx, String dataStringFormatID) throws Exception
    {
        List<T8DataEntity> elementEntities;
        T8DataFilter filter;

        filter = new T8DataFilter(DATA_STRING_ELEMENT_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID, dataStringFormatID));
        elementEntities = tx.select(DATA_STRING_ELEMENT_DE_IDENTIFIER, filter);

        return elementEntities.stream().map((entity)->(String)entity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_ELEMENT_ID)).collect(Collectors.toCollection(HashSet::new));
    }

    private List<T8DataStringElement> retrieveFormatElements(T8DataTransaction tx, String dataStringFormatID) throws Exception
    {
        List<T8DataEntity> elementEntities;
        T8DataFilter filter;

        filter = new T8DataFilter(DATA_STRING_ELEMENT_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID, dataStringFormatID));
        filter.setFieldOrdering(DATA_STRING_ELEMENT_DE_IDENTIFIER + F_SEQUENCE, OrderMethod.ASCENDING);
        elementEntities = tx.select(DATA_STRING_ELEMENT_DE_IDENTIFIER, filter);

        return constructElements(elementEntities, null);
    }

    private void reconstructFormat(T8DataStringFormat format, List<T8DataStringElement> elements)
    {
        T8DataStringElement rootElement;

        if (elements.size() > 1) throw new IllegalStateException("More than 1 root element found for data string format : " + format);
        else if (elements.size() < 1) throw new IllegalStateException("No root element found for data string format : " + format);
        else
        {
            // We now know this is the root element (also the format)
            rootElement = elements.get(0);

            // Populate the format as the root element, from the root element
            format.setRootElement(rootElement);

            // Add the sub elements of the root element to the format
            rootElement.getSubElements().forEach(format::addSubElement);
        }
    }

    public List<T8DataStringParticleSettings> retrieveDataStringParticleSettings(T8DataTransaction tx, String rootOrgID, String recordId) throws Exception
    {
        List<T8DataStringParticleSettings> particleSettingsList;
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        String entityIdentifier;
        T8DataFilter filter;

        // Retrieve the entity.
        entityIdentifier = DATA_STRING_PARTICLE_DE_IDENTIFIER;

        // Create the new filter object.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityIdentifier, filterCriteria);

        // Add the filter criteria.
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_ROOT_ORG_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, rootOrgID));
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_RECORD_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, recordId));

        // Select the entities that match the criteria.
        particleSettingsList = new ArrayList<T8DataStringParticleSettings>();
        entityList = tx.select(entityIdentifier, filter);
        for (T8DataEntity entity : entityList)
        {
            // Add the particle to the retrieved list.
            particleSettingsList.add(constructParticleSettings(entity));
        }

        // Return the results.
        return particleSettingsList;
    }

    public void saveDataStringParticleSettings(T8DataTransaction tx, List<T8DataStringParticleSettings> settingsList) throws Exception
    {
        for (T8DataStringParticleSettings settings : settingsList)
        {
            T8DataEntity settingsEntity;

            settingsEntity = constructParticleSettingsEntity(tx, DATA_STRING_PARTICLE_DE_IDENTIFIER, settings);
            if (!tx.update(settingsEntity))
            {
                tx.insert(settingsEntity);
            }
        }
    }

    public void insertDataStringParticleSettings(T8DataTransaction tx, T8DataStringParticleSettings settings) throws Exception
    {
        T8DataEntity settingsEntity;

        settingsEntity = constructParticleSettingsEntity(tx, DATA_STRING_PARTICLE_DE_IDENTIFIER, settings);
        tx.insert(settingsEntity);
    }

    public boolean updateDataStringParticleSettings(T8DataTransaction tx, T8DataStringParticleSettings settings) throws Exception
    {
        T8DataEntity settingsEntity;

        settingsEntity = constructParticleSettingsEntity(tx, DATA_STRING_PARTICLE_DE_IDENTIFIER, settings);
        return tx.update(settingsEntity);
    }

    public boolean deleteDataStringParticleSettings(T8DataTransaction tx, T8DataStringParticleSettings settings) throws Exception
    {
        T8DataEntity settingsEntity;

        settingsEntity = constructParticleSettingsEntity(tx, DATA_STRING_PARTICLE_DE_IDENTIFIER, settings);
        return tx.delete(settingsEntity);
    }

    public int deleteDataStringParticleSettingsByRecordId(T8DataTransaction tx, String recordId) throws Exception
    {
        return tx.delete(DATA_STRING_PARTICLE_DE_IDENTIFIER, new T8DataFilter(DATA_STRING_PARTICLE_DE_IDENTIFIER, HashMaps.newHashMap(DATA_STRING_PARTICLE_DE_IDENTIFIER + F_RECORD_ID, recordId)));
    }

    /**
     * Retrieves all rendering particles matching the supplied lists of ID's.
     * @param rootOrgID The root organization for which retrieval will be
     * filtered.
     * @param drIds The set of DR ID's to use for filtering.  If null, the
     * DR filter will fetch only particles that are not linked to any DR's.
     * @param drInstanceIds The set of DR Instance ID's to use for filtering.
     * If null, the DR Instance filter will fetch only particles that are not
     * linked to any DR Instances.
     * @param recordIds The set of Record ID's to use for filtering.  If null,
     * the record filter will fetch only particles that are not linked to any
     * records.
     * @return The list of all particles available for the sets of filtering
     * criteria.
     * @throws Exception
     */
    public T8DataStringParticleSettingsList retrieveDataStringParticleSettings(T8DataTransaction tx, String rootOrgID, Collection<String> drIds, Collection<String> drInstanceIds, Collection<String> recordIds) throws Exception
    {
        T8DataStringParticleSettingsList particleSettingsList;
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        String entityIdentifier;
        T8DataFilter filter;

        // Retrieve the entity.
        entityIdentifier = DATA_STRING_PARTICLE_DE_IDENTIFIER;

        // Create the new filter object.
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityIdentifier, filterCriteria);

        // Add the filter criteria.
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_ROOT_ORG_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, rootOrgID));

        // Add the record filter.
        if (CollectionUtilities.hasContent(recordIds))
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_RECORD_ID, T8DataFilterCriterion.DataFilterOperator.IN, recordIds));
        }
        else filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_RECORD_ID, T8DataFilterCriterion.DataFilterOperator.IS_NULL, null));

        // Add the DR Instance filter.
        if (CollectionUtilities.hasContent(drInstanceIds))
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DR_INSTANCE_ID, T8DataFilterCriterion.DataFilterOperator.IN, drInstanceIds));
        }
        else filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DR_INSTANCE_ID, T8DataFilterCriterion.DataFilterOperator.IS_NULL, null));

        // Add DR filter.
        if (CollectionUtilities.hasContent(drIds))
        {
            filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DR_ID, T8DataFilterCriterion.DataFilterOperator.IN, drIds));
        }
        else filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + F_DR_ID, T8DataFilterCriterion.DataFilterOperator.IS_NULL, null));

        // Select the entities that match the criteria.
        particleSettingsList = new T8DataStringParticleSettingsList();
        entityList = tx.select(entityIdentifier, filter);
        for (T8DataEntity entity : entityList)
        {
            // Add the particle to the retrieved list.
            particleSettingsList.addParticleSettings(constructParticleSettings(entity));
        }

        // Return the results.
        return particleSettingsList;
    }

    private T8DataStringParticleSettings constructParticleSettings(T8DataEntity entity)
    {
        T8DataStringParticleSettings particleSettings;

        // Construct a rendering particle from the retrieved data.
        particleSettings = new T8DataStringParticleSettings((String)entity.getFieldValue(F_PARTICLE_ID));

        particleSettings.setDrId((String)entity.getFieldValue(F_DR_ID));
        particleSettings.setDrInstanceId((String)entity.getFieldValue(F_DR_INSTANCE_ID));
        particleSettings.setRecordId((String)entity.getFieldValue(F_RECORD_ID));
        particleSettings.setPropertyId((String)entity.getFieldValue(F_PROPERTY_ID));
        particleSettings.setFieldId((String)entity.getFieldValue(F_FIELD_ID));
        particleSettings.setDataTypeId((String)entity.getFieldValue(F_DATA_TYPE_ID));
        particleSettings.setLanguageId((String)entity.getFieldValue(F_LANGUAGE_ID));
        particleSettings.setDsTypeId((String)entity.getFieldValue(F_DATA_STRING_TYPE_ID));
        particleSettings.setOrgId((String)entity.getFieldValue(F_ORG_ID));
        particleSettings.setPriority(((Number)entity.getFieldValue(F_PRIORITY)).intValue());
        particleSettings.setSequence(((Number)entity.getFieldValue(F_SEQUENCE)).intValue());
        particleSettings.setInclude("Y".equals(entity.getFieldValue(F_INCLUDE)));
        return particleSettings;
    }

    private T8DataEntity constructParticleSettingsEntity(T8DataTransaction tx, String entityIdentifier, T8DataStringParticleSettings particleSettings) throws Exception
    {
        T8DataEntity entity;

        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + F_PARTICLE_ID, particleSettings.getId());
        entity.setFieldValue(entityIdentifier + F_ROOT_ORG_ID, sessionContext.getRootOrganizationIdentifier());
        entity.setFieldValue(entityIdentifier + F_DR_ID, particleSettings.getDrId());
        entity.setFieldValue(entityIdentifier + F_DR_INSTANCE_ID, particleSettings.getDrInstanceId());
        entity.setFieldValue(entityIdentifier + F_RECORD_ID, particleSettings.getRecordId());
        entity.setFieldValue(entityIdentifier + F_PROPERTY_ID, particleSettings.getPropertyId());
        entity.setFieldValue(entityIdentifier + F_FIELD_ID, particleSettings.getFieldId());
        entity.setFieldValue(entityIdentifier + F_DATA_TYPE_ID, particleSettings.getDataTypeId());
        entity.setFieldValue(entityIdentifier + F_LANGUAGE_ID, particleSettings.getLanguageId());
        entity.setFieldValue(entityIdentifier + F_DATA_STRING_TYPE_ID, particleSettings.getDsTypeId());
        entity.setFieldValue(entityIdentifier + F_ORG_ID, particleSettings.getOrgId());
        entity.setFieldValue(entityIdentifier + F_PRIORITY, particleSettings.getPriority());
        entity.setFieldValue(entityIdentifier + F_SEQUENCE, particleSettings.getSequence());
        entity.setFieldValue(entityIdentifier + F_INCLUDE, particleSettings.isInclude() ? "Y" : "N");
        return entity;
    }

    private Set<String> saveDataStringFormatElements(T8DataTransaction tx, Set<String> existingElementIDs, List<T8DataStringElement> elements, String dataStringFormatID, String parentElementID) throws Exception
    {
        // Simplifies the recursion of this method
        if (elements == null || elements.isEmpty()) return new HashSet<>();

        Set<String> elementIDs;
        String elementID;

        elementIDs = new HashSet<>();
        for (T8DataStringElement element : elements)
        {
            elementID = element.getID();

            if (!existingElementIDs.contains(elementID))
            {
                // Generate and set a new element ID if necessary
                if (elementID == null)
                {
                    elementID = T8IdentifierUtilities.createNewGUID();
                    element.setID(elementID);
                }

                tx.insert(elementToEntity(tx, element, dataStringFormatID, parentElementID));
            } else tx.update(elementToEntity(tx, element, dataStringFormatID, parentElementID));

            // Add the element ID to the set of current elements
            elementIDs.add(elementID);

            // Add the ID's from the sub-elements as they are saved
            elementIDs.addAll(saveDataStringFormatElements(tx, existingElementIDs, element.getSubElements(), dataStringFormatID, elementID));
        }

        return elementIDs;
    }

    /**
     * This method takes a bottom-up approach to deleting format elements to
     * ensure that an element, which is a parent element of another is not
     * removed before the parent element is removed.<br/>
     * <br/>
     * The current elements set simply contains a flat-list of all of the
     * element ID's which should still be valid for a specific data string
     * format. Any existing elements linked to a format, which do not form part
     * of this set of ID's will be deleted.
     *
     * @param tx The {@code T8DataTransaction} which provides an active
     *      communication link to the data source
     * @param existingElements The {@code List} of existing
     *      {@code T8DataStringElements} for a specific data string format
     * @param currentElementIDs The {@code Set} of data element ID's which form
     *      part of the new state for a data string format
     *
     * @throws Exception If there is any error while completing the deletion of
     *      all of the elements which no longer form part of the current
     *      elements for a specific format
     */
    private void deleteDataStringFormatElements(T8DataTransaction tx, List<T8DataStringElement> existingElements, Set<String> currentElementIDs) throws Exception
    {
        for (T8DataStringElement existingElement : existingElements)
        {
            if (existingElement.hasSubElements()) deleteDataStringFormatElements(tx, existingElement.getSubElements(), currentElementIDs);

            // Check if the new set of elements no longer contains the element ID
            if (!currentElementIDs.contains(existingElement.getID()))
            {
                tx.delete(DATA_STRING_ELEMENT_DE_IDENTIFIER, new T8DataFilter(DATA_STRING_ELEMENT_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_ELEMENT_ID, existingElement.getID())));
            }
        }
    }

    /**
     * Constructs the list of {@code T8DataStringElement} objects in its
     * organized hierarchy, having each of the elements assigned to its correct
     * parent element.
     *
     * @param entities The {@code T8DataEntity} list representing the format
     *      elements retrieved from the database
     * @param parentElement The {@code T8DataStringElement} representing a parent
     *      element for which its child elements will be assigned
     *
     * @return The {@code T8DataStringElement} list containing the organized
     *      hierarchy of rendering elements
     */
    private List<T8DataStringElement> constructElements(List<T8DataEntity> entities, T8DataStringElement parentElement)
    {
        List<T8DataStringElement> elements;
        List<T8DataEntity> childEntities;
        T8DataStringElement childElement;

        // Find the list of child entities to be used for construction of the child elements of the supplied parent.
        elements = new ArrayList<>();
        childEntities = T8DataUtilities.findDataEntities(entities, DATA_STRING_ELEMENT_DE_IDENTIFIER + F_PARENT_ELEMENT_ID, parentElement != null ? parentElement.getID() : null);
        for (T8DataEntity childEntity : childEntities)
        {
            // Construct the new child element.
            childElement = entityToElement(childEntity);
            elements.add(childElement);

            // Add the child element to its parent.
            if (parentElement != null)
            {
                parentElement.addSubElement(childElement);
            }

            // Construct the descendants of the child element.
            constructElements(entities, childElement);
        }

        // Return the list of constructed child elements.
        return elements;
    }

    private T8DataStringElement entityToElement(T8DataEntity elementEntity)
    {
        T8DataStringElement dataStringElement;

        dataStringElement = new T8DataStringElement();
        dataStringElement.setID((String)elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_ELEMENT_ID));
        dataStringElement.setDependencyElementID((String)elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_DEPENDENCY_ELEMENT_ID));
        dataStringElement.setName((String)elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_NAME));
        dataStringElement.setDescription((String)elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_DESCRIPTION));
        dataStringElement.setPriority((Integer)elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_PRIORITY));
        dataStringElement.setExpression((String) elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_EXPRESSION));
        dataStringElement.setLengthRestriction((int) elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_LENGTH_RESTRICTION));
        dataStringElement.setTruncationType((String) elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_TRUNCATION_TYPE));
        dataStringElement.setPrefix((String) elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_PREFIX));
        dataStringElement.setSuffix((String) elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_SUFFIX));
        dataStringElement.setSeparator((String) elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_SEPARATOR));
        dataStringElement.setActive("Y".equals(elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_ACTIVE)));
        dataStringElement.setGroupId((String) elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_GROUP_ID));
        dataStringElement.setStrictParticleInclusion("Y".equals(elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_STRICT_PARTICLE_INCLUSION)));
        dataStringElement.setConditionExpression((String) elementEntity.getFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_CONDITION_EXPRESSION));

        return dataStringElement;
    }

    private T8DataEntity elementToEntity(T8DataTransaction tx, T8DataStringElement dataStringElement, String dataStringFormatID, String parentElementID) throws Exception
    {
        T8DataEntity elementEntity;

        elementEntity = tx.create(DATA_STRING_ELEMENT_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_ELEMENT_ID, dataStringElement.getID()));
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID, dataStringFormatID);
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_NAME, dataStringElement.getName());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_DESCRIPTION, dataStringElement.getDescription());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_PARENT_ELEMENT_ID, parentElementID);
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_DEPENDENCY_ELEMENT_ID, dataStringElement.getDependencyElementID());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_SEQUENCE, dataStringElement.getIndex());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_PRIORITY, dataStringElement.getPriority());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_EXPRESSION, dataStringElement.getExpression());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_LENGTH_RESTRICTION, dataStringElement.getLengthRestriction());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_TRUNCATION_TYPE, dataStringElement.getTruncationType().toString());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_PREFIX, dataStringElement.getPrefix());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_SUFFIX, dataStringElement.getSuffix());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_SEPARATOR, dataStringElement.getSeparator());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_ACTIVE, (dataStringElement.isActive() ? "Y" : "N"));
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_GROUP_ID, dataStringElement.getGroupId());
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_STRICT_PARTICLE_INCLUSION, (dataStringElement.isStrictParticleInclusion() ? "Y" : "N"));
        elementEntity.setFieldValue(DATA_STRING_ELEMENT_DE_IDENTIFIER+F_CONDITION_EXPRESSION, dataStringElement.getConditionExpression());

        return elementEntity;
    }

    private T8DataStringFormatLink entityToFormatLink(T8DataEntity formatLinkEntity)
    {
        if (formatLinkEntity == null) return null;
        T8DataStringFormatLink formatLink;

        formatLink = new T8DataStringFormatLink();
        formatLink.setLinkId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_LINK_ID));
        formatLink.setRootOrgId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_ROOT_ORG_ID));
        formatLink.setDsFormatId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID));
        formatLink.setType((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_TYPE));
        formatLink.setDrId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DR_ID));
        formatLink.setDrInstanceId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DR_INSTANCE_ID));
        formatLink.setDataKeyId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DATA_KEY_ID));
        formatLink.setRootRecordId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_ROOT_RECORD_ID));
        formatLink.setPropertyId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_PROPERTY_ID));
        formatLink.setFieldId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_FIELD_ID));
        formatLink.setDataTypeId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DATA_TYPE_ID));
        formatLink.setLanguageId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_LANGUAGE_ID));
        formatLink.setDataStringTypeID((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DATA_STRING_TYPE_ID));
        formatLink.setOrgId((String) formatLinkEntity.getFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_ORG_ID));

        return formatLink;
    }

    private T8DataEntity formatLinkToEntity(T8DataTransaction tx, T8DataStringFormatLink formatLink) throws Exception
    {
        T8DataEntity formatLinkEntity;

        formatLinkEntity = tx.create(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_LINK_ID, formatLink.getLinkId()));
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_ROOT_ORG_ID, formatLink.getRootOrgId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID, formatLink.getDsFormatId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_TYPE, formatLink.getType().toString());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DR_ID, formatLink.getDrId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DR_INSTANCE_ID, formatLink.getDrInstanceId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DATA_KEY_ID, formatLink.getDataKeyId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_ROOT_RECORD_ID, formatLink.getRootRecordId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_PROPERTY_ID, formatLink.getPropertyId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_FIELD_ID, formatLink.getFieldId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DATA_TYPE_ID, formatLink.getDataTypeId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_LANGUAGE_ID, formatLink.getLanguageId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_DATA_STRING_TYPE_ID, formatLink.getDsTypeId());
        formatLinkEntity.setFieldValue(DATA_STRING_FORMAT_LINK_DE_IDENTIFIER+F_ORG_ID, formatLink.getOrgId());

        return formatLinkEntity;
    }

    private T8DataStringFormat entityToFormat(T8DataEntity formatEntity)
    {
        if (formatEntity == null) return null;
        T8DataStringFormat format;

        format = new T8DataStringFormat((String)formatEntity.getFieldValue(DATA_STRING_FORMAT_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID));
        format.setType((String)formatEntity.getFieldValue(DATA_STRING_FORMAT_DE_IDENTIFIER+F_TYPE));
        format.setName((String)formatEntity.getFieldValue(DATA_STRING_FORMAT_DE_IDENTIFIER+F_NAME));
        format.setDescription((String)formatEntity.getFieldValue(DATA_STRING_FORMAT_DE_IDENTIFIER+F_DESCRIPTION));

        return format;
    }

    private T8DataEntity formatToEntity(T8DataTransaction tx, T8DataStringFormat format, String formatID) throws Exception
    {
        T8DataEntity formatEntity;

        formatEntity = tx.create(DATA_STRING_FORMAT_DE_IDENTIFIER, HashMaps.createSingular(DATA_STRING_FORMAT_DE_IDENTIFIER+F_DATA_STRING_FORMAT_ID, formatID));
        formatEntity.setFieldValue(DATA_STRING_FORMAT_DE_IDENTIFIER+F_TYPE, format.getType().toString());
        formatEntity.setFieldValue(DATA_STRING_FORMAT_DE_IDENTIFIER+F_NAME, format.getName());
        formatEntity.setFieldValue(DATA_STRING_FORMAT_DE_IDENTIFIER+F_DESCRIPTION, format.getDescription());

        return formatEntity;
    }
}