package com.pilog.t8.data.ontology;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataCache;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.org.T8OrganizationDataHandler;
import com.pilog.t8.definition.data.ontology.T8OntologyStructureCacheDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyStructureCache implements T8DataCache
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8OntologyStructureCache.class);

    private final T8ServerContext serverContext;
    private final T8OntologyStructureCacheDefinition definition;
    private final Map<String, T8OntologyStructure> ontologyStructures;
    private boolean initialized;
    private boolean enabled;

    public T8OntologyStructureCache(T8Context context, T8OntologyStructureCacheDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.definition = definition;
        this.ontologyStructures = new HashMap<>();
        this.initialized = false;
        this.enabled = definition.isEnabled();
    }

    @Override
    public boolean isInitialized()
    {
        return initialized;
    }

    @Override
    public boolean cachesData(String dataIdentifier)
    {
        if (enabled)
        {
            return T8OntologyStructureCacheDefinition.DATA_IDENTIFIER.equals(dataIdentifier);
        }
        else return false;
    }

    @Override
    public Object recacheData(String dataIdentifier, Map<String, Object> dataParameters) throws Exception
    {
        recacheAllOntologyDataStructures();
        return getCachedData(dataIdentifier, dataParameters);
    }

    @Override
    public Object getCachedData(String dataIdentifier, Map<String, Object> dataParameters) throws Exception
    {
        if (dataParameters != null)
        {
            String osId;

            osId = (String)dataParameters.get(T8OntologyStructureCacheDefinition.DATA_PARAMETER_ODS_ID);
            if (osId != null)
            {
                T8OntologyStructure ontologyDataStructure;

                // Check the internal cache and if the Ontology Data Structure is present return it, else cache it.
                ontologyDataStructure = ontologyStructures.get(osId);
                if (ontologyDataStructure != null)
                {
                    return ontologyDataStructure;
                }
                else // Not found, so cache the required structure.
                {
                    return cacheOntologyDataStructure(osId);
                }
            }
            else throw new Exception("Required data parameters not specified.");
        }
        else throw new Exception("Required data parameters not specified.");
    }

    @Override
    public void init() throws Exception
    {
        if (enabled)
        {
            recacheAllOntologyDataStructures();
            initialized = true;
        }
    }

    @Override
    public void clear()
    {
        ontologyStructures.clear();
    }

    @Override
    public void destroy()
    {
        clear();
        initialized = false;
    }

    private T8OntologyStructure cacheOntologyDataStructure(String odsID) throws Exception
    {
        throw new Exception("Caching of individual Ontology Data Structures not yet implemented.  Cannot find requested Ontology Data Stucture in existing cache: " + odsID);
    }

    private void recacheAllOntologyDataStructures() throws Exception
    {
        List<T8DataEntity> orgEntityList;
        Set<String> osIDSet;
        T8DataTransaction tx;
        String entityIdentifier;
        T8OrganizationDataHandler dataHandler;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Get the ontology structures used by the organizations in the system.
        entityIdentifier = ORG_ROOT_DE_IDENTIFIER;
        orgEntityList = tx.select(entityIdentifier, (T8DataFilter)null);
        osIDSet = T8DataUtilities.getEntityFieldValueSet(orgEntityList, entityIdentifier + EF_ODS_ID);

        // Get the entity identifier an retrieve all Ontology Data Structure entities.
        LOGGER.log("Caching Ontology Structures...");
        dataHandler = new T8OrganizationDataHandler(serverContext);
        ontologyStructures.clear();
        ontologyStructures.putAll(dataHandler.retrieveOntologyStructures(tx, osIDSet));

        // Log the result.
        for (String odsID : ontologyStructures.keySet())
        {
            T8OntologyStructure dataStructure;

            dataStructure = ontologyStructures.get(odsID);
            LOGGER.log("Ontology Data Structure cached: " + odsID + " - " + dataStructure.getSize() + " nodes.");
        }
    }
}
