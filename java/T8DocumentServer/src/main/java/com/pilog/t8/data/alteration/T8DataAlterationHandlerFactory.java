package com.pilog.t8.data.alteration;

import com.pilog.t8.data.alteration.handler.T8DataRequirementAlterationHandler;
import com.pilog.t8.data.alteration.handler.T8OntologyAbbreviationAlterationHandler;
import com.pilog.t8.data.alteration.handler.T8OntologyCodeAlterationHandler;
import com.pilog.t8.data.alteration.handler.T8CompleteConceptAlterationHandler;
import com.pilog.t8.data.alteration.handler.T8OntologyConceptAlterationHandler;
import com.pilog.t8.data.alteration.handler.T8OntologyDefinitionAlterationHandler;
import com.pilog.t8.data.alteration.handler.T8CompleteTermAlterationHandler;
import com.pilog.t8.data.alteration.type.T8DataRequirementAlteration;
import com.pilog.t8.security.T8Context;

/**
 * This factory is responsible for the construction of applicable alteration
 * handlers for requested alteration types.  Currently the factory is hard-coded but
 * may be extended in future to employ a more dynamic methodology.
 *
 * Alteration handlers are stateless and can therefore be cached and reused by
 * multiple threads.
 *
 * @author Bouwer du Preez
 */
public class T8DataAlterationHandlerFactory
{
    private final T8DataRequirementAlterationHandler drAlterationHandler;
    private final T8CompleteConceptAlterationHandler completeConceptAlterationHandler;
    private final T8OntologyConceptAlterationHandler conceptAlterationHandler;
    private final T8CompleteTermAlterationHandler completeTermAlterationHandler;
    private final T8OntologyDefinitionAlterationHandler definitionAlterationHandler;
    private final T8OntologyCodeAlterationHandler codeAlterationHandler;
    private final T8OntologyAbbreviationAlterationHandler abbreviationAlterationHandler;

    public T8DataAlterationHandlerFactory(T8Context context)
    {
        this.drAlterationHandler = new T8DataRequirementAlterationHandler(context);
        this.completeConceptAlterationHandler = new T8CompleteConceptAlterationHandler(context);
        this.conceptAlterationHandler = new T8OntologyConceptAlterationHandler();
        this.completeTermAlterationHandler = new T8CompleteTermAlterationHandler(context);
        this.definitionAlterationHandler = new T8OntologyDefinitionAlterationHandler();
        this.codeAlterationHandler = new T8OntologyCodeAlterationHandler();
        this.abbreviationAlterationHandler = new T8OntologyAbbreviationAlterationHandler();
    }

    public T8DataAlterationHandler getAlterationHandler(String alterationId)
    {
        switch (alterationId)
        {
            case T8DataRequirementAlteration.ALTERATION_ID:
                return drAlterationHandler;
            case "@ALTERATION_COMPLETE_CONCEPT":
                return completeConceptAlterationHandler;
            case "@ALTERATION_CONCEPT":
                return conceptAlterationHandler;
            case "@ALTERATION_TERM":
                return completeTermAlterationHandler;
            case "@ALTERATION_DEFINITION":
                return definitionAlterationHandler;
            case "@ALTERATION_CODE":
                return codeAlterationHandler;
            case "@ALTERATION_ABBREVIATION":
                return abbreviationAlterationHandler;
            default:
                throw new RuntimeException("Unsupported alteration: " + alterationId);
        }
    }
}
