package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import com.pilog.t8.data.document.datafile.T8DataFileOutputStream;
import com.pilog.t8.data.object.T8DataRecordObjectHandler;
import com.pilog.t8.definition.data.object.T8DataRecordObjectDefinition;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordObjectOutputStream implements T8DataFileOutputStream
{
    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8DefinitionManager definitionManager;
    private final List<T8DataRecordObjectHandler> objectHandlers;
    private boolean enabled;

    public T8DataRecordObjectOutputStream(T8DataTransaction tx, List<String> filterObjectIds)
    {
        this.context = tx.getContext();
        this.tx = tx;
        this.objectHandlers = new ArrayList<>();
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.definitionManager = serverContext.getDefinitionManager();
        this.enabled = true;

        // Construct the object handlers.
        try
        {
            List<String> objectIds;
            String projectId;

            projectId = tx.getContext().getProjectId();
            objectIds = definitionManager.getDefinitionIdentifiers(projectId, T8DataRecordObjectDefinition.TYPE_IDENTIFIER);
            for (String objectId : objectIds)
            {
                if (filterObjectIds == null || filterObjectIds.isEmpty() || filterObjectIds.contains(objectId))
                {
                    T8DataRecordObjectDefinition definition;
                    T8DataRecordObjectHandler objectHandler;

                    definition = definitionManager.getInitializedDefinition(context, projectId, objectId, null);
                    objectHandler = (T8DataRecordObjectHandler)definition.getDataObjectHandler(tx);
                    objectHandlers.add(objectHandler);
                }
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing data file views.", e);
        }
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    @Override
    public void write(DataRecord dataFile, T8DataFileAlteration alteration, Collection<RecordMetaDataType> metaDataTypes) throws Exception
    {
        if (enabled)
        {
            for (T8DataRecordObjectHandler objectHandler : objectHandlers)
            {
                // Update objects for each of the records contained in the file.
                for (DataRecord dataRecord : dataFile.getDataFileRecords())
                {
                    objectHandler.recordUpdated(dataRecord, alteration);
                }
            }
        }
    }
}
