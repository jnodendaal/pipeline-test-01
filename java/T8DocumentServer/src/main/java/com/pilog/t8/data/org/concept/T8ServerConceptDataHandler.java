package com.pilog.t8.data.org.concept;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.data.T8DataTransaction;
import java.util.Map;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Gavin Boshoff
 */
class T8ServerConceptDataHandler
{
    private final T8ServerContext serverContext;

    T8ServerConceptDataHandler(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
    }

    private int executeConceptUsageCount(T8DataTransaction tx, String entityIdentifier, Map<String, Object> columnValueFilter) throws Exception
    {
        T8DataFilter filter;

        filter = new T8DataFilter(entityIdentifier, new T8DataFilterCriteria(columnValueFilter));

        return tx.count(entityIdentifier, filter);
    }

    int getPropertyDRUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_REQUIREMENT_PROPERTY_DE_IDENTIFIER, HashMaps.createSingular(EF_PROPERTY_ID, conceptID));
    }

    int getPropertyRecordUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_RECORD_PROPERTY_DE_IDENTIFIER, HashMaps.createSingular(EF_PROPERTY_ID, conceptID));
    }

    int getClassDRUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_REQUIREMENT_DE_IDENTIFIER, HashMaps.createSingular(EF_CLASS_ID, conceptID));
    }

    int getClassRecordUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_RECORD_DOCUMENT_CLASS_DE_IDENTIFIER, HashMaps.createSingular(EF_CLASS_ID, conceptID));
    }

    int getFieldDRUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_REQUIREMENT_DATA_TYPE_DE_IDENTIFIER, HashMaps.createTypeSafeMap(new String[]{EF_DATA_TYPE_ID, EF_VALUE}, new String[]{"FIELD_TYPE", conceptID}));
    }

    int getFieldRecordUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_RECORD_PROPERTY_DATA_TYPE_DE_IDENTIFIER, HashMaps.createTypeSafeMap(new String[]{EF_DATA_TYPE_ID, EF_VALUE}, new String[]{"FIELD_TYPE", conceptID}));
    }

    int getDataRequirementDRUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_REQUIREMENT_CHILDREN_DE_IDENTIFIER, HashMaps.createSingular(EF_DATA_REQUIREMENT_CHILD_ID, conceptID));
    }

    int getDataRequirementRecordUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_RECORD_DOCUMENT_DE_IDENTIFIER, HashMaps.createSingular(EF_DR_ID, conceptID));
    }

    int getDRInstanceDRUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_REQUIREMENT_INSTANCE_DE_IDENTIFIER, HashMaps.createSingular(EF_DR_INSTANCE_ID, conceptID));
    }

    int getDRInstanceRecordUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_RECORD_DOCUMENT_DE_IDENTIFIER, HashMaps.createSingular(EF_DR_INSTANCE_ID, conceptID));
    }

    int getValueRecordUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_RECORD_VALUE_DE_IDENTIFIER, HashMaps.createSingular(EF_VALUE, conceptID));
    }

    int getValueSVUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_REQUIREMENT_VALUE_DE_IDENTIFIER, HashMaps.createSingular(EF_CONCEPT_ID, conceptID));
    }

    int getUOMRecordValueUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_RECORD_VALUE_DE_IDENTIFIER, HashMaps.createSingular(EF_VALUE, conceptID));
    }

    int getUOMSVUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_REQUIREMENT_VALUE_DE_IDENTIFIER, HashMaps.createSingular(EF_CONCEPT_ID, conceptID));
    }

    int getQOMRecordValueUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_RECORD_VALUE_DE_IDENTIFIER, HashMaps.createSingular(EF_VALUE, conceptID));
    }

    int getQOMSVUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, DATA_REQUIREMENT_VALUE_DE_IDENTIFIER, HashMaps.createSingular(EF_CONCEPT_ID, conceptID));
    }

    int getLanguageTerminologyUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, ORG_TERMINOLOGY_DE_IDENTIFIER, HashMaps.createSingular(EF_LANGUAGE_ID, conceptID));
    }

    int getLanguageOntologyTermUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, ONTOLOGY_TERM_DE_IDENTIFIER, HashMaps.createSingular(EF_LANGUAGE_ID, conceptID));
    }

    int getLanguageOntologyDefinitionUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, ONTOLOGY_DEFINITION_DE_IDENTIFIER, HashMaps.createSingular(EF_LANGUAGE_ID, conceptID));
    }

    /**
     *
     * @throws IllegalStateException If the usage count is anything other than
     *      0 or 1, the system is in an inconsistent state
     */
    int getOrganizationOrgStructureUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        int usageCount;

        usageCount = executeConceptUsageCount(tx, ORG_STRUCTURE_DE_IDENTIFIER, HashMaps.createSingular(EF_ORG_ID, conceptID));
        if (usageCount != 0 && usageCount != 1) throw new IllegalStateException("Inconsistent system state. Organization concept {"+conceptID+"} has multiple references.");

        return usageCount;
    }

    int getDataTypeDataStructureUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        return executeConceptUsageCount(tx, ONTOLOGY_CLASS_DE_IDENTIFIER, HashMaps.createSingular(EF_ODT_ID, conceptID));
    }

    int getConceptGraphRelationalUsageCount(T8DataTransaction tx, String conceptID) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        String entityIdentifier;
        T8DataFilter filter;

        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityIdentifier, filterCriteria);

        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.OR, entityIdentifier+EF_TAIL_CONCEPT_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, conceptID);
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.OR, entityIdentifier+EF_HEAD_CONCEPT_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, conceptID);

        return tx.count(entityIdentifier, filter);
    }
}