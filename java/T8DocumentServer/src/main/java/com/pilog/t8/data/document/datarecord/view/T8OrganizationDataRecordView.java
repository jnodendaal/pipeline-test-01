package com.pilog.t8.data.document.datarecord.view;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.definition.data.document.datarecord.view.T8DataRecordViewDefinition;
import com.pilog.t8.definition.data.document.datarecord.view.T8DataRecordViewConstructorDefinition;
import com.pilog.t8.definition.data.document.datarecord.view.T8DataRecordViewDefinition.ViewRefreshTrigger;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.data.T8DataTransaction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.definition.data.document.datarecord.view.T8DataRecordObjectResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationDataRecordView implements T8DataRecordView
{
    private final T8DataTransaction tx;
    private final List<T8DataRecordViewConstructor> constructors;
    private final List<String> dataFileDrIdentifiers;
    private final List<String> dataFileDrInstanceIdentifiers;
    private final List<String> drIdentifiers;
    private final List<String> drInstanceIdentifiers;
    private final List<String> languageIds;
    private final Map<String, String> fieldMapping;
    private final ViewRefreshTrigger refreshTrigger;
    private final String identifier;
    private final String dataEntityIdentifier;
    private final String orgId;

    public T8OrganizationDataRecordView(T8DataTransaction tx, T8DataRecordViewDefinition definition)
    {
        this.tx = tx;
        this.orgId = tx.getContext().getSessionContext().getRootOrganizationIdentifier();
        this.identifier = definition.getIdentifier();
        this.refreshTrigger = definition.getViewRefreshTrigger();
        this.dataFileDrIdentifiers = definition.getDataFileDrIdentifiers();
        this.dataFileDrInstanceIdentifiers = definition.getDataFileDrInstanceIdentifiers();
        this.drIdentifiers = definition.getDrIdentifiers();
        this.drInstanceIdentifiers = definition.getDrInstanceIdentifiers();
        this.languageIds = definition.getLanguageIdentifiers();
        this.fieldMapping = definition.getConstructorFieldMapping();
        this.dataEntityIdentifier = definition.getDataEntityIdentifier();

        // Construct all of the data extractors used by this view.
        this.constructors = new ArrayList<>();
        for (T8DataRecordViewConstructorDefinition dataExtractorDefinition : definition.getConstructorDefinitions())
        {
            constructors.add(dataExtractorDefinition.createNewViewConstructor(tx));
        }
    }

    @Override
    public void updateView(DataRecord dataRecord, T8DataFileAlteration alteration)
    {
        // Make sure the data record is applicable to this view.
        if (checkRefresh(dataRecord, alteration))
        {
            // For each language, we have to extract data from the file and update the view with it.
            for (String languageId : languageIds)
            {
                Map<String, Object> viewValues;

                // Compile the view values to be updated.
                viewValues = new HashMap<>();
                for (T8DataRecordViewConstructor constructor : constructors)
                {
                    Map<String, Object> viewData;

                    viewData = constructor.constructViewData(dataRecord, alteration, languageId);
                    if (viewData != null) viewValues.putAll(viewData);
                }

                // Map the extractor values to entity fields and add the default field values.
                viewValues = T8IdentifierUtilities.mapParameters(viewValues, fieldMapping);
                viewValues.put(F_RECORD_ID, dataRecord.getID());
                viewValues.put(F_ROOT_RECORD_ID, dataRecord.getDataFileID());
                viewValues.put(F_DR_ID, dataRecord.getDataRequirementID());
                viewValues.put(F_DR_INSTANCE_ID, dataRecord.getDataRequirementInstanceID());
                viewValues.put(F_LANGUAGE_ID, languageId);
                viewValues.put(F_ORG_ID, orgId);
                viewValues.put(F_OBJECT_ID, identifier);

                // Update the view with the latest values.
                try
                {
                    updateView(tx, viewValues);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while updating data file view: " + identifier + " using values: " + viewValues, e);
                }
            }
        }
    }

    private boolean checkRefresh(DataRecord dataRecord, T8DataFileAlteration alteration)
    {
        Set<String> changedRecordIdSet;
        DataRecord dataFile;

        // Firstly; make sure that the input record is included in one of the filter lists.
        if (!drIdentifiers.contains(dataRecord.getDataRequirementID()))
        {
            if (!drInstanceIdentifiers.contains(dataRecord.getDataRequirementInstanceID()))
            {
                return false;
            }
        }

        // Secondly; if any DataFile filters are specified, make sure that the input record's data file is included in one of the filter lists.
        dataFile = dataRecord.getDataFile();
        if (!dataFileDrIdentifiers.isEmpty() && !dataFileDrIdentifiers.contains(dataFile.getDataRequirementID()))
        {
            return false;
        }
        else if (!dataFileDrInstanceIdentifiers.isEmpty() && !dataFileDrInstanceIdentifiers.contains(dataFile.getDataRequirementInstanceID()))
        {
            return false;
        }

        // Now check the refresh trigger against the altered record list to find out if we need to refresh this view.
        changedRecordIdSet = alteration.getOntologyUpdatedRecordIdSet();
        changedRecordIdSet.addAll(alteration.getInsertedAndUpdatedRecordIdList());
        switch (refreshTrigger)
        {
            case FILE:
                // Return true if any part of the file has been changed.
                return !changedRecordIdSet.isEmpty();
            case CONTENT:
                // Return true if this record has been altered.
                return changedRecordIdSet.contains(dataRecord.getID());
            case CONTENT_OR_DESCENDANT:
                // Return true if this record or any of its descendants have been altered.
                return changedRecordIdSet.contains(dataRecord.getID()) || CollectionUtilities.containsAny(changedRecordIdSet, dataRecord.getDescendantRecordIDList());
            case LINEAGE:
                // Return true if this record or any of its descendants have been altered.
                return CollectionUtilities.containsAny(changedRecordIdSet, dataRecord.getDescendantRecordIDList()) || CollectionUtilities.containsAny(changedRecordIdSet, dataRecord.getPathRecordIDList());
            default:
                throw new RuntimeException("Invalid refresh trigger: " + refreshTrigger);
        }
    }

    private void updateView(T8DataTransaction tx, Map<String, Object> viewValues) throws Exception
    {
        T8DataEntity viewEntity;

        // Create the entity.
        viewEntity = tx.create(this.dataEntityIdentifier, T8IdentifierUtilities.prependNamespace(this.dataEntityIdentifier, viewValues, false));

        // Update it if it exists, else insert.
        if (!tx.update(viewEntity))
        {
            tx.insert(viewEntity);
        }
    }
}
