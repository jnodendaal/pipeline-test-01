package com.pilog.t8.data.org;

import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8BaseDataHandler
{
    private final T8OntologyApi ontApi;
    private final T8OrganizationOntologyFactory factory;

    public T8BaseDataHandler(T8OntologyApi orgAPI)
    {
        this.ontApi = orgAPI;
        this.factory = orgAPI.getOntologyFactory();
    }

    public void createBaseConceptTypeConcepts(T8DataTransaction tx) throws Exception
    {
        T8OntologyConcept concept;

        // All of the concepts in this method are created under the primary CONCEPT_TYPE group.
        factory.setConceptODTID(T8PrimaryOntologyDataType.CONCEPT_TYPE.getDataTypeID());

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "2A00A96C173342ABA2EBD2A697138CC5");
        concept.setStandard(true);
        factory.addTerm(concept, "C57259F283ED4B7998B1A9B07230F018", "Class");
        factory.addDefinition(concept, "884CF47B9A3E4ACD814ED48D2C43CBC5", "Concepts that define classes used in Data Requirement documents");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "DE723BACCAF4488A9B10D42CCF3B13E3");
        concept.setStandard(true);
        factory.addTerm(concept, "3EE0633063B44881B04A903073A27E1C", "Code Type");
        factory.addDefinition(concept, "9FF2C74F064148E9938B33F5EF4B12CD", "Concepts that define the type of a code");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "51D5007DAFBF4530B6CAC7013E9FFDCE");
        concept.setStandard(true);
        factory.addTerm(concept, "DD6CBB2120D348E0B8798943F0308E5F", "Comment Type");
        factory.addDefinition(concept, "306FB21ED63F40EFB75D80F164A5B6A9", "Concepts that define the type of an ontology comment");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "92016888549E4D059C9197FE08025F05");
        concept.setStandard(true);
        factory.addTerm(concept, "C001CBA0A0FD4023A447616CF97A7828", "Concept Relationship Graph");
        factory.addDefinition(concept, "3923081380EE4DF8A69590F997DB080F", "Concepts that define relationships types between other concepts");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "0F26B52CE763433D8B793DFA120E59AE");
        concept.setStandard(true);
        factory.addTerm(concept, "0797A9C158E64213832927D538F114AD", "Concept Type");
        factory.addDefinition(concept, "83967C3DD98F4CF5A5BB125066E590D0", "Concepts that define the type of other concepts");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "6362265F89E347B59A7AFFF206BDFDD4");
        concept.setStandard(true);
        factory.addTerm(concept, "5922CF6E017F4AF28601B42577238D2E", "Currency");
        factory.addDefinition(concept, "DB1673B58F4541129EFD50ABD660D4B0", "Concepts that define currencies used in master data");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "35FBF4EC3F224825AD2CA873FF91E6CE");
        concept.setStandard(true);
        factory.addTerm(concept, "8ADB9C20B0184E6EB74FDB735C38C75E", "Data Record");
        factory.addDefinition(concept, "F527CF143E694D0BB6579B005F85AC9E", "Concepts that define Data Record documents");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "E9903DE2AF7242CD98222F323BBE168E");
        concept.setStandard(true);
        factory.addTerm(concept, "87DD605D7F394D96B8C11F8CD36D46B4", "Data Requirement");
        factory.addDefinition(concept, "59443B8D05CD42D08F2482A1F7E43A50", "Concepts that define Data Requirement documents");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "97636A82A1F34654AF21B54A908CA1B4");
        concept.setStandard(true);
        factory.addTerm(concept, "EDB1303986574670831F4B157A276BC3", "Data Requirement Instance");
        factory.addDefinition(concept, "909C1F1313AE45D99C05C8CA90686706", "Concepts that define Data Requirement Instance documents");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "1E2DA27EA5C6463880898E8BD05E25FC");
        concept.setStandard(true);
        factory.addTerm(concept, "C7402C031C9E4E4187875700A494CDAC", "Data Type");
        factory.addDefinition(concept, "0BA9C842BB9040578EA8A8AE3DAE453F", "Concepts that define groups used in ontology data structures");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "756DDA63A2204966BC628D246AB8D82C");
        concept.setStandard(true);
        factory.addTerm(concept, "ACA20090D15F405284F87092C52DA33E", "Rendering Classification");
        factory.addDefinition(concept, "4624B39AA6B94FE4B7C19F92374BEDC6", "Concepts that define the classification of rendered strings");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "BA34CAB469C942EEB731EAB79F3BD1AC");
        concept.setStandard(true);
        factory.addTerm(concept, "4CC3F60FE5224ABC9CB8CF9140982F5B", "Rendering Format");
        factory.addDefinition(concept, "D90840DC9F4B40E79D83D7BB95AB8B7B", "Concepts that define structural format of rendered stings");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "FE4C837B5A454525930639E67852FBFD");
        concept.setStandard(true);
        factory.addTerm(concept, "904A54EFD408498CAF82FDC527FCEBBD", "Rendering Instance");
        factory.addDefinition(concept, "84DBAB3F63904E418D9DC5BF06590A74", "Concepts that define the instances of rendered strings");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "2B642CB206F240DF91BA7E8AC291B369");
        concept.setStandard(true);
        factory.addTerm(concept, "E59D4D1D5CD94CF78E4AC5E80A0ED900", "Rendering Type");
        factory.addDefinition(concept, "58BB62014A684202A3282C2BF1BE4B69", "Concepts that define the types of rendered strings");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "60989377F62D407DB635B3D89ACE85C8");
        concept.setStandard(true);
        factory.addTerm(concept, "015A2A9DD467478886FDDB27F1983E25", "Language");
        factory.addDefinition(concept, "343C3B33E30E4561865AC70E7AF2B99A", "Concepts that define languages linked to concept terminology");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "B6C0D0E62DB0430D84031B2E717F9BA4");
        concept.setStandard(true);
        factory.addTerm(concept, "B7A213E21D484494BE79F4FCE29249DD", "Ontology Data Structure");
        factory.addDefinition(concept, "24EA0CE54C454FF6A0E09965C04A774F", "Concepts that define data structures use for the classification of ontology data");
        ontApi.saveConcept(concept, true, true, true, true, true, true);
        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "E88CF2A37EBC4C5BACB4E81788AD9D72");
        concept.setStandard(true);
        factory.addTerm(concept, "9AF5CBB747B9441E80D53F71260B3721", "Organization");
        factory.addDefinition(concept, "A05E195DDD0943FFA5E2076135D24A68", "Concepts that define organizations to which ontology data is linked");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "FD78434E40A841139E272140037C4560");
        concept.setStandard(true);
        factory.addTerm(concept, "1919B8E8053549E1AD43BC7B8DE0E0DC", "Organization Type");
        factory.addDefinition(concept, "B86D56CA0B4744F3B97A379AF281D65F", "Concepts that define the types of organizations to which ontology data is linked");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "3A05D5B39AE34F2C97B76D72965D7A1E");
        concept.setStandard(true);
        factory.addTerm(concept, "6F76CDD84AF24480A5DC56564CC5D544", "Property");
        factory.addDefinition(concept, "046082485A9C4656A89AB52024B8E7E3", "Concepts that define properties used in Data Requirement documents");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "32A0DB944EBA4857BD1C63BAB0F59E4E");
        concept.setStandard(true);
        factory.addTerm(concept, "8F80EA6F706A466C834042397F37C21E", "Qualifier of Measure");
        factory.addDefinition(concept, "E92840CD5E734016BF91862CA6C0309B", "Concepts that define qualifiers of measure used in Data Requirement documents");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "17C8D90DD5924E3AB31AB17AC61F91DF");
        concept.setStandard(true);
        factory.addTerm(concept, "86FC80C606D944C294068DBC84BE8D3A", "Section");
        factory.addDefinition(concept, "EEB0F3938D694A6DBDC74F5C0882D9F4", "Concepts that define sections used in Data Requirement documents");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "E3FFDEB70F81414F92E23F966C1828E4");
        concept.setStandard(true);
        factory.addTerm(concept, "2228756FB8AA49C8906804410121FB3B", "Unit of Measure");
        factory.addDefinition(concept, "ECAAC99D2E944958A6675BA5974AEDAE", "Concepts that define units of measure used in Data Requirement documents");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "A197D00D2FB44ADDAFFA2AB180327F5B");
        concept.setStandard(true);
        factory.addTerm(concept, "4BC97D72A6DF4F40A8CAA23467EC36A1", "Value");
        factory.addDefinition(concept, "D5285FF8C9F646A6A412B9A22A5EFCB8", "Concepts that define controlled values used in Data Requirement documents");
        ontApi.saveConcept(concept, true, true, true, true, true, true);
    }

    public void createBaseDataTypeConcepts(T8DataTransaction tx) throws Exception
    {
        T8OntologyConcept concept;

        // All of the concepts in this method are created under the primary ONTOLOGY_CLASS group.
        factory.setConceptODTID(T8PrimaryOntologyDataType.ONTOLOGY_DATA_TYPES.getDataTypeID());

        // Of course, we cannot insert any concepts without the language ID, so populate the default just to make sure
        {
            concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "4972E8B5AC3546E7B54D898014B7C3C0");
            concept.setStandard(true);
            ontApi.saveConcept(concept, true, false, false, false, false, false);

            factory.setConceptODTID(T8PrimaryOntologyDataType.CONCEPT_TYPE.getDataTypeID());
            concept = factory.constructConcept(T8OntologyConceptType.CONCEPT_TYPE, "60989377F62D407DB635B3D89ACE85C8");
            concept.setStandard(true);
            ontApi.saveConcept(concept, true, false, false, false, false, false);

            factory.setConceptODTID(T8PrimaryOntologyDataType.LANGUAGES.getDataTypeID());
            concept = factory.constructConcept(T8OntologyConceptType.LANGUAGE, "CC0092ADF415443DB36744B5D9EF96E9");
            concept.setStandard(true);
            factory.addTerm(concept, "C8778A48B32E41638CA1C9D8C0E0848B", "English (en-US)");
            factory.addDefinition(concept, "0E69135D909F4F789A1309A238C5520A", "English (en-US)");
            ontApi.saveConcept(concept, true, true, true, true, true, true);
        }

        factory.setConceptODTID(T8PrimaryOntologyDataType.ONTOLOGY_DATA_TYPES.getDataTypeID());

        // Make sure to create the data type group first, since this is the group to which all others are linked.
        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "02164AEE4E364775AF428195D33164CB");
        concept.setStandard(true);
        factory.addTerm(concept, "3FCA036B21764C528832F1816E93D2CA", "Organization Types");
        factory.addDefinition(concept, "C0D4D6CD4925403395DC5A8BB92FD8B6", "Primary data group that contains all organization types");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "435E6326A83A478094FF60395FCD434C");
        concept.setStandard(true);
        factory.addTerm(concept, "F944B1A648F74A43BF3A3B95482ACA34", "Abbreviations");
        factory.addDefinition(concept, "039ED05D78794ECEA7D03C6C73FAF054", "Primary data group that contains all abbreviations");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "494EBE95276340B599D4B09D1D9B4391");
        concept.setStandard(true);
        factory.addTerm(concept, "F903BEDD3CA541919E8423035F8DC2C6", "Base Data");
        factory.addDefinition(concept, "87FF7027A77D43A6801E0A1AF67B47E4", "Contains all the base data values");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "222131C6D9204E55BC0FBDFC6B9E660F");
        concept.setStandard(true);
        factory.addTerm(concept, "4382C6398E8C4119A31AEF371F59F82D", "Classes");
        factory.addDefinition(concept, "19437B31BC524F9D99835E6F1155838A", "Primary data group that contains all classes");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "76C8864E692B46C8AFC5B4A5E2A57AE0");
        concept.setStandard(true);
        factory.addTerm(concept, "F6AC6B01216E4A56878CF0A2B6057FBA", "Code Types");
        factory.addDefinition(concept, "79CCF340AF554C08B6639A3D46401707", "Primary data group that contains all code types");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

	concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "FA3F7BFBEC6D4C2B8E72BFB288D37168");
        concept.setStandard(true);
        factory.addTerm(concept, "FC561610536F46F89176F36B1686C0EC", "Codes");
        factory.addDefinition(concept, "7F5D58B850314B0C8BC56AD158F68879", "Primary data group that contains all codes");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "3DFA79973874433E83EFBD4670EBFF42");
        concept.setStandard(true);
        factory.addTerm(concept, "0DA7EFC394FB4F7FAE1833A965B5BD2C", "Comment Types");
        factory.addDefinition(concept, "0C4B2886FC71494DAE63DE25B81088BD", "Primary data group that contains all comment types");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "7B2973E5D29349A4A576A74213C1F65D");
        concept.setStandard(true);
        factory.addTerm(concept, "ABBC1ECF40C44A63944D99A5D7324D1F", "Concept Relationships");
        factory.addDefinition(concept, "254EA5C8050747188B274065278A1E52", "Primary data group that contains all concept relationships");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "6C4CC77A1F8D4B3DA669B5718C6C152A");
        concept.setStandard(true);
        factory.addTerm(concept, "EF75086EF9F645F2BA87CC3C9A4D979F", "Concept Types");
        factory.addDefinition(concept, "928C99B4B7E94BBD9D98F41FB5F83313", "Primary data group that contains all concept types");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "46A1D25715F74B9D8F0048E3B080B464");
        concept.setStandard(true);
        factory.addTerm(concept, "56980C909C194141932A55B9015BF2EC", "Currencies");
        factory.addDefinition(concept, "DECF42FE781044F890EC91C8194B31A4", "Primary data group that contains all currencies");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

	concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "0BFCA2C6A82B4E6A99D51B3FC6FE7711");
        concept.setStandard(true);
        factory.addTerm(concept, "2AA5FBAD5B8B4ECAB3883E3F2EEFC1C7", "Data Record Match Plans");
        factory.addDefinition(concept, "79D320A10F864E3CAAFAD347275570A8", "Primary data group that contains all data record match plans");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "7E13F778C7D149ABA22D134A290F1754");
        concept.setStandard(true);
        factory.addTerm(concept, "67F16CF2C59745B9A3B9706D26BC4760", "Data Records");
        factory.addDefinition(concept, "3D7D99408FC34CA098833673F865CD61", "Primary data group that contains all data records");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "0CA3364A02CC47388EED4F71908B0213");
        concept.setStandard(true);
        factory.addTerm(concept, "6EBC6E283E1A46FD9AB28587A4851E3B", "Data Requirement Elements");
        factory.addDefinition(concept, "877A8A61552D45B9B9D7B602054408F6", "Data group that contains all of the constituent elements that make up a Data Requirement document's structure.");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "54EA4FDC1C9C4267A708EE3F4A90FE94");
        concept.setStandard(true);
        factory.addTerm(concept, "38F49A70FDCA42DCBC028871758E29F6", "Data Requirement Instances");
        factory.addDefinition(concept, "2FDE1493C8E84897A7B58DE0898EA6D8", "Primary data group that contains all Data Requirement Instances");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "9FCF20DD2C42457BBF6F269024D2093F");
        concept.setStandard(true);
        factory.addTerm(concept, "B52D7B9266784E3782FB9CF8227BA3AD", "Data Requirements");
        factory.addDefinition(concept, "BA50DD27E8ED4228AD31019EFDCE6995", "Primary data group that contains all Data Requirements");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "267A194C0E0B4E05B4C83A39B808A36F");
        concept.setStandard(true);
        factory.addTerm(concept, "A93A0EFDAF9B4C659C511154A6B1F12F", "Definitions");
        factory.addDefinition(concept, "539D8D13F3E44832BC23793458B9EB45", "Primary data group that contains all definitions");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "082D8F43D336470EBDEFBDEDCA7D9093");
        concept.setStandard(true);
        factory.addTerm(concept, "B726666D2E804A968EA35B8597144736", "Rendering Classifications");
        factory.addDefinition(concept, "095D38F0893E45CAB8A8E9CC52C6DD85", "Primary data group that contains all rendering classifications");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "DA70E6220068497E84DE98A60D858CB7");
        concept.setStandard(true);
        factory.addTerm(concept, "60B563B51D3843718F08ED3A3DAE1AA2", "Rendering Formats");
        factory.addDefinition(concept, "7C6CBA5F897F4959BC5FDDF33DB6E3FD", "Primary data group that contains all rendering formats");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "2A78617D48C049BB99AD55BD2478C540");
        concept.setStandard(true);
        factory.addTerm(concept, "84793A2EB01D415BB671C0555A1AAB41", "Rendering Instances");
        factory.addDefinition(concept, "B87A58D372E447F8B8E1FE1507B71768", "Primary data group that contains all rendering instances");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "6CF50D78B1C947309ED9152905B177F3");
        concept.setStandard(true);
        factory.addTerm(concept, "3021E9BF347C4A9E99F060DBD2BD42E9", "Rendering Types");
        factory.addDefinition(concept, "93E3C50F52E4411385B78C31352C8025", "Primary data group that contains all rendering types");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "4972E8B5AC3546E7B54D898014B7C3C0");
        concept.setStandard(true);
        factory.addTerm(concept, "FFAF9C3935D84F09A9EDB95D87503FCD", "Languages");
        factory.addDefinition(concept, "388A43B8F3F2432E992F68FF014BD55D", "Primary data group that contains all languages");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "68A50FD2546C4F11AECD93F9F55D62A8");
        concept.setStandard(true);
        factory.addTerm(concept, "6C4DC694362F461EAC1FCD992ACBD800", "Ontology");
        factory.addDefinition(concept, "8D4EC424F403436F9B5E0B2F9240BCDD", "Root data group for base ontology data structure");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "25BD559DAF1B417FB11D4F6147BFBBDB");
        concept.setStandard(true);
        factory.addTerm(concept, "29D0C817DEDD439F9DE1F65A7E0D2707", "Ontology Data Structures");
        factory.addDefinition(concept, "8D59DD167F834F93840C80B4A8E1EEBD", "Primary data group that contains all ontology data structures");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "7852B3431F8947F2B4A8EC4A93B6095C");
        concept.setStandard(true);
        factory.addTerm(concept, "3799861396264434B0B7DC50F2F17E1E", "Organizations");
        factory.addDefinition(concept, "5D10A894187C4387B9F3ECF74DE9A1D5", "Primary data group that contains all organizations");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "1F9227907F91459980D235BB1F4FB4AC");
        concept.setStandard(true);
        factory.addTerm(concept, "EE63B630CB964F809806576E0B7EF68E", "Product Domain");
        factory.addDefinition(concept, "54722CB345BF45F7BCA0BA99DCFA01CA", "Contains all the product domain's Data Requirements");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "5CE86DD57A5E4B10AC62622887DCCD71");
        concept.setStandard(true);
        factory.addTerm(concept, "99830E584EFE43189D48A389EF0AE8C6", "Product Domain");
        factory.addDefinition(concept, "4E9D2860F2C24D4EB2ACD1E36E130800", "Contains all the product domain's Sections");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "3906B2794A314AC2A6F68EA9A543AF7E");
        concept.setStandard(true);
        factory.addTerm(concept, "063268631BE64B07AB2C509AA3475B12", "Product Domain");
        factory.addDefinition(concept, "BC042472B6C74AD2BD016889CA88A266", "Contains all the product domain's Data Requirements Instances");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "8B10384B4C854CD0AD3A7B7F62BA8F22");
        concept.setStandard(true);
        factory.addTerm(concept, "51940F312A0C4BC392DB10D9B855FB68", "Product Domain");
        factory.addDefinition(concept, "DC408802390740C3B035ABAEC54D5E33", "Contains all the product domain's classes");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "C976D75D0DC944C6919B15CAA0BDF091");
        concept.setStandard(true);
        factory.addTerm(concept, "11118ADB57F143AFA548DB7931D4EA63", "Product Domain");
        factory.addDefinition(concept, "E6D242221AE84581A1B2C3107CE0BD7D", "Contains all the product domain's base data values");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "C88111C9B0784AD9B0E71ABCC5C29DAE");
        concept.setStandard(true);
        factory.addTerm(concept, "78159FFA6E424F7FAE4BA8D9B009B204", "Product Domain");
        factory.addDefinition(concept, "E0996B1ECE2B44BB8CC03918CF847AF0", "Contains all the product domain's properties");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "FE48BB07AB8E4425AACCE6D374997D5E");
        concept.setStandard(true);
        factory.addTerm(concept, "2D1B2853752840A2A4954F1E4BC0C338", "Product Domain");
        factory.addDefinition(concept, "A5B542BB6E564D03943F3FC37DEC4850", "Contains all the product domain's data records");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "2B47C93FC2A4459785DEF402D7ACC41A");
        concept.setStandard(true);
        factory.addTerm(concept, "EFCF7244474C4404B10930AD098AB8E7", "Product Domain");
        factory.addDefinition(concept, "0B033FFBC4C649D8989D70FB5907636D", "Contains all the product domain's concept relationships");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "D46004B66BC7421899CFE8947AD65290");
        concept.setStandard(true);
        factory.addTerm(concept, "B7D4975F59C546CEA937D3DF4327362D", "Project Domain");
        factory.addDefinition(concept, "7CC4DB033D3A42229962E8A00259872F", "Contains all the project domain's base data values");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "9E7A3A78917145E4B7CAD29293F16B1D");
        concept.setStandard(true);
        factory.addTerm(concept, "61F8386B9F864F3BA9F4B4117AC08676", "Project Domain");
        factory.addDefinition(concept, "5CB23AA817F2448183681930427A745B", "Contains all the project domain's Data Requirements");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "93E5948433C14D019F2EF01CF7CE0D2A");
        concept.setStandard(true);
        factory.addTerm(concept, "A8101A4264604CCE9057D6B075179BAF", "Project Domain");
        factory.addDefinition(concept, "2FECCC9C298C42A1A2DD469EBBA49C67", "Contains all the project domain's concept relationships");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "7FE924E2F5BD4C19A6FDF2C60E645E25");
        concept.setStandard(true);
        factory.addTerm(concept, "4F77E65987C84EA0A0BC397093EFE441", "Project Domain");
        factory.addDefinition(concept, "C5D313F1667C4309AF59F17AAA7F4B97", "Contains all the project domain's data records");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "B137DDB369C344A2857730D93C46CBE5");
        concept.setStandard(true);
        factory.addTerm(concept, "0233630FC5CD47F59A589FF2D775F4EC", "Project Domain");
        factory.addDefinition(concept, "EDA94C38B6E84E2CB0010BE4BCF2445A", "Contains all the project domain's Data Requirements instances");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "B9017C1220E74530BB881B96C31A1E75");
        concept.setStandard(true);
        factory.addTerm(concept, "F1B14A7182574257ADB1EC0F2192FF43", "Properties");
        factory.addDefinition(concept, "673460C8CA644E388E0002186F34ADA6", "Primary data group that contains all properties");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "59218B4D879249AD84B541D73BE8A0B5");
        concept.setStandard(true);
        factory.addTerm(concept, "A109D115C5C34BF2A431C026900D35D8", "Qualifiers of Measure");
        factory.addDefinition(concept, "372A31F81D494E489810B5085A08738B", "Primary data group that contains all qualifiers of measures");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "400F7B05DFC64F9E9D517E43D22AA7B4");
        concept.setStandard(true);
        factory.addTerm(concept, "D18AEEA27BCE44168D73610561C9E7C9", "Reference Data");
        factory.addDefinition(concept, "0E0E145E82CF4144BF9B43334F28A9A7", "Contains all the reference data values");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "D1FDE5AF1A4E40D6BE0EE27A5742C9EE");
        concept.setStandard(true);
        factory.addTerm(concept, "30A02C24ACA04B0183BB12C5708086C0", "Sections");
        factory.addDefinition(concept, "0E5384A8B2E74B4F945D994E0AE37C0A", "Primary data group that contains all sections");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "6B48B408741342D3A2BD2FFD6CF8A988");
        concept.setStandard(true);
        factory.addTerm(concept, "6737DF29B3C24819A8E3A66470241952", "Service Domain");
        factory.addDefinition(concept, "2D3AE349AEF4453B8E5C2A594BFB6E1E", "Contains all the service domain's data records");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "63ADF06F44D1450B84DB4794EB9DB7B2");
        concept.setStandard(true);
        factory.addTerm(concept, "0CA663A8E63442A79984BFB093F98AE4", "Service Domain");
        factory.addDefinition(concept, "40DD2CAF24D44857B64D35E5CA99DD41", "Contains all the service domain's Data Requirements");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "0897CD8D1C8D4935BC87D385874137CE");
        concept.setStandard(true);
        factory.addTerm(concept, "C7BB5D0C2A604BBA83064FD4CD3D5047", "Service Domain");
        factory.addDefinition(concept, "76A517551D7E4D928DC68F1CA2A5004C", "Contains all the service domain's base data values");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "8A7F69A55457494F8B5CE8C000AB8469");
        concept.setStandard(true);
        factory.addTerm(concept, "A7B7F7DFC39E411E91DD7CB388B6831A", "Service Domain");
        factory.addDefinition(concept, "6DA77E94FC1947F1AF197688F974F473", "Contains all the service domain's concept relationships");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "8A3BEC6C0A9F4CFBB2B19EB8D23E9696");
        concept.setStandard(true);
        factory.addTerm(concept, "2D11816F5B9947D6B965FCCEB4687F4A", "Service Domain");
        factory.addDefinition(concept, "C3D730A47E0348CB86A679D6A8DDA18D", "Contains all the service domain's Data Requirements instances");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "9D3C35918B8C48B9AEC3E5C46730E134");
        concept.setStandard(true);
        factory.addTerm(concept, "468E0F34B69241A889F8564D24D1750A", "Service Domain");
        factory.addDefinition(concept, "C367EB906E494A259B39C1E3A3809296", "Contains all the service domain's classes");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "E11D779955A541B083863F109FFC78AF");
        concept.setStandard(true);
        factory.addTerm(concept, "6FCD6B8BC3384B0AA8B2946A393EDD8F", "Service Domain");
        factory.addDefinition(concept, "845829DE65B446678EB49EEC00BC3E4D", "Contains all the service domain's sections");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "1BCA2FF7E77541F09BD1DBE445D5D044");
        concept.setStandard(true);
        factory.addTerm(concept, "B52165D351374029A05D6E67BF8686E1", "Service Domain");
        factory.addDefinition(concept, "12C6F1B4234045F1B17D1D1DF381A8E2", "Contains all the service domain's properties");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "16D5F1AC06014437B71F90922A72CE9E");
        concept.setStandard(true);
        factory.addTerm(concept, "470E52D4755744FE82355483CD23B16D", "System Default");
        factory.addDefinition(concept, "86E670064DBC4ABDADD1744C5DDEA848", "Contains all the system default concept relationships");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "800D877A26E748F1B057CB12497EACD7");
        concept.setStandard(true);
        factory.addTerm(concept, "A3E03759E0104FEDA8AC21565B8E6E14", "Terminology");
        factory.addDefinition(concept, "1B4BF87523D04F74BFB368CF13897290", "Primary data group that contains all terminology");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "5E1CED2209CC451D813CC8BAB20FAD0C");
        concept.setStandard(true);
        factory.addTerm(concept, "DF1011258DD2403EA4D9FF57ADDEDC17", "Terms");
        factory.addDefinition(concept, "262E20583C46402BB2286EBD057D780C", "Primary data group that contains all terms");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "34F7CFAC5CED4D8394EB2CCA9896B01D");
        concept.setStandard(true);
        factory.addTerm(concept, "662DBE748F9B48C38C74C4F145DBA283", "Units of Measure");
        factory.addDefinition(concept, "26A77CBEC644493683E78AAE22B0B931", "Primary data group that contains all unit of measures");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "E31FD919F6C242CB8770D456A53CA32C");
        concept.setStandard(true);
        factory.addTerm(concept, "1DA69E0846764777B9DAD44C773EFFA9", "Values");
        factory.addDefinition(concept, "E778DFA92F15457891C291D63B322345", "Primary data group that contains all values");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "DD22CDC773ED45C39E5DE7347DEE2434");
        concept.setStandard(true);
        factory.addTerm(concept, "904F0C5034984D428C76C0CBC6405388", "Vendor Domain");
        factory.addDefinition(concept, "FA576A0CFA1F4D2BADC13CE8A5145367", "Contains all the vendor domain's concept relationships");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "45591E766EAC4C38A13548D493432598");
        concept.setStandard(true);
        factory.addTerm(concept, "24611EFD67384C969C93ECB761DFF8A7", "Vendor Domain");
        factory.addDefinition(concept, "049FDC6364EA45518F4E6AAC803891F2", "Contains all the vendor domain's sections");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "EE32627DADB6403290A56CCC204CA26C");
        concept.setStandard(true);
        factory.addTerm(concept, "05C79E253D2D49DABAD84F71E15F075F", "Vendor Domain");
        factory.addDefinition(concept, "B25D7FDA491245D69C0CDA72F0126C63", "Contains all the vendor domain's Data Requirements");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "7CEF8CC8611944928102C3F69C056171");
        concept.setStandard(true);
        factory.addTerm(concept, "0636B717A74549FFB7AD18D7BEE50941", "Vendor Domain");
        factory.addDefinition(concept, "CCAC6EA2E05E4B3B8EF34E06D398A17F", "Contains all the vendor domain's Data Requirement Instances");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "4A10982EF00F4696AE9CB9FA64785BFD");
        concept.setStandard(true);
        factory.addTerm(concept, "3281E97C40A547B99E53965104FFBCE7", "Vendor Domain");
        factory.addDefinition(concept, "1EA2FDF229D3459AB2B41698B3AFB545", "Contains all the vendor domain's properties");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "A2CB102549E44EBABFF8316C8574AD7C");
        concept.setStandard(true);
        factory.addTerm(concept, "D64080C62F1E48C581373F10824333F8", "Vendor Domain");
        factory.addDefinition(concept, "591986911DAD4C7199FBBDA4CEB51436", "Contains all the vendor domain's data records");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "E914FBC02B3440F593E3D449B99CCE59");
        concept.setStandard(true);
        factory.addTerm(concept, "67364E3E73E9467CA8B421FF82941843", "Vendor Domain");
        factory.addDefinition(concept, "70F5382A3AFB4B00A320405E1918E06E", "Contains all the vendor domain's base data values");
        ontApi.saveConcept(concept, true, true, true, true, true, true);

        concept = factory.constructConcept(T8OntologyConceptType.ONTOLOGY_CLASS, "3A3E58545A9F43BC96EC9A39667C8522");
        concept.setStandard(true);
        factory.addTerm(concept, "16FB0F8840914C879969E4D2AB5D2793", "Vendor Domain");
        factory.addDefinition(concept, "0BF85F72EAB346FEB11E20A8B0E873BD", "Contains all the vendor domain's classes");
        ontApi.saveConcept(concept, true, true, true, true, true, true);
    }

    public void createBaseDataStructure(T8DataTransaction tx) throws Exception
    {
        // Add the root.
        ontApi.saveOntologyClass(null, "68A50FD2546C4F11AECD93F9F55D62A8");

        // Link primary groups to the group structure.
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "435E6326A83A478094FF60395FCD434C");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "2A78617D48C049BB99AD55BD2478C540");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "25BD559DAF1B417FB11D4F6147BFBBDB");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "267A194C0E0B4E05B4C83A39B808A36F");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "9FCF20DD2C42457BBF6F269024D2093F");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "0BFCA2C6A82B4E6A99D51B3FC6FE7711");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "6CF50D78B1C947309ED9152905B177F3");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "7E13F778C7D149ABA22D134A290F1754");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "7852B3431F8947F2B4A8EC4A93B6095C");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "54EA4FDC1C9C4267A708EE3F4A90FE94");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "800D877A26E748F1B057CB12497EACD7");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "FA3F7BFBEC6D4C2B8E72BFB288D37168");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "082D8F43D336470EBDEFBDEDCA7D9093");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "3DFA79973874433E83EFBD4670EBFF42");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "6C4CC77A1F8D4B3DA669B5718C6C152A");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "76C8864E692B46C8AFC5B4A5E2A57AE0");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "7B2973E5D29349A4A576A74213C1F65D");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "DA70E6220068497E84DE98A60D858CB7");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "4972E8B5AC3546E7B54D898014B7C3C0");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "02164AEE4E364775AF428195D33164CB");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "0CA3364A02CC47388EED4F71908B0213");
        ontApi.saveOntologyClass("68A50FD2546C4F11AECD93F9F55D62A8", "5E1CED2209CC451D813CC8BAB20FAD0C");

        // Link primary DR elements.
        ontApi.saveOntologyClass("0CA3364A02CC47388EED4F71908B0213", "D1FDE5AF1A4E40D6BE0EE27A5742C9EE");
        ontApi.saveOntologyClass("0CA3364A02CC47388EED4F71908B0213", "222131C6D9204E55BC0FBDFC6B9E660F");
        ontApi.saveOntologyClass("0CA3364A02CC47388EED4F71908B0213", "E31FD919F6C242CB8770D456A53CA32C");
        ontApi.saveOntologyClass("0CA3364A02CC47388EED4F71908B0213", "34F7CFAC5CED4D8394EB2CCA9896B01D");
        ontApi.saveOntologyClass("0CA3364A02CC47388EED4F71908B0213", "B9017C1220E74530BB881B96C31A1E75");
        ontApi.saveOntologyClass("0CA3364A02CC47388EED4F71908B0213", "46A1D25715F74B9D8F0048E3B080B464");
        ontApi.saveOntologyClass("0CA3364A02CC47388EED4F71908B0213", "59218B4D879249AD84B541D73BE8A0B5");

        // Create Base Values group.
        ontApi.saveOntologyClass("E31FD919F6C242CB8770D456A53CA32C", "494EBE95276340B599D4B09D1D9B4391");

        // Create Reference Values group.
        ontApi.saveOntologyClass("E31FD919F6C242CB8770D456A53CA32C", "400F7B05DFC64F9E9D517E43D22AA7B4");

        // Add all other base groups to the primary primary groups.
        ontApi.saveOntologyClass("222131C6D9204E55BC0FBDFC6B9E660F", "9D3C35918B8C48B9AEC3E5C46730E134");
        ontApi.saveOntologyClass("222131C6D9204E55BC0FBDFC6B9E660F", "8B10384B4C854CD0AD3A7B7F62BA8F22");
        ontApi.saveOntologyClass("222131C6D9204E55BC0FBDFC6B9E660F", "3A3E58545A9F43BC96EC9A39667C8522");
        ontApi.saveOntologyClass("494EBE95276340B599D4B09D1D9B4391", "D46004B66BC7421899CFE8947AD65290");
        ontApi.saveOntologyClass("494EBE95276340B599D4B09D1D9B4391", "E914FBC02B3440F593E3D449B99CCE59");
        ontApi.saveOntologyClass("494EBE95276340B599D4B09D1D9B4391", "C976D75D0DC944C6919B15CAA0BDF091");
        ontApi.saveOntologyClass("494EBE95276340B599D4B09D1D9B4391", "0897CD8D1C8D4935BC87D385874137CE");
        ontApi.saveOntologyClass("54EA4FDC1C9C4267A708EE3F4A90FE94", "B137DDB369C344A2857730D93C46CBE5");
        ontApi.saveOntologyClass("54EA4FDC1C9C4267A708EE3F4A90FE94", "3906B2794A314AC2A6F68EA9A543AF7E");
        ontApi.saveOntologyClass("54EA4FDC1C9C4267A708EE3F4A90FE94", "8A3BEC6C0A9F4CFBB2B19EB8D23E9696");
        ontApi.saveOntologyClass("54EA4FDC1C9C4267A708EE3F4A90FE94", "7CEF8CC8611944928102C3F69C056171");
        ontApi.saveOntologyClass("7B2973E5D29349A4A576A74213C1F65D", "2B47C93FC2A4459785DEF402D7ACC41A");
        ontApi.saveOntologyClass("7B2973E5D29349A4A576A74213C1F65D", "16D5F1AC06014437B71F90922A72CE9E");
        ontApi.saveOntologyClass("7B2973E5D29349A4A576A74213C1F65D", "8A7F69A55457494F8B5CE8C000AB8469");
        ontApi.saveOntologyClass("7B2973E5D29349A4A576A74213C1F65D", "DD22CDC773ED45C39E5DE7347DEE2434");
        ontApi.saveOntologyClass("7B2973E5D29349A4A576A74213C1F65D", "93E5948433C14D019F2EF01CF7CE0D2A");
        ontApi.saveOntologyClass("7E13F778C7D149ABA22D134A290F1754", "6B48B408741342D3A2BD2FFD6CF8A988");
        ontApi.saveOntologyClass("7E13F778C7D149ABA22D134A290F1754", "FE48BB07AB8E4425AACCE6D374997D5E");
        ontApi.saveOntologyClass("7E13F778C7D149ABA22D134A290F1754", "A2CB102549E44EBABFF8316C8574AD7C");
        ontApi.saveOntologyClass("7E13F778C7D149ABA22D134A290F1754", "7FE924E2F5BD4C19A6FDF2C60E645E25");
        ontApi.saveOntologyClass("9FCF20DD2C42457BBF6F269024D2093F", "EE32627DADB6403290A56CCC204CA26C");
        ontApi.saveOntologyClass("9FCF20DD2C42457BBF6F269024D2093F", "1F9227907F91459980D235BB1F4FB4AC");
        ontApi.saveOntologyClass("9FCF20DD2C42457BBF6F269024D2093F", "63ADF06F44D1450B84DB4794EB9DB7B2");
        ontApi.saveOntologyClass("9FCF20DD2C42457BBF6F269024D2093F", "9E7A3A78917145E4B7CAD29293F16B1D");
        ontApi.saveOntologyClass("B9017C1220E74530BB881B96C31A1E75", "1BCA2FF7E77541F09BD1DBE445D5D044");
        ontApi.saveOntologyClass("B9017C1220E74530BB881B96C31A1E75", "C88111C9B0784AD9B0E71ABCC5C29DAE");
        ontApi.saveOntologyClass("B9017C1220E74530BB881B96C31A1E75", "4A10982EF00F4696AE9CB9FA64785BFD");
        ontApi.saveOntologyClass("D1FDE5AF1A4E40D6BE0EE27A5742C9EE", "E11D779955A541B083863F109FFC78AF");
        ontApi.saveOntologyClass("D1FDE5AF1A4E40D6BE0EE27A5742C9EE", "45591E766EAC4C38A13548D493432598");
        ontApi.saveOntologyClass("D1FDE5AF1A4E40D6BE0EE27A5742C9EE", "5CE86DD57A5E4B10AC62622887DCCD71");
    }
}
