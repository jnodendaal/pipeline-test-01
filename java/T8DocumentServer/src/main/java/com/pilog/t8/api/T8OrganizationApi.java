package com.pilog.t8.api;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.org.T8OrganizationDataHandler;
import com.pilog.t8.data.org.T8OrganizationStructureProvider;
import com.pilog.t8.definition.data.org.T8OrganizationDataCacheDefinition;
import com.pilog.t8.definition.data.org.T8OrganizationStructureCacheDefinition;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalSettingsDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.org.operational.T8OrganizationOperationalHoursCalculator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationApi implements T8Api, T8PerformanceStatisticsProvider, T8OrganizationStructureProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8OrganizationApi.class);

    private final T8DataTransaction tx;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8OrganizationSetupDefinition setupDefinition;
    private final T8OrganizationDataHandler organizationDataHandler;
    private T8OrganizationStructure organizationStructure;
    private T8OrganizationOperationalHoursCalculator operationalHoursCalculator;
    private T8PerformanceStatistics stats;

    public static final String API_IDENTIFIER = "@API_ORGANIZATION";

    public T8OrganizationApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.sessionContext = tx.getContext().getSessionContext();
        this.serverContext = tx.getContext().getServerContext();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is always false.  Other clients of the API can set this variable to their own instance of stats.
        this.setupDefinition = T8OrganizationSetupDefinition.getOrganizationSetupDefinition(tx.getContext());
        this.organizationDataHandler = new T8OrganizationDataHandler(serverContext);
        this.organizationDataHandler.setPerformanceStatistics(stats);
        this.organizationStructure = (T8OrganizationStructure)serverContext.getDataManager().getCachedData(T8OrganizationStructureCacheDefinition.DATA_IDENTIFIER, HashMaps.createSingular(T8OrganizationStructureCacheDefinition.DATA_PARAMETER_ORG_ID, sessionContext.getOrganizationIdentifier()));
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    public String getDefaultContentLanguageId()
    {
        return setupDefinition.getDefaultContentLanguageIdentifier();
    }

    public String getAttachmentHandlerId()
    {
        return setupDefinition.getAttachmentHandlerIdentifier();
    }

    public T8OrganizationOperationalSettingsDefinition getOperationalSettingsDefinition()
    {
        T8OrganizationOperationalSettingsDefinition operationalSettingsDefinition;
        String operationalSettingsId;

        operationalSettingsId = this.setupDefinition.getOrganizationOperationalSettingsIdentifier();

        try
        {
            operationalSettingsDefinition = this.serverContext.getDefinitionManager().getRawDefinition(tx.getContext(), null, operationalSettingsId);
            if (operationalSettingsDefinition != null)
            {
                return operationalSettingsDefinition;
            }
            else throw new NullPointerException("Could not find definiion : {"+operationalSettingsId+"}");
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to retrieve the Organization Operational Settings Definition : {"+operationalSettingsId+"}", ex);
            throw new RuntimeException("Failed to retrieve the Organization Operational Settings Definition : {"+operationalSettingsId+"}", ex);
        }
    }

    public String getRootOrgId()
    {
        return organizationStructure.getRootOrganizationID();
    }

    @Override
    public T8OrganizationStructure getOrganizationStructure()
    {
        return organizationStructure;
    }

    public T8OrganizationStructure recacheOrganizationStructure() throws Exception
    {
        T8DataManager dataManager;

        dataManager = serverContext.getDataManager();
        this.organizationStructure = (T8OrganizationStructure)dataManager.recacheData(T8OrganizationStructureCacheDefinition.DATA_IDENTIFIER, HashMaps.createSingular(T8OrganizationStructureCacheDefinition.DATA_PARAMETER_ORG_ID, sessionContext.getOrganizationIdentifier()));
        return organizationStructure;
    }

    public List<String> getCurrentLineageIdList()
    {
        return organizationStructure.getOrganizationLineageIDList(sessionContext.getOrganizationIdentifier());
    }

    public void refreshOrganizationStructureNestedIndices() throws Exception
    {
        List<String> nestedList;
        List<String> orgIDList;

        nestedList = this.organizationStructure.getNestedIdList();
        orgIDList = this.organizationStructure.getOrganizationIDList();
        for (String orgID : orgIDList)
        {
            this.organizationDataHandler.updateOrganizationIndices(this.tx, orgID, nestedList.indexOf(orgID), nestedList.lastIndexOf(orgID));
        }
    }

    public void saveOrganization(String parentOrgId, String orgTypeId, T8OntologyConcept organizationConcept, boolean saveTerminology) throws Exception
    {
        int organizationLevel;
        T8OntologyApi ontApi;
        String rootOrgId;
        String orgId;

        // Log the start of the operation.
        stats.logExecutionStart("saveOrganization");

        // Get the data we require for the update.
        rootOrgId = organizationStructure.getRootOrganizationID();
        orgId = organizationConcept.getID();
        organizationLevel = organizationDataHandler.retrieveOrganizationLevel(tx, parentOrgId) + 1;

        // First save the concept.
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        ontApi.saveConcept(organizationConcept, false, true, true, true, true, saveTerminology);

        // Now save the organization (in ORG_STRUCTURE).
        organizationDataHandler.saveOrganization(tx, parentOrgId, orgId, orgTypeId, rootOrgId, organizationLevel);

        // Move the organization to the proper parent.
        moveOrganization(orgId, parentOrgId);

        // Update the organization type.
        organizationDataHandler.updateOrganizationType(tx, orgId, orgTypeId);

        // Now log the completion of the operation.
        stats.logExecutionEnd("saveOrganization");
    }

    public void insertOrganization(String parentOrgId, String orgTypeId, T8OntologyConcept organizationConcept, boolean insertTerminology) throws Exception
    {
        T8OntologyApi ontApi;
        String rootOrgID;

        // Log the start of the operation.
        stats.logExecutionStart("insertOrganization");

        // First insert the concept.
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        ontApi.insertConcept(organizationConcept, true, true, true, true, insertTerminology);

        // Get the list of Organization ID's in the path to the new Organization.
        rootOrgID = organizationStructure.getRootOrganizationID();

        // Now add the new concept to the organization structure.
        organizationDataHandler.insertOrganization(tx, parentOrgId, organizationConcept.getID(), orgTypeId, rootOrgID, 0);

        // Now log the completion of the operation.
        stats.logExecutionEnd("insertOrganization");
    }

    public boolean deleteOrganization(String orgId, boolean deleteOntology) throws Exception
    {
        stats.logExecutionStart("deleteOrganization");

        // First remove the organization from the organization structure.
        organizationDataHandler.deleteOrganization(tx, orgId);

        // Delete the ontology if required.
        if (deleteOntology)
        {
            T8OntologyApi ontApi;

            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            ontApi.deleteConcept(orgId);
        }

        // Log statistics.
        stats.logExecutionEnd("deleteOrganization");

        // Return the result.
        return true;
    }

    public boolean moveOrganization(String orgId, String newParentOrgId) throws Exception
    {
        String rootOrgId;
        boolean result;

        // Log the start of the operation.
        stats.logExecutionStart("moveOrganizationToNewParent");

        // Get the list of Organization ID's in the path to the new parent Organization.
        rootOrgId = organizationStructure.getRootOrganizationID();

        // Now move the organization by updating the entity.
        result = organizationDataHandler.moveOrganizationToNewParent(tx, orgId, newParentOrgId, rootOrgId, 0);

        // Log the end of the operation.
        stats.logExecutionEnd("moveOrganizationToNewParent");
        return result;
    }

    public boolean updateOrganizationType(String orgID, String newOrgTypeID) throws Exception
    {
        boolean result;

        stats.logExecutionStart("updateOrganizationType");
        result = organizationDataHandler.updateOrganizationType(tx, orgID, newOrgTypeID);
        stats.logExecutionEnd("updateOrganizationType");
        return result;
    }

    /**
     * Returns all Language ID's linked to the specified organization or one of
     * its parents from the organization's data cache.
     * @return The set of Language ID's linked to the specified organization or
     * one of its parents.
     * @param orgId The organization for which to retrieve the language ID set.
     * @throws java.lang.Exception
     */
    public Set<String> getLanguageIdSet(String orgId) throws Exception
    {
        T8DataManager dataManager;

        dataManager = serverContext.getDataManager();
        return (Set<String>)dataManager.getCachedData(T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_LANGUAGE_ID_SET, HashMaps.createSingular(T8OrganizationDataCacheDefinition.DATA_PARAMETER_ORG_ID, orgId));
    }

    /**
     * Returns all Language ID's linked to the current session organization any
     * of the organization belonging to the same root.
     * @return The set of Language ID's linked to the current session organization any
     * of the organization belonging to the same root.
     * @throws java.lang.Exception
     */
    public Set<String> getLanguageIdSet() throws Exception
    {
        T8DataManager dataManager;

        dataManager = serverContext.getDataManager();
        return (Set<String>)dataManager.getCachedData(T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_GLOBAL_LANGUAGE_ID_SET, HashMaps.createSingular(T8OrganizationDataCacheDefinition.DATA_PARAMETER_ORG_ID, organizationStructure.getRootOrganizationID()));
    }

    /**
     * Returns all Language ID's linked to the current session organization or
     * one of its parents.
     * @return The set of Language ID's linked to the current organization or
     * one of its parents.
     * @throws java.lang.Exception
     */
    public Set<String> retrieveOrganizationLanguageIdSet() throws Exception
    {
        T8OntologyStructure ontologyStructure;
        T8OntologyApi ontApi;
        Set<String> languageIDList;
        List<String> orgIdList;
        List<String> ocIdList;

        stats.logExecutionStart("retrieveOrganizationLanguageIdSet");

        // Get the list of ORG_ID's that we can use in the filter.
        orgIdList = organizationStructure.getOrganizationPathIDList(sessionContext.getOrganizationIdentifier());

        // Get the list of ODT_ID's that we can use in the filter.
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        ontologyStructure = ontApi.getOntologyStructure();
        ocIdList = ontologyStructure.getDescendantClassIDList(T8PrimaryOntologyDataType.LANGUAGES.getDataTypeID());
        ocIdList.add(T8PrimaryOntologyDataType.LANGUAGES.getDataTypeID()); // Add the ODT_ID to the list of its descendants.

        languageIDList = new HashSet<>(organizationDataHandler.retrieveConceptIDSet(tx, orgIdList, ocIdList));
        stats.logExecutionEnd("retrieveOrganizationLanguageIdSet");
        return languageIDList;
    }

    /**
     * Returns the operational hours calculator to be used for the current session organization.  This
     * calculator can be used to determine time spans and periods that take the pre-configured business hours
     * of the organization into account.
     * @return The operational hours calculator to be used for the current session organization.
     * @throws Exception
     */
    public T8OrganizationOperationalHoursCalculator getOperationalHoursCalculator() throws Exception
    {
        if (operationalHoursCalculator != null)
        {
            return operationalHoursCalculator;
        }
        else
        {
            String settingsId;

            settingsId = setupDefinition.getOrganizationOperationalSettingsIdentifier();
            if (settingsId != null)
            {
                T8OrganizationOperationalSettingsDefinition settingsDefinition;

                settingsDefinition = serverContext.getDefinitionManager().getInitializedDefinition(tx.getContext(), null, settingsId, null);
                if (settingsDefinition != null)
                {
                    operationalHoursCalculator = settingsDefinition.getOrganizationOperationalHoursCalculator();
                    return operationalHoursCalculator;
                }
                else throw new RuntimeException("Cannot find specified operational hours settings: " + settingsId);
            }
            else throw new RuntimeException("No operational hours settings specified in organization setup definition: " + setupDefinition);
        }
    }
}
