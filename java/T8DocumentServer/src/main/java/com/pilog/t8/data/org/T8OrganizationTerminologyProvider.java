package com.pilog.t8.data.org;

import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.document.TerminologyProvider;
import java.util.Collection;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationTerminologyProvider implements TerminologyProvider
{
    private final T8TerminologyApi trmApi;
    private String defaultLanguageId;

    public T8OrganizationTerminologyProvider(T8TerminologyApi trmApi)
    {
        this.trmApi = trmApi;
    }

    @Override
    public void setLanguage(String languageId)
    {
        this.defaultLanguageId = languageId;
    }

    @Override
    public String getCode(String conceptId)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(null, conceptId);
        return terminology != null ? terminology.getCode() : null;
    }

    @Override
    public String getTerm(String languageId, String conceptId)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageId, conceptId);
        return terminology != null ? terminology.getTerm() : null;
    }

    @Override
    public String getAbbreviation(String languageId, String conceptId)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageId, conceptId);
        return terminology != null ? terminology.getAbbreviation() : null;
    }

    @Override
    public String getDefinition(String languageId, String conceptId)
    {
        T8ConceptTerminology terminology;

        terminology = getTerminology(languageId, conceptId);
        return terminology != null ? terminology.getDefinition() : null;
    }

    @Override
    public T8ConceptTerminology getTerminology(String languageId, String conceptId)
    {
        try
        {
            return trmApi.retrieveTerminology(languageId != null ? languageId : defaultLanguageId, conceptId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving terminology.", e);
        }
    }

    @Override
    public T8ConceptTerminologyList getTerminology(Collection<String> languageIds, Collection<String> conceptIds)
    {
        try
        {
            return trmApi.retrieveTerminology(languageIds, conceptIds);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving terminology.", e);
        }
    }
}

