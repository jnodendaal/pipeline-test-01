package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.datarecord.value.BooleanValue;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarecord.value.IntegerValue;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.datarecord.value.RealValue;
import com.pilog.t8.data.document.datarecord.value.StringValue;
import com.pilog.t8.data.document.datarecord.value.TextValue;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredRangeRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.strings.Strings;
import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordFactory
{
    private final T8OntologyApi ontApi;
    private DataRecord contextRecord;

    // Uses the regex: (?>[0-9]* [0-9]+\/[0-9]+)|(?>[0-9]+\/[0-9]+)|-?\.\d*|-?\d+(?>\.\d*)?|[0-9]+
    // Matches: 49832
    //          -5738
    //          0.5
    //          .5
    //          -0.5
    //          -.5
    // Non-Matches: 1 000
    //              1,000
    private static final Pattern DEFAULT_NUMERIC_VALUE_PATTERN = Pattern.compile("(?>[0-9]* [0-9]+\\/[0-9]+)|(?>[0-9]+\\/[0-9]+)|-?\\.\\d*|-?\\d+(?>\\.\\d*)?|[0-9]+");

    public T8DataRecordFactory(T8DataTransaction tx)
    {
        this.ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
    }

    public void setContext(DataRecord context)
    {
        this.contextRecord = context;
    }

    public RecordValue setValue(String propertyId, Object value)
    {
        return setValue(contextRecord, propertyId, null, value);
    }

    public RecordValue setValue(String recordId, String propertyId, Object value)
    {
        return setValue(recordId, propertyId, null, value);
    }

    public RecordValue setValue(String recordId, String propertyId, String fieldId, Object value)
    {
        return setValue(contextRecord.findDataRecord(recordId), propertyId, fieldId, value);
    }

    public RecordValue setValue(DataRecord record, String propertyId, String fieldId, Object value)
    {
        RecordProperty targetProperty;
        String valueString;

        valueString = value != null ? Strings.trimToNull(value.toString()) : null;
        targetProperty = record.getOrAddRecordProperty(propertyId);
        if (fieldId != null)
        {
            RecordValue newValue;
            Field targetField;

            targetField = targetProperty.getOrAddField(fieldId);
            if (valueString != null)
            {
                newValue = constructValue(targetField.getFieldValueRequirement(), valueString);
                if (newValue != null)
                {
                    targetField.setFieldValue(newValue);
                    return newValue;
                }
                else
                {
                    targetField.getParentRecordProperty().appendFFTChecked(valueString, " ");
                    return null;
                }
            }
            else
            {
                targetField.setValue(null);
                return null;
            }
        }
        else if (valueString != null)
        {
            RecordValue newValue;

            newValue = constructValue(targetProperty.getPropertyRequirement().getValueRequirement(), valueString);
            if (newValue != null)
            {
                targetProperty.setRecordValue(newValue);
                return newValue;
            }
            else
            {
                targetProperty.appendFFTChecked(valueString, " ");
                return null;
            }
        }
        else
        {
            targetProperty.setRecordValue(null);
            return null;
        }
    }

    public RecordValue addAttachment(String propertyId, String fileContextIid, String path)
    {
        T8AttachmentDetails attachmentDetails;
        ValueRequirement valueRequirement;
        RequirementType requirementType;
        RecordProperty property;
        String attachmentId;

        // Create the details of the new attachment.
        attachmentId = T8IdentifierUtilities.createNewGUID();
        attachmentDetails = new T8AttachmentDetails(attachmentId, fileContextIid, null, path, new File(path).getName(), -1, null, null);

        // Get the record property or add it if it does not exist.
        property = contextRecord.getOrAddRecordProperty(propertyId);

        // Now add the new property value.
        valueRequirement = property.getPropertyRequirement().getValueRequirement();
        requirementType = valueRequirement.getRequirementType();
        if (requirementType == RequirementType.ATTACHMENT)
        {
            AttachmentList attachmentList;

            // Get the attachment list or create it if it does not yet exist.
            attachmentList = (AttachmentList)property.getOrAddValue();
            attachmentList.addAttachment(attachmentId);
            attachmentList.addAttachmentDetails(attachmentId, attachmentDetails);
            return attachmentList;
        }
        else throw new RuntimeException("Property Value Requirement is not Attachment Type: " + requirementType);
    }

    public RecordValue addAttachment(String propertyId, T8AttachmentDetails attachmentDetails)
    {
        ValueRequirement valueRequirement;
        RequirementType requirementType;
        RecordProperty property;
        String attachmentId;

        // Create the details of the new attachment.
        attachmentId = attachmentDetails.getAttachmentId();

        // Get the record property or add it if it does not exist.
        property = contextRecord.getOrAddRecordProperty(propertyId);

        // Now add the new property value.
        valueRequirement = property.getPropertyRequirement().getValueRequirement();
        requirementType = valueRequirement.getRequirementType();
        if (requirementType == RequirementType.ATTACHMENT)
        {
            AttachmentList attachmentList;

            // Get the attachment list or create it if it does not yet exist.
            attachmentList = (AttachmentList)property.getOrAddValue();
            attachmentList.addAttachment(attachmentId);
            attachmentList.addAttachmentDetails(attachmentId, attachmentDetails);
            return attachmentList;
        }
        else throw new RuntimeException("Property Value Requirement is not Attachment Type: " + requirementType);
    }

    private RecordValue constructValue(ValueRequirement valueRequirement, String inputString)
    {
        RecordValue newValue;
        String valueString;

        valueString = inputString;

        newValue = RecordValue.createValue(valueRequirement);
        if (newValue instanceof StringValue)
        {
            StringValue stringValue;

            stringValue = (StringValue)newValue;
            stringValue.setString(valueString);
            return stringValue;
        }
        else if (newValue instanceof TextValue)
        {
            TextValue textValue;

            textValue = (TextValue)newValue;
            textValue.setText(valueString);
            return textValue;
        }
        else if (newValue instanceof BooleanValue)
        {
            BooleanValue boolValue;

            boolValue = (BooleanValue)newValue;
            boolValue.setBooleanValue(Strings.isTrueRepresentation(inputString, null));
            return boolValue;
        }
        else if (newValue instanceof RealValue)
        {
            RealValue realValue;

            realValue = (RealValue)newValue;
            realValue.setValue(valueString);
            return realValue;
        }
        else if (newValue instanceof IntegerValue)
        {
            IntegerValue integerValue;

            integerValue = (IntegerValue)newValue;
            integerValue.setValue(valueString);
            return integerValue;
        }
        else if (newValue instanceof ControlledConcept)
        {
            ControlledConceptRequirement requirement;
            ControlledConcept controlledConcept;
            String conceptId;

            controlledConcept = (ControlledConcept)newValue;
            requirement = controlledConcept.getRequirement();

            conceptId = extractConceptId(requirement.getOntologyClassID(), valueString);
            if (conceptId != null)
            {
                controlledConcept.setConceptId(conceptId, false);
                return controlledConcept;
            }
            else
            {
                controlledConcept.setTerm(valueString);
                return controlledConcept;
            }
        }
        else if (newValue instanceof MeasuredNumber)
        {
            MeasuredNumberRequirement requirement;
            MeasuredNumber measuredNumber;
            String number;
            String uomId;

            measuredNumber = (MeasuredNumber)newValue;
            requirement = measuredNumber.getRequirement();

            number = extractNumericValue(valueString);
            valueString = valueString.replace(number, "");
            valueString =  Strings.trimToNull(valueString);
            uomId = valueString != null ? extractConceptId(requirement.getUomOntologyClassId(), valueString) : null;

            measuredNumber.setNumber(number);
            measuredNumber.setUomId(uomId);
            return measuredNumber;
        }
        else if (newValue instanceof MeasuredRange)
        {
            MeasuredRangeRequirement requirement;
            MeasuredRange measuredRange;
            String lowerBoundNumber;
            String upperBoundNumber;
            String uomId;
            int vLength;

            measuredRange = (MeasuredRange)newValue;
            requirement = measuredRange.getRequirement();

            vLength = valueString.length();
            lowerBoundNumber = extractNumericValue(valueString);

            if(lowerBoundNumber != null)
            {
                valueString = valueString.substring(0 + (valueString.indexOf(lowerBoundNumber)+lowerBoundNumber.length()), vLength);
            }

            vLength = valueString.length();
            upperBoundNumber = extractNumericValue(valueString);

            if(upperBoundNumber != null)
            {
                valueString =  valueString.substring(0 + (valueString.indexOf(upperBoundNumber)+upperBoundNumber.length()), vLength);
            }

            valueString =  Strings.trimToNull(valueString);
            uomId = valueString != null ? extractConceptId(requirement.getUomOntologyClassId(), valueString) : null;

            measuredRange.setLowerBoundNumber(lowerBoundNumber);
            measuredRange.setUpperBoundNumber(upperBoundNumber);
            measuredRange.setUomId(uomId);
            return measuredRange;
        }
        else return null;
    }

    private String extractNumericValue(String valueString)
    {
        Matcher matcher;

        matcher = DEFAULT_NUMERIC_VALUE_PATTERN.matcher(valueString);
        if (matcher.find())
        {
            return matcher.group();
        }
        else return null;
    }

    private String extractConceptId(String ocId, String valueString)
    {
        if (T8IdentifierUtilities.isConceptId(valueString)) return valueString;

        try
        {
            List<String> conceptIds;

            conceptIds = ontApi.retrieveConceptIdsByTerm(ocId, valueString, true);
            if (conceptIds.isEmpty())
            {
                conceptIds = ontApi.retrieveConceptIdsByCode(ocId, valueString, true);
            }

            return conceptIds.size() > 0 ? conceptIds.get(0) : null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving concept id for term: " + valueString, e);
        }
    }
}
