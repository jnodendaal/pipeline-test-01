package com.pilog.t8.data.document.datarecord;

import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordDefinitionGenerator;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordOntologyHandler
{
    private final T8DataTransaction tx;
    private final T8OntologyApi ontApi;
    private final T8TerminologyApi trmApi;
    private final T8OntologyStructure ontologyStructure;
    private final TerminologyProvider terminologyProvider;

    public T8DataRecordOntologyHandler(T8DataTransaction tx)
    {
        this.tx = tx;
        this.ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
        this.trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
        this.ontologyStructure = ontApi.getOntologyStructure();
        this.terminologyProvider = trmApi.getTerminologyProvider();
    }

    public void normalizeRecordTerminology(DataRecord record, Set<String> languageIdSet)
    {
        T8DataRecordDefinitionGenerator definitionGenerator;
        T8OrganizationOntologyFactory ontologyFactory;
        T8OntologyConcept concept;
        String drInstanceId;

        // Get an ontology factory to use for this operation.
        ontologyFactory = ontApi.getOntologyFactory();

        // Construct the value String generator.
        definitionGenerator = new T8DataRecordDefinitionGenerator(terminologyProvider);
        // We never want the definition Strings to include boolean values, as they serve little/no display purpose.
        definitionGenerator.addExcludedRequirementType(RequirementType.BOOLEAN_TYPE);
        // We never want the definition Strings to include File GUID values, as they serve little/no display purpose.
        definitionGenerator.addExcludedRequirementType(RequirementType.ATTACHMENT);

        // Get the existing record ontology details.
        drInstanceId = record.getDataRequirementInstanceID();
        concept = record.getOntology();

        // Generate the value string for the record in each on of the languages required.
        for (String languageId : languageIdSet)
        {
            T8ConceptTerminology recordTerminology;
            StringBuilder definitionValueString;
            T8OntologyTerm existingTerm;
            T8OntologyDefinition existingDefinition;
            String recordTerm;
            String recordDefinition;

            // Set the language on the ontology factory.
            ontologyFactory.setLanguageID(languageId);

            // Get the existing terminology of the record.
            recordTerminology = terminologyProvider.getTerminology(languageId, concept.getID());

            // Set the term of the record equal to the DR Instance term.
            recordTerm = terminologyProvider.getTerm(languageId, drInstanceId);
            if (recordTerm == null) recordTerm = drInstanceId; // If we could not find a proper term, use the concept id.

            // Set the new term on the record concept.  If an existing term was found in the record's concept, use it so we do not create unneeded terms.
            existingTerm = concept.getTermByLanguage(languageId);
            if (existingTerm != null)
            {
                existingTerm.setTerm(recordTerm);
            }
            else if (recordTerminology != null)
            {
                String termId;

                // Try to use the term id from the already cached terminology (if present).
                termId = recordTerminology.getTermId();
                if (termId != null)
                {
                    ontologyFactory.addTerm(concept, termId, recordTerm);
                }
                else
                {
                    ontologyFactory.setTerm(concept, recordTerm);
                }
            }
            else
            {
                ontologyFactory.setTerm(concept, recordTerm);
            }

            // Generate and set the definition of the record.
            definitionGenerator.setLanguageID(languageId);
            definitionValueString = definitionGenerator.generateValueString(record);
            recordDefinition = definitionValueString != null ? definitionValueString.toString().trim() : null;
            if (Strings.isNullOrEmpty(recordDefinition))
            {
                recordDefinition = recordTerm;
            }

            // Set the new definition on the record concept.  If an existing definition was found in the record's concept, use it so we do not create unneeded definitions.
            existingDefinition = concept.getDefinitionByLanguage(languageId);
            if (existingDefinition != null)
            {
                existingDefinition.setDefinition(recordDefinition);
            }
            else if (recordTerminology != null)
            {
                String definitionId;

                // Try to use the definition id from the already cached terminology (if present).
                definitionId = recordTerminology.getDefinitionId();
                if (definitionId != null)
                {
                    ontologyFactory.addDefinition(concept, definitionId, recordDefinition);
                }
                else
                {
                    ontologyFactory.setDefinition(concept, recordDefinition);
                }
            }
            else
            {
                ontologyFactory.setDefinition(concept, recordDefinition);
            }
        }
    }

    public void normalizeRecordOntologyLinks(DataRecord record, List<T8OntologyLink> drInstanceLinks)
    {
        T8OntologyConcept concept;
        Set<String> orgIds;
        String osId;
        String recordOcId;

        // Get the existing record ontology details.
        concept = record.getOntology();
        osId = ontologyStructure.getID();
        recordOcId = record.getDataRequirementInstance().getRecordOcId();
        if (recordOcId == null) recordOcId = T8PrimaryOntologyDataType.DATA_RECORDS.getDataTypeID();

        // Create a set of all organization id's to which the DR Instance is linked.
        orgIds = new HashSet<String>();
        for (T8OntologyLink drInstanceLink : drInstanceLinks)
        {
            // Only use links to the Main Ontology Strucutre.
            if (drInstanceLink.getOntologyStructureID().equals(osId))
            {
                orgIds.add(drInstanceLink.getOrganizationID());
            }
        }

        // Make sure the record is linked to each of the organizations to which its DR Instance is linked.
        for (String orgId : orgIds)
        {
            T8OntologyLink requiredLink;

            requiredLink = new T8OntologyLink(orgId, osId, recordOcId, concept.getID());
            if (!concept.containsEquivalentLink(requiredLink))
            {
                concept.addOntologyLink(requiredLink);
            }
        }
    }

    public void resolveDataRecordConcepts(T8DataTransaction tx, List<DataRecord> dataRecords) throws Exception
    {
        for (DataRecord dataRecord : dataRecords)
        {
            resolveRecordValueConcepts(tx, dataRecord.getAllPropertyValues());
        }
    }

    private void resolveRecordValueConcepts(T8DataTransaction tx, List<RecordValue> recordValues) throws Exception
    {
        T8OrganizationOntologyFactory ontologyFactory;

        ontologyFactory = ontApi.getOntologyFactory();
        for (RecordValue recordValue : recordValues)
        {
            RequirementType requirementType;

            requirementType = recordValue.getRequirementType();
            switch (requirementType)
            {
                case CONTROLLED_CONCEPT:
                    ControlledConcept controlledConcept;
                    ControlledConceptRequirement controlledConceptRequirement;

                    // Only resolved the concept id if it is not already set.  Invalid concepts will be detected by access logic.
                    controlledConcept = (ControlledConcept)recordValue;
                    controlledConceptRequirement = (ControlledConceptRequirement)controlledConcept.getValueRequirement();
                    if (controlledConcept.getConceptId() == null)
                    {
                        String term;

                        term = controlledConcept.getTerm();
                        if (!Strings.isNullOrEmpty(term))
                        {
                            List<String> conceptIds;

                            term = term.trim();
                            conceptIds = ontApi.retrieveConceptIdsByTerm(controlledConcept.getOntologyClassId(), term, true);
                            if (conceptIds.size() > 0)
                            {
                                controlledConcept.setConceptId(conceptIds.get(0), true);
                                controlledConcept.setTerm(null); // Reset the term to null, as it is mutually exclusive to the concept id.
                            }
                            else if (controlledConceptRequirement.isAllowNewValue())
                            {
                                T8OntologyConcept newConcept;

                                // Create and insert the new concept.
                                T8Log.log("Inserting new concept resolved for record: " + recordValue.getParentDataRecord());
                                ontologyFactory.setConceptODTID(controlledConceptRequirement.getOntologyClassID());
                                newConcept = ontologyFactory.constructConcept(T8OntologyConceptType.VALUE);
                                ontologyFactory.addTerm(newConcept, term);
                                ontApi.generateAbbreviations(newConcept);
                                ontApi.insertConcept(newConcept, true, true, true, true, true);
                                controlledConcept.setConceptId(newConcept.getID(), true);
                                controlledConcept.setTerm(null);
                            }
                        }
                    }
            }
        }
    }
}
