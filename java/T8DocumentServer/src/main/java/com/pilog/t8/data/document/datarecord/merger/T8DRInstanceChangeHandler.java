package com.pilog.t8.data.document.datarecord.merger;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.api.T8DataRequirementApi;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.DataRequirementInstanceProvider;
import com.pilog.t8.data.document.OntologyProvider;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator.ConceptRenderType;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceChecker;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.merger.T8DRInstanceChangeHandler.DRInstanceMatch.MatchType;
import com.pilog.t8.data.document.datarecord.value.BooleanValue;
import com.pilog.t8.data.document.datarecord.value.DateTimeValue;
import com.pilog.t8.data.document.datarecord.value.DateValue;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarecord.value.IntegerValue;
import com.pilog.t8.data.document.datarecord.value.RealValue;
import com.pilog.t8.data.document.datarecord.value.StringValue;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DRInstanceChangeHandler
{
    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8DataRecordApi recApi;
    private final OntologyProvider ontologyProvider;
    private final TerminologyProvider terminologyProvider;
    private final DataRequirementInstanceProvider drInstanceProvider;
    private final T8DataRecordValueStringGenerator fftValueStringGenerator;

    public T8DRInstanceChangeHandler(T8DataTransaction tx)
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
        this.ontologyProvider = ((T8OntologyApi)tx.getApi(T8OntologyApi.API_IDENTIFIER)).getOntologyProvider();
        this.terminologyProvider = ((T8TerminologyApi)tx.getApi(T8TerminologyApi.API_IDENTIFIER)).getTerminologyProvider();
        this.drInstanceProvider = ((T8DataRequirementApi)tx.getApi(T8DataRequirementApi.API_IDENTIFIER)).getDrInstanceProvider();

        // Contruct the FFT value string generator.
        this.fftValueStringGenerator = new T8DataRecordValueStringGenerator();
        this.fftValueStringGenerator.setTerminologyProvider(terminologyProvider);
    }

    public DataRecord changeDrInstance(DataRecord dataFile, String targetRecordId, String drInstanceId) throws Exception
    {
        DataRecord targetRecord;

        // Now start with the target record.
        targetRecord = dataFile.findDataRecord(targetRecordId);
        if (targetRecord == null) throw new IllegalArgumentException("Target record not found: " + targetRecordId);
        else
        {
            RecordProperty parentProperty;
            DataRecord targetRecordParent;

            // Log the operation.
            T8Log.log("Changing Record " + targetRecord + " DR Instance to '" + drInstanceId + "'...");

            // Create the new document.
            targetRecordParent = targetRecord.getParentRecord();
            parentProperty = targetRecordParent.getPropertyContainingReference(targetRecordId);
            if (parentProperty != null)
            {
                DataRecord newTargetRecord;

                // Create the new record.
                newTargetRecord = createNewDataRecord(dataFile, parentProperty, targetRecord, drInstanceId, true);

                // Return the new root record.
                return dataFile;
            }
            else throw new RuntimeException("Referencing property for dr instance change target record not found: " + targetRecordId);
        }
    }

    /**
     * Creates a new data record based on the specified DR Instance.  The values
     * from the supplied old record are transferred to the new record as far as
     * possible.  Any properties from the old record that cannot be properly
     * mapped to the new record will be transferred to the new record's FFT.
     * @param recordID The ID of the old record from which data will be
     * transferred to the newly created record.
     * @param drInstanceID The DR Instance on which to base the new record.
     * @param recordCache The record cache to which the new record will be
     * added once created.
     * @param drInstanceCache The DR Instance cache to use for the creation
     * operation.
     * @return The ID of the newly created record.
     * @throws Exception
     */
    private DataRecord createNewDataRecord(DataRecord dataFile, RecordProperty parentProperty, DataRecord oldRecord, String drInstanceId, boolean useOldId) throws Exception
    {
        T8DataRecordConformanceChecker conformanceChecker;
        DataRecord parentRecord;
        DataRecord newRecord;

        // Create a new record using the new DR Instance.
        newRecord = recApi.createRecord(dataFile, parentProperty.getRecordID(), parentProperty.getPropertyID(), null, drInstanceId, sessionContext.getContentLanguageIdentifier(), false, false);

        // Remove the old record from the sub-record collection of its parent.
        parentRecord = parentProperty.getParentDataRecord();
        parentRecord.removeSubRecord(oldRecord);

        // Now set the newly created record's id to that of the old record it is replacing.
        if (useOldId)
        {
            String newIdNoLongerUsed;

            // Save the id used by the newly created record.
            newIdNoLongerUsed = newRecord.getId();

            // Set the newly created record's id to the old one.
            newRecord.setId(oldRecord.getId());

            // Remove the dangling new id that no longer points to a record.
            parentRecord.removeDocumentReference(newIdNoLongerUsed);
        }
        else
        {
            for (DataRecord record : dataFile.getDataRecords())
            {
                record.replaceDocumentReference(oldRecord.getId(), newRecord.getId());
            }
        }

        // Move the old FFT to the new record.
        newRecord.setFFT(oldRecord.getFFT());

        // Move all property values from the old record to the new one.
        for (RecordProperty oldProperty : oldRecord.getRecordProperties())
        {
            PropertyRequirement newPropertyRequirement;
            String propertyID;

            // Get the corresponding property requirement from the new DR Instance.
            propertyID = oldProperty.getPropertyRequirement().getConceptID();
            newPropertyRequirement = newRecord.getDataRequirement().getPropertyRequirement(propertyID);

            // If the old property still exists in the new DR Instance, we can attemp to add the data to it.
            if (newPropertyRequirement != null)
            {
                RecordProperty newProperty;

                // Create the new property and add it to the record.
                newProperty = createNewProperty(dataFile, newRecord, oldProperty, newPropertyRequirement);
                newRecord.setRecordProperty(newProperty);
            }
            else // No corresponding property found in the new DR Instance, so we need to add all of the sub-record data to this record's FFT.
            {
                String oldPropertyValueString;

                oldPropertyValueString = createPropertyValueString(terminologyProvider, oldProperty);
                if (!Strings.isNullOrEmpty(oldPropertyValueString))
                {
                    if (newRecord.hasFFT()) newRecord.appendFFT(" ");
                    newRecord.appendFFT(oldPropertyValueString);
                }
            }
        }

        // Move invalid data to FFT.
        conformanceChecker = new T8DataRecordConformanceChecker(fftValueStringGenerator);
        conformanceChecker.moveInvalidDataToFFT(newRecord);

        // Return the ID of the newly created record.
        return newRecord;
    }

    /**
     * Creates a new property based on the new property requirement supplied.
     * The value from the supplied old property is transferred to the newly
     * created property
     * @param recordCache The cache of data records to use during the
     * construction of the new property.
     * @param drInstanceCache The DR Instance cache to use while creating the
     * new property.
     * @param oldProperty The old property from which data will be transferred
     * to the new property.
     * @param newRequirement The new property requirement on which the newly
     * created property will be based.
     * @return A new property created from the supplied property requirement and
     * the value contained by the supplied old property.
     */
    private RecordProperty createNewProperty(DataRecord dataFile, DataRecord newRecord, RecordProperty oldProperty, PropertyRequirement newRequirement) throws Exception
    {
        RecordProperty newProperty;
        RecordValue oldValue;
        DataRecord oldRecord;

        // Create the new property using the new requirement.
        newProperty = new RecordProperty(newRequirement);

        // Move the old FFT to the new property.
        newProperty.setFFT(oldProperty.getFFT());

        // If the old property has a value, transfer it to the new property.
        oldValue = oldProperty.getRecordValue();
        oldRecord = oldProperty.getParentDataRecord();
        if (oldValue != null)
        {
            PropertyRequirement oldPropertyRequirement;
            ValueRequirement oldPropertyValueRequirement;
            ValueRequirement newPropertyValueRequirement;

            oldPropertyRequirement = oldProperty.getPropertyRequirement();
            oldPropertyValueRequirement = oldPropertyRequirement.getValueRequirement();
            newPropertyValueRequirement = newRequirement.getValueRequirement();

            // If the old and new property values are based on equivalent requirements, do a direct transfer of the value.
            if (newPropertyValueRequirement.isEquivalentRequirement(oldPropertyValueRequirement))
            {
                // The old value may be based on a required that is lower down in the requirement tree (not the first level value requirement directly below the property).
                if (oldPropertyValueRequirement == oldValue.getValueRequirement())
                {
                    // The value is based on the first-level value requirement, so do a straight copy.
                    newProperty.setRecordValue(oldValue.copy(newPropertyValueRequirement));
                }
                else
                {
                    ValueRequirement newValueRequirement;

                    // The value is not based on the first-level value requirement, so find the equivalent requirement and use it for the value copy.
                    newValueRequirement = newPropertyValueRequirement.findFirstEquivalentDescendant(oldValue.getValueRequirement());
                    newProperty.setRecordValue(oldValue.copy(newValueRequirement));
                }

                // Make sure to add sub-records pointed to by copied references.
                for (String subRecordID : oldRecord.getSubRecordIDsReferencedFromProperty(oldProperty.getPropertyID()))
                {
                    DataRecord subRecord;

                    // Removing the sub-record referrers will break the old
                    // record, so the old record should no longer be used.
                    subRecord = oldRecord.getSubRecord(subRecordID);
                    subRecord.removeFromReferrerRecords();
                    newRecord.addSubRecord(subRecord);
                }
            }
            else // The requirements differ, so we have to do some kind of mapping.
            {
                RecordValue newValue;

                // Try to create a new value from the old one.
                newValue = createNewValue(dataFile, newRecord, newProperty, oldValue, newPropertyValueRequirement);
                if (newValue != null) // We were able to create a new value.
                {
                    // Set the new value on the property.
                    newProperty.setRecordValue(newValue);

                    // Transfer all value FFT to the property FFT.
                    if (newValue.hasFFT())
                    {
                        if (newProperty.hasFFT()) newProperty.appendFFT(" ");
                        newProperty.appendFFT(newValue.getFFT());
                    }
                }
                else // We were not able to create a new value so we need to create a new FFT value instead.
                {
                    String valueString;

                    valueString = createValueString(terminologyProvider, oldValue);
                    if (valueString != null)
                    {
                        if (newProperty.hasFFT()) newProperty.appendFFT(" ");
                        newProperty.appendFFT(valueString);
                    }
                }
            }
        }

        // Return the new property.
        return newProperty;
    }

    /**
     * This method attempts to create a new record value based on the supplied
     * value requirement by converting the data from the supplied old value.  If
     * no property conversion can be performed (if any data will be lost during
     * conversion), null is returned.
     * @param recordProvider The record cache to use while creating the new value.
     * @param drInstanceProvider The DR Instance cache to use while creating the
     * new value.
     * @param oldValue The old value form which data will be transferred.
     * @param newRequirement The requirement on which the new value will be
     * based.
     * @return A new record value based on the supplied requirement and
     * containing all data from the supplied old record value, converted to the
     * new format.  Returns null if any data is lost during conversion, which
     * means the old value is simply not compatible with the new requirement.
     */
    private RecordValue createNewValue(DataRecord dataFile, DataRecord newRecord, RecordProperty newProperty, RecordValue oldValue, ValueRequirement newRequirement) throws Exception
    {
        ValueRequirement oldRequirement;
        RecordValue newValue;
        RequirementType oldRequirementType;
        RequirementType newRequirementType;

        // Get the old and new requirement type.
        oldRequirement = oldValue.getValueRequirement();
        oldRequirementType = oldRequirement.getRequirementType();
        newRequirementType = newRequirement.getRequirementType();

        // Create a new value if the old and new types are compatible.
        newValue = null;
        if (RequirementType.DOCUMENT_REFERENCE.equals(oldRequirementType))
        {
            if (RequirementType.DOCUMENT_REFERENCE.equals(newRequirementType))
            {
                newValue = createDocumentReferenceValue(dataFile, newRecord, newProperty, (DocumentReferenceList)oldValue, newRequirement);
            }
        }
        else if (RequirementType.INTEGER_TYPE.equals(oldRequirementType))
        {
            if (RequirementType.INTEGER_TYPE.equals(newRequirementType))
            {
                newValue = new IntegerValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
            else if (RequirementType.STRING_TYPE.equals(newRequirementType))
            {
                newValue = new StringValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
        }
        else if (RequirementType.REAL_TYPE.equals(oldRequirementType))
        {
            if (RequirementType.REAL_TYPE.equals(newRequirementType))
            {
                newValue = new RealValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
            else if (RequirementType.STRING_TYPE.equals(newRequirementType))
            {
                newValue = new StringValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
        }
        else if (RequirementType.BOOLEAN_TYPE.equals(oldRequirementType))
        {
            if (RequirementType.STRING_TYPE.equals(newRequirementType))
            {
                newValue = new BooleanValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
        }
        else if (RequirementType.DATE_TYPE.equals(oldRequirementType))
        {
            if (RequirementType.DATE_TYPE.equals(newRequirementType))
            {
                newValue = new DateValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
            else if (RequirementType.STRING_TYPE.equals(newRequirementType))
            {
                newValue = new StringValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
        }
        else if (RequirementType.DATE_TIME_TYPE.equals(oldRequirementType))
        {
            if (RequirementType.DATE_TIME_TYPE.equals(newRequirementType))
            {
                newValue = new DateTimeValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
            else if (RequirementType.STRING_TYPE.equals(newRequirementType))
            {
                newValue = new StringValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
        }
        else if (RequirementType.YEAR_TYPE.equals(oldRequirementType))
        {
            if (RequirementType.STRING_TYPE.equals(newRequirementType))
            {
                newValue = new StringValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
        }
        else if (RequirementType.STRING_TYPE.equals(oldRequirementType))
        {
            String oldValueString;

            oldValueString = oldValue.getValue();
            if (RequirementType.STRING_TYPE.equals(newRequirementType))
            {
                newValue = new StringValue(newRequirement);
                newValue.setValue(oldValue.getValue());
            }
            else if (RequirementType.INTEGER_TYPE.equals(newRequirementType))
            {
                if (Strings.isInteger(oldValueString))
                {
                    newValue = new IntegerValue(newRequirement);
                    newValue.setValue(oldValue.getValue());
                }
            }
        }

        // If we have constructed a new value based on type, check that it still conforms to all restrictions.
        if (newValue != null)
        {
            T8DataRecordConformanceChecker conformanceChecker;

            conformanceChecker = new T8DataRecordConformanceChecker(new T8DataRecordValueStringGenerator());
            if (conformanceChecker.validateRecordValue(newValue).isEmpty()) return newValue; // No conformance errors.
            else return null; // Conformance errors, so return null.
        }
        else return null;
    }

    private RecordValue createDocumentReferenceValue(DataRecord dataFile, DataRecord newRecord, RecordProperty newProperty, DocumentReferenceList oldReferenceList, ValueRequirement newRequirement) throws Exception
    {
        DocumentReferenceList newReferenceList;

        // Create the new document reference value to which all of the mapped references will be added.
        newReferenceList = new DocumentReferenceList(newRequirement);

        // Loop through the old values and for each one, create the best possible match using the new requirements.
        for (String oldRecordID : oldReferenceList.getReferenceIds())
        {
            DRInstanceMatch bestRequirementMatch;
            DataRecord oldRecord;

            // Find the best requirement match.
            oldRecord = oldReferenceList.getParentDataRecord().findDataRecord(oldRecordID);
            bestRequirementMatch = findBestReferenceRequirementMatch(oldRecord, newReferenceList.getOntologyClassID());

            // If we found a matching requirement, create the new reference.
            if (bestRequirementMatch != null)
            {
                DataRequirementInstance newDrInstance;
                String newDRInstanceID;
                String newDRID;

                // If the record that was referenced by the old value is based on the same DR Instance as the old record, we can use it again.
                newDRInstanceID = bestRequirementMatch.getDrInstance().getConceptID();
                newDrInstance = bestRequirementMatch.getDrInstance();
                newDRID = newDrInstance.getDataRequirement().getConceptID();

                // Direct DR Instance match, so we can use the old record without changes.
                if (oldRecord.getDataRequirementInstance().getConceptID().equals(newDRInstanceID))
                {
                    oldRecord.removeFromReferrerRecords();
                    newReferenceList.addReference(oldRecordID); // Use the old record and no further steps required.
                    newRecord.addSubRecord(oldRecord);
                }
                else if (oldRecord.getDataRequirement().getConceptID().equals(newDRID)) // Match on DR, but not DR Instance.
                {
                    // If the old DR Instance was dependent, we can use the same record we just need to change its DR Instance.
                    if (!oldRecord.getDataRequirementInstance().isIndependent())
                    {
                        // We need to change the instance of the old record, since we only matched on DR.
                        oldRecord.removeFromReferrerRecords();
                        oldRecord.getDataRequirementInstance().setConceptID(newDRInstanceID);
                        newReferenceList.addReference(oldRecordID); // Use the old record and no further steps required.
                        newRecord.addSubRecord(oldRecord);
                    }
                    else // Ok we need to create a new record.
                    {
                        DataRecord newSubRecord;

                        // Create a new record and add its ID to the reference value.
                        newSubRecord = createNewDataRecord(dataFile, newProperty, oldRecord, newDrInstance.getConceptID(), false);
                        newReferenceList.addReference(newSubRecord.getID());
                        newSubRecord.addSubRecord(newSubRecord);
                    }
                }
                else // No match on DR or DR Instance, so we need to create a new record.
                {
                    DataRecord newSubRecord;

                    // Create a new record and add its ID to the reference value.
                    newSubRecord = createNewDataRecord(dataFile, newProperty, oldRecord, newDrInstance.getConceptID(), false);
                    newReferenceList.addReference(newSubRecord.getID());
                    newSubRecord.addSubRecord(newSubRecord);
                }
            }
            else // We couldn't find any data requirement that matches, so we need to generate a value string that will be appended to the property FFT.
            {
                String valueString;

                valueString = createValueString(terminologyProvider, oldRecord);
                if (valueString != null)
                {
                    if (newReferenceList.hasFFT()) newReferenceList.appendFFT(" ");
                    newReferenceList.appendFFT(valueString);
                }
            }
        }

        // Return the new value that now contains all of the newly mapped references.
        return newReferenceList;
    }

    private DRInstanceMatch findBestReferenceRequirementMatch(DataRecord oldRecord, String ocID)
    {
        DataRequirementInstance oldDRInstance;
        T8DataIterator<String> drInstanceIDIterator;
        DRInstanceMatch match;

        // Get the old DR Instance and its details.
        oldDRInstance = oldRecord.getDataRequirementInstance();

        // Get an iterator over th DR Instances belonging to the ontology class.
        drInstanceIDIterator = ontologyProvider.getConceptIDIterator(ocID, true);

        // Find the best match for the group and compare it to the best match so far.
        match = findBestDataRequirementInstanceMatch(oldDRInstance, drInstanceIDIterator);

        // Make sure to close the iterator.
        drInstanceIDIterator.close();

        // Return the best reference match.
        return match;
    }

    private DRInstanceMatch findBestDataRequirementInstanceMatch(DataRequirementInstance oldDRInstance, Iterator<String> newDRInstanceIDIterator)
    {
        DataRequirementInstance bestMatchDRInstance;
        DataRequirementInstance drInstanceMatchedOnDR;
        int bestMatchPropertyCount;

        // Iterate over all of the available DR Instances and find the best match.
        bestMatchDRInstance = null;
        drInstanceMatchedOnDR = null;
        bestMatchPropertyCount = 0;
        while (newDRInstanceIDIterator.hasNext())
        {
            DataRequirementInstance newDRInstance;
            String newDRInstanceID;

            // Get the DR Instance referenced by the new requirement.
            newDRInstanceID = newDRInstanceIDIterator.next();
            newDRInstance = drInstanceProvider.getDataRequirementInstance(newDRInstanceID, null, false, false);

            // If the old and new DR Instances are the same it is a direct match.
            if (Objects.equals(oldDRInstance.getConceptID(), newDRInstanceID))
            {
                // No need to continue the loop.
                return new DRInstanceMatch(newDRInstance, MatchType.DIRECT, -1);
            }
            else if (oldDRInstance.getDataRequirement().getConceptID().equals(newDRInstance.getDataRequirement().getConceptID()))
            {
                // If the old and new DR Instance are both based on the same DR the instance can be used if no direct match is found.
                drInstanceMatchedOnDR = newDRInstance;
            }
            else
            {
                List<String> oldPropertyIDList;
                List<String> newPropertyIDList;
                int matchingPropertyCount;

                oldPropertyIDList = oldDRInstance.getDataRequirement().getPropertyRequirementIDList();
                newPropertyIDList = newDRInstance.getDataRequirement().getPropertyRequirementIDList();
                matchingPropertyCount = CollectionUtilities.countIntersection(oldPropertyIDList, newPropertyIDList);
                if (matchingPropertyCount > bestMatchPropertyCount)
                {
                    bestMatchDRInstance = newDRInstance;
                    bestMatchPropertyCount = matchingPropertyCount;
                }
            }
        }

        // Return the best match.
        if (drInstanceMatchedOnDR != null) return new DRInstanceMatch(drInstanceMatchedOnDR, MatchType.DR, -1);
        else if (bestMatchDRInstance != null) return new DRInstanceMatch(bestMatchDRInstance, MatchType.PROPERTY, bestMatchPropertyCount);
        else return null;
    }

    /**
     * This method generates a value string for a record.
     * @param oldRecord
     * @return
     */
    private String createValueString(TerminologyProvider terminologyProvider, DataRecord oldRecord)
    {
        T8DataRecordValueStringGenerator generator;
        StringBuilder valueStringBuffer;

        generator = new T8DataRecordValueStringGenerator();
        generator.setIncludeSubDocumentValueStrings(true);
        generator.setTerminologyProvider(terminologyProvider);
        generator.setIncludeRecordFFT(true);
        generator.setIncludeSectionConcepts(true);
        generator.setSectionConceptRenderType(ConceptRenderType.TERM);
        generator.setIncludePropertyFFT(true);
        generator.setIncludePropertyConcepts(true);
        generator.setPropertyConceptRenderType(ConceptRenderType.TERM);
        generator.setValueConceptRenderType(ConceptRenderType.TERM);
        generator.setLanguageID(sessionContext.getContentLanguageIdentifier());
        valueStringBuffer = generator.generateValueString(oldRecord);
        return valueStringBuffer != null ? valueStringBuffer.toString() : null;
    }

    /**
     * This method generates a value string for a property.  If the property
     * references a document, the entire referenced document's value string will
     * be included in this value string.  The supplied property's FFT will also
     * be added to the value string returned by this method.
     * @param oldProperty
     * @return
     */
    private String createPropertyValueString(TerminologyProvider terminologyProvider, RecordProperty oldProperty)
    {
        T8DataRecordValueStringGenerator generator;
        StringBuilder valueStringBuffer;

        generator = new T8DataRecordValueStringGenerator();
        generator.setIncludeSubDocumentValueStrings(true);
        generator.setTerminologyProvider(terminologyProvider);
        generator.setIncludeRecordFFT(true);
        generator.setIncludeSectionConcepts(true);
        generator.setSectionConceptRenderType(ConceptRenderType.TERM);
        generator.setIncludePropertyFFT(true);
        generator.setIncludePropertyConcepts(true);
        generator.setPropertyConceptRenderType(ConceptRenderType.TERM);
        generator.setValueConceptRenderType(ConceptRenderType.TERM);
        generator.setLanguageID(sessionContext.getContentLanguageIdentifier());
        valueStringBuffer = generator.generateValueString(oldProperty);
        return valueStringBuffer != null ? valueStringBuffer.toString() : null;
    }

    /**
     * This method generates a value string for a value.  If the property
     * references a document, the entire referenced document's value string will
     * be included in this value string.  The supplied property's FFT will also
     * be added to the value string returned by this method.
     * @param oldValue
     * @return
     */
    private String createValueString(TerminologyProvider terminologyProvider, RecordValue oldValue)
    {
        T8DataRecordValueStringGenerator generator;
        StringBuilder valueString;

        generator = new T8DataRecordValueStringGenerator();
        generator.setIncludeSubDocumentValueStrings(true);
        generator.setTerminologyProvider(terminologyProvider);
        generator.setIncludeRecordFFT(true);
        generator.setIncludeSectionConcepts(true);
        generator.setSectionConceptRenderType(ConceptRenderType.TERM);
        generator.setIncludePropertyFFT(true);
        generator.setIncludePropertyConcepts(true);
        generator.setPropertyConceptRenderType(ConceptRenderType.TERM);
        generator.setValueConceptRenderType(ConceptRenderType.TERM);
        generator.setLanguageID(sessionContext.getContentLanguageIdentifier());
        valueString = generator.generateValueString(oldValue);
        return valueString != null ? valueString.toString() : null;
    }

    public static class ReferenceRequirementMatch
    {
        private ValueRequirement requirement;
        private DRInstanceMatch drInstanceMatch;

        public ReferenceRequirementMatch(ValueRequirement requirement, DRInstanceMatch drInstanceMatch)
        {
            this.requirement = requirement;
            this.drInstanceMatch = drInstanceMatch;
        }

        public boolean isBetterMatch(ReferenceRequirementMatch match)
        {
            if (match == null) return false;
            else return drInstanceMatch.isBetterMatch(match.getDrInstanceMatch());
        }

        public ValueRequirement getRequirement()
        {
            return requirement;
        }

        public void setRequirement(ValueRequirement requirement)
        {
            this.requirement = requirement;
        }

        public DRInstanceMatch getDrInstanceMatch()
        {
            return drInstanceMatch;
        }

        public void setDrInstanceMatch(DRInstanceMatch drInstanceMatch)
        {
            this.drInstanceMatch = drInstanceMatch;
        }
    }

    public static class DRInstanceMatch
    {
        private DataRequirementInstance drInstance;
        private MatchType matchType;
        private int propertyMatchCount;

        public enum MatchType {DIRECT, DR, PROPERTY};

        public DRInstanceMatch(DataRequirementInstance drInstance, MatchType matchType, int propertyMatchCount)
        {
            this.drInstance = drInstance;
            this.matchType = matchType;
            this.propertyMatchCount = propertyMatchCount;
        }

        public boolean isBetterMatch(DRInstanceMatch match)
        {
            if (match == null) return false;
            else if (match.getMatchType() == MatchType.DIRECT)
            {
                // It's a better match if this match type is on properties or DR.
                return ((this.matchType == MatchType.DR) || (this.matchType == MatchType.PROPERTY));
            }
            else if (match.getMatchType() == MatchType.DR)
            {
                // It's only a better match if this match type is on properties only.
                return this.matchType == MatchType.PROPERTY;
            }
            else if (match.getMatchType() == MatchType.PROPERTY)
            {
                // It's only a better match if the number of matched properties is higher that this match.
                return match.getPropertyMatchCount() > propertyMatchCount;
            }
            else return false;
        }

        public DataRequirementInstance getDrInstance()
        {
            return drInstance;
        }

        public void setDrInstance(DataRequirementInstance drInstance)
        {
            this.drInstance = drInstance;
        }

        public MatchType getMatchType()
        {
            return matchType;
        }

        public void setMatchType(MatchType matchType)
        {
            this.matchType = matchType;
        }

        public int getPropertyMatchCount()
        {
            return propertyMatchCount;
        }

        public void setPropertyMatchCount(int propertyMatchCount)
        {
            this.propertyMatchCount = propertyMatchCount;
        }
    }
}
