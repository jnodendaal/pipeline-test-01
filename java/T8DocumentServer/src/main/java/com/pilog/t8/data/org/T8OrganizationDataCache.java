package com.pilog.t8.data.org;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataCache;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.definition.data.ontology.T8OntologyStructureCacheDefinition;
import com.pilog.t8.definition.data.org.T8OrganizationDataCacheDefinition;
import com.pilog.t8.definition.data.org.T8OrganizationStructureCacheDefinition;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationDataCache implements T8DataCache
{
    private final T8ServerContext serverContext;
    private final T8OrganizationDataCacheDefinition definition;
    private final Map<String, Set<String>> organizationLanguageIDSets;
    private final Map<String, Set<String>> organizationGlobalLanguageIDSets;
    private final Map<String, List<T8OntologyLink>> organizationDRInstanceLinks;
    private boolean initialized;
    private boolean enabled;

    public T8OrganizationDataCache(T8Context context, T8OrganizationDataCacheDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.definition = definition;
        this.organizationLanguageIDSets = new HashMap<>();
        this.organizationGlobalLanguageIDSets = new HashMap<>();
        this.organizationDRInstanceLinks = new HashMap<>();
        this.initialized = false;
        this.enabled = definition.isEnabled();
    }

    @Override
    public boolean isInitialized()
    {
        return initialized;
    }

    @Override
    public boolean cachesData(String dataIdentifier)
    {
        if (enabled)
        {
            if (T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_LANGUAGE_ID_SET.equals(dataIdentifier)) return true;
            else if (T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_GLOBAL_LANGUAGE_ID_SET.equals(dataIdentifier)) return true;
            else if (T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_DR_INSTANCE_LINKS.equals(dataIdentifier)) return true;
            else return false;
        }
        else return false;
    }

    @Override
    public Object recacheData(String dataIdentifier, Map<String, Object> dataParameters) throws Exception
    {
        // Clear the specified data cache.
        if (T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_LANGUAGE_ID_SET.equals(dataIdentifier))
        {
            organizationLanguageIDSets.clear();
        }
        else if (T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_GLOBAL_LANGUAGE_ID_SET.equals(dataIdentifier))
        {
            organizationGlobalLanguageIDSets.clear();
        }
        else if (T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_GLOBAL_LANGUAGE_ID_SET.equals(dataIdentifier))
        {
            organizationDRInstanceLinks.clear();
        }

        // Now return the data (it will be recached during retrieval).
        return getCachedData(dataIdentifier, dataParameters);
    }

    @Override
    public Object getCachedData(String dataIdentifier, Map<String, Object> dataParameters) throws Exception
    {
        if (T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_LANGUAGE_ID_SET.equals(dataIdentifier))
        {
            if (dataParameters != null)
            {
                String orgID;

                orgID = (String)dataParameters.get(T8OrganizationDataCacheDefinition.DATA_PARAMETER_ORG_ID);
                if (orgID != null)
                {
                    Set<String> languageIDSet;

                    // Check the internal cache and if the organization data is present return it, else cache it.
                    languageIDSet = organizationLanguageIDSets.get(orgID);
                    if (languageIDSet != null)
                    {
                        return languageIDSet;
                    }
                    else // Not found, so cache the required data.
                    {
                        T8Log.log("Caching Language ID Set for Organization '" + orgID + "'...");
                        languageIDSet = retrieveOrganizationLanguageIDSet(orgID);
                        organizationLanguageIDSets.put(orgID, languageIDSet);
                        return languageIDSet;
                    }
                }
                else throw new Exception("Required data parameters not specified.");
            }
            else throw new Exception("Required data parameters not specified.");
        }
        else if (T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_GLOBAL_LANGUAGE_ID_SET.equals(dataIdentifier))
        {
            if (dataParameters != null)
            {
                String orgID;

                orgID = (String)dataParameters.get(T8OrganizationDataCacheDefinition.DATA_PARAMETER_ORG_ID);
                if (orgID != null)
                {
                    Set<String> languageIDSet;

                    // Check the internal cache and if the organization data is present return it, else cache it.
                    languageIDSet = organizationGlobalLanguageIDSets.get(orgID);
                    if (languageIDSet != null)
                    {
                        return languageIDSet;
                    }
                    else // Not found, so cache the required data.
                    {
                        T8Log.log("Caching Global Language ID Set for Organization '" + orgID + "'...");
                        languageIDSet = retrieveOrganizationGlobalLanguageIDSet(orgID);
                        organizationGlobalLanguageIDSets.put(orgID, languageIDSet);
                        return languageIDSet;
                    }
                }
                else throw new Exception("Required data parameters not specified.");
            }
            else throw new Exception("Required data parameters not specified.");
        }
        else if (T8OrganizationDataCacheDefinition.DATA_IDENTIFIER_ORGANIZATION_DR_INSTANCE_LINKS.equals(dataIdentifier))
        {
            if (dataParameters != null)
            {
                String drInstanceID;
                String orgID;

                drInstanceID = (String)dataParameters.get(T8OrganizationDataCacheDefinition.DATA_PARAMETER_DR_INSTANCE_ID);
                orgID = (String)dataParameters.get(T8OrganizationDataCacheDefinition.DATA_PARAMETER_ORG_ID);
                if (drInstanceID != null)
                {
                    List<T8OntologyLink> conceptLinks;

                    // Check the internal cache and if the required data is present return it, else cache it.
                    conceptLinks = organizationDRInstanceLinks.get(drInstanceID);
                    if (conceptLinks != null)
                    {
                        return conceptLinks;
                    }
                    else // Not found, so cache the required data.
                    {
                        T8Log.log("Caching Organization DR Instance links for Organization '" + orgID + "' and DR Instance '" + drInstanceID + "'...");
                        conceptLinks = retrieveOrganizationConceptLinks(orgID, drInstanceID);
                        organizationDRInstanceLinks.put(drInstanceID, conceptLinks);
                        return conceptLinks;
                    }
                }
                else throw new Exception("Required data parameters not specified.");
            }
            else throw new Exception("Required data parameters not specified.");
        }
        else throw new Exception("Invalid Data request: " + dataIdentifier);
    }

    @Override
    public void init() throws Exception
    {
        if (enabled)
        {
            initialized = true;
        }
    }

    @Override
    public void clear()
    {
        organizationLanguageIDSets.clear();
    }

    @Override
    public void destroy()
    {
        clear();
        initialized = false;
    }

    private List<T8OntologyLink> retrieveOrganizationConceptLinks(String rootOrgID, String conceptID) throws Exception
    {
        T8OrganizationDataHandler dataHandler;
        T8OrganizationStructure organizationStructure;
        T8DataTransaction tx;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Get the list of ORG_ID's that we can use in the filter.
        organizationStructure = getOrganizationStructure(rootOrgID);

        // Retrieve the data and return the result.
        dataHandler = new T8OrganizationDataHandler(serverContext);
        return dataHandler.retrieveOntologyLinks(tx, rootOrgID, organizationStructure.getOntologyStructureID(), conceptID);
    }

    private Set<String> retrieveOrganizationLanguageIDSet(String orgID) throws Exception
    {
        T8OrganizationDataHandler dataHandler;
        T8OrganizationStructure organizationStructure;
        T8OntologyStructure ontologyStructure;
        T8DataTransaction tx;
        List<String> orgIDList;
        List<String> odtIDList;
        Set<String> languageIDSet;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Get the list of ORG_ID's that we can use in the filter.
        organizationStructure = getOrganizationStructure(orgID);
        orgIDList = organizationStructure.getOrganizationPathIDList(orgID);

        // Get the list of ODT_ID's that we can use in the filter.
        ontologyStructure = getOntologyStructure(organizationStructure.getOntologyStructureID());
        odtIDList = ontologyStructure.getDescendantClassIDList(T8PrimaryOntologyDataType.LANGUAGES.getDataTypeID());
        odtIDList.add(T8PrimaryOntologyDataType.LANGUAGES.getDataTypeID()); // Add the ODT_ID to the list of its descendants.

        // Retrieve the data and return the result.
        dataHandler = new T8OrganizationDataHandler(serverContext);
        languageIDSet = new HashSet<>(dataHandler.retrieveConceptIDSet(tx, orgIDList, odtIDList));
        return languageIDSet;
    }

    private Set<String> retrieveOrganizationGlobalLanguageIDSet(String orgID) throws Exception
    {
        T8OrganizationDataHandler dataHandler;
        T8OrganizationStructure organizationStructure;
        T8OntologyStructure ontologyStructure;
        T8DataTransaction tx;
        List<String> orgIDList;
        List<String> odtIDList;
        Set<String> languageIDSet;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Get the list of ORG_ID's that we can use in the filter.
        organizationStructure = getOrganizationStructure(orgID);
        orgIDList = organizationStructure.getOrganizationIDList();

        // Get the list of ODT_ID's that we can use in the filter.
        ontologyStructure = getOntologyStructure(organizationStructure.getOntologyStructureID());
        odtIDList = ontologyStructure.getDescendantClassIDList(T8PrimaryOntologyDataType.LANGUAGES.getDataTypeID());
        odtIDList.add(T8PrimaryOntologyDataType.LANGUAGES.getDataTypeID()); // Add the ODT_ID to the list of its descendants.

        // Retrieve the data and return the result.
        dataHandler = new T8OrganizationDataHandler(serverContext);
        languageIDSet = new HashSet<>(dataHandler.retrieveConceptIDSet(tx, orgIDList, odtIDList));
        return languageIDSet;
    }

    private T8OrganizationStructure getOrganizationStructure(String rootOrgId) throws Exception
    {
        T8DataManager dataManager;

        dataManager = serverContext.getDataManager();
        return (T8OrganizationStructure)dataManager.getCachedData(T8OrganizationStructureCacheDefinition.DATA_IDENTIFIER, HashMaps.newHashMap(T8OrganizationStructureCacheDefinition.DATA_PARAMETER_ORG_ID, rootOrgId));
    }

    private T8OntologyStructure getOntologyStructure(String osId) throws Exception
    {
        T8DataManager dataManager;

        dataManager = serverContext.getDataManager();
        return (T8OntologyStructure)dataManager.getCachedData(T8OntologyStructureCacheDefinition.DATA_IDENTIFIER, HashMaps.newHashMap(T8OntologyStructureCacheDefinition.DATA_PARAMETER_ODS_ID, osId));
    }
}
