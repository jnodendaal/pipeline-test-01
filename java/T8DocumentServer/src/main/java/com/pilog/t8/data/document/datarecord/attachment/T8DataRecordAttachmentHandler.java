package com.pilog.t8.data.document.datarecord.attachment;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.source.datarecord.T8AttachmentHandler;
import com.pilog.t8.definition.data.source.datarecord.T8AttachmentStorageDefinition;
import com.pilog.t8.definition.data.source.datarecord.T8DataRecordAttachmentHandlerDefinition;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordAttachmentHandler extends T8AbstractAttachmentHandler implements T8AttachmentHandler
{
    private final T8DataRecordAttachmentHandlerDefinition definition;

    public T8DataRecordAttachmentHandler(T8DataRecordAttachmentHandlerDefinition definition, T8DataTransaction tx) throws Exception
    {
        super(tx.getContext(), tx.getDataConnection(definition.getConnectionIdentifier()), getFileManager(tx), definition.getAttachmentTableName());
        this.definition = definition;
    }

    private static T8FileManager getFileManager(T8DataTransaction tx)
    {
        return tx.getContext().getServerContext().getFileManager();
    }

    private T8AttachmentStorageDefinition getStorageDefinition(String drInstanceID)
    {
        T8AttachmentStorageDefinition wildcardDefinition;

        wildcardDefinition = null;
        for (T8AttachmentStorageDefinition storageDefinition : definition.getAttachmentStorageDefinitions())
        {
            List<String> drInstanceIDList;

            drInstanceIDList = storageDefinition.getDataRequirementInstanceIDList();
            if ((drInstanceIDList == null) || (drInstanceIDList.isEmpty()))
            {
                wildcardDefinition = storageDefinition;
            }
            else if (drInstanceIDList.contains(drInstanceID))
            {
                return storageDefinition;
            }
        }

        // No specific storage definition found, so return the wildcard.
        return wildcardDefinition;
    }

    @Override
    public String getFileContextID(String drInstanceID) throws Exception
    {
        T8AttachmentStorageDefinition storageDefinition;

        storageDefinition = getStorageDefinition(drInstanceID);
        if (storageDefinition != null)
        {
            return storageDefinition.getFileContextID();
        }
        else throw new Exception("No attachment storage definition found for DR Instance: " + drInstanceID);
    }

    @Override
    public boolean isDatabaseStorageEnabled(String drInstanceID) throws Exception
    {
        T8AttachmentStorageDefinition storageDefinition;

        storageDefinition = getStorageDefinition(drInstanceID);
        if (storageDefinition != null)
        {
            return storageDefinition.isDatabaseStorage();
        }
        else throw new Exception("No attachment storage definition found for DR Instance: " + drInstanceID);
    }
}
