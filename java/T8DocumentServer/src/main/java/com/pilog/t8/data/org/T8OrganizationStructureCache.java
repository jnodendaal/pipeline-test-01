package com.pilog.t8.data.org;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataCache;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.data.org.T8OrganizationStructureCacheDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8OrganizationStructureCache implements T8DataCache
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8OrganizationStructureCache.class);

    private final T8ServerContext serverContext;
    private final T8OrganizationStructureCacheDefinition definition;
    private final Map<String, T8OrganizationStructure> organizationStructures;
    private boolean initialized;
    private boolean enabled;

    public T8OrganizationStructureCache(T8Context context, T8OrganizationStructureCacheDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.definition = definition;
        this.organizationStructures = new HashMap<>();
        this.initialized = false;
        this.enabled = definition.isEnabled();
    }

    @Override
    public boolean isInitialized()
    {
        return initialized;
    }

    @Override
    public boolean cachesData(String dataId)
    {
        if (enabled)
        {
            return T8OrganizationStructureCacheDefinition.DATA_IDENTIFIER.equals(dataId);
        }
        else return false;
    }

    @Override
    public Object recacheData(String dataIdentifier, Map<String, Object> dataParameters) throws Exception
    {
        recacheAllOrganizationStructures();
        return getCachedData(dataIdentifier, dataParameters);
    }

    @Override
    public Object getCachedData(String dataIdentifier, Map<String, Object> dataParameters) throws Exception
    {
        if (dataParameters != null)
        {
            String organizationID;

            organizationID = (String)dataParameters.get(T8OrganizationStructureCacheDefinition.DATA_PARAMETER_ORG_ID);
            if (organizationID != null)
            {
                T8OrganizationStructure organizationStructure;

                // Check the internal cache and if the organization structure is present return it, else cache it.
                organizationStructure = findOrganizationStructure(organizationID);
                if (organizationStructure != null)
                {
                    return organizationStructure;
                }
                else // Not found, so cache the required structure.
                {
                    return cacheOrganizationStructure(organizationID);
                }
            }
            else throw new Exception("Organization ID required.");
        }
        else throw new Exception("Required data parameters not specified.");
    }

    @Override
    public void init() throws Exception
    {
        if (enabled)
        {
            recacheAllOrganizationStructures();
            initialized = true;
        }
    }

    @Override
    public void clear()
    {
        organizationStructures.clear();
    }

    @Override
    public void destroy()
    {
        clear();
        initialized = false;
    }

    private T8OrganizationStructure cacheOrganizationStructure(String organizationID) throws Exception
    {
        throw new Exception("Caching of individual Organization Structures not yet implemented.  Cannot find requested organization in existing cache: " + organizationID);
    }

    private void recacheAllOrganizationStructures() throws Exception
    {
        T8OrganizationDataHandler dataHandler;
        T8DataTransaction tx;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Clear the collection of organization structures.
        organizationStructures.clear();

        // Retrieve the Organization Structures.
        LOGGER.log("Caching all Organization Structures...");
        dataHandler = new T8OrganizationDataHandler(serverContext);
        organizationStructures.putAll(dataHandler.retrieveOrganizationStructures(tx, null));

        // Log the result.
        for (String orgID : organizationStructures.keySet())
        {
            T8OrganizationStructure dataStructure;

            dataStructure = organizationStructures.get(orgID);
            LOGGER.log("Organization Structure cached: " + orgID + " - " + dataStructure.getSize() + " nodes.");
        }
    }

    /**
     * Finds and returns the organization structure that contains the specified
     * organization ID.
     * @param organizationID The organization ID to look for.
     * @return The organization structure that contains the specified
     * organization ID.
     */
    private T8OrganizationStructure findOrganizationStructure(String organizationID)
    {
        for (T8OrganizationStructure organizationStructure : organizationStructures.values())
        {
            if (organizationStructure.containsNode(organizationID)) return organizationStructure;
        }

        return null;
    }
}
