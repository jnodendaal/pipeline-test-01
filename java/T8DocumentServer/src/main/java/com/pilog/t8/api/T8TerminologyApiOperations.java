package com.pilog.t8.api;

import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8TerminologyApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8TerminologyApiOperations
{
    public static class ApiRetrieveTerminologyList extends T8DefaultServerOperation
    {
        public ApiRetrieveTerminologyList(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8ConceptTerminology>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8ConceptTerminology> terminologyList;
            List<String> conceptIdList;
            List<String> languageIdList;
            T8TerminologyApi trmApi;

            // Get the operation parameters we need.
            conceptIdList = (List<String>)operationParameters.get(PARAMETER_CONCEPT_ID_LIST);
            languageIdList = (List<String>)operationParameters.get(PARAMETER_LANGUAGE_ID_LIST);

            // Perform the API operation.
            trmApi = (T8TerminologyApi)tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            terminologyList = trmApi.retrieveTerminology(languageIdList, conceptIdList);
            return HashMaps.createSingular(PARAMETER_TERMINOLOGY_LIST, terminologyList);
        }
    }

    public static class ApiRetrieveTerminology extends T8DefaultServerOperation
    {
        public ApiRetrieveTerminology(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8ConceptTerminology> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ConceptTerminology terminology;
            String conceptId;
            String languageId;
            T8TerminologyApi trmApi;

            // Get the operation parameters we need.
            conceptId = (String)operationParameters.get(PARAMETER_CONCEPT_ID);
            languageId = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);

            // Perform the API operation.
            trmApi = (T8TerminologyApi)tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            terminology = trmApi.retrieveTerminology(languageId, conceptId);
            return HashMaps.createSingular(PARAMETER_TERMINOLOGY, terminology);
        }
    }

    public static class ApiSaveTerminology extends T8DefaultServerOperation
    {
        public ApiSaveTerminology(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ConceptTerminology terminology;
            boolean deletionEnabled;
            T8TerminologyApi trmApi;

            // Get the operation parameters we need.
            terminology = (T8ConceptTerminology)operationParameters.get(PARAMETER_TERMINOLOGY);
            deletionEnabled = (boolean)operationParameters.get(PARAMETER_INCLUDE_DELETION);

            // Perform the API operation.
            trmApi = (T8TerminologyApi)tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            trmApi.saveTerminology(terminology, deletionEnabled);

            return null;
        }
    }
}
