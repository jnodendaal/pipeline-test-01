package com.pilog.t8.data.alteration;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.data.org.T8OntologyLink;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import static com.pilog.t8.definition.api.T8DataAlterationApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataAlterationDataHandler
{
    private final T8ServerContext serverContext;

    public T8DataAlterationDataHandler(T8ServerContext serverContext) throws Exception
    {
        this.serverContext = serverContext;
    }

    /**
     * Delete a data alteration package index in the applicable alteration tables.
     * This method does not check existing data and performs a straight delete.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param entryIid
     * @throws Exception
     */
    public void deleteAlterationPackageIndex(T8DataTransaction tx, String packageIid, String entryIid) throws Exception
    {
        String entityId;
        T8DataFilter filter;
        Map<String, Object> filterKeyMap;

        // Get the entity identifier from the model definition.
        entityId = ALT_PACKAGE_INDEX_DE_IDENTIFIER;

        // Initialize the filter key map with the packageIid and entryId.
        filterKeyMap = new HashMap<>();
        filterKeyMap.put(entityId + EF_PACKAGE_IID, packageIid);
        filterKeyMap.put(entityId + EF_ENTRY_IID, packageIid);

        // Delete the package index with the given filter.
        filter = new T8DataFilter(entityId, filterKeyMap);
        tx.delete(entityId,filter);
    }

    /**
     * Delete a data alteration package in the applicable alteration tables.
     * This method does not check existing data and performs a straight delete.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @throws Exception
     */
    public void deleteAlterationPackage(T8DataTransaction tx, String packageIid) throws Exception
    {
        String entityId;
        T8DataFilter filter;
        Map<String, Object> filterKeyMap;

        // Get the entity identifier from the model definition.
        entityId = ALT_PACKAGE_DE_IDENTIFIER;

        // Initialize the filter key map with the packageIid.
        filterKeyMap = new HashMap<>();
        filterKeyMap.put(entityId + EF_PACKAGE_IID, packageIid);

        // Delete the package with the given filter.
        filter = new T8DataFilter(entityId, filterKeyMap);
        tx.delete(entityId,filter);
    }

    /**
     * Delete a data alteration ontology abbreviation in the applicable alteration tables.
     * This method does not check existing data and performs a straight delete.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @return
     * @throws Exception
     */
    public int deleteOntologyAbbreviation(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        String entityId;
        T8DataFilter filter;
        Map<String, Object> filterKeyMap;
        int recordsDeletedCount;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;

        // Initialize the filter key map with the packageIid.
        filterKeyMap = new HashMap<>();
        filterKeyMap.put(entityId + EF_PACKAGE_IID, packageIid);

        // If step is equal to -1 delete all where the packageIid match with input
        if (step < 0)
        {
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId,filter);
        }
        else
        {
            filterKeyMap.put(entityId + EF_STEP, step);
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId,filter);
        }

        return recordsDeletedCount;
    }

    /**
     * Delete a data alteration ontology concept in the applicable alteration tables.
     * This method does not check existing data and performs a straight delete.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @return
     * @throws Exception
     */
    public int deleteOntologyConcept(T8DataTransaction tx,String packageIid, int step) throws Exception
    {
        String entityId;
        T8DataFilter filter;
        Map<String, Object> filterKeyMap;
        int recordsDeletedCount;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_CONCEPT_DE_IDENTIFIER;

        // Initialize the filter key map with the packageIid.
        filterKeyMap = new HashMap<>();
        filterKeyMap.put(entityId + EF_PACKAGE_IID, packageIid);

        // If step is equal to -1 delete all where the packageIid match with input
        if (step < 0)
        {
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId, filter);
        }
        else
        {
            filterKeyMap.put(entityId + EF_STEP, step);
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId, filter);
        }

        // Delete ontology links.
        deleteOntologyLinks(tx, packageIid, step);

        // Delete ontology definitions.
        deleteOntologyDefinitions(tx, packageIid, step);

        // Delete ontology codes.
        deleteOntologyCodes(tx, packageIid, step);

        // Delete ontology terms.
        deleteOntologyTerms(tx, packageIid, step);

        return recordsDeletedCount;
    }

    public int deleteOntologyLinks(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        Map<String, Object> filterKeyMap;
        String entityId;
        T8DataFilter filter;
        int recordsDeletedCount;

        // Get the entity identifier from the model definition.
        entityId = ALT_ORG_ONTOLOGY_DE_IDENTIFIER;

        // Initialize the filter key map with the packageIid.
        filterKeyMap = new HashMap<>();
        filterKeyMap.put(entityId + EF_PACKAGE_IID, packageIid);

        // If step is equal to -1 delete all where the packageIid match with input.
        if (step < 0)
        {
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId, filter);
        }
        else
        {
            filterKeyMap.put(entityId + EF_STEP, step);
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId, filter);
        }

        return recordsDeletedCount;
    }

    /**
     * Delete a data alteration ontology code in the applicable alteration tables.
     * This method does not check existing data and performs a straight delete.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @return
     * @throws Exception
     */
    public int deleteOntologyCodes(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        String entityId;
        T8DataFilter filter;
        Map<String, Object> filterKeyMap;
        int recordsDeletedCount;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_CODE_DE_IDENTIFIER;

        // Initialize the filter key map with the packageIid.
        filterKeyMap = new HashMap<>();
        filterKeyMap.put(entityId + EF_PACKAGE_IID, packageIid);

        // If step is equal to -1 delete all where the packageIid match with input
        if (step < 0)
        {
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId,filter);
        }
        else
        {
            filterKeyMap.put(entityId + EF_STEP, step);
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId,filter);
        }

        return recordsDeletedCount;
    }

    /**
     * Delete a data alteration ontology definition in the applicable alteration tables.
     * This method does not check existing data and performs a straight delete.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @return
     * @throws Exception
     */
    public int deleteOntologyDefinitions(T8DataTransaction tx,String packageIid, int step) throws Exception
    {
        String entityId;
        T8DataFilter filter;
        Map<String, Object> filterKeyMap;
        int recordsDeletedCount;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_DEFINITION_DE_IDENTIFIER;

        // Initialize the filter key map with the packageIid.
        filterKeyMap = new HashMap<>();
        filterKeyMap.put(entityId + EF_PACKAGE_IID, packageIid);

        // If step is equal to -1 delete all where the packageIid match with input
        if (step < 0)
        {
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId,filter);
        }
        else
        {
            filterKeyMap.put(entityId + EF_STEP, step);
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId,filter);
        }

        return recordsDeletedCount;
    }

    /**
     * Delete a data alteration ontology term in the applicable alteration tables.
     * This method does not check existing data and performs a straight delete.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @return
     * @throws Exception
     */
    public int deleteOntologyTerms(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        String entityId;
        T8DataFilter filter;
        Map<String, Object> filterKeyMap;
        int recordsDeletedCount;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_TERM_DE_IDENTIFIER;

        // Initialize the filter key map with the packageIid.
        filterKeyMap = new HashMap<>();
        filterKeyMap.put(entityId + EF_PACKAGE_IID, packageIid);

        // If step is equal to -1 delete all where the packageIid match with input
        if (step < 0)
        {
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId,filter);
        }
        else
        {
            filterKeyMap.put(entityId + EF_STEP, step);
            filter = new T8DataFilter(entityId, filterKeyMap);
            recordsDeletedCount = tx.delete(entityId,filter);
        }

        // Delete ontology abbreviations
        deleteOntologyAbbreviation(tx, packageIid, step);
        return recordsDeletedCount;
    }

    /**
     * Inserts a data alteration package in the applicable alteration tables.
     * This method does not check existing data and performs a straight insert.
     * @param tx The transaction to use for this operation.
     * @param alterationPackage The data alteration package to persist.
     * @throws Exception
     */
    public void insertAlterationPackage(T8DataTransaction tx, T8DataAlterationPackage alterationPackage) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;

        // Get the entity identifier from the model definition.
        entityId = ALT_PACKAGE_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, alterationPackage.getIid());
        entryEntity.setFieldValue(entityId + EF_PACKAGE_ID, alterationPackage.getId());
        entryEntity.setFieldValue(entityId + EF_PACKAGE_VERSION, alterationPackage.getVersion());
        entryEntity.setFieldValue(entityId + EF_PACKAGE_VERSION_REQUIRED, alterationPackage.getVersionRequired());
        entryEntity.setFieldValue(entityId + EF_SOURCE_ID, alterationPackage.getSourceId());
        entryEntity.setFieldValue(entityId + EF_SOURCE_IID, alterationPackage.getSourceIid());
        entryEntity.setFieldValue(entityId + EF_DISPLAY_NAME, alterationPackage.getDisplayName());
        entryEntity.setFieldValue(entityId + EF_DESCRIPTION, alterationPackage.getDescription());
        entryEntity.setFieldValue(entityId + EF_APPLIED_STEP, alterationPackage.getAppliedStep());
        tx.insert(entryEntity);
    }

    /**
     * Inserts a data alteration ontology abbreviation in the applicable alteration tables.
     * This method does not check existing data and performs a straight insert.
     * @param tx The transaction to use for this operation.
     * @param abbreviation The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void insertOntologyAbbreviation(T8DataTransaction tx, T8OntologyAbbreviation abbreviation, String packageIid, int step, String method) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_ABBREVIATION_ID, abbreviation.getID());
        entryEntity.setFieldValue(entityId + EF_TERM_ID, abbreviation.getTermID());
        entryEntity.setFieldValue(entityId + EF_ABBREVIATION, abbreviation.getAbbreviation());
        entryEntity.setFieldValue(entityId + EF_IRDI, abbreviation.getIrdi());
        entryEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, abbreviation.isStandard() ? "Y" : "N");
        entryEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, abbreviation.isActive() ? "Y" : "N");
        tx.insert(entryEntity);
    }

    /**
     * Inserts a data alteration ontology concept in the applicable alteration tables.
     * This method does not check existing data and performs a straight insert.
     * @param tx The transaction to use for this operation.
     * @param concept The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void insertOntologyConcept(T8DataTransaction tx, T8OntologyConcept concept, String packageIid, int step, String method) throws Exception
    {
        T8DataEntity conceptEntity;
        String entityId;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_CONCEPT_DE_IDENTIFIER;

        // Create the new package entity
        conceptEntity = tx.create(entityId, null);
        conceptEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        conceptEntity.setFieldValue(entityId + EF_STEP, step);
        conceptEntity.setFieldValue(entityId + EF_METHOD, method);
        conceptEntity.setFieldValue(entityId + EF_CONCEPT_ID, concept.getID());
        conceptEntity.setFieldValue(entityId + EF_CONCEPT_TYPE_ID, concept.getConceptType().getConceptTypeID());
        conceptEntity.setFieldValue(entityId + EF_IRDI, concept.getIrdi());
        conceptEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, concept.isStandard() ? "Y" : "N");
        conceptEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, concept.isActive() ? "Y" : "N");
        tx.insert(conceptEntity);

        // Insert ontology links.
        for (T8OntologyLink link : concept.getOntologyLinks())
        {
            insertOntologyLink(tx, link, packageIid, step, method);
        }

        // Insert ontology definitions.
        for (T8OntologyDefinition definition : concept.getDefinitions())
        {
            insertOntologyDefinition(tx, definition, packageIid, step, method);
        }

        // Insert ontology codes.
        for (T8OntologyCode code : concept.getCodes())
        {
            insertOntologyCode(tx, code, packageIid, step, method);
        }

        // Insert ontology terms.
        for (T8OntologyTerm term : concept.getTerms())
        {
            insertOntologyTerm(tx, term, packageIid, step, method);
        }
    }

    public void insertOntologyLink(T8DataTransaction tx, T8OntologyLink link, String packageIid, int step, String method) throws Exception
    {
        T8DataEntity entryEntity;
        String entityId;

        // Create the new package entity
        entityId = ALT_ORG_ONTOLOGY_DE_IDENTIFIER;
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_ORG_ID, link.getOrganizationID());
        entryEntity.setFieldValue(entityId + EF_ODS_ID, link.getOntologyStructureID());
        entryEntity.setFieldValue(entityId + EF_ODT_ID, link.getOntologyClassID());
        entryEntity.setFieldValue(entityId + EF_CONCEPT_ID, link.getConceptID());
        tx.insert(entryEntity);
    }

    /**
     * Inserts a data alteration ontology code in the applicable alteration tables.
     * This method does not check existing data and performs a straight insert.
     * @param tx The transaction to use for this operation.
     * @param code The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void insertOntologyCode(T8DataTransaction tx, T8OntologyCode code, String packageIid, int step, String method) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_CODE_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_CODE_ID, code.getID());
        entryEntity.setFieldValue(entityId + EF_CONCEPT_ID, code.getConceptID());
        entryEntity.setFieldValue(entityId + EF_CODE_TYPE_ID, code.getCodeTypeID());
        entryEntity.setFieldValue(entityId + EF_CODE, code.getCode());
        entryEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, code.isStandard() ? "Y" : "N");
        entryEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, code.isActive() ? "Y" : "N");

        tx.insert(entryEntity);
    }

    /**
     * Inserts a data alteration ontology definition in the applicable alteration tables.
     * This method does not check existing data and performs a straight insert.
     * @param tx The transaction to use for this operation.
     * @param definition The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void insertOntologyDefinition(T8DataTransaction tx, T8OntologyDefinition definition, String packageIid, int step, String method) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_DEFINITION_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_DEFINITION_ID, definition.getID());
        entryEntity.setFieldValue(entityId + EF_CONCEPT_ID, definition.getConceptID());
        entryEntity.setFieldValue(entityId + EF_LANGUAGE_ID, definition.getLanguageID());
        entryEntity.setFieldValue(entityId + EF_DEFINITION, definition.getDefinition());
        entryEntity.setFieldValue(entityId + EF_IRDI, definition.getIrdi());
        entryEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, definition.isStandard() ? "Y" : "N");
        entryEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, definition.isActive() ? "Y" : "N");

        tx.insert(entryEntity);
    }

    /**
     * Inserts a data alteration ontology term in the applicable alteration tables.
     * This method does not check existing data and performs a straight insert.
     * @param tx The transaction to use for this operation.
     * @param term The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void insertOntologyTerm(T8DataTransaction tx, T8OntologyTerm term, String packageIid, int step, String method) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;
        List<T8OntologyAbbreviation> abbreviations;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_TERM_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_TERM_ID, term.getID());
        entryEntity.setFieldValue(entityId + EF_CONCEPT_ID, term.getConceptID());
        entryEntity.setFieldValue(entityId + EF_LANGUAGE_ID, term.getLanguageID());
        entryEntity.setFieldValue(entityId + EF_TERM, term.getTerm());
        entryEntity.setFieldValue(entityId + EF_IRDI, term.getIrdi());
        entryEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, term.isStandard() ? "Y" : "N");
        entryEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, term.isActive() ? "Y" : "N");

        tx.insert(entryEntity);

        // Insert ontology abbreviations
        abbreviations = term.getAbbreviations();

        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            insertOntologyAbbreviation(tx, abbreviation, packageIid, step, method);
        }
    }

    /**
     * Applies the specified changes to alteration index entries.
     * @param tx The transaction to use for this operation.
     * @param changes The changes to apply to the alteration index.
     * @throws Exception
     */
    public void updateAlterationIndex(T8DataTransaction tx, T8DataAlterationPackageIndexChanges changes) throws Exception
    {
        String entityId;

        // Get the entity identifier from the model definition.
        entityId = ALT_PACKAGE_INDEX_DE_IDENTIFIER;

        // Apply all of the changes to the alteration index.
        for (T8DataAlterationPackageIndexChange change : changes.getChanges())
        {
            T8DataAlterationPackageIndexEntry entry;
            T8DataEntity entryEntity;

            // Create the index entry entity from the specified change.
            entry = change.getEntry();
            entryEntity = tx.create(entityId, null);
            entryEntity.setFieldValue(ALT_PACKAGE_INDEX_DE_IDENTIFIER + EF_ENTRY_IID, entry.getEntryIid());
            entryEntity.setFieldValue(ALT_PACKAGE_INDEX_DE_IDENTIFIER + EF_PACKAGE_IID, entry.getPackageIid());
            entryEntity.setFieldValue(ALT_PACKAGE_INDEX_DE_IDENTIFIER + EF_ALTERATION_ID, entry.getAlterationId());
            entryEntity.setFieldValue(ALT_PACKAGE_INDEX_DE_IDENTIFIER + EF_STEP, entry.getStep());
            entryEntity.setFieldValue(ALT_PACKAGE_INDEX_DE_IDENTIFIER + EF_SIZE, entry.getSize());

            // Apply the changes based on its type.
            switch (change.getChangeType())
            {
                case INSERT:
                    tx.insert(entryEntity);
                    break;
                case DELETE:
                    tx.delete(entryEntity);
                    break;
                case UPDATE:
                    tx.update(entryEntity);
                    break;
                default: throw new RuntimeException("Invalid alteration change type: " + change.getChangeType());
            }
        }
    }

    /**
     * Updates a data alteration package in the applicable alteration tables.
     * This method does not check existing data and performs a straight update.
     * @param tx The transaction to use for this operation.
     * @param alterationPackage The data alteration package to persist.
     * @throws Exception
     */
    public void updateAlterationPackage(T8DataTransaction tx, T8DataAlterationPackage alterationPackage) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;

        // Get the entity identifier from the model definition.
        entityId = ALT_PACKAGE_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, alterationPackage.getIid());
        entryEntity.setFieldValue(entityId + EF_PACKAGE_ID, alterationPackage.getId());
        entryEntity.setFieldValue(entityId + EF_PACKAGE_VERSION, alterationPackage.getVersion());
        entryEntity.setFieldValue(entityId + EF_PACKAGE_VERSION_REQUIRED, alterationPackage.getVersionRequired());
        entryEntity.setFieldValue(entityId + EF_SOURCE_ID, alterationPackage.getSourceId());
        entryEntity.setFieldValue(entityId + EF_SOURCE_IID, alterationPackage.getSourceIid());
        entryEntity.setFieldValue(entityId + EF_DISPLAY_NAME, alterationPackage.getDisplayName());
        entryEntity.setFieldValue(entityId + EF_DESCRIPTION, alterationPackage.getDescription());
        entryEntity.setFieldValue(entityId + EF_APPLIED_STEP, alterationPackage.getAppliedStep());

        tx.update(entryEntity);
    }

    /**
     * Update a data alteration ontology abbreviation in the applicable alteration tables.
     * This method does not check existing data and performs a straight update.
     * @param tx The transaction to use for this operation.
     * @param abbreviation The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void updateOntologyAbbreviation(T8DataTransaction tx, T8OntologyAbbreviation abbreviation, String packageIid, int step, String method) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_ABBREVIATION_ID, abbreviation.getID());
        entryEntity.setFieldValue(entityId + EF_TERM_ID, abbreviation.getTermID());
        entryEntity.setFieldValue(entityId + EF_ABBREVIATION, abbreviation.getAbbreviation());
        entryEntity.setFieldValue(entityId + EF_IRDI, abbreviation.getIrdi());
        entryEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, abbreviation.isStandard() ? "Y" : "N");
        entryEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, abbreviation.isActive() ? "Y" : "N");

        tx.update(entryEntity);
    }

    /**
     * Update a data alteration ontology concept in the applicable alteration tables.
     * This method does not check existing data and performs a straight update.
     * @param tx The transaction to use for this operation.
     * @param concept The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void updateOntologyConcept(T8DataTransaction tx, T8OntologyConcept concept, String packageIid, int step, String method) throws Exception
    {
        T8DataEntity entryEntity;
        String entityId;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_CONCEPT_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_CONCEPT_ID, concept.getID());
        entryEntity.setFieldValue(entityId + EF_CONCEPT_TYPE_ID, concept.getConceptType().getConceptTypeID());
        entryEntity.setFieldValue(entityId + EF_IRDI, concept.getIrdi());
        entryEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, concept.isStandard() ? "Y" : "N");
        entryEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, concept.isActive() ? "Y" : "N");
        tx.update(entryEntity);

        // Update ontology definitions.
        for (T8OntologyDefinition definition : concept.getDefinitions())
        {
            updateOntologyDefinition(tx, definition, packageIid, step, method);
        }

        // Update ontology codes.
        for (T8OntologyCode code : concept.getCodes())
        {
            updateOntologyCode(tx, code, packageIid, step, method);
        }

        // Update ontology terms.
        for (T8OntologyTerm term : concept.getTerms())
        {
            updateOntologyTerm(tx, term, packageIid, step, method);
        }
    }

    /**
     * Update a data alteration ontology code in the applicable alteration tables.
     * This method does not check existing data and performs a straight update.
     * @param tx The transaction to use for this operation.
     * @param code The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void updateOntologyCode(T8DataTransaction tx, T8OntologyCode code, String packageIid, int step, String method) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_CODE_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_CODE_ID, code.getID());
        entryEntity.setFieldValue(entityId + EF_CONCEPT_ID, code.getConceptID());
        entryEntity.setFieldValue(entityId + EF_CODE_TYPE_ID, code.getCodeTypeID());
        entryEntity.setFieldValue(entityId + EF_CODE, code.getCode());
        entryEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, code.isStandard() ? "Y" : "N");
        entryEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, code.isActive() ? "Y" : "N");

        tx.update(entryEntity);
    }

    /**
     * Update a data alteration ontology definition in the applicable alteration tables.
     * This method does not check existing data and performs a straight update.
     * @param tx The transaction to use for this operation.
     * @param definition The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void updateOntologyDefinition(T8DataTransaction tx, T8OntologyDefinition definition, String packageIid, int step, String method) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_DEFINITION_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_DEFINITION_ID, definition.getID());
        entryEntity.setFieldValue(entityId + EF_CONCEPT_ID, definition.getConceptID());
        entryEntity.setFieldValue(entityId + EF_LANGUAGE_ID, definition.getLanguageID());
        entryEntity.setFieldValue(entityId + EF_DEFINITION, definition.getDefinition());
        entryEntity.setFieldValue(entityId + EF_IRDI, definition.getIrdi());
        entryEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, definition.isStandard() ? "Y" : "N");
        entryEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, definition.isActive() ? "Y" : "N");

        tx.update(entryEntity);
    }

    /**
     * Update a data alteration ontology term in the applicable alteration tables.
     * This method does not check existing data and performs a straight update.
     * @param tx The transaction to use for this operation.
     * @param term The data alteration package to persist.
     * @param packageIid
     * @param step
     * @param method
     * @throws Exception
     */
    public void updateOntologyTerm(T8DataTransaction tx, T8OntologyTerm term, String packageIid, int step, String method) throws Exception
    {
        String entityId;
        T8DataEntity entryEntity;
        List<T8OntologyAbbreviation> abbreviations;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_TERM_DE_IDENTIFIER;

        // Create the new package entity
        entryEntity = tx.create(entityId, null);
        entryEntity.setFieldValue(entityId + EF_PACKAGE_IID, packageIid);
        entryEntity.setFieldValue(entityId + EF_STEP, step);
        entryEntity.setFieldValue(entityId + EF_METHOD, method);
        entryEntity.setFieldValue(entityId + EF_TERM_ID, term.getID());
        entryEntity.setFieldValue(entityId + EF_CONCEPT_ID, term.getConceptID());
        entryEntity.setFieldValue(entityId + EF_LANGUAGE_ID, term.getLanguageID());
        entryEntity.setFieldValue(entityId + EF_TERM, term.getTerm());
        entryEntity.setFieldValue(entityId + EF_IRDI, term.getIrdi());
        entryEntity.setFieldValue(entityId + EF_STANDARD_INDICATOR, term.isStandard() ? "Y" : "N");
        entryEntity.setFieldValue(entityId + EF_ACTIVE_INDICATOR, term.isActive() ? "Y" : "N");

        tx.update(entryEntity);

        // Update ontology abbreviations
        abbreviations = term.getAbbreviations();

        for (T8OntologyAbbreviation abbreviation : abbreviations)
        {
            updateOntologyAbbreviation(tx, abbreviation, packageIid, step, method);
        }
    }

    /**
     * Retrieves the specified alteration index from the persistence store.
     * @param tx The transaction to use for this operation.
     * @param packageIid The id of the alteration package instance for which to retrieve the index.
     * @return The {@code T8DataAlterationPackageIndex} retrieved for the specified alteration instance.
     * @throws Exception
     */
    public T8DataAlterationPackageIndex retrieveDataAlterationIndex(T8DataTransaction tx, String packageIid) throws Exception
    {
        T8DataFilterCriteria filterCriteria;
        List<T8DataEntity> entityList;
        T8DataAlterationPackageIndex index;
        String entityId;
        T8DataFilter filter;

        // Get the entity identifier from the model definition.
        entityId = ALT_PACKAGE_INDEX_DE_IDENTIFIER;

        // Create a map to filter out only the required records.
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + EF_PACKAGE_IID, DataFilterOperator.EQUAL, packageIid));
        filter = new T8DataFilter(entityId, filterCriteria);

        // Select the entities and pack all entries into the index.
        index = new T8DataAlterationPackageIndex(packageIid);
        entityList = tx.select(entityId, filter);
        for (T8DataEntity entity : entityList)
        {
            String entryIid;
            String alterationId;
            int step;
            int size;

            entryIid = (String)entity.getFieldValue(EF_ENTRY_IID);
            alterationId = (String)entity.getFieldValue(EF_ALTERATION_ID);
            step = (Integer)entity.getFieldValue(EF_STEP);
            size = (Integer)entity.getFieldValue(EF_SIZE);
            index.addEntry(new T8DataAlterationPackageIndexEntry(entryIid, packageIid, alterationId, step, size));
        }

        // Return the index.
        return index;
    }

    /**
     * @param tx The transaction to use for this operation.
     * @param packageIid The unique instance id of the alteration package to retrieve.
     * @return The specified alteration package retrieved from the persistence store or
     * null if no such alteration package exists.
     * @throws Exception
     */
    public T8DataAlterationPackage retrieveAlterationPackage(T8DataTransaction tx, String packageIid) throws Exception
    {
        T8DataEntity entity;
        String entityId;
        T8DataAlterationPackage alterationPackage;
        Map<String, Object> filterMap;

        // Get the entity identifier from the model definition.
        entityId = ALT_PACKAGE_DE_IDENTIFIER;

        alterationPackage = new T8DataAlterationPackage();

        // Create a map to filter out only the required records.
        filterMap = new HashMap<>();
        filterMap.put(entityId + EF_PACKAGE_IID, packageIid);

        // Retreive the package details.
        entity = tx.retrieve(entityId, filterMap);

        // Build the T8DataAlterationPackage object.
        alterationPackage.setIid((String) entity.getFieldValue(entityId + EF_PACKAGE_IID));
        alterationPackage.setId((String) entity.getFieldValue(entityId + EF_PACKAGE_ID));
        alterationPackage.setVersion((String) entity.getFieldValue(entityId + EF_PACKAGE_VERSION));
        alterationPackage.setVersionRequired((String) entity.getFieldValue(entityId + EF_PACKAGE_VERSION_REQUIRED));
        alterationPackage.setSourceId((String) entity.getFieldValue(entityId + EF_SOURCE_ID));
        alterationPackage.setSourceIid((String) entity.getFieldValue(entityId + EF_SOURCE_IID));
        alterationPackage.setDisplayName((String) entity.getFieldValue(entityId + EF_DISPLAY_NAME));
        alterationPackage.setDescription((String) entity.getFieldValue(entityId + EF_DESCRIPTION));
        alterationPackage.setAppliedStep((int) entity.getFieldValue(entityId + EF_APPLIED_STEP));

        // Return the T8DataAlterationPackage object
        return alterationPackage;
    }

    /**
     * @param tx The transaction to use for this operation.
     * @param packageIid The unique instance id of the alteration package to retrieve.
     * @param step
     * @return The specified package concept method.
     * @throws Exception
     */
    public String retrieveConceptAlterationMethod(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        T8DataEntity entity;
        String entityId;
        Map<String, Object> filterMap;
        String method;

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_CONCEPT_DE_IDENTIFIER;

        // Create a map to filter out only the required records.
        filterMap = new HashMap<>();
        filterMap.put(entityId + EF_PACKAGE_IID, packageIid);
        filterMap.put(entityId + EF_STEP, step);

        // Retreive the package details.
        entity = tx.retrieve(entityId, filterMap);

        // Retrieve the method from the entity.
        method = (String) entity.getFieldValue(entityId + EF_METHOD);

        // Return the Tconcept method.
        return method;
    }

    /**
     * Retrieve a data alteration ontology abbreviation in the applicable alteration tables.
     * This method does not check existing data and performs a straight retrieve.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @param termId
     * @return
     * @throws Exception
     */
     public List<T8OntologyAbbreviation> retrieveOntologyAbbreviations(T8DataTransaction tx, String packageIid, int step, String termId) throws Exception
    {
        String entityId;
        List<T8DataEntity> entityList;
        Map<String, Object> keyMap;
        List<T8OntologyAbbreviation> abbreviationList;
        T8OntologyAbbreviation abbreviationObject;

        abbreviationList = new ArrayList<>();

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;

        // Initialize the map with input paramaters.
        keyMap = new HashMap<>();
        keyMap.put(entityId + EF_PACKAGE_IID, packageIid);
        keyMap.put(entityId + EF_STEP, step);
        keyMap.put(entityId + EF_TERM_ID, termId);

        // Retrieve the abbreviations.
        entityList = tx.select(entityId, keyMap);

        // Build up all abbreviation objects.
        for(T8DataEntity entity : entityList)
        {
            String id;
            String abbreviation;
            Boolean standard;
            Boolean active;
            String irdi;

            id = (String) entity.getFieldValue(entityId + EF_ABBREVIATION_ID);
            termId = (String) entity.getFieldValue(entityId + EF_TERM_ID);
            abbreviation = (String) entity.getFieldValue(entityId + EF_ABBREVIATION);
            standard = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_STANDARD_INDICATOR));
            active = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_ACTIVE_INDICATOR));
            irdi = (String) entity.getFieldValue(entityId + EF_IRDI);

            // Create the abbreviation object.
            abbreviationObject = new T8OntologyAbbreviation(id, termId, abbreviation, standard, active);
            abbreviationObject.setIrdi(irdi);

            abbreviationList.add(abbreviationObject);
        }

        return abbreviationList;
    }

    /**
     * Retrieve data alteration ontology concepts in the applicable alteration tables.
     * This method does not check existing data and performs a straight retrieve.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @return
     * @throws Exception
     */
    public List<T8OntologyConcept> retrieveConcepts(T8DataTransaction tx, String packageIid, int step) throws Exception
    {
        String entityId;
        List<T8DataEntity> entityList;
        Map<String, Object> keyMap;
        List<T8OntologyConcept> conceptList;
        List<T8OntologyDefinition> definitions;
        List<T8OntologyTerm> terms;
        List<T8OntologyCode> codes;
        T8OntologyConceptType conceptType;
        String conceptId;
        String conceptTypeId;
        Boolean standard;
        Boolean active;
        String irdi;

        conceptList = new ArrayList<>();

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_CONCEPT_DE_IDENTIFIER;

        // Initialize the map with input paramaters.
        keyMap = new HashMap<>();
        keyMap.put(entityId + EF_PACKAGE_IID, packageIid);
        keyMap.put(entityId + EF_STEP, step);

        // Retrieve the concept.
        entityList = tx.select(entityId, keyMap);

        for (T8DataEntity entity : entityList)
        {
            T8OntologyConcept conceptObject;

            // Build up the concept object.
            conceptId = (String) entity.getFieldValue(entityId + EF_CONCEPT_ID);
            conceptTypeId = (String) entity.getFieldValue(entityId + EF_CONCEPT_TYPE_ID);
            standard = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_STANDARD_INDICATOR));
            active = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_ACTIVE_INDICATOR));
            irdi = (String) entity.getFieldValue(entityId + EF_IRDI);

            // Create concept type object.
            conceptType = T8OntologyConceptType.getConceptType(conceptTypeId);

            conceptObject = new T8OntologyConcept(conceptId, conceptType, standard, active);
            conceptObject.setIrdi(irdi);

            // Add ontology links.
            conceptObject.addOntologyLinks(retrieveOntologyLinks(tx, packageIid, step, conceptId));

            // Add the concept definitions.
            definitions = retrieveOntologyDefinitions(tx, packageIid, step, conceptId);
            conceptObject.addDefinitions(definitions);

            // Add the concept codes.
            codes = retrieveOntologyCodes(tx, packageIid, step, conceptId);
            conceptObject.addCodes(codes);

            // Add the concept terms.
            terms = retrieveOntologyTerms(tx, packageIid, step, conceptId);
            conceptObject.addTerms(terms);

            // Add the concept to the list.
            conceptList.add(conceptObject);
        }

        return conceptList;
    }

    public List<T8OntologyLink> retrieveOntologyLinks(T8DataTransaction tx, String packageIid, int step, String conceptId) throws Exception
    {
        List<T8DataEntity> entityList;
        Map<String, Object> keyMap;
        List<T8OntologyLink> linkList;
        String entityId;

        // Get the entity identifier from the model definition.
        entityId = ALT_ORG_ONTOLOGY_DE_IDENTIFIER;

        // Initialize the map with input paramaters.
        keyMap = new HashMap<>();
        keyMap.put(entityId + EF_PACKAGE_IID, packageIid);
        keyMap.put(entityId + EF_STEP, step);
        keyMap.put(entityId + EF_CONCEPT_ID, conceptId);

        // Retrieve the codes.
        entityList = tx.select(entityId, keyMap);

        // Build up all link objects.
        linkList = new ArrayList<>();
        for (T8DataEntity entity : entityList)
        {
            T8OntologyLink link;
            String orgId;
            String osId;
            String ocId;

            orgId = (String) entity.getFieldValue(entityId + EF_ORG_ID);
            osId = (String) entity.getFieldValue(entityId + EF_ODS_ID);
            ocId = (String) entity.getFieldValue(entityId + EF_ODT_ID);
            conceptId = (String) entity.getFieldValue(entityId + EF_CONCEPT_ID);

            // Create the link object.
            link = new T8OntologyLink(orgId, osId, ocId, conceptId);
            linkList.add(link);
        }

        return linkList;
    }

    /**
     * Retrieve a data alteration ontology code in the applicable alteration tables.
     * This method does not check existing data and performs a straight retrieve.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @param conceptId
     * @return
     * @throws Exception
     */
    public List<T8OntologyCode> retrieveOntologyCodes(T8DataTransaction tx, String packageIid, int step, String conceptId) throws Exception
    {
        String entityId;
        List<T8DataEntity> entityList;
        Map<String, Object> keyMap;
        List<T8OntologyCode> codeList;
        T8OntologyCode codeObject;

        codeList = new ArrayList<>();

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_CODE_DE_IDENTIFIER;

        // Initialize the map with input paramaters.
        keyMap = new HashMap<>();
        keyMap.put(entityId + EF_PACKAGE_IID, packageIid);
        keyMap.put(entityId + EF_STEP, step);
        keyMap.put(entityId + EF_CONCEPT_ID, conceptId);

        // Retrieve the codes.
        entityList = tx.select(entityId, keyMap);

        // Build up all code objects.
        for(T8DataEntity entity : entityList)
        {
            String id;
            String codeTypeId;
            String code;
            Boolean standard;
            Boolean active;

            id = (String) entity.getFieldValue(entityId + EF_CODE_ID);
            conceptId = (String) entity.getFieldValue(entityId + EF_CONCEPT_ID);
            codeTypeId = (String) entity.getFieldValue(entityId + EF_CODE_TYPE_ID);
            code = (String) entity.getFieldValue(entityId + EF_CODE);
            standard = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_STANDARD_INDICATOR));
            active = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_ACTIVE_INDICATOR));

            // Create the code object.
            codeObject = new T8OntologyCode(id, conceptId, codeTypeId, code, standard, active);
            codeList.add(codeObject);
        }

        return codeList;
    }

    /**
     * Retrieve a data alteration ontology definition in the applicable alteration tables.
     * This method does not check existing data and performs a straight retrieve.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @param conceptId
     * @return
     * @throws Exception
     */
    public List<T8OntologyDefinition> retrieveOntologyDefinitions(T8DataTransaction tx, String packageIid, int step, String conceptId) throws Exception
    {
        String entityId;
        List<T8DataEntity> entityList;
        Map<String, Object> keyMap;
        List<T8OntologyDefinition> definitionList;
        T8OntologyDefinition definitionObject;

        definitionList = new ArrayList<>();

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_DEFINITION_DE_IDENTIFIER;

        // Initialize the map with input paramaters.
        keyMap = new HashMap<>();
        keyMap.put(entityId + EF_PACKAGE_IID, packageIid);
        keyMap.put(entityId + EF_STEP, step);
        keyMap.put(entityId + EF_CONCEPT_ID, conceptId);

        // Retrieve the definitions.
        entityList = tx.select(entityId, keyMap);

        // Build up all definition objects.
        for(T8DataEntity entity : entityList)
        {
            String id;
            String languageId;
            String definition;
            Boolean standard;
            Boolean active;
            String irdi;

            id = (String) entity.getFieldValue(entityId + EF_DEFINITION_ID);
            conceptId = (String) entity.getFieldValue(entityId + EF_CONCEPT_ID);
            languageId = (String) entity.getFieldValue(entityId + EF_LANGUAGE_ID);
            definition = (String) entity.getFieldValue(entityId + EF_DEFINITION);
            standard = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_STANDARD_INDICATOR));
            active = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_ACTIVE_INDICATOR));
            irdi = (String) entity.getFieldValue(entityId + EF_IRDI);

            // Create the definition object.
            definitionObject = new T8OntologyDefinition(id, conceptId, languageId, definition, standard, active);
            definitionObject.setIrdi(irdi);

            definitionList.add(definitionObject);
        }

        return definitionList;
    }

    /**
     * Retrieve a data alteration ontology term in the applicable alteration tables.
     * This method does not check existing data and performs a straight retrieve.
     * @param tx The transaction to use for this operation.
     * @param packageIid
     * @param step
     * @param conceptId
     * @return
     * @throws Exception
     */
    public List<T8OntologyTerm> retrieveOntologyTerms(T8DataTransaction tx, String packageIid, int step, String conceptId) throws Exception
    {
        String entityId;
        List<T8DataEntity> entityList;
        Map<String, Object> keyMap;
        List<T8OntologyTerm> termList;
        T8OntologyTerm termObject;

        termList = new ArrayList<>();

        // Get the entity identifier from the model definition.
        entityId = ALT_ONTOLOGY_TERM_DE_IDENTIFIER;

        // Initialize the map with input paramaters.
        keyMap = new HashMap<>();
        keyMap.put(entityId + EF_PACKAGE_IID, packageIid);
        keyMap.put(entityId + EF_STEP, step);
        keyMap.put(entityId + EF_CONCEPT_ID, conceptId);

        // Retrieve the terms.
        entityList = tx.select(entityId, keyMap);

        // Build up all term objects.
        for(T8DataEntity entity : entityList)
        {
            String termId;
            String languageId;
            String term;
            Boolean standard;
            Boolean active;
            String irdi;
            List<T8OntologyAbbreviation> abbreviations;

            termId = (String) entity.getFieldValue(entityId + EF_TERM_ID);
            conceptId = (String) entity.getFieldValue(entityId + EF_CONCEPT_ID);
            languageId = (String) entity.getFieldValue(entityId + EF_LANGUAGE_ID);
            term = (String) entity.getFieldValue(entityId + EF_TERM);
            standard = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_STANDARD_INDICATOR));
            active = (Boolean) "Y".equals(entity.getFieldValue(entityId + EF_ACTIVE_INDICATOR));
            irdi = (String) entity.getFieldValue(entityId + EF_IRDI);

            // Create the term object.
            termObject = new T8OntologyTerm(termId, conceptId, languageId, term, standard, active);
            termObject.setIrdi(irdi);

            // Add the abbreviations on the term.
            abbreviations = retrieveOntologyAbbreviations(tx, packageIid, step, termId);
            termObject.addAbbreviations(abbreviations);

            termList.add(termObject);
        }

        return termList;
    }
}
