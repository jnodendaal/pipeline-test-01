package com.pilog.t8.data.ontology;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.security.T8Context;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.LinkedList;
import java.util.Collections;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8OntologyDataHandler
{
    private final T8Context context;

    public T8OntologyDataHandler(T8Context context) throws Exception
    {
        this.context = context;
    }

    public void insertOntologyClass(T8DataTransaction tx, String odsID, String parentODTID, String newODTID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CLASS_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ODS_ID, odsID);
        entity.setFieldValue(entityIdentifier + EF_ODT_ID, newODTID);
        entity.setFieldValue(entityIdentifier + EF_PARENT_ODT_ID, parentODTID);

        // Insert the data type.
        tx.insert(entity);
    }

    public void updateOntologyClassIndices(T8DataTransaction tx, String osID, String ocID, int leftIndex, int rightIndex) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CLASS_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ODS_ID, osID);
        entity.setFieldValue(entityIdentifier + EF_ODT_ID, ocID);
        entity.setFieldValue(entityIdentifier + EF_LFT, leftIndex);
        entity.setFieldValue(entityIdentifier + EF_RGT, rightIndex);

        // Update the class.
        tx.update(entity);
    }

    public void saveOntologyClass(T8DataTransaction tx, String odsID, String parentODTID, String newODTID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CLASS_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ODS_ID, odsID);
        entity.setFieldValue(entityIdentifier + EF_ODT_ID, newODTID);
        entity.setFieldValue(entityIdentifier + EF_PARENT_ODT_ID, parentODTID);

        // Save the data type.
        if (!tx.update(entity))
        {
            tx.insert(entity);
        }
    }

    public boolean deleteOntologyClass(T8DataTransaction tx, String odsID, String odtID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CLASS_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ODS_ID, odsID);
        entity.setFieldValue(entityIdentifier + EF_ODT_ID, odtID);

        // Delete the data type.
        return tx.delete(entity);
    }

    public boolean moveDataTypeToNewParent(T8DataTransaction tx, String odsID, String odtID, String newParentODTID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CLASS_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_ODS_ID, odsID);
        entity.setFieldValue(entityIdentifier + EF_ODT_ID, odtID);
        entity.setFieldValue(entityIdentifier + EF_PARENT_ODT_ID, newParentODTID);

        // Update the data type.
        return tx.update(entity);
    }

    public Set<String> retrieveConceptGraphHeadIDSet(T8DataTransaction tx, Collection<String> conceptGraphIDList, Collection<String> tailConceptIDList) throws Exception
    {
        List<T8DataEntity> relationshipEntities;
        Set<String> headIDSet;
        T8DataFilter relationshipFilter;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Create the filter that will be used to fetch all relationships specified.
        relationshipFilter = new T8DataFilter(entityIdentifier);
        if (CollectionUtilities.hasContent(conceptGraphIDList)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_GRAPH_ID, DataFilterOperator.IN, conceptGraphIDList, false);
        if (CollectionUtilities.hasContent(tailConceptIDList)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_TAIL_CONCEPT_ID, DataFilterOperator.IN, tailConceptIDList, false);

        // Select all relationships identified by the filter.
        relationshipEntities = tx.select(entityIdentifier, relationshipFilter);

        // Pack the query results into a set.
        headIDSet = new HashSet<>();
        for (T8DataEntity relationshipEntity : relationshipEntities)
        {
            headIDSet.add((String)relationshipEntity.getFieldValue(entityIdentifier + EF_HEAD_CONCEPT_ID));
        }

        // Return the result set.
        return headIDSet;
    }

    public Set<String> retrieveConceptGraphTailIDSet(T8DataTransaction tx, Collection<String> conceptGraphIDList, Collection<String> headConceptIDList) throws Exception
    {
        List<T8DataEntity> relationshipEntities;
        Set<String> tailIDSet;
        T8DataFilter relationshipFilter;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Create the filter that will be used to fetch all relationships specified.
        relationshipFilter = new T8DataFilter(entityIdentifier);
        if (CollectionUtilities.hasContent(conceptGraphIDList)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_GRAPH_ID, DataFilterOperator.IN, conceptGraphIDList, false);
        if (CollectionUtilities.hasContent(headConceptIDList)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_HEAD_CONCEPT_ID, DataFilterOperator.IN, headConceptIDList, false);

        // Select all relationships identified by the filter.
        relationshipEntities = tx.select(entityIdentifier, relationshipFilter);

        // Pack the query results into a set.
        tailIDSet = new HashSet<>();
        for (T8DataEntity relationshipEntity : relationshipEntities)
        {
            tailIDSet.add((String)relationshipEntity.getFieldValue(entityIdentifier + EF_TAIL_CONCEPT_ID));
        }

        // Return the result set.
        return tailIDSet;
    }

    public List<T8ConceptPair> retrieveConceptRelations(T8DataTransaction tx, Collection<String> relationIds, Collection<String> tailConceptIds, Collection<String> headConceptIds) throws Exception
    {
        List<T8DataEntity> relationshipEntities;
        List<T8ConceptPair> relationshipList;
        T8DataFilter relationshipFilter;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Create the filter that will be used to fetch all relationships specified.
        relationshipFilter = new T8DataFilter(entityIdentifier);
        if (CollectionUtilities.hasContent(relationIds)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_GRAPH_ID, DataFilterOperator.IN, relationIds, false);
        if (CollectionUtilities.hasContent(tailConceptIds)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_TAIL_CONCEPT_ID, DataFilterOperator.IN, tailConceptIds, false);
        if (CollectionUtilities.hasContent(headConceptIds)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_HEAD_CONCEPT_ID, DataFilterOperator.IN, headConceptIds, false);

        // Select all relationships identified by the filter.
        relationshipEntities = tx.select(entityIdentifier, relationshipFilter);

        // Pack the query results into a list.
        relationshipList = new ArrayList<>();
        for (T8DataEntity relationshipEntity : relationshipEntities)
        {
            String conceptGraphID;
            String tailConceptID;
            String headConceptID;

            conceptGraphID = (String)relationshipEntity.getFieldValue(entityIdentifier + EF_CONCEPT_GRAPH_ID);
            tailConceptID = (String)relationshipEntity.getFieldValue(entityIdentifier + EF_TAIL_CONCEPT_ID);
            headConceptID = (String)relationshipEntity.getFieldValue(entityIdentifier + EF_HEAD_CONCEPT_ID);
            relationshipList.add(new T8ConceptPair(conceptGraphID, tailConceptID, headConceptID));
        }

        // Return the result list.
        return relationshipList;
    }

    /**
     * Validates each of the edges in the supplied list by retrieving it from
     * the database.  If the edge is not found, it is returned in the invalid
     * list.
     * @param tx The transaction to use for this validation operation.
     * @param edgesToValidate The list of edges to validate.
     * @return The list of edges not found in the database.
     * @throws Exception
     */
    public List<T8ConceptPair> validateConceptRelations(T8DataTransaction tx, List<T8ConceptPair> edgesToValidate) throws Exception
    {
        List<T8ConceptPair> invalidEdgeList;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Create the output list to hold all invalid edges.
        invalidEdgeList = new ArrayList<>();

        // Retrieve each of the edges in the list to validate and if one is not found, add it to the invalid list.
        for (T8ConceptPair edgeToValidate : edgesToValidate)
        {
            Map<String, Object> filterMap;

            // Create the filter key map that will be used to fetch the edge.
            filterMap = new HashMap<>();
            filterMap.put(entityIdentifier + EF_CONCEPT_GRAPH_ID, edgeToValidate.getRelationId());
            filterMap.put(entityIdentifier + EF_HEAD_CONCEPT_ID, edgeToValidate.getRightConceptId());
            filterMap.put(entityIdentifier + EF_TAIL_CONCEPT_ID, edgeToValidate.getLeftConceptId());

            // Retrieve the edge.
            if (tx.retrieve(entityIdentifier, filterMap) == null)
            {
                invalidEdgeList.add(edgeToValidate);
            }
        }

        // Return the result list.
        return invalidEdgeList;
    }

    /**
     * This method retrieves all of the edges from the specified concept ID to
     * the source of the specified graph.
     * @param tx The transaction to use.
     * @param conceptRelationIds The list of concept graphs from which to fetch the results.
     * @param conceptID The starting point of the operation.
     * @param headToTail If true, traversal will be from head to tail, otherwise
     * from tail to head.
     * @return The list of graph edges that define the paths from the specified
     * concept to the source of the graph.
     * @throws Exception
     */
    public List<T8ConceptPair> retrieveConceptRelationPath(T8DataTransaction tx, List<String> conceptRelationIds, String conceptID, boolean headToTail) throws Exception
    {
        LinkedList<String> conceptRelationIdQueue;
        List<T8ConceptPair> edgeList;
        String entityIdentifier;
        String nextConceptId;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Create the output list to hold all retrieved edges.
        edgeList = new ArrayList<>();

        // Add the first concept to the queue, and continue to process concepts from the queue while it is not empty.
        nextConceptId = conceptID;
        conceptRelationIdQueue = new LinkedList<>(conceptRelationIds);
        if (headToTail) Collections.reverse(conceptRelationIdQueue);
        while ((!conceptRelationIdQueue.isEmpty()) && (nextConceptId != null))
        {
            T8DataEntity edgeEntity;
            Map<String, Object> filterMap;
            String nextConceptGraphID;
            String tailConceptID;

            // Get the next concept graph ID from the queue and make sure we have not yet traversed it.
            nextConceptGraphID = conceptRelationIdQueue.removeFirst();

            // Create the filter key map that will be used to fetch all relationships specified.
            filterMap = new HashMap<>();
            filterMap.put(entityIdentifier + EF_CONCEPT_GRAPH_ID, nextConceptGraphID);
            filterMap.put(entityIdentifier + EF_HEAD_CONCEPT_ID, nextConceptId);

            // Select all relationships identified by the filter.
            edgeEntity = tx.retrieve(entityIdentifier, filterMap);
            if (edgeEntity != null)
            {
                // Add the next edge to the result list.
                tailConceptID = (String)edgeEntity.getFieldValue(entityIdentifier + EF_TAIL_CONCEPT_ID);
                edgeList.add(new T8ConceptPair(nextConceptGraphID, tailConceptID, nextConceptId));

                // Set the next concept ID to use when the loop retrieves the next result set.
                nextConceptId = tailConceptID;
            }
            else nextConceptId = null;
        }

        // Return the result list.
        return edgeList;
    }

    /**
     * This method retrieves all of the edges from the specified concept ID to
     * the source of the specified graph.
     * @param tx The transaction to use.
     * @param conceptGraphID The concept graph from which to fetch the results.
     * @param conceptID The starting point of the operation.
     * @return The list of graph edges that define the paths from the specified
     * concept to the source of the graph.
     * @throws Exception
     */

    public List<T8ConceptPair> retrieveConceptGraphPathsToSource(T8DataTransaction tx, String conceptGraphID, String conceptID) throws Exception
    {
        List<T8ConceptPair> edgeList;
        LinkedList<String> conceptQueue;
        Set<String> traversedConceptIDSet;
        Map<String, Object> filterMap;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Create the filter key map that will be used to fetch all relationships specified.
        filterMap = new HashMap<>();
        filterMap.put(entityIdentifier + EF_CONCEPT_GRAPH_ID, conceptGraphID);

        // Create the output list to hold all retrieved edges.
        edgeList = new ArrayList<>();

        // Create a set to hold all concept ID's that are traversed so that we can avoid endless loops.
        traversedConceptIDSet = new HashSet<>();

        // Add the first concept to the queue, and continue to process concepts from the queue while it is not empty.
        conceptQueue = new LinkedList<>();
        conceptQueue.add(conceptID);
        while (!conceptQueue.isEmpty())
        {
            String nextConceptID;

            // Get the next concept ID from the queue and make sure we have not yet traversed it.
            nextConceptID = conceptQueue.removeFirst();
            if (!traversedConceptIDSet.contains(nextConceptID))
            {
                List<T8DataEntity> edgeEntities;

                // Add the next concept ID to the set of traversed concepts.
                traversedConceptIDSet.add(nextConceptID);

                // Add the next concept to the filter.
                filterMap.put(entityIdentifier + EF_HEAD_CONCEPT_ID, nextConceptID);

                // Select all relationships identified by the filter.
                edgeEntities = tx.select(entityIdentifier, filterMap);

                // Pack the query results into the edge list.
                for (T8DataEntity edgeEntity : edgeEntities)
                {
                    String tailConceptID;
                    String headConceptID;

                    // Add the next edge to the result list.
                    conceptGraphID = (String)edgeEntity.getFieldValue(entityIdentifier + EF_CONCEPT_GRAPH_ID);
                    tailConceptID = (String)edgeEntity.getFieldValue(entityIdentifier + EF_TAIL_CONCEPT_ID);
                    headConceptID = (String)edgeEntity.getFieldValue(entityIdentifier + EF_HEAD_CONCEPT_ID);
                    edgeList.add(new T8ConceptPair(conceptGraphID, tailConceptID, headConceptID));

                    // Add the tail concept ID to the concept queue so that its tails will be fetched.
                    conceptQueue.add(tailConceptID);
                }
            }
        }

        // Return the result list.
        return edgeList;
    }

    public void insertConceptRelation(T8DataTransaction tx, String conceptGraphID, String tailConceptID, String headConceptID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_CONCEPT_GRAPH_ID, conceptGraphID);
        entity.setFieldValue(entityIdentifier + EF_TAIL_CONCEPT_ID, tailConceptID);
        entity.setFieldValue(entityIdentifier + EF_HEAD_CONCEPT_ID, headConceptID);

        // Insert the relation.
        tx.insert(entity);
    }

    public void saveConceptRelation(T8DataTransaction tx, String conceptGraphID, String tailConceptID, String headConceptID) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_CONCEPT_GRAPH_ID, conceptGraphID);
        entity.setFieldValue(entityIdentifier + EF_TAIL_CONCEPT_ID, tailConceptID);
        entity.setFieldValue(entityIdentifier + EF_HEAD_CONCEPT_ID, headConceptID);

        // Insert the relation.
        if (tx.retrieve(entityIdentifier, entity.getKeyFieldValues()) == null)
        {
            tx.insert(entity);
        }
        else
        {
            tx.update(entity);
        }
    }

    public boolean deleteConceptRelation(T8DataTransaction tx, String relationId, String tailConceptId, String headConceptId) throws Exception
    {
        String entityIdentifier;
        T8DataEntity entity;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Construct the entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_CONCEPT_GRAPH_ID, relationId);
        entity.setFieldValue(entityIdentifier + EF_TAIL_CONCEPT_ID, tailConceptId);
        entity.setFieldValue(entityIdentifier + EF_HEAD_CONCEPT_ID, headConceptId);

        // Delete the relation.
        return tx.delete(entity);
    }

    public int deleteConceptRelationships(T8DataTransaction tx, Collection<String> conceptGraphIDList, Collection<String> tailConceptIDList, Collection<String> headConceptIDList) throws Exception
    {
        T8DataFilter relationshipFilter;
        String entityIdentifier;

        // Get the entity identifier.
        entityIdentifier = ONTOLOGY_CONCEPT_GRAPH_DE_IDENTIFIER;

        // Create the filter that will be used to identify all relationships to be deleted.
        relationshipFilter = new T8DataFilter(entityIdentifier);
        if (CollectionUtilities.hasContent(conceptGraphIDList)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_CONCEPT_GRAPH_ID, DataFilterOperator.IN, conceptGraphIDList, false);
        if (CollectionUtilities.hasContent(tailConceptIDList)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_TAIL_CONCEPT_ID, DataFilterOperator.IN, tailConceptIDList, false);
        if (CollectionUtilities.hasContent(headConceptIDList)) relationshipFilter.addFilterCriterion(DataFilterConjunction.AND, entityIdentifier + EF_HEAD_CONCEPT_ID, DataFilterOperator.IN, headConceptIDList, false);

        // Delete all relationships identified by the filter.
        return tx.delete(entityIdentifier, relationshipFilter);
    }

    public List<T8OntologyConcept> retrieveConcepts(T8DataTransaction tx, Collection<String> conceptIdList, boolean includeTerms, boolean includeAbbreviations, boolean includeDefinitions, boolean includeCodes) throws Exception
    {
        List<T8OntologyConcept> conceptList;

        conceptList = new ArrayList<>();
        for (String conceptID : conceptIdList)
        {
            T8OntologyConcept concept;

            concept = retrieveConcept(tx, conceptID, includeTerms, includeAbbreviations, includeDefinitions, includeCodes);
            if (concept != null) conceptList.add(concept);
        }

        return conceptList;
    }

    public List<T8OntologyTerm> retrieveTerms(T8DataTransaction tx, Collection<String> termIdList, boolean includeAbbreviations) throws Exception
    {
        List<T8OntologyTerm> termList;

        termList = new ArrayList<>();
        for (String termID : termIdList)
        {
            T8OntologyTerm term;

            term = retrieveTerm(tx, termID, includeAbbreviations);
            if (term != null) termList.add(term);
        }

        return termList;
    }

    public List<T8OntologyDefinition> retrieveDefinitions(T8DataTransaction tx, Collection<String> definitionIDList) throws Exception
    {
        List<T8OntologyDefinition> definitionList;

        definitionList = new ArrayList<>();
        for (String definitionID : definitionIDList)
        {
            T8OntologyDefinition definition;

            definition = retrieveDefinition(tx, definitionID);
            if (definition != null) definitionList.add(definition);
        }

        return definitionList;
    }

    public List<T8OntologyAbbreviation> retrieveAbbreviations(T8DataTransaction tx, Collection<String> abbreviationIDList) throws Exception
    {
        List<T8OntologyAbbreviation> abbreviationList;

        abbreviationList = new ArrayList<>();
        for (String abbreviationID : abbreviationIDList)
        {
            T8OntologyAbbreviation abbreviation;

            abbreviation = retrieveAbbreviation(tx, abbreviationID);
            if (abbreviation != null) abbreviationList.add(abbreviation);
        }

        return abbreviationList;
    }

    public List<T8OntologyCode> retrieveCodes(T8DataTransaction tx, Collection<String> codeIDList) throws Exception
    {
        List<T8OntologyCode> codeList;

        codeList = new ArrayList<>();
        for (String codeID : codeIDList)
        {
            T8OntologyCode code;

            code = retrieveCode(tx, codeID);
            if (code != null) codeList.add(code);
        }

        return codeList;
    }

    public T8OntologyConcept retrieveConcept(T8DataTransaction tx, String conceptID, boolean includeTerms, boolean includeAbbreviations, boolean includeDefinitions, boolean includeCodes) throws Exception
    {
        T8DataEntity conceptEntity;
        String entityIdentifier;

        entityIdentifier = ONTOLOGY_CONCEPT_DE_IDENTIFIER;
        conceptEntity = tx.retrieve(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_CONCEPT_ID, conceptID));
        if (conceptEntity != null)
        {
            T8OntologyConcept ontologyConcept;
            String conceptTypeId;
            boolean standardIndicator;
            boolean activeIndicator;

            // Get required the entity field values.
            conceptTypeId = (String)conceptEntity.getFieldValue(entityIdentifier + EF_CONCEPT_TYPE_ID);
            activeIndicator = "Y".equalsIgnoreCase((String)conceptEntity.getFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR));
            standardIndicator = "Y".equalsIgnoreCase((String)conceptEntity.getFieldValue(entityIdentifier + EF_STANDARD_INDICATOR));

            // Construct the concept object.
            ontologyConcept = new T8OntologyConcept(conceptID, T8OntologyConceptType.getConceptType(conceptTypeId), standardIndicator, activeIndicator);

            // Add the concept terms if required.
            if (includeTerms)
            {
                ontologyConcept.addTerms(retrieveConceptTerms(tx, conceptID, includeAbbreviations));
            }

            // Add the concept definitions if required.
            if (includeDefinitions)
            {
                ontologyConcept.addDefinitions(retrieveConceptDefinitions(tx, conceptID));
            }

            // Add the concept codes if required.
            if (includeCodes)
            {
                ontologyConcept.addCodes(retrieveConceptCodes(tx, conceptID));
            }

            // Return the complete concept.
            return ontologyConcept;
        }
        else return null;
    }

    public T8OntologyTerm retrieveTerm(T8DataTransaction tx, String termID, boolean includeAbbreviations) throws Exception
    {
        T8DataEntity termEntity;
        String entityIdentifier;

        entityIdentifier = ONTOLOGY_TERM_DE_IDENTIFIER;
        termEntity = tx.retrieve(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_TERM_ID, termID));
        if (termEntity != null)
        {
            T8OntologyTerm ontologyTerm;

            // Construct the term object.
            ontologyTerm = constructOntologyTerm(termEntity);

            // Add the term abbreviations if required.
            if (includeAbbreviations)
            {
                ontologyTerm.addAbbreviations(retrieveTermAbbreviations(tx, termID));
            }

            // Return the complete term.
            return ontologyTerm;
        }
        else return null;
    }

    public T8OntologyDefinition retrieveDefinition(T8DataTransaction tx, String definitionID) throws Exception
    {
        T8DataEntity definitionEntity;
        String entityIdentifier;

        entityIdentifier = ONTOLOGY_DEFINITION_DE_IDENTIFIER;
        definitionEntity = tx.retrieve(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_DEFINITION_ID, definitionID));
        if (definitionEntity != null)
        {
            T8OntologyDefinition ontologyDefinition;

            // Construct the definition object.
            ontologyDefinition = constructOntologyDefinition(definitionEntity);

            // Return the complete definition.
            return ontologyDefinition;
        }
        else return null;
    }

    public T8OntologyAbbreviation retrieveAbbreviation(T8DataTransaction tx, String abbreviationID) throws Exception
    {
        T8DataEntity abbreviationEntity;
        String entityIdentifier;

        entityIdentifier = ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;
        abbreviationEntity = tx.retrieve(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_ABBREVIATION_ID, abbreviationID));
        if (abbreviationEntity != null)
        {
            T8OntologyAbbreviation ontologyAbbreviation;

            // Construct the abbreviation object.
            ontologyAbbreviation = constructOntologyAbbreviation(abbreviationEntity);

            // Return the complete abbreviation.
            return ontologyAbbreviation;
        }
        else return null;
    }

    public T8OntologyCode retrieveCode(T8DataTransaction tx, String codeID) throws Exception
    {
        T8DataEntity codeEntity;
        String entityIdentifier;

        entityIdentifier = ONTOLOGY_CODE_DE_IDENTIFIER;
        codeEntity = tx.retrieve(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_CODE_ID, codeID));
        if (codeEntity != null)
        {
            T8OntologyCode ontologyCode;

            // Construct the code object.
            ontologyCode = constructOntologyCode(codeEntity);

            // Return the complete code.
            return ontologyCode;
        }
        else return null;
    }

    public T8OntologyCodeType retrieveCodeType(T8DataTransaction tx, String codeTypeId) throws Exception
    {
        T8DataEntity codeTypeEntity;
        String entityIdentifier;

        entityIdentifier = ONTOLOGY_CODE_DE_IDENTIFIER;
        codeTypeEntity = tx.retrieve(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_CODE_TYPE_ID, codeTypeId));
        if (codeTypeEntity != null)
        {
            T8OntologyCodeType ontologyCodeType;

            // Construct the code type object.
            ontologyCodeType = constructOntologyCodeType(codeTypeEntity);

            // Return the complete code type.
            return ontologyCodeType;
        }
        else return null;
    }

    public List<T8OntologyTerm> retrieveConceptTerms(T8DataTransaction tx, String conceptId, boolean includeAbbreviations) throws Exception
    {
        List<T8DataEntity> termEntities;
        List<T8OntologyTerm> ontologyTerms;
        String entityIdentifier;

        ontologyTerms = new ArrayList<>();
        entityIdentifier = ONTOLOGY_TERM_DE_IDENTIFIER;
        termEntities = tx.select(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_CONCEPT_ID, conceptId));
        for (T8DataEntity termEntity : termEntities)
        {
            T8OntologyTerm ontologyTerm;

            // Construct the term object.
            ontologyTerm = constructOntologyTerm(termEntity);

            // Add the term abbreviations if required.
            if (includeAbbreviations)
            {
                ontologyTerm.addAbbreviations(retrieveTermAbbreviations(tx, ontologyTerm.getID()));
            }

            // Add the term to the retrieved collection.
            ontologyTerms.add(ontologyTerm);
        }

        // Return the retrieved collection.
        return ontologyTerms;
    }

    public List<T8OntologyTerm> retrieveConceptTerms(T8DataTransaction tx, Collection<String> conceptIds, boolean includeAbbreviations) throws Exception
    {
        if (conceptIds == null || conceptIds.isEmpty())
        {
            return new ArrayList<>();
        }
        else
        {
            List<T8DataEntity> termEntities;
            List<T8OntologyTerm> ontologyTerms;
            List<String> termIds;
            T8DataFilter filter;
            String entityId;

            ontologyTerms = new ArrayList<>();
            entityId = ONTOLOGY_TERM_DE_IDENTIFIER;
            filter = new T8DataFilter(entityId);
            filter.addFilterCriteria(DataFilterConjunction.AND, entityId + EF_CONCEPT_ID, conceptIds);
            termEntities = tx.select(entityId, filter);
            termIds = new ArrayList<>();
            for (T8DataEntity termEntity : termEntities)
            {
                T8OntologyTerm ontologyTerm;

                // Construct the term object.
                ontologyTerm = constructOntologyTerm(termEntity);

                // Add the term to the retrieved collection.
                ontologyTerms.add(ontologyTerm);
                termIds.add(ontologyTerm.getID());
            }

            // Add the term abbreviations if required.
            if (includeAbbreviations)
            {
                List<T8OntologyAbbreviation> abbreviations;

                abbreviations = retrieveTermAbbreviations(tx, termIds);
                for (T8OntologyAbbreviation abbreviation : abbreviations)
                {
                    String termId;

                    termId = abbreviation.getTermID();
                    for (T8OntologyTerm term : ontologyTerms)
                    {
                        if (term.getID().equals(termId))
                        {
                            term.addAbbreviation(abbreviation);
                        }
                    }
                }
            }

            // Return the retrieved collection.
            return ontologyTerms;
        }
    }

    public List<T8OntologyDefinition> retrieveConceptDefinitions(T8DataTransaction tx, String conceptId) throws Exception
    {
        List<T8DataEntity> definitionEntities;
        List<T8OntologyDefinition> ontologyDefinitions;
        String entityIdentifier;

        ontologyDefinitions = new ArrayList<>();
        entityIdentifier = ONTOLOGY_DEFINITION_DE_IDENTIFIER;
        definitionEntities = tx.select(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_CONCEPT_ID, conceptId));
        for (T8DataEntity definitionEntity : definitionEntities)
        {
            T8OntologyDefinition ontologyDefinition;

            // Construct the definition object.
            ontologyDefinition = constructOntologyDefinition(definitionEntity);

            // Add the definition to the retrieved collection.
            ontologyDefinitions.add(ontologyDefinition);
        }

        // Return the retrieved collection.
        return ontologyDefinitions;
    }

    public List<T8OntologyDefinition> retrieveConceptDefinitions(T8DataTransaction tx, Collection<String> conceptIds) throws Exception
    {
        if ((conceptIds == null) || (conceptIds.isEmpty()))
        {
            return new ArrayList<>();
        }
        else
        {
            List<T8DataEntity> definitionEntities;
            List<T8OntologyDefinition> ontologyDefinitions;
            T8DataFilter filter;
            String entityId;

            ontologyDefinitions = new ArrayList<>();
            entityId = ONTOLOGY_DEFINITION_DE_IDENTIFIER;
            filter = new T8DataFilter(entityId);
            filter.addFilterCriteria(DataFilterConjunction.AND, entityId + EF_CONCEPT_ID, conceptIds);
            definitionEntities = tx.select(entityId, filter);
            for (T8DataEntity definitionEntity : definitionEntities)
            {
                T8OntologyDefinition ontologyDefinition;

                // Construct the definition object.
                ontologyDefinition = constructOntologyDefinition(definitionEntity);

                // Add the definition to the retrieved collection.
                ontologyDefinitions.add(ontologyDefinition);
            }

            // Return the retrieved collection.
            return ontologyDefinitions;
        }
    }

    public List<T8OntologyAbbreviation> retrieveTermAbbreviations(T8DataTransaction tx, String termId) throws Exception
    {
        List<T8DataEntity> abbreviationEntities;
        List<T8OntologyAbbreviation> ontologyAbbreviations;
        String entityIdentifier;

        ontologyAbbreviations = new ArrayList<>();
        entityIdentifier = ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;
        abbreviationEntities = tx.select(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_TERM_ID, termId));
        for (T8DataEntity abbreviationEntity : abbreviationEntities)
        {
            T8OntologyAbbreviation ontologyAbbreviation;

            // Construct the abbreviation object.
            ontologyAbbreviation = constructOntologyAbbreviation(abbreviationEntity);

            // Add the abbreviation to the retrieved collection.
            ontologyAbbreviations.add(ontologyAbbreviation);
        }

        // Return the retrieved collection.
        return ontologyAbbreviations;
    }

    public List<T8OntologyAbbreviation> retrieveTermAbbreviations(T8DataTransaction tx, Collection<String> termIds) throws Exception
    {
        if ((termIds == null) || (termIds.isEmpty()))
        {
            return new ArrayList<>();
        }
        else
        {
            List<T8DataEntity> abbreviationEntities;
            List<T8OntologyAbbreviation> ontologyAbbreviations;
            T8DataFilter filter;
            String entityId;

            ontologyAbbreviations = new ArrayList<>();
            entityId = ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;
            filter = new T8DataFilter(entityId);
            filter.addFilterCriteria(DataFilterConjunction.AND, entityId + EF_TERM_ID, termIds);
            abbreviationEntities = tx.select(entityId, filter);
            for (T8DataEntity abbreviationEntity : abbreviationEntities)
            {
                T8OntologyAbbreviation ontologyAbbreviation;

                // Construct the abbreviation object.
                ontologyAbbreviation = constructOntologyAbbreviation(abbreviationEntity);

                // Add the abbreviation to the retrieved collection.
                ontologyAbbreviations.add(ontologyAbbreviation);
            }

            // Return the retrieved collection.
            return ontologyAbbreviations;
        }
    }

    /**
     * Retrieves and returns all abbreviations in the specific language for phrases starting with the specified words.
     * @param tx The transaction to use for this operation.
     * @param languageId The language to use for filtering of the retrieved phrase abbreviations.
     * @param words The words to use as filter for retrieved phrases.
     * @return A map containing all phrases in the specified language, starting with the supplied words, and
     * linked to each phrase (key) the applicable abbreviation (value).
     * @throws Exception
     */
    public Map<String, String> retrievePhraseAbbreviations(T8DataTransaction tx, String languageId, List<String> words) throws Exception
    {
        Map<String, String> phraseAbbreviations;
        List<T8DataEntity> phraseEntities;
        T8DataFilterCriteria wordCriteria;
        T8DataFilter dataFilter;
        String entityId;

        // Create the filter to be used for retrieval.
        phraseAbbreviations = new LinkedHashMap<>();
        entityId = ONTOLOGY_PHRASE_ABBREVIATION_DE_IDENTIFIER;
        dataFilter = new T8DataFilter(entityId);
        dataFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_ROOT_ORG_ID, DataFilterOperator.EQUAL, context.getRootOrganizationId(), false);
        dataFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_LANGUAGE_ID, DataFilterOperator.EQUAL, languageId, false);

        // Add the criteria to fetch all phrases starting with the words supplied.
        wordCriteria = new T8DataFilterCriteria();
        for (String word : words)
        {
            wordCriteria.addFilterCriterion(DataFilterConjunction.OR, entityId + EF_PHRASE, DataFilterOperator.STARTS_WITH, word, false);
        }
        dataFilter.addFilterCriteria(DataFilterConjunction.AND, wordCriteria);

        // Sort the results by the sequence, so that we can establish precendence.
        dataFilter.addFieldOrdering(entityId + EF_SEQUENCE, T8DataFilter.OrderMethod.ASCENDING);

        // Retrieve and extract the results.
        phraseEntities = tx.select(entityId, dataFilter);
        for (T8DataEntity phraseEntity : phraseEntities)
        {
            String phrase;
            String abbreviation;

            // Get the required fields from the entity and add them to the result map.
            phrase = (String)phraseEntity.getFieldValue(EF_PHRASE);
            abbreviation = (String)phraseEntity.getFieldValue(EF_ABBREVIATION);
            phraseAbbreviations.put(phrase, abbreviation);
        }

        // Return the finaly result map.
        return phraseAbbreviations;
    }

    public List<T8OntologyCode> retrieveConceptCodes(T8DataTransaction tx, String conceptId) throws Exception
    {
        List<T8DataEntity> codeEntities;
        List<T8OntologyCode> ontologyCodes;
        String entityIdentifier;

        ontologyCodes = new ArrayList<>();
        entityIdentifier = ONTOLOGY_CODE_DE_IDENTIFIER;
        codeEntities = tx.select(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_CONCEPT_ID, conceptId));
        for (T8DataEntity codeEntity : codeEntities)
        {
            T8OntologyCode ontologyCode;

            // Construct the code object.
            ontologyCode = constructOntologyCode(codeEntity);

            // Add the code to the retrieved collection.
            ontologyCodes.add(ontologyCode);
        }

        // Return the retrieved collection.
        return ontologyCodes;
    }

    public List<T8OntologyCode> retrieveConceptCodes(T8DataTransaction tx, Collection<String> conceptIds) throws Exception
    {
        if ((conceptIds == null) || (conceptIds.isEmpty()))
        {
            return new ArrayList<>();
        }
        else
        {
            List<T8DataEntity> codeEntities;
            List<T8OntologyCode> ontologyCodes;
            T8DataFilter filter;
            String entityId;

            ontologyCodes = new ArrayList<>();
            entityId = ONTOLOGY_CODE_DE_IDENTIFIER;
            filter = new T8DataFilter(entityId);
            filter.addFilterCriteria(DataFilterConjunction.AND, entityId + EF_CONCEPT_ID, conceptIds);
            codeEntities = tx.select(entityId, filter);
            for (T8DataEntity codeEntity : codeEntities)
            {
                T8OntologyCode ontologyCode;

                // Construct the code object.
                ontologyCode = constructOntologyCode(codeEntity);

                // Add the code to the retrieved collection.
                ontologyCodes.add(ontologyCode);
            }

            // Return the retrieved collection.
            return ontologyCodes;
        }
    }

    public void insertConcept(T8DataTransaction tx, T8OntologyConcept concept, boolean insertTerms, boolean insertAbbreviations, boolean insertDefinitions, boolean insertCodes) throws Exception
    {
        // Insert the concept data.
        tx.insert(constructOntologyConceptEntity(tx, concept));

        // Insert all linked term data.
        if (insertTerms)
        {
            for (T8OntologyTerm term : concept.getTerms())
            {
                insertTerm(tx, term, insertAbbreviations);
            }
        }

        // Insert all linked definition data.
        if (insertDefinitions)
        {
            for (T8OntologyDefinition definition : concept.getDefinitions())
            {
                insertDefinition(tx, definition);
            }
        }

        // Insert all linked code data.
        if (insertCodes)
        {
            for (T8OntologyCode code : concept.getCodes())
            {
                insertCode(tx, code);
            }
        }
    }

    public void insertTerm(T8DataTransaction tx, T8OntologyTerm term, boolean insertAbbreviations) throws Exception
    {
        // Insert the term data.
        tx.insert(constructOntologyTermEntity(tx, term));

        // Insert all linked abbreviation data.
        if (insertAbbreviations)
        {
            for (T8OntologyAbbreviation abbreviation : term.getAbbreviations())
            {
                insertAbbreviation(tx, abbreviation);
            }
        }
    }

    public void insertDefinition(T8DataTransaction tx, T8OntologyDefinition definition) throws Exception
    {
        // Insert the definition data.
        tx.insert(constructOntologyDefinitionEntity(tx, definition));
    }

    public void insertAbbreviation(T8DataTransaction tx, T8OntologyAbbreviation abbreviation) throws Exception
    {
        // Insert the definition data.
        tx.insert(constructOntologyAbbreviationEntity(tx, abbreviation));
    }

    public void insertCode(T8DataTransaction tx, T8OntologyCode code) throws Exception
    {
        // Insert the definition data.
        tx.insert(constructOntologyCodeEntity(tx, code));
    }

    public void updateConcept(T8DataTransaction tx, T8OntologyConcept concept, boolean updateTerms, boolean updateAbbreviations, boolean updateDefinitions, boolean updateCodes) throws Exception
    {
        // Update the concept data.
        tx.update(constructOntologyConceptEntity(tx, concept));

        // Update all linked term data.
        if (updateTerms)
        {
            for (T8OntologyTerm term : concept.getTerms())
            {
                updateTerm(tx, term, updateAbbreviations);
            }
        }

        // Update all linked definition data.
        if (updateDefinitions)
        {
            for (T8OntologyDefinition definition : concept.getDefinitions())
            {
                updateDefinition(tx, definition);
            }
        }

        // Update all linked code data.
        if (updateCodes)
        {
            for (T8OntologyCode code : concept.getCodes())
            {
                updateCode(tx, code);
            }
        }
    }


    public void updateTerm(T8DataTransaction tx, T8OntologyTerm term, boolean updateAbbreviations) throws Exception
    {
        // Update the term data.
        tx.update(constructOntologyTermEntity(tx, term));

        // Update all linked abbreviation data.
        if (updateAbbreviations)
        {
            for (T8OntologyAbbreviation abbreviation : term.getAbbreviations())
            {
                updateAbbreviation(tx, abbreviation);
            }
        }
    }


    public void updateDefinition(T8DataTransaction tx, T8OntologyDefinition definition) throws Exception
    {
        // Update the definition data.
        tx.update(constructOntologyDefinitionEntity(tx, definition));
    }


    public void updateAbbreviation(T8DataTransaction tx, T8OntologyAbbreviation abbreviation) throws Exception
    {
        // Update the definition data.
        tx.update(constructOntologyAbbreviationEntity(tx, abbreviation));
    }


    public void updateCode(T8DataTransaction tx, T8OntologyCode code) throws Exception
    {
        // Update the code data.
        tx.update(constructOntologyCodeEntity(tx, code));
    }


    public void deleteConcept(T8DataTransaction tx, T8OntologyConcept concept, boolean retrieveTerms, boolean retrieveAbbreviations, boolean retrieveDefinitions, boolean retrieveCodes) throws Exception
    {
        List<T8OntologyTerm> termsToDelete;
        List<T8OntologyDefinition> definitionsToDelete;
        List<T8OntologyCode> codesToDelete;

        // Delete the terms linked to the concept.
        termsToDelete = retrieveTerms ? retrieveConceptTerms(tx, concept.getID(), false) : concept.getTerms();
        for (T8OntologyTerm termToDelete : termsToDelete)
        {
            deleteTerm(tx, termToDelete, retrieveAbbreviations);
        }

        // Delete the definitions linked to the concept.
        definitionsToDelete = retrieveDefinitions ? retrieveConceptDefinitions(tx, concept.getID()) : concept.getDefinitions();
        for (T8OntologyDefinition definitionToDelete : definitionsToDelete)
        {
            deleteDefinition(tx, definitionToDelete);
        }

        // Delete the codes linked to the concept.
        codesToDelete = retrieveCodes ? retrieveConceptCodes(tx, concept.getID()) : concept.getCodes();
        for (T8OntologyCode codeToDelete : codesToDelete)
        {
            deleteCode(tx, codeToDelete);
        }

        // Delete the concept entity.
        tx.delete(constructOntologyConceptEntity(tx, concept));
    }


    public void deleteTerm(T8DataTransaction tx, T8OntologyTerm term, boolean retrieveAbbreviations) throws Exception
    {
        List<T8OntologyAbbreviation> abbreviationsToDelete;

        // Delete the abbreviations linked to the term first.
        abbreviationsToDelete = retrieveAbbreviations ? retrieveTermAbbreviations(tx, term.getID()) : term.getAbbreviations();
        for (T8OntologyAbbreviation abbreviationToDelete : abbreviationsToDelete)
        {
            deleteAbbreviation(tx, abbreviationToDelete);
        }

        // Delete the term entity.
        tx.delete(constructOntologyTermEntity(tx, term));
    }


    public void deleteDefinition(T8DataTransaction tx, T8OntologyDefinition definition) throws Exception
    {
        // Delete the definition entity.
        tx.delete(constructOntologyDefinitionEntity(tx, definition));
    }


    public void deleteAbbreviation(T8DataTransaction tx, T8OntologyAbbreviation abbreviation) throws Exception
    {
        // Delete the abbreviation entity.
        tx.delete(constructOntologyAbbreviationEntity(tx, abbreviation));
    }


    public void deleteCode(T8DataTransaction tx, T8OntologyCode code) throws Exception
    {
        // Delete the code entity.
        tx.delete(constructOntologyCodeEntity(tx, code));
    }


    public void deleteConcept(T8DataTransaction tx, String conceptID) throws Exception
    {
        // Delete Concept Terms.
        deleteConceptTerms(tx, conceptID);

        // Delete Concept Definitions.
        deleteConceptDefinitions(tx, conceptID);

        // Delete Concept Codes.
        deleteConceptCodes(tx, conceptID);

        // Delete the Concept.
        tx.delete(constructOntologyConceptEntity(tx, conceptID));
    }


    public void deleteTerm(T8DataTransaction tx, String termID) throws Exception
    {
        // Delete Term Abbreviations.
        deleteTermAbbreviations(tx, termID);

        // Delete the Term.
        tx.delete(constructOntologyTermEntity(tx, termID));
    }


    public void deleteAbbreviation(T8DataTransaction tx, String abbreviationID) throws Exception
    {
        // Delete the Abbreviation.
        tx.delete(constructOntologyAbbreviationEntity(tx, abbreviationID));
    }


    public void deleteDefinition(T8DataTransaction tx, String definitionID) throws Exception
    {
        // Delete the Definition.
        tx.delete(constructOntologyDefinitionEntity(tx, definitionID));
    }


    public void deleteCode(T8DataTransaction tx, String codeID) throws Exception
    {
        // Delete the Code.
        tx.delete(constructOntologyCodeEntity(tx, codeID));
    }


    public void deleteConceptTerms(T8DataTransaction tx, String conceptID) throws Exception
    {
        // Delete Terms using Concept ID filter.
        tx.delete(ONTOLOGY_TERM_DE_IDENTIFIER, constructConceptIDFilter(ONTOLOGY_TERM_DE_IDENTIFIER, conceptID));
    }


    public void deleteConceptDefinitions(T8DataTransaction tx, String conceptID) throws Exception
    {
        // Delete Definitions using Concept ID filter.
        tx.delete(ONTOLOGY_DEFINITION_DE_IDENTIFIER, constructConceptIDFilter(ONTOLOGY_DEFINITION_DE_IDENTIFIER, conceptID));
    }


    public void deleteTermAbbreviations(T8DataTransaction tx, String termID) throws Exception
    {
        // Delete Abbreviations using Term ID filter.
        tx.delete(ONTOLOGY_ABBREVIATION_DE_IDENTIFIER, constructTermIDFilter(ONTOLOGY_ABBREVIATION_DE_IDENTIFIER, termID));
    }


    public void deleteConceptCodes(T8DataTransaction tx, String conceptID) throws Exception
    {
        // Delete Codes using Concept ID filter.
        tx.delete(ONTOLOGY_CODE_DE_IDENTIFIER, constructConceptIDFilter(ONTOLOGY_CODE_DE_IDENTIFIER, conceptID));
    }


    public void saveConcept(T8DataTransaction tx, T8OntologyConcept concept, boolean deletionEnabled, boolean saveTerms, boolean saveAbbreviations, boolean saveDefinitions, boolean saveCodes) throws Exception
    {
        T8OntologyConcept existingConcept;

        // Try to retrieve the object.  If found, update it else insert a new entry.
        existingConcept = retrieveConcept(tx, concept.getID(), deletionEnabled && saveTerms, false, deletionEnabled && saveDefinitions, deletionEnabled && saveCodes);
        if (existingConcept != null) updateConcept(tx, concept, false, false, false, false);
        else insertConcept(tx, concept, false, false, false, false);

        // If deletion is enabled, compare old and new terms, definitions and codes.  Otherwise just save all linked terms, definitions and codes.
        if ((deletionEnabled) && (existingConcept != null))
        {
            // Check the terms.
            if (saveTerms)
            {
                // Delete old terms that are no longer linked to the concept.
                for (T8OntologyTerm oldTerm : existingConcept.getTerms())
                {
                    T8OntologyTerm newTerm;
                    String termID;

                    termID = oldTerm.getID();
                    newTerm = concept.getTerm(termID);
                    if (newTerm == null) deleteTerm(tx, oldTerm, true);
                }

                // Insert new terms and update existing ones.
                for (T8OntologyTerm newTerm : concept.getTerms())
                {
                    T8OntologyTerm oldTerm;
                    String termID;

                    termID = newTerm.getID();
                    oldTerm = existingConcept.getTerm(termID);
                    if (oldTerm == null) insertTerm(tx, newTerm, saveAbbreviations);
                    else updateTerm(tx, newTerm, saveAbbreviations);
                }
            }

            // Check the definitions.
            if (saveDefinitions)
            {
                // Delete old definitions that are no longer linked to the concept.
                for (T8OntologyDefinition oldDefinition : existingConcept.getDefinitions())
                {
                    T8OntologyDefinition newDefinition;
                    String definitionID;

                    definitionID = oldDefinition.getID();
                    newDefinition = concept.getDefinition(definitionID);
                    if (newDefinition == null) deleteDefinition(tx, oldDefinition);
                }

                // Insert new definitions and update existing ones.
                for (T8OntologyDefinition newDefinition : concept.getDefinitions())
                {
                    T8OntologyDefinition oldDefinition;
                    String definitionID;

                    definitionID = newDefinition.getID();
                    oldDefinition = existingConcept.getDefinition(definitionID);
                    if (oldDefinition == null) insertDefinition(tx, newDefinition);
                    else updateDefinition(tx, newDefinition);
                }
            }

            // Check the codes.
            if (saveDefinitions)
            {
                // Delete old codes that are no longer linked to the concept.
                for (T8OntologyCode oldCode : existingConcept.getCodes())
                {
                    T8OntologyCode newCode;
                    String codeID;

                    codeID = oldCode.getID();
                    newCode = concept.getCode(codeID);
                    if (newCode == null) deleteCode(tx, oldCode);
                }

                // Insert new codes and update existing ones.
                for (T8OntologyCode newCode : concept.getCodes())
                {
                    T8OntologyCode oldCode;
                    String codeID;

                    codeID = newCode.getID();
                    oldCode = existingConcept.getCode(codeID);
                    if (oldCode == null) insertCode(tx, newCode);
                    else updateCode(tx, newCode);
                }
            }
        }
        else
        {
            // Save all linked terms.
            if (saveTerms)
            {
                for (T8OntologyTerm term : concept.getTerms())
                {
                    saveTerm(tx, term, false, saveAbbreviations);
                }
            }

            // Save all linked definitions.
            if (saveDefinitions)
            {
                for (T8OntologyDefinition definition : concept.getDefinitions())
                {
                    saveDefinition(tx, definition);
                }
            }

            // Save all linked codes.
            if (saveCodes)
            {
                for (T8OntologyCode code : concept.getCodes())
                {
                    saveCode(tx, code);
                }
            }
        }
    }

    public void saveTerm(T8DataTransaction tx, T8OntologyTerm term, boolean deletionEnabled, boolean saveAbbreviations) throws Exception
    {
        T8OntologyTerm existingTerm;

        // Try to retrieve the object.  If found, update it else insert a new entry.
        existingTerm = retrieveTerm(tx, term.getID(), deletionEnabled && saveAbbreviations);
        if (existingTerm != null) updateTerm(tx, term, false);
        else insertTerm(tx, term, false);

        // If deletion is enabled, compare old and new terms. Otherwise just save all linked abbreviations.
        if ((deletionEnabled) && (existingTerm != null))
        {
            // Check the abbreviations.
            if (saveAbbreviations)
            {
                // Delete old abbreviations that are no longer linked to the term.
                for (T8OntologyAbbreviation oldAbbreviation : existingTerm.getAbbreviations())
                {
                    T8OntologyAbbreviation newAbbreviation;
                    String abbreviationID;

                    abbreviationID = oldAbbreviation.getID();
                    newAbbreviation = term.getAbbreviation(abbreviationID);
                    if (newAbbreviation == null) deleteAbbreviation(tx, oldAbbreviation);
                }

                // Insert new abbreviations and update existing ones.
                for (T8OntologyAbbreviation newAbbreviation : term.getAbbreviations())
                {
                    T8OntologyAbbreviation oldAbbreviation;
                    String abbreviationID;

                    abbreviationID = newAbbreviation.getID();
                    oldAbbreviation = existingTerm.getAbbreviation(abbreviationID);
                    if (oldAbbreviation == null) insertAbbreviation(tx, newAbbreviation);
                    else updateAbbreviation(tx, newAbbreviation);
                }
            }
        }
        else
        {
            // Just save all linked abbreviations.
            if (saveAbbreviations)
            {
                for (T8OntologyAbbreviation abbreviation : term.getAbbreviations())
                {
                    saveAbbreviation(tx, abbreviation);
                }
            }
        }
    }

    public void saveDefinition(T8DataTransaction tx, T8OntologyDefinition definition) throws Exception
    {
        T8OntologyDefinition existingDefinition;

        // Try to retrieve the object.  If found, update it else insert a new entry.
        existingDefinition = retrieveDefinition(tx, definition.getID());
        if (existingDefinition != null) updateDefinition(tx, definition);
        else insertDefinition(tx, definition);
    }

    public void saveAbbreviation(T8DataTransaction tx, T8OntologyAbbreviation abbreviation) throws Exception
    {
        T8OntologyAbbreviation existingAbbreviation;

        // Try to retrieve the object.  If found, update it else insert a new entry.
        existingAbbreviation = retrieveAbbreviation(tx, abbreviation.getID());
        if (existingAbbreviation != null) updateAbbreviation(tx, abbreviation);
        else insertAbbreviation(tx, abbreviation);
    }

    public void saveCode(T8DataTransaction tx, T8OntologyCode code) throws Exception
    {
        T8OntologyCode existingCode;

        // Try to retrieve the object.  If found, update it else insert a new entry.
        existingCode = retrieveCode(tx, code.getID());
        if (existingCode != null) updateCode(tx, code);
        else insertCode(tx, code);
    }

    public Map<String, String> retrieveConceptIdsByIrdi(T8DataTransaction tx, List<String> irdis) throws Exception
    {
        List<T8DataEntity> conceptEntities;
        Map<String, String> conceptIdMap;
        T8DataFilter dataFilter;
        String entityId;

        entityId = ONTOLOGY_CONCEPT_DE_IDENTIFIER;
        dataFilter = new T8DataFilter(entityId);
        dataFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_IRDI, DataFilterOperator.IN, irdis, false);
        conceptEntities = tx.select(entityId, dataFilter);

        conceptIdMap = new HashMap<>();
        for (T8DataEntity conceptEntity : conceptEntities)
        {
            conceptIdMap.put((String)conceptEntity.getFieldValue(EF_IRDI), (String)conceptEntity.getFieldValue(EF_CONCEPT_ID));
        }

        // Return the retrieved collection.
        return conceptIdMap;
    }

    public Map<String, String> retrieveTermIdsByIrdi(T8DataTransaction tx, List<String> irdis) throws Exception
    {
        List<T8DataEntity> entities;
        Map<String, String> termIdMap;
        T8DataFilter dataFilter;
        String entityId;

        entityId = ONTOLOGY_TERM_DE_IDENTIFIER;
        dataFilter = new T8DataFilter(entityId);
        dataFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_IRDI, DataFilterOperator.IN, irdis, false);
        entities = tx.select(entityId, dataFilter);

        termIdMap = new HashMap<>();
        for (T8DataEntity entity : entities)
        {
            termIdMap.put((String)entity.getFieldValue(EF_IRDI), (String)entity.getFieldValue(EF_TERM_ID));
        }

        // Return the retrieved collection.
        return termIdMap;
    }

    public Map<String, String> retrieveAbbreviationIdsByIrdi(T8DataTransaction tx, List<String> irdis) throws Exception
    {
        List<T8DataEntity> entities;
        Map<String, String> termIdMap;
        T8DataFilter dataFilter;
        String entityId;

        entityId = ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;
        dataFilter = new T8DataFilter(entityId);
        dataFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_IRDI, DataFilterOperator.IN, irdis, false);
        entities = tx.select(entityId, dataFilter);

        termIdMap = new HashMap<>();
        for (T8DataEntity entity : entities)
        {
            termIdMap.put((String)entity.getFieldValue(EF_IRDI), (String)entity.getFieldValue(EF_ABBREVIATION_ID));
        }

        // Return the retrieved collection.
        return termIdMap;
    }

    public Map<String, String> retrieveDefinitionIdsByIrdi(T8DataTransaction tx, List<String> irdis) throws Exception
    {
        List<T8DataEntity> entities;
        Map<String, String> definitionIdMap;
        T8DataFilter dataFilter;
        String entityId;

        entityId = ONTOLOGY_DEFINITION_DE_IDENTIFIER;
        dataFilter = new T8DataFilter(entityId);
        dataFilter.addFilterCriterion(DataFilterConjunction.AND, entityId + EF_IRDI, DataFilterOperator.IN, irdis, false);
        entities = tx.select(entityId, dataFilter);

        definitionIdMap = new HashMap<>();
        for (T8DataEntity entity : entities)
        {
            definitionIdMap.put((String)entity.getFieldValue(EF_IRDI), (String)entity.getFieldValue(EF_DEFINITION_ID));
        }

        // Return the retrieved collection.
        return definitionIdMap;
    }

    private T8DataFilter constructConceptIDFilter(String entityIdentifier, String conceptID)
    {
        return new T8DataFilter(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_CONCEPT_ID, conceptID));
    }

    private T8DataFilter constructTermIDFilter(String entityIdentifier, String termID)
    {
        return new T8DataFilter(entityIdentifier, HashMaps.newHashMap(entityIdentifier + EF_TERM_ID, termID));
    }

    private T8OntologyTerm constructOntologyTerm(T8DataEntity termEntity)
    {
        T8OntologyTerm ontologyTerm;
        String entityIdentifier;
        String conceptId;
        String termId;
        String irdi;
        String languageId;
        String term;
        boolean primaryIndicator;
        boolean standardIndicator;
        boolean activeIndicator;

        // Get required the entity field values.
        entityIdentifier = termEntity.getIdentifier();
        termId = (String)termEntity.getFieldValue(entityIdentifier + EF_TERM_ID);
        irdi = (String)termEntity.getFieldValue(entityIdentifier + EF_IRDI);
        conceptId = (String)termEntity.getFieldValue(entityIdentifier + EF_CONCEPT_ID);
        languageId = (String)termEntity.getFieldValue(entityIdentifier + EF_LANGUAGE_ID);
        term = (String)termEntity.getFieldValue(entityIdentifier + EF_TERM);
        primaryIndicator = "Y".equalsIgnoreCase((String)termEntity.getFieldValue(entityIdentifier + EF_PRIMARY_INDICATOR));
        activeIndicator = "Y".equalsIgnoreCase((String)termEntity.getFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR));
        standardIndicator = "Y".equalsIgnoreCase((String)termEntity.getFieldValue(entityIdentifier + EF_STANDARD_INDICATOR));

        // Return the newly constructed term object.
        ontologyTerm = new T8OntologyTerm(termId, conceptId, languageId, term, primaryIndicator, standardIndicator, activeIndicator);
        ontologyTerm.setIrdi(irdi);
        return ontologyTerm;
    }

    private T8OntologyDefinition constructOntologyDefinition(T8DataEntity definitionEntity)
    {
        T8OntologyDefinition ontologyDefinition;
        String entityId;
        String definitionId;
        String irdi;
        String conceptId;
        String languageId;
        String definition;
        boolean primaryIndicator;
        boolean standardIndicator;
        boolean activeIndicator;

        // Get required the entity field values.
        entityId = definitionEntity.getIdentifier();
        definitionId = (String)definitionEntity.getFieldValue(entityId + EF_DEFINITION_ID);
        irdi = (String)definitionEntity.getFieldValue(entityId + EF_IRDI);
        conceptId = (String)definitionEntity.getFieldValue(entityId + EF_CONCEPT_ID);
        definition = (String)definitionEntity.getFieldValue(entityId + EF_DEFINITION);
        languageId = (String)definitionEntity.getFieldValue(entityId + EF_LANGUAGE_ID);
        primaryIndicator = "Y".equalsIgnoreCase((String)definitionEntity.getFieldValue(entityId + EF_PRIMARY_INDICATOR));
        activeIndicator = "Y".equalsIgnoreCase((String)definitionEntity.getFieldValue(entityId + EF_ACTIVE_INDICATOR));
        standardIndicator = "Y".equalsIgnoreCase((String)definitionEntity.getFieldValue(entityId + EF_STANDARD_INDICATOR));

        // Construct the definition object.
        ontologyDefinition = new T8OntologyDefinition(definitionId, conceptId, languageId, definition, primaryIndicator, standardIndicator, activeIndicator);
        ontologyDefinition.setIrdi(irdi);
        return ontologyDefinition;
    }

    private T8OntologyAbbreviation constructOntologyAbbreviation(T8DataEntity abbreviationEntity)
    {
        T8OntologyAbbreviation ontologyAbbreviation;
        String entityIdentifier;
        String abbreviationID;
        String irdi;
        String termId;
        String abbreviation;
        boolean primaryIndicator;
        boolean standardIndicator;
        boolean activeIndicator;

        // Get required the entity field values.
        entityIdentifier = abbreviationEntity.getIdentifier();
        abbreviationID = (String)abbreviationEntity.getFieldValue(entityIdentifier + EF_ABBREVIATION_ID);
        irdi = (String)abbreviationEntity.getFieldValue(entityIdentifier + EF_IRDI);
        termId = (String)abbreviationEntity.getFieldValue(entityIdentifier + EF_TERM_ID);
        abbreviation = (String)abbreviationEntity.getFieldValue(entityIdentifier + EF_ABBREVIATION);
        primaryIndicator = "Y".equalsIgnoreCase((String)abbreviationEntity.getFieldValue(entityIdentifier + EF_PRIMARY_INDICATOR));
        activeIndicator = "Y".equalsIgnoreCase((String)abbreviationEntity.getFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR));
        standardIndicator = "Y".equalsIgnoreCase((String)abbreviationEntity.getFieldValue(entityIdentifier + EF_STANDARD_INDICATOR));

        // Construct the abbreviation object.
        ontologyAbbreviation = new T8OntologyAbbreviation(abbreviationID, termId, abbreviation, primaryIndicator, standardIndicator, activeIndicator);
        ontologyAbbreviation.setIrdi(irdi);
        return ontologyAbbreviation;
    }

    private T8OntologyCode constructOntologyCode(T8DataEntity codeEntity)
    {
        String entityIdentifier;
        String codeId;
        String codeTypeId;
        String conceptId;
        String code;
        boolean primaryIndicator;
        boolean standardIndicator;
        boolean activeIndicator;

        // Get required the entity field values.
        entityIdentifier = codeEntity.getIdentifier();
        codeId = (String)codeEntity.getFieldValue(entityIdentifier + EF_CODE_ID);
        codeTypeId = (String)codeEntity.getFieldValue(entityIdentifier + EF_CODE_TYPE_ID);
        conceptId = (String)codeEntity.getFieldValue(entityIdentifier + EF_CONCEPT_ID);
        code = (String)codeEntity.getFieldValue(entityIdentifier + EF_CODE);
        primaryIndicator = "Y".equalsIgnoreCase((String)codeEntity.getFieldValue(entityIdentifier + EF_PRIMARY_INDICATOR));
        activeIndicator = "Y".equalsIgnoreCase((String)codeEntity.getFieldValue(entityIdentifier + EF_ACTIVE_INDICATOR));
        standardIndicator = "Y".equalsIgnoreCase((String)codeEntity.getFieldValue(entityIdentifier + EF_STANDARD_INDICATOR));

        // Construct the code object.
        return new T8OntologyCode(codeId, conceptId, codeTypeId, code, primaryIndicator, standardIndicator, activeIndicator);
    }

    private T8OntologyCodeType constructOntologyCodeType(T8DataEntity codeTypeEntity)
    {
        String entityIdentifier;
        String codeTypeID;
        String conceptTypeID;
        String displayName;

        // Get required the entity field values.
        entityIdentifier = codeTypeEntity.getIdentifier();
        codeTypeID = (String)codeTypeEntity.getFieldValue(entityIdentifier + EF_CODE_TYPE_ID);
        conceptTypeID = (String)codeTypeEntity.getFieldValue(entityIdentifier + EF_CONCEPT_TYPE_ID);
        displayName = (String)codeTypeEntity.getFieldValue(entityIdentifier + EF_TERM);

        // Construct the code type object.
        return new T8OntologyCodeType(codeTypeID, T8OntologyConceptType.getConceptType(conceptTypeID), displayName);
    }

    private T8DataEntity constructOntologyConceptEntity(T8DataTransaction tx, String conceptID) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_CONCEPT_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_CONCEPT_ID, conceptID);

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }

    private T8DataEntity constructOntologyConceptEntity(T8DataTransaction tx, T8OntologyConcept ontologyConcept) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_CONCEPT_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_CONCEPT_ID, ontologyConcept.getID());
        fieldValues.put(entityIdentifier + EF_IRDI, ontologyConcept.getIrdi());
        fieldValues.put(entityIdentifier + EF_CONCEPT_TYPE_ID, ontologyConcept.getConceptType().getConceptTypeID());
        fieldValues.put(entityIdentifier + EF_ACTIVE_INDICATOR, ontologyConcept.isActive() ? "Y" : "N");
        fieldValues.put(entityIdentifier + EF_STANDARD_INDICATOR, ontologyConcept.isStandard() ? "Y" : "N");

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }

    private T8DataEntity constructOntologyTermEntity(T8DataTransaction tx, String termID) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_TERM_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_TERM_ID, termID);

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }

    private T8DataEntity constructOntologyTermEntity(T8DataTransaction tx, T8OntologyTerm ontologyTerm) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_TERM_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_TERM_ID, ontologyTerm.getID());
        fieldValues.put(entityIdentifier + EF_IRDI, ontologyTerm.getIrdi());
        fieldValues.put(entityIdentifier + EF_CONCEPT_ID, ontologyTerm.getConceptID());
        fieldValues.put(entityIdentifier + EF_LANGUAGE_ID, ontologyTerm.getLanguageID());
        fieldValues.put(entityIdentifier + EF_TERM, ontologyTerm.getTerm());
        fieldValues.put(entityIdentifier + EF_PRIMARY_INDICATOR, ontologyTerm.isPrimary() ? "Y" : "N");
        fieldValues.put(entityIdentifier + EF_ACTIVE_INDICATOR, ontologyTerm.isActive() ? "Y" : "N");
        fieldValues.put(entityIdentifier + EF_STANDARD_INDICATOR, ontologyTerm.isStandard() ? "Y" : "N");

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }

    private T8DataEntity constructOntologyAbbreviationEntity(T8DataTransaction tx, String abbreviationID) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_ABBREVIATION_ID, abbreviationID);

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }

    private T8DataEntity constructOntologyAbbreviationEntity(T8DataTransaction tx, T8OntologyAbbreviation ontologyAbbreviation) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_ABBREVIATION_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_ABBREVIATION_ID, ontologyAbbreviation.getID());
        fieldValues.put(entityIdentifier + EF_IRDI, ontologyAbbreviation.getIrdi());
        fieldValues.put(entityIdentifier + EF_TERM_ID, ontologyAbbreviation.getTermID());
        fieldValues.put(entityIdentifier + EF_ABBREVIATION, ontologyAbbreviation.getAbbreviation());
        fieldValues.put(entityIdentifier + EF_PRIMARY_INDICATOR, ontologyAbbreviation.isPrimary() ? "Y" : "N");
        fieldValues.put(entityIdentifier + EF_ACTIVE_INDICATOR, ontologyAbbreviation.isActive() ? "Y" : "N");
        fieldValues.put(entityIdentifier + EF_STANDARD_INDICATOR, ontologyAbbreviation.isStandard() ? "Y" : "N");

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }

    private T8DataEntity constructOntologyDefinitionEntity(T8DataTransaction tx, String definitionID) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_DEFINITION_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_DEFINITION_ID, definitionID);

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }

    private T8DataEntity constructOntologyDefinitionEntity(T8DataTransaction tx, T8OntologyDefinition ontologyDefinition) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_DEFINITION_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_DEFINITION_ID, ontologyDefinition.getID());
        fieldValues.put(entityIdentifier + EF_IRDI, ontologyDefinition.getIrdi());
        fieldValues.put(entityIdentifier + EF_CONCEPT_ID, ontologyDefinition.getConceptID());
        fieldValues.put(entityIdentifier + EF_LANGUAGE_ID, ontologyDefinition.getLanguageID());
        fieldValues.put(entityIdentifier + EF_DEFINITION, ontologyDefinition.getDefinition());
        fieldValues.put(entityIdentifier + EF_PRIMARY_INDICATOR, ontologyDefinition.isPrimary() ? "Y" : "N");
        fieldValues.put(entityIdentifier + EF_ACTIVE_INDICATOR, ontologyDefinition.isActive() ? "Y" : "N");
        fieldValues.put(entityIdentifier + EF_STANDARD_INDICATOR, ontologyDefinition.isStandard() ? "Y" : "N");

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }

    private T8DataEntity constructOntologyCodeEntity(T8DataTransaction tx, String codeID) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_CODE_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_CODE_ID, codeID);

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }

    private T8DataEntity constructOntologyCodeEntity(T8DataTransaction tx, T8OntologyCode ontologyCode) throws Exception
    {
        Map<String, Object> fieldValues;
        String entityIdentifier;

        // Get the entity identifier to use.
        entityIdentifier = ONTOLOGY_CODE_DE_IDENTIFIER;

        // Create the map of entity field values.
        fieldValues = new HashMap<>();
        fieldValues.put(entityIdentifier + EF_CODE_ID, ontologyCode.getID());
        fieldValues.put(entityIdentifier + EF_CONCEPT_ID, ontologyCode.getConceptID());
        fieldValues.put(entityIdentifier + EF_CODE_TYPE_ID, ontologyCode.getCodeTypeID());
        fieldValues.put(entityIdentifier + EF_CODE, ontologyCode.getCode());
        fieldValues.put(entityIdentifier + EF_PRIMARY_INDICATOR, ontologyCode.isPrimary() ? "Y" : "N");
        fieldValues.put(entityIdentifier + EF_ACTIVE_INDICATOR, ontologyCode.isActive() ? "Y" : "N");
        fieldValues.put(entityIdentifier + EF_STANDARD_INDICATOR, ontologyCode.isStandard() ? "Y" : "N");

        // Create the entity and return it.
        return tx.create(entityIdentifier, fieldValues);
    }
}
