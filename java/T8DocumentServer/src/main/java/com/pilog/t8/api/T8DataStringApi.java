package com.pilog.t8.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.T8DocumentDataHandler;
import com.pilog.t8.data.document.datastring.T8DataString;
import com.pilog.t8.data.document.datastring.T8DataStringFilter;
import com.pilog.t8.data.document.datastring.T8DataStringGenerator;
import com.pilog.t8.data.document.datastring.T8DataStringInstanceLink;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import java.util.Collection;
import java.util.List;
import com.pilog.t8.data.document.datastring.T8DataStringFormat;
import com.pilog.t8.data.document.datastring.T8DataStringFormatLink;
import com.pilog.t8.data.document.datastring.T8DataStringInstance;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettingsList;
import com.pilog.t8.data.document.datastring.T8DataStringType;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataStringApi implements T8Api, T8PerformanceStatisticsProvider
{
    public static final String API_IDENTIFIER = "@API_DATA_STRING";

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8SessionContext sessionContext;
    private final T8ServerContext serverContext;
    private final T8DataStringApiDataHandler dataHandler;
    private final T8DocumentDataHandler documentDataHandler;
    private T8PerformanceStatistics stats;

    public T8DataStringApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.dataHandler = new T8DataStringApiDataHandler(serverContext, sessionContext);
        this.documentDataHandler = new T8DocumentDataHandler(context);
        this.stats = tx.getPerformanceStatistics(); // Default behviour of all API's is to use the performance statistics object of its parent transaction.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8DataStringGenerator getDataStringGenerator()
    {
        try
        {
            T8DataStringGenerator dsGenerator;

            dsGenerator = new T8DataStringGenerator(tx);
            dsGenerator.setPerformanceStatistics(stats);
            return dsGenerator;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while creating data string generator.", e);
        }
    }

    public List<T8DataString> retrieveRenderings(List<T8DataStringFilter> renderingFilters) throws Exception
    {
        return documentDataHandler.retrieveDataStrings(tx, renderingFilters);
    }

    public T8DataStringFormat retrieveDataStringFormat(String dsFormatId) throws Exception
    {
        return this.dataHandler.retrieveDataStringFormat(tx, dsFormatId);
    }

    /**
     * Retrieves the Data String Instance Links for the specified DR Instances.
     * This method returns a list of link objects but the {@code T8DataStringInstance}
     * object in each link object may be shared with others in the list if
     * more than one link points to the same instance.
     * @param drInstanceIds The DR Instances for which to retrieve links.  If null, all links to all DR Instances will be retrieved.
     * @return The list of retrieve Data String Instance Links.
     * @throws Exception
     */
    public List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> retrieveDataStringInstanceLinkPairs(Collection<String> drInstanceIds) throws Exception
    {
        return documentDataHandler.retrieveDataStringInstanceLinkPairs(tx, sessionContext.getRootOrganizationIdentifier(), drInstanceIds);
    }

    /**
     * Generates all of the concept data required for the data string type to be
     * available in the system. The data string type is also added to the required
     * data string tables and then becomes available for use to set up the actual
     * formatting and detail for the type.
     *
     * @param dataStringType The {@code T8DataStringType} which contains all of the details for the new type.
     * @throws Exception If there is any error during the creation of the new data string type
     */
    public void insertDataStringType(T8DataStringType dataStringType) throws Exception
    {
        T8OrganizationOntologyFactory ontologyFactory;
        T8OntologyConcept rendererConcept;
        T8OntologyApi ontApi;

        // Get the organization API
        ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);

        // Get an instance of the ontology factory to construct the new concept
        ontologyFactory = ontApi.getOntologyFactory();
        ontologyFactory.setConceptODTID(T8PrimaryOntologyDataType.DATA_STRING_TYPES.getDataTypeID());

        // Construct the concept using the factory and add the concept components
        rendererConcept = ontologyFactory.constructConcept(T8OntologyConceptType.DATA_STRING_TYPE);
        ontologyFactory.addTerm(rendererConcept, dataStringType.getRendererTerm());
        ontologyFactory.addAbbreviation(rendererConcept, dataStringType.getRendererAbbreviation());
        ontologyFactory.addDefinition(rendererConcept, dataStringType.getRendererDescription());

        // Add the actual concept to the organization
        ontApi.insertConcept(rendererConcept, true, true, true, false, true);
    }

    public T8DataStringInstance retrieveDataStringInstance(String dsIid) throws Exception
    {
        return dataHandler.retrieveDataStringInstance(tx, dsIid);
    }

    public void saveDataStringInstance(T8DataStringInstance dsInstance) throws Exception
    {
        T8DataStringInstance existingInstance;

        existingInstance = retrieveDataStringInstance(dsInstance.getInstanceID());
        if (existingInstance != null)
        {
            dataHandler.updateDataStringInstance(tx, dsInstance);
        }
        else
        {
            dataHandler.insertDataStringInstance(tx, dsInstance);
        }
    }

    public void insertDataStringInstance(T8DataStringInstance dsInstance) throws Exception
    {
        dataHandler.insertDataStringInstance(tx, dsInstance);
    }

    public void updateDataStringInstance(T8DataStringInstance dsInstance) throws Exception
    {
        dataHandler.updateDataStringInstance(tx, dsInstance);
    }

    public void deleteDataStringInstance(String dsInstanceId, boolean deleteConcept) throws Exception
    {
        dataHandler.deleteDataStringInstance(tx, dsInstanceId);
        if (deleteConcept)
        {
            T8OntologyApi ontApi;

            // Get the organization API
            ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);

            // Delete the concept using the org API
            ontApi.deleteConcept(dsInstanceId);
        }
    }

    public void insertDataStringInstanceLink(T8DataStringInstanceLink link) throws Exception
    {
        dataHandler.insertDataStringInstanceLink(tx, link);
    }

    public void updateDataStringInstanceLink(T8DataStringInstanceLink link) throws Exception
    {
        dataHandler.updateDataStringInstanceLink(tx, link);
    }

    public void saveDataStringInstanceLink(T8DataStringInstanceLink link) throws Exception
    {
        dataHandler.saveDataStringInstanceLink(tx, link);
    }

    public void insertDataString(String recordId, String dsInstanceId, String dataString) throws Exception
    {
        documentDataHandler.insertDataString(tx, recordId, dsInstanceId, dataString);
    }

    public void saveDataString(String recordId, String dsInstanceId, String dataString) throws Exception
    {
        documentDataHandler.saveDataString(tx, recordId, dsInstanceId, dataString);
    }

    public void insertDataStringFormat(T8DataStringFormat format) throws Exception
    {
        dataHandler.insertDataStringFormat(tx, format);
    }

    public void updateDataStringFormat(T8DataStringFormat format) throws Exception
    {
        dataHandler.updateDataStringFormat(tx, format);
    }

    public void saveDataStringFormat(T8DataStringFormat format) throws Exception
    {
        T8DataStringFormat existingFormat;

        existingFormat = retrieveDataStringFormat(format.getDataStringFormatID());
        if (existingFormat == null)
        {
            insertDataStringFormat(format);
        }
        else
        {
            updateDataStringFormat(format);
        }
    }

    /**
     * Deletes the data string format and all associated data string details for
     * the specified format ID. This will delete all format elements, the format
     * links, as well as the format itself.
     *
     * @param dsFormatId The {@code String} ID of the data string format to be deleted.
     * @throws Exception If there is any error during the deletion of the data string format and its associated data.
     */
    public void deleteDataStringFormat(String dsFormatId) throws Exception
    {
        this.dataHandler.deleteDataStringFormat(tx, dsFormatId);
    }

    public void saveDataStringFormatLink(T8DataStringFormatLink formatLink) throws Exception
    {
        T8DataStringFormatLink existingLink;

        existingLink = retrieveDataStringFormatLink(formatLink.getLinkId());
        if (existingLink == null)
        {
            insertDataStringFormatLink(formatLink);
        }
        else
        {
            updateDataStringFormatLink(formatLink);
        }
    }

    /**
     * Deletes a data string format link instance associated with the specified
     * data string format link ID.<br/>
     * <br/>
     * Optionally allows for the deletion of a data string format with the
     * format link ID as the entry point. In this instance, all elements
     * associated with the data string format will be deleted, including all
     * format links linked to the format, and finally, the format itself is
     * deleted.
     *
     * @param linkId The {@code String} ID of the data string format link to
     *      be deleted, or which is used to find the format to be deleted
     * @param deleteFormat {@code true} if the entire format and associated
     *      data should be deleted. {@code false} to delete the format link only
     *
     * @throws Exception If there is any error during the deletion of the format
     *      link and associated data, if any
     */
    public void deleteDataStringFormatLink(String linkId, boolean deleteFormat) throws Exception
    {
        if (deleteFormat)
        {
            T8DataStringFormatLink formatLink;

            formatLink = this.dataHandler.retrieveDataStringFormatLink(tx, linkId);
            deleteDataStringFormat(formatLink.getDsFormatId());
        }
        else
        {
            this.dataHandler.deleteDataStringFormatLink(tx, linkId);
        }
    }

    public void insertDataStringFormatLink(T8DataStringFormatLink link) throws Exception
    {
        dataHandler.insertDataStringFormatLink(tx, link, null);
    }

    public void updateDataStringFormatLink(T8DataStringFormatLink link) throws Exception
    {
        dataHandler.updateDataStringFormatLink(tx, link);
    }

    public T8DataStringFormatLink retrieveDataStringFormatLink(String linkId) throws Exception
    {
        return dataHandler.retrieveDataStringFormatLink(tx, linkId);
    }

    public void saveDataStringParticleSettings(List<T8DataStringParticleSettings> settingsList) throws Exception
    {
        this.dataHandler.saveDataStringParticleSettings(tx, settingsList);
    }

    public void insertDataStringParticleSettings(T8DataStringParticleSettings settings) throws Exception
    {
        this.dataHandler.insertDataStringParticleSettings(tx, settings);
    }

    public boolean updateDataStringParticleSettings(T8DataStringParticleSettings settings) throws Exception
    {
        return dataHandler.updateDataStringParticleSettings(tx, settings);
    }

    public boolean deleteDataStringParticleSettings(T8DataStringParticleSettings settings) throws Exception
    {
        return dataHandler.deleteDataStringParticleSettings(tx, settings);
    }

    public int deleteDataStringParticleSettingsByRecordId(String recordId) throws Exception
    {
        return dataHandler.deleteDataStringParticleSettingsByRecordId(tx, recordId);
    }

    public List<T8DataStringParticleSettings> retrieveDataStringParticleSettings(String recordId) throws Exception
    {
        return dataHandler.retrieveDataStringParticleSettings(tx, sessionContext.getRootOrganizationIdentifier(), recordId);
    }

    public T8DataStringParticleSettingsList retrieveDataStringParticleSettings(Collection<String> drIds, Collection<String> drInstanceIds, Collection<String> recordIds) throws Exception
    {
        return this.dataHandler.retrieveDataStringParticleSettings(tx, sessionContext.getRootOrganizationIdentifier(), drIds, drInstanceIds, recordIds);
    }

    /**
     * Deletes the data strings associated with the specified record ID list
     * and the data string instance ID list. Either the record ID list or the
     * data string instance ID list can be empty, but not both.<br/>
     * <br/>
     * The following scenarios are supported:<br/>
     * <ol>
     * <li>
     * If only record ID's are specified, all data strings associated with those
     * records will be deleted.</li>
     * <li>
     * If only data string instance ID's are specified, all the data string
     * associated with those data string instances will be deleted.<li/>
     * <li>
     * If both record ID's and instance ID's are specified, they are combined,
     * and only the records which have data strings for the specific data string
     * instances will be deleted.</li>
     * </ol>
     *
     * @param recordIds The list of record ID's for which the data strings
     *      should be deleted
     * @param dsIids The {@code List} of data string instance
     *      ID's for which the data strings should be deleted
     *
     * @return The total number of data strings which were deleted, using the
     *      filter values specified
     *
     * @throws IllegalArgumentException If both of the lists are {@code null} or
     *      empty
     * @throws Exception If there is any other error during the deletion of the
     *      required data strings
     */
    public int deleteDataStrings(List<String> recordIds, List<String> dsIids) throws Exception
    {
        if (recordIds == null || recordIds.isEmpty())
        {
            if (dsIids == null || dsIids.isEmpty()) throw new IllegalArgumentException("Have to specify at least 1 Record ID or Data String Instance ID for which data strings should be deleted.");
        }

        return dataHandler.deleteDataStrings(tx, recordIds, dsIids);
    }
}
