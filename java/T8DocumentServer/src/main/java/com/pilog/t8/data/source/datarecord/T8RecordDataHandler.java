package com.pilog.t8.data.source.datarecord;

import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.DocumentHandler;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordSection;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.dataquality.T8RecordDataQualityReport;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute.RequirementAttributeType;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8RecordDataHandler
{
    public static Map<String, Object> getOldRecordRow(List<Map<String, Object>> oldData, Map<String, Object> newRow)
    {
        String recordId;

        recordId = (String)newRow.get("RECORD_ID");
        for (Map<String, Object> oldRow : oldData)
        {
            if (recordId.equals((String)oldRow.get("RECORD_ID")))
            {
                return oldRow;
            }
        }

        return null;
    }

    public static Map<String, Object> getOldPropertyRow(List<Map<String, Object>> oldData, Map<String, Object> newRow)
    {
        String propertyId;

        propertyId = (String)newRow.get("PROPERTY_ID");
        for (Map<String, Object> oldRow : oldData)
        {
            if (propertyId.equals((String)oldRow.get("PROPERTY_ID")))
            {
                return oldRow;
            }
        }

        return null;
    }

    public static Map<String, Object> getOldValueRow(List<Map<String, Object>> oldData, Map<String, Object> newRow)
    {
        String propertyId;
        String dataTypeId;
        Integer dataTypeSeq;
        Integer valueSeq;

        propertyId = (String)newRow.get("PROPERTY_ID");
        dataTypeId = (String)newRow.get("DATA_TYPE_ID");
        dataTypeSeq = (Integer)newRow.get("DATA_TYPE_SEQUENCE");
        valueSeq = (Integer)newRow.get("VALUE_SEQUENCE");
        for (Map<String, Object> oldRow : oldData)
        {
            if (propertyId.equals((String)oldRow.get("PROPERTY_ID")))
            {
                if (dataTypeId.equals((String)oldRow.get("DATA_TYPE_ID")))
                {
                    if (dataTypeSeq.equals((Integer)oldRow.get("DATA_TYPE_SEQUENCE")))
                    {
                        if (valueSeq.equals((Integer)oldRow.get("VALUE_SEQUENCE")))
                        {
                            return oldRow;
                        }
                    }
                }
            }
        }

        return null;
    }

    public static Map<String, Object> getOldAttributeRow(List<Map<String, Object>> oldData, Map<String, Object> newRow)
    {
        String attributeId;

        attributeId = (String)newRow.get("ATTRIBUTE_ID");
        for (Map<String, Object> oldRow : oldData)
        {
            if (attributeId.equals((String)oldRow.get("ATTRIBUTE_ID")))
            {
                return oldRow;
            }
        }

        return null;
    }

    public static List<Map<String, Object>> getInsertedPropertyData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getAddedRows(oldData, newData, new String[]{"PROPERTY_ID"});
    }

    public static List<Map<String, Object>> getUpdatedPropertyData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getUpdatedRows(oldData, newData, new String[]{"PROPERTY_ID"});
    }

    public static List<Map<String, Object>> getDeletedPropertyData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getAddedRows(newData, oldData, new String[]{"PROPERTY_ID"});
    }

    public static List<Map<String, Object>> getInsertedValueData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getAddedRows(oldData, newData, new String[]{"PROPERTY_ID", "DATA_TYPE_ID", "DATA_TYPE_SEQUENCE", "VALUE_SEQUENCE"});
    }

    public static List<Map<String, Object>> getUpdatedValueData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getUpdatedRows(oldData, newData, new String[]{"PROPERTY_ID", "DATA_TYPE_ID", "DATA_TYPE_SEQUENCE", "VALUE_SEQUENCE"});
    }

    public static List<Map<String, Object>> getDeletedValueData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getAddedRows(newData, oldData, new String[]{"PROPERTY_ID", "DATA_TYPE_ID", "DATA_TYPE_SEQUENCE", "VALUE_SEQUENCE"});
    }

    public static List<Map<String, Object>> getInsertedAttachmentData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getAddedRows(oldData, newData, new String[]{"ATTACHMENT_ID"});
    }

    public static List<Map<String, Object>> getDeletedAttachmentData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getAddedRows(newData, oldData, new String[]{"ATTACHMENT_ID"});
    }

    public static List<Map<String, Object>> getInsertedAttributeData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getAddedRows(oldData, newData, new String[]{"ATTRIBUTE_ID"});
    }

    public static List<Map<String, Object>> getUpdatedAttributeData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getUpdatedRows(oldData, newData, new String[]{"ATTRIBUTE_ID"});
    }

    public static List<Map<String, Object>> getDeletedAttributeData(List<Map<String, Object>> oldData, List<Map<String, Object>> newData)
    {
        return CollectionUtilities.getAddedRows(newData, oldData, new String[]{"ATTRIBUTE_ID"});
    }

    public static List<Map<String, Object>> getDataRecordDataRows(DataRecord record)
    {
        List<Map<String, Object>> dataRows;
        Map<String, Object> dataRow;
        T8RecordDataQualityReport dataQualityReport;

        dataQualityReport = record.getDataQualityReport();

        dataRow = new HashMap<>();
        dataRow.put("RECORD_ID", record.getID());
        dataRow.put("ROOT_RECORD_ID", record.getDataFileID());
        dataRow.put("PARENT_RECORD_ID", record.getParentRecordId());
        dataRow.put("DR_ID", record.getDataRequirement() != null ? record.getDataRequirement().getConceptID() : null);
        dataRow.put("DR_INSTANCE_ID", record.getDataRequirementInstance() != null ? record.getDataRequirementInstance().getConceptID() : null);
        dataRow.put("FFT", record.getFFT());
        dataRow.put("PROPERTY_COUNT", dataQualityReport.getPropertyCount());
        dataRow.put("PROPERTY_DATA_COUNT", dataQualityReport.getPropertyDataCount());
        dataRow.put("PROPERTY_VALID_COUNT", dataQualityReport.getPropertyValidCount());
        dataRow.put("PROPERTY_VERIFICATION_COUNT", dataQualityReport.getPropertyVerificationCount());
        dataRow.put("PROPERTY_ACCURATE_COUNT", dataQualityReport.getPropertyAccurateCount());
        dataRow.put("CHRCTRSTC_COUNT", dataQualityReport.getCharacteristicCount());
        dataRow.put("CHRCTRSTC_DATA_COUNT", dataQualityReport.getCharacteristicDataCount());
        dataRow.put("CHRCTRSTC_VALID_COUNT", dataQualityReport.getCharacteristicValidCount());
        dataRow.put("CHRCTRSTC_VERIFICATION_COUNT", dataQualityReport.getCharacteristicVerificationCount());
        dataRow.put("CHRCTRSTC_ACCURATE_COUNT", dataQualityReport.getCharacteristicAccurateCount());

        dataRows = new ArrayList<>();
        dataRows.add(dataRow);
        return dataRows;
    }

    public static List<Map<String, Object>> getDataRecordAttributeDataRows(DataRecord dataRecord)
    {
        List<Map<String, Object>> dataRows;
        Map<String, Object> attributes;

        attributes = dataRecord.getAttributes();
        dataRows = new ArrayList<>();
        for (String attributeID : attributes.keySet())
        {
            HashMap<String, Object> dataRow;
            Object value;

            value = attributes.get(attributeID);

            dataRow = new HashMap<>();
            dataRow.put("RECORD_ID", dataRecord.getID());
            dataRow.put("ROOT_RECORD_ID", dataRecord.getDataFileID());
            dataRow.put("ATTRIBUTE_ID", attributeID);
            dataRow.put("VALUE", value != null ? value.toString() : null);
            if (value instanceof BigDecimal) dataRow.put("TYPE", RequirementAttributeType.NUMBER.toString());
            else if (value instanceof Boolean) dataRow.put("TYPE", RequirementAttributeType.BOOLEAN.toString());
            else dataRow.put("TYPE", RequirementAttributeType.STRING.toString());

            dataRows.add(dataRow);
        }

        return dataRows;
    }

    public static List<Map<String, Object>> getRecordSectionDataRows(DataRecord dataRecord) throws Exception
    {
        List<Map<String, Object>> dataRows;

        dataRows = new ArrayList<>();
        for (RecordSection recordSection : dataRecord.getRecordSections())
        {
            HashMap<String, Object> dataRow;
            DataRecord record;
            SectionRequirement sectionRequirement;

            record = recordSection.getParentDataRecord();
            sectionRequirement = recordSection.getSectionRequirement();

            dataRow = new HashMap<>();
            dataRow.put("RECORD_ID", record.getID());
            dataRow.put("ROOT_RECORD_ID", record.getDataFileID());
            dataRow.put("DR_ID", record.getDataRequirement().getConceptID());
            dataRow.put("SECTION_ID", sectionRequirement.getConceptID());

            dataRows.add(dataRow);
        }

        return dataRows;
    }

    public static List<Map<String, Object>> getRecordPropertyDataRows(DataRecord dataRecord)
    {
        List<Map<String, Object>> dataRows;

        dataRows = new ArrayList<>();
        for (RecordProperty recordProperty : dataRecord.getRecordProperties())
        {
            HashMap<String, Object> dataRow;
            RecordSection recordSection;
            DataRecord record;
            SectionRequirement sectionRequirement;

            recordSection = recordProperty.getParentRecordSection();
            record = recordSection.getParentDataRecord();
            sectionRequirement = recordSection.getSectionRequirement();

            dataRow = new HashMap<>();
            dataRow.put("RECORD_ID", record.getID());
            dataRow.put("ROOT_RECORD_ID", record.getDataFileID());
            dataRow.put("DR_ID", record.getDataRequirement().getConceptID());
            dataRow.put("SECTION_ID", sectionRequirement.getConceptID());
            dataRow.put("PROPERTY_ID", recordProperty.getPropertyRequirement().getConceptID());
            dataRow.put("FFT", recordProperty.getFFT());
            dataRow.put("VALIDITY_INDICATOR", null);

            dataRows.add(dataRow);
        }

        return dataRows;
    }

    public static List<Map<String, Object>> getRecordValueDataRows(DataRecord dataRecord)
    {
        List<Map<String, Object>> dataRows;
        List<RecordValue> recordValues;
        List<String> dataFields;

        // Create a list of all the fields that must exist in the returned rows.
        dataFields = ArrayLists.typeSafeList
        (
            "RECORD_ID",
            "ROOT_RECORD_ID",
            "DR_ID", "SECTION_ID",
            "PROPERTY_ID",
            "FIELD_ID",
            "DATA_TYPE_ID",
            "DATA_TYPE_SEQUENCE",
            "VALUE_SEQUENCE",
            "PARENT_VALUE_SEQUENCE",
            "VALUE",
            "VALUE_CONCEPT_ID",
            "TEXT",
            "FFT",
            "VALUE_ID"
        );

        // Get all record values from the record and from each one create a list of data rows.
        recordValues = DocumentHandler.getAllRecordValues(dataRecord);
        dataRows = new ArrayList<>();
        for (RecordValue recordValue : recordValues)
        {
            List<HashMap<String, Object>> valueRows;

            // Add the extracted data row to the list.
            valueRows = recordValue.getDataRows();
            CollectionUtilities.addMissingKeys(valueRows, dataFields);
            dataRows.addAll(valueRows);
        }

        return dataRows;
    }

    public static List<Map<String, Object>> getRecordAttachmentDataRows(DataRecord dataRecord)
    {
        List<Map<String, Object>> dataRows;
        List<RecordValue> recordValues;

        recordValues = DocumentHandler.getAllRecordValues(dataRecord);
        dataRows = new ArrayList<>();
        for (RecordValue recordValue : recordValues)
        {
            // Only use attachment type values.
            if (recordValue instanceof AttachmentList)
            {
                AttachmentList attachmentList;

                attachmentList = (AttachmentList)recordValue;
                for (String attachmentId : attachmentList.getAttachmentIds())
                {
                    HashMap<String, Object> dataRow;
                    T8FileDetails attachmentDetails;

                    dataRow = new HashMap<>();
                    dataRow.put("ROOT_RECORD_ID", dataRecord.getDataFileID());
                    dataRow.put("RECORD_ID", dataRecord.getID());
                    dataRow.put("ATTACHMENT_ID", attachmentId);
                    dataRow.put("DR_ID", dataRecord.getDataRequirement().getConceptID());
                    dataRow.put("DR_INSTANCE_ID", dataRecord.getDataRequirementInstance().getConceptID());

                    attachmentDetails = attachmentList.getAttachmentDetails(attachmentId);
                    if (attachmentDetails != null)
                    {
                        dataRow.put("CONTEXT_ID", attachmentDetails.getContextId());
                        dataRow.put("FILE_NAME", attachmentDetails.getFileName());
                        dataRow.put("FILE_SIZE", attachmentDetails.getFileSize());
                        dataRow.put("MD5_CHECKSUM", attachmentDetails.getMD5Checksum());
                        dataRow.put("MEDIA_TYPE", attachmentDetails.getMediaType());
                    }

                    dataRows.add(dataRow);
                }
            }
        }

        return dataRows;
    }

    public static DataRecord buildDataRecord(DataRequirementInstance drInstance, Map<String, Object> recordData, List<Map<String, Object>> recordAttributeData, List<Map<String, Object>> propertyData, List<Map<String, Object>> valueData)
    {
        DataRecord newRecord;
        Date insertedAt;
        Date updatedAt;

        // Check auditing fields.
        insertedAt = (Date)recordData.get("INSERTED_AT");
        updatedAt = (Date)recordData.get("UPDATED_AT");

        // Create the new record based on the supplied DR Instance.
        newRecord = new DataRecord(drInstance);

        // Set the data record fields.
        newRecord.setID((String)recordData.get("RECORD_ID"));
        newRecord.setOwnerRecordID((String)recordData.get("ROOT_RECORD_ID"));
        newRecord.setFFT((String)recordData.get("FFT"));
        newRecord.setInsertedByID((String)recordData.get("INSERTED_BY_ID"));
        newRecord.setInsertedByIid((String)recordData.get("INSERTED_BY_IID"));
        newRecord.setInsertedAt(insertedAt != null ? new T8Timestamp(insertedAt.getTime()) : null);
        newRecord.setUpdatedByID((String)recordData.get("UPDATED_BY_ID"));
        newRecord.setUpdatedByIid((String)recordData.get("UPDATED_BY_IID"));
        newRecord.setUpdatedAt(updatedAt != null ? new T8Timestamp(updatedAt.getTime()) : null);

        // Add the Data Record attributes.
        if (recordAttributeData != null)
        {
            for (Map<String, Object> attributeDataRow : recordAttributeData)
            {
                String attributeId;
                String type;
                String value;

                attributeId = (String)attributeDataRow.get("ATTRIBUTE_ID");
                value = (String)attributeDataRow.get("VALUE");
                type = (String)attributeDataRow.get("TYPE");

                if (type == null)
                {
                    newRecord.setAttribute(attributeId, value);
                }
                else
                {
                    switch (type)
                    {
                        case "NUMBER":
                            newRecord.setAttribute(attributeId, new BigDecimal(value));
                            break;
                        case "BOOLEAN":
                            newRecord.setAttribute(attributeId, Boolean.parseBoolean(value));
                            break;
                        default:
                            newRecord.setAttribute(attributeId, value);
                    }
                }
            }
        }

        // Add all the record properties.
        if (propertyData != null)
        {
            for (PropertyRequirement propertyRequirement : newRecord.getDataRequirement().getPropertyRequirements())
            {
                RecordProperty recordProperty;

                recordProperty = buildRecordProperty(propertyRequirement, propertyData, valueData);
                if (recordProperty != null)
                {
                    newRecord.setRecordProperty(recordProperty);
                }
            }
        }

        // Return the complete record.
        return newRecord;
    }

    private static RecordProperty buildRecordProperty(PropertyRequirement propertyRequirement, List<Map<String, Object>> propertyData, List<Map<String, Object>> valueData)
    {
        ArrayList<Map<String, Object>> dataRows;
        HashMap<String, Object> filterValues;

        filterValues = new HashMap<>();
        filterValues.put("PROPERTY_ID", propertyRequirement.getConceptID());
        dataRows = CollectionUtilities.getFilteredDataRows(propertyData, filterValues);
        if (dataRows.size() > 0)
        {
            RecordProperty recordProperty;

            recordProperty = new RecordProperty(propertyRequirement);
            recordProperty.setFFT((String)dataRows.get(0).get("FFT"));

            if ((valueData != null) && (propertyRequirement.getValueRequirement() != null))
            {
                ArrayList<RecordValue> recordValues;

                recordValues = propertyRequirement.getValueRequirement().buildRecordValues(valueData, null);
                if (recordValues.size() > 0)
                {
                    RecordValue rootValue;

                    rootValue = recordValues.get(0);
                    rootValue = transformValue(rootValue);
                    recordProperty.setRecordValue(rootValue);
                }
            }

            return recordProperty;
        }
        else return null;
    }

    /**
     * This method performs transformations on the retrieved record value to
     * ensure that the object model is consistent with the requirements of the
     * code base.
     * @param inputValue The input root value to transform.
     * @return The transformed output root value.
     */
    private static RecordValue transformValue(RecordValue inputValue)
    {
        RequirementType requirementType;

        requirementType = inputValue.getRequirementType();
        if (requirementType == RequirementType.ONTOLOGY_CLASS)
        {
            RecordValue parentValue;

            // Remove the input value from its parent.
            inputValue.removeFromParent();

            // We create a CONTROLLED_CONCEPT parent value and return it as the new root.
            parentValue = RecordValue.createValue(inputValue.getValueRequirement().getParentValueRequirement());
            parentValue.setSubValue(inputValue);
            return parentValue;
        }
        else if (requirementType == RequirementType.COMPOSITE_TYPE)
        {
            // Transform each of the field values individually.
            for (RecordValue field : inputValue.getSubValues())
            {
                RecordValue fieldValue;

                fieldValue = field.getFirstSubValue();
                if (fieldValue != null)
                {
                    field.replaceSubValue(transformValue(fieldValue));
                }
            }

            // Return the input value.
            return inputValue;
        }
        else return inputValue;
    }
}
