package com.pilog.t8.api;

import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.T8DataRequirementComment;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.api.T8DataRequirementApiResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementApiOperations
{
    public static class ApiRetrieveDataRequirement extends T8DefaultServerOperation
    {
        public ApiRetrieveDataRequirement(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRequirementApi drqApi;
            DataRequirement dr;
            String drId;
            String languageId;
            Boolean includeOntology;
            Boolean includeTerminology;

            // Get the operation parameters we need.
            drId = (String)operationParameters.get(PARAMETER_DR_ID);
            languageId = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);
            includeOntology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_ONTOLOGY);
            includeTerminology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY);

            // Perform the API operation.
            drqApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            dr = drqApi.retrieveDataRequirement(drId, languageId, includeOntology != null && includeOntology, includeTerminology != null && includeTerminology);
            return HashMaps.createSingular(PARAMETER_DR, dr);
        }
    }

    public static class ApiRetrieveDataRequirementInstance extends T8DefaultServerOperation
    {
        public ApiRetrieveDataRequirementInstance(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRequirementApi drqApi;
            DataRequirementInstance drInstance;
            String drInstanceId;
            String languageId;
            Boolean includeOntology;
            Boolean includeTerminology;

            // Get the operation parameters we need.
            drInstanceId = (String)operationParameters.get(PARAMETER_DR_IID);
            languageId = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);
            includeOntology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_ONTOLOGY);
            includeTerminology = (Boolean)operationParameters.get(PARAMETER_INCLUDE_TERMINOLOGY);

            // Perform the API operation.
            drqApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            drInstance = drqApi.retrieveDataRequirementInstance(drInstanceId, languageId, includeOntology != null && includeOntology, includeTerminology != null && includeTerminology);
            return HashMaps.createSingular(PARAMETER_DRI, drInstance);
        }
    }

    public static class ApiRetrieveDataRequirementComments extends T8DefaultServerOperation
    {
        public ApiRetrieveDataRequirementComments(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataRequirementComment>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRequirementApi drqApi;
            List<String> orgIds;
            List<String> languageIds;
            List<String> drIds;
            List<String> drInstanceIds;
            List<String> commentTypeIds;
            List<T8DataRequirementComment> comments;

            // Get the operation parameters we need.
            orgIds = (List<String>)operationParameters.get(PARAMETER_ORG_IDS);
            languageIds = (List<String>)operationParameters.get(PARAMETER_LANGUAGE_IDS);
            drIds = (List<String>)operationParameters.get(PARAMETER_DR_IDS);
            drInstanceIds = (List<String>)operationParameters.get(PARAMETER_DR_IIDS);
            commentTypeIds = (List<String>)operationParameters.get(PARAMETER_COMMENT_TYPE_IDS);

            // Perform the API operation.
            drqApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            comments = drqApi.retrieveDataRequirementComments(orgIds, languageIds, drIds, drInstanceIds, commentTypeIds);
            return HashMaps.createSingular(PARAMETER_DR_COMMENTS, comments);
        }
    }

    public static class ApiInsertDataRequirementInstance extends T8DefaultServerOperation
    {
        public ApiInsertDataRequirementInstance(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataRequirementComment>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRequirementApi drqApi;
            T8OntologyConcept concept;
            String drId;
            Boolean independent;

            // Get the operation parameters we need.
            concept = (T8OntologyConcept)operationParameters.get(PARAMETER_CONCEPT);
            drId = (String)operationParameters.get(PARAMETER_DR_ID);
            independent = (Boolean)operationParameters.get(PARAMETER_INDEPENDENT);

            // Perform the API operation.
            drqApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            drqApi.insertDataRequirementInstance(concept, drId, independent != null && independent);
            return null;
        }
    }

    public static class ApiSaveDataRequirementInstance extends T8DefaultServerOperation
    {
        public ApiSaveDataRequirementInstance(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataRequirementComment>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRequirementApi drqApi;
            T8OntologyConcept concept;
            String drId;
            Boolean independent;

            // Get the operation parameters we need.
            concept = (T8OntologyConcept)operationParameters.get(PARAMETER_CONCEPT);
            drId = (String)operationParameters.get(PARAMETER_DR_ID);
            independent = (Boolean)operationParameters.get(PARAMETER_INDEPENDENT);

            // Perform the API operation.
            drqApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            drqApi.saveDataRequirementInstance(concept, drId, independent != null && independent);
            return null;
        }
    }

    public static class ApiSaveDataRequirement extends T8DefaultServerOperation
    {
        public ApiSaveDataRequirement(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataRequirementComment>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRequirementApi drqApi;
            DataRequirement dr;
            Boolean saveOntology;

            // Get the operation parameters we need.
            dr = (DataRequirement)operationParameters.get(PARAMETER_DR);
            saveOntology = (Boolean)operationParameters.get(PARAMETER_SAVE_ONTOLOGY);

            // Perform the API operation.
            drqApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            drqApi.saveDataRequirement(dr, saveOntology != null && saveOntology);
            return null;
        }
    }

    public static class ApiDeleteDataRequirementInstance extends T8DefaultServerOperation
    {
        public ApiDeleteDataRequirementInstance(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataRequirementComment>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRequirementApi drqApi;
            String drInstanceId;
            Boolean deleteOntology;

            // Get the operation parameters we need.
            drInstanceId = (String)operationParameters.get(PARAMETER_DR_IID);
            deleteOntology = (Boolean)operationParameters.get(PARAMETER_DELETE_ONTOLOGY);

            // Perform the API operation.
            drqApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            drqApi.deleteDataRequirementInstance(drInstanceId, deleteOntology != null && deleteOntology);
            return null;
        }
    }

    public static class ApiDeleteDataRequirement extends T8DefaultServerOperation
    {
        public ApiDeleteDataRequirement(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataRequirementComment>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataRequirementApi drqApi;
            Boolean deleteOntology;
            String drId;

            // Get the operation parameters we need.
            drId = (String)operationParameters.get(PARAMETER_DR_ID);
            deleteOntology = (Boolean)operationParameters.get(PARAMETER_DELETE_ONTOLOGY);

            // Perform the API operation.
            drqApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            drqApi.deleteDataRequirement(drId, deleteOntology != null && deleteOntology);
            return null;
        }
    }
}
