package com.pilog.t8.api;

import com.pilog.t8.data.document.datastring.T8DataString;
import com.pilog.t8.data.document.datastring.T8DataStringFilter;
import com.pilog.t8.data.document.datastring.T8DataStringFormat;
import com.pilog.t8.data.document.datastring.T8DataStringFormatLink;
import com.pilog.t8.data.document.datastring.T8DataStringInstance;
import com.pilog.t8.data.document.datastring.T8DataStringInstanceLink;
import com.pilog.t8.data.document.datastring.T8DataStringParticleSettings;
import com.pilog.t8.data.document.datastring.T8DataStringType;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.definition.api.T8DataStringApiResource.*;

/**
 * @author Pieter Strydom
 */
public class T8DataStringApiOperations
{
    public static class ApiRetrieveRenderings extends T8DefaultServerOperation
    {
        public ApiRetrieveRenderings(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataString>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            List<T8DataStringFilter> renderingFilters;
            List<T8DataString> dataStringList;

            // Get the operation parameters we need.
            renderingFilters = (List<T8DataStringFilter>)operationParameters.get(PARAMETER_DATA_STRING_FILTER_LIST);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dataStringList = dsApi.retrieveRenderings(renderingFilters);
            return HashMaps.createSingular(PARAMETER_DATA_STRING_LIST, dataStringList);
        }
    }

    public static class ApiRetrieveDataStringFormat extends T8DefaultServerOperation
    {
        public ApiRetrieveDataStringFormat(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8DataStringFormat> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            String dsFormatId;
            T8DataStringFormat dsFormat;

            // Get the operation parameters we need.
            dsFormatId = (String)operationParameters.get(PARAMETER_DATA_STRING_FORMAT_ID);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsFormat = dsApi.retrieveDataStringFormat(dsFormatId);
            return HashMaps.createSingular(PARAMETER_DATA_STRING_FORMAT, dsFormat);
        }
    }

    public static class ApiDeleteDataStringFormat extends T8DefaultServerOperation
    {
        public ApiDeleteDataStringFormat(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            String dsFormatId;

            // Get the operation parameters we need.
            dsFormatId = (String)operationParameters.get(PARAMETER_DATA_STRING_FORMAT_ID);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.deleteDataStringFormat(dsFormatId);
            return null;
        }
    }

    public static class ApiDeleteDataStringFormatLink extends T8DefaultServerOperation
    {
        public ApiDeleteDataStringFormatLink(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            Boolean deleteFormat;
            String linkId;

            // Get the operation parameters we need.
            deleteFormat = (Boolean)operationParameters.get(PARAMETER_DELETE_FORMAT);
            linkId = (String)operationParameters.get(PARAMETER_DATA_STRING_FORMAT_LINK_ID);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.deleteDataStringFormatLink(linkId, deleteFormat != null && deleteFormat);
            return null;
        }
    }

    public static class ApiDeleteDataStringInstance extends T8DefaultServerOperation
    {
        public ApiDeleteDataStringInstance(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            Boolean deleteConcept;
            String dataStringInstanceId;

            // Get the operation parameters we need.
            deleteConcept = (Boolean)operationParameters.get(PARAMETER_DELETE_CONCEPT);
            dataStringInstanceId = (String)operationParameters.get(PARAMETER_DATA_STRING_INSTANCE_ID);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.deleteDataStringInstance(dataStringInstanceId, deleteConcept != null && deleteConcept);
            return null;
        }
    }

    public static class ApiDeleteDataStrings extends T8DefaultServerOperation
    {
        public ApiDeleteDataStrings(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Integer> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            List<String> recordIdList;
            List<String> dataStringIidList;
            int deletedCount;

            // Get the operation parameters we need.
            recordIdList = (List<String>)operationParameters.get(PARAMETER_RECORD_ID_LIST);
            dataStringIidList = (List<String>)operationParameters.get(PARAMETER_DATA_STRING_INSTANCE_ID_LIST);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            deletedCount = dsApi.deleteDataStrings(recordIdList, dataStringIidList);
            return HashMaps.createSingular(PARAMETER_DELETE_COUNT, deletedCount);
        }
    }

    public static class ApiInsertDataStringType extends T8DefaultServerOperation
    {
        public ApiInsertDataStringType(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringType dsType;
            T8DataStringApi dsApi;

            // Get the operation parameters we need.
            dsType = (T8DataStringType)operationParameters.get(PARAMETER_DATA_STRING_TYPE);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.insertDataStringType(dsType);
            return null;
        }
    }

    public static class ApiRetrieveDataStringInstanceLinkPairs extends T8DefaultServerOperation
    {
        public ApiRetrieveDataStringInstanceLinkPairs(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<Pair<T8DataStringInstanceLink, T8DataStringInstance>>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            Collection<String> dsInstanceIdList;
            List<Pair<T8DataStringInstanceLink, T8DataStringInstance>> dsInstanceLinkPairs;

            // Get the operation parameters we need.
            dsInstanceIdList = (Collection<String>)operationParameters.get(PARAMETER_DR_INSTANCE_ID_LIST);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsInstanceLinkPairs = dsApi.retrieveDataStringInstanceLinkPairs(dsInstanceIdList);
            return HashMaps.createSingular(PARAMETER_DATA_STRING_INSTANCE_LINK_LIST, dsInstanceLinkPairs);
        }
    }

    public static class ApiRetrieveDataStringFormatLink extends T8DefaultServerOperation
    {
        public ApiRetrieveDataStringFormatLink(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8DataStringFormatLink> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            String dsFormatLinkId;
            T8DataStringFormatLink dsFormatLink;

            // Get the operation parameters we need.
            dsFormatLinkId = (String)operationParameters.get(PARAMETER_DATA_STRING_FORMAT_LINK_ID);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsFormatLink = dsApi.retrieveDataStringFormatLink(dsFormatLinkId);
            return HashMaps.createSingular(PARAMETER_DATA_STRING_FORMAT_LINK, dsFormatLink);
        }
    }

    public static class ApiRetrieveDataStringParticleSettings extends T8DefaultServerOperation
    {
        public ApiRetrieveDataStringParticleSettings(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            String recordId;
            Set<String> drIdSet;
            Set<String> drInstanceIdSet;
            Set<String> recordIdSet;
            Object dsParticleSettingsList;

            // Get the operation parameters we need.
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);
            drIdSet = (Set<String>)operationParameters.get(PARAMETER_DR_ID_LIST);
            drInstanceIdSet = (Set<String>)operationParameters.get(PARAMETER_DR_INSTANCE_ID_LIST);
            recordIdSet = (Set<String>)operationParameters.get(PARAMETER_RECORD_ID_LIST);

            // Check if the record id is not null, and call the correct method.
            if (recordId != null)
            {
                // Perform the API operation.
                dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
                dsParticleSettingsList = dsApi.retrieveDataStringParticleSettings(recordId);
            }
            else
            {
                // Perform the API operation.
                dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
                dsParticleSettingsList = dsApi.retrieveDataStringParticleSettings(drIdSet, drInstanceIdSet, recordIdSet);
            }

            return HashMaps.createSingular(PARAMETER_DATA_STRING_PARTICLE_SETTINGS_LIST, dsParticleSettingsList);
        }
    }

    public static class ApiInsertDataStringInstanceLink extends T8DefaultServerOperation
    {
        public ApiInsertDataStringInstanceLink(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringInstanceLink dsInstanceLink;

            // Get the operation parameters we need.
            dsInstanceLink = (T8DataStringInstanceLink)operationParameters.get(PARAMETER_DATA_STRING_INSTANCE_LINK);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.insertDataStringInstanceLink(dsInstanceLink);
            return null;
        }
    }

    public static class ApiInsertDataString extends T8DefaultServerOperation
    {
        public ApiInsertDataString(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            String recordId;
            String dsInstanceId;
            String dataString;

            // Get the operation parameters we need.
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);
            dsInstanceId = (String)operationParameters.get(PARAMETER_DATA_STRING_INSTANCE_ID);
            dataString = (String)operationParameters.get(PARAMETER_DATA_STRING);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.insertDataString(recordId, dsInstanceId, dataString);
            return null;
        }
    }

    public static class ApiInsertDataStringFormat extends T8DefaultServerOperation
    {
        public ApiInsertDataStringFormat(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringFormat dsFormat;

            // Get the operation parameters we need.
            dsFormat = (T8DataStringFormat)operationParameters.get(PARAMETER_DATA_STRING_FORMAT);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.insertDataStringFormat(dsFormat);
            return null;
        }
    }

    public static class ApiInsertDataStringFormatLink extends T8DefaultServerOperation
    {
        public ApiInsertDataStringFormatLink(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringFormatLink dsFormatLink;

            // Get the operation parameters we need.
            dsFormatLink = (T8DataStringFormatLink)operationParameters.get(PARAMETER_DATA_STRING_FORMAT_LINK);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.insertDataStringFormatLink(dsFormatLink);
            return null;
        }
    }

    public static class ApiInsertDataStringParticleSettings extends T8DefaultServerOperation
    {
        public ApiInsertDataStringParticleSettings(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringParticleSettings dsParticleSettings;

            // Get the operation parameters we need.
            dsParticleSettings = (T8DataStringParticleSettings)operationParameters.get(PARAMETER_DATA_STRING_PARTICLE_SETTINGS);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.insertDataStringParticleSettings(dsParticleSettings);
            return null;
        }
    }

    public static class ApiUpdateDataStringInstanceLink extends T8DefaultServerOperation
    {
        public ApiUpdateDataStringInstanceLink(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringInstanceLink dsInstanceLink;

            // Get the operation parameters we need.
            dsInstanceLink = (T8DataStringInstanceLink)operationParameters.get(PARAMETER_DATA_STRING_INSTANCE_LINK);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.updateDataStringInstanceLink(dsInstanceLink);
            return null;
        }
    }

    public static class ApiUpdateDataStringFormat extends T8DefaultServerOperation
    {
        public ApiUpdateDataStringFormat(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringFormat dsFormat;

            // Get the operation parameters we need.
            dsFormat = (T8DataStringFormat)operationParameters.get(PARAMETER_DATA_STRING_FORMAT);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.updateDataStringFormat(dsFormat);
            return null;
        }
    }

    public static class ApiUpdateDataStringFormatLink extends T8DefaultServerOperation
    {
        public ApiUpdateDataStringFormatLink(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringFormatLink dsFormatLink;

            // Get the operation parameters we need.
            dsFormatLink = (T8DataStringFormatLink)operationParameters.get(PARAMETER_DATA_STRING_FORMAT_LINK);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.updateDataStringFormatLink(dsFormatLink);
            return null;
        }
    }

    public static class ApiUpdateDataStringParticleSettings extends T8DefaultServerOperation
    {
        public ApiUpdateDataStringParticleSettings(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringParticleSettings dsParticleSettings;
            Boolean isUpdated;

            // Get the operation parameters we need.
            dsParticleSettings = (T8DataStringParticleSettings)operationParameters.get(PARAMETER_DATA_STRING_PARTICLE_SETTINGS);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            isUpdated = dsApi.updateDataStringParticleSettings(dsParticleSettings);
            return HashMaps.createSingular(PARAMETER_IS_UPDATED, isUpdated);
        }
    }

    public static class ApiSaveDataStringInstanceLink extends T8DefaultServerOperation
    {
        public ApiSaveDataStringInstanceLink(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringInstanceLink dsInstanceLink;

            // Get the operation parameters we need.
            dsInstanceLink = (T8DataStringInstanceLink)operationParameters.get(PARAMETER_DATA_STRING_INSTANCE_LINK);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.saveDataStringInstanceLink(dsInstanceLink);
            return null;
        }
    }

    public static class ApiSaveDataString extends T8DefaultServerOperation
    {
        public ApiSaveDataString(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            String recordId;
            String dsInstanceId;
            String dataString;

            // Get the operation parameters we need.
            recordId = (String)operationParameters.get(PARAMETER_RECORD_ID);
            dsInstanceId = (String)operationParameters.get(PARAMETER_DATA_STRING_INSTANCE_ID);
            dataString = (String)operationParameters.get(PARAMETER_DATA_STRING);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.saveDataString(recordId, dsInstanceId, dataString);
            return null;
        }
    }

    public static class ApiSaveDataStringFormat extends T8DefaultServerOperation
    {
        public ApiSaveDataStringFormat(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringFormat dsFormat;

            // Get the operation parameters we need.
            dsFormat = (T8DataStringFormat)operationParameters.get(PARAMETER_DATA_STRING_FORMAT);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.saveDataStringFormat(dsFormat);
            return null;
        }
    }

    public static class ApiSaveDataStringFormatLink extends T8DefaultServerOperation
    {
        public ApiSaveDataStringFormatLink(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringFormatLink dsFormatLink;

            // Get the operation parameters we need.
            dsFormatLink = (T8DataStringFormatLink)operationParameters.get(PARAMETER_DATA_STRING_FORMAT_LINK);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.saveDataStringFormatLink(dsFormatLink);
            return null;
        }
    }

    public static class ApiSaveDataStringParticleSettings extends T8DefaultServerOperation
    {
        public ApiSaveDataStringParticleSettings(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            List<T8DataStringParticleSettings> dsParticleSettingsList;

            // Get the operation parameters we need.
            dsParticleSettingsList = (List<T8DataStringParticleSettings>)operationParameters.get(PARAMETER_DATA_STRING_PARTICLE_SETTINGS_LIST);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.saveDataStringParticleSettings(dsParticleSettingsList);
            return null;
        }
    }

    public static class ApiSaveDataStringInstance extends T8DefaultServerOperation
    {
        public ApiSaveDataStringInstance(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringInstance dsInstance;
            T8DataStringApi dsApi;

            // Get the operation parameters we need.
            dsInstance = (T8DataStringInstance)operationParameters.get(PARAMETER_DATA_STRING_INSTANCE);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            dsApi.saveDataStringInstance(dsInstance);
            return null;
        }
    }

    public static class ApiDeleteDataStringParticleSettings extends T8DefaultServerOperation
    {
        public ApiDeleteDataStringParticleSettings(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataStringApi dsApi;
            T8DataStringParticleSettings dsParticleSettings;
            Boolean isDeleted;

            // Get the operation parameters we need.
            dsParticleSettings = (T8DataStringParticleSettings)operationParameters.get(PARAMETER_DATA_STRING_PARTICLE_SETTINGS);

            // Perform the API operation.
            dsApi = tx.getApi(T8DataStringApi.API_IDENTIFIER);
            isDeleted = dsApi.deleteDataStringParticleSettings(dsParticleSettings);
            return HashMaps.createSingular(PARAMETER_IS_DELETED, isDeleted);
        }
    }
}
