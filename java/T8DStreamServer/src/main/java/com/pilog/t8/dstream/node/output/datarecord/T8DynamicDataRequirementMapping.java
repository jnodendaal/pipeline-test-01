package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceChecker;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceError;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.value.Composite;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarecord.value.MeasuredNumber;
import com.pilog.t8.data.document.datarecord.value.MeasuredRange;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredRangeRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DefaultValueRequirementMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DynamicDataRequirementMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8ValueRequirementMappingDefinition;
import com.pilog.t8.utilities.strings.FormattedString;
import com.pilog.t8.utilities.strings.Strings;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DynamicDataRequirementMapping extends T8AbstractDataRecordCreationMapping implements T8DataRecordCreationMapping
{
    private final T8DynamicDataRequirementMappingDefinition definition;
    private final T8DataRecordValueStringGenerator fftValueStringGenerator;
    private final String propertyIDParameterIdentifier;
    private final String fieldIDParameterIdentifier;
    private final String recordFFTParameterIdentifier;
    private final String propertyFFTParameterIdentifier;
    private final String valueParameterId;
    private final String conceptIdParameterId;
    private final String conceptTermParameterId;
    private final String conceptCodeParameterId;
    private final String lowerValueParameterId;
    private final String upperValueParameterId;
    private final String uomIdParameterId;
    private final String uomTermParameterId;
    private final String uomCodeParameterId;
    private final String qomIdParameterId;
    private final String qomTermParameterId;
    private final String qomCodeParameterId;
    private TerminologyProvider terminologyProvider;
    private final List<String> keyParameterIdentifiers;
    private final Map<Integer, DataRecord> recordConstructionCache;
    private String constructionRootRecordID;

    public T8DynamicDataRequirementMapping(T8DataRecordCreationContext creationContext, T8DynamicDataRequirementMappingDefinition definition)
    {
        super(creationContext, definition);
        this.definition = definition;
        this.fftValueStringGenerator = creationContext.getFFTValueStringGenerator();
        this.recordFFTParameterIdentifier = definition.getRecordFFTParameterIdentifier();
        this.propertyFFTParameterIdentifier = definition.getPropertyFFTParameterIdentifier();
        this.propertyIDParameterIdentifier = definition.getPropertyIDParameterIdentifier();
        this.fieldIDParameterIdentifier = definition.getFieldIDParameterIdentifier();
        this.valueParameterId = definition.getValueParameterIdentifier();
        this.conceptIdParameterId = definition.getConceptIDParameterIdentifier();
        this.conceptTermParameterId = definition.getConceptTermParameterIdentifier();
        this.conceptCodeParameterId = definition.getConceptCodeParameterIdentifier();
        this.lowerValueParameterId = definition.getLowerValueParameterIdentifier();
        this.upperValueParameterId = definition.getUpperValueParameterIdentifier();
        this.uomIdParameterId = definition.getUOMIDParameterIdentifier();
        this.uomTermParameterId = definition.getUOMTermParameterIdentifier();
        this.uomCodeParameterId = definition.getUOMCodeParameterIdentifier();
        this.qomIdParameterId = definition.getQOMIDParameterIdentifier();
        this.qomTermParameterId = definition.getQOMTermParameterIdentifier();
        this.qomCodeParameterId = definition.getQOMCodeParameterIdentifier();
        this.recordConstructionCache = new HashMap<Integer, DataRecord>();
        this.keyParameterIdentifiers = definition.getKeyParameterIdentifiers();
    }

    @Override
    public boolean isApplicable(DataRequirementInstance dataRequirementInstance, Map<String, Object> dataParameters)
    {
        // This mapping is applicable to all data requirement types.
        return true;
    }

    @Override
    public DataRecordConstructionResult constructDataRecord(T8DataTransaction tx, String inputRootRecordID, DataRecord parentRecord, DataRequirementInstance dataRequirementInstance, Map<String, Object> dataParameters) throws Exception
    {
        int matchingHash;

        // Generate a matching hashcode for the input parameters.
        matchingHash = createMatchingHashCode(parentRecord, keyParameterIdentifiers, dataParameters);
        if (matchingHash != 0) // If the matching hash is 0, no content was found and the row must be skipped.
        {
            List<T8DataRecordConformanceError> conformanceFailures;
            T8DataRecordConformanceChecker conformanceChecker;
            PropertyRequirement propertyRequirement;
            DataRequirement dataRequirement;
            DataRecord dataRecord;
            String recordFFT;
            String rootRecordID;

            String propertyID;

            // Get the Data Requirement from the input instance.
            dataRequirement = dataRequirementInstance.getDataRequirement();

            // Check the construction cache to see if this record has already been constructed.
            dataRecord = recordConstructionCache.get(matchingHash);
            if (dataRecord == null)
            {
                // Record has not been constructed yet, so create the new record and add it to the cache.
                dataRecord = new DataRecord(dataRequirementInstance);
                dataRecord.setID(T8IdentifierUtilities.createNewGUID());
                recordConstructionCache.put(matchingHash, dataRecord);
            }

            // Set the root record ID (if the one supplied to the method is null, it means we are constructing the root).
            rootRecordID = inputRootRecordID != null ? inputRootRecordID : dataRecord.getID();

            // Clear the cache if we are constructing a new root.
            if (!rootRecordID.equals(constructionRootRecordID))
            {
                constructionRootRecordID = rootRecordID;
                recordConstructionCache.clear(); // Clear the cache.
                recordConstructionCache.put(matchingHash, dataRecord); // We have to add the constructed record back in.
            }

            // Create a collection for conformance errors.
            conformanceFailures = new ArrayList<T8DataRecordConformanceError>();

            // Add the record FFT.
            recordFFT = (String)dataParameters.get(recordFFTParameterIdentifier);
            dataRecord.appendFFTChecked(recordFFT, ",");

            // Get thr property ID to set a value for.
            stats.logExecutionStart(PE_RECORD_PROPERTY_CONSTRUCTION);
            propertyID = (String)dataParameters.get(propertyIDParameterIdentifier);
            propertyRequirement = dataRequirement.getPropertyRequirement(propertyID);
            if (propertyRequirement != null)
            {
                RecordValueConstructionResult recordValueConstructionResult = null;
                ValueRequirement valueRequirement;
                String propertyFFT;

                // Get the property FFT value from the data parameters.
                propertyFFT = (String)dataParameters.get(propertyFFTParameterIdentifier);

                // Construct the property value.
                stats.logExecutionStart(PE_RECORD_VALUE_CONSTRUCTION);
                valueRequirement = propertyRequirement.getValueRequirement();
                if (valueRequirement != null)
                {
                    // Construct the value.
                    stats.logExecutionStart(PE_RECORD_VALUE_CONSTRUCTION);
                    recordValueConstructionResult = constructRecordValue(tx, valueRequirement, dataParameters, dataRecord);
                    stats.logExecutionEnd(PE_RECORD_VALUE_CONSTRUCTION);
                }
                stats.logExecutionEnd(PE_RECORD_VALUE_CONSTRUCTION);

                // Process the construction result.
                if (recordValueConstructionResult != null)
                {
                    Map<String, T8AttachmentDetails> attachmentDetails;
                    List<DataRecord> subRecords;
                    RecordValue recordValue;

                    recordValue = recordValueConstructionResult.getRecordValue();
                    subRecords = recordValueConstructionResult.getSubRecords();
                    attachmentDetails = recordValueConstructionResult.getAttachmentDetails();
                    conformanceFailures.addAll(recordValueConstructionResult.getConformanceErrors());

                    // Add all sub-records created during the value construction.
                    if ((subRecords != null) && (recordValue != null))
                    {
                        // Add all newly created sub-records.
                        for (DataRecord subRecord : subRecords)
                        {
                            // We may have already added this record, so check first.
                            if (!dataRecord.containsSubRecord(subRecord.getID()))
                            {
                                dataRecord.addSubRecord(subRecord);
                            }
                        }
                    }

                    // Add all attachment details created during the value construction.
                    if ((attachmentDetails != null) && (recordValue != null))
                    {
                        dataRecord.addAttachmentDetails(attachmentDetails);
                    }

                    // If no value was successfully created for the property, create a String concatenation of all input data to be added to the property FFT.
                    if (recordValue == null)
                    {
                        // Create an FFT value for all mapped values.
                    }
                    else // Add the FFT from the value to the property.
                    {
                        FormattedString valueFFT;

                        valueFFT = new FormattedString();
                        valueFFT.append(recordValue.getFFT());
                        for (RecordValue descendantValue : recordValue.getDescendantValues())
                        {
                            valueFFT.append(descendantValue.getFFT());
                        }

                        if (valueFFT.length() > 0)
                        {
                            if (propertyFFT == null) propertyFFT = valueFFT.toString();
                            else propertyFFT += (" " + valueFFT.toString());
                        }
                    }

                    // We only add the property if a value has been constructed for it or if a non-null FFT value exists.
                    if ((recordValue != null) || (propertyFFT != null))
                    {
                        RecordProperty recordProperty;

                        recordProperty = dataRecord.getRecordProperty(propertyID);
                        if (recordProperty == null)
                        {
                            recordProperty = new RecordProperty(propertyRequirement);
                            recordProperty.setRecordValue(recordValue);
                            recordProperty.setFFT(propertyFFT);
                            dataRecord.setRecordProperty(recordProperty);
                        }
                        else
                        {
                            if (recordValue instanceof DocumentReferenceList)
                            {
                                DocumentReferenceList existingReferenceList;
                                DocumentReferenceList newReferenceList;

                                existingReferenceList = (DocumentReferenceList)recordProperty.getRecordValue();
                                newReferenceList = (DocumentReferenceList)recordValue;
                                for (String newReferenceRecordID : newReferenceList.getReferenceIds())
                                {
                                    if (!existingReferenceList.containsReference(newReferenceRecordID))
                                    {
                                        existingReferenceList.addReference(newReferenceRecordID);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            stats.logExecutionEnd(PE_RECORD_PROPERTY_CONSTRUCTION);

            // If we found a valid section mapping and a section was added to the record, proceed with the rest of the record construction.
            if (dataRecord.hasContent(true))
            {
                Set<String> referencedRecordIDSet;
                Set<String> createdRecordIDSet;

                // Log statistics.
                stats.logExecutionStart(PE_RECORD_CONFORMANCE_CHECKING);

                // Construct the tags and add them to the record.
                dataRecord.setAttributes(constructAttributes(definition.getAttributeMappingDefinitions(), dataParameters));

                // Add all mapped codes to the record ontology.
                addOntologyCodes(definition.getCodeMappingDefinitions(), dataParameters, dataRecord);

                // Check the record's conformance to its DR so that all invalid data is moved to FFT.
                conformanceChecker = new T8DataRecordConformanceChecker(fftValueStringGenerator);
                conformanceFailures.addAll(conformanceChecker.moveInvalidDataToFFT(dataRecord));

                // Normalize the record.
                dataRecord.normalize();

                // Make sure that all dependent records that are referenced, have been added to the object model.
                referencedRecordIDSet = dataRecord.getRecordReferenceIDSet(true, false);
                createdRecordIDSet = dataRecord.getDataRecordIDSet();
                referencedRecordIDSet.removeAll(createdRecordIDSet);
                if (referencedRecordIDSet.size() > 0) throw new RuntimeException("Constructed Data Record '" + dataRecord + "' contains the following record references that were not properly created: " + referencedRecordIDSet);

                // Return the completed data record if any sections (content values) were added to it, else return null.
                stats.logExecutionEnd(PE_RECORD_CONFORMANCE_CHECKING);
                return new DataRecordConstructionResult(dataRecord, conformanceFailures);
            }
            else return new DataRecordConstructionResult(null, new ArrayList<T8DataRecordConformanceError>());
        }
        else return new DataRecordConstructionResult(null, new ArrayList<T8DataRecordConformanceError>());
    }

    private String createValueMappingFFT(T8ValueRequirementMappingDefinition valueMapping, Map<String, Object> dataParameters)
    {
        List<T8Definition> mappingDefinitions;
        StringBuffer fft;

        // Get all the descendant mapping definitions.
        mappingDefinitions = valueMapping.getDescendentDefinitions();
        mappingDefinitions.add(valueMapping);

        // Concatenate all parameters used by the descendant mapping definitions.
        fft = new StringBuffer();
        for (T8Definition mappingDefinition : mappingDefinitions)
        {
            if (mappingDefinition instanceof T8DefaultValueRequirementMappingDefinition)
            {
                T8DefaultValueRequirementMappingDefinition valueMappingDefinition;
                RequirementType requirementType;

                valueMappingDefinition = (T8DefaultValueRequirementMappingDefinition)mappingDefinition;
                requirementType = RequirementType.getRequirementType(valueMappingDefinition.getRequirementDataTypeIdentifier());
                if (requirementType != RequirementType.DOCUMENT_REFERENCE)
                {
                    Object sourceParameter;

                    sourceParameter = dataParameters.get(valueMappingDefinition.getSourceParameterIdentifier());
                    if (sourceParameter != null)
                    {
                        fft.append(sourceParameter.toString());
                        fft.append(" ");
                    }
                }
            }
        }

        // Return the FFT result.
        return (fft.length() > 0) ? fft.toString().trim() : null;
    }

    private RecordValueConstructionResult constructRecordValue(T8DataTransaction tx, ValueRequirement valueRequirement, Map<String, Object> dataParameters, DataRecord dataRecord) throws Exception
    {
        List<T8DataRecordConformanceError> conformanceErrors;
        T8ConceptTerminologyList terminologyList;
        DataRequirement dataRequirement;
        PropertyRequirement propertyRequirement;
        RequirementType requirementType;
        String propertyID;
        String sectionID;

        // Get the document informantion that we need.
        dataRequirement = valueRequirement.getParentDataRequirement();
        propertyRequirement = valueRequirement.getParentPropertyRequirement();
        propertyID = propertyRequirement.getConceptID();
        sectionID = propertyRequirement.getParentSectionRequirement().getConceptID();
        terminologyList = new T8ConceptTerminologyList();
        dataRequirement.addContentTerminology(terminologyList);

        // Create a collection to hold conformance failures.
        conformanceErrors = new ArrayList<T8DataRecordConformanceError>();

        // Get the value parameter and Requirement type.
        requirementType = valueRequirement.getRequirementType();
        if (RequirementType.COMPOSITE_TYPE.equals(requirementType))
        {
            ValueRequirement fieldRequirement;
            ValueRequirement fieldValueRequirement;
            RecordValue compositeValue;
            String fieldInput;
            String fieldID;

            // Create the composite value.
            compositeValue = new Composite(valueRequirement);

            // Determine the field to set.
            fieldInput = (String)dataParameters.get(fieldIDParameterIdentifier);
            if (!Strings.isNullOrEmpty(fieldInput))
            {
                if (propertyRequirement.containsFieldRequirement(fieldInput)) fieldID = fieldInput;
                else
                {
                    fieldID = terminologyList.getConceptIDByTerm(T8OntologyConceptType.PROPERTY, fieldInput);
                    if (fieldID == null) fieldID = findFieldMatchingTerm(propertyRequirement, fieldInput);
                    if (fieldID == null) throw new RuntimeException("No matching field terminology found in " + dataRequirement + " for field input: " + fieldInput);
                }
            }
            else throw new RuntimeException("No field specified for " + dataRequirement + " composite property: " + propertyRequirement);

            // Get the field requirement
            fieldRequirement = propertyRequirement.getFieldRequirement(fieldID);
            fieldValueRequirement = fieldRequirement.getFirstSubRequirement();
            if (fieldValueRequirement != null)
            {
                RecordValueConstructionResult fieldValueConstructionResult;
                RecordValue fieldContentValue;

                // Construct the field content value.
                fieldValueConstructionResult = constructRecordValue(tx, fieldValueRequirement, dataParameters, dataRecord);

                // Get the constructed field content value and if it was successfully constructed, add it to the field.
                fieldContentValue = fieldValueConstructionResult.getRecordValue();
                if (fieldContentValue != null)
                {
                    RecordValue fieldValue;

                    // We only add field values if the field content value was successfully constructed.
                    fieldValue = new Field(fieldRequirement);
                    fieldValue.setSubValue(fieldContentValue);
                    compositeValue.setSubValue(fieldValue);
                }
                else // No value created from the field mapping, so create an FFT for the value.
                {
                    String valueFFT;

                    valueFFT = "Test FFT";
                    compositeValue.appendFFTChecked(valueFFT, " ");
                }

                // Always add the conformance errors.
                conformanceErrors.addAll(fieldValueConstructionResult.getConformanceErrors());
            }

            // Check the composite value and if any fields were successfully added, return the composite value
            if (compositeValue.getSubValues().size() > 0)
            {
                return new RecordValueConstructionResult(compositeValue, null, null, conformanceErrors);
            }
            else return new RecordValueConstructionResult(null, null, null, conformanceErrors);
        }
        else if ((RequirementType.DATE_TYPE.equals(requirementType)) || (RequirementType.DATE_TIME_TYPE.equals(requirementType)) || (RequirementType.TIME_TYPE.equals(requirementType)))
        {
            Object valueParameter;

            valueParameter = dataParameters.get(valueParameterId);
            if (valueParameter != null)
            {
                String dateString;

                // Get the String representation of the value parameter.
                dateString = valueParameter.toString().trim();

                // Try to format the string (it may already be a long value so check for that as well).
                if (Strings.isInteger(dateString))
                {
                    RecordValue newValue;

                    newValue = RecordValue.createValue(valueRequirement);
                    newValue.setValue(dateString);
                    return new RecordValueConstructionResult(newValue, null, null, conformanceErrors);
                }
                else // Ok so the mapped value is not a long and we need to format the string in order to get the millisecond long value.
                {
                    String formatPattern;

                    // Get the format to use.
                    formatPattern = (String)valueRequirement.getAttribute(RequirementAttribute.DATE_TIME_FORMAT_PATTERN.getIdentifier());
                    if (!Strings.isNullOrEmpty(formatPattern))
                    {
                        try
                        {
                            SimpleDateFormat dateFormat;
                            RecordValue newValue;
                            Date date;

                            dateFormat = new SimpleDateFormat(formatPattern);
                            date = dateFormat.parse(dateString);
                            newValue = RecordValue.createValue(valueRequirement);
                            newValue.setValue(((Long)date.getTime()).toString());
                            return new RecordValueConstructionResult(newValue, null, null, conformanceErrors);
                        }
                        catch (Exception e)
                        {
                            String drInstanceID;
                            String drID;
                            String classID;

                            // No matching concept found, so just return a null.
                            drInstanceID = valueRequirement.getParentDataRequirementInstance().getConceptID();
                            drID = valueRequirement.getParentDataRequirement().getConceptID();
                            classID = valueRequirement.getParentDataRequirement().getClassConceptID();
                            conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.INVALID_DATA_FORMAT, dataRecord.getID(), drInstanceID, drID, classID, sectionID, propertyID, dateString, "Invalid date format: " + dateString));
                            return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                        }
                    }
                    else
                    {
                        String drInstanceID;
                        String drID;
                        String classID;

                        // No matching concept found, so just return a null.
                        drInstanceID = valueRequirement.getParentDataRequirementInstance().getConceptID();
                        drID = valueRequirement.getParentDataRequirement().getConceptID();
                        classID = valueRequirement.getParentDataRequirement().getClassConceptID();
                        conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.INVALID_REQUIREMENT, dataRecord.getID(), drInstanceID, drID, classID, sectionID, propertyID, dateString, "No Date Time format specified."));
                        return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                    }
                }
            }
            else
            {
                return new RecordValueConstructionResult(null, null, null, conformanceErrors);
            }
        }
        else if (RequirementType.CONTROLLED_CONCEPT.equals(requirementType))
        {
            ControlledConceptRequirement conceptRequirement;
            Object conceptIDParameter;
            Object conceptTermParameter;
            Object conceptCodeParameter;

            conceptRequirement = (ControlledConceptRequirement)valueRequirement;
            conceptIDParameter = dataParameters.get(conceptIdParameterId);
            conceptTermParameter = dataParameters.get(conceptTermParameterId);
            conceptCodeParameter = dataParameters.get(conceptCodeParameterId);
            if ((conceptIDParameter != null) || (conceptTermParameter != null) || (conceptCodeParameter != null))
            {
                String organizationID;
                String conceptID;

                // Get the concept ID.
                conceptID = conceptIDParameter != null ? conceptIDParameter.toString().trim() : null;
                if (Strings.isNullOrEmpty(conceptID))
                {
                    String conceptTermString;
                    String conceptCodeString;

                    // Try to find the concept by matching the supplied code or term.
                    conceptTermString = conceptTermParameter != null ? conceptTermParameter.toString().trim() : null;
                    conceptCodeString = conceptCodeParameter != null ? conceptCodeParameter.toString().trim() : null;
                    organizationID = sessionContext.getOrganizationIdentifier();
                    conceptID = findConceptId(tx, organizationID, conceptRequirement.getOntologyClassID(), conceptTermString, conceptCodeString);
                }

                if (!Strings.isNullOrEmpty(conceptID))
                {
                    ControlledConcept controlledConcept;

                    controlledConcept = new ControlledConcept(conceptRequirement);
                    controlledConcept.setConceptId(conceptID, false);
                    return new RecordValueConstructionResult(controlledConcept, null, null, conformanceErrors);
                }
                else
                {
                    String drInstanceID;
                    String drID;
                    String classID;

                    // No matching concept found, so just return a null.
                    drInstanceID = valueRequirement.getParentDataRequirementInstance().getConceptID();
                    drID = valueRequirement.getParentDataRequirement().getConceptID();
                    classID = valueRequirement.getParentDataRequirement().getClassConceptID();
                    conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.CONCEPT_MATCH_NOT_FOUND, dataRecord.getID(), drInstanceID, drID, classID, sectionID, propertyID, conceptID, "No matching controlled value found for input data: " + conceptID));
                    return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                }
            }
            else
            {
                return new RecordValueConstructionResult(null, null, null, conformanceErrors);
            }
        }
        else if (RequirementType.MEASURED_NUMBER.equals(requirementType))
        {
            MeasuredNumberRequirement measuredNumberRequirement;
            Object uomIdParameter;
            Object uomTermParameter;
            Object uomCodeParameter;
            Object valueParameter;

            measuredNumberRequirement = (MeasuredNumberRequirement)valueRequirement;
            valueParameter = dataParameters.get(valueParameterId);
            uomIdParameter = dataParameters.get(uomIdParameterId);
            uomTermParameter = dataParameters.get(uomTermParameterId);
            uomCodeParameter = dataParameters.get(uomCodeParameterId);
            if ((uomIdParameter != null) || (uomTermParameter != null) || (uomCodeParameter != null))
            {
                String orgId;
                String uomId;

                // Get the concept ID.
                uomId = uomIdParameter != null ? uomIdParameter.toString().trim() : null;
                if (Strings.isNullOrEmpty(uomId))
                {
                    String uomTermString;
                    String uomCodeString;

                    // Try to find the concept by matching the supplied code or term.
                    uomTermString = uomTermParameter != null ? uomTermParameter.toString().trim() : null;
                    uomCodeString = uomCodeParameter != null ? uomCodeParameter.toString().trim() : null;
                    orgId = sessionContext.getOrganizationIdentifier();
                    uomId = findConceptId(tx, orgId, measuredNumberRequirement.getUomOntologyClassId(), uomTermString, uomCodeString);
                }

                // Make sure we have a number and UOM to construct the value.
                if (valueParameter == null)
                {
                    String drInstanceId;
                    String drId;
                    String classId;

                    // No matching concept found, so just return a null.
                    drInstanceId = valueRequirement.getParentDataRequirementInstance().getConceptID();
                    drId = valueRequirement.getParentDataRequirement().getConceptID();
                    classId = valueRequirement.getParentDataRequirement().getClassConceptID();
                    conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.REQUIRED_DATA_NOT_FOUND, dataRecord.getID(), drInstanceId, drId, classId, sectionID, propertyID, uomId, "No numeric value found to construct MeasuredNumber.  Value parameter id: " + valueParameterId));
                    return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                }
                else if (Strings.isNullOrEmpty(uomId))
                {
                    String drInstanceId;
                    String drId;
                    String classId;

                    // No matching concept found, so just return a null.
                    drInstanceId = valueRequirement.getParentDataRequirementInstance().getConceptID();
                    drId = valueRequirement.getParentDataRequirement().getConceptID();
                    classId = valueRequirement.getParentDataRequirement().getClassConceptID();
                    conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.CONCEPT_MATCH_NOT_FOUND, dataRecord.getID(), drInstanceId, drId, classId, sectionID, propertyID, uomId, "No matching UOM value found for input data: " + uomId));
                    return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                }
                else
                {
                    MeasuredNumber measuredNumber;

                    measuredNumber = new MeasuredNumber(measuredNumberRequirement);
                    measuredNumber.setNumber(valueParameter.toString());
                    measuredNumber.setUomId(uomId);
                    return new RecordValueConstructionResult(measuredNumber, null, null, conformanceErrors);
                }
            }
            else
            {
                return new RecordValueConstructionResult(null, null, null, conformanceErrors);
            }
        }
        else if (RequirementType.MEASURED_RANGE.equals(requirementType))
        {
            MeasuredRangeRequirement measuredRangeRequirement;
            Object uomIdParameter;
            Object uomTermParameter;
            Object uomCodeParameter;
            Object lowerBoundParameter;
            Object upperBoundParameter;

            measuredRangeRequirement = (MeasuredRangeRequirement)valueRequirement;
            lowerBoundParameter = dataParameters.get(lowerValueParameterId);
            upperBoundParameter = dataParameters.get(upperValueParameterId);
            uomIdParameter = dataParameters.get(uomIdParameterId);
            uomTermParameter = dataParameters.get(uomTermParameterId);
            uomCodeParameter = dataParameters.get(uomCodeParameterId);
            if ((uomIdParameter != null) || (uomTermParameter != null) || (uomCodeParameter != null))
            {
                String orgId;
                String uomId;

                // Get the concept ID.
                uomId = uomIdParameter != null ? uomIdParameter.toString().trim() : null;
                if (Strings.isNullOrEmpty(uomId))
                {
                    String uomTermString;
                    String uomCodeString;

                    // Try to find the concept by matching the supplied code or term.
                    uomTermString = uomTermParameter != null ? uomTermParameter.toString().trim() : null;
                    uomCodeString = uomCodeParameter != null ? uomCodeParameter.toString().trim() : null;
                    orgId = sessionContext.getOrganizationIdentifier();
                    uomId = findConceptId(tx, orgId, measuredRangeRequirement.getUomOntologyClassId(), uomTermString, uomCodeString);
                }

                // Make sure we have a lower and upper bound number and UOM to construct the value.
                if (lowerBoundParameter == null)
                {
                    String drInstanceId;
                    String drId;
                    String classId;

                    // No matching concept found, so just return a null.
                    drInstanceId = valueRequirement.getParentDataRequirementInstance().getConceptID();
                    drId = valueRequirement.getParentDataRequirement().getConceptID();
                    classId = valueRequirement.getParentDataRequirement().getClassConceptID();
                    conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.REQUIRED_DATA_NOT_FOUND, dataRecord.getID(), drInstanceId, drId, classId, sectionID, propertyID, uomId, "No lower bound numeric value found to construct MeasuredRange.  Value parameter id: " + lowerValueParameterId));
                    return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                }
                else if (upperBoundParameter == null)
                {
                    String drInstanceId;
                    String drId;
                    String classId;

                    // No matching concept found, so just return a null.
                    drInstanceId = valueRequirement.getParentDataRequirementInstance().getConceptID();
                    drId = valueRequirement.getParentDataRequirement().getConceptID();
                    classId = valueRequirement.getParentDataRequirement().getClassConceptID();
                    conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.REQUIRED_DATA_NOT_FOUND, dataRecord.getID(), drInstanceId, drId, classId, sectionID, propertyID, uomId, "No upper bound numeric value found to construct MeasuredRange.  Value parameter id: " + upperValueParameterId));
                    return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                }
                else if (Strings.isNullOrEmpty(uomId))
                {
                    String drInstanceId;
                    String drId;
                    String classId;

                    // No matching concept found, so just return a null.
                    drInstanceId = valueRequirement.getParentDataRequirementInstance().getConceptID();
                    drId = valueRequirement.getParentDataRequirement().getConceptID();
                    classId = valueRequirement.getParentDataRequirement().getClassConceptID();
                    conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.CONCEPT_MATCH_NOT_FOUND, dataRecord.getID(), drInstanceId, drId, classId, sectionID, propertyID, uomId, "No matching UOM value found for input data: " + uomId));
                    return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                }
                else
                {
                    MeasuredRange measuredRange;

                    measuredRange = new MeasuredRange(measuredRangeRequirement);
                    measuredRange.setLowerBoundNumber(lowerBoundParameter.toString());
                    measuredRange.setUpperBoundNumber(lowerBoundParameter.toString());
                    measuredRange.setUomId(uomId);
                    return new RecordValueConstructionResult(measuredRange, null, null, conformanceErrors);
                }
            }
            else
            {
                return new RecordValueConstructionResult(null, null, null, conformanceErrors);
            }
        }
        else
        {
            Object valueParameter;

            valueParameter = dataParameters.get(valueParameterId);
            if (valueParameter != null)
            {
                RecordValue recordValue;

                // Create the new record value and construct its sub-values.
                recordValue = RecordValue.createValue(valueRequirement);
                recordValue.setValue(valueParameter.toString());

                return new RecordValueConstructionResult(recordValue, null, null, conformanceErrors);
            }
            else return new RecordValueConstructionResult(null, null, null, conformanceErrors);
        }
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        this.stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    private String findPropertyMatchingTerm(DataRequirement dataRequirement, String term)
    {
        return findConceptIDMatchingTerm(dataRequirement.getPropertyRequirementIDList(), term);
    }

    private String findFieldMatchingTerm(PropertyRequirement propertyRequirement, String term)
    {
        return findConceptIDMatchingTerm(propertyRequirement.getFieldIDList(), term);
    }

    private String findConceptIDMatchingTerm(List<String> conceptIDList, String term)
    {
        if (term == null)
        {
            return null;
        }
        else
        {
            // Make sure we have a terminology provider.
            if (terminologyProvider == null) terminologyProvider = creationContext.getTerminologyProvider();

            // Loop through the concepts and find one with the specified term.
            for (T8ConceptTerminology terminology : terminologyProvider.getTerminology(null, conceptIDList))
            {
                if (term.equals(terminology.getTerm()))
                {
                    return terminology.getConceptId();
                }
            }

            return null;
        }
    }
}
