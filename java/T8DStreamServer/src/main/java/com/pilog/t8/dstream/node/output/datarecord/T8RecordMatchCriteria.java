package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationMappingDefinition.DataRecordMatchType;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8RecordMatchCriteria
{
    private DataRecordMatchType type;
    private List<String> keyPropertyIdentifiers;
    private String codeTypeID;

    public T8RecordMatchCriteria(DataRecordMatchType type, List<String> keyPropertyIdentifiers, String codeTypeID)
    {
        this.type = type;
        this.keyPropertyIdentifiers = keyPropertyIdentifiers;
        this.codeTypeID = codeTypeID;
    }

    public DataRecordMatchType getType()
    {
        return type;
    }

    public void setType(DataRecordMatchType type)
    {
        this.type = type;
    }

    public List<String> getKeyPropertyIdentifiers()
    {
        return keyPropertyIdentifiers;
    }

    public void setKeyPropertyIdentifiers(List<String> keyPropertyIdentifiers)
    {
        this.keyPropertyIdentifiers = keyPropertyIdentifiers;
    }

    public String getCodeTypeID()
    {
        return codeTypeID;
    }

    public void setCodeTypeID(String codeTypeID)
    {
        this.codeTypeID = codeTypeID;
    }
}
