package com.pilog.t8.dstream;

import com.pilog.t8.api.T8TerminologyApi;
import static com.pilog.t8.definition.dstream.T8DStreamAPIHandler.*;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8OrganizationApi;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRequirementMappingDefinition;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamOperations
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DStreamOperations.class);

    public static class DStreamExecutionOperation extends T8DefaultServerOperation
    {
        private T8DStream stream;
        private volatile T8DStreamExecutionStatus lastStatus;
        private String streamDisplayName;

        public DStreamExecutionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            HashMap<String, Object> inputParameters;
            T8DStreamDefinition streamDefinition;
            String projectId;
            String streamId;
            T8DStreamFileContext streamContext;

            // Get all required input parameters.
            streamDefinition = (T8DStreamDefinition)operationParameters.get(PARAMETER_DSTREAM_DEFINITION);
            projectId = (String)operationParameters.get(PARAMETER_DSTREAM_DEFINITION_IDENTIFIER);
            streamId = (String)operationParameters.get(PARAMETER_DSTREAM_DEFINITION_IDENTIFIER);
            streamContext = (T8DStreamFileContext)operationParameters.get(PARAMETER_DSTREAM_FILE_CONTEXT);
            inputParameters = (HashMap<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETERS);

            try
            {
                // Load th stream definition.
                if (streamDefinition == null)
                {
                    streamDefinition = (T8DStreamDefinition)serverContext.getDefinitionManager().getRawDefinition(context, projectId, streamId);

                    if (streamDefinition == null) throw new Exception("Stream Definition not found: " + streamId);
                }
                streamDisplayName = streamDefinition.getExecutionDialogHeaderText();

                // Execute the stream.
                stream = streamDefinition.getNewDataStreamInstance(context);
                stream.execute(tx, inputParameters, streamContext);

                //Just update the last progress so that process can see the stream finished execution
                getProgressReport(context);

                // End the operation.
                return null;
            }
            finally
            {
                try
                {
                    // Close the file context if necessary.
                    if (streamDefinition.isExecuteAsProcess())
                    {
                        T8FileManager fileManager;

                        fileManager = serverContext.getFileManager();
                        fileManager.closeFileContext(context, streamContext.getFileContextInstanceIdentifier());
                    }
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while closing file context.", e);
                }
            }
        }

        @Override
        public double getProgress(T8Context context)
        {
            if (stream != null)
            {
                return stream.getOverallExecutionProgress();
            }
            else if(lastStatus != null) return lastStatus.getOverallProgress();
            else return 0;
        }

        @Override
        public T8ProgressReport getProgressReport(T8Context context)
        {
            if (stream != null)
            {
                T8DStreamExecutionProgress executionProgress;
                T8DStreamExecutionStatus status;

                status = new T8DStreamExecutionStatus();
                executionProgress = stream.getExecutionProgress();
                status.setOverallProgress(executionProgress.getOverallExecutionProgress());
                status.setOverallProgressText(executionProgress.getOverallExecutionProgressText());
                status.setPhaseProgress(executionProgress.getPhaseExecutionProgress());
                status.setPhaseProgressText(executionProgress.getPhaseExecutionProgressText());
                status.setTitle(streamDisplayName);
                status.setPhaseIterationCount(executionProgress.getPhasIterationCount());
                status.setPhaseIterationsCompleted(executionProgress.getPhaseIterationsCompleted());

                lastStatus = status;
                return status;
            }
            else
            {
                return lastStatus;
            }
        }

        @Override
        public void stop(T8Context context)
        {
            if (stream != null)
            {
                stream.stopExecution();
            }
        }

        @Override
        public void cancel(T8Context context)
        {
            if (stream != null)
            {
                stream.stopExecution();
            }
        }
    }

    public static class CreateDataRequirementMappingDefinitionsOperation extends T8DefaultServerOperation
    {
        public CreateDataRequirementMappingDefinitionsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<T8DataRequirementMappingDefinition>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String drID;
            String drInstanceID;
            String drEntityIdentifier;
            String drInstanceEntityIdentifier;
            T8OrganizationApi orgApi;
            TerminologyProvider terminologyProvider;
            T8DataRequirementMappingHandler mappingHandler;

            // Get the data handler and terminology handler (to fetch terms to use as default definition identifiers).
            orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            terminologyProvider = ((T8TerminologyApi)tx.getApi(T8TerminologyApi.API_IDENTIFIER)).getTerminologyProvider();

            // Get the parameters we need.
            drID = (String)operationParameters.get(PARAMETER_DR_ID);
            drInstanceID = (String)operationParameters.get(PARAMETER_DR_INSTANCE_ID);
            drInstanceEntityIdentifier = DATA_REQUIREMENT_INSTANCE_DOCUMENT_DE_IDENTIFIER;
            drEntityIdentifier = DATA_REQUIREMENT_DOCUMENT_DE_IDENTIFIER;

            // Create the mapping handler and use it to create the mappings.
            mappingHandler = new T8DataRequirementMappingHandler(tx, terminologyProvider, drInstanceEntityIdentifier, drEntityIdentifier, orgApi.getDefaultContentLanguageId());
            if (!Strings.isNullOrEmpty(drID))
            {
                mappingHandler.createDataRequirementMappingsFromDR(drID);
            }
            else
            {
                mappingHandler.createDataRequirementMappingsFromDRInstance(drInstanceID);
            }

            // Return the result.
            return HashMaps.createSingular(PARAMETER_DR_MAPPING_DEFINITION_LIST, mappingHandler.getDataRequirementMappingDefinitions());
        }
    }
}
