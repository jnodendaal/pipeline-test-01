package com.pilog.t8.dstream;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.T8DStreamFinalizationScriptDefinition;
import com.pilog.t8.definition.dstream.T8DStreamInitializationScriptDefinition;
import com.pilog.t8.definition.dstream.input.T8DStreamInputDefinition;
import com.pilog.t8.definition.dstream.input.T8DStreamInputFileDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.dstream.output.T8DStreamOutputDefinition;
import com.pilog.t8.definition.dstream.output.T8DStreamOutputFileDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDStream implements T8DStream, T8PerformanceStatisticsProvider
{
    private final T8DStreamDefinition definition;
    private final T8Context context;
    private T8DStreamFileContext streamContext;
    private boolean outputFileStreamEnabled;
    private final ArrayList<T8DStreamNode> initializationNodes;
    private final ArrayList<T8DStreamNode> rootNodes;
    private final ArrayList<T8DStreamNode> finalizationNodes;
    private final ArrayList<T8DStreamNode> exceptionRootNodes;
    private final HashMap<String, Object> streamVariables;
    private final T8ServerContext serverContext;
    private T8FileManager fileManager;
    private T8DataTransaction tx;
    private T8DStreamLogFile logFile;
    private T8PerformanceStatistics stats;
    private HashMap<String, T8DStreamFileHandler> inputFiles;
    private HashMap<String, T8DStreamFileHandler> outputFiles;
    private ProcessingStage processingStage;
    private T8DStreamStatistics statistics;
    private enum ProcessingStage {DATA_PROCESSING, OUTPUT_FILE_STREAM, COMPLETE}

    private static final String PC_DATA_STREAM = "DATA_STREAM";

    private static final String PE_STREAM_EXECUTION = "STREAM_EXECUTION";

    public T8DefaultDStream(T8Context context, T8DStreamDefinition definition)
    {
        this.context = context;
        this.serverContext = context.getServerContext();
        this.definition = definition;
        this.outputFileStreamEnabled = false;
        this.initializationNodes = new ArrayList<T8DStreamNode>();
        this.rootNodes = new ArrayList<T8DStreamNode>();
        this.finalizationNodes = new ArrayList<T8DStreamNode>();
        this.exceptionRootNodes = new ArrayList<T8DStreamNode>();
        this.streamVariables = new HashMap<String, Object>();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(definition.isRecordPerformanceStatistics());
        createNodeStructure();
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
        this.tx.setPerformanceStatistics(stats);

        // Set the performance statistics on all root nodes.
        for (T8DStreamNode rootNode : rootNodes)
        {
            rootNode.setPerformanceStatistics(statistics);
        }
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    private void createNodeStructure()
    {
        System.out.println("Session context in stream: " + context);
        // Create the stream initialization nodes.
        for (T8DStreamNodeDefinition initializationNodeDefinition : definition.getInitializationNodeDefinitions())
        {
            try
            {
                T8DStreamNode initializationNode;

                initializationNode = initializationNodeDefinition.getNewNodeInstance(context);
                initializationNodes.add(initializationNode);
                initializationNode.setParentStream(this);
                initializationNode.setPerformanceStatistics(stats);
            }
            catch(InvocationTargetException ex)
            {
               throw new RuntimeException("Failed to create node instance " + initializationNodeDefinition.getIdentifier() + ". \nCause: " + ((ex.getCause()!=null)?ex.getCause().getMessage():""),ex);
            }
            catch(Throwable ex)
            {
               throw new RuntimeException("Failed to create node instance " + initializationNodeDefinition.getIdentifier() + ". \nCause: " + ((ex!=null)?ex.getMessage():""),ex);
            }
        }

        // Create the stream root nodes.
        for (T8DStreamNodeDefinition rootNodeDefinition : definition.getRootNodeDefinitions())
        {
            try
            {
                T8DStreamNode rootNode;

                rootNode = rootNodeDefinition.getNewNodeInstance(context);
                rootNodes.add(rootNode);
                rootNode.setParentStream(this);
                rootNode.setPerformanceStatistics(stats);
            }
            catch(InvocationTargetException ex)
            {
               throw new RuntimeException("Failed to create node instance " + rootNodeDefinition.getIdentifier() + ". \nCause: " + ((ex.getCause()!=null)?ex.getCause().getMessage():""),ex);
            }
            catch(Throwable ex)
            {
               throw new RuntimeException("Failed to create node instance " + rootNodeDefinition.getIdentifier() + ". \nCause: " + ((ex!=null)?ex.getMessage():""),ex);
            }
        }

        // Create the stream finalization nodes.
        for (T8DStreamNodeDefinition finalizationNodeDefinition : definition.getFinalizationNodeDefinitions())
        {
            try
            {
                T8DStreamNode finalizationNode;

                finalizationNode = finalizationNodeDefinition.getNewNodeInstance(context);
                finalizationNodes.add(finalizationNode);
                finalizationNode.setParentStream(this);
                finalizationNode.setPerformanceStatistics(stats);
            }
            catch(InvocationTargetException ex)
            {
               throw new RuntimeException("Failed to create node instance " + finalizationNodeDefinition.getIdentifier() + ". \nCause: " + ((ex.getCause()!=null)?ex.getCause().getMessage():""),ex);
            }
            catch(Throwable ex)
            {
               throw new RuntimeException("Failed to create node instance " + finalizationNodeDefinition.getIdentifier() + ". \nCause: " + ((ex!=null)?ex.getMessage():""),ex);
            }
        }

        // Create the exception root nodes.
        for (T8DStreamNodeDefinition exceptionRootNodeDefinition : definition.getExceptionRootNodeDefinitions())
        {
            try
            {
                T8DStreamNode exceptionRootNode;

                exceptionRootNode = exceptionRootNodeDefinition.getNewNodeInstance(context);
                exceptionRootNodes.add(exceptionRootNode);
                exceptionRootNode.setParentStream(this);
                exceptionRootNode.setPerformanceStatistics(stats);
            }
            catch(InvocationTargetException ex)
            {
               throw new RuntimeException("Failed to create node instance " + exceptionRootNodeDefinition.getIdentifier() + ". \nCause: " + ((ex.getCause()!=null)?ex.getCause().getMessage():""),ex);
            }
            catch(Throwable ex)
            {
               throw new RuntimeException("Failed to create node instance " + exceptionRootNodeDefinition.getIdentifier() + ". \nCause: " + ((ex!=null)?ex.getMessage():""),ex);
            }
        }
    }

    @Override
    public void execute(T8DataTransaction tx, HashMap<String, Object> inputParameters, T8DStreamFileContext streamContext) throws T8DStreamException, Exception
    {
        try
        {
            HashMap<String, Object> streamParameters;

            // Record the stream start time.
            stats.logExecutionStart(PE_STREAM_EXECUTION);

            // Set the current session.
            this.fileManager = serverContext.getFileManager();
            this.inputFiles = new HashMap<String, T8DStreamFileHandler>();
            this.outputFiles = new HashMap<String, T8DStreamFileHandler>();
            this.streamContext = streamContext;
            this.tx = tx;
            this.tx.setPerformanceStatistics(stats);
            this.logFile = new T8DefaultDStreamLogFile(fileManager.getFileOutputStream(context, streamContext.getFileContextInstanceIdentifier(), streamContext.getLogFilePath(), false));
            this.logFile.openFile();

            // Process all root nodes if there are any.
            logFile.log("Starting stream execution...");
            processingStage = ProcessingStage.DATA_PROCESSING;
            if (rootNodes.size() > 0)
            {
                // Create a new data row to be used as input for root nodes.
                streamParameters = new HashMap<String, Object>();
                if (inputParameters != null) streamParameters.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), inputParameters, true));

                // Execute the initialization script and add all of its output to the input data row.
                streamParameters.putAll(runInitializationScript(inputParameters));

                // Create a new statistics object.
                statistics = new T8DStreamStatistics();

                // Process all initialization nodes.
                for (T8DStreamNode initializationNode : initializationNodes)
                {
                    if (initializationNode.isEnabled())
                    {
                        try
                        {
                            initializationNode.initializeNode(tx, statistics, inputParameters);
                            initializationNode.processNode(tx, streamParameters);
                        }
                        finally // Make sure to finaly the node even if an exception is thrown, so that resources can be released.
                        {
                            initializationNode.finalizeNode(tx);
                        }
                    }
                }

                // Process all root nodes.
                for (T8DStreamNode rootNode : rootNodes)
                {
                    if (rootNode.isEnabled())
                    {
                        try
                        {
                            rootNode.initializeNode(tx, statistics, inputParameters);
                            rootNode.processNode(tx, streamParameters);
                        }
                        finally // Make sure to finaly the node even if an exception is thrown, so that resources can be released.
                        {
                            rootNode.finalizeNode(tx);
                        }
                    }
                }

                // Process all finalization nodes.
                for (T8DStreamNode finalizationNode : finalizationNodes)
                {
                    if (finalizationNode.isEnabled())
                    {
                        try
                        {
                            finalizationNode.initializeNode(tx, statistics, inputParameters);
                            finalizationNode.processNode(tx, streamParameters);
                        }
                        finally // Make sure to finaly the node even if an exception is thrown, so that resources can be released.
                        {
                            finalizationNode.finalizeNode(tx);
                        }
                    }
                }

                // Finalize all exception nodes.
                for (T8DStreamNode exceptionRootNode : exceptionRootNodes)
                {
                    if ((exceptionRootNode.isEnabled()) && (exceptionRootNode.isInitialized()))
                    {
                        exceptionRootNode.finalizeNode(tx);
                    }
                }

                // Run the finalization node.
                streamParameters.putAll(runFinalizationScript(inputParameters));
            }

            // Run the output file stream if it is enabled.
            if (outputFileStreamEnabled)
            {
                processingStage = ProcessingStage.OUTPUT_FILE_STREAM;
                //outputFileStream.execute(sessionContext, inputParameters, streamContext.getAllOutputFilePaths(), streamContext.getOutputDirectoryPath());
            }

            // Set the processing stage flag.
            processingStage = ProcessingStage.COMPLETE;
            logFile.log("Stream execution completed successfully.");
        }
        catch (T8DStreamException e)
        {
            Throwable exception;

            exception = e;
            while (exception != null)
            {
                // For each cause in the exception stack, print an applicable message.
                if (exception instanceof T8DStreamException)
                {
                    T8DStreamException streamException;

                    streamException = (T8DStreamException)exception;
                    if (exception.getCause() == null)
                    {
                        logFile.log("Failure while executing T8DStream '" + definition + "' on node '" + streamException.getNodeName() + "' using input data row " + streamException.getDataRow());
                        T8Log.log("While executing T8DStream '" + definition + "' on node '" + streamException.getNodeName() + "' using input data row " + streamException.getDataRow(), streamException);
                    }
                    else
                    {
                        T8Log.log("Exception on node '" + streamException.getNodeName() + "' using input data: " + streamException.getDataRow() + " Message: " + streamException.getMessage());
                    }
                }
                else T8Log.log("While executing T8DStream '" + definition + "' on node '" + e.getNodeName() + "' using input data row " + e.getDataRow(), e);

                // Get the next cause in the stack.
                exception = exception.getCause();
            }

            // Throw the exception so that clients can use it.
            throw e;
        }
        catch (Exception e)
        {
            T8Log.log("While executing T8DStream '" + definition + "'.", e);
            throw e;
        }
        finally
        {
            if (logFile != null) logFile.closeFile();
            closeFiles();

            // Log the performance statistics if required.
            stats.logExecutionEnd(PE_STREAM_EXECUTION);
            if (definition.isRecordPerformanceStatistics()) stats.printStatistics(System.out);
        }
    }

    @Override
    public boolean hasExceptionFlow()
    {
        return exceptionRootNodes.size() > 0;
    }

    @Override
    public void processExceptionFlow(Throwable cause, String detailMessage, String userMessage, String nodeName, Map<String, Object> dataRow) throws T8DStreamException
    {
        // Add some parameters to the data row before using it for the exception flow.
        dataRow.put(STREAM_PARAMETER_EXCEPTION_NODE_NAME, nodeName);
        dataRow.put(STREAM_PARAMETER_EXCEPTION_CAUSE, cause);
        dataRow.put(STREAM_PARAMETER_EXCEPTION_ROOT_CAUSE, ExceptionUtilities.getRootCause(cause));
        dataRow.put(STREAM_PARAMETER_EXCEPTION_ROOT_CAUSE_MESSAGE, ExceptionUtilities.getRootCauseMessage(cause));
        dataRow.put(STREAM_PARAMETER_EXCEPTION_DETAIL_MESSAGE, detailMessage);
        dataRow.put(STREAM_PARAMETER_EXCEPTION_USER_MESSAGE, userMessage);

        // Process all exception root nodes.
        for (T8DStreamNode exceptionNode : exceptionRootNodes)
        {
            if (exceptionNode.isEnabled())
            {
                // Make sure the node has been initialized.
                if (!exceptionNode.isInitialized()) exceptionNode.initializeNode(tx, statistics, dataRow);

                // Process the node.
                exceptionNode.processNode(tx, dataRow);
            }
        }
    }

    @Override
    public void stopExecution()
    {
        // Stop all initialization nodes.
        for (T8DStreamNode node : initializationNodes)
        {
            node.stopExecution();
        }

        // Stop all stream nodes.
        for (T8DStreamNode node : rootNodes)
        {
            node.stopExecution();
        }

        // Stop all finalization nodes.
        for (T8DStreamNode node : finalizationNodes)
        {
            node.stopExecution();
        }

        // Stop all exception nodes.
        for (T8DStreamNode exceptionRootNode : exceptionRootNodes)
        {
            exceptionRootNode.stopExecution();
        }
    }

    @Override
    public T8DStreamExecutionProgress getExecutionProgress()
    {
        T8DStreamExecutionProgress executionProgress;

        executionProgress = new T8DStreamExecutionProgress(getOverallExecutionProgress(), getOveralExecutionProgressText(), getPhaseExecutionProgress(), getPhaseExecutionProgressText());
        executionProgress.setPhasIterationCount(getPhaseExecutionIterationCount());
        executionProgress.setPhaseIterationsCompleted(getPhaseExecutionIterationsCompleted());

        return executionProgress;
    }

    @Override
    public double getOverallExecutionProgress()
    {
        double progress;
        double totalWeight;

        progress = 0.0;
        totalWeight = 0.0;

        // Get the total weight of the stream nodes.
        for (T8DStreamNode rootNode : rootNodes)
        {
            if (rootNode.isEnabled())
            {
                totalWeight += rootNode.getDefinition().getProgressWeight();
            }
        }

        // Compute the progress.
        for (T8DStreamNode rootNode : rootNodes)
        {
            if (rootNode.isEnabled())
            {
                double nodeProgress;

                // Get the progress of the node.
                nodeProgress = rootNode.getProgress();
                if (nodeProgress > 0)
                {
                    double nodeWeight;

                    nodeWeight = rootNode.getDefinition().getProgressWeight();
                    if (nodeWeight > 0.0)
                    {
                        nodeProgress = nodeProgress * (nodeWeight / totalWeight);
                        progress += nodeProgress;
                    }
                }
            }
        }

        // Round the total progress.
        return (int)Math.round(progress);
    }

    public String getOveralExecutionProgressText()
    {
        return definition.getDefaultProgressText();
    }

    public double getPhaseExecutionProgress()
    {
        for (T8DStreamNode rootNode : rootNodes)
        {
            if (rootNode.isEnabled())
            {
                double nodeProgress;

                nodeProgress = rootNode.getProgress();
                if ((nodeProgress > -1) && (nodeProgress < 100))
                {
                    return nodeProgress;
                }
            }
        }

        return 100;
    }

    public String getPhaseExecutionProgressText()
    {
        for (T8DStreamNode rootNode : rootNodes)
        {
            if (rootNode.isEnabled())
            {
                double nodeProgress;

                nodeProgress = rootNode.getProgress();
                if ((nodeProgress > -1) && (nodeProgress < 100))
                {
                    String progressText;

                    progressText = rootNode.getProgressText();
                    if (progressText != null)
                    {
                        return progressText;
                    }
                }
            }
        }

        return definition.getDefaultProgressText();
    }

    public int getPhaseExecutionIterationCount()
    {
        if (processingStage == ProcessingStage.OUTPUT_FILE_STREAM)
        {
            return -1;
        }
        else
        {
            for (T8DStreamNode rootNode : rootNodes)
            {
                if (rootNode.isEnabled())
                {
                    double nodeProgress;

                    nodeProgress = rootNode.getProgress();
                    if ((nodeProgress > -1) && (nodeProgress < 100))
                    {
                        return rootNode.getNodeIterationCount();
                    }
                }
            }

            return 100;
        }
    }

    public int getPhaseExecutionIterationsCompleted()
    {
        if (processingStage == ProcessingStage.OUTPUT_FILE_STREAM)
        {
            return -1;
        }
        else
        {
            for (T8DStreamNode rootNode : rootNodes)
            {
                if (rootNode.isEnabled())
                {
                    double nodeProgress;

                    nodeProgress = rootNode.getProgress();
                    if ((nodeProgress > -1) && (nodeProgress < 100))
                    {
                        return rootNode.getNodeIterationsCompleted();
                    }
                }
            }

            return 100;
        }
    }

    @Override
    public Object getVariable(String identifier)
    {
        return streamVariables.get(identifier);
    }

    @Override
    public void setVariable(String identifier, Object value)
    {
        streamVariables.put(identifier, value);
    }

    private void closeFiles() throws Exception
    {
        // Close all input files.
        for (T8DStreamFileHandler fileHandler : inputFiles.values())
        {
            fileHandler.closeFile();
        }

        // Close all output files.
        for (T8DStreamFileHandler fileHandler : outputFiles.values())
        {
            fileHandler.closeFile();
        }
    }

    public ArrayList<T8DStreamNode> getRootNodes()
    {
        return rootNodes;
    }

    public void setRootNodes(ArrayList<T8DStreamDefaultNode> rootNodes)
    {
        this.rootNodes.clear();
        for (T8DStreamDefaultNode newNode : rootNodes)
        {
            addRootNode(newNode);
        }
    }

    public void addRootNode(T8DStreamDefaultNode rootNode)
    {
        rootNode.setParentNode(null);
        rootNode.setParentStream(this);
        rootNodes.add(rootNode);
    }

    public void insertRootNode(T8DStreamDefaultNode rootNode, int index)
    {
        rootNode.setParentNode(null);
        rootNode.setParentStream(this);
        rootNodes.add(index, rootNode);
    }

    public void removeRootNode(T8DStreamDefaultNode rootNode)
    {
        if (rootNodes.remove(rootNode))
        {
            rootNode.setParentStream(null);
        }
    }

    @Override
    public T8DStreamLogFile getLogFile()
    {
        return logFile;
    }

    @Override
    public T8DStreamFileHandler getInputFileHandler(String inputIdentifier) throws Exception
    {
        if (!inputFiles.containsKey(inputIdentifier))
        {
            for (T8DStreamInputDefinition inputDefinition : definition.getInputDefinitions())
            {
                if (inputDefinition.getIdentifier().equals(inputIdentifier))
                {
                    T8DStreamFileHandler newFileHandler;

                    newFileHandler = ((T8DStreamInputFileDefinition)inputDefinition).getFileHandler(context, fileManager, streamContext);
                    if (newFileHandler.isFileAvailable())
                    {
                        // Input file handlers are opened automatically.
                        if (!newFileHandler.isOpen())
                        {
                            newFileHandler.openFile();
                            newFileHandler.validateFile(); //Make sure the file is compatable with the stream.
                        }
                    }

                    inputFiles.put(inputIdentifier, newFileHandler);
                    return newFileHandler;
                }
            }

            // If this point is reached, the file definition was not found.
            throw new Exception("Input File Definition not found: " + inputIdentifier);
        }
        else
        {
            return inputFiles.get(inputIdentifier);
        }
    }

    @Override
    public T8DStreamFileHandler getOutputFileHandler(String ouputIdentifier) throws Exception
    {
        if (!outputFiles.containsKey(ouputIdentifier))
        {
            for (T8DStreamOutputDefinition outputDefinition : definition.getOutputDefinitions())
            {
                if (outputDefinition.getIdentifier().equals(ouputIdentifier))
                {
                    T8DStreamFileHandler newFileHandler;

                    newFileHandler = ((T8DStreamOutputFileDefinition)outputDefinition).getFileHandler(context, fileManager, streamContext);
                    outputFiles.put(ouputIdentifier, newFileHandler);

                    return newFileHandler;
                }
            }

            // If this point is reached, the file definition was not found.
            throw new Exception("Output File Definition not found: " + ouputIdentifier);
        }
        else
        {
            return outputFiles.get(ouputIdentifier);
        }
    }

    public boolean isOutputFileStreamEnabled()
    {
        return outputFileStreamEnabled;
    }

    public void setOutputFileStreamEnabled(boolean outputFileStreamEnabled)
    {
        this.outputFileStreamEnabled = outputFileStreamEnabled;
    }

    private Map<String, T8DStreamNode> getNodeMap()
    {
        List<T8DStreamNode> nodeList;
        Map<String, T8DStreamNode> nodeMap;

        // Create a list of all the nodes in the stream.
        nodeList = new ArrayList<T8DStreamNode>();
        for (T8DStreamNode rootNode : rootNodes)
        {
            nodeList.add(rootNode);
            nodeList.addAll(rootNode.getDescendantNodes());
        }

        // Add all the nodes in the list to a identifier key map.
        nodeMap = new HashMap<String, T8DStreamNode>();
        for (T8DStreamNode streamNode : nodeList)
        {
            nodeMap.put(streamNode.getDefinition().getIdentifier(), streamNode);
        }

        // Return the compiledm map.
        return nodeMap;
    }

    private Map<String, Object> runInitializationScript(Map<String, Object> inputParameters) throws Exception
    {
        T8ServerContextScriptDefinition scriptDefinition;

        // Execute the script.
        scriptDefinition = definition.getInitializationScriptDefinition();
        if (scriptDefinition != null)
        {
            T8Script script;
            Object outputObject;

            // Add some additional input parameters.
            if (inputParameters == null) inputParameters = new HashMap<String, Object>();
            inputParameters.put("session", context.getSessionContext());
            inputParameters.put(T8DStreamInitializationScriptDefinition.PARAMETER_IDENTIFIER_STREAM_NODE_MAP, getNodeMap());

            // Create the script instance and execute it.
            script = scriptDefinition.getNewScriptInstance(context);
            outputObject = script.executeScript(inputParameters);

            // Check the script output.
            if (outputObject == null) return new HashMap<String, Object>();
            else if (outputObject instanceof Map)
            {
                return T8IdentifierUtilities.stripNamespace(scriptDefinition.getNamespace(), (Map<String, Object>)outputObject, false);
            }
            else throw new RuntimeException("Output of DStream initialization script is not an instance of Map: " + outputObject);
        }
        else return new HashMap<String, Object>();
    }

    private Map<String, Object> runFinalizationScript(Map<String, Object> inputParameters) throws Exception
    {
        T8ServerContextScriptDefinition scriptDefinition;

        // Execute the script.
        scriptDefinition = definition.getFinalizationScriptDefinition();
        if (scriptDefinition != null)
        {
            T8Script script;
            Object outputObject;

            // Add some additional input parameters.
            if (inputParameters == null) inputParameters = new HashMap<String, Object>();
            inputParameters.put("session", context.getSessionContext());
            inputParameters.put(T8DStreamFinalizationScriptDefinition.PARAMETER_IDENTIFIER_STREAM_NODE_MAP, getNodeMap());

            // Create the script instance and execute it.
            script = scriptDefinition.getNewScriptInstance(context);
            outputObject = script.executeScript(inputParameters);

            // Check the script output.
            if (outputObject == null) return new HashMap<String, Object>();
            else if (outputObject instanceof Map)
            {
                return T8IdentifierUtilities.stripNamespace(scriptDefinition.getNamespace(), (Map<String, Object>)outputObject, false);
            }
            else throw new RuntimeException("Output of DStream finalization script is not an instance of Map: " + outputObject);
        }
        else return new HashMap<String, Object>();
    }
}
