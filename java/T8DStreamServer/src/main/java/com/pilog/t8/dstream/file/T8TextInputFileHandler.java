package com.pilog.t8.dstream.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.dstream.input.textfile.T8TextFileInputDefinition;
import com.pilog.t8.definition.dstream.input.textfile.T8TextFileInputFieldDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.Maps;
import com.pilog.t8.utilities.files.text.TabularTextInputFile;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextInputFileHandler implements T8DStreamFileHandler
{
    private final T8TextFileInputDefinition inputDefinition;
    private final T8Context context;
    private final T8FileManager fileManager;
    private final T8DStreamFileContext fileContext;
    private TabularTextInputFile textInputFile;
    private final Map<String, String> sourceToFieldMapping;
    private final String fileContextIdentifier;
    private final String filePath;
    private final boolean trimStrings;
    private final boolean convertEmptyStringsToNull;

    public T8TextInputFileHandler(T8Context context, T8TextFileInputDefinition inputDefinition, T8FileManager fileManager, T8DStreamFileContext streamContext) throws Exception
    {
        this.context = context;
        this.inputDefinition = inputDefinition;
        this.fileManager = fileManager;
        this.fileContext = streamContext;
        this.fileContextIdentifier = fileContext.getFileContextInstanceIdentifier();
        this.filePath = fileContext.getInputFilePath(inputDefinition);
        this.sourceToFieldMapping = getSourceToFieldMapping();
        this.trimStrings = inputDefinition.trimStrings();
        this.convertEmptyStringsToNull = inputDefinition.convertEmptyStringsToNull();
    }

    @Override
    public boolean isOpen()
    {
        return (textInputFile != null);
    }

    @Override
    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIdentifier, filePath);
    }

    @Override
    public File getCurrentFile()
    {
        return new File(fileContext.getInputFilePath(inputDefinition));
    }

    @Override
    public void openFile() throws Exception
    {
        InputStream inputStream;

        inputStream = fileManager.getFileInputStream(context, fileContextIdentifier, filePath);
        textInputFile = new TabularTextInputFile(inputDefinition.getDefaultEncoding().getCode(), inputDefinition.getSeparatorString());
        textInputFile.openFile(inputStream);
    }

    public int getRowCount() throws Exception
    {
        InputStream inputStream;

        inputStream = fileManager.getFileInputStream(context, fileContextIdentifier, filePath);
        return textInputFile.countRows(inputStream);
    }

    public Map<String, Object> readDataRow() throws Exception
    {
        HashMap<String, Object> dataRow;

        // Read the next data row from the input file.
        dataRow = textInputFile.readDataRow();
        if (dataRow != null)
        {
            Map<String, Object> mappedDataRow;

            // Map the data row to the proper stream parameter identifiers.
            mappedDataRow = T8IdentifierUtilities.mapParameters(dataRow, sourceToFieldMapping);

            // Do some trimming if required.
            if (trimStrings) Maps.trimStringValues(mappedDataRow, convertEmptyStringsToNull);
            else if (convertEmptyStringsToNull) Maps.setEmptyStringValuesToNull(mappedDataRow);

            // Return the data row.
            return mappedDataRow;
        }
        else return null; // We have to return null if the row read from the file is null, to allow the invoker of this method to handle the end of file event.
    }

    @Override
    public void closeFile() throws Exception
    {
        if (textInputFile != null) textInputFile.closeFile();
        textInputFile = null;
    }

    @Override
    public void validateFile() throws Exception
    {
        //TODO: Add file validations
    }

    private Map<String, String> getSourceToFieldMapping()
    {
        List<T8TextFileInputFieldDefinition> fieldDefinitions;
        Map<String, String> fieldMapping;

        fieldMapping = new LinkedHashMap<String, String>();
        fieldDefinitions = inputDefinition.getFieldDefinitions();
        for (T8TextFileInputFieldDefinition fieldDefinition : fieldDefinitions)
        {
            fieldMapping.put(fieldDefinition.getTextColumnIdentifier(), fieldDefinition.getIdentifier());
        }

        return fieldMapping;
    }
}
