package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationMappingDefinition.DataRecordMatchType;

/**
 * @author Bouwer du Preez
 */
public class T8RecordMatchStringGenerator
{
    private final T8DataRecordValueStringGenerator stringGenerator;

    public T8RecordMatchStringGenerator(T8DataRecordValueStringGenerator stringGenerator)
    {
        this.stringGenerator = stringGenerator;
    }

    public String generateMatchString(DataRecord recordToMatch, T8RecordMatchCriteria matchCriteria)
    {
        DataRecordMatchType matchType;

        matchType = matchCriteria.getType();
        switch (matchType)
        {
            case VALUE_STRING:
            {
                // Generate a value String for the record.
                stringGenerator.clearPropertySequences();
                stringGenerator.setIncludeDataRequirementInstanceConcepts(true);
                return stringGenerator.generateValueString(recordToMatch).toString();
            }
            case KEY_PROPERTIES:
            {
                stringGenerator.setIncludeDataRequirementInstanceConcepts(false);
                stringGenerator.setPropertySequence(recordToMatch.getDataRequirement().getConceptID(), matchCriteria.getKeyPropertyIdentifiers());
                return stringGenerator.generateValueString(recordToMatch).toString();
            }
            case CODE:
            {
                T8OntologyConcept concept;

                concept = recordToMatch.getOntology();
                if (concept != null)
                {
                    T8OntologyCode code;

                    code = concept.getCodeByType(matchCriteria.getCodeTypeID());
                    if (code != null)
                    {
                        return code.getCode();
                    }
                    else throw new RuntimeException("Record Match type set to '" + matchType + "' but no code generated for record: " + recordToMatch);
                }
                else throw new RuntimeException("Record Match type set to '" + matchType + "' but no ontology found for record: " + recordToMatch);
            }
            default:
            {
                throw new RuntimeException("Unsupported match type: " + matchType);
            }
        }
    }
}
