package com.pilog.t8.dstream.node.output;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.dstream.node.output.T8TextFileOutputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.file.T8TextOutputFileHandler;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8TextFileOutputNode extends T8DStreamDefaultNode
{
    private T8TextFileOutputNodeDefinition definition;
    private T8TextOutputFileHandler fileHandler;

    public T8TextFileOutputNode(T8Context context, T8TextFileOutputNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.fileHandler = (T8TextOutputFileHandler)parentStream.getOutputFileHandler(definition.getOutputIdentifier());
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            // Open the output file if it is not already open.
            if (!fileHandler.isOpen()) fileHandler.openFile();

            // Write the data to the output file.
            fileHandler.writeDataRow(T8IdentifierUtilities.mapParameters(inputDataRow, definition.getInputParameterMapping()));
            processChildNodes(tx, inputDataRow);
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while writing to output file: " + definition.getOutputIdentifier(), null, inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
