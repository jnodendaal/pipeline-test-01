package com.pilog.t8.dstream;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;

import static com.pilog.t8.definition.dstream.T8DStreamAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamExecutor
{
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final String projectId;
    private final String streamId;
    private T8DStreamFileContext fileContext;

    public T8DStreamExecutor(T8Context context, String projectId, String streamId)
    {
        this.context = context;
        this.serverContext = context.getServerContext();
        this.projectId = projectId;
        this.streamId = streamId;
    }

    public void executeStream() throws Exception
    {
        HashMap<String, Object> operationParameters;

        // Create a new file context.
        this.fileContext = new T8DStreamFileContext(T8IdentifierUtilities.createNewGUID());

        // Create the operation parameters collection.
        operationParameters = new HashMap<String, Object>();
        operationParameters.put(PARAMETER_DSTREAM_DEFINITION_IDENTIFIER, streamId);
        operationParameters.put(PARAMETER_DSTREAM_FILE_CONTEXT, fileContext);
        operationParameters.put(PARAMETER_INPUT_PARAMETERS, new HashMap<String, Object>());

        // Execute the Stream.
        serverContext.executeSynchronousOperation(context, OPERATION_DSTREAM_EXECUTION_SERVER, operationParameters);
    }
}
