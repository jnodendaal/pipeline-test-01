package com.pilog.t8.dstream.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.exception.T8DefaultUserException;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputFieldDefinition;
import com.pilog.t8.definition.dstream.input.excelfile.T8ExcelFileInputSheetDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.Maps;
import com.pilog.t8.utilities.excel.ExcelInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelInputFileHandler implements T8DStreamFileHandler
{
    private final T8ExcelFileInputDefinition inputDefinition;
    private final T8Context context;
    private final T8FileManager fileManager;
    private final T8DStreamFileContext fileContext;
    private ExcelInputStream excelInputStream;
    private final Map<String, Map<String, String>> sourceToFieldMappings; // Used to cache mappings of sheet source column names to field parameter identifiers.
    private final String fileContextIid;
    private final String filePath;
    private final boolean trimStrings;
    private final boolean convertEmptyStringsToNull;

    public T8ExcelInputFileHandler(T8Context context, T8ExcelFileInputDefinition inputDefinition, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        this.context = context;
        this.inputDefinition = inputDefinition;
        this.fileManager = fileManager;
        this.fileContext = fileContext;
        this.sourceToFieldMappings = new HashMap<String, Map<String, String>>();
        this.fileContextIid = fileContext.getFileContextInstanceIdentifier();
        this.filePath = fileContext.getInputFilePath(inputDefinition);
        this.trimStrings = inputDefinition.trimStrings();
        this.convertEmptyStringsToNull = inputDefinition.convertEmptyStringsToNull();
    }

    @Override
    public boolean isOpen()
    {
        return (excelInputStream != null);
    }

    @Override
    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIid, filePath);
    }

    @Override
    public File getCurrentFile()
    {
        return new File(fileContext.getInputFilePath(inputDefinition));
    }

    @Override
    public void openFile() throws Exception
    {
        InputStream inputStream;

        inputStream = fileManager.getFileInputStream(context, fileContextIid, filePath);
        excelInputStream = new ExcelInputStream();
        excelInputStream.openFile(inputStream);
    }

    public int getRowCount(String sheetIdentifier) throws Exception
    {
        return excelInputStream.getSheetRowCount(getSheetName(sheetIdentifier));
    }

    public Map<String, Object> readDataRow(String sheetIdentifier) throws Exception
    {
        String sheetName;

        sheetName = getSheetName(sheetIdentifier);
        if (sheetName != null)
        {
            HashMap<String, Object> dataRow;

            // Read the next data row from the input file.
            dataRow = excelInputStream.readRow(sheetName);
            if (dataRow != null)
            {
                Map<String, Object> mappedDataRow;

                // Map the data row to the proper stream parameter identifiers.
                mappedDataRow = T8IdentifierUtilities.mapParameters(dataRow, getSourceToFieldMapping(sheetIdentifier));

                // Do some trimming if required.
                if (trimStrings) Maps.trimStringValues(mappedDataRow, convertEmptyStringsToNull);
                else if (convertEmptyStringsToNull) Maps.setEmptyStringValuesToNull(mappedDataRow);

                return mappedDataRow; // Return the data row.
            }
            return null; // We have to return null if the row read from the file is null, to allow the invoker of this method to handle the end of file event.
        }
        else throw new Exception("Excel input sheet definition not found: " + sheetIdentifier);
    }

    @Override
    public void closeFile() throws Exception
    {
        if (excelInputStream != null) excelInputStream.closeFile();
        excelInputStream = null;
    }

    @Override
    public void validateFile() throws Exception
    {
        if (isFileAvailable())
        {
            if (isOpen())
            {
                List<String> excelSheetNames;

                excelSheetNames = excelInputStream.getSheetNames();
                for (T8ExcelFileInputSheetDefinition t8ExcelFileInputSheetDefinition : inputDefinition.getSheetDefinitions())
                {
                    if (!excelSheetNames.contains(t8ExcelFileInputSheetDefinition.getSheetName()))
                    {
                        throw new T8DefaultUserException("Required sheet " + t8ExcelFileInputSheetDefinition.getSheetName() + " was not found in excel file.");
                    }
                    else
                    {
                        List<String> excelColumnNames;

                        excelColumnNames = excelInputStream.getColumnNames(t8ExcelFileInputSheetDefinition.getSheetName());
                        for (T8ExcelFileInputFieldDefinition fieldDefinition : t8ExcelFileInputSheetDefinition.getFieldDefinitions())
                        {
                            if ((fieldDefinition.isRequired()) && (!excelColumnNames.contains(fieldDefinition.getExcelColumnIdentifier())))
                            {
                                throw new T8DefaultUserException("The required column " + fieldDefinition.getExcelColumnIdentifier() + " was not found on sheet " + t8ExcelFileInputSheetDefinition.getSheetName());
                            }
                        }
                    }
                }
            }
            else throw new RuntimeException("File validations can only be performed on files that are open.");
        }
        else throw new RuntimeException("The file to validate is not available.");
    }

    private String getSheetName(String sheetIdentifier)
    {
        T8ExcelFileInputSheetDefinition sheetDefinition;

        sheetDefinition = inputDefinition.getSheetDefinition(sheetIdentifier);
        return sheetDefinition != null ? sheetDefinition.getSheetName() : null;
    }

    private Map<String, String> getSourceToFieldMapping(String sheetIdentifier)
    {
        // If the mapping is not available from the cache, create it now.
        if (!sourceToFieldMappings.containsKey(sheetIdentifier))
        {
            List<T8ExcelFileInputFieldDefinition> fieldDefinitions;
            T8ExcelFileInputSheetDefinition sheetDefinition;
            Map<String, String> fieldMapping;

            fieldMapping = new HashMap<String, String>();
            sheetDefinition = inputDefinition.getSheetDefinition(sheetIdentifier);
            fieldDefinitions = sheetDefinition.getFieldDefinitions();
            for (T8ExcelFileInputFieldDefinition fieldDefinition : fieldDefinitions)
            {
                fieldMapping.put(fieldDefinition.getExcelColumnIdentifier(), fieldDefinition.getIdentifier());
            }

            // Cache the newly created mapping.
            sourceToFieldMappings.put(sheetIdentifier, fieldMapping);
            return fieldMapping;
        }
        else
        {
            // Return the mapping from the cache.
            return sourceToFieldMappings.get(sheetIdentifier);
        }
    }
}
