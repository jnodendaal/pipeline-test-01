package com.pilog.t8.dstream.node.input;

import com.pilog.t8.definition.dstream.node.input.statistics.T8NodeStatisticsInputNodeDefinition;
import com.pilog.t8.definition.dstream.node.input.statistics.T8NodeStatisticsMappingDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamNodeStatistics;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8NodeStatisticsInputNode extends T8DStreamDefaultNode
{
    private final T8NodeStatisticsInputNodeDefinition definition;
    private volatile boolean stopFlag;

    public T8NodeStatisticsInputNode(T8Context context, T8NodeStatisticsInputNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.stopFlag = false;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return definition.getProgressText();
    }

    @Override
    public void stopExecution()
    {
        stopFlag = true;
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            // Add all compiled statistics to the input data row.
            inputDataRow.putAll(compileStatistics());

            // Process child nodes.
            if (!stopFlag) processChildNodes(tx, inputDataRow);
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while reading statistics in node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }

    private Map<String, Object> compileStatistics()
    {
        Map<String, Object> statisticsMap;

        // Compile the statistics.
        statisticsMap = new HashMap<String, Object>();
        for (T8NodeStatisticsMappingDefinition mappingDefinition : definition.getNodeStatisticMappingDefinitions())
        {
            T8DStreamNodeStatistics sourceStatistics;
            String inputRowCountParameterIdentifier;
            String outputRowCountParameterIdentifier;
            String exceptionRowCountParameterIdentifier;
            String statsNodeIdentifier;

            // Get the mapping information.
            statsNodeIdentifier = mappingDefinition.getNodeIdentifier();
            inputRowCountParameterIdentifier = mappingDefinition.getInputRowCountParameterIdentifier();
            outputRowCountParameterIdentifier = mappingDefinition.getOutputRowCountParameterIdentifier();
            exceptionRowCountParameterIdentifier = mappingDefinition.getExceptionRowCountParameterIdentifier();

            // Get the specified node statistics.
            sourceStatistics = statistics.getNodeStatistics(statsNodeIdentifier);
            if (sourceStatistics != null)
            {
                // Only add statistics for which a valid parameter is mapped.
                if (inputRowCountParameterIdentifier != null) statisticsMap.put(inputRowCountParameterIdentifier, sourceStatistics.getInputRowCount());
                if (outputRowCountParameterIdentifier != null) statisticsMap.put(outputRowCountParameterIdentifier, sourceStatistics.getOutputRowCount());
                if (exceptionRowCountParameterIdentifier != null) statisticsMap.put(exceptionRowCountParameterIdentifier, sourceStatistics.getExceptionRowCount());
            }
        }

        // Return the compiled map.
        return statisticsMap;
    }
}
