package com.pilog.t8.dstream;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition.NodeTransactionType;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamNodeStatistics;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * This class defines the default behavior for nodes in the stream.  All nodes
 * must extends this class and override as few methods as possible.  When nodes
 * are processed, the following rules apply:
 *  1.  All nodes are initialized by their parent node before being used.  At
 *      initialization, a node is supplied with the total data row as passed
 *      down through the stream, including the parameters already set by its
 *      parent.
 *  2.  When a node initialized, it processes all of its initialization nodes.
 *  3.  After all initialization nodes have been processed, the normal child
 *      nodes are processed.
 *  4.  All nodes are finalized by their parent node once they are no longer
 *      needed.  During finalization a node first processes all of the
 *      finalization nodes it contains and then takes care of its own
 *      finalization and cleanup.  It is up to a node to decide what parameters
 *      it will pass to its finalization nodes.
 *
 * @author Bouwer du Preez
 */
public abstract class T8DStreamDefaultNode implements T8DStreamNode
{
    protected final T8DStreamNodeDefinition definition;
    protected final T8Context context;
    protected final T8ServerContext serverContext;
    protected final T8SessionContext sessionContext;
    protected T8DStreamStatistics statistics;
    protected T8DStreamNodeStatistics nodeStatistics;
    protected T8DStreamNode parentNode;
    protected T8DStream parentStream;
    protected NodeTransactionType txType;
    protected ArrayList<T8DStreamNode> childNodes;
    protected ArrayList<T8DStreamNode> exceptionNodes;
    protected Map<String, Object> lastOutputDataRow;
    protected T8PerformanceStatistics stats;
    protected boolean enabled;  // A flag that indicates whether or not this node is enabled.
    protected boolean initialized;

    // Transaction management references.
    private T8DataTransaction isolatedTx; // This transaction is hidden from sub-classes because they do not need to worry about transaction handling.  Each sub-class must use the transaction passed to it.
    private T8DataTransaction suspendedTx; // The input transaction currently suspended so that this node's isolated transaction can be used by the executing thread.
    private T8DataSession dataSession;
    private final int batchSize;
    private int batchProcessedCount;

    public T8DStreamDefaultNode(T8Context context, T8DStreamNodeDefinition definition)
    {
        this.context = context;
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.definition = definition;
        this.childNodes = new ArrayList<T8DStreamNode>();
        this.exceptionNodes = new ArrayList<T8DStreamNode>();
        this.initialized = false;
        this.lastOutputDataRow = new HashMap<String, Object>();
        this.enabled = true;
        this.batchSize = definition.getBatchSize();
        this.txType = definition.getTransactionType();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default is false.
        System.out.println("Session Context in node '" + definition + "': " + sessionContext);
        createChildNodes();
        createExceptionNodes();
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
        //if (tx != null) this.tx.setPerformanceStatistics(stats);

        // Set the performance statistics on all child nodes.
        for (T8DStreamNode childNode : childNodes)
        {
            childNode.setPerformanceStatistics(statistics);
        }
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    @Override
    public T8DStreamNodeStatistics getStatistics()
    {
        return nodeStatistics;
    }

    private void createChildNodes()
    {
        for (T8DStreamNodeDefinition nodeDefinition : definition.getChildNodeDefinitions())
        {
            try
            {
                T8DStreamNode childNode;

                childNode = nodeDefinition.getNewNodeInstance(context);
                childNodes.add(childNode);
                childNode.setParentNode(this);
            }
            catch(InvocationTargetException ex)
            {
               throw new RuntimeException("Failed to create node instance " + nodeDefinition.getIdentifier() + ". \nCause: " + ((ex.getCause()!=null)?ex.getCause().getMessage():""),ex);
            }
            catch(Throwable ex)
            {
               throw new RuntimeException("Failed to create node instance " + nodeDefinition.getIdentifier() + ". \nCause: " + ((ex!=null)?ex.getMessage():""),ex);
            }
        }
    }

    private void createExceptionNodes()
    {
        for (T8DStreamNodeDefinition nodeDefinition : definition.getExceptionNodeDefinitions())
        {
            try
            {
                T8DStreamNode exceptionNode;

                exceptionNode = nodeDefinition.getNewNodeInstance(context);
                exceptionNodes.add(exceptionNode);
                exceptionNode.setParentNode(this);
            }
            catch(InvocationTargetException ex)
            {
               throw new RuntimeException("Failed to create node instance " + nodeDefinition.getIdentifier() + ". \nCause: " + ((ex.getCause()!=null)?ex.getCause().getMessage():""),ex);
            }
            catch(Throwable ex)
            {
               throw new RuntimeException("Failed to create node instance " + nodeDefinition.getIdentifier() + ". \nCause: " + ((ex!=null)?ex.getMessage():""),ex);
            }
        }
    }

    @Override
    public T8DStreamNodeDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public String getTypeName()
    {
        return definition.getTypeMetaData().getDisplayName();
    }

    @Override
    public boolean isInitialized()
    {
        return initialized;
    }

    @Override
    public T8DStream getParentStream()
    {
        return parentStream;
    }

    @Override
    public void setParentStream(T8DStream parentStream)
    {
        this.parentStream = parentStream;

        // Set the parent on all child nodes.
        for (T8DStreamNode childNode : childNodes)
        {
            childNode.setParentStream(parentStream);
        }

        // Set the parent on all exception nodes.
        for (T8DStreamNode exceptionNode : exceptionNodes)
        {
            exceptionNode.setParentStream(parentStream);
        }
    }

    @Override
    public ArrayList<String> getRecursableNodeNames()
    {
        ArrayList<String> names;

        names = new ArrayList<String>();
        names.add(definition.getIdentifier());
        if (parentNode != null) names.addAll(parentNode.getRecursableNodeNames());

        return names;
    }

    @Override
    public T8DStreamNode getParentNode()
    {
        return parentNode;
    }

    @Override
    public void setParentNode(T8DStreamNode parentNode)
    {
        this.parentNode = parentNode;
    }

    @Override
    public boolean isEnabled()
    {
        return (definition.isEnabled() && enabled);
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    @Override
    public List<T8DStreamNode> getDescendantNodes()
    {
        List<T8DStreamNode> descendants;

        // Create a list to hold all descendants.
        descendants = new ArrayList<T8DStreamNode>();

        // Add all child descendants.
        for (T8DStreamNode childNode : childNodes)
        {
            descendants.add(childNode);
            descendants.addAll(childNode.getDescendantNodes());
        }

        // Add all exception descendants.
        for (T8DStreamNode exceptionNode : exceptionNodes)
        {
            descendants.add(exceptionNode);
            descendants.addAll(exceptionNode.getDescendantNodes());
        }

        return descendants;
    }

    protected boolean hasExceptionFlow()
    {
        return exceptionNodes.size() > 0;
    }

    // TODO: GBO - Review whether or not all these transaction types are still
    // required. As per BDP: The ISOLATED_AUTO_COMMIT may no longer be relevant
    // as well as when an Instant Transaction is being used, it should be
    // implicitly inherited rather than end up being suspended.
    // When changes have been made to this structure, define the requirements
    // when building a data stream to ensure all developers are aware of the
    // required settings for transactions to be used on nodes and node levels.

    private T8DataTransaction getTransactionToUse(T8DataTransaction tx)
    {
        switch (txType)
        {
            case INHERITED:
                // Use the transaction passed in.
                suspendedTx = null; // We are using the input tx so no need for suspension.
                return tx;
            case ISOLATED:
                if (isolatedTx != null)
                {
                    // Suspend the input transaction.
                    suspendedTx = dataSession.suspend();

                    // Create a new transaction for use by this node and its descendants.
                    dataSession.resume(isolatedTx);
                    return isolatedTx;
                }
                else
                {
                    // Suspend the input transaction.
                    suspendedTx = dataSession.suspend();

                    // Create a new transaction for use by this node and its descendants.
                    isolatedTx = dataSession.beginTransaction();
                    isolatedTx.setPerformanceStatistics(stats);
                    return isolatedTx;
                }
            case ISOLATED_AUTO_COMMIT:
                // Suspend the input transaction.
                suspendedTx = dataSession.suspend();

                // Create a new transaction for use by this node and its descendants.
                isolatedTx = dataSession.instantTransaction();
                return isolatedTx;
            default:
                throw new RuntimeException("Invalid Transaction Type encountered: " + txType);
        }
    }

    private void suspendTransaction()
    {
        switch (txType)
        {
            case INHERITED:
                // Nothing to do in this case.
                return;
            case ISOLATED:
                dataSession.suspend();
                if (this.suspendedTx != null) dataSession.resume(suspendedTx);
                return;
            case ISOLATED_AUTO_COMMIT:
                if (this.suspendedTx != null) dataSession.resume(suspendedTx);
                return;
            default:
                throw new RuntimeException("Invalid Transaction Type encountered: " + txType);
        }
    }

    private void commitTransaction()
    {
        switch (txType)
        {
            case INHERITED:
                // Nothing to do in this case.
                return;
            case ISOLATED:
                isolatedTx.commit();
                isolatedTx = null;
                if (this.suspendedTx != null) dataSession.resume(suspendedTx);
                return;
            case ISOLATED_AUTO_COMMIT:
                if (this.suspendedTx != null) dataSession.resume(suspendedTx);
                return;
            default:
                throw new RuntimeException("Invalid Transaction Type encountered: " + txType);
        }
    }

    private void rollbackTransaction()
    {
        switch (txType)
        {
            case INHERITED:
                // Nothing to do in this case.
                return;
            case ISOLATED:
                if (isolatedTx != null)
                {
                    isolatedTx.rollback();
                    isolatedTx = null;
                    if (this.suspendedTx != null) dataSession.resume(suspendedTx);
                    return;
                }
                else return;
            case ISOLATED_AUTO_COMMIT:
                if (this.suspendedTx != null) dataSession.resume(suspendedTx);
                return;
            default:
                throw new RuntimeException("Invalid Transaction Type encountered: " + txType);
        }
    }

    @Override
    public final void initializeNode(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        T8DataTransaction txToUse;

        // Set initialization parameters.
        this.statistics = streamStatistics;
        this.nodeStatistics = statistics.createNodeStatistics(definition.getIdentifier());
        this.dataSession = serverContext.getDataManager().getCurrentSession();
        this.batchProcessedCount = 0;

        // Set the appropriate transaction for this node and its descendants.
        txToUse = getTransactionToUse(tx);

        try
        {
            // Initialize this node's specific functionality.
            initializeNodeSpecific(txToUse, streamStatistics, inputDataRow);

            // Set the flag to indicate that this node has been initialized
            initialized = true;
        }
        finally
        {
            suspendTransaction();
        }
    }

    @Override
    public final void processNode(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        T8DataTransaction txToUse;

        // Set the appropriate transaction for this node and its descendants.
        txToUse = getTransactionToUse(tx);

        // Process this node using the correct transaction type.
        try
        {
            processNodeSpecific(txToUse, inputDataRow);
            suspendTransaction();
        }
        catch (T8DStreamException e)
        {
            rollbackTransaction();
            throw e;
        }
        catch (Exception e)
        {
            rollbackTransaction();
            throw new T8DStreamException(e, "Exception during node execution: " + definition, "An unexpected error occurred during processing.", definition.getIdentifier(), inputDataRow);
        }
    }

    @Override
    public final void finalizeNode(T8DataTransaction tx) throws Exception
    {
        if (initialized)
        {
            T8DataTransaction txToUse;

            // Set the appropriate transaction for this node and its descendants.
            txToUse = getTransactionToUse(tx);

            // Process this node using the correct transaction type.
            try
            {
                // Finalize all child nodes.
                for (T8DStreamNode childNode : childNodes)
                {
                    if ((childNode.isEnabled()) && (childNode.isInitialized()))
                    {
                        childNode.finalizeNode(txToUse);
                    }
                }

                // Finalize all exception nodes.
                for (T8DStreamNode exceptionNode : exceptionNodes)
                {
                    if ((exceptionNode.isEnabled()) && (exceptionNode.isInitialized()))
                    {
                        exceptionNode.finalizeNode(txToUse);
                    }
                }

                // Finalize this node's specific implementation.
                finalizeNodeSpecific(txToUse);
            }
            finally
            {
                initialized = false;
                commitTransaction();
            }
        }
    }

    /**
     * This is a callback method invoked by sub-classes after a unit of work has been processed.
     * @param tx The transaction used by the sub-class.
     */
    protected void processed(T8DataTransaction tx)
    {
        // Increment the processed counter.
        batchProcessedCount++;

        // If we've reached the batch size, commit the current transaction.
        if ((txType == NodeTransactionType.ISOLATED) && (batchSize > 0) && (batchProcessedCount >= batchSize))
        {
            commitTransaction();
            getTransactionToUse(tx);
            batchProcessedCount = 0;
        }
    }

    protected abstract void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException;
    protected abstract void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException;
    protected abstract void finalizeNodeSpecific(T8DataTransaction tx) throws Exception;

    protected void processChildNodes(T8DataTransaction tx, Map<String, Object> outputDataRow) throws T8DStreamException
    {
        // Process all of the child nodes.
        lastOutputDataRow = outputDataRow;
        nodeStatistics.incrementOutputRowCount();
        for (T8DStreamNode childNode : childNodes)
        {
            if (childNode.isEnabled())
            {
                // Make sure the node has been initialized.
                if (!childNode.isInitialized()) childNode.initializeNode(tx, statistics, outputDataRow);

                // Process the node.
                childNode.getStatistics().incrementInputRowCount();
                childNode.processNode(tx, outputDataRow);
            }
        }
    }

    protected void processExceptionFlow(T8DataTransaction tx, Throwable cause, String detailMessage, String userMessage, String nodeName, Map<String, Object> dataRow) throws T8DStreamException
    {
        // Add some parameters to the data row before using it for the exception flow.
        dataRow.put(T8DStream.STREAM_PARAMETER_EXCEPTION_NODE_NAME, nodeName);
        dataRow.put(T8DStream.STREAM_PARAMETER_EXCEPTION_CAUSE, cause);
        dataRow.put(T8DStream.STREAM_PARAMETER_EXCEPTION_ROOT_CAUSE, ExceptionUtilities.getRootCause(cause));
        dataRow.put(T8DStream.STREAM_PARAMETER_EXCEPTION_ROOT_CAUSE_MESSAGE, ExceptionUtilities.getRootCauseMessage(cause));
        dataRow.put(T8DStream.STREAM_PARAMETER_EXCEPTION_DETAIL_MESSAGE, detailMessage);
        dataRow.put(T8DStream.STREAM_PARAMETER_EXCEPTION_USER_MESSAGE, userMessage);

        // Process all of the exception nodes.
        for (T8DStreamNode exceptionNode : exceptionNodes)
        {
            if (exceptionNode.isEnabled())
            {
                // Make sure the node has been initialized.
                if (!exceptionNode.isInitialized()) exceptionNode.initializeNode(tx, statistics, dataRow);

                // Process the node.
                exceptionNode.processNode(tx, dataRow);
            }
        }
    }

    protected void handleException(T8DataTransaction tx, Throwable e, String detailMessage, String userMessage, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        // Increment the exception count on the node statistics.
        nodeStatistics.incrementExceptionRowCount();

        // First print the stack trace to make sure it does not get lost.
        e.printStackTrace();

        // Now handle the exception depending on the setup of the stream.
        if (hasExceptionFlow()) processExceptionFlow(tx, e, detailMessage, userMessage, definition.getIdentifier(), inputDataRow);
        else if (parentStream.hasExceptionFlow()) parentStream.processExceptionFlow(e, detailMessage, null, definition.getIdentifier(), inputDataRow);
        else throw new T8DStreamException(e, detailMessage, userMessage, definition.getIdentifier(), inputDataRow);
    }

    protected void stopDescendantNodes()
    {
        // Stop all child nodes.
        for (T8DStreamNode childNode : childNodes)
        {
            childNode.stopExecution();
        }

        // Stop all exception nodes.
        for (T8DStreamNode exceptionNode : exceptionNodes)
        {
            exceptionNode.stopExecution();
        }
    }
}
