package com.pilog.t8.dstream.node.output;

import com.pilog.t8.definition.dstream.node.output.T8DataFileOutputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.file.T8DataFileOutputFileHandler;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.io.Serializable;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileOutputNode extends T8DStreamDefaultNode implements Serializable
{
    private final T8DataFileOutputNodeDefinition definition;
    private T8DataFileOutputFileHandler fileHandler;

    public T8DataFileOutputNode(T8Context context, T8DataFileOutputNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.fileHandler = (T8DataFileOutputFileHandler)parentStream.getOutputFileHandler(definition.getOutputIdentifier());
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            // Open the output file if it is not already open.
            if (!fileHandler.isOpen()) fileHandler.openFile();

            // Write the data to the output file.
            // TODO: fix this: fileHandler.writeDataRow(definition.getTableIdentifier(), compileDataRow(inputDataRow));
            processChildNodes(tx, inputDataRow);
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while writing to output file: " + definition.getOutputIdentifier(), null, inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
