package com.pilog.t8.dstream.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.dstream.output.textfile.T8TextFileOutputDefinition;
import com.pilog.t8.definition.dstream.output.textfile.T8TextFileOutputFieldDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.files.text.TabularTextOutputFile;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TextOutputFileHandler implements T8DStreamFileHandler
{
    private final T8TextFileOutputDefinition outputDefinition;
    private final T8Context context;
    private final T8FileManager fileManager;
    private final T8DStreamFileContext fileContext;
    private TabularTextOutputFile textOutputFile;
    private final Map<String, String> fieldToSourceMapping;
    private String fileContextIdentifier;
    private String directoryPath;
    private String filePath;

    public T8TextOutputFileHandler(T8Context context, T8TextFileOutputDefinition outputDefinition, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        this.context = context;
        this.outputDefinition = outputDefinition;
        this.fileManager = fileManager;
        this.fileContext = fileContext;
        this.fileContextIdentifier = null;
        this.directoryPath = null;
        this.filePath = null;
        this.fieldToSourceMapping = getFieldToSourceMapping();
    }

    @Override
    public boolean isOpen()
    {
        return (textOutputFile != null);
    }

    @Override
    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIdentifier, filePath);
    }

    @Override
    public File getCurrentFile()
    {
        return new File(filePath);
    }

    @Override
    public void openFile() throws Exception
    {
        OutputStream outputStream;

        fileContext.addOuputFileName(outputDefinition, outputDefinition.getFileName());
        fileContextIdentifier = fileContext.getFileContextInstanceIdentifier();
        directoryPath = fileContext.getOutputDirectoryPath(outputDefinition);
        filePath = fileContext.getOutputFilePath(outputDefinition);

        fileManager.createDirectory(context, fileContextIdentifier, directoryPath);
        outputStream = fileManager.getFileOutputStream(context, fileContextIdentifier, filePath, false);
        textOutputFile = new TabularTextOutputFile(outputDefinition.getEncoding().getCode(), outputDefinition.getSeparatorString());
        textOutputFile.openFile(outputStream);
    }

    public void writeDataRow(Map<String, Object> dataRow) throws Exception
    {
        Map<String, Object> mappedDataRow;

        // Map the input data row to the source fields.
        mappedDataRow = T8IdentifierUtilities.mapParameters(dataRow, fieldToSourceMapping);

        // If no rows have been written to the output file, write the column names first.
        if (textOutputFile.getRowCount() == 0)
        {
            textOutputFile.writeColumnNames(getColumnNames());
        }

        // Write the data row to the output file.
        textOutputFile.writeRow(mappedDataRow);
    }

    @Override
    public void closeFile() throws Exception
    {
        if (textOutputFile != null) textOutputFile.closeFile();
        textOutputFile = null;
    }

    @Override
    public void validateFile() throws Exception
    {
        //Validations are not neccesarry for output files
    }

    private List<String> getColumnNames()
    {
        List<String> columnNames;

        columnNames = new ArrayList<String>();
        for (T8TextFileOutputFieldDefinition fieldDefinition : outputDefinition.getFieldDefinitions())
        {
            columnNames.add(fieldDefinition.getTextColumnIdentifier());
        }

        return columnNames;
    }

    private Map<String, String> getFieldToSourceMapping()
    {
        List<T8TextFileOutputFieldDefinition> fieldDefinitions;
        Map<String, String> fieldMapping;

        fieldMapping = new LinkedHashMap<String, String>();
        fieldDefinitions = outputDefinition.getFieldDefinitions();
        for (T8TextFileOutputFieldDefinition fieldDefinition : fieldDefinitions)
        {
            fieldMapping.put(fieldDefinition.getIdentifier(), fieldDefinition.getTextColumnIdentifier());
        }

        return fieldMapping;
    }
}
