/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.dstream.utils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Bouwer.duPreez
 */
public class ColumnNameHandler
{
    public static HashMap<String, Object> normalizeColumnNames(HashMap<String, String> normalizedColumnNames, HashMap<String, Object> dataRow)
    {
        HashMap<String, Object> normalizedDataRow;

        normalizedDataRow = new HashMap<String, Object>();
        for (String columnName : dataRow.keySet())
        {
            normalizedDataRow.put(normalizedColumnNames.get(columnName), dataRow.get(columnName));
        }

        return normalizedDataRow;
    }

    public static HashMap<String, String> createNormalizedColumnNames(ArrayList<String> columnNames) throws Exception
    {
        HashMap<String, String> columnNameMap;

        columnNameMap = new HashMap<String, String>();
        for (String columnName : columnNames)
        {
            if (columnName != null)
            {
                String normalizedColumnName;

                normalizedColumnName = columnName.toUpperCase();
                normalizedColumnName = normalizedColumnName.replace(' ', '_');
                columnNameMap.put(columnName, normalizedColumnName);
            }
            else
            {
                throw new Exception("Null Column Name found!");
            }
        }

        return columnNameMap;
    }
}
