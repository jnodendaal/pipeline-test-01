package com.pilog.t8.dstream.utils;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class DBScriptRunner
{
    private static final String DEFAULT_DELIMITER = ";";
    private static final String DELIMITER_LINE_REGEX = "(?i)DELIMITER.+";
    private static final String DELIMITER_LINE_SPLIT_REGEX = "(?i)DELIMITER";
    private final Connection connection;
    private final boolean stopOnError;
    private final boolean autoCommit;
    private PrintWriter logWriter = new PrintWriter(System.out);
    private PrintWriter errorLogWriter = new PrintWriter(System.err);
    private String delimiter = DEFAULT_DELIMITER;
    private boolean fullLineDelimiter = false;
    private int lineNumber;

    public DBScriptRunner(Connection connection, boolean autoCommit, boolean stopOnError)
    {
        this.connection = connection;
        this.autoCommit = autoCommit;
        this.stopOnError = stopOnError;
    }

    public void setDelimiter(String delimiter, boolean fullLineDelimiter)
    {
        println("Setting delimiter: " + delimiter);
        this.delimiter = delimiter;
        this.fullLineDelimiter = fullLineDelimiter;
    }

    public void setLogWriter(PrintWriter logWriter)
    {
        this.logWriter = logWriter;
    }

    public void setErrorLogWriter(PrintWriter errorLogWriter)
    {
        this.errorLogWriter = errorLogWriter;
    }

    public void runScript(Reader reader) throws IOException, SQLException
    {
        runScript(connection, reader);
    }

    public int getLineNumber()
    {
        return lineNumber;
    }

    private void runScript(Connection connection, Reader reader) throws IOException, SQLException
    {
        StringBuffer command = null;

        try
        {
            LineNumberReader lineReader;
            String line = null;

            lineReader = new LineNumberReader(reader);
            while ((line = lineReader.readLine()) != null)
            {
                String trimmedLine;

                // Keep track of the current line number.
                lineNumber = lineReader.getLineNumber();

                // Create a new buffer to hold the next command if required.
                if (command == null)
                {
                    command = new StringBuffer();
                }
                
                // Trim the next line and handle it according to the type of text it contains.
                trimmedLine = line.trim();
                if (trimmedLine.startsWith("--"))
                {
                    println(trimmedLine);
                }
                else if (trimmedLine.startsWith("//"))
                {
                    println(trimmedLine);
                }
                else if (trimmedLine.length() < 1)
                {
                    // No statement - do nothing.
                }
                else if (trimmedLine.endsWith(getDelimiter()))
                {
                    Statement statement = null;
                    ResultSet resultSet = null;
                    boolean hasResults = false;
                    Pattern pattern;
                    Matcher matcher;

                    pattern = Pattern.compile(DELIMITER_LINE_REGEX);
                    matcher = pattern.matcher(trimmedLine);
                    if (matcher.matches())
                    {
                        setDelimiter(trimmedLine.split(DELIMITER_LINE_SPLIT_REGEX)[1].trim(), fullLineDelimiter);

                        line = lineReader.readLine();
                        if (line == null)
                        {
                            break;
                        }

                        trimmedLine = line.trim();
                    }

                    // Append the String up to the next delimiter to the command buffer.
                    command.append(line.substring(0, line.lastIndexOf(getDelimiter())));
                    command.append(" ");

                    // Create the next statement and execute it.
                    try
                    {
                        String commandString;

                        // Get the command String from the buffer.
                        commandString = command.toString();
                        if (commandString.toUpperCase().startsWith("EXEC"))
                        {
                            // The next command is an invocation of a Stored Procedure, so use a callable statement to execute it.
                            commandString = commandString.replace("execute", "call");
                            commandString = commandString.replace("EXECUTE", "call");
                            commandString = commandString.replace("exec", "call");
                            commandString = commandString.replace("EXEC", "call");
                            commandString = "{" + commandString + "}";
                            statement = connection.prepareCall(commandString);
                            ((CallableStatement)statement).execute();
                        }
                        else
                        {
                            statement = connection.createStatement();
                            hasResults = statement.execute(command.toString());
                        }

                        // Log the command that was executed.
                        println(command);
                    }
                    catch (SQLException e)
                    {
                        e.fillInStackTrace();
                        printlnError("Error executing: " + command);
                        printlnError(e);
                        if (stopOnError) throw e;
                    }

                    // Commit the connection if necessary.
                    if (autoCommit && !connection.getAutoCommit())
                    {
                        connection.commit();
                    }

                    // Get any results that might be available from the statement.
                    resultSet = statement.getResultSet();
                    if (hasResults && resultSet != null)
                    {
                        ResultSetMetaData resultSetMetaData;
                        int columnCount;
                        
                        resultSetMetaData = resultSet.getMetaData();
                        columnCount = resultSetMetaData.getColumnCount();
                        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                        {
                            String name = resultSetMetaData.getColumnLabel(columnIndex + 1);
                            print(name + "\t");
                        }

                        println("");
                        while (resultSet.next())
                        {
                            for (int i = 1; i <= columnCount; i++)
                            {
                                print(resultSet.getString(i) + "\t");
                            }

                            println("");
                        }
                    }

                    // Clear the command variable and close open resources.
                    command = null;
                    if (resultSet != null) resultSet.close();
                    if (statement != null) statement.close();
                }
                else
                {
                    Pattern pattern;
                    Matcher matcher;

                    pattern = Pattern.compile(DELIMITER_LINE_REGEX);
                    matcher = pattern.matcher(trimmedLine);
                    if (matcher.matches())
                    {
                        setDelimiter(trimmedLine.split(DELIMITER_LINE_SPLIT_REGEX)[1].trim(), fullLineDelimiter);
                        line = lineReader.readLine();
                        if (line == null)
                        {
                            break;
                        }

                        trimmedLine = line.trim();
                    }
                    
                    command.append(line);
                    command.append(" ");
                }
            }
            
            if (!autoCommit)
            {
                connection.commit();
            }
        }
        catch (SQLException e)
        {
            e.fillInStackTrace();
            printlnError("Error executing: " + command);
            printlnError(e);
            throw e;
        }
        catch (IOException e)
        {
            e.fillInStackTrace();
            printlnError("Error executing: " + command);
            printlnError(e);
            throw e;
        }
        finally
        {
            connection.rollback();
            flush();
        }
    }

    private String getDelimiter()
    {
        return delimiter;
    }

    private void print(Object o)
    {
        if (logWriter != null)
        {
            logWriter.print(o);
        }
    }

    private void println(Object o)
    {
        if (logWriter != null)
        {
            logWriter.println(o);
        }
    }

    private void printlnError(Object o)
    {
        if (errorLogWriter != null)
        {
            errorLogWriter.println(o);
        }
    }

    private void flush()
    {
        if (logWriter != null)
        {
            logWriter.flush();
        }

        if (errorLogWriter != null)
        {
            errorLogWriter.flush();
        }
    }
}
