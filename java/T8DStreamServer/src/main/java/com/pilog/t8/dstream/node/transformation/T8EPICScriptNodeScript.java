package com.pilog.t8.dstream.node.transformation;

import com.pilog.epic.ParserContext;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.dstream.T8DStream;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.script.T8DefaultServerContextScript;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EPICScriptNodeScript extends T8DefaultServerContextScript
{
    private T8ServerContextScriptDefinition definition;
    private T8DStream parentStream;
    private Map<String, Object> inputDataRow;
    private String nodeIdentifier;

    public T8EPICScriptNodeScript(T8Context context, T8ServerContextScriptDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    public String getIdentifier()
    {
        return definition.getIdentifier();
    }

    public void setInputDataRow(Map<String, Object> inputDataRow)
    {
        this.inputDataRow = inputDataRow;
    }

    public void setNodeIdentifier(String nodeIdentifier)
    {
        this.nodeIdentifier = nodeIdentifier;
    }

    public void setParentStream(T8DStream parentStream)
    {
        this.parentStream = parentStream;
    }

    @Override
    protected void addProgramImports(ParserContext parserContext) throws Exception
    {
        // Make sure the default imports are performed.
        super.addProgramImports(parserContext);

        // Method imports.
        parserContext.addMethodImport("raiseError", this, this.getClass().getDeclaredMethod("raiseError", String.class, String.class));
        parserContext.addMethodImport("getVar", this, this.getClass().getDeclaredMethod("getVariable", String.class));
        parserContext.addMethodImport("setVar", this, this.getClass().getDeclaredMethod("setVariable", String.class, Object.class));
    }

    public void setVariable(String variableIdentifier, Object variableValue)
    {
        parentStream.setVariable(variableIdentifier, variableValue);
    }

    public Object getVariable(String variableIdentifier)
    {
        return parentStream.getVariable(variableIdentifier);
    }

    public void raiseError(String detailMessage, String userMessage) throws Exception
    {
        throw new T8DStreamException(detailMessage, userMessage, nodeIdentifier, inputDataRow);
    }
}
