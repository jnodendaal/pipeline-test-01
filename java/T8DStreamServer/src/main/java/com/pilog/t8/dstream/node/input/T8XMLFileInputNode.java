package com.pilog.t8.dstream.node.input;

import com.pilog.t8.definition.dstream.node.T8DStreamInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.file.T8XMLInputFileHandler;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.ArrayList;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8XMLFileInputNode extends T8DStreamDefaultNode
{
    private String inputName;
    private String elementName;
    private String progressText;
    private ArrayList<Map<String, String>> xTractConfiguration;
    private boolean countRows;
    private T8XMLInputFileHandler fileHandler;
    private Object xTractNode;
    private volatile int rowsProcessed;
    private volatile int rowCount;
    private volatile boolean stopFlag;

    public static final String XTRACT_NODE_TYPE_STRING_LIST = "LIST";
    public static final String XTRACT_NODE_TYPE_STRING_MAP = "MAP";
    public static final String XTRACT_NODE_TYPE_STRING_VALUE = "VALUE";

    public static final String TYPE_NAME = "XML Input File";

    public T8XMLFileInputNode(T8Context context, T8DStreamInputNodeDefinition definition)
    {
        super(context, definition);
        this.countRows = false;
        this.stopFlag = false;
        this.rowsProcessed = 0;
        this.rowCount = -1;
    }

    public String getInputName()
    {
        return inputName;
    }

    public void setInputName(String inputName)
    {
        this.inputName = inputName;
    }

    public String getElementName()
    {
        return elementName;
    }

    public void setElementName(String sheetName)
    {
        this.elementName = sheetName;
    }

    public boolean isCountRows()
    {
        return countRows;
    }

    public void setCountRows(boolean countRows)
    {
        this.countRows = countRows;
    }

    public ArrayList<Map<String, String>> getXtractConfiguration()
    {
        return xTractConfiguration;
    }

    public void setXtractConfiguration(ArrayList<Map<String, String>> xTractConfiguration)
    {
        this.xTractConfiguration = xTractConfiguration;
    }

    @Override
    public String getTypeName()
    {
        return TYPE_NAME;
    }

    @Override
    public double getProgress()
    {
        if (rowCount == -1)
        {
            return -1;
        }
        else
        {
            return ((double)rowsProcessed/(double)rowCount*100.00);
        }
    }

    @Override
    public int getNodeIterationCount()
    {
        return rowCount;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return rowsProcessed;
    }

    @Override
    public String getProgressText()
    {
        return progressText;
    }

    public void setProgressText(String progressText)
    {
        this.progressText = progressText;
    }

    @Override
    public void stopExecution()
    {
        stopFlag = true;
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.fileHandler = (T8XMLInputFileHandler)parentStream.getInputFileHandler(inputName);
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
//        HashMap<String, Object> xmlDataRow;
//
//        // Count the number of input rows if required.
//        if ((countRows) && (rowCount == -1))
//        {
//            rowCount = fileHandler.getElementCount(elementName);
//        }
//
//        // Compile the Xtract Scheme is it has not yet been done.
//        if (xTractNode == null) xTractNode = compileXtractScheme(xTractConfiguration);
//
//        // Read all data rows from the source file.
//        while (((xmlDataRow = fileHandler.readDataRow(elementName, xTractNode)) != null) && (!stopFlag))
//        {
//            HashMap<String, Object> outputDataRow;
//
//            outputDataRow = new HashMap<String, Object>(inputDataRow);
//            outputDataRow.putAll(xmlDataRow);
//            processChildNodes(outputDataRow);
//            rowsProcessed++;
//        }
    }

//    private XtractNode compileXtractScheme(ArrayList<Map<String, String>> xTractData) throws Exception
//    {
//        XtractNode schemeNode = null;
//
//        for (Map<String, String> dataRow : xTractData)
//        {
//            XtractNode newNode;
//            String parentNodeName;
//            String xTractNodeName;
//            String dataName;
//            String valueExpression;
//            String conditionExpression;
//            String typeString;
//            NodeType nodeType;
//
//            parentNodeName = dataRow.get("PARENT_NODE");
//            xTractNodeName = dataRow.get("NODE_NAME");
//            dataName = dataRow.get("DATA_NAME");
//            valueExpression = dataRow.get("XPATH_VALUE_EXPRESSION");
//            conditionExpression = dataRow.get("XPATH_CONDITION_EXPRESSION");
//            typeString = dataRow.get("NODE_TYPE");
//            if (XTRACT_NODE_TYPE_STRING_LIST.equals(typeString)) nodeType = NodeType.LIST;
//            else if (XTRACT_NODE_TYPE_STRING_MAP.equals(typeString)) nodeType = NodeType.MAP;
//            else if (XTRACT_NODE_TYPE_STRING_VALUE.equals(typeString)) nodeType = NodeType.VALUE;
//            else throw new Exception("Invalid NODE_TYPE found: " + typeString);
//
//            newNode = new XtractNode(xTractNodeName, dataName, valueExpression, conditionExpression, nodeType);
//            if (schemeNode == null)
//            {
//                schemeNode = newNode;
//            }
//            else
//            {
//                schemeNode.addSubNode(parentNodeName, newNode);
//            }
//        }
//
//        if (schemeNode == null) throw new Exception("No Xtract scheme configuration found.");
//        return schemeNode;
//    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
