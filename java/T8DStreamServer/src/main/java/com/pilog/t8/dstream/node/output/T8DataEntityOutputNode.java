package com.pilog.t8.dstream.node.output;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.dstream.node.output.T8DataEntityOutputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityOutputNode extends T8DStreamDefaultNode implements Serializable
{
    private final T8DataEntityOutputNodeDefinition definition;
    private ArrayList<String> keyColumnNames;
    private T8DataEntityDefinition entityDefinition;
    private long processedRowCount;
    private boolean insert;
    private boolean update;
    private boolean delete;
    private boolean blind;

    public T8DataEntityOutputNode(T8Context context, T8DataEntityOutputNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.insert = definition.insert();
        this.update = definition.update();
        this.delete = definition.delete();
        this.blind = definition.blind();
    }

    public ArrayList<String> getKeyColumnNames()
    {
        return keyColumnNames;
    }

    public void setKeyColumnNames(ArrayList<String> keyColumnNames)
    {
        this.keyColumnNames = keyColumnNames;
    }

    public boolean isKeyColumn(String columnName)
    {
        return keyColumnNames.contains(columnName);
    }

    public void addKeyColumn(String columnName)
    {
        keyColumnNames.add(columnName);
    }

    public void clearKeyColumns()
    {
        keyColumnNames.clear();
    }

    public boolean isInsert()
    {
        return insert;
    }

    public void setInsert(boolean insert)
    {
        this.insert = insert;
    }

    public boolean isUpdate()
    {
        return update;
    }

    public void setUpdate(boolean update)
    {
        this.update = update;
    }

    public boolean isDelete()
    {
        return delete;
    }

    public void setDelete(boolean delete)
    {
        this.delete = delete;
    }

    public boolean isBlind()
    {
        return blind;
    }

    public void setBlind(boolean blind)
    {
        this.blind = blind;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.entityDefinition = (T8DataEntityDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, definition.getRootProjectId(), definition.getDataEntityIdentifier(), null);
            if (entityDefinition == null) throw new Exception("Entity required by stream not found: " + definition.getDataEntityIdentifier());
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            T8DataEntity existingDataEntity;
            T8DataEntity dataEntity;
            boolean recordExists;

            // Create a new entity from the input data.
            dataEntity = entityDefinition.getNewDataEntityInstance();

            //Insert data into entity
            dataEntity.setFieldValues(T8IdentifierUtilities.mapParameters(inputDataRow, definition.getFieldMapping()));

            // Check entity existence if necessary.
            existingDataEntity = null;
            recordExists = false;
            if (!blind)
            {
                existingDataEntity = tx.retrieve(dataEntity.getIdentifier(), dataEntity.getKeyFieldValues());
                recordExists = (existingDataEntity != null);
            }

            // Insert the entity if necessary.
            if (insert)
            {
                if (definition.blind() || !recordExists)
                {
                    tx.insert(dataEntity);
                }
            }

            // Update the entity if necessary.
            if (update)
            {
                if (definition.blind() || recordExists)
                {
                    // We don't do this step unless we need to
                    if (recordExists)
                    {
                        existingDataEntity.setFieldValues(T8IdentifierUtilities.mapParameters(inputDataRow, definition.getFieldMapping()));
                        tx.update(existingDataEntity);
                    } else tx.update(dataEntity);
                }
            }

            // Delete the entity if necessary.
            if (delete)
            {
                if (definition.blind() || recordExists)
                {
                    tx.delete(dataEntity);
                }
            }

            // Increment the processed entity count.
            processedRowCount++;

            // Process child nodes.
            processChildNodes(tx, inputDataRow);
        }
        catch (SQLException e)
        {
            //The client does not have access to sql exception object, so do not throw it up.
            throw new T8DStreamException("Exeption while reading from data entity: " + definition.getDataEntityIdentifier() + ". Cause: " + e.getMessage(), "Error updating data to database", definition.getIdentifier(), inputDataRow);
        }
        catch (Throwable e)
        {
            handleException(tx, e, "Exception while writing to data entity: " + definition.getDataEntityIdentifier(), "Error updating data to database", inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
