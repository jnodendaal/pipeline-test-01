package com.pilog.t8.dstream.node.input;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.dstream.node.input.T8TextFileInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.file.T8TextInputFileHandler;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8TextFileInputNode extends T8DStreamDefaultNode implements Serializable
{
    private T8TextFileInputNodeDefinition definition;
    private T8TextInputFileHandler fileHandler;
    private volatile int rowsProcessed;
    private volatile int rowCount;
    private volatile boolean stopFlag;
    private String fileNameOutputParameterId;

    public T8TextFileInputNode(T8Context context, T8TextFileInputNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.stopFlag = false;
        this.rowsProcessed = 0;
        this.rowCount = -1;
    }

    @Override
    public double getProgress()
    {
        if (rowCount == -1)
        {
            return -1;
        }
        else
        {
            return ((double)rowsProcessed/(double)rowCount*100.00);
        }
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return definition.getProgressText();
    }

    @Override
    public void stopExecution()
    {
        stopFlag = true;
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.fileHandler = (T8TextInputFileHandler)parentStream.getInputFileHandler(definition.getInputIdentifier());
            this.fileNameOutputParameterId = definition.getFileNameOutputParameterIdentifier();
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            // Only process this node if the input file is available.
            if (fileHandler.isFileAvailable())
            {
                Map<String, Object> textDataRow;

                // Count the number of input rows if required.
                if ((definition.countRows()) && (rowCount == -1))
                {
                    rowCount = fileHandler.getRowCount();
                }

                // Continue reading from the input file while more data rows are available and the stream has not been stopped.
                while (((textDataRow = fileHandler.readDataRow()) != null) && (!stopFlag))
                {
                    HashMap<String, Object> outputDataRow;

                    // Create the output data row.
                    outputDataRow = new HashMap<String, Object>(inputDataRow);
                    outputDataRow.putAll(T8IdentifierUtilities.mapParameters(textDataRow, definition.getOutputParameterMapping()));

                    // Add the input file name to the output data row if required.
                    if (fileNameOutputParameterId != null)
                    {
                        outputDataRow.put(fileNameOutputParameterId, fileHandler.getCurrentFile().getName());
                    }

                    // Process the child nodes using the output data row.
                    processChildNodes(tx, outputDataRow);
                    rowsProcessed++;
                }
            }
        }
        catch (Exception e)
        {
            if (hasExceptionFlow()) processExceptionFlow(tx, e, "Exception while reading from input file: " + definition.getInputIdentifier(), "Error while reading the text file", definition.getIdentifier(), inputDataRow);
            else if (parentStream.hasExceptionFlow()) parentStream.processExceptionFlow(e, "Exception while reading from input file: " + definition.getInputIdentifier(), "Error while reading the text file", definition.getIdentifier(), inputDataRow);
            else throw new T8DStreamException(e, "Exception while reading from input file: " + definition.getInputIdentifier(), "Error while reading the text file", definition.getIdentifier(), inputDataRow);
        }
        finally
        {
            //Report this nodes progress as 100% if it did not do anything
            if(rowCount < 1)
            {
                rowCount = rowsProcessed = rowsProcessed < 1 ? 1 : rowsProcessed;
            }
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
