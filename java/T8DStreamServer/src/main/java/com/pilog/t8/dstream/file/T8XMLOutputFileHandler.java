package com.pilog.t8.dstream.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.definition.dstream.output.T8XMLFileOutputDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.xml.dom.DOMHandler;
import com.pilog.t8.utilities.xml.domstream.DOMOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Bouwer du Preez
 */
public class T8XMLOutputFileHandler implements T8DStreamFileHandler
{
    private final T8XMLFileOutputDefinition outputDefinition;
    private final T8Context context;
    private final T8FileManager fileManager;
    private final T8DStreamFileContext fileContext;
    private DOMOutputStream outputStream;
    private int elementCount;
    private boolean documentElementWritten;
    private String fileContextIdentifier;
    private String directoryPath;
    private String filePath;

    public T8XMLOutputFileHandler(T8Context context, T8XMLFileOutputDefinition outputDefinition, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        this.context = context;
        this.outputDefinition = outputDefinition;
        this.fileManager = fileManager;
        this.fileContext = fileContext;
        this.elementCount = 0;
        this.documentElementWritten = false;
        this.fileContextIdentifier = null;
        this.directoryPath = null;
        this.filePath = null;
    }

    @Override
    public boolean isOpen()
    {
        return (outputStream != null);
    }

    @Override
    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIdentifier, filePath);
    }

    @Override
    public File getCurrentFile()
    {
        return new File(filePath);
    }

    @Override
    public void openFile() throws Exception
    {
        OutputStream stream;

        fileContext.addOuputFileName(outputDefinition, outputDefinition.getFileName());
        fileContextIdentifier = fileContext.getFileContextInstanceIdentifier();
        directoryPath = fileContext.getOutputDirectoryPath(outputDefinition);
        filePath = fileContext.getOutputFilePath(outputDefinition);

        fileManager.createDirectory(context, fileContextIdentifier, directoryPath);
        stream = fileManager.getFileOutputStream(context, fileContextIdentifier, filePath, false);
        outputStream = new DOMOutputStream();
        outputStream.setIndent(outputDefinition.isIndent());
        outputStream.setIndentAmount(outputDefinition.getIndentAmount());
        outputStream.open(stream);
        elementCount = 0;
        documentElementWritten = false;
    }

    public void writeXML(Document xmlDocument) throws Exception
    {
        ArrayList<Element> contentElements;
        Element documentElement;

        documentElement = xmlDocument.getDocumentElement();
        contentElements = DOMHandler.getChildElements(documentElement);

        if (!documentElementWritten)
        {
            outputStream.writeDocumentElement(documentElement);
            documentElementWritten = true;
        }

        for (Element contentElement : contentElements)
        {
            outputStream.writeElement(contentElement);
            elementCount++;
        }
    }

    @Override
    public void closeFile() throws Exception
    {
        if (outputStream != null) outputStream.close();
        outputStream = null;
    }

    @Override
    public void validateFile() throws Exception
    {
        //No validations are necesarry for output files
    }
}
