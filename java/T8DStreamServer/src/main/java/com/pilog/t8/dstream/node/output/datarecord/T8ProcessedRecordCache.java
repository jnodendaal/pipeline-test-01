package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.data.document.datarecord.DataRecord;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessedRecordCache
{
    private DataRecord processedRecord;
    private DataRecord existingRecord;

    public T8ProcessedRecordCache()
    {
    }

    public boolean containsRecord()
    {
        return processedRecord != null;
    }

    public void clear()
    {
        processedRecord = null;
        existingRecord = null;
    }

    public String getRecordID()
    {
        if (existingRecord != null)
        {
            return existingRecord.getID();
        }
        else if (processedRecord != null)
        {
            return processedRecord.getID();
        }
        else return null;
    }

    public DataRecord getProcessedRecord()
    {
        return processedRecord;
    }

    public void setProcessedRecord(DataRecord processedRecord)
    {
        this.processedRecord = processedRecord;
    }

    public DataRecord getExistingRecord()
    {
        return existingRecord;
    }

    public void setExistingRecord(DataRecord existingRecord)
    {
        this.existingRecord = existingRecord;
    }
}
