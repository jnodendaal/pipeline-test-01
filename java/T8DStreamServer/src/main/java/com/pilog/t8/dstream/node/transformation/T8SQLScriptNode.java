package com.pilog.t8.dstream.node.transformation;

import com.pilog.t8.definition.dstream.node.transformation.T8SQLScriptNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import com.pilog.t8.dstream.utils.DBScriptRunner;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8SQLScriptNode extends T8DStreamDefaultNode
{
    private T8SQLScriptNodeDefinition definition;
    private DBScriptRunner scriptRunner;
    private int lineCount;

    public T8SQLScriptNode(T8Context context, T8SQLScriptNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    public double getProgress()
    {
        if (scriptRunner != null)
        {
            return ((double)scriptRunner.getLineNumber()/(double)lineCount*100.00);
        }
        else return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        if (scriptRunner != null)
        {
            return lineCount;
        }
        else return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        if (scriptRunner != null)
        {
            return scriptRunner.getLineNumber();
        }
        else return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.lineCount = countScriptLines();
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
//        Connection connection;
//
//        connection = parentStream.getConnection(definition.getConnectionIdentifier());
//        if (connection != null)
//        {
//            scriptRunner = new DBScriptRunner(connection, false, true);
//            scriptRunner.runScript(new StringReader(definition.getSQLScript()));
//            scriptRunner = null; // Release the runner when execution of the script is complete.
//        }
//        else throw new T8DStreamException("The connection '" + definition.getConnectionIdentifier() + "' required by node ' " + definition + " ' could not be found.", definition.getIdentifier(), null);
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }

    private int countScriptLines() throws Exception
    {
        if (definition.getSQLScript() != null)
        {
            LineNumberReader lineReader;
            int lines;

            lines = 0;
            lineReader = new LineNumberReader(new StringReader(definition.getSQLScript()));
            while (lineReader.readLine() != null) lines++;
            return lines;
        }
        else return -1;
    }
}
