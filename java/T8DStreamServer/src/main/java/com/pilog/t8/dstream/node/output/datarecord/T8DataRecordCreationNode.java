package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.api.T8DataObjectApi;
import com.pilog.t8.api.T8DataRecordApi;
import com.pilog.t8.api.T8DataRequirementApi;
import com.pilog.t8.api.T8OntologyApi;
import com.pilog.t8.api.T8OrganizationApi;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceError;
import com.pilog.t8.data.document.datafile.T8DataFileAlteration;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordMetaDataType;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeConfiguration;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMergeContext;
import com.pilog.t8.data.document.datarecord.merger.T8DataRecordMerger;
import com.pilog.t8.data.document.datarecord.merger.T8DefaultDataRecordMergeContext;
import com.pilog.t8.data.document.datarecord.merger.T8DefaultDataRecordMerger;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.definition.dstream.node.T8DStreamNodeDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataConformanceErrorOutputDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationNodeDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRequirementInstanceMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationMappingDefinition.DataRecordMatchType;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DirectDataRequirementInstanceMappingDefinition;
import com.pilog.t8.data.document.datastring.T8DataStringFilter;
import com.pilog.t8.data.document.datastring.T8DataStringFilter.T8DataStringFilterBuilder;
import com.pilog.t8.dstream.T8DStream;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.T8DStreamNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import com.pilog.t8.utilities.cache.LRUCache;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.lang.reflect.InvocationTargetException;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationMappingDefinition;
import com.pilog.t8.data.document.datastring.T8DataStringGenerator;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8RecordStateMappingDefinition;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;


/**
 * @author Bouwer du Preez
 */
public class T8DataRecordCreationNode extends T8DStreamDefaultNode implements T8DataRecordCreationContext
{
    private final T8DataRecordCreationNodeDefinition definition;
    protected ArrayList<T8DStreamNode> recordConformanceErrorOutputNodes;
    private T8DataRecordValueStringGenerator fftValueStringGenerator;
    private Map<String, DataRequirementInstance> dataRequirementInstanceCache; // Temporarily stores Data Requirement Instance documents used for record creation.
    private final List<T8DataRecordCreationMapping> mappings;
    private T8DataStringGenerator renderer; // Only used if specific descriptions need to be generated.
    private T8OrganizationApi orgApi;
    private T8OntologyApi ontApi;
    private T8DataRecordApi recApi;
    private T8DataRequirementApi drApi;
    private T8OrganizationOntologyFactory ontologyFactory;
    private TerminologyProvider terminologyProvider;
    private final T8FileManager fileManager;
    private LRUCache<String, List<String>> conceptIdsByTerm;
    private LRUCache<String, List<String>> conceptIdsByCode;
    private final T8ProcessedRecordCache processedRecordCache;
    private boolean recordPersistenceEnabled;
    private final boolean recordExistenceCheck;
    private final Collection<RecordMetaDataType> recordMetaDataTypes;
    private T8DataTransaction currentTx; // Stores the transaction for which this node is currently set.
    private int rowsProcessed;
    private int statsPrintoutInterval;

    private static final int TERMINOLOGY_CACHE_SIZE = 5000; // Number of concepts for which terminology is cached by the terminology provider.
    private static final int CONCEPT_BY_TERM_CACHE_SIZE = 2000;
    private static final int CONCEPT_BY_CODE_CACHE_SIZE = 2000;

    private static final String PC_DATA_RECORD_OUTPUT = "DATA_RECORD_OUTPUT";

    private static final String PE_RECORD_CACHE_HITS = "RECORD_CACHE_HITS";
    private static final String PE_RECORD_CACHE_MISSES = "RECORD_CACHE_MISSES";
    private static final String PE_DR_INSTANCE_CACHE_HITS = "DR_INSTANCE_CACHE_HITS";
    private static final String PE_DR_INSTANCE_CACHE_MISSES = "DR_INSTANCE_CACHE_MISSES";
    private static final String PE_INPUT_ROW_PROCESSING = "INTPUT_ROW_PROCESSING";
    private static final String PE_DR_INSTANCE_RETRIEVAL = "DR_INSTANCE_RETRIEVAL";
    private static final String PE_RECORD_RETRIEVAL = "RECORD_RETRIEVAL";
    private static final String PE_RECORD_CONSTRUCTION = "RECORD_CONSTRUCTION";
    private static final String PE_RECORD_SAVE = "RECORD_SAVE";
    private static final String PE_RECORD_INSERT = "RECORD_INSERT";
    private static final String PE_RECORD_UPDATE = "RECORD_UPDATE";
    private static final String PE_CONFORMANCE_OUTPUT = "CONFORMANCE_OUTPUT";
    private static final String PE_ADDITIONAL_DESCRIPTION_GENERATION = "ADDITIONAL_DESCRIPTION_GENERATION";

    public T8DataRecordCreationNode(T8Context context, T8DataRecordCreationNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.fileManager = serverContext.getFileManager();
        this.recordConformanceErrorOutputNodes = new ArrayList<T8DStreamNode>();
        this.recordPersistenceEnabled = definition.isRecordPersistence();
        this.recordExistenceCheck = definition.isRecordExistenceCheck();
        this.recordMetaDataTypes = EnumSet.noneOf(RecordMetaDataType.class);
        this.mappings = new ArrayList<T8DataRecordCreationMapping>();
        if (definition.isDescriptionGeneration()) recordMetaDataTypes.add(RecordMetaDataType.DESCRIPTION);
        if (definition.isDuplicateDetectionHashGeneration()) recordMetaDataTypes.add(RecordMetaDataType.DUPLICATE_DETECTION_HASH);
        this.processedRecordCache = new T8ProcessedRecordCache();
        this.rowsProcessed = 0;
        this.statsPrintoutInterval = definition.getStatisticsPrintoutInterval();
        createRecordConformanceErrorOutputNodes();
    }

    private void createRecordConformanceErrorOutputNodes()
    {
        recordConformanceErrorOutputNodes.clear();
        for (T8DStreamNodeDefinition childNodeDefinition : definition.getConformanceErrorOutputNodeDefinitions())
        {
            try
            {
                T8DStreamNode childNode;

                childNode = childNodeDefinition.getNewNodeInstance(context);
                recordConformanceErrorOutputNodes.add(childNode);
                childNode.setParentNode(this);
            }
            catch(InvocationTargetException ex)
            {
               throw new RuntimeException("Failed to create node instance " + childNodeDefinition.getIdentifier() + ". \nCause: " + ((ex.getCause()!=null)?ex.getCause().getMessage():""),ex);
            }
            catch(Throwable ex)
            {
               throw new RuntimeException("Failed to create node instance " + childNodeDefinition.getIdentifier() + ". \nCause: " + ((ex!=null)?ex.getMessage():""),ex);
            }
        }
    }

    private void constructMappings()
    {
        try
        {
            for (T8DataRecordCreationMappingDefinition mappingDefinition : definition.getDataRequirementMappingDefinitions())
            {
                T8DataRecordCreationMapping mapping;

                mapping = mappingDefinition.createNewMappingInstance(this);
                mapping.setPerformanceStatistics(stats);
                mappings.add(mapping);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing record data mapping.", e);
        }
    }

    /**
     * This method may be used by the stream initialization script and sets the
     * flag to indicate whether generated records must be persisted or not.
     * @param enabled The boolean value to which the persistence flag will be
     * set.
     */
    public void setRecordPersistenceEnabled(boolean enabled)
    {
        recordPersistenceEnabled = enabled;
    }

    @Override
    public T8DataRecordCreationNodeDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public T8Context getContext()
    {
        return context;
    }

    @Override
    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    @Override
    public T8FileManager getFileManager()
    {
        return fileManager;
    }

    @Override
    public TerminologyProvider getTerminologyProvider()
    {
        return terminologyProvider;
    }

    @Override
    public T8OrganizationOntologyFactory getOntologyFactory()
    {
        return ontologyFactory;
    }

    @Override
    public T8DataRecordValueStringGenerator getFFTValueStringGenerator()
    {
        return fftValueStringGenerator;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
        stopConformanceErrorOutputNodes();
    }

    @Override
    public void setParentStream(T8DStream parentStream)
    {
        super.setParentStream(parentStream);
        for (T8DStreamNode childNode : recordConformanceErrorOutputNodes)
        {
            childNode.setParentStream(parentStream);
        }
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.dataRequirementInstanceCache = new HashMap<String, DataRequirementInstance>();
            this.conceptIdsByTerm = new LRUCache<String, List<String>>(CONCEPT_BY_TERM_CACHE_SIZE);
            this.conceptIdsByCode = new LRUCache<String, List<String>>(CONCEPT_BY_CODE_CACHE_SIZE);
            this.orgApi = tx.getApi(T8OrganizationApi.API_IDENTIFIER);
            this.ontApi = tx.getApi(T8OntologyApi.API_IDENTIFIER);
            this.recApi = tx.getApi(T8DataRecordApi.API_IDENTIFIER);
            this.drApi = tx.getApi(T8DataRequirementApi.API_IDENTIFIER);
            this.ontologyFactory = ontApi.getOntologyFactory();
            setTransaction(tx);
            constructMappings();
            initializeConformanceErrorOutputNodes(sessionContext, tx, streamStatistics, inputDataRow);
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    private void setTransaction(T8DataTransaction tx) throws Exception
    {
        if (this.currentTx != tx)
        {
            this.currentTx = tx;
            this.renderer = new T8DataStringGenerator(tx);
            this.terminologyProvider = ((T8TerminologyApi)tx.getApi(T8TerminologyApi.API_IDENTIFIER)).getTerminologyProvider();
            this.terminologyProvider.setLanguage(orgApi.getDefaultContentLanguageId());
            this.fftValueStringGenerator = new T8DataRecordValueStringGenerator();
            this.fftValueStringGenerator.setTerminologyProvider(terminologyProvider);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            T8DataRequirementInstanceMappingDefinition drInstanceMapping;

            // Set the transaction.
            setTransaction(tx);

            // Record the node start time.
            stats.logExecutionStart(PE_INPUT_ROW_PROCESSING);

            // Construct the mapped record.
            drInstanceMapping = definition.getDataRequirementInstanceMappingDefinition();
            if (drInstanceMapping instanceof T8DirectDataRequirementInstanceMappingDefinition)
            {
                DataRecordConstructionResult recordConstructionResult;
                Map<String, Object> outputDataRow;
                DataRecord constructedRecord;
                String recordOutputParameterIdentifier;
                String recordIDOutputParameterIdentifier;
                String drInstanceID;

                // Get the DR Instance ID from the mapping and construct the data record.
                drInstanceID = ((T8DirectDataRequirementInstanceMappingDefinition)drInstanceMapping).getDataRequirementInstanceIdentifier();
                stats.logExecutionStart(PE_RECORD_CONSTRUCTION);
                recordConstructionResult = constructDataRecord(tx, null, null, drInstanceID, inputDataRow);
                constructedRecord = recordConstructionResult.getDataRecord();
                stats.logExecutionEnd(PE_RECORD_CONSTRUCTION);

                // Save the constructed records.
                if (recordPersistenceEnabled)
                {
                    if (constructedRecord != null)
                    {
                        stats.logExecutionStart(PE_RECORD_SAVE);
                        saveDataRecord(tx, constructedRecord);
                        stats.logExecutionEnd(PE_RECORD_SAVE);
                    }
                }

                // Process conformance erro output.
                processConformanceErrorOutputNodes(tx, recordConstructionResult.getConformanceErrors(), inputDataRow);

                // Create the output data row.
                recordOutputParameterIdentifier = definition.getDataRecordOutputParameterIdentifier();
                recordIDOutputParameterIdentifier = definition.getDataRecordIDOutputParameterIdentifier();
                outputDataRow = new HashMap<String, Object>(inputDataRow);
                if (recordOutputParameterIdentifier != null) outputDataRow.put(recordOutputParameterIdentifier, constructedRecord);
                if (recordIDOutputParameterIdentifier != null) outputDataRow.put(recordIDOutputParameterIdentifier, constructedRecord != null ? constructedRecord.getID() : null);

                // Process child nodes.
                processChildNodes(tx, outputDataRow);

                // Record the node end time.
                stats.logExecutionEnd(PE_INPUT_ROW_PROCESSING);

                // Increment row counter.
                rowsProcessed++;
                if ((statsPrintoutInterval > 0) && (rowsProcessed % statsPrintoutInterval == 0))
                {
                    stats.printStatistics(System.out);
                }
            }
            else throw new Exception("The root DR Instance Mapping definition must be a direct type.");
        }
        catch (Exception e)
        {
            // Record the node end time.
            stats.logExecutionEnd(PE_INPUT_ROW_PROCESSING);
            handleException(tx, e, "Exception in record creation node.", null, inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
        // Save the last processed record still cached.
        saveProcessedRecords(tx);

        // Finzalize conformance output nodes.
        finalizeConformanceErrorOutputNodes(tx);
    }

    DataRequirementInstance getDataRequirementInstance(T8DataTransaction tx, String drInstanceId) throws Exception
    {
        DataRequirementInstance drInstance;

        drInstance = dataRequirementInstanceCache.get(drInstanceId);
        if (drInstance != null)
        {
            // Log the fact that the DR Instance was found in the cache.
            stats.logEvent(PE_DR_INSTANCE_CACHE_HITS);
            return drInstance;
        }
        else
        {
            // Log the start of DR Instance retrieval.
            stats.logEvent(PE_DR_INSTANCE_CACHE_MISSES);
            stats.logExecutionStart(PE_DR_INSTANCE_RETRIEVAL);

            drInstance = drApi.retrieveDataRequirementInstance(drInstanceId, null, false, false);
            dataRequirementInstanceCache.put(drInstanceId, drInstance);

            // Log the end of DR Instance retrieval.
            stats.logExecutionEnd(PE_DR_INSTANCE_RETRIEVAL);
            return drInstance;
        }
    }

    private DataRecord fetchCachedDataRecord(DataRecord recordToMatch, T8RecordMatchCriteria matchCriteria)
    {
        // Generate a value String for the record.
        recordToMatch.normalize();
        return null;
    }

    private void cacheDataRecord(DataRecord recordToCache, T8RecordMatchCriteria matchCriteria)
    {
        // Generate a value String for the record.
        recordToCache.normalize();
    }

    private DataRecord retrieveDataRecord(DataRecord recordToMatch, T8RecordMatchCriteria matchCriteria) throws Exception
    {
        // A new record caching strategy needs to be developed here.
        return null;
    }

    private void saveDataRecord(T8DataTransaction tx, DataRecord newRecord) throws Exception
    {
        T8DataRecordCreationMappingDefinition drMappingDefinition;
        T8RecordMatchCriteria matchCriteria;

        // Get the Data Requirement ID and the applicable mapping.
        drMappingDefinition = definition.getDataRequirementMappingDefinition(newRecord.getDataRequirement().getConceptID());

        // If record persistence is enabled, we wil try to find an existing record matching the generated record's value String.
        matchCriteria = new T8RecordMatchCriteria(drMappingDefinition.getDataRecordMatchType(), drMappingDefinition.getKeyPropertyIdentifiers(), drMappingDefinition.getMatchCodeTypeID());

        // If the newly generated record does not match the cache, save the previously processed record in the cache and then add the new record to the cache.
        if (newRecord != processedRecordCache.getProcessedRecord())
        {
            DataRecord existingRecord;

            // Save the already processed cache records.
            saveProcessedRecords(tx);

            // If existence checking is enabled, try to find a record in the DB that matches the newly generated record so that we can update the existing document.
            existingRecord = recordExistenceCheck ? retrieveDataRecord(newRecord, matchCriteria) : null;

            // Set the newly generated record (and the existing record from the DB, if any) in the cache.
            processedRecordCache.setExistingRecord(existingRecord);
            processedRecordCache.setProcessedRecord(newRecord);
        }
    }

    private void saveProcessedRecords(T8DataTransaction tx) throws Exception
    {
        final DataRecord existingRecord;
        final DataRecord processedRecord;

        // Get the processed records from the cache.
        existingRecord = processedRecordCache.getExistingRecord();
        processedRecord = processedRecordCache.getProcessedRecord();
        if (processedRecord != null)
        {
            T8DataRecordCreationMappingDefinition drMappingDefinition;
            T8RecordMatchCriteria matchCriteria;
            DataRecordMatchType recordMatchType;
            List<String> keyPropertyIdentifiers;
            String codeTypeID;

            // Get the Data Requirement ID and the applicable mapping.
            drMappingDefinition = definition.getDataRequirementMappingDefinition(processedRecord.getDataRequirement().getConceptID());
            recordMatchType = drMappingDefinition.getDataRecordMatchType();
            keyPropertyIdentifiers = drMappingDefinition.getKeyPropertyIdentifiers();
            codeTypeID = drMappingDefinition.getMatchCodeTypeID();
            matchCriteria = new T8RecordMatchCriteria(recordMatchType, keyPropertyIdentifiers, codeTypeID);

            // Save the processed record.
            if (existingRecord != null)
            {
                T8DataRecordMergeContext mergeContext;
                T8DataRecordMerger recordMerger;

                // Merge the new record with the existing one to form an complete new record.
                mergeContext = new T8DefaultDataRecordMergeContext(tx);
                recordMerger = new T8DefaultDataRecordMerger(mergeContext, new T8DataRecordMergeConfiguration());
                recordMerger.reset(); // Clear current state (insert/updated documents).
                recordMerger.mergeDataRecords(existingRecord, processedRecord);

                // Update the newly constructed record with the existing one and then save the changes to the existing record.
                stats.logExecutionStart(PE_RECORD_UPDATE);
                cacheDataRecord(existingRecord, matchCriteria); // Re-cache the existing record because we've added references to it.
                recApi.saveFileStateChange(existingRecord);
                stats.logExecutionEnd(PE_RECORD_UPDATE);
            }
            else // If a matched record was not found just insert the new record.
            {
                T8DataFileAlteration alteration;

                // Create the data file alteration.
                alteration = new T8DataFileAlteration(processedRecord);

                // Add the new records to the alteration.
                for (DataRecord newRecord : processedRecord.getDataRecords())
                {
                    alteration.addInsertedRecord(newRecord);
                }

                // Persist the new data record.
                stats.logExecutionStart(PE_RECORD_INSERT);
                cacheDataRecord(processedRecord, matchCriteria); // Cache the newly created record.
                recApi.saveFileStateChange(processedRecord);
                stats.logExecutionEnd(PE_RECORD_INSERT);

                // Set the state of the new record.
                setNewDataFileState(tx, processedRecord);

                // Generate any additionally specified descriptions.
                stats.logExecutionStart(PE_ADDITIONAL_DESCRIPTION_GENERATION);
                generateInsertDescriptions(processedRecord);
                stats.logExecutionEnd(PE_ADDITIONAL_DESCRIPTION_GENERATION);
            }
        }

        // Clear the cache.
        processedRecordCache.clear();

        // Call the processed method to indicate that a unit of work has been completed.
        processed(tx);
    }

    @Override
    public DataRecordConstructionResult constructDataRecord(T8DataTransaction tx, String rootRecordID, DataRecord parentRecord, String drInstanceID, Map<String, Object> dataParameters) throws Exception
    {
        DataRequirementInstance dataRequirementInstance;

        dataRequirementInstance = getDataRequirementInstance(tx, drInstanceID);
        if (dataRequirementInstance != null)
        {
            // Find an applicable mapping to use for record construction.
            for (T8DataRecordCreationMapping mapping : mappings)
            {
                if (mapping.isApplicable(dataRequirementInstance, dataParameters))
                {
                    return mapping.constructDataRecord(tx, rootRecordID, parentRecord, dataRequirementInstance, dataParameters);
                }
            }

            // No applicable mapping found.
            return new DataRecordConstructionResult(null, new ArrayList<T8DataRecordConformanceError>());
        }
        else return new DataRecordConstructionResult(null, new ArrayList<T8DataRecordConformanceError>());
    }

    @Override
    public List<String> getConceptIDsByCode(T8DataTransaction tx, String orgId, String ocId, String code) throws Exception
    {
        String matchString;

        matchString = ocId + code;
        if (conceptIdsByCode.containsKey(matchString))
        {
            return conceptIdsByCode.get(matchString);
        }
        else
        {
            List<String> conceptIDs;

            conceptIDs = ontApi.retrieveConceptIdsByCode(ocId, code, true);
            conceptIdsByCode.put(matchString, conceptIDs);
            return conceptIDs;
        }
    }

    @Override
    public List<String> getConceptIDsByTerm(T8DataTransaction tx, String orgID, String ocId, String term) throws Exception
    {
        String matchString;

        matchString = ocId + term;
        if (conceptIdsByTerm.containsKey(matchString))
        {
            return conceptIdsByTerm.get(matchString);
        }
        else
        {
            List<String> conceptIDs;

            conceptIDs = ontApi.retrieveConceptIdsByTerm(ocId, term, true);
            conceptIdsByTerm.put(matchString, conceptIDs);
            return conceptIDs;
        }
    }

    private void setNewDataFileState(T8DataTransaction tx, DataRecord newRecord) throws Exception
    {
        T8DataObjectApi objApi;

        objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
        for (T8RecordStateMappingDefinition stateMapping : definition.getStateMappingDefinitions())
        {
            if (stateMapping.isMappingApplicable(serverContext, sessionContext, newRecord))
            {
                String dataObjectIid;
                String dataObjectId;
                String stateId;

                dataObjectId = stateMapping.getDataObjectIdentifier(serverContext, sessionContext, newRecord);
                dataObjectIid = newRecord.getID();
                stateId = stateMapping.getStateIdentifier(serverContext, sessionContext, newRecord);

                objApi.saveState(dataObjectId, dataObjectIid, stateId);
                return;
            }
        }
    }

    private void generateInsertDescriptions(DataRecord record) throws Exception
    {
        if (renderer != null)
        {
            List<String> descriptionInstanceList;

            descriptionInstanceList = definition.getInsertDescriptionInstanceList();
            if ((descriptionInstanceList != null) && (descriptionInstanceList.size() > 0))
            {
                T8DataStringFilter filter;

                filter = new T8DataStringFilterBuilder().record(record.getID()).instances(descriptionInstanceList).build();
                renderer.refreshDataStrings(ArrayLists.newArrayList(filter));
            }
        }
    }

    protected void initializeConformanceErrorOutputNodes(T8SessionContext sessionContext, T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputParameters) throws Exception
    {
        for (T8DStreamNode childNode : recordConformanceErrorOutputNodes)
        {
            if (childNode.isEnabled())
            {
                childNode.initializeNode(tx, streamStatistics, inputParameters);
            }
        }
    }

    private void processConformanceErrorOutputNodes(T8DataTransaction tx, List<T8DataRecordConformanceError> errors, Map<String, Object> outputData) throws Exception
    {
        T8DataConformanceErrorOutputDefinition outputDefinition;

        // Log the start of the execution.
        stats.logExecutionStart(PE_CONFORMANCE_OUTPUT);
        outputDefinition = definition.getConformanceErrorOutputDefinition();
        if ((outputDefinition != null) && (recordConformanceErrorOutputNodes != null))
        {
            for (T8DataRecordConformanceError error : errors)
            {
                Map<String, Object> errorOutputDataRow;
                String recordIDParameterIdentifier;
                String drInstanceIDParameterIdentifier;
                String drIDParameterIdentifier;
                String classIDParameterIdentifier;
                String sectionIDParameterIdentifier;
                String propertyIDParameterIdentifier;
                String valueParameterIdentifier;
                String errorTypeParameterIdentifier;
                String errorMessageParameterIdentifier;

                recordIDParameterIdentifier = outputDefinition.getDataRecordIDParameterIdentifier();
                drInstanceIDParameterIdentifier = outputDefinition.getDRInstanceIDParameterIdentifier();
                drIDParameterIdentifier = outputDefinition.getDRIDParameterIdentifier();
                classIDParameterIdentifier = outputDefinition.getClassIDParameterIdentifier();
                sectionIDParameterIdentifier = outputDefinition.getSectionIDParameterIdentifier();
                propertyIDParameterIdentifier = outputDefinition.getPropertyIDParameterIdentifier();
                valueParameterIdentifier = outputDefinition.getValueParameterIdentifier();
                errorTypeParameterIdentifier = outputDefinition.getErrorTypeParameterIdentifier();
                errorMessageParameterIdentifier = outputDefinition.getErrorMessageParameterIdentifier();

                errorOutputDataRow = new HashMap<String, Object>();
                if (outputData != null) errorOutputDataRow.putAll(outputData);
                if (recordIDParameterIdentifier != null) errorOutputDataRow.put(recordIDParameterIdentifier, error.getRecordID());
                if (drInstanceIDParameterIdentifier != null) errorOutputDataRow.put(drInstanceIDParameterIdentifier, error.getDataRequirementInstanceID());
                if (drIDParameterIdentifier != null) errorOutputDataRow.put(drIDParameterIdentifier, error.getDataRequirementID());
                if (classIDParameterIdentifier != null) errorOutputDataRow.put(classIDParameterIdentifier, error.getClassID());
                if (sectionIDParameterIdentifier != null) errorOutputDataRow.put(sectionIDParameterIdentifier, error.getSectionID());
                if (propertyIDParameterIdentifier != null) errorOutputDataRow.put(propertyIDParameterIdentifier, error.getPropertyID());
                if (valueParameterIdentifier != null) errorOutputDataRow.put(valueParameterIdentifier, error.getValue());
                if (errorTypeParameterIdentifier != null) errorOutputDataRow.put(errorTypeParameterIdentifier, error.getErrorType().toString());
                if (errorMessageParameterIdentifier != null) errorOutputDataRow.put(errorMessageParameterIdentifier, error.getErrorMessage());
                for (T8DStreamNode errorOutputNode : recordConformanceErrorOutputNodes)
                {
                    if (errorOutputNode.isEnabled())
                    {
                        errorOutputNode.processNode(tx, errorOutputDataRow);
                    }
                }
            }
        }

        // Log the end of the execution.
        stats.logExecutionEnd(PE_CONFORMANCE_OUTPUT);
    }

    protected void stopConformanceErrorOutputNodes()
    {
        for (T8DStreamNode childNode : recordConformanceErrorOutputNodes)
        {
            childNode.stopExecution();
        }
    }

    protected void finalizeConformanceErrorOutputNodes(T8DataTransaction tx) throws Exception
    {
        for (T8DStreamNode childNode : recordConformanceErrorOutputNodes)
        {
            if (childNode.isEnabled())
            {
                childNode.finalizeNode(tx);
            }
        }
    }
}
