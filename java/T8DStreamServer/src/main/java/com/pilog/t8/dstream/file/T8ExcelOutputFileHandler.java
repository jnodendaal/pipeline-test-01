package com.pilog.t8.dstream.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputFieldDefinition;
import com.pilog.t8.definition.dstream.output.excelfile.T8ExcelFileOutputSheetDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.excel.ExcelOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelOutputFileHandler implements T8DStreamFileHandler
{
    private final T8ExcelFileOutputDefinition outputDefinition;
    private final T8Context context;
    private final T8FileManager fileManager;
    private final T8DStreamFileContext fileContext;
    private ExcelOutputStream excelOutputStream;
    private int sequenceNumber;
    private final Map<String, Map<String, String>> fieldToSourceMappings; // Used to cache mappings of sheet field parameter identifiers to the source column names.
    private String fileContextIid;
    private String directoryPath;
    private String filePath;

    public T8ExcelOutputFileHandler(T8Context context, T8ExcelFileOutputDefinition outputDefinition, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        this.context = context;
        this.outputDefinition = outputDefinition;
        this.fileManager = fileManager;
        this.fileContext = fileContext;
        this.sequenceNumber = 0;
        this.fieldToSourceMappings = new HashMap<>();
        this.fileContextIid = null;
        this.directoryPath = null;
        this.filePath = null;
    }

    @Override
    public boolean isOpen()
    {
        return (excelOutputStream != null);
    }

    @Override
    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIid, filePath);
    }

    @Override
    public File getCurrentFile()
    {
        return new File(filePath);
    }

    @Override
    public void openFile() throws Exception
    {
        OutputStream outputStream;

        fileContext.addOuputFileName(outputDefinition, outputDefinition.getFileName());
        fileContextIid = fileContext.getFileContextInstanceIdentifier();
        directoryPath = fileContext.getOutputDirectoryPath(outputDefinition);
        filePath = fileContext.getOutputFilePath(outputDefinition);

        fileManager.createDirectory(context, fileContextIid, directoryPath);
        outputStream = fileManager.getFileOutputStream(context, fileContextIid, filePath, false);
        excelOutputStream = new ExcelOutputStream();
        excelOutputStream.openFile(outputStream);
    }

    public void writeDataRow(String sheetIdentifier, Map<String, Object> dataRow) throws Exception
    {
        int recordCount;
        String sheetName;

        // Get the sheet name for the given identifier.
        sheetName = getSheetName(sheetIdentifier);
        if (sheetName != null)
        {
            Map<String, Object> mappedDataRow;
            Map<String, String> fieldToSourceMapping;

            // Map the input data to the correct fields.
            fieldToSourceMapping = getFieldToSourceMapping(sheetIdentifier);
            mappedDataRow = T8IdentifierUtilities.mapParameters(dataRow, fieldToSourceMapping);

            // Create the destination sheet if it does not yet exist.
            if (!excelOutputStream.containsSheet(sheetName))
            {
                excelOutputStream.writeSheet(sheetName, getColumnNames(sheetIdentifier));
                setExcelSheetCellStyles(sheetIdentifier);
            }

            // Write the row of data.
            recordCount = excelOutputStream.writeRow(sheetName, mappedDataRow);
            if (recordCount >= ExcelOutputStream.SHEET_ROW_LIMIT)
            {
                createNextSequenceFile();
            }
        }
        else throw new Exception("Could not resolve Excel output sheet name for sheet identifier: " + sheetIdentifier);
    }

    @Override
    public void closeFile() throws Exception
    {
        if (excelOutputStream != null) excelOutputStream.closeFile();
        excelOutputStream = null;
    }

    @Override
    public void validateFile() throws Exception
    {
        //No validations are required for outputs
    }

    private List<String> getColumnNames(String sheetIdentifier)
    {
        T8ExcelFileOutputSheetDefinition sheetDefinition;
        List<String> columnNames;

        columnNames = new ArrayList<>();
        sheetDefinition = outputDefinition.getSheetDefinition(sheetIdentifier);
        if (sheetDefinition != null)
        {
            for (T8ExcelFileOutputFieldDefinition fieldDefinition : sheetDefinition.getFieldDefinitions())
            {
                columnNames.add(fieldDefinition.getExcelColumnIdentifier());
            }

            return columnNames;
        }
        else throw new RuntimeException("Excel Sheet not found: " + sheetIdentifier);
    }

    private void setExcelSheetCellStyles(String sheetIdentifier)
    {
        T8ExcelFileOutputSheetDefinition sheetDefinition;
        String sheetName;
        int columnIndex;

        sheetDefinition = outputDefinition.getSheetDefinition(sheetIdentifier);
        sheetName = sheetDefinition.getSheetName();

        columnIndex = 0;
        for (T8ExcelFileOutputFieldDefinition fieldDefinition : sheetDefinition.getFieldDefinitions())
        {
            this.excelOutputStream.setCellFormatPattern(sheetName, columnIndex, fieldDefinition.getExcelColumnStyle().getStylePattern());
            columnIndex++;
        }
    }

    private String getSheetName(String sheetIdentifier)
    {
        T8ExcelFileOutputSheetDefinition sheetDefinition;

        sheetDefinition = outputDefinition.getSheetDefinition(sheetIdentifier);
        return sheetDefinition != null ? sheetDefinition.getSheetName() : null;
    }

    private Map<String, String> getFieldToSourceMapping(String sheetIdentifier)
    {
        // If the mapping is not available from the cache, create it now.
        if (!fieldToSourceMappings.containsKey(sheetIdentifier))
        {
            List<T8ExcelFileOutputFieldDefinition> fieldDefinitions;
            T8ExcelFileOutputSheetDefinition sheetDefinition;
            Map<String, String> fieldMapping;

            fieldMapping = new LinkedHashMap<>();
            sheetDefinition = outputDefinition.getSheetDefinition(sheetIdentifier);
            fieldDefinitions = sheetDefinition.getFieldDefinitions();
            for (T8ExcelFileOutputFieldDefinition fieldDefinition : fieldDefinitions)
            {
                fieldMapping.put(fieldDefinition.getIdentifier(), fieldDefinition.getExcelColumnIdentifier());
            }

            // Cache the newly created mapping.
            fieldToSourceMappings.put(sheetIdentifier, fieldMapping);
            return fieldMapping;
        }
        else
        {
            // Return the mapping from the cache.
            return fieldToSourceMappings.get(sheetIdentifier);
        }
    }

    private void createNextSequenceFile() throws Exception
    {
        OutputStream outputStream;
        String filePath;

        // Close the current file.
        closeFile();

        // Create the new workbook.
        outputStream = fileManager.getFileOutputStream(context, fileContext.getFileContextInstanceIdentifier(), getNextFilePath(), false);
        excelOutputStream = new ExcelOutputStream();
        excelOutputStream.openFile(outputStream);
    }

    private String getNextFilePath()
    {
        String originalFilePath;
        String filePath;

        // Create a new file path with the next sequence number appended to the original file name.
        originalFilePath = fileContext.getOutputDirectoryPath(outputDefinition);
        filePath = originalFilePath.substring(0, originalFilePath.lastIndexOf('.'));
        filePath = filePath + "_" + sequenceNumber + ".xlsx";
        sequenceNumber++;

        return filePath;
    }
}
