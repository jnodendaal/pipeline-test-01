package com.pilog.t8.dstream.node.transformation;

import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.transformation.T8ConditionNodeDefinition;
import com.pilog.t8.definition.dstream.node.transformation.T8ConditionNodeDefinition.ConditionFailureType;
import com.pilog.t8.dstream.ExpressionString;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ConditionNode extends T8DStreamDefaultNode
{
    private T8ConditionNodeDefinition definition;
    private T8DStreamDefinition streamDefinition;
    private ExpressionString conditionExpressionString;
    private ExpressionString failureMessageExpressionString;

    public T8ConditionNode(T8Context context, T8ConditionNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.conditionExpressionString = new ExpressionString(definition.getConditionExpression());
        this.failureMessageExpressionString = new ExpressionString(definition.getFailureMessageExpression());
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        this.streamDefinition = (T8DStreamDefinition)definition.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            Map<String, Object> expressionParameters;
            Object conditionResult;

            // Create a map that contains ALL of the stream parameters and then add the input parameter to it (this prevents the expression from referencing parameters that are not part of the input for this node).
            expressionParameters = streamDefinition.createStreamParameterMap();
            if (inputDataRow != null) expressionParameters.putAll(inputDataRow);

            // Now evaluate the expression.
            conditionResult = conditionExpressionString == null ? true : conditionExpressionString.evaluate(sessionContext, null, expressionParameters);

            // Only if the expression evaluated to true, do we process the child nodes.
            if (conditionResult == Boolean.TRUE)
            {
                processChildNodes(tx, inputDataRow);
            }
            else if (definition.getFailureType() == ConditionFailureType.STREAM_FAILURE)
            {
                Object errorMessage;

                // Throw the stream failure exception.
                errorMessage = failureMessageExpressionString.evaluate(sessionContext, null, expressionParameters);
                if (errorMessage instanceof String) throw new T8DStreamException("Stream failure on condition node '" + definition.getIdentifier() + "'.", (String)errorMessage, definition.getIdentifier(), inputDataRow);
                else throw new T8DStreamException("Error message expression did not return a String result: " + errorMessage, "Failed to evaluate a condition on the data stream", definition.getIdentifier(), inputDataRow);
            }
            else if ((definition.getFailureType() == ConditionFailureType.LOG_AND_SKIP_CHILD_NODES) || (definition.getFailureType() == ConditionFailureType.LOG_AND_PROCESS_CHILD_NODES))
            {
                Object errorMessage;

                // Log the error.
                errorMessage = (String)failureMessageExpressionString.evaluate(sessionContext, null, expressionParameters);
                if (errorMessage instanceof String) parentStream.getLogFile().log((String)errorMessage);
                else throw new T8DStreamException("Error message expression did not return a String result: " + errorMessage, "Failed to evaluate a condition on the data stream", definition.getIdentifier(), inputDataRow);

                // If necessary continue to process child nodes.
                if (definition.getFailureType() == ConditionFailureType.LOG_AND_PROCESS_CHILD_NODES)
                {
                    processChildNodes(tx, inputDataRow);
                }
            }
        }
        catch (Throwable e)
        {
            throw new T8DStreamException(e, "Exception while executing condition node using expression: " + definition.getConditionExpression(), "Failure occured while trying to evauluate a condition on the data stream", definition.getIdentifier(), inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
