package com.pilog.t8.dstream.node.transformation;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.dstream.node.transformation.T8GUIDGenerationNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8GUIDGenerationNode extends T8DStreamDefaultNode
{
    private final T8GUIDGenerationNodeDefinition definition;

    public T8GUIDGenerationNode(T8Context context, T8GUIDGenerationNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            Map<String, Object> outputParameters;

            // Add all input parameters to the output collection.
            outputParameters = new HashMap<String, Object>(inputDataRow);
            outputParameters.put(definition.getResultParameterIdentifier(), T8IdentifierUtilities.createNewGUID());

            // Process child nodes.
            processChildNodes(tx, outputParameters);
        }
        catch (Exception e)
        {
            throw new T8DStreamException(e, "Exception while executing GUID generation node.", "Failed to create a new Unique ID", definition.getIdentifier(), inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
