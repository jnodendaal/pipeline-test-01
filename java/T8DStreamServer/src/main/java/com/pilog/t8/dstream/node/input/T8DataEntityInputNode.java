package com.pilog.t8.dstream.node.input;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.definition.dstream.node.input.T8DataEntityInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityInputNode extends T8DStreamDefaultNode
{
    private final T8DataEntityInputNodeDefinition definition;
    private final T8DataFilterDefinition filterDefinition;
    private final String filterParameterIdentifier;
    private final String dataEntityIdentifier;
    private volatile int rowsProcessed;
    private volatile int rowCount;
    private volatile boolean stopFlag;

    public T8DataEntityInputNode(T8Context context, T8DataEntityInputNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.filterDefinition = definition.getDataFilterDefinition();
        this.filterParameterIdentifier = definition.getDataFilterInputParameterIdentifier();
        this.dataEntityIdentifier = definition.getDataEntityIdentifier();
        this.stopFlag = false;
        this.rowsProcessed = 0;
        this.rowCount = -1;
    }

    @Override
    public double getProgress()
    {
        if (rowCount == -1)
        {
            return -1;
        }
        else
        {
            return ((double)rowsProcessed/(double)rowCount*100.00);
        }
    }

    @Override
    public int getNodeIterationCount()
    {
        return rowCount;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return rowsProcessed;
    }

    @Override
    public String getProgressText()
    {
        return definition.getProgressText();
    }

    @Override
    public void stopExecution()
    {
        stopFlag = true;
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        // No specific behavior.
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        if (definition.isLookupMode())
        {
            processNodeLookupMode(tx, inputDataRow);
        }
        else
        {
            processNodeNormalMode(tx, inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx)
    {
    }

    public void processNodeLookupMode(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            List<T8DataEntity> fetchedEntities;
            T8DataFilter dataFilter;

            // Get the entity results.
            dataFilter = filterDefinition != null ? filterDefinition.getNewDataFilterInstance(context, inputDataRow) : null;
            fetchedEntities = tx.select(definition.getDataEntityIdentifier(), dataFilter, 0, 1);

            // Process a single result row.
            if (!stopFlag)
            {
                if (fetchedEntities.size() > 0)
                {
                    HashMap<String, Object> outputDataRow;
                    T8DataEntity inputEntity;

                    inputEntity = fetchedEntities.get(0);
                    outputDataRow = new HashMap<String, Object>(inputDataRow);
                    outputDataRow.putAll(T8IdentifierUtilities.mapParameters(inputEntity.getFieldValues(), definition.getOutputParameterMapping()));

                    // Process the child nodes.
                    processChildNodes(tx, outputDataRow);
                    rowsProcessed++;
                }
                else
                {
                    Map<String, String> outputParameterMapping;
                    HashMap<String, Object> outputDataRow;

                    // Create an output data row and clear all mapped stream parameters (because no results were retrieved from the entity).
                    outputDataRow = new HashMap<String, Object>(inputDataRow);
                    outputParameterMapping = definition.getOutputParameterMapping();
                    if (outputParameterMapping != null)
                    {
                        for (String streamParameterIdentifier : outputParameterMapping.values())
                        {
                            outputDataRow.put(streamParameterIdentifier, null); // Clear all mapped stream parameters.
                        }
                    }

                    // Process the child nodes.
                    processChildNodes(tx, outputDataRow);
                    rowsProcessed++;
                }
            }
        }
        catch (Exception e)
        {
            if (hasExceptionFlow()) processExceptionFlow(tx, e, "Exeption while reading from data entity: " + definition.getDataEntityIdentifier(), null, definition.getIdentifier(), inputDataRow);
            else if (parentStream.hasExceptionFlow()) parentStream.processExceptionFlow(e, "Exeption while reading from data entity: " + definition.getDataEntityIdentifier(), null, definition.getIdentifier(), inputDataRow);
            else throw new T8DStreamException(e, "Exeption while reading from data entity: " + definition.getDataEntityIdentifier(), "Error while reading the data from the database", definition.getIdentifier(), inputDataRow);
        }
        finally
        {
            //Report this nodes progress as 100% if it did not do anything
            if(rowCount < 1)
            {
                rowCount = rowsProcessed = rowsProcessed < 1 ? 1 : rowsProcessed;
            }
        }
    }

    public void processNodeNormalMode(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        T8DataEntityResults entityResults = null;

        try
        {
            T8DataFilter dataFilter;

            // Get the filter to use.
            if (filterParameterIdentifier != null)
            {
                // Get the data filter from the input parameters.
                dataFilter = (T8DataFilter)inputDataRow.get(filterParameterIdentifier);

                // Check that the data filter is based on the correct entity.
                if ((dataFilter != null) && (!Objects.equals(dataFilter.getEntityIdentifier(), dataEntityIdentifier)))
                {
                    throw new Exception("Input data filter is not based on the same entity as the node where it is used.  Input Filter Entity: " + dataFilter.getEntityIdentifier() + " Node Entity: " + dataEntityIdentifier);
                }
            }
            else if (filterDefinition != null)
            {
                dataFilter = filterDefinition != null ? filterDefinition.getNewDataFilterInstance(context, inputDataRow) : null;
            }
            else dataFilter = null;

            // Scroll through the data.
            entityResults = tx.scroll(dataEntityIdentifier, dataFilter);

            // Count the number of input rows if required.
            if ((definition.countRows()))
            {
                rowCount = tx.count(definition.getDataEntityIdentifier(), dataFilter);
            }

            // Process the result rows.
            while ((entityResults.next()) && (!stopFlag))
            {
                HashMap<String, Object> outputDataRow;
                T8DataEntity inputEntity;

                inputEntity = entityResults.get();
                outputDataRow = new HashMap<String, Object>(inputDataRow);
                outputDataRow.putAll(T8IdentifierUtilities.mapParameters(inputEntity.getFieldValues(), definition.getOutputParameterMapping()));
                processChildNodes(tx, outputDataRow);
                rowsProcessed++;
            }

            // Close the entity results
            entityResults.close();
        }
        catch (SQLException e)
        {
            //The client does not have access to sql exception object, so do not throw it up.
            throw new T8DStreamException("Exeption while reading from data entity: " + definition.getDataEntityIdentifier() + ". Cause: " + e.getMessage(), "Error while reading the data from the database", definition.getIdentifier(), inputDataRow);
        }
        catch (Exception e)
        {
            throw new T8DStreamException(e, "Exeption while reading from data entity: " + definition.getDataEntityIdentifier(), "Error while reading the data from the database", definition.getIdentifier(), inputDataRow);
        }
        finally
        {
            if (entityResults != null) entityResults.close();

            //Report this nodes progress as 100% if it did not do anything
            if(rowCount < 1)
            {
                rowCount = rowsProcessed = rowsProcessed < 1 ? 1 : rowsProcessed;
            }
        }
    }
}
