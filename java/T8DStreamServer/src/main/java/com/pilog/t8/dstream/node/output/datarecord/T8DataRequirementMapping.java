package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceChecker;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceError;
import com.pilog.t8.data.document.conformance.T8DataRecordConformanceError.ConformanceErrorType;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarecord.RecordProperty;
import com.pilog.t8.data.document.datarecord.RecordValue;
import com.pilog.t8.data.document.datarecord.attachment.T8AttachmentDetails;
import com.pilog.t8.data.document.datarecord.value.AttachmentList;
import com.pilog.t8.data.document.datarecord.value.Composite;
import com.pilog.t8.data.document.datarecord.value.ControlledConcept;
import com.pilog.t8.data.document.datarecord.value.DocumentReferenceList;
import com.pilog.t8.data.document.datarecord.value.Field;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.valuestring.T8DataRecordValueStringGenerator;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRequirementMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DefaultValueRequirementMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8PropertyRequirementMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8ValueRequirementMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.datatypes.T8DRInstanceGroupReferenceMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.datatypes.T8DocumentReferenceMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.datatypes.T8FileContextAttachmentMappingDefinition;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.FormattedString;
import com.pilog.t8.utilities.strings.Strings;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementMapping extends T8AbstractDataRecordCreationMapping implements T8DataRecordCreationMapping
{
    private final T8DataRequirementMappingDefinition definition;
    private final T8FileManager fileManager;
    private final T8DataRecordValueStringGenerator fftValueStringGenerator;
    private final List<String> matchParameterIdentifiers;
    private final Map<Integer, DataRecord> recordConstructionCache;
    private final T8ServerExpressionEvaluator expressionEvaluator;
    private String constructionRootRecordID;
    private final String conditionExpression;

    public T8DataRequirementMapping(T8DataRecordCreationContext parentNode, T8DataRequirementMappingDefinition definition)
    {
        super(parentNode, definition);
        this.definition = definition;
        this.fileManager = parentNode.getFileManager();
        this.fftValueStringGenerator = parentNode.getFFTValueStringGenerator();
        this.recordConstructionCache = new HashMap<Integer, DataRecord>();
        this.matchParameterIdentifiers = definition.getMatchParameterIdentifiers();
        this.conditionExpression = definition.getConditionExpression();
        this.expressionEvaluator = new T8ServerExpressionEvaluator(context);
    }

    @Override
    public boolean isApplicable(DataRequirementInstance dataRequirementInstance, Map<String, Object> dataParameters)
    {
        if (!Strings.isNullOrEmpty(conditionExpression))
        {
            try
            {
                if (expressionEvaluator.evaluateBooleanExpression(conditionExpression, dataParameters, null))
                {
                    return (dataRequirementInstance.getDataRequirement().getConceptID().equals(definition.getDataRequirementIdentifier()));
                }
                else return false;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while evaluating condition expression in node: " + definition, e);
            }
        }
        else
        {
            return (dataRequirementInstance.getDataRequirement().getConceptID().equals(definition.getDataRequirementIdentifier()));
        }
    }

    @Override
    public DataRecordConstructionResult constructDataRecord(T8DataTransaction tx, String inputRootRecordID, DataRecord parentRecord, DataRequirementInstance dataRequirementInstance, Map<String, Object> dataParameters) throws Exception
    {
        int matchingHash;

        // Generate a matching hashcode for the input parameters.
        matchingHash = createMatchingHashCode(parentRecord, matchParameterIdentifiers, dataParameters);
        if (matchingHash != 0) // If the matching hash is 0, no content was found and the row must be skipped.
        {
            List<T8DataRecordConformanceError> conformanceFailures;
            T8DataRecordConformanceChecker conformanceChecker;
            DataRequirement dataRequirement;
            DataRecord dataRecord;
            String recordFFT;
            String rootRecordID;

            // Get the Data Requirement ID and the applicable mapping.
            dataRequirement = dataRequirementInstance.getDataRequirement();

            // Check the construction cache to see if this record has already been constructed.
            dataRecord = recordConstructionCache.get(matchingHash);
            if (dataRecord == null)
            {
                String recordIDSourceParameterIdentifier;
                String newRecordID;

                // Generate the new record ID or use a mapping if available.
                recordIDSourceParameterIdentifier = definition.getRecordIDSourceParameterIdentifier();
                if (Strings.isNullOrEmpty(recordIDSourceParameterIdentifier))
                {
                    newRecordID = T8IdentifierUtilities.createNewGUID();
                }
                else
                {
                    newRecordID = (String)dataParameters.get(recordIDSourceParameterIdentifier);
                    if (Strings.isNullOrEmpty(newRecordID)) throw new RuntimeException("Parameter mapped for use as Record ID in definition '" + definition + "' is null: " + recordIDSourceParameterIdentifier);
                }

                // Record has not been constructed yet, so create the new record and add it to the cache.
                dataRecord = new DataRecord(dataRequirementInstance);
                dataRecord.setID(newRecordID);
                recordConstructionCache.put(matchingHash, dataRecord);
            }

            // Set the root record ID (if the one supplied to the method is null, it means we are constructing the root).
            rootRecordID = inputRootRecordID != null ? inputRootRecordID : dataRecord.getID();

            // Clear the cache if we are constructing a new root.
            if (!rootRecordID.equals(constructionRootRecordID))
            {
                constructionRootRecordID = rootRecordID;
                recordConstructionCache.clear(); // Clear the cache.
                recordConstructionCache.put(matchingHash, dataRecord); // We have to add the constructed record back in.
            }

            // Create a collection for conformance errors.
            conformanceFailures = new ArrayList<T8DataRecordConformanceError>();

            // Add the record FFT.
            recordFFT = (String)dataParameters.get(definition.getFFTSourceParameterIdentifier());
            dataRecord.appendFFTChecked(recordFFT, ",");

            // Construct the record content using the mapping definitions.
            stats.logExecutionStart(PE_RECORD_PROPERTY_CONSTRUCTION);
            for (PropertyRequirement propertyRequirement : dataRequirement.getPropertyRequirements())
            {
                T8PropertyRequirementMappingDefinition propertyMapping;
                String propertyID;

                propertyID = propertyRequirement.getConceptID();
                propertyMapping = definition.getPropertyMappingDefinition(propertyID);
                if (propertyMapping != null)
                {
                    String propertyConditionExpression;

                    propertyConditionExpression = propertyMapping.getConditionExpression();
                    if (Strings.isNullOrEmpty(propertyConditionExpression) || expressionEvaluator.evaluateBooleanExpression(propertyConditionExpression, dataParameters, null))
                    {
                        RecordValueConstructionResult recordValueConstructionResult = null;
                        T8ValueRequirementMappingDefinition valueMapping;
                        String propertyFFT;

                        // Get the property FFT value from the data parameters.
                        propertyFFT = (String)dataParameters.get(propertyMapping.getFFTSourceParameterIdentifier());

                        // Construct the property value.
                        valueMapping = propertyMapping.getValueMappingDefinition();
                        if (valueMapping instanceof T8DefaultValueRequirementMappingDefinition)
                        {
                            T8DefaultValueRequirementMappingDefinition defaultValueMapping;
                            ValueRequirement valueRequirement;

                            defaultValueMapping = (T8DefaultValueRequirementMappingDefinition)valueMapping;
                            valueRequirement = propertyRequirement.getValueRequirement();
                            if ((valueRequirement != null) && (valueRequirement.getRequirementType() == defaultValueMapping.getRequirementType()))
                            {
                                // Construct the value.
                                stats.logExecutionStart(PE_RECORD_VALUE_CONSTRUCTION);
                                recordValueConstructionResult = constructRecordValue(tx, rootRecordID, defaultValueMapping, valueRequirement, dataParameters, dataRecord);
                                stats.logExecutionEnd(PE_RECORD_VALUE_CONSTRUCTION);
                            }
                        }
                        else
                        {
                            ValueRequirement valueRequirement;

                            valueRequirement = propertyRequirement.getValueRequirement();
                            if ((valueRequirement != null) && (valueRequirement.getRequirementType() == valueMapping.getRequirementType()))
                            {
                                // Construct the value.
                                stats.logExecutionStart(PE_RECORD_VALUE_CONSTRUCTION);
                                recordValueConstructionResult = constructRecordValue(tx, rootRecordID, valueMapping, valueRequirement, dataParameters, dataRecord);
                                stats.logExecutionEnd(PE_RECORD_VALUE_CONSTRUCTION);
                            }
                        }

                        // Process the construction result.
                        if (recordValueConstructionResult != null)
                        {
                            Map<String, T8AttachmentDetails> attachmentDetails;
                            List<DataRecord> subRecords;
                            RecordValue recordValue;

                            recordValue = recordValueConstructionResult.getRecordValue();
                            subRecords = recordValueConstructionResult.getSubRecords();
                            attachmentDetails = recordValueConstructionResult.getAttachmentDetails();
                            conformanceFailures.addAll(recordValueConstructionResult.getConformanceErrors());

                            // Add all sub-records created during the value construction.
                            if ((subRecords != null) && (recordValue != null))
                            {
                                // Add all newly created sub-records.
                                for (DataRecord subRecord : subRecords)
                                {
                                    // We may have already added this record, so check first.
                                    if (!dataRecord.containsSubRecord(subRecord.getID()))
                                    {
                                        dataRecord.addSubRecord(subRecord);
                                    }
                                }
                            }

                            // If no value was successfully created for the property, create a String concatenation of all input data to be added to the property FFT.
                            if (recordValue == null)
                            {
                                // We can only create a value mapping FFT if we have a value mapping.
                                if (valueMapping != null)
                                {
                                    String valueFFT;

                                    valueFFT = createValueMappingFFT(valueMapping, dataParameters);
                                    if (valueFFT != null)
                                    {
                                        if (propertyFFT == null) propertyFFT = valueFFT;
                                        else propertyFFT += (" " + valueFFT);
                                    }
                                }
                            }
                            else // Add the FFT from the value to the property.
                            {
                                FormattedString valueFFT;

                                valueFFT = new FormattedString();
                                valueFFT.append(recordValue.getFFT());
                                for (RecordValue descendantValue : recordValue.getDescendantValues())
                                {
                                    valueFFT.append(descendantValue.getFFT());
                                }

                                if (valueFFT.length() > 0)
                                {
                                    if (propertyFFT == null) propertyFFT = valueFFT.toString();
                                    else propertyFFT += (" " + valueFFT.toString());
                                }
                            }

                            // We only add the property if a value has been constructed for it or if a non-null FFT value exists.
                            if ((recordValue != null) || (propertyFFT != null))
                            {
                                RecordProperty recordProperty;

                                recordProperty = dataRecord.getRecordProperty(propertyID);
                                if (recordProperty == null)
                                {
                                    recordProperty = new RecordProperty(propertyRequirement);
                                    recordProperty.setRecordValue(recordValue);
                                    recordProperty.setFFT(propertyFFT);
                                    dataRecord.setRecordProperty(recordProperty);
                                }
                                else
                                {
                                    if (recordValue instanceof DocumentReferenceList)
                                    {
                                        DocumentReferenceList existingReferenceList;
                                        DocumentReferenceList newReferenceList;

                                        existingReferenceList = (DocumentReferenceList)recordProperty.getRecordValue();
                                        newReferenceList = (DocumentReferenceList)recordValue;
                                        for (String newReferenceRecordID : newReferenceList.getReferenceIds())
                                        {
                                            if (!existingReferenceList.containsReference(newReferenceRecordID))
                                            {
                                                existingReferenceList.addReference(newReferenceRecordID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            stats.logExecutionEnd(PE_RECORD_PROPERTY_CONSTRUCTION);

            // If we found a valid section mapping and a section was added to the record, proceed with the rest of the record construction.
            if ((definition.isAllowEmptyRecord()) || (dataRecord.hasContent(true)))
            {
                Set<String> referencedRecordIDSet;
                Set<String> createdRecordIDSet;

                // Log statistics.
                stats.logExecutionStart(PE_RECORD_CONFORMANCE_CHECKING);

                // Construct the attributes and add them to the record.
                dataRecord.setAttributes(constructAttributes(definition.getAttributeMappingDefinitions(), dataParameters));

                // Add all mapped codes to the record ontology.
                addOntologyCodes(definition.getCodeMappingDefinitions(), dataParameters, dataRecord);

                // Check the record's conformance to its DR so that all invalid data is moved to FFT.
                conformanceChecker = new T8DataRecordConformanceChecker(fftValueStringGenerator);
                conformanceFailures.addAll(conformanceChecker.moveInvalidDataToFFT(dataRecord));

                // Normalize the record.
                dataRecord.normalize();

                // Make sure that all dependent records that are referenced, have been added to the object model.
                referencedRecordIDSet = dataRecord.getRecordReferenceIDSet(true, false);
                createdRecordIDSet = dataRecord.getDataRecordIDSet();
                referencedRecordIDSet.removeAll(createdRecordIDSet);
                if (referencedRecordIDSet.size() > 0) throw new RuntimeException("Constructed Data Record '" + dataRecord + "' contains the following record references that were not properly created: " + referencedRecordIDSet);

                // Return the completed data record if any sections (content values) were added to it, else return null.
                stats.logExecutionEnd(PE_RECORD_CONFORMANCE_CHECKING);
                return new DataRecordConstructionResult(dataRecord, conformanceFailures);
            }
            else return new DataRecordConstructionResult(null, new ArrayList<T8DataRecordConformanceError>());
        }
        else return new DataRecordConstructionResult(null, new ArrayList<T8DataRecordConformanceError>());
    }

    private String createValueMappingFFT(T8ValueRequirementMappingDefinition valueMapping, Map<String, Object> dataParameters)
    {
        List<T8Definition> mappingDefinitions;
        StringBuffer fft;

        // Get all the descendant mapping definitions.
        mappingDefinitions = valueMapping.getDescendentDefinitions();
        mappingDefinitions.add(valueMapping);

        // Concatenate all parameters used by the descendant mapping definitions.
        fft = new StringBuffer();
        for (T8Definition mappingDefinition : mappingDefinitions)
        {
            if (mappingDefinition instanceof T8DefaultValueRequirementMappingDefinition)
            {
                T8DefaultValueRequirementMappingDefinition valueMappingDefinition;
                RequirementType requirementType;

                valueMappingDefinition = (T8DefaultValueRequirementMappingDefinition)mappingDefinition;
                requirementType = RequirementType.getRequirementType(valueMappingDefinition.getRequirementDataTypeIdentifier());
                if (requirementType != RequirementType.DOCUMENT_REFERENCE)
                {
                    Object sourceParameter;

                    sourceParameter = dataParameters.get(valueMappingDefinition.getSourceParameterIdentifier());
                    if (sourceParameter != null)
                    {
                        fft.append(sourceParameter.toString());
                        fft.append(" ");
                    }
                }
            }
        }

        // Return the FFT result.
        return (fft.length() > 0) ? fft.toString().trim() : null;
    }

    private RecordValueConstructionResult constructRecordValue(T8DataTransaction tx, String rootRecordID, T8ValueRequirementMappingDefinition valueMapping, ValueRequirement valueRequirement, Map<String, Object> dataParameters, DataRecord dataRecord) throws Exception
    {
        List<T8DataRecordConformanceError> conformanceErrors;
        T8ServerExpressionEvaluator valueExpressionEvaluator;
        RequirementType requirementType;
        String requirementPropertyID;
        String requirementSectionID;
        String parentRecordID;

        // Get the document informantion that we need.
        parentRecordID = dataRecord.getID();
        requirementPropertyID = valueRequirement.getParentPropertyRequirement().getConceptID();
        requirementSectionID = valueRequirement.getParentPropertyRequirement().getParentSectionRequirement().getConceptID();

        // Create an expression evaluator to check condition expressions.
        valueExpressionEvaluator = new T8ServerExpressionEvaluator(context);

        // Create a collection to hold conformance failures.
        conformanceErrors = new ArrayList<T8DataRecordConformanceError>();

        // Get the value parameter and Requirement type.
        requirementType = valueRequirement.getRequirementType();
        if (RequirementType.ATTACHMENT == requirementType)
        {
            RecordValueConstructionResult result;

            // Create a collection to hold conformance failures.
            result = new RecordValueConstructionResult();
            result.setConformanceErrors(conformanceErrors);
            if (valueMapping instanceof T8FileContextAttachmentMappingDefinition)
            {
                T8FileContextAttachmentMappingDefinition mappingDefinition;
                T8FileDetails attachmentFileDetails;
                String attachmentId;
                String filename;
                String filepath;
                String fileContextId;

                // Log the start of the type construction.
                stats.logExecutionStart(RequirementType.ATTACHMENT.toString());

                // Get the mapping parameters.
                mappingDefinition = (T8FileContextAttachmentMappingDefinition)valueMapping;
                attachmentId = (String)dataParameters.get(mappingDefinition.getAttachmentIDParameterIdentifier());
                filename = (String)dataParameters.get(mappingDefinition.getAttachmentFilenameParameterIdentifier());
                filepath = (String)dataParameters.get(mappingDefinition.getSourceFilepathParameterIdentifier());
                fileContextId = mappingDefinition.getSourceFileContextIdentifier();

                // Open the file context to the target file.
                fileManager.openFileContext(context, attachmentId, fileContextId, new T8Minute(10));

                // Make sure the target file exists.
                if ((!Strings.isNullOrEmpty(filepath)) && (fileManager.fileExists(context, attachmentId, filepath)))
                {
                    AttachmentList attachmentList;
                    long fileSize;

                    // Get the target file size and create an object specifying the file details.
                    fileSize = fileManager.getFileSize(context, attachmentId, filepath);
                    attachmentFileDetails = new T8FileDetails(attachmentId, fileContextId, filepath, fileSize, false, null);

                    // Construct the attachment and attachment file values.
                    attachmentList = new AttachmentList(valueRequirement);
                    attachmentList.addAttachment(attachmentId);
                    attachmentList.addAttachmentDetails(HashMaps.newHashMap(attachmentId, attachmentFileDetails));
                    result.setRecordValue(attachmentList);

                    // Close the file context and return the result.
                    fileManager.closeFileContext(context, attachmentId);
                    stats.logExecutionEnd(RequirementType.ATTACHMENT.toString());
                    return result;
                }
                else
                {
                    String requirementDRInstanceID;
                    String requirementDRID;
                    String requirementClassID;

                    // The attachment file was not found at the specified loacation so create an appropriate conformance error.
                    requirementDRInstanceID = valueRequirement.getParentDataRequirementInstance().getConceptID();
                    requirementDRID = valueRequirement.getParentDataRequirement().getConceptID();
                    requirementClassID = valueRequirement.getParentDataRequirement().getClassConceptID();
                    conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.REQUIRED_DATA_NOT_FOUND, parentRecordID, requirementDRInstanceID, requirementDRID, requirementClassID, requirementSectionID, requirementPropertyID, filepath, "Attachment file '" + attachmentId + "' not found: " + filepath));

                    // Close the file context and return the result.
                    fileManager.closeFileContext(context, attachmentId);
                    stats.logExecutionEnd(RequirementType.ATTACHMENT.toString());
                    return result;
                }
            }
            else throw new Exception("Unsupported attachment mapping definition type: " + valueMapping);
        }
        else if (RequirementType.DOCUMENT_REFERENCE == requirementType)
        {
            T8DocumentReferenceMappingDefinition documentReferenceMapping;
            DocumentReferenceList documentReference;
            List<DataRecord> subRecords;
            String organizationID;

            // Log the start of the type construction.
            stats.logExecutionStart(RequirementType.DOCUMENT_REFERENCE.toString());

            // Cast the value mapping to the convenient type.
            documentReferenceMapping = (T8DocumentReferenceMappingDefinition)valueMapping;

            // Get the organizationID.
            organizationID = null; // Setting this organization Identifier will force matching of concepts only for the specified organization.  If null, all organizations in the tree will be used.

            // Create the parent document reference value.
            documentReference = new DocumentReferenceList(valueRequirement);

            // Add all DR Instance Group References.
            subRecords = new ArrayList<DataRecord>();
            for (T8DRInstanceGroupReferenceMappingDefinition drInstanceGroupReferenceMapping : documentReferenceMapping.getDRInstanceGroupReferenceMappingDefinitions())
            {
                String documentReferenceConditionExpression;

                documentReferenceConditionExpression = drInstanceGroupReferenceMapping.getConditionExpression();
                if ((Strings.isNullOrEmpty(documentReferenceConditionExpression)) || (valueExpressionEvaluator.evaluateBooleanExpression(documentReferenceConditionExpression, dataParameters, null)))
                {
                    String drInstanceID;
                    String drInstanceGroupID;
                    String drInstanceTerm;
                    String drInstanceCode;
                    String recordID;
                    String drInstanceGroupParameterIdentifier;
                    String drInstanceIDParameterIdentifier;
                    String recordIDParameterIdentifier;
                    String restrictionODTID;

                    // Get some details from the mappping definition.
                    drInstanceID = drInstanceGroupReferenceMapping.getDRInstanceID();
                    drInstanceGroupID = drInstanceGroupReferenceMapping.getDRInstanceGroupID();
                    recordID = drInstanceGroupReferenceMapping.getRecordID();
                    drInstanceGroupParameterIdentifier = drInstanceGroupReferenceMapping.getDRInstanceGroupParameterIdentifier();
                    drInstanceIDParameterIdentifier = drInstanceGroupReferenceMapping.getDRInstanceIDParameterIdentifier();
                    recordIDParameterIdentifier = drInstanceGroupReferenceMapping.getRecordIDParameterIdentifier();
                    restrictionODTID = drInstanceGroupID != null ? drInstanceGroupID : (String)dataParameters.get(drInstanceGroupParameterIdentifier);

                    // If the DR Instance ID is null, try to fetch it from the stream parameters.
                    if (Strings.isNullOrEmpty(drInstanceID)) drInstanceID = (String)dataParameters.get(drInstanceIDParameterIdentifier);

                    // If the Record ID is null, try to fetch it from the stream parameters.
                    if (Strings.isNullOrEmpty(recordID)) recordID = (String)dataParameters.get(recordIDParameterIdentifier);

                    // Get the String to use for DR Instance Term Matching.
                    drInstanceTerm = (String)dataParameters.get(drInstanceGroupReferenceMapping.getDRInstanceTermParameterIdentifier());

                    // Get the String to use for DR Instance Code Matching.
                    drInstanceCode = (String)dataParameters.get(drInstanceGroupReferenceMapping.getDRInstanceCodeParameterIdentifier());

                    // We can only proceed if we have at least a record ID, DR Instance ID, DR Instance term or code.  If not, there is no record to construct.
                    if ((!Strings.isNullOrEmpty(recordID)) || (!Strings.isNullOrEmpty(drInstanceID)) || (!Strings.isNullOrEmpty(drInstanceTerm)) || (!Strings.isNullOrEmpty(drInstanceCode)))
                    {
                        // If we have a record ID we can construct the reference directly without any further steps.
                        if (!Strings.isNullOrEmpty(recordID))
                        {
                            documentReference.addReference(recordID);
                            stats.logExecutionEnd(RequirementType.DOCUMENT_REFERENCE.toString());
                            return new RecordValueConstructionResult(documentReference, null, null, conformanceErrors);
                        }
                        else
                        {
                            // If we do not have DR Instance ID, find it by matching the supplied code or term.
                            if (Strings.isNullOrEmpty(drInstanceID))
                            {
                                drInstanceID = findConceptId(tx, organizationID, restrictionODTID, drInstanceTerm, drInstanceCode);
                            }

                            // If we managed to create a record value (reference) from either a DR Instance Reference or a Group Reference, construct the sub-record.
                            if (!Strings.isNullOrEmpty(drInstanceID))
                            {
                                DataRecordConstructionResult referenceRecordConstructionResult;

                                // Use the matched reference value and create the referenced sub-record and then use the returned reference ID to create the value.
                                referenceRecordConstructionResult = creationContext.constructDataRecord(tx, rootRecordID, dataRecord, drInstanceID, dataParameters);
                                if (referenceRecordConstructionResult.getDataRecord() != null)
                                {
                                    DataRecord subRecord;

                                    subRecord = referenceRecordConstructionResult.getDataRecord();
                                    subRecords.add(subRecord);
                                    documentReference.addReference(subRecord.getID());
                                }

                                // Add all conformance errors to the collection.
                                conformanceErrors.addAll(referenceRecordConstructionResult.getConformanceErrors());
                            }
                            else
                            {
                                String requirementDRInstanceID;
                                String requirementDRID;
                                String requirementClassID;
                                String drInstanceMatchString;
                                Map<String, String> inputData;

                                // No matching DR Instance found, so add a conformance error.
                                requirementDRInstanceID = valueRequirement.getParentDataRequirementInstance().getConceptID();
                                requirementDRID = valueRequirement.getParentDataRequirement().getConceptID();
                                requirementClassID = valueRequirement.getParentDataRequirement().getClassConceptID();
                                drInstanceMatchString = drInstanceTerm != null ? drInstanceTerm : drInstanceCode;

                                inputData = new HashMap<String, String>();
                                inputData.put("RECORD_ID", recordID);
                                inputData.put("DR_INSTANCE_ID", drInstanceID);
                                inputData.put("DR_INSTANCE_MATCH_TERM", drInstanceTerm);
                                inputData.put("DR_INSTANCE_MATCH_CODE", drInstanceCode);
                                inputData.put("DR_INSTANCE_GROUP_ID", drInstanceGroupID);
                                conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.CONCEPT_MATCH_NOT_FOUND, parentRecordID, requirementDRInstanceID, requirementDRID, requirementClassID, requirementSectionID, requirementPropertyID, drInstanceMatchString, "No matching DR Instance Group Reference Requirement found for input data: " + inputData));
                            }
                        }
                    }
                }
            }

            // Log the end of the operation.
            stats.logExecutionEnd(RequirementType.DOCUMENT_REFERENCE.toString());

            // If we actually match and created any referenced records, return the document reference value or else return null.
            if (documentReference.getReferenceCount() > 0)
            {
                return new RecordValueConstructionResult(documentReference, subRecords, null, conformanceErrors);
            }
            else return new RecordValueConstructionResult(null, subRecords, null, conformanceErrors);
        }
        else return new RecordValueConstructionResult(null, null, null, conformanceErrors);
    }

    private RecordValueConstructionResult constructRecordValue(T8DataTransaction tx, String rootRecordID, T8DefaultValueRequirementMappingDefinition valueMapping, ValueRequirement valueRequirement, Map<String, Object> dataParameters, DataRecord dataRecord) throws Exception
    {
        List<T8DataRecordConformanceError> conformanceErrors;
        T8ServerExpressionEvaluator valueExpressionEvaluator;
        String valueConditionExpression;
        RequirementType requirementType;
        Object valueParameter;
        String propertyID;
        String sectionID;
        String parentRecordID;

        // Get the document information that we need.
        parentRecordID = dataRecord.getID();
        propertyID = valueRequirement.getParentPropertyRequirement().getConceptID();
        sectionID = valueRequirement.getParentPropertyRequirement().getParentSectionRequirement().getConceptID();

        // Create an expression evaluator to check condition expressions.
        valueExpressionEvaluator = new T8ServerExpressionEvaluator(context);
        valueConditionExpression = valueMapping.getConditionExpression();

        // Create a collection to hold conformance failures.
        conformanceErrors = new ArrayList<T8DataRecordConformanceError>();

        // First check the condition expression of the mapping.
        if ((Strings.isNullOrEmpty(valueConditionExpression)) || (valueExpressionEvaluator.evaluateBooleanExpression(valueConditionExpression, dataParameters, null)))
        {
            // Get the value parameter and Requirement type.
            valueParameter = dataParameters.get(valueMapping.getSourceParameterIdentifier());
            requirementType = valueRequirement.getRequirementType();
            if (RequirementType.COMPOSITE_TYPE.equals(requirementType))
            {
                List<T8DefaultValueRequirementMappingDefinition> fieldMappings;
                RecordValue compositeValue;

                // Create the composite value.
                compositeValue = new Composite(valueRequirement);

                // Run through all Field mappings and check each one for applicability.
                fieldMappings = valueMapping.getValueMappingDefinitions(RequirementType.FIELD_TYPE.getID());
                for (T8DefaultValueRequirementMappingDefinition fieldMapping : fieldMappings)
                {
                    String mappingConditionExpression;
                    String fieldConceptID;

                    // Get some details from the mappping definition.
                    mappingConditionExpression = fieldMapping.getConditionExpression();
                    fieldConceptID = fieldMapping.getRequirementConceptIdentifier();

                    // First check the condition expression of the mapping.
                    if ((!Strings.isNullOrEmpty(mappingConditionExpression)) && (!valueExpressionEvaluator.evaluateBooleanExpression(mappingConditionExpression, dataParameters, null)))
                    {
                        // Continue with the loop, since the condition expression did not evaluate to true.
                        continue;
                    }
                    else
                    {
                        List<ValueRequirement> fieldRequirements;

                        // Get the matching field requirement.
                        fieldRequirements = valueRequirement.getSubRequirements(RequirementType.FIELD_TYPE, fieldConceptID, null);
                        if (fieldRequirements.size() > 0)
                        {
                            ValueRequirement fieldRequirement;
                            ValueRequirement fieldValueRequirement;
                            List<T8DefaultValueRequirementMappingDefinition> fieldValueMappings;

                            fieldRequirement = fieldRequirements.get(0);
                            fieldValueRequirement = fieldRequirement.getFirstSubRequirement();
                            if (fieldValueRequirement != null)
                            {
                                fieldValueMappings = fieldMapping.getValueMappingDefinitions(fieldValueRequirement.getRequirementType().getID());
                                if (fieldValueMappings.size() > 0)
                                {
                                    T8DefaultValueRequirementMappingDefinition fieldValueMapping;
                                    RecordValueConstructionResult fieldValueConstructionResult;
                                    RecordValue fieldContentValue;

                                    // Construct the field content value.
                                    fieldValueMapping = fieldValueMappings.get(0);
                                    fieldValueConstructionResult = constructRecordValue(tx, rootRecordID, fieldValueMapping, fieldValueRequirement, dataParameters, dataRecord);

                                    // Get the constructed field content value and if it was successfully constructed, add it to the field.
                                    fieldContentValue = fieldValueConstructionResult.getRecordValue();
                                    if (fieldContentValue != null)
                                    {
                                        RecordValue fieldValue;

                                        // We only add field values if the field content value was successfully constructed.
                                        fieldValue = new Field(fieldRequirement);
                                        fieldValue.setSubValue(fieldContentValue);
                                        compositeValue.setSubValue(fieldValue);
                                    }
                                    else // No value created from the field mapping, so create an FFT for the value.
                                    {
                                        String valueFFT;

                                        valueFFT = createValueMappingFFT(fieldValueMapping, dataParameters);
                                        compositeValue.appendFFTChecked(valueFFT, " ");
                                    }

                                    // Always add the conformance errors.
                                    conformanceErrors.addAll(fieldValueConstructionResult.getConformanceErrors());
                                }
                            }
                        }
                    }
                }

                // Check the composite value and if any fields were successfully added, return the composite value
                if (compositeValue.getSubValues().size() > 0)
                {
                    return new RecordValueConstructionResult(compositeValue, null, null, conformanceErrors);
                }
                else return new RecordValueConstructionResult(null, null, null, conformanceErrors);
            }
            else if ((RequirementType.DATE_TYPE.equals(requirementType)) || (RequirementType.DATE_TIME_TYPE.equals(requirementType)) || (RequirementType.TIME_TYPE.equals(requirementType)))
            {
                if (valueParameter != null)
                {
                    String dateString;

                    // Get the String representation of the value parameter.
                    dateString = valueParameter.toString().trim();

                    // Try to format the string (it may already be a long value so check for that as well).
                    if (Strings.isInteger(dateString))
                    {
                        RecordValue newValue;

                        newValue = RecordValue.createValue(valueRequirement);
                        newValue.setValue(dateString);
                        return new RecordValueConstructionResult(newValue, null, null, conformanceErrors);
                    }
                    else // Ok so the mapped value is not a long and we need to format the string in order to get the millisecond long value.
                    {
                        String formatPattern;

                        // Get the format to use.
                        formatPattern = (String)valueRequirement.getAttribute(RequirementAttribute.DATE_TIME_FORMAT_PATTERN.getIdentifier());
                        if (!Strings.isNullOrEmpty(formatPattern))
                        {
                            try
                            {
                                SimpleDateFormat dateFormat;
                                RecordValue newValue;
                                Date date;

                                dateFormat = new SimpleDateFormat(formatPattern);
                                date = dateFormat.parse(dateString);
                                newValue = RecordValue.createValue(valueRequirement);
                                newValue.setValue(((Long)date.getTime()).toString());
                                return new RecordValueConstructionResult(newValue, null, null, conformanceErrors);
                            }
                            catch (Exception e)
                            {
                                String drInstanceID;
                                String drID;
                                String classID;

                                // No matching concept found, so just return a null.
                                drInstanceID = valueRequirement.getParentDataRequirementInstance().getConceptID();
                                drID = valueRequirement.getParentDataRequirement().getConceptID();
                                classID = valueRequirement.getParentDataRequirement().getClassConceptID();
                                conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.INVALID_DATA_FORMAT, parentRecordID, drInstanceID, drID, classID, sectionID, propertyID, dateString, "Invalid date format: " + dateString));
                                return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                            }
                        }
                        else
                        {
                            String drInstanceID;
                            String drID;
                            String classID;

                            // No matching concept found, so just return a null.
                            drInstanceID = valueRequirement.getParentDataRequirementInstance().getConceptID();
                            drID = valueRequirement.getParentDataRequirement().getConceptID();
                            classID = valueRequirement.getParentDataRequirement().getClassConceptID();
                            conformanceErrors.add(new T8DataRecordConformanceError(T8DataRecordConformanceError.ConformanceErrorType.INVALID_REQUIREMENT, parentRecordID, drInstanceID, drID, classID, sectionID, propertyID, dateString, "No Date Time format specified."));
                            return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                        }
                    }
                }
                else
                {
                    return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                }
            }
            else if (RequirementType.CONTROLLED_CONCEPT.equals(requirementType))
            {
                if (valueParameter != null)
                {
                    ControlledConceptRequirement conceptRequirement;
                    String conceptMatchString;
                    String organizationParameterIdentifier;
                    String organizationID;
                    String conceptID;

                    // Get the String representation of the value parameter.
                    stats.logExecutionStart(RequirementType.CONTROLLED_CONCEPT.toString());
                    conceptRequirement = (ControlledConceptRequirement)valueRequirement;
                    conceptMatchString = valueParameter.toString().trim();
                    organizationParameterIdentifier = valueMapping.getOrganizationParameterIdentifier();
                    organizationID = organizationParameterIdentifier != null ? (String)dataParameters.get(organizationParameterIdentifier) : null;

                    // If the concept match string is a valid ID, use it directly, else find a concept matching the string by code or term.
                    if (T8IdentifierUtilities.isConceptID(conceptMatchString))
                    {
                        conceptID = conceptMatchString;
                    }
                    else
                    {
                        conceptID = findConceptId(tx, organizationID, conceptRequirement.getOntologyClassID(), conceptMatchString, conceptMatchString);
                    }

                    // If we found a valid concept ID, use it to conctruct the controlled concept value.
                    if (!Strings.isNullOrEmpty(conceptID))
                    {
                        ControlledConcept controlledConcept;

                        controlledConcept = new ControlledConcept(conceptRequirement);
                        controlledConcept.setConceptId(conceptID, false);
                        stats.logExecutionEnd(RequirementType.CONTROLLED_CONCEPT.toString());
                        return new RecordValueConstructionResult(controlledConcept, null, null, conformanceErrors);
                    }
                    else
                    {
                        String drInstanceID;
                        String drID;
                        String classID;

                        // No matching concept found, so just return a null.
                        drInstanceID = valueRequirement.getParentDataRequirementInstance().getConceptID();
                        drID = valueRequirement.getParentDataRequirement().getConceptID();
                        classID = valueRequirement.getParentDataRequirement().getClassConceptID();
                        conformanceErrors.add(new T8DataRecordConformanceError(ConformanceErrorType.CONCEPT_MATCH_NOT_FOUND, parentRecordID, drInstanceID, drID, classID, sectionID, propertyID, conceptMatchString, "No matching concept found for input data: " + conceptMatchString));
                        stats.logExecutionEnd(RequirementType.CONTROLLED_CONCEPT.toString());
                        return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                    }
                }
                else
                {
                    return new RecordValueConstructionResult(null, null, null, conformanceErrors);
                }
            }
            else if (RequirementType.getValueDataTypes().contains(requirementType))
            {
                RecordValue recordValue;
                List<DataRecord> subRecords;

                // Create the new record value and construct its sub-values.
                recordValue = RecordValue.createValue(valueRequirement);
                subRecords = new ArrayList<DataRecord>();
                for (T8DefaultValueRequirementMappingDefinition subValueMapping : valueMapping.getValueMappingDefinitions())
                {
                    List<ValueRequirement> subValueRequirements;

                    subValueRequirements = valueRequirement.getSubRequirements(RequirementType.getRequirementType(subValueMapping.getRequirementDataTypeIdentifier()), null, null);
                    for (ValueRequirement subValueRequirement : subValueRequirements)
                    {
                        RecordValueConstructionResult subValueConstructionResult;
                        RecordValue subValue;

                        // Construct the sub-value and add the constructed value (if not null) to this value.
                        subValueConstructionResult = constructRecordValue(tx, rootRecordID, subValueMapping, subValueRequirement, dataParameters, dataRecord);
                        subValue = subValueConstructionResult.getRecordValue();
                        if (subValue != null)
                        {
                            recordValue.setSubValue(subValue);
                        }

                        if (subValueConstructionResult.getSubRecords() != null)
                        {
                            subRecords.addAll(subValueConstructionResult.getSubRecords());
                        }

                        // Add the sub-value conformance failures to those of this value.
                        conformanceErrors.addAll(subValueConstructionResult.getConformanceErrors());
                    }
                }

                // Set the text value of the record value object.
                if (valueParameter != null)
                {
                    recordValue.setValue(valueParameter.toString());
                }

                // If no valid value text or sub-values were added to the value, we return null to indicate that it was empty.
                if ((recordValue.getValue() != null) || (recordValue.getSubValues().size() > 0))
                {
                    return new RecordValueConstructionResult(recordValue, subRecords, null, conformanceErrors);
                }
                else return new RecordValueConstructionResult(null, subRecords, null, conformanceErrors);
            }
            else return new RecordValueConstructionResult(null, null, null, conformanceErrors);
        }
        else return new RecordValueConstructionResult(null, null, null, conformanceErrors);
    }
}
