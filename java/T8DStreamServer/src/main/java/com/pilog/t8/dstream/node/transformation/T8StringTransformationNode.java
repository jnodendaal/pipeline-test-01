package com.pilog.t8.dstream.node.transformation;

import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.transformation.T8StringTransformationNodeDefinition;
import com.pilog.t8.definition.dstream.node.transformation.T8StringTransformationNodeDefinition.TransformationType;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Hennie Brink
 */
public class T8StringTransformationNode extends T8DStreamDefaultNode
{
    private final T8StringTransformationNodeDefinition definition;
    private T8DStreamDefinition streamDefinition;
    private TransformationType transformationType;

    public T8StringTransformationNode(T8Context context, T8StringTransformationNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.streamDefinition = (T8DStreamDefinition)definition.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            this.transformationType = definition.getTransformationType();
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            Map<String, Object> outputDataRow;

            outputDataRow = new HashMap<>(inputDataRow);

            for (Map.Entry<String, String> entrySet : definition.getTransformationParameterMapping().entrySet())
            {
                Object fieldValue;

                fieldValue =  inputDataRow.get(entrySet.getKey());

                if(fieldValue == null) continue; //Just ignore it if we recieve null values

                if(fieldValue instanceof String)
                {
                    String fielValueText;

                    fielValueText = (String)fieldValue;
                    switch(transformationType)
                    {
                        case LOWER_CASE:
                            outputDataRow.put(entrySet.getValue(), toLower(fielValueText));
                            break;
                        case UPPER_CASE:
                            outputDataRow.put(entrySet.getValue(), toUpper(fielValueText));
                            break;
                        case SENTENCE_CASE:
                            outputDataRow.put(entrySet.getValue(), toSentence(fielValueText));
                            break;
                        case TITLE_CASE:
                            outputDataRow.put(entrySet.getValue(), toTitle(fielValueText));
                            break;
                        default:
                            throw new AssertionError("Unkown Transformation Type: " + transformationType);
                    }
                }
                else
                {
                    throw new AssertionError("Expected String type but found " + fieldValue.getClass().getName() + " instead.");
                }
            }

            // Process child nodes using the output data row.
            processChildNodes(tx, outputDataRow);
        }
        catch (Exception e)
        {
            throw new T8DStreamException(e, "Exception while executing String Transformation node.", "Failure occured while executing a String Transformation node", definition.getIdentifier(), inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }

    private String toLower(String text)
    {
        return text.toLowerCase();
    }

    private String toUpper(String text)
    {
        return text.toUpperCase();
    }

    private String toSentence(String text)
    {
        String result = "";
        if (text.length() == 0)
        {
            return result;
        }

        char firstChar = text.charAt(0);
        char firstCharToUpperCase = Character.toUpperCase(firstChar);
        result += firstCharToUpperCase;
        boolean terminalCharacterEncountered = false;
        char[] terminalCharacters =
        {
            '.', '?', '!'
        };
        for (int i = 1; i < text.length(); i++)
        {
            char currentChar = text.charAt(i);
            if (terminalCharacterEncountered)
            {
                if (currentChar == ' ')
                {
                    result += currentChar;
                }
                else
                {
                    char currentCharToUpperCase = Character.toUpperCase(currentChar);
                    result += currentCharToUpperCase;
                    terminalCharacterEncountered = false;
                }
            }
            else
            {
                char currentCharToLowerCase = Character.toLowerCase(currentChar);
                result += currentCharToLowerCase;
            }
            for (int j = 0; j < terminalCharacters.length; j++)
            {
                if (currentChar == terminalCharacters[j])
                {
                    terminalCharacterEncountered = true;
                    break;
                }
            }
        }
        return result;
    }

    private String toTitle(String text)
    {
        String result = "";
        if (text.length() == 0)
        {
            return result;
        }

        char firstChar = text.charAt(0);
        char firstCharToUpperCase = Character.toUpperCase(firstChar);
        result += firstCharToUpperCase;
        boolean terminalCharacterEncountered = false;
        char[] terminalCharacters =
        {
            '.', '?', '!', ' ', '-'
        };
        for (int i = 1; i < text.length(); i++)
        {
            char currentChar = text.charAt(i);
            if (terminalCharacterEncountered)
            {
                if (currentChar == ' ')
                {
                    result += currentChar;
                }
                else
                {
                    char currentCharToUpperCase = Character.toUpperCase(currentChar);
                    result += currentCharToUpperCase;
                    terminalCharacterEncountered = false;
                }
            }
            else
            {
                char currentCharToLowerCase = Character.toLowerCase(currentChar);
                result += currentCharToLowerCase;
            }
            for (int j = 0; j < terminalCharacters.length; j++)
            {
                if (currentChar == terminalCharacters[j])
                {
                    terminalCharacterEncountered = true;
                    break;
                }
            }
        }
        return result;
    }
}
