package com.pilog.t8.dstream;

import com.pilog.t8.utilities.files.text.LogFile;
import java.io.OutputStream;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDStreamLogFile extends LogFile implements T8DStreamLogFile
{
    public T8DefaultDStreamLogFile(OutputStream outputStream)
    {
        super (outputStream, LogFile.ENCODING_UTF_8);
    }
}
