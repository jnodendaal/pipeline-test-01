package com.pilog.t8.dstream.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.commons.io.RandomAccessStream;
import com.pilog.t8.definition.dstream.input.T8DataFileInputDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.dstream.utils.ColumnNameHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.files.data.DataFileHandler;
import java.io.File;
import java.util.HashMap;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileInputFileHandler implements T8DStreamFileHandler
{
    private T8DataFileInputDefinition inputDefinition;
    private T8Context context;
    private DataFileHandler dataFileHandler;
    private T8FileManager fileManager;
    private T8DStreamFileContext fileContext;
    private HashMap<String, String> normalizedColumnNames;
    private int rowIndex;
    private String fileContextIdentifier;
    private String filePath;

    public T8DataFileInputFileHandler(T8Context context, T8DataFileInputDefinition inputDefinition, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        this.context = context;
        this.inputDefinition = inputDefinition;
        this.fileManager = fileManager;
        this.fileContext = fileContext;
        this.fileContextIdentifier = fileContext.getFileContextInstanceIdentifier();
        this.filePath = fileContext.getInputFilePath(inputDefinition);
    }

    @Override
    public boolean isOpen()
    {
        return (dataFileHandler != null) && (dataFileHandler.isFileOpen());
    }

    @Override
    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIdentifier, filePath);
    }

    @Override
    public File getCurrentFile()
    {
        return new File(fileContext.getInputFilePath(inputDefinition));
    }

    @Override
    public void openFile() throws Exception
    {
        RandomAccessStream stream;

        stream = fileManager.getFileRandomAccessStream(context, fileContextIdentifier, filePath);
        dataFileHandler = new DataFileHandler();
        dataFileHandler.setSecretKey(inputDefinition.getPassword().getBytes());
        dataFileHandler.openFile(stream, false);
        rowIndex = 0;
    }

    public int getRowCount(String tableName) throws Exception
    {
        return dataFileHandler.getTableRecordCount(tableName);
    }

    public HashMap<String, Object> readDataRow(String tableName) throws Exception
    {
        HashMap<String, Object> dataRow;

        // If the column names have not been normalized, do it now.
        if (normalizedColumnNames == null)
        {
            normalizedColumnNames = ColumnNameHandler.createNormalizedColumnNames(dataFileHandler.getTableColumnNames(tableName));
        }

        // Read the next data row from the input file.
        dataRow = dataFileHandler.readDataRow(tableName, rowIndex++);

        // If a next data row was found, return it after normalizing its column names.
        return dataRow != null ? ColumnNameHandler.normalizeColumnNames(normalizedColumnNames, dataRow) : null;
    }

    @Override
    public void closeFile() throws Exception
    {
        if (dataFileHandler != null) dataFileHandler.closeFile();
        dataFileHandler = null;
    }

    @Override
    public void validateFile() throws Exception
    {
        //TODO: Add input file validations
    }
}
