package com.pilog.t8.dstream;

import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class ConnectionSettings implements Serializable
{
    // The ID used to verify serialized object version.
    static final long serialVersionUID = 1L;

    private String connectionName;
    private String driverClassName;
    private String connectionString;
    private String username;
    private String password;
    private String schema;

    public String getConnectionName()
    {
        return connectionName;
    }

    public void setConnectionName(String connectionName)
    {
        this.connectionName = connectionName;
    }

    public String getDriverClassName()
    {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName)
    {
        this.driverClassName = driverClassName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getConnectionString()
    {
        return connectionString;
    }

    public void setConnectionString(String connectionString)
    {
        this.connectionString = connectionString;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }
}
