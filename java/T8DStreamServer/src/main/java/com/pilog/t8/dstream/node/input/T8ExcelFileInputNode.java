package com.pilog.t8.dstream.node.input;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.dstream.node.input.T8ExcelFileInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.file.T8ExcelInputFileHandler;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import com.pilog.t8.utilities.collections.Maps;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelFileInputNode extends T8DStreamDefaultNode
{
    private final T8ExcelFileInputNodeDefinition definition;
    private T8ExcelInputFileHandler fileHandler;
    private volatile int rowsProcessed = 0;
    private volatile int rowCount = -1;
    private volatile boolean stopFlag = false;
    private String fileNameOutputParameterIdentifier;
    private String rowNumberOutputParameterIdentifier;

    public T8ExcelFileInputNode(T8Context context, T8ExcelFileInputNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.rowsProcessed = 0;
        this.rowCount = -1;
        this.stopFlag = false;
    }

    @Override
    public double getProgress()
    {
        if (rowCount == -1)
        {
            return -1;
        }
        else
        {
            return ((double)rowsProcessed/(double)rowCount*100.00);
        }
    }

    @Override
    public int getNodeIterationCount()
    {
        return rowCount;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return rowsProcessed;
    }

    @Override
    public String getProgressText()
    {
        return definition.getProgressText();
    }

    @Override
    public void stopExecution()
    {
        stopFlag = true;
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.fileHandler = (T8ExcelInputFileHandler)parentStream.getInputFileHandler(definition.getInputIdentifier());
            this.fileNameOutputParameterIdentifier = definition.getFileNameOutputParameterIdentifier();
            this.rowNumberOutputParameterIdentifier = definition.getRowNumberOutputParameterIdentifier();
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            // Only process this node if the input file is available.
            if (fileHandler.isFileAvailable())
            {
                Map<String, Object> excelDataRow;

                // Count the number of input rows if required.
                if ((definition.countRows()) && (rowCount == -1))
                {
                    rowCount = fileHandler.getRowCount(definition.getSheetIdentifier());
                }

                // Continue reading from the input file while more data rows are available and the stream has not been stopped.
                while (((excelDataRow = fileHandler.readDataRow(definition.getSheetIdentifier())) != null) && (!stopFlag))
                {
                    //Check if the data row is empty, if it is not, then continue processing
                    if (!Maps.isNullOrValuesAllNull(excelDataRow))
                    {
                        HashMap<String, Object> outputDataRow;

                        // Create the output data row.
                        outputDataRow = new HashMap<String, Object>(inputDataRow);
                        outputDataRow.putAll(T8IdentifierUtilities.mapParameters(excelDataRow, definition.getOutputParameterMapping()));

                        // Add the input file name to the output data row if required.
                        if (fileNameOutputParameterIdentifier != null)
                        {
                            outputDataRow.put(fileNameOutputParameterIdentifier, fileHandler.getCurrentFile().getName());
                        }

                        // Add the row number to the output data row if required.
                        if (rowNumberOutputParameterIdentifier != null)
                        {
                            outputDataRow.put(rowNumberOutputParameterIdentifier, rowsProcessed);
                        }

                        // Process the child nodes using the output data row.
                        processChildNodes(tx, outputDataRow);
                    }
                    rowsProcessed++;
                }
            }
        }
        catch (Exception e)
        {
            if (hasExceptionFlow()) processExceptionFlow(tx, e, "Exception while reading from input file: " + definition.getInputIdentifier(), "Error while reading the excel file", definition.getIdentifier(), inputDataRow);
            else if (parentStream.hasExceptionFlow()) parentStream.processExceptionFlow(e, "Exception while reading from input file: " + definition.getInputIdentifier(), "Error while reading the excel file", definition.getIdentifier(), inputDataRow);
            else throw new T8DStreamException(e, "Exception while reading from input file: " + definition.getInputIdentifier(), "Error while reading the excel file", definition.getIdentifier(), inputDataRow);
        }
        finally
        {
            //Report this nodes progress as 100% if it did not do anything
            if(rowCount < 1)
            {
                rowCount = rowsProcessed = rowsProcessed < 1 ? 1 : rowsProcessed;
            }
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx)
    {
    }
}
