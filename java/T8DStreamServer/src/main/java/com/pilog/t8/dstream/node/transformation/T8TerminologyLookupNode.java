package com.pilog.t8.dstream.node.transformation;

import com.pilog.t8.api.T8TerminologyApi;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.document.DocumentHandler;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.transformation.T8TerminologyLookupNodeDefinition;
import com.pilog.t8.definition.dstream.node.transformation.T8TerminologyLookupTypeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8TerminologyLookupNode extends T8DStreamDefaultNode
{
    private final T8TerminologyLookupNodeDefinition definition;
    private T8LRUCachedTerminologyProvider terminologyProvider;
    private T8DStreamDefinition streamDefinition;
    private T8TerminologyApi trmApi;
    private T8DataManager dataManager;
    private final Set<String> cacheLanguageIDSet;

    public T8TerminologyLookupNode(T8Context context, T8TerminologyLookupNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.cacheLanguageIDSet = getLanguageIDSet();
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.dataManager = serverContext.getDataManager();
            this.streamDefinition = (T8DStreamDefinition)definition.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            this.trmApi = tx.getApi(T8TerminologyApi.API_IDENTIFIER);
            this.terminologyProvider = new T8LRUCachedTerminologyProvider(trmApi.getTerminologyProvider(), definition.getTerminologyCacheSize());
            cacheTerminology();
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            Map<String, Object> outputDataRow;

            outputDataRow = new HashMap<String, Object>(inputDataRow);
            for (T8TerminologyLookupTypeDefinition lookupType : definition.getTerminologyLookupTypeDefinitions())
            {
                Map<String, String> conceptParameterMapping;
                String languageID;

                languageID = lookupType.getLanguageID();
                conceptParameterMapping = lookupType.getConceptTerminologyParameterMapping();
                if (conceptParameterMapping != null)
                {
                    for (String conceptInputParameter : conceptParameterMapping.keySet())
                    {
                        String terminologyOutputParameter;
                        String conceptID;

                        conceptID = (String)inputDataRow.get(conceptInputParameter);
                        terminologyOutputParameter = conceptParameterMapping.get(conceptInputParameter);
                        if (conceptID != null)
                        {
                            outputDataRow.put(terminologyOutputParameter, terminologyProvider.getTerm(languageID, conceptID));
                        }
                        else
                        {
                            outputDataRow.put(terminologyOutputParameter, null);
                        }
                    }
                }
            }

            // Process child nodes using output data.
            processChildNodes(tx, outputDataRow);
        }
        catch (Exception e)
        {
            throw new T8DStreamException(e, "Exception while executing terminology lookup node.", "Failed to look up terminology", definition.getIdentifier(), inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }

    private Set<String> getLanguageIDSet()
    {
        Set<String> languageIDSet;

        languageIDSet = new HashSet<String>();
        for (T8TerminologyLookupTypeDefinition lookupType : definition.getTerminologyLookupTypeDefinitions())
        {
            String languageID;

            languageID = lookupType.getLanguageID();
            if (languageID != null) languageIDSet.add(lookupType.getLanguageID());
        }

        return languageIDSet;
    }

    private void cacheTerminology() throws Exception
    {
        List<String> cacheDRIDList;

        cacheDRIDList = definition.getCacheDRIDList();
        if (cacheDRIDList != null)
        {
            for (String drID : cacheDRIDList)
            {
                DataRequirement dataRequirement;

                dataRequirement = retrieveDataRequirement(drID);
                if (dataRequirement != null)
                {
                    cacheDataRequirementTerminology(cacheLanguageIDSet, dataRequirement);
                }
            }
        }
    }

    public DataRequirement retrieveDataRequirement(String drID) throws Exception
    {
        T8DataSession ds;
        T8DataEntity drEntity;
        String drEntityIdentifier;

        drEntityIdentifier = definition.getDataRequirementDocumentDEID();

        ds = dataManager.getCurrentSession();
        drEntity = ds.instantTransaction().retrieve(drEntityIdentifier, HashMaps.newHashMap(drEntityIdentifier + "$ID", drID));
        if (drEntity != null)
        {
            return (DataRequirement)drEntity.getFieldValue(drEntityIdentifier + "$DOCUMENT");
        }
        else return null;
    }

    private void cacheDataRequirementTerminology(Set<String> languageIDSet, DataRequirement dataRequirement) throws Exception
    {
        HashSet<String> conceptIDs;

        conceptIDs = DocumentHandler.getAllConceptIds(dataRequirement);
        terminologyProvider.cacheTerminology(languageIDSet, conceptIDs);
    }
}
