package com.pilog.t8.dstream.node.transformation;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.transformation.T8EPICScriptNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8EPICScriptNode extends T8DStreamDefaultNode
{
    private final T8EPICScriptNodeDefinition definition;
    private T8DStreamDefinition streamDefinition;
    private T8EPICScriptNodeScript script;

    public T8EPICScriptNode(T8Context context, T8EPICScriptNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.streamDefinition = (T8DStreamDefinition)definition.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
            this.script = (T8EPICScriptNodeScript)definition.getNewScriptInstance(context);
            this.script.setParentStream(parentStream);
            this.script.prepareScript();
            this.script.setNodeIdentifier(definition.getIdentifier());
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            Map<String, Object> scriptOutput;
            Map<String, Object> outputDataRow;
            Map<String, Object> scriptInput;

            // Create a map that contains ALL of the stream parameters and then add the input parameter to it (this prevents the script from referencing parameters that are not part of the input for this node).
            scriptInput = streamDefinition.createStreamParameterMap();
            if (inputDataRow != null) scriptInput.putAll(inputDataRow);

            // Execute the script.
            script.setInputDataRow(inputDataRow);
            scriptOutput = script.executePreparedScript(scriptInput);

            // Compile the output data row.
            outputDataRow = new HashMap<String, Object>(inputDataRow);
            if (scriptOutput != null)
            {
                scriptOutput = T8IdentifierUtilities.stripNamespace(definition.getNamespace(), scriptOutput, false);
                outputDataRow.putAll(scriptOutput);
            }

            // Process child nodes using the output data row.
            processChildNodes(tx, outputDataRow);
        }
        catch (Exception e)
        {
            throw new T8DStreamException(e, "Exception while executing EPIC Script node.", "Failure occured while executing a script node", definition.getIdentifier(), inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
        script.finalizeScript();
    }
}
