package com.pilog.t8.dstream;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.T8SessionContext;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class ExpressionString implements Serializable
{
    // The ID used to verify serialized object version.
    static final long serialVersionUID = 1L;

    private String expressionString;

    private transient ExpressionEvaluator expressionEvaluator; // This variable is used to hold the compiled expression evaluator during stream execution.

    public ExpressionString(String expressionString)
    {
        this.expressionString = expressionString;
    }

    public String getExpressionString()
    {
        return expressionString;
    }

    public void setExpressionString(String expressionString)
    {
        this.expressionString = expressionString;
    }

    public Object evaluate(T8SessionContext sessionContext, Map<String, Object> inputParameters, Map<String, Object> inputDataRow) throws Exception
    {
        if (expressionString == null)
        {
            return null;
        }
        else
        {
            HashMap<String, Object> expressionParameters;

            // Create the expression parameters collection.
            expressionParameters = new HashMap();
            expressionParameters.putAll(inputDataRow);
            expressionParameters.put("SESSION_PARAMETERS", sessionContext.getSessionParameterMap());
            expressionParameters.put("INPUT_PARAMETERS", inputParameters);

            // Compile the expression String if it hasn't already been done.
            if (expressionEvaluator == null)
            {
                expressionEvaluator = new ExpressionEvaluator();
                expressionEvaluator.compileExpression(expressionString);
            }

            // Return the expression result.
            return expressionEvaluator.evaluateExpression(expressionParameters, null);
        }
    }
}
