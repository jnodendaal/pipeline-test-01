package com.pilog.t8.dstream.functionality;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8FunctionalityInstance;
import com.pilog.t8.ui.functionality.event.T8FunctionalityInstanceListener;
import com.pilog.t8.definition.functionality.T8DStreamFunctionalityDefinition;
import com.pilog.t8.definition.functionality.T8DStreamFunctionalityExecutionHandle;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.functionality.T8DefaultFunctionalityInstance;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamFunctionalityInstance extends T8DefaultFunctionalityInstance implements T8FunctionalityInstance
{
    private final T8DStreamFunctionalityDefinition definition;

    public T8DStreamFunctionalityInstance(T8Context context, T8DStreamFunctionalityDefinition definition)
    {
        super(context, definition, T8IdentifierUtilities.createNewGUID());
        this.definition = definition;
    }

    @Override
    public T8FunctionalityDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void initialize()
    {
    }

    @Override
    public boolean release()
    {
        return true;
    }

    @Override
    public T8FunctionalityAccessHandle access(Map<String, Object> parameters) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = new HashMap<String, Object>();
        timeAccessed = new T8Timestamp(System.currentTimeMillis());
        // TODO:  Use a mapping in the definition to compile a list of input parameters by extracting data from the target object.
        // TODO:  Start execution of the data stream so that it runs on the server.  Return a view dialog to show the progress.
        return new T8DStreamFunctionalityExecutionHandle(getFunctionalityId(), iid, definition.getDataStreamIdentifier(), inputParameters, getDisplayName(), getDescription(), getIcon());
    }

    @Override
    public boolean stop()
    {
        return true;
    }

    @Override
    public void addFunctionalityInstanceListener(T8FunctionalityInstanceListener listener)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeFunctionalityInstanceListener(T8FunctionalityInstanceListener listener)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8FunctionalityState getFunctionalityState()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
