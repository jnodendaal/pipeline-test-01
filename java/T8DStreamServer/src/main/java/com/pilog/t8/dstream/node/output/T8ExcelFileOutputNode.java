package com.pilog.t8.dstream.node.output;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.dstream.node.output.T8ExcelFileOutputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.file.T8ExcelOutputFileHandler;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelFileOutputNode extends T8DStreamDefaultNode
{
    private T8ExcelFileOutputNodeDefinition definition;
    private T8ExcelOutputFileHandler fileHandler;

    public T8ExcelFileOutputNode(T8Context context, T8ExcelFileOutputNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.fileHandler = (T8ExcelOutputFileHandler)parentStream.getOutputFileHandler(definition.getOutputIdentifier());
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            // Open the output file if it is not already open.
            if (!fileHandler.isOpen()) fileHandler.openFile();

            // Write the data to the output file.
            fileHandler.writeDataRow(definition.getSheetIdentifier(), T8IdentifierUtilities.mapParameters(inputDataRow, definition.getInputParameterMapping()));
            processChildNodes(tx, inputDataRow);
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while writing to output file: " + definition.getOutputIdentifier(), null, inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
