package com.pilog.t8.dstream;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementDataSourceDefinition;
import com.pilog.t8.definition.data.source.datarequirement.T8DataRequirementInstanceDataSourceDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRequirementInstanceMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRequirementMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DefaultValueRequirementMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DirectDataRequirementInstanceMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8PropertyRequirementMappingDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementMappingHandler
{
    private final String drInstanceEntityIdentifier;
    private final String drEntityIdentifier;
    private final String languageID;
    private final T8DataTransaction tx;
    private final TerminologyProvider terminologyProvider;
    private final Map<String, T8DataRequirementMappingDefinition> drMappingDefinitions;

    public T8DataRequirementMappingHandler(T8DataTransaction tx, TerminologyProvider terminologyProvider, String drInstanceEntityIdentifier, String drEntityIdentifier, String languageID)
    {
        this.tx = tx;
        this.terminologyProvider = terminologyProvider;
        this.drInstanceEntityIdentifier = drInstanceEntityIdentifier;
        this.drEntityIdentifier = drEntityIdentifier;
        this.languageID = languageID;
        this.drMappingDefinitions = new HashMap<String, T8DataRequirementMappingDefinition>();
    }

    public List<T8DataRequirementMappingDefinition> getDataRequirementMappingDefinitions()
    {
        return new ArrayList<T8DataRequirementMappingDefinition>(drMappingDefinitions.values());
    }

    public void clearDataRequirementMappingDefinitions()
    {
        drMappingDefinitions.clear();
    }

    public T8DataRequirementInstanceMappingDefinition createDataRequirementMappingsFromDRInstance(String drInstanceID) throws Exception
    {
        T8DirectDataRequirementInstanceMappingDefinition drInstanceMappingDefinition;

        drInstanceMappingDefinition = new T8DirectDataRequirementInstanceMappingDefinition(T8Definition.getLocalIdPrefix() + T8IdentifierUtilities.createNewGUID());
        drInstanceMappingDefinition.setDataRequirementInstanceIdentifier(drInstanceID);
        createDataRequirementMappings(drInstanceID, false);
        return drInstanceMappingDefinition;
    }

    public T8DefaultValueRequirementMappingDefinition createValueRequirementMappingsFromDRInstance(String drInstanceID, boolean excludeIndependant) throws Exception
    {
        T8DefaultValueRequirementMappingDefinition drInstanceMappingDefinition;

        drInstanceMappingDefinition = new T8DefaultValueRequirementMappingDefinition(T8Definition.getLocalIdPrefix() + T8IdentifierUtilities.createNewGUID());
        drInstanceMappingDefinition.setRequirementConceptIdentifier(drInstanceID);
        createDataRequirementMappings(drInstanceID, excludeIndependant);
        return drInstanceMappingDefinition;
    }

    public void createDataRequirementMappingsFromDR(String drID) throws Exception
    {
        T8DataEntity dataEntity;

        // Retrieve the sub-Data Requirement and continue to update its content mappings.
        dataEntity = tx.retrieve(drEntityIdentifier, HashMaps.newHashMap(drEntityIdentifier + T8DataRequirementDataSourceDefinition.KEY_FIELD_IDENTIFIER, drID));
        if (dataEntity != null)
        {
            DataRequirement dataRequirement;

            // Get the Data Requirement document from the entity and create a mapping definition for it.
            dataRequirement = (DataRequirement)dataEntity.getFieldValue(drEntityIdentifier + T8DataRequirementDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);

            // Only construct the new Data Requirement Mapping definition if one for this DR does not already exist.
            if (!drMappingDefinitions.containsKey(dataRequirement.getConceptID()))
            {
                T8DataRequirementMappingDefinition drMappingDefinition;

                // Create the new DR mapping definition.
                drMappingDefinition = new T8DataRequirementMappingDefinition(T8Definition.getLocalIdPrefix() + T8IdentifierUtilities.createNewGUID());
                drMappingDefinition.setDataRequirementIdentifier(dataRequirement.getConceptID());
                drMappingDefinitions.put(dataRequirement.getConceptID(), drMappingDefinition);

                // Add all of the property mappings contained by the DR mapping definition.
                for (PropertyRequirement propertyRequirement : dataRequirement.getPropertyRequirements())
                {
                    addPropertyMapping(drMappingDefinition, propertyRequirement);
                }
            }
        }
    }

    public void createDataRequirementMappings(String drInstanceIdentifier, boolean excludeIndependant) throws Exception
    {
        T8DataEntity dataEntity;

        // Retrieve the sub-Data Requirement and continue to update its content mappings.
        dataEntity = tx.retrieve(drInstanceEntityIdentifier, HashMaps.newHashMap(drInstanceEntityIdentifier + T8DataRequirementInstanceDataSourceDefinition.KEY_FIELD_IDENTIFIER, drInstanceIdentifier));
        if (dataEntity != null)
        {
            DataRequirementInstance dataRequirementInstance;
            DataRequirement dataRequirement;

            // Get the Data Requirement document from the entity and create a mapping definition for it.
            dataRequirementInstance = (DataRequirementInstance)dataEntity.getFieldValue(drInstanceEntityIdentifier + T8DataRequirementInstanceDataSourceDefinition.DOCUMENT_FIELD_IDENTIFIER);
            dataRequirement = dataRequirementInstance.getDataRequirement();

            // Only construct the new Data Requirement Mapping definition if one for this DR does not already exist.
            if (!drMappingDefinitions.containsKey(dataRequirement.getConceptID()) && !(excludeIndependant && dataRequirementInstance.isIndependent()))
            {
                T8DataRequirementMappingDefinition drMappingDefinition;

                // Create the new DR mapping definition.
                drMappingDefinition = new T8DataRequirementMappingDefinition(T8IdentifierUtilities.createLocalIdentifier(T8IdentifierUtilities.createNewGUID()));
                drMappingDefinition.setDataRequirementIdentifier(dataRequirement.getConceptID());
                drMappingDefinitions.put(dataRequirement.getConceptID(), drMappingDefinition);

                // Add all of the property mappings contained by the DR mapping definition.
                for (PropertyRequirement propertyRequirement : dataRequirement.getPropertyRequirements())
                {
                    addPropertyMapping(drMappingDefinition, propertyRequirement);
                }
            }
        }
    }

    public void addPropertyMapping(T8DataRequirementMappingDefinition drMapping, PropertyRequirement propertyRequirement) throws Exception
    {
        T8PropertyRequirementMappingDefinition propertyMapping;
        T8ConceptTerminology terminology;
        String conceptTerm;

        // Get the terminology for the property.
        terminology = terminologyProvider.getTerminology(languageID, propertyRequirement.getConceptID());
        conceptTerm = terminology != null ? terminology.getTerm() + "_" + T8IdentifierUtilities.createNewGUID() : propertyRequirement.getConceptID();

        // Create the property mapping.
        propertyMapping = new T8PropertyRequirementMappingDefinition(T8IdentifierUtilities.createLocalIdentifier(conceptTerm));
        propertyMapping.setPropertyIdentifier(propertyRequirement.getConceptID());
        drMapping.addPropertyMappingDefinition(propertyMapping);

        // Add the value mapping contained by the property.
        propertyMapping.setValueMappingDefinition(createValueMapping(propertyRequirement.getValueRequirement()));
    }

    public T8DefaultValueRequirementMappingDefinition createValueMapping(ValueRequirement valueRequirement) throws Exception
    {
        T8DefaultValueRequirementMappingDefinition valueMapping;
        String mappingIdentifier;

        // Create the identifier for the new value mapping definition.
        if (valueRequirement.getRequirementType().isConceptRequirementValue())
        {
            T8ConceptTerminology terminology;
            String conceptID;
            String conceptTerm;

            // Get the terminology for the property.
            conceptID = valueRequirement.getValue();
            if (conceptID != null)
            {
                terminology = terminologyProvider.getTerminology(languageID, conceptID);
                conceptTerm = terminology != null ? terminology.getTerm() + "_" + T8IdentifierUtilities.createNewGUID() : valueRequirement.getValue();
            }
            else
            {
                conceptTerm = valueRequirement.getRequirementType() + "_" + T8IdentifierUtilities.createNewGUID();
            }

            // Create the identifier string from the concept term.
            mappingIdentifier = T8IdentifierUtilities.createLocalIdentifier(conceptTerm);
        }
        else
        {
            // Create the identifier string from the value requirement ID.
            mappingIdentifier = valueRequirement.getRequirementType() + "_" + T8IdentifierUtilities.createNewGUID();
            mappingIdentifier = T8IdentifierUtilities.createLocalIdentifier(mappingIdentifier);
        }

        // Create the value mapping.
        valueMapping = new T8DefaultValueRequirementMappingDefinition(mappingIdentifier);
        valueMapping.setRequirementDataTypeIdentifier(valueRequirement.getRequirementType().getID());
        if (valueRequirement.getRequirementType().isConceptRequirementValue()) valueMapping.setRequirementConceptIdentifier(valueRequirement.getValue());

        // Add all sub-value mappings contained by the value mapping.
        for (ValueRequirement subValueRequirement : valueRequirement.getSubRequirements())
        {
            valueMapping.addValueMappingDefinition(createValueMapping(subValueRequirement));
        }

        // Return the newly create value mapping.
        return valueMapping;
    }
}
