package com.pilog.t8.dstream.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.commons.io.RandomAccessStream;
import com.pilog.t8.definition.dstream.output.T8DataFileOutputDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.files.data.DataFileHandler;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileOutputFileHandler implements T8DStreamFileHandler
{
    private final T8DataFileOutputDefinition outputDefinition;
    private final T8Context context;
    private DataFileHandler dataFileHandler;
    private final T8FileManager fileManager;
    private final T8DStreamFileContext fileContext;
    private String fileContextIdentifier;
    private String directoryPath;
    private String filePath;

    public T8DataFileOutputFileHandler(T8Context context, T8DataFileOutputDefinition outputDefinition, T8FileManager fileManager, T8DStreamFileContext fileContext) throws Exception
    {
        this.context = context;
        this.outputDefinition = outputDefinition;
        this.fileManager = fileManager;
        this.fileContext = fileContext;
        this.fileContextIdentifier = null;
        this.directoryPath = null;
        this.filePath = null;
    }

    @Override
    public boolean isOpen()
    {
        return ((dataFileHandler != null) && (dataFileHandler.isFileOpen()));
    }

    @Override
    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIdentifier, filePath);
    }

    @Override
    public File getCurrentFile()
    {
        return new File(filePath);
    }

    @Override
    public void openFile() throws Exception
    {
        RandomAccessStream stream;

        fileContext.addOuputFileName(outputDefinition, outputDefinition.getFileName());
        fileContextIdentifier = fileContext.getFileContextInstanceIdentifier();
        directoryPath = fileContext.getOutputDirectoryPath(outputDefinition);
        filePath = fileContext.getOutputFilePath(outputDefinition);

        fileManager.createDirectory(context, fileContextIdentifier, directoryPath);
        stream = fileManager.getFileRandomAccessStream(context, fileContextIdentifier, filePath);
        dataFileHandler = new DataFileHandler();
        dataFileHandler.setSecretKey(outputDefinition.getPassword().getBytes());
        dataFileHandler.openFile(stream, true);
    }

    public void writeDataRow(String sheetName, HashMap<String, Object> dataRow) throws Exception
    {
        // Create the destination table if it does not yet exist.
        if (!dataFileHandler.containsTable(sheetName))
        {
            dataFileHandler.addTable(sheetName, new ArrayList<String>(dataRow.keySet()));
        }

        // Write the row of data.
        dataFileHandler.writeDataRow(sheetName, dataRow);
    }

    @Override
    public void closeFile() throws Exception
    {
        if (dataFileHandler != null) dataFileHandler.closeFile();
        dataFileHandler = null;
    }

    @Override
    public void validateFile() throws Exception
    {
        //No validations are necesarry for output files
    }
}
