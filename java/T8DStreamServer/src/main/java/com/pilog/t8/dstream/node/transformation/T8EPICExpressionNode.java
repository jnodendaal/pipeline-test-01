package com.pilog.t8.dstream.node.transformation;

import com.pilog.t8.definition.dstream.T8DStreamDefinition;
import com.pilog.t8.definition.dstream.node.transformation.T8EPICExpressionNodeDefinition;
import com.pilog.t8.dstream.ExpressionString;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8EPICExpressionNode extends T8DStreamDefaultNode
{
    private final T8EPICExpressionNodeDefinition definition;
    private T8DStreamDefinition streamDefinition;
    private final ExpressionString conditionExpressionString;
    private final ExpressionString valueExpressionString;

    public T8EPICExpressionNode(T8Context context, T8EPICExpressionNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.conditionExpressionString = new ExpressionString(definition.getConditionExpression());
        this.valueExpressionString = new ExpressionString(definition.getValueExpression());
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        this.streamDefinition = (T8DStreamDefinition)definition.getAncestorDefinition(T8DStreamDefinition.TYPE_IDENTIFIER);
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            Map<String, Object> expressionParameters;
            Map<String, Object> outputParameters;
            Object conditionResult;

            // Add all input parameters to the output collection.
            outputParameters = new HashMap<String, Object>(inputDataRow);

            // Create a map that contains ALL of the stream parameters and then add the input parameter to it (this prevents the expression from referencing parameters that are not part of the input for this node).
            expressionParameters = streamDefinition.createStreamParameterMap();
            if (inputDataRow != null) expressionParameters.putAll(inputDataRow);

            // Now evaluate the expression.
            conditionResult = conditionExpressionString == null ? true : conditionExpressionString.evaluate(sessionContext, inputDataRow, expressionParameters);

            // Only if the expression evaluated to true, do we process value expression.
            if (conditionResult == Boolean.TRUE)
            {
                Object valueResult;

                valueResult = valueExpressionString == null ? true : valueExpressionString.evaluate(sessionContext, inputDataRow, expressionParameters);
                outputParameters.put(definition.getResultParameterIdentifier(), valueResult);
            }

            // Process child nodes.
            processChildNodes(tx, outputParameters);
        }
        catch (Throwable e)
        {
            throw new T8DStreamException(e, "Exception while executing EPIC expression node using condition expression: " + definition.getConditionExpression(), "Failure occured while trying to evaluate an expression", definition.getIdentifier(), inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
