package com.pilog.t8.dstream.node.output;

import com.pilog.t8.definition.dstream.node.T8DStreamOutputNodeDefinition;
import com.pilog.t8.dstream.ExpressionString;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.file.T8XMLOutputFileHandler;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import org.w3c.dom.Document;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8XMLFileOutputNode extends T8DStreamDefaultNode
{
    private String outputName;
    private String connectionName;
    private LinkedHashMap<String, ExpressionString> columnValueExpressions;
    public static final String TYPE_NAME = "XML Output File";
    private T8XMLOutputFileHandler fileHandler;

    public T8XMLFileOutputNode(T8Context context, T8DStreamOutputNodeDefinition definition)
    {
        super(context, definition);
        columnValueExpressions = new LinkedHashMap<String, ExpressionString>();
    }

    public String getOutputName()
    {
        return outputName;
    }

    public void setOutputName(String outputName)
    {
        this.outputName = outputName;
    }

    public LinkedHashMap<String, ExpressionString> getColumnValueExpressions()
    {
        return columnValueExpressions;
    }

    public void setColumnValueExpressions(LinkedHashMap<String, ExpressionString> columnValueExpressions)
    {
        this.columnValueExpressions = columnValueExpressions;
    }

    public void clearColumnValueExpressions()
    {
        columnValueExpressions.clear();
    }

    public String getConnectionName()
    {
        return connectionName;
    }

    public void setConnectionName(String connectionName)
    {
        this.connectionName = connectionName;
    }

    @Override
    public String getTypeName()
    {
        return TYPE_NAME;
    }

    @Override
    public double getProgress()
    {
        return -1;
    }

    @Override
    public int getNodeIterationCount()
    {
        return -1;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return -1;
    }

    @Override
    public String getProgressText()
    {
        return null;
    }

    @Override
    public void stopExecution()
    {
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.fileHandler = (T8XMLOutputFileHandler)parentStream.getOutputFileHandler(outputName);
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            ArrayList<Map<String, Object>> inputDataRows;
            Document generatedXML;

            // Open the output file if it is not already open.
            if (!fileHandler.isOpen()) fileHandler.openFile();

            // Write the data to the output file.
            inputDataRows = new ArrayList<Map<String, Object>>();
            inputDataRows.add(inputDataRow);

            generatedXML = null; //xGenOperation.executeOperation(sessionContext, connection, inputDataRows);
            if (generatedXML != null)
            {
                fileHandler.writeXML(generatedXML);
            }

            processChildNodes(tx, inputDataRow);
        }
        catch (Throwable e)
        {
            throw new T8DStreamException(e, "Exception while writing to output file: ", "Error occured while writing data to xml file", definition.getIdentifier(), inputDataRow);
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx) throws Exception
    {
    }
}
