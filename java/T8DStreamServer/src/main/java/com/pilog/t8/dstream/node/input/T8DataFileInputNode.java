package com.pilog.t8.dstream.node.input;

import com.pilog.t8.definition.dstream.node.input.T8DataFileInputNodeDefinition;
import com.pilog.t8.dstream.T8DStreamDefaultNode;
import com.pilog.t8.dstream.exception.T8DStreamException;
import com.pilog.t8.dstream.file.T8DataFileInputFileHandler;
import com.pilog.t8.dstream.statistics.T8DStreamStatistics;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataFileInputNode extends T8DStreamDefaultNode
{
    private T8DataFileInputNodeDefinition definition;
    private T8DataFileInputFileHandler fileHandler;
    private volatile int rowsProcessed;
    private volatile int rowCount;
    private volatile boolean stopFlag;

    public T8DataFileInputNode(T8Context context, T8DataFileInputNodeDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
        this.stopFlag = false;
        this.rowsProcessed = 0;
        this.rowCount = -1;
    }

    @Override
    public double getProgress()
    {
        if (rowCount == -1)
        {
            return -1;
        }
        else
        {
            return ((double)rowsProcessed/(double)rowCount*100.00);
        }
    }

    @Override
    public int getNodeIterationCount()
    {
        return rowCount;
    }

    @Override
    public int getNodeIterationsCompleted()
    {
        return rowsProcessed;
    }

    @Override
    public String getProgressText()
    {
        return definition.getProgressText();
    }

    @Override
    public void stopExecution()
    {
        stopFlag = true;
        stopDescendantNodes();
    }

    @Override
    public void initializeNodeSpecific(T8DataTransaction tx, T8DStreamStatistics streamStatistics, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            this.fileHandler = (T8DataFileInputFileHandler)parentStream.getInputFileHandler(definition.getInputIdentifier());
        }
        catch (Exception e)
        {
            handleException(tx, e, "Exception while initializing node: " + definition, null, inputDataRow);
        }
    }

    @Override
    public void processNodeSpecific(T8DataTransaction tx, Map<String, Object> inputDataRow) throws T8DStreamException
    {
        try
        {
            // Only process this node if the input file is available.
            if (fileHandler.isFileAvailable())
            {
                HashMap<String, Object> dataRow;

                // Count the number of input rows if required.
                if ((definition.countRows()) && (rowCount == -1))
                {
                    rowCount = fileHandler.getRowCount(definition.getTableIdentifier());
                }

                while (((dataRow = fileHandler.readDataRow(definition.getTableIdentifier())) != null) && (!stopFlag))
                {
                    HashMap<String, Object> outputDataRow;

                    outputDataRow = new HashMap<String, Object>(inputDataRow);
                    outputDataRow.putAll(dataRow);
                    processChildNodes(tx, outputDataRow);
                    rowsProcessed++;
                }
            }
        }
        catch (Exception e)
        {
            if (hasExceptionFlow()) processExceptionFlow(tx, e, "Exception while reading from input file: " + definition.getInputIdentifier(), "Error while reading the input file", definition.getIdentifier(), inputDataRow);
            else if (parentStream.hasExceptionFlow()) parentStream.processExceptionFlow(e, "Exception while reading from input file: " + definition.getInputIdentifier(), "Error while reading the input file", definition.getIdentifier(), inputDataRow);
            else throw new T8DStreamException(e, "Exception while reading from input file: " + definition.getInputIdentifier(), "Error while reading the input file", definition.getIdentifier(), inputDataRow);
        }
        finally
        {
            //Report this nodes progress as 100% if it did not do anything
            if(rowCount < 1)
            {
                rowCount = rowsProcessed = rowsProcessed < 1 ? 1 : rowsProcessed;
            }
        }
    }

    @Override
    public void finalizeNodeSpecific(T8DataTransaction tx)
    {
    }
}
