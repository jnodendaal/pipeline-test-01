package com.pilog.t8.dstream.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.definition.dstream.input.xml.T8XMLFileInputDefinition;
import com.pilog.t8.dstream.T8DStreamFileContext;
import com.pilog.t8.dstream.T8DStreamFileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.xml.domstream.DOMInputStream;
import java.io.File;
import java.io.InputStream;

/**
 * @author Bouwer du Preez
 */
public class T8XMLInputFileHandler implements T8DStreamFileHandler
{
    private final T8XMLFileInputDefinition inputDefinition;
    private final T8Context context;
    private final T8FileManager fileManager;
    private final T8DStreamFileContext fileContext;
    private DOMInputStream inputStream;
    private final String fileContextIdentifier;
    private final String filePath;

    public T8XMLInputFileHandler(T8Context context, T8XMLFileInputDefinition inputDefinition, T8FileManager fileManager, T8DStreamFileContext streamContext) throws Exception
    {
        this.context = context;
        this.inputDefinition = inputDefinition;
        this.fileManager = fileManager;
        this.fileContext = streamContext;
        this.fileContextIdentifier = fileContext.getFileContextInstanceIdentifier();
        this.filePath = fileContext.getInputFilePath(inputDefinition);
    }

    @Override
    public boolean isOpen()
    {
        return (inputStream != null);
    }

    @Override
    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIdentifier, filePath);
    }

    @Override
    public File getCurrentFile()
    {
        return new File(fileContext.getInputFilePath(inputDefinition));
    }

    @Override
    public void openFile() throws Exception
    {
        InputStream stream;

        stream = fileManager.getFileInputStream(context, fileContextIdentifier, filePath);
        inputStream = new DOMInputStream();
        inputStream.open(stream);
    }

    public int getElementCount(String elementName) throws Exception
    {
        int count;

        count = 0;
        while (inputStream.readElement(elementName) != null) count++;

        closeFile();
        openFile();

        return count;
    }

//    public HashMap<String, Object> readDataRow(String elementName, XtractNode xTractScheme) throws Exception
//    {
//        Element nextElement;
//
//        nextElement = inputStream.readElement(elementName);
//        if (nextElement != null)
//        {
//            HashMap<String, Object> dataRow;
//            if (context == null) context = new XtractContext();
//            dataRow =  (HashMap<String, Object>)xTractScheme.extractValue(context, nextElement);
//            return dataRow;
//        }
//        else return null;
//    }

    @Override
    public void closeFile() throws Exception
    {
        if (inputStream != null) inputStream.close();
        inputStream = null;
    }

    @Override
    public void validateFile() throws Exception
    {
        //TODO: Add file validations
    }
}
