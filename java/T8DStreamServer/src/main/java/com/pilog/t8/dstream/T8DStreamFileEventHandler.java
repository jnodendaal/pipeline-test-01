package com.pilog.t8.dstream;

import com.pilog.t8.file.T8FileEventHandler;
import java.io.File;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DStreamFileEventHandler implements T8FileEventHandler
{
    @Override
    public void handleFileEvent(Map<String, Object> contextParameters, Map<String, Object> inputParameters, File file, FileEventType eventType)
    {
        if (eventType == FileEventType.CREATED)
        {
            try
            {
                if (file.exists()) file.delete();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
