package com.pilog.t8.dstream.node.output.datarecord;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8DataRecordCreationNodeDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8RecordAttributeMappingDefinition;
import com.pilog.t8.definition.dstream.node.output.datarecord.T8RecordCodeMappingDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public abstract class T8AbstractDataRecordCreationMapping implements T8DataRecordCreationMapping
{
    private final T8DataRecordCreationMappingDefinition definition;
    protected final T8Context context;
    protected final T8ServerContext serverContext;
    protected final T8SessionContext sessionContext;
    protected final T8DataRecordCreationContext creationContext;
    protected final T8OrganizationOntologyFactory ontologyFactory;
    protected T8PerformanceStatistics stats;

    protected static final String PC_DATA_RECORD_OUTPUT = "DATA_RECORD_OUTPUT";

    protected static final String PE_RECORD_PROPERTY_CONSTRUCTION = "RECORD_PROPERTY_CONSTRUCTION";
    protected static final String PE_RECORD_VALUE_CONSTRUCTION = "RECORD_VALUE_CONSTRUCTION";
    protected static final String PE_RECORD_VALUE_CONCEPT_CONSTRUCTION = "RECORD_VALUE_CONCEPT_CONSTRUCTION";
    protected static final String PE_RECORD_CONCEPT_LOOKUP = "RECORD_CONCEPT_LOOKUP";
    protected static final String PE_RECORD_CONFORMANCE_CHECKING = "RECORD_CONFORMANCE_CHECKING";

    public T8AbstractDataRecordCreationMapping(T8DataRecordCreationContext parentNode, T8DataRecordCreationMappingDefinition definition)
    {
        this.context = parentNode.getContext();
        this.serverContext = parentNode.getServerContext();
        this.sessionContext = parentNode.getSessionContext();
        this.creationContext = parentNode;
        this.definition = definition;
        this.ontologyFactory = parentNode.getOntologyFactory();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default is false.
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        this.stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    protected int createMatchingHashCode(DataRecord parentRecord, List<String> parameterIdentifiers, Map<String, Object> dataParameters)
    {
        boolean contentFound;
        int hash;

        // Set a flag to indicate whether any content was found (using the parameter identifiers).
        contentFound = false;

        // Start the hashcode computation.
        hash = 5;
        hash = 53 * hash + java.util.Objects.hashCode(parentRecord != null ? parentRecord.getID() : null);
        for (String parameterIdentifier : parameterIdentifiers)
        {
            Object parameterValue;

            // Get the next parameter value.
            parameterValue = dataParameters.get(parameterIdentifier);

            // If the parameter value is not null, set the flag to indicate that some content was found.
            if (parameterValue != null) contentFound = true;

            // Add the parameter value to the hashcode.
            hash = 53 * hash + java.util.Objects.hashCode(parameterValue);
        }

        // If no content was found, we return 0 to indicate that fact.
        return contentFound ? hash : 0;
    }

    protected Map<String, Object> constructAttributes(List<T8RecordAttributeMappingDefinition> attributeMappings, Map<String, Object> dataParameters) throws Exception
    {
        List<T8RecordAttributeMappingDefinition> attributeMappingList;
        Map<String, Object> attributes;

        // Get the global tag mappings and add the new list to them.
        attributeMappingList = ((T8DataRecordCreationNodeDefinition)definition.getAncestorDefinition(T8DataRecordCreationNodeDefinition.TYPE_IDENTIFIER)).getAttributeMappingDefinitions();
        if (attributeMappings != null) attributeMappingList.addAll(attributeMappings);

        // Now construct the tag.
        attributes = new HashMap<>();
        for (T8RecordAttributeMappingDefinition attributeMapping : attributeMappingList)
        {
            if (attributeMapping.isAttributeApplicable(serverContext, sessionContext, dataParameters))
            {
                String attributeIdentifier;
                Object attributeValue;

                attributeIdentifier = attributeMapping.getAttributeIdentifier(serverContext, sessionContext, dataParameters);
                attributeValue = attributeMapping.getAttributeValue(serverContext, sessionContext, dataParameters);
                attributes.put(attributeIdentifier, attributeValue);
            }
        }

        return attributes;
    }

    protected void addOntologyCodes(List<T8RecordCodeMappingDefinition> codeMappings, Map<String, Object> dataParameters, DataRecord dataRecord) throws Exception
    {
        if (CollectionUtilities.hasContent(codeMappings))
        {
            T8OntologyConcept recordOntology;

            // Get the record ontology (create it if it does not exist).
            recordOntology = (T8OntologyConcept)dataRecord.getOntology();
            if (recordOntology == null)
            {
                recordOntology = new T8OntologyConcept(dataRecord.getID(), T8OntologyConceptType.DATA_RECORD, false, true);
                dataRecord.setOntology(recordOntology);
            }

            // Now construct the ontology code and add it to the record if it does not already exist.
            for (T8RecordCodeMappingDefinition codeMapping : codeMappings)
            {
                String codeTypeIdentifier;
                Object codeValue;
                Boolean found;

                found = false;
                codeTypeIdentifier = codeMapping.getCodeTypeIdentifier();
                codeValue = dataParameters.get(codeMapping.getSourceParameterIdentifier());
                if (codeValue != null)
                {
                    // Try to find an existing code with the same type (to avoid adding duplicates).
                    for (T8OntologyCode existingCode : recordOntology.getCodes())
                    {
                        if (Objects.equals(codeTypeIdentifier, existingCode.getCodeTypeID()))
                        {
                            found = true;
                            break;
                        }
                    }

                    // If a code of the same type does not yet exist on the record, add it now.
                    if (!found)
                    {
                        ontologyFactory.addCode(recordOntology, codeTypeIdentifier, codeValue.toString());
                    }
                }
            }
        }
    }

    protected String findConceptId(T8DataTransaction tx, String organizationID, String ocID, String termMatchString, String codeMatchString) throws Exception
    {
        List<String> conceptIDs;

        // Log stats.
        stats.logExecutionStart(PE_RECORD_CONCEPT_LOOKUP);

        // Try to fetch the controlled concept directly from the available values.
        if (!Strings.isNullOrEmpty(codeMatchString))
        {
            conceptIDs = creationContext.getConceptIDsByCode(tx, organizationID, ocID, codeMatchString);
            if (conceptIDs.size() > 0) // If we find a match, create a record value and return it.
            {
                stats.logExecutionEnd(PE_RECORD_CONCEPT_LOOKUP);
                return conceptIDs.get(0);
            }
        }

        // Try to match the input value with the term of the concepts defined by the value requirement.
        if (!Strings.isNullOrEmpty(termMatchString))
        {
            // Try to match the input value with the term of the concepts defined by the value requirement.
            conceptIDs = creationContext.getConceptIDsByTerm(tx, organizationID, ocID, termMatchString);
            if (conceptIDs.size() > 0) // If we find a match, create a record value and return it.
            {
                stats.logExecutionEnd(PE_RECORD_CONCEPT_LOOKUP);
                return conceptIDs.get(0);
            }
        }

        // No matching requirement found, so return null.
        stats.logExecutionEnd(PE_RECORD_CONCEPT_LOOKUP);
        return null;
    }
}
