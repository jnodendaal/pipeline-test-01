package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8UtilitiesServerVersion
{
    public final static String VERSION = "32";
}
