package com.pilog.t8.utilities.data.resultset.extractor;

import com.pilog.t8.utilities.codecs.HexCodec;

/**
 * @author Bouwer du Preez
 */
public class GUIDBytesToStringConverter implements DataConverter
{
    @Override
    public Object convertDataValue(String tableName, String columnName, Object dataValue, int sqlType)
    {
        if (dataValue instanceof byte[])
        {
            byte[] byteArray;

            byteArray = (byte[])dataValue;
            if (byteArray.length == 16)
            {
                return HexCodec.convertBytesToHexString(byteArray);
            }
            else return dataValue;
        }
        else return dataValue;
    }
}
