package com.pilog.t8.utilities.data.resultset.extractor;

/**
 * @author Bouwer du Preez
 */
public class BlobToBytesConverter implements DataConverter
{
    @Override
    public Object convertDataValue(String tableName, String columnName, Object dataValue, int sqlType) throws Exception
    {
        if (dataValue instanceof java.sql.Blob)
        {
            java.sql.Blob blob;

            blob = (java.sql.Blob)dataValue;
            return blob.getBytes(1, (int)blob.length());
        }
        else return dataValue;
    }
}
