package com.pilog.t8.utilities.data.resultset.extractor;

/**
 * @author Bouwer du Preez
 */
public class ClobToStringConverter implements DataConverter
{
    @Override
    public Object convertDataValue(String tableName, String columnName, Object dataValue, int sqlType) throws Exception
    {
        if (dataValue instanceof java.sql.Clob)
        {
            java.sql.Clob clob;

            clob = (java.sql.Clob)dataValue;
            return clob.getSubString(1, (int) clob.length());
        }
        else return dataValue;
    }
}
