package com.pilog.t8.utilities.data.resultset.extractor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class BigDecimalToLongConverter implements DataConverter
{
    private final List<String> columns;

    public BigDecimalToLongConverter(String... columnNames)
    {
        this();
        if (columnNames != null)
        {
            this.columns.addAll(Arrays.asList(columnNames));
        }
    }

    public BigDecimalToLongConverter()
    {
        this.columns = new ArrayList<>();
    }

    private boolean isColumnApplicable(String tableName, String columnName)
    {
        if (columns.isEmpty())
        {
            return true;
        }
        else return columns.contains(columnName);
    }

    @Override
    public Object convertDataValue(String tableName, String columnName, Object dataValue, int sqlType)
    {
        if ((dataValue != null) && (dataValue instanceof BigDecimal) && isColumnApplicable(tableName, columnName))
        {
            return ((Number)dataValue).longValue();
        }
        else return dataValue;
    }
}
