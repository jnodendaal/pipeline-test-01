package com.pilog.t8.utilities.data.resultset.extractor;

/**
 * This converter checks for GUID's in the format:
 *  HHHHHHHH-HHHH-HHHH-HHHH-HHHHHHHHHHHH
 *
 * This type of GUID is returned by MSSQL from a UNIQUEIDENTIFIER type column.
 *
 * @author Bouwer du Preez
 */
public class GUIDStringConverter implements DataConverter
{
    @Override
    public Object convertDataValue(String tableName, String columnName, Object dataValue, int sqlType)
    {
        if ((sqlType == java.sql.Types.CHAR) && (dataValue instanceof String)) // Convert GUID char arrays to String representations.
        {
            String guidString;

            guidString = (String)dataValue;
            if ((guidString != null) && (guidString.length() == 36)) // Length of 36: 16 byte GUID * 2 = 32 hex characters + 4 dash characters (MSSQL).
            {
                dataValue = guidString.replaceAll("-", "");
                return dataValue;
            }
            else return dataValue;
        }
        else return dataValue;
    }
}
