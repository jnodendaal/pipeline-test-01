package com.pilog.t8.compiler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class CompilationReport implements Serializable
{
    private final List<CompilationError> errors;
    private final List<String> options;

    public CompilationReport()
    {
        errors = new ArrayList<>();
        options = new ArrayList<>();
    }

    public void addError(String sourceObject, long lineNumber, long columnNumber, String message)
    {
        errors.add(new CompilationError(sourceObject, lineNumber, columnNumber, message));
    }

    public List<CompilationError> getErrors()
    {
        return new ArrayList<>(errors);
    }

    public void setOptions(List<String> options)
    {
        this.options.clear();
        if (options != null)
        {
            this.options.addAll(options);
        }
    }

    public List<String> getOptions()
    {
        return new ArrayList<>(options);
    }

    public static class CompilationError
    {
        private final String sourceObject;
        private final long columnNumber;
        private final long lineNumber;
        private final String message;

        public CompilationError(String sourceObject, long lineNumber, long columnNumber, String message)
        {
            this.sourceObject = sourceObject;
            this.columnNumber = columnNumber;
            this.lineNumber = lineNumber;
            this.message = message;
        }

        public String getSourceObject()
        {
            return sourceObject;
        }

        public long getColumnNumber()
        {
            return columnNumber;
        }

        public long getLineNumber()
        {
            return lineNumber;
        }

        public String getMessage()
        {
            return message;
        }
    }
}
