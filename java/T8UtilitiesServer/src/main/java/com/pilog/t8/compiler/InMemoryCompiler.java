package com.pilog.t8.compiler;

import com.google.common.collect.ImmutableMap;

import javax.tools.JavaCompiler.CompilationTask;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import java.util.ArrayList;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.ToolProvider;

/**
 * The dynamic compiler uses the JavaCompiler with custom implementations
 * of a JavaFileManager and JavaFileObject to compile a Java Source from a
 * String to Bytecode.
 *
 * @see InMemoryClassManager
 * @see JavaSourceFromString
 * @see JavaMemoryObject
 */
public class InMemoryCompiler
{
    /**
     * Compiles a single class.
     *
     * <pre>
     * Example code:
     * <code>
     * try {
     *  DynamicCompiler compiler = new DynamicCompiler();
     * 	CompilationPackage pkg = compiler.singleCompile(
     * 		"public class HelloWorld {\n"+
     * 		"	public static void main(String[] args) {\n"+
     * 		"		System.out.println(\"hello,world!\");\n"+
     * 		"	}\n"+
     * 		"}\n");
     * }
     * catch(CompilerException e) {
     * 	// face it or throw it away
     * }
     * </code>
     * </pre>
     *
     * @param className Ruleset name
     * @param code Rule class code
     * @return Compilation package
     * @throws CompilerException Thrown when a compiler exception occurs
     */
    public CompilationPackage singleCompile(String className, String code, String classpath) throws CompilerException
    {
        return compile(ImmutableMap.<String, String>builder().put(className, code).build(), classpath);
    }

    /**
     * Given a map of class FQDN and its source code, this method compiles the code and returns a Compilation Package that encapsulates it.
     *
     * @param classesToCompile Map of className/classSource to compile
     * @return Compilation package
     * @throws CompilerException Thrown when a compiler exception occurs
     */
    public CompilationPackage compile(Map<String, String> classesToCompile, String classpath) throws CompilerException
    {
        DiagnosticCollector<JavaFileObject> collector;
        List<JavaSourceFromString> strFiles;
        InMemoryClassManager manager;
        JavaCompiler compiler;
        CompilationTask task;
        List<String> options;

        // Get the compiler and diagnostics to use for compilation.
        compiler = getSystemJavaCompiler();
        collector = getDiagnosticCollector();
        manager = getClassManager(compiler);
        options = Arrays.asList("-classpath", classpath);

        // Create java source objects from the input class source strings.
        strFiles = new ArrayList<>();
        for (String className : classesToCompile.keySet())
        {
            String classCode = classesToCompile.get(className);
            strFiles.add(new JavaSourceFromString(className, classCode));
        }

        // Compile the source code.
        task = compiler.getTask(null, manager, collector, options, null, strFiles);

        // Check the result of the compilation and if it is a failure, throw a new compiler exception.
        if (task.call())
        {
            List<CompilationUnit> compilationUnits = manager.getAllClasses();
            return new CompilationPackage(compilationUnits);
        }
        else
        {
            CompilationReport report;

            // Compilation failure.
            report = buildCompilationReport(collector, options);
            throw new CompilerException(report, "Compilation failure.");
        }
    }

    DiagnosticCollector<JavaFileObject> getDiagnosticCollector()
    {
        return new DiagnosticCollector<>();
    }

    InMemoryClassManager getClassManager(JavaCompiler compiler)
    {
        return new InMemoryClassManager(compiler.getStandardFileManager(null, null, null));
    }

    JavaCompiler getSystemJavaCompiler()
    {
        return ToolProvider.getSystemJavaCompiler();
    }

    private CompilationReport buildCompilationReport(DiagnosticCollector<JavaFileObject> collector, List<String> options)
    {
        CompilationReport report;

        // Create the compilation report.
        report = new CompilationReport();
        report.setOptions(options);
        for (Diagnostic<? extends JavaFileObject> diagnostic : collector.getDiagnostics())
        {
            JavaFileObject source;

            source = diagnostic.getSource();
            report.addError(source != null ? source.getName() : null, diagnostic.getLineNumber(), diagnostic.getColumnNumber(), diagnostic.getMessage(null));
        }

        return report;
    }

    public static class CompilerException extends Exception
    {
        private final CompilationReport report;

        /**
         * Creates a new compiler exception.
         *
         * @param report The report containing the details of the compilation failure.
         * @param message Message describing the general nature of the exception.
         */
        public CompilerException(CompilationReport report, String message)
        {
            super(message);
            this.report = report;
        }

        public CompilationReport getReport()
        {
            return report;
        }
    }
}
