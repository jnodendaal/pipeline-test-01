package com.pilog.t8.utilities.data.resultset.extractor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class ResultSetDataExtractor
{
    private ArrayList<DataConverter> dataConverters;

    public ResultSetDataExtractor()
    {
        this.dataConverters = new ArrayList<DataConverter>();
    }

    public ResultSetDataExtractor(DataConverter... converters)
    {
        this();
        for (DataConverter dataConverter : converters)
        {
            addDataConverter(dataConverter);
        }
    }

    public final void addDataConverter(DataConverter dataConverter)
    {
        dataConverters.add(dataConverter);
    }

    public void clearDataConverters()
    {
        dataConverters.clear();
    }

    public final List<Map<String, Object>> getResultSetDataRows(ResultSet resultSet) throws Exception
    {
        return getResultSetDataRows(resultSet, 10000000);
    }

    public final List<Map<String, Object>> getResultSetDataRows(ResultSet resultSet, int rowCount) throws Exception
    {
        List<Map<String, Object>> returnList;
        LinkedHashMap<String, Object> rowMap;

        // Pack the retrieved data rows into the ArrayList<HashMap>.
        returnList = new ArrayList<Map<String, Object>>();
        while (((rowMap = getNextResultSetDataRow(resultSet)) != null) && (returnList.size() < rowCount))
        {
            returnList.add(rowMap);
        }

        return returnList;
    }

    public final LinkedHashMap<String, Object> getNextResultSetDataRow(ResultSet resultSet) throws Exception
    {
        if (resultSet.next())
        {
            LinkedHashMap<String, Object> rowMap;
            ResultSetMetaData rSetMeta;

            // Get the Result Set's meta data.
            rSetMeta = resultSet.getMetaData();

            // Compile the data row.
            rowMap = new LinkedHashMap<String, Object>();
            for (int columnIndex = 0; columnIndex < rSetMeta.getColumnCount(); columnIndex++)
            {
                String tableName;
                String columnName;
                Object dataValue;
                int columnType;

                // Get the column SQL type ResultSet.
                columnType = rSetMeta.getColumnType(columnIndex + 1);
                columnName = rSetMeta.getColumnName(columnIndex + 1);
                tableName = rSetMeta.getTableName(columnIndex + 1);

                // This check fixes a bug - Oracle only has a Date type, and that type is automatically mapped to the JDBC Date type, causing any time data to be lost.
                if (columnType == java.sql.Types.DATE)
                {
                    dataValue = resultSet.getTimestamp(columnIndex + 1);
                }
                else // Get the data value from the ResultSet.
                {
                    dataValue = resultSet.getObject(columnIndex + 1);
                }

                // Apply all data converters.
                for (DataConverter dataConverter : dataConverters)
                {
                    dataValue = dataConverter.convertDataValue(tableName, columnName, dataValue, columnType);
                }

                // Pack the data value into the data row.
                rowMap.put(rSetMeta.getColumnName(columnIndex + 1).toUpperCase(), dataValue);
            }

            return rowMap;
        }
        else return null; // There was now next row to retrieve.
    }
}
