package com.pilog.t8.utilities.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 * @author Bouwer du Preez
 */
public class ExcelOutputStream
{
    private OutputStream outputStream;
    private Workbook workBook;
    private HashMap<String, Integer> rowCounts;
    private HashMap<String, List<String>> sheetColumns;
    private HashMap<String, Map<Integer, XSSFCellStyle>> cellStyles; // Keeps track of the columns for which types have been determined.

    public static final int SHEET_ROW_LIMIT = 1000001; // Row limit allows 1 million records plus one line containing the column headers.

    public ExcelOutputStream()
    {
    }

    public void openFile(File outputFile) throws Exception
    {
        openFile(new FileOutputStream(outputFile));
    }

    public void openFile(OutputStream outputStream) throws Exception
    {
        this.outputStream = outputStream;
        this.workBook = new SXSSFWorkbook();
        this.rowCounts = new HashMap<>();
        this.sheetColumns = new HashMap<>();
        this.cellStyles = new HashMap<>();
    }

    public void flush(String sheetName) throws Exception
    {
        SXSSFSheet sheet;

        sheet = (SXSSFSheet)workBook.getSheet(sheetName);
        sheet.flushRows();
    }

    public boolean containsSheet(String sheetName)
    {
        return sheetColumns.containsKey(sheetName);
    }

    public Sheet createSheet(String sheetName, List<String> columnNames) throws Exception
    {
        Sheet newSheet;

        newSheet = workBook.createSheet(sheetName);
        rowCounts.put(sheetName, 0);
        sheetColumns.put(sheetName, columnNames);

        return newSheet;
    }

    public void writeSheet(String sheetName) throws Exception
    {
        HashMap<String, Object> columnNameDataRow;

        // Write the first row of data to the sheet (the column names).
        columnNameDataRow = new HashMap<>();
        for (String columnName : sheetColumns.get(sheetName))
        {
            columnNameDataRow.put(columnName, columnName);
        }

        writeRow(sheetName, columnNameDataRow);
    }

    public Sheet writeSheet(String sheetName, List<String> columnNames) throws Exception
    {
        HashMap<String, Object> columnNameDataRow;
        Sheet newSheet;

        newSheet = workBook.createSheet(sheetName);
        rowCounts.put(sheetName, 0);
        sheetColumns.put(sheetName, columnNames);

        // Write the first row of data to the sheet (the column names).
        columnNameDataRow = new HashMap<>();
        for (String columnName : columnNames)
        {
            columnNameDataRow.put(columnName, columnName);
        }

        writeRow(sheetName, columnNameDataRow);
        return newSheet;
    }

    public int writeRow(String sheetName, Map<String, Object> rowData) throws Exception
    {
        List<String> columns;
        int rowNumber;
        Sheet sheet;
        Row row;

        sheet = workBook.getSheet(sheetName);
        if (sheet == null)
        {
            writeSheet(sheetName, new ArrayList<>(rowData.keySet()));
            sheet = workBook.getSheet(sheetName);
        }

        columns = sheetColumns.get(sheetName);
        rowNumber = rowCounts.get(sheetName);
        row = sheet.createRow(rowNumber);

        for (int cellIndex = 0; cellIndex < columns.size(); cellIndex++)
        {
            String stringValue;
            Object value;
            Cell cell;

            value = rowData.get(columns.get(cellIndex));
            stringValue = value == null ? "" : value.toString();

            // Replace some characters known to cause problems in Excel (XML) output.
            stringValue = stringValue.replace('\u00A0', ' ');
            stringValue = stringValue.replace("\u0000", "");

            cell = row.createCell(cellIndex);
            cell.setCellValue(stringValue);
            cell.setCellStyle(getCellStyle(sheetName, cellIndex));
        }

        // Increment the row number and add it back into the colleciton.
        rowNumber++;
        rowCounts.put(sheetName, rowNumber);
        return rowNumber; // Return the number of records currently in the sheet.
    }

    public int writeRow(String sheetName, List<Object> rowData) throws Exception
    {
        int rowNumber;
        Sheet sheet;
        Row row;

        sheet = workBook.getSheet(sheetName);
        if (sheet == null)
        {
            writeSheet(sheetName, (List)rowData);
            sheet = workBook.getSheet(sheetName);
        }

        rowNumber = rowCounts.get(sheetName);
        row = sheet.createRow(rowNumber);

        for (int cellIndex = 0; cellIndex < rowData.size(); cellIndex++)
        {
            Object value;
            Cell cell;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(getCellStyle(sheetName, cellIndex));

            value = rowData.get(cellIndex);
            if (value == null)
            {
                cell.setCellValue((String)null);
            }
            else if (value instanceof Number)
            {
                Number numberValue;

                numberValue = (Number)value;
                cell.setCellValue(numberValue.doubleValue());
            }
            else if (value instanceof Date)
            {
                Date dateValue;

                dateValue = (Date)value;
                cell.setCellValue(dateValue);
            }
            else if (value instanceof Boolean)
            {
                Boolean booleanValue;

                booleanValue = (Boolean)value;
                cell.setCellValue(booleanValue);
            }
            else
            {
                String stringValue;

                // Replace some characters known to cause problems in Excel (XML) output.
                stringValue = value.toString();
                stringValue = stringValue.replace('\u00A0', ' ');
                stringValue = stringValue.replace("\u0000", "");
                cell.setCellValue(stringValue);
            }
        }

        // Increment the row number and add it back into the colleciton.
        rowNumber++;
        rowCounts.put(sheetName, rowNumber);
        return rowNumber; // Return the number of records currently in the sheet.
    }

    public void setCellFormatPatterns(String sheetName, List<String> formatPatterns)
    {
        for (int columnIndex = 0; columnIndex < formatPatterns.size(); columnIndex++)
        {
            setCellFormatPattern(sheetName, columnIndex, formatPatterns.get(columnIndex));
        }
    }

    public void setCellFormatPattern(String sheetName, int columnIndex, String formatPattern)
    {
        CellStyle cellStyle;
        DataFormat format;

        format = workBook.createDataFormat();
        cellStyle = createCellStyle(sheetName, columnIndex);
        cellStyle.setDataFormat(format.getFormat(formatPattern));
    }

    private XSSFCellStyle getCellStyle(String sheetName, int columnIndex)
    {
        Map<Integer, XSSFCellStyle> sheetCellStyles;

        sheetCellStyles = cellStyles.get(sheetName);
        return sheetCellStyles != null ? sheetCellStyles.get(columnIndex) : null;
    }

    private XSSFCellStyle createCellStyle(String sheetName, int columnIndex)
    {
        Map<Integer, XSSFCellStyle> sheetCellStyles;
        XSSFCellStyle cellStyle;

        sheetCellStyles = cellStyles.get(sheetName);
        if (sheetCellStyles == null)
        {
            sheetCellStyles = new HashMap<>();
            cellStyles.put(sheetName, sheetCellStyles);
        }

        cellStyle = sheetCellStyles.get(columnIndex);
        if (cellStyle == null)
        {
            cellStyle = (XSSFCellStyle)workBook.createCellStyle();
            sheetCellStyles.put(columnIndex, cellStyle);
            return cellStyle;
        }
        else
        {
            // Create a clone of the style, keeping all existing settings but
            // allowing new settings to be added without changing cells that already use the style.
            cellStyle = (XSSFCellStyle)cellStyle.clone();
            sheetCellStyles.put(columnIndex, cellStyle);
            return cellStyle;
        }
    }

    private void clearCellStyle(String sheetName, int columnIndex)
    {
        Map<Integer, XSSFCellStyle> sheetCellStyles;

        sheetCellStyles = cellStyles.get(sheetName);
        if (sheetCellStyles != null)
        {
            sheetCellStyles.remove(columnIndex);
        }
    }

    public void clearColumnStyle(String sheetName, String columnName)
    {
        List<String> sheetColumnNames;

        sheetColumnNames = sheetColumns.get(sheetName);
        clearCellStyle(sheetName, sheetColumnNames.indexOf(columnName));
    }

    public void setColumnWidth(String sheetName, String columnName, int width)
    {
        List<String> sheetColumnNames;
        Sheet sheet;

        sheet = workBook.getSheet(sheetName);
        sheetColumnNames = sheetColumns.get(sheetName);
        sheet.setColumnWidth(sheetColumnNames.indexOf(columnName), width);
    }

    public void setColumnWidth(String sheetName, int width)
    {
        List<String> sheetColumnNames;
        Sheet sheet;

        sheet = workBook.getSheet(sheetName);
        sheetColumnNames = sheetColumns.get(sheetName);
        for (int columnIndex = 0; columnIndex < sheetColumnNames.size(); columnIndex++)
        {
            sheet.setColumnWidth(columnIndex, width);
        }
    }

    public void setColumnColor(String sheetName, String columnName, int red, int green, int blue)
    {
        List<String> sheetColumnNames;
        XSSFCellStyle cellStyle;

        sheetColumnNames = sheetColumns.get(sheetName);
        cellStyle = createCellStyle(sheetName, sheetColumnNames.indexOf(columnName));
        cellStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(red, green, blue)));
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    }

    public void autoSizeColumn(String sheetName, String columnName)
    {
        List<String> sheetColumnNames;
        Sheet sheet;

        sheet = workBook.getSheet(sheetName);
        sheetColumnNames = sheetColumns.get(sheetName);
        sheet.autoSizeColumn(sheetColumnNames.indexOf(columnName));
    }

    public void autoSizeColumns(String sheetName)
    {
        List<String> sheetColumnNames;
        SXSSFSheet sheet;

        sheet = (SXSSFSheet)workBook.getSheet(sheetName);
        sheet.trackAllColumnsForAutoSizing();
        sheetColumnNames = sheetColumns.get(sheetName);
        for (int columnIndex = 0; columnIndex < sheetColumnNames.size(); columnIndex++)
        {
            sheet.autoSizeColumn(columnIndex);
        }
    }

    public int getRowCount(String sheetName)
    {
        return rowCounts.get(sheetName);
    }

    public void closeFile() throws Exception
    {
        if (workBook != null) workBook.write(outputStream);
        if (outputStream != null) outputStream.close();
    }

    public void setDefaultColumnFormatPattern(String formatPattern, String sheetName, String columnName)
    {
        List<String> sheetColumnNames;
        Sheet sheet;
        DataFormat format;
        CellStyle style;

        format = workBook.createDataFormat();
        style = workBook.createCellStyle();
        sheet  = workBook.getSheet(sheetName);
        style.setDataFormat(format.getFormat(formatPattern));

        sheetColumnNames = sheetColumns.get(sheetName);
        sheet.setDefaultColumnStyle(sheetColumnNames.indexOf(columnName), style);
    }
}
