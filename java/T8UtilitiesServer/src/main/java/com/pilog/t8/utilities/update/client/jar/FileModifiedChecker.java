/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.utilities.update.client.jar;

import java.io.*;
import java.security.*;
import java.util.*;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class FileModifiedChecker
{
    /**
     * Create a new instance of ClientJarModifiedChecker to check for modified jars and update the last modified date of those files if the MD5 hash for a jar differs
     * from a saved MD5 of that jar.
     * @param persistanceFile The file that will be used to save the hash and last modified date information of files.
     */
    public FileModifiedChecker(File persistanceFile)
    {
        this.persistanceFile = persistanceFile;
    }
    private File persistanceFile;
    private List<FileInformation> savedJarsInformation;
    private List<FileInformation> currentJars;

    /***
     * Checks the files in the the location provided against those saved in the persistence file.
     * New files get added to the persistence file with the current system time.
     * Files stored in the persistence file gets compared to the files in the specified directory, if the file MD5 hash of the two files match, the file in the directory
     * gets updated with the last modified date information saved in the persistence file.
     * @param clientSideFileLocation
     * @throws ServletException
     */
    public void checkFiles(File clientSideFileLocation) throws Exception
    {
        loadSavedJarInfo();
        try
        {
            loadCurrentJarInformation(clientSideFileLocation);
        }
        catch (NoSuchAlgorithmException ex)
        {
            System.out.println("Failed to get algorithm for MD5 Hash");
            throw new Exception("Failed to load current jar information.", ex);
        }
        saveJarInfo();
        System.out.println("FileModifiedChecker: Checked " + currentJars.size() + " jar files.");
    }

    private void loadSavedJarInfo()
    {
        if (persistanceFile.exists())
        {
            ObjectInputStream objectInputStream = null;
            try
            {
                objectInputStream = new ObjectInputStream(new FileInputStream(persistanceFile));
                savedJarsInformation = (List<FileInformation>) objectInputStream.readObject();
            }
            catch (Exception e)
            {
                System.out.println("Failed to load saved client jar hash information from " + persistanceFile);
                System.out.println(e.getMessage());
            }
            finally
            {
                if (objectInputStream != null)
                {
                    try
                    {
                        objectInputStream.close();
                    }
                    catch (IOException ex)
                    {
                        System.out.println("Failed to close output stream.");
                    }
                }
                if (savedJarsInformation == null)
                {
                    savedJarsInformation = new ArrayList<FileInformation>(0);
                }
            }
        }
    }

    private void loadCurrentJarInformation(File clientSideJars) throws
            NoSuchAlgorithmException
    {
        currentJars = new ArrayList<FileInformation>();
        for (File file : clientSideJars.listFiles())
        {
            try
            {
                FileInformation jarInformation = new FileInformation(file);
                currentJars.add(jarInformation);
                if (savedJarsInformation.contains(jarInformation))
                {
                    FileInformation savedJarInfo = savedJarsInformation.get(savedJarsInformation.indexOf(jarInformation));
                    if (savedJarInfo.getHashString().equals(jarInformation.getHashString()))
                    {
                        file.setLastModified(savedJarInfo.getLastModified());
                    }
                    else
                    {
                        System.out.println("Jar " + jarInformation.getJarName() + " has been updated, saving new timestamp information.");
                        savedJarsInformation.remove(savedJarInfo);
                        savedJarsInformation.add(jarInformation);
                    }
                }
                else
                {
                    System.out.println("Jar " + jarInformation.getJarName() + " is new, saving MD5 Hash("+jarInformation.getHashString()+") and timestamp.");
                    System.out.println("Loaded jar: " + jarInformation);
                    savedJarsInformation.add(jarInformation);
                }
            }
            catch (Exception e)
            {
                System.out.println("Digest evaluation failed for file " + file);
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    private void saveJarInfo()
    {
        ObjectOutputStream objectOutputStream = null;
        try
        {
            objectOutputStream = new ObjectOutputStream(new FileOutputStream(persistanceFile));
            objectOutputStream.writeObject(savedJarsInformation);
        }
        catch (Exception e)
        {
            System.out.println("Failed to save updated jar information");
        }
        finally
        {
            if (objectOutputStream != null)
            {
                try
                {
                    objectOutputStream.close();
                }
                catch (IOException ex)
                {
                    System.out.println("Failed to close updated jar information stream.");
                }
            }
        }
    }
}
