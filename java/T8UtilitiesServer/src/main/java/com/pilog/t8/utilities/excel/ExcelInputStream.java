package com.pilog.t8.utilities.excel;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.NoSuchElementException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;

/**
 * @author Bouwer du Preez
 */
public class ExcelInputStream
{
    private OPCPackage pkg;
    private XSSFReader xssfReader;
    private ReadOnlySharedStringsTable sst;
    private StylesTable stylesTable;
    private DataFormatter formatter;
    private final HashMap<String, LinkedHashMap<String, String>> columnNames;
    private final HashMap<String, InputStream> sheetInputStreams;
    private final HashMap<String, XMLEventReader> sheetReaders;
    private final HashMap<String, Integer> sheetRowCounts;

    public ExcelInputStream()
    {
        this.sheetInputStreams = new HashMap<String, InputStream>();
        this.sheetReaders = new HashMap<String, XMLEventReader>();
        this.columnNames = new HashMap<String, LinkedHashMap<String, String>>();
        this.sheetRowCounts = new HashMap<String, Integer>();
    }

    public void openFile(File inputFile) throws Exception
    {
        pkg = OPCPackage.open(inputFile.getAbsoluteFile(), PackageAccess.READ_WRITE);
        xssfReader = new XSSFReader(pkg);
        sst = new ReadOnlySharedStringsTable(pkg);
        stylesTable = xssfReader.getStylesTable();
        formatter = new DataFormatter();
    }

    public void openFile(InputStream inputStream) throws Exception
    {
        pkg = OPCPackage.open(inputStream);
        xssfReader = new XSSFReader(pkg);
        sst = new ReadOnlySharedStringsTable(pkg);
        stylesTable = xssfReader.getStylesTable();
        formatter = new DataFormatter();
    }

    public ArrayList<String> getColumnNames(String sheetName) throws Exception
    {
        XMLEventReader sheetReader;

        sheetReader = getCurrentSheetReader(sheetName);
        if (sheetReader != null)
        {
            // If the sheet columns have not been read yet, read them before reading subsequent data.
            if (!columnNames.containsKey(sheetName))
            {
                columnNames.put(sheetName, new LinkedHashMap(extractDataRow(sheetName, sheetReader)));
            }

            return new ArrayList<String>(columnNames.get(sheetName).values());
        }
        else throw new Exception("Sheet not found: " + sheetName);
    }

    public List<String> getSheetNames() throws Exception
    {
        XSSFReader.SheetIterator sheetIterator;
        InputStream sheetInputStream = null;
        List<String> sheetNames;

        try
        {
            sheetIterator = (XSSFReader.SheetIterator)xssfReader.getSheetsData();
            sheetNames = new ArrayList<String>();
            while (sheetIterator.hasNext())
            {
                sheetInputStream = sheetIterator.next();
                sheetNames.add(sheetIterator.getSheetName());
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            if (sheetInputStream != null) sheetInputStream.close();
        }
        return sheetNames;
    }

    public int getSheetRowCount(String sheetName) throws Exception
    {
        if (sheetRowCounts.containsKey(sheetName))
        {
            return sheetRowCounts.get(sheetName);
        }
        else
        {
            InputStream sheetInputStream = null;
            XMLEventReader sheetReader = null;

            try
            {
                int rowCount;

                sheetInputStream = getNewSheetInputStream(sheetName);
                sheetReader = getNewSheetReader(sheetInputStream);
                rowCount = countRows(sheetReader) - 1; // Subtract the first row, since it only contains the column names and no data.

                sheetRowCounts.put(sheetName, rowCount);
                return rowCount;
            }
            finally
            {
                if (sheetReader != null) sheetReader.close();
                if (sheetInputStream != null) sheetInputStream.close();
            }
        }
    }

    public HashMap<String, Object> readRow(String sheetName) throws Exception
    {
        XMLEventReader sheetReader;

        sheetReader = getCurrentSheetReader(sheetName);
        if (sheetReader != null)
        {
            LinkedHashMap<String, Object> extractedDataRow;

            // If the sheet columns have not been read yet, read them before reading subsequent data.
            if (!columnNames.containsKey(sheetName))
            {
                columnNames.put(sheetName, new LinkedHashMap(extractDataRow(sheetName, sheetReader)));
            }

            extractedDataRow = extractDataRow(sheetName, sheetReader);
            if (extractedDataRow != null)
            {
                LinkedHashMap<String, Object> resultantDataRow;

                // Create a new data row, add all the columns on the sheet to it.
                resultantDataRow = new LinkedHashMap<String, Object>();
                for (String columnName : getColumnNames(sheetName))
                {
                    resultantDataRow.put(columnName, null);
                }

                // Add all available values from the next sheet row.
                resultantDataRow.putAll(extractedDataRow);

                // Return the data row.
                return resultantDataRow;
            }
            else return null;
        }
        else throw new Exception("Sheet not found: " + sheetName);
    }

    public void closeFile() throws Exception
    {
        // Close all readers.
        for (XMLEventReader reader : sheetReaders.values())
        {
            reader.close();
        }

        // Close all of the underlying input streams.
        for (InputStream inputStream : sheetInputStreams.values())
        {
            inputStream.close();
        }

        // Clear the transient collections.
        sheetInputStreams.clear();
        sheetReaders.clear();
        pkg.close();

        // Empty all instance variables.
        pkg = null;
        xssfReader = null;
        sst = null;
        stylesTable = null;
        formatter = null;
    }

    private String getColumnName(String sheetName, String columnAddress)
    {
        String addressLetter;
        String columnName;

        addressLetter = getAddressLetter(columnAddress);
        if (!columnNames.containsKey(sheetName))
        {
            return addressLetter;
        }
        else
        {
            columnName = columnNames.get(sheetName).get(addressLetter);
            return columnName != null ? columnName : columnAddress;
        }
    }

    private String getAddressLetter(String columnAddress)
    {
        int length;

        length = 0;
        for (int charIndex = 0; charIndex < columnAddress.length(); charIndex++)
        {
            char character;

            character = columnAddress.charAt(charIndex);
            if (Character.isLetter(character))
            {
                length++;
            }
            else break;
        }

        return columnAddress.substring(0, length);
    }

    private XMLEventReader getCurrentSheetReader(String sheetName) throws Exception
    {
        if (sheetReaders.containsKey(sheetName))
        {
            return sheetReaders.get(sheetName);
        }
        else
        {
            InputStream sheetInputStream;
            XMLEventReader reader;

            sheetInputStream = getNewSheetInputStream(sheetName);
            reader = getNewSheetReader(sheetInputStream);

            sheetInputStreams.put(sheetName, sheetInputStream);
            sheetReaders.put(sheetName, reader);

            return reader;
        }
    }

    private XMLEventReader getNewSheetReader(InputStream sheetInputStream) throws Exception
    {
        XMLInputFactory factory;
        XMLEventReader reader;

        factory = XMLInputFactory.newInstance();
        reader = factory.createXMLEventReader(sheetInputStream);

        return reader;
    }

    private InputStream getNewSheetInputStream(String sheetName) throws Exception
    {
        XSSFReader.SheetIterator sheetIterator;
        InputStream sheetInputStream = null;

        try
        {
            sheetIterator = (XSSFReader.SheetIterator)xssfReader.getSheetsData();
            while ((sheetInputStream = sheetIterator.next()) != null)
            {
                    if (sheetIterator.getSheetName().equals(sheetName))
                    {
                        return sheetInputStream;
                    }
                    else
                    {
                        // Close the input stream.
                        sheetInputStream.close();
                    }
            }
        }
        catch (NoSuchElementException e)
        {
            if (sheetInputStream != null) sheetInputStream.close();
            throw new Exception("Excel sheet not found: " + sheetName, e);
        }
        catch (Exception e)
        {
            if (sheetInputStream != null) sheetInputStream.close();
            throw e;
        }

        throw new RuntimeException("Sheet not found: " + sheetName);
    }

    private LinkedHashMap<String, Object> extractDataRow(String sheetName, XMLEventReader sheetReader) throws Exception
    {
        XMLElement rowElement;

        rowElement = extractXMLElement(sheetReader, "row");
        if (rowElement != null)
        {
            ArrayList<XMLElement> cellElements;
            LinkedHashMap<String, Object> dataRow;

            dataRow = new LinkedHashMap<String, Object>();
            cellElements = rowElement.getChildElements("c");
            for (XMLElement cellElement : cellElements)
            {
                String columnName;
                String cellType;
                String cellAddress;
                String cellStyle;

                // Determine the column value, given its type.
                cellAddress = cellElement.getAttributeValue("r");
                cellType = cellElement.getAttributeValue("t");
                cellStyle = cellElement.getAttributeValue("s");
                columnName = getColumnName(sheetName, cellAddress);
                if (cellType == null) // Numeric value.
                {
                    ArrayList<XMLElement> valueElements;
                    XSSFCellStyle style;
                    int styleIndex;
                    short formatIndex = -1;
                    String formatString = null;

                    // Get the style and format String for this numeric value.
                    if (cellStyle != null)
                    {
                        styleIndex = Integer.parseInt(cellStyle);
                        style = stylesTable.getStyleAt(styleIndex);
                        formatIndex = style.getDataFormat();
                        formatString = style.getDataFormatString();
                        if (formatString == null)
                        {
                            formatString = BuiltinFormats.getBuiltinFormat(formatIndex);
                        }
                    }

                    valueElements = cellElement.getChildElements("v");
                    if (valueElements.size() > 0)
                    {
                        double numericValue;

                        numericValue = Double.parseDouble(valueElements.get(0).getValue());
                        if (formatString != null)
                        {
                            dataRow.put(columnName, formatter.formatRawCellContents(numericValue, formatIndex, formatString));
                        }
                        else
                        {
                            dataRow.put(columnName, numericValue);
                        }
                    }
                }
                else if (cellType.equals("b")) // Boolean value.
                {
                    ArrayList<XMLElement> valueElements;

                    valueElements = cellElement.getChildElements("v");
                    if (valueElements.size() > 0)
                    {
                        String value;

                        value = valueElements.get(0).getValue();
                        dataRow.put(columnName, "1".equals(value));
                    }
                }
                else if (cellType.equals("s")) // Shared String value.
                {
                    ArrayList<XMLElement> valueElements;

                    valueElements = cellElement.getChildElements("v");
                    if (valueElements.size() > 0)
                    {
                        String stringValue;
                        int idx;

                        idx = Integer.parseInt(valueElements.get(0).getValue());
                        stringValue = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
                        dataRow.put(columnName, (stringValue == null || stringValue.trim().length() == 0) ? null : stringValue);
                    }
                }
                else if (cellType.equals("inlineStr"))
                {
                    ArrayList<XMLElement> isElements;

                    isElements = cellElement.getChildElements("is");
                    if (isElements.size() > 0)
                    {
                        ArrayList<XMLElement> tElements;
                        XMLElement isElement;

                        isElement = isElements.get(0);
                        tElements = isElement.getChildElements("t");
                        if (tElements.size() > 0)
                        {
                            String stringValue;

                            stringValue = new XSSFRichTextString(tElements.get(0).getValue()).toString();
                            dataRow.put(columnName, (stringValue == null || stringValue.trim().length() == 0) ? null : stringValue);
                        }
                    }
                }
                else
                {
                    System.out.println("Unsupported cell type found: " + cellType);
                }
            }

            return dataRow;
        }
        else return null;
    }

    private int countRows(XMLEventReader eventReader) throws Exception
    {
        XMLEvent nextEvent;
        int eventType;
        int rowCount;

        // Loop through all of available events, counting each row.
        rowCount = 0;
        while (eventReader.hasNext())
        {
            nextEvent = eventReader.nextEvent();
            eventType = nextEvent.getEventType();
            if (eventType == XMLEvent.START_ELEMENT)
            {
                StartElement startElement;

                startElement = (StartElement)nextEvent;
                if (startElement.getName().getLocalPart().equals("row"))
                {
                    rowCount++;
                }
            }
        }

        // No more elements to read.
        return rowCount;
    }

    private XMLElement extractXMLElement(XMLEventReader eventReader, String elementName) throws Exception
    {
        XMLEvent nextEvent;
        int eventType;

        // Loop through all of available events until the start of the specified element is found.
        while (eventReader.hasNext())
        {
            nextEvent = eventReader.peek();
            eventType = nextEvent.getEventType();
            if (eventType == XMLEvent.START_ELEMENT)
            {
                StartElement startElement;

                startElement = (StartElement)nextEvent;
                if (startElement.getName().getLocalPart().equals(elementName))
                {
                    return extractXMLElement(eventReader);
                }
                else
                {
                    // Consume the event, since it is not the required type for extraction.
                    nextEvent = eventReader.nextEvent();
                }
            }
            else
            {
                // Consume the event, since it is not the required type for extraction.
                nextEvent = eventReader.nextEvent();
            }
        }

        // No more elements to read.
        return null;
    }

    private XMLElement extractXMLElement(XMLEventReader eventReader) throws Exception
    {
        XMLEvent nextEvent;
        int nextEventType;
        int eventType;

        nextEvent = eventReader.nextEvent();
        eventType = nextEvent.getEventType();
        if (eventType == XMLEvent.START_ELEMENT)
        {
            XMLElement newElement;
            Iterator<Attribute> attributeIterator;
            StartElement startElement;
            StringBuffer value;

            startElement = (StartElement)nextEvent;
            newElement = new XMLElement(startElement.getName().getLocalPart());
            attributeIterator = startElement.getAttributes();
            value = new StringBuffer();

            // Add all non-namespace declared attributes.
            while (attributeIterator.hasNext())
            {
                Attribute attribute;

                attribute = attributeIterator.next();
                newElement.addAttribute(new XMLAttribute(attribute.getName().getLocalPart(), attribute.getValue()));
            }

            // Parse any remaining events that are part of this element's content.
            while ((nextEventType = eventReader.peek().getEventType()) != XMLEvent.END_ELEMENT)
            {
                if (nextEventType == XMLEvent.CHARACTERS)
                {
                    Characters charEvent;

                    nextEvent = eventReader.nextEvent();
                    charEvent = (Characters)nextEvent;
                    value.append(charEvent.getData());
                }
                else if (nextEventType == XMLEvent.ATTRIBUTE)
                {
                    newElement.addAttribute(extractXMLAttribute(eventReader));
                }
                else if (nextEventType == XMLEvent.START_ELEMENT)
                {
                    newElement.addChildElement(extractXMLElement(eventReader));
                }
            }

            // Add the text value of this element.
            newElement.setValue(value.toString());

            // Consume the END_ELEMENT event and then return the extracted element.
            nextEvent = eventReader.nextEvent();
            return newElement;
        }
        else throw new Exception("Invalid start of element: " + nextEvent.getLocation());
    }

    private XMLAttribute extractXMLAttribute(XMLEventReader eventReader) throws Exception
    {
        XMLEvent nextEvent;
        int eventType;

        nextEvent = eventReader.nextEvent();
        eventType = nextEvent.getEventType();
        if (eventType == XMLEvent.ATTRIBUTE)
        {
            XMLAttribute newAttribute;
            Attribute attribute;

            attribute = (Attribute)nextEvent;
            newAttribute = new XMLAttribute(attribute.getName().getLocalPart(), attribute.getValue());
            return newAttribute;
        }
        else throw new Exception("Invalid start of attribute: " + nextEvent.getLocation());
    }

    private class XMLElement
    {
        private String name;
        private String value;
        private ArrayList<XMLAttribute> attributes;
        private ArrayList<XMLElement> childElements;

        public XMLElement(String name)
        {
            this.name = name;
            this.value = null;
            this.attributes = new ArrayList<XMLAttribute>();
            this.childElements = new ArrayList<XMLElement>();
        }

        public ArrayList<XMLAttribute> getAttributes()
        {
            return attributes;
        }

        public void setAttributes(ArrayList<XMLAttribute> attributes)
        {
            this.attributes = attributes;
        }

        public void addAttribute(XMLAttribute attribute)
        {
            this.attributes.add(attribute);
        }

        public XMLAttribute getAttribute(String attributeName)
        {
            for (XMLAttribute attribute : attributes)
            {
                if (attribute.getName().equals(attributeName))
                {
                    return attribute;
                }
            }

            return null;
        }

        public String getAttributeValue(String attributeName)
        {
            for (XMLAttribute attribute : attributes)
            {
                if (attribute.getName().equals(attributeName))
                {
                    return attribute.getValue();
                }
            }

            return null;
        }

        public ArrayList<XMLElement> getChildElements()
        {
            return childElements;
        }

        public ArrayList<XMLElement> getChildElements(String elementName)
        {
            ArrayList<XMLElement> matchingElements;

            matchingElements = new ArrayList<XMLElement>();
            for (XMLElement childElement : childElements)
            {
                if (childElement.getName().equals(elementName))
                {
                    matchingElements.add(childElement);
                }
            }

            return matchingElements;
        }

        public void setChildElements(ArrayList<XMLElement> childElements)
        {
            this.childElements = childElements;
        }

        public void addChildElement(XMLElement element)
        {
            this.childElements.add(element);
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }
    }

    private class XMLAttribute
    {
        private String name;
        private String value;

        public XMLAttribute(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getName()
        {
            return name;
        }

        public String getValue()
        {
            return value;
        }
    }
}
