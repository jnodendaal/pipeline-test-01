package com.pilog.t8.utilities.data.resultset.extractor;

/**
 * @author Bouwer du Preez
 */
public interface DataConverter
{
    public Object convertDataValue(String tableName, String columnName, Object dataValue, int sqlType) throws Exception;
}
