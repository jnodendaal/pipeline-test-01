package com.pilog.t8.utilities.data.resultset.extractor;

import java.math.BigDecimal;

/**
 * @author Bouwer du Preez
 */
public class BigDecimalToIntegerConverter implements DataConverter
{
    @Override
    public Object convertDataValue(String tableName, String columnName, Object dataValue, int sqlType)
    {
        if ((dataValue != null) && (dataValue instanceof BigDecimal))
        {
            return ((Number)dataValue).intValue();
        }
        else return dataValue;
    }
}
