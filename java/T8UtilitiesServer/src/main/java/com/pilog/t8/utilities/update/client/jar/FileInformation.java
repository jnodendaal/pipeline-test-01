/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.utilities.update.client.jar;

import java.io.*;
import java.security.*;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class FileInformation implements Serializable
{

    public FileInformation(File file)
    {
        this.jarName = file.getName();
        this.jarHash = getHash(file);
        this.lastModified = System.currentTimeMillis();
    }
    private static final long serialVersionUID = 1L;
    private String jarName;
    private byte[] jarHash;
    private long lastModified;

    private byte[] getHash(File file)
    {
        FileInputStream digestInputStream = null;
        try
        {
            digestInputStream = new FileInputStream(file);

            byte[] buffer = new byte[1024];
            MessageDigest complete = MessageDigest.getInstance("MD5");
            int numRead;

            do
            {
                numRead = digestInputStream.read(buffer);
                if (numRead > 0)
                {
                    complete.update(buffer, 0, numRead);
                }
            }
            while (numRead != -1);

            return complete.digest();
        }
        catch (Exception ex)
        {
            System.out.println("Failed to create has for file " + file);
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        finally
        {
            if (digestInputStream != null)
            {
                try
                {
                    digestInputStream.close();
                }
                catch (IOException ex)
                {
                }
            }
        }
        return null;
    }

    /**
     * @return the jarName
     */
    public String getJarName()
    {
        return jarName;
    }

    /**
     * @param jarName the jarName to set
     */
    public void setJarName(String jarName)
    {
        this.jarName = jarName;
    }

    /**
     * @return the jarHash
     */
    public byte[] getJarHash()
    {
        return jarHash;
    }

    /**
     * @param jarHash the jarHash to set
     */
    public void setJarHash(byte[] jarHash)
    {
        this.jarHash = jarHash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof FileInformation)
        {
            return this.getJarName().equals(((FileInformation) obj).getJarName());
        }
        return super.equals(obj);
    }

    /**
     * @return the lastModified
     */
    public long getLastModified()
    {
        return lastModified;
    }

    /**
     * @param lastModified the lastModified to set
     */
    public void setLastModified(long lastModified)
    {
        this.lastModified = lastModified;
    }

    public String getHashString()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < getJarHash().length; i++)
        {
            sb.append(Integer.toString((getJarHash()[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return "Jarname: " + getJarName() + ", Hash: " + getHashString() + ", lastModified: " + lastModified;
    }

    private String byteToString(byte[] array)
    {
        StringBuilder builder = new StringBuilder();
        for (byte b : array)
        {
            builder.append(b);
        }
        return builder.toString();
    }
}
