package com.pilog.t8.utilities.components.menu;

import javax.swing.JMenuItem;

/**
 * @author Bouwer du Preez
 */
public class MenuItem extends JMenuItem
{
    private String code;
    private Object value;
    
    public MenuItem(String displayName, String code, Object value)
    {
        super(displayName);
        this.code = code;
        this.value = value;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }
}
