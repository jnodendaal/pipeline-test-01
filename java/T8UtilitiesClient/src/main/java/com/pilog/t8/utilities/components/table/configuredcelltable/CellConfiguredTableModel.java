package com.pilog.t8.utilities.components.table.configuredcelltable;

import java.awt.Dimension;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 * @author Bouwer du Preez
 */
public class CellConfiguredTableModel extends DefaultTableModel
{
    protected CellConfiguration cellConfiguration;

    public CellConfiguredTableModel(int numRows, int numColumns)
    {
        Vector names;
        
        names = new Vector(numColumns);
        names.setSize(numColumns);
        setColumnIdentifiers(names);
        dataVector = new Vector();
        setNumRows(numRows);
        cellConfiguration = new DefaultCellConfiguration(numRows, numColumns);
    }

    public CellConfiguredTableModel(List<String> columnNames, int numRows)
    {
        setColumnIdentifiers(new Vector(columnNames));
        dataVector = new Vector();
        setNumRows(numRows);
        cellConfiguration = new DefaultCellConfiguration(numRows, columnNames.size());
    }

    public CellConfiguredTableModel(Object[] columnNames, int numRows)
    {
        this(convertToVector(columnNames), numRows);
    }

    public CellConfiguredTableModel(Vector data, Vector columnNames)
    {
        setDataVector(data, columnNames);
    }

    public CellConfiguredTableModel(Object[][] data, Object[] columnNames)
    {
        setDataVector(data, columnNames);
    }

    @Override
    public void setDataVector(Vector newData, Vector columnNames)
    {
        cellConfiguration = new DefaultCellConfiguration(newData.size(), columnNames.size());
        super.setDataVector(newData, columnNames);
    }

    @Override
    public void addColumn(Object columnName, Vector columnData)
    {
        cellConfiguration.addColumn();
        super.addColumn(columnName, columnData);
    }

    @Override
    public void insertRow(int row, Vector rowData)
    {
        cellConfiguration.insertRow(row);
        super.insertRow(row, rowData);
    }

    public CellConfiguration getCellConfiguration()
    {
        return cellConfiguration;
    }

    public void setCellConfiguration(CellConfiguration newCellConfiguration)
    {
        int numColumns;
        int numRows;
        
        numColumns = getColumnCount();
        numRows = getRowCount();
        if ((newCellConfiguration.getSize().width != numColumns) || (newCellConfiguration.getSize().height != numRows))
        {
            newCellConfiguration.setSize(new Dimension(numRows, numColumns));
        }
        
        cellConfiguration = newCellConfiguration;
        fireTableDataChanged();
    }
}
