package com.pilog.t8.utilities.components.table.configuredcelltable;


import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.TableCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class CellConfiguredTableUI extends BasicTableUI
{
    @Override
    public void paint(Graphics g, JComponent c)
    {
        Rectangle oldClipBounds;
        Rectangle clipBounds;
        int tableWidth;
        int firstRowIndex;
        int lastRowIndex;

        oldClipBounds = g.getClipBounds();
        clipBounds = new Rectangle(oldClipBounds);
        tableWidth = table.getColumnModel().getTotalColumnWidth();
        clipBounds.width = Math.min(clipBounds.width, tableWidth);
        g.setClip(clipBounds);

        firstRowIndex = table.rowAtPoint(new Point(0, clipBounds.y));
        lastRowIndex = table.getRowCount() - 1;

        // Paint all visible cells.
        for (int rowIndex = firstRowIndex; rowIndex <= lastRowIndex; rowIndex++)
        {
            Rectangle rowRect;
            
            rowRect = table.getCellRect(rowIndex, 0, true);
            rowRect.add(table.getCellRect(rowIndex, table.getColumnCount(), true));
            if (rowRect.intersects(clipBounds))
            {
                paintRow(g, rowIndex);
            }
        }
        
        g.setClip(oldClipBounds);
    }

    private void paintRow(Graphics g, int rowIndex)
    {
        CellConfiguredTableModel tableModel;
        CellConfiguration cellConfiguration;
        Rectangle rect;
        boolean drawn;
        int columnCount;

        rect = g.getClipBounds();
        drawn = false;
        tableModel = (CellConfiguredTableModel)table.getModel();
        cellConfiguration = tableModel.getCellConfiguration();
        columnCount = table.getColumnCount();

        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
        {
            Rectangle cellRect;
            int cellRow;
            int cellColumn;
            int modelRowIndex;
            int modelColumnIndex;
            
            modelRowIndex = table.convertRowIndexToModel(rowIndex);
            modelColumnIndex = table.convertColumnIndexToModel(columnIndex);

            cellRect = table.getCellRect(rowIndex, columnIndex, true);
            if (cellConfiguration.isVisible(modelRowIndex, modelColumnIndex))
            {
                cellRow = rowIndex;
                cellColumn = columnIndex;
            }
            else
            {
                cellRow = rowIndex + cellConfiguration.getSpan(modelRowIndex, modelColumnIndex).getRowsSpanned();
                cellColumn = columnIndex + cellConfiguration.getSpan(modelRowIndex, modelColumnIndex).getColumnSpanned();
            }
            
            if (cellRect.intersects(rect))
            {
                drawn = true;
                paintCell(g, cellRect, cellRow, cellColumn);
            }
            else if (drawn)
            {
                break;
            }
        }
    }

    private void paintCell(Graphics g, Rectangle cellRect, int rowIndex, int columnIndex)
    {
        CellConfiguredTableModel tableModel;
        CellConfiguration cellConfiguration;
        int spacingHeight;
        int spacingWidth;
        Color colorBackup;
        Color backgroundColor;
        int modelRowIndex;
        int modelColumnIndex;

        modelRowIndex = table.convertRowIndexToModel(rowIndex);
        modelColumnIndex = table.convertColumnIndexToModel(columnIndex);

        tableModel = (CellConfiguredTableModel)table.getModel();
        cellConfiguration = tableModel.getCellConfiguration();
        spacingHeight = table.getRowMargin();
        spacingWidth = table.getColumnModel().getColumnMargin();
        backgroundColor = cellConfiguration.getBackground(modelRowIndex, modelColumnIndex);
        
        // Store the color of the graphics context.
        colorBackup = g.getColor();
        
        // Draw the cell background if required.
        if (backgroundColor != null)
        {
            g.setColor(backgroundColor);
            g.fillRect(cellRect.x, cellRect.y, cellRect.width, cellRect.height);
        }
        
        // Draw the grid and then reset the color as it was.
        g.setColor(table.getGridColor());
        g.drawRect(cellRect.x, cellRect.y, cellRect.width, cellRect.height);
        g.setColor(colorBackup);

        // Set the bounds of the cell recangle by taking spacing into account.
        cellRect.setBounds(cellRect.x + spacingWidth, cellRect.y + spacingHeight, cellRect.width - spacingWidth, cellRect.height - spacingHeight);

        // Set the bounds of the editor ot use the renderer component to render the cell contents.
        if (table.isEditing() && table.getEditingRow() == rowIndex && table.getEditingColumn() == columnIndex)
        {
            Component component;
                    
            component = table.getEditorComponent();
            component.setBounds(cellRect);
            component.validate();
        }
        else
        {
            TableCellRenderer renderer;
            Component component;

            // Prepare the renderer component.
            renderer = table.getCellRenderer(rowIndex, columnIndex);
            component = table.prepareRenderer(renderer, rowIndex, columnIndex);
            if (component.getParent() == null)
            {
                rendererPane.add(component);
            }
            
            // Paint the renderer component.
            rendererPane.paintComponent(g, component, table, cellRect.x, cellRect.y, cellRect.width, cellRect.height, true);
        }
    }
}
