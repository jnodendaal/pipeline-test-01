package com.pilog.t8.utilities.components.table.configuredcelltable;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

/**
 * @author Bouwer du Preez
 */
public interface CellConfiguration
{
    public void addColumn();
    public void addRow();
    public void insertRow(int row);
    public Dimension getSize();
    public void setSize(Dimension size);
    
    public Font getFont(int row, int column);
    public void setFont(Font font, int row, int column);
    public void setFont(Font font, int[] rows, int[] columns);
    
    public Color getForeground(int row, int column);
    public void setForeground(Color color, int row, int column);
    public void setForeground(Color color, int[] rows, int[] columns);
    public Color getBackground(int row, int column);
    public void setBackground(Color color, int row, int column);
    public void setBackground(Color color, int[] rows, int[] columns);
    
    public CellSpan getSpan(int row, int column);
    public void setSpan(CellSpan span, int row, int column);
    public boolean isVisible(int row, int column);
    public void combine(int startRow, int startColumn, int rowSpan, int columnSpan);
    public void split(int row, int column);
}
