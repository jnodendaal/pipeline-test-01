package com.pilog.t8.utilities.components.table.configuredcelltable;

/**
 * @author Bouwer du Preez
 */
public class CellSpan
{
    int rowsSpanned;
    int columnSpanned;

    public CellSpan(int rowsSpanned, int columnsSpanned)
    {
        this.rowsSpanned = rowsSpanned;
        this.columnSpanned = columnsSpanned;
    }

    public int getRowsSpanned()
    {
        return rowsSpanned;
    }

    public void setRowsSpanned(int rowsSpanned)
    {
        this.rowsSpanned = rowsSpanned;
    }

    public int getColumnSpanned()
    {
        return columnSpanned;
    }

    public void setColumnSpanned(int columnSpanned)
    {
        this.columnSpanned = columnSpanned;
    }
}
