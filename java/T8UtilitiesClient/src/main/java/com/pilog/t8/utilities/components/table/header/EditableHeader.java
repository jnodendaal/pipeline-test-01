package com.pilog.t8.utilities.components.table.header;

import java.awt.Component;
import java.awt.Rectangle;
import java.util.EventObject;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 * @author Bouwer du Preez
 */
public class EditableHeader extends JTableHeader implements CellEditorListener
{
    public final int HEADER_ROW = -10;
    transient protected int editingColumn;
    transient protected TableCellEditor cellEditor;
    transient protected Component editorComp;

    public EditableHeader(TableColumnModel columnModel)
    {
        super(columnModel);
        setReorderingAllowed(false);
        cellEditor = null;
        recreateTableColumn(columnModel);
    }

    @Override
    public void updateUI()
    {
        setUI(new EditableHeaderUI());
        resizeAndRepaint();
        invalidate();
    }

    protected void recreateTableColumn(TableColumnModel columnModel)
    {
        int columnCount = columnModel.getColumnCount();
        EditableHeaderTableColumn[] newCols = new EditableHeaderTableColumn[columnCount];
        TableColumn[] oldCols = new TableColumn[columnCount];
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
        {
            oldCols[columnIndex] = columnModel.getColumn(columnIndex);
            newCols[columnIndex] = new EditableHeaderTableColumn();
            newCols[columnIndex].copyValues(oldCols[columnIndex]);
        }
        
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
        {
            columnModel.removeColumn(oldCols[columnIndex]);
        }
        
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
        {
            columnModel.addColumn(newCols[columnIndex]);
        }
    }

    public boolean editCellAt(int index)
    {
        return editCellAt(index);
    }

    public boolean editCellAt(int index, EventObject e)
    {
        if (cellEditor != null && !cellEditor.stopCellEditing())
        {
            return false;
        }
        else if (!isCellEditable(index))
        {
            return false;
        }
        else
        {
            TableCellEditor editor;

            editor = getCellEditor(index);
            if (editor != null && editor.isCellEditable(e))
            {
                editorComp = prepareEditor(editor, index);
                editorComp.setBounds(getHeaderRect(index));
                add(editorComp);
                editorComp.validate();
                setCellEditor(editor);
                setEditingColumn(index);
                editor.addCellEditorListener(this);

                return true;
            }
            
            return false;
        }
    }

    public boolean isCellEditable(int index)
    {
        if (getReorderingAllowed())
        {
            return false;
        }
        else
        {
            int columnIndex;
            EditableHeaderTableColumn col;
            
            columnIndex = columnModel.getColumn(index).getModelIndex();
            col = (EditableHeaderTableColumn)columnModel.getColumn(columnIndex);
            return col.isHeaderEditable();
        }
    }

    public TableCellEditor getCellEditor(int index)
    {
        int columnIndex = columnModel.getColumn(index).getModelIndex();
        EditableHeaderTableColumn col = (EditableHeaderTableColumn)columnModel.getColumn(columnIndex);
        return col.getHeaderEditor();
    }

    public void setCellEditor(TableCellEditor newEditor)
    {
        TableCellEditor oldEditor;
        
        oldEditor = cellEditor;
        cellEditor = newEditor;

        if (oldEditor != null && oldEditor instanceof TableCellEditor)
        {
            ((TableCellEditor)oldEditor).removeCellEditorListener((CellEditorListener)this);
        }
        
        if (newEditor != null && newEditor instanceof TableCellEditor)
        {
            ((TableCellEditor)newEditor).addCellEditorListener((CellEditorListener)this);
        }
    }

    public Component prepareEditor(TableCellEditor editor, int index)
    {
        Object value = columnModel.getColumn(index).getHeaderValue();
        boolean isSelected = true;
        int row = HEADER_ROW;
        JTable table = getTable();
        Component comp = editor.getTableCellEditorComponent(table, value, isSelected, row, index);
        if (comp instanceof JComponent)
        {
            ((JComponent)comp).setNextFocusableComponent(this);
        }
        return comp;
    }

    public TableCellEditor getCellEditor()
    {
        return cellEditor;
    }

    public Component getEditorComponent()
    {
        return editorComp;
    }

    public void setEditingColumn(int aColumn)
    {
        editingColumn = aColumn;
    }

    public int getEditingColumn()
    {
        return editingColumn;
    }

    public void removeEditor()
    {
        TableCellEditor editor = getCellEditor();
        if (editor != null)
        {
            editor.removeCellEditorListener(this);

            requestFocus();
            remove(editorComp);

            int index = getEditingColumn();
            Rectangle cellRect = getHeaderRect(index);

            setCellEditor(null);
            setEditingColumn(-1);
            editorComp = null;

            repaint(cellRect);
        }
    }

    public boolean isEditing()
    {
        return (cellEditor == null) ? false : true;
    }

    @Override
    public void editingStopped(ChangeEvent e)
    {
        TableCellEditor editor = getCellEditor();
        if (editor != null)
        {
            Object value = editor.getCellEditorValue();
            int index = getEditingColumn();
            columnModel.getColumn(index).setHeaderValue(value);
            removeEditor();
        }
    }

    @Override
    public void editingCanceled(ChangeEvent e)
    {
        removeEditor();
    }
}
