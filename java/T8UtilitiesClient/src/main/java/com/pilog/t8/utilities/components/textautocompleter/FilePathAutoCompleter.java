package com.pilog.t8.utilities.components.textautocompleter;

import java.io.File;
import java.io.FilenameFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

/**
 * @author Bouwer du Preez
 */
public class FilePathAutoCompleter extends TextAutoCompleter
{
    public FilePathAutoCompleter(JTextComponent comp)
    {
        super(comp);
    }

    @Override
    protected boolean updateListData()
    {
        String value;
        int index1;
        int index2;
        int index;

        value = textComponent.getText();
        index1 = value.lastIndexOf('\\');
        index2 = value.lastIndexOf('/');
        index = Math.max(index1, index2);
        if (index == -1)
        {
            return false;
        }
        else
        {
            String directory;
            String prefix;
            String[] files;

            // Use the currently entered text in the text component as the prefix for filtering files available as the choices in the list.
            directory = value.substring(0, index + 1);
            prefix = index == value.length() - 1 ? null : value.substring(index + 1).toLowerCase();
            files = new File(directory).list(new PrefixFilenameFilter(prefix));

            // If any files were found, update the list data.
            if (files == null)
            {
                list.setListData(new String[0]);
                return true;
            }
            else if ((files.length == 1) && files[0].equalsIgnoreCase(prefix))
            {
                list.setListData(new String[0]);
                return true;
            }
            else
            {
                list.setListData(files);
                return true;
            }
        }
    }

    @Override
    protected void listItemSelected(Object selected)
    {
        if (selected != null)
        {
            String value;
            int index1;
            int index2;
            int index;

            value = textComponent.getText();
            index1 = value.lastIndexOf('\\');
            index2 = value.lastIndexOf('/');
            index = Math.max(index1, index2);
            if (index != -1)
            {
                try
                {
                    int prefixLength;

                    prefixLength = textComponent.getDocument().getLength() - index - 1;
                    textComponent.getDocument().insertString(textComponent.getCaretPosition(), (String.valueOf(selected)).substring(prefixLength), null);
                }
                catch (BadLocationException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void listDataUpdated()
    {
    }

    private class PrefixFilenameFilter implements FilenameFilter
    {
        private final String prefix;

        public PrefixFilenameFilter(String prefix)
        {
            this.prefix = prefix;
        }

        @Override
        public boolean accept(File dir, String name)
        {
            return prefix != null ? name.toLowerCase().startsWith(prefix) : true;
        }
    }
}
