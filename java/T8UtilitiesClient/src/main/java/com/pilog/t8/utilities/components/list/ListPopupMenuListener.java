package com.pilog.t8.utilities.components.list;

/**
 * @author Bouwer du Preez
 */
public interface ListPopupMenuListener
{
    public void popupItemSelected(Object selectedItem);

    public void popupItemsSelected(Object[] selectedItems);
}
