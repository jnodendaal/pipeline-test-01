package com.pilog.t8.utilities.components.processingdialogs;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;

/**
 * This class provides a small dialog that displays a single label of text
 * and a progress bar while a specific task is executed.  The client can
 * specify the text to be displayed as well as a grace period.  The grace
 * period defines the amount of time that the dialog will wait before
 * displaying itself.  If the task is completed before the grace period
 * expires, the processing dialog will not shown at all.
 *
 * @author Bouwer du Preez
 */
public class ProcessingDialog extends javax.swing.JDialog
{
    private static final T8Logger logger = T8Log.getLogger(ProcessingDialog.class);

    private ThreadTask task;
    private Object taskResult;
    private Throwable taskThrowable;
    private boolean processingCompleted;

    /**
     * Constructor for the class ProcessingDialog.
     *
     * @param parent The parent Frame in which this dialog will be displayed.
     * @param task The ThreadTask to execute.
     * @param processingText The text to be displayed on the processing dialog.
     * @param gracePeriod The period of time (milliseconds) that this dialod
     * will wait before displaying itself.  If the task is completed before the
     * grace period expires, the processing dialog will not shown at all.
     * @param width The width of the dialog.
     * @param height The height of the dialog.
     */
    private ProcessingDialog(java.awt.Window parent, ThreadTask task, String processingText, int gracePeriod, int width, int height)
    {
        super(parent, java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        initComponents();

        this.processingCompleted = false;
        this.task = task;
        this.jLabelProcessingText.setText(processingText);
        this.jProgressBar.setIndeterminate(task.isIndeterminate());
        this.jProgressBar.setMinimum(task.getMinimumProgress());
        this.jProgressBar.setMaximum(task.getMaximumProgress());

        // Execute the process in a separate thread.
        new Thread(new Runner()).start();
        new Thread(new Checker()).start();

        // Show this dialog while processing.
        setSize(width, height);
        setLocationRelativeTo(parent);
        setResizable(false);

        // Wait for a few milliseconds (if the processing is already complete after this period, the processing bar will not even be shown).
        if (gracePeriod > -1)
        {
            try
            {
                Thread.sleep(gracePeriod);
            }
            catch (InterruptedException e)
            {
                logger.log("An exception occurred during grace period.", e);
            }

            // If processing has not been completed, show the processing dialog.
            if (!processingCompleted)
            {
                setVisible(true);
            }
        }
    }

    /**
     * Displays a small dialog that contains a single label of text
     * and a progress bar while a specific task is executed.  The client can
     * specify the text to be displayed as well as a grace period.  The grace
     * period defines the amount of time that the dialog will wait before
     * displaying itself.  If the task is completed before the grace period
     * expires, the processing dialog will not shown at all.
     *
     * @param task The ThreadTask to execute.
     * @param processingText The text to be displayed on the processing dialog.
     * @param gracePeriod The period of time (milliseconds) that this dialog
     * will wait before displaying itself.  If the task is completed before the
     * grace period expires, the processing dialog will not shown at all.
     * @param width The width of the dialog.
     * @param height The height of the dialog.
     * @return The result of the task after execution.
     */
    public static final Object doTask(ThreadTask task, String processingText, int gracePeriod, int width, int height, java.awt.Window parentContainer) throws Throwable
    {
        ProcessingDialog dialog;

        dialog = new ProcessingDialog(parentContainer, task, processingText, gracePeriod, width, height);
        if (dialog.taskThrowable != null) throw dialog.taskThrowable;
        else return dialog.getTaskResult();
    }

    /**
     * Returns the result of the executed task.
     *
     * @return The result of the executed task.
     */
    private Object getTaskResult()
    {
        return taskResult;
    }

    /**
     * Closes the processing dialog.
     */
    private void closeProcessingDialog()
    {
        this.setVisible(false);
        this.dispose();
    }

    /**
     * This class is used to execute the processing task in a separate thread.
     */
    class Runner implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                // Do the thread task.
                taskResult = task.doTask();
            }
            catch (Throwable e)
            {
                taskThrowable = e;
                logger.log(e);
            }

            // Once the task is complete, close the ProcessingDialog.
            processingCompleted = true;
            closeProcessingDialog();
        }
    }

    /**
     * This thread checks the currently running ThreadTask 5 times every second
     * to determine how much progress has been made and updates the progress bar
     * accordingly.  If the current ThreadTask is indeterminate, this thread
     * does no do anything.
     */
    class Checker implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                while (!processingCompleted)
                {
                    if (!jProgressBar.isIndeterminate())
                    {
                        jLabelProcessingText.setText(task.getProgressText());
                        jProgressBar.setValue(task.getProgress());
                        logger.log("Progress: " + task.getProgress());
                    }

                    Thread.sleep(200);
                }
            }
            catch (InterruptedException e)
            {
                logger.log("An exception occurred in ProcessingDialog Checker Thread.", e);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelProcessingText = new javax.swing.JLabel();
        jProgressBar = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabelProcessingText.setText("Processing Text...");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 2, 10);
        getContentPane().add(jLabelProcessingText, gridBagConstraints);

        jProgressBar.setIndeterminate(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 10, 10);
        getContentPane().add(jProgressBar, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelProcessingText;
    private javax.swing.JProgressBar jProgressBar;
    // End of variables declaration//GEN-END:variables

}
