package com.pilog.t8.utilities.components.tabbedpane.tab;

import java.awt.Component;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class TabClosingEvent extends EventObject
{
    private Component tabClosed;
    private boolean cancel;

    public TabClosingEvent(Component source, Component tabClosed)
    {
        super(source);
        this.tabClosed = tabClosed;
        this.cancel = false;
    }

    public Component getTabClosing()
    {
        return tabClosed;
    }

    public void cancel()
    {
        this.cancel = true;
    }

    public boolean isCanceled()
    {
        return this.cancel;
    }
}
