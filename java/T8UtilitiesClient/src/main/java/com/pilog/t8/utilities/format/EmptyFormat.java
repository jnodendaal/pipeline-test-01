package com.pilog.t8.utilities.format;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;

/**
 * Wraps a given <code>Format</code> and adds behavior to convert to/from
 * the empty string. Therefore it holds an <em>empty value</em> that is
 * the counterpart of the empty string. The <code>#format</code> result
 * of the empty value is the empty string, and the <code>#parse</code>
 * result of the empty string is the empty value. In all other cases
 * the formatting and parsing is forwarded to the wrapped Format.<p>
 *
 * Often the empty value is <code>null</code>. For example you can wrap
 * a <code>DateFormat</code> to format <code>null</code> to an empty string
 * and parse the empty string to <code>null</code>. Another example is
 * the mapping of the <code>-1</code> to an empty string using a wrapped
 * <code>NumberFormat</code>.
 *
 * <strong>Examples:</strong><pre>
 * new EmptyFormat(new NumberFormat());
 * new EmptyFormat(new NumberFormat(), -1);
 *
 * new EmptyFormat(DateFormat.getDateInstance());
 * new EmptyFormat(DateFormat.getDateInstance(DateFormat.SHORT));
 * </pre>
 *
 * @author Karsten Lentzsch
 * @version $Revision: 1.10 $
 */
public final class EmptyFormat extends Format
{
    /**
     * Refers to the wrapped Format that is used to forward
     * <code>#format</code> and <code>#parseObject</code>.
     */
    private final Format format;
    /**
     * Holds the object that represents the <em>empty</em> value.
     * The result of formatting this value is the empty string;
     * the result of parsing an empty string is this object.
     */
    private final Object emptyValue;

    /**
     * Constructs an <code>EmptyFormat</code> that wraps the given format
     * to convert <code>null</code> to the empty string and vice versa.
     *
     * @param format The format that handles the standard cases.
     */
    public EmptyFormat(Format format)
    {
        this(format, null);
    }

    /**
     * Constructs an <code>EmptyFormat</code> that wraps the given format
     * to convert the given <code>emptyValue</code> to the empty string
     * and vice versa.
     *
     * @param format       the format that handles non-<code>null</code> values
     * @param emptyValue   the representation of the empty string
     */
    public EmptyFormat(Format format, Object emptyValue)
    {
        this.format = format;
        this.emptyValue = emptyValue;
    }

    /**
     * Constructs an <code>EmptyFormat</code> that wraps the given format
     * to convert the given <code>emptyValue</code> to the empty string
     * and vice versa.
     *
     * @param format       the format that handles non-<code>null</code> values
     * @param emptyValue   the representation of the empty string
     */
    public EmptyFormat(Format format, int emptyValue)
    {
        this.format = format;
        this.emptyValue = Integer.valueOf(emptyValue);
    }

    // Implementing Abstract Behavior ***************************************
    /**
     * Formats an object and appends the resulting text to a given string
     * buffer. If the <code>pos</code> argument identifies a field used by
     * the format, then its indices are set to the beginning and end of
     * the first such field encountered.
     *
     * @param obj    The object to format
     * @param toAppendTo    where the text is to be appended
     * @param pos    A <code>FieldPosition</code> identifying a field
     *               in the formatted text
     * @return       the string buffer passed in as <code>toAppendTo</code>,
     *               with formatted text appended
     * @exception NullPointerException if <code>toAppendTo</code> or
     *            <code>pos</code> is null
     * @exception IllegalArgumentException if the Format cannot format the given
     *            object
     */
    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo,
            FieldPosition pos)
    {
        return equals(obj, emptyValue) ? new StringBuffer() : format.format(obj, toAppendTo, pos);
    }

    /**
     * Parses text from the beginning of the given string to produce an object.
     * The method may not use the entire text of the given string.<p>
     *
     * Unlike super this method returns the <code>emptyValue</code> if the
     * source string is empty and does not throw a <code>ParseException</code>.
     *
     * @param source A <code>String</code> whose beginning should be parsed.
     * @return An <code>Object</code> parsed from the string.
     * @exception ParseException if the beginning of the specified string
     *            cannot be parsed.
     * @see java.text.Format#parseObject(java.lang.String)
     * @see javax.swing.JFormattedTextField#setValue(java.lang.Object)
     */
    @Override
    public Object parseObject(String source) throws ParseException
    {
        return source.length() == 0 ? emptyValue : super.parseObject(source);
    }

    /**
     * Parses text from a string to produce an object.<p>
     *
     * The method attempts to parse text starting at the index given by
     * <code>pos</code>.
     * If parsing succeeds, then the index of <code>pos</code> is updated
     * to the index after the last character used (parsing does not necessarily
     * use all characters up to the end of the string), and the parsed
     * object is returned. The updated <code>pos</code> can be used to
     * indicate the starting point for the next call to this method.
     * If an error occurs, then the index of <code>pos</code> is not
     * changed, the error index of <code>pos</code> is set to the index of
     * the character where the error occurred, and null is returned.
     *
     * @param source A <code>String</code>, part of which should be parsed.
     * @param pos A <code>ParsePosition</code> object with index and error
     *            index information as described above.
     * @return An <code>Object</code> parsed from the string. In case of
     *         error, returns null.
     * @exception NullPointerException if <code>pos</code> is null.
     */
    @Override
    public Object parseObject(String source, ParsePosition pos)
    {
        return format.parseObject(source, pos);
    }

    // Helper Code ************************************************************
    /**
     * Checks and answers if the two objects are both <code>null</code> or equal.
     *
     * @param o1        the first object to compare
     * @param o2        the second object to compare
     * @return boolean  true iff both objects are <code>null</code> or equal
     */
    private boolean equals(Object o1, Object o2)
    {
        return (o1 != null && o2 != null && o1.equals(o2))
                || (o1 == null && o2 == null);
    }
}
