package com.pilog.t8.utilities.components.tabbedpane.tab;

import java.awt.Component;
import java.util.EventObject;

/**
 * @author Bouwer du Preez
 */
public class TabClosedEvent extends EventObject
{
    private Component tabClosed;

    public TabClosedEvent(Component source, Component tabClosed)
    {
        super(source);
        this.tabClosed = tabClosed;
    }

    public Component getTabClosed()
    {
        return tabClosed;
    }
}
