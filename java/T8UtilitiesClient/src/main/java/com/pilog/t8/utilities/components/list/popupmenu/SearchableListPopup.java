package com.pilog.t8.utilities.components.list.popupmenu;

import com.pilog.t8.utilities.components.list.ListPopupMenuListener;
import com.pilog.t8.utilities.components.list.T8List;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.RowFilter;
import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.JXSearchField;
import org.jdesktop.swingx.JXTextField;

/**
 * @author Gavin Boshoff
 */
public class SearchableListPopup<T extends Object> extends JPopupMenu implements KeyListener, T8List
{
    private ListModel<T> model;
    private JXList list;
    private JXTextField textField;
    private final List<ListPopupMenuListener> listeners = new ArrayList<>();

    public SearchableListPopup(List<T> items)
    {
        model = new DefaultListModel<>();

        for (T item : items)
        {
            ((DefaultListModel)model).addElement(item);
        }

        buildUI();
        setPreferredSize(new Dimension(400, 400));
        pack();
    }

    public Object getSelectedValue()
    {
        return list.getSelectedValue();
    }

    @Override
    public Object getSelectedItem()
    {
        return getSelectedValue();
    }

    public Object[] getSelectedValues()
    {
        return list.getSelectedValues();
    }

    public Object[] getSelectedItems()
    {
        return getSelectedValues();
    }

    @Override
    public void removeAllListeners()
    {
        listeners.clear();
    }

    @Override
    public void removePopupListener(ListPopupMenuListener listener)
    {
        listeners.remove(listener);
    }

    @Override
    public void setCellRenderer(ListCellRenderer renderer)
    {
        list.setCellRenderer(renderer);
    }

    @Override
    public void setListModel(ListModel listModel)
    {
        model = listModel;
        list.setModel(model);
    }

    @Override
    public void addPopupListener(ListPopupMenuListener listener)
    {
        listeners.add(listener);
    }

    private void buildUI()
    {
        JScrollPane scrollPane;

        textField = new JXSearchField();
        textField.addKeyListener(this);

        list = new JXList(model, true);
        list.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                super.mouseClicked(e);
                if (e.getClickCount() == 2)
                {
                    setVisible(false);
                    closeParentPopupMenu(SearchableListPopup.this);
                    for (ListPopupMenuListener listPopupMenuListener : listeners)
                    {
                        listPopupMenuListener.popupItemSelected(getSelectedItem());
                    }
                }
            }
        });

        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "TriggerItemsSelected");
        getActionMap().put("TriggerItemsSelected", new AbstractAction("TriggerItemsSelected")
                        {
                            @Override
                            public void actionPerformed(ActionEvent e)
                            {
                                setVisible(false);
                                closeParentPopupMenu(SearchableListPopup.this);
                                listeners.forEach(listener -> listener.popupItemsSelected(getSelectedItems()));
                            }
                        });

        setLayout(new BorderLayout(2, 2));
        add(textField, BorderLayout.NORTH);
        scrollPane = new JScrollPane(list);
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        add(scrollPane, BorderLayout.CENTER);
    }

    private void closeParentPopupMenu(Component component)
    {
        Component invoker;

        if (component instanceof JMenu)
        {
            invoker = component.getParent();
        }
        else if (component instanceof JMenuItem)
        {
            invoker = component.getParent();
        }
        else if (component instanceof JPopupMenu)
        {
            invoker = ((JPopupMenu)component).getInvoker();
        }
        else return;

        if (!((invoker instanceof JPopupMenu) || (invoker instanceof JMenuItem) || (invoker instanceof JMenu)))
        {
            component.setVisible(false);
        }
        else
        {
            closeParentPopupMenu(invoker);
        }
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        list.setRowFilter(new RowFilter<ListModel<String>, Integer>()
        {
            @Override
            public boolean include(RowFilter.Entry<? extends ListModel<String>, ? extends Integer> entry)
            {
                String searchValue;

                searchValue = textField.getText();
                if (Strings.isNullOrEmpty(searchValue))
                {
                    return true;
                }

                // Now we know we have something to search for
                String entryStringValue;

                entryStringValue = entry.getStringValue(0);
                if (!Strings.isNullOrEmpty(entryStringValue))
                {
                    return entryStringValue.toUpperCase().contains(searchValue.toUpperCase());
                } else return false;
            }
        });
    }
}
