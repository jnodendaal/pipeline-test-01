package com.pilog.t8.utilities.components.filechoosers;

import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;

/**
 * @author Bouwer du Preez
 */
public class BasicFileChooser
{
    public enum FileSelectionMode
    {
        FILES(JFileChooser.FILES_ONLY),
        DIRECTORIES(JFileChooser.DIRECTORIES_ONLY),
        FILES_AND_DIRECTORIES(JFileChooser.FILES_AND_DIRECTORIES);

        private int jFileChooserType;

        private FileSelectionMode(int jFileChooserType)
        {
            this.jFileChooserType = jFileChooserType;
        }

        public int getjFileChooserType()
        {
            return jFileChooserType;
        }
    }


    /**
     * Displays a file chooser dialog that enables the user to select files of
     * the specified type to open.
     *
     * @param parentComponent The display parent of this pop-up dialog.
     * @param fileExtension The file extension to which the selection will be
     * limited.
     * @param fileDescription A description of the file type that the user must
     * select.
     * @return The canonical file path to the selected file or null if the user
     * cancelled the selection.
     */
    public static final String getLoadFilePath(Component parentComponent, String fileExtension, String fileDescription) throws Exception
    {
        return getLoadFilePath(parentComponent, fileExtension, fileDescription, null);
    }

    /**
     * Displays a file chooser dialog that enables the user to select files of
     * the specified type to open.
     *
     * @param parentComponent The display parent of this pop-up dialog.
     * @param fileExtension The file extension to which the selection will be
     * limited.
     * @param fileDescription A description of the file type that the user must
     * select.
     * @param selectedFile The file to be selected by default when the dialog
     * opens.
     * @return The canonical file path to the selected file or null if the user
     * cancelled the selection.
     */
    public static final String getLoadFilePath(Component parentComponent, String fileExtension, String fileDescription, File selectedFile) throws Exception
    {
        return getLoadFilePath(parentComponent, fileExtension, fileDescription, selectedFile, FileSelectionMode.FILES_AND_DIRECTORIES);
    }
    /**
     * Displays a file chooser dialog that enables the user to select files of
     * the specified type to open.
     *
     * @param parentComponent The display parent of this pop-up dialog.
     * @param fileExtension The file extension to which the selection will be
     * limited.
     * @param fileDescription A description of the file type that the user must
     * select.
     * @param selectedFile The file to be selected by default when the dialog
     * opens.
     * @return The canonical file path to the selected file or null if the user
     * cancelled the selection.
     */
    public static final String getLoadFilePath(Component parentComponent, String fileExtension, String fileDescription, File selectedFile, FileSelectionMode fileSelectionMode) throws Exception
    {
        JFileChooser fileChooser;
        int returnValue;

        fileChooser = new JFileChooser();
        fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        fileChooser.setFileSelectionMode(fileSelectionMode.getjFileChooserType());
        if(selectedFile != null)
        {
            if(selectedFile.isDirectory()) fileChooser.setCurrentDirectory(selectedFile);
            else fileChooser.setSelectedFile(selectedFile);
        }
        if (fileExtension != null)
        {
            if (!fileExtension.startsWith(".")) fileExtension = "." + fileExtension;
            fileChooser.addChoosableFileFilter(new GenericFileFilter(fileExtension, fileDescription));
        }

        returnValue = fileChooser.showOpenDialog(parentComponent);
        if (returnValue == JFileChooser.APPROVE_OPTION)
        {
            String filePath;

            filePath = fileChooser.getSelectedFile().getCanonicalPath();
            if ((filePath != null) && (fileExtension != null) && (!filePath.toUpperCase().endsWith(fileExtension.toUpperCase())))
            {
                filePath += fileExtension;
            }

            return filePath;
        }
        else
        {
            return null;
        }
    }

    /**
     * Displays a file chooser dialog that enables the user to select the path
     * and name of a file to which data will be saved.
     *
     * @param parentComponent The display parent of this pop-up dialog.
     * @param fileExtension The file extension to which the selection will be
     * limited.
     * @param fileDescription A description of the file type extension that will
     * be added to the file name if the user doesn't add it.
     * @return The canonical file path to the selected file or null if the user
     * cancelled the selection.
     */
    public static final String getSaveFilePath(Component parentComponent, String fileExtension, String fileDescription) throws Exception
    {
        return getSaveFilePath(parentComponent, fileExtension, fileDescription, null);
    }

    /**
     * Displays a file chooser dialog that enables the user to select the path
     * and name of a file to which data will be saved.
     *
     * @param parentComponent The display parent of this pop-up dialog.
     * @param fileExtension The file extension to which the selection will be
     * limited.
     * @param fileDescription A description of the file type extension that will
     * be added to the file name if the user doesn't add it.
     * @param selectedFile The file to be selected by default when the dialog
     * opens.
     * @return The canonical file path to the selected file or null if the user
     * cancelled the selection.
     */
    public static final String getSaveFilePath(Component parentComponent, String fileExtension, String fileDescription, File selectedFile) throws Exception
    {
        return getSaveFilePath(parentComponent, fileExtension, fileDescription, selectedFile, FileSelectionMode.FILES_AND_DIRECTORIES);
    }

    /**
     * Displays a file chooser dialog that enables the user to select the path
     * and name of a file to which data will be saved.
     *
     * @param parentComponent The display parent of this pop-up dialog.
     * @param fileExtension The file extension to which the selection will be
     * limited.
     * @param fileDescription A description of the file type extension that will
     * be added to the file name if the user doesn't add it.
     * @param selectedFile The file to be selected by default when the dialog
     * opens.
     * @param fileSelectionMode
     * @return The canonical file path to the selected file or null if the user
     * cancelled the selection.
     */
    public static final String getSaveFilePath(Component parentComponent, String fileExtension, String fileDescription, File selectedFile, FileSelectionMode fileSelectionMode) throws Exception
    {
        JFileChooser fileChooser;
        int returnValue;

        fileChooser = new JFileChooser();
        fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        fileChooser.setFileSelectionMode(fileSelectionMode.getjFileChooserType());
        if(selectedFile != null)
        {
            fileChooser.setSelectedFile(selectedFile);
        }
        if (fileExtension != null)
        {
            if (!fileExtension.startsWith(".")) fileExtension = "." + fileExtension;
            fileChooser.addChoosableFileFilter(new GenericFileFilter(fileExtension, fileDescription));
        }

        returnValue = fileChooser.showSaveDialog(parentComponent);
        if (returnValue == JFileChooser.APPROVE_OPTION)
        {
            String filePath;

            filePath = fileChooser.getSelectedFile().getCanonicalPath();
            if ((filePath != null) && (fileExtension != null) && (!filePath.toUpperCase().endsWith(fileExtension.toUpperCase())))
            {
                filePath += fileExtension;
            }
            return filePath;
        }
        else
        {
            return null;
        }
    }

    /**
     * This class extends FileFilter and is used in a JFileChooser to filter out
     * all files except those using the specified extension.
     */
    private static class GenericFileFilter extends javax.swing.filechooser.FileFilter
    {
        private String fileExtension;
        private String fileDescription;

        public GenericFileFilter(String fileExtension, String fileDescription)
        {
            this.fileExtension = fileExtension;
            this.fileDescription = fileDescription;
        }

        @Override
        public boolean accept(File f)
        {
            if (f.isDirectory())
            {
                return true;
            }
            else
            {
                return (f.getName().toUpperCase().endsWith(fileExtension.toUpperCase()));
            }
        }

        @Override
        public String getDescription()
        {
            return fileDescription;
        }
    }
}
