package com.pilog.t8.utilities.components.cellrenderers;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

/**
 * @author Bouwer.duPreez
 */
public class CellRendererUtilities
{
    /**
     * This method uses the available cell renderer to determine the maximum
     * width for the values in each of the columns in the supplied table and
     * then to resize the columns accordingly so that all values are displayed
     * in full.
     *
     * @param table The table for which columns will be resized.
     * @param minimumWidth  The minimum width to which columns will be resized.
     */
    public static void autoSizeTableColumns(JTable table, int minimumWidth)
    {
        TableColumnModel columnModel;

        columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            int width;

            width = minimumWidth;
            for (int rowIndex = 0; rowIndex < table.getRowCount(); rowIndex++)
            {
                TableCellRenderer renderer;
                Component rendererComponent;

                renderer = table.getCellRenderer(rowIndex, column);
                rendererComponent = table.prepareRenderer(renderer, rowIndex, column);
                width = Math.max(rendererComponent.getPreferredSize().width + 5, width);
            }

            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }

    /**
     * This method queries all the cell renderers on the table and checks each one
     * to determine the minimum height of the table rows to determine
     * the minimum height of the rows for the table that will satisfy all table
     * cell renderers
     * @param table The table to check
     * @param minimumHeight The minimum height that will be applied to table rows
     */
    public static void autoSizeTableRows(JTable table, int minimumHeight)
    {
        int height;

        height = minimumHeight;
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            for (int rowIndex = 0; rowIndex < table.getRowCount(); rowIndex++)
            {
                TableCellRenderer renderer;
                Component rendererComponent;

                renderer = table.getCellRenderer(rowIndex, column);
                rendererComponent = table.prepareRenderer(renderer, rowIndex, column);
                height = Math.max(rendererComponent.getPreferredSize().height + 5, height);

                table.setRowHeight(rowIndex, height);
            }
        }
    }
}
