package com.pilog.t8.utilities.components.tooltip;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolTipUI;

/**
 * @author Hennie Brink
 */
public class T8TooltipUI extends BasicToolTipUI
{
    static T8TooltipUI sharedInstance = new T8TooltipUI();

    public static ComponentUI createUI(JComponent c) {
        return sharedInstance;
    }

    public T8TooltipUI()
    {
        super();
    }

    @Override
    public void paint(Graphics g, JComponent c)
    {
        Graphics2D graphics2D;

        graphics2D = (Graphics2D) g.create();

        graphics2D.setPaint(new GradientPaint(new Point(c.getWidth() / 2, 0), new Color(255, 255, 255), new Point(c.getWidth() / 2, c.getHeight()), new Color(210, 210, 255)));

        graphics2D.fill(new Rectangle(0, 0, c.getWidth(), c.getHeight()));

        super.paint(graphics2D, c);

        graphics2D.dispose();
    }
}
