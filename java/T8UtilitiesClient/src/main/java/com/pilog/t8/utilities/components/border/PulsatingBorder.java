package com.pilog.t8.utilities.components.border;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.geom.Rectangle2D;
import java.util.concurrent.TimeUnit;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.PropertySetter;
import org.jdesktop.core.animation.timing.TimingTargetAdapter;

/**
 * @author Gavin Boshoff
 */
public class PulsatingBorder implements Border
{
    private float thickness = 0.0f;
    private final JComponent comp;
    private final Paint color;

    public PulsatingBorder(Paint color, JComponent component)
    {
        this.color = color;
        this.comp = component;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height)
    {
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        Rectangle2D r = new Rectangle2D.Double(x, y, width - 1, height - 1);
        g2.setStroke(new BasicStroke(2.0f * getThickness()));

        g2.setComposite(AlphaComposite.SrcOver.derive(getThickness()));

        g2.setPaint(color);

        g2.draw(r);

        g2.dispose();
    }

    @Override
    public Insets getBorderInsets(Component c)
    {
        return new Insets(0, 0, 0, 0);
    }

    @Override
    public boolean isBorderOpaque()
    {
        return false;
    }

    public float getThickness()
    {
        return thickness;
    }

    public void setThickness(float thickness)
    {
        this.thickness = thickness;
        comp.repaint();
    }

    public static PulsatingBorder install(JComponent component, Paint color)
    {
        PulsatingBorder border = new PulsatingBorder(color, component);

        component.setBorder(new CompoundBorder(border, component.getBorder()));

        return border;
    }

    public static void installWithFocusAnimation(JComponent component, Paint color)
    {
        final PulsatingBorder border = install(component, color);
        final Animator animator = new Animator.Builder()
                .addTarget(PropertySetter.getTarget(border, "thickness", 0.0f, 1.0f))
                .addTarget(
                        new TimingTargetAdapter()
                        {
                            @Override
                            public void begin(Animator source)
                            {
                                border.setThickness(0f);
                            }

                            @Override
                            public void end(Animator source)
                            {
                                border.setThickness(0f);
                            }
                        })
                .setRepeatBehavior(Animator.RepeatBehavior.REVERSE)
                .setRepeatCount(Animator.INFINITE)
                .setDuration(1500, TimeUnit.MILLISECONDS)
                .build();

        component.addFocusListener(new FocusAdapter()
        {
            @Override
            public void focusGained(FocusEvent e)
            {
                if(!animator.isRunning()) animator.start();
            }

            @Override
            public void focusLost(FocusEvent e)
            {
                if(animator.isRunning()) animator.stop();
            }
        });
    }
}
