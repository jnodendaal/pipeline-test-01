package com.pilog.t8.utilities.components.textautocompleter;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

/**
 * @author Bouwer du Preez
 */
public abstract class TextAutoCompleter
{
    private static final T8Logger LOGGER = T8Log.getLogger(TextAutoCompleter.class);

    protected JList list;
    protected JTextComponent textComponent;
    private final JScrollPane scrollPane;
    private JPopupMenu popup;
    private KeyListener keyListener;
    private ListMouseListener mouseListener;
    private int updateDelay;
    private Updater updater;
    private boolean enabled;

    private static final String AUTOCOMPLETER = "AUTOCOMPLETER";

    // Action performed when the user accepts one of the suggested options from the list.
    private static final Action ACCEPT_ACTION = new AbstractAction()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (enabled)
            {
                JComponent textField;
                TextAutoCompleter completer;

                textField = (JComponent)e.getSource();
                completer = (TextAutoCompleter)textField.getClientProperty(AUTOCOMPLETER);
                completer.popup.setVisible(false);
                completer.listItemSelected(completer.list.getSelectedValue());
            }
        }
    };

    // Action performed when the user types the key that triggers list popup.
    private static final Action SHOW_ACTION = new AbstractAction()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (enabled)
            {
                JComponent textField;
                TextAutoCompleter completer;

                textField = (JComponent)e.getSource();
                completer = (TextAutoCompleter)textField.getClientProperty(AUTOCOMPLETER);
                if (textField.isEnabled())
                {
                    if (completer.popup.isVisible())
                    {
                        completer.selectNextPossibleValue();
                    }
                    else
                    {
                        completer.showPopup();
                    }
                }
            }
        }
    };

    // Action performed when the user moves the popup list caret to the previous index using the keyboard up arrow.
    private static final Action UP_ACTION = new AbstractAction()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (enabled)
            {
                JComponent textField;
                TextAutoCompleter completer;

                textField = (JComponent) e.getSource();
                completer = (TextAutoCompleter)textField.getClientProperty(AUTOCOMPLETER);
                if (textField.isEnabled())
                {
                    if (completer.popup.isVisible())
                    {
                        completer.selectPreviousPossibleValue();
                    }
                }
            }
        }
    };

    // Action performed when the user hides the list popup.
    private static final Action HIDE_POPUP_ACTION = new AbstractAction()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (enabled)
            {
                JComponent textField;
                TextAutoCompleter completer;

                textField = (JComponent) e.getSource();
                completer = (TextAutoCompleter)textField.getClientProperty(AUTOCOMPLETER);
                if (textField.isEnabled())
                {
                    completer.popup.setVisible(false);
                }
            }
        }
    };

    public TextAutoCompleter(JTextComponent txtComponent)
    {
        this.list = new JList();
        this.popup = new JPopupMenu();
        this.textComponent = txtComponent;
        this.textComponent.putClientProperty(AUTOCOMPLETER, this);
        this.scrollPane = new JScrollPane(list);
        this.scrollPane.setBorder(null);
        this.list.setFocusable(false);
        this.scrollPane.getVerticalScrollBar().setFocusable(false);
        this.scrollPane.getHorizontalScrollBar().setFocusable(false);
        this.popup.setBorder(BorderFactory.createLineBorder(Color.black));
        this.popup.add(scrollPane);
        this.popup.setFocusable(false);
        this.mouseListener = new ListMouseListener();
        this.list.addMouseListener(mouseListener);
        this.updateDelay = 0;
        this.enabled = true;

        // For text fields, we register and action to show the popup when the down arrow is pressed.  For other text components, Ctrl-Space is used.
        if (this.textComponent instanceof JTextField)
        {
            this.keyListener = new TextComponentKeyListener();
            this.textComponent.registerKeyboardAction(SHOW_ACTION, KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), JComponent.WHEN_FOCUSED);
            this.textComponent.addKeyListener(keyListener);
        }
        else
        {
            this.textComponent.registerKeyboardAction(SHOW_ACTION, KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.CTRL_MASK), JComponent.WHEN_FOCUSED);
        }

        this.textComponent.registerKeyboardAction(UP_ACTION, KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), JComponent.WHEN_FOCUSED);
        this.textComponent.registerKeyboardAction(HIDE_POPUP_ACTION, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_FOCUSED);
        this.popup.addPopupMenuListener(new ListPopupMenuListener());
        this.list.setRequestFocusEnabled(false);
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void hidePopup()
    {
        popup.setVisible(false);
    }

    private void showPopup()
    {
        popup.setVisible(false);
        if (enabled && textComponent.isEnabled() && updateListData() && (list.getModel().getSize() != 0))
        {
            int size;
            int xPosition;

            // Once the popup is shown, we register a new action to allow the user to select one of the options from the list.
            textComponent.registerKeyboardAction(ACCEPT_ACTION, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_FOCUSED);

            // Make sure only 10 items in the list are visible so that the list does not get too large.
            size = list.getModel().getSize();
            list.setVisibleRowCount(size < 10 ? size : 10);

            // Calculate an x coordinate for the list popup so that it is near to the current text caret.
            try
            {
                int position;

                position = Math.min(textComponent.getCaret().getDot(), textComponent.getCaret().getMark());
                if (position > 0)
                {
                    xPosition = textComponent.getUI().modelToView(textComponent, position).x;
                }
                else xPosition = 0;
            }
            catch (BadLocationException e)
            {
                // This should never happen so no need to handle the exception.
                LOGGER.log(e);
                xPosition = 0;
            }

            // Display the the list popup.
            popup.show(textComponent, xPosition, textComponent.getHeight());
            if (size > 0) list.setSelectedIndex(0);
        }
        else
        {
            // Hide the popup.
            popup.setVisible(false);
        }

        // After the popup has been shown or hidden, request focus on the text input.
        textComponent.requestFocus();

        // Call after-update hook.
        listDataUpdated();
    }

    public Object getSelectedValue()
    {
        return list.getSelectedValue();
    }

    /**
     * Selects the next item in the list. It won't change the selection if the
     * currently selected item is already the last item.
     */
    protected void selectNextPossibleValue()
    {
        int selectedIndex;

        selectedIndex = list.getSelectedIndex();
        if (selectedIndex < (list.getModel().getSize() - 1))
        {
            list.setSelectedIndex(selectedIndex + 1);
            list.ensureIndexIsVisible(selectedIndex + 1);
        }
    }

    /**
     * Selects the previous item in the list. It won't change the selection if
     * the currently selected item is already the first item.
     */
    protected void selectPreviousPossibleValue()
    {
        int selectedIndex;

        selectedIndex = list.getSelectedIndex();
        if (selectedIndex > 0)
        {
            list.setSelectedIndex(selectedIndex - 1);
            list.ensureIndexIsVisible(selectedIndex - 1);
        }
    }

    private class ListPopupMenuListener implements PopupMenuListener
    {
        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e)
        {
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
        {
            // When the list popup is hidden, we unregister the 'Accept' action so that the triggering key can be used on the text component.
            textComponent.unregisterKeyboardAction(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent e)
        {
        }
    }

    private class TextComponentKeyListener extends KeyAdapter
    {
        @Override
        public void keyReleased(KeyEvent e)
        {
            if (enabled)
            {
                int keyCode;

                keyCode = e.getKeyCode();
                if ((keyCode != KeyEvent.VK_DOWN) && (keyCode != KeyEvent.VK_UP) && (keyCode != KeyEvent.VK_ENTER) && (keyCode != KeyEvent.VK_ESCAPE) && (keyCode != KeyEvent.VK_SHIFT))
                {
                    synchronized (this)
                    {
                        if ((updater == null) || (!updater.isActive()))
                        {
                            updater = new Updater(TextAutoCompleter.this, updateDelay);
                            updater.start();
                        }
                        else
                        {
                            updater.delay(updateDelay);
                        }
                    }
                }
            }
        }
    }

    public void setUpdateDelay(int delay)
    {
        this.updateDelay = delay;
    }

    private synchronized void update()
    {
        LOGGER.log("Updating autocompleter list...");
        updater = null;
        showPopup();
    }

    private JTextComponent getTextComponent()
    {
        return textComponent;
    }

    private class ListMouseListener extends MouseAdapter
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.getClickCount() == 2)
            {
                popup.setVisible(false);
                listItemSelected(list.getSelectedValue());
            }
        }
    }

    private static class Updater extends Thread
    {
        private final TextAutoCompleter completer;
        private volatile long delay = 0;
        private volatile boolean active;

        private Updater(TextAutoCompleter completer, long delay)
        {
            this.completer = completer;
            this.delay = delay;
            this.active = true;
        }

        public synchronized void delay(long delay)
        {
            this.delay = delay;
        }

        public synchronized boolean isActive()
        {
            return active;
        }

        @Override
        public void run()
        {
            // While the delay is set, wait.
            while (delay > 0)
            {
                try
                {
                    long timeToWait;

                    // Reset the delay.
                    timeToWait = delay;
                    synchronized (this)
                    {
                        delay = 0;
                    }

                    // Wait the specified amount of time before continuing.
                    Thread.sleep(timeToWait);
                }
                catch (InterruptedException e)
                {
                    LOGGER.log(e);

                    // Reset the delay.
                    synchronized (this)
                    {
                        delay = 0;
                    }
                }
            }

            // Immediately set the active flag to false so that this updater thread will no longer be used.
            active = false;

            // Now do the update.
            if (completer.getTextComponent().hasFocus())
            {
                completer.update();
            }
        }
    }

    /**
     * Updates list model depending on the data in the textfield.  This method
     * must return false if the list model is empty, so that no popup is
     * displayed.
     * @return True if valid options are available in the list model after
     * update.
     */
    protected abstract boolean updateListData();

    /**
     * Hook called after list data has been updated and the popup has either been displayed
     * with the latest data or hidden if empty.
     */
    protected abstract void listDataUpdated();

    /**
     * User has selected some item in the list.  Update the textfield accordingly.
     * @param selectedItem The item selected.
     */
    protected abstract void listItemSelected(Object selectedItem);
}
