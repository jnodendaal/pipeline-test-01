package com.pilog.t8.utilities.components.cellrenderers;

import java.awt.Component;
import java.text.Format;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author Hennie Brink
 */
public class FormattedTableCellRenderer extends DefaultTableCellRenderer
{
    private final Format formatter;

    public FormattedTableCellRenderer(Format formatter)
    {
        this.formatter = formatter;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus, int row,
                                                   int column)
    {
        return super.getTableCellRendererComponent(table, formatter.format(value), isSelected, hasFocus, row, column);
    }
}
