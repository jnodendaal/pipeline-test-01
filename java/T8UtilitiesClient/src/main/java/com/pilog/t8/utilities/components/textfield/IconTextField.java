package com.pilog.t8.utilities.components.textfield;

import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.Icon;
import javax.swing.JFormattedTextField;
import javax.swing.border.Border;

/**
 * @author Bouwer du Preez
 */
public class IconTextField extends JFormattedTextField
{
    private Insets insets;
    private Icon icon;
    private IconAlignment iconAlignment;

    public enum IconAlignment {LEFT, RIGHT};

    public IconTextField()
    {
        super();
        this.icon = null;
        this.iconAlignment = IconAlignment.LEFT;
        setDefaultInsets();
    }

    private void setDefaultInsets()
    {
        Border border = getBorder();
        if (border != null)
        {
            this.insets = border.getBorderInsets(this);
        }
        else insets = new Insets(0, 0, 0, 0);
    }

    public void setIcon(Icon icon)
    {
        this.icon = icon;
    }

    public Icon getIcon()
    {
        return this.icon;
    }

    public IconAlignment getIconAlignment()
    {
        return iconAlignment;
    }

    public void setIconAlignment(IconAlignment iconAlignment)
    {
        this.iconAlignment = iconAlignment;
    }

    @Override
    protected void paintComponent(Graphics g)
    {

        // Do the normal painting rountine.
        super.paintComponent(g);

        // If the icon is set, paint it.
        if (this.icon != null)
        {
            int iconWidth;
            int iconHeight;

            iconWidth = icon.getIconWidth();
            iconHeight = icon.getIconHeight();

            if (iconAlignment == IconAlignment.LEFT)
            {
                int leftMargin;
                int iconX;
                int iconY;

                iconX = insets.left + 2;
                leftMargin = iconX + iconWidth + 2;
                iconY = (this.getHeight() - iconHeight) / 2;
                icon.paintIcon(this, g, iconX, iconY);

                // Set the new marging to take the icon into account.
                setMargin(new Insets(2, leftMargin, 2, 2));
            }
            else
            {
                int rightMargin;
                int iconX;
                int iconY;

                iconX = getWidth() - insets.right - 2 - iconWidth;
                rightMargin = getWidth() - iconX + 2;
                iconY = (this.getHeight() - iconHeight) / 2;
                icon.paintIcon(this, g, iconX, iconY);

                // Set the new marging to take the icon into account.
                setMargin(new Insets(2, 2, 2, rightMargin));
            }
        }
    }
}
