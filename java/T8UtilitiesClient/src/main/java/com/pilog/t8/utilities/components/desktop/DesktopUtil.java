/**
 * Created on 18 Jun 2015, 10:52:21 AM
 */
package com.pilog.t8.utilities.components.desktop;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;

/**
 * @author Gavin Boshoff
 */
public class DesktopUtil
{
    private static final T8Logger logger = T8Log.getLogger(DesktopUtil.class);

    /**
     * Opens the default browser on the client machine. This will only work if
     * the current machine supports the action.
     *
     * @param uri The {@code URI} to be opened in the browser
     *
     * @return {@code true} if the call to the client desktop was successful.
     *      {@code false} otherwise
     *
     * @throws IOException If there is an exception while attempting to open the
     *      URI in the browser
     */
    public static boolean openWebpage(URI uri) throws IOException
    {
        Desktop desktop;

        desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE))
        {
            desktop.browse(uri);
            return true;
        } else logger.log(T8Logger.Level.ERROR, "User environment does not support opening browser for URL.");
        return false;
    }

    /**
     * Opens the default program on the client machine to view the specified
     * file. This will only work if the current machine supports this action and
     * has a program available to open the specified file.
     *
     * @param filePath The full file path of the file to be opened
     *
     * @return {@code true} if the call to the client desktop was successful.
     *      {@code false} otherwise
     *
     * @throws IOException If there is an error while attempting to call for the
     *      file to be opened
     */
    public static boolean openFile(String filePath) throws IOException
    {
        return openFile(new File(filePath));
    }

    /**
     * @see #openFile(java.lang.String)
     *
     * @param file The {@code File} to be opened on the client machine
     *
     * @return {@code true} if the call to the client desktop was successful.
     *      {@code false} otherwise
     *
     * @throws IOException If there is an error while attempting to call for the
     *      file to be opened
     */
    public static boolean openFile(File file) throws IOException
    {
        Desktop desktop;

        desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.OPEN))
        {
            desktop.open(file);
            return true;
        } else logger.log(T8Logger.Level.ERROR, "User environment does not support opening file : " + file.getPath());
        return false;
    }

    private DesktopUtil() {}
}