package com.pilog.t8.utilities.components.table.header;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.basic.BasicTableHeaderUI;
import javax.swing.table.TableColumnModel;

/**
 * @author Bouwer du Preez
 */
public class EditableHeaderUI extends BasicTableHeaderUI
{
    public EditableHeaderUI()
    {
    }
    
    @Override
    protected MouseInputListener createMouseInputListener()
    {
        return new MouseInputHandler((EditableHeader)header);
    }

    public class MouseInputHandler extends BasicTableHeaderUI.MouseInputHandler
    {
        private Component dispatchComponent;
        protected EditableHeader header;

        public MouseInputHandler(EditableHeader header)
        {
            this.header = header;
        }

        private void setDispatchComponent(MouseEvent e)
        {
            Component editorComponent;
            Point p;
            Point p2;

            editorComponent = header.getEditorComponent();
            p = e.getPoint();
            p2 = SwingUtilities.convertPoint(header, p, editorComponent);
            dispatchComponent = SwingUtilities.getDeepestComponentAt(editorComponent, p2.x, p2.y);
        }

        private boolean repostEvent(MouseEvent e)
        {
            if (dispatchComponent != null)
            {
                MouseEvent e2;
                
                e2 = SwingUtilities.convertMouseEvent(header, e, dispatchComponent);
                dispatchComponent.dispatchEvent(e2);
                return true;
            }
            else return false;
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            if (SwingUtilities.isLeftMouseButton(e))
            {
                super.mousePressed(e);
                if (header.getResizingColumn() == null)
                {
                    Point p = e.getPoint();
                    TableColumnModel columnModel = header.getColumnModel();
                    int index = columnModel.getColumnIndexAtX(p.x);
                    if (index != -1)
                    {
                        if (header.editCellAt(index, e))
                        {
                            setDispatchComponent(e);
                            repostEvent(e);
                        }
                    }
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            super.mouseReleased(e);
            if (SwingUtilities.isLeftMouseButton(e))
            {
                repostEvent(e);
                dispatchComponent = null;
            }
        }
    }
}