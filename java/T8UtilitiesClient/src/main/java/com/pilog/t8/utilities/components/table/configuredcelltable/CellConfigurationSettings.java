package com.pilog.t8.utilities.components.table.configuredcelltable;

import java.awt.Color;
import java.awt.Font;

/**
 * @author Bouwer du Preez
 */
public class CellConfigurationSettings
{
    private Font font;
    private Color foregroundColor;
    private Color backgroundColor;
    private CellSpan span;
    private boolean visible;

    public CellConfigurationSettings(CellSpan span)
    {
        this.span = span;
        this.visible = true;
        this.backgroundColor = null;
        this.foregroundColor = null;
        this.font = null;
    }
    
    public Font getFont()
    {
        return font;
    }

    public void setFont(Font font)
    {
        this.font = font;
    }

    public Color getForegroundColor()
    {
        return foregroundColor;
    }

    public void setForegroundColor(Color foregroundColor)
    {
        this.foregroundColor = foregroundColor;
    }

    public Color getBackgroundColor()
    {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor)
    {
        this.backgroundColor = backgroundColor;
    }

    public CellSpan getSpan()
    {
        return span;
    }

    public void setSpan(CellSpan span)
    {
        this.span = span;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }
}
