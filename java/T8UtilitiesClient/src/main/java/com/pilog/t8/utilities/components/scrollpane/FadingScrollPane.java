package com.pilog.t8.utilities.components.scrollpane;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

/**
 * @author Hennie Brink
 */
public class FadingScrollPane extends JScrollPane
{

    public FadingScrollPane(Component view, int vsbPolicy, int hsbPolicy)
    {
        super(view, vsbPolicy, hsbPolicy);
        getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
    }

    public FadingScrollPane(Component view)
    {
        super(view);
        getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
    }

    public FadingScrollPane(int vsbPolicy, int hsbPolicy)
    {
        super(vsbPolicy, hsbPolicy);
        getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
    }

    public FadingScrollPane()
    {
        getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);

        Graphics2D g2;
        int scrollbarHeight;
        int scrollbarWidth;

        g2 = (Graphics2D) g.create();
        scrollbarHeight = getHorizontalScrollBar().isVisible() ? getHorizontalScrollBar().getWidth() : 0;
        scrollbarWidth = getVerticalScrollBar().isVisible() ? getVerticalScrollBar().getWidth() : 0;


        int startX = getX() + (getWidth() / 2);
        int endX = startX;
        //If the viewport y is larger then component why the user is scrolling down, so indicate that there is content up
        if(getViewport().getViewRect().getY() > getY())
        {
            int startY = getY() - 15;
            int endy = startY + 25;

            g2.setPaint(new LinearGradientPaint(startX, startY, endX, endy, new float[]{ 0f, 1f}, new Color[]{Color.BLACK, new Color(0, 0, 0, 0)}));

            g2.fillRect(getX(), getY(), getWidth() - scrollbarWidth, getHeight());
        }

        //If the viewport y + height is smaller than the view size then there is still scrolling space left
        if((getViewport().getViewRect().getY() + getViewport().getViewRect().getHeight()) < getViewport().getViewSize().getHeight())
        {
            int startY = getY() + getHeight() + 10;
            int endy = getY() + getHeight() - 10;

            g2.setPaint(new LinearGradientPaint(startX, startY, endX, endy, new float[]{0f, 1f}, new Color[]{Color.BLACK, new Color(0, 0, 0, 0)}));

            g2.fillRect(getX(), getY(), getWidth() - scrollbarWidth, getHeight() - scrollbarHeight);
        }

        g2.dispose();
    }
}
