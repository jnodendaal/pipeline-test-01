package com.pilog.t8.utilities.components.celleditor;

import com.pilog.t8.utilities.components.combobox.SearchableComboBox;
import java.awt.Component;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * @author hennie.brink@pilog.co.za
 * @param <T>
 */
public class SearchableComboBoxTableCellEditor<T extends Object> extends AbstractCellEditor implements TableCellEditor
{
    private SearchableComboBox jComboBoxAttributes;
    private boolean enforceNull;

    public SearchableComboBoxTableCellEditor(boolean enforceNull)
    {
        this.enforceNull = enforceNull;
        this.jComboBoxAttributes = new SearchableComboBox<T>();
    }

    public SearchableComboBoxTableCellEditor(boolean enforceNull, List<T> items)
    {
        this(enforceNull);
        setItems(items);
    }

    public SearchableComboBoxTableCellEditor(boolean enforceNull, T... items)
    {
        this(enforceNull);
        setItems(Arrays.asList(items));
    }

    public final void setItems(List<T> items)
    {
        jComboBoxAttributes.removeAllItems();
        if (items != null)
        {
            if ((enforceNull) && (!items.contains(null))) jComboBoxAttributes.addItem(null);
            for (Object item : items) jComboBoxAttributes.addItem(item);
        }
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        jComboBoxAttributes.setSelectedItem(value);
        return jComboBoxAttributes;
    }

    @Override
    public Object getCellEditorValue()
    {
        return jComboBoxAttributes.getSelectedItem();
    }
}
