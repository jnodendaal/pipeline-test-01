package com.pilog.t8.utilities.components.celleditor;

import java.awt.Component;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * A simple table cell editor that can be used to enable selection of one of the
 * supplied values via a combo box control.  The values of the editor can be
 * changed at any time.  The editor allows explicity specification of whether or
 * not the editor must provide a null value for selection regardless of the list
 * of items supplied to the editor.
 *
 * @author Bouwer du Preez
 */
public class ComboBoxTableCellEditor extends AbstractCellEditor implements TableCellEditor
{
    private final JComboBox<Object> jComboBoxAttributes;
    private boolean enforceNull;

    public ComboBoxTableCellEditor(boolean enforceNull)
    {
        this(enforceNull, (List<Object>)null);
    }

    public ComboBoxTableCellEditor(boolean enforceNull, Object... items)
    {
        this(enforceNull, Arrays.asList(items));
    }

    public ComboBoxTableCellEditor(boolean enforceNull, List<? extends Object> items)
    {
        this.jComboBoxAttributes = new JComboBox<>();
        setItems(items);
    }

    private void setItems(List<? extends Object> items)
    {
        jComboBoxAttributes.removeAllItems();
        if (items != null)
        {
            if ((enforceNull) && (!items.contains(null))) jComboBoxAttributes.addItem(null);
            for (Object item : items) jComboBoxAttributes.addItem(item);
        }
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        jComboBoxAttributes.setSelectedItem(value);
        return jComboBoxAttributes;
    }

    @Override
    public Object getCellEditorValue()
    {
        return jComboBoxAttributes.getSelectedItem();
    }
}
