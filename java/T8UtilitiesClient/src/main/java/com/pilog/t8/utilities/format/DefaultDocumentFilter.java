package com.pilog.t8.utilities.format;

import java.awt.Toolkit;
import java.io.Serializable;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
* @author Bouwer du Preez
*/
public class DefaultDocumentFilter extends DocumentFilter implements Serializable
{
    private int lengthRestriction;
    private Case textCase;

    public enum Case {UPPER, LOWER, ANY};

    public DefaultDocumentFilter(int lengthRestriction, Case textCase)
    {
        this.lengthRestriction = lengthRestriction;
        this.textCase = textCase;
    }

    public int getLengthRestriction()
    {
        return lengthRestriction;
    }

    public void setLengthRestriction(int lengthRestriction)
    {
        this.lengthRestriction = lengthRestriction;
    }

    public Case getTextCase()
    {
        return textCase;
    }

    public void setTextCase(Case textCase)
    {
        this.textCase = textCase;
    }

    @Override
    public void insertString(DocumentFilter.FilterBypass fb, int offset, String text, AttributeSet attr) throws BadLocationException
    {
        // This rejects the entire insertion if it would make
        // the contents too long. Another option would be
        // to truncate the inserted string so the contents
        // would be exactly lengthRestriction in length.
        if ((lengthRestriction < 1) || ((fb.getDocument().getLength() + text.length()) <= lengthRestriction))
        {
            switch (textCase)
            {
                case UPPER:
                {
                    super.insertString(fb, offset, text.toUpperCase(), attr);
                    break;
                }
                case LOWER:
                {
                    super.insertString(fb, offset, text.toLowerCase(), attr);
                    break;
                }
                default:
                {
                    super.insertString(fb, offset, text, attr);
                }
            }
        }
        else
        {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    @Override
    public void replace(DocumentFilter.FilterBypass bypass, int offset, int length, String text, AttributeSet attr) throws BadLocationException
    {
        int textLength;

        // This rejects the entire replacement if it would make
        // the contents too long. Another option would be
        // to truncate the replacement string so the contents
        // would be exactly lengthRestriction in length.
        textLength = (text != null ? text.length() : 0);
        if ((lengthRestriction < 1) || ((bypass.getDocument().getLength() + textLength - length) <= lengthRestriction))
        {
            switch (textCase)
            {
                case UPPER:
                {
                    super.replace(bypass, offset, length, text != null ? text.toUpperCase() : null, attr);
                    break;
                }
                case LOWER:
                {
                    super.replace(bypass, offset, length, text != null ? text.toLowerCase() : null, attr);
                    break;
                }
                default:
                {
                    super.replace(bypass, offset, length, text, attr);
                }
            }
        }
        else
        {
            Toolkit.getDefaultToolkit().beep();
        }
    }
}
