package com.pilog.t8.utilities.components.progressbar;

import java.util.concurrent.TimeUnit;
import javax.swing.JProgressBar;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.PropertySetter;

/**
 * @author Hennie Brink
 */
public class T8ProgressBar extends JProgressBar
{
    private Animator animator;

    public T8ProgressBar()
    {
        setUI(new T8ProgressBarUI());
    }

    public void setProgress(int progress)
    {
        super.setValue(progress);
    }

    public int getProgress()
    {
        return super.getValue();
    }

    @Override
    public void setValue(int n)
    {
        if(animator != null && animator.isRunning()) animator.stop();
        //Only animate for increments larger that 10
        if (n - getProgress() > 10)
        {
            animator = new Animator.Builder()
                    .addTarget(PropertySetter.getTargetTo(this, "progress", n))
                    .setDuration(100, TimeUnit.MILLISECONDS)
                    .build();
            animator.start();
        }
        else
        {
            setProgress(n);
        }
    }
}
