package com.pilog.t8.utilities.components.list.popupmenu;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Point;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.MenuElement;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.MenuItemUI;
import javax.swing.plaf.PopupMenuUI;

/**
 * @author Bouwer du Preez
 */
public class ListSubMenu extends JMenu
{
    /*
     * The popup menu portion of the menu.
     */
    private JPopupMenu listPopup;

    /*
     * Location of the popup component. Location is <code>null</code>
     * if it was not customized by <code>setMenuLocation</code>
     */
    private Point customMenuLocation = null;

    /**
     * Constructs a new <code>JMenu</code> with the supplied string
     * as its text.
     *
     * @param s  the text for the menu label
     */
    public ListSubMenu(String s, JPopupMenu listPopupMenu)
    {
        super(s);
        this.setPopupMenu(listPopupMenu);
    }

    /**
     * Resets the UI property with a value from the current look and feel.
     *
     * @see JComponent#updateUI
     */
    @Override
    public void updateUI()
    {
        setUI((MenuItemUI) UIManager.getUI(this));

        if (listPopup != null)
        {
            listPopup.setUI((PopupMenuUI) UIManager.getUI(listPopup));
        }

    }

    /**
     * Returns true if the menu's popup window is visible.
     *
     * @return true if the menu is visible, else false
     */
    @Override
    public boolean isPopupMenuVisible()
    {
        ensurePopupMenuCreated();
        return listPopup.isVisible();
    }

    public void setPopupMenu(JPopupMenu listPopup)
    {
        if (listPopup != null)
        {
            this.listPopup = listPopup;
            listPopup.setInvoker(this);
            popupListener = createWinListener(listPopup);
        }
    }

    private void ensurePopupMenuCreated()
    {
        if (listPopup == null)
        {
            this.listPopup = new ListPopupMenu("New List Popup");
            listPopup.setInvoker(this);
            popupListener = createWinListener(listPopup);
        }
    }

    /**
     * Creates a window-closing listener for the popup.
     *
     * @param p the <code>JPopupMenu</code>
     * @return the new window-closing listener
     *
     * @see WinListener
     */
    @Override
    protected WinListener createWinListener(JPopupMenu p)
    {
        return new WinListener(p);
    }

    /**
     * Sets the location of the popup component.
     *
     * @param x the x coordinate of the popup's new position
     * @param y the y coordinate of the popup's new position
     */
    @Override
    public void setMenuLocation(int x, int y)
    {
        customMenuLocation = new Point(x, y);
        if (listPopup != null)
        {
            listPopup.setLocation(x, y);
        }
    }

    /**
     * Returns a properly configured <code>PropertyChangeListener</code>
     * which updates the control as changes to the <code>Action</code> occur.
     */
    @Override
    protected PropertyChangeListener createActionChangeListener(JMenuItem b)
    {
        return null;
    }

    /**
     * Returns the number of components on the menu.
     *
     * @return an integer containing the number of components on the menu
     */
    @Override
    public int getMenuComponentCount()
    {
        int componentCount = 0;
        if (listPopup != null)
        {
            componentCount = listPopup.getComponentCount();
        }
        return componentCount;
    }

    /**
     * Returns the component at position <code>n</code>.
     *
     * @param n the position of the component to be returned
     * @return the component requested, or <code>null</code>
     *			if there is no popup menu
     *
     */
    @Override
    public Component getMenuComponent(int n)
    {
        if (listPopup != null)
        {
            return listPopup.getComponent(n);
        }

        return null;
    }

    /**
     * Returns an array of <code>Component</code>s of the menu's
     * subcomponents.  Note that this returns all <code>Component</code>s
     * in the popup menu, including separators.
     *
     * @return an array of <code>Component</code>s or an empty array
     *		if there is no popup menu
     */
    @Override
    public Component[] getMenuComponents()
    {
        if (listPopup != null)
        {
            return listPopup.getComponents();
        }

        return new Component[0];
    }

    /**
     * Returns the popup menu associated with this menu.  If there is
     * no popup menu, it will create one.
     */
    @Override
    public JPopupMenu getPopupMenu()
    {
        ensurePopupMenuCreated();
        return listPopup;
    }

    class MenuChangeListener implements ChangeListener, Serializable
    {
        boolean isSelected = false;

        @Override
        public void stateChanged(ChangeEvent e)
        {
            ButtonModel model = (ButtonModel) e.getSource();
            boolean modelSelected = model.isSelected();

            if (modelSelected != isSelected)
            {
                if (modelSelected == true)
                {
                    fireMenuSelected();
                }
                else
                {
                    fireMenuDeselected();
                }
                isSelected = modelSelected;
            }
        }
    }

    /**
     * Returns an array of <code>MenuElement</code>s containing the submenu
     * for this menu component.  If popup menu is <code>null</code> returns
     * an empty array.  This method is required to conform to the
     * <code>MenuElement</code> interface.  Note that since
     * <code>JSeparator</code>s do not conform to the <code>MenuElement</code>
     * interface, this array will only contain <code>JMenuItem</code>s.
     *
     * @return an array of <code>MenuElement</code> objects
     */
    @Override
    public MenuElement[] getSubElements()
    {
        if (listPopup == null)
        {
            return new MenuElement[0];
        }
        else
        {
            MenuElement result[] = new MenuElement[1];
            result[0] = listPopup;
            return result;
        }
    }

    /**
     * Sets the <code>ComponentOrientation</code> property of this menu
     * and all components contained within it. This includes all
     * components returned by {@link #getMenuComponents getMenuComponents}.
     *
     * @param o the new component orientation of this menu and
     *        the components contained within it.
     * @exception NullPointerException if <code>orientation</code> is null.
     * @see java.awt.Component#setComponentOrientation
     * @see java.awt.Component#getComponentOrientation
     * @since 1.4
     */
    @Override
    public void applyComponentOrientation(ComponentOrientation o)
    {
        super.applyComponentOrientation(o);

        if (listPopup != null)
        {
            int ncomponents = getMenuComponentCount();
            for (int i = 0; i < ncomponents; ++i)
            {
                getMenuComponent(i).applyComponentOrientation(o);
            }
            listPopup.setComponentOrientation(o);
        }
    }

    @Override
    public void setComponentOrientation(ComponentOrientation o)
    {
        super.setComponentOrientation(o);
        if (listPopup != null)
        {
            listPopup.setComponentOrientation(o);
        }
    }
}
