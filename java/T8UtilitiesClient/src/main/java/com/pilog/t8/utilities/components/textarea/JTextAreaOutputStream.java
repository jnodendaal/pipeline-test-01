package com.pilog.t8.utilities.components.textarea;

import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;

/**
 * @author Bouwer du Preez
 */
public class JTextAreaOutputStream extends OutputStream
{
    private JTextArea textArea;

    public JTextAreaOutputStream(JTextArea tarea)
    {
        this.textArea = tarea;
    }

    @Override
    public void write(int b) throws IOException
    {
        byte[] bytes;
        String newText;

        bytes = new byte[1];
        bytes[0] = (byte)b;
        newText = new String(bytes);

        textArea.append(newText);
        if (newText.indexOf('\n') > -1)
        {
            try
            {
                textArea.scrollRectToVisible(textArea.modelToView(textArea.getDocument().getLength()));
            }
            catch (BadLocationException err)
            {
                err.printStackTrace();
            }
        }
    }

    @Override
    public final void write(byte[] byteArray) throws IOException
    {
        String txt;

        txt = new String(byteArray);
        textArea.append(txt);
        try
        {
            textArea.scrollRectToVisible(textArea.modelToView(textArea.getDocument().getLength()));
        }
        catch (BadLocationException err)
        {
            err.printStackTrace();
        }
    }
}
