package com.pilog.t8.utilities.components.list.dialog;

import com.pilog.t8.utilities.components.list.ListPopupMenuListener;
import com.pilog.t8.utilities.components.list.T8List;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.RowFilter;
import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.JXSearchField;
import org.jdesktop.swingx.JXTextField;

/**
 * @author Hennie Brink
 * @param <T>
 */
public class SearchableListDialog<T extends Object> extends JDialog implements KeyListener, T8List
{
    private ListModel<T> model;
    private JXList list;
    private JXTextField textField;
    private final List<ListPopupMenuListener> listeners;

    public SearchableListDialog(Window parent, ListModel<T> listModel)
    {
        super(parent, ModalityType.APPLICATION_MODAL);
        super.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        model = listModel;
        listeners = new ArrayList<ListPopupMenuListener>();
        buildUI();
        setPreferredSize(new Dimension(600, 400));
        pack();
        setLocationRelativeTo(this);
    }

    public SearchableListDialog(Window parent, List<T> items)
    {
        super(parent, ModalityType.APPLICATION_MODAL);
        super.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        model = new DefaultListModel<T>();
        listeners = new ArrayList<ListPopupMenuListener>();
        buildUI();
        setPreferredSize(new Dimension(400, 400));
        pack();

        for (T item : items)
        {
            ((DefaultListModel) model).addElement(item);
        }
    }

    public T getSelectedValue()
    {
        return (T)list.getSelectedValue();
    }

    @Override
    public Object getSelectedItem()
    {
        return getSelectedValue();
    }

    public T[] getSelectedValues()
    {
        return (T[])list.getSelectedValues();
    }

    public T[] getSelectedItems()
    {
        return getSelectedValues();
    }

    @Override
    public void removeAllListeners()
    {
        listeners.clear();
    }

    @Override
    public void removePopupListener(ListPopupMenuListener listener)
    {
        listeners.remove(listener);
    }

    @Override
    public void setCellRenderer(ListCellRenderer renderer)
    {
        list.setCellRenderer(renderer);
    }

    @Override
    public void setListModel(ListModel listModel)
    {
        model = listModel;
        list.setModel(model);
    }

    @Override
    public void addPopupListener(ListPopupMenuListener listener)
    {
        listeners.add(listener);
    }

    private void buildUI()
    {
        JScrollPane scrollPane;

        textField = new JXSearchField();
        textField.addKeyListener(this);

        list = new JXList(model, true);
        list.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                super.mouseClicked(e);
                if (e.getClickCount() == 2)
                {
                    for (ListPopupMenuListener listPopupMenuListener : listeners)
                    {
                        listPopupMenuListener.popupItemSelected(getSelectedItem());
                    }
                    setVisible(false);
                }
            }
        });

        list.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "TriggerItemsSelected");
        list.getActionMap().put("TriggerItemsSelected", new AbstractAction("TriggerItemsSelected")
                        {

                            @Override
                            public void actionPerformed(ActionEvent e)
                            {
                                for (ListPopupMenuListener listPopupMenuListener : listeners)
                                {
                                    listPopupMenuListener.popupItemsSelected(getSelectedItems());
                                }
                                setVisible(false);
                            }
        });

        setLayout(new BorderLayout(2, 2));
        add(textField, BorderLayout.NORTH);
        scrollPane = new JScrollPane(list);
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        add(scrollPane, BorderLayout.CENTER);
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        String textFieldValue;

        textFieldValue = textField.getText();
        if (!Strings.isNullOrEmpty(textFieldValue))
        {
            final String searchText;

            searchText = textFieldValue.toUpperCase();
            list.setRowFilter(new RowFilter<ListModel<String>, Integer>()
            {
                @Override
                public boolean include(RowFilter.Entry<? extends ListModel<String>, ? extends Integer> entry)
                {
                    String value;

                    value = entry.getStringValue(0);
                    if (value != null)
                    {
                        return value.toUpperCase().contains(searchText);
                    }
                    else return false;
                }
            });
        }
    }
}
