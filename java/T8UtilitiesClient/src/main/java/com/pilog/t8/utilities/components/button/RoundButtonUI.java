package com.pilog.t8.utilities.components.button;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.RoundRectangle2D;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicButtonUI;

/**
 * @author Hennie Brink
 */
public class RoundButtonUI extends BasicButtonUI
{
    private static float arcwidth;
    private static float archeight;
    protected static int focusstroke;
    protected Paint focusColour = new Color(100, 150, 255, 200);
    protected Paint armedColour = new Color(230, 230, 230);
    protected Paint rolloverColour = Color.ORANGE;
    protected Shape shape;
    protected Shape border;
    protected Shape base;

    public RoundButtonUI()
    {
        this.focusstroke = 2;
        this.arcwidth = 16.0f;
        this.archeight = 16.0f;
    }

    public RoundButtonUI(int focusstroke, float arcwidth, float archeight)
    {
        this.focusstroke = focusstroke;
        this.arcwidth = arcwidth;
        this.archeight = archeight;
    }

    @Override
    protected void installDefaults(AbstractButton b)
    {
        super.installDefaults(b);
        b.setContentAreaFilled(false);
        b.setOpaque(false);
        initShape(b);
    }

    @Override
    protected void installListeners(AbstractButton b)
    {
        BasicButtonListener listener = new BasicButtonListener(b)
        {
            @Override
            public void mousePressed(MouseEvent e)
            {
                AbstractButton b = (AbstractButton) e.getSource();
                initShape(b);
                if (shape.contains(e.getX(), e.getY()))
                {
                    super.mousePressed(e);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e)
            {
                if (shape.contains(e.getX(), e.getY()))
                {
                    super.mouseEntered(e);
                }
            }

            @Override
            public void mouseMoved(MouseEvent e)
            {
                if (shape.contains(e.getX(), e.getY()))
                {
                    super.mouseEntered(e);
                }
                else
                {
                    super.mouseExited(e);
                }
            }
        };
        if (listener != null)
        {
            b.addMouseListener(listener);
            b.addMouseMotionListener(listener);
            b.addFocusListener(listener);
            b.addPropertyChangeListener(listener);
            b.addChangeListener(listener);
        }
    }

    @Override
    public void paint(Graphics g, JComponent c)
    {
        Graphics2D g2 = (Graphics2D) g;
        AbstractButton b = (AbstractButton) c;
        ButtonModel model = b.getModel();
        initShape(b);
        //ContentArea
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);
        if (model.isArmed())
        {
            g2.setPaint(armedColour);
            g2.fill(shape);
        }
        else if (b.isRolloverEnabled() && model.isRollover())
        {
            paintFocusAndRollover(g2, c, rolloverColour);
        }
        else if (b.hasFocus())
        {
            paintFocusAndRollover(g2, c, focusColour);
        }
        else
        {
            g2.setColor(c.getBackground());
            g2.fill(shape);
        }
        //Border
        g2.setPaint(c.getForeground());
        g2.draw(shape);

        g2.setColor(c.getBackground());
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_OFF);
        super.paint(g2, c);
    }

    private void initShape(JComponent c)
    {
        if (!c.getBounds().equals(base))
        {
            base = c.getBounds();
            shape = new RoundRectangle2D.Float(0, 0, c.getWidth() - 1, c.getHeight() - 1,
                                               arcwidth, archeight);
            border = new RoundRectangle2D.Float(focusstroke, focusstroke,
                                                c.getWidth() - 1 - focusstroke * 2,
                                                c.getHeight() - 1 - focusstroke * 2,
                                                arcwidth, archeight);
        }
    }

    private void paintFocusAndRollover(Graphics2D g2, JComponent c, Paint color)
    {
        g2.setPaint(color);
        g2.fill(shape);
        g2.setColor(c.getBackground());
        g2.fill(border);
    }

    public void setFocusColour(Paint focusColour)
    {
        this.focusColour = focusColour;
    }

    public void setArmedColour(Paint armedColour)
    {
        this.armedColour = armedColour;
    }

    public void setRolloverColour(Paint rolloverColour)
    {
        this.rolloverColour = rolloverColour;
    }

}
