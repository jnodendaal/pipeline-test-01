package com.pilog.t8.utilities.components.table.configuredcelltable;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

/**
 * @author Bouwer du Preez
 */
public class CellConfiguredTable extends JTable
{
    private final TableColumnWidthListener columnResizeListener;
    
    public CellConfiguredTable(TableModel model)
    {
        super(model);
        setUI(new CellConfiguredTableUI());
        getTableHeader().setReorderingAllowed(false);
        setCellSelectionEnabled(true);
        setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        columnResizeListener = new TableColumnWidthListener();
        getColumnModel().addColumnModelListener(columnResizeListener);
    }
    
    public void setCellRenderer(TableCellRenderer renderer)
    {
        for (int columnIndex = 0; columnIndex < columnModel.getColumnCount(); columnIndex++)
        {
            columnModel.getColumn(columnIndex).setCellRenderer(renderer);
        }
    }

    public void autoSizeRowHeights()
    {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
        {
            int newRowHeight;

            // Calculate the new row height.
            newRowHeight = getRowHeight();
            for (int columnIndex = 0; columnIndex < getColumnCount(); columnIndex++)
            {
                Rectangle cellRect;
                Component comp;

                cellRect = getCellRect(rowIndex, columnIndex, true);
                comp = prepareRenderer(getCellRenderer(rowIndex, columnIndex), rowIndex, columnIndex);
                comp.setBounds(cellRect);
                newRowHeight = Math.max(newRowHeight, comp.getPreferredSize().height);
            }

            // Set the row height on this table.
            setRowHeight(rowIndex, newRowHeight);
        }
    }
    
    @Override
    public Rectangle getCellRect(int rowIndex, int columnIndex, boolean includeSpacing)
    {
        if ((rowIndex < 0) || (columnIndex < 0) || (getRowCount() <= rowIndex) || (getColumnCount() <= columnIndex))
        {
            Rectangle sRect;
        
            sRect = super.getCellRect(rowIndex, columnIndex, includeSpacing);
            return sRect;
        }
        else
        {
            CellConfiguration cellConfiguration;
            CellSpan cellSpan;
            Rectangle cellRect;
            int modelRowIndex;
            int modelColumnIndex;
            
            modelRowIndex = convertRowIndexToModel(rowIndex);
            modelColumnIndex = convertColumnIndexToModel(columnIndex);
            
            cellConfiguration = ((CellConfiguredTableModel)getModel()).getCellConfiguration();
            if (!cellConfiguration.isVisible(modelRowIndex, modelColumnIndex))
            {
                CellSpan span;
    
                span = cellConfiguration.getSpan(modelRowIndex, modelColumnIndex);
                rowIndex += span.getRowsSpanned();
                columnIndex += span.getColumnSpanned();
            }
            
            modelRowIndex = convertRowIndexToModel(rowIndex);
            modelColumnIndex = convertColumnIndexToModel(columnIndex);

            cellSpan = cellConfiguration.getSpan(modelRowIndex, modelColumnIndex);
            cellRect = new Rectangle();
            
            // Set the height of the cell to the height of the row.
            cellRect.height = getRowHeight(rowIndex);
            
            // Now determine the x-coordinate of the cell by adding all the widths of preceding columns in the row.
            for (int rIndex = 0; rIndex < rowIndex; rIndex++)
            {
                cellRect.y += getRowHeight(rIndex);
            }
            
            // Add to the height of the cell, the total height of all spanned rows.
            for (int rIndex = 1; rIndex < cellSpan.getRowsSpanned(); rIndex++)
            {
                cellRect.height += getRowHeight(rowIndex + rIndex);
            }
            
            // Set the width of the cell to the width of the column.
            cellRect.width = columnModel.getColumn(columnIndex).getWidth();
            
            // Now determine the x-coordinate of the cell by adding all the widths of preceding columns in the row.
            for (int cIndex = 0; cIndex < columnIndex; cIndex++)
            {
                cellRect.x += columnModel.getColumn(cIndex).getWidth();
            }

            // Add to the width of the cell, the total width of all spanned columns.
            for (int cIndex = 1; cIndex < cellSpan.getColumnSpanned(); cIndex++)
            {
                TableColumn column;
                
                column = columnModel.getColumn(columnIndex + cIndex);
                cellRect.width += column.getWidth();
            }

            if (!includeSpacing)
            {
                Dimension spacing;
                int boundsX;
                int boundsY;
                int boundsHeight;
                int boundsWidth;
                
                spacing = getIntercellSpacing();
                boundsX = cellRect.x + spacing.width / 2;
                boundsY = cellRect.y + spacing.height / 2;
                boundsHeight = cellRect.width - spacing.width;
                boundsWidth = cellRect.height - spacing.height;
                cellRect.setBounds(boundsX, boundsY, boundsHeight, boundsWidth);
            }
            
            return cellRect;
        }
    }

    private CellSpan rowColumnAtPoint(Point point)
    {
        CellConfiguration cellConfiguration;
        CellSpan retValue;
        int columnIndex;
        int rowIndex;
        int modelRowIndex;
        int modelColumnIndex;
        
        retValue = new CellSpan(-1, -1);
        rowIndex = super.rowAtPoint(point);
        columnIndex = getColumnModel().getColumnIndexAtX(point.x);
        modelRowIndex = convertRowIndexToModel(rowIndex);
        modelColumnIndex = convertColumnIndexToModel(columnIndex);
        
        cellConfiguration = ((CellConfiguredTableModel) getModel()).getCellConfiguration();
        if (cellConfiguration.isVisible(modelRowIndex, modelColumnIndex))
        {
            retValue.setColumnSpanned(columnIndex);
            retValue.setRowsSpanned(rowIndex);
            return retValue;
        }
        else
        {
            retValue.setColumnSpanned(columnIndex + cellConfiguration.getSpan(modelRowIndex, modelColumnIndex).getColumnSpanned());
            retValue.setRowsSpanned(rowIndex + cellConfiguration.getSpan(modelRowIndex, modelColumnIndex).getRowsSpanned());
            return retValue;
        }
    }

    @Override
    public int rowAtPoint(Point point)
    {
        return rowColumnAtPoint(point).getRowsSpanned();
    }

    @Override
    public int columnAtPoint(Point point)
    {
        return rowColumnAtPoint(point).getColumnSpanned();
    }

    @Override
    public void columnSelectionChanged(ListSelectionEvent e)
    {
        repaint();
    }
    
    private class TableColumnWidthListener implements TableColumnModelListener
    {

        @Override
        public void columnMarginChanged(ChangeEvent e)
        {
            /* columnMarginChanged is called continuously as the column width is changed
             by dragging. Therefore, execute code below ONLY if we are not already
             aware of the column width having changed */
            if (getTableHeader().getResizingColumn() != null)
            {
                autoSizeRowHeights();
            }
        }

        @Override
        public void columnMoved(TableColumnModelEvent e)
        {
        }

        @Override
        public void columnAdded(TableColumnModelEvent e)
        {
        }

        @Override
        public void columnRemoved(TableColumnModelEvent e)
        {
        }

        @Override
        public void columnSelectionChanged(ListSelectionEvent e)
        {
        }
    }
}
