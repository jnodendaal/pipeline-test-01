package com.pilog.t8.utilities.components.messagedialogs;

import com.pilog.t8.utilities.strings.Strings;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.RoundRectangle2D;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class Toast extends JDialog
{
    private static final long serialVersionUID = -1602907470843951525L;

    public enum Style
    {
        NORMAL, SUCCESS, ERROR
    };

    public static final int LENGTH_SHORT = 4000;
    public static final int LENGTH_LONG = 6000;
    public static final Color ERROR_RED = new Color(100, 0, 0);
    public static final Color SUCCESS_GREEN = new Color(30, 100, 40);
    public static final Color NORMAL_BLACK = new Color(0, 0, 0);
    private static final AtomicInteger activeToasts = new AtomicInteger(0);

    private final float MAX_OPACITY = 0.8f;
    private final float OPACITY_INCREMENT = 0.05f;
    private final int FADE_REFRESH_RATE = 25;
    private final int WINDOW_RADIUS = 15;
    private final int CHARACTER_LENGTH_MULTIPLIER = 7;
    private final int DISTANCE_FROM_PARENT_TOP = 100;

    private Window mOwner;
    private String mText;
    private int mDuration;
    private Color mBackgroundColor = Color.BLACK;
    private Color mForegroundColor = Color.WHITE;

    public Toast(Window owner)
    {
        super(owner);
        mOwner = owner;
        activeToasts.getAndIncrement();
    }

    private void createGUI()
    {
        Runnable createUI = new Runnable()
        {

            @Override
            public void run()
            {
                setLayout(new GridBagLayout());
                addComponentListener(new ComponentAdapter()
                {
                    @Override
                    public void componentResized(ComponentEvent e)
                    {
                        setShape(new RoundRectangle2D.Double(0, 0, getWidth(), getHeight(), WINDOW_RADIUS, WINDOW_RADIUS));
                    }
                });

                setAlwaysOnTop(true);
                setUndecorated(true);
                setFocusable(false);
                setFocusableWindowState(false);
                setModalityType(ModalityType.MODELESS);
                setSize(mText.length() * CHARACTER_LENGTH_MULTIPLIER, 25);
                getContentPane().setBackground(mBackgroundColor);

                JLabel label = new JLabel(mText);
                label.setForeground(mForegroundColor);
                label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
                add(label);

                setShape(new RoundRectangle2D.Double(0, 0, getWidth(), getHeight(), WINDOW_RADIUS, WINDOW_RADIUS));

                fadeIn();
            }
        };
        try
        {
            if (SwingUtilities.isEventDispatchThread())
            {
                createUI.run();
            }
            else
            {
                SwingUtilities.invokeAndWait(createUI);
            }
        }
        catch (InterruptedException ex)
        {
            ex.printStackTrace();
        }
        catch (InvocationTargetException ex)
        {
            ex.printStackTrace();
        }
    }

    public void fadeIn()
    {
        final Timer timer = new Timer(FADE_REFRESH_RATE, null);
        final Point location = getToastLocation();
        final int finalYLocation = location.y;
        location.y -= 35;
        timer.setRepeats(true);
        timer.addActionListener(new ActionListener()
        {
            private float opacity = 0;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                opacity += OPACITY_INCREMENT;
                if (location.y < finalYLocation)
                {
                    location.y += 5;
                    setLocation(location);
                }
                setOpacity(Math.min(opacity, MAX_OPACITY));
                if (opacity >= MAX_OPACITY)
                {
                    timer.stop();
                    fadeOut();
                }
            }
        });

        setOpacity(0);
        timer.start();

        setVisible(true);
    }

    public void fadeOut()
    {
        final Timer timer = new Timer(FADE_REFRESH_RATE, null);
        timer.setRepeats(true);
        timer.setInitialDelay(mDuration);
        timer.addActionListener(new ActionListener()
        {
            private float opacity = MAX_OPACITY;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                opacity -= OPACITY_INCREMENT;
                setOpacity(Math.max(opacity, 0));
                if (opacity <= 0)
                {
                    timer.stop();
                    setVisible(false);
                    dispose();
                    activeToasts.getAndDecrement();
                }
            }
        });

        timer.start();
    }

    private Point getToastLocation()
    {
        if (mOwner == null)
        {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

            Point displayPoint = new Point();
            displayPoint.x = (int) ((screenSize.getWidth() - this.getWidth()) / 2);
            displayPoint.y = DISTANCE_FROM_PARENT_TOP + ((activeToasts.get() - 1) * 30);

            return displayPoint;
        }
        Point ownerLoc = mOwner.getLocation();
        int x = (int) (ownerLoc.getX() + ((mOwner.getWidth() - this.getWidth()) / 2));
        int y = (int) (ownerLoc.getY() + DISTANCE_FROM_PARENT_TOP + ((activeToasts.get() - 1) * 30));
        return new Point(x, y);
    }

    public void setText(String text)
    {
        if (Strings.isNullOrEmpty(text)) throw new IllegalArgumentException("Toast Text Cannot be NULL or EMPTY!");
        mText = text;
    }

    public void setDuration(int duration)
    {
        mDuration = duration;
    }

    @Override
    public void setBackground(Color backgroundColor)
    {
        mBackgroundColor = backgroundColor;
    }

    @Override
    public void setForeground(Color foregroundColor)
    {
        mForegroundColor = foregroundColor;
    }

    public static Toast makeText(Window owner, String text)
    {
        return makeText(owner, text, LENGTH_SHORT);
    }

    public static Toast makeText(Window owner, String text, Style style)
    {
        return makeText(owner, text, LENGTH_SHORT, style);
    }

    public static Toast makeText(Window owner, String text, int duration)
    {
        return makeText(owner, text, duration, Style.NORMAL);
    }

    public static Toast makeText(Window owner, String text, int duration, Style style)
    {
        Toast toast = new Toast(owner);
        toast.setText(text);
        toast.setDuration(duration);

        switch (style)
        {
            case SUCCESS:
                toast.setBackground(SUCCESS_GREEN);
                break;
            case ERROR:
                toast.setBackground(ERROR_RED);
                break;
            default:
                toast.setBackground(NORMAL_BLACK);
        }

        return toast;
    }

    public void display()
    {
        createGUI();
    }

    public static void show(String message)
    {
        Toast.makeText(null, message).display();
    }

    public static void show(String message, Style style)
    {
        Toast.makeText(null, message, style).display();
    }

    public static void show(String message, int duration)
    {
        Toast.makeText(null, message, duration).display();
    }

    public static void show(String message, Style style, int duration)
    {
        Toast.makeText(null, message, duration, style).display();
    }
}
