package com.pilog.t8.utilities.components.table.configuredcelltable;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

/**
 * @author Bouwer du Preez
 */
public class DefaultCellConfiguration implements CellConfiguration
{
    private static final T8Logger logger = T8Log.getLogger(DefaultCellConfiguration.class);

    private int rowSize;
    private int columnSize;
    private CellConfigurationSettings[][] cellConfigurationSettings; // CellSpan

    public DefaultCellConfiguration()
    {
        this(1, 1);
    }

    public DefaultCellConfiguration(int numRows, int numColumns)
    {
        setSize(new Dimension(numColumns, numRows));
    }

    protected void initValue()
    {
        for (int rowIndex = 0; rowIndex < cellConfigurationSettings.length; rowIndex++)
        {
            for (int columnIndex = 0; columnIndex < cellConfigurationSettings[rowIndex].length; columnIndex++)
            {
                cellConfigurationSettings[rowIndex][columnIndex] = new CellConfigurationSettings(new CellSpan(1, 1));
            }
        }
    }

    @Override
    public CellSpan getSpan(int rowIndex, int columnIndex)
    {
        if (isOutOfBounds(rowIndex, columnIndex))
        {
            return new CellSpan(1, 1);
        }
        else return cellConfigurationSettings[rowIndex][columnIndex].getSpan();
    }

    @Override
    public void setSpan(CellSpan span, int rowIndex, int columnIndex)
    {
        if (!isOutOfBounds(rowIndex, columnIndex))
        {
            this.cellConfigurationSettings[rowIndex][columnIndex].setSpan(span);
        }
    }

    @Override
    public boolean isVisible(int rowIndex, int columnIndex)
    {
        if (isOutOfBounds(rowIndex, columnIndex))
        {
            return false;
        }
        else
        {
            CellSpan span;

            if (cellConfigurationSettings[rowIndex][columnIndex] == null)
            {
                logger.log("Null at: r" + rowIndex + ", c" + columnIndex);
                printStructure();
            }

            span = cellConfigurationSettings[rowIndex][columnIndex].getSpan();
            if ((span.getColumnSpanned() < 1) || (span.getRowsSpanned() < 1))
            {
                return false;
            }
            else return true;
        }
    }

    private void printStructure()
    {
        for (int rowIndex = 0; rowIndex < cellConfigurationSettings.length; rowIndex++)
        {
            logger.log("\n" + rowIndex + ": ");
            for (int columnIndex = 0; columnIndex < cellConfigurationSettings[rowIndex].length; columnIndex++)
            {
                boolean value;

                value = cellConfigurationSettings[rowIndex][columnIndex] != null;
                logger.log(value ? " 1" : " 0");
            }
        }
    }

    @Override
    public void combine(int startRow, int startColumn, int rowSpan, int columnSpan)
    {
        // First make sure that none of the cells are already spanning more than one index.
        for (int rowIndex = 0; rowIndex < rowSpan; rowIndex++)
        {
            for (int columnIndex = 0; columnIndex < columnSpan; columnIndex++)
            {
                CellSpan cellSpan;

                cellSpan = cellConfigurationSettings[startRow + rowIndex][startColumn + columnIndex].getSpan();
                if ((cellSpan.getColumnSpanned() != 1) || (cellSpan.getRowsSpanned() != 1))
                {
                    return;
                }
            }
        }

        // Now combine the cells.
        for (int rowIndex = 0, ii = 0; rowIndex < rowSpan; rowIndex++, ii--)
        {
            for (int columnIndex = 0, jj = 0; columnIndex < columnSpan; columnIndex++, jj--)
            {
                cellConfigurationSettings[startRow + rowIndex][startColumn + columnIndex].getSpan().setColumnSpanned(jj);
                cellConfigurationSettings[startRow + rowIndex][startColumn + columnIndex].getSpan().setRowsSpanned(ii);
            }
        }

        cellConfigurationSettings[startRow][startColumn].getSpan().setColumnSpanned(columnSpan);
        cellConfigurationSettings[startRow][startColumn].getSpan().setRowsSpanned(rowSpan);
    }

    @Override
    public void split(int row, int column)
    {
        if (!isOutOfBounds(row, column))
        {
            int columnSpan = cellConfigurationSettings[row][column].getSpan().getColumnSpanned();
            int rowSpan = cellConfigurationSettings[row][column].getSpan().getRowsSpanned();
            for (int rowIndex = 0; rowIndex < rowSpan; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < columnSpan; columnIndex++)
                {
                    cellConfigurationSettings[row + rowIndex][column + columnIndex].getSpan().setColumnSpanned(1);
                    cellConfigurationSettings[row + rowIndex][column + columnIndex].getSpan().setRowsSpanned(1);
                }
            }
        }
    }

    @Override
    public Color getForeground(int rowIndex, int columnIndex)
    {
        if (isOutOfBounds(rowIndex, columnIndex))
        {
            return null;
        }
        else return cellConfigurationSettings[rowIndex][columnIndex].getForegroundColor();
    }

    @Override
    public void setForeground(Color color, int rowIndex, int columnIndex)
    {
        if (!isOutOfBounds(rowIndex, columnIndex))
        {
            cellConfigurationSettings[rowIndex][columnIndex].setForegroundColor(color);
        }
    }

    @Override
    public void setForeground(Color color, int[] rowIndices, int[] columnIndices)
    {
        if (!isOutOfBounds(rowIndices, columnIndices))
        {
            for (int rows = 0; rows < rowIndices.length; rows++)
            {
                for (int columns = 0; columns < columnIndices.length; columns++)
                {
                    setForeground(color, rowIndices[rows], columnIndices[columns]);
                }
            }
        }
    }

    @Override
    public Color getBackground(int rowIndex, int columnIndex)
    {
        if (!isOutOfBounds(rowIndex, columnIndex))
        {
            return cellConfigurationSettings[rowIndex][columnIndex].getBackgroundColor();
        }
        else return null;
    }

    @Override
    public void setBackground(Color color, int rowIndex, int columnIndex)
    {
        if (!isOutOfBounds(rowIndex, columnIndex))
        {
            cellConfigurationSettings[rowIndex][columnIndex].setBackgroundColor(color);
        }
    }

    @Override
    public void setBackground(Color color, int[] rowIndices, int[] columnIndices)
    {
        if (!isOutOfBounds(rowIndices, columnIndices))
        {
            for (int rows = 0; rows < rowIndices.length; rows++)
            {
                for (int columns = 0; columns < columnIndices.length; columns++)
                {
                    setBackground(color, rowIndices[rows], columnIndices[columns]);
                }
            }
        }
    }

    @Override
    public Font getFont(int rowIndex, int columnIndex)
    {
        if (!isOutOfBounds(rowIndex, columnIndex))
        {
            return cellConfigurationSettings[rowIndex][columnIndex].getFont();
        }
        else return null;
    }

    @Override
    public void setFont(Font font, int rowIndex, int columnIndex)
    {
        if (!isOutOfBounds(rowIndex, columnIndex))
        {
            this.cellConfigurationSettings[rowIndex][columnIndex].setFont(font);
        }
    }

    @Override
    public void setFont(Font font, int[] rowIndices, int[] columnIndices)
    {
        if (!isOutOfBounds(rowIndices, columnIndices))
        {
            for (int rows = 0; rows < rowIndices.length; rows++)
            {
                for (int columns = 0; columns < columnIndices.length; columns++)
                {
                    setFont(font, rowIndices[rows], columnIndices[columns]);
                }
            }
        }
    }

    @Override
    public void addColumn()
    {
        CellConfigurationSettings[][] oldSettings;

        oldSettings = cellConfigurationSettings;
        cellConfigurationSettings = new CellConfigurationSettings[rowSize][columnSize + 1];
        System.arraycopy(oldSettings, 0, cellConfigurationSettings, 0, rowSize);
        for (int rowIndex = 0; rowIndex < rowSize; rowIndex++)
        {
            cellConfigurationSettings[rowIndex][columnSize] = new CellConfigurationSettings(new CellSpan(1, 1));
        }

        columnSize++;
    }

    @Override
    public void addRow()
    {
        CellConfigurationSettings[][] oldSpan = cellConfigurationSettings;

        cellConfigurationSettings = new CellConfigurationSettings[rowSize + 1][columnSize];
        System.arraycopy(oldSpan, 0, cellConfigurationSettings, 0, rowSize);
        for (int columnIndex = 0; columnIndex < columnSize; columnIndex++)
        {
            cellConfigurationSettings[rowSize][columnIndex] = new CellConfigurationSettings(new CellSpan(1, 1));
        }

        rowSize++;
    }

    @Override
    public void insertRow(int rowIndex)
    {
        CellConfigurationSettings[][] oldSpan = cellConfigurationSettings;

        // Copy the old array up to the point where the new row must be inserted.
        cellConfigurationSettings = new CellConfigurationSettings[rowSize + 1][columnSize];
        if (0 < rowIndex)
        {
            System.arraycopy(oldSpan, 0, cellConfigurationSettings, 0, rowIndex);
        }

        // Insert the new row at the specified index.
        for (int columnIndex = 0; columnIndex < columnSize; columnIndex++)
        {
            cellConfigurationSettings[rowIndex][columnIndex] = new CellConfigurationSettings(new CellSpan(1, 1));
        }

        // Copy the old array from the row index where the new row was inserte.
        System.arraycopy(oldSpan, rowIndex, cellConfigurationSettings, rowIndex + 1, rowSize - rowIndex);

        // Increment the row size.
        rowSize++;
    }

    @Override
    public Dimension getSize()
    {
        return new Dimension(rowSize, columnSize);
    }

    @Override
    public void setSize(Dimension size)
    {
        columnSize = size.width;
        rowSize = size.height;
        cellConfigurationSettings = new CellConfigurationSettings[rowSize][columnSize];
        initValue();
    }

    protected boolean isOutOfBounds(int rowIndex, int columnIndex)
    {
        if ((rowIndex < 0) || (rowSize <= rowIndex) || (columnIndex < 0) || (columnSize <= columnIndex))
        {
            return true;
        }
        else return false;
    }

    protected boolean isOutOfBounds(int[] rows, int[] columns)
    {
        for (int i = 0; i < rows.length; i++)
        {
            if ((rows[i] < 0) || (rowSize <= rows[i]))
            {
                return true;
            }
        }

        for (int i = 0; i < columns.length; i++)
        {
            if ((columns[i] < 0) || (columnSize <= columns[i]))
            {
                return true;
            }
        }

        return false;
    }

    protected void setValues(Object[][] target, Object value, int[] rows, int[] columns)
    {
        for (int i = 0; i < rows.length; i++)
        {
            int row = rows[i];
            for (int j = 0; j < columns.length; j++)
            {
                int column = columns[j];
                target[row][column] = value;
            }
        }
    }
}
