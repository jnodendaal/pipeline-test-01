package com.pilog.t8.utilities.components.list;

import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

/**
 * @author Hennie Brink
 */
public interface T8List {

    Object getSelectedItem();

    void removeAllListeners();

    void removePopupListener(ListPopupMenuListener listener);
    
    void addPopupListener(ListPopupMenuListener listener);

    void setCellRenderer(ListCellRenderer renderer);

    void setListModel(ListModel listModel);

}
