package com.pilog.t8.utilities.components.processingdialogs;

/**
 * @author Bouwer du Preez
 */
public abstract class ThreadTask
{
    public String getProgressText()
    {
        return "Processing...";
    }

    public int getMinimumProgress()
    {
        return -1;
    }

    public int getMaximumProgress()
    {
        return -1;
    }

    public int getProgress()
    {
        return -1;
    }

    public boolean isIndeterminate()
    {
        return true;
    }

    public abstract Object doTask() throws Exception;
}
