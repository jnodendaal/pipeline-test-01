package com.pilog.t8.utilities.components.progressbar;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.Point;
import javax.swing.JComponent;
import javax.swing.JProgressBar;
import javax.swing.plaf.basic.BasicProgressBarUI;

/**
 * @author Hennie Brink
 */
public class T8ProgressBarUI extends BasicProgressBarUI
{
    private int glowLocation;

    public void setGlowLocation(int glowLocation)
    {
        this.glowLocation = glowLocation;
    }

    public int getGlowLocation()
    {
        return glowLocation;
    }

    @Override
    public void installUI(JComponent c)
    {
        super.installUI(c);
    }

    @Override
    protected void paintDeterminate(Graphics g, JComponent c)
    {
        JProgressBar progressBar = (JProgressBar)c;

        int amountFull = getAmountFull(c.getInsets(), c.getWidth(), c.getHeight());

        Graphics2D g2d = (Graphics2D) g.create();

        Color blue = new Color(100, 150, 255);
        Color white = new Color(200, 230, 255);

        LinearGradientPaint linearGradientPaint = new LinearGradientPaint(c.getWidth() / 2, 0, c.getWidth() / 2, c.getHeight(), new float[]{0.3f, 0.6f, 0.9f}, new Color[]{blue, white, blue});

        g2d.setPaint(linearGradientPaint);

        g2d.fillRect(0, 0, amountFull, c.getHeight());

        g2d.setFont(c.getFont());
        g2d.setColor(Color.BLACK);
        Point stringPlacement = getStringPlacement(g2d, progressBar.getString(), 0, 0, c.getWidth(), c.getHeight());

        g2d.drawString(progressBar.getString(), stringPlacement.x, stringPlacement.y);

        g2d.dispose();
    }

}
