package com.pilog.t8.utilities.components.tabbedpane.tab;

import java.util.EventListener;

/**
 * @author Bouwer du Preez
 */
public interface TabComponentListener extends EventListener
{
    public void tabClosed(TabClosedEvent event);
    public void tabClosing(TabClosingEvent event);
}
