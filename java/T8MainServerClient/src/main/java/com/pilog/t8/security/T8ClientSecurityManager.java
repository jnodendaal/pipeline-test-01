package com.pilog.t8.security;

import static com.pilog.t8.definition.system.T8SecurityManagerResource.*;

import com.pilog.mainserver.T8MainServerClient;
import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerModule;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.http.T8HttpServletRequest;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.definition.remote.server.connection.T8RemoteConnectionUser;
import com.pilog.t8.definition.system.T8SecurityManagerResource;
import com.pilog.t8.mainserver.T8ServerRequest;
import com.pilog.t8.mainserver.T8ServerRequest.RequestType;
import com.pilog.t8.mainserver.T8ServerResponse;
import com.pilog.t8.mainserver.T8ServerResponse.ResponseType;
import com.pilog.t8.security.authentication.T8AuthenticationResponse;
import com.pilog.t8.security.event.T8SessionEventListener;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;

/**
 * @author Bouwer du Preez
 */
public class T8ClientSecurityManager implements T8SecurityManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ClientSecurityManager.class);

    private final T8ClientContext clientContext;

    public T8ClientSecurityManager(T8ClientContext clientContext)
    {
        this.clientContext = clientContext;
    }

    @Override
    public void init()
    {
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public T8Context getMainServerContext()
    {
        throw new RuntimeException("Security Exception.  This method is not accessibly within the current context.");
    }

    @Override
    public T8Context createOrganizationContext(T8Context context, String orgId)
    {
        throw new RuntimeException("Security Exception.  This method is not accessibly within the current context.");
    }

    @Override
    public T8Context createServerModuleContext(T8ServerModule managerObject)
    {
        throw new RuntimeException("Security Exception.  This method is not accessibly within the current context.");
    }

    @Override
    public T8SessionContext createNewSessionContext() throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        BasicService basicService;
        ResponseType responseType;
        T8Session tempSession;
        String serverURL;
        String guid;

        // Create a temporary session to use during dispatch of new session request.
        guid = getNewGUID();
        tempSession = new T8Session(guid, "ANONYMOUS_" + guid, "ANONYMOUS_" + guid); // Anonymous user always has the session identifier as part of the user and user profile identifers.

        // Set the server URL.
        basicService = (BasicService)ServiceManager.lookup("javax.jnlp.BasicService");
        serverURL = basicService.getCodeBase().toString();
        if (serverURL.endsWith("/")) serverURL = serverURL.substring(0, serverURL.length() - 1);
        LOGGER.log("Using server url: " + serverURL);
        tempSession.setServerURL(serverURL);

        // Request a new session from the server.
        serverRequest = new T8ServerRequest(new T8Context(clientContext, tempSession), RequestType.CREATE_NEW_SESSION, null);
        serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/create_session", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            throw (Exception)serverResponse.getResponseData();
        }
        else if (responseType == ResponseType.SESSION_CONTEXT)
        {
            T8SessionContext authenticSession;

            // Get the authenticated session from the server response.
            authenticSession = (T8SessionContext)serverResponse.getResponseData();
            if (authenticSession != null)
            {
                // Make sure this property is set.
                ((T8Session)authenticSession).setServerURL(serverURL);
                return authenticSession;
            }
            throw new Exception("Could not create new session!");
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    @Override
    public void lockUser(T8Context context, String userIdentifier, String lockReason) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_USER_ID, userIdentifier);
        operationParameters.put(PARAMETER_USER_ID, lockReason);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_CHANGE_PASSWORD, operationParameters);
    }

    @Override
    public void unlockUser(T8Context context, String userIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_USER_ID, userIdentifier);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_CHANGE_PASSWORD, operationParameters);
    }

    @Override
    public T8UserLoginResponse login(T8Context context, LoginLocation loginLocation, String userIdentifier, char[] password, boolean endExistingSession) throws Exception
    {
        // Acquire the client exclusive lock so that we can update the session context before proceeding with any other server requests.
        T8MainServerClient.acquireLock();

        try
        {
            T8ServerRequest serverRequest;
            T8ServerResponse serverResponse;
            ResponseType responseType;

            serverRequest = new T8ServerRequest(context, RequestType.LOGIN, new T8UserLoginRequest(loginLocation, userIdentifier, password, endExistingSession));
            serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/login", null);
            responseType = serverResponse.getResponseType();
            if (responseType == ResponseType.THROWABLE)
            {
                throw (Exception)serverResponse.getResponseData();
            }
            else if (responseType == ResponseType.LOGIN_RESPONSE)
            {
                T8SessionContext authenticSession;
                T8UserLoginResponse loginResponse;

                // Get the authenticated session from the server response.
                loginResponse = (T8UserLoginResponse)serverResponse.getResponseData();
                authenticSession = loginResponse.getSessionContext();
                if ((authenticSession != null) && (loginResponse.getResponseType().isSessionAuthenticated()))
                {
                    T8SessionContext localSessionContext;
                    String serverUrl;

                    // The serverUrl is a client-side property and must therefore be kept separately because the server will send back a null value in the newly created authentic session.
                    localSessionContext = context.getSessionContext();
                    serverUrl = localSessionContext.getServerURL();

                    // Update the client-side session object with the authenticated session content.
                    ((T8Session)localSessionContext).updateSession(authenticSession);

                    // Restore the serverUrl and return the login response.
                    ((T8Session)localSessionContext).setServerURL(serverUrl);

                    // Update the context to reflect changes to the session context.
                    context.resetSessionContext();
                    return loginResponse;
                }
                else return loginResponse;
            }
            else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
        }
        finally
        {
            // Make sure to always release the lock.
            T8MainServerClient.releaseLock();
        }
    }

    @Override
    public boolean logout(T8Context context) throws Exception
    {
        // Acquire the client exclusive lock so that we can update the session context before proceeding with any other server requests.
        T8MainServerClient.acquireLock();

        try
        {
            T8ServerRequest serverRequest;
            T8ServerResponse serverResponse;
            ResponseType responseType;

            serverRequest = new T8ServerRequest(context, RequestType.LOGOUT, null);
            serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/logout", null);
            responseType = serverResponse.getResponseType();
            if (responseType == ResponseType.THROWABLE)
            {
                throw (Exception)serverResponse.getResponseData();
            }
            else if (responseType == ResponseType.SESSION_CONTEXT)
            {
                T8SessionContext authenticSession;

                // Get the authenticated session from the server response.
                authenticSession = (T8SessionContext)serverResponse.getResponseData();
                if (authenticSession != null)
                {
                    // Update the client-side session to reflect the fact that it has now been logged out.
                    ((T8Session)context.getSessionContext()).anonymize();
                    return true;
                }
                else return false;
            }
            else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
        }
        finally
        {
            // Make sure to always release the lock.
            T8MainServerClient.releaseLock();
        }
    }

    @Override
    public T8PasswordChangeResponse changePassword(T8Context context, char[] password) throws Exception
    {
        HashMap<String, Object> operationParameters;
        Map<String, Object> outputParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_PASSWORD, password);
        outputParameters = T8MainServerClient.executeSynchronousOperation(context, OPERATION_CHANGE_PASSWORD, operationParameters);
        return new T8PasswordChangeResponse(context.getSessionContext().getUserIdentifier(), (Boolean)outputParameters.get(PARAMETER_SUCCESS), (String)outputParameters.get(PARAMETER_MESSAGE));
    }

    @Override
    public List<T8DefinitionMetaData> getAccessibleUserProfileMetaData(T8Context context) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        return (List<T8DefinitionMetaData>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_ACCESSIBLE_USER_PROFILE_META_DATA, operationParameters).get(PARAMETER_USER_PROFILE_META_DATA);
    }

    @Override
    public List<String> getAccessibleUserProfileIds(T8Context context) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        return (List<String>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_ACCESSIBLE_USER_PROFILE_IDS, operationParameters).get(PARAMETER_USER_PROFILE_IDS);
    }

    @Override
    public boolean switchUserProfile(T8Context context, String userProfileIdentifier) throws Exception
    {
        // Acquire the client exclusive lock so that we can update the session context before proceeding with any other server requests.
        T8MainServerClient.acquireLock();

        try
        {
            T8ServerRequest serverRequest;
            T8ServerResponse serverResponse;
            ResponseType responseType;

            LOGGER.log("Client switching profile to: " + userProfileIdentifier);
            serverRequest = new T8ServerRequest(context, RequestType.SWITCH_USER_PROFILE, userProfileIdentifier);
            serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/switch_profile", null);
            responseType = serverResponse.getResponseType();
            if (responseType == ResponseType.THROWABLE)
            {
                throw (Exception)serverResponse.getResponseData();
            }
            else if (responseType == ResponseType.SESSION_CONTEXT)
            {
                T8SessionContext authenticSession;

                // Get the authenticated session from the server response.
                authenticSession = (T8SessionContext)serverResponse.getResponseData();
                LOGGER.log("Client got response: " + authenticSession);
                if (authenticSession != null)
                {
                    T8SessionContext localSessionContext;
                    String serverUrl;

                    // The serverUrl is a client-side property and must therefore be kept separately because the server will send back a null value in the newly created authentic session.
                    localSessionContext = context.getSessionContext();
                    serverUrl = localSessionContext.getServerURL();

                    // Update the client-side session object witht the authenticated session content.
                    LOGGER.log("Client updating session profile to: " + authenticSession.getUserProfileIdentifier());
                    ((T8Session)localSessionContext).updateSession(authenticSession);

                    // Restore the serverUrl and return the login response.
                    ((T8Session)localSessionContext).setServerURL(serverUrl);

                    // Update the context to reflect changes to the session context.
                    context.resetSessionContext();
                    return true;
                }
                else return false;
            }
            else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
        }
        finally
        {
            // Make sure to always release the lock.
            T8MainServerClient.releaseLock();
        }
    }

    @Override
    public T8SessionContext switchSessionLanguage(T8Context context, String languageId) throws Exception
    {
        // Acquire the client exclusive lock so that we can update the session context before proceeding with any other server requests.
        T8MainServerClient.acquireLock();

        try
        {
            HashMap<String, Object> operationParameters;
            T8SessionContext authenticSessionContext;
            T8SessionContext localSessionContext;
            String serverUrl;

            // The serverUrl is a client-side property and must therefore be kept separately because the server will send back a null value in the newly created authentic session.
            localSessionContext = context.getSessionContext();
            serverUrl = localSessionContext.getServerURL();

            operationParameters = new HashMap<>();
            operationParameters.put(PARAMETER_LANGUAGE_ID, languageId);
            authenticSessionContext = (T8SessionContext) T8MainServerClient.executeSynchronousOperation(context, OPERATION_SWITCH_SESSION_USER_LANUAGE, operationParameters).get(PARAMETER_SESSION_CONTEXT);

            // Update the client-side session object witht the authenticated session content.
            ((T8Session)localSessionContext).updateSession(authenticSessionContext);

            // Restore the serverUrl and return the login response.
            ((T8Session)localSessionContext).setServerURL(serverUrl);

            // Update the context to reflect changes to the session context.
                    context.resetSessionContext();
            return localSessionContext;
        }
        finally
        {
            // Make sure to always release the lock.
            T8MainServerClient.releaseLock();
        }
    }

    private static String getNewGUID()
    {
        UUID id;

        id = UUID.randomUUID();
        return id.toString().replace("-", "").toUpperCase();
    }

    @Override
    public void addSessionEventListener(T8SessionEventListener tl)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeSessionEventListener(T8SessionEventListener tl)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8SessionDetails getSessionDetails(T8Context context) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        return (T8SessionDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_SESSION_DETAILS, operationParameters).get(PARAMETER_SESSION_DETAILS);
    }

    @Override
    public List<T8SessionDetails> getAllSessionDetails(T8Context context) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        return (List<T8SessionDetails>)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_ALL_SESSION_DETAILS, operationParameters).get(PARAMETER_SESSION_DETAILS_LIST);
    }

    @Override
    public boolean isSessionActive(T8Context context, String sessionIdentifier) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_SESSION_ID, sessionIdentifier);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_IS_SESSION_ACTIVE, operationParameters).get(PARAMETER_ACTIVE);
    }

    @Override
    public T8UserUpdateResponse createNewUser(T8Context context, T8UserDetails userDetails) throws Exception
    {
        Map<String, Object> operationParameters;
        Map<String, Object> outputParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_USER_DETAILS, userDetails);
        outputParameters = T8MainServerClient.executeSynchronousOperation(context, OPERATION_CREATE_NEW_USER, operationParameters);
        return new T8UserUpdateResponse((String)outputParameters.get(PARAMETER_USER_ID), (Boolean)outputParameters.get(PARAMETER_SUCCESS), (String)outputParameters.get(PARAMETER_MESSAGE), null);
    }

    @Override
    public T8UserUpdateResponse updateUserDetails(T8Context context, T8UserDetails userDetails) throws Exception
    {
        Map<String, Object> operationParameters;
        Map<String, Object> outputParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_USER_DETAILS, userDetails);
        outputParameters = T8MainServerClient.executeSynchronousOperation(context, OPERATION_UPDATE_USER_DETAILS, operationParameters);
        return new T8UserUpdateResponse(userDetails.getIdentifier(), (Boolean)outputParameters.get(PARAMETER_SUCCESS), (String)outputParameters.get(PARAMETER_MESSAGE), null);
    }

    @Override
    public T8UserDetails getUserDetails(T8Context context, String userId) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_USER_ID, userId);
        return (T8UserDetails)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_USER_DETAILS, operationParameters).get(PARAMETER_USER_DETAILS);
    }

    @Override
    public boolean deleteUser(T8Context context, String userId) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_USER_ID, userId);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_CREATE_NEW_USER, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public boolean resetUserPassword(T8Context context, String userId) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_USER_ID, userId);
        return (Boolean)T8MainServerClient.executeSynchronousOperation(context, OPERATION_RESET_USER_PASSWORD, operationParameters).get(PARAMETER_SUCCESS);
    }

    @Override
    public List<String> getAccessibleFunctionalityIds(T8Context context) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8PasswordChangeResponse changePassword(T8Context context, String userId, char[] password) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.CHANGE_PASSWORD, new T8PasswordChangeRequest(userId, password));
        serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/change_password", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            throw (Exception)serverResponse.getResponseData();
        }
        else if(responseType == ResponseType.PASSWORD_CHANGE_RESPONSE)
        {
            return (T8PasswordChangeResponse)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    @Override
    public boolean isPasswordChangeAvailable(T8Context context, String userId) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.CHECK_PASSWORD_CHANGE_AVAILABILITY, userId);
        serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/password_change_available", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            throw (Exception)serverResponse.getResponseData();
        }
        else if(responseType == ResponseType.SUCCESS)
        {
            return (Boolean)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    @Override
    public boolean isUsernameAvailable(T8Context context, String userIdentifier) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.CHECK_USERNAME_AVAILABILITY, userIdentifier);
        serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/username_available", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            throw (Exception)serverResponse.getResponseData();
        }
        else if(responseType == ResponseType.SUCCESS)
        {
            return (Boolean)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    @Override
    public boolean isUserRegistered(T8Context context, String userId) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.CHECK_USER_REGISTERED, userId);
        serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/user_registered", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            throw (Exception)serverResponse.getResponseData();
        }
        else if(responseType == ResponseType.SUCCESS)
        {
            return (Boolean)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    @Override
    public boolean requestPasswordChange(T8Context context, String userId) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.REQUEST_PASSWORD_CHANGE, userId);
        serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/request_password_change", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            throw (Exception)serverResponse.getResponseData();
        }
        else if(responseType == ResponseType.SUCCESS)
        {
            return (Boolean)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    @Override
    public String getWebServiceUserApiKey(T8Context context) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        return (String)T8MainServerClient.executeSynchronousOperation(context, T8SecurityManagerResource.OPERATION_GET_WEB_SERVICE_USER_API_KEY, operationParameters).get(T8SecurityManagerResource.PARAMETER_API_KEY);
    }

    @Override
    public void generateWebServiceUserApiKey(T8Context context)
    {
        Map<String, Object> operationParameters;

        try
        {
            operationParameters = new HashMap<>();
            T8MainServerClient.executeSynchronousOperation(context, T8SecurityManagerResource.OPERATION_GENERATE_WEB_SERVICE_USER_API_KEY, operationParameters);
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to generate api key for user", ex);
        }
    }

    @Override
    public void setUserActive(T8Context context, String userId, boolean active) throws Exception
    {
        Map<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_USER_ID, userId);
        operationParameters.put(PARAMETER_ACTIVE, active);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_SET_USER_ACTIVE, operationParameters);
    }

    public void activeUser(T8Context context, String userId) throws Exception
    {
        setUserActive(context, userId, true);
    }

    public void deactiveUser(T8Context context, String userId) throws Exception
    {
        setUserActive(context, userId, false);
    }

    @Override
    public T8RemoteConnectionUser getRemoteConnectionUser(T8Context context, String connectionIdentifier)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void start() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void extendSession(T8Context context) throws Exception
    {
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_EXTEND_USER_SESSION, new HashMap<>());
    }

    @Override
    public T8AuthenticationResponse authenticate(T8HttpServletRequest request)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public T8AuthenticationResponse authenticate(T8Context context, String authorizationHeaderValue) throws Exception
    {
        // Acquire the client exclusive lock so that we can update the session context before proceeding with any other server requests.
        T8MainServerClient.acquireLock();

        try
        {
            T8ServerRequest serverRequest;
            T8ServerResponse serverResponse;
            ResponseType responseType;

            serverRequest = new T8ServerRequest(context, RequestType.AUTHENTICATE, null);
            serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/authenticate", HashMaps.newHashMap("authorization", authorizationHeaderValue));
            responseType = serverResponse.getResponseType();
            if (responseType == ResponseType.THROWABLE)
            {
                throw (Exception)serverResponse.getResponseData();
            }
            else if (responseType == ResponseType.AUTHENTICATION_RESPONSE)
            {
                T8AuthenticationResponse authenticationResponse;

                // Get the authenticated session from the server response.
                authenticationResponse = (T8AuthenticationResponse)serverResponse.getResponseData();
                if (authenticationResponse.getResponseType() == T8AuthenticationResponse.ResponseType.COMPLETED)
                {
                    T8SessionContext authenticatedSession;
                    T8SessionContext localSessionContext;
                    String serverUrl;

                    // Get the authenticated session.
                    authenticatedSession = authenticationResponse.getSessionContext();
                    localSessionContext = context.getSessionContext();

                    // The serverUrl is a client-side property and must therefore be kept separately because the server will send back a null value in the newly created authentic session.
                    serverUrl = localSessionContext.getServerURL();

                    // Update the client-side session object with the authenticated session content.
                    ((T8Session)localSessionContext).updateSession(authenticatedSession);

                    // Restore the serverUrl and return the login response.
                    ((T8Session)localSessionContext).setServerURL(serverUrl);

                    // Update the context to reflect changes to the session context.
                    context.resetSessionContext();
                    return authenticationResponse;
                }
                else return authenticationResponse;
            }
            else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
        }
        finally
        {
            // Make sure to always release the lock.
            T8MainServerClient.releaseLock();
        }
    }

    @Override
    public T8Context createContext(T8SessionContext sessionContext)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroyContext()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8Context getContext()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSessionPerformanceStatisticsEnabled(T8Context context, String sessionId, boolean enabled) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_SESSION_ID, sessionId);
        operationParameters.put(PARAMETER_ENABLED, enabled);
        T8MainServerClient.executeSynchronousOperation(context, OPERATION_SET_SESSION_PERFORMANCE_STATISTICS_ENABLED, operationParameters);
    }

    @Override
    public T8PerformanceStatistics getSessionPerformanceStatistics(T8Context context, String sessionId) throws Exception
    {
        HashMap<String, Object> operationParameters;

        operationParameters = new HashMap<>();
        operationParameters.put(PARAMETER_SESSION_ID, sessionId);
        return (T8PerformanceStatistics)T8MainServerClient.executeSynchronousOperation(context, OPERATION_GET_SESSION_PERFORMANCE_STATISTICS, operationParameters).get(PARAMETER_PERFORMANCE_STATISTICS);
    }

    @Override
    public List<T8UserProfileDetails> getAccessibleUserProfileDetails(T8Context context) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8Context accessContext(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
