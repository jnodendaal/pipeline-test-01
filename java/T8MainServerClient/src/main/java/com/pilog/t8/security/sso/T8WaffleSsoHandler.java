package com.pilog.t8.security.sso;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8Log;
import com.pilog.t8.datastructures.tuples.Pair;
import com.pilog.t8.definition.security.sso.T8WaffleSsoHandlerDefinition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8ClientSecurityManager;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.authentication.T8AuthenticationResponse;
import com.pilog.t8.security.authentication.T8AuthenticationResponse.ResponseType;
import com.pilog.t8.security.authentication.T8SingleSignOnHandler;
import com.pilog.t8.security.authentication.T8WaffleAuthenticationResponse;
import com.pilog.t8.utilities.codecs.Base64Codec;
import com.pilog.t8.utilities.strings.Strings;
import com.sun.jna.platform.win32.Sspi;
import com.sun.jna.platform.win32.Sspi.SecBufferDesc;
import waffle.windows.auth.IWindowsSecurityContext;
import waffle.windows.auth.impl.WindowsSecurityContextImpl;

/**
 * @author Bouwer du Preez
 */
public class T8WaffleSsoHandler implements T8SingleSignOnHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8WaffleSsoHandler.class);
    private final T8WaffleSsoHandlerDefinition definition;
    private final T8ClientSecurityManager securityManager;
    private final T8ClientContext clientContext;
    private final T8Context context;

    public T8WaffleSsoHandler(T8Context context, T8WaffleSsoHandlerDefinition definition)
    {
        this.definition = definition;
        this.context = context;
        this.clientContext = context.getClientContext();
        this.securityManager = (T8ClientSecurityManager)clientContext.getSecurityManager();
    }

    @Override
    public boolean signOn()
    {
        try
        {
            IWindowsSecurityContext securityContext;
            Pair<ResponseType, byte[]> response1;
            byte[] tokenToServer;
            String targetName;
            String securityPackage;

            // Initialize a security context.
            securityPackage = definition.getSecurityPackage();
            targetName = Strings.trimToNull(definition.getTargetName());
            securityContext = WindowsSecurityContextImpl.getCurrent(securityPackage, targetName);

            // Send the first token to the server.
            tokenToServer = securityContext.getToken();
            response1 = sendAuthenticationRequest(tokenToServer);
            if (response1.getValue1() == ResponseType.CONTINUED)
            {
                Pair<ResponseType, byte[]> response2;
                SecBufferDesc continueToken;

                // Reply with the second token to the server.
                continueToken = new Sspi.SecBufferDesc(Sspi.SECBUFFER_TOKEN, response1.getValue2());
                securityContext.initialize(securityContext.getHandle(), continueToken, targetName);
                tokenToServer = securityContext.getToken();
                response2 = sendAuthenticationRequest(tokenToServer);

                // Check the second response type and return the result.
                if (response2.getValue1() == ResponseType.COMPLETED)
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }
        catch (Exception e)
        {
            LOGGER.log("Single-sign-on failure.", e);
            return false;
        }
    }

    private Pair<ResponseType, byte[]> sendAuthenticationRequest(byte[] token) throws Exception
    {
        T8AuthenticationResponse authenticationResponse;
        ResponseType responseType;
        String header;

        header = "WafSec-Token ";
        header += new String(Base64Codec.encode(token));

        authenticationResponse = securityManager.authenticate(context, header);
        responseType = authenticationResponse.getResponseType();
        if (responseType == ResponseType.CONTINUED)
        {
            return new Pair<>(responseType, ((T8WaffleAuthenticationResponse)authenticationResponse).getToken());
        }
        else return new Pair<>(responseType, null);
    }
}
