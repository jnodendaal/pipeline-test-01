package com.pilog.mainserver;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class T8MainServerClientStatistics
{
    private final int maximumCacheSize = 20;
    private int totalBytesSent;
    private int totalBytesReceived;
    private final List<Integer> bytesSent;
    private final List<Integer> bytesReceived;
    private final List<Long> responseTimes;
    private final List<Double> throughputs;

    public T8MainServerClientStatistics()
    {
        this.bytesSent = new ArrayList<>(maximumCacheSize);
        this.bytesReceived = new ArrayList<>(maximumCacheSize);
        this.responseTimes = new ArrayList<>(maximumCacheSize);
        this.throughputs = new ArrayList<>(maximumCacheSize);
    }

    protected void addSentBytes(Integer sentSize, long transferTime)
    {
        if (sentSize != null)
        {
            bytesSent.add(sentSize);
            totalBytesSent += sentSize;
            if (bytesSent.size() > maximumCacheSize) bytesSent.remove(0);
            
            addThroughput(sentSize, transferTime);
        }
    }

    protected void addRecievedBytes(Integer recievedSize, long transferTime)
    {
        if (recievedSize != null)
        {
            bytesReceived.add(recievedSize);
            totalBytesReceived += recievedSize;
            if (bytesReceived.size() > maximumCacheSize) bytesReceived.remove(0);
            
            addThroughput(recievedSize, transferTime);
        }
    }
    
    private void addThroughput(int bytes, long time)
    {
        if (time > 0)
        {
            double throughput;

            throughput = (double)bytes*8.0/(double)time*1000.00;
            throughputs.add(throughput);
            if (throughputs.size() > maximumCacheSize) throughputs.remove(0);
        }
    }

    protected void addServerResponseTime(Long responseTime)
    {
        responseTimes.add(responseTime);
        if (responseTimes.size() > maximumCacheSize) responseTimes.remove(0);
    }

    public Integer getTotalBytesSent()
    {
        return totalBytesSent;
    }

    public Integer getTotalBytesReceived()
    {
        return totalBytesReceived;
    }

    public Integer getAverageBytesSent()
    {
        int bytesSentTotal;

        bytesSentTotal = 0;
        for (Integer integer : bytesSent)
        {
            bytesSentTotal += integer;
        }

        return bytesSent.isEmpty() ? 0 : bytesSentTotal / bytesSent.size();
    }

    public Integer getAverageBytesReceived()
    {
        int bytesSentTotal;

        bytesSentTotal = 0;
        for (Integer integer : bytesReceived)
        {
            bytesSentTotal += integer;
        }

        return bytesReceived.isEmpty() ? 0 : bytesSentTotal / bytesReceived.size();
    }

    public Long getAverageServerResponseTime()
    {
        long totalResponseTime;

        totalResponseTime = 0;
        for (Long responseTime : responseTimes)
        {
            totalResponseTime += responseTime;
        }

        return responseTimes.isEmpty() ? 0 : totalResponseTime / responseTimes.size();
    }
    
    /**
     * Returns the average recorded network through in bit/s.
     * @return 
     */
    public double getAverageNetworkThroughput()
    {
        double totalThroughput;

        totalThroughput = 0;
        for (double throughput : throughputs)
        {
            totalThroughput += throughput;
        }

        return throughputs.isEmpty() ? 0.0 : totalThroughput / throughputs.size();
    }
}
