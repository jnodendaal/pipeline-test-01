package com.pilog.mainserver;

import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.exception.T8ServerOperationException;
import com.pilog.t8.commons.stream.MonitorInputStream;
import com.pilog.t8.commons.stream.MonitorOuputStream;
import com.pilog.t8.mainserver.T8MainServerRuntimeData;
import com.pilog.t8.mainserver.T8OperationExecutionRequest;
import com.pilog.t8.mainserver.T8OperationExecutionRequest.ExecutionType;
import com.pilog.t8.mainserver.T8ServerRequest;
import com.pilog.t8.mainserver.T8ServerRequest.RequestType;
import com.pilog.t8.mainserver.T8ServerResponse;
import com.pilog.t8.mainserver.T8ServerResponse.ResponseType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8HeartbeatResponse;
import com.pilog.t8.utilities.threads.ThreadGate;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * This class implements the client-side relay to the MainServer and handles all
 * requests that need to be sent to the server.
 *
 * @author Bouwer du Preez
 */
public class T8MainServerClient
{
    private static final String SERVLET_MAPPING = "/java_api";
    private static final ThreadGate EXCLUSIVE_USE_GATE = new ThreadGate(true, true);
    private static final T8MainServerClientStatistics STATISTICS = new T8MainServerClientStatistics();
    private static long clientTimeDifference = 0;

    /**
     * Acquires an exclusive-use lock on the client.  When acquired by a thread,
     * only that thread is allowed to send communication to the server until the
     * lock is released.
     */
    public static final void acquireLock()
    {
        EXCLUSIVE_USE_GATE.close();
    }

    /**
     * Releases the exclusive-use lock on the client.  Only the thread that
     * currently holds the lock can invoke this method.
     */
    public static final void releaseLock()
    {
        EXCLUSIVE_USE_GATE.open();
    }

    /**
     * Returns the difference between client and server times.  A negative value
     * indicates that client-time is behind server time by the specified number
     * of milliseconds.  The time difference is updated every time a heartbeat
     * is performed.
     * @return The number of milliseconds difference between client and server
     * time (i.e. difference = clientTime - serverTime).
     */
    public static final long getClientTimeDifference()
    {
        return clientTimeDifference;
    }

    public static final T8MainServerClientStatistics getStatistics()
    {
        return STATISTICS;
    }

    public static final T8HeartbeatResponse heartbeat(T8Context context) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.HEARTBEAT, null);
        serverResponse = T8MainServerClient.postServerRequest(serverRequest, "/heartbeat", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            throw (Exception)serverResponse.getResponseData();
        }
        else if (responseType == ResponseType.HEARTBEAT)
        {
            T8HeartbeatResponse response;

            // Get the heartbeat response from the server response.
            response = (T8HeartbeatResponse)serverResponse.getResponseData();

            // If we got a valid heartbeat response, update the client-server time difference.
            if (response != null)
            {
                long serverTime;
                long currentTime;
                long responseTime;

                // Record the current time and calculate the total response time.
                currentTime = System.currentTimeMillis();
                responseTime = (currentTime - serverRequest.getTimeSent());

                // Update the client time difference.
                serverTime = response.getServerTime();
                serverTime = (long)(serverTime + (responseTime / 2.0)); // We add half of the total response time, to create an estimate that takes latency into account.
                clientTimeDifference = response == null ? 0 : (currentTime - serverTime);
            }

            // Return the response.
            return response;
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Restarts the server, reloading all meta data and re-initializing all
     * services.
     *
     * @param context The context within which this request will be executed.
     * @return The boolean result success/failure of the operation.
     * @throws Exception
     */
    public static final boolean restartServer(T8Context context) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.RESTART_SERVER, null);
        serverResponse = postServerRequest(serverRequest, "/restart_server", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            throw new InvocationTargetException((Throwable)serverResponse.getResponseData(), "Server-side Exception occurred.");
        }
        else if (responseType == ResponseType.SUCCESS)
        {
            return (Boolean)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Returns a report of the current server runtime data.
     *
     * @param context The context within which this request will be executed.
     * @return The runtime data fetched from the server.
     * @throws Exception
     */
    public static final T8MainServerRuntimeData getServerRuntimeData(T8Context context) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.GET_SERVER_RUNTIME_DATA, null);
        serverResponse = postServerRequest(serverRequest, "/get_server_runtime_data", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            throw new InvocationTargetException((Throwable)serverResponse.getResponseData(), "Server-side Exception occurred.");
        }
        else if (responseType == ResponseType.SERVER_RUNTIME_DATA)
        {
            return (T8MainServerRuntimeData)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Returns a status report of an operation that is currently executing on
     * the server.  If this method is invoked for an operation that has
     * completed execution on the server, a status report reflecting its
     * 'completed' status will be returned and the report will also contain the
     * result of the completed operation.  Additionally the operation will be
     * removed from the server's collection of operations so that subsequent
     * calls of this method will result in an exception (since the operation
     * will no longer be found on the server).
     *
     * @param context The context within which this request will be executed.
     * @param operationIid The unique iid of the operation for which
     * to fetch a status report.
     * @return The status report for the specified operation.
     * @throws Exception
     */
    public static final T8ServerOperationStatusReport getOperationStatus(T8Context context, String operationIid) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.GET_OPERATION_STATUS, operationIid);
        serverResponse = postServerRequest(serverRequest, "/get_operation_status/" + operationIid, null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            Throwable serverException;
            Throwable cause;

            serverException = (Throwable) serverResponse.getResponseData();
            cause = serverException.getCause();
            if(serverException instanceof T8ServerOperationException && cause instanceof Exception)
            {
                throw (Exception)cause;
            }
            else
                throw new InvocationTargetException(serverException, "Server-side Exception occurred.");
        }
        else if (responseType == ResponseType.OPERATION_STATUS)
        {
            return (T8ServerOperationStatusReport)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Returns a list containing the operation status of each of the requested
     * operations currently executing on the server.
     *
     * @param context The context within which this request will be executed.
     * @param operationIids A Set of all the operation iids for which to retrieve
     * a status from the server.
     * @return A list of status report for all operations currently executing on
     * the server.
     * @throws Exception
     */
    public static final ArrayList<T8ServerOperationStatusReport> getOperationStatuses(T8Context context, HashSet<String> operationIids) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.GET_OPERATION_STATUSES, operationIids);
        serverResponse = postServerRequest(serverRequest, "/get_operation_statuses", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            Throwable serverException;
            Throwable cause;

            serverException = (Throwable) serverResponse.getResponseData();
            cause = serverException.getCause();
            if(serverException instanceof T8ServerOperationException && cause instanceof Exception)
            {
                throw (Exception)cause;
            }
            else
                throw new InvocationTargetException(serverException, "Server-side Exception occurred.");
        }
        else if(responseType == ResponseType.OPERATION_STATUSES)
        {
            return (ArrayList<T8ServerOperationStatusReport>)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Returns a list containing the operation status of each operation
     * currently executing on the server.  This method should only be used by
     * administration software and should never be invoked by regular
     * client-side code.
     *
     * @param context The context within which this request will be executed.
     * @return A list of status report for all operations currently executing on
     * the server.
     * @throws Exception
     */
    public static final ArrayList<T8ServerOperationStatusReport> getAllOperationStatuses(T8Context context) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.GET_ALL_OPERATION_STATUSES, null);
        serverResponse = postServerRequest(serverRequest, "/get_all_operation_statuses", null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            Throwable serverException;
            Throwable cause;

            serverException = (Throwable) serverResponse.getResponseData();
            cause = serverException.getCause();
            if(serverException instanceof T8ServerOperationException && cause instanceof Exception)
            {
                throw (Exception)cause;
            }
            else
                throw new InvocationTargetException(serverException, "Server-side Exception occurred.");
        }
        else if(responseType == ResponseType.OPERATION_STATUSES)
        {
            return (ArrayList<T8ServerOperationStatusReport>)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Stops an operation that is currently executing on the server.  This
     * differs from canceling the operation in that no rollback or reverting of
     * already completed steps will occur.
     *
     * @param context The context within which this request will be executed.
     * @param operationIid The unique iid the operation to stop.
     * @return The status report for the operation after it was stopped.
     * @throws Exception
     */
    public static final T8ServerOperationStatusReport stopOperation(T8Context context, String operationIid) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.STOP_OPERATION, operationIid);
        serverResponse = postServerRequest(serverRequest, "/stop_operation/" + operationIid, null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            Throwable serverException;
            Throwable cause;

            serverException = (Throwable) serverResponse.getResponseData();
            cause = serverException.getCause();
            if(serverException instanceof T8ServerOperationException && cause instanceof Exception)
            {
                throw (Exception)cause;
            }
            else
                throw new InvocationTargetException(serverException, "Server-side Exception occurred.");
        }
        else if(responseType == ResponseType.OPERATION_STATUS)
        {
            return (T8ServerOperationStatusReport)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Cancels an operation that is currently executing on the server.  A
     * rollback or reverting of already completed steps will be attempted,
     * although not all operations support this feature.
     *
     * @param context The context within which this request will be executed.
     * @param operationIid The unique iid of the operation to cancel.
     * @return The status report for the operation after it was canceled.
     * @throws Exception
     */
    public static final T8ServerOperationStatusReport cancelOperation(T8Context context, String operationIid) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.CANCEL_OPERATION, operationIid);
        serverResponse = postServerRequest(serverRequest, "/cancel_operation/" + operationIid, null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            Throwable serverException;
            Throwable cause;

            serverException = (Throwable) serverResponse.getResponseData();
            cause = serverException.getCause();
            if(serverException instanceof T8ServerOperationException && cause instanceof Exception)
            {
                throw (Exception)cause;
            }
            else
                throw new InvocationTargetException(serverException, "Server-side Exception occurred.");
        }
        else if(responseType == ResponseType.OPERATION_STATUS)
        {
            return (T8ServerOperationStatusReport)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Suspends the execution of a specified operation.  Not all operations
     * support this feature.
     *
     * @param context The context within which this request will be executed.
     * @param operationIid The unique of the operation to pause.
     * @return The status report for the operation after it was paused.
     * @throws Exception
     */
    public static final T8ServerOperationStatusReport pauseOperation(T8Context context, String operationIid) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.PAUSE_OPERATION, operationIid);
        serverResponse = postServerRequest(serverRequest, "/pause_operation/" + operationIid, null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            Throwable serverException;
            Throwable cause;

            serverException = (Throwable) serverResponse.getResponseData();
            cause = serverException.getCause();
            if(serverException instanceof T8ServerOperationException && cause instanceof Exception)
            {
                throw (Exception)cause;
            }
            else
                throw new InvocationTargetException(serverException, "Server-side Exception occurred.");
        }
        else if(responseType == ResponseType.OPERATION_STATUS)
        {
            return (T8ServerOperationStatusReport)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Resumes an operation that was previously paused.  Not all operations
     * support this feature.
     *
     * @param context The context within which this request will be executed.
     * @param operationIid The unique iid identifying the operation to resume.
     * @return The status report for the operation after it was resumed.
     * @throws Exception
     */
    public static final T8ServerOperationStatusReport resumeOperation(T8Context context, String operationIid) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        ResponseType responseType;

        serverRequest = new T8ServerRequest(context, RequestType.RESUME_OPERATION, operationIid);
        serverResponse = postServerRequest(serverRequest, "/resume_operation/" + operationIid, null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            Throwable serverException;
            Throwable cause;

            serverException = (Throwable) serverResponse.getResponseData();
            cause = serverException.getCause();
            if(serverException instanceof T8ServerOperationException && cause instanceof Exception)
            {
                throw (Exception)cause;
            }
            else
                throw new InvocationTargetException(serverException, "Server-side Exception occurred.");
        }
        else if(responseType == ResponseType.OPERATION_STATUS)
        {
            return (T8ServerOperationStatusReport)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Executes a server operation asynchronously which means that the operation
     * will be started on the server and this method will immediately return
     * with a status report on the progress of the operation even though it is
     * still continuing to execute.  The returned status report will contain the
     * operation iids of the operation executing on the server which can then be
     * used to repeatedly poll the server operation's progress until it has
     * completed.
     *
     * @param context The context within which this request will be executed.
     * @param operationId The identifier of the operation to execute.
     * @param operationParameters The parameters to pass to the operation.
     * @return The T8ServerOperationStatusReport obtained from the executing operation
     * directly after it was started.  This report will contain the unique iids
     * identifying the operation executing on the server.
     * @throws Exception Any exception that occurs during the execution of this
     * method either on the server or on the client.
     */
    public static final T8ServerOperationStatusReport executeAsynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        T8OperationExecutionRequest executionRequest;
        ResponseType responseType;

        executionRequest = new T8OperationExecutionRequest(operationId, ExecutionType.ASYNCHRONOUS, operationParameters);
        serverRequest = new T8ServerRequest(context, RequestType.EXECUTE_OPERATION, executionRequest);
        serverResponse = postServerRequest(serverRequest, "/operation/" + operationId, null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            Throwable serverException;
            Throwable cause;

            serverException = (Throwable) serverResponse.getResponseData();
            cause = serverException.getCause();
            if(serverException instanceof T8ServerOperationException && cause instanceof Exception)
            {
                throw (Exception)cause;
            }
            else
                throw new InvocationTargetException(serverException, "Server-side Exception occurred.");
        }
        else if (responseType == ResponseType.OPERATION_STATUS)
        {
            return (T8ServerOperationStatusReport)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Executes a server operation synchronously which means that this method
     * will block until the server-side operation has completed execution and
     * will then return the result of the operation as received from the server.
     *
     * @param context The security access rights required by this request.
     * @param operationId The identifier of the operation to execute.
     * @param operationParameters The parameters to pass to the operation.
     * @return The result of the operation after execution.
     * @throws Exception Any exception that occurs during the execution of the
     * operation.
     */
    public static final Map<String, Object> executeSynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        T8ServerRequest serverRequest;
        T8ServerResponse serverResponse;
        T8OperationExecutionRequest executionRequest;
        ResponseType responseType;

        executionRequest = new T8OperationExecutionRequest(operationId, ExecutionType.SYNCHRONOUS, operationParameters);
        serverRequest = new T8ServerRequest(context, RequestType.EXECUTE_OPERATION, executionRequest);
        serverResponse = postServerRequest(serverRequest, "/operation/" + operationId, null);
        responseType = serverResponse.getResponseType();
        if (responseType == ResponseType.THROWABLE)
        {
            Throwable serverException;
            Throwable cause;

            serverException = (Throwable) serverResponse.getResponseData();
            cause = serverException.getCause();
            if(serverException instanceof T8ServerOperationException && cause instanceof Exception)
            {
                throw (Exception)cause;
            }
            else
                throw new InvocationTargetException(serverException, "Server-side Exception occurred.");
        }
        else if (responseType == ResponseType.OPERATION_RESULT)
        {
            return (Map<String, Object>)serverResponse.getResponseData();
        }
        else throw new Exception("Invalid Response Type '" + responseType + "' received from the server.");
    }

    /**
     * Posts a specified server request via URL connection to the server, waits
     * for the response from the server and then returns the result.
     *
     * @param serverRequest The T8ServerRequest to send to the server.
     * @param urlPath
     * @param headers
     * @return The T8ServerResponse returned from the server.
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws MalformedURLException
     * @throws ProtocolException
     * @throws java.lang.InterruptedException
     */
    public static T8ServerResponse postServerRequest(T8ServerRequest serverRequest, String urlPath, Map<String, String> headers) throws ClassNotFoundException, IOException, MalformedURLException, ProtocolException, InterruptedException
    {
        ObjectOutputStream objectOutputStream;
        MonitorOuputStream outMeasurer;
        ObjectInputStream objectInputStream;
        MonitorInputStream inMeasurer;
        HttpURLConnection urlConnection;
        T8ServerResponse serverResponse;
        String urlString;
        URL url;
        long timeReceived;

        // Make sure that we check the exclusive use gate before proceeding.
        EXCLUSIVE_USE_GATE.await();

        // Set the servlet URL.
        urlString = serverRequest.getContext().getSessionContext().getServerURL();
        urlString = urlString + SERVLET_MAPPING + urlPath;
        url = new URL(urlString);

        // Do the Http connection setup.
        urlConnection = (HttpURLConnection)url.openConnection();
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.setUseCaches(true);
        urlConnection.setAllowUserInteraction(false);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-java-serialized-object");

        // Add additional headers.
        if (headers != null)
        {
            for (String header : headers.keySet())
            {
                urlConnection.setRequestProperty(header, headers.get(header));
            }
        }

        // Post the request to the server.
        serverRequest.setTimeSent(System.currentTimeMillis());
        outMeasurer = new MonitorOuputStream(urlConnection.getOutputStream());
        objectOutputStream = new ObjectOutputStream(new GZIPOutputStream(outMeasurer));
        objectOutputStream.writeObject(serverRequest);
        objectOutputStream.flush();
        objectOutputStream.close();

        // Wait for the reponse from the server and then return the result.
        inMeasurer = new MonitorInputStream(urlConnection.getInputStream());
        objectInputStream = new ObjectInputStream(new GZIPInputStream(inMeasurer));
        serverResponse = (T8ServerResponse)objectInputStream.readObject();
        objectInputStream.close();

        // Calculate performance metrics.
        timeReceived = System.currentTimeMillis();
        STATISTICS.addSentBytes(outMeasurer.getByteCount(), (serverResponse.getTimeRequestReceived() + clientTimeDifference) - serverRequest.getTimeSent());
        STATISTICS.addRecievedBytes(inMeasurer.getByteCount(), timeReceived - (serverResponse.getTimeSent() + clientTimeDifference));
        STATISTICS.addServerResponseTime(timeReceived);

        // Return the server response.
        return serverResponse;
    }
}
