package com.pilog.t8.security;

import java.util.HashSet;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Gavin Boshoff
 */
public class T8RandomPasswGeneratorTest
{

    public T8RandomPasswGeneratorTest() {}

    /**
     * Test of generate method, of class T8RandomPasswGenerator.
     */
    @Test
    public void testGenerate()
    {
        System.out.println("generate");

        Set<String> passwSet;
        String newPassw;

        passwSet = new HashSet<>(1000);
        for (int i = 0; i < 1000; i++)
        {
            newPassw = T8RandomPasswGenerator.generate();
            System.out.println("GBO - Generated Passw : " + newPassw);

            if (passwSet.contains(newPassw)) fail("Generator has created a duplicate password within 1000 iterations.");
            else passwSet.add(newPassw);
        }
    }
}