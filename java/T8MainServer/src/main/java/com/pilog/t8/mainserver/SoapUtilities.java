package com.pilog.t8.mainserver;

import com.pilog.xml.XmlDocument;
import java.io.InputStream;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Bouwer du Preez
 */
public class SoapUtilities
{
    public static final SOAPMessage createSoapMessage(XmlDocument content) throws Exception
    {
        MessageFactory messageFactory;
        Document requestContentDocument;
        SOAPMessage requestMessage;
        SOAPPart requestSoapPart;
        SOAPEnvelope requestSoapEnvelope;
        SOAPHeader requestHeader;
        SOAPBody requestBody;

        // Create the request envelope.
        messageFactory = MessageFactory.newInstance();
        requestMessage = messageFactory.createMessage();
        requestSoapPart = requestMessage.getSOAPPart();
        requestSoapEnvelope = requestSoapPart.getEnvelope();
        requestHeader = requestSoapEnvelope.getHeader();
        requestBody = requestSoapEnvelope.getBody();

        // Add the content to the body of the envelope.
        requestContentDocument = content.createDocument();
        requestBody.addDocument(requestContentDocument);
        requestMessage.saveChanges();
        return requestMessage;
    }

    public static final XmlDocument getSoapMessageBodyXml(InputStream inputStream) throws Exception
    {
        MessageFactory messageFactory;
        SOAPMessage soapMessage;

        messageFactory = MessageFactory.newInstance();
        soapMessage = messageFactory.createMessage(new MimeHeaders(), inputStream);
        return getSoapMessageBodyXml(soapMessage);
    }

    public static final XmlDocument getSoapMessageBodyXml(SOAPMessage message) throws Exception
    {
        SOAPBody messageBody;
        NodeList nodeList;

        // Get the first element in the response body.  SOAP 1.2 allows for more than one body entry but this method does not implement such a use case.
        messageBody = message.getSOAPBody();
        nodeList = messageBody.getChildNodes();
        if (nodeList.getLength() > 0)
        {
            for (int nodeIndex = 0; nodeIndex < nodeList.getLength(); nodeIndex++)
            {
                Node nextNode;

                nextNode = nodeList.item(nodeIndex);
                if (nextNode instanceof Element)
                {
                    XmlDocument responseDocument;

                    // Create an XmlDocument from the body element and return it so that incoking script can process the result.
                    responseDocument = new XmlDocument();
                    responseDocument.setContent((Element)nextNode);
                    return responseDocument;
                }
            }

            // No elements found in the body, so return null indicating an empty message.
            return null;
        }
        else return null; // No body entries found, so we return null to indicate an empty response.
    }

    public static final XmlDocument getSoapFaultXml(SOAPMessage message) throws Exception
    {
        SOAPBody responseBody;
        SOAPFault soapFault;

        // Get the fault element from the response body.
        responseBody = message.getSOAPBody();
        soapFault = responseBody.getFault();
        if (soapFault != null)
        {
            XmlDocument faultDocument;

            // Create an XmlDocument from the SOAPFault.
            faultDocument = new XmlDocument();
            faultDocument.setContent(soapFault);
            return faultDocument;
        }
        else return null; // No body entries found, so we return null to indicate no fault element present.
    }
}
