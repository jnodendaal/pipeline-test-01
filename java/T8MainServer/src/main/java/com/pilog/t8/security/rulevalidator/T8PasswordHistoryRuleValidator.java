/**
 * Created on Jul 2, 2015, 11:17:21 AM
 */
package com.pilog.t8.security.rulevalidator;

import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.definition.user.password.T8PasswordHistoryRuleDefinition;
import com.pilog.t8.definition.user.password.T8PasswordRuleValidator;
import com.pilog.t8.security.T8PasswordEncryptor;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
public class T8PasswordHistoryRuleValidator implements T8PasswordRuleValidator
{
    private final T8PasswordHistoryRuleDefinition definition;

    public T8PasswordHistoryRuleValidator(T8PasswordHistoryRuleDefinition definition)
    {
        this.definition = definition;
    }

    @Override
    public boolean validate(T8UserDefinition userDefinition, char[] newPassw, String newHash)
    {
        List<String> userPasswordHistoryList = userDefinition.getUserPasswordHistoryList();
        if(userPasswordHistoryList == null || userPasswordHistoryList.isEmpty()) return true;

        try
        {
            for (int i = 0; i < userPasswordHistoryList.size() && i <= this.definition.getLastXAmount(); i++)
            {
                if (T8PasswordEncryptor.validatePassword(newPassw, userPasswordHistoryList.get(i))) return false;
            }
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException ex)
        {
            throw new RuntimeException("Server cannot process required encryption specification.", ex);
        }

        return true;
    }
}