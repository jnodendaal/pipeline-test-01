package com.pilog.t8.mainserver;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8ReportManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServiceManager;
import com.pilog.t8.T8UserManager;
import java.io.File;

/**
 * @author Bouwer du Preez
 */
public interface T8ServerModuleFactory
{
    public T8DefinitionManager constructDefinitionManager();
    public T8SecurityManager constructSecurityManager();
    public T8ProcessManager constructProcessManager();
    public T8FlowManager constructFlowManager();
    public T8FunctionalityManager constructFunctionalityManager();
    public T8DataManager constructDataManager();
    public T8FileManager constructFileManager(File rootFolder);
    public T8ConfigurationManager constructConfigurationManager();
    public T8ServiceManager constructServiceManager();
    public T8CommunicationManager constructCommunicationManager();
    public T8NotificationManager constructNotificationManager();
    public T8UserManager constructUserManager();
    public T8ReportManager constructReportManager();
}
