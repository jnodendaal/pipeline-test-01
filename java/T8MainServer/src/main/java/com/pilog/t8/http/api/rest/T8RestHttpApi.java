package com.pilog.t8.http.api.rest;

import com.pilog.json.JsonObject;
import com.pilog.t8.T8Server;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.http.api.T8HttpApi;
import com.pilog.t8.definition.http.api.rest.T8RestApiDefinition;
import com.pilog.t8.definition.http.api.rest.T8UriParserDefinition;
import com.pilog.t8.http.api.T8HttpApiResult;
import com.pilog.t8.mainserver.T8MainServer;
import com.pilog.t8.mainserver.T8OperationExecutionRequest;
import com.pilog.t8.mainserver.T8ServerRequest;
import com.pilog.t8.mainserver.T8ServerResponse;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.authentication.T8AuthenticationResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Bouwer du Preez
 */
public class T8RestHttpApi implements T8HttpApi
{
    private final T8Context context;
    private final T8RestApiDefinition definition;
    private final T8MainServer server;
    private final List<T8UriParser> uriParsers;

    public T8RestHttpApi(T8Server server, T8Context context, T8RestApiDefinition definition)
    {
        this.server = (T8MainServer)server; // This cast is not ideal.  We are missing an interface here.
        this.context = context;
        this.definition = definition;
        this.uriParsers = new ArrayList<>();
        for (T8UriParserDefinition parserDefinition : definition.getUriParserDefinitions())
        {
            this.uriParsers.add(parserDefinition.getUriParser(context));
        }
    }

    @Override
    public String getName()
    {
        return definition.getApiName();
    }

    @Override
    public T8HttpApiResult handleRequest(T8Context context, String method, String uri, Map<String, List<String>> parameters, JsonObject input)
    {
        // Iterate over all available sub-parsers until one if found that is aplicable to the sub-uri.
        for (T8UriParser uriParser : uriParsers)
        {
            // Test applicability before using the parser.
            if (uriParser.isApplicable(uri))
            {
                T8UriParseResult parseResult;

                // Parse the uri and then process the result.
                parseResult = uriParser.parseUri(null, method, uri);
                if (parseResult.isSuccess())
                {
                    String operationId;

                    operationId = parseResult.getOperationId();
                    if (operationId != null)
                    {
                        Map<String, Object> apiParameters;
                        Map<String, Object> operationInput;
                        Map<String, Object> responseBody;
                        T8ServerResponse serverResponse;
                        T8ServerRequest serverRequest;
                        T8DtMap dtMap;

                        // Get the data type to handle body serialization/deserialization.
                        dtMap = new T8DtMap(server.getDefinitionManager());

                        // Set some API parameters required for operation execution.
                        apiParameters = parseResult.getParameters();
                        apiParameters.put(definition.getMethodParameterId(), method);
                        apiParameters.put(definition.getRequestBodyParameterId(), input != null ? dtMap.deserialize(input) : null);
                        apiParameters.put(definition.getQueryParameterId(), parameters);

                        // Create the server request and handle it.
                        operationInput = T8IdentifierUtilities.mapParameters(apiParameters, parseResult.getOperationInputParameterMapping());
                        serverRequest = new T8ServerRequest(context, T8ServerRequest.RequestType.EXECUTE_OPERATION, new T8OperationExecutionRequest(parseResult.getOperationId(), T8OperationExecutionRequest.ExecutionType.SYNCHRONOUS, operationInput));
                        serverResponse = server.handleRequest(serverRequest);
                        switch (serverResponse.getResponseType())
                        {
                            case AUTHENTICATION_RESPONSE:
                                T8AuthenticationResponse authResponse;

                                // Process the authentication output.
                                authResponse = (T8AuthenticationResponse)serverResponse.getResponseData();
                                if (authResponse.getResponseType() == T8AuthenticationResponse.ResponseType.COMPLETED)
                                {
                                    // Return the http result.
                                    return new T8HttpApiResult(HttpServletResponse.SC_OK, null);
                                }
                                else
                                {
                                    // Return the http result.
                                    return new T8HttpApiResult(HttpServletResponse.SC_UNAUTHORIZED, null);
                                }
                            case OPERATION_RESULT:
                                Map<String, Object> operationOutput;

                                // Process the operation ouput.
                                operationOutput = (Map<String, Object>)serverResponse.getResponseData();
                                apiParameters.putAll(T8IdentifierUtilities.mapParameters(operationOutput, parseResult.getOperationOutputParameterMapping()));
                                responseBody = (Map<String, Object>)apiParameters.get(definition.getResponseBodyParameterId());

                                // Return the http result.
                                return new T8HttpApiResult(HttpServletResponse.SC_OK, responseBody != null ? dtMap.serialize(responseBody).asObject() : null);
                            default:

                        }
                    }
                    else
                    {
                        // Send response code to indicate URI not found.
                        return new T8HttpApiResult(HttpServletResponse.SC_NOT_FOUND, null);
                    }
                }
                else
                {
                    // Send response code to indicate URI not found.
                    return new T8HttpApiResult(HttpServletResponse.SC_NOT_FOUND, null);
                }
            }
        }

        // This point should not be reached if the api is correctly setup.
        throw new RuntimeException("In api " + definition + ", no applicable parser was found for uri: " + uri);
    }
}
