package com.pilog.t8.mainserver;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8ReportManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServiceManager;
import com.pilog.t8.T8UserManager;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.io.File;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultServerModuleFactory implements T8ServerModuleFactory
{
    private final T8ServerContext serverContext;

    public T8DefaultServerModuleFactory(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
    }

    @Override
    public T8DefinitionManager constructDefinitionManager()
    {
        return getServerModule("com.pilog.t8.definition.T8ServerDefinitionManager");
    }

    @Override
    public T8SecurityManager constructSecurityManager()
    {
        return getServerModule("com.pilog.t8.security.T8ServerSecurityManager");
    }

    @Override
    public T8ProcessManager constructProcessManager()
    {
        return getServerModule("com.pilog.t8.process.T8ServerProcessManager");
    }

    @Override
    public T8FlowManager constructFlowManager()
    {
        return getServerModule("com.pilog.t8.flow.T8ServerFlowManager");
    }

    @Override
    public T8FunctionalityManager constructFunctionalityManager()
    {
        return getServerModule("com.pilog.t8.functionality.T8ServerFunctionalityManager");
    }

    @Override
    public T8DataManager constructDataManager()
    {
        return getServerModule("com.pilog.t8.data.T8ServerDataManager");
    }

    @Override
    public T8FileManager constructFileManager(File rootFolder)
    {
        return T8Reflections.getInstance("com.pilog.t8.file.T8ServerFileManager", new Class<?>[]{T8ServerContext.class, File.class}, serverContext, rootFolder);
    }

    @Override
    public T8ConfigurationManager constructConfigurationManager()
    {
        return getServerModule("com.pilog.t8.system.T8ServerConfigurationManager");
    }

    @Override
    public T8ServiceManager constructServiceManager()
    {
        return getServerModule("com.pilog.t8.service.T8ServerServiceManager");
    }

    @Override
    public T8CommunicationManager constructCommunicationManager()
    {
        return getServerModule("com.pilog.t8.communication.T8ServerCommunicationManager");
    }

    @Override
    public T8NotificationManager constructNotificationManager()
    {
        return getServerModule("com.pilog.t8.notification.T8ServerNotificationManager");
    }

    @Override
    public T8UserManager constructUserManager()
    {
        return getServerModule("com.pilog.t8.user.T8ServerUserManager");
    }

    @Override
    public T8ReportManager constructReportManager()
    {
        return getServerModule("com.pilog.t8.report.T8ServerReportManager");
    }

    private <T> T getServerModule(String moduleClazz)
    {
        return T8Reflections.getFastFailInstance(moduleClazz, new Class<?>[]{T8ServerContext.class}, serverContext);
    }
}
