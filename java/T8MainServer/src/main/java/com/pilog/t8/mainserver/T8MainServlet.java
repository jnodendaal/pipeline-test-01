package com.pilog.t8.mainserver;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.http.T8HttpServletRequest;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.mainserver.T8ServerResponse.ResponseType;
import com.pilog.version.T8MainServerVersion;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.pilog.t8.security.authentication.T8AuthenticationResponse;

/**
 * @author Bouwer du Preez
 */
public class T8MainServlet extends HttpServlet
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8MainServlet.class);

    private ServletContext servletContext;

    /**
     * Returns a short description of the Servlet.
     */
    @Override
    public String getServletInfo()
    {
        return "T8MainServer Version " + T8MainServerVersion.VERSION;
    }

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init();
        this.servletContext = config.getServletContext();
    }

    @Override
    public void destroy()
    {
        super.destroy();
        this.servletContext = null;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * TODO:  Implemented handling of concurrent connection through use of HTTP error code 429: Too Many Requests.
     *
     * @param request Servlet request.
     * @param response Servlet response.
     */
    @Override
    protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        T8MainServerContext mainServerContext;
        T8ServerResponse serverResponse;
        T8ServerRequest serverRequest;
        long timeRequestReceived;

        // Get the T8ServerRequest object and the request type.
        try
        {
            timeRequestReceived = System.currentTimeMillis();
            serverRequest = getServerRequest(request);
        }
        catch (Throwable e)
        {
            LOGGER.log("Server Exception while processing HTTP request.", e);
            setServerResponse(response, new T8ServerResponse(ResponseType.THROWABLE, e));
            return;
        }

        // Process the server request.
        try
        {
            // Get the main server reference from the servlet context and check that it is valid.
            mainServerContext = (T8MainServerContext)servletContext.getAttribute(T8MainServer.T8_MAIN_SERVER_CONTEXT_IDENTIFIER);
            if (mainServerContext == null) throw new Exception("T8MainServerContext not found in servlet context.");
            else
            {
                T8AuthenticationResponse authenticationResponse;
                T8HttpServletRequest servletRequest;
                T8SecurityManager securityManager;

                // Bind the request thread to a new access context and data session.
                servletRequest = new T8DefaultHttpServletRequest(request);
                securityManager = mainServerContext.getMainServer().getSecurityManager();
                authenticationResponse = securityManager.authenticate(servletRequest);
                if (authenticationResponse.getResponseType() == T8AuthenticationResponse.ResponseType.COMPLETED)
                {
                    T8SessionContext sessionContext;
                    T8DataManager dataManager;

                    // Get the authenticated session context (which may still be logged out) and create a context for this server request.
                    sessionContext = authenticationResponse.getSessionContext();
                    dataManager = mainServerContext.getMainServer().getDataManager();
                    dataManager.openDataSession(securityManager.createContext(sessionContext));

                    // Handle the server request.
                    try
                    {
                        // Access the context required and then handle the request.
                        securityManager.accessContext(serverRequest.getContext());
                        serverResponse = mainServerContext.handleRequest(serverRequest);
                    }
                    finally
                    {
                        // Unbind the request thread from the data session.
                        dataManager.closeCurrentSession();
                        securityManager.destroyContext();
                    }
                }
                else
                {
                    // Authentication is not yet complete.  A handshake may be in progress, so return the authentication to the client.
                    serverResponse = new T8ServerResponse(ResponseType.AUTHENTICATION_RESPONSE, authenticationResponse);
                }
            }
        }
        catch (Throwable e)
        {
            LOGGER.log("Exception while processing server request: " + serverRequest.getRequestType() + " using request data: " + serverRequest.getRequestData(), e);
            setServerResponse(response, new T8ServerResponse(ResponseType.THROWABLE, e));
            return;
        }

        // Process the server response.
        try
        {
            // Set the successful server response.
            serverResponse.setTimeRequestReceived(timeRequestReceived);
            setServerResponse(response, serverResponse);
        }
        catch (Throwable e)
        {
            LOGGER.log("Exception while sending response to server request: " + serverRequest.getRequestType() + " using request data: " + serverRequest.getRequestData());
            LOGGER.log("Exception while sending server response: " + serverResponse.getResponseType() + " using response data: " + serverResponse.getResponseData(), e);
            setServerResponse(response, new T8ServerResponse(ResponseType.THROWABLE, e));
        }
    }

    /**
     * Returns the T8ServerRequest object that was sent via the specified
     * HttpServletRequest.
     *
     * @param request The request from which to extract the T8ServerRequest
     * object.
     * @return The T8ServerRequest object extracted from the supplied HTTP
     * request.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private T8ServerRequest getServerRequest(HttpServletRequest request) throws IOException, ClassNotFoundException
    {
        ObjectInputStream inputStream;
        T8ServerRequest serverRequest;

        inputStream = new ObjectInputStream(new GZIPInputStream(request.getInputStream()));
        serverRequest = (T8ServerRequest)inputStream.readObject();
        inputStream.close();
        return serverRequest;
    }

    /**
     * Sets the T8ServerResponse object in the specified HttpServletResponse.
     *
     * @param response The HttpServletResponse object into which the
     * T8ServerResponse will be loaded.
     * @param serverResponse The T8ServerResponse to load into the HTTP response.
     * @throws IOException
     */
    private void setServerResponse(HttpServletResponse response, T8ServerResponse serverResponse) throws IOException
    {
        ObjectOutputStream outputStream;

        serverResponse.setTimeSent(System.currentTimeMillis());
        outputStream = new ObjectOutputStream(new GZIPOutputStream(response.getOutputStream()));
        outputStream.writeObject(serverResponse);
        outputStream.flush();
        outputStream.close();
    }
}
