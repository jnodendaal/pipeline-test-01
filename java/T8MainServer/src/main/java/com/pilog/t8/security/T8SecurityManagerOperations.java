package com.pilog.t8.security;

import com.google.common.base.Strings;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.system.T8SecurityManagerResource;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.system.T8SecurityManagerResource.*;
import com.pilog.t8.definition.system.T8SystemDefinition;


/**
 * @author Bouwer du Preez
 */
public class T8SecurityManagerOperations
{
    public static class LoginOperation extends T8DefaultServerOperation
    {
        public LoginOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8UserLoginResponse loginResponse;
            String username;
            char[] password;
            Boolean endExistingSession;

            // Get operation parameters.
            username = (String)operationParameters.get(PARAMETER_USERNAME);
            password = (char[])operationParameters.get(PARAMETER_PASSWORD);
            endExistingSession = (Boolean)operationParameters.get(PARAMETER_END_EXISTING_SESSION);

            // Set default values for null parameters.
            if (endExistingSession == null) endExistingSession = false;

            // Execute the login operation.
            loginResponse = serverContext.getSecurityManager().login(context, T8SecurityManager.LoginLocation.CLIENT, username, password, endExistingSession);
            return HashMaps.newHashMap(PARAMETER_RESPONSE_CODE, loginResponse.getResponseType().toString(), PARAMETER_MESSAGE, loginResponse.getMessage(), PARAMETER_API_KEY, loginResponse.getAPIKey(), PARAMETER_SESSION_DETAILS, loginResponse.getSessionDetails());
        }
    }

    public static class LogoutOperation extends T8DefaultServerOperation
    {
        public LogoutOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            boolean success;

            success = serverContext.getSecurityManager().logout(context);
            return HashMaps.createSingular(PARAMETER_SUCCESS, success);
        }
    }

    public static class ChangePasswordOperation extends T8DefaultServerOperation
    {
        public ChangePasswordOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8PasswordChangeResponse changeResponse;
            char[] password;

            password = (char[])operationParameters.get(PARAMETER_PASSWORD);
            changeResponse = serverContext.getSecurityManager().changePassword(context, password);
            return HashMaps.createTypeSafeMap(new String[]{PARAMETER_SUCCESS, PARAMETER_MESSAGE}, new Object[]{changeResponse.isSuccess(), changeResponse.getMessage()});
        }
    }

    public static class GetSessionDetailsOperation extends T8DefaultServerOperation
    {
        public GetSessionDetailsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<T8SessionDetails>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8SessionDetails> sessionDetailsList;

            sessionDetailsList = serverContext.getSecurityManager().getAllSessionDetails(context);
            return HashMaps.createSingular(PARAMETER_SESSION_DETAILS_LIST, sessionDetailsList);
        }
    }

    public static class IsSessionActiveOperation extends T8DefaultServerOperation
    {
        public IsSessionActiveOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Boolean active;
            String sessionIdentifier;

            sessionIdentifier = (String) operationParameters.get(PARAMETER_SESSION_ID);

            active = serverContext.getSecurityManager().isSessionActive(context, sessionIdentifier);
            return HashMaps.createSingular(PARAMETER_ACTIVE, active);
        }
    }

    public static class LockUser extends T8DefaultServerOperation
    {
        public LockUser(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String userIdentifier;
            String lockReason;

            userIdentifier = (String) operationParameters.get(PARAMETER_USER_ID);
            lockReason = (String) operationParameters.get(PARAMETER_LOCK_REASON);

            serverContext.getSecurityManager().lockUser(context, userIdentifier, lockReason);
            return null;
        }
    }

    public static class UnlockUser extends T8DefaultServerOperation
    {
        public UnlockUser(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String userId;

            userId = (String) operationParameters.get(PARAMETER_USER_ID);

            serverContext.getSecurityManager().unlockUser(context, userId);
            return null;
        }
    }

    public static class ApiSwitchUserProfile extends T8DefaultServerOperation
    {
        public ApiSwitchUserProfile(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            String userProfileId;

            userProfileId = (String) operationParameters.get(PARAMETER_USER_PROFILE_ID);
            securityManager = serverContext.getSecurityManager();
            if (securityManager.switchUserProfile(context, userProfileId))
            {
                T8SessionDetails sessionDetails;

                sessionDetails = securityManager.getSessionDetails(context);
                return HashMaps.newHashMap(PARAMETER_SUCCESS, true, PARAMETER_SESSION_DETAILS, sessionDetails);
            }
            else
            {
                return HashMaps.newHashMap(PARAMETER_SUCCESS, false);
            }
        }
    }

    public static class ApiGetAccessibleUserProfileDetails extends T8DefaultServerOperation
    {
        public ApiGetAccessibleUserProfileDetails(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<T8UserProfileDetails>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            return HashMaps.createSingular(PARAMETER_USER_PROFILE_DETAILS, serverContext.getSecurityManager().getAccessibleUserProfileDetails(context));
        }
    }

    public static class GetAccessibleUserProfileIdentifiersOperation extends T8DefaultServerOperation
    {
        public GetAccessibleUserProfileIdentifiersOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<String>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            return HashMaps.createSingular(PARAMETER_USER_PROFILE_IDS, serverContext.getSecurityManager().getAccessibleUserProfileIds(context));
        }
    }

    public static class GetAccessibleUserProfileMetaDataOperation extends T8DefaultServerOperation
    {
        public GetAccessibleUserProfileMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<T8DefinitionMetaData>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            return HashMaps.createSingular(PARAMETER_USER_PROFILE_META_DATA, serverContext.getSecurityManager().getAccessibleUserProfileMetaData(context));
        }
    }

    public static class CreateNewUserOperation extends T8DefaultServerOperation
    {
        public CreateNewUserOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            T8UserUpdateResponse updateResponse;
            T8UserDetails userDetails;

            if(operationParameters.containsKey(PARAMETER_USER_DETAILS))
            {
                userDetails = (T8UserDetails)operationParameters.get(PARAMETER_USER_DETAILS);
            }
            else
            {
                userDetails  = new T8UserDetails();
                userDetails.setUsername((String)operationParameters.get(PARAMETER_USERNAME));
                userDetails.setName((String)operationParameters.get(PARAMETER_NAME));
                userDetails.setSurname((String)operationParameters.get(PARAMETER_SURNAME));
                userDetails.setEmailAddress((String)operationParameters.get(PARAMETER_EMAIL_ADDRESS));
                userDetails.setMobileNumber((String)operationParameters.get(PARAMETER_MOBILE_NUMBER));
                userDetails.setWorkTelephoneNumber((String)operationParameters.get(PARAMETER_WORK_TELEPHONE_NUMBER));
                userDetails.setLanguageIdentifier((String)operationParameters.get(PARAMETER_LANGUAGE_ID));
                userDetails.setOrganizationIdentifier((String)operationParameters.get(PARAMETER_ORGANIZATION_ID));
                userDetails.setActive((Boolean)operationParameters.get(PARAMETER_ACTIVE));
                userDetails.setUserProfileIds((List<String>)operationParameters.get(PARAMETER_USER_PROFILE_IDS));
                userDetails.setWorkflowProfileIds((List<String>)operationParameters.get(PARAMETER_WORKFLOW_PROFILE_IDS));
                userDetails.setExpiryDate((T8Timestamp)operationParameters.get(PARAMETER_USER_EXPIRY_DATE));
            }

            securityManager = serverContext.getSecurityManager();
            updateResponse = securityManager.createNewUser(context, userDetails);
            return HashMaps.createTypeSafeMap(new String[]{PARAMETER_USER_ID, PARAMETER_SUCCESS, PARAMETER_MESSAGE}, new Object[]{updateResponse.getIdentifier(), updateResponse.isSuccess(), updateResponse.getMessage()});
        }
    }

    public static class UpdateUserDetailsOperation extends T8DefaultServerOperation
    {
        public UpdateUserDetailsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            T8UserUpdateResponse updateResponse;
            T8UserDetails userDetails;

            if (operationParameters.containsKey(PARAMETER_USER_DETAILS))
            {
                userDetails = (T8UserDetails)operationParameters.get(PARAMETER_USER_DETAILS);
            }
            else
            {
                userDetails = new T8UserDetails();
                userDetails.setIdentifier((String)operationParameters.get(PARAMETER_USER_ID));
                userDetails.setUsername((String)operationParameters.get(PARAMETER_USERNAME));
                userDetails.setName((String)operationParameters.get(PARAMETER_NAME));
                userDetails.setSurname((String)operationParameters.get(PARAMETER_SURNAME));
                userDetails.setEmailAddress((String)operationParameters.get(PARAMETER_EMAIL_ADDRESS));
                userDetails.setMobileNumber((String)operationParameters.get(PARAMETER_MOBILE_NUMBER));
                userDetails.setWorkTelephoneNumber((String)operationParameters.get(PARAMETER_WORK_TELEPHONE_NUMBER));
                userDetails.setLanguageIdentifier((String)operationParameters.get(PARAMETER_LANGUAGE_ID));
                userDetails.setOrganizationIdentifier((String)operationParameters.get(PARAMETER_ORGANIZATION_ID));
                userDetails.setActive((Boolean)operationParameters.get(PARAMETER_ACTIVE));
                userDetails.setUserProfileIds((List<String>)operationParameters.get(PARAMETER_USER_PROFILE_IDS));
                userDetails.setWorkflowProfileIds((List<String>)operationParameters.get(PARAMETER_WORKFLOW_PROFILE_IDS));
                userDetails.setExpiryDate((T8Timestamp)operationParameters.get(PARAMETER_USER_EXPIRY_DATE));
            }

            securityManager = serverContext.getSecurityManager();
            updateResponse = securityManager.updateUserDetails(context, userDetails);
            return HashMaps.createTypeSafeMap(new String[]{PARAMETER_SUCCESS, PARAMETER_MESSAGE, PARAMETER_SESSION_DETAILS}, new Object[]{updateResponse.isSuccess(), updateResponse.getMessage(), updateResponse.getSessionDetails()});
        }
    }

    public static class SetUserActiveOperation extends T8DefaultServerOperation
    {
        public SetUserActiveOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String userIdentifer;
            Boolean active;

            userIdentifer = (String)operationParameters.get(PARAMETER_USER_ID);
            active = (Boolean)operationParameters.get(PARAMETER_ACTIVE);

            serverContext.getSecurityManager().setUserActive(context, userIdentifer, active);
            return null;
        }
    }

    public static class GetUserDetailsOperation extends T8DefaultServerOperation
    {
        public GetUserDetailsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8UserDetails> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            String userIdentifier;
            T8UserDetails userDetails;

            securityManager = serverContext.getSecurityManager();
            userIdentifier = (String)operationParameters.get(PARAMETER_USER_ID);
            userDetails = securityManager.getUserDetails(context, !Strings.isNullOrEmpty(userIdentifier) ? userIdentifier : context.getSessionContext().getUserIdentifier());

            return HashMaps.createSingular(PARAMETER_USER_DETAILS, userDetails);
        }
    }

    public static class DeleteUserOperation extends T8DefaultServerOperation
    {
        public DeleteUserOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            String userIdentifier;
            boolean success;

            securityManager = serverContext.getSecurityManager();
            userIdentifier = (String)operationParameters.get(PARAMETER_USER_ID);
            success = securityManager.deleteUser(context, userIdentifier);
            return HashMaps.createSingular(PARAMETER_SUCCESS, success);
        }
    }

    public static class ResetUserPasswordOperation extends T8DefaultServerOperation
    {
        public ResetUserPasswordOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            String userIdentifier;
            boolean success;

            securityManager = serverContext.getSecurityManager();
            userIdentifier = (String)operationParameters.get(PARAMETER_USER_ID);
            success = securityManager.resetUserPassword(context, userIdentifier);
            return HashMaps.createSingular(PARAMETER_SUCCESS, success);
        }
    }

    public static class GetWebServiceApiKeyOperation extends T8DefaultServerOperation
    {
        public GetWebServiceApiKeyOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            String apiKey;

            securityManager = serverContext.getSecurityManager();
            apiKey = securityManager.getWebServiceUserApiKey(context);
            return HashMaps.createSingular(T8SecurityManagerResource.PARAMETER_API_KEY, apiKey);
        }
    }

    public static class GenerateWebServiceApiKeyOperation extends T8DefaultServerOperation
    {
        public GenerateWebServiceApiKeyOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;

            securityManager = serverContext.getSecurityManager();
            securityManager.generateWebServiceUserApiKey(context);
            return null;
        }
    }

    public static class SwitchSessionLanguage extends T8DefaultServerOperation
    {
        public SwitchSessionLanguage(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8SessionContext> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            String langaugeIdentifier;
            T8SessionContext authenticSessionContext;

            securityManager = serverContext.getSecurityManager();
            langaugeIdentifier = (String)operationParameters.get(PARAMETER_LANGUAGE_ID);
            authenticSessionContext = securityManager.switchSessionLanguage(context, langaugeIdentifier);
            return HashMaps.createSingular(PARAMETER_SESSION_CONTEXT, authenticSessionContext);
        }
    }

    public static class ExtendUserSession extends T8DefaultServerOperation
    {
        public ExtendUserSession(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            this.serverContext.getSecurityManager().extendSession(context);
            return null;
        }
    }

    public static class SetSessionPerformanceStatisticsEnabled extends T8DefaultServerOperation
    {
        public SetSessionPerformanceStatisticsEnabled(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Void> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            Boolean enabled;
            String sessionId;

            securityManager = serverContext.getSecurityManager();
            sessionId = (String)operationParameters.get(PARAMETER_SESSION_ID);
            enabled = (Boolean)operationParameters.get(PARAMETER_ENABLED);
            securityManager.setSessionPerformanceStatisticsEnabled(context, sessionId, enabled);
            return null;
        }
    }

    public static class GetSessionPerformanceStatistics extends T8DefaultServerOperation
    {
        public GetSessionPerformanceStatistics(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SecurityManager securityManager;
            T8PerformanceStatistics stats;
            String sessionId;

            securityManager = serverContext.getSecurityManager();
            sessionId = (String)operationParameters.get(PARAMETER_SESSION_ID);
            stats = securityManager.getSessionPerformanceStatistics(context, sessionId);
            return HashMaps.createSingular(PARAMETER_PERFORMANCE_STATISTICS, stats);
        }
    }

    public static class GetSsoHandlerDefinition extends T8DefaultServerOperation
    {
        public GetSsoHandlerDefinition(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DefinitionManager definitionManager;
            T8SystemDefinition systemDefinition;
            String ssoHandlerId;

            definitionManager = serverContext.getDefinitionManager();
            systemDefinition = definitionManager.getSystemDefinition(context);
            ssoHandlerId = systemDefinition.getSsoHandlerId();
            if (ssoHandlerId != null)
            {
                return HashMaps.createSingular(PARAMETER_DEFINITION, definitionManager.getInitializedDefinition(context, null, ssoHandlerId, null));
            }
            else return null;
        }
    }
}
