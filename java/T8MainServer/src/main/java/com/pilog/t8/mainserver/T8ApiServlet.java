package com.pilog.t8.mainserver;

import com.pilog.t8.data.T8JsonParameterSerializer;
import com.pilog.json.JsonObject;
import com.pilog.json.JsonValue;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ReportManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.http.api.T8HttpApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.entity.T8DataEntitySerializer;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.http.T8HttpServletRequest;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.data.document.datarecord.filter.T8RecordFilterSerializer;
import com.pilog.t8.datatype.T8DtHeartbeatResponse;
import com.pilog.t8.definition.api.T8DataRecordApiResource;
import com.pilog.t8.definition.data.T8DataManagerResource;
import com.pilog.t8.definition.gfx.image.T8ImageDefinition;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.definition.operation.script.T8SoapServiceOperationDefinition;
import com.pilog.t8.http.api.T8HttpApiResult;
import com.pilog.t8.mainserver.T8OperationExecutionRequest.ExecutionType;
import com.pilog.t8.mainserver.T8ServerRequest.RequestType;
import com.pilog.t8.mainserver.T8ServerResponse.ResponseType;
import com.pilog.t8.report.T8ReportDetails;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8HeartbeatResponse;
import com.pilog.t8.security.authentication.T8AuthenticationResponse;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.utilities.threads.ThreadPrinter;
import com.pilog.version.T8MainServerVersion;
import com.pilog.xml.XmlDocument;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPMessage;

/**
 * @author Bouwer du Preez
 */
public class T8ApiServlet extends HttpServlet
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ApiServlet.class);

    private ServletContext servletContext;
    private T8MainServer mainServer;
    private T8MainServerContext mainServerContext;
    private T8JsonParameterSerializer jsonHandler;

    public enum PathType
    {
        API,
        ATTACHMENT,
        REPORT,
        FILE,
        OPERATION,
        SOAP,
        IMAGE,
        DATA,
        DATA_COUNT,
        DATA_TABLE,
        HEARTBEAT,
        SYSTEM;

        public static PathType getType(String value)
        {
            for (PathType type : PathType.values())
            {
                if (type.toString().equals(value))
                {
                    return type;
                }
            }

            return PathType.API;
        }
    };

    public T8ApiServlet()
    {
    }

    /**
     * Returns a short description of the Servlet.
     * @return The Servlet info text.
     */
    @Override
    public String getServletInfo()
    {
        return "T8 Api Servlet Version " + T8MainServerVersion.VERSION;
    }

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init();

        // Get the servlet context.
        this.servletContext = config.getServletContext();

        // Get the main server reference from the servlet context and check that it is valid.
        mainServerContext = (T8MainServerContext)servletContext.getAttribute(T8MainServer.T8_MAIN_SERVER_CONTEXT_IDENTIFIER);
        if (mainServerContext == null) throw new ServletException("T8MainServerContext not found in servlet context.");
        else
        {
            // Get the direct main server reference.
            mainServer = mainServerContext.getMainServer();

            // Create an JSON handler.
            jsonHandler = new T8JsonParameterSerializer();
        }
    }

    @Override
    public void destroy()
    {
        super.destroy();
        this.servletContext = null;
    }

    @Override
    protected final void doTrace(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        LOGGER.log("Trace operation called from: " + request.getHeader("origin"));
    }

    @Override
    protected final void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        LOGGER.log("Options operation called from: " + request.getHeader("origin"));

        // Set the headers to enabled CORS (preflight check by browsers will hit the OPTIONS operation before actual GET/POST).
        setResponseHeaders(request, response);
    }

    @Override
    protected final void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        LOGGER.log("Head operation called from: " + request.getHeader("origin"));
    }

    @Override
    protected final void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handleRequest(request, response);
    }

    @Override
    protected final void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handleRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request Servlet request.
     * @param response Servlet response.
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handleRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request Servlet request.
     * @param response Servlet response.
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected final void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handleRequest(request, response);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        T8AuthenticationResponse authenticationResponse;
        T8HttpServletRequest servletRequest;
        T8SecurityManager securityManager;
        T8DataManager dataManager;

        // Get the data and security managers.
        dataManager = mainServerContext.getMainServer().getDataManager();
        securityManager = mainServer.getSecurityManager();

        // Set additional response headers.
        setResponseHeaders(request, response);

        // Create a T8 servlet request object wrapper to allow easy access to request parameters and then authenticate the request.
        servletRequest = new T8DefaultHttpServletRequest(request);
        authenticationResponse = securityManager.authenticate(servletRequest);
        if (authenticationResponse.getResponseType() == T8AuthenticationResponse.ResponseType.COMPLETED)
        {
            try
            {
                T8SessionContext sessionContext;
                T8Context context;
                String pathString;

                // Get the session context from the completed authentication response and create a context for this request.
                sessionContext = authenticationResponse.getSessionContext();
                dataManager.openDataSession(securityManager.createContext(sessionContext));

                // Request additional access rights required (very important that this step is done after the data session has been opened).
                context = securityManager.accessContext(servletRequest.getAccessContext());

                // Get the request path.
                pathString = request.getPathInfo();

                // Any valid T8 path must be longer than 4 characters ((2 * '/') + '@' + (2 * identifier of at least one character)).
                if (pathString.length() > 4)
                {
                    T8ServerRequest serverRequest;
                    T8ServerResponse serverResponse;
                    String pathTypeString;
                    String pathId;
                    PathType pathType;
                    int separatorIndex;

                    // Trim the first separator.
                    pathString = pathString.substring(1);

                    // Make sure we have a valid path separator.
                    separatorIndex = pathString.indexOf('/');

                    // Get the path type and identifier;
                    if (separatorIndex > 0) // Cannot be the first character because the must be the path type identifier.
                    {
                        pathTypeString = pathString.substring(0, separatorIndex).toUpperCase();
                        pathType = PathType.getType(pathTypeString);
                        pathId = pathString.substring(separatorIndex + 1).toUpperCase();
                    }
                    else
                    {
                        pathTypeString = pathString.toUpperCase();
                        pathType = PathType.getType(pathTypeString);
                        pathId = null;
                    }

                    // Execute the request depending on the path type.
                    switch (pathType)
                    {
                        case OPERATION:
                        {
                            T8ServerOperationDefinition operationDefinition;
                            String contentType;

                            // Get the server operation definition.
                            operationDefinition = mainServer.getOperationDefinition(context, pathId);
                            if (operationDefinition != null)
                            {
                                // Parse operation input/output depending on the request MIME type.
                                contentType = request.getContentType();
                                if ((contentType == null) || (contentType.startsWith("application/json")) || (contentType.startsWith("text/plain"))) // JSON is the default MIME type used for operation execution requests.
                                {
                                    try
                                    {
                                        Map<String, Object> operationInput;
                                        Map<String, Object> operationOutput;
                                        PrintWriter outputWriter;

                                        // Parse the operation input JSON.
                                        if (request.getContentLength() > 0)
                                        {
                                            operationInput = jsonHandler.deserializeParameters(operationDefinition.getInputParameterDefinitions(), request.getReader());
                                        }
                                        else operationInput = null;

                                        // Create the server request and handle it.
                                        serverRequest = new T8ServerRequest(context, RequestType.EXECUTE_OPERATION, new T8OperationExecutionRequest(pathId, ExecutionType.SYNCHRONOUS, operationInput));
                                        serverResponse = mainServerContext.handleRequest(serverRequest);

                                        // Write the HTML output to the HTTP response.
                                        response.setContentType("application/json");
                                        response.setCharacterEncoding("UTF-8");

                                        // We need to process the server response accordingly
                                        outputWriter = response.getWriter();
                                        switch (serverResponse.getResponseType())
                                        {
                                            case THROWABLE:
                                                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                                                break;
                                            case AUTHENTICATION_RESPONSE:
                                                T8AuthenticationResponse authResponse;

                                                // Process the authentication output.
                                                authResponse = (T8AuthenticationResponse)serverResponse.getResponseData();
                                                if (authResponse.getResponseType() == T8AuthenticationResponse.ResponseType.COMPLETED)
                                                {
                                                    response.setStatus(HttpServletResponse.SC_OK);
                                                    break;
                                                }
                                                else
                                                {
                                                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                                                    break;
                                                }
                                            default:
                                                JsonValue outputJson;

                                                // Process the operation output.
                                                operationOutput = (Map<String, Object>)serverResponse.getResponseData();
                                                outputJson = jsonHandler.serializeParameters(operationDefinition.getOutputParameterDefinitions(), operationOutput);
                                                outputWriter.print(outputJson.toString());
                                                outputWriter.flush();
                                        }
                                    }
                                    catch (IOException e)
                                    {
                                        throw e;
                                    }
                                    catch (Exception e)
                                    {
                                        throw new ServletException("Exception while invoking operation (JSON): " + pathId, e);
                                    }
                                }
                                else throw new ServletException("Unsupported Media Type: " + contentType);
                            }
                            else throw new ServletException("Operation not found: " + pathId);
                        }
                        break;
                        case SOAP:
                        {
                            String operationId;
                            String queryString;

                            // Get the operation id from the url.
                            operationId = pathId;
                            if (!operationId.startsWith("@")) operationId = ("@" + operationId);
                            queryString = request.getQueryString();

                            // Make the distinction between the operation and its WSDL.
                            if ((queryString != null) && (queryString.equalsIgnoreCase("WSDL")))
                            {
                                T8SoapServiceOperationDefinition operationDefinition;

                                // Get the server operation definition.
                                operationDefinition = (T8SoapServiceOperationDefinition)mainServer.getOperationDefinition(null, operationId);
                                if (operationDefinition != null)
                                {
                                    PrintWriter outputWriter;
                                    String wsdl;

                                    // Get the wsdl from the operation definition.
                                    wsdl = operationDefinition.getWsdl();
                                    wsdl = wsdl.replace("<<-SERVICE_URL->>", request.getRequestURL());

                                    // Write the HTML output to the HTTP response.
                                    response.setContentType("text/xml");
                                    response.setCharacterEncoding("UTF-8");

                                    // We need to process the server response accordingly
                                    outputWriter = response.getWriter();
                                    outputWriter.print(wsdl);
                                    outputWriter.flush();
                                }
                                else throw new ServletException("SOAP Operation not found: " + operationId);
                            }
                            else
                            {
                                T8SoapServiceOperationDefinition operationDefinition;
                                String contentType;

                                // Get the server operation definition.
                                operationDefinition = (T8SoapServiceOperationDefinition)mainServer.getOperationDefinition(null, operationId);
                                if (operationDefinition != null)
                                {
                                    // Parse operation input/output depending on the request MIME type.
                                    contentType = request.getContentType();
                                    if ((contentType == null) || (contentType.startsWith("application/soap+xml")) || (contentType.startsWith("text/xml"))) // JSON is the default MIME type used for operation execution requests.
                                    {
                                        try
                                        {
                                            Map<String, Object> operationInput;
                                            Map<String, Object> operationOutput;
                                            SOAPMessage responseMessage;

                                            // Create the operation input map.
                                            operationInput = new HashMap<>();
                                            operationInput.put(operationId + T8SoapServiceOperationDefinition.XML_INPUT_PARAMETER_ID, SoapUtilities.getSoapMessageBodyXml(request.getInputStream()));

                                            // Create the server request and handle it.
                                            serverRequest = new T8ServerRequest(context, RequestType.EXECUTE_OPERATION, new T8OperationExecutionRequest(operationId, ExecutionType.SYNCHRONOUS, operationInput));
                                            serverResponse = mainServerContext.handleRequest(serverRequest);

                                            // Create a SOAP message from the operation output;
                                            operationOutput = (Map<String, Object>)serverResponse.getResponseData();
                                            response.setContentType("text/xml");
                                            response.setCharacterEncoding("UTF-8");
                                            responseMessage = SoapUtilities.createSoapMessage((XmlDocument)operationOutput.get(operationId + T8SoapServiceOperationDefinition.XML_OUTPUT_PARAMETER_ID));
                                            responseMessage.writeTo(response.getOutputStream());
                                        }
                                        catch (IOException e)
                                        {
                                            throw e;
                                        }
                                        catch (Exception e)
                                        {
                                            throw new ServletException("Exception while invoking SOAP operation (JSON): " + operationId, e);
                                        }
                                    }
                                    else throw new ServletException("Unsupported Media Type: " + contentType);
                                }
                                else throw new ServletException("SOAP Operation not found: " + operationId);
                            }
                        }
                        break;
                        case IMAGE:
                        {
                            try
                            {
                                T8ImageDefinition imageDefinition;

                                // Load the image from definition.
                                imageDefinition = (T8ImageDefinition)mainServer.getDefinitionManager().getRawDefinition(context, context.getProjectId(), pathId);
                                if (imageDefinition != null)
                                {
                                    BufferedImage image;

                                    image = imageDefinition.getImage().getBufferedImage();
                                    response.setContentType("image/png");

                                    // Write the image to the response output stream.
                                    try (OutputStream outputStream = response.getOutputStream())
                                    {
                                        // IndexOutOfBoundsException is sometimes thrown from this line.  This bug is fixed in JDK 9.
                                        // See: https://bugs.java.com/bugdatabase/view_bug.do?bug_id=6967419
                                        ImageIO.write(image, "png", outputStream);
                                    }
                                }
                                else throw new ServletException("Image not found: " + pathId);
                            }
                            catch (IOException e)
                            {
                                throw e;
                            }
                            catch (Exception e)
                            {
                                throw new ServletException("Exception while fetching image: " + pathId, e);
                            }
                        }
                        break;
                        case ATTACHMENT:
                        {
                            try
                            {
                                Map<String, Object> operationInput;
                                Map<String, Object> operationOutput;
                                T8FileDetails fileDetails;
                                String attachmentId;
                                String fileContextIid;

                                // Use the path identifier as the attachment id.
                                attachmentId = pathId;
                                fileContextIid = T8IdentifierUtilities.createNewGUID();

                                // Create the operation input parameter map.
                                operationInput = new HashMap<>();
                                operationInput.put(T8DataRecordApiResource.PARAMETER_ATTACHMENT_ID, attachmentId);
                                operationInput.put(T8DataRecordApiResource.PARAMETER_FILE_CONTEXT_IID, fileContextIid);
                                operationInput.put(T8DataRecordApiResource.PARAMETER_FILE_CONTEXT_ID, null); // Use the temporary file context.
                                operationInput.put(T8DataRecordApiResource.PARAMETER_PATH, null); // Export to root folder in temporary context.
                                operationInput.put(T8DataRecordApiResource.PARAMETER_FILENAME, null); // Use original filename of attachment.

                                // Export the attachment to the temporary file location.
                                serverRequest = new T8ServerRequest(context, RequestType.EXECUTE_OPERATION, new T8OperationExecutionRequest(T8DataRecordApiResource.OPERATION_API_REC_EXPORT_ATTACHMENT, ExecutionType.SYNCHRONOUS, operationInput));
                                serverResponse = mainServerContext.handleRequest(serverRequest);
                                operationOutput = (Map<String, Object>)serverResponse.getResponseData();

                                // Load the image from definition.
                                fileDetails = (T8FileDetails)operationOutput.get(T8DataRecordApiResource.PARAMETER_ATTACHMENT_DETAILS);
                                if (fileDetails != null)
                                {
                                    T8FileManager fileManager;

                                    // Set the content type of the response according to the media type of the attachment.
                                    response.setContentType(fileDetails.getMediaType());
                                    response.setHeader( "Content-Disposition", "attachment;filename=" + fileDetails.getFileName()); // Ensures that browsers interpret the content as an attachment to be downloaded and also to use the suggested filename.
                                    fileManager = mainServerContext.getMainServer().getFileManager();

                                    // Write the attachment to the response output stream.
                                    try
                                    (
                                        // Get an input stream from the exported attachment in the temporary file context.
                                        InputStream inputStream = fileManager.getFileInputStream(context, fileContextIid, fileDetails.getFilePath());
                                        // Get the response output stream.
                                        OutputStream outputStream = response.getOutputStream();
                                    )
                                    {
                                        int numRead;
                                        byte[] buffer;

                                        // Read from the input stream into the byte buffer and write the buffer to the output stream.
                                        buffer = new byte[1024];
                                        while ((numRead = inputStream.read(buffer)) >= 0)
                                        {
                                            outputStream.write(buffer, 0, numRead);
                                        }
                                    }

                                    // Close the file context as it is no longer needed.
                                    fileManager.closeFileContext(context, fileContextIid);
                                }
                                else throw new ServletException("Attachment not found: " + attachmentId);
                            }
                            catch (IOException e)
                            {
                                throw e;
                            }
                            catch (Exception e)
                            {
                                throw new ServletException("Exception while fetching attachment: " + pathId, e);
                            }
                        }
                        break;
                        case FILE:
                        {
                            try
                            {
                                T8FileManager fileManager;
                                String fileContextIid;
                                String fileContextId;
                                String filePath;
                                int pathSeparatorIndex;

                                // Get the path identifier and parse the file context specifics from it.
                                // Currently we generate a new context iid but plans are to use a url /contextId:contextIid/file_path in future so that context iid can be specified.
                                pathSeparatorIndex = pathId.indexOf("/");
                                fileContextId = pathId.substring(0, pathSeparatorIndex);
                                fileContextIid = T8IdentifierUtilities.createNewGUID();
                                filePath = pathId.substring(pathSeparatorIndex + 1);

                                // Get the file manager to use for file retrieval.
                                fileManager = mainServerContext.getMainServer().getFileManager();
                                fileManager.openFileContext(context, fileContextIid, fileContextId, new T8Minute(2));

                                // Write the attachment to the response output stream.
                                try
                                (
                                    // Get an input stream from the exported attachment in the temporary file context.
                                    InputStream inputStream = fileManager.getFileInputStream(context, fileContextIid, filePath);
                                    // Get the response output stream.
                                    OutputStream outputStream = response.getOutputStream();
                                )
                                {
                                    int numRead;
                                    byte[] buffer;

                                    // Read from the input stream into the byte buffer and write the buffer to the output stream.
                                    buffer = new byte[1024];
                                    while ((numRead = inputStream.read(buffer)) >= 0)
                                    {
                                        outputStream.write(buffer, 0, numRead);
                                    }
                                }

                                // Close the file context as it is no longer needed.
                                fileManager.closeFileContext(context, fileContextIid);
                            }
                            catch (Exception e)
                            {
                                throw new ServletException("Exception while fetching file: " + pathId, e);
                            }
                        }
                        break;
                        case REPORT:
                        {
                            try
                            {
                                T8ReportManager reportManager;
                                T8ReportDetails reportDetails;
                                String reportIid;

                                // Get the details of the report.
                                reportIid = pathId;
                                reportManager = mainServerContext.getMainServer().getReportManager();
                                reportDetails = reportManager.getReportDetails(context, reportIid);
                                if (reportDetails != null)
                                {
                                    T8FileManager fileManager;
                                    String fileContextIid;
                                    String fileContextId;
                                    String filePath;

                                    // Get the details of the report file.
                                    fileContextId = reportDetails.getFileContextId();
                                    fileContextIid = T8IdentifierUtilities.createNewGUID();
                                    filePath = reportDetails.getFilePath();

                                    // Get the file manager to use for file retrieval.
                                    fileManager = mainServerContext.getMainServer().getFileManager();
                                    fileManager.openFileContext(context, fileContextIid, fileContextId, new T8Minute(2));

                                    // Write the attachment to the response output stream.
                                    try
                                    (
                                        // Get an input stream from the exported attachment in the temporary file context.
                                        InputStream inputStream = fileManager.getFileInputStream(context, fileContextIid, filePath);
                                        // Get the response output stream.
                                        OutputStream outputStream = response.getOutputStream();
                                    )
                                    {
                                        int numRead;
                                        byte[] buffer;

                                        // Read from the input stream into the byte buffer and write the buffer to the output stream.
                                        buffer = new byte[1024];
                                        while ((numRead = inputStream.read(buffer)) >= 0)
                                        {
                                            outputStream.write(buffer, 0, numRead);
                                        }
                                    }

                                    // Close the file context as it is no longer needed.
                                    fileManager.closeFileContext(context, fileContextIid);
                                }
                                else throw new Exception("Report not found: " + reportIid);
                            }
                            catch (Exception e)
                            {
                                throw new ServletException("Exception while fetching file: " + pathId, e);
                            }
                        }
                        break;
                        case DATA_COUNT:
                        {
                            try
                            {
                                Map<String, Object> operationOutput;
                                Map<String, Object> operationInput;
                                String operationId;
                                T8DataFilter dataFilter;
                                String callbackFunction;
                                int resultCount;

                                // Get the request parameters.
                                operationId = T8DataManagerResource.OPERATION_COUNT_DATA_ENTITIES;
                                if (request.getContentLength() > 0)
                                {
                                    JsonObject jsonObject;

                                    // Read the JSON object from the input reader.
                                    jsonObject = JsonObject.readFrom(request.getReader());
                                    dataFilter = new T8RecordFilterSerializer().deserializeDataFilter(jsonObject);
                                }
                                else
                                {
                                    dataFilter = null;
                                }

                                // Create the operation input parameter map.
                                operationInput = new HashMap<>();
                                operationInput.put(T8DataManagerResource.PARAMETER_DATA_ENTITY_IDENTIFIER, pathId);
                                operationInput.put(T8DataManagerResource.PARAMETER_DATA_FILTER, dataFilter);

                                // Create the server request and handle it.
                                serverRequest = new T8ServerRequest(context, RequestType.EXECUTE_OPERATION, new T8OperationExecutionRequest(operationId, ExecutionType.SYNCHRONOUS, operationInput));
                                serverResponse = mainServerContext.handleRequest(serverRequest);
                                operationOutput = (Map<String, Object>)serverResponse.getResponseData();
                                resultCount = operationOutput != null ? (Integer)operationOutput.get(T8DataManagerResource.PARAMETER_DATA_ENTITY_COUNT) : 0;

                                // Write the JSON/JSONP output to the HTTP response.
                                callbackFunction = request.getParameter("callback");
                                if (Strings.isNullOrEmpty(callbackFunction))
                                {
                                    response.setContentType("application/json");
                                    response.setCharacterEncoding("UTF-8");
                                    response.getWriter().write(T8DataEntitySerializer.serializeEntityCount(pathId, resultCount).toString());
                                }
                                else // If we found a callback function as part of the query parameters, return JSONP.
                                {
                                    String jsonString;

                                    // Serialize the output.
                                    jsonString = T8DataEntitySerializer.serializeEntityCount(pathId, resultCount).toString();

                                    // Write the output as JSONP format using the supplied callback function.
                                    response.setContentType("application/javascript");
                                    response.setCharacterEncoding("UTF-8");
                                    response.getWriter().write(callbackFunction + "(" + jsonString + ");");
                                }
                            }
                            catch (IOException e)
                            {
                                throw e;
                            }
                            catch (Exception e)
                            {
                                throw new ServletException("Exception while fetching data from entity: " + pathId, e);
                            }
                        }
                        break;
                        case DATA:
                        {
                            try
                            {
                                Map<String, Object> operationInput;
                                Map<String, Object> operationOutput;
                                List<T8DataEntity> retrievedEntities;
                                String operationId;
                                String callbackFunction;
                                T8DataFilter dataFilter;
                                Integer pageOffset;
                                Integer pageSize;

                                // Get the request parameters.
                                operationId = T8DataManagerResource.OPERATION_SELECT_DATA_ENTITIES;
                                if (request.getContentLength() > 0)
                                {
                                    JsonObject jsonObject;

                                    // Read the JSON object from the input reader.
                                    jsonObject = JsonObject.readFrom(request.getReader());
                                    dataFilter = new T8RecordFilterSerializer().deserializeDataFilter(jsonObject);
                                    pageOffset = jsonObject.getInt("pageOffset", 0);
                                    pageSize = jsonObject.getInt("pageSize", 10_000);
                                }
                                else
                                {
                                    dataFilter = null;
                                    pageOffset = 0;
                                    pageSize = 10_000;
                                }

                                // Create the operation input parameter map.
                                operationInput = new HashMap<>();
                                operationInput.put(T8DataManagerResource.PARAMETER_DATA_ENTITY_IDENTIFIER, pathId);
                                operationInput.put(T8DataManagerResource.PARAMETER_DATA_FILTER, dataFilter);
                                operationInput.put(T8DataManagerResource.PARAMETER_PAGE_OFFSET, pageOffset);
                                operationInput.put(T8DataManagerResource.PARAMETER_PAGE_SIZE, pageSize);

                                // Create the server request and handle it.
                                serverRequest = new T8ServerRequest(context, RequestType.EXECUTE_OPERATION, new T8OperationExecutionRequest(operationId, ExecutionType.SYNCHRONOUS, operationInput));
                                serverResponse = mainServerContext.handleRequest(serverRequest);
                                operationOutput = (Map<String, Object>)serverResponse.getResponseData();
                                retrievedEntities = operationOutput != null ? (List<T8DataEntity>)operationOutput.get(T8DataManagerResource.PARAMETER_DATA_ENTITY_LIST) : null;

                                // Write the JSON/JSONP output to the HTTP response.
                                callbackFunction = request.getParameter("callback");
                                if (Strings.isNullOrEmpty(callbackFunction))
                                {
                                    response.setContentType("application/json");
                                    response.setCharacterEncoding("UTF-8");
                                    response.getWriter().write(T8DataEntitySerializer.serializeToJSON(dataFilter != null ? dataFilter.getFieldIdentifiers() : null, retrievedEntities).toString());
                                }
                                else // If we found a callback function as part of the query parameters, return JSONP.
                                {
                                    String jsonString;

                                    // Serialize the output.
                                    jsonString = T8DataEntitySerializer.serializeToJSON(dataFilter != null ? dataFilter.getFieldIdentifiers() : null, retrievedEntities).toString();

                                    // Write the output as JSONP format using the supplied callback function.
                                    response.setContentType("application/javascript");
                                    response.setCharacterEncoding("UTF-8");
                                    response.getWriter().write(callbackFunction + "(" + jsonString + ");");
                                }
                            }
                            catch (IOException e)
                            {
                                throw e;
                            }
                            catch (Exception e)
                            {
                                throw new ServletException("Exception while fetching data from entity: " + pathId, e);
                            }
                        }
                        break;
                        case DATA_TABLE:
                        {
                            try
                            {
                                Map<String, Object> operationInput;
                                Map<String, Object> operationOutput;
                                List<T8DataEntity> retrievedEntities;
                                String operationId;
                                String callbackFunction;
                                T8DataFilter dataFilter;
                                Integer pageOffset;
                                Integer pageSize;

                                // Get the request parameters.
                                operationId = T8DataManagerResource.OPERATION_SELECT_DATA_ENTITIES;
                                if (request.getContentLength() > 0)
                                {
                                    JsonObject jsonObject;

                                    // Read the JSON object from the input reader.
                                    jsonObject = JsonObject.readFrom(request.getReader());
                                    dataFilter = new T8RecordFilterSerializer().deserializeDataFilter(jsonObject);
                                    pageOffset = jsonObject.getInt("pageOffset", 0);
                                    pageSize = jsonObject.getInt("pageSize", 100);
                                }
                                else
                                {
                                    dataFilter = null;
                                    pageOffset = 0;
                                    pageSize = 100;
                                }

                                // Create the operation input parameter map.
                                operationInput = new HashMap<>();
                                operationInput.put(T8DataManagerResource.PARAMETER_DATA_ENTITY_IDENTIFIER, pathId);
                                operationInput.put(T8DataManagerResource.PARAMETER_DATA_FILTER, dataFilter);
                                operationInput.put(T8DataManagerResource.PARAMETER_PAGE_OFFSET, pageOffset);
                                operationInput.put(T8DataManagerResource.PARAMETER_PAGE_SIZE, pageSize);

                                // Create the server request and handle it.
                                serverRequest = new T8ServerRequest(context, RequestType.EXECUTE_OPERATION, new T8OperationExecutionRequest(operationId, ExecutionType.SYNCHRONOUS, operationInput));
                                serverResponse = mainServerContext.handleRequest(serverRequest);
                                operationOutput = (Map<String, Object>)serverResponse.getResponseData();
                                retrievedEntities = operationOutput != null ? (List<T8DataEntity>)operationOutput.get(T8DataManagerResource.PARAMETER_DATA_ENTITY_LIST) : null;

                                // Write the JSON/JSONP output to the HTTP response.
                                callbackFunction = request.getParameter("callback");
                                if (Strings.isNullOrEmpty(callbackFunction))
                                {
                                    response.setContentType("application/json");
                                    response.setCharacterEncoding("UTF-8");
                                    response.getWriter().write(T8DataEntitySerializer.serializeToJSONTable(dataFilter != null ? dataFilter.getFieldIdentifiers() : null, retrievedEntities).toString());
                                }
                                else // If we found a callback function as part of the query parameters, return JSONP.
                                {
                                    String jsonString;

                                    // Serialize the output.
                                    jsonString = T8DataEntitySerializer.serializeToJSONTable(dataFilter != null ? dataFilter.getFieldIdentifiers() : null, retrievedEntities).toString();

                                    // Write the output as JSONP format using the supplied callback function.
                                    response.setContentType("application/javascript");
                                    response.setCharacterEncoding("UTF-8");
                                    response.getWriter().write(callbackFunction + "(" + jsonString + ");");
                                }
                            }
                            catch (IOException e)
                            {
                                throw e;
                            }
                            catch (Exception e)
                            {
                                throw new ServletException("Exception while fetching data from entity: " + pathId, e);
                            }
                        }
                        break;
                        case HEARTBEAT:
                        {
                            long startTime;

                            // Send the request to the server.
                            startTime = System.currentTimeMillis();
                            serverRequest = new T8ServerRequest(context, RequestType.HEARTBEAT, null);
                            serverResponse = mainServerContext.handleRequest(serverRequest);
                            if (serverResponse.getResponseType() == ResponseType.HEARTBEAT)
                            {
                                T8HeartbeatResponse heartbeatResponse;
                                JsonValue jsonHeartbeatResponse;

                                // Request the heartbeat response from the server.
                                heartbeatResponse = (T8HeartbeatResponse)serverResponse.getResponseData();
                                jsonHeartbeatResponse = new T8DtHeartbeatResponse(null).serialize(heartbeatResponse);

                                // Create a new thread printer.
                                response.setContentType("application/json");
                                response.setCharacterEncoding("UTF-8");
                                response.getWriter().write(jsonHeartbeatResponse.toString());
                                LOGGER.log("Heartbeat: " + (System.currentTimeMillis() - startTime) + "ms");
                            }
                            else
                            {
                                // On error, just return internal server response code.
                                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                            }
                        }
                        break;
                        case SYSTEM:
                        {
                            switch (pathId)
                            {
                                case "THREADS":
                                    ThreadPrinter threadPrinter;
                                    PrintStream printStream;

                                    // Create a new thread printer.
                                    response.setContentType("text/plain");
                                    printStream = new PrintStream(response.getOutputStream());
                                    threadPrinter = new ThreadPrinter(printStream);
                                    threadPrinter.threadDump();
                                    printStream.flush();
                                    break;
                                default:
                                    throw new ServletException("Invalid system operation: " + pathId);
                            }
                        }
                        break;
                        case API:
                        {
                            T8HttpApi httpApi;

                            // Try to find an api that matches the path identifier.
                            httpApi = mainServerContext.getMainServer().getHttpApi(pathTypeString);
                            if (httpApi != null)
                            {
                                String contentType;

                                // Parse operation input/output depending on the request MIME type.
                                contentType = request.getContentType();
                                if ((contentType == null) || (contentType.startsWith("application/json")) || (contentType.startsWith("text/plain"))) // JSON is the default MIME type used for operation execution requests.
                                {
                                    try
                                    {
                                        PrintWriter outputWriter;
                                        T8HttpApiResult result;
                                        JsonObject outputJson;
                                        JsonObject inputJson;

                                        // Parse the operation input JSON.
                                        inputJson = (request.getContentLength() > 0) ? JsonObject.readFrom(request.getReader()) : null;

                                        // Use the api to handle the request.
                                        result = httpApi.handleRequest(context, request.getMethod(), pathString.substring(pathTypeString.length()), servletRequest.getParameterMap(), inputJson);

                                        // Write the HTML output to the HTTP response.
                                        response.setContentType("application/json");
                                        response.setCharacterEncoding("UTF-8");
                                        response.setStatus(result.getStatusCode());

                                        // Write the response object to the output.
                                        outputJson = result.getOutput();
                                        if (outputJson != null)
                                        {
                                            outputWriter = response.getWriter();
                                            outputWriter.print(outputJson.toString());
                                            outputWriter.flush();
                                        }
                                    }
                                    catch (IOException e)
                                    {
                                        throw e;
                                    }
                                    catch (Exception e)
                                    {
                                        throw new ServletException("Exception while invoking api " + pathTypeString + " using url: " + pathString, e);
                                    }
                                }
                            }
                            else throw new ServletException("Path not found: " + pathString);
                        }
                        break;
                        default:
                        {
                            throw new ServletException("Path not found: " + pathString);
                        }
                    }
                }
                else throw new ServletException("Path not found: " + pathString);
            }
            finally
            {
                // Unbind the request thread from the data session.
                dataManager.closeCurrentSession();
                securityManager.destroyContext();
            }
        }
        else
        {
            // Write the authentication response as JSON to the servlet response.
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(authenticationResponse.serialize().toString());
        }
    }

    private void setResponseHeaders(HttpServletRequest request, HttpServletResponse response)
    {
        String clientOrigin;

        // Get the request origin.
        clientOrigin = request.getHeader("origin");

        // Add the CORS headers.
        response.setHeader("Access-Control-Allow-Origin", clientOrigin);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
        response.setHeader("Access-Control-Max-Age", "604800");
    }
}
