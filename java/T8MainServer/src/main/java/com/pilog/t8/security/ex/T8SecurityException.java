package com.pilog.t8.security.ex;

/**
 * @author Gavin Boshoff
 */
public class T8SecurityException extends Exception
{
    public T8SecurityException(String message)
    {
        super(message);
    }

    public T8SecurityException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public T8SecurityException(Throwable cause)
    {
        super(cause);
    }
}
