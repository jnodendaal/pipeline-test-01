package com.pilog.t8.mainserver;

/**
 * @author Bouwer du Preez
 */
public class T8MainServerContext
{
    private final T8MainServer mainServer;
    
    public T8MainServerContext(T8MainServer mainServer)
    {
        this.mainServer = mainServer;
    }
    
    T8MainServer getMainServer()
    {
        return mainServer;
    }
    
    public T8ServerResponse handleRequest(T8ServerRequest request)
    {
        return mainServer.handleRequest(request);
    }
}
