package com.pilog.t8.mainserver;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.log.T8Logger;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author Bouwer du Preez
 */
public class T8CodeVersionScanner
{
    private static final T8Logger logger = T8Log.getLogger(T8CodeVersionScanner.class);

    public static Map<String, String> getServerSideCodeVersions(T8ServerContext serverContext)
    {
        return getCodeVersions(serverContext, T8ClassScanner.SERVER_SIDE_JAR_PATH);
    }

    private static Map<String, String> getCodeVersions(T8ServerContext serverContext, String classPath)
    {
        T8ClassScanner classScanner;
        Map<String, String> versionMap;
        Set<Class> classes;

        classScanner = new T8ClassScanner(serverContext, classPath);
        classes = classScanner.getPackageClasses("com.pilog.version");
        versionMap = new TreeMap<>();
        for (Class versionClass : classes)
        {
            try
            {
                versionMap.put(versionClass.getCanonicalName(), (String)versionClass.getField("VERSION").get(null));
            }
            catch (Exception e)
            {
                logger.log("Exception while checking code version in class: " + versionClass.getCanonicalName(), e);
                versionMap.put(versionClass.getCanonicalName(), "Not Found");
            }
        }

        return versionMap;
    }

    private T8CodeVersionScanner() {}
}
