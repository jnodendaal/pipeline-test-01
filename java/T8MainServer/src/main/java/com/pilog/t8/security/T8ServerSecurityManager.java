package com.pilog.t8.security;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerModule;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.event.T8DefinitionDeletedEvent;
import com.pilog.t8.definition.event.T8DefinitionManagerEventAdapter;
import com.pilog.t8.definition.event.T8DefinitionRenamedEvent;
import com.pilog.t8.definition.event.T8DefinitionSavedEvent;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.user.T8UserProfile;
import com.pilog.t8.definition.data.org.T8OrganizationStructureCacheDefinition;
import com.pilog.t8.definition.language.T8LanguageDefinition;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.definition.remote.server.connection.T8RemoteConnectionUser;
import com.pilog.t8.definition.system.T8ConfigurationManagerResource;
import com.pilog.t8.definition.system.T8SecurityManagerResource.UserHistoryEvent;
import com.pilog.t8.definition.user.T8ClientUserDefinition;
import com.pilog.t8.definition.user.T8DeveloperUserDefinition;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.definition.user.T8UserProfileDefinition;
import com.pilog.t8.definition.user.password.T8PasswordPolicyDefinition;
import com.pilog.t8.definition.user.password.T8PasswordPolicyRuleDefinition;
import com.pilog.t8.definition.user.password.T8PasswordRuleValidator;
import com.pilog.t8.security.event.T8SessionEvent;
import com.pilog.t8.security.event.T8SessionEventListener;
import com.pilog.t8.security.event.T8SessionLogoutEvent;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.event.EventListenerList;
import org.joda.time.DateTime;
import org.joda.time.Days;
import com.pilog.t8.http.T8HttpServletRequest;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.definition.flow.T8WorkFlowProfileDefinition;
import com.pilog.t8.definition.remote.server.connection.T8ConnectionDefinition;
import com.pilog.t8.definition.security.authentication.T8AuthenticationHandlerDefinition;
import com.pilog.t8.definition.user.T8UserRemoteServerConnectionDefinition;
import com.pilog.t8.functionality.access.T8FunctionalityAccessRights;
import com.pilog.t8.security.authentication.T8AuthenticationHandler;
import com.pilog.t8.security.authentication.T8AuthenticationResponse;
import com.pilog.t8.security.authentication.T8AuthenticationResponse.ResponseType;
import com.pilog.t8.security.authentication.T8DefaultAuthenticationResponse;
import com.pilog.t8.utilities.codecs.Base64Codec;
import java.util.Calendar;

/**
 * @author Bouwer du Preez
 */
public class T8ServerSecurityManager implements T8SecurityManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerSecurityManager.class);

    private final T8SecurityPersistenceHandler persistenceHandler;
    private final T8ServerContext serverContext;
    private final Map<String, T8SessionContextHandle> activeSessions; // Stores currently active user sessions <sessionId:T8SessionContextHandle>.
    private final Map<String, String> activeTokens; // Stores all authentication tokens linked to currently active session <token:sessionId>.
    private final Map<String, String> usernameCache; // Stores the usernames of all registered users <userName:userId>.
    private final Map<String, T8UserDefinition> userDefinitionCache; // Stores the definitions of all users of the system.
    private final Map<String, T8AuthenticationHandler> authenticationHandlers; // Stores handlers used to authenticate http requests.
    private final EventListenerList sessionListeners;
    private final ScheduledExecutorService userMaintenanceExecutorService; // Timer used to schedule maintenance tasks.
    private final Map<Thread, T8Context> threadAccess; // Stores the access context for each authorized thread in the system.
    private final long initializationThreadId; // This ID is used to perform security checks on some of the methods.
    private T8DefinitionManager definitionManager;
    private T8Context internalContext;
    private String serverSessionId;

    private static final String DEFAULT_LANGUAGE_IDENTIFIER = "@L_EN_US"; // en-US: The default to be used when no other language can be resolved.
    private static final String DEFAULT_CONTENT_LANGUAGE_IDENTIFIER = "CC0092ADF415443DB36744B5D9EF96E9"; // en-US: The default to be used when no other language can be resolved.
    private static final long SESSION_EXPIRATION_TIME = TimeUnit.MINUTES.toMillis(1); // The number of miliseconds for which a session will be maintained after the last recorded activity.
    private static final long USER_EXPIRATION_CHECKER_INTERVAL = TimeUnit.MINUTES.toMillis(5); // The interval at which the user expiration checker will run.
    private static final int DEFAULT_TEMP_PW_EXPIRATION_TIME = 30; // The number of minutes for which a temporary password is valid.

    // Details for the communication to be sent when user details are updated.
    public static final String COMMUNICATION_NEW_USER_IDENTIFIER = "@COMM_NEW_USER";
    public static final String COMMUNICATION_PASSWORD_CHANGE_IDENTIFIER = "@COMM_USER_PASSWORD_CHANGE";
    public static final String COMMUNICATION_PARAMETER_USER_IDENTIFIER = "$P_USER_IDENTIFIER";
    public static final String COMMUNICATION_PARAMETER_USERNAME = "$P_USERNAME";
    public static final String COMMUNICATION_PARAMETER_TEMPORARY_PASSW = "$P_TEMPORARY_PASSW";

    public T8ServerSecurityManager(T8ServerContext serverContext)
    {
        this.initializationThreadId = Thread.currentThread().getId();
        this.serverContext = serverContext;
        createServerSessionContext();
        this.usernameCache = Collections.synchronizedMap(new HashMap<String, String>());
        this.userDefinitionCache = Collections.synchronizedMap(new HashMap<String, T8UserDefinition>());
        this.activeTokens = Collections.synchronizedMap(new HashMap<String, String>());
        this.activeSessions = Collections.synchronizedMap(new HashMap<String, T8SessionContextHandle>());
        this.threadAccess = Collections.synchronizedMap(new HashMap<Thread, T8Context>());
        this.sessionListeners = new EventListenerList();
        this.persistenceHandler = new T8SecurityPersistenceHandler(internalContext);
        this.userMaintenanceExecutorService = Executors.newScheduledThreadPool(2, new NameableThreadFactory("T8SecurityManager"));
        this.authenticationHandlers = new HashMap<>();
    }

    private void createServerSessionContext()
    {
        T8Session internalSession;

        // Create the new session.
        serverSessionId = T8IdentifierUtilities.createNewGUID();
        internalSession = new T8Session(serverSessionId);
        internalSession.setUserIdentifier("T8SERVER");
        internalSession.setUserProfileIdentifier("T8SERVER");
        internalSession.setSystemAgentIdentifier("@T8SERVER");
        internalSession.setOrganizationIdentifier(null);
        internalSession.setServerURL(System.getProperty("SERVER_URL"));
        internalSession.setLanguageIdentifier(DEFAULT_LANGUAGE_IDENTIFIER);
        internalSession.setContentLanguageIdentifier(DEFAULT_CONTENT_LANGUAGE_IDENTIFIER);
        internalSession.setSystemSession(true);
        internalContext = new T8Context(serverContext, internalSession);
    }

    @Override
    public void init()
    {
        this.userMaintenanceExecutorService.scheduleWithFixedDelay(new SessionExpirationChecker(), SESSION_EXPIRATION_TIME, SESSION_EXPIRATION_TIME, TimeUnit.MILLISECONDS);
        this.userMaintenanceExecutorService.scheduleWithFixedDelay(new T8UserExpirationChecker(internalContext), USER_EXPIRATION_CHECKER_INTERVAL, USER_EXPIRATION_CHECKER_INTERVAL, TimeUnit.MILLISECONDS);
    }

    @Override
    public void start() throws Exception
    {
        this.definitionManager = serverContext.getDefinitionManager();

        // We add a new definition manager listener to ensure we keep the cache(s) up to date
        this.definitionManager.addDefinitionManagerListener(new T8SecurityDefinitionManagerListener());

        // Cache usernames.
        cacheUserDetails();

        // Cache authentication handlers.
        cacheAuthenticationHandlers();
    }

    @Override
    public void destroy()
    {
        // Clear all user sessions.
        activeSessions.clear();

        // Terminate the timer thread and all scheduled tasks.
        userMaintenanceExecutorService.shutdown();
    }

    private void cacheUserDetails()
    {
        try
        {
            List<T8Definition> userDefinitions;

            LOGGER.log("Caching user details...");
            userDefinitions = definitionManager.getRawGroupDefinitions(internalContext, null, T8UserDefinition.GROUP_IDENTIFIER);
            for (T8Definition definition : userDefinitions)
            {
                T8UserDefinition userDefinition;
                String username;
                String userId;
                String existingId;

                userDefinition = (T8UserDefinition)definition;
                username = userDefinition.getUsername();
                userId = userDefinition.getIdentifier();
                existingId = usernameCache.get(username);

                // Cache the user definition.
                userDefinitionCache.put(userId, userDefinition);

                // Make sure no existing identifier was found (usernames have to be unique system-wide).
                if (existingId == null)
                {
                    usernameCache.put(username, userId);
                }
                else throw new Exception("Duplicate username '" + username + "' found in definition '" + userId + "' and '" + existingId + "'.");
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while caching usernames.", e);
        }
    }

    private void cacheAuthenticationHandlers()
    {
        try
        {
            List<String> handlerIds;

            LOGGER.log("Caching authentication handlers...");
            handlerIds = definitionManager.getGroupDefinitionIdentifiers(null, T8AuthenticationHandlerDefinition.GROUP_IDENTIFIER);
            for (String handlerId : handlerIds)
            {
                cacheAuthenticationHandler(handlerId);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while caching authentication handlers.", e);
        }
    }

    private void cacheAuthenticationHandler(String handlerId)
    {
        try
        {
            T8AuthenticationHandlerDefinition handlerDefinition;

            handlerDefinition = (T8AuthenticationHandlerDefinition)definitionManager.getInitializedDefinition(internalContext, null, handlerId, null);
            if (handlerDefinition != null)
            {
                T8AuthenticationHandler handler;

                // Create the new handler.
                handler = handlerDefinition.createAuthenticationHandler(serverContext);

                // Register all users to the handler.
                for (T8UserDefinition userDefinition : userDefinitionCache.values())
                {
                    handler.registerUser(userDefinition);
                }

                // Cache the handler.
                authenticationHandlers.put(handlerId, handler);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while caching authentication handler: " + handlerId, e);
        }
    }

    /**
     * Returns the context to be used internal to the main server.  This
     * method can only be invoked from the same thread that created this
     * instance of the security manager.
     * @return The context to be used by the main server.
     */
    @Override
    public T8Context getMainServerContext()
    {
        if (initializationThreadId == Thread.currentThread().getId())
        {
            return internalContext;
        }
        else throw new RuntimeException("Security Exception."); // No more information is required as part of this message.
    }

    @Override
    public final T8Context createOrganizationContext(T8Context context, String orgId)
    {
        try
        {
            if (context ==  internalContext)
            {
                T8OrganizationSetupDefinition organizationSetupDefinition;
                String sessionIdentifier;
                T8Session session;
                String rootOrganizationID;

                // Get the organization structure.
                if (orgId != null)
                {
                    T8DataManager dataManager;
                    T8OrganizationStructure orgStructure;

                    dataManager = serverContext.getDataManager();
                    orgStructure = (T8OrganizationStructure)dataManager.getCachedData(T8OrganizationStructureCacheDefinition.DATA_IDENTIFIER, HashMaps.newHashMap(T8OrganizationStructureCacheDefinition.DATA_PARAMETER_ORG_ID, orgId));
                    rootOrganizationID = orgStructure.getRootOrganizationID();
                }
                else rootOrganizationID = null;

                // Fetcht the organization setup if an organization is specified.
                organizationSetupDefinition = orgId != null ? (T8OrganizationSetupDefinition)definitionManager.getResolvedDefinition(null, T8OrganizationSetupDefinition.TYPE_IDENTIFIER, HashMaps.newHashMap(T8OrganizationSetupDefinition.RESOLUTION_PARAMETER_ORG_ID, orgId)) : null;

                // Create the new session.
                sessionIdentifier = T8IdentifierUtilities.createNewGUID();
                session = new T8Session(sessionIdentifier);
                session.setSystemSession(true);
                session.setSystemAgentIdentifier(context.getSystemAgentId());
                session.setSystemAgentInstanceIdentifier(context.getSystemAgentIid());
                session.setUserIdentifier(null);
                session.setUserProfileIdentifier(null);
                session.setOrganizationIdentifier(orgId);
                session.setRootOrganizationIdentifier(rootOrganizationID);
                session.setServerURL(null);
                session.setLanguageIdentifier(DEFAULT_LANGUAGE_IDENTIFIER);
                session.setContentLanguageIdentifier(organizationSetupDefinition != null ? organizationSetupDefinition.getDefaultContentLanguageIdentifier() : DEFAULT_CONTENT_LANGUAGE_IDENTIFIER);

                // Return the newly create session.
                return new T8Context(serverContext, session);
            }
            else throw new RuntimeException("Cannot create server session from the invoking session."); // No more information is required as part of this message.
        }
        catch (Exception e)
        {
            throw new RuntimeException("Security Exception", e);
        }
    }

    @Override
    public final T8Context createServerModuleContext(T8ServerModule module)
    {
        try
        {
            String sessionId;
            T8Session session;

            // Create the new session.
            sessionId = T8IdentifierUtilities.createNewGUID();
            session = new T8Session(sessionId);
            session.setSystemSession(true);
            session.setSystemAgentIdentifier(module.getClass().getName());
            session.setSystemAgentInstanceIdentifier(null);
            session.setUserIdentifier(null);
            session.setUserProfileIdentifier(null);
            session.setOrganizationIdentifier(null);
            session.setRootOrganizationIdentifier(null);
            session.setServerURL(null);
            session.setLanguageIdentifier(DEFAULT_LANGUAGE_IDENTIFIER);
            session.setContentLanguageIdentifier(DEFAULT_CONTENT_LANGUAGE_IDENTIFIER);

            // Return the newly create session.
            return new T8Context(serverContext, session);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Security Exception", e);
        }
    }

    @Override
    public T8SessionContext createNewSessionContext()
    {
        T8ConfigurationManager configurationManager;
        T8Session session;
        String guid;

        // Get the configuration manager.
        configurationManager = serverContext.getConfigurationManager();

        guid = T8IdentifierUtilities.createNewGUID();
        session = new T8Session(guid);
        session.setUserIdentifier("ANONYMOUS_" + guid); // Anonymous user always has the session identifier as part of the user identifier.
        session.setUserProfileIdentifier("ANONYMOUS_" + guid); // Anonymous user always has the session identifier as part of the user profile identifier.
        session.setOrganizationIdentifier(null);
        session.setServerURL(null);
        session.setLanguageIdentifier((String)configurationManager.getProperty(null, T8ConfigurationManagerResource.PROPERTY_DEFAULT_LANGUAGE_IDENTIFIER));
        session.setContentLanguageIdentifier(DEFAULT_CONTENT_LANGUAGE_IDENTIFIER);
        return session;
    }


    @Override
    public void lockUser(T8Context context, String userId, String lockReason) throws Exception
    {
        T8UserDefinition userDefinition;

        userDefinition = getUserDefinition(userId);
        if (userDefinition == null) userDefinition = findUserDefinition(userId);
        if (userDefinition != null)
        {
            userDefinition.setUserLocked(true);
            userDefinition.setUserLockReason(lockReason);
            definitionManager.saveDefinition(context, userDefinition, null);
        } else throw new Exception("User definition with user identifier " + userId + " not found.");
    }

    @Override
    public void unlockUser(T8Context context, String userId) throws Exception
    {
        T8UserDefinition userDefinition;

        userDefinition = getUserDefinition(userId);
        if (userDefinition == null) userDefinition = findUserDefinition(userId);
        if (userDefinition != null)
        {
            userDefinition.setUserLocked(false);
            userDefinition.setUserLockReason(null);
            definitionManager.saveDefinition(context, userDefinition, null);
        }
        else throw new Exception("User definition with user identifier " + userId + " not found.");
    }

    @Override
    public T8UserLoginResponse login(T8Context context, LoginLocation loginLocation, String username, char[] password, boolean endExistingSession)
    {
        try
        {
            T8SessionContextHandle existingHandle;

            // First make sure that the session has not already been logged in.
            existingHandle = activeSessions.get(context.getSessionId());
            if (existingHandle != null)
            {
                T8UserDefinition userDefinition;

                // Find the user definition required for session login.
                userDefinition = getUserDefinition(existingHandle.getSessionContext().getUserIdentifier());
                return new T8UserLoginResponse(context, T8LoginResponseType.SESSION_ALREADY_LOGGED_IN, "Session already logged in.", getSessionDetails(existingHandle, userDefinition));
            }
            else
            {
                T8UserDefinition userDefinition;

                // Find the user definition required for session login.
                userDefinition = getUserDefinition(username);
                if (userDefinition == null) userDefinition = findUserDefinition(username);
                if (userDefinition != null)
                {
                    boolean tempPasswVerified;
                    boolean resetPassword;

                    // Check if the user entry point allows for the user type to log in.
                    if (!loginLocation.isValidUserType(userDefinition))
                    {
                        return new T8UserLoginResponse(context, T8LoginResponseType.USER_NOT_ALLOWED, "User cannot log in here", null);
                    }

                    // Check if the user is flagged as inactive.
                    if (!userDefinition.isActive())
                    {
                        return new T8UserLoginResponse(context, T8LoginResponseType.USER_INACTIVE, "User Inactive", null);
                    }

                    // Check for user expiration.
                    if (userDefinition.getUserExpiryDate() != null)
                    {
                        if (DateTime.now().isAfter(userDefinition.getUserExpiryDate().getMilliseconds()))
                        {
                            // If the user has expired, we need to make sure they are inactive as well
                            userDefinition.setActive(false);
                            this.definitionManager.saveDefinition(context, userDefinition, null);
                            return new T8UserLoginResponse(context, T8LoginResponseType.USER_EXPIRED, "User Expired", null);
                        }
                    }

                    // If the user acccount has been locked prevent access.
                    if (userDefinition.isUserLocked())
                    {
                        return new T8UserLoginResponse(context, T8LoginResponseType.USER_ACCOUNT_LOCKED, userDefinition.getUserLockReason(), null);
                    }

                    // Check details for temporary password login
                    if (isPasswordChangeAvailable(context, username))
                    {
                        tempPasswVerified = T8PasswordEncryptor.validatePassword(password, userDefinition.getTemporaryPasswHash());
                    }
                    else tempPasswVerified = false;

                    // Verify the password.
                    resetPassword = userDefinition.isResetPasswordFlag();
                    if ((resetPassword) || (tempPasswVerified) || ((password != null) && (password.length > 0) && (T8PasswordEncryptor.validatePassword(password, userDefinition.getPassword()))))
                    {
                        List<T8SessionContextHandle> activeHandles;
                        T8SessionContextHandle newSessionHandle;
                        List<String> profileIds;
                        String rootOrgId;
                        String profileId;
                        T8Session session;
                        Long sessionTimeout;
                        int maximumConcurrentSessionLimit;
                        int activeSessionCount;

                        // Check whether or not a session is already active for this user and handle the scenario if it occurs.
                        maximumConcurrentSessionLimit = userDefinition.getMaximumConcurrentSessionLimit();
                        activeHandles = getActiveSessionHandlesByUser(userDefinition.getIdentifier());
                        activeSessionCount = activeHandles.size();
                        if (activeSessionCount > 0)
                        {
                            // If we have a maximum concurrent session limit larger than the default (1), make sure that we do not exceed the limit.
                            if (maximumConcurrentSessionLimit > 1)
                            {
                                if (activeSessionCount >= maximumConcurrentSessionLimit)
                                {
                                    return new T8UserLoginResponse(context, T8LoginResponseType.MAXIMUM_CONCURRENT_SESSION_LIMIT_REACHED, "Maximum concurrent session limit reached.", null);
                                }
                            }
                            else // So maximum concurrent session limit is the default (1) and we have one logged in session.
                            {
                                // If we are allowed to end the current session do that now, otherwise return the login failure.
                                if (endExistingSession)
                                {
                                    logout(context, activeHandles.get(0).getSessionContext().getSessionIdentifier());
                                }
                                else
                                {
                                    return new T8UserLoginResponse(context, T8LoginResponseType.USER_ALREADY_LOGGED_IN, "User already logged in.", getSessionDetails(activeHandles.get(0), userDefinition));
                                }
                            }
                        }

                        // Get the organization structure.
                        rootOrgId = resolveRootOrganizationId(userDefinition.getOrganizationIdentifier());

                        // Use the first profile as the default.
                        sessionTimeout = definitionManager.getSystemDefinition(internalContext).getSessionTimeout();
                        profileIds = userDefinition.getProfileIdentifiers();
                        if ((profileIds == null) || (profileIds.isEmpty()))
                        {
                            return new T8UserLoginResponse(context, T8LoginResponseType.FAILURE, "No user profiles linked. Please contact your administrator.", null);
                        }
                        else
                        {
                            profileId = profileIds.get(0);
                        }

                        // Set the user details on the session.
                        session = (T8Session)context.getSessionContext();
                        session.setSessionIdentifier(T8IdentifierUtilities.createNewGUID());
                        session.setUserIdentifier(userDefinition.getIdentifier());
                        session.setUserProfileIdentifier(profileId);
                        session.setWorkFlowProfileIdentifiers(userDefinition.getWorkFlowProfileIdentifiers());

                        // Make sure that there is always at least a default language specified.
                        session.setLanguageIdentifier(userDefinition.getLanguageIdentifier() == null ? DEFAULT_LANGUAGE_IDENTIFIER : userDefinition.getLanguageIdentifier());
                        session.setContentLanguageIdentifier(getContentLanguageIdentifier(userDefinition.getLanguageIdentifier()));
                        session.setOrganizationIdentifier(userDefinition.getOrganizationIdentifier());
                        session.setRootOrganizationIdentifier(rootOrgId);
                        session.setUserName(userDefinition.getName());
                        session.setUserSurname(userDefinition.getSurname());

                        // Set the logged-in flag.
                        session.setLoggedIn(true);

                        // Update the context to reflect the changes to the session.
                        context.resetSessionContext();

                        // Create a session handle for the session and add it to the active session collection.
                        newSessionHandle = new T8SessionContextHandle(session);
                        newSessionHandle.setAccessibleUserProfileIdentifiers(userDefinition.getProfileIdentifiers());
                        newSessionHandle.setLoginTime(System.currentTimeMillis());
                        newSessionHandle.setLastActivityTime(System.currentTimeMillis());
                        newSessionHandle.setSessionTimeout(sessionTimeout == null ? Long.MAX_VALUE : sessionTimeout);
                        activeSessions.put(context.getSessionId(), newSessionHandle);

                        // Log the event and return the response.
                        persistenceHandler.logUserEvent(context, UserHistoryEvent.LOG_IN);

                        //Set the last login date.
                        userDefinition.setLastLoginDate(new Date());
                        userDefinition.setIncorrectPasswordEntryCount(0);
                        userDefinition.setUserLockReason(null);

                        // Reset the temporary password, regardless of whether it was
                        // used or if it expired or whatever. We no longer want it available
                        userDefinition.setTempPasswExpiryTime(null);
                        userDefinition.setTempPasswHash(null);
                        definitionManager.saveDefinition(context, userDefinition, null);

                        // If the password should be reset the just skip the expiry check because it might mess things up
                        if (!resetPassword && !tempPasswVerified)
                        {
                            T8UserLoginResponse checkPasswordExpiry;

                            checkPasswordExpiry = checkPasswordExpiry(context, newSessionHandle, userDefinition);
                            if (checkPasswordExpiry != null) return checkPasswordExpiry;
                        }

                        // Return the login response.
                        return new T8UserLoginResponse(context, userDefinition.getApiKey(), ((resetPassword || tempPasswVerified) ? T8LoginResponseType.SUCCESS_NEW_PASSWORD : T8LoginResponseType.SUCCESS), getSessionDetails(newSessionHandle, userDefinition));
                    }
                    else // Incorrect password.
                    {
                        Integer incorrectEntryCount;
                        T8UserLoginResponse loginResponse;

                        // Get the user current incorrect password entry count.
                        incorrectEntryCount = userDefinition.getIncorrectPasswordEntryCount();

                        // Set a default value if not available.
                        if (incorrectEntryCount == null) incorrectEntryCount = 0;

                        // Increment the value by one and save it to the definition.
                        incorrectEntryCount++;

                        // Update the user definition.
                        userDefinition.setIncorrectPasswordEntryCount(incorrectEntryCount);
                        definitionManager.saveDefinition(context, userDefinition, null);
                        loginResponse = checkPasswordIncorrectEntries(context, userDefinition);
                        return loginResponse != null ? loginResponse : new T8UserLoginResponse(context, T8LoginResponseType.FAILURE);
                    }
                }
                else return new T8UserLoginResponse(context, T8LoginResponseType.FAILURE);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("While attempting user login: " + username, e);
            return new T8UserLoginResponse(context, T8LoginResponseType.FAILURE);
        }
    }

    @Override
    public boolean logout(T8Context context)
    {
        return logout(context, context.getSessionId());
    }

    /**
     * Logs the specified session out, effectively ending its 'active' existence.
     * @param context The context from which this logout is initiated.
     * @param sessionId The id of the session to end.
     * @return A boolean flag that is true if the session was found to be active and logged out successfully.
     */
    private boolean logout(T8Context context, String sessionId)
    {
        try
        {
            T8SessionContextHandle sessionHandle;

            sessionHandle = activeSessions.remove(sessionId);
            if (sessionHandle != null)
            {
                T8Session session;

                // Set the logged-in flag.
                session = (T8Session)sessionHandle.getSessionContext();
                session.setLoggedIn(false);

                // Remove the authentication token from the active collection.
                activeTokens.remove(sessionHandle.getAuthenticationToken());

                // Log the event.
                persistenceHandler.logUserEvent(context, UserHistoryEvent.LOG_OUT);

                // Fire the event to let all listeners know that the sesssion was logged out.
                fireSessionEvent(new T8SessionLogoutEvent(session.getSessionIdentifier()));
                return true;
            }
            else return false;
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while logging out user session: " + sessionId, e);
            return false;
        }
    }

    private T8AuthenticationHandler getAuthenticationHandler(String authorizationHeader)
    {
        for (T8AuthenticationHandler handler : authenticationHandlers.values())
        {
            if (handler.isApplicable(authorizationHeader))
            {
                return handler;
            }
        }

        return null;
    }

    @Override
    public T8AuthenticationResponse authenticate(T8HttpServletRequest request)
    {
        T8SessionContext sessionContext;

        // First check to see if any session context is set for the http session and if not, create one.
        sessionContext = request.getSessionContext();
        if (sessionContext == null)
        {
            // Create a new anonymous session context.
            sessionContext = createNewSessionContext();

            // Store the valid session context on the http session to prevent subsequent unnecessary authentication.
            request.setSessionContext(sessionContext);
        }

        // Now handle authentication of the session.
        if (sessionContext.isLoggedIn())
        {
            return new T8DefaultAuthenticationResponse(ResponseType.COMPLETED, sessionContext);
        }
        else
        {
            try
            {
                String authorizationHeader;

                // Make sure to create a data session for the current thread.
                createContext(internalContext.getSessionContext());
                serverContext.getDataManager().openDataSession(internalContext);

                // If an authorization header is present, use it to log in.
                authorizationHeader = request.getAuthorizationHeader();
                if (authorizationHeader != null)
                {
                    // Trim whitespace from the header value.
                    authorizationHeader = authorizationHeader.trim();
                    if (authorizationHeader.startsWith("Basic"))
                    {
                        String credentialString;
                        int colonIndex;

                        // Get the credential string from the authorization header.
                        credentialString = authorizationHeader.substring(5).trim();
                        credentialString = Base64Codec.decodeString(credentialString, "UTF-8");
                        colonIndex = credentialString.indexOf(":");
                        if (colonIndex > -1)
                        {
                            T8UserLoginResponse loginResponse;
                            String username;
                            String password;

                            // Parse the username and password from the credential string and use them to perform a login request.
                            username = credentialString.substring(0, colonIndex);
                            password = credentialString.substring(colonIndex + 1);
                            loginResponse = login(new T8Context(serverContext, sessionContext), LoginLocation.WEB_SERVICE, username, password.toCharArray(), false);
                            if (loginResponse.getResponseType().isSessionAuthenticated())
                            {
                                // The login request was successful.
                                return new T8DefaultAuthenticationResponse(ResponseType.COMPLETED, sessionContext);
                            }
                            else return new T8DefaultAuthenticationResponse(ResponseType.FAILED, null);
                        }
                        else
                        {
                            LOGGER.log("Unrecognized authorization scheme in HTTP Request: " + authorizationHeader);
                            return new T8DefaultAuthenticationResponse(ResponseType.FAILED, null);
                        }
                    }
                    else if (authorizationHeader.startsWith("Token"))
                    {
                        T8UserDefinition userDefinition;
                        String tokenString;

                        // Get the token string from the authorization header.
                        tokenString = authorizationHeader.substring(5).trim();
                        userDefinition = findUserByApiKey(tokenString);
                        if (userDefinition != null)
                        {
                            return new T8DefaultAuthenticationResponse(ResponseType.COMPLETED, login(new T8Context(serverContext, sessionContext), userDefinition, tokenString, "Token", UserHistoryEvent.LOG_IN_API));
                        }
                        else throw new Exception("No user found for token " + tokenString);
                    }
                    else if (authorizationHeader.startsWith("Bearer"))
                    {
                        T8UserDefinition userDefinition;
                        String tokenString;

                        // Get the token string from the authorization header.
                        tokenString = authorizationHeader.substring(6).trim();
                        userDefinition = findUserByApiKey(tokenString);
                        if (userDefinition != null)
                        {
                            return new T8DefaultAuthenticationResponse(ResponseType.COMPLETED, login(new T8Context(serverContext, sessionContext), userDefinition, tokenString, "Bearer", UserHistoryEvent.LOG_IN_API));
                        }
                        else throw new Exception("No user found for bearer token " + tokenString);
                    }
                    else
                    {
                        T8AuthenticationHandler authHandler;

                        // Try to find a registered authentication handler that is applicable to this authorization scheme.
                        authHandler = getAuthenticationHandler(authorizationHeader);
                        if (authHandler != null)
                        {
                            T8AuthenticationResponse authenticationResponse;

                            authenticationResponse = authHandler.authenticate(request);
                            if (authenticationResponse.getResponseType() == T8AuthenticationResponse.ResponseType.COMPLETED)
                            {
                                T8UserDefinition userDefinition;
                                String userId;

                                userId = authenticationResponse.getUserId();
                                userDefinition = getUserDefinition(userId);
                                if (userDefinition != null)
                                {
                                    login(new T8Context(serverContext, sessionContext), userDefinition, userDefinition.getApiKey(), authHandler.getId(), UserHistoryEvent.LOG_IN_SSO);
                                    return authenticationResponse;
                                }
                                else throw new Exception("User " + userId + " identified by authentication handler " + authHandler.getId() + " not found.");
                            }
                            else return authenticationResponse;
                        }
                        else
                        {
                            LOGGER.log("Unrecognized authorization scheme in HTTP Request: " + authorizationHeader);
                            return new T8DefaultAuthenticationResponse(ResponseType.FAILED, null);
                        }
                    }
                }
                else
                {
                    // Attempt to use query parameters for authorization.
                    // TODO:  Implement query parameter authorization.
                    return new T8DefaultAuthenticationResponse(ResponseType.COMPLETED, sessionContext);
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception during HTTP Request authentication.", e);
                return new T8DefaultAuthenticationResponse(ResponseType.FAILED, null);
            }
            finally
            {
                // Unlink the data session from this thread.
                serverContext.getDataManager().closeCurrentSession();
                serverContext.getSecurityManager().destroyContext();
            }
        }
    }

    private T8SessionContext login(T8Context context, T8UserDefinition userDefinition, String authenticationToken, String authenticationHandleId, UserHistoryEvent eventType) throws Exception
    {
        try
        {
            T8SessionContextHandle newSessionHandle;
            T8OrganizationStructure orgStructure;
            List<String> profileIdentifiers;
            T8DataManager dataManager;
            String profileIdentifier;
            Long sessionTimeout;
            T8Session session;

            // Firstly check if the user has not expired.
            if (userDefinition.getUserExpiryDate() != null)
            {
                if (DateTime.now().isAfter(userDefinition.getUserExpiryDate().getMilliseconds())) throw new Exception("User expired.");
            }

            // Get the organization structure.
            dataManager = serverContext.getDataManager();
            orgStructure = (T8OrganizationStructure)dataManager.getCachedData(T8OrganizationStructureCacheDefinition.DATA_IDENTIFIER, HashMaps.newHashMap(T8OrganizationStructureCacheDefinition.DATA_PARAMETER_ORG_ID, userDefinition.getOrganizationIdentifier()));

            // Use the first profile as the default.  This may change later.
            sessionTimeout = definitionManager.getSystemDefinition(internalContext).getSessionTimeout();

            // Use the first profile as the default.  This may change later.
            profileIdentifiers = userDefinition.getProfileIdentifiers();
            profileIdentifier = ((profileIdentifiers != null) && (profileIdentifiers.size() > 0)) ? profileIdentifiers.get(0) : null;

            // Update the session with the new logged-in details.
            session = (T8Session)context.getSessionContext();
            session.setSessionIdentifier(T8IdentifierUtilities.createNewGUID());
            session.setUserIdentifier(userDefinition.getIdentifier());
            session.setUserProfileIdentifier(profileIdentifier);
            session.setWorkFlowProfileIdentifiers(userDefinition.getWorkFlowProfileIdentifiers());
            session.setLanguageIdentifier(userDefinition.getLanguageIdentifier());
            session.setContentLanguageIdentifier(getContentLanguageIdentifier(userDefinition.getLanguageIdentifier()));
            session.setOrganizationIdentifier(userDefinition.getOrganizationIdentifier());
            session.setRootOrganizationIdentifier(orgStructure.getRootOrganizationID());
            session.setUserName(userDefinition.getName());
            session.setUserSurname(userDefinition.getSurname());

            // Set the logged-in flag.
            session.setLoggedIn(true);

            // Update the context to reflect the changes to the session.
            context.resetSessionContext();

            // Create a handle for the active session.
            newSessionHandle = new T8SessionContextHandle(session);
            newSessionHandle.setAuthenticationToken(authenticationToken);
            newSessionHandle.setAccessibleUserProfileIdentifiers(userDefinition.getProfileIdentifiers());
            newSessionHandle.setLoginTime(System.currentTimeMillis());
            newSessionHandle.setLastActivityTime(System.currentTimeMillis());
            newSessionHandle.setSessionTimeout(sessionTimeout == null ? Long.MAX_VALUE : sessionTimeout);
            activeSessions.put(context.getSessionId(), newSessionHandle);
            activeTokens.put(authenticationToken, context.getSessionId());

            // Log the event and return the response.
            persistenceHandler.logUserEvent(context, eventType);
            return session;
        }
        catch (Exception e)
        {
            LOGGER.log("Failed to authenticate user.", e);
            throw e;
        }
    }

    /**
     * This method resolves the root organization to which the specified organization belongs.
     * If an organization structure is not available or an exception prevents successful resolution, the
     * specified organization id is considered to be its own root (since no structure exists) and is therefore returned.
     * @param orgId The organization for which to determine the root of the structure to which it belongs.
     * @return The root organization id in the structure to which the specified organization belongs or just the same input organization id if no structure is found.
     */
    private String resolveRootOrganizationId(String orgId)
    {
        try
        {
            T8OrganizationStructure orgStructure;
            T8DataManager dataManager;

            dataManager = serverContext.getDataManager();
            orgStructure = (T8OrganizationStructure)dataManager.getCachedData(T8OrganizationStructureCacheDefinition.DATA_IDENTIFIER, HashMaps.newHashMap(T8OrganizationStructureCacheDefinition.DATA_PARAMETER_ORG_ID, orgId));
            if (orgStructure != null)
            {
                return orgStructure.getRootOrganizationID();
            }
            else return orgId;
        }
        catch (Exception e)
        {
            LOGGER.log("Failed to get the Organization Structure to determine the root organization for Org Id: " + orgId, e);
            return orgId;
        }
    }

    @Override
    public String getWebServiceUserApiKey(T8Context context) throws Exception
    {
        List<T8Definition> userDefinitions;

        userDefinitions = definitionManager.getRawGroupDefinitions(context, null, T8UserDefinition.GROUP_IDENTIFIER);
        for (T8Definition definition : userDefinitions)
        {
            if(Objects.equals(context.getUserId(), definition.getIdentifier()))
            {
                T8UserDefinition userDefinition;

                userDefinition = (T8UserDefinition)definition;
                return userDefinition.getApiKey();
            }
        }

        throw new Exception("User definition not found or user is not a valid web service user.");
    }

    @Override
    public void generateWebServiceUserApiKey(T8Context context)
    {
        try
        {
            List<T8Definition> userDefinitions;

            userDefinitions = definitionManager.getRawGroupDefinitions(context, null, T8UserDefinition.GROUP_IDENTIFIER);
            for (T8Definition definition : userDefinitions)
            {
                if (Objects.equals(context.getUserId(), definition.getIdentifier()))
                {
                    T8UserDefinition userDefinition;

                    userDefinition = (T8UserDefinition)definition;
                    userDefinition.generateApiKey();

                    // Save the new api key
                    definitionManager.saveDefinition(context, definition, null);
                }
            }
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to generate api key for user " + context.getUserId(), ex);
        }
    }

    /**
     * Returns all session handles containing currently active sessions belonging
     * to the specified user.
     * @param userId The id of the user for which to fetch session handles.
     * @return The list of handles belonging to the specified user.
     */
    private List<T8SessionContextHandle> getActiveSessionHandlesByUser(String userId)
    {
        List<T8SessionContextHandle> handles;

        handles = new ArrayList<>();
        synchronized (activeSessions)
        {
            for (T8SessionContextHandle sessionHandle : activeSessions.values())
            {
                if (sessionHandle.getSessionContext().getUserIdentifier().equals(userId))
                {
                    handles.add(sessionHandle);
                }
            }
        }

        return handles;
    }

    private T8SessionContextHandle getSessionHandle(String sessionIdentifier)
    {
        T8SessionContextHandle authenticSessionHandle;

        authenticSessionHandle = activeSessions.get(sessionIdentifier);
        if (authenticSessionHandle != null)
        {
            T8SessionContext authenticSession;

            authenticSession = authenticSessionHandle.getSessionContext();
            if (authenticSession.getSessionIdentifier().equals(sessionIdentifier))
            {
                return authenticSessionHandle;
            }
            else return null;
        }
        else return null;
    }

    @Override
    public T8PasswordChangeResponse changePassword(T8Context context, char[] password) throws Exception
    {
        T8SessionContext sessionContext;

        sessionContext = context.getSessionContext();
        if (sessionContext.isAnonymousSession())
        {
            // Cannot set or change the password for anonymous user.
            return new T8PasswordChangeResponse(sessionContext.getUserIdentifier(), false, "Cannot change passwords from anonymous session.");
        }
        else
        {
            T8UserDefinition userDefinition;
            String userId;
            T8PasswordChangeResponse checkPasswordPolicies;

            userId = sessionContext.getUserIdentifier();
            userDefinition = (T8UserDefinition)definitionManager.getInitializedDefinition(context, null, userId, null);

            checkPasswordPolicies = checkPasswordPolicies(context, userDefinition, password);
            if(checkPasswordPolicies != null) return checkPasswordPolicies;

            userDefinition.setPassword(T8PasswordEncryptor.createHash(password));
            userDefinition.setResetPasswordFlag(false);
            userDefinition.setIncorrectPasswordEntryCount(0); //Since the password is changed, the count becomes invalid
            definitionManager.saveDefinition(context, userDefinition, null);
            return new T8PasswordChangeResponse(sessionContext.getUserIdentifier(), true, "Password successfully changed.");
        }
    }

    @Override
    public List<T8UserProfileDetails> getAccessibleUserProfileDetails(T8Context context) throws Exception
    {
        T8SessionContextHandle authenticSessionHandle;

        authenticSessionHandle = activeSessions.get(context.getSessionId());
        if (authenticSessionHandle != null)
        {
            List<T8UserProfileDetails> detailsList;

            detailsList = new ArrayList<>();
            for (String profileId : authenticSessionHandle.getAccessibleUserProfileIdentifiers())
            {
                T8UserProfileDefinition profileDefinition;

                profileDefinition = getUserProfileDefinition(profileId);
                if (profileDefinition != null)
                {
                    detailsList.add(new T8UserProfileDetails(profileId, profileDefinition.getUserProfileDisplayName(), profileDefinition.getMetaDescription()));
                }
                else
                {
                    detailsList.add(new T8UserProfileDetails(profileId, profileId, null));
                }
            }

            return detailsList;
        }
        else return new ArrayList<>();
    }

    @Override
    public List<String> getAccessibleUserProfileIds(T8Context context)
    {
        T8SessionContextHandle authenticSessionHandle;

        authenticSessionHandle = activeSessions.get(context.getSessionId());
        if (authenticSessionHandle != null)
        {
            // Make sure to return a new list and not the internally maintained one.
            return new ArrayList<>(authenticSessionHandle.getAccessibleUserProfileIdentifiers());
        }
        else return new ArrayList<>();
    }

    @Override
    public List<T8DefinitionMetaData> getAccessibleUserProfileMetaData(T8Context context) throws Exception
    {
        List<String> profileIds;

        profileIds = getAccessibleUserProfileIds(context);
        if (profileIds != null)
        {
            List<T8DefinitionMetaData> metaDataList;

            metaDataList = new ArrayList<>();
            for (String profileId : profileIds)
            {
                metaDataList.add(definitionManager.getDefinitionMetaData(null, profileId));
            }

            return metaDataList;
        }
        else return null;
    }

    @Override
    public List<String> getAccessibleFunctionalityIds(T8Context context) throws Exception
    {
        T8SessionContextHandle authenticSessionHandle;

        authenticSessionHandle = activeSessions.get(context.getSessionContext().getSessionIdentifier());
        if (authenticSessionHandle != null)
        {
            T8UserProfileDefinition profileDefinition;
            String profileId;

            profileId = authenticSessionHandle.getSessionContext().getUserProfileIdentifier();
            profileDefinition = (T8UserProfileDefinition)definitionManager.getInitializedDefinition(context, null, profileId, null);
            if (profileDefinition != null)
            {
                T8UserProfile userProfile;

                userProfile = profileDefinition.getNewUserProfileInstance(context);
                return new ArrayList<>(userProfile.getAccessibleFunctionalityIdentifiers());
            }
            else throw new RuntimeException("Profile Definition not found: " + profileId);
        }
        else return new ArrayList<>();
    }

    @Override
    public boolean switchUserProfile(T8Context context, String userProfileIdentifier)
    {
        try
        {
            T8UserDefinition userDefinition;

            userDefinition = (T8UserDefinition)definitionManager.getRawDefinition(context, null, context.getSessionContext().getUserIdentifier());
            if (userDefinition != null)
            {
                if (sessionExists(context))
                {
                    if (userDefinition.getProfileIdentifiers().contains(userProfileIdentifier))
                    {
                        T8Session session;

                        // Switch the profile on the session object provided.
                        session = (T8Session)context.getSessionContext();
                        session.setUserProfileIdentifier(userProfileIdentifier);

                        // Switch the profile on the session object in the sessions
                        // collection.  This is the master/authentic session object
                        // and may be exactly the same object as the method
                        // parameter but will be different object if it came from
                        // the client-side.
                        session = (T8Session)activeSessions.get(session.getSessionIdentifier()).getSessionContext();
                        session.setUserProfileIdentifier(userProfileIdentifier);

                        // Log the event and return the result.
                        persistenceHandler.logUserEvent(context, UserHistoryEvent.PROFILE_SWITCH);
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }
        catch (Exception e)
        {
            LOGGER.log("While attempting to switch user profile to: " + userProfileIdentifier, e);
            return false;
        }
    }

    @Override
    public T8SessionContext switchSessionLanguage(T8Context context, String languageId) throws Exception
    {
        T8LanguageDefinition langaugeDefinition;

        langaugeDefinition = (T8LanguageDefinition)definitionManager.getRawDefinition(context, null, languageId);
        if (langaugeDefinition != null)
        {
            if (sessionExists(context))
            {
                T8Session session;

                // Switch the profile on the session object provided.
                session = (T8Session)context.getSessionContext();
                session.setContentLanguageIdentifier(langaugeDefinition.getContentLanguageID());
                session.setLanguageIdentifier(langaugeDefinition.getIdentifier());

                // Switch the profile on the session object in the sessions
                // collection.  This is the master/authentic session object
                // and may be exactly the same object as the method
                // parameter but will be different object if it came from
                // the client-side.
                session = (T8Session)activeSessions.get(session.getSessionIdentifier()).getSessionContext();
                session.setContentLanguageIdentifier(langaugeDefinition.getContentLanguageID());
                session.setLanguageIdentifier(langaugeDefinition.getIdentifier());

                return session;
            }
            else throw new Exception("Session not authenticated");
        }
        else throw new Exception("Language definition not found: " + languageId);
    }

    @Override
    public void setUserActive(T8Context context, String userId, boolean active) throws Exception
    {
        T8UserDefinition userDefinition;

        userDefinition = (T8UserDefinition)definitionManager.getInitializedDefinition(context, null, userId, null);
        if(userDefinition == null) throw new Exception("Invalid user identifier, user not found " + userId);

        userDefinition.setActive(active);
        definitionManager.saveDefinition(context, userDefinition, null);
    }

    private boolean sessionExists(T8Context context)
    {
        T8SessionContextHandle sessionHandle;

        sessionHandle = activeSessions.get(context.getSessionId());
        if (sessionHandle != null)
        {
            T8SessionContext session;

            session = sessionHandle.getSessionContext();
            return session.getUserIdentifier().equals(context.getUserId());
        }
        else return false;
    }

    private T8UserDefinition findUserDefinition(String username) throws Exception
    {
        String userId;

        userId = usernameCache.get(username);
        if (userId != null)
        {
            return (T8UserDefinition)definitionManager.getRawDefinition(internalContext, null, userId);
        }

        return null;
    }

    private T8UserDefinition findUserByApiKey(String apiKey) throws Exception
    {
        List<T8Definition> userDefinitions;

        userDefinitions = definitionManager.getRawGroupDefinitions(internalContext, null, T8UserDefinition.GROUP_IDENTIFIER);
        for (T8Definition definition : userDefinitions)
        {
            T8UserDefinition userDefinition;

            userDefinition = (T8UserDefinition)definition;
            if (Objects.equals(apiKey, userDefinition.getApiKey()))
            {
                return userDefinition;
            }
        }

        return null;
    }

    private T8UserDefinition getUserDefinition(String userId) throws Exception
    {
        T8Definition definition;

        definition = definitionManager.getRawDefinition(internalContext, null, userId);
        if (definition instanceof T8UserDefinition)
        {
            return (T8UserDefinition)definition;
        }
        else return null;
    }

    private T8PasswordPolicyDefinition getPasswordPolicyDefinition(String policyId) throws Exception
    {
        T8Definition definition;

        definition = definitionManager.getRawDefinition(null, null, policyId);
        if (definition instanceof T8PasswordPolicyDefinition)
        {
            return (T8PasswordPolicyDefinition)definition;
        }
        else return null;
    }

    private T8UserProfileDefinition getUserProfileDefinition(String profileId) throws Exception
    {
        T8Definition definition;

        definition = definitionManager.getRawDefinition(internalContext, null, profileId);
        if (definition instanceof T8UserProfileDefinition)
        {
            return (T8UserProfileDefinition)definition;
        }
        else return null;
    }

    private T8WorkFlowProfileDefinition getWorkflowProfileDefinition(String profileId) throws Exception
    {
        T8Definition definition;

        definition = definitionManager.getRawDefinition(internalContext, null, profileId);
        if (definition instanceof T8UserProfileDefinition)
        {
            return (T8WorkFlowProfileDefinition)definition;
        }
        else return null;
    }

    private String getContentLanguageIdentifier(String languageId) throws Exception
    {
        if (languageId != null)
        {
            T8LanguageDefinition languageDefinition;
            String isoIdentifier;

            languageDefinition = (T8LanguageDefinition)definitionManager.getRawDefinition(null, null, languageId);
            isoIdentifier = languageDefinition != null ? languageDefinition.getContentLanguageID() : null;
            return isoIdentifier != null ? isoIdentifier : DEFAULT_CONTENT_LANGUAGE_IDENTIFIER;
        }
        else return DEFAULT_CONTENT_LANGUAGE_IDENTIFIER;
    }

    private T8PasswordChangeResponse checkPasswordPolicies(T8Context context, T8UserDefinition userDefinition, char[] password) throws Exception
    {
        if(userDefinition.getProfileIdentifiers() != null && !(userDefinition instanceof T8DeveloperUserDefinition))
        {
            //Validate agains the profile pasword policies for the default profile, if the default does not have one move down in the list until one is found
            for (String profileId : userDefinition.getProfileIdentifiers())
            {
                T8UserProfileDefinition rawDefinition;
                T8PasswordPolicyDefinition passwordPolicyDefinition;

                rawDefinition = (T8UserProfileDefinition) definitionManager.getRawDefinition(context, null, profileId);

                if(rawDefinition == null) throw new Exception("The user profile definition " + profileId + " linked to user " + userDefinition.getIdentifier() + " was not found.");

                if (!Strings.isNullOrEmpty(rawDefinition.getPasswordPolicyDefinitionIdentifier()))
                {
                    passwordPolicyDefinition = (T8PasswordPolicyDefinition) definitionManager.getInitializedDefinition(context, rawDefinition.getRootProjectId(), rawDefinition.getPasswordPolicyDefinitionIdentifier(), null);

                    if(passwordPolicyDefinition == null) throw new Exception("No password policy definition found matching identifier " + rawDefinition.getPasswordPolicyDefinitionIdentifier());

                    List<T8PasswordPolicyRuleDefinition> passwordFormatRuleDefinitions;
                    T8PasswordRuleValidator ruleValidator;

                    passwordFormatRuleDefinitions = passwordPolicyDefinition.getPasswordFormatRuleDefinitions();
                    if(passwordFormatRuleDefinitions != null && !passwordFormatRuleDefinitions.isEmpty())
                    {
                        for (T8PasswordPolicyRuleDefinition passwordFormatRuleDefinition : passwordFormatRuleDefinitions)
                        {
                            ruleValidator = passwordFormatRuleDefinition.getValidator();
                            if (!ruleValidator.validate(userDefinition, password, T8PasswordEncryptor.createHash(password)))
                            {
                                return new T8PasswordChangeResponse(context.getUserId(), false, passwordFormatRuleDefinition.getFailureMessage());
                            }
                        }
                        break;
                    }
                }
            }
        }
        return null;
    }

    private T8UserLoginResponse checkPasswordExpiry(T8Context context, T8SessionContextHandle sessionHandle, T8UserDefinition userDefinition) throws Exception
    {
        // Only proceed with the rest of the checks if the password has been activated.
        if (userDefinition.getProfileIdentifiers() != null && userDefinition.getPasswordActivationDate() != null)
        {
            // Validate agains the profile pasword policies for the default profile, if the default does not have one move down in the list until one is found
            for (String profileId : userDefinition.getProfileIdentifiers())
            {
                T8UserProfileDefinition profileDefinition;
                DateTime passwordActivation;
                DateTime passwordExpiry;

                // Get the profile definition.
                profileDefinition = (T8UserProfileDefinition)definitionManager.getRawDefinition(internalContext, null, profileId);
                if(profileDefinition == null) throw new RuntimeException("Invalid user profile identifier found " + profileId);
                if(!Strings.isNullOrEmpty(profileDefinition.getPasswordPolicyDefinitionIdentifier()))
                {
                    T8PasswordPolicyDefinition passwordPolicyDefinition;

                    passwordPolicyDefinition = getPasswordPolicyDefinition(profileDefinition.getPasswordPolicyDefinitionIdentifier());
                    if (passwordPolicyDefinition == null)
                    {
                        continue;
                    }
                    else if (passwordPolicyDefinition.getPasswordValidityTime() != null && passwordPolicyDefinition.getPasswordValidityTime() != -1)
                    {
                        T8Timestamp timestamp;
                        Days daysToExpiry;

                        // Calculate the expiration date.
                        timestamp = userDefinition.getPasswordActivationDate();
                        passwordActivation = timestamp != null ? new DateTime(timestamp.getMilliseconds()) : new DateTime();
                        passwordExpiry = new DateTime(passwordActivation);
                        passwordExpiry = passwordExpiry.plusDays(passwordPolicyDefinition.getPasswordValidityTime());

                        // If the password expiry date is before today then this password already expired.
                        if (passwordExpiry.isBeforeNow()) return new T8UserLoginResponse(context, T8LoginResponseType.PASSWORD_EXPIRED);

                        // Calculate the days to expiry and return a warning if required.
                        daysToExpiry = Days.daysBetween(passwordExpiry, DateTime.now());
                        if (daysToExpiry.getDays() > -10) return new T8UserLoginResponse(context, T8LoginResponseType.PASSWORD_EXPIRY_WARNING, Integer.toString(daysToExpiry.getDays()), getSessionDetails(sessionHandle, userDefinition));
                    }
                    else return null;
                }
            }
        }
        return null;
    }

    private T8UserLoginResponse checkPasswordIncorrectEntries(T8Context context, T8UserDefinition userDefinition) throws Exception
    {
        // If this user's password has not yet been activated, skip the rest of the checks.
        if (userDefinition.getProfileIdentifiers() != null && userDefinition.getPasswordActivationDate() != null)
        {
            // Check for a password policy for each of the profile ids linked to the user, starting from the first (default) profile in the list.
            for (String profileId : userDefinition.getProfileIdentifiers())
            {
                T8UserProfileDefinition profileDefinition;

                profileDefinition = getUserProfileDefinition(profileId);
                if (!Strings.isNullOrEmpty(profileDefinition.getPasswordPolicyDefinitionIdentifier()))
                {
                    T8PasswordPolicyDefinition passwordPolicyDefinition;

                    passwordPolicyDefinition = (T8PasswordPolicyDefinition)definitionManager.getInitializedDefinition(internalContext, profileDefinition.getRootProjectId(), profileDefinition.getPasswordPolicyDefinitionIdentifier(), null);
                    if (passwordPolicyDefinition == null)
                    {
                        continue;
                    }
                    else
                    {
                        // The the user login failure retries.
                        if (passwordPolicyDefinition.getPasswordRetriesAllowed() != null && passwordPolicyDefinition.getPasswordRetriesAllowed() > 0)
                        {
                            Integer incorrectPasswordEntryCount;

                            incorrectPasswordEntryCount = userDefinition.getIncorrectPasswordEntryCount();
                            if (incorrectPasswordEntryCount == null) incorrectPasswordEntryCount = 0;

                            if (incorrectPasswordEntryCount >= passwordPolicyDefinition.getPasswordRetriesAllowed())
                            {
                                userDefinition.setUserLocked(true);
                                userDefinition.setUserLockReason("Maximum incorrect password retries reached, user account locked.");
                                definitionManager.saveDefinition(internalContext, userDefinition, null);
                                return new T8UserLoginResponse(context, T8LoginResponseType.USER_ACCOUNT_LOCKED, serverContext.getConfigurationManager().getUITranslation(context, userDefinition.getUserLockReason()), null);
                            }
                        }

                        return null;
                    }
                }
            }

            // No password policy found.
            return null;
        }
        else return null;
    }

    @Override
    public void addSessionEventListener(T8SessionEventListener listener)
    {
        sessionListeners.add(T8SessionEventListener.class, listener);
    }

    @Override
    public void removeSessionEventListener(T8SessionEventListener listener)
    {
        sessionListeners.remove(T8SessionEventListener.class, listener);
    }

    public void fireSessionEvent(T8SessionEvent event)
    {
        if (event instanceof T8SessionLogoutEvent)
        {
            for (T8SessionEventListener listener : sessionListeners.getListeners(T8SessionEventListener.class))
            {
                listener.sessionLoggedOut((T8SessionLogoutEvent)event);
            }
        }
    }

    @Override
    public T8SessionDetails getSessionDetails(T8Context context) throws Exception
    {
        return getSessionDetails(context.getSessionId());
    }

    private T8SessionDetails getSessionDetails(String sessionId) throws Exception
    {
        synchronized (activeSessions)
        {
            T8SessionContextHandle authenticSessionHandle;

            authenticSessionHandle = getSessionHandle(sessionId);
            if (authenticSessionHandle != null)
            {
                T8UserDefinition userDefinition;

                userDefinition = getUserDefinition(authenticSessionHandle.getSessionContext().getUserIdentifier());
                return getSessionDetails(authenticSessionHandle, userDefinition);
            }
            else return null;
        }
    }

    private T8SessionDetails getSessionDetails(T8SessionContextHandle sessionHandle, T8UserDefinition userDefinition) throws Exception
    {
        T8UserProfileDefinition profileDefinition;
        T8SessionDetails sessionDetails;
        T8SessionContext session;
        List<String> workflowProfileIds;
        String userProfileId;

        session = sessionHandle.getSessionContext();
        sessionDetails = new T8SessionDetails(session.getSessionIdentifier());
        sessionDetails.setUserId(session.getUserIdentifier());
        sessionDetails.setUserName(userDefinition.getName());
        sessionDetails.setUserSurname(userDefinition.getSurname());
        sessionDetails.setUserProfileId(session.getUserProfileIdentifier());
        sessionDetails.setLoginTime(sessionHandle.getLoginTime());
        sessionDetails.setLastActivityTime(sessionHandle.getLastActivityTime());
        sessionDetails.setPerformanceStatisticsEnabled(session.getPerformanceStatistics().isEnabled());
        sessionDetails.setExpirationTime(sessionDetails.getLastActivityTime() + sessionHandle.getSessionTimeout());

        // Add user profile details.
        userProfileId = session.getUserProfileIdentifier();
        profileDefinition = getUserProfileDefinition(userProfileId);
        if (profileDefinition != null)
        {
            sessionDetails.setUserProfileDetails(new T8UserProfileDetails(userProfileId, profileDefinition.getUserProfileDisplayName(), profileDefinition.getMetaDescription()));
        }
        else
        {
            sessionDetails.setUserProfileDetails(new T8UserProfileDetails(userProfileId, userProfileId, null));
        }

        // Add workflow profile details.
        workflowProfileIds = session.getWorkFlowProfileIdentifiers();
        if (workflowProfileIds != null)
        {
            for (String workflowProfileId : workflowProfileIds)
            {
                T8WorkFlowProfileDefinition wfProfileDefinition;

                wfProfileDefinition = getWorkflowProfileDefinition(workflowProfileId);
                if (wfProfileDefinition != null)
                {
                    sessionDetails.addWorkflowProfileDetails(new T8WorkflowProfileDetails(workflowProfileId, wfProfileDefinition.getMetaDisplayName(), wfProfileDefinition.getMetaDescription()));
                }
                else
                {
                    sessionDetails.addWorkflowProfileDetails(new T8WorkflowProfileDetails(workflowProfileId, workflowProfileId, null));
                }
            }
        }

        return sessionDetails;
    }

    @Override
    public List<T8SessionDetails> getAllSessionDetails(T8Context context) throws Exception
    {
        synchronized (activeSessions)
        {
            List<T8SessionDetails> sessionDetailsList;

            sessionDetailsList = new ArrayList<>();
            for (T8SessionContextHandle sessionHandle : activeSessions.values())
            {
                sessionDetailsList.add(getSessionDetails(sessionHandle.getSessionContext().getSessionIdentifier()));
            }

            return sessionDetailsList;
        }
    }

    @Override
    public boolean isSessionActive(T8Context context, String sessionId) throws Exception
    {
        return activeSessions.containsKey(sessionId);
    }

    @Override
    public T8UserUpdateResponse createNewUser(T8Context context, T8UserDetails userDetails) throws Exception
    {
        String username;

        username = userDetails.getUsername();
        if (isUsernameValid(username))
        {
            if (checkUsernameAvailability(username))
            {
                T8UserDefinition userDefinition;
                String userId;

                try
                {
                    userId = T8Definition.getPublicIdPrefix() + serverContext.getDataManager().generateKey("@KEY_NEW_USER");
                    if (!isUserIdentifierAvailable(userId))
                    {
                        return new T8UserUpdateResponse(userId, false, "Generated User Identifier is invalid. Contact Administrator.", null);
                    }
                }
                catch(Exception ex)
                {
                    LOGGER.log("Failed to generate a new key for user " + username + ", defaulting to GUID key.", ex);
                    userId = T8Definition.getPublicIdPrefix() + T8IdentifierUtilities.createNewGUID();
                }

                // Add the new username to the username cache.
                usernameCache.put(username, userId);

                // Create the new user definition.
                userDefinition = new T8ClientUserDefinition(userId);
                userDefinition.setMetaDisplayName(userDetails.getName() + " " + userDetails.getSurname() + " (" + userDetails.getUsername() + ")");
                userDefinition.setUsername(username);
                userDefinition.setName(userDetails.getName());
                userDefinition.setSurname(userDetails.getSurname());
                userDefinition.setEmailAddress(userDetails.getEmailAddress());
                userDefinition.setMobileNumber(userDetails.getMobileNumber());
                userDefinition.setWorkTelephoneNumber(userDetails.getWorkTelephoneNumber());
                userDefinition.setUserExpiryDate(userDetails.getExpiryDate());
                // Backwards compatibility with old operations, only update the active indicator if it is set.
                if (userDetails.isActive() != null) userDefinition.setActive(userDetails.isActive());
                userDefinition.setLanguageIdentifier(userDetails.getLanguageIdentifier());
                userDefinition.setOrganizationIdentifier(userDetails.getOrganizationIdentifier());
                userDefinition.setProfileIdentifiers(userDetails.getUserProfileIds());
                userDefinition.setWorkFlowProfileIdentifiers(userDetails.getWorkFlowProfileIds());
                userDefinition.setResetPasswordFlag(false);

                //Set the password reset token and send the creation mail
                T8CommunicationManager communicationManager;
                Map<String, Object> communicationParameters;
                String tempPassw;

                // Get and assign the temp passw for the user
                tempPassw = getTemporaryPassword(context, userDefinition);

                //Save the user definition
                definitionManager.saveDefinition(context, userDefinition, null);

                try
                {
                    // Send the communication.
                    communicationParameters = new HashMap<>();
                    communicationParameters.put(COMMUNICATION_NEW_USER_IDENTIFIER + COMMUNICATION_PARAMETER_USER_IDENTIFIER, userDefinition.getIdentifier());
                    communicationParameters.put(COMMUNICATION_NEW_USER_IDENTIFIER + COMMUNICATION_PARAMETER_USERNAME, userDefinition.getUsername());
                    communicationParameters.put(COMMUNICATION_NEW_USER_IDENTIFIER + COMMUNICATION_PARAMETER_TEMPORARY_PASSW, tempPassw);
                    communicationManager = serverContext.getCommunicationManager();
                    communicationManager.sendCommunication(context, COMMUNICATION_NEW_USER_IDENTIFIER, communicationParameters);
                }
                catch(Exception ex)
                {
                    LOGGER.log("Failed to send new user creation mail", ex);
                }

                return new T8UserUpdateResponse(userId, true, "New user created successfully.", null);
            }
            else return new T8UserUpdateResponse(userDetails.getIdentifier(), false, "Username unavailable.", null);
        }
        else return new T8UserUpdateResponse(userDetails.getIdentifier(), false, "Invalid username format: " + username, null);
    }

    @Override
    public T8UserDetails getUserDetails(T8Context context, String userId) throws Exception
    {
        T8UserDefinition userDefinition;

        userDefinition = getUserDefinition(userId);
        if (userDefinition == null) userDefinition = findUserDefinition(userId);
        if (userDefinition != null)
        {
            return getUserDetails(userDefinition);
        }
        else throw new Exception("User not found: " + userId);
    }

    private T8UserDetails getUserDetails(T8UserDefinition userDefinition) throws Exception
    {
        List<String> workflowProfileIds;
        List<String> userProfileIds;
        T8UserDetails userDetails;

        // Set user details.
        userDetails = new T8UserDetails();
        userDetails.setIdentifier(userDefinition.getIdentifier());
        userDetails.setEmailAddress(userDefinition.getEmailAddress());
        userDetails.setMobileNumber(userDefinition.getMobileNumber());
        userDetails.setLanguageIdentifier(userDefinition.getLanguageIdentifier());
        userDetails.setUsername(userDefinition.getUsername());
        userDetails.setName(userDefinition.getName());
        userDetails.setSurname(userDefinition.getSurname());
        userDetails.setWorkTelephoneNumber(userDefinition.getWorkTelephoneNumber());
        userDetails.setActive(userDefinition.isActive());
        userDetails.setOrganizationIdentifier(userDefinition.getOrganizationIdentifier());

        // Add user profile details.
        userProfileIds = userDefinition.getProfileIdentifiers();
        if (userProfileIds != null)
        {
            for (String userProfileId : userProfileIds)
            {
                T8UserProfileDefinition profileDefinition;

                profileDefinition = getUserProfileDefinition(userProfileId);
                if (profileDefinition != null)
                {
                    userDetails.addUserProfileDetails(new T8UserProfileDetails(userProfileId, profileDefinition.getUserProfileDisplayName(), profileDefinition.getMetaDescription()));
                }
                else
                {
                    userDetails.addUserProfileDetails(new T8UserProfileDetails(userProfileId, userProfileId, null));
                }
            }
        }

        // Add workflow profile details.
        workflowProfileIds = userDefinition.getWorkFlowProfileIdentifiers();
        if (workflowProfileIds != null)
        {
            for (String workflowProfileId : workflowProfileIds)
            {
                T8WorkFlowProfileDefinition profileDefinition;

                profileDefinition = getWorkflowProfileDefinition(workflowProfileId);
                if (profileDefinition != null)
                {
                    userDetails.addWorkflowProfileDetails(new T8WorkflowProfileDetails(workflowProfileId, profileDefinition.getMetaDisplayName(), profileDefinition.getMetaDescription()));
                }
                else
                {
                    userDetails.addWorkflowProfileDetails(new T8WorkflowProfileDetails(workflowProfileId, workflowProfileId, null));
                }
            }
        }

        // Return the final details.
        return userDetails;
    }

    @Override
    public T8UserUpdateResponse updateUserDetails(T8Context context, T8UserDetails userDetails) throws Exception
    {
        T8UserDefinition userDefinition;
        String userId;
        String newUsername;
        String oldUsername;

        // Get the username and user definition to use.
        userId = userDetails.getIdentifier();
        newUsername = userDetails.getUsername();
        userDefinition = (T8UserDefinition)definitionManager.getInitializedDefinition(context, null, userId, null);
        oldUsername = userDefinition.getUsername();

        // If the username is unchanged or the change is valid, continue with the update.
        if ((oldUsername.equals(newUsername)) || (checkUsernameAvailability(newUsername)))
        {
            // Add the new username to the cache..
            usernameCache.put(newUsername, userId);

            // Update the user definition.
            userDefinition.setMetaDisplayName(userDetails.getName() + " " + userDetails.getSurname() + " (" + userDetails.getUsername() + ")");
            userDefinition.setUsername(newUsername);
            userDefinition.setName(userDetails.getName());
            userDefinition.setSurname(userDetails.getSurname());
            userDefinition.setEmailAddress(userDetails.getEmailAddress());
            userDefinition.setMobileNumber(userDetails.getMobileNumber());
            userDefinition.setWorkTelephoneNumber(userDetails.getWorkTelephoneNumber());
            userDefinition.setLanguageIdentifier(userDetails.getLanguageIdentifier());
            userDefinition.setOrganizationIdentifier(userDetails.getOrganizationIdentifier());
            userDefinition.setProfileIdentifiers(userDetails.getUserProfileIds());
            userDefinition.setWorkFlowProfileIdentifiers(userDetails.getWorkFlowProfileIds());
            userDefinition.setUserExpiryDate(userDetails.getExpiryDate());

            // Backwards compatibility with old operations, only update the active indicator if it is set.
            if (userDetails.isActive() != null) userDefinition.setActive(userDetails.isActive());

            // Save the updated user definition.
            definitionManager.saveDefinition(context, userDefinition, null);

            // If the new username is different from the old one, remove the old one to make it available again.
            if (!oldUsername.equals(newUsername)) usernameCache.remove(oldUsername);

            // If the updated user is the currently logged-in user, update the session.
            if (context.getUserId().equals(userId))
            {
                T8Session session;

                // Update the session with the new details.
                session = (T8Session)context.getSessionContext();
                session.setUserName(userDetails.getName());
                session.setUserSurname(userDetails.getSurname());

                // Update the context to reflect the changes to the session.
                context.resetSessionContext();

                // Return the update response.
                return new T8UserUpdateResponse(userDetails.getIdentifier(), true, "User details updated successfully.", getSessionDetails(getSessionHandle(context.getSessionId()), userDefinition));
            }
            else
            {
                return new T8UserUpdateResponse(userDetails.getIdentifier(), true, "User details updated successfully.", null);
            }
        }
        else return new T8UserUpdateResponse(userDetails.getIdentifier(), false, "Username unavailable.", null);
    }

    @Override
    public boolean deleteUser(T8Context context, String userId) throws Exception
    {
        definitionManager.deleteDefinition(context, null, userId, true, null);
        return true;
    }

    @Override
    public boolean resetUserPassword(T8Context context, String userId) throws Exception
    {
        T8UserDefinition userDefinition;

        userDefinition = (T8UserDefinition)definitionManager.getInitializedDefinition(context, null, userId, null);
        userDefinition.setResetPasswordFlag(true);
        definitionManager.saveDefinition(context, userDefinition, null);
        return true;
    }

    /**
     * Checks whether or not the specified username is available.
     *
     * @param username The {@code String} username to be checked
     *
     * @return {@code true} if the username is available. {@code false} if the
     *      username is already in use
     */
    public boolean checkUsernameAvailability(String username)
    {
        return !this.usernameCache.containsKey(username);
    }

    /**
     * Checks whether or not the specified user identifier already exists as any
     * definition identifier.
     *
     * @param userId The {@code String} user identifier to check the
     *      availability for
     *
     * @return {@code true} if the identifier is available. {@code false}
     *      otherwise
     *
     * @throws Exception If there is an error during the query of existing
     *      definitions
     */
    private boolean isUserIdentifierAvailable(String userId) throws Exception
    {
        return this.definitionManager.getRawDefinition(this.internalContext, null, userId) == null;
    }

    @Override
    public T8PasswordChangeResponse changePassword(T8Context context, String userId, char[] password) throws Exception
    {
        try
        {
            T8UserDefinition userDefinition;

            // Get the specified user definition either using the username of the user identifier
            userDefinition = getUserDefinition(userId);
            if (userDefinition == null) userDefinition = findUserDefinition(userId);
            if (userDefinition != null)
            {
                T8PasswordChangeResponse checkPasswordPolicies = checkPasswordPolicies(context, userDefinition, password);
                if(checkPasswordPolicies != null) return checkPasswordPolicies;
                // Set the new password and save the user definition.
                userDefinition.setPassword(T8PasswordEncryptor.createHash(password));
                userDefinition.setResetPasswordFlag(false);
                // Reset the temporary password fields, just in case it hasn't been done
                userDefinition.setTempPasswHash(null);
                userDefinition.setTempPasswExpiryTime(null);
                userDefinition.setIncorrectPasswordEntryCount(0); //Since the password is changed, the count becomes invalid
                definitionManager.saveDefinition(context, userDefinition, null);

                // Log the event and return the response.
                persistenceHandler.logUserEvent(context, UserHistoryEvent.PASSWORD_CHANGED);
                return new T8PasswordChangeResponse(null, true, "");
            }
            else return new T8PasswordChangeResponse(null, false, "User not found.");
        }
        catch (Exception e)
        {
            LOGGER.log("While attempting user password change: " + userId, e);
            return new T8PasswordChangeResponse(null, false, "Unexpected Exception.");
        }
    }

    @Override
    public boolean isPasswordChangeAvailable(T8Context context, String userId) throws Exception
    {
        try
        {
            T8UserDefinition userDefinition;

            // Get the specified user definition either using the username of the user identifier.
            userDefinition = getUserDefinition(userId);
            if (userDefinition == null) userDefinition = findUserDefinition(userId);
            if (userDefinition != null)
            {
                String tempPasswHash;

                // If the temporary passw is available, and not yet expired, then password change is available.
                tempPasswHash = userDefinition.getTemporaryPasswHash();
                return (!Strings.isNullOrEmpty(tempPasswHash)) && (new Date().before(new Date(userDefinition.getTempPasswExpiryTime().getMilliseconds())));
            }
            else return false;
        }
        catch (Exception e)
        {
            LOGGER.log("While checking password change availability for Identifier:" + userId, e);
            return false;
        }
    }

    @Override
    public boolean isUsernameAvailable(T8Context context, String username) throws Exception
    {
        return checkUsernameAvailability(username);
    }

    public boolean isUsernameValid(String username)
    {
        // More validity checks to be added in future.
        return !Strings.isNullOrEmpty(username);
    }

    @Override
    public boolean isUserRegistered(T8Context context, String userId) throws Exception
    {
        T8UserDefinition userDefinition;

        // Get the specified user definition either using the username of the user identifier.
        userDefinition = getUserDefinition(userId);
        if (userDefinition == null) userDefinition = findUserDefinition(userId);
        return userDefinition != null;
    }

    @Override
    public boolean requestPasswordChange(T8Context context, String userId) throws Exception
    {
        try
        {
            T8UserDefinition userDefinition;

            // Get the specified user definition either using the username of the user identifier.
            userDefinition = getUserDefinition(userId);
            if (userDefinition == null) userDefinition = findUserDefinition(userId);
            if (userDefinition != null)
            {
                T8CommunicationManager communicationManager;
                Map<String, Object> communicationParameters;
                String tempPassw;

                // Generate and assign a new temp passw for the user
                tempPassw = getTemporaryPassword(context, userDefinition);
                userDefinition.setIncorrectPasswordEntryCount(0);

                definitionManager.saveDefinition(context, userDefinition, null);

                // Send the communication.
                communicationParameters = new HashMap<>();
                communicationParameters.put(COMMUNICATION_PASSWORD_CHANGE_IDENTIFIER + COMMUNICATION_PARAMETER_USER_IDENTIFIER, userDefinition.getIdentifier());
                communicationParameters.put(COMMUNICATION_PASSWORD_CHANGE_IDENTIFIER + COMMUNICATION_PARAMETER_USERNAME, userDefinition.getUsername());
                communicationParameters.put(COMMUNICATION_PASSWORD_CHANGE_IDENTIFIER + COMMUNICATION_PARAMETER_TEMPORARY_PASSW, tempPassw);
                communicationManager = serverContext.getCommunicationManager();
                communicationManager.sendCommunication(context, COMMUNICATION_PASSWORD_CHANGE_IDENTIFIER, communicationParameters);

                // Return the result.
                return true;
            }
            else return false;
        }
        catch (Exception e)
        {
            LOGGER.log("While sending password change communication for Identifier:" + userId, e);
            return false;
        }
    }

    @Override
    public T8RemoteConnectionUser getRemoteConnectionUser(T8Context context, String connectionId)
    {
        try
        {
            T8UserDefinition userDefinition;

            // Get the specified user definition either using the username of the user identifier.
            userDefinition = getUserDefinition(context.getUserId());
            if (userDefinition == null) userDefinition = findUserDefinition(context.getUserId());
            if (userDefinition != null)
            {
                if(userDefinition.getUserRemoteServerConnectionDefinitions() != null)
                {
                    for (T8UserRemoteServerConnectionDefinition t8UserRemoteServerConnectionDefinition : userDefinition.getUserRemoteServerConnectionDefinitions())
                    {
                        if(Objects.equals(t8UserRemoteServerConnectionDefinition.getRemoteServerConnectionIdentifier(), connectionId))
                        {
                            T8ConnectionDefinition remoteServerConnectionDefinition;

                            remoteServerConnectionDefinition = (T8ConnectionDefinition)definitionManager.getRawDefinition(context, null, connectionId);
                            for (T8RemoteConnectionUser t8RemoteConnectionUser : remoteServerConnectionDefinition.getRemoteUserDefinitions())
                            {
                                if(Objects.equals(t8RemoteConnectionUser.getPublicIdentifier(), t8UserRemoteServerConnectionDefinition.getRemoteServerUserIdentifier()))
                                {
                                    return t8RemoteConnectionUser;
                                }
                            }
                        }
                    }
                }

                //if we got this far, then the user does not have the connection specified, the lets check if the connection exists on the profile
                T8UserProfileDefinition profileDefinition;

                profileDefinition = (T8UserProfileDefinition) definitionManager.getRawDefinition(context, null, context.getUserProfileId());
                if(profileDefinition.getRemoteServerConnectionDefinitions() != null)
                {
                    for (T8UserRemoteServerConnectionDefinition t8UserRemoteServerConnectionDefinition : profileDefinition.getRemoteServerConnectionDefinitions())
                    {
                        if(Objects.equals(t8UserRemoteServerConnectionDefinition.getRemoteServerConnectionIdentifier(), connectionId))
                        {
                            T8ConnectionDefinition remoteServerConnectionDefinition;

                            remoteServerConnectionDefinition = (T8ConnectionDefinition) definitionManager.getRawDefinition(context, null, connectionId);
                            for (T8RemoteConnectionUser t8RemoteConnectionUser : remoteServerConnectionDefinition.getRemoteUserDefinitions())
                            {
                                if(Objects.equals(t8RemoteConnectionUser.getPublicIdentifier(), t8UserRemoteServerConnectionDefinition.getRemoteServerUserIdentifier()))
                                {
                                    return t8RemoteConnectionUser;
                                }
                            }
                        }
                    }
                }
            }

            //If we reached this point then no mathcing connection identifier has been found
            throw new Exception("Could not find any remote connection API credentials for this user profile combination");
        }
        catch (Exception e)
        {
            LOGGER.log("Failed to get remote connection user for " + connectionId, e);
            return null;
        }
    }

    private String getTemporaryPassword(T8Context context, T8UserDefinition userDefinition) throws Exception
    {
        Integer temporaryPasswordMinutesValid;
        Calendar expiryTime;
        String tempPassw;

        // Generate a new temp passw.
        temporaryPasswordMinutesValid = DEFAULT_TEMP_PW_EXPIRATION_TIME;
        tempPassw = T8RandomPasswGenerator.generate();

        // Calculate expiry time for temp passw.
        expiryTime = Calendar.getInstance();
        expiryTime.add(Calendar.MINUTE, temporaryPasswordMinutesValid);

        userDefinition.setTempPasswHash(T8PasswordEncryptor.createHash(tempPassw.toCharArray()));
        userDefinition.setTempPasswExpiryTime(expiryTime.getTime());

        return tempPassw;
    }

    @Override
    public void extendSession(T8Context context) throws Exception
    {
        T8SessionContextHandle sessionHandle;

        sessionHandle = activeSessions.get(context.getSessionId());
        if (sessionHandle != null)
        {
            sessionHandle.setLastActivityTime(System.currentTimeMillis());
        }
        else throw new Exception("Cannot extend inactive session: " + context);
    }

    @Override
    public void setSessionPerformanceStatisticsEnabled(T8Context context, String sessionId, boolean enabled)
    {
        T8SessionContextHandle sessionHandle;

        sessionHandle = activeSessions.get(context.getSessionId());
        if (sessionHandle != null)
        {
            T8Session session;

            session = (T8Session)sessionHandle.getSessionContext();
            session.setPerformanceStatisticsEnabled(enabled);
            if (!enabled) session.getPerformanceStatistics().reset(); // Clear the statistics already gathered, if setting to disabled.
        }
    }

    @Override
    public T8PerformanceStatistics getSessionPerformanceStatistics(T8Context context, String sessionId)
    {
        T8SessionContextHandle sessionHandle;

        sessionHandle = activeSessions.get(context.getSessionId());
        if (sessionHandle != null)
        {
            T8Session session;

            session = (T8Session)sessionHandle.getSessionContext();
            return session.getPerformanceStatistics();
        }
        else throw new RuntimeException("Session not found: " + sessionId);
    }

    private class T8SecurityDefinitionManagerListener extends T8DefinitionManagerEventAdapter
    {
        private T8SecurityDefinitionManagerListener() {}

        @Override
        public void definitionSaved(T8DefinitionSavedEvent event)
        {
            T8Definition savedDefinition;

            savedDefinition = event.getDefinition();
            if (savedDefinition instanceof T8UserDefinition)
            {
                T8UserDefinition userDefinition;
                String userId;
                String username;

                // Get the details of the updated user definition.
                userDefinition = (T8UserDefinition)event.getDefinition();
                userId = userDefinition.getIdentifier();
                username = userDefinition.getUsername();

                // Update the user definition cache.
                userDefinitionCache.put(userId, userDefinition);

                // If the username has changed, or this is a new user definition
                // we need to update the username cache accordingly
                if (!usernameCache.containsKey(username))
                {
                    String oldUsername = username;

                    // Check if an existing user's username has been changed
                    for (Map.Entry<String, String> cacheEntry : usernameCache.entrySet())
                    {
                        if (cacheEntry.getValue().equals(userId))
                        {
                            oldUsername = cacheEntry.getKey();
                            break;
                        }
                    }

                    // Remove the old username and add the new one.
                    usernameCache.remove(oldUsername);
                    usernameCache.put(username, userId);
                }

                // Update all authentication handlers.
                for (T8AuthenticationHandler authHandler : authenticationHandlers.values())
                {
                    authHandler.updateUser(userDefinition);
                }
            }
            else if (savedDefinition instanceof T8AuthenticationHandlerDefinition)
            {
                cacheAuthenticationHandler(savedDefinition.getIdentifier());
            }
        }

        @Override
        public void definitionRenamed(T8DefinitionRenamedEvent event)
        {
            T8Definition renamedDefinition;

            renamedDefinition = event.getDefinition();
            if (renamedDefinition instanceof T8UserDefinition)
            {
                T8UserDefinition userDefinition;
                String oldUserId;
                String newUserId;

                // We need to update the identifier for the user definition associated with the username
                userDefinition = (T8UserDefinition)event.getDefinition();
                oldUserId = event.getOldIdentifier();
                newUserId = userDefinition.getIdentifier();
                usernameCache.put(userDefinition.getUsername(), newUserId);

                // Update the user definition cache.
                userDefinitionCache.remove(oldUserId);
                userDefinitionCache.put(newUserId, userDefinition);

                // Update all authentication handlers.
                for (T8AuthenticationHandler authHandler : authenticationHandlers.values())
                {
                    authHandler.deregisterUser(oldUserId);
                    authHandler.registerUser(userDefinition);
                }
            }
            else if (renamedDefinition instanceof T8AuthenticationHandlerDefinition)
            {
                T8AuthenticationHandler handler;

                handler = authenticationHandlers.remove(event.getOldIdentifier());
                authenticationHandlers.put(renamedDefinition.getIdentifier(), handler);
            }
        }

        @Override
        public void definitionDeleted(T8DefinitionDeletedEvent event)
        {
            T8Definition deletedDefinition;

            deletedDefinition = event.getOldDefinition();
            if (deletedDefinition instanceof T8UserDefinition)
            {
                T8UserDefinition userDefinition;
                String oldUserId;

                // Get the details of the deleted definition.
                userDefinition = (T8UserDefinition)event.getOldDefinition();
                oldUserId = userDefinition.getIdentifier();
                usernameCache.remove(userDefinition.getUsername());

                // Update the user definition cache.
                userDefinitionCache.remove(oldUserId);

                // Update all authentication handlers.
                for (T8AuthenticationHandler authHandler : authenticationHandlers.values())
                {
                    authHandler.deregisterUser(oldUserId);
                }
            }
            else if (deletedDefinition instanceof T8AuthenticationHandlerDefinition)
            {
                authenticationHandlers.remove(deletedDefinition.getIdentifier());
            }
        }
    }

    /**
     * Creates a new access context associated with the current thread and the
     * specified session.
     * @param sessionContext The session context to associate with the current thread and the newly creates access context.
     * @return The newly created access context.
     */
    @Override
    public T8Context createContext(T8SessionContext sessionContext)
    {
        T8Context existingContext;
        Thread currentThread;

        // Get the current thread and the existing transaction associated with it (if any).
        currentThread = Thread.currentThread();
        existingContext = threadAccess.get(currentThread);
        if (existingContext == null)
        {
            try
            {
                T8Context newContext;

                // Create a new access context and return it.
                newContext = new T8Context(serverContext, sessionContext);
                threadAccess.put(currentThread, newContext);
                return newContext;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while creating access context.", e);
            }
        }
        else throw new RuntimeException("Cannot create multiple access contexts for the same thread.  Thread '" + currentThread + "' is already associated with access context: " + existingContext);
    }

    /**
     * Returns the access context associated with the current thread.
     */
    @Override
    public T8Context getContext()
    {
        Thread currentThread;
        T8Context context;

        // Get the current thread and the existing transaction associated with it (if any).
        currentThread = Thread.currentThread();
        return threadAccess.get(currentThread);
    }

    /**
     * Destroys the access context associated with the current thread.
     */
    @Override
    public void destroyContext()
    {
        T8Context existingContext;
        Thread currentThread;

        // Get the current thread and the existing transaction associated with it (if any).
        currentThread = Thread.currentThread();
        existingContext = threadAccess.get(currentThread);
        if (existingContext != null)
        {
            threadAccess.remove(currentThread);
        }
        else throw new RuntimeException("No access context associated with thread: " + currentThread);
    }

    /**
     * Asserts the specified data access rights for the current thread.  These
     * rights will be granted to the current thread until such a time as they
     * are revoked by the framework or by a subsequent call the the
     * {@code revokeAccess} method.
     * @param requestedContext The details of the access assertion.
     */
    @Override
    public T8Context accessContext(T8Context requestedContext)
    {
        T8Context existingContext;

        // Get the current thread and the existing context associated with it (if any).
        existingContext = getContext();
        if (existingContext != null)
        {
            T8SessionContext existingSession;
            T8FunctionalityAccessRights functionalityRights;
            T8Context context;
            String projectId;
            String functionalityId;
            String functionalityIid;
            String flowIid;
            String rootOrgId;
            String orgId;

            // Copy the input parameter so it is not affected by this method.
            context = new T8Context(requestedContext);
            context.clearOperationAccessRights();

            // Check each type of requested access right to ensure that the user is authorized to be granted the requested rights.
            existingSession = existingContext.getSessionContext();
            projectId = context.getProcessId();
            functionalityId = context.getFunctionalityId();
            functionalityIid = context.getFunctionalityIid();
            flowIid = context.getFlowIid();
            orgId = context.getOrganizationId();
            rootOrgId = context.getRootOrganizationId();

            // Determine the functionality rights applicable to the context.
            if (functionalityIid != null)
            {
                T8FunctionalityManager functionalityManager;

                functionalityManager = serverContext.getFunctionalityManager();
                functionalityRights = functionalityManager.getFunctionalityAccessRights(existingContext, functionalityIid);
            }
            else if (flowIid != null)
            {
                T8FunctionalityManager functionalityManager;

                functionalityManager = serverContext.getFunctionalityManager();
                functionalityRights = functionalityManager.getWorkflowAccessRights(existingContext, flowIid);
            }
            else functionalityRights = null;

            // Set the context according to the rights applicable.
            if (functionalityRights != null)
            {
                // Check the project id requested.
                if (projectId == null)
                {
                    context.setProjectId(functionalityRights.getProjectId());
                }
                else if (!projectId.equals(functionalityRights.getProjectId()))
                {
                    throw new RuntimeException("Access rights to project denied: " + projectId);
                }

                // Check the functionality id requested.
                if (functionalityId == null)
                {
                    context.setFunctionalityId(functionalityRights.getFunctionalityId());
                }
                else if (!functionalityId.equals(functionalityRights.getFunctionalityId()))
                {
                    throw new RuntimeException("Access rights to functionality denied: " + functionalityId);
                }

                // Check the functionality instance requested.
                if (functionalityIid == null)
                {
                    context.setFunctionalityIid(functionalityRights.getFunctionalityIid());
                }
                else if (!functionalityIid.equals(functionalityRights.getFunctionalityIid()))
                {
                    throw new RuntimeException("Access rights to functionality denied: " + functionalityIid);
                }

                // Set the data access rights.
                context.setDataAcccess(functionalityRights.getDataAccess());

                // Set the operation access rights.
                for (String operationId : functionalityRights.getOperationIds())
                {
                    context.addOperationAccessRights(new T8OperationAccessRights(operationId, functionalityRights.getDataObjectIid()));
                }
            }

            // Lastly, check organization context.
            if ((orgId != null) && (!orgId.equals(existingSession.getOrganizationIdentifier())))
            {
                throw new RuntimeException("Access rights to organization denied: " + orgId);
            }
            else if ((rootOrgId != null) && (!rootOrgId.equals(existingSession.getRootOrganizationIdentifier())))
            {
                throw new RuntimeException("Access rights to root organization denied: " + rootOrgId);
            }

            // All rights verified, so grant the access.
            existingContext.setContext(context);
            return existingContext;
        }
        else throw new RuntimeException("No access context associated with thread: " + Thread.currentThread());
    }

    /**
     * This class is a task that is scheduled using a timer object and is used
     * to run regular checks on the currently open sessions in order to close
     * and discard sessions that have expired.
     */
    private class SessionExpirationChecker extends TimerTask
    {
        @Override
        public void run()
        {
            try
            {
                Map<String, T8SessionContextHandle> sessionsToCheck;
                ArrayList<String> expiredSessions;
                T8Context context;
                long currentTime;

                // Create a data session for this execution thread.
                context = serverContext.getSecurityManager().createContext(internalContext.getSessionContext());
                serverContext.getDataManager().openDataSession(internalContext);

                // Make a copy of active sessions so that we don't cause a concurrent modification exception.
                synchronized (activeSessions)
                {
                    sessionsToCheck = new HashMap<>(activeSessions);
                }

                // Check the active sessions collection and remove any sessions that have been idle for longer than the expiration time.
                currentTime = System.currentTimeMillis();
                expiredSessions = new ArrayList<>();
                for (T8SessionContextHandle sessionHandle : sessionsToCheck.values())
                {
                    long lastActivityTime;

                    lastActivityTime = sessionHandle.getLastActivityTime();
                    if ((currentTime - lastActivityTime) > sessionHandle.getSessionTimeout())
                    {
                        expiredSessions.add(sessionHandle.getSessionContext().getSessionIdentifier());
                    }
                }

                // If there are any expired sessions, remove them.
                if (expiredSessions.size() > 0)
                {
                    for (String sessionId : expiredSessions)
                    {
                        try
                        {
                            LOGGER.log("Finalizing expired session: " + sessionId);
                            logout(context, sessionId);
                        }
                        catch (Exception e)
                        {
                            LOGGER.log("Exception while finalizing session from expiration checker thread: " + sessionId, e);
                        }
                    }
                }
            }
            finally
            {
                // Make sure to close the current session.
                serverContext.getDataManager().closeCurrentSession();
                serverContext.getSecurityManager().destroyContext();
            }
        }
    }
}
