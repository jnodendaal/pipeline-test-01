package com.pilog.t8.security.authentication;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.security.authentication.T8WaffleAuthenticationHandlerDefinition;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.http.T8HttpServletRequest;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.authentication.T8AuthenticationResponse.ResponseType;
import com.pilog.t8.utilities.cache.LRUCache;
import com.pilog.t8.utilities.codecs.Base64Codec;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import waffle.windows.auth.IWindowsAccount;
import waffle.windows.auth.IWindowsSecurityContext;
import waffle.windows.auth.impl.WindowsAuthProviderImpl;

/**
 * @author Bouwer du Preez
 */
public class T8WaffleAuthenticationHandler implements T8AuthenticationHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8WaffleAuthenticationHandler.class);
    private final T8WaffleAuthenticationHandlerDefinition definition;
    private final T8ServerContext serverContext;
    private final LRUCache<String, WindowsAuthProviderImpl> authProviders;
    private final Map<String, String> userSidRegister;
    private final List<String> authorizedUserGroupSids;

    public T8WaffleAuthenticationHandler(T8ServerContext serverContext, T8WaffleAuthenticationHandlerDefinition definition)
    {
        this.definition = definition;
        this.serverContext = serverContext;
        this.authProviders = new LRUCache<>(100); // Authentication for each session will be completed in a second or two, so we don't need to hold the provider for long.
        this.userSidRegister = new HashMap<>();
        this.authorizedUserGroupSids = definition.getAuthorizedUserGroupSids();
    }

    @Override
    public String getId()
    {
        return definition.getIdentifier();
    }

    @Override
    public boolean isApplicable(String authorizationHeader)
    {
        return ((authorizationHeader != null) && (authorizationHeader.startsWith("WafSec-Token")));
    }

    @Override
    public void registerUser(T8UserDefinition user)
    {
        userSidRegister.put(user.getIdentifier(), user.getWindowsSid());
    }

    @Override
    public void deregisterUser(String userId)
    {
        userSidRegister.remove(userId);
    }

    @Override
    public void updateUser(T8UserDefinition user)
    {
        userSidRegister.put(user.getIdentifier(), user.getWindowsSid());
    }

    private String getUserIdBySid(String sid)
    {
        for (String userId : userSidRegister.keySet())
        {
            if (sid.equals(userSidRegister.get(userId)))
            {
                return userId;
            }
        }

        return null;
    }

    @Override
    public T8AuthenticationResponse authenticate(T8HttpServletRequest request)
    {
        String authorizationHeader;

        // Get the authorization header from the request and make sure that it is the correct header for this handler.
        authorizationHeader = request.getAuthorizationHeader();
        if (isApplicable(authorizationHeader))
        {
            T8SessionContext sessionContext;

            sessionContext = request.getSessionContext();

            try
            {
                IWindowsSecurityContext securityContext;
                WindowsAuthProviderImpl authProvider;
                String sessionId;
                String securityPackage;
                byte[] inputToken;
                byte[] outputToken;

                // Decode the input authorization token.
                securityPackage = definition.getSecurityPackage();
                inputToken = Base64Codec.decode(authorizationHeader.substring(12).trim());

                // Get the cached authentication provider for this session.
                sessionId = sessionContext.getSessionIdentifier();
                authProvider = getAuthProvider(sessionContext);
                securityContext = authProvider.acceptSecurityToken(sessionId, inputToken, securityPackage);
                if (securityContext.isContinue())
                {
                    outputToken = securityContext.getToken();
                    return new T8WaffleAuthenticationResponse(ResponseType.CONTINUED, outputToken, null, null);
                }
                else
                {
                    String userId;
                    String sid;

                    sid = securityContext.getIdentity().getSidString();
                    userId = getUserIdBySid(sid);
                    if (userId != null)
                    {
                        // If specified user groups are specified for authorization, evalute them now and make sure the user is a member of at least one of them.
                        if (!authorizedUserGroupSids.isEmpty())
                        {
                            // Check each group of which the user is a member against the list of authorized groups.
                            for (IWindowsAccount group : securityContext.getIdentity().getGroups())
                            {
                                String groupSid;

                                groupSid = group.getSidString();
                                if (authorizedUserGroupSids.contains(groupSid))
                                {
                                    // Return the final authentication result.
                                    return new T8WaffleAuthenticationResponse(ResponseType.COMPLETED, null, sessionContext, userId);
                                }
                            }

                            // The user is not a member of one of the authorized groups.
                            LOGGER.log("WARNING: User " + userId + " attempted SSO but does not belong to an authorized group.");
                            return new T8WaffleAuthenticationResponse(ResponseType.FAILED, null, null, null);
                        }
                        else
                        {
                            // No groups specified for authorization so the user is automatically authorized.
                            return new T8WaffleAuthenticationResponse(ResponseType.COMPLETED, null, sessionContext, userId);
                        }
                    }
                    else throw new Exception("User SID " + sid + " does not map to any available user.");
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Authentication failed for session: " + sessionContext.getSessionIdentifier(), e);
                return new T8WaffleAuthenticationResponse(ResponseType.FAILED, null, null, null);
            }
        }
        else throw new RuntimeException("Incompatible authorization header in request: " + authorizationHeader);
    }

    private WindowsAuthProviderImpl getAuthProvider(T8SessionContext sessionContext)
    {
        WindowsAuthProviderImpl authProvider;
        String sessionId;

        sessionId = sessionContext.getSessionIdentifier();
        authProvider = authProviders.get(sessionId);
        if (authProvider != null)
        {
            return authProvider;
        }
        else
        {
            authProvider = new WindowsAuthProviderImpl();
            authProviders.put(sessionId, authProvider);
            return authProvider;
        }
    }
}
