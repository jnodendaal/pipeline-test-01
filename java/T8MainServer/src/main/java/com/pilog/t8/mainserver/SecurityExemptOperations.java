package com.pilog.t8.mainserver;

import com.pilog.t8.definition.T8DefinitionManagerResource;
import com.pilog.t8.definition.system.T8ConfigurationManagerResource;
import com.pilog.t8.definition.system.T8SecurityManagerResource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gavin Boshoff
 */
class SecurityExemptOperations
{
    private static final List<String> EXEMPT_OPERATIONS_LIST;

    static
    {
        EXEMPT_OPERATIONS_LIST = new ArrayList<>();
        EXEMPT_OPERATIONS_LIST.add(T8DefinitionManagerResource.OPERATION_GET_INITIALIZED_DEFINITION);
        EXEMPT_OPERATIONS_LIST.add(T8DefinitionManagerResource.OPERATION_GET_SYSTEM_DETAILS);
        EXEMPT_OPERATIONS_LIST.add(T8ConfigurationManagerResource.OPERATION_GET_LOOK_AND_FEEL_DEFINITION);
        EXEMPT_OPERATIONS_LIST.add(T8ConfigurationManagerResource.OPERATION_GET_UI_TRANSLATION_MAP);
        EXEMPT_OPERATIONS_LIST.add(T8ConfigurationManagerResource.OPERATION_REQUEST_UI_TRANSLATIONS);
        EXEMPT_OPERATIONS_LIST.add(T8ConfigurationManagerResource.OPERATION_GET_API_CLASS_MAP);
        EXEMPT_OPERATIONS_LIST.add(T8SecurityManagerResource.OPERATION_LOGIN);
        EXEMPT_OPERATIONS_LIST.add(T8SecurityManagerResource.OPERATION_GET_SSO_HANDLER_DEFINITION);
    }

    static boolean isExemptOperation(T8ServerRequest serverRequest)
    {
        if (serverRequest.getRequestData() instanceof T8OperationExecutionRequest)
        {
            return EXEMPT_OPERATIONS_LIST.contains(((T8OperationExecutionRequest)serverRequest.getRequestData()).getOperationId());
        }

        return false;
    }

    private SecurityExemptOperations() {}
}