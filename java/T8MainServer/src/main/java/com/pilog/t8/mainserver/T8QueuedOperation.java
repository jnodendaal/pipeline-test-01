package com.pilog.t8.mainserver;

import com.pilog.t8.time.T8Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8QueuedOperation
{
    private String operationIid;
    private String operationId;
    private T8Timestamp timeQueued;
    private T8Timestamp timeProcessed;
    private Integer priority;
    private String rootOrgId;
    private String orgId;
    private String agentIid;
    private String agentId;
    private T8QueuedOperationStatus status;
    private String information;
    private final Map<String, Object> parameters;

    public enum T8QueuedOperationStatus {QUEUED, PROCESSED, FAILED};

    public T8QueuedOperation(String operationIid, String operationId)
    {
        this.operationIid = operationIid;
        this.operationId = operationId;
        this.priority = 0;
        this.status = T8QueuedOperationStatus.QUEUED;
        this.parameters = new HashMap<String, Object>();
    }

    public String getOperationIid()
    {
        return operationIid;
    }

    public void setOperationIid(String operationIid)
    {
        this.operationIid = operationIid;
    }

    public String getOperationId()
    {
        return operationId;
    }

    public void setOperationId(String operationId)
    {
        this.operationId = operationId;
    }

    public T8Timestamp getTimeQueued()
    {
        return timeQueued;
    }

    public void setTimeQueued(T8Timestamp timeQueued)
    {
        this.timeQueued = timeQueued;
    }

    public Integer getPriority()
    {
        return priority;
    }

    public void setPriority(Integer priority)
    {
        this.priority = priority;
    }

    public String getRootOrgId()
    {
        return rootOrgId;
    }

    public void setRootOrgId(String rootOrgId)
    {
        this.rootOrgId = rootOrgId;
    }

    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getAgentIid()
    {
        return agentIid;
    }

    public void setAgentIid(String agentIid)
    {
        this.agentIid = agentIid;
    }

    public String getAgentId()
    {
        return agentId;
    }

    public void setAgentId(String agentId)
    {
        this.agentId = agentId;
    }

    public T8Timestamp getTimeProcessed()
    {
        return timeProcessed;
    }

    public void setTimeProcessed(T8Timestamp timeProcessed)
    {
        this.timeProcessed = timeProcessed;
    }

    public T8QueuedOperationStatus getStatus()
    {
        return status;
    }

    public void setStatus(T8QueuedOperationStatus status)
    {
        this.status = status;
    }

    public String getInformation()
    {
        return information;
    }

    public void setInformation(String information)
    {
        this.information = information;
    }

    public void setParameters(Map<String, Object> newParameters)
    {
        this.parameters.clear();
        this.parameters.putAll(newParameters);
    }

    public Map<String, Object> getParameters()
    {
        return new HashMap<>(parameters);
    }

    @Override
    public String toString()
    {
        return "T8QueuedOperation{" + "operationIid=" + operationIid + ", operationId=" + operationId + ", timeQueued=" + timeQueued + ", timeProcessed=" + timeProcessed + ", priority=" + priority + ", rootOrgId=" + rootOrgId + ", orgId=" + orgId + ", agentIid=" + agentIid + ", agentId=" + agentId + ", status=" + status + ", information=" + information + ", parameters=" + parameters + '}';
    }
}
