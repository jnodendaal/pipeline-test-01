package com.pilog.t8.security;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.system.T8SecurityManagerResource;
import com.pilog.t8.definition.system.T8SecurityManagerResource.UserHistoryEvent;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8SecurityPersistenceHandler
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private boolean persistenceEnabled; // Flag that can be set to false to disable persistence.
    private boolean isEntityAccessible = false;

    public T8SecurityPersistenceHandler(T8Context context)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.persistenceEnabled = true;
    }

    public void setEnabled(boolean enabled)
    {
        persistenceEnabled = enabled;
    }

    public void logUserEvent(T8Context eventContext, UserHistoryEvent event) throws Exception
    {
        if (isEntityAccessible)
        {
            T8DataEntity entity;
            String entityId;
            T8DataTransaction tx;

            // Get the transaction and entity definition.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
            entityId = T8SecurityManagerResource.USER_HISTORY_DE_ID;

            // Create a new proces history entity.
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + "$EVENT_INSTANCE_IDENTIFIER", T8IdentifierUtilities.createNewGUID());
            entity.setFieldValue(entityId + "$SESSION_IDENTIFIER", eventContext.getSessionId());
            entity.setFieldValue(entityId + "$USER_IDENTIFIER", eventContext.getUserId());
            entity.setFieldValue(entityId + "$USER_PROFILE_IDENTIFIER", eventContext.getUserProfileId());
            entity.setFieldValue(entityId + "$EVENT", event.toString());
            entity.setFieldValue(entityId + "$TIME", new java.sql.Timestamp(System.currentTimeMillis()));

            // Insert the entity.
            tx.insert(entity);
        }
        else
        {
            // This entity is not accessible, first check if the accessibility has not perhaps been updated, else just log the error
            if (isEntityAccessible = serverContext.getDataManager().isEntityAccessible(internalContext, T8SecurityManagerResource.USER_HISTORY_DE_ID))
            {
                logUserEvent(eventContext, event);
            }
            else
            {
                T8Log.log("History Event " + event + " could not be logged for user " + eventContext.getUserId());
            }
        }
    }
}
