package com.pilog.t8.mainserver;

import com.pilog.json.JsonObject;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.T8JsonParameterSerializer;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.mainserver.T8QueuedOperation.T8QueuedOperationStatus;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

import static com.pilog.t8.definition.mainserver.T8MainServerResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8OperationQueueDataHandler
{
    private final T8MainServer mainServer;
    private final T8ServerContext serverContext;

    public T8OperationQueueDataHandler(T8Context context, T8MainServer mainServer)
    {
        this.serverContext = context.getServerContext();
        this.mainServer = mainServer;
    }

    public void insertQueuedOperation(T8QueuedOperation operation) throws Exception
    {
        T8ServerOperationDefinition operationDefinition;

        // Get the operation definition.
        operationDefinition = mainServer.getOperationDefinition(null, operation.getOperationId());
        if (operationDefinition != null)
        {
            T8JsonParameterSerializer jsonHandler;
            T8DataEntity entityToInsert;
            T8DataTransaction tx;
            JsonObject jsonParameters;

            // Get the transaction to use.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

            // Create an JSON handler and serializer the operation parameters.
            jsonHandler = new T8JsonParameterSerializer();
            jsonParameters = jsonHandler.serializeParameters(operationDefinition.getInputParameterDefinitions(), operation.getParameters());

            // Create the entity to update (only the fields to be updated are needed).
            entityToInsert = tx.create(DE_STATE_OPERATION_QUEUE, null);
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_OPERATION_IID, operation.getOperationIid());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_OPERATION_ID, operation.getOperationId());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_ROOT_ORG_ID, operation.getRootOrgId());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_ORG_ID, operation.getOrgId());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_AGENT_IID, operation.getAgentIid());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_AGENT_ID, operation.getAgentId());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_INFORMATION, operation.getInformation());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_STATUS, operation.getStatus().toString());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_PRIORITY, operation.getPriority());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_TIME_QUEUED, operation.getTimeQueued());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_TIME_PROCESSED, operation.getTimeProcessed());
            entityToInsert.setFieldValue(DE_STATE_OPERATION_QUEUE + F_PARAMETERS, jsonParameters.toString());

            // Insert the operation entity.
            tx.insert(entityToInsert);
        }
        else throw new Exception("Operation not found: " + operation.getOperationId());
    }

    public List<T8QueuedOperation> retrieveQueuedOperations(int pageSize) throws Exception
    {
        List<T8QueuedOperation> operations;
        List<T8DataEntity> triggerEntities;
        T8JsonParameterSerializer jsonHandler;
        T8DataFilter queueFilter;
        T8DataTransaction tx;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Create an JSON handler to use when deserializing the operation parameters.
        jsonHandler = new T8JsonParameterSerializer();

        // Create the filter to use for the retrieval.
        queueFilter = new T8DataFilter(DE_STATE_OPERATION_QUEUE);
        queueFilter.addFilterCriterion(DE_STATE_OPERATION_QUEUE + F_STATUS, T8QueuedOperationStatus.QUEUED.toString());
        queueFilter.addFieldOrdering(DE_STATE_OPERATION_QUEUE + F_TIME_QUEUED, T8DataFilter.OrderMethod.ASCENDING);

        // Retrieve the operation entities.
        operations = new ArrayList();
        triggerEntities = tx.select(DE_STATE_OPERATION_QUEUE, queueFilter, 0, pageSize);
        for (T8DataEntity operationEntity : triggerEntities)
        {
            T8ServerOperationDefinition operationDefinition;
            String operationId;

            // Get the operation definition.
            operationId = (String)operationEntity.getFieldValue(F_OPERATION_ID);

            // Get the operation definition.
            operationDefinition = mainServer.getOperationDefinition(null, operationId);
            if (operationDefinition != null)
            {
                T8QueuedOperation operation;
                String parameterString;

                // Get the serialized operation parameter JSON string.
                parameterString = (String)operationEntity.getFieldValue(F_PARAMETERS);

                // Create the new queued operation object from the entity data.
                operation = new T8QueuedOperation((String)operationEntity.getFieldValue(F_OPERATION_IID), operationId);
                operation.setRootOrgId((String)operationEntity.getFieldValue(F_ROOT_ORG_ID));
                operation.setOrgId((String)operationEntity.getFieldValue(F_ORG_ID));
                operation.setAgentIid((String)operationEntity.getFieldValue(F_AGENT_IID));
                operation.setAgentId((String)operationEntity.getFieldValue(F_AGENT_ID));
                operation.setTimeQueued((T8Timestamp)operationEntity.getFieldValue(F_TIME_QUEUED));
                operation.setTimeProcessed((T8Timestamp)operationEntity.getFieldValue(F_TIME_PROCESSED));
                operation.setPriority((Integer)operationEntity.getFieldValue(F_PRIORITY));
                operation.setStatus(T8QueuedOperationStatus.valueOf((String)operationEntity.getFieldValue(F_STATUS)));
                operation.setParameters(parameterString != null ? jsonHandler.deserializeParameters(operationDefinition.getInputParameterDefinitions(), JsonObject.readFrom(parameterString)) : null);
                operations.add(operation);
            }
            else
            {
                // Update the queued operation entity to reflect the fact that the operation definition could not be found.
                operationEntity.setFieldValue(DE_STATE_OPERATION_QUEUE + F_STATUS, T8QueuedOperationStatus.FAILED.toString());
                operationEntity.setFieldValue(DE_STATE_OPERATION_QUEUE + F_INFORMATION, "Operation Definition not found.");
                tx.update(operationEntity);
            }
        }

        // Return the final list of queued operations.
        return operations;
    }

    public boolean deleteQueuedOperation(String operationIid) throws Exception
    {
        T8DataEntity entityToDelete;
        T8DataTransaction tx;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Create the entity to delete (only the primary key is needed).
        entityToDelete = tx.create(DE_STATE_OPERATION_QUEUE, HashMaps.createSingular(DE_STATE_OPERATION_QUEUE + F_OPERATION_IID, operationIid));

        // Delete the operation entity.
        return tx.delete(entityToDelete);
    }

    public boolean updateQueuedOperation(T8QueuedOperation operation) throws Exception
    {
        T8DataEntity entityToUpdate;
        T8DataTransaction tx;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Create the entity to update (only the fields to be updated are needed).
        entityToUpdate = tx.create(DE_STATE_OPERATION_QUEUE, null);
        entityToUpdate.setFieldValue(DE_STATE_OPERATION_QUEUE + F_OPERATION_IID, operation.getOperationIid());
        entityToUpdate.setFieldValue(DE_STATE_OPERATION_QUEUE + F_INFORMATION, operation.getInformation());
        entityToUpdate.setFieldValue(DE_STATE_OPERATION_QUEUE + F_STATUS, operation.getStatus().toString());
        entityToUpdate.setFieldValue(DE_STATE_OPERATION_QUEUE + F_TIME_PROCESSED, operation.getTimeProcessed());

        // Update the operation entity.
        return tx.update(entityToUpdate);
    }
}
