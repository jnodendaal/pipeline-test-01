package com.pilog.t8.security;

import com.pilog.t8.T8SessionContext;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8SessionContextHandle
{
    private final T8SessionContext session;
    private long creationTime;
    private long loginTime;
    private long lastActivityTime;
    private long sessionTimeout;
    private String authenticationToken;
    private List<String> accessibleUserProfileIdentifiers;

    public T8SessionContextHandle(T8SessionContext session)
    {
        this.session = session;
    }

    public T8SessionContext getSessionContext()
    {
        return session;
    }

    public long getCreationTime()
    {
        return creationTime;
    }

    public void setCreationTime(long creationTime)
    {
        this.creationTime = creationTime;
    }

    public long getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(long lastActivityTime)
    {
        this.lastActivityTime = lastActivityTime;
    }

    public long getLoginTime()
    {
        return loginTime;
    }

    public void setLoginTime(long loginTime)
    {
        this.loginTime = loginTime;
    }

    public long getSessionTimeout()
    {
        return sessionTimeout;
    }

    public void setSessionTimeout(long sessionTimeout)
    {
        this.sessionTimeout = sessionTimeout;
    }

    public String getAuthenticationToken()
    {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken)
    {
        this.authenticationToken = authenticationToken;
    }

    public List<String> getAccessibleUserProfileIdentifiers()
    {
        return accessibleUserProfileIdentifiers;
    }

    public void setAccessibleUserProfileIdentifiers(List<String> accessibleUserProfileIdentifiers)
    {
        this.accessibleUserProfileIdentifiers = accessibleUserProfileIdentifiers;
    }
}
