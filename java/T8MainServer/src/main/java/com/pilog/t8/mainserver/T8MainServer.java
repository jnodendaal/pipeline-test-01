package com.pilog.t8.mainserver;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8Server;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation.T8ServerOperationStatus;
import com.pilog.t8.T8ServerOperationNotificationHandle;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.T8ServiceManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.event.T8DefinitionCacheReloadedEvent;
import com.pilog.t8.definition.event.T8DefinitionManagerListener;
import com.pilog.t8.definition.event.T8DefinitionSavedEvent;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.system.event.T8OperationCompletedEvent;
import com.pilog.t8.system.event.T8ServerEventListener;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.mainserver.T8OperationExecutionRequest.ExecutionType;
import com.pilog.t8.mainserver.T8ServerRequest.RequestType;
import com.pilog.t8.mainserver.T8ServerResponse.ResponseType;
import com.pilog.t8.security.T8HeartbeatResponse;
import com.pilog.t8.security.T8PasswordChangeRequest;
import com.pilog.t8.security.T8PasswordChangeResponse;
import com.pilog.t8.security.T8SessionDetails;
import com.pilog.t8.security.T8UserLoginRequest;
import com.pilog.t8.security.T8UserLoginResponse;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import com.pilog.t8.utilities.update.client.jar.FileModifiedChecker;
import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.swing.event.EventListenerList;
import com.jezhumble.javasysmon.CpuTimes;
import com.jezhumble.javasysmon.JavaSysMon;
import com.pilog.t8.T8ReportManager;
import com.pilog.t8.T8UserManager;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.event.T8DefinitionManagerEventAdapter;
import com.pilog.t8.definition.http.api.T8HttpApiDefinition;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.http.api.T8HttpApi;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.authentication.T8AuthenticationResponse;
import com.pilog.t8.security.authentication.T8DefaultAuthenticationResponse;

/**
 * @author Bouwer du Preez
 */
@WebListener
public class T8MainServer implements T8Server, ServletContextListener
{
    // This is a logger which is an exception to the rule to ensure the prefix identifier
    // can be added. All other loggers should be declared final.
    private static T8Logger LOGGER = T8Log.getLogger(T8MainServer.class);

    private final Map<String, T8ServerOperationDefinition> publicOperationDefinitions; // Used to store all public operation definitions loaded from meta at servlet startup.
    private final Map<String, Map<String, T8ServerOperationDefinition>> privateOperationDefinitions; // Used to store all operation definitions loaded from meta at servlet startup.
    private final Map<String, OperationExecutionHandle> executionHandles; // Used to store handles to all currently executing and recently completed operations.
    private final Map<String, T8HttpApi> httpApis;
    private final EventListenerList listeners; // Server event listeners.
    private ScheduledExecutorService executor; // Timer used to run maintenance jobs.
    private ServerMaintenanceThread maintenanceThread;
    private T8OperationQueueProcessor operationQueueProcessor;
    private T8OperationQueueDataHandler operationQueueDataHandler; // Used to manage operations in the queue table.
    private T8ServerContext serverContext;
    private T8CommunicationManager communicationManager;
    private T8NotificationManager notificationManager;
    private T8DefinitionManager definitionManager;
    private T8SecurityManager securityManager;
    private T8ProcessManager processManager;
    private T8FlowManager flowManager;
    private T8FunctionalityManager functionalityManager;
    private T8DataManager dataManager;
    private T8FileManager fileManager;
    private T8ConfigurationManager configurationManager;
    private T8ServiceManager serviceManager;
    private T8UserManager userManager;
    private T8ReportManager reportManager;
    private String contextPath;
    private String applicationVersion;
    private String buildTimestamp;
    private T8DefinitionManagerListener definitionManagerListener;
    private ServletContext servletContext;
    private T8Context internalContext;
    private static int requestsDuringInterval = 0;
    private static double requestsPerSecond = 0;
    private static double serverLoad = 0;
    private static CpuTimes cpuTimes;
    private static volatile int concurrentRequestCount = 0;
    private static int maxConcurrentRequests = 0;

    public static final String T8_MAIN_SERVER_CONTEXT_IDENTIFIER = "T8_MAIN_SERVER_CONTEXT";
    public static final String T8_SERVER_CONTEXT_IDENTIFIER = "T8_SERVER_CONTEXT";
    private static final String FILE_MANAGER_TEMP_FILE_PATH = "/files/temp";
    private static final long MAINTENANCE_INTERVAL = 10000; // The interval (miliseconds) at which the maintenance task is executed.
    private static final long OPERATION_QUEUE_PROCESS_INTERVAL = 5000; // The interval (miliseconds) at which the operation queue will be processed (the next operation handled).
    private static final long OPERATION_EXPIRATION_TIME = 120000; // The number of miliseconds that a completed operation is maintained before it is discarded.

    /**
     * Servlet constructor.
     */
    public T8MainServer()
    {
        // Initialize the requried resources.
        publicOperationDefinitions = Collections.synchronizedMap(new HashMap<String, T8ServerOperationDefinition>());
        privateOperationDefinitions = Collections.synchronizedMap(new HashMap<String, Map<String, T8ServerOperationDefinition>>());
        executionHandles = Collections.synchronizedMap(new HashMap<String, OperationExecutionHandle>());
        listeners = new EventListenerList();
        httpApis = new HashMap<>();
    }

    /**
     * Initializes the Server, loading all operation definitions from the
     * database and starting all manager threads.
     * @param servletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent)
    {
        try
        {
            servletContext = servletContextEvent.getServletContext();
            initializeServer();
            servletContext.setAttribute(T8_MAIN_SERVER_CONTEXT_IDENTIFIER, new T8MainServerContext(this));
            servletContext.setAttribute(T8_SERVER_CONTEXT_IDENTIFIER, serverContext);
        }
        catch (Exception e)
        {
            LOGGER.log("Critical exception while initializing T8 Server Threads.", e);
        }
    }

    /**
     * Destroys the Server.  This method releases all resources used by the
     * server.
     * @param servletContextEvent
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent)
    {
        // Remove all references to T8 objects from the servlet context.
        LOGGER.log("******** T8 Servlet Context Destroyed ********");
        servletContext.removeAttribute(T8_MAIN_SERVER_CONTEXT_IDENTIFIER);
        servletContext.removeAttribute(T8_SERVER_CONTEXT_IDENTIFIER);
        shutDownServer();
    }

    private void initializeServer() throws Exception
    {
        try
        {
            T8ServerModuleFactory moduleFactory;

            // Initialize the server load metric.
            checkServerLoad();

            // Create a new server context.
            serverContext = createServerContext();
            contextPath = servletContext.getInitParameter("CONTEXT.PATH");
            applicationVersion = servletContext.getInitParameter("APPLICATION.VERSION");
            buildTimestamp = servletContext.getInitParameter("BUILD.TIMESTAMP");

            // Initialize logger.
            if (!T8Log.isPrefixIdentifierSet())
            {
                LOGGER.log("> Setting Logger Identifier: " + this.contextPath.substring(this.contextPath.lastIndexOf('\\')+1));
                T8Log.setPrefixIdentifier(this.contextPath.substring(this.contextPath.lastIndexOf('\\')+1));
            }
            LOGGER = T8Log.getLogger(T8MainServer.class);

            // Create a new module factory.
            moduleFactory = new T8DefaultServerModuleFactory(serverContext);

            // Do first round startup:  Initialize all server modules.
            LOGGER.log("======== T8 Application Version " + applicationVersion + " ========");
            LOGGER.log("> Build Timestamp: " + buildTimestamp);
            LOGGER.log("> Using Context Path: " + contextPath);
            LOGGER.log("> Initializing Security Manager...");
            securityManager = moduleFactory.constructSecurityManager();
            securityManager.init();
            internalContext = securityManager.getMainServerContext();
            LOGGER.log("> Initializing Definition Manager...");
            definitionManager = moduleFactory.constructDefinitionManager();
            definitionManager.init();
            LOGGER.log("> Initializing Configuration Manager...");
            configurationManager = moduleFactory.constructConfigurationManager();
            configurationManager.init();
            LOGGER.log("> Initializing Data Manager...");
            dataManager = moduleFactory.constructDataManager();
            dataManager.init();

            // From this point onward, the T8DataManager is ready peform data session and transaction management.
            // We create a data session so that it can be used by subsequent steps in the startup routine.
            internalContext = serverContext.getSecurityManager().createContext(internalContext.getSessionContext());
            dataManager.openDataSession(internalContext);
            LOGGER.log("> Caching definitions...");
            definitionManager.loadDefinitionData(null);
            LOGGER.log("> " + definitionManager.getCachedDataSize(null) + " bytes definition data loaded.");
            LOGGER.log("> " + definitionManager.getCachedDefinitionTypeCount(null) + " definition types loaded.");
            LOGGER.log("> " + definitionManager.getCachedDefinitionCount(null) + " definitions loaded.");
            definitionManagerListener = new DefinitionManagerListener();
            definitionManager.addDefinitionManagerListener(definitionManagerListener);
            LOGGER.log("> Initializing Process Manager...");
            processManager = moduleFactory.constructProcessManager();
            processManager.init();
            LOGGER.log("> Initializing Flow Manager...");
            flowManager = moduleFactory.constructFlowManager();
            flowManager.init();
            LOGGER.log("> Initializing Functionality Manager...");
            functionalityManager = moduleFactory.constructFunctionalityManager();
            functionalityManager.init();
            LOGGER.log("> Initializing File Manager...");
            fileManager = moduleFactory.constructFileManager(new File(contextPath + FILE_MANAGER_TEMP_FILE_PATH));
            fileManager.init();
            LOGGER.log("> Initializing User Manager...");
            userManager = moduleFactory.constructUserManager();
            userManager.init();
            LOGGER.log("> Initializing Report Manager...");
            reportManager = moduleFactory.constructReportManager();
            reportManager.init();
            LOGGER.log("> Initializing Service Manager...");
            serviceManager = moduleFactory.constructServiceManager();
            serviceManager.init();
            LOGGER.log("> Initializing Communication Manager...");
            communicationManager = moduleFactory.constructCommunicationManager();
            communicationManager.init();
            LOGGER.log("> Initializing Notification Manager...");
            notificationManager = moduleFactory.constructNotificationManager();
            notificationManager.init();

            // Do the second-round startup rountine: Start all server modules.
            definitionManager.start();
            securityManager.start();
            configurationManager.start();
            dataManager.start();
            processManager.start();
            flowManager.start();
            functionalityManager.start();
            fileManager.start();
            userManager.start();
            reportManager.start();
            serviceManager.start();
            communicationManager.start();
            notificationManager.start();

            LOGGER.log("> Caching Server Operations...");
            cacheOperationDefinitions();

            LOGGER.log("> Caching HTTP APIs...");
            cacheHttpApis();

            // Check JAR modification dates and log successful server startup.
            LOGGER.log("> Checking Jar Last Modified Dates...");
            checkJarLastModifiedDates();

            // Create server data handlers.
            operationQueueDataHandler = new T8OperationQueueDataHandler(internalContext, this);

            // Start the maintenance executor.
            LOGGER.log("> Starting Server Maintenance Executor...");
            executor = Executors.newSingleThreadScheduledExecutor(new NameableThreadFactory("T8MainServer-Maintenance"));
            maintenanceThread = new ServerMaintenanceThread(executionHandles, OPERATION_EXPIRATION_TIME);
            executor.scheduleWithFixedDelay(maintenanceThread, MAINTENANCE_INTERVAL, MAINTENANCE_INTERVAL, TimeUnit.MILLISECONDS);

            // Schedule the thread to process the operation queue.
            operationQueueProcessor = new T8OperationQueueProcessor(internalContext, this, operationQueueDataHandler, true);
            executor.scheduleWithFixedDelay(operationQueueProcessor, OPERATION_QUEUE_PROCESS_INTERVAL, OPERATION_QUEUE_PROCESS_INTERVAL, TimeUnit.MILLISECONDS);

            // Log final completion of server startup.
            LOGGER.log("> Server started successfully.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception during Main Server startup.", e);
            throw e;
        }
        finally
        {
            // Make sure to close the current data session used for server startup.
            dataManager.closeCurrentSession();
            serverContext.getSecurityManager().destroyContext();
        }
    }

    private void shutDownServer()
    {
        LOGGER.log("======== T8MainServer Shutdown ========");
        LOGGER.log("> Stopping maintenance threads...");

        // Terminate the maintenance thread and all scheduled tasks.
        try
        {
            // Stop the operation queue processor.
            operationQueueProcessor.setEnabled(false);

            // Shutdown the executor.
            executor.shutdown();
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) throw new Exception("Server maintenance thread executor could not by terminated.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while awaiting termination of server maintenance thread.", e);
        }

        // Destroy maintenance thread.
        maintenanceThread.destroy();
        maintenanceThread = null;

        LOGGER.log("> Stopping all currently executing server operations...");
        stopAllOperationExecution();
        LOGGER.log("> Clearing Main Server cached data...");
        publicOperationDefinitions.clear();
        executionHandles.clear();
        LOGGER.log("> Shutting down Communication manager...");
        communicationManager.destroy();
        LOGGER.log("> Shutting down Notification manager...");
        notificationManager.destroy();
        LOGGER.log("> Shutting down Configuration manager...");
        configurationManager.destroy();
        LOGGER.log("> Shutting down Service manager...");
        serviceManager.destroy();
        LOGGER.log("> Shutting down Report manager...");
        reportManager.destroy();
        LOGGER.log("> Shutting down User manager...");
        userManager.destroy();
        LOGGER.log("> Shutting down File manager...");
        fileManager.destroy();
        LOGGER.log("> Shutting down Functionality manager...");
        functionalityManager.destroy();
        LOGGER.log("> Shutting down Flow manager...");
        flowManager.destroy();
        LOGGER.log("> Shutting down Process manager...");
        processManager.destroy();
        LOGGER.log("> Shutting down Data manager...");
        dataManager.destroy();
        LOGGER.log("> Shutting down Security manager...");
        securityManager.destroy();
        LOGGER.log("> Shutting down Definition manager...");
        definitionManager.destroy();
        LOGGER.log("> Server shutdown completed.");
    }

    private void stopAllOperationExecution()
    {
        synchronized (executionHandles)
        {
            for (OperationExecutionHandle operationHandle : executionHandles.values())
            {
                try
                {
                    Thread executionThread;

                    // Cancel the operation.
                    operationHandle.cancelOperationExecution(null);

                    // If a thread is still executing, wait for it to die.
                    executionThread = operationHandle.getExecutionThread();
                    if ((executionThread != null) && (executionThread != Thread.currentThread()))
                    {
                        int waitCycle;

                        waitCycle = 0;
                        while (executionThread.isAlive())
                        {
                            waitCycle++;
                            executionThread.join(1000);
                            if (waitCycle > 10) LOGGER.log("Waiting for operation to finish execution: " + operationHandle.getOperationDefinition());
                            if (waitCycle > 60)
                            {
                                LOGGER.log("Could not successfully finish execution of operation...interrupting thread:" + operationHandle.getOperationDefinition());
                                executionThread.interrupt();
                            }
                            if (waitCycle > 80)
                            {
                                LOGGER.log("Could not successfully interrupt execution of operation...abandoning safe finalization of operation:" + operationHandle.getOperationDefinition());
                                break;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while trying to cancel operation execution: " + operationHandle.getOperationDefinition(), e);
                }
            }
        }
    }

    private T8ServerContext createServerContext() throws Exception
    {
        Constructor constructor;

        constructor = Class.forName("com.pilog.t8.system.T8DefaultServerContext").getConstructor(T8Server.class);
        return (T8ServerContext)constructor.newInstance(this);
    }

    private void cacheOperationDefinitions()
    {
        LOGGER.log("> Caching Operation Definitions...");
        publicOperationDefinitions.clear();
        privateOperationDefinitions.clear();

        try
        {
            List<String> operationIds;
            List<String> projectIds;

            // First cache all public operations.
            operationIds = definitionManager.getGroupDefinitionIdentifiers(null, T8ServerOperationDefinition.GROUP_IDENTIFIER);
            for (String operationId : operationIds)
            {
                try
                {
                    T8Definition operationDefinition;

                    operationDefinition = definitionManager.getInitializedDefinition(internalContext, null, operationId, null);
                    if (operationDefinition != null)
                    {
                        cacheOperationDefinition((T8ServerOperationDefinition)operationDefinition);
                    }
                    else LOGGER.log("> Operation Definition '" + operationId + "' not found and could not be cached.");
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while caching operation definition: " + operationId, e);
                }
            }

            // Now cache all project-specific private operations.
            projectIds = definitionManager.getGroupDefinitionIdentifiers(null, T8ProjectDefinition.GROUP_IDENTIFIER);
            for (String projectId : projectIds)
            {
                operationIds = definitionManager.getGroupDefinitionIdentifiers(projectId, T8ServerOperationDefinition.GROUP_IDENTIFIER);
                for (String operationId : operationIds)
                {
                    if (T8IdentifierUtilities.isPrivateId(operationId))
                    {
                        try
                        {
                            T8Definition operationDefinition;

                            operationDefinition = definitionManager.getInitializedDefinition(internalContext, projectId, operationId, null);
                            if (operationDefinition != null)
                            {
                                cacheOperationDefinition((T8ServerOperationDefinition)operationDefinition);
                            }
                            else LOGGER.log("> Operation Definition '" + operationId + "' not found and could not be cached.");
                        }
                        catch (Exception e)
                        {
                            LOGGER.log("Exception while caching operation definition: " + operationId + " in project: " + projectId, e);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while caching all operation definitions.", e);
        }
    }

    private void cacheOperationDefinition(T8ServerOperationDefinition operationDefinition)
    {
        try
        {
            String operationId;
            String projectId;

            LOGGER.log("> Caching Operation Definition " + operationDefinition + "...");
            projectId = operationDefinition.getRootProjectId();
            operationId = operationDefinition.getIdentifier();
            if (operationDefinition.isPublic())
            {
                publicOperationDefinitions.put(operationDefinition.getIdentifier(), (T8ServerOperationDefinition)operationDefinition);
            }
            else
            {
                Map<String, T8ServerOperationDefinition> projectDefinitions;

                projectDefinitions = privateOperationDefinitions.get(projectId);
                if (projectDefinitions == null)
                {
                    projectDefinitions = new HashMap<>();
                    privateOperationDefinitions.put(projectId, projectDefinitions);
                }

                projectDefinitions.put(operationId, operationDefinition);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while caching operation definition: " + operationDefinition, e);
        }
    }

    @Override
    public T8ServerOperationDefinition getOperationDefinition(T8Context context, String operationId)
    {
        T8ServerOperationDefinition operationDefinition;

        operationDefinition = publicOperationDefinitions.get(operationId);
        if (operationDefinition == null)
        {
            Map<String, T8ServerOperationDefinition> projectOperations;

            projectOperations = privateOperationDefinitions.get(context.getProjectId());
            if (projectOperations != null)
            {
                return projectOperations.get(operationId);
            }
            else return null;
        }
        else return operationDefinition;
    }

    @Override
    public List<T8ServerOperationDefinition> getOperationDefinitions()
    {
        return new ArrayList<>(publicOperationDefinitions.values());
    }

    private void cacheHttpApis()
    {
        LOGGER.log("> Caching Http Apis...");
        httpApis.clear();

        try
        {
            List<T8HttpApiDefinition> apiDefinitions;

            // First cache all public operations.
            apiDefinitions = definitionManager.getInitializedGroupDefinitions(internalContext, null, T8HttpApiDefinition.GROUP_IDENTIFIER, null);
            for (T8HttpApiDefinition apiDefinition : apiDefinitions)
            {
                cacheHttpApi(apiDefinition);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while caching http api definitions.", e);
        }
    }

    private void cacheHttpApi(T8HttpApiDefinition definition) throws Exception
    {
        T8HttpApi api;

        api = definition.getHttpApi(this, internalContext);
        httpApis.put(api.getName().toUpperCase(), api);
    }

    public T8HttpApi getHttpApi(String apiName)
    {
        return httpApis.get(apiName.toUpperCase());
    }

    @Override
    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    @Override
    public String getContextPath()
    {
        return contextPath;
    }

    @Override
    public String getRealPath(String relativePath)
    {
        return servletContext.getRealPath(relativePath);
    }

    @Override
    public void setContextPath(String contextPath) throws Exception
    {
        LOGGER.log("> Using Context Path: " + contextPath);
        this.contextPath = contextPath;
        definitionManager.loadDefinitionData(null);
        LOGGER.log("> " + definitionManager.getCachedDataSize(null) + " bytes definition data loaded.");
        LOGGER.log("> " + definitionManager.getCachedDefinitionCount(null) + " definitions loaded.");
    }

    @Override
    public T8DefinitionManager getDefinitionManager()
    {
        return definitionManager;
    }

    @Override
    public T8CommunicationManager getCommunicationManager()
    {
        return communicationManager;
    }

    @Override
    public T8NotificationManager getNotificationManager()
    {
        return notificationManager;
    }

    @Override
    public T8SecurityManager getSecurityManager()
    {
        return securityManager;
    }

    @Override
    public T8ProcessManager getProcessManager()
    {
        return processManager;
    }

    @Override
    public T8ServiceManager getServiceManager()
    {
        return serviceManager;
    }

    @Override
    public T8FlowManager getFlowManager()
    {
        return flowManager;
    }

    @Override
    public T8FunctionalityManager getFunctionalityManager()
    {
        return functionalityManager;
    }

    @Override
    public T8DataManager getDataManager()
    {
        return dataManager;
    }

    @Override
    public T8FileManager getFileManager()
    {
        return fileManager;
    }

    @Override
    public T8UserManager getUserManager()
    {
        return userManager;
    }

    @Override
    public T8ReportManager getReportManager()
    {
        return reportManager;
    }

    @Override
    public T8ConfigurationManager getConfigurationManager()
    {
        return configurationManager;
    }

    public final T8ServerResponse handleRequest(T8ServerRequest serverRequest)
    {
        try
        {
            RequestType requestType;
            T8Context context;

            // Update performance statistics.
            concurrentRequestCount++;
            requestsDuringInterval++;
            if (concurrentRequestCount > maxConcurrentRequests) maxConcurrentRequests = concurrentRequestCount;

            // Get the T8ServerRequest object and the request type.
            requestType = serverRequest.getRequestType();
            context = securityManager.getContext();

            // Only specific requests are allowed without authentication of the requesting session.
            // For all other request types authenticate the session context received from the client.
            switch (requestType)
            {
                case HEARTBEAT:
                {
                    T8SessionContext sessionContext;
                    String userId;

                    // Authenticate the session without rereshing the last activity time.
                    sessionContext = context.getSessionContext();
                    userId = sessionContext.getUserIdentifier();
                    if (!sessionContext.isLoggedIn())
                    {
                        throw new T8SessionExpiredException("Session authentication failed for " + userId + " during request: " + serverRequest, context != null);
                    }
                    else
                    {
                        T8HeartbeatResponse heartbeatResponse;

                        heartbeatResponse = heartbeat(context);
                        return new T8ServerResponse(ResponseType.HEARTBEAT, heartbeatResponse);
                    }
                }
                case AUTHENTICATE:
                {
                    return new T8ServerResponse(ResponseType.AUTHENTICATION_RESPONSE, new T8DefaultAuthenticationResponse(T8AuthenticationResponse.ResponseType.COMPLETED, context.getSessionContext()));
                }
                case CREATE_NEW_SESSION:
                {
                    T8SessionContext newSession;

                    newSession = getSecurityManager().createNewSessionContext();
                    return new T8ServerResponse(ResponseType.SESSION_CONTEXT, newSession);
                }
                case LOGIN:
                {
                    T8UserLoginRequest loginRequest;
                    T8UserLoginResponse loginResponse;

                    loginRequest = (T8UserLoginRequest)serverRequest.getRequestData();
                    loginResponse = getSecurityManager().login(context, loginRequest.getLoginLocation(), loginRequest.getUserIdentifier(), loginRequest.getPassword(), loginRequest.isEndExistingSession());
                    return new T8ServerResponse(ResponseType.LOGIN_RESPONSE, loginResponse);
                }
                case CHANGE_PASSWORD:
                {
                    T8PasswordChangeRequest passwordChangeRequest;
                    T8PasswordChangeResponse passwordChangeResponse;

                    passwordChangeRequest = (T8PasswordChangeRequest)serverRequest.getRequestData();
                    passwordChangeResponse = getSecurityManager().changePassword(context, passwordChangeRequest.getUserIdentifier(), passwordChangeRequest.getPassword());
                    return new T8ServerResponse(ResponseType.PASSWORD_CHANGE_RESPONSE, passwordChangeResponse);
                }
                case REQUEST_PASSWORD_CHANGE:
                {
                    String userIdentifier;
                    boolean success;

                    userIdentifier = (String)serverRequest.getRequestData();
                    success = getSecurityManager().requestPasswordChange(context, userIdentifier);
                    return new T8ServerResponse(ResponseType.SUCCESS, success);
                }
                case CHECK_PASSWORD_CHANGE_AVAILABILITY:
                {
                    String userIdentifier;
                    boolean available;

                    userIdentifier = (String)serverRequest.getRequestData();
                    available = getSecurityManager().isPasswordChangeAvailable(context, userIdentifier);
                    return new T8ServerResponse(ResponseType.SUCCESS, available);
                }
                case CHECK_USERNAME_AVAILABILITY:
                {
                    String userIdentifier;
                    boolean available;

                    userIdentifier = (String)serverRequest.getRequestData();
                    available = getSecurityManager().isPasswordChangeAvailable(context, userIdentifier);
                    return new T8ServerResponse(ResponseType.SUCCESS, available);
                }
                case CHECK_USER_REGISTERED:
                {
                    String userIdentifier;
                    boolean registered;

                    userIdentifier = (String)serverRequest.getRequestData();
                    registered = getSecurityManager().isUserRegistered(context, userIdentifier);
                    return new T8ServerResponse(ResponseType.SUCCESS, registered);
                }
                case EXECUTE_OPERATION:
                {
                    if (SecurityExemptOperations.isExemptOperation(serverRequest)) break;
                }
                default:
                {
                    T8SessionContext sessionContext;

                    // If none of the preceding request types were applicable, we have to authenticate the session before proceeding to the next section.
                    sessionContext = context.getSessionContext();
                    if (!sessionContext.isLoggedIn())
                    {
                        return new T8ServerResponse(ResponseType.AUTHENTICATION_RESPONSE, new T8DefaultAuthenticationResponse(T8AuthenticationResponse.ResponseType.FAILED, sessionContext));
                    }
                    else
                    {
                        // Update the last activity time.
                        securityManager.extendSession(context);
                    }
                }
            }

            // Now that the session has been authenticated, check the request type and generate the required response.
            switch (requestType)
            {
                case EXECUTE_OPERATION:
                {
                    T8OperationExecutionRequest executionRequest;
                    Map<String, Object> operationInput;
                    String operationId;

                    // Get the execution request and the matching operation definition.
                    executionRequest = (T8OperationExecutionRequest)serverRequest.getRequestData();
                    operationId = executionRequest.getOperationId();
                    operationInput = executionRequest.getOperationParameters();

                    // If no operation input was supplied, create an empty collection to eliminate the need for downstream null checks.
                    if (operationInput == null) operationInput = new HashMap<String, Object>();

                    // Check the execution type and create a execution handle for the operation to execute.  Run the operation depending on its execution type.
                    if (executionRequest.getOperationExecutionType() == ExecutionType.SYNCHRONOUS)
                    {
                        Map<String, Object> operationOutput;

                        operationOutput = executeSynchronousOperation(context, operationId, operationInput);
                        return new T8ServerResponse(ResponseType.OPERATION_RESULT, operationOutput);
                    }
                    else
                    {
                        T8ServerOperationStatusReport statusReport;

                        statusReport = executeAsynchronousOperation(context, operationId, operationInput);
                        return new T8ServerResponse(ResponseType.OPERATION_STATUS, statusReport);
                    }
                }
                case GET_OPERATION_STATUS: // Will also remove the operation from the executing collection if it has completed.
                {
                    T8ServerOperationStatusReport statusReport;
                    String operationIid;

                    operationIid = (String)serverRequest.getRequestData();
                    statusReport = getOperationStatus(context, operationIid);
                    return new T8ServerResponse(ResponseType.OPERATION_STATUS, statusReport);
                }
                case STOP_OPERATION:
                {
                    String operationGUID;
                    T8ServerOperationStatusReport statusReport;

                    operationGUID = (String)serverRequest.getRequestData();
                    statusReport = stopOperation(context, operationGUID);
                    return new T8ServerResponse(ResponseType.OPERATION_STATUS, statusReport);
                }
                case CANCEL_OPERATION:
                {
                    String operationGUID;
                    T8ServerOperationStatusReport statusReport;

                    operationGUID = (String)serverRequest.getRequestData();
                    statusReport = cancelOperation(context, operationGUID);
                    return new T8ServerResponse(ResponseType.OPERATION_STATUS, statusReport);
                }
                case PAUSE_OPERATION:
                {
                    String operationGUID;
                    T8ServerOperationStatusReport statusReport;

                    operationGUID = (String)serverRequest.getRequestData();
                    statusReport = pauseOperation(context, operationGUID);
                    return new T8ServerResponse(ResponseType.OPERATION_STATUS, statusReport);
                }
                case GET_OPERATION_STATUSES: // Admin operation.
                {
                    ArrayList<T8ServerOperationStatusReport> operationStatuses;
                    HashSet<String> requestedOperations;

                    // Get the collection of operations requested.
                    requestedOperations = (HashSet<String>)serverRequest.getRequestData();

                    // Get an T8ServerOperationStatusReport of all requested operations.
                    operationStatuses = new ArrayList<>();
                    for (String operationGUID : requestedOperations)
                    {
                        OperationExecutionHandle executionHandle;

                        executionHandle = executionHandles.get(operationGUID);
                        if (executionHandle != null) operationStatuses.add(executionHandle.getOperationStatusReport(context, false)); // Don't include the progress report, since we don't want this monitoring process to interfere with the normal execution of the operation.
                    }

                    return new T8ServerResponse(ResponseType.OPERATION_STATUSES, operationStatuses);
                }
                case GET_ALL_OPERATION_STATUSES: // Admin operation.
                {
                    ArrayList<T8ServerOperationStatusReport> operationStatuses;
                    TreeSet<String> executingOperations;

                    // Get an T8ServerOperationStatusReport of all currently executing operations.
                    operationStatuses = new ArrayList<>();
                    executingOperations = new TreeSet<>(executionHandles.keySet());
                    for (String operationGUID : executingOperations)
                    {
                        OperationExecutionHandle executionHandle;

                        executionHandle = executionHandles.get(operationGUID);
                        if (executionHandle != null) operationStatuses.add(executionHandle.getOperationStatusReport(context, false)); // Don't include the progress report, since we don't want this monitoring process to interfere with the normal execution of the operation.
                    }

                    return new T8ServerResponse(ResponseType.OPERATION_STATUSES, operationStatuses);
                }
                case GET_SERVER_RUNTIME_DATA: // Admin operation.
                {
                    T8MainServerRuntimeData runtimeData;

                    runtimeData = getRuntimeData();
                    return new T8ServerResponse(ResponseType.SERVER_RUNTIME_DATA, runtimeData);
                }
                case SET_CONTEXT_PATH:
                {
                    String newContextPath;

                    newContextPath = (String)serverRequest.getRequestData();
                    setContextPath(newContextPath);
                    return new T8ServerResponse(ResponseType.SUCCESS, true);
                }
                case LOGOUT:
                {
                    boolean success;

                    success = getSecurityManager().logout(context);
                    return new T8ServerResponse(ResponseType.SESSION_CONTEXT, success ? context : null);
                }
                case SWITCH_USER_PROFILE:
                {
                    String userProfileIdentifier;
                    boolean success;

                    userProfileIdentifier = (String)serverRequest.getRequestData();
                    success = getSecurityManager().switchUserProfile(context, userProfileIdentifier);
                    return new T8ServerResponse(ResponseType.SESSION_CONTEXT, success ? context : null);
                }
                default: throw new Exception("Invalid T8 Server Request Type: " + requestType);
            }
        }
        catch (T8SessionExpiredException see)
        {
            if (see.isAnonymousSession()) LOGGER.log(T8Logger.Level.ERROR, "Session Expired. " + see.getMessage());
            else LOGGER.log("Server Exception", see);
            return new T8ServerResponse(ResponseType.THROWABLE, see);
        }
        catch (Throwable e)
        {
            LOGGER.log("Server Exception", e);
            return new T8ServerResponse(ResponseType.THROWABLE, e);
        }
        finally
        {
            concurrentRequestCount--;
        }
    }

    /**
     * Returns the status report obtained from the specified operation.  This
     * method will also remove the operation from the collection of executing
     * operations if it has finished execution.
     *
     * @param context
     * @param operationIid
     * @return The status report obtained from the specified operation.
     * @throws Exception
     */
    @Override
    public T8ServerOperationStatusReport getOperationStatus(T8Context context, String operationIid) throws Exception
    {
        OperationExecutionHandle executionHandle;

        executionHandle = executionHandles.get(operationIid);
        if (executionHandle != null)
        {
            synchronized (executionHandle)
            {
                T8ServerOperationStatusReport statusReport;
                T8ServerOperationStatus operationStatus;

                // Get the status report from the execution handle.
                statusReport = executionHandle.getOperationStatusReport(context, true);
                operationStatus = statusReport.getOperationStatus();

                // If the execution of the process is not in progress any more, then remove it from the collection of executing operations.
                if (operationStatus != T8ServerOperationStatus.IN_PROGRESS
                    && operationStatus != T8ServerOperationStatus.CANCELLING
                    && operationStatus != T8ServerOperationStatus.STOPPING
                    && operationStatus != T8ServerOperationStatus.PAUSING
                    && operationStatus != T8ServerOperationStatus.PAUSED)
                {
                    executionHandles.remove(executionHandle.getOperationGUID());
                }

                // Return the status report to the client.
                return statusReport;
            }
        }
        else return null;
    }

    /**
     * This method instructs a specific operation that is currently executing to
 setEnabled its execution as soon as possible.  The method blocks until the
     * operation in question has stopped executing.
     *
     * @param context
     * @param operationIid
     * @return The T8ServerOperationStatusReport obtained from the specified operation
     * upon termination.
     * @throws Exception
     */
    @SuppressWarnings("SleepWhileHoldingLock")
    @Override
    public T8ServerOperationStatusReport stopOperation(T8Context context, String operationIid) throws Exception
    {
        OperationExecutionHandle executionHandle;

        executionHandle = executionHandles.get(operationIid);
        if (executionHandle != null)
        {
            // Only an operation that is still running can be stopped.
            if (executionHandle.getOperationStatus() == T8ServerOperationStatus.IN_PROGRESS)
            {
                T8ServerOperationNotificationHandle notificationHandle;

                notificationHandle = executionHandle.getOperationStatusReport(context, false).getNotificationHandle();
                synchronized(notificationHandle)
                {
                    int waitCount;

                    // Stop the operation execution.
                    executionHandle.stopOperationExecution(context);

                    // Wait for the executing operation to setEnabled processing.
                    waitCount = 0;
                    while (executionHandle.getOperationStatus() != T8ServerOperationStatus.STOPPED)
                    {
                        notificationHandle.wait(1000);
                        waitCount++;
                        if (waitCount > 20)
                        {
                            throw new Exception("Operation signalled to stop but did not respond: " + operationIid + " Current status: " + executionHandle.getOperationStatus());
                        }
                    }
                }
            }

            // Return a status report of the stopped operation.
            return executionHandle.getOperationStatusReport(context, true);
        }
        else throw new Exception("Operation Execution Handle '" + operationIid + "' not found.");
    }

    /**
     * This method instructs a specific operation that is currently executing to
     * cancel its execution as soon as possible.  The method blocks until the
     * operation in question has stopped executing.
     *
     * @param context
     * @param operationIid
     * @return The T8ServerOperationStatusReport obtained from the specified operation
     * upon termination.
     * @throws Exception
     */
    @SuppressWarnings("SleepWhileHoldingLock")
    @Override
    public T8ServerOperationStatusReport cancelOperation(T8Context context, String operationIid) throws Exception
    {
        OperationExecutionHandle executionHandle;

        executionHandle = executionHandles.get(operationIid);
        if (executionHandle != null)
        {
            // Only an operation that is still running can be canceled.
            if (executionHandle.getOperationStatus() == T8ServerOperationStatus.IN_PROGRESS)
            {
                T8ServerOperationNotificationHandle notificationHandle;

                notificationHandle = executionHandle.getOperationStatusReport(context, false).getNotificationHandle();
                synchronized(notificationHandle)
                {
                    // Cancel the operation execution.
                    executionHandle.cancelOperationExecution(context);

                    // Wait for the executing operation to setEnabled processing.
                    while (executionHandle.getOperationStatus() != T8ServerOperationStatus.CANCELLED)
                    {
                        notificationHandle.wait(1000);
                    }
                }
            }

            // Return a status report of the stopped operation.
            return executionHandle.getOperationStatusReport(context, true);
        }
        else throw new Exception("Operation Execution Handle '" + operationIid + "' not found.");
    }

    /**
     * This method instructs a specific operation that is currently executing to
     * pause its execution as soon as possible.  The method blocks until the
     * operation in question has stopped executing.
     *
     * @param context
     * @param operationIid
     * @return The T8ServerOperationStatusReport obtained from the specified operation
     * upon termination.
     * @throws Exception
     */
    @SuppressWarnings("SleepWhileHoldingLock")
    @Override
    public T8ServerOperationStatusReport pauseOperation(T8Context context, String operationIid) throws Exception
    {
        OperationExecutionHandle executionHandle;

        executionHandle = executionHandles.get(operationIid);
        if (executionHandle != null)
        {
            // Only an operation that is still running can be paused.
            if (executionHandle.getOperationStatus() == T8ServerOperationStatus.IN_PROGRESS)
            {
                // Cancel the operation execution.
                executionHandle.pauseOperationExecution(context);

                // Wait for the executing operation to setEnabled processing.
                while (executionHandle.getOperationStatus() != T8ServerOperationStatus.PAUSED)
                {
                    Thread.sleep(200);
                }
            }

            // Return a status report of the stopped operation.
            return executionHandle.getOperationStatusReport(context, true);
        }
        else throw new Exception("Operation Execution Handle '" + operationIid + "' not found.");
    }

    @Override
    public T8ServerOperationStatusReport resumeOperation(T8Context context, String operationIid) throws Exception
    {
        OperationExecutionHandle executionHandle;

        executionHandle = executionHandles.get(operationIid);
        if (executionHandle != null)
        {
            // Only an operation that is still running can be paused.
            if (executionHandle.getOperationStatus() == T8ServerOperationStatus.PAUSED)
            {
                // Cancel the operation execution.
                executionHandle.resumeOperationExecution(context);

                // Wait for the executing operation to setEnabled processing.
                while (executionHandle.getOperationStatus() != T8ServerOperationStatus.IN_PROGRESS)
                {
                    Thread.sleep(200);
                }
            }

            // Return a status report of the stopped operation.
            return executionHandle.getOperationStatusReport(context, true);
        }
        else throw new Exception("Operation Execution Handle '" + operationIid + "' not found.");
    }

    @Override
    public T8ServerOperationStatusReport executeAsynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        T8ServerOperationDefinition operationDefinition;

        // Get the operation definition.
        operationDefinition = getOperationDefinition(context, operationId);
        if (operationDefinition != null)
        {
            OperationExecutionHandle executionHandle;
            T8ServerOperationStatusReport statusReport;

            // Create an execution handle and execute the operation synchronously.
            executionHandle = new OperationExecutionHandle(this, operationDefinition, T8IdentifierUtilities.createNewGUID());
            executionHandles.put(executionHandle.getOperationGUID(), executionHandle); // The execution handle remains in the map during execution of the operation.
            statusReport = executionHandle.executeOperationAsynchronously(context, operationParameters);
            return statusReport;
        }
        else throw new Exception("Operation Definition not found: '" + operationId + "'.");
    }

    @Override
    public Map<String, Object> executeSynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        return this.executeSynchronousOperation(context, T8IdentifierUtilities.createNewGUID(), operationId, operationParameters);
    }

    @Override
    public String queueOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        T8QueuedOperation queuedOperation;

        // Create a new queued operation instance.
        queuedOperation = new T8QueuedOperation(T8IdentifierUtilities.createNewGUID(), operationId);
        queuedOperation.setParameters(operationParameters);
        queuedOperation.setStatus(T8QueuedOperation.T8QueuedOperationStatus.QUEUED);
        queuedOperation.setTimeQueued(new T8Timestamp(System.currentTimeMillis()));
        queuedOperation.setAgentId(context.getSystemAgentId());
        queuedOperation.setAgentIid(context.getSystemAgentIid());
        queuedOperation.setRootOrgId(context.getRootOrganizationId());
        queuedOperation.setOrgId(context.getOrganizationId());

        // Save the queued operation to the queue table.
        LOGGER.log("Queuing operation: " + queuedOperation);
        operationQueueDataHandler.insertQueuedOperation(queuedOperation);

        // Return the queued operation's iid so that clients can continue to track the status of the operation.
        return queuedOperation.getOperationIid();
    }

    Map<String, Object> executeSynchronousOperation(T8Context context, String operationIid, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        T8ServerOperationDefinition operationDefinition;

        // Get the operation definition.
        operationDefinition = getOperationDefinition(context, operationId);
        if (operationDefinition != null)
        {
            OperationExecutionHandle executionHandle;

            // Create an execution handle.
            executionHandle = new OperationExecutionHandle(this, operationDefinition, operationIid);
            executionHandles.put(executionHandle.getOperationGUID(), executionHandle); // The execution handle remains in the map during execution of the operation.

            // Execute the operation.
            try
            {
                // Execute the operation synchronously.
                return executionHandle.executeOperationSynchronously(context, operationParameters);
            }
            finally
            {
                executionHandles.remove(executionHandle.getOperationGUID()); // Remove the handle once the operation has completed.
            }
        }
        else throw new Exception("Operation Definition not found: '" + operationId + "'.");
    }

    @Override
    public List<T8ServerOperationStatusReport> getServerOperationStatusReports(T8Context context)
    {
        ArrayList<T8ServerOperationStatusReport> operationStatuses;
        TreeSet<String> executingOperations;

        // Get an T8ServerOperationStatusReport of all currently executing operations.
        operationStatuses = new ArrayList<>();
        executingOperations = new TreeSet<>(executionHandles.keySet());
        for (String operationIid : executingOperations)
        {
            OperationExecutionHandle executionHandle;

            executionHandle = executionHandles.get(operationIid);
            if (executionHandle != null) operationStatuses.add(executionHandle.getOperationStatusReport(context, false)); // Don't include the progress report, since we don't want this monitoring process to interfere with the normal execution of the operation.
        }

        return operationStatuses;
    }

    public T8HeartbeatResponse heartbeat(T8Context context) throws Exception
    {
        T8HeartbeatResponse response;
        T8SessionDetails sessionDetails;

        sessionDetails = securityManager.getSessionDetails(context);

        // If the user has not logged in yet the security manager will return null
        if (sessionDetails == null) return null;

        response = new T8HeartbeatResponse();
        response.setLastActivityTime(sessionDetails.getLastActivityTime());
        response.setExpirationTime(sessionDetails.getExpirationTime());
        response.setServerTime(System.currentTimeMillis());
        response.setServerLoad(serverLoad);
        response.setServerRequestsPerSecond(requestsPerSecond);
        response.setMaximumConcurrency(maxConcurrentRequests);
        response.setHasPendingNotifications(notificationManager.hasNewNotifications(context));
        response.setTaskCount(flowManager.getTaskCount(context, null));
        response.setReportCount(reportManager.countReports(context, null, null));
        response.setProcessCount(processManager.countProcesses(context, null, null));
        return response;
    }

    public T8MainServerRuntimeData getRuntimeData()
    {
        T8MainServerRuntimeData runtimeData;
        HashMap<String, String> systemPropertyMap;
        Properties systemProperties;
        Runtime runtime;

        systemProperties = System.getProperties();
        systemPropertyMap = new HashMap<>();
        for (Object property : System.getProperties().keySet())
        {
            String propertyKey;

            propertyKey = property.toString();
            systemPropertyMap.put(propertyKey, systemProperties.getProperty(propertyKey));
        }

        runtime = Runtime.getRuntime();
        runtimeData = new T8MainServerRuntimeData();
        runtimeData.setFreeMemory(runtime.freeMemory());
        runtimeData.setTotalMemory(runtime.totalMemory());
        runtimeData.setMaximumMemory(runtime.maxMemory());
        runtimeData.setAvailableProcessors(runtime.availableProcessors());
        runtimeData.setSystemProperties(systemPropertyMap);
        runtimeData.setApplicationVersion(applicationVersion);
        runtimeData.setBuildTimestamp(buildTimestamp);
        runtimeData.setServerCodeVersions(T8CodeVersionScanner.getServerSideCodeVersions(serverContext));

        return runtimeData;
    }

    void fireOperationCompletedEvent(String operationInstanceIdentifier)
    {
        T8OperationCompletedEvent event;

        event = new T8OperationCompletedEvent(operationInstanceIdentifier);
        for (T8ServerEventListener listener : listeners.getListeners(T8ServerEventListener.class))
        {
            listener.operationCompleted(event);
        }
    }

    @Override
    public void addServerEventListener(T8ServerEventListener listener)
    {
        listeners.add(T8ServerEventListener.class, listener);
    }

    @Override
    public void removeServerEventListener(T8ServerEventListener listener)
    {
        listeners.remove(T8ServerEventListener.class, listener);
    }

    /**
     * This listener reloads the cached operation definitions held by the server
     * as soon as the underlying definition data cache is reloaded.
     */
    private class DefinitionManagerListener extends T8DefinitionManagerEventAdapter
    {
        @Override
        public void definitionCacheReloaded(T8DefinitionCacheReloadedEvent tdcre)
        {
            cacheOperationDefinitions();
        }

        @Override
        public void definitionSaved(T8DefinitionSavedEvent event)
        {
            T8Definition eventDefinition;

            eventDefinition = event.getDefinition();
            if (eventDefinition instanceof T8ServerOperationDefinition)
            {
                T8ServerOperationDefinition definition;

                definition = (T8ServerOperationDefinition)eventDefinition;

                try
                {
                    // Even though we have the newly saved definition that we got from the event,
                    // we need a copy that has also been initialized and can be cached without exposure to other listener of the event.
                    // Therefore, a new initialized copy of the definition is retrieved for caching purposes.
                    definition = definitionManager.getInitializedDefinition(internalContext, definition.getProjectIdentifier(), definition.getIdentifier(), null);
                    cacheOperationDefinition(definition);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while caching operation definition: " + definition, e);
                }
            }
            else if (eventDefinition instanceof T8HttpApiDefinition)
            {
                try
                {
                    cacheHttpApi((T8HttpApiDefinition)eventDefinition);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while caching http api definition: " + eventDefinition, e);
                }
            }
        }
    }

    /**
     * This class is a task that is scheduled using a timer object and is used
     * to run regular maintenance checks on the server such as the discarding of
     * expired operations.
     */
    private static class ServerMaintenanceThread extends Thread
    {
        private Map<String, OperationExecutionHandle> executionHandles;
        private final long operationExpirationTime;

        private ServerMaintenanceThread(Map<String, OperationExecutionHandle> executionHandles, long operationExpirationTime)
        {
            this.executionHandles = executionHandles;
            this.operationExpirationTime = operationExpirationTime;
        }

        @Override
        public void destroy()
        {
            executionHandles = null;
        }

        @Override
        public void run()
        {
            Map<String, OperationExecutionHandle> copiedExecutionHandles;
            ArrayList<String> expiredOperations;
            long currentTime;

            // Update performance statistics.
            requestsPerSecond = ((double)requestsDuringInterval/MAINTENANCE_INTERVAL * 1000.00);
            requestsDuringInterval = 0;
            maxConcurrentRequests = 0;

            // Determine the server load.
            serverLoad = checkServerLoad();

            // Check for any expired operations;
            currentTime = System.currentTimeMillis();
            expiredOperations = new ArrayList<>();

            // Copy the execution handles, so that we don't slow down the server when synchronizing on the collection.
            synchronized (executionHandles)
            {
                copiedExecutionHandles = new HashMap<>(executionHandles);
            }

            // Check the open execution handles and remove any operations that have been idle for longer than the expiration time.
            for (OperationExecutionHandle executionHandle : copiedExecutionHandles.values())
            {
                long executionEndTime;

                executionEndTime = executionHandle.getExecutionEndTime();
                if (executionEndTime > 0) // If the end time is valid.
                {
                    if ((currentTime - executionEndTime) > operationExpirationTime)
                    {
                        expiredOperations.add(executionHandle.getOperationGUID());
                    }
                }
            }

            // If there are any expired operations, remove them.
            if (expiredOperations.size() > 0)
            {
                LOGGER.log("Removing " + expiredOperations.size() + " expired operations.");
                for (String operationIid : expiredOperations)
                {
                    executionHandles.remove(operationIid);
                }
            }
        }
    }

    private void checkJarLastModifiedDates()
    {
        try
        {
            File serverPersistenceFile = new File(contextPath + "/system/serverJarHashInfo.jhi");
            if(serverPersistenceFile.exists() || serverPersistenceFile.createNewFile())
            {
                File serverJarsPath = new File(getRealPath("WEB-INF/lib"));
                FileModifiedChecker fileChecker = new FileModifiedChecker(serverPersistenceFile);
                fileChecker.checkFiles(serverJarsPath);
            }
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to set last modified date for jars");
            LOGGER.log(ex.getMessage());
        }
    }

    private static double checkServerLoad()
    {
        JavaSysMon sysMon;
        CpuTimes lastCPUTimes;

        // Determine the server load..
        sysMon = new JavaSysMon();
        lastCPUTimes = sysMon.cpuTimes();
        if (cpuTimes == null)
        {
            cpuTimes = lastCPUTimes;
            return 0.0;
        }
        else
        {
            double load;

            load = lastCPUTimes.getCpuUsage(cpuTimes);
            cpuTimes = lastCPUTimes;
            return load * 100.00;
        }
    }
}
