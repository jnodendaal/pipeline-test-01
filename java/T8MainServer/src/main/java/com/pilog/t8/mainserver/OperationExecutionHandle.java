package com.pilog.t8.mainserver;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.T8ServerOperation.T8ServerOperationStatus;
import com.pilog.t8.T8ServerOperationNotificationHandle;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.exception.T8ServerOperationException;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.thread.T8ContextRunnable;

/**
 * This class servers as a handle for each operation that is executed on the
 * server.  It maintains statistics information on the operation it wraps and
 * takes care of multi-threading in the case of asynchronous operations.
 *
 * @author Bouwer du Preez
 */
public class OperationExecutionHandle
{
    private static final T8Logger LOGGER = T8Log.getLogger(OperationExecutionHandle.class);

    private final T8MainServer mainServer;
    private final String operationId;
    private final String operationIid;
    private long executionStartTime;
    private long executionEndTime;
    private Thread executionThread;
    private Throwable operationThrowable;
    private Map<String, Object> operationResult;
    private final T8ServerContext serverContext;
    private T8ServerOperation operation;
    private volatile T8ServerOperationStatus operationStatus;
    private final T8ServerOperationDefinition operationDefinition;
    private final T8ServerOperationNotificationHandle notificationHandle;

    /**
     * Constructor for the OperationExecutionHandle object.
     *
     * @param mainServer
     * @param operationDefinition The definition of the operation that will
     * be wrapped by this handle.
     * @param operationIid The instance id of the operation.
     */
    public OperationExecutionHandle(T8MainServer mainServer, T8ServerOperationDefinition operationDefinition, String operationIid)
    {
        this.mainServer = mainServer;
        this.serverContext = mainServer.getServerContext();
        this.operationId = operationDefinition.getIdentifier();
        this.operationIid = operationIid;
        this.operationDefinition = operationDefinition;
        this.operationStatus = T8ServerOperationStatus.NOT_STARTED;
        this.executionEndTime = -1;
        this.notificationHandle = new T8ServerOperationNotificationHandle(operationIid);
    }

    public T8ServerOperationDefinition getOperationDefinition()
    {
        return operationDefinition;
    }

    public synchronized long getExecutionEndTime()
    {
        return executionEndTime;
    }

    public synchronized long getExecutionStartTime()
    {
        return executionStartTime;
    }

    public synchronized T8ServerOperation getOperation()
    {
        return operation;
    }

    public synchronized String getOperationGUID()
    {
        return operationIid;
    }

    public synchronized T8ServerOperationStatus getOperationStatus()
    {
        return operationStatus;
    }

    public synchronized Map<String, Object> getOperationResult()
    {
        return operationResult;
    }

    public synchronized Throwable getOperationThrowable()
    {
        return operationThrowable;
    }

    private void notifyWaitingThreads()
    {
        synchronized (notificationHandle)
        {
            notificationHandle.setOperationStatus(operationStatus);
            notificationHandle.notifyAll();
        }
    }

    /**
     * Returns the thread that is currently executing the server operation.
     * The returned value will be null if no operation is executing.
     *
     * @return The thread currently is use to execute a server operation.
     */
    public Thread getExecutionThread()
    {
        return executionThread;
    }

    /**
     * Compiles and returns the status report of the operation wrapped by this
     * handle.
     *
     * @param context
     * @param includeProgressReport Boolean value indicating whether or not a
     * progress report must be extracted from the underlying operation and
     * included in the returned status report.
     * @return The status report compiled from the wrapped operation.
     */
    public synchronized T8ServerOperationStatusReport getOperationStatusReport(T8Context context, boolean includeProgressReport)
    {
        T8ServerOperationStatusReport statusReport;

        statusReport = new T8ServerOperationStatusReport();
        statusReport.setOperationIdentifier(operationDefinition.getIdentifier());
        statusReport.setOperationInstanceIdentifier(operationIid);
        statusReport.setOperationProgress(operation.getProgress(context));
        statusReport.setOperationStatus(operationStatus);
        statusReport.setExecutionStartTime(executionStartTime);
        statusReport.setExecutionEndTime(executionEndTime);
        statusReport.setProgressReportObject(operation.getProgressReport(context)); // This is a potentially heavy operation, so don't do it inless necessarry.
        statusReport.setOperationResult(operationResult);
        statusReport.setOperationThrowable(operationThrowable);
        statusReport.setNotificationHandle(notificationHandle);
        return statusReport;
    }

    /**
     * Executes the wrapped operation synchronously, blocking until the
     * operation completes.  The result of the operation is returned.
     *
     * @param context The access context within which this operation
     * executes.
     * @param operationParameters The parameter to pass to the operation for
     * execution.
     * @return The result of the completed operation.
     * @throws Exception
     */
    public Map<String, Object> executeOperationSynchronously(T8Context context, Map<String, Object> operationParameters) throws Exception
    {
        String threadName; // Used to store the original thread name.
        T8DataTransaction tx = null;
        boolean externalTransaction = false;

        // Set the access context.
        context.setOperationId(operationId);
        context.setOperationIid(operationIid);

        // If the current access context does not specify a project context, use the project of the operation being executed.
        if (context.getProjectId() == null) context.setProjectId(operationDefinition.getRootProjectId());

        // Save the current thread name so it can be used later.
        executionThread = Thread.currentThread();
        threadName = Thread.currentThread().getName();
        executionThread.setName(operationDefinition.getIdentifier()+ ":" + operationIid);

        // Log the start of the operation.
        context.getSessionContext().getPerformanceStatistics().logExecutionStart(operationId);

        try
        {
            // Set the transaction to use for operation execution.
            if (operationDefinition.isContainerManagedTransaction())
            {
                T8DataSession dataSession;

                // First check for an external transaction.  If one is present, use it or else create a new one.
                dataSession = serverContext.getDataManager().getCurrentSession();
                tx = dataSession.getTransaction();
                if (tx == null)
                {
                    tx = dataSession.beginTransaction();
                    externalTransaction = false;
                }
                else
                {
                    externalTransaction = true;
                }
            }

            // Create a new instance of the operation from the definition.
            operation = operationDefinition.getNewServerOperationInstance(context, operationIid);
            operation.init();

            // If the operation is null, it means something is wrong with the setup
            if (operation == null) throw new IllegalStateException("Attempted access to invalid operation. 1. Check operation name. 2. Check operation class.");

            // Execute the operation synchronously.
            executionStartTime = System.currentTimeMillis();
            operationStatus = T8ServerOperationStatus.IN_PROGRESS;
            operationResult = (Map<String, Object>)operation.execute(T8IdentifierUtilities.stripNamespace(operationDefinition.getNamespace(), operationParameters, false), T8ServerOperationExecutionType.SYNCHRONOUS);

            // Process the operation status.
            switch (operationStatus)
            {
                case IN_PROGRESS:
                    operationStatus = T8ServerOperationStatus.COMPLETED;
                    break;
                case STOPPING:
                    operationStatus = T8ServerOperationStatus.STOPPED;
                    break;
                case CANCELLING:
                    operationStatus = T8ServerOperationStatus.CANCELLED;
                    break;
                default:
                    throw new RuntimeException("Invalid operation status encountered: " + operationStatus);
            }

            // Commit the transaction.
            if ((!externalTransaction) && (tx != null) && (tx.isOpen())) tx.commit();

            // Return the result of the executed operation.
            return operationResult;
        }
        catch (Throwable e)
        {
            LOGGER.log(e);
            operationStatus = T8ServerOperationStatus.FAILED;
            operationResult = null;
            operationThrowable = e;

            // Rollback the transaction.
            if ((!externalTransaction) && (tx != null) && (tx.isOpen())) tx.rollback();
            throw new T8ServerOperationException(context.getSessionContext(), "Exception while executing operation: " + operationIid + " (" + operationDefinition + ") using parameters: " + operationParameters, e);
        }
        finally
        {
            context.getSessionContext().getPerformanceStatistics().logExecutionEnd(operationId);
            executionEndTime = System.currentTimeMillis();
            executionThread.setName(threadName); // Reset the original thread name.
            executionThread = null; // The thread will end after this call, so set the variable to null.
            notifyWaitingThreads();
            mainServer.fireOperationCompletedEvent(operationIid);
        }
    }

    /**
     * Executes the wrapped operation asynchronously.  An T8ServerOperationStatusReport
     * is return immediately upon invocation of this method while the execution
     * of the operation continues in a separate thread.
     *
     * @param context The context of execution.
     * @param operationParameters The parameters to pass to the operation for
     * execution.
     * @return The T8ServerOperationStatusReport obtained from the wrapped operation as
     * processing is started.  This report will contain the operation GUID that
     * can then be used by client applications to reference the operation
     * handle.
     * @throws Exception
     */
    public T8ServerOperationStatusReport executeOperationAsynchronously(T8Context context, final Map<String, Object> operationParameters) throws Exception
    {
        // Set the access context.
        context.setOperationId(operationId);
        context.setOperationIid(operationIid);

        // If the current access context does not specify a project context, use the project of the operation being executed.
        if (context.getProjectId() == null) context.setProjectId(operationDefinition.getRootProjectId());

        // Create a new instance of the operation from the definition.
        operation = operationDefinition.getNewServerOperationInstance(context, operationIid);

        // Execute the operation asynchronously.
        operationStatus = T8ServerOperationStatus.IN_PROGRESS;
        executionThread = new Thread(new OperationExecutor(context, T8IdentifierUtilities.stripNamespace(operationDefinition.getNamespace(), operationParameters, false)));
        executionThread.start();
        notifyWaitingThreads();

        // Return the status of the executing operation.
        return getOperationStatusReport(context, true);
    }

    /**
     * This method instructs the wrapped operation that is currently executing
     * to stop its execution as soon as possible.
     *
     * @param context The current session context.
     * @throws Exception
     */
    public void stopOperationExecution(T8Context context) throws Exception
    {
        operation.stop(context);
        operationStatus = T8ServerOperationStatus.STOPPING;
    }

    /**
     * This method instructs the wrapped operation that is currently executing
     * to cancel its execution as soon as possible.
     *
     * @param context The current session context.
     * @throws Exception
     */
    public void cancelOperationExecution(T8Context context) throws Exception
    {
        operation.cancel(context);
        operationStatus = T8ServerOperationStatus.CANCELLING;
    }

    /**
     * This method instructs the wrapped operation that is currently executing
     * to pause its execution as soon as possible.
     *
     * @param context The current session context.
     * @throws Exception
     */
    public void pauseOperationExecution(T8Context context) throws Exception
    {
        operation.pause(context);
        operationStatus = T8ServerOperationStatus.PAUSING;
    }

    /**
     * This method instructs the wrapped operation that is currently paused to
     * resume its execution.
     *
     * @param context The current session context.
     * @throws Exception
     */
    public void resumeOperationExecution(T8Context context) throws Exception
    {
        operation.resume(context);
        operationStatus = T8ServerOperationStatus.IN_PROGRESS;
    }

    /**
     * This class executes the wrapped operation in a separate thread.
     */
    private class OperationExecutor extends T8ContextRunnable
    {
        private final Map<String, Object> operationParameters;

        private OperationExecutor(T8Context context, Map<String, Object> operationParameters)
        {
            super(context, operationId + ":" + operationIid);
            this.operationParameters = operationParameters;
        }

        @Override
        public void exec()
        {
            T8DataTransaction tx = null;

            try
            {
                // Log the start of the operation.
                context.getSessionContext().getPerformanceStatistics().logExecutionStart(operationId);

                // Execute the operation.
                executionStartTime = System.currentTimeMillis();
                if (operationDefinition.isContainerManagedTransaction()) tx = this.serverContext.getDataManager().getCurrentSession().beginTransaction();
                operation.init();
                operationResult = (Map<String, Object>)operation.execute(operationParameters, T8ServerOperationExecutionType.ASYNCHRONOUS);

                // Commit the transaction.
                if ((tx != null) && (tx.isOpen())) tx.commit();

                // Process the operation status.
                switch (operationStatus)
                {
                    case IN_PROGRESS:
                        operationStatus = T8ServerOperationStatus.COMPLETED;
                        break;
                    case STOPPING:
                        operationStatus = T8ServerOperationStatus.STOPPED;
                        break;
                    case CANCELLING:
                        operationStatus = T8ServerOperationStatus.CANCELLED;
                        break;
                    default:
                        throw new RuntimeException("Invalid operation status encountered: " + operationStatus);
                }
            }
            catch (Throwable e)
            {
                LOGGER.log(e);
                operationStatus = T8ServerOperationStatus.FAILED;
                operationThrowable = e;
                operationResult = null;

                // Rollback the transaction.
                if ((tx != null) && (tx.isOpen())) tx.rollback();
            }
            finally
            {
                // Record the details of the execution completion.
                sessionContext.getPerformanceStatistics().logExecutionEnd(operationId);
                executionEndTime = System.currentTimeMillis();
                executionThread = null; // The thread will end after this call, so set the variable to null.

                // Notify listers of execution completion.
                try
                {
                    notifyWaitingThreads();
                    mainServer.fireOperationCompletedEvent(operationIid);
                }
                catch (Throwable finalExceptino)
                {
                    LOGGER.log("Exception while finalizing operation execution thread.  Operation id:" + operationId + " Operation iid:" + operationIid, finalExceptino);
                }
            }
        }
    }
}
