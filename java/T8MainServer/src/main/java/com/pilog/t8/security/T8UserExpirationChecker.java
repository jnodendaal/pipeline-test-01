package com.pilog.t8.security;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.user.T8DeveloperUserDefinition;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.thread.T8ContextRunnable;
import java.util.List;
import org.joda.time.DateTime;

/**
 * A runnable task which is responsible for the deactivation of users for which
 * the user expiration time has been reached.
 *
 * @author Gavin Boshoff
 */
class T8UserExpirationChecker extends T8ContextRunnable
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8UserExpirationChecker.class);

    T8UserExpirationChecker(T8Context context)
    {
        super(context, "UEXC-UserExpirationChecker");
    }

    @Override
    public void exec()
    {
        try
        {
            List<T8UserDefinition> userDefinitions;
            T8DefinitionManager definitionManager;

            // Get the definition manager to save modified users
            definitionManager = this.serverContext.getDefinitionManager();

            // Retrieve all users
            userDefinitions = definitionManager.getRawGroupDefinitions(context, null, T8UserDefinition.GROUP_IDENTIFIER);
            for (T8UserDefinition userDefinition : userDefinitions)
            {
                // Check if developer user, then skip
                if (userDefinition instanceof T8DeveloperUserDefinition) continue;

                // Check user expiry date and set user to inactive
                if (userDefinition.getUserExpiryDate() != null && userDefinition.isActive() && DateTime.now().isAfter(userDefinition.getUserExpiryDate().getMilliseconds()))
                {
                    userDefinition.setActive(false);
                    definitionManager.saveDefinition(context, userDefinition, null);
                }
            }
        }
        catch (Exception ex)
        {
            LOGGER.log("Failed to complete the user expiration checker task. " + ex.getMessage(), ex);
        }
    }
}