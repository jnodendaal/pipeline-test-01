/**
 * Created on 16 July 2015, 8:38:29 AM
 */
package com.pilog.t8.security;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * A very simple password generator which will generate a random, 130-bit
 * alpha-numeric value to server as a password of some type.
 *
 * @author Gavin Boshoff
 */
public final class T8RandomPasswGenerator
{
    public static String generate()
    {
        return T8RandomPasswGeneratorHolder.INSTANCE.next();
    }

    private final SecureRandom secureRandom = new SecureRandom();

    private T8RandomPasswGenerator() {}

    /**
     * Synchronized singleton method which retrieves the next random value.
     * This is an additional precaution to ensure that the values generated
     * are unique.
     *
     * @return The random {@code String} value generated
     */
    private synchronized String next()
    {
        return new BigInteger(130, secureRandom).toString(32);
    }

    /**
     * The holder of the singleton.
     */
    private static class T8RandomPasswGeneratorHolder
    {
        private static final T8RandomPasswGenerator INSTANCE = new T8RandomPasswGenerator();
        private T8RandomPasswGeneratorHolder() {}
    }
}
