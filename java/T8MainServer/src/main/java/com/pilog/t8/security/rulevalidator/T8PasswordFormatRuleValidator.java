/**
 * Created on Jul 2, 2015, 11:18:18 AM
 */
package com.pilog.t8.security.rulevalidator;

import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.definition.user.password.T8PasswordFormatRuleDefinition;
import com.pilog.t8.definition.user.password.T8PasswordRuleValidator;

/**
 * @author Gavin Boshoff
 */
public class T8PasswordFormatRuleValidator implements T8PasswordRuleValidator
{
    private final T8PasswordFormatRuleDefinition definition;

    public T8PasswordFormatRuleValidator(T8PasswordFormatRuleDefinition definition)
    {
        this.definition = definition;
    }

    @Override
    public boolean validate(T8UserDefinition userDefinition, char[] newPassw, String newHash)
    {
        // TODO : GBO - Whenever a password is changed to a String, it is VERY BAD!!!
        // Strings cannot be cleared from memory, until JVM garbages them, which is
        // very low priority when it comes to String caching. char[] can be overwritten.
        return new String(newPassw).matches(this.definition.getRegularExpression());
    }
}