package com.pilog.t8.mainserver;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8OperationContext;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.thread.T8ContextRunnable;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8OperationQueueProcessor extends T8ContextRunnable
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8OperationQueueProcessor.class);
    private final T8OperationQueueDataHandler dataHandler;
    private final T8MainServer mainServer;
    private volatile boolean enabled;

    private static final int BATCH_SIZE = 20; // The number of queued operations that will be retrieved at a time, when the queue is processed.

    public T8OperationQueueProcessor(T8Context context, T8MainServer mainServer, T8OperationQueueDataHandler dataHandler, boolean enabled) throws Exception
    {
        super(context, "OperationQueueProcessor");
        this.mainServer = mainServer;
        this.dataHandler = dataHandler;
        this.enabled = enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    @Override
    public void exec()
    {
        try
        {
            List<T8QueuedOperation> queuedOperations;

            // Retrieve a batch of queued operations.
            queuedOperations = dataHandler.retrieveQueuedOperations(BATCH_SIZE);
            while ((!queuedOperations.isEmpty()) && (enabled))
            {
                // Iterate over each operation in the batch and execute each one sequentially.
                for (T8QueuedOperation queuedOperation : queuedOperations)
                {
                    // Break the loop if the stopped flag has been set.
                    if (!enabled) break;
                    else
                    {
                        try
                        {
                            OperationThreadExceptionHandler exceptionHandler;
                            QueuedOperationRunnable operationRunnable;
                            T8Context operationContext;
                            Thread operationThread;

                            // Update the queued operation's status to reflect the fact that it has been processed (read from the queue).
                            queuedOperation.setStatus(T8QueuedOperation.T8QueuedOperationStatus.PROCESSED);
                            queuedOperation.setTimeProcessed(new T8Timestamp(System.currentTimeMillis()));
                            dataHandler.updateQueuedOperation(queuedOperation);

                            // Create a session context to be used for the operation execution.
                            operationContext = new T8OperationContext(context, queuedOperation.getOrgId(), queuedOperation.getAgentId(), queuedOperation.getAgentIid());

                            // Wrap the execution of the operation in a T8ContextRunnable to allow a new accesscontext to be create.
                            operationRunnable = new QueuedOperationRunnable(operationContext, mainServer, queuedOperation);
                            operationThread = new Thread(operationRunnable);
                            exceptionHandler = new OperationThreadExceptionHandler(context, dataHandler, queuedOperation);
                            operationThread.setUncaughtExceptionHandler(exceptionHandler);

                            // Star the operation thread and wait for it to finish execution.
                            operationThread.start();
                            operationThread.join();

                            // Remote the queued operation from the queue if it has successfully completed.
                            if (operationRunnable.isSuccess())
                            {
                                dataHandler.deleteQueuedOperation(queuedOperation.getOperationIid());
                            }
                        }
                        catch (Throwable e)
                        {
                            queuedOperation.setStatus(T8QueuedOperation.T8QueuedOperationStatus.FAILED);
                            queuedOperation.setInformation(ExceptionUtilities.getStackTraceString(e, -1, -1));
                            dataHandler.updateQueuedOperation(queuedOperation);
                        }
                    }
                }

                // Retrieve the next batch of queued operations.
                queuedOperations = dataHandler.retrieveQueuedOperations(BATCH_SIZE);
            }
        }
        catch (Throwable e)
        {
            LOGGER.log("Exception while processing operation queue.", e);
        }
    }

    private static class QueuedOperationRunnable extends T8ContextRunnable
    {
        private final T8QueuedOperation queuedOperation;
        private final T8MainServer mainServer;
        private boolean success;

        private QueuedOperationRunnable(T8Context context, T8MainServer mainServer, T8QueuedOperation queuedOperation) throws Exception
        {
            super(context, "QueuedOperationRunnable");
            this.mainServer = mainServer;
            this.queuedOperation = queuedOperation;
            this.success = false;
        }

        public boolean isSuccess()
        {
            return success;
        }

        @Override
        public void exec()
        {
            try
            {
                LOGGER.log("Executing operation: " + queuedOperation);
                mainServer.executeSynchronousOperation(context, queuedOperation.getOperationIid(), queuedOperation.getOperationId(), queuedOperation.getParameters());
                success = true;
            }
            catch (Throwable e)
            {
                LOGGER.log("Exception while executing queue operation " + queuedOperation, e);
                success = false;
                throw new RuntimeException(e);
            }
        }
    }

    private static class OperationThreadExceptionHandler extends T8ContextRunnable implements UncaughtExceptionHandler
    {
        private final T8OperationQueueDataHandler dataHandler;
        private final T8QueuedOperation queuedOperation;
        private Throwable throwable;

        public OperationThreadExceptionHandler(T8Context context, T8OperationQueueDataHandler dataHandler, T8QueuedOperation queuedOperation)
        {
            super(context, "OperationThreadExceptionHandler");
            this.dataHandler = dataHandler;
            this.queuedOperation = queuedOperation;
        }

        @Override
        public void exec()
        {
            try
            {
                queuedOperation.setStatus(T8QueuedOperation.T8QueuedOperationStatus.FAILED);
                queuedOperation.setInformation(ExceptionUtilities.getStackTraceString(throwable, -1, -1));
                dataHandler.updateQueuedOperation(queuedOperation);
            }
            catch (Throwable ue)
            {
                LOGGER.log("Critical exception while attempting to log queued operation failure: " + queuedOperation, ue);
            }
        }

        @Override
        public void uncaughtException(Thread t, Throwable e)
        {
            this.throwable = e;
            run();
        }
    }
}
