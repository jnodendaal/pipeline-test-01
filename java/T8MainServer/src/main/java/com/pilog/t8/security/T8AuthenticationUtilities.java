package com.pilog.t8.security;

import com.pilog.t8.utilities.codecs.Base64Codec;
import java.net.HttpURLConnection;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;

/**
 * @author Bouwer du Preez
 */
public class T8AuthenticationUtilities
{
    public static void addTokenAuthentication(HttpURLConnection connection, String token)
    {
        connection.setRequestProperty ("Authorization", "Token " + token);
    }

    public static void addBasicAuthentication(HttpURLConnection connection, String username, String password)
    {
        connection.setRequestProperty ("Authorization", createBasicAuthenticationHeader(username, password));
    }

    public static void addBasicAuthentication(SOAPMessage message, String username, String password)
    {
        String authorization;
        MimeHeaders headers;

        // Encode the authorization parameters.
        authorization = Base64Codec.encodeString(username + ":" + password);

        // Set the headers on the SOAP request.
        headers = message.getMimeHeaders();
        headers.addHeader("Authorization", "Basic " + authorization);
    }

    public static String createBasicAuthenticationHeader(String username, String password)
    {
        String authorization;

        // Encode the authorization parameters.
        authorization = Base64Codec.encodeString(username + ":" + password);
        return "Basic " + authorization;
    }
}
