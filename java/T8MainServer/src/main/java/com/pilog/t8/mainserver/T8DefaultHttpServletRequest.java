package com.pilog.t8.mainserver;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.http.T8HttpServletRequest;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultHttpServletRequest implements T8HttpServletRequest
{
    private final HttpServletRequest request;

    private static final String SESSION_CONTEXT_ATTRIBUTE_ID = "SESSION_CONTEXT";

    private static final String FUNCTIONALITY_IID_PARAMETER_ID = "functionality_iid";
    private static final String PROJECT_ID_PARAMETER_ID = "project_id";

    public T8DefaultHttpServletRequest(HttpServletRequest request)
    {
        this.request = request;
    }

    @Override
    public String getAuthorizationHeader()
    {
        return request.getHeader(AUTHORIZATION_HEADER);
    }

    @Override
    public T8SessionContext getSessionContext()
    {
        HttpSession session;

        // Get the session from the request.
        session = request.getSession(true);

        // Return the value from the session attributes where the session context is stored (if present).
        return (T8SessionContext)session.getAttribute(SESSION_CONTEXT_ATTRIBUTE_ID);
    }

    @Override
    public void setSessionContext(T8SessionContext sessionContext)
    {
        HttpSession session;

        // Get the session from the request and set the session context object on it.
        session = request.getSession(true);
        session.setAttribute(SESSION_CONTEXT_ATTRIBUTE_ID, sessionContext);
    }

    @Override
    public T8Context getAccessContext()
    {
        T8Context accessContext;

        accessContext = new T8Context((T8ServerContext)null, getSessionContext());
        accessContext.setFunctionalityIid(request.getParameter(FUNCTIONALITY_IID_PARAMETER_ID));
        accessContext.setProjectId(request.getParameter(PROJECT_ID_PARAMETER_ID));
        return accessContext;
    }

    @Override
    public Map<String, List<String>> getParameterMap()
    {
        Map<String, List<String>> result;
        Map<String, String[]> map;

        map = request.getParameterMap();
        result = new HashMap<>();
        if (map != null)
        {
            for (String key : map.keySet())
            {
                String[] value;

                value = map.get(key);
                if (value != null)
                {
                    result.put(key, Arrays.asList(value));
                }
                else
                {
                    result.put(key, new ArrayList<>());
                }
            }
        }

        return result;
    }
}
