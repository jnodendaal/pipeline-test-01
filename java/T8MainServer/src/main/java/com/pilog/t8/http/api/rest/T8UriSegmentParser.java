package com.pilog.t8.http.api.rest;

import com.pilog.t8.definition.http.api.rest.T8UriParserDefinition;
import com.pilog.t8.definition.http.api.rest.T8UriSegmentParserDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8UriSegmentParser implements T8UriParser
{
    private final T8UriSegmentParserDefinition definition;
    private final List<T8UriParser> subParsers;
    private final Pattern matchPattern;
    private final Pattern capturePattern;
    private final String captureParameterId;

    public T8UriSegmentParser(T8Context context, T8UriSegmentParserDefinition definition)
    {
        String captureRegex;
        String matchRegex;

        this.definition = definition;
        this.subParsers = new ArrayList<>();
        this.captureParameterId = definition.getCaptureParameterId();

        // Create the match pattern.
        matchRegex = definition.getMatchRegex();
        matchPattern = matchRegex != null ? Pattern.compile(matchRegex) : null;

        // Create the capture pattern.
        captureRegex = definition.getCaptureRegex();
        capturePattern = captureRegex != null ? Pattern.compile(captureRegex) : null;

        // Create the sub-parsers.
        for (T8UriParserDefinition subSegmentDefinition : definition.getSubParsers())
        {
            this.subParsers.add(subSegmentDefinition.getUriParser(context));
        }
    }

    @Override
    public boolean isApplicable(String uri)
    {
        String segment;

        segment = getNextSegment(uri);
        if ((segment != null) && (matchPattern != null))
        {
            return matchPattern.matcher(segment).matches();
        }
        else return false;
    }

    @Override
    public T8UriParseResult parseUri(T8UriParseResult precedingParseResult, String method, String uri)
    {
        T8UriParseResult result;
        String capturedValue;
        String segment;
        String subUri;

        // Create a new parse result to act as output from this parser.
        result = new T8UriParseResult(precedingParseResult);

        // Capture the value from the uri.
        segment = getNextSegment(uri);
        capturedValue = captureValue(segment);

        // Add the captured value to the result.
        if (captureParameterId != null) result.setParameter(definition.getCaptureParameterId(), capturedValue);

        // Process the sub-parsers.
        subUri = getSubUri(uri);
        if (subUri != null)
        {
            // Iterate over all available sub-parsers until one if found that is aplicable to the sub-uri.
            for (T8UriParser subParser : subParsers)
            {
                // Test applicability before using the parser.
                if (subParser.isApplicable(subUri))
                {
                    // Parse the sub-uri using the result from this parser as input.
                    result.add(subParser.parseUri(result, method, subUri));
                    return result;
                }
            }

            // Sub-uri not matched.
            result.setSuccess(false);
            return result;
        }
        else // So this was the last part of the uri.  Do the operation mapping.
        {
            String operationId;

            operationId = definition.getOperationId();
            result.setOperationId(operationId);
            result.setOperationInputParameterMapping(definition.getOperationInputParameterMapping());
            result.setOperationOutputParameterMapping(definition.getOperationOutputParameterMapping());
            result.setSuccess(true);
            return result;
        }
    }

    private String captureValue(String segment)
    {
        if ((segment != null) && (capturePattern != null))
        {
            Matcher matcher;

            matcher = capturePattern.matcher(segment);
            if (matcher.find())
            {
                return matcher.group();
            }
            else return null;
        }
        else return null;
    }

    private String getNextSegment(String uri)
    {
        if (uri != null)
        {
            String segment;
            int endIndex;

            if (uri.startsWith("/"))
            {
                segment = uri.substring(1);
            }
            else segment = uri;

            endIndex = segment.indexOf("/");
            if (endIndex > -1)
            {
                return segment.substring(0, endIndex);
            }
            else return segment;
        }
        else return null;
    }

    private String getSubUri(String uri)
    {
        if (uri != null)
        {
            int separatorIndex;

            if (uri.startsWith("/"))
            {
                // If the uri starts with a '/' look for the second one.
                separatorIndex = uri.indexOf("/", 1);
            }
            else
            {
                // The uri does not start with a '/' so just look for the first one.
                separatorIndex = uri.indexOf("/");
            }

            return ((separatorIndex > -1) && (uri.length() > separatorIndex + 1)) ? uri.substring(separatorIndex + 1) : null;
        }
        else return null;
    }
}
