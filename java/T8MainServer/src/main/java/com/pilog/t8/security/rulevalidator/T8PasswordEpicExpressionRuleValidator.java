/**
 * Created on Jul 2, 2015, 11:17:51 AM
 */
package com.pilog.t8.security.rulevalidator;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.definition.user.password.T8PasswordEpicExpressionRuleDefinition;
import com.pilog.t8.definition.user.password.T8PasswordRuleValidator;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8PasswordEpicExpressionRuleValidator implements T8PasswordRuleValidator
{
    private static final T8Logger logger = T8Log.getLogger(T8PasswordEpicExpressionRuleValidator.class);

    private final T8PasswordEpicExpressionRuleDefinition definition;

    public T8PasswordEpicExpressionRuleValidator(T8PasswordEpicExpressionRuleDefinition definition)
    {
        this.definition = definition;
    }

    @Override
    public boolean validate(T8UserDefinition userDefinition, char[] newPassw, String newHash)
    {
        try
        {
            ExpressionEvaluator expressionEvaluator;
            Map<String, Object> expressionParameters;

            expressionEvaluator = new ExpressionEvaluator();
            expressionParameters = userDefinition.getDefinitionDatums();
            // DO NOT create string from char[]!!
            expressionParameters.put("NEW_PASSWORD", new String(newPassw));
            expressionParameters.put("NEW_PASSWORD_ENCRYPTED", newHash);

            return expressionEvaluator.evaluateBooleanExpression(this.definition.getEpicExpression(), expressionParameters, null);
        }
        catch (EPICSyntaxException ex)
        {
            logger.log("Failed to evaluate expression " + this.definition.getEpicExpression(), ex);
        }
        catch (EPICRuntimeException ex)
        {
            logger.log("Failed to execute expression " + this.definition.getEpicExpression(), ex);
        }

        //This point is only reached if an exception occurs, which means the check should fail
        return false;
    }
}