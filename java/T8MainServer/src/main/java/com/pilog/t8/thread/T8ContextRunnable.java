package com.pilog.t8.thread;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 */
public abstract class T8ContextRunnable implements Runnable
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ContextRunnable.class);

    protected final T8SessionContext sessionContext;
    protected final T8ServerContext serverContext;
    private final String threadName;
    protected T8Context context;

    /**
     * Creates a wrapper for a context sensitive thread. This will ensure that
     * a data session is created for the thread in the correct manner.<br>
     * <br>
     * As an addition, the thread name can be specified for easier tracking of
     * the thread. If supplied, the original thread name will be restored once
     * execution has been completed. If not supplied, the thread name will
     * remain as assigned by the JVM.
     *
     * @param context The {@code T8Context} that needs to be associated with the thread when it is running.
     * @param threadName The {@code String} name of the thread to be set during execution. Can be {@code null}.
     */
    public T8ContextRunnable(T8Context context, String threadName)
    {
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.threadName = threadName;
    }

    @Override
    @SuppressWarnings("FinalMethod")
    public final void run()
    {
        T8SecurityManager securityManager;
        T8DataManager dataManager;
        String originalThreadName;

        // Store the original thread name so we can reset it after this runnable has finished execution.
        originalThreadName = Thread.currentThread().getName();

        // Assign the new thread name.
        Thread.currentThread().setName(threadName);

        // Gethte security manager to use for session creation.
        securityManager = serverContext.getSecurityManager();
        dataManager = serverContext.getDataManager();

        try
        {
            // Create the context and data session for this thread.
            context = securityManager.createContext(sessionContext);
            dataManager.openDataSession(context);

            // Now execute the actual work to be completed
            exec();
        }
        finally
        {
            // Close the data session and context associated with this thread.
            dataManager.closeCurrentSession();
            securityManager.destroyContext();

            // Reset the thread name to its original value
            Thread.currentThread().setName(originalThreadName);
        }
    }

    protected T8Context getContext()
    {
        return context;
    }

    public abstract void exec();
}