package com.pilog.t8.mainserver;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.log.T8Logger;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author Bouwer du Preez
 */
public class T8ClassScanner
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ClassScanner.class);

    private final T8ServerContext serverContext;
    private final String classPath;
    private Set<JarFile> jars;

    public static final String SERVER_SIDE_JAR_PATH = "/WEB-INF/lib";

    public T8ClassScanner(T8ServerContext serverContext, String classPath)
    {
        this.serverContext = serverContext;
        this.classPath = classPath;
    }

    private void cacheJarFiles()
    {
        jars = getAllJarFilesInClassPath(serverContext, classPath);
    }

    public List<File> getJarFiles()
    {
        return getJarFiles(serverContext.getRealPath(classPath));
    }

    public Set<Class> getPackageClasses(String packageName)
    {
        Set<Class> classes;

        // Find all available JAR files if this has not been done already.
        if (jars == null) cacheJarFiles();

        classes = new HashSet<>();
        for (JarFile jar : jars)
        {
            try
            {
                classes.addAll(getAllClassesInJarFile(jar, packageName.replace('.', '/')));
            }
            catch (Exception e)
            {
                LOGGER.log(e);
            }
        }

        return classes;
    }

    private static Set<Class<?>> getAllClassesInJarFile(JarFile jar, String packageName) throws Exception
    {
        Enumeration<JarEntry> enumeration;
        Set<Class<?>> classes;

        enumeration = jar.entries();
        classes = new HashSet<>();
        while (enumeration.hasMoreElements())
        {
            JarEntry jarEntry;

            jarEntry = enumeration.nextElement();
            if (jarEntry.getName().startsWith(packageName))
            {
                String className;

                className = jarEntry.getName();
                if (className.endsWith(".class"))
                {
                    className = className.substring(0, className.length() - ".class".length());
                    if (className.startsWith(packageName))
                    {
                        try
                        {
                            classes.add(Class.forName(className.replace('/', '.')));
                        }
                        catch (Throwable e)
                        {
                            LOGGER.log("Throwable while loading class: " + className, e);
                        }
                    }
                }
            }
        }

        return classes;
    }

    private static Set<JarFile> getAllJarFilesInClassPath(T8ServerContext serverContext, String classPath)
    {
        Set<JarFile> jarFiles;
        Set<String> jarPaths;

        // Add all server-side JAR files.
        jarPaths = getJarPaths(serverContext.getRealPath(classPath));

        jarFiles = new HashSet<>();
        for (String jarPath : jarPaths)
        {
            if (jarPath.toUpperCase().endsWith(".JAR"))
            {
                try
                {
                    jarFiles.add(new JarFile(jarPath));
                }
                catch (IOException e)
                {
                    LOGGER.log("Exception while opening JAR File: " + jarPath, e);
                }
            }
        }

        return jarFiles;
    }

    private static Set<String> getJarPaths(File directory)
    {
        Set<String> jarPaths;

        jarPaths = new HashSet<>();
        for (File jarFile : getJarFiles(directory))
        {
            jarPaths.add(jarFile.getAbsolutePath());
        }

        return jarPaths;
    }

    private static List<File> getJarFiles(File directory)
    {
        List<File> files;

        files = new ArrayList<>();
        for (File childFile : directory.listFiles())
        {
            if (".".equals(childFile.getName()) || "..".equals(childFile.getName()))
            {
                // Ignore.
            }
            else if (childFile.isDirectory())
            {
                files.addAll(getJarFiles(childFile));
            }
            else if (childFile.isFile())
            {
                files.add(childFile);
            }
        }

        return files;
    }
}
