package com.pilog.t8.ui.datarequirementeditor.ontologyview;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.ui.datarequirementeditor.view.tree.TransferableConceptTerminologyList;
import com.pilog.t8.ui.tree.T8Tree;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;

/**
 * @author Bouwer du Preez
 */
public class ConceptTreeTransferHandler extends TransferHandler
{
    public ConceptTreeTransferHandler()
    {
    }

    @Override
    protected Transferable createTransferable(JComponent c)
    {
        T8Tree ontologyTree;

        // Get the list panel (which is normally the direct parent of the list on which the drag event occurred).
        ontologyTree = (T8Tree)SwingUtilities.getAncestorOfClass(T8Tree.class, c);
        if (ontologyTree != null)
        {
            StringBuilder sb;
            List<T8DataEntity> selectedEntities;
            ArrayList<T8ConceptTerminology> terminologyList;

            selectedEntities = ontologyTree.getSelectedDataEntities();
            terminologyList = new ArrayList<T8ConceptTerminology>();
            sb = new StringBuilder();

            for (T8DataEntity selectedEntity : selectedEntities)
            {
                T8ConceptTerminology terminology;
                String conceptID;
                String conceptTypeID;
                String languageID;
                String term;
                String definition;
                String code;

                conceptID = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_CONCEPT_ID);
                conceptTypeID = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_CONCEPT_TYPE_ID);
                languageID = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_LANGUAGE_ID);
                term = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_TERM);
                definition = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_DEFINITION);
                code = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_CODE);

                terminology = new T8ConceptTerminology(T8OntologyConceptType.getConceptType(conceptTypeID), conceptID);
                terminology.setLanguageId(languageID);
                terminology.setTerm(term);
                terminology.setDefinition(definition);
                terminology.setCode(code);

                T8Log.log("Dragging Concept Terminology: " + terminology);
                terminologyList.add(terminology);
            }

            for (Iterator<T8ConceptTerminology> it = terminologyList.iterator(); it.hasNext();)
            {
                T8ConceptTerminology t8ConceptTerminology = it.next();
                sb.append(t8ConceptTerminology.getTerm());
                sb.append(": ");
                sb.append(t8ConceptTerminology.getConceptId());

                if(it.hasNext()) sb.append("\n");
            }

            return new TransferableConceptTerminologyList(terminologyList, sb.toString());
        }
        else return null;
    }

    @Override
    public int getSourceActions(JComponent c)
    {
        T8Tree parentTree;

        parentTree = (T8Tree)SwingUtilities.getAncestorOfClass(T8Tree.class, c);
	if (parentTree != null)
        {
	    return COPY;
	}
        else return NONE;
    }
}
