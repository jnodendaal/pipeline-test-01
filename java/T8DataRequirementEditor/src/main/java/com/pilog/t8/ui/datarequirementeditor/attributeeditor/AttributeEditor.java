package com.pilog.t8.ui.datarequirementeditor.attributeeditor;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.Requirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementAttribute.RequirementAttributeType;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datarequirementeditor.T8DataRequirementEditor;
import com.pilog.t8.utilities.strings.Strings;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;

/**
 * @author Bouwer du Preez
 */
public class AttributeEditor extends javax.swing.JPanel
{
    private final T8ConfigurationManager configurationManager;
    private final T8Context context;
    private final DefaultTableModel tableModel;
    private final AttributeTypeTableCellEditor editor;
    private final T8DataRequirementEditor parentEditor;
    private Requirement currentRequirement;

    public AttributeEditor(T8DataRequirementEditor parentEditor)
    {
        this.parentEditor = parentEditor;
        this.configurationManager = parentEditor.getController().getClientContext().getConfigurationManager();
        this.context = parentEditor.getController().getContext();

        // Construct the UI components.
        initComponents();
        this.tableModel = (DefaultTableModel)jTableAttributes.getModel();
        this.editor = new AttributeTypeTableCellEditor();
        this.jTableAttributes.getColumnModel().getColumn(1).setCellEditor(editor);
    }

    public void setSelectedRequirement(Requirement requirement)
    {
        // Save current changes first.
        saveChanges();

        // Clear the current table model.
        while (tableModel.getRowCount() > 0) tableModel.removeRow(0);

        // Set the new requirement.
        currentRequirement = requirement;
        if (currentRequirement != null)
        {
            Map<String, Object> attributes;

            attributes = getRequirementAttributeMap(currentRequirement);
            for (String attributeId : attributes.keySet())
            {
                RequirementAttributeType type;
                Object[] row;
                Object value;

                // Determine the type of the attribute value.
                value = attributes.get(attributeId);
                if (value instanceof BigDecimal) type = RequirementAttributeType.NUMBER;
                else if (value instanceof Boolean) type = RequirementAttributeType.BOOLEAN;
                else type = RequirementAttributeType.STRING;

                row = new Object[3];
                row[0] = attributeId;
                row[1] = type;
                row[2] = value != null ? value.toString() : null;
                tableModel.addRow(row);
            }
        }
    }

    public void saveChanges()
    {
        TableCellEditor cellEditor;

        // Stop editing on the table.
        cellEditor = jTableAttributes.getCellEditor();
        if (cellEditor != null) cellEditor.stopCellEditing();

        // Save all attribute to the requirement.
        if (currentRequirement != null)
        {
            Map<String, Object> attributes;

            attributes = new LinkedHashMap<String, Object>();
            for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++)
            {
                String attributeID;
                String value;
                RequirementAttributeType type;

                attributeID = (String)tableModel.getValueAt(rowIndex, 0);
                type = (RequirementAttributeType)tableModel.getValueAt(rowIndex, 1);
                value = (String)tableModel.getValueAt(rowIndex, 2);

                if (Strings.isNullOrEmpty(value))
                {
                    attributes.remove(attributeID);
                }
                else
                {
                    switch (type)
                    {
                        case NUMBER:
                            attributes.put(attributeID, new BigDecimal(value));
                            break;
                        case BOOLEAN:
                            attributes.put(attributeID, Boolean.parseBoolean(value));
                            break;
                        default:
                            attributes.put(attributeID, value);
                    }
                }
            }

            setRequirementAttributes(currentRequirement, attributes);
        }
    }

    private String translate(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    public Map<String, Object> getRequirementAttributeMap(Requirement requirement)
    {
        LinkedHashMap<String, Object> attributeMap;

        attributeMap = new LinkedHashMap<String, Object>();
        if (requirement instanceof DataRequirement)
        {
            List<RequirementAttribute> attributes;
            DataRequirement dataRequirement;

            // Get the data requirement.
            dataRequirement = (DataRequirement)requirement;

            // Add some default settings that we want to see on the UI even though they are not part of the attribute set.
            attributeMap.put(RequirementAttribute.DR_ID.toString(), dataRequirement.getConceptID());
            attributeMap.put(RequirementAttribute.CLASS_ID.toString(), dataRequirement.getClassConceptID());

            // Add all of the custom attributes defined on the data requirement.
            attributeMap.putAll(dataRequirement.getAttributes());

            // Make sure that all of the standard attributes are present in the map.
            attributes = dataRequirement.getRequirementType().getAttributes();
            if (attributes != null)
            {
                for (RequirementAttribute attribute : attributes)
                {
                    if (!attributeMap.containsKey(attribute.toString()))
                    {
                        attributeMap.put(attribute.toString(), null);
                    }
                }
            }
        }
        else if (requirement instanceof SectionRequirement)
        {
            SectionRequirement sectionRequirement;

            sectionRequirement = (SectionRequirement)requirement;
            attributeMap.put(RequirementAttribute.SECTION_ID.toString(), sectionRequirement.getConceptID());
        }
        else if (requirement instanceof PropertyRequirement)
        {
            PropertyRequirement propertyRequirement;
            List<RequirementAttribute> attributes;

            // Get the property requirement.
            propertyRequirement = (PropertyRequirement)requirement;

            // Add some default settings that we want to see on the UI even though they are not part of the attribute set.
            attributeMap.put(RequirementAttribute.PROPERTY_ID.toString(), propertyRequirement.getConceptID());
            attributeMap.put(RequirementAttribute.CHARACTERISTIC.toString(), propertyRequirement.isCharacteristic());

            // Add the rest of the custom attributes defined on the property.
            attributeMap.putAll(propertyRequirement.getAttributes());

            // Make sure that all of the standard attributes are present in the map.
            attributes = propertyRequirement.getRequirementType().getAttributes();
            if (attributes != null)
            {
                for (RequirementAttribute attribute : attributes)
                {
                    if (!attributeMap.containsKey(attribute.toString()))
                    {
                        attributeMap.put(attribute.toString(), null);
                    }
                }
            }
        }
        else
        {
            ValueRequirement valueRequirement;
            List<RequirementAttribute> attributes;

            // Get the value requirement.
            valueRequirement = (ValueRequirement)requirement;

            // Add some default settings that we want to see on the UI even though they are not part of the attribute set.
            if (valueRequirement.getRequirementType().isConceptRequirementValue()) attributeMap.put(RequirementAttribute.REQUIREMENT_CONCEPT_ID.toString(), valueRequirement.getValue());
            else if (valueRequirement.getRequirementType() == RequirementType.CONCEPT_TYPE) attributeMap.put(RequirementAttribute.CONCEPT_TYPE_ID.toString(), valueRequirement.getValue());

            // Add the rest of the custom attributes defined on the value requirement.
            attributeMap.putAll(valueRequirement.getAttributes());

            // Make sure that all of the standard attributes are present in the map.
            attributes = valueRequirement.getRequirementType().getAttributes();
            if (attributes != null)
            {
                for (RequirementAttribute attribute : attributes)
                {
                    if (!attributeMap.containsKey(attribute.toString()))
                    {
                        attributeMap.put(attribute.toString(), null);
                    }
                }
            }
        }

        // Reutrn the attribute map.
        return attributeMap;
    }

    public void setRequirementAttributes(Requirement requirement, Map<String, Object> attributes)
    {
        if (requirement instanceof DataRequirement)
        {
            DataRequirement dataRequirement;
            Map<String, Object> drAttributes;

            drAttributes = new HashMap<String, Object>(attributes);
            drAttributes.remove(RequirementAttribute.DR_ID.toString());
            drAttributes.remove(RequirementAttribute.CLASS_ID.toString());
            dataRequirement = (DataRequirement)requirement;
            dataRequirement.setAttributes(drAttributes);
        }
        else if (requirement instanceof SectionRequirement)
        {
            // Nothing to do here.
        }
        else if (requirement instanceof PropertyRequirement)
        {
            PropertyRequirement propertyRequirement;
            boolean characteristic;
            Map<String, Object> propertyAttributes;

            characteristic = Boolean.TRUE.equals(attributes.get(RequirementAttribute.CHARACTERISTIC.toString()));

            propertyAttributes = new HashMap<String, Object>(attributes);
            propertyAttributes.remove(RequirementAttribute.PROPERTY_ID.toString());
            propertyAttributes.remove(RequirementAttribute.CHARACTERISTIC.toString());

            propertyRequirement = (PropertyRequirement)requirement;
            propertyRequirement.setCharacteristic(characteristic);
            propertyRequirement.setAttributes(propertyAttributes);
        }
        else
        {
            Map<String, Object> valueAttributes;
            ValueRequirement valueRequirement;
            String conceptID;
            String conceptType;

            valueAttributes = new HashMap<String, Object>(attributes);
            conceptID = (String)valueAttributes.remove(RequirementAttribute.REQUIREMENT_CONCEPT_ID.toString());
            conceptType = (String)valueAttributes.remove(RequirementAttribute.CONCEPT_TYPE_ID.toString());

            valueRequirement = (ValueRequirement)requirement;
            valueRequirement.setAttributes(valueAttributes);

            // Set the concept ID if this value requirement contains one.
            if (valueRequirement.getRequirementType().isConceptRequirementValue())
            {
                valueRequirement.setValue(conceptID);
            }

            // Special case for concept type.
            if (valueRequirement.getRequirementType() == RequirementType.CONCEPT_TYPE)
            {
                valueRequirement.setValue(conceptType);
            }
        }
    }

    private void addAttribute()
    {
        String attributeID;

        attributeID = JOptionPane.showInputDialog(parentEditor, "Please enter a valid Attribute ID (capital letters and underscores only).", "Add New Attribute", JOptionPane.PLAIN_MESSAGE);
        if (!Strings.isNullOrEmpty(attributeID))
        {
            if (attributeID.matches(DataRequirement.ATTRIBUTE_IDENTIFIER_PATTERN))
            {
                Object[] row;

                row = new Object[3];
                row[0] = attributeID;
                row[1] = RequirementAttributeType.STRING;
                row[2] = null;
                tableModel.addRow(row);
            }
            else JOptionPane.showMessageDialog(parentEditor, "The ID '" + attributeID + "' is not in the correct format.\nCapital letters, digits and underscores only.\nFirst character must be a letter.\nNo double underscores.\nNo leading or trailing underscores.\nNo underscores followed by a digit.", "Invalid ID Format", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void deleteSelectedAttribute()
    {
        int selectedIndex;

        selectedIndex = jTableAttributes.getSelectedRow();
        if (selectedIndex > 0)
        {
            tableModel.removeRow(selectedIndex);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarAttributes = new javax.swing.JToolBar();
        jButtonAddAttribute = new javax.swing.JButton();
        jButtonDeleteAttribute = new javax.swing.JButton();
        jScrollPaneAttributes = new javax.swing.JScrollPane();
        jTableAttributes = new javax.swing.JTable();

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());

        jToolBarAttributes.setFloatable(false);
        jToolBarAttributes.setRollover(true);

        jButtonAddAttribute.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/tableInsertRowIcon.png"))); // NOI18N
        jButtonAddAttribute.setText("Add");
        jButtonAddAttribute.setFocusable(false);
        jButtonAddAttribute.setOpaque(false);
        jButtonAddAttribute.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddAttribute.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddAttributeActionPerformed(evt);
            }
        });
        jToolBarAttributes.add(jButtonAddAttribute);

        jButtonDeleteAttribute.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/tableDeleteRowIcon.png"))); // NOI18N
        jButtonDeleteAttribute.setText("Delete");
        jButtonDeleteAttribute.setFocusable(false);
        jButtonDeleteAttribute.setOpaque(false);
        jButtonDeleteAttribute.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDeleteAttribute.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDeleteAttributeActionPerformed(evt);
            }
        });
        jToolBarAttributes.add(jButtonDeleteAttribute);

        add(jToolBarAttributes, java.awt.BorderLayout.NORTH);

        jScrollPaneAttributes.setBorder(null);

        jTableAttributes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Attribute", "Type", "Value"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        jTableAttributes.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_LAST_COLUMN);
        jTableAttributes.setGridColor(new java.awt.Color(204, 204, 204));
        jTableAttributes.setRowHeight(20);
        jScrollPaneAttributes.setViewportView(jTableAttributes);

        add(jScrollPaneAttributes, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddAttributeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddAttributeActionPerformed
    {//GEN-HEADEREND:event_jButtonAddAttributeActionPerformed
        addAttribute();
    }//GEN-LAST:event_jButtonAddAttributeActionPerformed

    private void jButtonDeleteAttributeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteAttributeActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteAttributeActionPerformed
        deleteSelectedAttribute();
    }//GEN-LAST:event_jButtonDeleteAttributeActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddAttribute;
    private javax.swing.JButton jButtonDeleteAttribute;
    private javax.swing.JScrollPane jScrollPaneAttributes;
    private javax.swing.JTable jTableAttributes;
    private javax.swing.JToolBar jToolBarAttributes;
    // End of variables declaration//GEN-END:variables
}
