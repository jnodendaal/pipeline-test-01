package com.pilog.t8.ui.datarequirementeditor.prescribedvalueeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.ui.datarequirementeditor.view.tree.TransferableConceptTerminologyList;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import javax.swing.TransferHandler;

/**
 * @author Bouwer du Preez
 */
public class PrescribedValueTableTransferHandler extends TransferHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(PrescribedValueTableTransferHandler.class);

    private final PrescribedValueEditor editor;

    public PrescribedValueTableTransferHandler(PrescribedValueEditor editor)
    {
        this.editor = editor;
    }

    @Override
    public boolean canImport(PrescribedValueTableTransferHandler.TransferSupport support)
    {
        if (!support.isDataFlavorSupported(TransferableConceptTerminologyList.CONCEPT_TERMINOLOGY_LIST_FLAVOR) || !support.isDrop())
        {
            return false;
        }
        else
        {
            try
            {
                List<T8ConceptTerminology> transferredData;
                Transferable transferable;

                transferable = support.getTransferable();
                transferredData = (List<T8ConceptTerminology>)transferable.getTransferData(TransferableConceptTerminologyList.CONCEPT_TERMINOLOGY_LIST_FLAVOR);
                return editor.canImport(transferredData);
            }
            catch (UnsupportedFlavorException | IOException e)
            {
                LOGGER.log(e);
                return false;
            }
        }
    }

    @Override
    public boolean importData(PrescribedValueTableTransferHandler.TransferSupport support)
    {
        if (!canImport(support))
        {
            return false;
        }
        else
        {
            List<T8ConceptTerminology> transferredData;
            Transferable transferable;

            transferable = support.getTransferable();

            try
            {
                transferredData = (List<T8ConceptTerminology>)transferable.getTransferData(TransferableConceptTerminologyList.CONCEPT_TERMINOLOGY_LIST_FLAVOR);
            }
            catch (UnsupportedFlavorException | IOException e)
            {
                LOGGER.log(e);
                return false;
            }

            try
            {
                editor.insertPrescribedValues(transferredData);
                return true;
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while adding new prescribed values.", e);
                return false;
            }
        }
    }
}
