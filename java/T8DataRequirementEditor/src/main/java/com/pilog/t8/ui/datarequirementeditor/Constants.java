package com.pilog.t8.ui.datarequirementeditor;

/**
 * @author Bouwer du Preez
 */
public class Constants
{
    public static final String ACTION_SAVE = "SAVE";
    public static final boolean TEST_MODE = false;
}
