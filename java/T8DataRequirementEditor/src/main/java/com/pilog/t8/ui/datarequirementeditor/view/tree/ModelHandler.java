package com.pilog.t8.ui.datarequirementeditor.view.tree;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.Requirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.ConceptDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.ConceptRelationshipRequirement;
import com.pilog.t8.data.document.datarequirement.value.ConceptTypeRequirement;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.datarequirement.value.OntologyClassRequirement;
import com.pilog.t8.data.document.datarequirement.value.DRDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.DRInstanceDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.FieldRequirement;
import com.pilog.t8.data.document.datarequirement.value.LowerBoundNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.NumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.PropertyDependencyRequirement;
import com.pilog.t8.data.document.datarequirement.value.QualifierOfMeasureRequirement;
import com.pilog.t8.data.document.datarequirement.value.UnitOfMeasureRequirement;
import com.pilog.t8.data.document.datarequirement.value.UpperBoundNumberRequirement;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class ModelHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(ModelHandler.class);

    private final RequirementTree parentTree;
    private DefaultTreeModel treeModel;

    public ModelHandler(RequirementTree parentTree, DefaultTreeModel treeModel)
    {
        this.parentTree = parentTree;
        this.treeModel = treeModel;
    }

    void setTreeModel(DefaultTreeModel treeModel)
    {
        this.treeModel = treeModel;
    }

    public DefaultTreeModel createNewModel(DataRequirement dataRequirement)
    {
        if (dataRequirement == null)
        {
            return null;
        }
        else
        {
            DefaultMutableTreeNode rootNode;
            DefaultTreeModel model;

            rootNode = new DefaultMutableTreeNode(dataRequirement);
            rebuildNodeChildren(rootNode);
            model = new DefaultTreeModel(rootNode);
            return model;
        }
    }

    public void rebuildNodeChildren(DefaultMutableTreeNode treeNode)
    {
        try
        {
            Object userObject;

            treeNode.removeAllChildren();
            userObject = treeNode.getUserObject();
            if (userObject instanceof DataRequirement)
            {
                DataRequirement requirement;

                requirement  = (DataRequirement)userObject;
                for (SectionRequirement sectionRequirement : requirement.getSectionRequirements())
                {
                    DefaultMutableTreeNode newNode;

                    newNode = new DefaultMutableTreeNode(sectionRequirement);
                    treeNode.add(newNode);
                    rebuildNodeChildren(newNode);
                }
            }
            else if (userObject instanceof SectionRequirement)
            {
                SectionRequirement requirement;

                requirement  = (SectionRequirement)userObject;
                for (PropertyRequirement propertyRequirement : requirement.getPropertyRequirements())
                {
                    DefaultMutableTreeNode newNode;

                    newNode = new DefaultMutableTreeNode(propertyRequirement);
                    treeNode.add(newNode);
                    rebuildNodeChildren(newNode);
                }
            }
            else if (userObject instanceof PropertyRequirement)
            {
                PropertyRequirement requirement;

                requirement  = (PropertyRequirement)userObject;
                if (requirement.getValueRequirement() != null)
                {
                    DefaultMutableTreeNode newNode;

                    newNode = new DefaultMutableTreeNode(requirement.getValueRequirement());
                    treeNode.add(newNode);
                    rebuildNodeChildren(newNode);
                }
            }
            else if (userObject instanceof ValueRequirement)
            {
                ValueRequirement requirement;

                requirement  = (ValueRequirement)userObject;
                for (ValueRequirement subRequirement : requirement.getSubRequirements())
                {
                    DefaultMutableTreeNode newNode;

                    newNode = new DefaultMutableTreeNode(subRequirement);
                    treeNode.add(newNode);
                    rebuildNodeChildren(newNode);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while rebuilding node children: " + treeNode, e);
        }
    }

    public boolean canDeleteNode(DefaultMutableTreeNode node)
    {
        Object requirementObject;

        requirementObject = node.getUserObject();
        if (requirementObject instanceof SectionRequirement)
        {
            return true;
        }
        else if (requirementObject instanceof PropertyRequirement)
        {
            return true;
        }
        else if (requirementObject instanceof ValueRequirement)
        {
            ValueRequirement valueRequirement;
            RequirementType requirementType;

            valueRequirement = (ValueRequirement)requirementObject;
            requirementType = valueRequirement.getRequirementType();
            if (valueRequirement.getParentValueRequirement() == null) return true;
            else if (RequirementType.CONCEPT_RELATIONSHIP_TYPE.equals(requirementType)) return true;
            else if (RequirementType.CONCEPT_DEPENDENCY_TYPE.equals(requirementType)) return true;
            else if (RequirementType.CONCEPT_TYPE.equals(requirementType)) return true;
            else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(requirementType)) return true;
            else if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(requirementType)) return true;
            else if (RequirementType.FIELD_TYPE.equals(requirementType)) return true;
            else if (RequirementType.FIELD_DEPENDENCY_TYPE.equals(requirementType)) return true;
            else if (RequirementType.INTEGER_TYPE.equals(requirementType)) return true;
            else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(requirementType)) return true;
            else if (RequirementType.QUALIFIER_OF_MEASURE.equals(requirementType)) return true;
            else if (RequirementType.REAL_TYPE.equals(requirementType)) return true;
            else if (RequirementType.UNIT_OF_MEASURE.equals(requirementType)) return true;
            else return (RequirementType.ONTOLOGY_CLASS.equals(requirementType));
        }
        else return false;
    }

    public void addConcept(Requirement requirement, T8ConceptTerminology concept)
    {
        if (requirement instanceof DataRequirement)
        {
            if (concept.getConceptType() == T8OntologyConceptType.SECTION)
            {
                DataRequirement dataRequirement;
                SectionRequirement sectionRequirement;

                dataRequirement = (DataRequirement)requirement;
                sectionRequirement = new SectionRequirement(concept.getConceptId());
                dataRequirement.addSectionRequirement(sectionRequirement);
            }
            else if (concept.getConceptType() == T8OntologyConceptType.CLASS)
            {
                DataRequirement dataRequirement;

                dataRequirement = (DataRequirement)requirement;
                dataRequirement.setClassConceptID(concept.getConceptId());
            }
        }
        else if (requirement instanceof SectionRequirement)
        {
            if (concept.getConceptType() == T8OntologyConceptType.PROPERTY)
            {
                SectionRequirement sectionRequirement;
                PropertyRequirement propertyRequirement;

                sectionRequirement = (SectionRequirement)requirement;
                propertyRequirement = new PropertyRequirement(concept.getConceptId());
                sectionRequirement.addPropertyRequirement(propertyRequirement);
            }
        }
        else if (requirement instanceof PropertyRequirement)
        {
            if (concept.getConceptType() == T8OntologyConceptType.VALUE)
            {
                PropertyRequirement propertyRequirement;
                ValueRequirement controlledValueRequirement;

                propertyRequirement = (PropertyRequirement)requirement;
                if (propertyRequirement.getValueRequirement() == null)
                {
                    controlledValueRequirement = new ControlledConceptRequirement();
                    propertyRequirement.setValueRequirement(controlledValueRequirement);
                }
                else controlledValueRequirement = propertyRequirement.getValueRequirement();

                if ((controlledValueRequirement != null) && (RequirementType.CONTROLLED_CONCEPT.equals(controlledValueRequirement.getRequirementType())))
                {
                    ValueRequirement valueOfPropertyRequirement;

                    valueOfPropertyRequirement = new OntologyClassRequirement();
                    valueOfPropertyRequirement.setValue(concept.getConceptId());

                    controlledValueRequirement.addSubRequirement(valueOfPropertyRequirement);
                }
            }
        }
        else if (requirement instanceof ValueRequirement)
        {
            ValueRequirement valueRequirement;
            RequirementType requirementType;

            valueRequirement = (ValueRequirement)requirement;
            requirementType = valueRequirement.getRequirementType();
            if (RequirementType.CONTROLLED_CONCEPT.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.ONTOLOGY_CLASS)
                {
                    ValueRequirement valueGroupRequirement;

                    valueGroupRequirement = new OntologyClassRequirement();
                    valueGroupRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(valueGroupRequirement);
                }
                else if (concept.getConceptType() == T8OntologyConceptType.DATA_REQUIREMENT_INSTANCE)
                {
                    ValueRequirement valueGroupRequirement;

                    valueGroupRequirement = new DRInstanceDependencyRequirement();
                    valueGroupRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(valueGroupRequirement);
                }
                else if (concept.getConceptType() == T8OntologyConceptType.DATA_REQUIREMENT)
                {
                    ValueRequirement valueGroupRequirement;

                    valueGroupRequirement = new DRDependencyRequirement();
                    valueGroupRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(valueGroupRequirement);
                }
                else if (concept.getConceptType() == T8OntologyConceptType.PROPERTY)
                {
                    ValueRequirement valueGroupRequirement;

                    valueGroupRequirement = new PropertyDependencyRequirement();
                    valueGroupRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(valueGroupRequirement);
                }
            }
            else if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.PROPERTY)
                {
                    ValueRequirement propertyDependencyRequirement;

                    propertyDependencyRequirement = new PropertyDependencyRequirement();
                    propertyDependencyRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(propertyDependencyRequirement);
                }
            }
            else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.PROPERTY)
                {
                    ValueRequirement propertyDependencyRequirement;

                    propertyDependencyRequirement = new PropertyDependencyRequirement();
                    propertyDependencyRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(propertyDependencyRequirement);
                }
            }
            else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.PROPERTY)
                {
                    ValueRequirement fieldDependencyRequirement;

                    fieldDependencyRequirement = new FieldDependencyRequirement();
                    fieldDependencyRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(fieldDependencyRequirement);
                }
            }
            else if (RequirementType.CONCEPT_DEPENDENCY_TYPE.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.CONCEPT_RELATIONSHIP_GRAPH)
                {
                    ValueRequirement valueGroupRequirement;

                    valueGroupRequirement = new ConceptRelationshipRequirement();
                    valueGroupRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(valueGroupRequirement);
                }
            }
            else if (RequirementType.DOCUMENT_REFERENCE.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.ONTOLOGY_CLASS)
                {
                    ValueRequirement ontologyClassRequirement;

                    ontologyClassRequirement = new OntologyClassRequirement();
                    ontologyClassRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(ontologyClassRequirement);
                }
                else if (concept.getConceptType() == T8OntologyConceptType.PROPERTY)
                {
                    ValueRequirement valueGroupRequirement;

                    valueGroupRequirement = new PropertyDependencyRequirement();
                    valueGroupRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(valueGroupRequirement);
                }
            }
            else if (RequirementType.COMPOSITE_TYPE.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.PROPERTY)
                {
                    ValueRequirement fieldRequirement;

                    fieldRequirement = new FieldRequirement();
                    fieldRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(fieldRequirement);
                }
            }
            else if ((RequirementType.MEASURED_RANGE.equals(requirementType)))
            {
                if (concept.getConceptType() == T8OntologyConceptType.UNIT_OF_MEASURE)
                {
                    ValueRequirement uomRequirement;

                    uomRequirement = new UnitOfMeasureRequirement();
                    uomRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(uomRequirement);
                }
            }
            else if (RequirementType.MEASURED_NUMBER.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.UNIT_OF_MEASURE)
                {
                    ValueRequirement uomRequirement;

                    uomRequirement = new UnitOfMeasureRequirement();
                    uomRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(uomRequirement);
                }
                else if (concept.getConceptType() == T8OntologyConceptType.QUALIFIER_OF_MEASURE)
                {
                    ValueRequirement qomRequirement;

                    qomRequirement = new QualifierOfMeasureRequirement();
                    qomRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(qomRequirement);
                }
            }
            else if (RequirementType.UNIT_OF_MEASURE.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.ONTOLOGY_CLASS)
                {
                    ValueRequirement ontologyClassRequirement;

                    ontologyClassRequirement = new OntologyClassRequirement();
                    ontologyClassRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(ontologyClassRequirement);
                }
            }
            else if (RequirementType.QUALIFIER_OF_MEASURE.equals(requirementType))
            {
                if (concept.getConceptType() == T8OntologyConceptType.ONTOLOGY_CLASS)
                {
                    ValueRequirement ontologyClassRequirement;

                    ontologyClassRequirement = new OntologyClassRequirement();
                    ontologyClassRequirement.setValue(concept.getConceptId());
                    valueRequirement.addSubRequirement(ontologyClassRequirement);
                }
            }
        }
    }

    public void addRequirement(Requirement targetRequirement, Requirement newRequirement)
    {
        if (targetRequirement instanceof DataRequirement)
        {
            if (newRequirement instanceof SectionRequirement)
            {
                DataRequirement dataRequirement;

                dataRequirement = (DataRequirement)targetRequirement;
                dataRequirement.addSectionRequirement((SectionRequirement)newRequirement);
            }
        }
        else if (targetRequirement instanceof SectionRequirement)
        {
            if (newRequirement instanceof PropertyRequirement)
            {
                SectionRequirement sectionRequirement;

                sectionRequirement = (SectionRequirement)targetRequirement;
                sectionRequirement.addPropertyRequirement((PropertyRequirement)newRequirement);
            }
        }
        else if (targetRequirement instanceof ValueRequirement)
        {
            if (newRequirement instanceof ValueRequirement)
            {
                ValueRequirement valueRequirement;
                ValueRequirement newValueRequirement;
                RequirementType requirementType;
                RequirementType newRequirementType;

                valueRequirement = (ValueRequirement)targetRequirement;
                newValueRequirement = (ValueRequirement)newRequirement;

                requirementType = valueRequirement.getRequirementType();
                newRequirementType = newValueRequirement.getRequirementType();

                if (RequirementType.STRING_TYPE.equals(requirementType))
                {
                    if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.INTEGER_TYPE.equals(requirementType))
                {
                    if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.REAL_TYPE.equals(requirementType))
                {
                    if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.DATE_TYPE.equals(requirementType))
                {
                    if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.CONTROLLED_CONCEPT.equals(requirementType))
                {
                    if (RequirementType.ONTOLOGY_CLASS.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.CONTROLLED_CONCEPT.equals(requirementType))
                {
                    if (RequirementType.ONTOLOGY_CLASS.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(requirementType))
                {
                    if (RequirementType.CONCEPT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(requirementType))
                {
                    if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(requirementType))
                {
                    if (RequirementType.CONCEPT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.FIELD_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.FIELD_DEPENDENCY_TYPE.equals(requirementType))
                {
                    if (RequirementType.CONCEPT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.CONCEPT_DEPENDENCY_TYPE.equals(requirementType))
                {
                    if (RequirementType.CONCEPT_RELATIONSHIP_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.CONCEPT_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.LOCAL_STRING_TYPE.equals(requirementType))
                {
                    if (RequirementType.LANGUAGE_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.DOCUMENT_REFERENCE.equals(requirementType))
                {
                    if (RequirementType.ONTOLOGY_CLASS.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if ((RequirementType.MEASURED_RANGE.equals(requirementType)))
                {
                    if (RequirementType.LOWER_BOUND_NUMBER.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.UPPER_BOUND_NUMBER.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.INTEGER_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.REAL_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.UNIT_OF_MEASURE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.MEASURED_NUMBER.equals(requirementType))
                {
                    if (RequirementType.NUMBER.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.INTEGER_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.REAL_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.UNIT_OF_MEASURE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.QUALIFIER_OF_MEASURE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                    else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.UNIT_OF_MEASURE.equals(requirementType))
                {
                    if (RequirementType.ONTOLOGY_CLASS.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
                else if (RequirementType.QUALIFIER_OF_MEASURE.equals(requirementType))
                {
                    if (RequirementType.ONTOLOGY_CLASS.equals(newRequirementType))
                    {
                        valueRequirement.addSubRequirement(newValueRequirement);
                    }
                }
            }
        }
    }

    public void addConceptsToNode(DefaultMutableTreeNode parentNode, List<T8ConceptTerminology> concepts, int index)
    {
        Requirement requirement;
        TreePath parentPath;

        parentPath = new TreePath(parentNode.getPath());

        requirement = (Requirement)parentNode.getUserObject();
        for (T8ConceptTerminology concept : concepts)
        {
            addConcept(requirement, concept);
        }

        rebuildNodeChildren(parentNode);
        treeModel.nodeStructureChanged(parentNode);
        parentTree.expandPath(parentPath);
        parentTree.scrollRectToVisible(parentTree.getPathBounds(parentPath));
    }

    public void addRequirementsToNode(DefaultMutableTreeNode parentNode, List<Requirement> newRequirements)
    {
        newRequirements.stream().forEach((newRequirement) ->
        {
            addRequirementToNode(parentNode, newRequirement);
        });
    }

    public void addRequirementToNode(DefaultMutableTreeNode parentNode, Requirement newRequirement)
    {
        Requirement targetRequirement;
        TreePath parentPath;

        parentPath = new TreePath(parentNode.getPath());

        targetRequirement = (Requirement)parentNode.getUserObject();
        addRequirement(targetRequirement, newRequirement);

        rebuildNodeChildren(parentNode);
        treeModel.nodeStructureChanged(parentNode);
        parentTree.expandPath(parentPath);
        parentTree.scrollRectToVisible(parentTree.getPathBounds(parentPath));
    }

    public void deleteNodes(ArrayList<DefaultMutableTreeNode> nodes)
    {
        for (DefaultMutableTreeNode node : nodes)
        {
            if (canDeleteNode(node))
            {
                Object nodeObject;

                nodeObject = node.getUserObject();
                if (nodeObject instanceof SectionRequirement)
                {
                    SectionRequirement sectionRequirement;

                    sectionRequirement = (SectionRequirement)nodeObject;
                    sectionRequirement.getParentDataRequirement().removeSectionRequirement(sectionRequirement);
                    treeModel.removeNodeFromParent(node);
                }
                else if (nodeObject instanceof PropertyRequirement)
                {
                    PropertyRequirement propertyRequirement;

                    propertyRequirement = (PropertyRequirement)nodeObject;
                    propertyRequirement.getParentSectionRequirement().removePropertyRequirement(propertyRequirement);
                    treeModel.removeNodeFromParent(node);
                }
                else if (nodeObject instanceof ValueRequirement)
                {
                    ValueRequirement valueRequirement;

                    valueRequirement = (ValueRequirement)nodeObject;
                    if (valueRequirement.getParentValueRequirement() != null)
                    {
                        valueRequirement.getParentValueRequirement().removeSubRequirement(valueRequirement);
                    }
                    else
                    {
                        valueRequirement.getParentPropertyRequirement().setValueRequirement(null);
                    }

                    treeModel.removeNodeFromParent(node);
                }
            }
        }
    }

    public DefaultMutableTreeNode findNodeContaining(Object requirement)
    {
        LinkedList<DefaultMutableTreeNode> nodeList;

        nodeList = new LinkedList<>();
        nodeList.add((DefaultMutableTreeNode)treeModel.getRoot());
        while (nodeList.size() > 0)
        {
            DefaultMutableTreeNode nextNode;

            nextNode = nodeList.pollFirst();
            if (nextNode.getUserObject() == requirement)
            {
                return nextNode;
            }
            else
            {
                for (int childIndex = 0; childIndex < nextNode.getChildCount(); childIndex++)
                {
                    nodeList.add((DefaultMutableTreeNode)nextNode.getChildAt(childIndex));
                }
            }
        }

        return null;
    }

    public void refreshNodeContaining(Object requirementObject)
    {
        DefaultMutableTreeNode node;

        node = findNodeContaining(requirementObject);
        if (node != null)
        {
            treeModel.nodeChanged(node);
        }
    }

    public void setRequirementDataType(Object requirement, RequirementType requirementType)
    {
        DefaultMutableTreeNode node;

        node = findNodeContaining(requirement);
        if (node != null)
        {
            if (requirement instanceof PropertyRequirement)
            {
                ((PropertyRequirement)requirement).setValueRequirement(DataTypeConstructor.buildDataTypeRequirement(requirementType));
                rebuildNodeChildren(node);
                treeModel.nodeStructureChanged(node);
            }
            else if (requirement instanceof ValueRequirement)
            {
                ValueRequirement valueRequirement;

                valueRequirement = (ValueRequirement)requirement;
                valueRequirement.removeAllSubRequirements();
                valueRequirement.addSubRequirement(DataTypeConstructor.buildDataTypeRequirement(requirementType));
                rebuildNodeChildren(node);
                treeModel.nodeStructureChanged(node);
            }
        }
    }

    public void changeRequirementDataType(ValueRequirement requirement, RequirementType requirementType)
    {
        DefaultMutableTreeNode node;

        node = findNodeContaining(requirement);
        if (node != null)
        {
            requirement.setRequirementType(requirementType);
            requirement.removeAllSubRequirements();

            rebuildNodeChildren(node);
            treeModel.nodeStructureChanged(node);
        }
    }

    public void moveRequirementUp(Requirement requirement)
    {
        if (requirement instanceof SectionRequirement)
        {
            SectionRequirement sectionRequirement;
            DataRequirement dataRequirement;
            int sectionIndex;

            sectionRequirement = (SectionRequirement)requirement;
            dataRequirement = sectionRequirement.getParentDataRequirement();
            sectionIndex = dataRequirement.getSectionRequirementIndex(sectionRequirement);
            if (sectionIndex > 0)
            {
                DefaultMutableTreeNode sectionNode;

                sectionNode = findNodeContaining(dataRequirement);
                dataRequirement.removeSectionRequirement(sectionRequirement);
                dataRequirement.addSectionRequirement(sectionRequirement, sectionIndex-1);
                rebuildNodeChildren(sectionNode);
                treeModel.nodeStructureChanged(sectionNode);
                parentTree.setSelectedNode(findNodeContaining(sectionRequirement));
            }
        }
        else if (requirement instanceof PropertyRequirement)
        {
            PropertyRequirement propertyRequirement;
            SectionRequirement sectionRequirement;
            int propertyIndex;

            propertyRequirement = (PropertyRequirement)requirement;
            sectionRequirement = propertyRequirement.getParentSectionRequirement();
            propertyIndex = sectionRequirement.getPropertyRequirementIndex(propertyRequirement);
            if (propertyIndex > 0)
            {
                DefaultMutableTreeNode sectionNode;

                sectionNode = findNodeContaining(sectionRequirement);
                sectionRequirement.removePropertyRequirement(propertyRequirement);
                sectionRequirement.addPropertyRequirement(propertyRequirement, propertyIndex-1);
                rebuildNodeChildren(sectionNode);
                treeModel.nodeStructureChanged(sectionNode);
                parentTree.setSelectedNode(findNodeContaining(propertyRequirement));
            }
        }
        else if (requirement instanceof ValueRequirement)
        {
            ValueRequirement valueRequirement;
            ValueRequirement parentValueRequirement;

            valueRequirement = (ValueRequirement)requirement;
            parentValueRequirement = valueRequirement.getParentValueRequirement();
            if (parentValueRequirement != null)
            {
                int valueRequirementIndex;

                valueRequirementIndex = parentValueRequirement.getSubRequirementIndex(valueRequirement);
                if (valueRequirementIndex > 0)
                {
                    DefaultMutableTreeNode sectionNode;

                    sectionNode = findNodeContaining(parentValueRequirement);
                    parentValueRequirement.removeSubRequirement(valueRequirement);
                    parentValueRequirement.addSubRequirement(valueRequirement, valueRequirementIndex-1);
                    rebuildNodeChildren(sectionNode);
                    treeModel.nodeStructureChanged(sectionNode);
                    parentTree.setSelectedNode(findNodeContaining(valueRequirement));
                }
            }
        }
    }

    public void moveRequirementDown(Requirement requirement)
    {
        if (requirement instanceof SectionRequirement)
        {
            SectionRequirement sectionRequirement;
            DataRequirement dataRequirement;
            int sectionIndex;

            sectionRequirement = (SectionRequirement)requirement;
            dataRequirement = sectionRequirement.getParentDataRequirement();
            sectionIndex = dataRequirement.getSectionRequirementIndex(sectionRequirement);
            if (sectionIndex < dataRequirement.getSectionRequirementCount()-1)
            {
                DefaultMutableTreeNode sectionNode;

                sectionNode = findNodeContaining(dataRequirement);
                dataRequirement.removeSectionRequirement(sectionRequirement);
                dataRequirement.addSectionRequirement(sectionRequirement, sectionIndex+1);
                rebuildNodeChildren(sectionNode);
                treeModel.nodeStructureChanged(sectionNode);
                parentTree.setSelectedNode(findNodeContaining(sectionRequirement));
            }
        }
        else if (requirement instanceof PropertyRequirement)
        {
            PropertyRequirement propertyRequirement;
            SectionRequirement sectionRequirement;
            int propertyIndex;

            propertyRequirement = (PropertyRequirement)requirement;
            sectionRequirement = propertyRequirement.getParentSectionRequirement();
            propertyIndex = sectionRequirement.getPropertyRequirementIndex(propertyRequirement);
            if (propertyIndex < sectionRequirement.getPropertyRequirementCount()-1)
            {
                DefaultMutableTreeNode sectionNode;

                sectionNode = findNodeContaining(sectionRequirement);
                sectionRequirement.removePropertyRequirement(propertyRequirement);
                sectionRequirement.addPropertyRequirement(propertyRequirement, propertyIndex+1);
                rebuildNodeChildren(sectionNode);
                treeModel.nodeStructureChanged(sectionNode);
                parentTree.setSelectedNode(findNodeContaining(propertyRequirement));
            }
        }
        else if (requirement instanceof ValueRequirement)
        {
            ValueRequirement valueRequirement;
            ValueRequirement parentValueRequirement;

            valueRequirement = (ValueRequirement)requirement;
            parentValueRequirement = valueRequirement.getParentValueRequirement();
            if (parentValueRequirement != null)
            {
                int valueRequirementIndex;

                valueRequirementIndex = parentValueRequirement.getSubRequirementIndex(valueRequirement);
                if (valueRequirementIndex < parentValueRequirement.getSubRequirementCount()-1)
                {
                    DefaultMutableTreeNode parentNode;

                    parentNode = findNodeContaining(parentValueRequirement);
                    parentValueRequirement.removeSubRequirement(valueRequirement);
                    parentValueRequirement.addSubRequirement(valueRequirement, valueRequirementIndex+1);
                    rebuildNodeChildren(parentNode);
                    treeModel.nodeStructureChanged(parentNode);
                    parentTree.setSelectedNode(findNodeContaining(valueRequirement));
                }
            }
        }
    }

    public List<ValueRequirementOption> getValueRequirementAdditionOptions(Requirement targetRequirement)
    {
        List<ValueRequirementOption> options;
        ValueRequirementOption drDependencyOption;
        ValueRequirementOption drInstanceDependencyOption;
        ValueRequirementOption propertyDependencyOption;

        drInstanceDependencyOption = new ValueRequirementOption(new DRInstanceDependencyRequirement(), "DR Instance Dependency");
        drDependencyOption = new ValueRequirementOption(new DRDependencyRequirement(), "DR Dependency");
        propertyDependencyOption = new ValueRequirementOption(new PropertyDependencyRequirement(), "Property Dependency");

        options = new ArrayList<>();
        if (targetRequirement instanceof ValueRequirement)
        {
            ValueRequirement valueRequirement;
            RequirementType requirementType;

            valueRequirement = (ValueRequirement)targetRequirement;
            requirementType = valueRequirement.getRequirementType();
            if (RequirementType.DOCUMENT_REFERENCE.equals(requirementType))
            {
                options.add(new ValueRequirementOption(new OntologyClassRequirement(), "Ontology Class"));
                options.add(drInstanceDependencyOption);
                options.add(drDependencyOption);
                options.add(propertyDependencyOption);
            }
            else if (RequirementType.CONTROLLED_CONCEPT.equals(requirementType))
            {
                options.add(drInstanceDependencyOption);
                options.add(drDependencyOption);
                options.add(propertyDependencyOption);
            }
            else if (RequirementType.MEASURED_NUMBER.equals(requirementType))
            {
                options.add(new ValueRequirementOption(new NumberRequirement(), "Number"));
                options.add(new ValueRequirementOption(new UnitOfMeasureRequirement(), "UOM"));
                options.add(new ValueRequirementOption(new QualifierOfMeasureRequirement(), "Qualifier"));
            }
            else if (RequirementType.MEASURED_RANGE.equals(requirementType))
            {
                options.add(new ValueRequirementOption(new LowerBoundNumberRequirement(), "Lower Bound Number"));
                options.add(new ValueRequirementOption(new UpperBoundNumberRequirement(), "Upper Bound Number"));
                options.add(new ValueRequirementOption(new UnitOfMeasureRequirement(), "UOM"));
                options.add(new ValueRequirementOption(new QualifierOfMeasureRequirement(), "Qualifier"));
            }
            else if (RequirementType.DATA_REQUIREMENT_INSTANCE_DEPENDENCY_TYPE.equals(requirementType))
            {
                options.add(new ValueRequirementOption(new ConceptDependencyRequirement(), "Concept Dependency"));
                options.add(propertyDependencyOption);
            }
            else if (RequirementType.DATA_REQUIREMENT_DEPENDENCY_TYPE.equals(requirementType))
            {
                options.add(propertyDependencyOption);
            }
            else if (RequirementType.PROPERTY_DEPENDENCY_TYPE.equals(requirementType))
            {
                options.add(new ValueRequirementOption(new ConceptDependencyRequirement(), "Concept Dependency"));
                options.add(new ValueRequirementOption(new FieldDependencyRequirement(), "Field Dependency"));
            }
            else if (RequirementType.FIELD_DEPENDENCY_TYPE.equals(requirementType))
            {
                options.add(new ValueRequirementOption(new ConceptDependencyRequirement(), "Concept Dependency"));
                options.add(new ValueRequirementOption(new FieldDependencyRequirement(), "Field Dependency"));
            }
            else if (RequirementType.CONCEPT_DEPENDENCY_TYPE.equals(requirementType))
            {
                options.add(new ValueRequirementOption(new ConceptRelationshipRequirement(), "Concept Relationship"));
                options.add(new ValueRequirementOption(new ConceptTypeRequirement(), "Concept Type"));
            }
            else if (RequirementType.UNIT_OF_MEASURE.equals(requirementType))
            {
                options.add(new ValueRequirementOption(new OntologyClassRequirement(), "Ontology Class"));
            }
            else if (RequirementType.QUALIFIER_OF_MEASURE.equals(requirementType))
            {
                options.add(new ValueRequirementOption(new OntologyClassRequirement(), "Ontology Class"));
            }
        }

        return options;
    }

    public class ValueRequirementOption
    {
        private final ValueRequirement valueRequirement;
        private final String displayName;

        public ValueRequirementOption(ValueRequirement valueRequirement, String displayName)
        {
            this.valueRequirement = valueRequirement;
            this.displayName = displayName;
        }

        public ValueRequirement getValueRequirement()
        {
            return valueRequirement;
        }

        public String getDisplayName()
        {
            return displayName;
        }
    }
}
