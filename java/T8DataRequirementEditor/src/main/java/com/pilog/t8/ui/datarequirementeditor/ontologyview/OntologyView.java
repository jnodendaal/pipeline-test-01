package com.pilog.t8.ui.datarequirementeditor.ontologyview;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.ui.datarequirementeditor.T8DataRequirementEditor;

/**
 * @author Bouwer du Preez
 */
public class OntologyView extends javax.swing.JPanel
{
    private final T8DataRequirementEditor parentEditor;
    private final T8ConfigurationManager configurationManager;
    private final T8ComponentController controller;
    private final T8Context context;
    private DataRequirementView dataRequirementView;
    private DataRequirementInstanceView drInstanceView;
    private DataRequirementElementView drElementView;
    private ConceptRelationshipView conceptRelationshipView;

    public OntologyView(T8DataRequirementEditor parentEditor)
    {
        //Set the component controller and session context for translations of UI components
        this.controller = parentEditor.getController();
        this.configurationManager = this.controller.getClientContext().getConfigurationManager();
        this.context = controller.getContext();

        initComponents();
        this.parentEditor = parentEditor;
        this.setBorder(new T8ContentHeaderBorder("Ontology"));

        addDataRequirementView();
        addDataRequirementInstanceView();
        addConceptElementView();
        addConceptRelationshipView();
    }

    /**
     * Because this component implements the T8ComponentController interface it
     * is responsible for stopping the components it controls when required.
     */
    public void stopComponent()
    {
        if (dataRequirementView != null) dataRequirementView.stopComponent();
        if (drInstanceView != null) drInstanceView.stopComponent();
        if (drElementView != null) drElementView.stopComponent();
        if (conceptRelationshipView != null) conceptRelationshipView.stopComponent();
    }

    private String getTranslatedString(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    private void addDataRequirementView()
    {
        dataRequirementView = new DataRequirementView(parentEditor);
        jTabbedPaneGroups.add(dataRequirementView, getTranslatedString("Data Requirements"));
    }

    private void addDataRequirementInstanceView()
    {
        drInstanceView = new DataRequirementInstanceView(parentEditor);
        jTabbedPaneGroups.add(drInstanceView, getTranslatedString("Instances"));
    }

    private void addConceptElementView()
    {
        drElementView = new DataRequirementElementView(parentEditor);
        jTabbedPaneGroups.add(drElementView, getTranslatedString("Elements"));
    }

    private void addConceptRelationshipView()
    {
        conceptRelationshipView = new ConceptRelationshipView(parentEditor);
        jTabbedPaneGroups.add(conceptRelationshipView, getTranslatedString("Relationships"));
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jTabbedPaneGroups = new javax.swing.JTabbedPane();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        add(jTabbedPaneGroups, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane jTabbedPaneGroups;
    // End of variables declaration//GEN-END:variables
}
