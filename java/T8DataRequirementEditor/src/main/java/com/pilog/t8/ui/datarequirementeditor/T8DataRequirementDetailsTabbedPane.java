package com.pilog.t8.ui.datarequirementeditor;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.ui.tabbedpane.T8TabbedPaneUI;
import java.awt.Insets;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementDetailsTabbedPane extends JTabbedPane
{
    private final T8ComponentController controller;
    private final T8Context context;
    private final TabChangeListener tabListener;
    private final T8ConfigurationManager configurationManager;

    public T8DataRequirementDetailsTabbedPane(T8DataRequirementEditor parentEditor)
    {
        this.controller = parentEditor.getController();
        this.context = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        UIManager.put("TabbedPane.contentOpaque", true);
        this.setOpaque(true);
        this.setBorder(null);
        this.tabListener = new TabChangeListener();
        this.setFont(LAFConstants.CONTENT_FONT);
        this.setForeground(LAFConstants.CONTENT_HEADER_TEXT_COLOR);
        this.setBorder(new EmptyBorder(0, 0, 0, 0));
        this.setFont(LAFConstants.CONTENT_HEADER_FONT);
        setupComponent();
    }

    private void setupComponent()
    {
        T8LookAndFeelManager lafManager;
        T8TabbedPaneUI tabbedPaneUI;

        // Get the look and feel manager.
        lafManager = controller.getClientContext().getConfigurationManager().getLookAndFeelManager(context);

        // Initialize painter for painting the default background of the tabbed pane.
        tabbedPaneUI = new T8TabbedPaneUI();
        tabbedPaneUI.setBackgroundPainter(lafManager.getContentHeaderBackgroundPainter());
        tabbedPaneUI.setBackgroundSelectedPainter(lafManager.getContentHeaderBackgroundSelectedPainter());
        setUI(tabbedPaneUI);
    }

    @Override
    public Insets getInsets()
    {
        return new Insets(0, 0, 0, 0);
    }

    private String translate(String text)
    {
        return configurationManager.getUITranslation(context, text);
    }

    private class TabChangeListener implements ChangeListener
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
        }
    }
}
