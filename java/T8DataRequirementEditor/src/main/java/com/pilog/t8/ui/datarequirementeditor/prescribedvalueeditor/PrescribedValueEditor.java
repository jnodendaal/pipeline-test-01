package com.pilog.t8.ui.datarequirementeditor.prescribedvalueeditor;

import com.pilog.t8.data.document.T8ConceptTerminology;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public interface PrescribedValueEditor
{
    public boolean canImport(List<T8ConceptTerminology> concepts);
    public void insertPrescribedValues(List<T8ConceptTerminology> concepts) throws Exception;
}
