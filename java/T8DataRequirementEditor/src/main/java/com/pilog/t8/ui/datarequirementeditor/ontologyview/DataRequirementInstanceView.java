package com.pilog.t8.ui.datarequirementeditor.ontologyview;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.definition.ui.datarequirementeditor.T8DataRequirementEditorDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeDefinition;
import com.pilog.t8.ui.datarequirementeditor.T8DataRequirementEditor;
import com.pilog.t8.ui.tree.T8Tree;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.JPanel;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class DataRequirementInstanceView extends JPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(DataRequirementInstanceView.class);

    private final T8DataRequirementEditor parentEditor;
    private final T8ConfigurationManager configurationManager;
    private final T8ComponentController controller;
    private final T8Context context;
    private T8Tree dataRequirementInstanceTree;

    public DataRequirementInstanceView(T8DataRequirementEditor parentEditor)
    {
        //Set the component controller and session context for translations of UI components
        this.controller = parentEditor.getController();
        this.configurationManager = this.controller.getClientContext().getConfigurationManager();
        this.context = parentEditor.getController().getContext();

        initComponents();
        this.parentEditor = parentEditor;
        addDataRequirementInstanceTree();
    }

    public void setToolBarVisible(boolean visible)
    {
        jToolBarMain.setVisible(visible);
    }

    /**
     * Because this component implements the T8ComponentController interface it
     * is responsible for stopping the components it controls when required.
     */
    public void stopComponent()
    {
        if (dataRequirementInstanceTree != null) dataRequirementInstanceTree.stopComponent();
    }

    public String getSelectedDataRequirementInstanceID()
    {
        List<T8DataEntity> selectedEntities;

        selectedEntities = dataRequirementInstanceTree.getSelectedDataEntities();
        if (selectedEntities.size() > 0)
        {
            T8DataEntity selectedEntity;

            selectedEntity = selectedEntities.get(0);
            return (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_CONCEPT_ID);
        }
        else return null;
    }

    public String getSelectedDataRequirementInstanceODTID()
    {
        List<T8DataEntity> selectedEntities;

        selectedEntities = dataRequirementInstanceTree.getSelectedDataEntities();
        if (selectedEntities.size() > 0)
        {
            T8DataEntity selectedEntity;
            String ocId;

            selectedEntity = selectedEntities.get(0);
            ocId = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_ODT_ID);

            // If the ODT_ID selected is the Group for Data Types (Groups) then we have to use the concept select (the actual group).
            if (T8PrimaryOntologyDataType.ONTOLOGY_DATA_TYPES.getDataTypeID().equals(ocId))
            {
                ocId = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_CONCEPT_ID);
                return ocId;
            }
            else
            {
                return ocId;
            }
        }
        else return null;
    }

    private String translate(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    private void addDataRequirementInstanceTree()
    {
        try
        {
            T8TreeDefinition treeDefinition;

            treeDefinition = ((T8DataRequirementEditorDefinition)parentEditor.getComponentDefinition()).getDataRequirementInstanceTreeDefinition();
            if (treeDefinition != null)
            {
                dataRequirementInstanceTree = (T8Tree)treeDefinition.getNewComponentInstance(controller);
                dataRequirementInstanceTree.initializeComponent(null);
                dataRequirementInstanceTree.getTree().setTransferHandler(new ConceptTreeTransferHandler());
                dataRequirementInstanceTree.getTree().setDragEnabled(true);
                dataRequirementInstanceTree.getTree().addMouseListener(new DRTreeMouseListener(dataRequirementInstanceTree));
                add(dataRequirementInstanceTree, java.awt.BorderLayout.CENTER);
                dataRequirementInstanceTree.startComponent();
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while loading DR Instance tree.", e);
        }
    }

    private void refreshData()
    {
        dataRequirementInstanceTree.rebuildTree();
    }

    private void addDataRequirementInstance()
    {
        String ocId;

        ocId = getSelectedDataRequirementInstanceODTID();
        if (ocId != null)
        {
            String drId;

            // Allow the user to select a Data Requirement from which to create the new Data Requirement Instance.
            drId = DataRequirementSelectionDialog.getSelectedDataRequirementID(SwingUtilities.getWindowAncestor(this), parentEditor, "Please select a Data Requirement from which to create a new Instance.");
            if (drId != null)
            {
                String newDRInstanceTerm;

                newDRInstanceTerm = JOptionPane.showInputDialog(SwingUtilities.getWindowAncestor(this), "Please enter a title for the new Data Requirement", "New Data Requirement", JOptionPane.INFORMATION_MESSAGE);
                if (!Strings.isNullOrEmpty(newDRInstanceTerm))
                {
                    boolean independent;

                    independent = (JOptionPane.showConfirmDialog(SwingUtilities.getWindowAncestor(this), "Is the new Instance '" + newDRInstanceTerm + "' independent?", "Instance Independence", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);
                    T8OntologyClientApi ontApi;
                    T8DataRequirementClientApi drqApi;
                    T8OrganizationOntologyFactory factory;
                    T8OntologyConcept drInstanceConcept;

                    // Create the ontology for the new Data Requirement Instance.
                    ontApi = parentEditor.getOntologyApi();
                    drqApi = parentEditor.getDataRequirementApi();
                    factory = ontApi.getOntologyFactory();
                    factory.setConceptODTID(ocId);
                    factory.setLanguageID(context.getSessionContext().getContentLanguageIdentifier());
                    drInstanceConcept = factory.constructConcept(T8OntologyConceptType.DATA_REQUIREMENT_INSTANCE);
                    factory.addTerm(drInstanceConcept, newDRInstanceTerm.toUpperCase());

                    // Save the new instance concept.
                    try
                    {
                        drqApi.insertDataRequirementInstance(drInstanceConcept, drId, independent);

                        // Refresh the tree data to reflect the changes.
                        refreshData();
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while inserting new DR Instance Concept.", e);
                        JOptionPane.showMessageDialog(parentEditor, "An unexpected error prevented successful addition of the new Instance.", "Operation Failure", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }
        else JOptionPane.showMessageDialog(parentEditor, translate("Please select a valid Group to which the new Data Requirement Instance will be added."), translate("Invalid DR Instance Group Selection"), JOptionPane.ERROR_MESSAGE);
    }

    private void deleteSelectedDataRequirementInstance()
    {
        String drInstanceID;

        // Get the selected DR Instance ID.
        drInstanceID = getSelectedDataRequirementInstanceID();
        if (drInstanceID != null)
        {
            int confirmation;

            confirmation = JOptionPane.showConfirmDialog(SwingUtilities.getWindowAncestor(this), "Are you sure you want to delete the selected instance?", "Deletion Confirmation", JOptionPane.YES_NO_OPTION);
            if (confirmation == JOptionPane.YES_OPTION)
            {
                T8DataRequirementClientApi drqApi;

                // Create the ontology for the new Data Requirement Instance.
                drqApi = parentEditor.getDataRequirementApi();

                // Save the new instance concept.
                try
                {
                    drqApi.deleteDataRequirementInstance(drInstanceID, true);

                    // Refresh the tree data to reflect the changes.
                    refreshData();
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while deleting new DR Instance: " + drInstanceID, e);
                    JOptionPane.showMessageDialog(parentEditor, "An unexpected error prevented successful deletion of the Instance.", "Operation Failure", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else JOptionPane.showMessageDialog(parentEditor, "No Instance selected for deletion.", "Invalid Operation", JOptionPane.ERROR_MESSAGE);
    }

    private class DRTreeMouseListener implements MouseListener
    {
        private final T8Tree tree;

        private DRTreeMouseListener(T8Tree tree)
        {
            this.tree = tree;
        }

        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.getClickCount() == 2)
            {
                List<T8DataEntity> selectedEntities;

                selectedEntities = tree.getSelectedDataEntities();
                if (selectedEntities.size() > 0)
                {
                    T8DataEntity selectedEntity;
                    String drID;

                    selectedEntity = selectedEntities.get(0);
                    drID = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_CONCEPT_ID);
//                    if (drID != null)
//                    {
//                        loadDataRequirement(drID);
//                    }
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefreshPalette = new javax.swing.JButton();
        jButtonAddInstance = new javax.swing.JButton();
        jButtonDeleteInstance = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);
        jToolBarMain.setOpaque(false);

        jButtonRefreshPalette.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/refreshIcon.png"))); // NOI18N
        jButtonRefreshPalette.setText("Refresh");
        jButtonRefreshPalette.setFocusable(false);
        jButtonRefreshPalette.setOpaque(false);
        jButtonRefreshPalette.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefreshPalette.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshPaletteActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefreshPalette);

        jButtonAddInstance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAddInstance.setText("Add Instance");
        jButtonAddInstance.setFocusable(false);
        jButtonAddInstance.setOpaque(false);
        jButtonAddInstance.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddInstance.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddInstanceActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAddInstance);

        jButtonDeleteInstance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/cutIcon.png"))); // NOI18N
        jButtonDeleteInstance.setText("Delete Instance");
        jButtonDeleteInstance.setFocusable(false);
        jButtonDeleteInstance.setOpaque(false);
        jButtonDeleteInstance.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDeleteInstance.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDeleteInstanceActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDeleteInstance);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshPaletteActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshPaletteActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshPaletteActionPerformed
        refreshData();
    }//GEN-LAST:event_jButtonRefreshPaletteActionPerformed

    private void jButtonAddInstanceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddInstanceActionPerformed
    {//GEN-HEADEREND:event_jButtonAddInstanceActionPerformed
        addDataRequirementInstance();
    }//GEN-LAST:event_jButtonAddInstanceActionPerformed

    private void jButtonDeleteInstanceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteInstanceActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteInstanceActionPerformed
        deleteSelectedDataRequirementInstance();
    }//GEN-LAST:event_jButtonDeleteInstanceActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddInstance;
    private javax.swing.JButton jButtonDeleteInstance;
    private javax.swing.JButton jButtonRefreshPalette;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
