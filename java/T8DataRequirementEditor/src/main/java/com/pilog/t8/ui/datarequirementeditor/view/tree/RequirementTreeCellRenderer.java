package com.pilog.t8.ui.datarequirementeditor.view.tree;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.SectionRequirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.laf.LAFConstants;
import com.pilog.t8.utilities.strings.Strings;
import java.awt.Component;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

/**
 * @author Bouwer du Preez
 */
public class RequirementTreeCellRenderer extends javax.swing.JPanel implements TreeCellRenderer
{
    private static final T8Logger LOGGER = T8Log.getLogger(RequirementTreeCellRenderer.class);

    private final TerminologyProvider terminologyProvider;
    private final T8ConfigurationManager configurationManager;
    private final T8Context context;

    public RequirementTreeCellRenderer(T8Context context, TerminologyProvider terminologyProvider)
    {
        this.configurationManager = context.getClientContext().getConfigurationManager();
        this.context = context;

        initComponents();
        this.jLabelIcon.setVisible(false);
        this.terminologyProvider = terminologyProvider;
    }

    private String getTranslatedString(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        DefaultMutableTreeNode treeNode;
        Object treeNodeObject;

        treeNode = (DefaultMutableTreeNode)value;
        treeNodeObject = treeNode.getUserObject();
        if (treeNodeObject instanceof DataRequirement)
        {
            DataRequirement requirement;
            StringBuffer labelText;

            requirement = (DataRequirement)treeNodeObject;
            labelText = new StringBuffer();
            labelText.append(getTerm(requirement.getConceptID()));
            labelText.append(" (");
            labelText.append(getTranslatedString("Class"));
            labelText.append(": ");
            labelText.append(getTerm(requirement.getClassConceptID()));
            labelText.append(")");

            jLabelNodeType.setText(getTranslatedString("Data Requirement").concat(":"));
            jLabelNodeName.setText(labelText.toString());
            jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.PROPERTY_BLUE);
            jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);
            jPanelMain.validate();
            return this;
        }
        else if (treeNodeObject instanceof SectionRequirement)
        {
            SectionRequirement requirement;

            requirement = (SectionRequirement)treeNodeObject;
            jLabelNodeType.setText(getTranslatedString("Section").concat(":"));
            jLabelNodeName.setText(getTerm(requirement.getConceptID()));
            jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.PROPERTY_BLUE);
            jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);
            jPanelMain.validate();
            return this;
        }
        else if (treeNodeObject instanceof PropertyRequirement)
        {
            PropertyRequirement requirement;

            requirement = (PropertyRequirement)treeNodeObject;
            jLabelNodeType.setText(getTranslatedString("Property").concat(":"));
            jLabelNodeName.setText(getTerm(requirement.getConceptID())  + (requirement.isCharacteristic() ? " *" : ""));
            jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.PROPERTY_BLUE);
            jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);
            jPanelMain.validate();
            return this;
        }
        else if (treeNodeObject instanceof ValueRequirement)
        {
            ValueRequirement requirement;
            RequirementType requirementType;
            String valueString;

            requirement = (ValueRequirement)treeNodeObject;
            requirementType = requirement.getRequirementType();
            if (requirementType == RequirementType.CONCEPT_TYPE)
            {
                valueString = "" + T8OntologyConceptType.getConceptType(requirement.getValue());
            }
            else valueString = requirementType.isConceptRequirementValue() ? getTerm(requirement.getValue()) : requirement.getValue();

            jLabelNodeName.setText(requirement.getValue() != null ? ": " + valueString : "");
            jLabelNodeType.setText(requirement.getRequirementType().getDisplayName());
            jPanelMain.setBackground(selected ? LAFConstants.SELECTION_ORANGE : LAFConstants.PROPERTY_BLUE);
            jPanelMain.setBorder(selected ? LAFConstants.SELECTED_NODE_BORDER : LAFConstants.NORMAL_NODE_BORDER);
            jPanelMain.validate();
            return this;
        }
        else
        {
            jLabelNodeType.setText(null);
            jLabelNodeName.setText(getTranslatedString("Invalid Node").concat(": ") + treeNodeObject);
            jPanelMain.validate();
            return this;
        }
    }

    private String getTerm(String conceptID)
    {
        if (!Strings.isNullOrEmpty(conceptID))
        {
            try
            {
                return terminologyProvider.getTerm(null, conceptID);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while fetching concept term from provider: " + conceptID, e);
                return conceptID;
            }
        }
        else return conceptID;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jLabelIcon = new javax.swing.JLabel();
        jLabelNodeType = new javax.swing.JLabel();
        jLabelNodeName = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        jPanelMain.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanelMain.setLayout(new java.awt.GridBagLayout());
        jPanelMain.add(jLabelIcon, new java.awt.GridBagConstraints());

        jLabelNodeType.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelNodeType.setText("Node Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanelMain.add(jLabelNodeType, gridBagConstraints);

        jLabelNodeName.setText("Node Name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 5);
        jPanelMain.add(jLabelNodeName, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
        add(jPanelMain, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelIcon;
    private javax.swing.JLabel jLabelNodeName;
    private javax.swing.JLabel jLabelNodeType;
    private javax.swing.JPanel jPanelMain;
    // End of variables declaration//GEN-END:variables
}
