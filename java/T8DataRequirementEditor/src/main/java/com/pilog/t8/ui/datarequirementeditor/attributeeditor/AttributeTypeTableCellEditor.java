package com.pilog.t8.ui.datarequirementeditor.attributeeditor;

import com.pilog.t8.data.document.requirementtype.RequirementAttribute.RequirementAttributeType;
import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * @author Bouwer du Preez
 */
public class AttributeTypeTableCellEditor extends AbstractCellEditor implements TableCellEditor
{
    private final JComboBox comboBox;

    public AttributeTypeTableCellEditor()
    {
        comboBox = new JComboBox(RequirementAttributeType.values());
    }

    @Override
    public Object getCellEditorValue()
    {
        return comboBox.getSelectedItem();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        comboBox.setSelectedItem(value);
        return comboBox;
    }
}
