package com.pilog.t8.ui.datarequirementeditor.view.tree;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.T8ConceptTerminology;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class RequirementTreeTransferHandler extends TransferHandler
{
    private static final T8Logger logger = T8Log.getLogger(RequirementTreeTransferHandler.class);

    private final RequirementTree tree;

    public RequirementTreeTransferHandler(RequirementTree tree)
    {
        this.tree = tree;
    }

    @Override
    public boolean canImport(RequirementTreeTransferHandler.TransferSupport support)
    {
        if (!support.isDataFlavorSupported(TransferableConceptTerminologyList.CONCEPT_TERMINOLOGY_LIST_FLAVOR) || !support.isDrop())
        {
            return false;
        }
        else
        {
            JTree.DropLocation dropLocation;

            dropLocation = (JTree.DropLocation) support.getDropLocation();
            return dropLocation.getPath() != null;
        }
    }

    @Override
    public boolean importData(RequirementTreeTransferHandler.TransferSupport support)
    {
        if (!canImport(support))
        {
            return false;
        }
        else
        {
            JTree.DropLocation dropLocation;
            DefaultMutableTreeNode dropNode;
            TreePath path;
            Transferable transferable;
            List<T8ConceptTerminology> transferredData;
            int childIndex;

            dropLocation = (JTree.DropLocation) support.getDropLocation();
            path = dropLocation.getPath();
            dropNode = (DefaultMutableTreeNode)path.getLastPathComponent();
            transferable = support.getTransferable();

            try
            {
                transferredData = (List<T8ConceptTerminology>)transferable.getTransferData(TransferableConceptTerminologyList.CONCEPT_TERMINOLOGY_LIST_FLAVOR);
            }
            catch (UnsupportedFlavorException | IOException e)
            {
                logger.log(e);
                return false;
            }

            childIndex = dropLocation.getChildIndex();
            if (childIndex == -1)
            {
                childIndex = dropNode.getChildCount();
            }

            try
            {
                tree.addRequirementsToNode(dropNode, transferredData, childIndex);
                return true;
            }
            catch (Exception e)
            {
                logger.log("Exception while adding transferred data to requirement tree.", e);
                return false;
            }
        }
    }
}
