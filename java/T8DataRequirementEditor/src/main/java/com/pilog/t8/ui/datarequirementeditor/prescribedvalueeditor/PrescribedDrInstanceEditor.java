package com.pilog.t8.ui.datarequirementeditor.prescribedvalueeditor;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarequirement.PrescribedValue;
import com.pilog.t8.data.document.datarequirement.Requirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.api.T8PrescribedValueClientApi;
import com.pilog.t8.definition.ui.datarequirementeditor.T8DataRequirementEditorDefinition;
import com.pilog.t8.definition.ui.table.T8TableDefinition;
import com.pilog.t8.ui.datarequirementeditor.T8DataRequirementContainer;
import com.pilog.t8.ui.datarequirementeditor.T8DataRequirementEditor;
import com.pilog.t8.ui.table.T8Table;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import java.util.List;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class PrescribedDrInstanceEditor extends JPanel implements PrescribedValueEditor
{
    private final T8DataRequirementEditor requirementEditor;
    private final T8DataRequirementContainer requirementContainer;
    private final T8DataRequirementEditorDefinition parentEditorDefinition;
    private final T8ComponentController controller;
    private final T8PrescribedValueClientApi pvApi;
    private final T8ConfigurationManager configurationManager;
    private final T8Context context;
    private final PrescribedValueTableTransferHandler transferHandler;
    private DocumentReferenceRequirement requirement;
    private T8Table prescribedValueTable;
    private String entityId;

    public PrescribedDrInstanceEditor(T8DataRequirementEditor parentEditor, T8DataRequirementContainer container)
    {
        this.requirementEditor = parentEditor;
        this.requirementContainer = container;
        this.parentEditorDefinition = (T8DataRequirementEditorDefinition)parentEditor.getComponentDefinition();
        this.pvApi = parentEditor.getPrescribedValueApi();
        this.controller = parentEditor.getController();
        this.context = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.transferHandler = new PrescribedValueTableTransferHandler(this);

        initComponents();
        createPrescribedValueTable();
    }

    private void createPrescribedValueTable()
    {
        try
        {
            T8TableDefinition tableDefinition;

            tableDefinition = ((T8DataRequirementEditorDefinition)requirementEditor.getComponentDefinition()).getPrescribedDrInstanceTableDefinition();
            if (tableDefinition != null)
            {
                entityId = tableDefinition.getDataEntityIdentifier();
                prescribedValueTable = (T8Table)tableDefinition.getNewComponentInstance(controller);
                prescribedValueTable.initializeComponent(null);
                prescribedValueTable.getTable().setTransferHandler(transferHandler);
                prescribedValueTable.getViewport().setTransferHandler(transferHandler);
                add(prescribedValueTable, BorderLayout.CENTER);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while loading prescribed value table.", e);
        }
    }

    public void startComponent()
    {
        prescribedValueTable.startComponent();
    }

    public void stopComponent()
    {
        prescribedValueTable.stopComponent();
    }

    public void refresh()
    {
        prescribedValueTable.refreshData();
    }

    public void setSelectedRequirement(Requirement selectedRequirement)
    {
        T8DataFilter filter;
        Map<String, Object> keyMap;
        String filterDrId;
        String filterPropertyId;
        String filterFieldId;

        // Determine the filter values based on the input requirement.
        if (selectedRequirement instanceof DocumentReferenceRequirement)
        {
            requirement = (DocumentReferenceRequirement)selectedRequirement;
            filterDrId = requirement.getDataRequirementID();
            filterPropertyId = requirement.getPropertyID();
            filterFieldId = requirement.getFieldID();
        }
        else
        {
            requirement = null;
            filterDrId = null;
            filterPropertyId = null;
            filterFieldId = null;
        }

        // Create the data filter.
        keyMap = new HashMap<>();
        keyMap.put(entityId + EF_DR_ID, filterDrId);
        keyMap.put(entityId + EF_PROPERTY_ID, filterPropertyId);
        if (filterFieldId != null) keyMap.put(entityId + EF_FIELD_ID, filterFieldId);
        filter = new T8DataFilter(entityId, keyMap);

        // Set the filter on the table and refresh the data.
        prescribedValueTable.setPrefilter("SELECTION_FILTER", filter);
        prescribedValueTable.refreshData();
    }

    @Override
    public void insertPrescribedValues(List<T8ConceptTerminology> concepts) throws Exception
    {
        if (requirement != null)
        {
            if (canImport(concepts))
            {
                List<PrescribedValue> prescribedValues;
                String drId;
                String propertyId;
                String fieldId;

                drId = requirement.getDataRequirementID();
                propertyId = requirement.getPropertyID();
                fieldId = requirement.getFieldID();

                prescribedValues = new ArrayList<>();
                for (T8ConceptTerminology concept : concepts)
                {
                    PrescribedValue prescribedValue;

                    prescribedValue = new PrescribedValue(RequirementType.DOCUMENT_REFERENCE, T8IdentifierUtilities.createNewGUID());
                    prescribedValue.setOrganizationID(context.getSessionContext().getRootOrganizationIdentifier());
                    prescribedValue.setDataRequirementID(drId);
                    prescribedValue.setPropertyID(propertyId);
                    prescribedValue.setFieldID(fieldId);
                    prescribedValue.setConceptID(concept.getConceptId());
                    prescribedValues.add(prescribedValue);
                }

                // Insert the new prescribed values.
                pvApi.insertPrescribedValues(prescribedValues);

                // Refresh the table to reflect the changes.
                refresh();
            }
        }
    }

    @Override
    public boolean canImport(List<T8ConceptTerminology> concepts)
    {
        if (concepts != null)
        {
            for (T8ConceptTerminology concept : concepts)
            {
                if (concept.getConceptType() != T8OntologyConceptType.DATA_REQUIREMENT_INSTANCE)
                {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
