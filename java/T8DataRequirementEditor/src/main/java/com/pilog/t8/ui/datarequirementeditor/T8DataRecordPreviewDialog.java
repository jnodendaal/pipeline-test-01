package com.pilog.t8.ui.datarequirementeditor;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.api.T8OrganizationClientApi;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.DocumentHandler;
import com.pilog.t8.data.document.TerminologyProvider;
import com.pilog.t8.data.document.datarecord.DataRecord;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.DataRequirementInstance;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.definition.ui.datarecordeditor.T8DataRecordEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datarecordeditor.T8DataRecordEditor;
import java.awt.Window;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8DataRecordPreviewDialog extends javax.swing.JDialog
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRecordPreviewDialog.class);

    private T8DataRecordEditor dataRecordEditor;
    private final T8DataRequirementEditor parentEditor;
    private final T8Context context;
    private final TerminologyProvider terminologyProvider;
    private final T8OrganizationClientApi orgApi;
    private final T8OntologyClientApi ontApi;
    private final T8DataRequirementClientApi drqApi;
    private final DataRequirement dataRequirement;
    private final T8DataRecordEditorDefinition previewEditorDefinition;
    private final String previewDRInstanceID;

    private T8DataRecordPreviewDialog(Window parentWindow, T8DataRequirementEditor parentEditor, DataRequirement dataRequirement, T8DataRecordEditorDefinition previewEditorDefinition, String previewDrInstanceId)
    {
        super(parentWindow, ModalityType.DOCUMENT_MODAL);
        initComponents();
        this.parentEditor = parentEditor;
        this.orgApi = parentEditor.getOrganizationApi();
        this.ontApi = parentEditor.getOntologyApi();
        this.drqApi = parentEditor.getDataRequirementApi();
        this.context = parentEditor.getController().getContext();
        this.terminologyProvider = parentEditor.getTerminologyProvider();
        this.dataRequirement = dataRequirement;
        this.previewEditorDefinition = previewEditorDefinition;
        this.previewDRInstanceID = (previewDrInstanceId != null ? previewDrInstanceId : T8IdentifierUtilities.createNewGUID());

        setSize(1024, 768);
        setLocationRelativeTo(null);
        rebuildDocumentComponent();
    }

    private void rebuildDocumentComponent()
    {
        CachedTerminologyProvider recordEditorTerminologyProvider;
        T8OrganizationStructure organizationStructure;
        DataRequirementInstance testDRInstance;
        T8SessionContext sessionContext;
        DataRecord testRecord;
        HashSet<String> concepts;
        T8OrganizationOntologyFactory factory;
        List<String> drInstanceIds;
        List<String> drIds;
        List<String> orgIds;
        List<String> languageIds;

        // Get the concepts required by the data requirement.
        sessionContext = context.getSessionContext();
        organizationStructure = orgApi.getOrganizationStructure();
        concepts = DocumentHandler.getAllConceptIds(dataRequirement);
        drInstanceIds = Arrays.asList(previewDRInstanceID);
        drIds = Arrays.asList(dataRequirement.getConceptID());
        orgIds = organizationStructure.getOrganizationPathIDList(sessionContext.getOrganizationIdentifier());
        languageIds = Arrays.asList(sessionContext.getContentLanguageIdentifier());

        // Create a test DR Instance to use for the preview.
        factory = ontApi.getOntologyFactory();
        factory.setConceptODTID(T8PrimaryOntologyDataType.DATA_REQUIREMENT_INSTANCES.getDataTypeID());
        testDRInstance = new DataRequirementInstance(previewDRInstanceID, dataRequirement);
        testDRInstance.setOntology(factory.constructConcept(T8OntologyConceptType.DATA_REQUIREMENT_INSTANCE, previewDRInstanceID));

        // Fetch DR Comments.
        try
        {
            testDRInstance.setComments(drqApi.retrieveDataRequirementComments(orgIds, languageIds, drIds, drInstanceIds, null));
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while fetching DR Comments for DR: " + dataRequirement, e);
        }

        // Create a test record to use for the preview.
        testRecord = new DataRecord(testDRInstance);
        testRecord.setID(T8IdentifierUtilities.createNewGUID());

        dataRecordEditor = new T8DataRecordEditor(previewEditorDefinition, parentEditor.getController());
        dataRecordEditor.initializeComponent(null);

        // We don't want the record editor to re-load all of the terminology we already have, so we transfer it to the record editor's terminology provider.
        recordEditorTerminologyProvider = dataRecordEditor.getTerminologyProvider();
        for (String conceptID : concepts)
        {
            try
            {
                recordEditorTerminologyProvider.addTerminology(terminologyProvider.getTerminology(null, conceptID));
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while fetching terminology from provider for concept: " + conceptID, e);
            }
        }

        jPanelContent.removeAll();
        jPanelContent.add(dataRecordEditor, java.awt.BorderLayout.CENTER);
        jPanelContent.revalidate();

        dataRecordEditor.addDocument(testRecord);
        dataRecordEditor.showDocument(testRecord.getID());
    }

    public static final void showDataEntry(Window parentWindow, T8DataRequirementEditor parentEditor, DataRequirement dataRequirement, T8DataRecordEditorDefinition previewEditorDefinition, String previewDRInstanceID)
    {
        T8DataRecordPreviewDialog dialog;

        dialog = new T8DataRecordPreviewDialog(parentWindow, parentEditor, dataRequirement, previewEditorDefinition, previewDRInstanceID);
        dialog.setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelContent.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        getContentPane().add(jPanelContent, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelContent;
    // End of variables declaration//GEN-END:variables
}
