package com.pilog.t8.ui.datarequirementeditor;

import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.T8ConceptTerminologyList;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementEditorDataHandler
{
    private final T8Context context;
    private final T8DataRequirementClientApi drqApi;

    public T8DataRequirementEditorDataHandler(T8Context context, T8DataRequirementClientApi drqApi)
    {
        this.context = context;
        this.drqApi = drqApi;
    }

    public DataRequirement loadDataRequirement(String drId, CachedTerminologyProvider terminologyProvider) throws Exception
    {
        DataRequirement dataRequirement;

        // Retrieve the Data Requirement and add all of the retrieved terminology to the cached provider.
        dataRequirement = drqApi.retrieveDataRequirement(drId, context.getSessionContext().getContentLanguageIdentifier(), true, true);
        if (dataRequirement != null)
        {
            T8ConceptTerminologyList terminologyList;

            // Get the terminology loaded along with the Data Requirement.
            terminologyList = new T8ConceptTerminologyList();
            dataRequirement.addContentTerminology(terminologyList);

            // Cache the retrieved terminology.
            terminologyProvider.addTerminology(terminologyList);
            return dataRequirement;
        }
        else throw new Exception("Request Data Requirement not found: " + drId);
    }
}
