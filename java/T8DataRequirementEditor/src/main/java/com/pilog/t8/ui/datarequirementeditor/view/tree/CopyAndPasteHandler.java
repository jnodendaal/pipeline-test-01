package com.pilog.t8.ui.datarequirementeditor.view.tree;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.document.datarequirement.Requirement;
import com.pilog.t8.utilities.serialization.SerializationUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * @author Bouwer du Preez
 */
class CopyAndPasteHandler
{
    private static final T8Logger logger = T8Log.getLogger(CopyAndPasteHandler.class);

    private static final List<Requirement> copiedRequirements;

    static
    {
        copiedRequirements = new ArrayList<>();
    }

    static void copyRequirement(List<DefaultMutableTreeNode> nodesToCopy)
    {
        synchronized (copiedRequirements)
        {
            Requirement requirement;

            copiedRequirements.clear();
            for (DefaultMutableTreeNode nodeToCopy : nodesToCopy)
            {
                try
                {
                    requirement = (Requirement)nodeToCopy.getUserObject();
                    copiedRequirements.add((Requirement)SerializationUtilities.copyObjectBySerialization(requirement));
                }
                catch (IOException | ClassNotFoundException ex)
                {
                    logger.log("Failed to copy the specified requirement", ex);
                }
            }
        }
    }

    static List<Requirement> getCopiedRequirements()
    {
        synchronized(copiedRequirements)
        {
            ArrayList<Requirement> returnList;

            returnList = new ArrayList<>(copiedRequirements);
            copiedRequirements.clear();

            return returnList;
        }
    }

    static boolean hasCopiedRequirements()
    {
        return !copiedRequirements.isEmpty();
    }

    private CopyAndPasteHandler() {}
}
