package com.pilog.t8.ui.datarequirementeditor.view.tree;

import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.data.document.datarequirement.value.ControlledConceptRequirement;
import com.pilog.t8.data.document.datarequirement.value.OntologyClassRequirement;
import com.pilog.t8.data.document.datarequirement.value.LocalizedStringListRequirement;
import com.pilog.t8.data.document.datarequirement.value.LocalizedStringRequirement;
import com.pilog.t8.data.document.datarequirement.value.LowerBoundNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredNumberRequirement;
import com.pilog.t8.data.document.datarequirement.value.MeasuredRangeRequirement;
import com.pilog.t8.data.document.datarequirement.value.RealRequirement;
import com.pilog.t8.data.document.datarequirement.value.StringRequirement;
import com.pilog.t8.data.document.datarequirement.value.UpperBoundNumberRequirement;

/**
 * @author Bouwer du Preez
 */
public class DataTypeConstructor
{
    public static final ValueRequirement buildDataTypeRequirement(RequirementType requirementType)
    {
        if (RequirementType.MEASURED_RANGE.equals(requirementType))
        {
            return buildMeasureRangeType();
        }
        else if (RequirementType.MEASURED_NUMBER.equals(requirementType))
        {
            return buildMeasureNumberType();
        }
        else if (RequirementType.CONTROLLED_CONCEPT.equals(requirementType))
        {
            return buildControlledValueType();
        }
        else if (RequirementType.LOCALIZED_TEXT_TYPE.equals(requirementType))
        {
            return buildLocalizedTextType();
        }
        else
        {
            return ValueRequirement.createValueRequirement(requirementType);
        }
    }

    public static final ValueRequirement buildLocalizedTextType()
    {
        ValueRequirement localStringType;
        ValueRequirement localizedTextType;

        localizedTextType = new LocalizedStringListRequirement();
        localStringType = new LocalizedStringRequirement();
        localStringType.addSubRequirement(new StringRequirement());
        localizedTextType.addSubRequirement(localStringType);

        return localizedTextType;
    }

    public static final ValueRequirement buildControlledValueType()
    {
        ControlledConceptRequirement requirement;

        requirement = new ControlledConceptRequirement();
        requirement.addSubRequirement(new OntologyClassRequirement());
        return requirement;
    }

    public static final ValueRequirement buildMeasureRangeType()
    {
        ValueRequirement lowerBoundRequirement;
        ValueRequirement upperBoundRequirement;
        ValueRequirement rangeRequirement;

        rangeRequirement = new MeasuredRangeRequirement();
        lowerBoundRequirement = new LowerBoundNumberRequirement();
        upperBoundRequirement = new UpperBoundNumberRequirement();
        lowerBoundRequirement.addSubRequirement(new RealRequirement());
        upperBoundRequirement.addSubRequirement(new RealRequirement());

        rangeRequirement.addSubRequirement(lowerBoundRequirement);
        rangeRequirement.addSubRequirement(upperBoundRequirement);

        return rangeRequirement;
    }

    public static final ValueRequirement buildMeasureNumberType()
    {
        ValueRequirement measureNumberRequirement;

        measureNumberRequirement = new MeasuredNumberRequirement();
        measureNumberRequirement.addSubRequirement(new RealRequirement());

        return measureNumberRequirement;
    }
}
