package com.pilog.t8.ui.datarequirementeditor.view.tree;

import com.pilog.t8.data.document.T8ConceptTerminology;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class TransferableConceptTerminologyList implements Transferable
{
    private String concepts;
    private final ArrayList<T8ConceptTerminology> data;
    private final DataFlavor flavors[] = {CONCEPT_TERMINOLOGY_LIST_FLAVOR, DataFlavor.stringFlavor};
    public static final DataFlavor CONCEPT_TERMINOLOGY_LIST_FLAVOR = new DataFlavor(ArrayList.class, "Concept Terminology List");

    public TransferableConceptTerminologyList(ArrayList<T8ConceptTerminology> transferData)
    {
        data = transferData;
    }

    public TransferableConceptTerminologyList(ArrayList<T8ConceptTerminology> transferData, String concepts)
    {
        this.concepts = concepts;
        data = transferData;
    }

    @Override
    public synchronized Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException
    {
        if (isDataFlavorSupported(flavor))
        {
            if(flavor == CONCEPT_TERMINOLOGY_LIST_FLAVOR) return data;
            else if(flavor == DataFlavor.stringFlavor) return concepts;
            else throw new UnsupportedFlavorException(flavor);
        }
        else throw new UnsupportedFlavorException(flavor);
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor)
    {
        return ((flavor.getRepresentationClass() == ArrayList.class) && ("Concept Terminology List".equals(flavor.getHumanPresentableName())))
                || flavor == DataFlavor.stringFlavor;
    }

    @Override
    public synchronized DataFlavor[] getTransferDataFlavors()
    {
        return flavors;
    }
}
