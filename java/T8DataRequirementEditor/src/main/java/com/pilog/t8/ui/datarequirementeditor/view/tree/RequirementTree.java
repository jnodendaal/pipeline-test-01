package com.pilog.t8.ui.datarequirementeditor.view.tree;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.document.T8ConceptTerminology;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.PropertyRequirement;
import com.pilog.t8.data.document.datarequirement.Requirement;
import com.pilog.t8.data.document.datarequirement.ValueRequirement;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datarequirementeditor.view.tree.ModelHandler.ValueRequirementOption;
import com.pilog.t8.utilities.components.menu.MenuItem;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DropMode;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

/**
 * @author Bouwer du Preez
 */
public class RequirementTree extends JTree
{
    private DefaultTreeModel model;
    private DataRequirement dataRequirement;
    private final ModelHandler modelHandler;
    private final T8ConfigurationManager configurationManager;
    private final T8Context context;
    private final CachedTerminologyProvider terminologyProvider;

    public RequirementTree(T8Context context, CachedTerminologyProvider terminologyProvider)
    {
        this.terminologyProvider = terminologyProvider;
        this.setCellRenderer(new RequirementTreeCellRenderer(context, terminologyProvider));
        this.setRowHeight(-1);
        this.addMouseListener(new TreeMouseListener());
        this.addKeyListener(new TreeKeyListener());
        this.setTransferHandler(new RequirementTreeTransferHandler(this));
        this.setDragEnabled(true);
        this.setDropMode(DropMode.ON_OR_INSERT);
        this.modelHandler = new ModelHandler(this, null);
        this.configurationManager = context.getClientContext().getConfigurationManager();
        this.context = context;
        this.setOpaque(false);
        createNewModel();
    }

    public void setDataRequirement(DataRequirement dataRequirement)
    {
        this.dataRequirement = dataRequirement;
        createNewModel();
    }

    public ModelHandler getModelHandler()
    {
        return modelHandler;
    }

    public void refreshNodeContaining(Object requirementObject)
    {
        modelHandler.refreshNodeContaining(requirementObject);
    }

    public Object getSelectedObject()
    {
        TreePath selectionPath;

        selectionPath = this.getSelectionPath();
        if (selectionPath != null)
        {
            DefaultMutableTreeNode selectedNode;

            selectedNode = (DefaultMutableTreeNode)selectionPath.getLastPathComponent();
            return selectedNode.getUserObject();
        }
        else return null;
    }

    private String translate(String text)
    {
        return this.configurationManager.getUITranslation(context, text);
    }

    private void createNewModel()
    {
        model = modelHandler.createNewModel(dataRequirement);
        setModel(model);
        modelHandler.setTreeModel(model);
    }

    public void addRequirementsToNode(DefaultMutableTreeNode parentNode, List<T8ConceptTerminology> terminologyList, int index)
    {
        if (terminologyList != null) terminologyProvider.addTerminology(terminologyList);
        modelHandler.addConceptsToNode(parentNode, terminologyList, index);
    }

    private DefaultMutableTreeNode getNodeAtLocation(Point location)
    {
        TreePath locationPath;

        locationPath = getPathForLocation(location.x, location.y);
        return locationPath != null ? (DefaultMutableTreeNode)locationPath.getLastPathComponent() : null;
    }

    public void setSelectedNode(DefaultMutableTreeNode node)
    {
        clearSelection();
        if (node != null)
        {
            setSelectionPath(new TreePath(node.getPath()));
        }
    }

    private ArrayList<DefaultMutableTreeNode> getSelectedNodes()
    {
        ArrayList<DefaultMutableTreeNode> selectedNodes;
        TreePath[] selectedPaths;

        selectedNodes = new ArrayList<DefaultMutableTreeNode>();
        selectedPaths = getSelectionPaths();
        if (selectedPaths != null)
        {
            for (TreePath selectedPath : selectedPaths)
            {
                selectedNodes.add((DefaultMutableTreeNode)selectedPath.getLastPathComponent());
            }
        }

        return selectedNodes;
    }

    private void displayPopup(Point location)
    {
        DefaultMutableTreeNode locationNode;
        final ArrayList<DefaultMutableTreeNode> selectedNodes;
        TreePath locationPath;

        locationNode = getNodeAtLocation(location);
        locationPath = new TreePath(locationNode.getPath());
        if (!getSelectionModel().isPathSelected(locationPath))
        {
            this.setSelectionPath(locationPath);
        }

        selectedNodes = getSelectedNodes();
        {
            final DefaultMutableTreeNode selectedNode;
            final Requirement selectedRequirement;
            final List<ValueRequirementOption> valueRequirementOptions;
            JPopupMenu treePopupMenu;
            MenuItem menuItem;
            JMenu subMenu;

            selectedNode = selectedNodes.get(0);
            selectedRequirement = (Requirement)selectedNode.getUserObject();
            valueRequirementOptions = modelHandler.getValueRequirementAdditionOptions(selectedRequirement);

            // Create the ActionListener for the menu items.
            ActionListener menuListener = new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    Object value;
                    String code;

                    code = ((MenuItem)event.getSource()).getCode();
                    value = ((MenuItem)event.getSource()).getValue();
                    if (code.equals("SET_DATA_TYPE"))
                    {
                        RequirementType requirementType;

                        requirementType = (RequirementType)value;
                        for (DefaultMutableTreeNode node : selectedNodes)
                        {
                            modelHandler.setRequirementDataType(node.getUserObject(),requirementType);
                        }
                    }
                    else if (code.equals("DELETE_NODES"))
                    {
                        modelHandler.deleteNodes(selectedNodes);
                    }
                    else if (code.equals("COPY_NODE"))
                    {
                        CopyAndPasteHandler.copyRequirement(selectedNodes);
                    }
                    else if (code.equals("PASTE_REQUIREMENT"))
                    {
                        modelHandler.addRequirementsToNode(selectedNode, CopyAndPasteHandler.getCopiedRequirements());
                    }
                    else if (code.equals("ADD_VALUE_REQUIREMENT"))
                    {
                        ValueRequirement valueRequirementToAdd;

                        valueRequirementToAdd = (ValueRequirement)value;
                        modelHandler.addRequirementToNode(selectedNode, valueRequirementToAdd);
                    }
                }
            };

            // Create a new popup menu.
            treePopupMenu = new JPopupMenu();

            if (selectedNodes.size() == 1)
            {
                boolean canContainDataType;

                // Check whether or not the selected requirement can contain a sub-value requirement.
                if (selectedRequirement instanceof PropertyRequirement)
                {
                    canContainDataType = true;
                }
                else if (selectedRequirement instanceof ValueRequirement)
                {
                    RequirementType requirementType;

                    requirementType = selectedRequirement.getRequirementType();
                    if (RequirementType.FIELD_TYPE.equals(requirementType)) canContainDataType = true;
                    else canContainDataType = RequirementType.SET_TYPE.equals(requirementType);
                }
                else canContainDataType = false;

                // If the selected node can contain a sub-value requirement, add the option to adjust the data type.
                if (canContainDataType)
                {
                    subMenu = new JMenu(translate("Set Data Type"));
                    for (RequirementType requirementType : RequirementType.getRootDataTypes())
                    {
                        menuItem = new MenuItem(requirementType.getDisplayName(), "SET_DATA_TYPE", requirementType);
                        menuItem.addActionListener(menuListener);
                        subMenu.add(menuItem);
                    }
                    treePopupMenu.add(subMenu);
                }

                // Add any special case options to some of the value requirement types (such as masks/patterns to String types).
                if (valueRequirementOptions.size() > 0)
                {
                    subMenu = new JMenu("Add");
                    for (ValueRequirementOption option : valueRequirementOptions)
                    {
                        menuItem = new MenuItem(option.getDisplayName(), "ADD_VALUE_REQUIREMENT", option.getValueRequirement());
                        menuItem.addActionListener(menuListener);
                        subMenu.add(menuItem);
                    }
                    treePopupMenu.add(subMenu);
                }

                // If a copied requirement is available add the option to paste it.
                if (CopyAndPasteHandler.hasCopiedRequirements())
                {
                    menuItem = new MenuItem(translate("Paste (Best Effort)"), "PASTE_REQUIREMENT", null);
                    menuItem.addActionListener(menuListener);
                    treePopupMenu.add(menuItem);
                }

                if (modelHandler.canDeleteNode(selectedNode))
                {
                    // If the node can be copied, add the option (a node can be copied if it can be deleted).
                    menuItem = new MenuItem(translate("Copy"), "COPY_NODE", null);
                    menuItem.addActionListener(menuListener);
                    treePopupMenu.add(menuItem);

                    // If the node can be deleted, add the option.
                    menuItem = new MenuItem(translate("Delete Node"), "DELETE_NODES", null);
                    menuItem.setName("DELETE_NODES");
                    menuItem.addActionListener(menuListener);
                    treePopupMenu.add(menuItem);
                }
            }
            else if (selectedNodes.size() > 1)
            {
                if (allNodesContainClass(selectedNodes, PropertyRequirement.class))
                {
                    subMenu = new JMenu(translate("Set Data Type"));
                    for (RequirementType requirementType : RequirementType.getRootDataTypes())
                    {
                        menuItem = new MenuItem(requirementType.getDisplayName(), "SET_DATA_TYPE", requirementType);
                        menuItem.addActionListener(menuListener);
                        subMenu.add(menuItem);
                    }
                    treePopupMenu.add(subMenu);
                }

                menuItem = new MenuItem(translate("Copy"), "COPY_NODE", null);
                menuItem.addActionListener(menuListener);
                treePopupMenu.add(menuItem);

                menuItem = new MenuItem(translate("Delete Nodes"), "DELETE_NODES", null);
                menuItem.addActionListener(menuListener);
                treePopupMenu.add(menuItem);
            }

            // Display the newly constructed popup menu.
            if (treePopupMenu.getSubElements().length > 0) treePopupMenu.show(this, location.x, location.y);
        }
    }

    private boolean allNodesContainClass(ArrayList<DefaultMutableTreeNode> nodes, Class<?> testClass)
    {
        for (DefaultMutableTreeNode node : nodes)
        {
            if (!testClass.equals(node.getUserObject().getClass()))
            {
                return false;
            }
        }

        return true;
    }

    private void moveSelectedNodeUp()
    {
        ArrayList<DefaultMutableTreeNode> selectedNodes;

        selectedNodes = getSelectedNodes();
        if (selectedNodes.size() == 1)
        {
            DefaultMutableTreeNode selectedNode;

            selectedNode = selectedNodes.get(0);
            modelHandler.moveRequirementUp((Requirement)selectedNode.getUserObject());
        }
    }

    private void moveSelectedNodeDown()
    {
        ArrayList<DefaultMutableTreeNode> selectedNodes;

        selectedNodes = getSelectedNodes();
        if (selectedNodes.size() == 1)
        {
            DefaultMutableTreeNode selectedNode;

            selectedNode = selectedNodes.get(0);
            modelHandler.moveRequirementDown((Requirement)selectedNode.getUserObject());
        }
    }

    private class TreeKeyListener extends KeyAdapter
    {
        @Override
        public void keyPressed(KeyEvent e)
        {
            if ((e.getKeyCode() == KeyEvent.VK_UP) && (e.isControlDown()))
            {
                moveSelectedNodeUp();
            }
            else if ((e.getKeyCode() == KeyEvent.VK_DOWN) && (e.isControlDown()))
            {
                moveSelectedNodeDown();
            }
        }
    }

    private class TreeMouseListener extends MouseAdapter
    {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopup(e.getPoint());
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopup(e.getPoint());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (e.isPopupTrigger())
            {
                displayPopup(e.getPoint());
            }
        }
    }
}
