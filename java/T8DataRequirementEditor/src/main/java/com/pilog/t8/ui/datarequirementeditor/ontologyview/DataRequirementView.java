package com.pilog.t8.ui.datarequirementeditor.ontologyview;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.definition.ui.datarequirementeditor.T8DataRequirementEditorDefinition;
import com.pilog.t8.definition.ui.tree.T8TreeDefinition;
import com.pilog.t8.ui.datarequirementeditor.T8DataRequirementEditor;
import com.pilog.t8.ui.tree.T8Tree;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import com.pilog.t8.utilities.strings.Strings;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import static com.pilog.t8.definition.data.document.T8DocumentResources.*;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.components.messagedialogs.Toast;
import javax.swing.SwingUtilities;

/**
 * @author Bouwer du Preez
 */
public class DataRequirementView extends JPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(DataRequirementView.class);

    private final T8DataRequirementEditor parentEditor;
    private final T8ConfigurationManager configurationManager;
    private final T8ComponentController controller;
    private final T8Context context;
    private T8Tree dataRequirementTree;

    public DataRequirementView(T8DataRequirementEditor parentEditor)
    {
        //Set the component controller and session context for translations of UI components
        this.controller = parentEditor.getController();
        this.configurationManager = this.controller.getClientContext().getConfigurationManager();
        this.context = parentEditor.getController().getContext();

        initComponents();
        this.parentEditor = parentEditor;
        addDataRequirementTree();
    }

    public void setToolBarVisible(boolean visible)
    {
        jToolBarMain.setVisible(visible);
    }

    /**
     * Because this component implements the T8ComponentController interface it
     * is responsible for stopping the components it controls when required.
     */
    public void stopComponent()
    {
        if (dataRequirementTree != null) dataRequirementTree.stopComponent();
    }

    public String getSelectedDataRequirementID()
    {
        List<T8DataEntity> selectedEntities;

        selectedEntities = dataRequirementTree.getSelectedDataEntities();
        if (selectedEntities.size() > 0)
        {
            T8DataEntity selectedEntity;

            selectedEntity = selectedEntities.get(0);
            return (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_CONCEPT_ID);
        }
        else return null;
    }

    public String getSelectedDataRequirementODTID()
    {
        List<T8DataEntity> selectedEntities;

        selectedEntities = dataRequirementTree.getSelectedDataEntities();
        if (selectedEntities.size() > 0)
        {
            T8DataEntity selectedEntity;
            String odtID;

            selectedEntity = selectedEntities.get(0);
            odtID = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_ODT_ID);

            // If the ODT_ID selected is the Group for Data Types (Groups) then we have to use the concept select (the actual group).
            if (T8PrimaryOntologyDataType.ONTOLOGY_DATA_TYPES.getDataTypeID().equals(odtID))
            {
                odtID = (String)selectedEntity.getFieldValue(selectedEntity.getIdentifier() + EF_CONCEPT_ID);
                return odtID;
            }
            else
            {
                return odtID;
            }
        }
        else return null;
    }

    private String translate(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    private void addDataRequirementTree()
    {
        try
        {
            // Add the DR Instance List if defined.
            T8TreeDefinition treeDefinition;

            treeDefinition = ((T8DataRequirementEditorDefinition)parentEditor.getComponentDefinition()).getDataRequirementTreeDefinition();
            if (treeDefinition != null)
            {
                dataRequirementTree = (T8Tree)treeDefinition.getNewComponentInstance(controller);
                dataRequirementTree.initializeComponent(null);
                dataRequirementTree.getTree().setTransferHandler(new ConceptTreeTransferHandler());
                dataRequirementTree.getTree().setDragEnabled(true);
                dataRequirementTree.getTree().addMouseListener(new DRTreeMouseListener(dataRequirementTree));
                add(dataRequirementTree, java.awt.BorderLayout.CENTER);
                dataRequirementTree.startComponent();
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while loading DR tree.", e);
        }
    }

    private void refreshData()
    {
        dataRequirementTree.rebuildTree();
    }

    private void loadDataRequirement(String drID)
    {
        LOGGER.log("Loading Data Requirement: " + drID);
        if (drID != null)
        {
            try
            {
                parentEditor.loadDataRequirement(drID);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while loading data requirement: " + drID, e);
            }
        }
    }

    private void addNewDataRequirement()
    {
        String odtID;

        odtID = getSelectedDataRequirementODTID();
        if (!Strings.isNullOrEmpty(odtID))
        {
            parentEditor.createNewDataRequirement(odtID);
        }
        else JOptionPane.showMessageDialog(parentEditor, translate("Please select a valid Group to which the new Data Requirement will be added."), translate("Invalid DR Group Selection"), JOptionPane.ERROR_MESSAGE);
    }

    public void deleteSelectedDataRequirement()
    {
        String drId;

        drId = getSelectedDataRequirementID();
        if (drId != null)
        {
            boolean confirmation;

            confirmation = (JOptionPane.showConfirmDialog(SwingUtilities.getWindowAncestor(this), "Are you sure you want to delete the selected Data Requirement?", "Deletion Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);
            if (confirmation)
            {
                try
                {
                    T8DataRequirementClientApi drqApi;

                    drqApi = parentEditor.getDataRequirementApi();
                    drqApi.deleteDataRequirement(drId, true);
                    Toast.show("Data Requirement Deleted", Toast.Style.SUCCESS);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while deleting Data Requirement: " + drId, e);
                    JOptionPane.showMessageDialog(parentEditor, translate("An unexpected error prevented successful deletion of the selected Data Requirement."), translate("Operation Failure"), JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else JOptionPane.showMessageDialog(parentEditor, translate("Please select a valid Group to which the new Data Requirement will be added."), translate("Invalid DR Group Selection"), JOptionPane.ERROR_MESSAGE);
    }

    private void importDataRequirement()
    {
        String odtID;

        odtID = getSelectedDataRequirementODTID();
        if (!Strings.isNullOrEmpty(odtID))
        {
            parentEditor.importDataRequirement(odtID);
        }
        else JOptionPane.showMessageDialog(parentEditor, translate("Please select a valid Group into which the new Data Requirement will be imported."), translate("Invalid DR Group Selection"), JOptionPane.ERROR_MESSAGE);
    }

    private void exportDataRequirement()
    {
        String drID;

        drID = getSelectedDataRequirementID();
        if (!Strings.isNullOrEmpty(drID))
        {
            parentEditor.exportDataRequirement(drID);
        }
        else JOptionPane.showMessageDialog(parentEditor, translate("Please select a Data Requirement to export."), translate("Invalid DR Selection"), JOptionPane.ERROR_MESSAGE);
    }

    private class DRTreeMouseListener implements MouseListener
    {
        private final T8Tree tree;

        private DRTreeMouseListener(T8Tree tree)
        {
            this.tree = tree;
        }

        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (e.getClickCount() == 2)
            {
                String selectedDRID;

                selectedDRID = getSelectedDataRequirementID();
                if (selectedDRID != null)
                {
                    loadDataRequirement(selectedDRID);
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarMain = new javax.swing.JToolBar();
        jButtonRefreshPalette = new javax.swing.JButton();
        jButtonAddDataRequirement = new javax.swing.JButton();
        jButtonDeleteDataRequirement = new javax.swing.JButton();
        jButtonExport = new javax.swing.JButton();
        jButtonImport = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());

        jToolBarMain.setFloatable(false);
        jToolBarMain.setRollover(true);
        jToolBarMain.setOpaque(false);

        jButtonRefreshPalette.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/refreshIcon.png"))); // NOI18N
        jButtonRefreshPalette.setText("Refresh");
        jButtonRefreshPalette.setFocusable(false);
        jButtonRefreshPalette.setOpaque(false);
        jButtonRefreshPalette.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonRefreshPalette.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonRefreshPaletteActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRefreshPalette);

        jButtonAddDataRequirement.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/addDocumentIcon.png"))); // NOI18N
        jButtonAddDataRequirement.setText("Add DR");
        jButtonAddDataRequirement.setFocusable(false);
        jButtonAddDataRequirement.setOpaque(false);
        jButtonAddDataRequirement.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddDataRequirement.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonAddDataRequirementActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonAddDataRequirement);

        jButtonDeleteDataRequirement.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/crossIcon.png"))); // NOI18N
        jButtonDeleteDataRequirement.setText("Delete DR");
        jButtonDeleteDataRequirement.setFocusable(false);
        jButtonDeleteDataRequirement.setOpaque(false);
        jButtonDeleteDataRequirement.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDeleteDataRequirement.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDeleteDataRequirementActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonDeleteDataRequirement);

        jButtonExport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/exportIcon.png"))); // NOI18N
        jButtonExport.setText("Export");
        jButtonExport.setFocusable(false);
        jButtonExport.setOpaque(false);
        jButtonExport.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonExport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonExportActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonExport);

        jButtonImport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/importIcon.png"))); // NOI18N
        jButtonImport.setText("Import");
        jButtonImport.setFocusable(false);
        jButtonImport.setOpaque(false);
        jButtonImport.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonImport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonImportActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonImport);

        add(jToolBarMain, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRefreshPaletteActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRefreshPaletteActionPerformed
    {//GEN-HEADEREND:event_jButtonRefreshPaletteActionPerformed
        refreshData();
    }//GEN-LAST:event_jButtonRefreshPaletteActionPerformed

    private void jButtonAddDataRequirementActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddDataRequirementActionPerformed
    {//GEN-HEADEREND:event_jButtonAddDataRequirementActionPerformed
        addNewDataRequirement();
    }//GEN-LAST:event_jButtonAddDataRequirementActionPerformed

    private void jButtonExportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExportActionPerformed
    {//GEN-HEADEREND:event_jButtonExportActionPerformed
        exportDataRequirement();
    }//GEN-LAST:event_jButtonExportActionPerformed

    private void jButtonImportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonImportActionPerformed
    {//GEN-HEADEREND:event_jButtonImportActionPerformed
        importDataRequirement();
    }//GEN-LAST:event_jButtonImportActionPerformed

    private void jButtonDeleteDataRequirementActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteDataRequirementActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteDataRequirementActionPerformed
        deleteSelectedDataRequirement();
    }//GEN-LAST:event_jButtonDeleteDataRequirementActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddDataRequirement;
    private javax.swing.JButton jButtonDeleteDataRequirement;
    private javax.swing.JButton jButtonExport;
    private javax.swing.JButton jButtonImport;
    private javax.swing.JButton jButtonRefreshPalette;
    private javax.swing.JToolBar jToolBarMain;
    // End of variables declaration//GEN-END:variables
}
