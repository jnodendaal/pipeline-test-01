package com.pilog.t8.ui.datarequirementeditor;

import com.pilog.t8.ui.datarequirementeditor.prescribedvalueeditor.PrescribedDrInstanceEditor;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.document.datarequirement.Requirement;
import com.pilog.t8.data.document.datarequirement.value.DocumentReferenceRequirement;
import com.pilog.t8.definition.ui.datarequirementeditor.T8DataRequirementEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.datarequirementeditor.attributeeditor.AttributeEditor;
import com.pilog.t8.ui.datarequirementeditor.ontologyview.DataRequirementInstanceSelectionDialog;
import com.pilog.t8.ui.datarequirementeditor.view.tree.RequirementTreeView;
import com.pilog.t8.ui.optionpane.T8OptionPane;
import com.pilog.t8.utilities.strings.Strings;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.MultiSplitLayout;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementContainer extends javax.swing.JPanel
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRequirementContainer.class);

    private final T8DataRequirementEditor parentEditor;
    private final T8DataRequirementEditorDefinition parentEditorDefinition;
    private final DataRequirement dataRequirement;
    private final RequirementTreeView treeView;
    private final T8DataRequirementDetailsTabbedPane tabbedPane;
    private final T8ComponentController controller;
    private final T8DataRequirementClientApi drqApi;
    private final T8ConfigurationManager configurationManager;
    private final T8Context context;
    private String previewDrInstanceId;
    private final AttributeEditor attributeEditor;
    private final PrescribedDrInstanceEditor prescribedDrInstanceEditor;

    private static final String SPLIT_LAYOUT = "(COLUMN (LEAF name=top weight=0.6) (LEAF name=bottom weight=0.4))";

    public T8DataRequirementContainer(T8DataRequirementEditor parentEditor, DataRequirement dataRequirement)
    {
        this.parentEditor = parentEditor;
        this.parentEditorDefinition = (T8DataRequirementEditorDefinition)parentEditor.getComponentDefinition();
        this.drqApi = parentEditor.getDataRequirementApi();
        this.controller = parentEditor.getController();
        this.context = parentEditor.getController().getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.dataRequirement = dataRequirement;
        this.previewDrInstanceId = null;

        // Initialize UI components.
        initComponents();
        this.treeView = new RequirementTreeView(context, parentEditor.getTerminologyProvider());
        this.tabbedPane = new T8DataRequirementDetailsTabbedPane(parentEditor);
        this.treeView.setDataRequirement(dataRequirement);
        this.treeView.addSelectionListener(new RequirementSelectionListener());
        this.attributeEditor = new AttributeEditor(parentEditor);
        prescribedDrInstanceEditor = new PrescribedDrInstanceEditor(parentEditor, this);
        configureSplitPane();
    }

    private void configureSplitPane()
    {
        jXMultiSplitPaneContent.setModel(MultiSplitLayout.parseModel(SPLIT_LAYOUT));
        ((MultiSplitLayout)this.jXMultiSplitPaneContent.getLayout()).setLayoutMode(MultiSplitLayout.NO_MIN_SIZE_LAYOUT);
        ((MultiSplitLayout)this.jXMultiSplitPaneContent.getLayout()).setLayoutByWeight(true);
        jXMultiSplitPaneContent.add(treeView, "top");
        jXMultiSplitPaneContent.add(tabbedPane, "bottom");
        jXMultiSplitPaneContent.setOpaque(false);
    }

    public DataRequirement getDataRequirement()
    {
        attributeEditor.saveChanges();
        return dataRequirement;
    }

    private String translate(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    private void saveDataRequirement()
    {
        try
        {
            // Commit attribute editor changes.
            attributeEditor.saveChanges();

            // Check that the Data Requirement contains a valid class.
            if (!Strings.isNullOrEmpty(dataRequirement.getClassConceptID()))
            {
                // Save the Data Requirement.
                drqApi.saveDataRequirement(dataRequirement, true);
                T8OptionPane.showMessageDialog(this, translate("Data Requirement Saved"), translate("Success"), T8OptionPane.INFORMATION_MESSAGE, controller);
            }
            else
            {
                T8OptionPane.showMessageDialog(this, translate("Data Requirement must specify a valid class."), translate("Invalid Class"), T8OptionPane.INFORMATION_MESSAGE, controller);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while saving Data Requirement.", e);
            T8OptionPane.showMessageDialog(this, e.getMessage(), translate("Error"), T8OptionPane.ERROR_MESSAGE, controller);
        }
    }

    private void deleteDataRequirement()
    {
        if (JOptionPane.showConfirmDialog(this, translate("Are you sure that you want to delete this data requirement?"), translate("Deletion Confirmation"), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            try
            {
                // Delete the Data Requirement.
                drqApi.deleteDataRequirement(dataRequirement.getConceptID(), true);
                T8OptionPane.showMessageDialog(this, translate("Data Requirement Deleted"), translate("Success"), T8OptionPane.INFORMATION_MESSAGE, controller);

                // Remove this editor from the parent.
                parentEditor.removeSelectedDataRequirement();
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while deleting Data Requirement: " + dataRequirement, e);
                T8OptionPane.showMessageDialog(this, e.getMessage(), translate("Error"), T8OptionPane.ERROR_MESSAGE, controller);
            }
        }
    }

    private void previewDataEntry()
    {
        this.parentEditorDefinition.getPreviewRecordEditorDefinition().setToolbarRefreshHidden(true);
        T8DataRecordPreviewDialog.showDataEntry(SwingUtilities.getWindowAncestor(this), parentEditor, dataRequirement, parentEditorDefinition.getPreviewRecordEditorDefinition(), previewDrInstanceId);
    }

    private void setPreviewInstance()
    {
        previewDrInstanceId = DataRequirementInstanceSelectionDialog.getSelectedDataRequirementInstanceID(SwingUtilities.getWindowAncestor(this), parentEditor, "Please select the DR Instance to use for preview purposes.");
        if (Strings.isNullOrEmpty(previewDrInstanceId))
        {
            previewDrInstanceId = null;
            jLabelPreviewInstance.setText("No Instance Set");
        }
        else
        {
            jLabelPreviewInstance.setText(parentEditor.getTerminologyProvider().getTerm(null, previewDrInstanceId));
        }
    }

    private void exportDataRequirement()
    {
        T8DataRequirementExportDialog.exportDataRequirement(SwingUtilities.getWindowAncestor(this), parentEditor.getTerminologyProvider(), dataRequirement);
    }

    public void startComponent()
    {
        prescribedDrInstanceEditor.startComponent();
    }

    public void stopComponent()
    {
        prescribedDrInstanceEditor.stopComponent();
    }

    private class RequirementSelectionListener implements TreeSelectionListener
    {
        @Override
        public void valueChanged(TreeSelectionEvent e)
        {
            Requirement oldSelectionRequirement;
            Requirement newSelectionRequirement;
            TreePath oldSelectionPath;
            TreePath newSelectionPath;

            // Get the previously and currently selected requirements from the tree view.
            oldSelectionPath = e.getOldLeadSelectionPath();
            newSelectionPath = e.getNewLeadSelectionPath();
            oldSelectionRequirement = oldSelectionPath != null ? (Requirement)((DefaultMutableTreeNode)oldSelectionPath.getLastPathComponent()).getUserObject() : null;
            newSelectionRequirement = newSelectionPath != null ? (Requirement)((DefaultMutableTreeNode)newSelectionPath.getLastPathComponent()).getUserObject() : null;

            // Save existing changes.
            attributeEditor.saveChanges();

            // Refresh the tree to reflect saved changes.
            treeView.refreshNodeContaining(oldSelectionRequirement);

            // Now set the new selected requirement on the editors.
            attributeEditor.setSelectedRequirement(newSelectionRequirement);
            tabbedPane.removeAll();
            tabbedPane.add("Attributes", attributeEditor);
            if (newSelectionRequirement instanceof DocumentReferenceRequirement)
            {
                tabbedPane.add("Prescribed DR Instances", prescribedDrInstanceEditor);
                prescribedDrInstanceEditor.setSelectedRequirement(newSelectionRequirement);
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBarDocument = new javax.swing.JToolBar();
        jButtonSave = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jButtonDelete = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        jButtonDataEntry = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        jButtonSetPreviewInstance = new javax.swing.JButton();
        jLabelPreviewInstance = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        jButtonExport = new javax.swing.JButton();
        jXMultiSplitPaneContent = new org.jdesktop.swingx.JXMultiSplitPane();

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());

        jToolBarDocument.setFloatable(false);
        jToolBarDocument.setRollover(true);
        jToolBarDocument.setOpaque(false);

        jButtonSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/saveChangesIcon.png"))); // NOI18N
        jButtonSave.setText(translate("Save"));
        jButtonSave.setFocusable(false);
        jButtonSave.setOpaque(false);
        jButtonSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSaveActionPerformed(evt);
            }
        });
        jToolBarDocument.add(jButtonSave);
        jToolBarDocument.add(jSeparator1);

        jButtonDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/cutIcon.png"))); // NOI18N
        jButtonDelete.setText(translate("Delete"));
        jButtonDelete.setFocusable(false);
        jButtonDelete.setOpaque(false);
        jButtonDelete.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDelete.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDeleteActionPerformed(evt);
            }
        });
        jToolBarDocument.add(jButtonDelete);
        jToolBarDocument.add(jSeparator2);

        jButtonDataEntry.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/testIcon.png"))); // NOI18N
        jButtonDataEntry.setText(translate("Preview Data Entry"));
        jButtonDataEntry.setFocusable(false);
        jButtonDataEntry.setOpaque(false);
        jButtonDataEntry.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDataEntry.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonDataEntryActionPerformed(evt);
            }
        });
        jToolBarDocument.add(jButtonDataEntry);
        jToolBarDocument.add(jSeparator3);

        jButtonSetPreviewInstance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/searchIcon.png"))); // NOI18N
        jButtonSetPreviewInstance.setText(translate("Set Preview Instance"));
        jButtonSetPreviewInstance.setFocusable(false);
        jButtonSetPreviewInstance.setOpaque(false);
        jButtonSetPreviewInstance.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSetPreviewInstance.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonSetPreviewInstanceActionPerformed(evt);
            }
        });
        jToolBarDocument.add(jButtonSetPreviewInstance);

        jLabelPreviewInstance.setText("No Instance Set");
        jLabelPreviewInstance.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 4, 4, 4));
        jToolBarDocument.add(jLabelPreviewInstance);
        jToolBarDocument.add(jSeparator4);

        jButtonExport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pilog/t8/ui/datarequirementeditor/icons/exportIcon.png"))); // NOI18N
        jButtonExport.setText("Export");
        jButtonExport.setFocusable(false);
        jButtonExport.setOpaque(false);
        jButtonExport.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonExport.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButtonExportActionPerformed(evt);
            }
        });
        jToolBarDocument.add(jButtonExport);

        add(jToolBarDocument, java.awt.BorderLayout.NORTH);

        jXMultiSplitPaneContent.setOpaque(false);
        add(jXMultiSplitPaneContent, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSaveActionPerformed
    {//GEN-HEADEREND:event_jButtonSaveActionPerformed
        saveDataRequirement();
    }//GEN-LAST:event_jButtonSaveActionPerformed

    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDeleteActionPerformed
    {//GEN-HEADEREND:event_jButtonDeleteActionPerformed
        deleteDataRequirement();
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    private void jButtonDataEntryActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDataEntryActionPerformed
    {//GEN-HEADEREND:event_jButtonDataEntryActionPerformed
        previewDataEntry();
    }//GEN-LAST:event_jButtonDataEntryActionPerformed

    private void jButtonSetPreviewInstanceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSetPreviewInstanceActionPerformed
    {//GEN-HEADEREND:event_jButtonSetPreviewInstanceActionPerformed
        setPreviewInstance();
    }//GEN-LAST:event_jButtonSetPreviewInstanceActionPerformed

    private void jButtonExportActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExportActionPerformed
    {//GEN-HEADEREND:event_jButtonExportActionPerformed
        exportDataRequirement();
    }//GEN-LAST:event_jButtonExportActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDataEntry;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JButton jButtonExport;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JButton jButtonSetPreviewInstance;
    private javax.swing.JLabel jLabelPreviewInstance;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar jToolBarDocument;
    private org.jdesktop.swingx.JXMultiSplitPane jXMultiSplitPaneContent;
    // End of variables declaration//GEN-END:variables
}
