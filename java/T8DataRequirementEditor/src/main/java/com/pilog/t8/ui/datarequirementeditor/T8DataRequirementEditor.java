package com.pilog.t8.ui.datarequirementeditor;

import com.pilog.t8.api.T8TerminologyClientApi;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.api.T8DataRequirementClientApi;
import com.pilog.t8.api.T8OntologyClientApi;
import com.pilog.t8.api.T8OrganizationClientApi;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8Component;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.data.document.CachedTerminologyProvider;
import com.pilog.t8.data.document.T8LRUCachedTerminologyProvider;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.org.T8ClientTerminologyProvider;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.api.T8PrescribedValueClientApi;
import com.pilog.t8.data.org.T8OrganizationOntologyFactory;
import com.pilog.t8.definition.ui.T8ComponentDefinition;
import com.pilog.t8.definition.ui.datarequirementeditor.T8DataRequirementEditorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.ui.contentheaderborder.T8ContentHeaderBorder;
import com.pilog.t8.ui.datarequirementeditor.ontologyview.OntologyView;
import com.pilog.t8.utilities.components.tabbedpane.tab.ClosableTabComponent;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.MultiSplitLayout;

/**
 * @author Bouwer du Preez
 */
public class T8DataRequirementEditor extends javax.swing.JPanel implements T8Component
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataRequirementEditor.class);
    private static final String SPLIT_LAYOUT = "(ROW (LEAF name=left weight=0.3) (LEAF name=right weight=0.7))";

    private final T8DataRequirementEditorDefinition definition;
    private final T8ConfigurationManager configurationManager;
    private final T8ComponentController controller;
    private final T8Context context;
    private final List<T8DataRequirementContainer> drContainers;
    private T8DataRequirementEditorDataHandler dataHandler;
    private OntologyView dictionaryView;
    private CachedTerminologyProvider terminologyProvider;
    private T8OrganizationClientApi orgApi;
    private T8OntologyClientApi ontApi;
    private T8DataRequirementClientApi drqApi;
    private T8PrescribedValueClientApi pvApi;
    private T8TerminologyClientApi trmApi;

    public T8DataRequirementEditor(T8DataRequirementEditorDefinition definition, T8ComponentController controller)
    {
        this.definition = definition;
        this.controller = controller;
        this.context = controller.getContext();
        this.configurationManager = controller.getClientContext().getConfigurationManager();
        this.drContainers = new ArrayList<>();

        /// Initialize the UI components.
        initComponents();
        this.jXMultiSplitPaneContent.setModel(MultiSplitLayout.parseModel(SPLIT_LAYOUT));
        ((MultiSplitLayout)this.jXMultiSplitPaneContent.getLayout()).setLayoutMode(MultiSplitLayout.NO_MIN_SIZE_LAYOUT);
        ((MultiSplitLayout)this.jXMultiSplitPaneContent.getLayout()).setLayoutByWeight(true);
        this.jPanelView.setBorder(new T8ContentHeaderBorder(translate("Data Requirement Structure")));
        jXMultiSplitPaneContent.add(jPanelView, "right");
        jXMultiSplitPaneContent.setOpaque(false);
        jXMultiSplitPaneContent.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    }

    public CachedTerminologyProvider getTerminologyProvider()
    {
        return terminologyProvider;
    }

    public T8OrganizationClientApi getOrganizationApi()
    {
        return orgApi;
    }

    public T8PrescribedValueClientApi getPrescribedValueApi()
    {
        return pvApi;
    }

    public T8OntologyClientApi getOntologyApi()
    {
        return ontApi;
    }

    public T8DataRequirementClientApi getDataRequirementApi()
    {
        return drqApi;
    }

    public void exportDataRequirement(String drID)
    {
        try
        {
            DataRequirement dataRequirement;

            dataRequirement = dataHandler.loadDataRequirement(drID, terminologyProvider);
            if (dataRequirement != null)
            {
                T8DataRequirementExportDialog.exportDataRequirement(SwingUtilities.getWindowAncestor(this), terminologyProvider, dataRequirement);
            }
            else JOptionPane.showMessageDialog(this, "Data Requirement not found.", "Operation Failure", JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while loading data requirement: " + drID, e);
            JOptionPane.showMessageDialog(this, "Data Requirement could not be retrieved successfully.", "Operation Failure", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void importDataRequirement(String odtID)
    {
        DataRequirement newDataRequirement;

        newDataRequirement = T8DataRequirementImportDialog.importDataRequirement(SwingUtilities.getWindowAncestor(this), odtID);
        if (newDataRequirement != null)
        {
            // Add the new Data Requirement to the UI.
            addDataRequirement(newDataRequirement);
        }
    }

    public void createNewDataRequirement(String odtID)
    {
        String newDataRequirementTerm;

        newDataRequirementTerm = JOptionPane.showInputDialog(this, "Please enter a title for the new Data Requirement", "New Data Requirement", JOptionPane.INFORMATION_MESSAGE);
        if (!Strings.isNullOrEmpty(newDataRequirementTerm))
        {
            DataRequirement newDataRequirement;
            T8OrganizationOntologyFactory factory;
            T8OntologyConcept drConcept;

            // Create the ontology for the new Data Requirement.
            factory = ontApi.getOntologyFactory();
            factory.setConceptODTID(odtID);
            factory.setLanguageID(context.getSessionContext().getContentLanguageIdentifier());
            drConcept = factory.constructConcept(T8OntologyConceptType.DATA_REQUIREMENT);
            factory.addTerm(drConcept, newDataRequirementTerm.toUpperCase());

            // Add terminology for the new concept to the terminology cache.
            terminologyProvider.addTerminology(factory.constructTerminology(drConcept));

            // Create the new data requirement.
            newDataRequirement = new DataRequirement(drConcept.getID());
            newDataRequirement.setOntology(drConcept);

            // Add the new Data Requirement to the UI.
            addDataRequirement(newDataRequirement);
        }
    }

    private void addDataRequirement(DataRequirement dataRequirement)
    {
        try
        {
            T8DataRequirementContainer drContainer;
            String drID;
            String drTerm;

            drID = dataRequirement.getConceptID();
            drTerm = terminologyProvider.getTerm(null, drID);

            drContainer = new T8DataRequirementContainer(this, dataRequirement);
            drContainers.add(drContainer);
            jTabbedPaneMain.add(drTerm, drContainer);
            jTabbedPaneMain.setTabComponentAt(jTabbedPaneMain.getTabCount()-1, new ClosableTabComponent(jTabbedPaneMain, drTerm));
            jTabbedPaneMain.setSelectedIndex(jTabbedPaneMain.getTabCount()-1);

            drContainer.startComponent();
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while retrieving terminology for Data Requirement tab header.", e);
        }
    }

    public DataRequirement getSelectedDataRequirement()
    {
        T8DataRequirementContainer drContainer;

        drContainer = (T8DataRequirementContainer)jTabbedPaneMain.getSelectedComponent();
        if (drContainer != null)
        {
            return drContainer.getDataRequirement();
        }
        else return null;
    }

    public DataRequirement removeSelectedDataRequirement()
    {
        T8DataRequirementContainer drContainer;

        drContainer = (T8DataRequirementContainer)jTabbedPaneMain.getSelectedComponent();
        if (drContainer != null)
        {
            DataRequirement dr;

            jTabbedPaneMain.remove(drContainer);
            drContainers.remove(drContainer);
            dr = drContainer.getDataRequirement();
            drContainer.stopComponent();
            return dr;
        }
        else return null;
    }

    public void refreshDRContainerTitles()
    {
        for (int componentIndex = 0; componentIndex < jTabbedPaneMain.getTabCount(); componentIndex++)
        {
            try
            {
                T8DataRequirementContainer drContainer;
                String drID;

                drContainer = (T8DataRequirementContainer)jTabbedPaneMain.getComponentAt(componentIndex);
                drID = drContainer.getDataRequirement().getConceptID();

                ((ClosableTabComponent)jTabbedPaneMain.getTabComponentAt(componentIndex)).setText(terminologyProvider.getTerm(null, drID));
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while retrieving terminology for Data Requirement tab header.", e);
            }
        }
    }

    public void loadDataRequirement(String drID) throws Exception
    {
        DataRequirement dataRequirement;

        dataRequirement = dataHandler.loadDataRequirement(drID, terminologyProvider);
        if (dataRequirement != null)
        {
            addDataRequirement(dataRequirement);
        }
    }

    private String translate(String text)
    {
        return this.configurationManager.getUITranslation(this.context, text);
    }

    @Override
    public T8ComponentController getController()
    {
        return controller;
    }


    @Override
    public Map<String, Object> getInputParameters()
    {
        return null;
    }

    @Override
    public Map<String, Object> executeOperation(String string, Map<String, Object> map)
    {
        return null;
    }

    @Override
    public T8ComponentDefinition getComponentDefinition()
    {
        return definition;
    }

    @Override
    public void initializeComponent(Map<String, Object> inputParameters)
    {
        this.orgApi = controller.getApi(T8OrganizationClientApi.API_IDENTIFIER);
        this.ontApi = controller.getApi(T8OntologyClientApi.API_IDENTIFIER);
        this.pvApi = controller.getApi(T8PrescribedValueClientApi.API_IDENTIFIER);
        this.trmApi = controller.getApi(T8TerminologyClientApi.API_IDENTIFIER);
        this.drqApi = controller.getApi(T8DataRequirementClientApi.API_IDENTIFIER);
        this.dataHandler = new T8DataRequirementEditorDataHandler(context, drqApi);
        this.terminologyProvider = new T8LRUCachedTerminologyProvider(new T8ClientTerminologyProvider(trmApi), 200);
        this.terminologyProvider.setLanguage(context.getSessionContext().getContentLanguageIdentifier());
        this.dictionaryView = new OntologyView(this);
        jXMultiSplitPaneContent.add(dictionaryView, "left");
    }

    @Override
    public void startComponent()
    {
    }

    @Override
    public void stopComponent()
    {
        // Stop the dictionary view components.
        dictionaryView.stopComponent();

        // Stop each of the currently open DR Containers.
        for (T8DataRequirementContainer drContainer : drContainers)
        {
            drContainer.stopComponent();
        }
    }

    @Override
    public void addChildComponent(T8Component childComponent, Object layoutConstraints)
    {
    }

    @Override
    public T8Component getChildComponent(String identifier)
    {
        return null;
    }

    @Override
    public T8Component removeChildComponent(String identifier)
    {
        return null;
    }

    @Override
    public ArrayList<T8Component> getChildComponents()
    {
        return null;
    }

    @Override
    public int getChildComponentCount()
    {
        return 0;
    }

    @Override
    public void clearChildComponents()
    {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jPanelView = new javax.swing.JPanel();
        jTabbedPaneMain = new javax.swing.JTabbedPane();
        jXMultiSplitPaneContent = new org.jdesktop.swingx.JXMultiSplitPane();

        jPanelView.setOpaque(false);
        jPanelView.setLayout(new java.awt.BorderLayout());
        jPanelView.add(jTabbedPaneMain, java.awt.BorderLayout.CENTER);

        setOpaque(false);
        setLayout(new java.awt.BorderLayout());

        jXMultiSplitPaneContent.setOpaque(false);
        add(jXMultiSplitPaneContent, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelView;
    private javax.swing.JTabbedPane jTabbedPaneMain;
    private org.jdesktop.swingx.JXMultiSplitPane jXMultiSplitPaneContent;
    // End of variables declaration//GEN-END:variables
}
