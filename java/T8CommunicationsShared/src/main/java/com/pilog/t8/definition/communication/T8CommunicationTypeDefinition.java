package com.pilog.t8.definition.communication;

import com.pilog.t8.definition.communication.T8CommunicationServiceDefinition;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.communication.detail.provider.T8CommunicationDetailsProviderAdapterDefinition;
import com.pilog.t8.definition.communication.message.template.T8CommunicationMessageDefinition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public abstract class T8CommunicationTypeDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_COMMUNICATION_TYPES";
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_TYPE";
    public static final String DISPLAY_NAME = "Communication Type";
    public static final String DESCRIPTION = "Communication Type";
    public static final String IDENTIFIER_PREFIX = "COMM_TYPE_";
    public enum Datum {
        ACTIVE,
        SERVICE_IDENTIFIER,
        MESSAGE_TEMPLATE_IDENTIFIER,
        MESSAGE_TEMPLATE_PARAMETER_MAPPING,
        COMMUNICATION_DETAIL_PROVIDER_ADAPTERS,
        CONDITION_EXPRESSION
    };
    // -------- Definition Meta-Data -------- //

    public T8CommunicationTypeDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract T8CommunicationType getType();

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ACTIVE.toString(), "Active", "Sets this communication type to active/inactive.", true));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.SERVICE_IDENTIFIER.toString(), "Service Identifier", "The Service to use when sending messages."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.EPIC_EXPRESSION, Datum.CONDITION_EXPRESSION.toString(), "Condition Expression", "The condition expression that will determine if the communication type will be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MESSAGE_TEMPLATE_IDENTIFIER.toString(), "Message Template Identifier", "The message template identifier to use when constructing the template for the message."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.MESSAGE_TEMPLATE_PARAMETER_MAPPING.toString(), "Parameter Mapping:  Communication Input to Template Input", "A mapping of Communication input parameters to the corresponding Template input parameters."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.COMMUNICATION_DETAIL_PROVIDER_ADAPTERS.toString(), "Communication Detail Provider Adapters", "The Communication Detail Provider Adapaters to use to tell that will determine the recipients of the communication."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.MESSAGE_TEMPLATE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8CommunicationMessageDefinition.GROUP_IDENTIFIER));
        }
        else if (Datum.COMMUNICATION_DETAIL_PROVIDER_ADAPTERS.toString().equals(datumIdentifier))
        {
            return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8CommunicationDetailsProviderAdapterDefinition.TYPE_IDENTIFIER));
        }
        else if (Datum.MESSAGE_TEMPLATE_PARAMETER_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String templateId;

            optionList = new ArrayList<>();

            templateId = getMessageTemplateIdentifier();
            if (templateId != null)
            {
                T8CommunicationMessageDefinition templateDefinition;
                T8DefaultCommunicationDefinition communicationDefinition;

                templateDefinition = (T8CommunicationMessageDefinition) definitionContext.getRawDefinition(getRootProjectId(), templateId);
                communicationDefinition = (T8DefaultCommunicationDefinition) getParentDefinition();
                if (templateDefinition != null)
                {
                    List<T8DataParameterDefinition> communicationParameterDefinitions;
                    List<T8DataParameterDefinition> templateParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    communicationParameterDefinitions = communicationDefinition.getInputParameters();
                    templateParameterDefinitions = templateDefinition.getInputParametersDefinitions();

                    identifierMap = new HashMap<>();
                    if ((communicationParameterDefinitions != null) && (templateParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition processParameterDefinition : communicationParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<>();
                            for (T8DataParameterDefinition operationParameterDefinition : templateParameterDefinitions)
                            {
                                identifierList.add(operationParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(processParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                } else return new ArrayList<>();
            } else return new ArrayList<>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        if (Datum.SERVICE_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionMetaData> t8DefinitionMetaDatas = new ArrayList<>();
            for (T8DefinitionMetaData metaData : definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8CommunicationServiceDefinition.GROUP_IDENTIFIER))
            {
                T8CommunicationServiceDefinition definition = (T8CommunicationServiceDefinition) definitionContext.getRawDefinition(getRootProjectId(), metaData.getId());
                if (definition != null && definition.getType() != null && definition.getType().equals(getType()))
                {
                    t8DefinitionMetaDatas.add(metaData);
                }
            }
            return createIdentifierOptions(t8DefinitionMetaDatas);
        } else return null;
    }

    public void setActive(Boolean active)
    {
        setDefinitionDatum(Datum.ACTIVE, active);
    }

    public Boolean isActive()
    {
        return getDefinitionDatum(Datum.ACTIVE);
    }

    public void setServiceIdentifier(String templateIdentifier)
    {
        setDefinitionDatum(Datum.SERVICE_IDENTIFIER, templateIdentifier);
    }

    public String getServiceIdentifier()
    {
        return getDefinitionDatum(Datum.SERVICE_IDENTIFIER);
    }

    public void setMessageTemplateIdentifier(String templateIdentifier)
    {
        setDefinitionDatum(Datum.MESSAGE_TEMPLATE_IDENTIFIER, templateIdentifier);
    }

    public String getConditionExpression()
    {
        return getDefinitionDatum(Datum.CONDITION_EXPRESSION);
    }

    public void setConditionExpression(String conditionExpression)
    {
        setDefinitionDatum(Datum.CONDITION_EXPRESSION, conditionExpression);
    }

    public String getMessageTemplateIdentifier()
    {
        return getDefinitionDatum(Datum.MESSAGE_TEMPLATE_IDENTIFIER);
    }

    public void setMessageTemplateParameterMapping(Map<String, String> identifier)
    {
        setDefinitionDatum(Datum.MESSAGE_TEMPLATE_PARAMETER_MAPPING, identifier);
    }

    public Map<String, String> getMessageTemplateParameterMapping()
    {
        return getDefinitionDatum(Datum.MESSAGE_TEMPLATE_PARAMETER_MAPPING);
    }

    public void setCommunicationDetailProviderAdapters(List<T8CommunicationDetailsProviderAdapterDefinition> identifier)
    {
        setDefinitionDatum(Datum.COMMUNICATION_DETAIL_PROVIDER_ADAPTERS, identifier);
    }

    public List<T8CommunicationDetailsProviderAdapterDefinition> getCommunicationDetailProviderAdapters()
    {
        return getDefinitionDatum(Datum.COMMUNICATION_DETAIL_PROVIDER_ADAPTERS);
    }
}
