package com.pilog.t8.definition.communication.message.template;

import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageState;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.communication.T8MessageTemplate;
import com.pilog.t8.communication.message.T8FaxMessage;
import com.pilog.t8.communication.template.attachment.T8DefaultAttachment;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8FaxMessageTemplateFreeMarkerDefinition extends T8FaxTemplateDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_MESSAGE_TEMPLATE_FAX_FREE_MARKER";
    public static final String DISPLAY_NAME = "Fax FreeMarker";
    public static final String DESCRIPTION = "Fax Free Marker Template";
    public enum Datum
    {
    };
    // -------- Definition Meta-Data -------- //


    public T8FaxMessageTemplateFreeMarkerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8MessageTemplate getMessageTemplateInstance(T8Context context, Map<String,Object> messageParameters)
    {
         try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.communication.template.T8FaxMessageTemplateFreeMarker").getConstructor(T8Context.class, T8FaxMessageTemplateFreeMarkerDefinition.class, Map.class);
            return (T8MessageTemplate)constructor.newInstance(context, this, messageParameters);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext context, String datumId)
    {
        if ((T8CommunicationMessageDefinition.Datum.TEMPLATE_TEXT.toString().equals(datumId)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.TextAreaDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor) constructor.newInstance(context, this, getDatumType(datumId));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else
        {
            return super.getDatumEditor(context, datumId);
        }
    }

    @Override
    public T8CommunicationMessage getMessageInstance(T8Context context, T8CommunicationMessageState state)
    {
        Map<String, Object> stateParameters;
        T8FaxMessage message;

        // Create the message from the state.
        message = new T8FaxMessage(state.getRecipientDisplayName(), state.getPriority(), state.getRecipientAddress(), state.getRecipientDisplayName(), state.getSubject(), state.getContent());
        message.setIdentifier(state.getMessageIdentifier());
        message.setInstanceIdentifier(state.getMessageInstanceIdentifier());
        message.setCommunicationIdentifier(state.getCommunicationIdentifier());
        message.setCommunicationInstanceIdentifier(state.getCommunicationInstanceIdentifier());
        message.setFailureCount(state.getFailureCount());
        message.setSenderIdentifier(state.getSenderIdentifier());
        message.setSenderInstanceIdentifier(state.getSenderInstanceIdentifier());

        // Add message attachment available as state parameters.
        stateParameters = state.getParameters();
        if (stateParameters != null)
        {
            List<Map<String, Object>> attachmentList;

            attachmentList = (List<Map<String, Object>>)stateParameters.get(PARAMETER_IDENTIFIER_ATTACHMENTS);
            if (attachmentList != null)
            {
                for (Map<String, Object> attachmentMap : attachmentList)
                {
                    String attachmentIdentifier;
                    String attachmentName;
                    String attachmentDescription;
                    byte[] attachmentData;

                    attachmentIdentifier = (String)attachmentMap.get(PARAMETER_IDENTIFIER_ATTACHMENT_IDENTIFIER);
                    attachmentName = (String)attachmentMap.get(PARAMETER_IDENTIFIER_ATTACHMENT_NAME);
                    attachmentDescription = (String)attachmentMap.get(PARAMETER_IDENTIFIER_ATTACHMENT_DESCRIPTION);
                    attachmentData = (byte[])attachmentMap.get(PARAMETER_IDENTIFIER_ATTACHMENT_DATA);
                    message.addAttachment(new T8DefaultAttachment(attachmentIdentifier, attachmentName, attachmentDescription, attachmentData));
                }
            }
        }

        // Return the completed message.
        return message;
    }
}
