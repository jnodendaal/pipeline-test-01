/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.communication.message.template.email.attachment;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.communication.template.attachment.T8DefaultAttachment;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8EmailMessageTemplateAttachementDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //

    public static final String GROUP_IDENTIFIER = "@DG_EMAIL_MESSAGE_TEMPLATE_ATTACHMENTS";
    public static final String TYPE_IDENTIFIER = "@DT_EMAIL_MESSAGE_TEMPLATE_ATTACHMENT";
    public static final String DISPLAY_NAME = "E-Mail Attachement";
    public static final String DESCRIPTION = "E-Mail Attachment";
    public static final String IDENTIFIER_PREFIX = "EMAIL_ATTACH_";
    public enum Datum
    {
        ATTACHMENT_NAME,
        ATTACHMENT_DESCRIPTION,
        ATTACHMENT_DATA
    };
    // -------- Definition Meta-Data -------- //

    public T8EmailMessageTemplateAttachementDefinition(String identifier)
    {
        super(identifier);
    }

    public T8CommunicationMessageAttachment getInstance()
    {
        return new T8DefaultAttachment(getIdentifier(), getAttachmentName(), getAttachmentDescription(), getAttachmentData());
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
         ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ATTACHMENT_NAME.toString(), "Name", "Attachemnt Name."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.ATTACHMENT_DESCRIPTION.toString(), "Description", "Attachment Description."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BYTE_ARRAY, Datum.ATTACHMENT_DATA.toString(), "File Data", "Attachment Data."));
        return datumTypes;
    }
    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if(Datum.ATTACHMENT_DATA.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.FilePickerDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    public void setAttachmentName(String name)
    {
        setDefinitionDatum(Datum.ATTACHMENT_NAME, name);
    }

    public String getAttachmentName()
    {
        return getDefinitionDatum(Datum.ATTACHMENT_NAME);
    }

    public void setAttachmentDescription(String description)
    {
        setDefinitionDatum(Datum.ATTACHMENT_DESCRIPTION, description);
    }

    public String getAttachmentDescription()
    {
        return getDefinitionDatum(Datum.ATTACHMENT_DESCRIPTION);
    }

    public void setAttachmentData(byte [] data)
    {
        setDefinitionDatum(Datum.ATTACHMENT_DATA, data);
    }

    public boolean hasAttachmentData()
    {
        byte[] attachData = getAttachmentData();
        return attachData != null && attachData.length > 0;
    }

    public InputStream getAttachmentDataAsStream()
    {
        return new ByteArrayInputStream((byte []) getDefinitionDatum(Datum.ATTACHMENT_DATA));
    }

    public byte [] getAttachmentData()
    {
        return (byte []) getDefinitionDatum(Datum.ATTACHMENT_DATA);
    }
}
