/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.communication.message.template;

import com.pilog.t8.T8ClientContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8DefaultServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8MessageTemplateDataScriptDefinition extends T8DefaultServerContextScriptDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_SCRIPT_COMMUNICATION_MESSAGE_TEMPLATE_PARAMETER";
    public static final String GROUP_IDENTIFIER = "@DG_SCRIPT_COMMUNICATION_MESSAGE_TEMPLATE_PARAMETER";
    public static final String STORAGE_PATH = null;
    public static final String DISPLAY_NAME = "Message Template Data Script";
    public static final String DESCRIPTION = "A script that will be used to construct the data that will be used in the template to replace values.";
    public static final String VERSION = "0";
    // -------- Definition Meta-Data -------- //

    public T8MessageTemplateDataScriptDefinition(String id)
    {
        super(id);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameterDefinitions(T8Context context) throws Exception
    {
        T8CommunicationMessageDefinition webServiceOperationDefinition;

        webServiceOperationDefinition = (T8CommunicationMessageDefinition)getParentDefinition();
        if (webServiceOperationDefinition != null)
        {
            return webServiceOperationDefinition.getInputParametersDefinitions();
        }
        else return null;
    }

    @Override
    public List<T8DataParameterDefinition> getOutputParameterDefinitions(T8Context context) throws Exception
    {
        return null;
    }
}
