package com.pilog.t8.communication.message;

import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.communication.T8CommunicationMessageState;
import com.pilog.t8.definition.communication.message.template.T8FaxTemplateDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8FaxMessage extends T8DefaultCommunicationMessage implements T8CommunicationMessage
{
    private String faxNumber;
    private String faxRecipientName;
    private String faxRecipientCompanyName;
    private String notes;
    private String messageContent;
    private final List<T8CommunicationMessageAttachment> attachments;

    private static final T8Logger logger = com.pilog.t8.T8Log.getLogger(T8EmailMessage.class.getName());

    public T8FaxMessage(String recipientDisplayName, int priority, String faxNumber, String faxRecipientName, String notes, String messageContent)
    {
        super(recipientDisplayName, priority);
        this.faxNumber = faxNumber;
        this.faxRecipientName = faxRecipientName;
        this.notes = notes;
        this.messageContent = messageContent;
        this.attachments = new ArrayList<T8CommunicationMessageAttachment>();
    }

    public T8FaxMessage(String recipientDisplayName, int priority, String faxNumber, String faxRecipientName, String notes, String messageContent, List<T8CommunicationMessageAttachment> attachments)
    {
        super(recipientDisplayName, priority);
        this.faxNumber = faxNumber;
        this.faxRecipientName = faxRecipientName;
        this.notes = notes;
        this.messageContent = messageContent;
        this.attachments = attachments;
    }

    @Override
    public T8CommunicationMessageState getState()
    {
        T8CommunicationMessageState state;
        List<Map<String, Object>> attachmentList;

        // Get the default message state from the super implementation.
        state = getMessageState();

        // Add the attachments to the state.
        attachmentList = new ArrayList<Map<String, Object>>();
        state.addParameter(T8FaxTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENTS, attachmentList);
        for (T8CommunicationMessageAttachment attachment : attachments)
        {
            Map<String, Object> attachmentParameters;

            attachmentParameters = new HashMap<String, Object>();
            attachmentParameters.put(T8FaxTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENT_IDENTIFIER, attachment.getIdentifier());
            attachmentParameters.put(T8FaxTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENT_NAME, attachment.getName());
            attachmentParameters.put(T8FaxTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENT_DESCRIPTION, attachment.getDescription());
            attachmentParameters.put(T8FaxTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENT_DATA, attachment.getBinaryData());
            attachmentList.add(attachmentParameters);
        }

        // Return the created state.
        return state;
    }

    public String getMessageContent()
    {
        return messageContent;
    }

    public void setMessageContent(String messageContent)
    {
        this.messageContent = messageContent;
    }

    public List<T8CommunicationMessageAttachment> getAttachments()
    {
        return attachments;
    }

    public void setAttachments(List<T8CommunicationMessageAttachment> attachments)
    {
        this.attachments.clear();
        if (attachments != null)
        {
            this.attachments.addAll(attachments);
        }
    }

    public void addAttachment(T8CommunicationMessageAttachment attachment)
    {
        attachments.add(attachment);
    }

    public boolean removeAttachment(T8CommunicationMessageAttachment attachment)
    {
        return attachments.remove(attachment);
    }

    @Override
    public String getRecipientAddress()
    {
        return getFaxNumber();
    }

    @Override
    public String getContent()
    {
        return getMessageContent();
    }

    @Override
    public String getSubject()
    {
        return getNotes();
    }

    public String getFaxNumber()
    {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber)
    {
        this.faxNumber = faxNumber;
    }

    public String getFaxRecipientName()
    {
        return faxRecipientName;
    }

    public void setFaxRecipientName(String faxRecipientName)
    {
        this.faxRecipientName = faxRecipientName;
    }

    public String getFaxRecipientCompanyName()
    {
        return faxRecipientCompanyName;
    }

    public void setFaxRecipientCompanyName(String faxRecipientCompanyName)
    {
        this.faxRecipientCompanyName = faxRecipientCompanyName;
    }

    public String getNotes()
    {
        return notes;
    }

    public void setNotes(String notes)
    {
        this.notes = notes;
    }

    @Override
    public String toString()
    {
        return "T8DefaultCommunicationMessage{" + "identifier=" + identifier + ", instanceIdentifier=" + instanceIdentifier + ", communicationIdentifier=" + communicationIdentifier + ", communicationInstanceIdentifier=" + communicationInstanceIdentifier + '}';
    }
}
