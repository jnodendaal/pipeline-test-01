/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.communication.message.template.email.attachment;

import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8EmailDocxAttachmentDefinition extends T8EmailMessageTemplateAttachementDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_EMAIL_MESSAGE_TEMPLATE_ATTACHMENT_DOCX";
    public static final String DISPLAY_NAME = "DOCX Attachment";
    public static final String DESCRIPTION = "Attaches a file that is located at the specified URL.";
    public static final String IDENTIFIER_PREFIX = "EMAIL_ATTACH_DOCX_";
    public enum Datum
    {
        CONVERT
    };
    // -------- Definition Meta-Data -------- //

    public T8EmailDocxAttachmentDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.CONVERT.toString(), "Convert To PDF", "Convert the file to PDF."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if(T8EmailMessageTemplateAttachementDefinition.Datum.ATTACHMENT_DATA.toString().equals(datumIdentifier))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.FilePickerDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class, String.class, String.class, String.class}, definitionContext, this, getDatumType(datumIdentifier), ".docx", "MS Word 2007-", getAttachmentName());
        } else return super.getDatumEditor(definitionContext, datumIdentifier);
    }

    @Override
    public T8CommunicationMessageAttachment getInstance()
    {
        return T8Reflections.getInstance("com.pilog.t8.communication.template.attachment.email.T8EmailAttachmentDocX", new Class<?>[]{T8EmailDocxAttachmentDefinition.class}, this);
    }

    public void setConvertToPDF(Boolean name)
    {
        setDefinitionDatum(Datum.CONVERT, name);
    }

    public Boolean shouldConvertToPDF()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.CONVERT));
    }
}
