package com.pilog.t8.communication.message;

import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.communication.T8CommunicationMessageState;
import com.pilog.t8.definition.communication.message.template.T8EmailTemplateDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EmailMessage extends T8DefaultCommunicationMessage implements T8CommunicationMessage
{
    private final List<T8CommunicationMessageAttachment> attachments;

    private String bccGenericRecipient;
    private String messageContent;
    private String emailAddress;
    private boolean bccEnabled;
    private String subject;

    public T8EmailMessage(String recipientDisplayName, int priority, String emailAddress, String subject, String messageContent)
    {
        super(recipientDisplayName, priority);
        this.emailAddress = emailAddress;
        this.subject = subject;
        this.messageContent = messageContent;
        this.attachments = new ArrayList<>();
    }

    public T8EmailMessage(String recipientDisplayName, int priority, String emailAddress, String subject, String messageContent, List<T8CommunicationMessageAttachment> attachments)
    {
        super(recipientDisplayName, priority);
        this.emailAddress = emailAddress;
        this.subject = subject;
        this.messageContent = messageContent;
        this.attachments = new ArrayList<>();
        setAttachments(attachments);
    }

    @Override
    public T8CommunicationMessageState getState()
    {
        T8CommunicationMessageState state;
        List<Map<String, Object>> attachmentList;

        // Get the default message state from the super implementation.
        state = getMessageState();

        // Add the attachments to the state.
        attachmentList = new ArrayList<>();
        state.addParameter(T8EmailTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENTS, attachmentList);
        for (T8CommunicationMessageAttachment attachment : attachments)
        {
            Map<String, Object> attachmentParameters;

            attachmentParameters = new HashMap<>();
            attachmentParameters.put(T8EmailTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENT_IDENTIFIER, attachment.getIdentifier());
            attachmentParameters.put(T8EmailTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENT_NAME, attachment.getName());
            attachmentParameters.put(T8EmailTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENT_DESCRIPTION, attachment.getDescription());
            attachmentParameters.put(T8EmailTemplateDefinition.PARAMETER_IDENTIFIER_ATTACHMENT_DATA, attachment.getBinaryData());
            attachmentList.add(attachmentParameters);
        }

        // Return the created state.
        return state;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    @Override
    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getMessageContent()
    {
        return messageContent;
    }

    public void setMessageContent(String messageContent)
    {
        this.messageContent = messageContent;
    }

    public List<T8CommunicationMessageAttachment> getAttachments()
    {
        return attachments;
    }

    public void setAttachments(List<T8CommunicationMessageAttachment> attachments)
    {
        this.attachments.clear();
        if (attachments != null)
        {
            this.attachments.addAll(attachments);
        }
    }

    public void addAttachment(T8CommunicationMessageAttachment attachment)
    {
        attachments.add(attachment);
    }

    public boolean removeAttachment(T8CommunicationMessageAttachment attachment)
    {
        return attachments.remove(attachment);
    }

    public String getBccGenericRecipient()
    {
        return bccGenericRecipient;
    }

    public void setBccGenericRecipient(String bccGenericRecipient)
    {
        this.bccGenericRecipient = bccGenericRecipient;
    }

    public boolean isBccEnabled()
    {
        return bccEnabled;
    }

    public void setBccEnabled(boolean bccEnabled)
    {
        this.bccEnabled = bccEnabled;
    }

    @Override
    public String getRecipientAddress()
    {
        return getEmailAddress();
    }

    @Override
    public String getContent()
    {
        return getMessageContent();
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;
        toStringBuilder = new StringBuilder("T8EmailMessage{");
        toStringBuilder.append("identifier=").append(identifier);
        toStringBuilder.append(",instanceIdentifier=").append(instanceIdentifier);
        toStringBuilder.append(",communicationIdentifier=").append(communicationIdentifier);
        toStringBuilder.append(",communicationInstanceIdentifier=").append(communicationInstanceIdentifier);
        toStringBuilder.append(",bccEnabled=").append(this.bccEnabled);
        toStringBuilder.append(",emailAddress=").append(this.emailAddress);
        toStringBuilder.append('}');
        return toStringBuilder.toString();
    }
}
