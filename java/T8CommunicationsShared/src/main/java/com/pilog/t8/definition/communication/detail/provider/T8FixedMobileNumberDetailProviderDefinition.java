/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.communication.detail.provider;

import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.communication.T8CommunicationDetailsProvider;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8FixedMobileNumberDetailProviderDefinition extends T8CommunicationDetailsProviderDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_DETAIL_PROVIDER_T8FIXED_MOBILE";
    public static final String DISPLAY_NAME = "T8 Fixed Mobile Number";
    public static final String DESCRIPTION = "T8 Fixed Mobile Number Detail provider";

    public enum Datum
    {
    };
// -------- Definition Meta-Data -------- //
    public static final String P_ALTERNATE_MOBILE_NUMBER_LIST = "$P_ALTERNATE_MOBILE_NUMBER_LIST";
    public static final String P_ALTERNATE_MOBILE_NUMBER = "$P_ALTERNATE_MOBILE_NUMBER";

    public T8FixedMobileNumberDetailProviderDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8CommunicationType> getSupportedCommunicationTypes()
    {
        List<T8CommunicationType> communicationTypes = new ArrayList<T8CommunicationType>();
        communicationTypes.add(T8CommunicationType.SMS);
        return communicationTypes;
    }

    @Override
    public T8CommunicationDetailsProvider getCommunicationDetailProviderInstance(T8Context context, Map<String, Object> providerParameters)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.communication.detail.provider.T8FixedMobileNumberDetailProvider").getConstructor(T8Context.class, T8FixedMobileNumberDetailProviderDefinition.class, Map.class);
            return (T8CommunicationDetailsProvider) constructor.newInstance(context, this, providerParameters);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameters()
    {
        List<T8DataParameterDefinition> inputParameterDefinitions = new ArrayList<T8DataParameterDefinition>();
        inputParameterDefinitions.add(new T8DataParameterDefinition(getPublicIdentifier() + P_ALTERNATE_MOBILE_NUMBER_LIST, "Atlernate Mobile Number List", "A List of Mobile Numbers if the default should not be used.", new T8DtList(T8DataType.STRING)));
        inputParameterDefinitions.add(new T8DataParameterDefinition(getPublicIdentifier() + P_ALTERNATE_MOBILE_NUMBER, "Atlernate Mobile Number", "The mobile number that will be retured to the communication service.", T8DataType.STRING));

        return inputParameterDefinitions;
    }

    @Override
    public List<T8DataParameterDefinition> getAvailableDetails()
    {
        List<T8DataParameterDefinition> availableDetails = new ArrayList<T8DataParameterDefinition>();

        return availableDetails;
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {

        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
}
