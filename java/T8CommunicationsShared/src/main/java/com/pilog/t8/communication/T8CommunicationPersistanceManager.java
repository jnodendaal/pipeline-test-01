/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.communication;


/**
 *
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8CommunicationPersistanceManager
{
    public abstract void communicationPacketAdded(String serviceIdentifier, T8CommunicationMessage communicationPacket);

    public abstract void communicationPacketRemoved(String serviceIdentifier, T8CommunicationMessage communicationPacket);

    public abstract void communicationFailed(String serviceIdentifier, T8CommunicationMessage communicationPacket);
}
