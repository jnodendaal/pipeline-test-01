package com.pilog.t8.communication;

import java.util.*;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public interface T8CommunicationDetailsProvider
{
    /***
     * @return Communication Types supported by this provider. Most times this will just ask the definition for supported types.
     */
    public List<T8CommunicationType> getSupportedCommunicationTypes();
    /***
     * @param tx
     * @param communicationType The Communication Type for which details are requested
     * @param messageDetailsMapping
     * @return A List of communication details of the requested type.
     */
    public Set<T8CommunicationDetails> getCommunicationDetails(T8DataTransaction tx, T8CommunicationType communicationType, Map<String, String> messageDetailsMapping);
}
