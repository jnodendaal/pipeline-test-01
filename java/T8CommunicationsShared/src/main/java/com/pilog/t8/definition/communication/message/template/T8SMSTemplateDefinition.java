package com.pilog.t8.definition.communication.message.template;

import com.pilog.t8.communication.T8CommunicationMessageState;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.communication.message.T8SMSMessage;
import com.pilog.t8.security.T8Context;

/**
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8SMSTemplateDefinition extends T8CommunicationMessageDefinition
{
    // -------- Definition Meta-Data -------- /=
    public static final String IDENTIFIER_PREFIX = "SMS_";
    public enum Datum
    {
    };
    // -------- Definition Meta-Data -------- //

    public T8SMSTemplateDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8CommunicationType getType()
    {
        return T8CommunicationType.SMS;
    }

    @Override
    public T8SMSMessage getMessageInstance(T8Context context, T8CommunicationMessageState state)
    {
        T8SMSMessage message;

        message = new T8SMSMessage(state.getRecipientDisplayName(), state.getPriority(), state.getRecipientAddress(), state.getContent());
        message.setIdentifier(state.getMessageIdentifier());
        message.setInstanceIdentifier(state.getMessageInstanceIdentifier());
        message.setCommunicationIdentifier(state.getCommunicationIdentifier());
        message.setCommunicationInstanceIdentifier(state.getCommunicationInstanceIdentifier());
        message.setFailureCount(state.getFailureCount());
        message.setSenderIdentifier(state.getSenderIdentifier());
        message.setSenderInstanceIdentifier(state.getSenderInstanceIdentifier());

        return message;
    }
}
