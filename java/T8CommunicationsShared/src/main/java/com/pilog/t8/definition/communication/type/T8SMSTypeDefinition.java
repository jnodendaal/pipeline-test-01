/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.communication.type;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.definition.communication.T8CommunicationTypeDefinition;
import com.pilog.t8.security.T8Context;
import java.util.*;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8SMSTypeDefinition extends T8CommunicationTypeDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_TYPE_SMS";
    public static final String DISPLAY_NAME = "SMS";
    public static final String DESCRIPTION = "SMS Communication Type";
    public static final String IDENTIFIER_PREFIX = "SMS_";
    public static final String VERSION = "0";

    public enum Datum
    {
    };
// -------- Definition Meta-Data -------- //
    public T8SMSTypeDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8CommunicationType getType()
    {
        return T8CommunicationType.SMS;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings)
            throws Exception
    {
    }

     @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>();
        datumTypes.addAll(super.getDatumTypes());
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
}
