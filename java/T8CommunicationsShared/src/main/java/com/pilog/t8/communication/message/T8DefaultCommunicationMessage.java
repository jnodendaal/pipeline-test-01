package com.pilog.t8.communication.message;

import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.communication.T8CommunicationMessageState;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultCommunicationMessage implements T8CommunicationMessage
{
    protected String serviceIdentifier;
    protected String identifier;
    protected String instanceIdentifier;
    protected String communicationIdentifier;
    protected String communicationInstanceIdentifier;
    private String senderIdentifier;
    private String senderInstanceIdentifier;
    private int priority;
    private String recipientDefinitionIdentifier;
    private String recipientRecordID;
    private String recipientDisplayName;
    private int failureCount;
    private long timeQueued;

    public T8DefaultCommunicationMessage(String recipientDisplayName, int priority)
    {
        this.instanceIdentifier = T8IdentifierUtilities.createNewGUID();
        this.recipientDisplayName = recipientDisplayName;
        this.priority = priority;
        this.failureCount = 0;
        this.timeQueued = -1;
    }

    protected T8CommunicationMessageState getMessageState()
    {
        T8CommunicationMessageState state;

        state = new T8CommunicationMessageState(identifier, instanceIdentifier);
        state.setServiceIdentifier(serviceIdentifier);
        state.setMessageInstanceIdentifier(instanceIdentifier);
        state.setCommunicationIdentifier(communicationIdentifier);
        state.setCommunicationInstanceIdentifier(communicationInstanceIdentifier);
        state.setSenderIdentifier(senderIdentifier);
        state.setSenderInstanceIdentifier(senderInstanceIdentifier);
        state.setPriority(priority);
        state.setRecipientAddress(getRecipientAddress());
        state.setRecipientDisplayName(recipientDisplayName);
        state.setFailureCount(failureCount);
        state.setSubject(getSubject());
        state.setContent(getContent());
        state.setTimeQueued(timeQueued);

        return state;
    }

    @Override
    public String getInstanceIdentifier()
    {
        return instanceIdentifier;
    }

    public void setInstanceIdentifier(String identifier)
    {
        this.instanceIdentifier = identifier;
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    @Override
    public String getServiceIdentifier()
    {
        return serviceIdentifier;
    }

    @Override
    public void setServiceIdentifier(String serviceIdentifier)
    {
        this.serviceIdentifier = serviceIdentifier;
    }

    @Override
    public int getPriority()
    {
        return priority;
    }

    public void setPriortiy(int priority)
    {
        this.priority = priority;
    }

    @Override
    public int getFailureCount()
    {
        return failureCount;
    }

    @Override
    public void setFailureCount(int count)
    {
        this.failureCount = count;
    }

    public void incrementFailureCount()
    {
        failureCount++;
    }

    @Override
    public String getRecipientRecordID()
    {
        return recipientRecordID;
    }

    public void setRecipientRecordID(String recipientRecordID)
    {
        this.recipientRecordID = recipientRecordID;
    }

    @Override
    public String getRecipientIdentifier()
    {
        return recipientDefinitionIdentifier;
    }

    public void setRecipientDefinitionIdentifier(String recipientDefinitionIdentifier)
    {
        this.recipientDefinitionIdentifier = recipientDefinitionIdentifier;
    }

    @Override
    public String getRecipientDisplayName()
    {
        return recipientDisplayName;
    }

    public void setRecipientDisplayName(String recipientDisplayName)
    {
        this.recipientDisplayName = recipientDisplayName;
    }

    @Override
    public String getCommunicationIdentifier()
    {
        return communicationIdentifier;
    }

    @Override
    public void setCommunicationIdentifier(String communicationIdentifier)
    {
        this.communicationIdentifier = communicationIdentifier;
    }

    @Override
    public String getCommunicationInstanceIdentifier()
    {
        return communicationInstanceIdentifier;
    }

    @Override
    public void setCommunicationInstanceIdentifier(String communicationInstanceIdentifier)
    {
        this.communicationInstanceIdentifier = communicationInstanceIdentifier;
    }

    @Override
    public String getSenderIdentifier()
    {
        return senderIdentifier;
    }

    public void setSenderIdentifier(String initiatorUserIdentifier)
    {
        this.senderIdentifier = initiatorUserIdentifier;
    }

    @Override
    public String getSenderInstanceIdentifier()
    {
        return senderInstanceIdentifier;
    }

    public void setSenderInstanceIdentifier(String senderInstanceIdentifier)
    {
        this.senderInstanceIdentifier = senderInstanceIdentifier;
    }

    @Override
    public long getTimeQueued()
    {
        return timeQueued;
    }

    @Override
    public void setTimeQueued(long timeQueued)
    {
        this.timeQueued = timeQueued;
    }

    @Override
    public String toString()
    {
        return "T8DefaultCommunicationMessage{" + "identifier=" + identifier + ", instanceIdentifier=" + instanceIdentifier + ", communicationIdentifier=" + communicationIdentifier + ", communicationInstanceIdentifier=" + communicationInstanceIdentifier + '}';
    }
}
