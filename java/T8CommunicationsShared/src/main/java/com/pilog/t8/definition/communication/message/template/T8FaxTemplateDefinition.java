package com.pilog.t8.definition.communication.message.template;

import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.communication.message.template.email.attachment.T8EmailMessageTemplateAttachementDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public abstract class T8FaxTemplateDefinition extends T8CommunicationMessageDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_MESSAGE_TEMPLATE_FAX_TYPE";
    public static final String DISPLAY_NAME = "Fax Template";
    public static final String DESCRIPTION = "Fax Message Template";
    public static final String IDENTIFIER_PREFIX = "FAX_TEMPL_";
    public enum Datum
    {
        NOTES,
        ATTACHMENTS
    };
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_IDENTIFIER_ATTACHMENTS = "$ATTACHMENTS";
    public static final String PARAMETER_IDENTIFIER_ATTACHMENT_IDENTIFIER = "$IDENTIFIER";
    public static final String PARAMETER_IDENTIFIER_ATTACHMENT_NAME = "$NAME";
    public static final String PARAMETER_IDENTIFIER_ATTACHMENT_DESCRIPTION = "$DESCRIPTION";
    public static final String PARAMETER_IDENTIFIER_ATTACHMENT_DATA = "$DATA";

    public T8FaxTemplateDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8CommunicationType getType()
    {
        return T8CommunicationType.FAX;
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.NOTES.toString(), "Notes", "The notes that will be printed on the bottom of the fax message."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ATTACHMENTS.toString(), "Attachments", "The attachments that will be sent with the email."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ATTACHMENTS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8EmailMessageTemplateAttachementDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    public void setNotes(String notes)
    {
        setDefinitionDatum(Datum.NOTES.toString(), notes);
    }

    public String getNotes()
    {
        return (String) getDefinitionDatum(Datum.NOTES.toString());
    }

    public void setAttachments(List<T8EmailMessageTemplateAttachementDefinition> definitions)
    {
        setDefinitionDatum(Datum.ATTACHMENTS.toString(), definitions);
    }

    public List<T8EmailMessageTemplateAttachementDefinition> getAttachments()
    {
        return (List<T8EmailMessageTemplateAttachementDefinition>) getDefinitionDatum(Datum.ATTACHMENTS.toString());
    }
}
