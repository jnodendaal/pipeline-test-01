package com.pilog.t8.communication;

import com.pilog.t8.T8CommunicationManager;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public interface T8MessageTemplate
{
    /**
     * Initializes the generator in order to set the state required for message
     * generation.
     * @param tx The transaction to use for initialization and subsequent
     * message generation.
     * @throws Exception throws an exception if an error occurred
     * while initializing.
     */
    public void initializeGenerator(T8DataTransaction tx) throws Exception;

    /**
     * This method will generate a message that will be used when sending a communication.
     * @param context The session context from which this message is
     * generated (if any).
     * @param communicationDetails The communication details of the communication about to be sent
     * @param manager The communication manager
     * @param additionalDetails
     * @return A String of the message ready to be sent to the communication detail recipient
     * @throws java.lang.Exception
     */
    public T8CommunicationMessage generateMessage(T8Context context, T8CommunicationDetails communicationDetails, T8CommunicationManager manager, Map<String, Object> additionalDetails) throws Exception;
}
