package com.pilog.t8.definition.communication.detail.provider;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.communication.T8CommunicationDetailsProvider;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8CommunicationDetailsProviderDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_COMMUNICATION_ADDRESS_PROVIDERS";
    public static final String STORAGE_PATH = "/communication";
    public static final String IDENTIFIER_PREFIX = "DP_";

    public enum Datum
    {
        ACTIVE
    };
// -------- Definition Meta-Data -------- //

    public T8CommunicationDetailsProviderDefinition(String identifier)
    {
        super(identifier);
    }

    public abstract T8CommunicationDetailsProvider getCommunicationDetailProviderInstance(T8Context context, Map<String, Object> providerParameters);

    public abstract List<T8CommunicationType> getSupportedCommunicationTypes();

    public abstract List<T8DataParameterDefinition> getInputParameters();

    public abstract List<T8DataParameterDefinition> getAvailableDetails();

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings)
            throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {

        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>(2);
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.ACTIVE.toString(), "Active", "Sets the communication detail provider to active/inactive."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return null;
    }

    public Boolean isActive()
    {
        return (Boolean) getDefinitionDatum(Datum.ACTIVE.toString());
    }

    public void setActive(Boolean active)
    {
        setDefinitionDatum(Datum.ACTIVE.toString(), active);
    }
}
