package com.pilog.t8.communication.message;

import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageState;

/**
 * @author Bouwer du Preez
 */
public class T8SMSMessage extends T8DefaultCommunicationMessage implements T8CommunicationMessage
{
    private String cellphoneNumber;
    private String messageContent;

    public T8SMSMessage(String recipientDisplayName, int priority, String cellphoneNumber, String messageContent)
    {
        super(recipientDisplayName, priority);
        this.cellphoneNumber = cellphoneNumber;
        this.messageContent = messageContent;
    }

    @Override
    public T8CommunicationMessageState getState()
    {
        T8CommunicationMessageState state;

        // No Sms-specific state to set.
        state = getMessageState();
        return state;
    }

    public String getCellphoneNumber()
    {
        return cellphoneNumber;
    }

    public void setCellphoneNumber(String cellphoneNumber)
    {
        this.cellphoneNumber = cellphoneNumber;
    }

    public String getMessageContent()
    {
        return messageContent;
    }

    public void setMessageContent(String messageContent)
    {
        this.messageContent = messageContent;
    }

    @Override
    public String getRecipientAddress()
    {
        return getCellphoneNumber();
    }

    @Override
    public String getContent()
    {
        return getMessageContent();
    }

    @Override
    public String getSubject()
    {
        return null;
    }
}
