package com.pilog.t8.definition.communication;

import com.pilog.t8.definition.communication.T8CommunicationDefinition;
import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.communication.T8Communication;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8DefaultCommunicationDefinition extends T8CommunicationDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION";
    public static final String DISPLAY_NAME = "Communication";
    public static final String DESCRIPTION = "A Communication Packet";
    public static final String IDENTIFIER_PREFIX = "COMM_";
    public enum Datum
    {
        COMMUNICATION_TYPE_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public T8DefaultCommunicationDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.COMMUNICATION_TYPE_DEFINITIONS.toString(), "Communication Type", "The communication types that this communication will execute when triggered."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.COMMUNICATION_TYPE_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8CommunicationTypeDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8Communication getCommunicationInstance(T8CommunicationManager communicationManager)
    {
        return T8Reflections.getInstance("com.pilog.t8.communication.T8DefaultCommunication", new Class<?>[]{T8DefaultCommunicationDefinition.class, T8CommunicationManager.class}, this, communicationManager);
    }

    public List<T8CommunicationTypeDefinition> getCommunicationTypeDefinitions()
    {
        return getDefinitionDatum(Datum.COMMUNICATION_TYPE_DEFINITIONS);
    }

    public void setCommunicationTypeDefinitions(List<T8CommunicationTypeDefinition> definitions)
    {
        setDefinitionDatum(Datum.COMMUNICATION_TYPE_DEFINITIONS, definitions);
    }
}
