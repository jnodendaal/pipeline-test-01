/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.communication.detail;

import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.definition.user.T8UserDefinition.Datum;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class AvailableT8UserDetails
{
    public static List<T8DataParameterDefinition> getAvailableT8UserDetails(String identifier)
    {
        List<T8DataParameterDefinition> details =  new ArrayList<T8DataParameterDefinition>();
        T8DataParameterDefinition detail;

        for (Datum datum : T8UserDefinition.Datum.values())
        {
            detail = new T8DataParameterDefinition(identifier + "$" + datum.toString());
            details.add(detail);
        }
        return details;
    }
}
