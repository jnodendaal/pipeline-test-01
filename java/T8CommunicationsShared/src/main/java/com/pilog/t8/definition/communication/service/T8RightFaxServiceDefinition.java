package com.pilog.t8.definition.communication.service;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.communication.T8CommunicationService;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8RightFaxServiceDefinition extends T8EmailServiceDefinition
{
    private static final T8Logger logger = T8Log.getLogger(T8RightFaxServiceDefinition.class);

    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_SERVICE_FAX_RIGHTFAX";
    public static final String DISPLAY_NAME = "Right Fax Service";
    public static final String DESCRIPTION = "Right Fax Service";
    public static final String IDENTIFIER_PREFIX = "FAX_SERV_";
    public enum Datum
    {
        MAIL_SERVER_DOMAIN_ADDRESS
    };
    // -------- Definition Meta-Data -------- //

    public T8RightFaxServiceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.MAIL_SERVER_DOMAIN_ADDRESS.toString(), "Fax Mail Server Domain Adrress", "The domain adress of the mail server to which fax emails will be sent. Eg: faxserver@faxdomain.com"));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8CommunicationType getType()
    {
        return T8CommunicationType.FAX;
    }

    @Override
    public T8CommunicationService getServiceInstance(T8Context context, T8CommunicationManager manager)
    {
        return T8Reflections.getInstance("com.pilog.t8.communication.service.T8RightFaxService", new Class<?>[]{T8Context.class, T8RightFaxServiceDefinition.class, T8CommunicationManager.class}, context, this, manager);
    }

    public void setMailServerDomainAdress(String mailServerDomainAdress)
    {
        setDefinitionDatum(Datum.MAIL_SERVER_DOMAIN_ADDRESS.toString(), mailServerDomainAdress);
    }

    public String getMailServerDomainAdress()
    {
        return (String) getDefinitionDatum(Datum.MAIL_SERVER_DOMAIN_ADDRESS.toString());
    }
}
