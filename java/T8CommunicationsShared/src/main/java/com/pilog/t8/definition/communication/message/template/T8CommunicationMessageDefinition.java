package com.pilog.t8.definition.communication.message.template;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationMessage;
import com.pilog.t8.communication.T8CommunicationMessageState;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.communication.T8MessageTemplate;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8CommunicationMessageDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_COMMUNICATION_MESSAGE_TEMPLATE";
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_MESSAGE_TEMPLATE";
    public static final String STORAGE_PATH = "/communication";
    public static final String DISPLAY_NAME = "Message Template";
    public static final String DESCRIPTION = "Communication Message Template";
    public static final String IDENTIFIER_PREFIX = "COMM_MESS_TEMPL_";
    public enum Datum
    {
        INPUT_PARAMETER_DEFINITIONS,
        TEMPLATE_DATA_SCRIPT,
        TEMPLATE_TEXT,
        COMMUNICATION_DETAIL_PARAMETER_DEFINITIONS
    };
    // -------- Definition Meta-Data -------- //

    public static final String P_COMMUNICATION_DETAILS_OBJECT = "communication_details";

    public T8CommunicationMessageDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<>();
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETER_DEFINITIONS.toString(), "Input Parameters", "The input data parameters required by this Message template."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.COMMUNICATION_DETAIL_PARAMETER_DEFINITIONS.toString(), "Communication Detail Parameters", "The communication detail data parameters required by this Message template."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.TEMPLATE_DATA_SCRIPT.toString(), "Template Data Script", "The script used to generate the template data that will be used by the meesage template."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.TEMPLATE_TEXT.toString(), "Template Text", "The Template Text that will be used to construct the final message to be sent."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.COMMUNICATION_DETAIL_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8DataParameterDefinition.TYPE_IDENTIFIER));
        else if (Datum.TEMPLATE_DATA_SCRIPT.toString().equals(datumIdentifier)) return createDefinitionOption(definitionContext.getDefinitionTypeMetaData(T8MessageTemplateDataScriptDefinition.TYPE_IDENTIFIER));
        else return null;
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.INPUT_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)) || (Datum.COMMUNICATION_DETAIL_PARAMETER_DEFINITIONS.toString().equals(datumIdentifier)))
        {
            return T8Reflections.getInstance("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor", new Class<?>[]{T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class}, definitionContext, this, getDatumType(datumIdentifier));
        } else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        T8DataParameterDefinition communicationDetailParameterDefinition;

        // Make sure the communication details object is always available.
        if (getInputParametersDefinitions() == null || (getInputParametersDefinitions() != null && getInputParametersDefinitions().isEmpty()))
        {
            setInputParameterDefinitions(new ArrayList<>(1));
        }

        communicationDetailParameterDefinition = new T8DataParameterDefinition(P_COMMUNICATION_DETAILS_OBJECT, "Communication Detail", "The Communication Detail that will be used when the template gets generated.", T8DataType.CUSTOM_OBJECT);
        if (!getInputParametersDefinitions().contains(communicationDetailParameterDefinition))
        {
            getInputParametersDefinitions().add(communicationDetailParameterDefinition);
        }
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (Strings.isNullOrEmpty(getTemplate())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.TEMPLATE_TEXT.toString(), "Template is Empty", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    public abstract T8MessageTemplate getMessageTemplateInstance(T8Context context, Map<String, Object> messageParameters);
    public abstract T8CommunicationMessage getMessageInstance(T8Context context, T8CommunicationMessageState state);
    public abstract T8CommunicationType getType();

    public void setTemplate(String messageBodyTemplate)
    {
        setDefinitionDatum(Datum.TEMPLATE_TEXT, messageBodyTemplate);
    }

    public String getTemplate()
    {
        return getDefinitionDatum(Datum.TEMPLATE_TEXT);
    }

    public void setTemplateDataScript(T8ServerContextScriptDefinition templateScript)
    {
        setDefinitionDatum(Datum.TEMPLATE_DATA_SCRIPT, templateScript);
    }

    public T8ServerContextScriptDefinition getTemplateDataScript()
    {
        return getDefinitionDatum(Datum.TEMPLATE_DATA_SCRIPT);
    }

    public void setInputParameterDefinitions(List<T8DataParameterDefinition> inputParameters)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS, inputParameters);
    }

    public List<T8DataParameterDefinition> getInputParametersDefinitions()
    {
        return getDefinitionDatum(Datum.INPUT_PARAMETER_DEFINITIONS);
    }

    public void setCommunicationDetailParameterDefinitions(List<T8DataParameterDefinition> inputParameters)
    {
        setDefinitionDatum(Datum.COMMUNICATION_DETAIL_PARAMETER_DEFINITIONS, inputParameters);
    }

    public List<T8DataParameterDefinition> getCommunicationDetailParameterDefinitions()
    {
        return getDefinitionDatum(Datum.COMMUNICATION_DETAIL_PARAMETER_DEFINITIONS);
    }
}
