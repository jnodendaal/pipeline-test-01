/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.communication.message.template;

import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.communication.T8MessageTemplate;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.Map;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class T8SMSMessageTemplateFreeMarkerDefinition extends T8SMSTemplateDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_MESSAGE_TEMPLATE_SMS_FREE_MARKER";
    public static final String DISPLAY_NAME = "SMS FreeMarker";
    public static final String DESCRIPTION = "SMS Free Marker Template";

    public enum Datum
    {
    };
// -------- Definition Meta-Data -------- //
    public T8SMSMessageTemplateFreeMarkerDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8MessageTemplate getMessageTemplateInstance(T8Context context, Map<String,Object> messageParameters)
    {
         try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.communication.template.T8SMSMessageTemplateFreeMarker").getConstructor(T8Context.class, T8SMSMessageTemplateFreeMarkerDefinition.class, Map.class);
            return (T8MessageTemplate) constructor.newInstance(context, this, messageParameters);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((T8CommunicationMessageDefinition.Datum.TEMPLATE_TEXT.toString().equals(datumIdentifier)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.TextAreaDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor) constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else
        {
            return super.getDatumEditor(definitionContext, datumIdentifier);
        }
    }
}
