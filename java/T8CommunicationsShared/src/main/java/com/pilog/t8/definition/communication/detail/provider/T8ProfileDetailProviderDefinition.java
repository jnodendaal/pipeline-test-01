/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.definition.communication.detail.provider;

import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.communication.T8CommunicationDetailsProvider;
import com.pilog.t8.communication.detail.AvailableT8UserDetails;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8ProfileDetailProviderDefinition extends T8CommunicationDetailsProviderDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_DETAIL_PROVIDER_T8PROFILE";
    public static final String DISPLAY_NAME = "T8 Profile";
    public static final String DESCRIPTION = "T8 Profile Detail provider";
    public enum Datum
    {
    };
    // -------- Definition Meta-Data -------- //

    public static final String P_PROFILE_IDENTIFIER_LIST = "$P_PROFILE_IDENTIFIER_LIST";
    public static final String P_PROFILE_IDENTIFIER = "$P_PROFILE_IDENTIFIER";

    public T8ProfileDetailProviderDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public List<T8CommunicationType> getSupportedCommunicationTypes()
    {
        List<T8CommunicationType> communicationTypes = new ArrayList<T8CommunicationType>();
        communicationTypes.add(T8CommunicationType.EMAIL);
        communicationTypes.add(T8CommunicationType.SMS);
        return communicationTypes;
    }

    @Override
    public T8CommunicationDetailsProvider getCommunicationDetailProviderInstance(T8Context context, Map<String, Object> providerParameters)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.communication.detail.provider.T8ProfileDetailProvider").getConstructor(T8Context.class, Map.class, T8ProfileDetailProviderDefinition.class);
            return (T8CommunicationDetailsProvider)constructor.newInstance(context, this, providerParameters);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameters()
    {
        List<T8DataParameterDefinition> inputParameterDefinitions = new ArrayList<T8DataParameterDefinition>();
        inputParameterDefinitions.add(new T8DataParameterDefinition(getPublicIdentifier() + P_PROFILE_IDENTIFIER_LIST, "Profile Parameter List", "The profiles that must be retrieved.", new T8DtList(T8DataType.DEFINITION_IDENTIFIER)));
        inputParameterDefinitions.add(new T8DataParameterDefinition(getPublicIdentifier() + P_PROFILE_IDENTIFIER, "Profile Parameter", "The profile that must be retrieved.", T8DataType.STRING));

        return inputParameterDefinitions;
    }

    @Override
    public List<T8DataParameterDefinition> getAvailableDetails()
    {
        return AvailableT8UserDetails.getAvailableT8UserDetails(getIdentifier());
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {

        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }
}
