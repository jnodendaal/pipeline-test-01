package com.pilog.t8.definition.communication.message.template;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.definition.communication.message.template.email.attachment.T8EmailMessageTemplateAttachementDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hennie.brink@pilog.co.za
 */
public abstract class T8EmailTemplateDefinition extends T8CommunicationMessageDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String IDENTIFIER_PREFIX = "EMAIL_TEMPL_";
    public enum Datum {
        SUBJECT,
        ATTACHMENTS,
        BCC_ENABLED,
        BCC_GENERIC_RECIPIENT
    };
    // -------- Definition Meta-Data -------- //

    public static final String PARAMETER_IDENTIFIER_ATTACHMENTS = "$ATTACHMENTS";
    public static final String PARAMETER_IDENTIFIER_ATTACHMENT_IDENTIFIER = "$IDENTIFIER";
    public static final String PARAMETER_IDENTIFIER_ATTACHMENT_NAME = "$NAME";
    public static final String PARAMETER_IDENTIFIER_ATTACHMENT_DESCRIPTION = "$DESCRIPTION";
    public static final String PARAMETER_IDENTIFIER_ATTACHMENT_DATA = "$DATA";

    public T8EmailTemplateDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public T8CommunicationType getType()
    {
        return T8CommunicationType.EMAIL;
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        //Add the subject on top of the template so that it is more visible
        datumTypes.add(datumTypes.size() - 1, new T8DefinitionDatumType(T8DataType.STRING, Datum.SUBJECT.toString(), "E-Mail Subject", "The email subject to use. Can use FreeMarker Template."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ATTACHMENTS.toString(), "Attachments", "The attachments that will be sent with the email."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.BCC_ENABLED.toString(), "BCC Enabled", "Whether or not to add the recipient as a Blind Copy.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.BCC_GENERIC_RECIPIENT.toString(), "BCC Generic Recipient", "Visible recipient when BCC Enabled is in effect."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.ATTACHMENTS.toString().equals(datumIdentifier)) return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8EmailMessageTemplateAttachementDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public List<T8DefinitionValidationError> validateDefinition(T8ServerContext serverContext, T8SessionContext sessionContext, DefinitionValidationType validationType)
    {
        List<T8DefinitionValidationError> validationErrors;

        validationErrors = super.validateDefinition(serverContext, sessionContext, validationType);
        if (Strings.isNullOrEmpty(getSubject())) validationErrors.add(new T8DefinitionValidationError(getProjectIdentifier(), getPublicIdentifier(), Datum.SUBJECT.toString(), "Subject is Empty", T8DefinitionValidationError.ErrorType.CRITICAL));

        return validationErrors;
    }

    public void setSubject(String subject)
    {
        setDefinitionDatum(Datum.SUBJECT, subject);
    }

    public String getSubject()
    {
        return getDefinitionDatum(Datum.SUBJECT);
    }

    public void setAttachments(List<T8EmailMessageTemplateAttachementDefinition> definitions)
    {
        setDefinitionDatum(Datum.ATTACHMENTS, definitions);
    }

    public List<T8EmailMessageTemplateAttachementDefinition> getAttachments()
    {
        return getDefinitionDatum(Datum.ATTACHMENTS);
    }

    public void setBccEnabled(Boolean bccEnabled)
    {
        setDefinitionDatum(Datum.BCC_ENABLED, bccEnabled);
    }

    public boolean isBccEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.BCC_ENABLED));
    }

    public void setBccGenericRecipient(String genericRecipient)
    {
        setDefinitionDatum(Datum.BCC_GENERIC_RECIPIENT, genericRecipient);
    }

    public String getBccGenericRecipient()
    {
        return getDefinitionDatum(Datum.BCC_GENERIC_RECIPIENT);
    }
}
