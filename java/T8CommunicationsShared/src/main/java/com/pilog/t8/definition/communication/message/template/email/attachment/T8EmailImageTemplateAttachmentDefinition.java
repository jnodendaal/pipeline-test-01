package com.pilog.t8.definition.communication.message.template.email.attachment;

import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.gfx.image.T8ImageDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;

/**
 * @author Hennie Brink
 */
public class T8EmailImageTemplateAttachmentDefinition extends T8EmailMessageTemplateAttachementDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_EMAIL_MESSAGE_TEMPLATE_ATTACHMENT_IMAGE";
    public static final String DISPLAY_NAME = "T8 Image Attachment";
    public static final String DESCRIPTION = "Attaches a image file.";
    public static final String IDENTIFIER_PREFIX = "EMAIL_ATTACH_IMG_";
    public enum Datum
    {
        IMAGE_IDENTIFIER,
        EMBEDDED,
        CONTENT_ID
    };
    // -------- Definition Meta-Data -------- //

    public T8EmailImageTemplateAttachmentDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.IMAGE_IDENTIFIER.toString(), "Image Identifier", "The T8 image identifier to be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.EMBEDDED.toString(), "Embedded", "If the image should be embedded in the body instead of attached.", false));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.CONTENT_ID.toString(), "Content ID", "The content ID that will be used in the html."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if(Datum.IMAGE_IDENTIFIER.toString().equals(datumIdentifier)) return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8ImageDefinition.GROUP_IDENTIFIER));
        else return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public T8CommunicationMessageAttachment getInstance()
    {
        return T8Reflections.getInstance("com.pilog.t8.communication.template.attachment.email.T8EmailAttachmentImage", new Class<?>[]{T8EmailImageTemplateAttachmentDefinition.class}, this);
    }

    public void setImageIdentifier(String name)
    {
        setDefinitionDatum(Datum.IMAGE_IDENTIFIER, name);
    }

    public String getImageIdentifier()
    {
        return getDefinitionDatum(Datum.IMAGE_IDENTIFIER);
    }

    public void setEmbedded(Boolean embedded)
    {
        setDefinitionDatum(Datum.EMBEDDED, embedded);
    }

    public Boolean isEmbedded()
    {
        return getDefinitionDatum(Datum.EMBEDDED);
    }

    public void setContentID(String name)
    {
        setDefinitionDatum(Datum.CONTENT_ID, name);
    }

    public String getContentID()
    {
        return getDefinitionDatum(Datum.CONTENT_ID);
    }

}
