package com.pilog.t8.definition.communication.service;

import com.pilog.t8.definition.communication.T8CommunicationServiceDefinition;
import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.communication.T8CommunicationService;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8EmailServiceDefinition extends T8CommunicationServiceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_EMAIL_SERVICE";
    public static final String DISPLAY_NAME = "Email Service";
    public static final String DESCRIPTION = "Email Service";
    public static final String IDENTIFIER_PREFIX = "EMAIL_SERV_";
    public enum Datum
    {
        HOSTNAME,
        FROM_MAIL_ADDRESS,
        USERNAME,
        PASSWORD,
        SSL_CONNECTION,
        SSL_CHECK_SERVER_IDENTITY,
        START_TLS_ENABLED,
        START_TLS_REQUIRED,
        SMTP_PORT,
        SSL_SMTP_PORT
    };
    // -------- Definition Meta-Data -------- //

    public T8EmailServiceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.HOSTNAME.toString(), "Hostname/IP Address", "The Host Name or IP Address of the mail server to use when sending emails."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FROM_MAIL_ADDRESS.toString(), "From Mail Address", "The Email Address that will be used to tell the sender the origine of the email."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.USERNAME.toString(), "User Name(Optional)", "The username to use when sending a messages."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PASSWORD.toString(), "Password(Optional)", "The password to use when sending a messages."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SSL_CONNECTION.toString(), "SSL Connection(Optional)", "If an SSL connection should be used."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.SSL_CHECK_SERVER_IDENTITY.toString(), "SSL Check Server Identity(Optional)", "If the SSL Check server identity property should be enabled."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.START_TLS_ENABLED.toString(), "Start TLS Enabled(Optional)", "If the start TLS property should be enabled."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.BOOLEAN, Datum.START_TLS_REQUIRED.toString(), "Start TLS Required(Optional)", "If the start TLS proeprty is required."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.SMTP_PORT.toString(), "SMTP Port(Optional)", "The SMTP port if different from default."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.INTEGER, Datum.SSL_SMTP_PORT.toString(), "SSL SMTP Port(Optional)", "The SSL SMTP port if different from default."));
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8CommunicationType getType()
    {
        return T8CommunicationType.EMAIL;
    }

    @Override
    public T8CommunicationService getServiceInstance(T8Context context, T8CommunicationManager manager)
    {
        return T8Reflections.getInstance("com.pilog.t8.communication.service.T8EmailService", new Class<?>[]{T8Context.class, T8EmailServiceDefinition.class, T8CommunicationManager.class}, context, this, manager);
    }

    public void setHostname(String hostName)
    {
        setDefinitionDatum(Datum.HOSTNAME, hostName);
    }

    public String getHostname()
    {
        return getDefinitionDatum(Datum.HOSTNAME);
    }

    public void setFromMailAddress(String hostName)
    {
        setDefinitionDatum(Datum.FROM_MAIL_ADDRESS, hostName);
    }

    public String getFromMailAddress()
    {
        return getDefinitionDatum(Datum.FROM_MAIL_ADDRESS);
    }

    public void setUserName(String UserName)
    {
        setDefinitionDatum(Datum.USERNAME, UserName);
    }

    public String getUserName()
    {
        return getDefinitionDatum(Datum.USERNAME);
    }

    public void setPassword(String Password)
    {
        setDefinitionDatum(Datum.PASSWORD, Password);
    }

    public String getPassword()
    {
        return getDefinitionDatum(Datum.PASSWORD);
    }

    public void setPassword(Boolean property)
    {
        setDefinitionDatum(Datum.PASSWORD, property);
    }

    public Boolean isSSLConnection()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.SSL_CONNECTION));
    }

    public void setSSLConnection(Boolean property)
    {
        setDefinitionDatum(Datum.SSL_CONNECTION.toString(), property);
    }

    public Boolean isCheckSSLServerIdentity()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.SSL_CHECK_SERVER_IDENTITY));
    }

    public void setCheckSSLServerIdentity(Boolean property)
    {
        setDefinitionDatum(Datum.SSL_CHECK_SERVER_IDENTITY, property);
    }

    public Boolean isStartTLSEnabled()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.START_TLS_ENABLED));
    }

    public void setStartTLSEnabled(Boolean property)
    {
        setDefinitionDatum(Datum.START_TLS_ENABLED, property);
    }

    public Boolean isStartTLSRequired()
    {
        return Boolean.TRUE.equals(getDefinitionDatum(Datum.START_TLS_REQUIRED));
    }

    public void setStartTLSRequired(Boolean property)
    {
        setDefinitionDatum(Datum.START_TLS_REQUIRED, property);
    }

    public Integer getSMTPPort()
    {
        return getDefinitionDatum(Datum.SMTP_PORT);
    }

    public void setSMTPPort(Integer property)
    {
        setDefinitionDatum(Datum.SMTP_PORT, property);
    }

    public Integer getSSLSMTPPort()
    {
        return getDefinitionDatum(Datum.SSL_SMTP_PORT);
    }

    public void setSSLSMTPPort(Integer property)
    {
        setDefinitionDatum(Datum.SSL_SMTP_PORT, property);
    }
}
