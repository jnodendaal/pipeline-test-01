package com.pilog.t8.definition.communication.detail.provider.entity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.ui.T8DefinitionDatumEditor;
import com.pilog.t8.communication.T8CommunicationDetailsProvider;
import com.pilog.t8.definition.communication.detail.provider.T8CommunicationDetailsProviderDefinition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.filter.T8DataFilterDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8EntityDetailProviderDefinition extends T8CommunicationDetailsProviderDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_DETAIL_PROVIDER_ENTITY";
    public static final String DISPLAY_NAME = "T8 Data Entity";
    public static final String DESCRIPTION = "A Detail Provider that can query a T8 Data entity communication details";
    public enum Datum
    {
        DATA_ENTITY_IDENTIFIER,
        HISTORY_RECORD_ID_FIELD,
        EMAIL_ADDRESS_FIELD,
        MOBILE_NUMBER_FIELD,
        FAX_NUMBER_FIELD,
        FAX_RECIPIENT_NAME_FIELD,
        FAX_COMPANY_NAME_FIELD,
        ADDITIONAL_FIELDS,
        ADDITIONAL_FIELDS_MAPPING,
        INPUT_PARAMETERS,
        DATA_FILTER_DEFINITION
    };
    // -------- Definition Meta-Data -------- //

    public T8EntityDetailProviderDefinition(String id)
    {
        super(id);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();

        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.INPUT_PARAMETERS.toString(), "Input Parameters", "The input parameters of this detail provider."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.DATA_ENTITY_IDENTIFIER.toString(), "Data Entity Identifier", "The identifier of the data entity that should be used for retrieving the communication details."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.HISTORY_RECORD_ID_FIELD.toString(), "History Record ID Field", "The field that will be used as the record ID to save in the history."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.EMAIL_ADDRESS_FIELD.toString(), "Email Address Field", "The field that will be used to retrieve email address information."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.MOBILE_NUMBER_FIELD.toString(), "Mobile Number Field", "The field that will be used to retrieve mobile number information."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FAX_NUMBER_FIELD.toString(), "Fax Number Field", "The field that will be used to retrieve fax number information."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FAX_RECIPIENT_NAME_FIELD.toString(), "Fax Recipient Name Field", "The field that will be used to retrieve fax recipient name information."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.FAX_COMPANY_NAME_FIELD.toString(), "Fax Company Name Field", "The field that will be used to retrieve the fax company name information."));
        datumTypes.add(T8DefinitionDatumType.definitionList(Datum.ADDITIONAL_FIELDS.toString(), "Additional Fields", "Any additional data that can be provided by this detail provider."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.ADDITIONAL_FIELDS_MAPPING.toString(), "Additional Fields To Entity Field Mapping", "The mapping of additional field identifiers to entity field identifiers."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION, Datum.DATA_FILTER_DEFINITION.toString(), "Data Filter", "The data filter that can be used to filter the entity."));


        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.DATA_ENTITY_IDENTIFIER.toString().equals(datumIdentifier))
        {
            return createIdentifierOptions(definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8DataEntityDefinition.GROUP_IDENTIFIER));
        }
        else if (Datum.DATA_FILTER_DEFINITION.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataFilterDefinition.GROUP_IDENTIFIER));
        }
        else if (Datum.EMAIL_ADDRESS_FIELD.toString().equals(datumIdentifier)
                || Datum.MOBILE_NUMBER_FIELD.toString().equals(datumIdentifier)
                || Datum.FAX_NUMBER_FIELD.toString().equals(datumIdentifier)
                || Datum.FAX_RECIPIENT_NAME_FIELD.toString().equals(datumIdentifier)
                || Datum.FAX_COMPANY_NAME_FIELD.toString().equals(datumIdentifier)
                || Datum.HISTORY_RECORD_ID_FIELD.toString().equals(datumIdentifier))
        {
            if(Strings.isNullOrEmpty(getDataEntityIdentifier())) return new ArrayList<T8DefinitionDatumOption>(0);

            T8DataEntityDefinition dataEntityDefinition;
            List<String> fieldIdentifiers;

            dataEntityDefinition = (T8DataEntityDefinition) definitionContext.getRawDefinition(getRootProjectId(), getDataEntityIdentifier());
            fieldIdentifiers = new ArrayList<String>();

            for (T8DataEntityFieldDefinition t8DataEntityFieldDefinition : dataEntityDefinition.getFieldDefinitions())
            {
                fieldIdentifiers.add(t8DataEntityFieldDefinition.getIdentifier());
            }

            return createStringOptions(fieldIdentifiers, true, "Unnavailable");
        }
        else if (Datum.ADDITIONAL_FIELDS.toString().equals(datumIdentifier) || Datum.INPUT_PARAMETERS.toString().equals(datumIdentifier))
        {
            return createDefinitionOptions(definitionContext.getGroupDefinitionTypeMetaData(T8DataParameterDefinition.GROUP_IDENTIFIER));
        }
        else if (Datum.ADDITIONAL_FIELDS_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String entityId;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            entityId = getDataEntityIdentifier();
            if (entityId != null)
            {
                T8DataEntityDefinition dataEntityDefinition;

                dataEntityDefinition = (T8DataEntityDefinition)definitionContext.getRawDefinition(getRootProjectId(), entityId);
                if (dataEntityDefinition != null)
                {
                    List<T8DataParameterDefinition> additionalFields;
                    List<T8DataEntityFieldDefinition> entityFields;
                    HashMap<String, List<String>> identifierMap;

                    additionalFields = getAdditionalDataList();
                    entityFields = dataEntityDefinition.getFieldDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if (additionalFields != null && entityFields != null)
                    {
                        for (T8DataParameterDefinition additionalFieldDefinition : additionalFields)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataEntityFieldDefinition entityFieldDefinition : entityFields)
                            {
                                identifierList.add(entityFieldDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(additionalFieldDefinition.getIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
                else return new ArrayList<T8DefinitionDatumOption>();
            }
            else return new ArrayList<T8DefinitionDatumOption>(); // Returning null would indicate that any value is accepted for this datum, while we only want to show that no valid values are yet available.
        }
        else
        {
            return super.getDatumOptions(definitionContext, datumIdentifier);
        }
    }

    @Override
    public T8DefinitionDatumEditor getDatumEditor(T8DefinitionContext definitionContext, String datumIdentifier)
    {
        if ((Datum.ADDITIONAL_FIELDS.toString().equals(datumIdentifier)) || (Datum.INPUT_PARAMETERS.toString().equals(datumIdentifier)))
        {
            try
            {
                T8DefinitionDatumEditor datumEditor;
                Constructor constructor;

                constructor = Class.forName("com.pilog.t8.developer.definitions.defaulteditor.datumeditor.DefinitionTableDatumEditor").getConstructor(T8DefinitionContext.class, T8Definition.class, T8DefinitionDatumType.class);
                datumEditor = (T8DefinitionDatumEditor)constructor.newInstance(definitionContext, this, getDatumType(datumIdentifier));
                return datumEditor;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else return null;
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings)
            throws Exception
    {
    }

    @Override
    public T8CommunicationDetailsProvider getCommunicationDetailProviderInstance(T8Context context, Map<String, Object> providerParameters)
    {
        try
        {
            Constructor constructor;

            constructor = Class.forName("com.pilog.t8.communication.detail.provider.entity.T8EntityDetailProvider").getConstructor(T8Context.class, T8EntityDetailProviderDefinition.class, Map.class);
            return (T8CommunicationDetailsProvider) constructor.newInstance(context, this, providerParameters);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<T8CommunicationType> getSupportedCommunicationTypes()
    {
        List<T8CommunicationType> supportedCommunicationTypes;

        supportedCommunicationTypes = new ArrayList<T8CommunicationType>();

        if(!Strings.isNullOrEmpty(getEmailFieldIdentifier())) supportedCommunicationTypes.add(T8CommunicationType.EMAIL);
        if(!Strings.isNullOrEmpty(getMobileFieldIdentifier())) supportedCommunicationTypes.add(T8CommunicationType.SMS);
        if(!Strings.isNullOrEmpty(getFaxNumberFieldIdentifier()) && !Strings.isNullOrEmpty(getFaxRecipientNameFieldIdentifier())) supportedCommunicationTypes.add(T8CommunicationType.FAX);

        return supportedCommunicationTypes;
    }

    @Override
    public List<T8DataParameterDefinition> getAvailableDetails()
    {
        return getAdditionalDataList();
    }

    public void setDataEntityIdentifier(String dataEntityIdentifier)
    {
        setDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString(), dataEntityIdentifier);
    }

    public String getDataEntityIdentifier()
    {
        return (String) getDefinitionDatum(Datum.DATA_ENTITY_IDENTIFIER.toString());
    }

    public void setHistoryRecordIDFieldIdentifier(String fieldIdentifier)
    {
        setDefinitionDatum(Datum.HISTORY_RECORD_ID_FIELD.toString(), fieldIdentifier);
    }

    public String getHistoryRecordIDFieldIdentifier()
    {
        return (String) getDefinitionDatum(Datum.HISTORY_RECORD_ID_FIELD.toString());
    }

    public void setEmailFieldIdentifier(String emailFieldIdentifier)
    {
        setDefinitionDatum(Datum.EMAIL_ADDRESS_FIELD.toString(), emailFieldIdentifier);
    }

    public String getEmailFieldIdentifier()
    {
        return (String) getDefinitionDatum(Datum.EMAIL_ADDRESS_FIELD.toString());
    }

    public void setMobileFieldIdentifier(String mobileFieldIdentifier)
    {
        setDefinitionDatum(Datum.MOBILE_NUMBER_FIELD.toString(), mobileFieldIdentifier);
    }

    public String getMobileFieldIdentifier()
    {
        return (String) getDefinitionDatum(Datum.MOBILE_NUMBER_FIELD.toString());
    }

    public void setFaxNumberFieldIdentifier(String faxNumber)
    {
        setDefinitionDatum(Datum.FAX_NUMBER_FIELD.toString(), faxNumber);
    }

    public String getFaxNumberFieldIdentifier()
    {
        return (String) getDefinitionDatum(Datum.FAX_NUMBER_FIELD.toString());
    }

    public void setFaxRecipientNameFieldIdentifier(String faxRecipientName)
    {
        setDefinitionDatum(Datum.FAX_RECIPIENT_NAME_FIELD.toString(), faxRecipientName);
    }

    public String getFaxRecipientNameFieldIdentifier()
    {
        return (String) getDefinitionDatum(Datum.FAX_RECIPIENT_NAME_FIELD.toString());
    }

    public void setFaxCompanyNameFieldIdentifier(String faxCompanyName)
    {
        setDefinitionDatum(Datum.FAX_COMPANY_NAME_FIELD.toString(), faxCompanyName);
    }

    public String getFaxCompanyNameFieldIdentifier()
    {
        return (String) getDefinitionDatum(Datum.FAX_COMPANY_NAME_FIELD.toString());
    }

    public void setDataFilterDefinition(T8DataFilterDefinition dataFilterDefinition)
    {
        setDefinitionDatum(Datum.DATA_FILTER_DEFINITION.toString(), dataFilterDefinition);
    }

    public T8DataFilterDefinition getDataFilterDefinition()
    {
        return (T8DataFilterDefinition) getDefinitionDatum(Datum.DATA_FILTER_DEFINITION.toString());
    }

    public void setAdditionalDataList(List<T8DataParameterDefinition> additionalDetailList)
    {
        setDefinitionDatum(Datum.ADDITIONAL_FIELDS.toString(), additionalDetailList);
    }

    public List<T8DataParameterDefinition> getAdditionalDataList()
    {
        return (List<T8DataParameterDefinition>) getDefinitionDatum(Datum.ADDITIONAL_FIELDS.toString());
    }

    public void setInputParameters(List<T8DataParameterDefinition> inputParameters)
    {
        setDefinitionDatum(Datum.INPUT_PARAMETERS.toString(), inputParameters);
    }

    @Override
    public List<T8DataParameterDefinition> getInputParameters()
    {
       return (List<T8DataParameterDefinition>) getDefinitionDatum(Datum.INPUT_PARAMETERS.toString());
    }

    public Map<String, String> getAdditionalFieldsMapping()
    {
        return (Map<String, String>)getDefinitionDatum(Datum.ADDITIONAL_FIELDS_MAPPING.toString());
    }

    public void setAdditionalFieldsMapping(Map<String, String> inputParameterMapping)
    {
        setDefinitionDatum(Datum.ADDITIONAL_FIELDS_MAPPING.toString(), inputParameterMapping);
    }

}
