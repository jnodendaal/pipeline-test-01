package com.pilog.t8.definition.communication.service;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.communication.T8CommunicationService;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.definition.communication.T8CommunicationServiceDefinition;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8ClickATellSMSServiceDefinition extends T8CommunicationServiceDefinition
{
    // -------- Definition Meta-Data -------- //
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_SMS_SERVICE";
    public static final String DISPLAY_NAME = "Click-a-tell SMS Service";
    public static final String DESCRIPTION = "Click-a-tell SMS Service";
    public static final String IDENTIFIER_PREFIX = "SMS_SERV_";
    public enum Datum
    {
        API_ID,
        USERNAME,
        PASSWORD,
        FROM_CELL_NUMBER
    };
    // -------- Definition Meta-Data -------- //

    public T8ClickATellSMSServiceDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = super.getDatumTypes();
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.API_ID.toString(), "Click-A-Tell API ID", "The Click-A-Tell api id to use when sending sms messages."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.USERNAME.toString(), "Click-A-Tell User Name", "The Click-A-Tell username to use when sending sms messages."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.PASSWORD.toString(), "Click-A-Tell Password", "The Click-A-Tell password to use when sending sms messages."));
        datumTypes.add(new T8DefinitionDatumType(T8DataType.STRING, Datum.FROM_CELL_NUMBER.toString(), "From Number(Optional)", "The from cell number that will be displayed on the sms message."));
        datumTypes.addAll(super.getDatumTypes());
        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        return super.getDatumOptions(definitionContext, datumIdentifier);
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
        super.initializeDefinition(context, inputParameters, configurationSettings);
    }

    @Override
    public T8CommunicationType getType()
    {
        return T8CommunicationType.SMS;
    }

    @Override
    public T8CommunicationService getServiceInstance(T8Context context, T8CommunicationManager manager)
    {
        return T8Reflections.getInstance("com.pilog.t8.communication.service.clickatell.T8ClickATellSMSService", new Class<?>[]{T8Context.class, T8ClickATellSMSServiceDefinition.class, T8CommunicationManager.class}, context, this, manager);
    }

    public void setUserName(String UserName)
    {
        setDefinitionDatum(Datum.USERNAME.toString(), UserName);
    }

    public String getUserName()
    {
        return (String) getDefinitionDatum(Datum.USERNAME.toString());
    }

    public void setPassword(String Password)
    {
        setDefinitionDatum(Datum.PASSWORD.toString(), Password);
    }

    public String getPassword()
    {
        return (String) getDefinitionDatum(Datum.PASSWORD.toString());
    }

    public void setApiID(String ApiID)
    {
        setDefinitionDatum(Datum.API_ID.toString(), ApiID);
    }

    public String getApiID()
    {
        return (String) getDefinitionDatum(Datum.API_ID.toString());
    }

    public void setFromNumber(String FromNumber)
    {
        setDefinitionDatum(Datum.FROM_CELL_NUMBER.toString(), FromNumber);
    }

    public String getFromNumber()
    {
        return (String) getDefinitionDatum(Datum.FROM_CELL_NUMBER.toString());
    }
}
