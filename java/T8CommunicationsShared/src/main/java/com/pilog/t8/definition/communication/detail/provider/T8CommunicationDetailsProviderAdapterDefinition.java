package com.pilog.t8.definition.communication.detail.provider;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.communication.T8CommunicationType;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionContext;
import com.pilog.t8.definition.T8DefinitionDatumOption;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.communication.T8CommunicationTypeDefinition;
import com.pilog.t8.definition.communication.T8DefaultCommunicationDefinition;
import com.pilog.t8.definition.communication.message.template.T8CommunicationMessageDefinition;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8CommunicationDetailsProviderAdapterDefinition extends T8Definition
{
    // -------- Definition Meta-Data -------- //
    public static final String GROUP_IDENTIFIER = "@DG_COMMUNICATION_DETAIL_PROVIDER_ADAPTERS";
    public static final String TYPE_IDENTIFIER = "@DT_COMMUNICATION_DETAIL_PROVIDER_ADAPTER";
    public static final String DISPLAY_NAME = "Communication Detail Provider Adapter";
    public static final String DESCRIPTION = "Communication Detail Provider Adapter Used to make mappings easier.";
    public static final String IDENTIFIER_PREFIX = "COMM_DET_PROV_ADAPT_";
    public enum Datum
    {
        COMMUNICATION_DETAIL_PROVIDER_IDENTIFIER,
        COMMUNICATION_DETAIL_PROVIDER_INPUT_MAPPING,
        COMMUNICATION_DETAIL_PROVIDER_DETAILS_MESSAGE_TEMPLATE_MAPPING
    };
    // -------- Definition Meta-Data -------- //

    public T8CommunicationDetailsProviderAdapterDefinition(String identifier)
    {
        super(identifier);
    }

    @Override
    public ArrayList<T8DefinitionDatumType> getDatumTypes()
    {
        ArrayList<T8DefinitionDatumType> datumTypes;

        datumTypes = new ArrayList<T8DefinitionDatumType>(2);
        datumTypes.add(new T8DefinitionDatumType(T8DataType.DEFINITION_IDENTIFIER, Datum.COMMUNICATION_DETAIL_PROVIDER_IDENTIFIER.toString(), "Communication Detail Provider", "The communication detail provider to use."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.COMMUNICATION_DETAIL_PROVIDER_INPUT_MAPPING.toString(), "Parameter Mapping:  Communication Input to Detail Provider Input", "A mapping of Communication input parameters to the corresponding Detail Provider input parameters."));
        datumTypes.add(T8DefinitionDatumType.identifierMap(Datum.COMMUNICATION_DETAIL_PROVIDER_DETAILS_MESSAGE_TEMPLATE_MAPPING.toString(), "Message Details Parameter Mapping:  Detail Provider Input to Message Template Input", "A mapping of Detail Provider input parameters to Message template details parameter."));

        return datumTypes;
    }

    @Override
    public ArrayList<T8DefinitionDatumOption> getDatumOptions(T8DefinitionContext definitionContext, String datumIdentifier) throws Exception
    {
        if (Datum.COMMUNICATION_DETAIL_PROVIDER_IDENTIFIER.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionMetaData> t8DefinitionMetaDatas = new ArrayList<T8DefinitionMetaData>();
            T8CommunicationTypeDefinition parentDefinition = (T8CommunicationTypeDefinition) getParentDefinition();
            for (T8DefinitionMetaData metaData : definitionContext.getGroupDefinitionMetaData(getRootProjectId(), T8CommunicationDetailsProviderDefinition.GROUP_IDENTIFIER))
            {
                T8CommunicationDetailsProviderDefinition communicationDetailProviderDefinition = (T8CommunicationDetailsProviderDefinition) definitionContext.getRawDefinition(getRootProjectId(), metaData.getId());
                for (T8CommunicationType t8CommunicationType : communicationDetailProviderDefinition.getSupportedCommunicationTypes())
                {
                    if (parentDefinition.getType() == t8CommunicationType)
                    {
                        t8DefinitionMetaDatas.add(metaData);
                    }
                }
            }
            return createIdentifierOptions(t8DefinitionMetaDatas);
        }
        else if (Datum.COMMUNICATION_DETAIL_PROVIDER_INPUT_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String identifier;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            identifier = getCommunicationDetailProviderDefinitionIdentifier();
            if (identifier != null)
            {
                T8CommunicationDetailsProviderDefinition providerDefinition;
                T8DefaultCommunicationDefinition communicationDefinition;

                providerDefinition = (T8CommunicationDetailsProviderDefinition) definitionContext.getRawDefinition(getRootProjectId(), identifier);
                communicationDefinition = getParentCommunicationDefinition(this);
                if (providerDefinition != null && communicationDefinition != null)
                {
                    List<T8DataParameterDefinition> providerOperationParameterDefinitions;
                    List<T8DataParameterDefinition> communicationParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    providerOperationParameterDefinitions = providerDefinition.getInputParameters();
                    communicationParameterDefinitions = communicationDefinition.getInputParameters();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((providerOperationParameterDefinitions != null) && (communicationParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition communicationDataParameterDefinition : communicationParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition providerDataParameterDefinition : providerOperationParameterDefinitions)
                            {
                                identifierList.add(providerDataParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(communicationDataParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
            }
            return new ArrayList<T8DefinitionDatumOption>();
        }
        else if (Datum.COMMUNICATION_DETAIL_PROVIDER_DETAILS_MESSAGE_TEMPLATE_MAPPING.toString().equals(datumIdentifier))
        {
            ArrayList<T8DefinitionDatumOption> optionList;
            String identifier;

            optionList = new ArrayList<T8DefinitionDatumOption>();

            identifier = getCommunicationDetailProviderDefinitionIdentifier();
            if (identifier != null)
            {
                T8CommunicationDetailsProviderDefinition providerDefinition;
                T8CommunicationTypeDefinition communicationTypeDefinition;
                T8CommunicationMessageDefinition messageTemplateDefinition;

                providerDefinition = (T8CommunicationDetailsProviderDefinition) definitionContext.getRawDefinition(getRootProjectId(), identifier);
                communicationTypeDefinition = (T8CommunicationTypeDefinition) getParentDefinition();
                messageTemplateDefinition = (T8CommunicationMessageDefinition) definitionContext.getRawDefinition(getRootProjectId(), communicationTypeDefinition.getMessageTemplateIdentifier());
                if (providerDefinition != null && messageTemplateDefinition != null)
                {
                    List<T8DataParameterDefinition> providerOperationParameterDefinitions;
                    List<T8DataParameterDefinition> temaplateParameterDefinitions;
                    HashMap<String, List<String>> identifierMap;

                    providerOperationParameterDefinitions = providerDefinition.getAvailableDetails();
                    temaplateParameterDefinitions = messageTemplateDefinition.getCommunicationDetailParameterDefinitions();

                    identifierMap = new HashMap<String, List<String>>();
                    if ((providerOperationParameterDefinitions != null) && (temaplateParameterDefinitions != null))
                    {
                        for (T8DataParameterDefinition communicationDataParameterDefinition : providerOperationParameterDefinitions)
                        {
                            ArrayList<String> identifierList;

                            identifierList = new ArrayList<String>();
                            for (T8DataParameterDefinition providerDataParameterDefinition : temaplateParameterDefinitions)
                            {
                                identifierList.add(providerDataParameterDefinition.getPublicIdentifier());
                            }

                            identifierMap.put(communicationDataParameterDefinition.getPublicIdentifier(), identifierList);
                        }
                    }

                    optionList.add(new T8DefinitionDatumOption("Possible Identifier Mappings", identifierMap));
                    return optionList;
                }
            }
            return new ArrayList<T8DefinitionDatumOption>();
        }
        else
        {
            return null;
        }
    }

    @Override
    public void initializeDefinition(T8Context context, Map<String, Object> inputParameters, Map<String, Object> configurationSettings) throws Exception
    {
    }

    private T8DefaultCommunicationDefinition getParentCommunicationDefinition(T8Definition parent)
    {
        if (parent == null)
        {
            return null;
        }
        else
        {
            T8Definition myParentDefinition;

            myParentDefinition = parent.getParentDefinition();
            if (myParentDefinition instanceof T8DefaultCommunicationDefinition)
            {
                return (T8DefaultCommunicationDefinition) myParentDefinition;
            }
            else
            {
                return getParentCommunicationDefinition(myParentDefinition);
            }
        }
    }

    public void setCommunicationDetailProviderDefinitionIdentifier(String identifier)
    {
        setDefinitionDatum(Datum.COMMUNICATION_DETAIL_PROVIDER_IDENTIFIER.toString(), identifier);
    }

    public String getCommunicationDetailProviderDefinitionIdentifier()
    {
        return (String) getDefinitionDatum(Datum.COMMUNICATION_DETAIL_PROVIDER_IDENTIFIER.toString());
    }

    public void setCommunicationDetailProviderInputMapping(Map<String, String> identifier)
    {
        setDefinitionDatum(Datum.COMMUNICATION_DETAIL_PROVIDER_INPUT_MAPPING.toString(), identifier);
    }

    public Map<String, String> getCommunicationDetailProviderInputMapping()
    {
        return (Map<String, String>) getDefinitionDatum(Datum.COMMUNICATION_DETAIL_PROVIDER_INPUT_MAPPING.toString());
    }

    public void setMessageDetailsInputMapping(Map<String, String> identifier)
    {
        setDefinitionDatum(Datum.COMMUNICATION_DETAIL_PROVIDER_DETAILS_MESSAGE_TEMPLATE_MAPPING.toString(), identifier);
    }

    public Map<String, String> getMessageDetailsInputMapping()
    {
        return (Map<String, String>) getDefinitionDatum(Datum.COMMUNICATION_DETAIL_PROVIDER_DETAILS_MESSAGE_TEMPLATE_MAPPING.toString());
    }
}
