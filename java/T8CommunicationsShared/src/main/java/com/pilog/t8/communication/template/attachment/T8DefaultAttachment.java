package com.pilog.t8.communication.template.attachment;

import com.pilog.t8.communication.T8CommunicationMessageAttachment;
import com.pilog.t8.security.T8Context;
import java.util.Map;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8DefaultAttachment implements T8CommunicationMessageAttachment
{
    protected String attachmentName;
    protected String attachmentDescription;
    protected byte[] attachmentData;
    protected String identifier;

    public T8DefaultAttachment(String identifier, String attachmentName, String attachmentDescription)
    {
        this.attachmentName = attachmentName;
        this.attachmentDescription = attachmentDescription;
    }

    public T8DefaultAttachment(String identifier, String attachmentName, String attachmentDescription, byte[] attachmentData)
    {
        this(identifier, attachmentName, attachmentDescription);
        this.attachmentData = attachmentData;
    }

    @Override
    public void generateAttachment(T8Context context, Map<String, Object> templateInputParameters) throws Exception
    {
        // Only sub classes will implement this method if they can generate attachments from templates.
    }

    @Override
    public DataSource getAttachment() throws Exception
    {
        return new ByteArrayDataSource(attachmentData, getContentType());
    }

    @Override
    public String getName()
    {
        return attachmentName;
    }

    @Override
    public String getDescription()
    {
        return attachmentDescription;
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public byte[] getBinaryData()
    {
        return attachmentData;
    }

    @Override
    public boolean isEmbedded()
    {
        return false;
    }

    @Override
    public String getContentID()
    {
        return null;
    }

//    public void loadAttachment(T8CommunicationMessageAttachment attachment) throws Exception, IOException
//    {
//        ByteArrayOutputStream dataOut;
//        InputStream dataIn;
//        int byteData;
//
//        setAttachmentName(attachment.getName());
//        setAttachmentDescription(attachment.getDescription());
//        dataOut = new ByteArrayOutputStream();
//        dataIn = attachment.getAttachment().getInputStream();
//        while((byteData = dataIn.read()) != -1)
//        {
//            dataOut.write(byteData);
//        }
//
//        setAttachmentData(dataOut.toByteArray());
//
//        dataOut.close();
//        dataIn.close();
//    }

    public String getContentType()
    {
        return "application/octet-stream";
    }
}
