package com.pilog.version;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8CommunicationsSharedVersion
{
    public final static String VERSION = "70";
}
