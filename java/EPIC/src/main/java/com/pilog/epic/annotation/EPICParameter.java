package com.pilog.epic.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hennie.brink@pilog.co.za
 *
 * This Annotation can be used to optionally specify an alternate variable name that will used for auto completions
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface EPICParameter
{
    /*
    Alternate variable name that will be used in the epic script
    */
    String VariableName();

    String Description() default "";
}
