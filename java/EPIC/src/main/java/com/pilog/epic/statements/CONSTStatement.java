package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;

/**
 * @author Bouwer du Preez
 */
public class CONSTStatement extends Statement
{
    private Object constant;

    private CONSTStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public void setConstant(Object constant)
    {
        this.constant = constant;
    }

    public static CONSTStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        CONSTStatement newStatement;
        Token nextToken;

        newStatement = new CONSTStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.getType() == Token.STRING)
        {
            newStatement.setConstant(nextToken.getValue());
            return newStatement;
        }
        else throw new EPICSyntaxException("Invalid constant: " + nextToken.getValue(), tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        returnObject = constant;
        if (chainedStatement != null)
        {
            return chainedStatement.execute(returnObject);
        }
        else return Statement.RETURN_CODE_COMPLETED;
    }
}
