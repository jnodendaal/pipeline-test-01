package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class COLLECTIONStatement extends Statement
{
    private String collectionName;
    private List<Expression> elementExpressions;
    private Expression valueExpression;
    private int type;

    public static final int TYPE_ASSIGNMENT = 0;
    public static final int TYPE_ACCESSOR = 1;

    private COLLECTIONStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
        this.elementExpressions = new ArrayList<>();
    }

    public void setCollectionName(String collectionName)
    {
        this.collectionName = collectionName;
    }

    public void setValueExpression(Expression valueExpression)
    {
        this.valueExpression = valueExpression;
    }

    public void addElementExpression(Expression elementExpression)
    {
        this.elementExpressions.add(elementExpression);
    }

    public static COLLECTIONStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        COLLECTIONStatement newStatement;
        Token nextToken;

        newStatement = new COLLECTIONStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.getType() == Token.COLLECTION)
        {
            String collectionName;

            collectionName = (String)nextToken.getValue();
            newStatement.setCollectionName(collectionName);
            nextToken = tokenizer.nextToken();
            while (nextToken.isSymbol('['))
            {
                Expression elementExpression;

                elementExpression = ExpressionParser.parseExpression(tokenizer, parentExecutionUnit);
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(']'))
                {
                    newStatement.addElementExpression(elementExpression);
                    nextToken = tokenizer.nextToken();
                }
                else throw new EPICSyntaxException("Missing ']' in collection assignement.", tokenizer.getLineNumber());
            }

            if (nextToken.isSymbol('='))
            {
                Expression valueExpression;

                valueExpression = ExpressionParser.parseExpression(tokenizer, parentExecutionUnit);
                newStatement.setValueExpression(valueExpression);
            }
            else
            {
                tokenizer.returnToken(nextToken);
            }

            return newStatement;
        }
        else throw new EPICSyntaxException("Invalid start of collection assignment.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Object collection;

        collection = parentExecutionUnit.getVariable(collectionName);
        if (collection != null)
        {
            Iterator<Expression> elementExpressionIterator;

            elementExpressionIterator = elementExpressions.iterator();
            while (elementExpressionIterator.hasNext())
            {
                Expression elementExpression;
                Object elementKey;

                // Get the next element parseExpression.  Only evaluate the
                elementExpression = elementExpressionIterator.next();

                // Evalute the next element parseExpression to get the element key.
                elementKey = elementExpression.evaluate(parentExecutionUnit, contextObject);

                // If this is the last element parseExpression and we have a value assignment, then do it now, else just access the next element.
                if ((!elementExpressionIterator.hasNext()) && (valueExpression != null))
                {
                    if (collection instanceof List)
                    {
                        List list;
                        int index;

                        list = (List)collection;
                        index = (Integer)elementKey;
                        if (index < list.size())
                        {
                            list.set(index, valueExpression.evaluate(parentExecutionUnit, contextObject));
                        }
                        else list.add(index, valueExpression.evaluate(parentExecutionUnit, contextObject));
                    }
                    else if (collection instanceof Map)
                    {
                        Map map;

                        map = (Map)collection;
                        map.put(elementKey, valueExpression.evaluate(parentExecutionUnit, contextObject));
                    }
                    else throw new EPICRuntimeException("Invalid collection type: " + collection.getClass(), lineNumber);
                }
                else
                {
                    if (collection instanceof List)
                    {
                        collection = ((List)collection).get((Integer)elementKey);
                    }
                    else if (collection instanceof Map)
                    {
                        collection = ((Map)collection).get(elementKey);
                    }
                    else throw new EPICRuntimeException("Invalid collection type: " + collection.getClass(), lineNumber);
                }
            }

            returnObject = collection;
            if (chainedStatement != null)
            {
                return chainedStatement.execute(returnObject);
            }
            else return Statement.RETURN_CODE_COMPLETED;
        }
        else throw new EPICRuntimeException("Collection '" + collectionName + "' not declared.", lineNumber);
    }
}
