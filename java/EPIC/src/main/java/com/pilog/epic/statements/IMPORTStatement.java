package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Program;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;

/**
 * @author Bouwer du Preez
 */
public class IMPORTStatement extends Statement
{
    private IDENTIFIERStatement identifierStatement;
    private boolean staticImport;

    private IMPORTStatement(Program parentProgram, int lineNumber)
    {
        super(parentProgram, lineNumber);
    }

    public IDENTIFIERStatement getIdentifierStatement()
    {
        return identifierStatement;
    }

    public void setIdentifierStatement(IDENTIFIERStatement identifierStatement)
    {
        this.identifierStatement = identifierStatement;
    }

    public boolean isStaticImport()
    {
        return staticImport;
    }

    public void setStaticImport(boolean staticImport)
    {
        this.staticImport = staticImport;
    }

    public static IMPORTStatement parseStatement(LexicalTokenizer tokenizer, Program parentProgram) throws EPICSyntaxException
    {
        IMPORTStatement newStatement;
        Token nextToken;

        newStatement = new IMPORTStatement(parentProgram, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.IMPORT))
        {
            IDENTIFIERStatement identifier;

            // Check for static import.
            nextToken = tokenizer.nextToken();
            if ((nextToken.isVariable()) && (((String)nextToken.getValue()).equals("static")))
            {
                newStatement.setStaticImport(true);
            }
            else tokenizer.returnToken(nextToken);

            // Parse the identifier to import.
            identifier = IDENTIFIERStatement.parseStatement(tokenizer, parentProgram, false, false);
            newStatement.setIdentifierStatement(identifier);
            parentProgram.addVariableDeclaration(identifier.getClassIdentifier().getSimpleName()); // Add the declaration so that subsequent variables can be resolved during parsing.
            return newStatement;
        }
        else throw new EPICSyntaxException("Invalid start of break statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Class importedClass;

        identifierStatement.execute(contextObject);
        importedClass = (Class)identifierStatement.getReturnObject();

        if (staticImport)
        {
            // To Do:  Add support for static imports.
            return ExecutionUnit.RETURN_CODE_COMPLETED;
        }
        else
        {
            ((Program)parentExecutionUnit).declareClassImport(importedClass.getSimpleName(), importedClass);
            return ExecutionUnit.RETURN_CODE_COMPLETED;
        }
    }
}
