package com.pilog.epic;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class ScopeBlock extends ExecutionUnit
{
    private final ArrayList<ExecutionUnit> executionUnits;

    public ScopeBlock(ExecutionUnit parentExecutionUnit)
    {
        super(parentExecutionUnit);
        this.executionUnits = new ArrayList<ExecutionUnit>();
    }

    public void addExecutionUnit(ExecutionUnit unit)
    {
        executionUnits.add(unit);
    }

    @Override
    public List<ExecutionUnit> getDescendantExecutionUnits()
    {
        List<ExecutionUnit> descendants;

        descendants = new ArrayList<ExecutionUnit>();
        for (ExecutionUnit executionUnit : executionUnits)
        {
            descendants.add(executionUnit);
            descendants.addAll(executionUnit.getDescendantExecutionUnits());
        }

        return descendants;
    }

    public static ScopeBlock parseScopeBlock(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        ScopeBlock newScopeBlock;
        Token nextToken;

        newScopeBlock = new ScopeBlock(parentExecutionUnit);
        nextToken = tokenizer.nextToken();
        if (nextToken.isSymbol('{'))
        {
            nextToken = tokenizer.nextToken();
            while (!nextToken.isSymbol('}'))
            {
                ExecutionUnit executionUnit;

                tokenizer.returnToken(nextToken);
                executionUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newScopeBlock);
                if (executionUnit != null) newScopeBlock.addExecutionUnit(executionUnit);
                nextToken = tokenizer.nextToken();
            }

            return newScopeBlock;
        }
        else throw new EPICSyntaxException("Invalid start of ScopeBlock: " + nextToken, tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        // Clear variables so that previous execution data is discarded.
        variables.clear();

        // Execute all execution units in the scope block.
        for (ExecutionUnit executionUnit : executionUnits)
        {
            int returnCode;

            returnCode = executionUnit.execute(contextObject);
            if (returnCode != ExecutionUnit.RETURN_CODE_COMPLETED) return returnCode;
        }

        // Return the default completion return code.
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
