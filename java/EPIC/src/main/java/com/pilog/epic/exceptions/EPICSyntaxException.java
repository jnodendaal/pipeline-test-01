package com.pilog.epic.exceptions;

/**
 * Thrown by the parser or evaluator when invalid syntax is encountered.
 *
 * @author Bouwer du Preez
 */
public class EPICSyntaxException extends Exception
{
    private final int lineNumber;

    /**
     * Constructs a Syntax error with the message <i>errorMessage</i>.
     *
     * @param errorMessage The error message with which to construct this error.
     */
    public EPICSyntaxException(String errorMessage, int lineNumber)
    {
        super("Line[" + lineNumber + "]: " + errorMessage);
        this.lineNumber = lineNumber;
    }

    public int getLineNumber()
    {
        return lineNumber;
    }
}
