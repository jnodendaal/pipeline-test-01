package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class DOUBLEStatement extends Statement
{
    private Expression valueExpression;

    private DOUBLEStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public void setValueExpression(Expression valueExpression)
    {
        this.valueExpression = valueExpression;
    }

    public Expression getValueExpression()
    {
        return valueExpression;
    }

    public static DOUBLEStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        DOUBLEStatement newStatement;
        Token nextToken;

        newStatement = new DOUBLEStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.DOUBLE))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                Expression valueExpression;

                valueExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                newStatement.setValueExpression(valueExpression);

                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(')'))
                {
                    return newStatement;
                }
                else throw new EPICSyntaxException("')' character missing in double cast statement.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("'(' character missing in double cast statement.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid constant: " + nextToken.getValue(), tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        returnObject = castToFloat(valueExpression.evaluate(parentExecutionUnit, contextObject));
        if (chainedStatement != null)
        {
            return chainedStatement.execute(returnObject);
        }
        else return Statement.RETURN_CODE_COMPLETED;
    }

    private double castToFloat(Object value) throws EPICRuntimeException
    {
        if (value == null)
        {
            throw new EPICRuntimeException("Cannot cast null value to double.", null, lineNumber);
        }
        else if (value instanceof String)
        {
            try
            {
                return Double.parseDouble((String)value);
            }
            catch (Exception e)
            {
                throw new EPICRuntimeException("Cannot cast String value '" + value + "' to double.", e, lineNumber);
            }
        }
        else if (value instanceof Number)
        {
            return ((Number)value).doubleValue();
        }
        else throw new EPICRuntimeException("Cannot cast type '" + value.getClass() + "' to float.", null, lineNumber);
    }
}
