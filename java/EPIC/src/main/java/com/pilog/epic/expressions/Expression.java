package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.exceptions.EPICRuntimeException;

/**
 * @author Bouwer du Preez
 */
public interface Expression
{
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException;
    public String unparse();
}
