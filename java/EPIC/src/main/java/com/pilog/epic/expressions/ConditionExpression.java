package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.exceptions.EPICRuntimeException;

/**
 * @author Bouwer du Preez
 */
public class ConditionExpression extends DefaultExpression
{
    private final Expression conditionExpression;
    private final Expression trueValueExpression;
    private final Expression falseValueExpression;

    public ConditionExpression(Expression conditionExpression, Expression trueValueExpression, Expression falseValueExpression)
    {
        super();
        this.conditionExpression = conditionExpression;
        this.trueValueExpression = trueValueExpression;
        this.falseValueExpression = falseValueExpression;
    }

    @Override
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        Object result;

        result = conditionExpression.evaluate(parentExecutionUnit, contextObject);
        if (result instanceof Boolean)
        {
            return (Boolean)result ? trueValueExpression.evaluate(parentExecutionUnit, contextObject) : falseValueExpression.evaluate(parentExecutionUnit, contextObject);
        }
        else if (result instanceof String)
        {
            return Boolean.parseBoolean((String)result) ? trueValueExpression.evaluate(parentExecutionUnit, contextObject) : falseValueExpression.evaluate(parentExecutionUnit, contextObject);
        }
        else throw new EPICRuntimeException("Non-boolean result used in conditional expression.", lineNumber);
    }
}
