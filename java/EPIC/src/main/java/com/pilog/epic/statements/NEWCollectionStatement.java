package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class NEWCollectionStatement extends Statement
{
    private final ArrayList<CollectionElement> elements;
    private int collectionType;

    public static final int COLLECTION_TYPE_UNDEFINED = -1;
    public static final int COLLECTION_TYPE_LIST = 0;
    public static final int COLLECTION_TYPE_MAP = 1;

    public NEWCollectionStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
        elements = new ArrayList<CollectionElement>();
        collectionType = NEWCollectionStatement.COLLECTION_TYPE_LIST;
    }

    /**
     * Adds an element to the list of collection element used during evaluation
 of this parseExpression.
     *
     * @param element The element to add.
     */
    private void addCollectionElement(CollectionElement element)
    {
        elements.add(element);
    }

    private ArrayList<CollectionElement> getElements()
    {
        return this.elements;
    }

    /**
     * Sets the type of this collection.
     *
     * @param type The type of the collection.
     */
    public void setCollectionType(int type)
    {
        this.collectionType = type;
    }

    private int getCollectionType()
    {
        return this.collectionType;
    }

    public static NEWCollectionStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Token nextToken;

        // Check whether the statement starts with an opening bracket.
        // Make sure we start parsing at the correct index.
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.NEW))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('['))
            {
                NEWCollectionStatement newStatement;
                int collectionType;

                // Create a new collection parseExpression.
                newStatement = new NEWCollectionStatement(parentExecutionUnit, tokenizer.getLineNumber());

                // First check for the creation of an empty map.
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(':'))
                {
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol(']'))
                    {
                        newStatement.setCollectionType(NEWCollectionStatement.COLLECTION_TYPE_MAP);
                        return newStatement;
                    }
                    else throw new EPICSyntaxException("Invalid empty map declaration.", tokenizer.getLineNumber());
                }
                else // No empty map declaration, so return the token.
                {
                    tokenizer.returnToken(nextToken);
                }

                // Continue to parse the normal collection.
                collectionType = COLLECTION_TYPE_UNDEFINED;
                while (!(nextToken = tokenizer.nextToken()).isSymbol(']'))
                {
                    Expression elementPart1 = null;
                    Expression elementPart2 = null; // Only used if the collection being parsed is a map.

                    // Return the next token so it can be parsed as an parseExpression.
                    tokenizer.returnToken(nextToken);

                    // Parse the first part of the element (this will be the only part, if the collection is a list).
                    elementPart1 = ExpressionParser.parseExpression(tokenizer, parentExecutionUnit);

                    // Check the next token to see if it is a comma.
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol(':')) // This is a Map definition, so continue parsing the second part of the element.
                    {
                        elementPart2 = ExpressionParser.parseExpression(tokenizer, parentExecutionUnit);
                        nextToken = tokenizer.nextToken();
                    }

                    // Consume the separating comma if it is the next token.
                    if (!nextToken.isSymbol(',')) tokenizer.returnToken(nextToken);

                    // Add the newly parsed element to the collection.
                    if (elementPart2 == null)
                    {
                        if (collectionType == COLLECTION_TYPE_UNDEFINED)
                        {
                            collectionType = COLLECTION_TYPE_LIST;
                            newStatement.setCollectionType(COLLECTION_TYPE_LIST);
                            newStatement.addCollectionElement(new CollectionElement(elementPart1, elementPart2));
                        }
                        else if (collectionType == COLLECTION_TYPE_LIST)
                        {
                            newStatement.addCollectionElement(new CollectionElement(elementPart1, elementPart2));
                        }
                        else throw new EPICSyntaxException("Unexpected LIST element found in MAP collection expression: " + elementPart1, tokenizer.getLineNumber());
                    }
                    else
                    {
                        if (collectionType == COLLECTION_TYPE_UNDEFINED)
                        {
                            collectionType = COLLECTION_TYPE_MAP;
                            newStatement.setCollectionType(COLLECTION_TYPE_MAP);
                            newStatement.addCollectionElement(new CollectionElement(elementPart1, elementPart2));
                        }
                        else if (collectionType == COLLECTION_TYPE_MAP)
                        {
                            newStatement.addCollectionElement(new CollectionElement(elementPart1, elementPart2));
                        }
                        else throw new EPICSyntaxException("Unexpected MAP entry found in LIST collection expression: " + elementPart1, tokenizer.getLineNumber());
                    }
                }

                return newStatement;
            }
            else throw new EPICSyntaxException("Invalid start of collection declaration.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of collection declaration.", tokenizer.getLineNumber());
    }

    private static void loopBackMapValidation(NEWCollectionStatement newStatement, LexicalTokenizer tokenizer) throws EPICSyntaxException
    {
        // This validation is only applicable once we have a map collection type
        if (newStatement.collectionType == NEWCollectionStatement.COLLECTION_TYPE_MAP)
        {
            for (CollectionElement element : newStatement.elements)
            {
                if (element.getPart2() == null)
                {
                    throw new EPICSyntaxException("Unexpected token found for new MAP Collection", tokenizer.getLineNumber());
                }
            }
        }
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        if (collectionType == NEWCollectionStatement.COLLECTION_TYPE_LIST)
        {
            ArrayList newList;

            newList = new ArrayList();
            for (CollectionElement element : elements)
            {
                newList.add(element.getPart1().evaluate(parentExecutionUnit, contextObject));
            }

            returnObject = newList;
            if (chainedStatement != null) return chainedStatement.execute(returnObject);
            else return Statement.RETURN_CODE_COMPLETED;
        }
        else
        {
            LinkedHashMap newMap;

            newMap = new LinkedHashMap();
            for (CollectionElement element : elements)
            {
                newMap.put(element.getPart1().evaluate(parentExecutionUnit, contextObject), element.getPart2().evaluate(parentExecutionUnit, contextObject));
            }

            returnObject = newMap;
            if (chainedStatement != null) return chainedStatement.execute(returnObject);
            else return Statement.RETURN_CODE_COMPLETED;
        }
    }

    private static class CollectionElement
    {
        private final Expression part1;
        private final Expression part2;

        private CollectionElement(Expression part1, Expression part2)
        {
            this.part1 = part1;
            this.part2 = part2;
        }

        private Expression getPart1()
        {
            return part1;
        }

        private Expression getPart2()
        {
            return part2;
        }
    }
}
