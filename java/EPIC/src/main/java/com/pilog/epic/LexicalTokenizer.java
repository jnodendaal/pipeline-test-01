package com.pilog.epic;

import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.DefaultExpression;
import com.pilog.epic.expressions.FunctionExpression;
import com.pilog.epic.statements.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This class parses the KEYWORDS and symbols from the supplied String. Each
 * tokenizer maintains a state that includes the current position of processing
 * within the input String.
 *
 * @author Bouwer du Preez
 */
public class LexicalTokenizer
{
    private boolean checkVariableDeclaration;
    private int characterPosition;
    private final char characterArray[];
    private final int bufferSize; // The actual number of valid bytes in the buffer.
    private final long totalCharactersRead;
    private Token lastParsedToken;  // Holds the last token parsed from the input stream.
    private final LinkedList<Token> returnedTokens;
    private final List<ExternalExpressionParser> externalExpressionParsers;
    private int lineNumber;

    // Look-ahead characters.
    private char char0;
    private char char1;
    private char char2;
    private char char3;

    private static final Token EOL_TOKEN = new Token(Token.EOL, 0);

    /**
     * Constructor for the class LexicalTokenizer.
     * @param inputString
     */
    public LexicalTokenizer(String inputString)
    {
        this.checkVariableDeclaration = true;
        this.characterArray = inputString.toCharArray();
        this.bufferSize = characterArray.length;
        this.returnedTokens = new LinkedList<Token>();
        this.externalExpressionParsers = new ArrayList<>();
        this.totalCharactersRead = 0;
        this.lineNumber = 0;
        setPosition(0);
    }

    public boolean isCheckVariableDeclaration()
    {
        return checkVariableDeclaration;
    }

    public void setCheckVariableDeclaration(boolean checkVariableDeclaration)
    {
        this.checkVariableDeclaration = checkVariableDeclaration;
    }

    public void addExternalExpressionParser(ExternalExpressionParser parser)
    {
        externalExpressionParsers.add(parser);
    }

    public void addExternalExpressionParsers(List<ExternalExpressionParser> parsers)
    {
        externalExpressionParsers.addAll(parsers);
    }

    public ExternalExpressionParser getExternalExpressionEvaluator(String expression)
    {
        for (ExternalExpressionParser parser : externalExpressionParsers)
        {
            if (parser.isApplicableTo(expression))
            {
                return parser;
            }
        }

        return null;
    }

    public int getLineNumber()
    {
        return lineNumber;
    }

    public long getTotalCharactersRead()
    {
        return totalCharactersRead;
    }

    /**
     * Returns true if un-tokenized data remains.
     *
     * @return True if un-tokenized data remains, false otherwise.
     */
    public boolean hasMoreTokens()
    {
        return (characterPosition < bufferSize);
    }

    /**
     * Returns the supplied token to the tokenizer so that a subsequent call to
     * the nextToken() will yield the returned token.
     * @param token The token to return.
     */
    public void returnToken(Token token)
    {
        returnedTokens.push(token);
    }

    private void incrementPosition()
    {
        characterPosition++;
        char0 = (characterPosition < bufferSize) ? characterArray[characterPosition] : ' ';
        char1 = (characterPosition < bufferSize - 1) ? characterArray[characterPosition + 1] : ' ';
        char2 = (characterPosition < bufferSize - 2) ? characterArray[characterPosition + 2] : ' ';
        char3 = (characterPosition < bufferSize - 3) ? characterArray[characterPosition + 3] : ' ';
    }

    private void adjustPosition(int adjustment)
    {
        characterPosition += adjustment;
        char0 = (characterPosition < bufferSize) ? characterArray[characterPosition] : ' ';
        char1 = (characterPosition < bufferSize - 1) ? characterArray[characterPosition + 1] : ' ';
        char2 = (characterPosition < bufferSize - 2) ? characterArray[characterPosition + 2] : ' ';
        char3 = (characterPosition < bufferSize - 3) ? characterArray[characterPosition + 3] : ' ';
    }

    private void setPosition(int newPosition)
    {
        characterPosition = newPosition;
        char0 = (characterPosition < bufferSize) ? characterArray[characterPosition] : ' ';
        char1 = (characterPosition < bufferSize - 1) ? characterArray[characterPosition + 1] : ' ';
        char2 = (characterPosition < bufferSize - 2) ? characterArray[characterPosition + 2] : ' ';
        char3 = (characterPosition < bufferSize - 3) ? characterArray[characterPosition + 3] : ' ';
    }

    /**
     * This method will attempt to parse out a numeric constant.
     * A numeric constant satisfies the form:
     *      999.888e777
     * where '999' is the optional integral part, '888' is the optional
     * fractional part and '777' is the optional exponential part.
     * The '.' and 'E' are required if the fractional or exponential
     * parts are present.  There can be no internal spaces in the number.
     * Note that unary minuses are always stripped as a symbol.
     *
     * Also note that until the second character is read .5 and .and.
     * appear to start similarly.
     *
     * @return The newly parsed Token (if any was successfully parsed).
     */
    private Token parseNumericConstant()
    {
        long integralPart;
        double fraction;
        int oldPos;
        boolean isConstant;
        boolean fractionPresent;
        boolean longValue;

        // Set the default values.
        integralPart = 0;
        fraction = 0;
        oldPos = characterPosition; // Save the current position in the buffer place.
        isConstant = false;
        fractionPresent = false;
        longValue = false;

        // Look for the integral part.
        while (isDigit(char0))
        {
            isConstant = true;
            integralPart = (integralPart * 10) + (char0 - '0');
            incrementPosition();
        }

        // Now look for the fractional part.
        if (char0 == '.')
        {
            double multiplier = .1;

            incrementPosition();
            fractionPresent = true;
            while (isDigit(char0))
            {
                isConstant = true;
                fraction = fraction + (multiplier * (char0 - '0'));
                incrementPosition();
                multiplier = multiplier / 10.0;
            }
        }

        // If now mantissa or fractional digits were found, we can abort the parse because no numeric constant could be found.
        if (!isConstant)
        {
            setPosition(oldPos);
            return null;
        }

        // Check for the long indicator.
        if ((char0 == 'L') || (char0 == 'l'))
        {
            longValue = true;
            incrementPosition();
        }

        // If no exponent is present, parsing of the numeric constant is finished.
        if ((char0 != 'E') && (char0 != 'e'))
        {
            if (fractionPresent)
            {
                return new Token(Token.CONSTANT, integralPart + fraction);
            }
            else if (longValue)
            {
                return new Token(Token.CONSTANT, (long)integralPart);
            }
            else
            {
                return new Token(Token.CONSTANT, (int)integralPart);
            }
        }
        else // Parse the exponent.
        {
            boolean wasNegative;
            int power = 0;
            double exponent;

            wasNegative = false;
            incrementPosition(); // Skip over the 'e'/'E'.

            // Check for a negative exponent.
            if (char0 == '-')
            {
                wasNegative = true;
                incrementPosition();
            }
            else
            {
                if (char0 == '+')
                {
                    incrementPosition();
                }
            }

            while (isDigit(char0))
            {
                power = (power * 10) + (char0 - '0');
                incrementPosition();
            }

            try
            {
                exponent = Math.pow(10, (double)power);
            }
            catch (ArithmeticException e)
            {
                return new Token(Token.ERROR, "Illegal numeric constant.");
            }

            if (wasNegative)
            {
                exponent = 1 / exponent;
            }

            if (fraction > 0)
            {
                return new Token(Token.CONSTANT, (integralPart + fraction) * exponent);
            }
            else if (longValue)
            {
                return new Token(Token.CONSTANT, (long)integralPart);
            }
            else
            {
                return new Token(Token.CONSTANT, (int)(integralPart * exponent));
            }
        }
    }

    /**
     * Return true if supplied character is between a-z or A=Z.
     *
     * @param character The character to test.
     * @return True if supplied character is between a-z or A=Z.
     */
    public static boolean isLetter(char character)
    {
        return (((character >= 'a') && (character <= 'z')) || ((character >= 'A') && (character <= 'Z')));
    }

    /**
     * Return true if the supplied character is between 0 and 9.
     *
     * @param character The character to test.
     * @return True if supplied character is between 0 and 9.
     */
    static boolean isDigit(char character)
    {
        return ((character >= '0') && (character <= '9'));
    }

    /**
     * Return true if supplied character is whitespace.
     *
     * @param character The character to test.
     * @return True if supplied character is whitespace.
     */
    static boolean isSpace(char character)
    {
        return ((character == ' ') || (character == '\t') || (character == '\r') || (character == '\n'));
    }

    /**
     * Increments the current position to the first index where a non-whitespace character is found.
     * This method also increments the line number counter every time a newline character is encountered.
     */
    private void skipWhiteSpace()
    {
        // Clear all the white space.
        while ((!isEob()) && isSpace(char0))
        {
            if (char0 == '\n') lineNumber++;
            incrementPosition();
        }
    }

    /**
     * Returns the next token parsed from the data buffer.
     *
     * @return The next token parsed from the data buffer.
     * @throws com.pilog.epic.exceptions.EPICSyntaxException
     */
    public Token nextToken() throws EPICSyntaxException
    {
        // If any tokens have been returned to the tokenizer, use them first.
        if (returnedTokens.size() > 0)
        {
            return returnedTokens.pop();
        }
        else // Parse the next token.
        {
            // Store next parsed token and then return it.
            lastParsedToken = parseNextToken();
            return lastParsedToken;
        }
    }

    /**
     * Returns true if the current position index is out of buffer range (end of buffer).
     * @return true if the current position index is out of buffer range (end of buffer).
     */
    private boolean isEob()
    {
        return characterPosition >= bufferSize;
    }

    /**
     * Returns the next token parsed from the data buffer.
     *
     * @return The next token parsed from the data buffer.
     */
    private Token parseNextToken() throws EPICSyntaxException
    {
        Token newToken;

        // Clear all the white space.
        skipWhiteSpace();

        // Check the buffer.
        if (isEob()) return EOL_TOKEN;

        // Skip any comments.
        while ((char0 == '/') && (char1 == '/'))
        {
            // Skip all characters until a newline character is encountered.
            while (char0 != '\n') incrementPosition();

            // Skip white space after the comment.
            skipWhiteSpace();
        }

        // First check for all special characters.
        switch (char0)
        {
            case '+':
            {
                incrementPosition();
                return new Token(Token.OPERATOR, DefaultExpression.OP_ADD);
            }
            case '-':
            {
                incrementPosition();
                return new Token(Token.OPERATOR, DefaultExpression.OP_SUB);
            }
            case '*':
            {
                if (char1 == '*')
                {
                    adjustPosition(2);
                    return new Token(Token.OPERATOR, DefaultExpression.OP_EXP);
                }
                else
                {
                    incrementPosition();
                    return new Token(Token.OPERATOR, DefaultExpression.OP_MUL);
                }
            }
            case '/':
            {
                incrementPosition();
                return new Token(Token.OPERATOR, DefaultExpression.OP_DIV);
            }
            case '%':
            {
                incrementPosition();
                return new Token(Token.OPERATOR, DefaultExpression.OP_MOD);
            }
            case '^':
            {
                if (char1 == '^')
                {
                    adjustPosition(2);
                    return new Token(Token.OPERATOR, DefaultExpression.OP_BXOR);
                }
                else
                {
                    incrementPosition();
                    return new Token(Token.OPERATOR, DefaultExpression.OP_XOR);
                }
            }
            case '&':
            {
                if (char1 == '&')
                {
                    adjustPosition(2);
                    return new Token(Token.OPERATOR, DefaultExpression.OP_BAND);
                }
                else
                {
                    incrementPosition();
                    return new Token(Token.OPERATOR, DefaultExpression.OP_AND);
                }
            }
            case '|':
            {
                if (char1 == '|')
                {
                    adjustPosition(2);
                    return new Token(Token.OPERATOR, DefaultExpression.OP_BIOR);
                }
                else
                {
                    incrementPosition();
                    return new Token(Token.OPERATOR, DefaultExpression.OP_IOR);
                }
            }
            case '~':
            {
                incrementPosition();
                return new Token(Token.OPERATOR, DefaultExpression.OP_NOT);
            }
            case '!':
            {
                if (char1 == '=')
                {
                    adjustPosition(2);
                    return new Token(Token.OPERATOR, DefaultExpression.OP_NE);
                }
                else
                {
                    incrementPosition();
                    return new Token(Token.OPERATOR, DefaultExpression.OP_BNOT);
                }
            }
            case '=':
            {
                if (char1 == '=')
                {
                    adjustPosition(2);
                    return new Token(Token.OPERATOR, DefaultExpression.OP_EQ);
                }
                else
                {
                    char character;

                    character = char0;
                    incrementPosition();
                    return new Token(Token.SYMBOL, (double)character);
                }
            }
            case '<':
            {
                if (char1 == '=')
                {
                    adjustPosition(2);
                    return new Token(Token.OPERATOR, DefaultExpression.OP_LE);
                }
                else
                {
                    incrementPosition();
                    return new Token(Token.OPERATOR, DefaultExpression.OP_LT);
                }
            }
            case '>':
            {
                switch (char1)
                {
                    case '=':
                        adjustPosition(2);
                        return new Token(Token.OPERATOR, DefaultExpression.OP_GE);
                    case '<':
                        adjustPosition(2);
                        return new Token(Token.OPERATOR, DefaultExpression.OP_NE);
                    default:
                        incrementPosition();
                        return new Token(Token.OPERATOR, DefaultExpression.OP_GT);
                }
            }
            case '?':
            {
                if (char1 == '?')
                {
                    adjustPosition(2);
                    return new Token(Token.OPERATOR, DefaultExpression.OP_COA);
                }
                else
                {
                    incrementPosition();
                    return new Token(Token.OPERATOR, DefaultExpression.OP_CIF);
                }
            }
            case ':':
            case '[':
            case ']':
            case '{':
            case '}':
            case '(':
            case ')':
            case ';':
            case ',':
            {
                char character;

                character = char0;
                incrementPosition();
                return new Token(Token.SYMBOL, (double)character);
            }
            case '.':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            {
                newToken = parseNumericConstant();
                if (newToken != null) return newToken;
                else
                {
                    char character;

                    character = char0;
                    incrementPosition();
                    return new Token(Token.SYMBOL, (double)character);
                }
            }
            case '"': // Text enclosed in quotes is a String constant.
            {
                StringBuilder stringBuilder;

                stringBuilder = new StringBuilder();
                incrementPosition();
                while (true)
                {
                    switch (char0)
                    {
                        case '\n':
                            return new Token(Token.ERROR, "Missing end quote.");
                        case '\\':
                            char nextChar = char1;
                            if (nextChar == '"')
                            {
                                incrementPosition();
                                stringBuilder.append('"');
                                break;
                            }
                            else if (nextChar == '\\')
                            {
                                incrementPosition();
                                stringBuilder.append('\\');
                                break;
                            }
                            else if (nextChar == 'n')
                            {
                                incrementPosition();
                                stringBuilder.append('\n');
                                break;
                            }
                            else if (nextChar == 'r')
                            {
                                incrementPosition();
                                stringBuilder.append('\r');
                                break;
                            }
                            else if (nextChar == 't')
                            {
                                incrementPosition();
                                stringBuilder.append('\t');
                                break;
                            }
                            else if (nextChar == 'u')
                            {
                                String hexString;

                                adjustPosition(2); // Move to the start of the hex String.
                                hexString = ("" + char0 + char1 + char2 + char3);
                                adjustPosition(3); // Move past the already parsed hex.

                                stringBuilder.append((char)Integer.parseInt(hexString, 16));
                                break;
                            }
                        case '"':
                            incrementPosition();
                            return new Token(Token.STRING, stringBuilder.toString());
                        default:
                            stringBuilder.append(char0);
                    }

                    incrementPosition();
                    if (isEob())
                    {
                        return new Token(Token.ERROR, "Missing end quote.");
                    }
                }
            }
            case '\'':
            {
                if (char2 == '\'')
                {
                    char character;

                    character = char1;
                    adjustPosition(3);
                    return new Token(Token.CHARACTER, character);
                }
                else
                {
                    char character;

                    character = char0;
                    incrementPosition();
                    return new Token(Token.SYMBOL, (double)character);
                }
            }
            case '`': // Everything enclosed by two backticks, is regarded as an external expression (in some ways similar to the UNIX use of backticks).
            {
                StringBuilder stringBuilder;

                stringBuilder = new StringBuilder();
                incrementPosition();
                while (true)
                {
                    switch (char0)
                    {
                        case '`':
                            incrementPosition();
                            return new Token(Token.EXT_EXPRESSION, stringBuilder.toString());
                        default:
                            stringBuilder.append(char0);
                    }

                    incrementPosition();
                    if (isEob())
                    {
                        return new Token(Token.ERROR, "Missing enclosing backtick.");
                    }
                }
            }
            default:
            {
                break;
            }
        }

        // No symbols could be parsed, so that means that the next token is a word.  Now parse the entire word and try to match it to the applicable type.
        if (isLetter(char0) || isDigit(char0) || char0 == '_' || char0 == '@' || char0 == '$' || char0 == '#')
        {
            StringBuffer stringBuffer;
            String word;

            stringBuffer = new StringBuffer();
            while (isLetter(char0) || isDigit(char0) || char0 == '_' || char0 == '@' || char0 == '$' || char0 == '#')
            {
                stringBuffer.append(char0);
                incrementPosition();
            }

            // Get the word from the string buffer.
            word = stringBuffer.toString();

            // Check for NULL keyword.
            if (word.equals("null")) return new Token(Token.CONSTANT, null);

            // Check for true keyword.
            if (word.equals("true")) return new Token(Token.CONSTANT, true);

            // Check for true keyword.
            if (word.equals("false")) return new Token(Token.CONSTANT, false);

            // Check for instanceof operator.
            if (word.equals("instanceof")) return new Token(Token.OPERATOR, DefaultExpression.OP_INSTANCEOF);

            // Check for the accessor symbol to determine if this word is an identifier.
            if (char0 == '.')
            {
                return new Token(Token.IDENTIFIER, word);
            }

            // Check for opening square brackets to determine if this word is a collection.
            if (char0 == '[')
            {
                if (!word.equals("new")) // The new keyword cannot be used as a collection identifier.
                {
                    return new Token(Token.COLLECTION, word);
                }
            }

            // Statement and program KEYWORDS as well as FUNCTIONS can only be parsed if they are not preceded by the '.' operator in which case an identifier is more applicable.
            if ((lastParsedToken == null) || (!lastParsedToken.isSymbol('.')))
            {
                // Check if the parsed word is a function name.
                for (int functionIndex = 0; functionIndex < FunctionExpression.FUNCTIONS.length; functionIndex++)
                {
                    if (word.compareTo(FunctionExpression.FUNCTIONS[functionIndex]) == 0)
                    {
                        return new Token(Token.FUNCTION, functionIndex);
                    }
                }

                // Check if the parsed word is a Statement keyword.
                for (int index = 0; index < Statement.KEYWORDS.length; index++)
                {
                    if (word.compareTo(Statement.KEYWORDS[index]) == 0)
                    {
                        return new Token(Token.KEYWORD, index);
                    }
                }

                // Check if the parsed word is a Program keyword.
                for (int index = 0; index < Program.keywords.length; index++)
                {
                    if (word.compareTo(Program.keywords[index]) == 0)
                    {
                        return new Token(Token.KEYWORD, index);
                    }
                }
            }

            // Check for opening brackets which would indicate a procedure.
            if (char0 == '(')
            {
                return new Token(Token.PROCEDURE, word);
            }

            // At this point, through the process of elimination, the word must be a variable.
            return new Variable(word);
        }
        else throw new EPICSyntaxException("Unexpected character '" + char0 + "' found.", getLineNumber());
    }
}
