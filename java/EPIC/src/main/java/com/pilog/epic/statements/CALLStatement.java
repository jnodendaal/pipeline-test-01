package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.ExtensionMethodStub;
import com.pilog.epic.ExtensionProcedure;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Procedure;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import java.util.ArrayList;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class CALLStatement extends Statement
{
    private final ArrayList<Expression> parameters;
    private String procedureName;
    private Procedure reflectiveProcedure;
    private boolean reflectiveProcedureIsStatic;

    private CALLStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
        parameters = new ArrayList<>();
        reflectiveProcedureIsStatic = false;
    }

    public void setProcedureName(String procedureName)
    {
        this.procedureName = procedureName;
    }

    public void addParameter(Expression parameter)
    {
        parameters.add(parameter);
    }

    public static CALLStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Token nextToken;

        // Check whether the parseExpression starts with an opening bracket.
        nextToken = tokenizer.nextToken();
        if (nextToken.isProcedureName())
        {
            String procedureName;

            procedureName = (String)nextToken.getValue();
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                CALLStatement newStatement;

                // Create a new function parseExpression.
                newStatement = new CALLStatement(parentExecutionUnit, tokenizer.getLineNumber());
                newStatement.setProcedureName(procedureName);
                while (!(nextToken = tokenizer.nextToken()).isSymbol(')'))
                {
                    // Return the next token so it can be parsed as an parseExpression.
                    tokenizer.returnToken(nextToken);

                    // Parse the next token as an parseExpression and add the result as a function argument.
                    newStatement.addParameter(ExpressionParser.parseExpression(tokenizer, newStatement));

                    // Check the next token to see if it is a comma.
                    nextToken = tokenizer.nextToken();
                    if (!nextToken.isSymbol(',')) tokenizer.returnToken(nextToken);
                }

                return newStatement;
            }
            else throw new EPICSyntaxException("No procedure arguments found.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of procedure call: " + nextToken, tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        ArrayList<Object> parameterValues;
        Procedure procedure;

        // Evaluate the procedure parameters.
        parameterValues = new ArrayList<Object>();
        for (Expression parameterExpression : parameters)
        {
            parameterValues.add(parameterExpression.evaluate(this, null)); // The context object here has to be null, because the expressions are executed independently.
        }

        // First, if this call is not chained to a preceding statement that establishes the conextObject,
        // try to get the procedure from the statically imported collection.
        if (inputStatement == null)
        {
            // Find the procedure in the parent execution unit.
            procedure = parentExecutionUnit.getProcedure(procedureName);

            // At this stage, if the procedure was not found, the only remaining possibility to consider is the availability
            // of a contextObject even if this is not a chained statement.  This scenario occurs when a contextObject is
            // supplied as input to a script execution.
            if (procedure == null)
            {
                // Get the reflective method.
                if (reflectiveProcedure == null) reflectiveProcedure = getReflectiveProcedure(procedureName, contextObject);

                // Make sure that the reflective procedure is either static or that a context object is available.
                if ((contextObject != null) || (reflectiveProcedureIsStatic))
                {
                    procedure = reflectiveProcedure;
                }
                else throw new EPICRuntimeException("Context object for method '" + procedureName + "' is null.", lineNumber);
            }
        }
        else // Try to resolve the chained call dynamically.
        {
            // Get the reflective method.
            if (reflectiveProcedure == null) reflectiveProcedure = getReflectiveProcedure(procedureName, contextObject);

            // Make sure that the reflective procedure is either static or that a context object is available.
            if ((contextObject != null) || (reflectiveProcedureIsStatic))
            {
                procedure = reflectiveProcedure;
            }
            else throw new EPICRuntimeException("Context object for method '" + procedureName + "' is null.", lineNumber);
        }

        // If the procedure could still not be resolved, throw an exception.
        if (procedure == null) throw new EPICRuntimeException("Procedure '" + procedureName + "' not defined.", lineNumber);
        else
        {
            try
            {
                // If a statement is chained to this one, execute it, otherwise just return the result.
                if (chainedStatement != null)
                {
                    procedure.setParameterValues(parameterValues);
                    procedure.execute(contextObject);
                    returnObject = procedure.getReturnObject();
                    return chainedStatement.execute(procedure.getReturnObject());
                }
                else
                {
                    int returnValue;

                    procedure.setParameterValues(parameterValues);
                    returnValue = procedure.execute(contextObject);
                    returnObject = procedure.getReturnObject();
                    return returnValue;
                }
            }
            catch (Exception e)
            {
                throw new EPICRuntimeException("Exception while calling '" + procedureName + "' using context object: " + contextObject + " and input parameters: " + parameterValues, e, lineNumber);
            }
        }
    }

    private Procedure getReflectiveProcedure(String procedureName, Object contextObject) throws EPICRuntimeException
    {
        if (contextObject instanceof Class)
        {
            reflectiveProcedureIsStatic = true;
            return new ExtensionProcedure(procedureName, new ExtensionMethodStub((Class)contextObject, procedureName));
        }
        else
        {
            if (contextObject != null)
            {
                reflectiveProcedureIsStatic = false;
                return new ExtensionProcedure(procedureName, new ExtensionMethodStub(procedureName));
            }
            else throw new EPICRuntimeException("Context object for method '" + procedureName + "' is null.", lineNumber);
        }
    }
}
