package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.exceptions.EPICRuntimeException;

/**
 * @author Bouwer du Preez
 */
public class CoalesceExpression implements Expression
{
    private final Expression valueExpression;
    private final Expression defaultExpression;

    public CoalesceExpression(Expression valueExpression, Expression defaultExpression)
    {
        super();
        this.valueExpression = valueExpression;
        this.defaultExpression = defaultExpression;
    }

    @Override
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        Object value;

        value = valueExpression.evaluate(parentExecutionUnit, contextObject);
        if (value != null)
        {
            return value;
        }
        else
        {
            return defaultExpression.evaluate(parentExecutionUnit, contextObject);
        }
    }

    @Override
    public String unparse()
    {
        return valueExpression.unparse() + "??" + defaultExpression.unparse();
    }
}
