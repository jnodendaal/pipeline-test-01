package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.statements.Statement;

/**
 * @author Bouwer du Preez
 */
public class StatementExpression implements Expression
{
    private final Statement statement;

    public StatementExpression(Statement statement)
    {
        this.statement = statement;
    }

    /**
     * Returns the evaluated value of this expression.
     *
     * @param parentExecutionUnit The ExpressionEvaluator executing this expression.
     * @param contextObject
     * @return The result of this expression.
     * @throws EPICRuntimeException
     */
    @Override
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        statement.execute(contextObject);
        return statement.getReturnObject();
    }

    @Override
    public String unparse()
    {
        return statement.getOriginalString();
    }
}
