package com.pilog.epic.rsyntax;

import java.util.List;
import javax.swing.text.JTextComponent;

/**
 * @author Hennie Brink
 */
public interface RegexCompletionProvider
{
    public String getRegexPatternToMatch();

    public List<BasicEpicCompletion> getCompletionChoices(JTextComponent tc, String matchedString);
}
