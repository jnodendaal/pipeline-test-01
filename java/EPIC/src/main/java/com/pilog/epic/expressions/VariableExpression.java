package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.Variable;
import com.pilog.epic.exceptions.EPICRuntimeException;

/**
 * This class implements an expression that is simply a variable.
 *
 * @author Bouwer du Preez
 */
public class VariableExpression extends DefaultExpression
{
    private final Variable variable;

    public VariableExpression(Variable variable)
    {
        super();
        this.variable = variable;
    }

    @Override
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        return parentExecutionUnit.getVariable(variable.getVariableName());
    }

    @Override
    public String unparse()
    {
        return variable.toString();
    }

    @Override
    public String toString()
    {
        return variable.toString();
    }
}
