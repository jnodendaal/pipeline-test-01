package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.t8.utilities.strings.Strings;
import java.util.Collection;
import com.pilog.epic.expressions.Expression;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class EMPTYStatement extends Statement
{
    private Expression valueExpression;

    private EMPTYStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public Expression getStringExpression()
    {
        return valueExpression;
    }

    public void setStringExpression(Expression stringExpression)
    {
        this.valueExpression = stringExpression;
    }

    public static EMPTYStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        EMPTYStatement newStatement;
        Token nextToken;

        newStatement = new EMPTYStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.EMPTY))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                Expression valueExpression;

                valueExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                newStatement.setStringExpression(valueExpression);

                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(')'))
                {
                    return newStatement;
                }
                else throw new EPICSyntaxException("Missing closing ')' in empty() statement.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("'(' character missing in empty() statement.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of empty() statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Object value;

        // Evaluate the value expression end check it according to the type of result.
        value = valueExpression.evaluate(parentExecutionUnit, contextObject);
        if (value == null)
        {
            returnObject = true;
        }
        else if (value instanceof Collection)
        {
            returnObject = ((Collection)value).isEmpty();
        }
        else if (value instanceof Map)
        {
            returnObject = ((Map)value).isEmpty();
        }
        else if (value instanceof String)
        {
            returnObject = Strings.isNullOrEmpty((String)value);
        }
        else throw new EPICRuntimeException("Invalid operand in 'empty' statement: " + value + " of type: " + value.getClass().getCanonicalName(), lineNumber);

        // Return the final result code.
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
