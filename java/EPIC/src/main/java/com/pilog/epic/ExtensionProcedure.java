package com.pilog.epic;

import com.pilog.epic.exceptions.EPICRuntimeException;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class ExtensionProcedure extends Procedure implements Serializable
{
    private final ExtensionMethodStub methodStub;

    public ExtensionProcedure(String procedureName, ExtensionMethodStub methodStub)
    {
        this.methodStub = methodStub;
        setProcedureName(procedureName);
    }
    
    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Object result;

        result = methodStub.executeMethod(contextObject, getParameterValues());
        setReturnObject(result);
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
