package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class SWITCHStatement extends Statement
{
    private final ArrayList<Expression> caseConditions;
    private final ArrayList<List<ExecutionUnit>> caseExecutionUnits;
    private final List<ExecutionUnit> defaultExecutionUnits;
    private Expression switchExpression;

    private SWITCHStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
        this.caseConditions = new ArrayList<>();
        this.caseExecutionUnits = new ArrayList<List<ExecutionUnit>>();
        this.defaultExecutionUnits = new ArrayList<ExecutionUnit>();
    }

    public void addCase(Expression condition, List<ExecutionUnit> executionUnits)
    {
        caseConditions.add(condition);
        caseExecutionUnits.add(executionUnits);
    }

    public void addDefaultCase(List<ExecutionUnit> executionUnits)
    {
        defaultExecutionUnits.addAll(executionUnits);
    }

    public void setSwitchExpression(Expression expression)
    {
        this.switchExpression = expression;
    }

    public static SWITCHStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        SWITCHStatement newStatement;
        Token nextToken;

        newStatement = new SWITCHStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.SWITCH))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                Expression switchExpression;

                switchExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                newStatement.setSwitchExpression(switchExpression);
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(')'))
                {
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol('{'))
                    {
                        // Parse all the cases.
                        while (true)
                        {
                            nextToken = tokenizer.nextToken();
                            if (nextToken.isKeyword(Statement.CASE))
                            {
                                Expression caseExpression;

                                caseExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                                nextToken = tokenizer.nextToken();
                                if (nextToken.isSymbol(':'))
                                {
                                    List<ExecutionUnit> caseExecutionUnits;

                                    caseExecutionUnits = new ArrayList<ExecutionUnit>();
                                    while (true)
                                    {
                                        nextToken = tokenizer.nextToken();
                                        if ((!nextToken.isKeyword(Statement.CASE)) && (!nextToken.isKeyword(Statement.DEFAULT)) && (!nextToken.isSymbol('}')))
                                        {
                                            ExecutionUnit executionUnit;

                                            tokenizer.returnToken(nextToken);
                                            executionUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newStatement);
                                            caseExecutionUnits.add(executionUnit);
                                        }
                                        else
                                        {
                                            newStatement.addCase(caseExpression, caseExecutionUnits);
                                            tokenizer.returnToken(nextToken);
                                            break;
                                        }
                                    }
                                }
                                else throw new EPICSyntaxException("Missing ':' in case statement.", tokenizer.getLineNumber());
                            }
                            else if (nextToken.isKeyword(Statement.DEFAULT))
                            {
                                nextToken = tokenizer.nextToken();
                                if (nextToken.isSymbol(':'))
                                {
                                    List<ExecutionUnit> defaultCaseExecutionUnits;

                                    defaultCaseExecutionUnits = new ArrayList<ExecutionUnit>();
                                    while (true)
                                    {
                                        nextToken = tokenizer.nextToken();
                                        if (!nextToken.isSymbol('}'))
                                        {
                                            ExecutionUnit executionUnit;

                                            tokenizer.returnToken(nextToken);
                                            executionUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newStatement);
                                            defaultCaseExecutionUnits.add(executionUnit);
                                        }
                                        else
                                        {
                                            newStatement.addDefaultCase(defaultCaseExecutionUnits);
                                            tokenizer.returnToken(nextToken);
                                            break;
                                        }
                                    }
                                }
                                else throw new EPICSyntaxException("Missing ':' in case default statement.", tokenizer.getLineNumber());
                            }
                            else if (nextToken.isSymbol('}'))
                            {
                                return newStatement;
                            }
                            else throw new EPICSyntaxException("Invalid token found in switch statement: " + nextToken, tokenizer.getLineNumber());
                        }
                    }
                    else throw new EPICSyntaxException("Missing opening '{' in switch statement.", tokenizer.getLineNumber());
                }
                else throw new EPICSyntaxException("Missing closing ')' in switch statement.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("'(' character missing in switch statement.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of switch statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Object switchResult;
        boolean fallThrough;

        // Clear variables so that previous execution data is discarded.
        variables.clear();

        fallThrough = false;
        switchResult = switchExpression.evaluate(parentExecutionUnit, contextObject);
        for (int clauseIndex = 0; clauseIndex < caseConditions.size(); clauseIndex++)
        {
            Object conditionResult;
            Expression condition;

            condition = caseConditions.get(clauseIndex);
            conditionResult = condition.evaluate(parentExecutionUnit, contextObject);
            if ((fallThrough) || (Objects.equals(switchResult, conditionResult)))
            {
                List<ExecutionUnit> executionUnits;

                fallThrough = true;
                executionUnits = caseExecutionUnits.get(clauseIndex);
                for (ExecutionUnit executionUnit : executionUnits)
                {
                    int result;

                    // Execute the unit.
                    result = executionUnit.execute(contextObject);

                    // If the execution result is 'return' we have to return as this
                    // will possibly end the parent execution unit.
                    if (result == ExecutionUnit.RETURN_CODE_RETURN)
                    {
                        return result;
                    }
                    else if (result == ExecutionUnit.RETURN_CODE_BREAK)
                    {
                        // If a break is executed inside the switch case, it means the entire switch has completed.
                        return ExecutionUnit.RETURN_CODE_COMPLETED;
                    }
                }
            }
        }

        // If a default case exists and we've reached this point, execute it since no other term broke the fall-through.
        if (defaultExecutionUnits.size() > 0)
        {
            for (ExecutionUnit executionUnit : defaultExecutionUnits)
            {
                int result;

                // Execute the unit.
                result = executionUnit.execute(contextObject);

                // If the execution result is 'return' we have to return as this
                // will possibly end the parent execution unit.
                if (result == ExecutionUnit.RETURN_CODE_RETURN)
                {
                    return result;
                }
                else if (result == ExecutionUnit.RETURN_CODE_BREAK)
                {
                    // If a break is executed inside the switch case, it means the entire switch has completed.
                    return ExecutionUnit.RETURN_CODE_COMPLETED;
                }
            }
        }

        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
