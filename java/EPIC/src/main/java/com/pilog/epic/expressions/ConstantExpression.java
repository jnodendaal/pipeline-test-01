package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.exceptions.EPICRuntimeException;

/**
 * This class implements the simplest possible expression, a constant.
 *
 * @author Bouwer du Preez
 */
public class ConstantExpression implements Expression
{
    private final Object constantValue;

    public ConstantExpression(Object constantValue)
    {
        super();
        this.constantValue = constantValue;
    }

    @Override
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        return constantValue;
    }

    @Override
    public String toString()
    {
        return constantValue.toString();
    }

    @Override
    public String unparse()
    {
        return constantValue.toString();
    }
}
