package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;

/**
 * @author Bouwer du Preez
 */
public class TRYStatement extends Statement
{
    private ExecutionUnit tryBlock;
    private VARStatement exceptionVariableStatement;
    private ExecutionUnit catchBlock;
    private ExecutionUnit finallyBlock;

    private TRYStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public ExecutionUnit getTryBlock()
    {
        return tryBlock;
    }

    public void setTryBlock(ExecutionUnit tryBlock)
    {
        this.tryBlock = tryBlock;
    }

    public void setCatchBlock(VARStatement exceptionVariable, ExecutionUnit catchBlock)
    {
        this.exceptionVariableStatement = exceptionVariable;
        this.catchBlock = catchBlock;
    }

    public ExecutionUnit getFinallyBlock()
    {
        return finallyBlock;
    }

    public void setFinallyBlock(ExecutionUnit finallyBlock)
    {
        this.finallyBlock = finallyBlock;
    }

    public static TRYStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        TRYStatement newStatement;
        Token nextToken;

        newStatement = new TRYStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.TRY))
        {
            newStatement.setTryBlock(ExecutionUnit.parseExecutionUnit(tokenizer, newStatement));
            nextToken = tokenizer.nextToken();
            if (nextToken.isKeyword(Statement.CATCH))
            {
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol('('))
                {
                    VARStatement exceptionVariable;

                    exceptionVariable = VARStatement.parseStatement(tokenizer, newStatement);
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol(')'))
                    {
                        ExecutionUnit catchUnit;

                        catchUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newStatement);
                        newStatement.setCatchBlock(exceptionVariable, catchUnit);
                    }
                    else throw new EPICSyntaxException("Missing closing parenthesis ')'.", tokenizer.getLineNumber());
                }
                else throw new EPICSyntaxException("Invalid catch statement; no Exception class defined.", tokenizer.getLineNumber());
            }
            else tokenizer.returnToken(nextToken);

            // Check for the finally clause.
            nextToken = tokenizer.nextToken();
            if (nextToken.isKeyword(Statement.FINALLY))
            {
                ExecutionUnit finallyUnit;

                finallyUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newStatement);
                newStatement.setFinallyBlock(finallyUnit);
            }
            else tokenizer.returnToken(nextToken);

            // Return the complete statement.
            return newStatement;
        }
        else throw new EPICSyntaxException("Invalid start of try statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Exception unhandledException;
        int returnCode;

        // Set the default return code.
        returnCode = RETURN_CODE_COMPLETED;
        unhandledException = null;

        try
        {
            // Execute the try-block and return the return code.
            returnCode = tryBlock.execute(contextObject);
        }
        catch (Exception e)
        {
            // Execute the catch block (if any).
            if (catchBlock != null)
            {
                // Execute the exception variable declaration statement and then set its value.
                exceptionVariableStatement.execute(contextObject);
                setVariable(exceptionVariableStatement.getVariableName(), e);

                // Now execute the catch block.
                try
                {
                    returnCode = catchBlock.execute(contextObject);
                }
                catch (Exception innerException)
                {
                    unhandledException = innerException;
                }
            }
            else unhandledException = e;
        }
        finally
        {
            // Execute the finally block (if any).
            if (finallyBlock != null)
            {
                int finallyReturnCode;

                // Execute the finally block and check the return code.
                finallyReturnCode = finallyBlock.execute(contextObject);

                // If the return code of the finally block is not the default, use it as the return code of the entire try-catch-finally.
                if (finallyReturnCode != RETURN_CODE_COMPLETED) returnCode = finallyReturnCode;
            }

            // Clear exception variables for subsequent execution.
            variables.clear();

            // If the exception was unhandled, throw it.
            if (unhandledException != null)
            {
                if (unhandledException instanceof EPICRuntimeException) throw (EPICRuntimeException)unhandledException;
                else throw new EPICRuntimeException("Unhandled exception", unhandledException, lineNumber);
            }
            else return returnCode;
        }
    }
}
