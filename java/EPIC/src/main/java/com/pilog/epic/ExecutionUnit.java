package com.pilog.epic;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.statements.StatementParser;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public abstract class ExecutionUnit
{
    protected ExecutionUnit rootExecutionUnit; // Keep this reference so it does not have to be computed during execution.
    protected ExecutionUnit parentExecutionUnit;
    protected HashMap<String, Object> variables;
    protected HashSet<String> variableDeclarations; // This is a compile time collection, only used to identfy variables that have already been declared in the stack.
    protected Set<String> variableNames; // Holds the names of all variables referenced by this execution unit.
    protected Object returnObject;

    public static final int RETURN_CODE_COMPLETED = 0;
    public static final int RETURN_CODE_CONTINUE = 1;
    public static final int RETURN_CODE_BREAK = 2;
    public static final int RETURN_CODE_RETURN = 3;

    @SuppressWarnings("LeakingThisInConstructor")
    public ExecutionUnit(ExecutionUnit parentExecutionUnit)
    {
        this.parentExecutionUnit = parentExecutionUnit;
        this.variables = new HashMap<String, Object>();
        this.variableDeclarations = new HashSet<String>();
        this.variableNames = new HashSet<String>();
        this.returnObject = null;

        // This will be evaluated during compilation and not execution (speeds up procedure calls).
        rootExecutionUnit = this;
        while (rootExecutionUnit.parentExecutionUnit != null) rootExecutionUnit = rootExecutionUnit.parentExecutionUnit;
    }

    /**
     * Returns the parent ExecutionUnit to which this ExecutionUnit belongs.
     * @return The parent ExecutionUnit to which this ExecutionUnit belongs.
     */
    public ExecutionUnit getParentExecutionUnit()
    {
        return parentExecutionUnit;
    }

    /**
     * Returns all of the descendant ExecutionUnits that form part of this ExecutionUnit.
     * @return All of the descendant ExecutionUnits that form part of this ExecutionUnit.
     */
    public abstract List<ExecutionUnit> getDescendantExecutionUnits();

    public static ExecutionUnit parseExecutionUnit(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Token nextToken;

        nextToken = tokenizer.nextToken();
        if (nextToken.isSymbol('{'))
        {
            tokenizer.returnToken(nextToken);
            return ScopeBlock.parseScopeBlock(tokenizer, parentExecutionUnit);
        }
        else
        {
            tokenizer.returnToken(nextToken);
            return StatementParser.parseStatement(tokenizer, parentExecutionUnit, true);
        }
    }

    public Set<String> getVariableNames()
    {
        return new HashSet<String>(variableNames);
    }

    public void addVariableName(String variableName)
    {
        variableNames.add(variableName);
    }

    public Object getVariable(String variableName) throws EPICRuntimeException
    {
        if (variables.containsKey(variableName))
        {
            return variables.get(variableName);
        }
        else if (parentExecutionUnit != null)
        {
            return parentExecutionUnit.getVariable(variableName);
        }
        else
        {
            throw new EPICRuntimeException("Variable '" + variableName + "' not found.", -1);
        }
    }

    public void declareVariable(String variableName, Object variableValue) throws EPICRuntimeException
    {
        if (!containsVariable(variableName))
        {
            variables.put(variableName, variableValue);
        }
        else throw new EPICRuntimeException("Invalid variable declaration.  Variable '" + variableName + "' is already declared.", -1);
    }

    public void setVariable(String variableName, Object variableValue) throws EPICRuntimeException
    {
        if (variables.containsKey(variableName))
        {
            variables.put(variableName, variableValue);
        }
        else if (parentExecutionUnit != null)
        {
            parentExecutionUnit.setVariable(variableName, variableValue);
        }
        else throw new EPICRuntimeException("Invalid variable assignment.  Variable '" + variableName + "' is not declared in this scope.", -1);
    }

    public boolean containsVariable(String variableName)
    {
        if (variables.containsKey(variableName))
        {
            return true;
        }
        else if (parentExecutionUnit != null)
        {
            return parentExecutionUnit.containsVariable(variableName);
        }
        else return false;
    }

    public Object getReturnObject()
    {
        return returnObject;
    }

    public void setReturnObject(Object returnObject)
    {
        this.returnObject = returnObject;
    }

    public void addVariableDeclaration(String variableName) throws EPICSyntaxException
    {
        if (!containsVariable(variableName))
        {
            variableDeclarations.add(variableName);
        }
        else throw new EPICSyntaxException("Invalid variable declaration.  Variable '" + variableName + "' is already declared.", -1);
    }

    public boolean isVariableDeclared(String variableName)
    {
        if (variableDeclarations.contains(variableName))
        {
            return true;
        }
        else if (parentExecutionUnit != null)
        {
            return parentExecutionUnit.isVariableDeclared(variableName);
        }
        else return false;
    }

    public boolean isClassImported(String className)
    {
        if (parentExecutionUnit != null)
        {
            return parentExecutionUnit.isClassImported(className);
        }
        else return false;
    }

    public Class getClassImport(String className) throws EPICRuntimeException
    {
        return ((Program)rootExecutionUnit).getClassImport(className);
    }

    public Procedure getProcedure(String procedureName) throws EPICRuntimeException
    {
        return ((Program)rootExecutionUnit).getProcedure(procedureName);
    }

    public abstract int execute(Object contextObject) throws EPICRuntimeException;
}
