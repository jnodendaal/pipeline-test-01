package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class FLOATStatement extends Statement
{
    private Expression valueExpression;

    private FLOATStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public void setValueExpression(Expression valueExpression)
    {
        this.valueExpression = valueExpression;
    }

    public Expression getValueExpression()
    {
        return valueExpression;
    }

    public static FLOATStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        FLOATStatement newStatement;
        Token nextToken;

        newStatement = new FLOATStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.FLOAT))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                Expression valueExpression;

                valueExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                newStatement.setValueExpression(valueExpression);

                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(')'))
                {
                    return newStatement;
                }
                else throw new EPICSyntaxException("')' character missing in float cast statement.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("'(' character missing in float cast statement.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid constant: " + nextToken.getValue(), tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        returnObject = castToFloat(valueExpression.evaluate(parentExecutionUnit, contextObject));
        if (chainedStatement != null)
        {
            return chainedStatement.execute(returnObject);
        }
        else return Statement.RETURN_CODE_COMPLETED;
    }

    private float castToFloat(Object value) throws EPICRuntimeException
    {
        if (value == null)
        {
            throw new EPICRuntimeException("Cannot cast null value to float.", null, lineNumber);
        }
        else if (value instanceof String)
        {
            try
            {
                return Float.parseFloat((String)value);
            }
            catch (Exception e)
            {
                throw new EPICRuntimeException("Cannot cast String value '" + value + "' to float.", e, lineNumber);
            }
        }
        else if (value instanceof Number)
        {
            return ((Number)value).floatValue();
        }
        else throw new EPICRuntimeException("Cannot cast type '" + value.getClass() + "' to float.", null, lineNumber);
    }
}
