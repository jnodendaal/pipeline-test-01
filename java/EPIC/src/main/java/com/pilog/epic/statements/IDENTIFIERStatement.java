package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class IDENTIFIERStatement extends Statement
{
    private Class classIdentifier;
    private String variableIdentifier;

    private IDENTIFIERStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public Class getClassIdentifier()
    {
        return classIdentifier;
    }

    public void setClassIdentifier(Class classIdentifier)
    {
        this.classIdentifier = classIdentifier;
    }

    public String getVariableIdentifier()
    {
        return variableIdentifier;
    }

    public void setVariableIdentifier(String variableIdentifier)
    {
        this.variableIdentifier = variableIdentifier;
    }

    public static IDENTIFIERStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit, boolean constructor, boolean allowVariableIdentifier) throws EPICSyntaxException
    {
        IDENTIFIERStatement newStatement;
        StringBuffer className;
        Token nextToken;

        // Create the new statement.
        newStatement = new IDENTIFIERStatement(parentExecutionUnit, tokenizer.getLineNumber());

        // Attempt to parse a package name from the current tokenizer location.
        className = parseClass(null, tokenizer, false);
        if (className != null)
        {
            Class newClass;

            newClass = tryClass(className.toString());
            if (newClass != null)
            {
                newStatement.setClassIdentifier(newClass);
                return newStatement;
            }
            else
            {
                // No valid identifier could be parsed, so throw a Syntax Error.
                throw new EPICSyntaxException("Unresolvable identifier encountered: " + className + tokenizer.nextToken(), tokenizer.getLineNumber());
            }
        }
        else // No valid package could be parsed, which means the identifier can only be a variable.
        {
            nextToken = tokenizer.nextToken();
            if ((nextToken.isVariable() || nextToken.isProcedureName() || nextToken.isIdentifier()) && (allowVariableIdentifier))
            {
                String variableName;

                variableName = (String)nextToken.getValue();
                if ((!tokenizer.isCheckVariableDeclaration()) || (parentExecutionUnit.isVariableDeclared(variableName)) || (parentExecutionUnit.isClassImported(variableName)))
                {
                    newStatement.setVariableIdentifier(variableName);
                    return newStatement;
                }
                else throw new EPICSyntaxException("Undeclared variable: " + variableName, tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("Unresolvable identifier encountered: " + nextToken, tokenizer.getLineNumber());
        }
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        if (classIdentifier != null) // Static class identifier.
        {
            returnObject = classIdentifier;
        }
        else if (variableIdentifier != null) // Dynamic context object.
        {
            returnObject = getClassImport(variableIdentifier);
            if (returnObject == null) returnObject = getVariable(variableIdentifier);
        }

        if (chainedStatement != null)
        {
            return chainedStatement.execute(returnObject);
        }
        else return Statement.RETURN_CODE_COMPLETED;
    }

    private static Class tryClass(String classIdentifier)
    {
        try
        {
            return Class.forName(classIdentifier, false, IDENTIFIERStatement.class.getClassLoader());
        }
        catch (Exception e)
        {
            // Do not handle this exception since it will always occur during successful parsing.
            return null;
        }
    }

    private static StringBuffer parseClass(StringBuffer prefix, LexicalTokenizer tokenizer, boolean innerClass) throws EPICSyntaxException
    {
        StringBuffer identifierString;
        ArrayList<Token> removedTokens;
        Token nextToken;

        identifierString = new StringBuffer();
        removedTokens = new ArrayList<Token>();

        nextToken = tokenizer.nextToken();
        if (prefix != null) identifierString.append(prefix); // A prefix will only be supplied if we are trying to parse an inner class.
        while ((nextToken.isVariable()) || (nextToken.isProcedureName()) || (nextToken.isIdentifier()) || (nextToken.isSymbol('.')))
        {
            if (nextToken.isSymbol('.'))
            {
                // If we are trying to parse an inner class we have to use the correct conjunction.
                if (innerClass)
                {
                    identifierString.append("$");
                }
                else
                {
                    identifierString.append((char)((Number)nextToken.getValue()).intValue());
                }

                removedTokens.add(nextToken);
            }
            else if (nextToken.isIdentifier())
            {
                identifierString.append(nextToken.getValue());
                removedTokens.add(nextToken);
            }
            else if (nextToken.isProcedureName()) // This will happen when a constructor is called e.g. var test = new java.util.ArrayList();
            {
                identifierString.append(nextToken.getValue());
                removedTokens.add(nextToken);
            }
            else if (nextToken.isVariable()) // This will happen in an import statement e.g. import java.lang.System;
            {
                identifierString.append(nextToken.getValue());
                removedTokens.add(nextToken);
            }

            // Check if a valid class name has been found.
            if (tryClass(identifierString.toString()) != null)
            {
                StringBuffer innerClassIdentifierString;

                // A valid class has been found, so try to parse a valid inner class using the next tokens.  If no inner class can be parsed, just return the class we already found.
                innerClassIdentifierString = parseClass(identifierString, tokenizer, true);
                return innerClassIdentifierString != null ? innerClassIdentifierString : identifierString;
            }

            // Get the next token.
            nextToken = tokenizer.nextToken();
        }

        // Return the next token.
        tokenizer.returnToken(nextToken);

        // If the parsed package is invalid, replace all removed tokens and return null.
        for (int tokenIndex = removedTokens.size()-1; tokenIndex > -1; tokenIndex--)
        {
            tokenizer.returnToken(removedTokens.get(tokenIndex));
        }

        // No valid class found so return null.
        return null;
    }
}
