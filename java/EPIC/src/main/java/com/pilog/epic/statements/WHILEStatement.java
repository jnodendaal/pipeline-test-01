package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class WHILEStatement extends Statement
{
    private Expression conditionExpression;
    private ExecutionUnit executionUnit;

    private WHILEStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public Expression getConditionExpression()
    {
        return conditionExpression;
    }

    public void setConditionExpression(Expression conditionExpression)
    {
        this.conditionExpression = conditionExpression;
    }

    public ExecutionUnit getExecutionUnit()
    {
        return executionUnit;
    }

    public void setExecutionUnit(ExecutionUnit executionUnit)
    {
        this.executionUnit = executionUnit;
    }

    public static WHILEStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        WHILEStatement newStatement;
        Token nextToken;

        newStatement = new WHILEStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.WHILE))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                Expression whileExpression;

                whileExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                newStatement.setConditionExpression(whileExpression);

                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(')'))
                {
                    ExecutionUnit whileExecutionUnit;

                    whileExecutionUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newStatement);
                    newStatement.setExecutionUnit(whileExecutionUnit);
                    return newStatement;
                }
                else throw new EPICSyntaxException("Missing closing ')' in while statement.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("'(' character missing in while statement.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of while statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        while ((Boolean)conditionExpression.evaluate(this, contextObject))
        {
            int returnCode;

            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
