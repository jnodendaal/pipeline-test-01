package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICSyntaxException;
import static com.pilog.epic.statements.Statement.BREAK;
import static com.pilog.epic.statements.Statement.CONTINUE;
import static com.pilog.epic.statements.Statement.DOUBLE;
import static com.pilog.epic.statements.Statement.EMPTY;
import static com.pilog.epic.statements.Statement.FLOAT;
import static com.pilog.epic.statements.Statement.FOR;
import static com.pilog.epic.statements.Statement.IF;
import static com.pilog.epic.statements.Statement.INT;
import static com.pilog.epic.statements.Statement.LONG;
import static com.pilog.epic.statements.Statement.NEW;
import static com.pilog.epic.statements.Statement.PARSE;
import static com.pilog.epic.statements.Statement.PRINT;
import static com.pilog.epic.statements.Statement.PRINTLN;
import static com.pilog.epic.statements.Statement.RETURN;
import static com.pilog.epic.statements.Statement.SWITCH;
import static com.pilog.epic.statements.Statement.TRY;
import static com.pilog.epic.statements.Statement.THROW;
import static com.pilog.epic.statements.Statement.VAR;
import static com.pilog.epic.statements.Statement.WHILE;

/**
 * @author Bouwer du Preez
 */
public class StatementParser
{
    /**
     * This method is the main entry point for statement parsing.  It determines
     * the type of statement to be parsed by evaluating some of the tokens from
     * the tokenizer and then delegates further parsing to the appropriate type
     * statement parsing method.
     *
     * @param tokenizer The tokenizer from which a statement will be parsed.
     * @param parentExecutionUnit The parent execution unit to which the newly
     * parsed statement will be assigned.
     * @param checkTerminationCharacter A boolean value indicating whether or
     * not to check for the ';' statement termination character.  This value is
     * only set to false when the statements that usually require the
     * termination character are evaluated inside of an expression (then the
     * character should be checked).
     * @return The newly parsed statement.
     * @throws EPICSyntaxException
     */
    public static Statement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit, boolean checkTerminationCharacter) throws EPICSyntaxException
    {
        Statement newStatement;
        Token nextToken;

        nextToken = tokenizer.nextToken();
        if (nextToken.getType() == Token.KEYWORD)
        {
            int keywordCode;

            keywordCode = ((Number)nextToken.getValue()).intValue();
            switch (keywordCode)
            {
                case IF:
                    tokenizer.returnToken(nextToken);
                    newStatement = IFStatement.parseStatement(tokenizer, parentExecutionUnit);
                    break;
                case SWITCH:
                    tokenizer.returnToken(nextToken);
                    newStatement = SWITCHStatement.parseStatement(tokenizer, parentExecutionUnit);
                    break;
                case EMPTY:
                    tokenizer.returnToken(nextToken);
                    newStatement = EMPTYStatement.parseStatement(tokenizer, parentExecutionUnit);
                    if ((checkTerminationCharacter) && (!tokenizer.nextToken().isSymbol(';'))) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
                    break;
                case FOR:
                    tokenizer.returnToken(nextToken);
                    newStatement = FORStatement.parseStatement(tokenizer, parentExecutionUnit);
                    break;
                case VAR:
                    tokenizer.returnToken(nextToken);
                    newStatement = VARStatement.parseStatement(tokenizer, parentExecutionUnit);
                    if ((checkTerminationCharacter) && (!tokenizer.nextToken().isSymbol(';'))) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
                    break;
                case WHILE:
                    tokenizer.returnToken(nextToken);
                    newStatement = WHILEStatement.parseStatement(tokenizer, parentExecutionUnit);
                    break;
                case BREAK:
                    tokenizer.returnToken(nextToken);
                    newStatement = BREAKStatement.parseStatement(tokenizer, parentExecutionUnit);
                    if ((checkTerminationCharacter) && (!tokenizer.nextToken().isSymbol(';'))) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
                    break;
                case CONTINUE:
                    tokenizer.returnToken(nextToken);
                    newStatement = CONTINUEStatement.parseStatement(tokenizer, parentExecutionUnit);
                    if ((checkTerminationCharacter) && (!tokenizer.nextToken().isSymbol(';'))) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
                    break;
                case RETURN:
                    tokenizer.returnToken(nextToken);
                    newStatement = RETURNStatement.parseStatement(tokenizer, parentExecutionUnit);
                    if ((checkTerminationCharacter) && (!tokenizer.nextToken().isSymbol(';'))) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
                    break;
                case NEW:
                    Token afterNextToken;

                    afterNextToken = tokenizer.nextToken();
                    if (afterNextToken.isSymbol('['))
                    {
                        tokenizer.returnToken(afterNextToken);
                        tokenizer.returnToken(nextToken);
                        newStatement = NEWCollectionStatement.parseStatement(tokenizer, parentExecutionUnit);
                        break;
                    }
                    else
                    {
                        tokenizer.returnToken(afterNextToken);
                        tokenizer.returnToken(nextToken);
                        newStatement = NEWObjectStatement.parseStatement(tokenizer, parentExecutionUnit);
                        break;
                    }
                case TRY:
                    tokenizer.returnToken(nextToken);
                    newStatement = TRYStatement.parseStatement(tokenizer, parentExecutionUnit);
                    break;
                case THROW:
                    tokenizer.returnToken(nextToken);
                    newStatement = THROWStatement.parseStatement(tokenizer, parentExecutionUnit);
                    if ((checkTerminationCharacter) && (!tokenizer.nextToken().isSymbol(';'))) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
                    break;
                case INT:
                    tokenizer.returnToken(nextToken);
                    newStatement = INTEGERStatement.parseStatement(tokenizer, parentExecutionUnit);
                    break;
                case LONG:
                    tokenizer.returnToken(nextToken);
                    newStatement = LONGStatement.parseStatement(tokenizer, parentExecutionUnit);
                    break;
                case FLOAT:
                    tokenizer.returnToken(nextToken);
                    newStatement = FLOATStatement.parseStatement(tokenizer, parentExecutionUnit);
                    break;
                case DOUBLE:
                    tokenizer.returnToken(nextToken);
                    newStatement = DOUBLEStatement.parseStatement(tokenizer, parentExecutionUnit);
                    break;
                case PRINT:
                    tokenizer.returnToken(nextToken);
                    newStatement = PRINTStatement.parseStatement(tokenizer, parentExecutionUnit);
                    if ((checkTerminationCharacter) && (!tokenizer.nextToken().isSymbol(';'))) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
                    break;
                case PRINTLN:
                    tokenizer.returnToken(nextToken);
                    newStatement = PRINTLNStatement.parseStatement(tokenizer, parentExecutionUnit);
                    if ((checkTerminationCharacter) && (!tokenizer.nextToken().isSymbol(';'))) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
                    break;
                case PARSE:
                    tokenizer.returnToken(nextToken);
                    newStatement = PARSEStatement.parseStatement(tokenizer, parentExecutionUnit);
                    if ((checkTerminationCharacter) && (!tokenizer.nextToken().isSymbol(';'))) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
                    break;
                default:
                    throw new EPICSyntaxException("Invalid keyword.", tokenizer.getLineNumber());
            }
        }
        else if (nextToken.getType() == Token.PROCEDURE)
        {
            tokenizer.returnToken(nextToken);
            newStatement = CALLStatement.parseStatement(tokenizer, parentExecutionUnit);
        }
        else if (nextToken.getType() == Token.VARIABLE)
        {
            if (parentExecutionUnit instanceof IDENTIFIERStatement) // Field Statement.
            {
                tokenizer.returnToken(nextToken);
                newStatement = FIELDStatement.parseStatement(tokenizer, parentExecutionUnit);
            }
            else // Variable assignment statement.
            {
                tokenizer.returnToken(nextToken);
                newStatement = VARIABLEStatement.parseStatement(tokenizer, parentExecutionUnit);
            }
        }
        else if (nextToken.getType() == Token.STRING)
        {
            tokenizer.returnToken(nextToken);
            newStatement = CONSTStatement.parseStatement(tokenizer, parentExecutionUnit);
        }
        else if (nextToken.getType() == Token.COLLECTION)
        {
            tokenizer.returnToken(nextToken);
            newStatement = COLLECTIONStatement.parseStatement(tokenizer, parentExecutionUnit);
        }
        else if (nextToken.getType() == Token.IDENTIFIER)
        {
            if (parentExecutionUnit instanceof IDENTIFIERStatement) // Field Statement.
            {
                tokenizer.returnToken(nextToken);
                newStatement = FIELDStatement.parseStatement(tokenizer, parentExecutionUnit);
            }
            else // Identifier statement.
            {
                tokenizer.returnToken(nextToken);
                newStatement = IDENTIFIERStatement.parseStatement(tokenizer, parentExecutionUnit, false, true);
            }
        }
        else throw new EPICSyntaxException("Invalid start of statement: " + nextToken, tokenizer.getLineNumber());

        // Check for chained statements or assignments.
        nextToken = tokenizer.nextToken();
        if (nextToken.isSymbol('.'))
        {
            nextToken = tokenizer.nextToken();
            if ((nextToken.isVariable()) || (nextToken.isIdentifier()) || (nextToken.isProcedureName()))
            {
                tokenizer.returnToken(nextToken);
                newStatement.setChainedStatement(StatementParser.parseStatement(tokenizer, newStatement, checkTerminationCharacter));
                return newStatement;
            }
            else throw new EPICSyntaxException("Unexpected token after '.' operator: " + nextToken, tokenizer.getLineNumber());
        }
        else if (nextToken.isSymbol('='))
        {
            // Property assignment code to be added here.
            return newStatement;
        }
        else if (nextToken.isSymbol(';'))
        {
            if (!checkTerminationCharacter) tokenizer.returnToken(nextToken);
            return newStatement;
        }
        else
        {
            tokenizer.returnToken(nextToken);
            return newStatement;
        }
    }
}
