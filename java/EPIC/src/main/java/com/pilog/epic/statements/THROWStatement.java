package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class THROWStatement extends Statement
{
    private Expression exceptionExpression;

    private THROWStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public Expression getExceptionExpression()
    {
        return exceptionExpression;
    }

    public void setExceptionExpression(Expression exceptionExpression)
    {
        this.exceptionExpression = exceptionExpression;
    }

    public static THROWStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        THROWStatement throwStatement;
        Token nextToken;

        throwStatement = new THROWStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.THROW))
        {
            Expression expression;

            expression = ExpressionParser.parseExpression(tokenizer, throwStatement);
            throwStatement.setExceptionExpression(expression);
            return throwStatement;
        }
        else throw new EPICSyntaxException("Invalid start of return statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Object exceptionObject;

        exceptionObject = exceptionExpression.evaluate(parentExecutionUnit, contextObject);
        if (exceptionObject instanceof Exception)
        {
            throw new EPICRuntimeException("", (Exception)exceptionObject, lineNumber);
        }
        else if (exceptionObject instanceof String)
        {
            throw new EPICRuntimeException((String)exceptionObject, null, lineNumber);
        }
        else throw new EPICRuntimeException("Invalid exception type: " + (exceptionObject != null ? exceptionObject.getClass().getCanonicalName() : null), null, lineNumber);
    }
}
