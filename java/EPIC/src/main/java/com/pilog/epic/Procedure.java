package com.pilog.epic;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.statements.VARStatement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class Procedure extends ExecutionUnit
{
    private String procedureName;
    private ScopeBlock procedureBody;
    private ArrayList<VARStatement> parameterDeclarations;
    private ArrayList<Object> parameterValues;

    public Procedure()
    {
        super(null);
        parameterDeclarations = new ArrayList<VARStatement>();
    }

    public void setParentProgram(Program parentProgram)
    {
        parentExecutionUnit = parentProgram;
    }

    public void addParameterDeclaration(VARStatement parameterDeclaration)
    {
        parameterDeclarations.add(parameterDeclaration);
    }

    public ArrayList<Object> getParameterValues()
    {
        return parameterValues;
    }

    public void setParameterValues(ArrayList<Object> parameterValues)
    {
        this.parameterValues = parameterValues;
    }

    public ScopeBlock getProcedureBody()
    {
        return procedureBody;
    }

    public void setProcedureBody(ScopeBlock procedureBody)
    {
        this.procedureBody = procedureBody;
    }

    public String getProcedureName()
    {
        return procedureName;
    }

    public void setProcedureName(String procedureName)
    {
        this.procedureName = procedureName;
    }

    @Override
    public List<ExecutionUnit> getDescendantExecutionUnits()
    {
        List<ExecutionUnit> descendants;

        descendants = new ArrayList<ExecutionUnit>();
        if (procedureBody != null)
        {
            descendants.add(procedureBody);
            descendants.addAll(procedureBody.getDescendantExecutionUnits());
        }

        return descendants;
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        // Declare all procedure parameters and set all parameter values.
        variables.clear();
        for (int parameterIndex = 0; parameterIndex < parameterDeclarations.size(); parameterIndex++)
        {
            VARStatement parameterDeclaration;

            parameterDeclaration = parameterDeclarations.get(parameterIndex);
            parameterDeclaration.execute(contextObject);
            variables.put(parameterDeclaration.getVariableName(), parameterValues.get(parameterIndex));
        }

        // Execute the procedure body.
        procedureBody.execute(contextObject);

        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
