package com.pilog.epic;

import com.pilog.epic.exceptions.EPICRuntimeException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class ExtensionMethodStub implements Serializable
{
    private String methodName;
    private Class<?> presetClassReference;
    private transient Method cachedMethod;
    private Object presetContextObject;

    public ExtensionMethodStub(Class<?> classReference, String methodName)
    {
        this.presetClassReference = classReference;
        this.methodName = methodName;
    }

    public ExtensionMethodStub(Object contextObject, Method method)
    {
        this.presetContextObject = contextObject;
        this.presetClassReference = method.getDeclaringClass();
        this.cachedMethod = method;
        this.methodName = method.getName();
    }

    public ExtensionMethodStub(String methodName)
    {
        this.methodName = methodName;
    }

    public Class<?> getClassReference()
    {
        return presetClassReference;
    }

    public void setClassReference(Class<?> classReference)
    {
        this.presetClassReference = classReference;
    }

    public String getMethodName()
    {
        return methodName;
    }

    public void setMethodName(String methodName)
    {
        this.methodName = methodName;
    }

    private Method getMethod(Class<?> contextClass, Class<?>[] parameterTypes) throws NoSuchMethodException
    {
        // If the already cache method is applicable, return it.
        if (cachedMethod != null)
        {
            if (cachedMethod.getClass().isAssignableFrom(contextClass))
            {
                return cachedMethod;
            }
        }

        // In order to allow for polymorphism, first try to find an applicable method one of the implemented interfaces.
        for (Class<?> classInterface : contextClass.getInterfaces())
        {
            cachedMethod = ReflectionTools.getBestCandidate(parameterTypes, methodName, classInterface, classInterface.getDeclaredMethods(), true);
            if (cachedMethod != null) return cachedMethod;
        }

        // The method we need is not one of those specified by an interface, so let's try to find it in the class hierarchy.
        if (Modifier.isPublic(contextClass.getModifiers()))
        {
            Class<?> superClass;

            superClass = contextClass;
            while ((cachedMethod == null) && (superClass != null))
            {
                cachedMethod = ReflectionTools.getBestCandidate(parameterTypes, methodName, superClass, superClass.getDeclaredMethods(), true);
                superClass = superClass.getSuperclass();
            }

            // If we found a method, return it.
            if (cachedMethod != null) return cachedMethod;
        }
        else // The implementing class is not accessible (not public), so try to find an applicable interface.
        {
            Class<?> superClass;

            // If the method was not found in one of the interfaces and the class is not public so it could be an abstract extension.
            // In this case we iterate up the inheritence hierarchy.
            superClass = contextClass.getSuperclass();
            if (superClass != null) return getMethod(superClass, parameterTypes);
        }

        // We could not find a matching method anywhere.
        throw new NoSuchMethodException("Method " + contextClass.getCanonicalName() + "." + methodName + "(" + getParamterTypeString(parameterTypes) + ") not found.");
    }

    public Object executeMethod(Object contextObject, ArrayList<Object> parameters) throws EPICRuntimeException
    {
        try
        {
            if (presetContextObject != null)
            {
                // In this case, both the context object and the target method was supplied to this instance during construction.
                if (cachedMethod.isVarArgs()) return cachedMethod.invoke(presetContextObject, getVarArgParams(cachedMethod, parameters).toArray());
                else return cachedMethod.invoke(presetContextObject, parameters.toArray());
            }
            else if (presetClassReference != null)
            {
                Method method;

                // In this case, the class reference and method name was supplied to this instance during construction.
                method = getMethod(presetClassReference, getParameterTypes(parameters));
                if (method.isVarArgs()) return method.invoke(contextObject, getVarArgParams(method, parameters).toArray());
                else return method.invoke(contextObject, parameters.toArray());
            }
            else
            {
                Method method;

                // In this case, only a method name was supplied to this instance during construction and we therefore have to use the current context object to find it.
                method = getMethod(contextObject.getClass(), getParameterTypes(parameters));
                if (method.isVarArgs()) return method.invoke(contextObject, getVarArgParams(method, parameters).toArray());
                else return method.invoke(contextObject, parameters.toArray());
            }
        }
        catch (NoSuchMethodException | EPICRuntimeException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
        {
            throw new EPICRuntimeException("Method '" + methodName + "' failed using context: " + presetClassReference, e, -1);
        }
    }

    public ArrayList<Object> getVarArgParams(Method method, ArrayList<Object> parameters) throws EPICRuntimeException
    {
        boolean firstRun;
        int varargIndex;
        ArrayList<Object> varargs;
        ArrayList<Object> parametersCopy;

        //This method has a varargs parameter, so package every parameter from the incoming array into a new one based on the varargs index
        varargs = new ArrayList<>();
        parametersCopy = new ArrayList<>(parameters);
        varargIndex = method.getParameterTypes().length - 1;
        firstRun = true;

        while(parametersCopy.size() > varargIndex)
        {
            Object parameter;

            parameter = parametersCopy.remove(varargIndex);

            //Check if the last parameter is an array object, then it must be passed as seperate parameters to the varargs method
            if(firstRun && parameter instanceof Object[])
            {
                Object[] arrayObjects;

                arrayObjects = (Object[])parameter;
                for (Object object : arrayObjects)
                {
                    varargs.add(object);
                }
            } else varargs.add(parameter);

            firstRun = false;
        }

        //Add the varargs parameters back to the original parameter list as an array
        parametersCopy.add(varargs.toArray());

        return parametersCopy;
    }

    private static String getParamterTypeString(Class<?>[] parameterTypes)
    {
        if ((parameterTypes == null) || (parameterTypes.length == 0))
        {
            return "";
        }
        else
        {
            StringBuffer typeString;

            typeString = new StringBuffer();
            for (int typeIndex = 0; typeIndex < parameterTypes.length; typeIndex++)
            {
                typeString.append(parameterTypes[typeIndex].getCanonicalName());
                if (typeIndex < parameterTypes.length -1) typeString.append(", ");
            }

            return typeString.toString();
        }
    }

    private static Class<?>[] getParameterTypes(ArrayList<Object> parameters)
    {
        Class<?>[] parameterTypes;

        parameterTypes = new Class<?>[parameters.size()];
        for (int parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex++)
        {
            Object parameter;

            parameter = parameters.get(parameterIndex);
            parameterTypes[parameterIndex] = parameter != null ? parameter.getClass() : Object.class;
        }

        return parameterTypes;
    }
}
