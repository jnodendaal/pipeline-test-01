package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class FIELDStatement extends Statement
{
    private String fieldName;
    private Expression fieldValueExpression;

    private transient boolean fieldCached = false;
    private transient Field cachedField;

    private FIELDStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public String getFieldName()
    {
        return fieldName;
    }

    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }

    public Expression getFieldValueExpression()
    {
        return fieldValueExpression;
    }

    public void setFieldValueExpression(Expression fieldValueExpression)
    {
        this.fieldValueExpression = fieldValueExpression;
    }

    public static FIELDStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        FIELDStatement newStatement;
        Token nextToken;

        newStatement = new FIELDStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if ((nextToken.isVariable()) || (nextToken.isIdentifier()))
        {
            String fieldName;

            fieldName = (String)nextToken.getValue();
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('='))
            {
                Expression variableValueExpression;

                variableValueExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                newStatement.setFieldName(fieldName);
                newStatement.setFieldValueExpression(variableValueExpression);
                return newStatement;
            }
            else
            {
                tokenizer.returnToken(nextToken);
                newStatement.setFieldName(fieldName);
                return newStatement;
            }
        }
        else throw new EPICSyntaxException("Invalid field name.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        // We have to check for array[].length because it is not a normal field and needs to be resolved manually.
        if ((contextObject != null) && (contextObject.getClass().isArray()) && (fieldName.equals("length")))
        {
            returnObject = Array.getLength(contextObject);
        }
        else
        {
            // If the field has not already been resolved, resolve it now.
            if (!fieldCached)
            {
                try
                {
                    fieldCached = true;
                    cachedField = (contextObject instanceof Class) ? ((Class)contextObject).getField(fieldName) : contextObject.getClass().getField(fieldName);
                }
                catch (Exception e)
                {
                    throw new EPICRuntimeException("Field '" + fieldName + "' could not be resolved using context object: " + contextObject + ".", e, lineNumber);
                }
            }

            // Get the value of the field from the context object.
            try
            {
                returnObject = cachedField.get(null);
            }
            catch (Exception e)
            {
                throw new EPICRuntimeException("Field '" + fieldName + "' could not be successfully accessed on context object: " + contextObject + ".", e, lineNumber);
            }
        }

        // Execute chained statement if one is present.
        if (chainedStatement != null)
        {
            return chainedStatement.execute(returnObject);
        }
        else return Statement.RETURN_CODE_COMPLETED;
    }
}
