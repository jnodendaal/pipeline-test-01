package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;

/**
 * @author Bouwer du Preez
 */
public class BREAKStatement extends Statement
{
    private BREAKStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public static BREAKStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        BREAKStatement newStatement;
        Token nextToken;

        newStatement = new BREAKStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.BREAK))
        {
            return newStatement;
        }
        else throw new EPICSyntaxException("Invalid start of break statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        return ExecutionUnit.RETURN_CODE_BREAK;
    }
}
