package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.ExternalExpressionParser;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.Variable;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.statements.Statement;
import com.pilog.epic.statements.StatementParser;

/**
 * This class implements a recursive-descent parser for the parseExpression grammar.
 *
 * The symbols of the grammar are defined in such a way that operator precedence
 * can be embedded into the grammar itself.  The grammar is defined as follows:
 *
 * <ELEMENT>    ::= ( <EXPRESSION> ) | function
 * <PRIMARY>    ::= - <ELEMENT> | ! <ELEMENT> | "NOT" <ELEMENT> | <ELEMENT>
 * <FACTOR>     ::= <PRIMARY> "**" <FACTOR> | <PRIMARY>
 * <TERM>       ::= <TERM> "*" <FACTOR> | <TERM> "/" <FACTOR> | <FACTOR> "->" <FACTOR> | <FACTOR>
 * <SUM>        ::= <SUM> "+" <TERM> | <SUM> "-" <TERM> | <TERM>
 * <LOGIC>      ::= <LOGIC> "&" <SUM> | <LOGIC> "^" <SUM> | <LOGIC> "|" <SUM> | <SUM>
 * <RELATION>   ::= <LOGIC> "=" <LOGIC>  | <LOGIC> "<" <LOGIC> | <LOGIC> ">" <LOGIC> | <LOGIC> "<>" <LOGIC> | <LOGIC> "<=" <LOGIC> | <LOGIC> ">=" <LOGIC> | <LOGIC>
 * <EXPRESSION> ::= <RELATION> "&&" <RELATION> | <RELATION> "||" <RELATION> | <RELATION> "^^" <RELATION> | <RELATION> | <EXPRESSION> ? <EXPRESSION> : <EXPRESSION>
 *
 * Precedence rules from lowest to highest priority:
 *  0.  &&, ||, ^^, ? :
 *  1.  =, < , <=, >, >=
 *  2.  &, |, ^
 *  3.  +, -
 *  4.  *, /, ->
 *  5.  **, ->
 *  6.  unary -, unary !, unary .NOT.
 *
 * @author Bouwer du Preez
 */
public class ExpressionParser extends DefaultExpression
{
    protected static Expression element(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Token nextToken;

        nextToken = tokenizer.nextToken();
        if (nextToken.isSymbol('('))
        {
            Expression result;

            result = parseExpression(tokenizer, parentExecutionUnit);
            nextToken = tokenizer.nextToken();
            if (!nextToken.isSymbol(')'))
            {
                tokenizer.returnToken(nextToken);
                throw new EPICSyntaxException("Mismatched parenthesis in expression.", tokenizer.getLineNumber());
            }
            else
            {
                return result;
            }
        }
        else
        {
            if (nextToken.getType() == Token.CONSTANT)
            {
                return new ConstantExpression(nextToken.getValue());
            }
            else if (nextToken.getType() == Token.STRING)
            {
                Token followingToken;

                followingToken = tokenizer.nextToken();
                if (followingToken.isSymbol('.')) // This will occur when a method is invoked on a String literal e.g. "myString".equals("mySecondString");
                {
                    tokenizer.returnToken(followingToken);
                    tokenizer.returnToken(nextToken);
                    return new StatementExpression(StatementParser.parseStatement(tokenizer, parentExecutionUnit, false));
                }
                else
                {
                    tokenizer.returnToken(followingToken);
                    return new ConstantExpression(nextToken.getValue());
                }
            }
            else if (nextToken.getType() == Token.CHARACTER)
            {
                return new CharacterExpression((char)nextToken.getValue());
            }
            else if (nextToken.getType() == Token.VARIABLE)
            {
                String variableName;
                Variable variable;

                variable = (Variable)nextToken;
                variableName = variable.getVariableName();
                if ((!tokenizer.isCheckVariableDeclaration()) || (parentExecutionUnit.isVariableDeclared(variableName)))
                {
                    parentExecutionUnit.addVariableName(variable.getVariableName());
                    return new VariableExpression(variable);
                }
                else throw new EPICSyntaxException("Undeclared variable: " + variableName, tokenizer.getLineNumber());
            }
            else if (nextToken.getType() == Token.PROCEDURE)
            {
                tokenizer.returnToken(nextToken);
                return new StatementExpression(StatementParser.parseStatement(tokenizer, parentExecutionUnit, false));
            }
            else if (nextToken.getType() == Token.COLLECTION)
            {
                tokenizer.returnToken(nextToken);
                return CollectionValueExpression.parse(tokenizer, parentExecutionUnit);
            }
            else if (nextToken.getType() == Token.IDENTIFIER)
            {
                tokenizer.returnToken(nextToken);
                return new StatementExpression(StatementParser.parseStatement(tokenizer, parentExecutionUnit, false));
            }
            else if (nextToken.isKeyword(Statement.NEW))
            {
                tokenizer.returnToken(nextToken);
                return new StatementExpression(StatementParser.parseStatement(tokenizer, parentExecutionUnit, false));
            }
            else if (nextToken.getType() == Token.FUNCTION)
            {
                return FunctionExpression.parse((Integer)nextToken.getValue(), tokenizer, parentExecutionUnit);
            }
            else if (nextToken.getType() == Token.KEYWORD)
            {
                tokenizer.returnToken(nextToken);
                return new StatementExpression(StatementParser.parseStatement(tokenizer, parentExecutionUnit, false));
            }
            else if (nextToken.isExternalExpression())
            {
                Token followingToken;

                followingToken = tokenizer.nextToken();
                if (followingToken.isSymbol('.')) // This will occur when a method is invoked on a String literal e.g. "myString".equals("mySecondString");
                {
                    tokenizer.returnToken(followingToken);
                    tokenizer.returnToken(nextToken);
                    return new StatementExpression(StatementParser.parseStatement(tokenizer, parentExecutionUnit, false));
                }
                else
                {
                    ExternalExpressionParser parser;
                    String externalExpression;

                    tokenizer.returnToken(followingToken);
                    externalExpression = (String)nextToken.getValue();
                    parser = tokenizer.getExternalExpressionEvaluator(externalExpression);
                    if (parser != null)
                    {
                        return parser.parse(externalExpression);
                    }
                    else throw new EPICSyntaxException("No applicable parser found for external expression: " + externalExpression, tokenizer.getLineNumber());
                }
            }
            else
            {
                throw new EPICSyntaxException("Unexpected symbol in expression: " + nextToken, tokenizer.getLineNumber());
            }
        }
    }

    protected static Expression primary(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Token nextToken;

        nextToken = tokenizer.nextToken();
        if (nextToken.isOperator(OP_NOT))
        {
            return new DefaultExpression(OP_NOT, primary(tokenizer, parentExecutionUnit));
        }
        else if (nextToken.isOperator(OP_SUB))
        {
            return new DefaultExpression(OP_NEG, primary(tokenizer, parentExecutionUnit));
        }
        else if (nextToken.isOperator(OP_BNOT))
        {
            return new BooleanExpression(OP_BNOT, primary(tokenizer, parentExecutionUnit));
        }
        else
        {
            tokenizer.returnToken(nextToken);
            return element(tokenizer, parentExecutionUnit);
        }
    }

    protected static Expression factor(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Expression result;
        Token nextToken;

        result = primary(tokenizer, parentExecutionUnit);
        nextToken = tokenizer.nextToken();
        if (nextToken.isOperator(OP_EXP))
        {
            return new DefaultExpression(OP_EXP, result, factor(tokenizer, parentExecutionUnit));
        }
        else
        {
            tokenizer.returnToken(nextToken);
            return result;
        }
    }

    protected static Expression term(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Expression result;
        Token nextToken;

        result = factor(tokenizer, parentExecutionUnit);
        while (true)
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isOperator(OP_MUL))
            {
                result = new DefaultExpression(OP_MUL, result, factor(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_DIV))
            {
                result = new DefaultExpression(OP_DIV, result, factor(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_MOD))
            {
                result = new DefaultExpression(OP_MOD, result, factor(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_CAC))
            {
                result = new DefaultExpression(OP_CAC, result, factor(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isSymbol('['))
            {
                result = new DefaultExpression(OP_CAC, result, factor(tokenizer, parentExecutionUnit));
                nextToken = tokenizer.nextToken();
                if (!nextToken.isSymbol(']')) throw new EPICSyntaxException("Missing ']' in collection value expression.", tokenizer.getLineNumber());
            }
            else
            {
                tokenizer.returnToken(nextToken);
                break;
            }
        }

        return result;
    }

    protected static Expression sum(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Expression result;
        Token nextToken;

        result = term(tokenizer, parentExecutionUnit);
        while (true)
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isOperator(OP_ADD))
            {
                result = new DefaultExpression(OP_ADD, result, term(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_SUB))
            {
                result = new DefaultExpression(OP_SUB, result, term(tokenizer, parentExecutionUnit));
            }
            else
            {
                tokenizer.returnToken(nextToken);
                break;
            }
        }

        return result;
    }

    protected static Expression logic(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Expression result;
        Token nextToken;

        result = sum(tokenizer, parentExecutionUnit);
        while (true)
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isOperator(OP_AND))
            {
                result = new DefaultExpression(OP_AND, result, sum(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_XOR))
            {
                result = new DefaultExpression(OP_XOR, result, sum(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_IOR))
            {
                result = new DefaultExpression(OP_IOR, result, sum(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_INSTANCEOF))
            {
                result = new DefaultExpression(OP_INSTANCEOF, result, sum(tokenizer, parentExecutionUnit));
            }
            else
            {
                tokenizer.returnToken(nextToken);
                break;
            }
        }

        return result;
    }

    protected static Expression relation(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Expression result;
        Token nextToken;

        result = logic(tokenizer, parentExecutionUnit);
        nextToken = tokenizer.nextToken();
        if (nextToken.getType() != Token.OPERATOR)
        {
            tokenizer.returnToken(nextToken);
            return result;
        }

        switch (((Number)nextToken.getValue()).intValue())
        {
            case OP_EQ:
            {
                result = new BooleanExpression(OP_EQ, result, logic(tokenizer, parentExecutionUnit));
                break;
            }
            case OP_NE:
            {
                result = new BooleanExpression(OP_NE, result, logic(tokenizer, parentExecutionUnit));
                break;
            }
            case OP_LT:
            {
                result = new BooleanExpression(OP_LT, result, logic(tokenizer, parentExecutionUnit));
                break;
            }
            case OP_LE:
            {
                result = new BooleanExpression(OP_LE, result, logic(tokenizer, parentExecutionUnit));
                break;
            }
            case OP_GT:
            {
                result = new BooleanExpression(OP_GT, result, logic(tokenizer, parentExecutionUnit));
                break;
            }
            case OP_GE:
            {
                result = new BooleanExpression(OP_GE, result, logic(tokenizer, parentExecutionUnit));
                break;
            }
            default:
            {
                tokenizer.returnToken(nextToken);
                break;
            }
        }

        return result;
    }

    public static Expression parseExpression(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Expression result;
        Token nextToken;
        int lineNumber;

        // Set the line number of the parseExpression according to the current location of the tokenizer.
        lineNumber = tokenizer.getLineNumber();

        // Parse the parseExpression.
        result = relation(tokenizer, parentExecutionUnit);
        while (true)
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isOperator(OP_BAND))
            {
                result = new BooleanExpression(OP_BAND, result, relation(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_BIOR))
            {
                result = new BooleanExpression(OP_BIOR, result, relation(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_BXOR))
            {
                result = new BooleanExpression(OP_BXOR, result, relation(tokenizer, parentExecutionUnit));
            }
            else if (nextToken.isOperator(OP_CIF))
            {
                Expression trueValueExpression;

                trueValueExpression = parseExpression(tokenizer, parentExecutionUnit);
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(':'))
                {
                    Expression falseValueExpression;

                    falseValueExpression = parseExpression(tokenizer, parentExecutionUnit);
                    result = new ConditionExpression(result, trueValueExpression, falseValueExpression);
                    break;
                }
                else throw new EPICSyntaxException("Conditional '?' operator found, without matching ':' operator." + nextToken, tokenizer.getLineNumber());
            }
            else if (nextToken.isOperator(OP_COA))
            {
                Expression defaultExpression;

                defaultExpression = parseExpression(tokenizer, parentExecutionUnit);
                result = new CoalesceExpression(result, defaultExpression);
            }
            else
            {
                tokenizer.returnToken(nextToken);
                break;
            }
        }

        // Set the line number of the newly parsed parseExpression.
        //result.setLineNumber(lineNumber);
        return result;
    }
}
