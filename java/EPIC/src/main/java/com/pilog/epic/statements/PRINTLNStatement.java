package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class PRINTLNStatement extends Statement
{
    private Expression stringExpression;

    private PRINTLNStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public Expression getStringExpression()
    {
        return stringExpression;
    }

    public void setStringExpression(Expression stringExpression)
    {
        this.stringExpression = stringExpression;
    }

    public static PRINTLNStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        PRINTLNStatement newStatement;
        Token nextToken;

        newStatement = new PRINTLNStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.PRINTLN))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                Expression stringExpression;

                stringExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                newStatement.setStringExpression(stringExpression);

                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(')'))
                {
                    return newStatement;
                }
                else throw new EPICSyntaxException("Missing closing ')' in print statement.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("'(' character missing in print statement.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of print statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Object value;

        value = stringExpression.evaluate(parentExecutionUnit, contextObject);
        System.out.println(value == null ? "null" : value.toString());
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
