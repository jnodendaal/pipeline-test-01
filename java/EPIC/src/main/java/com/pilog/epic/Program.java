package com.pilog.epic;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.statements.IMPORTStatement;
import com.pilog.epic.statements.Statement;
import com.pilog.epic.statements.VARStatement;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class Program extends ExecutionUnit implements Serializable
{
    private ScopeBlock programBody;
    private String programName;
    private final HashMap<String, Class> classImports;
    private final ArrayList<VARStatement> parameterDeclarations;
    private final HashMap<String, Procedure> procedures;
    private final ArrayList<IMPORTStatement> importStatements;

    /**
     * These are all of the statement KEYWORDS we can parse.
     */
    public final static String keywords[] = {
        "*NONE*",   // 0.
        "program",       // 1.
        "procedure",       // 2.
    };

    // Statement type constants (should match the indexes of the above KEYWORDS).
    public final static int NONE = 0; // invalid statement
    public final static int PROGRAM = 1;
    public final static int PROCEDURE = 2;

    public Program()
    {
        super(null);
        classImports = new HashMap<String, Class>();
        parameterDeclarations = new ArrayList<VARStatement>();
        procedures = new HashMap<String, Procedure>();
        importStatements = new ArrayList<IMPORTStatement>();
    }

    public String getProgramName()
    {
        return programName;
    }

    public void setProgramName(String programName)
    {
        this.programName = programName;
    }

    public void addParameterDeclaration(VARStatement parameterDeclaration)
    {
        parameterDeclarations.add(parameterDeclaration);
    }

    public ScopeBlock getProgramBody()
    {
        return programBody;
    }

    public void setProgramBody(ScopeBlock programBody)
    {
        this.programBody = programBody;
    }

    @Override
    public List<ExecutionUnit> getDescendantExecutionUnits()
    {
        List<ExecutionUnit> descendants;

        descendants = new ArrayList<ExecutionUnit>();
        if (programBody != null)
        {
            descendants.add(programBody);
            descendants.addAll(programBody.getDescendantExecutionUnits());
        }

        for (Procedure procedure : procedures.values())
        {
            descendants.add(procedure);
            descendants.addAll(procedure.getDescendantExecutionUnits());
        }

        return descendants;
    }

    public void addImport(String procedureName, Class classReference, String methodName)
    {
        ExtensionProcedure extensionProcedure;

        extensionProcedure = new ExtensionProcedure(procedureName, new ExtensionMethodStub(classReference, methodName));
        addProcedure(extensionProcedure);
    }

    public void addImport(String procedureName, Object contextObject, Method method)
    {
        ExtensionProcedure extensionProcedure;

        extensionProcedure = new ExtensionProcedure(procedureName, new ExtensionMethodStub(contextObject, method));
        addProcedure(extensionProcedure);
    }

    public void addProcedure(Procedure procedure)
    {
        procedure.setParentProgram(this);
        procedures.put(procedure.getProcedureName(), procedure);
    }

    public void addClassImport(String className, Class declaredClass)
    {
        classImports.put(className, declaredClass);
    }

    /**
     * This method is used internally during execution of a program.
     * @param className The name of the class to declare.
     * @param declaredClass The class to declare.
     * @throws EPICRuntimeException
     */
    public void declareClassImport(String className, Class declaredClass) throws EPICRuntimeException
    {
        if (!classImports.containsKey(className))
        {
            classImports.put(className, declaredClass);
        }
        else throw new EPICRuntimeException("Invalid class import.  Class '" + className + "' is already imported.", -1);
    }

    @Override
    public boolean isClassImported(String className)
    {
        return classImports.containsKey(className);
    }

    public boolean containsClassImport(String className)
    {
        return classImports.containsKey(className);
    }

    @Override
    public Class getClassImport(String className) throws EPICRuntimeException
    {
        return classImports.get(className);
    }

    @Override
    public Procedure getProcedure(String procedureName) throws EPICRuntimeException
    {
        if (procedures.containsKey(procedureName))
        {
            return procedures.get(procedureName);
        }
        else return null;
    }

    public void addImportStatement(IMPORTStatement importStatement)
    {
        importStatements.add(importStatement);
    }

    /**
     * The main method that should be used when executing a compiled program.
     * @param parameters The program input parameters to use during execution.
     * @return The output object of the program.
     * @throws EPICRuntimeException
     */
    public int execute(Map<String, Object> parameters) throws EPICRuntimeException
    {
        return execute(null, parameters);
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        return execute(contextObject, null);
    }

    /**
     * The main method that should be used when executing a compiled program.
     * @param contextObject The default context for all methods that do not specify a context object.
     * @param parameters The program input parameters to use during execution.
     * @return The output object of the program.
     * @throws EPICRuntimeException
     */
    public int execute(Object contextObject, Map<String, Object> parameters) throws EPICRuntimeException
    {
        // Clear the variables collection.
        variables.clear();

        // Execute all import statements.
        for (IMPORTStatement importStatement : importStatements) importStatement.execute(contextObject);

        // Declare all procedure parameters and set all parameter values.
        for (int parameterIndex = 0; parameterIndex < parameterDeclarations.size(); parameterIndex++)
        {
            VARStatement parameterDeclaration;

            parameterDeclaration = parameterDeclarations.get(parameterIndex);
            parameterDeclaration.execute(contextObject);
            variables.put(parameterDeclaration.getVariableName(), parameters != null ? parameters.get(parameterDeclaration.getVariableName()) : null);
        }

        // Execute the program.
        programBody.execute(contextObject);
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    public static Program parseProgram(LexicalTokenizer tokenizer, ParserContext context) throws EPICSyntaxException
    {
        Program newProgram;
        Token nextToken;

        newProgram = new Program();

        // Set the parser context.
        if (context != null)
        {
            HashSet<String> variableDeclarations;
            List<ExtensionProcedure> methodImports;
            Map<String, Class> classImports;

            // Declare all of the variables that are used, so that they can be resolved during parsing.
            variableDeclarations = context.getVariableDeclarations();
            for (String variableName : variableDeclarations)
            {
                newProgram.addVariableDeclaration(variableName);
            }

            // Add all of the class imports available in the parser context.
            classImports = context.getClassImports();
            for (String classReference : classImports.keySet())
            {
                newProgram.addClassImport(classReference, classImports.get(classReference));
            }

            // Add all of the method imports available in the parser context.
            methodImports = context.getMethodImports();
            for (ExtensionProcedure importedProcedure : methodImports)
            {
                newProgram.addProcedure(importedProcedure);
            }

            // Add any available external parsers to the tokenizer.
            tokenizer.addExternalExpressionParsers(context.getExternalExpressionParsers());
        }

        // Parse import statements.
        while (!(nextToken = tokenizer.nextToken()).isKeyword(PROGRAM))
        {
            if (nextToken.isKeyword(Statement.IMPORT))
            {
                tokenizer.returnToken(nextToken);
                newProgram.addImportStatement(IMPORTStatement.parseStatement(tokenizer, newProgram));
                nextToken = tokenizer.nextToken();
                if (!nextToken.isSymbol(';')) throw new EPICSyntaxException("Missing ';' character.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("Invalid statement.  Main expected. " + nextToken, tokenizer.getLineNumber());
        }

        // Parse the main method.
        if (nextToken.isKeyword(PROGRAM))
        {

            nextToken = tokenizer.nextToken();
            if (nextToken.isProcedureName())
            {
                newProgram.setProgramName((String)nextToken.getValue());
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol('('))
                {
                    // Parse all the program parameter declarations.
                    while (!(nextToken = tokenizer.nextToken()).isSymbol(')'))
                    {
                        if (nextToken.isKeyword(Statement.VAR))
                        {
                            VARStatement variableDeclaration;

                            tokenizer.returnToken(nextToken);
                            variableDeclaration = VARStatement.parseStatement(tokenizer, newProgram);
                            newProgram.addParameterDeclaration(variableDeclaration);

                            nextToken = tokenizer.nextToken();
                            if (nextToken.isSymbol(','))
                            {
                                nextToken = tokenizer.nextToken();
                                if (nextToken.isKeyword(Statement.VAR))
                                {
                                    tokenizer.returnToken(nextToken);
                                }
                                else throw new EPICSyntaxException("Missing parameter declaration.", tokenizer.getLineNumber());
                            }
                            else
                            {
                                tokenizer.returnToken(nextToken);
                            }
                        }
                        else throw new EPICSyntaxException("Invalid character in procedure delcaration: " + nextToken, tokenizer.getLineNumber());
                    }

                    // Parse the program body.
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol('{'))
                    {
                        tokenizer.returnToken(nextToken);
                        newProgram.setProgramBody(ScopeBlock.parseScopeBlock(tokenizer, newProgram));

                        // Parse all procedures.
                        while ((nextToken = tokenizer.nextToken()).isKeyword(PROCEDURE))
                        {
                            Procedure newProcedure;

                            tokenizer.returnToken(nextToken);
                            newProcedure = parseProcedure(tokenizer, newProgram);
                            newProgram.addProcedure(newProcedure);
                        }

                        return newProgram;
                    }
                    else throw new EPICSyntaxException("Missing program body." + nextToken, tokenizer.getLineNumber());
                }
                else throw new EPICSyntaxException("Missing '(' character in procedure declaration.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("Missing program name.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of Program.", tokenizer.getLineNumber());
    }

    private static Procedure parseProcedure(LexicalTokenizer tokenizer, Program parentProgram) throws EPICSyntaxException
    {
        Token nextToken;

        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(PROCEDURE))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isProcedureName())
            {
                Procedure newProcedure;

                newProcedure = new Procedure();
                newProcedure.setParentProgram(parentProgram);
                newProcedure.setProcedureName((String)nextToken.getValue());
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol('('))
                {
                    // Parse all the procedure parameter declarations.
                    while (!(nextToken = tokenizer.nextToken()).isSymbol(')'))
                    {
                        if (nextToken.isKeyword(Statement.VAR))
                        {
                            VARStatement variableDeclaration;

                            tokenizer.returnToken(nextToken);
                            variableDeclaration = VARStatement.parseStatement(tokenizer, newProcedure);
                            newProcedure.addParameterDeclaration(variableDeclaration);

                            nextToken = tokenizer.nextToken();
                            if (nextToken.isSymbol(','))
                            {
                                nextToken = tokenizer.nextToken();
                                if (nextToken.isKeyword(Statement.VAR))
                                {
                                    tokenizer.returnToken(nextToken);
                                }
                                else throw new EPICSyntaxException("Missing parameter declaration.", tokenizer.getLineNumber());
                            }
                            else
                            {
                                tokenizer.returnToken(nextToken);
                            }
                        }
                        else throw new EPICSyntaxException("Invalid character in procedure delcaration: " + nextToken, tokenizer.getLineNumber());
                    }

                    // Parse the procedure body.
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol('{'))
                    {
                        tokenizer.returnToken(nextToken);
                        newProcedure.setProcedureBody(ScopeBlock.parseScopeBlock(tokenizer, newProcedure));
                        return newProcedure;
                    }
                    else throw new EPICSyntaxException("Missing procedure body." + nextToken, tokenizer.getLineNumber());
                }
                else throw new EPICSyntaxException("Missing '(' character in procedure declaration.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("Missing procedure name.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of Procedure.", tokenizer.getLineNumber());
    }
}
