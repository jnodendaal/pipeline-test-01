/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.epic.rsyntax;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class BasicEpicCompletion
{
    private String replacementText;
    private String shortDesc;
    private String summary;

    /**
     * Constructor.
     *
     * @param provider        The parent completion provider.
     * @param replacementText The text to replace.
     */
    public BasicEpicCompletion(String replacementText)
    {
        this(replacementText, null);
    }

    /**
     * Constructor.
     *
     * @param provider        The parent completion provider.
     * @param replacementText The text to replace.
     * @param shortDesc       A short description of the completion. This will be
     *                        displayed in the completion list. This may be <code>null</code>.
     */
    public BasicEpicCompletion(String replacementText,
                           String shortDesc)
    {
        this(replacementText, shortDesc, null);
    }

    /**
     * Constructor.
     *
     * @param provider        The parent completion provider.
     * @param replacementText The text to replace.
     * @param shortDesc       A short description of the completion. This will be
     *                        displayed in the completion list. This may be <code>null</code>.
     * @param summary         The summary of this completion. This should be HTML.
     *                        This may be <code>null</code>.
     */
    public BasicEpicCompletion(String replacementText,
                           String shortDesc, String summary)
    {
        this.replacementText = replacementText;
        this.shortDesc = shortDesc;
        this.summary = summary;
    }

    /**
     * {@inheritDoc}
     */
    public String getReplacementText()
    {
        return replacementText;
    }

    /**
     * Returns the short description of this completion, usually used in
     * the completion choices list.
     *
     * @return The short description, or <code>null</code> if there is none.
     * <p/>
     * @see #setShortDescription(String)
     */
    public String getShortDescription()
    {
        return shortDesc;
    }

    /**
     * {@inheritDoc}
     */
    public String getSummary()
    {
        return summary;
    }

    /**
     * Sets the short description of this completion.
     *
     * @param shortDesc The short description of this completion.
     * <p/>
     * @see #getShortDescription()
     */
    public void setShortDescription(String shortDesc)
    {
        this.shortDesc = shortDesc;
    }

    /**
     * Sets the summary for this completion.
     *
     * @param summary The summary for this completion.
     * <p/>
     * @see #getSummary()
     */
    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    /**
     * Returns a string representation of this completion. If the short
     * description is not
     * <code>null</code>, this method will return:
     * <p/>
     * <code>getInputText() + " - " + shortDesc</code>
     * <p/>
     * otherwise, it will return <tt>getInputText()</tt>.
     *
     * @return A string representation of this completion.
     */
    @Override
    public String toString()
    {
        if (shortDesc == null)
        {
            return replacementText;
        }
        return replacementText + " - " + shortDesc;
    }
}
