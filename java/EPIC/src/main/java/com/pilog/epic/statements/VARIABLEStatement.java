package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.Variable;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.DefaultExpression;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class VARIABLEStatement extends Statement
{
    private String variableName;
    private Expression variableValueExpression;
    private int type;

    public static final int TYPE_ASSIGNMENT = 0;
    public static final int TYPE_INCREMENTER = 1;
    public static final int TYPE_DECREMENTER = 2;
    public static final int TYPE_ADD_ASSIGNMENT = 3;
    public static final int TYPE_SUBTRACT_ASSIGNMENT = 4;

    private VARIABLEStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public void setVariable(String variableName, Expression variableValueExpression, int type)
    {
        this.variableName = variableName;
        this.variableValueExpression = variableValueExpression;
        this.type = type;
    }

    public static VARIABLEStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        VARIABLEStatement newStatement;
        Token nextToken;

        newStatement = new VARIABLEStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.getType() == Token.VARIABLE)
        {
            String variableName;

            variableName = ((Variable)nextToken).getVariableName();
            if ((!tokenizer.isCheckVariableDeclaration()) || (parentExecutionUnit.isVariableDeclared(variableName)))
            {
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol('='))
                {
                    Expression variableValueExpression;

                    variableValueExpression = ExpressionParser.parseExpression(tokenizer, parentExecutionUnit);
                    newStatement.setVariable(variableName, variableValueExpression, VARIABLEStatement.TYPE_ASSIGNMENT);
                    return newStatement;
                }
                else if (nextToken.isOperator(DefaultExpression.OP_ADD))
                {
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isOperator(DefaultExpression.OP_ADD))
                    {
                        newStatement.setVariable(variableName, null, VARIABLEStatement.TYPE_INCREMENTER);
                        return newStatement;
                    }
                    else if (nextToken.isSymbol('='))
                    {
                        Expression variableValueExpression;

                        variableValueExpression = ExpressionParser.parseExpression(tokenizer, parentExecutionUnit);
                        newStatement.setVariable(variableName, variableValueExpression, VARIABLEStatement.TYPE_ADD_ASSIGNMENT);
                        return newStatement;
                    }
                    else throw new EPICSyntaxException("Invalid variable assignment.", tokenizer.getLineNumber());
                }
                else if (nextToken.isOperator(DefaultExpression.OP_SUB))
                {
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isOperator(DefaultExpression.OP_SUB))
                    {
                        newStatement.setVariable(variableName, null, VARIABLEStatement.TYPE_DECREMENTER);
                        return newStatement;
                    }
                    else if (nextToken.isSymbol('='))
                    {
                        Expression variableValueExpression;

                        variableValueExpression = ExpressionParser.parseExpression(tokenizer, parentExecutionUnit);
                        newStatement.setVariable(variableName, variableValueExpression, VARIABLEStatement.TYPE_SUBTRACT_ASSIGNMENT);
                        return newStatement;
                    }
                    else throw new EPICSyntaxException("Invalid variable assignment.", tokenizer.getLineNumber());
                }
                else throw new EPICSyntaxException("Invalid variable assignment.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("Undeclared variable: " + variableName, tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of variable assignment.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        if (type == VARIABLEStatement.TYPE_ASSIGNMENT)
        {
            parentExecutionUnit.setVariable(variableName, variableValueExpression.evaluate(parentExecutionUnit, contextObject));
            return ExecutionUnit.RETURN_CODE_COMPLETED;
        }
        else if (type == VARIABLEStatement.TYPE_INCREMENTER)
        {
            Number variableValue;

            variableValue = (Number)parentExecutionUnit.getVariable(variableName);
            parentExecutionUnit.setVariable(variableName, variableValue.intValue() + 1);
                return ExecutionUnit.RETURN_CODE_COMPLETED;
        }
        else if (type == VARIABLEStatement.TYPE_DECREMENTER)
        {
            Number variableValue;

            variableValue = (Number)parentExecutionUnit.getVariable(variableName);
            parentExecutionUnit.setVariable(variableName, variableValue.intValue() - 1);
            return ExecutionUnit.RETURN_CODE_COMPLETED;
        }
        else if (type == VARIABLEStatement.TYPE_ADD_ASSIGNMENT)
        {
            Object variableValue;
            Object expressionResult;

            expressionResult = variableValueExpression.evaluate(parentExecutionUnit, contextObject);
            variableValue = parentExecutionUnit.getVariable(variableName);
            if (variableValue instanceof Integer)
            {
                if (expressionResult != null)
                {
                    parentExecutionUnit.setVariable(variableName, (Integer)variableValue + ((Number)expressionResult).intValue());
                    return ExecutionUnit.RETURN_CODE_COMPLETED;
                }
                else throw new EPICRuntimeException("Cannot add null to integer value.", lineNumber);
            }
            else if (variableValue instanceof Double)
            {
                if (expressionResult != null)
                {
                    parentExecutionUnit.setVariable(variableName, (Double)variableValue + ((Number)expressionResult).doubleValue());
                    return ExecutionUnit.RETURN_CODE_COMPLETED;
                }
                else throw new EPICRuntimeException("Cannot add null to double value.", lineNumber);
            }
            else if (variableValue instanceof CharSequence)
            {
                parentExecutionUnit.setVariable(variableName, ((CharSequence)variableValue).toString() + expressionResult);
                return ExecutionUnit.RETURN_CODE_COMPLETED;
            }
            else throw new EPICRuntimeException("Cannot increment variable of type: " + variableValue, lineNumber);
        }
        else if (type == VARIABLEStatement.TYPE_SUBTRACT_ASSIGNMENT)
        {
            Object variableValue;
            Object expressionResult;

            expressionResult = variableValueExpression.evaluate(parentExecutionUnit, contextObject);
            variableValue = parentExecutionUnit.getVariable(variableName);
            if (variableValue instanceof Integer)
            {
                parentExecutionUnit.setVariable(variableName, (Integer)variableValue - ((Number)expressionResult).intValue());
                return ExecutionUnit.RETURN_CODE_COMPLETED;
            }
            else if (variableValue instanceof Double)
            {
                parentExecutionUnit.setVariable(variableName, (Double)variableValue - ((Number)expressionResult).doubleValue());
                return ExecutionUnit.RETURN_CODE_COMPLETED;
            }
            else throw new EPICRuntimeException("Cannot decrement variable of type: " + variableValue, lineNumber);
        }
        else throw new EPICRuntimeException("Invalid variable statement type: " + type, lineNumber);
    }
}
