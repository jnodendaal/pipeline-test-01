package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import java.util.Collection;
import java.util.Iterator;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class FORStatement extends Statement
{
    private Statement initializationStatement;
    private Expression terminationExpression;
    private Statement incrementStatement;
    private VARStatement variableStatement;
    private Expression collectionExpression;
    private ExecutionUnit executionUnit;

    private FORStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public ExecutionUnit getExecutionUnit()
    {
        return executionUnit;
    }

    public void setExecutionUnit(ExecutionUnit executionUnit)
    {
        this.executionUnit = executionUnit;
    }

    public Statement getIncrementStatement()
    {
        return incrementStatement;
    }

    public void setIncrementStatement(Statement incrementStatement)
    {
        this.incrementStatement = incrementStatement;
    }

    public Statement getInitializationStatement()
    {
        return initializationStatement;
    }

    public void setInitializationStatement(Statement initializationStatement)
    {
        this.initializationStatement = initializationStatement;
    }

    public Expression getTerminationExpression()
    {
        return terminationExpression;
    }

    public void setTerminationExpression(Expression terminationExpression)
    {
        this.terminationExpression = terminationExpression;
    }

    public Expression getCollectionExpression()
    {
        return collectionExpression;
    }

    public void setCollectionExpression(Expression collectionExpression)
    {
        this.collectionExpression = collectionExpression;
    }

    public VARStatement getVariableStatement()
    {
        return variableStatement;
    }

    public void setVariableStatement(VARStatement variableStatement)
    {
        this.variableStatement = variableStatement;
    }

    public static FORStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        FORStatement newStatement;
        Token nextToken;

        newStatement = new FORStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.FOR))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                if (isForEachLoop(tokenizer))
                {
                    VARStatement variableStatement;

                    variableStatement = VARStatement.parseStatement(tokenizer, newStatement);
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol(':'))
                    {
                        Expression collectionExpression;

                        collectionExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                        nextToken = tokenizer.nextToken();
                        if (nextToken.isSymbol(')'))
                        {
                            ExecutionUnit executionUnit;

                            executionUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newStatement);
                            newStatement.setExecutionUnit(executionUnit);
                            newStatement.setVariableStatement(variableStatement);
                            newStatement.setCollectionExpression(collectionExpression);
                            return newStatement;
                        }
                        else throw new EPICSyntaxException("Missing closing ')' in for-each statement.  Found: " + nextToken, tokenizer.getLineNumber());
                    }
                    else throw new EPICSyntaxException("Missing ':' in for-each statement.", tokenizer.getLineNumber());
                }
                else
                {
                    Statement initializationStatement;
                    Expression terminationExpression;
                    Statement incrementStatement;

                    // Construct the initialization statement.
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol(';'))
                    {
                        tokenizer.returnToken(nextToken);
                        initializationStatement = null;
                    }
                    else
                    {
                        tokenizer.returnToken(nextToken);
                        initializationStatement = StatementParser.parseStatement(tokenizer, newStatement, false);
                    }

                    // Check the termination character.
                    nextToken = tokenizer.nextToken();
                    if (!nextToken.isSymbol(';')) throw new EPICSyntaxException("Missing ';' after initialization clause in for statement.", tokenizer.getLineNumber());

                    // Construct the termination parseExpression.
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol(';'))
                    {
                        tokenizer.returnToken(nextToken);
                        terminationExpression = null;
                    }
                    else
                    {
                        tokenizer.returnToken(nextToken);
                        terminationExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                    }

                    // Check the termination character.
                    nextToken = tokenizer.nextToken();
                    if (!nextToken.isSymbol(';')) throw new EPICSyntaxException("Missing ';' after termination clause in for statement.", tokenizer.getLineNumber());

                    // Construct the termination parseExpression.
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol(')'))
                    {
                        tokenizer.returnToken(nextToken);
                        incrementStatement = null;
                    }
                    else
                    {
                        tokenizer.returnToken(nextToken);
                        incrementStatement = StatementParser.parseStatement(tokenizer, newStatement, false);
                    }

                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol(')'))
                    {
                        ExecutionUnit executionUnit;

                        executionUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newStatement);
                        newStatement.setExecutionUnit(executionUnit);
                        newStatement.setInitializationStatement(initializationStatement);
                        newStatement.setTerminationExpression(terminationExpression);
                        newStatement.setIncrementStatement(incrementStatement);
                        return newStatement;
                    }
                    else throw new EPICSyntaxException("Missing closing ')' in for statement.  Found: " + nextToken, tokenizer.getLineNumber());
                }
            }
            else throw new EPICSyntaxException("'(' character missing in for statement.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of for statement.", tokenizer.getLineNumber());
    }

    private static boolean isForEachLoop(LexicalTokenizer tokenizer) throws EPICSyntaxException
    {
        Token varToken;
        Token variableToken;
        Token colonToken;
        boolean forEach;

        varToken = tokenizer.nextToken();
        variableToken = tokenizer.nextToken();
        colonToken = tokenizer.nextToken();
        forEach = (varToken.isKeyword(Statement.VAR) && variableToken.isVariable() && colonToken.isSymbol(':'));

        tokenizer.returnToken(colonToken);
        tokenizer.returnToken(variableToken);
        tokenizer.returnToken(varToken);
        return forEach;
    }

    private int executeIterator(Object contextObject, Iterator<?> iterator) throws EPICRuntimeException
    {
        while (iterator.hasNext())
        {
            Object elementValue;
            int returnCode;

            // Get the next value from the collection.
            elementValue = iterator.next();
            this.setVariable(variableStatement.getVariableName(), elementValue);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    private int executeArrayIterator(Object contextObject, Object[] array) throws EPICRuntimeException
    {
        for (Object arrObj : array)
        {
            int returnCode;

            this.setVariable(variableStatement.getVariableName(), arrObj);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    private int executeCharArrayIterator(Object contextObject, char[] cArray) throws EPICRuntimeException
    {
        for (char c : cArray)
        {
            int returnCode;

            this.setVariable(variableStatement.getVariableName(), c);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    private int executeIntArrayIterator(Object contextObject, int[] iArray) throws EPICRuntimeException
    {
        for (int i : iArray)
        {
            int returnCode;

            this.setVariable(variableStatement.getVariableName(), i);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    private int executeDoubleArrayIterator(Object contextObject, double[] dArray) throws EPICRuntimeException
    {
        for (double d : dArray)
        {
            int returnCode;

            this.setVariable(variableStatement.getVariableName(), d);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    private int executeLongArrayIterator(Object contextObject, long[] lArray) throws EPICRuntimeException
    {
        for (long l : lArray)
        {
            int returnCode;

            this.setVariable(variableStatement.getVariableName(), l);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    private int executeBoolArrayIterator(Object contextObject, boolean[] bArray) throws EPICRuntimeException
    {
        for (boolean b : bArray)
        {
            int returnCode;

            this.setVariable(variableStatement.getVariableName(), b);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    private int executeFloatArrayIterator(Object contextObject, float[] fArray) throws EPICRuntimeException
    {
        for (float f : fArray)
        {
            int returnCode;

            this.setVariable(variableStatement.getVariableName(), f);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    private int executeShortArrayIterator(Object contextObject, short[] sArray) throws EPICRuntimeException
    {
        for (short s : sArray)
        {
            int returnCode;

            this.setVariable(variableStatement.getVariableName(), s);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }

    private int executeByteArrayIterator(Object contextObject, byte[] bArray) throws EPICRuntimeException
    {
        for (byte b : bArray)
        {
            int returnCode;

            this.setVariable(variableStatement.getVariableName(), b);

            // Execute the execution unit for this iteration.
            returnCode = executionUnit.execute(contextObject);
            if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
            else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
            {
                variables.clear();
                return returnCode;
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }


    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        if (collectionExpression != null) // For-each loop.
        {
            Object collectionObject;

            variableStatement.execute(contextObject);

            // Get the collection object.
            collectionObject = collectionExpression.evaluate(this, contextObject);
            if (collectionObject instanceof Collection)
            {
                Iterator<?> iterator;

                // Get an iterator for the collection.
                iterator = ((Iterable)collectionObject).iterator();
                return executeIterator(contextObject, iterator);
            }
            else if (collectionObject instanceof Iterator)
            {
                return executeIterator(contextObject, (Iterator)collectionObject);
            }
            else if (collectionObject instanceof Object[])
            {
                return executeArrayIterator(contextObject, (Object[])collectionObject);
            }
            else if (collectionObject instanceof char[])
            {
                return executeCharArrayIterator(contextObject, (char[])collectionObject);
            }
            else if (collectionObject instanceof int[])
            {
                return executeIntArrayIterator(contextObject, (int[])collectionObject);
            }
            else if (collectionObject instanceof double[])
            {
                return executeDoubleArrayIterator(contextObject, (double[])collectionObject);
            }
            else if (collectionObject instanceof long[])
            {
                return executeLongArrayIterator(contextObject, (long[])collectionObject);
            }
            else if (collectionObject instanceof boolean[])
            {
                return executeBoolArrayIterator(contextObject, (boolean[])collectionObject);
            }
            else if (collectionObject instanceof float[])
            {
                return executeFloatArrayIterator(contextObject, (float[])collectionObject);
            }
            else if (collectionObject instanceof short[])
            {
                return executeShortArrayIterator(contextObject, (short[])collectionObject);
            }
            else if (collectionObject instanceof byte[])
            {
                return executeByteArrayIterator(contextObject, (byte[])collectionObject);
            }
            else throw new EPICRuntimeException("Invalid collection found: " + (collectionObject != null ? collectionObject.getClass() : null), lineNumber);
        }
        else // Regular for-loop.
        {
            if (initializationStatement != null) initializationStatement.execute(contextObject);
            while ((terminationExpression == null) || ((Boolean)terminationExpression.evaluate(this, contextObject)))
            {
                int returnCode;

                returnCode = executionUnit.execute(contextObject);
                if (returnCode == ExecutionUnit.RETURN_CODE_BREAK) break;
                else if (returnCode == ExecutionUnit.RETURN_CODE_RETURN)
                {
                    variables.clear();
                    return returnCode;
                }

                if (incrementStatement != null) incrementStatement.execute(contextObject);
            }
        }

        variables.clear();
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
