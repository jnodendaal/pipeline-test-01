package com.pilog.epic.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Thrown by the parser or evaluator when invalid syntax is encountered.
 *
 * @author Bouwer du Preez
 */
public class EPICRuntimeException extends Exception
{
    private final int lineNumber;

    /**
     * Constructs a Syntax error with the message <i>errorMessage</i>.
     *
     * @param errorMessage The error message with which to construct this error.
     * @param causeException The underlying exception that caused the EPIC
     * runtime exception.
     * @param lineNumber The line number where the runtime exception occurred.
     */
    public EPICRuntimeException(String errorMessage, Exception causeException, int lineNumber)
    {
        super("Line[" + lineNumber + "]: " + errorMessage, causeException);
        this.lineNumber = lineNumber;
    }

    /**
     * Constructs a Syntax error with the message <i>errorMessage</i>.
     *
     * @param errorMessage The error message with which to construct this error.
     * @param lineNumber The line number where the runtime exception occurred.
     */
    public EPICRuntimeException(String errorMessage, int lineNumber)
    {
        super("Line[" + lineNumber + "]: " + errorMessage);
        this.lineNumber = lineNumber;
    }

    /**
     * Returns the root cause of this exception.
     *
     * @return The root cause of this exception.
     */
    public Throwable getRootCause()
    {
        Throwable cause;

        cause = this;
        while (cause.getCause() != null)
        {
            cause = cause.getCause();
        }

        return cause;
    }

    /**
     * Returns the stack trace of this exception as a String value.  A length limit (expressed in number of characters) can be specified
     * in which case the resultant String will be truncated in such a way that the specified number of characters from the start of
     * the stack trace remain in the resultant string along with the specified number of characters from the end of the String.
     * Truncation in the middle of the String is implemented in order to allow the result to be shortened while preserving
     * the root cause at the bottom of the stack, which will be printed at the end of the String, and/or the initial EPIC method invocation
     * at the top of the stack, which is printed at the beginning of the String.
     *
     * @param lengthLimitFromStart The length limit from the start (top) of the stack for the String value to return.  -1 indicates no limit.
     * @param lengthLimitFromEnd The length limit from the end (bottom) of the stack for the String value to return.  -1 indicates no limit.
     * @return The stack trace of this exception as a String value.
     */
    public String getStackTraceString(int lengthLimitFromStart, int lengthLimitFromEnd)
    {
        StringWriter stackTraceWriter;
        String stackTrace;
        int length;

        stackTraceWriter = new StringWriter();
        printStackTrace(new PrintWriter(stackTraceWriter));
        stackTrace = stackTraceWriter.toString();
        length = stackTrace.length();

        if (lengthLimitFromStart > 0)
        {
            if (lengthLimitFromEnd > 0)
            {
                if (length > (lengthLimitFromStart + lengthLimitFromEnd - 5))
                {
                    String startPart;
                    String endPart;

                    startPart = stackTrace.substring(0, lengthLimitFromStart - 5);
                    endPart = stackTrace.substring(length - lengthLimitFromEnd + 5, length);
                    return startPart + "\n\t.\n\t.\n\t.\n" + endPart;
                }
                else
                {
                    return stackTrace;
                }
            }
            else
            {
                return stackTrace.substring(0, lengthLimitFromStart);
            }
        }
        else if (lengthLimitFromEnd > 0)
        {
            return stackTrace.substring(length - lengthLimitFromEnd, length);
        }
        else
        {
            return stackTrace;
        }
    }

    @Override
    public String getMessage()
    {
        return super.getMessage();
    }

    public int getLineNumber()
    {
        return lineNumber;
    }
}
