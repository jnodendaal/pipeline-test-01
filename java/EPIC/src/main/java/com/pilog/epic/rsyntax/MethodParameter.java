/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.epic.rsyntax;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class MethodParameter
{
    private String name;
    private Object type;
    private String desc;
    private boolean isEndParam;

    public MethodParameter(String name)
    {
        this(null, name, false);
    }

    /**
     * Constructor.
     *
     * @param type The type of this parameter. This may be
     *             <code>null</code> for languages without specific types,
     *             dynamic typing, etc. Usually you'll pass a String for this
     *             value, but you may pass any object representing a type in
     *             your language, as long as its <code>toString()</code> method
     *             returns a string representation of the type.
     * @param name The name of the parameter.
     */
    public MethodParameter(Object type, String name)
    {
        this(type, name, false);
    }

    /**
     * Constructor.
     *
     * @param type     The type of this parameter. This may be
     *                 <code>null</code> for languages without specific types,
     *                 dynamic typing, etc. Usually you'll pass a String for this
     *                 value, but you may pass any object representing a type in
     *                 your language, as long as its <code>toString()</code> method
     *                 returns a string representation of the type.
     * @param name     The name of the parameter.
     * @param endParam Whether this parameter is an "ending parameter;"
     *                 that is, whether this parameter is at a logical "ending
     *                 point" in the completion text. If the user types in a
     *                 parameter that is an ending point, parameter completion mode
     *                 terminates. Set this to <code>true</code> for a trailing
     *                 parameter after a function call's closing ')', for example.
     */
    public MethodParameter(Object type, String name, boolean endParam)
    {
        this.name = name;
        this.type = type;
        this.isEndParam = endParam;
    }

    public String getDescription()
    {
        return desc;
    }

    public String getName()
    {
        return name;
    }

    /**
     * Returns the type of this parameter, as a string.
     *
     * @return The type of the parameter, or <code>null</code> for none.
     */
    public String getType()
    {
        return type == null ? null : type.toString();
    }

    /**
     * Returns the object used to describe the type of this parameter.
     *
     * @return The type object, or <code>null</code> for none.
     */
    public Object getTypeObject()
    {
        return type;
    }

    /**
     * @return Whether this parameter is an "ending parameter;"
     *         that is, whether this parameter is at a logical "ending
     *         point" in the completion text. If the user types in a
     *         parameter that is an ending point, parameter completion mode
     *         terminates.
     */
    public boolean isEndParam()
    {
        return isEndParam;
    }

    public void setDescription(String desc)
    {
        this.desc = desc;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        if (getType() != null)
        {
            sb.append(getType());
        }
        if (getName() != null)
        {
            if (getType() != null)
            {
                sb.append(' ');
            }
            sb.append(getName());
        }
        return sb.toString();
    }
}
