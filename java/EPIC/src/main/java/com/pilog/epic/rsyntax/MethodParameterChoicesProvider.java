/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.epic.rsyntax;

import java.util.List;
import javax.swing.text.JTextComponent;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public interface MethodParameterChoicesProvider
{
    public List<BasicEpicCompletion> getParameterChoices(JTextComponent tc,
                                    MethodParameter param);
}
