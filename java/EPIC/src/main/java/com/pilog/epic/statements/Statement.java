package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public abstract class Statement extends ExecutionUnit
{
    protected int lineNumber;
    private String originalString; // The original String that was parsed into this Statement.
    protected Statement inputStatement;
    protected Statement chainedStatement;

    /**
     * These are all of the statement KEYWORDS we can parse.
     */
    public final static String[] KEYWORDS =
    {
        "*NONE*",       // 0.
        "if",           // 1.
        "else",         // 2.
        "print",        // 3.
        "println",      // 4.
        "for",          // 5.
        "var",          // 6.
        "while",        // 7.
        "break",        // 8.
        "continue",     // 9.
        "return",       // 10.
        "new",          // 11.
        "import",       // 12.
        "try",          // 13.
        "catch",        // 14.
        "finally",      // 15.
        "int",          // 16.
        "long",         // 17.
        "float",        // 18.
        "double",       // 19.
        "number",       // 20.
        "empty",        // 21.
        "switch",       // 22.
        "case",         // 23.
        "default",      // 24.
        "parse",        // 25.
        "throw",        // 25.
    };

    // Statement type constants (should match the indexes of the above KEYWORDS).
    public final static int NONE = 0; // invalid statement
    public final static int IF = 1;
    public final static int ELSE = 2;
    public final static int PRINT = 3;
    public final static int PRINTLN = 4;
    public final static int FOR = 5;
    public final static int VAR = 6;
    public final static int WHILE = 7;
    public final static int BREAK = 8;
    public final static int CONTINUE = 9;
    public final static int RETURN = 10;
    public final static int NEW = 11;
    public final static int IMPORT = 12;
    public final static int TRY = 13;
    public final static int CATCH = 14;
    public final static int FINALLY = 15;
    public final static int INT = 16;
    public final static int LONG = 17;
    public final static int FLOAT = 18;
    public final static int DOUBLE = 19;
    public final static int NUMBER = 20;
    public final static int EMPTY = 21;
    public final static int SWITCH = 22;
    public final static int CASE = 23;
    public final static int DEFAULT = 24;
    public final static int PARSE = 25;
    public final static int THROW = 26;

    /**
     * Constructs a Statement with the specified type.
     *
     * @param parentExecutionUnit The execution unit within the context of which
     * this statement exists.
     * @param lineNumber The line number in the EPIC script from which this
     * statement was parsed.
     */
    protected Statement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit);
        this.lineNumber = lineNumber;
    }

    /**
     * Returns the original String that was parsed into this Statement.
     *
     * @return The original String that was parsed into this Statement.
     */
    public String getOriginalString()
    {
        return originalString;
    }

    /**
     * Sets the original String that was parsed into this Statement.
     *
     * @param originalString The original String that was parsed into this
     * Statement.
     */
    public void setOriginalString(String originalString)
    {
        this.originalString = originalString;
    }

    /**
     * Returns the line number of this Statement.
     *
     * @return The line number of this Statement.
     */
    public int getLineNumber()
    {
        return lineNumber;
    }

    /**
     * Sets the line number of this Statement.
     *
     * @param lineNumber The new line number of this Statement.
     */
    public void setLineNumber(int lineNumber)
    {
        this.lineNumber = lineNumber;
    }

    public Statement getInputStatement()
    {
        return inputStatement;
    }

    private void setInputStatement(Statement inputStatement)
    {
        this.inputStatement = inputStatement;
    }

    public Statement getChainedStatement()
    {
        return chainedStatement;
    }

    public void setChainedStatement(Statement chainedStatement)
    {
        this.chainedStatement = chainedStatement;
        this.chainedStatement.setInputStatement(this);
    }

    @Override
    public List<ExecutionUnit> getDescendantExecutionUnits()
    {
        List<ExecutionUnit> descendants;

        descendants = new ArrayList<ExecutionUnit>();
        if (chainedStatement != null)
        {
            descendants.add(chainedStatement);
            descendants.addAll(chainedStatement.getDescendantExecutionUnits());
        }

        return descendants;
    }

    @Override
    public Object getReturnObject()
    {
        if (chainedStatement != null) return chainedStatement.getReturnObject();
        else return returnObject;
    }
}
