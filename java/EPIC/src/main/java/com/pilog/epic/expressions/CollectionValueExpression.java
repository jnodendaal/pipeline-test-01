package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class CollectionValueExpression extends DefaultExpression
{
    private final List<Expression> elementExpressions;
    private String collectionName;

    public CollectionValueExpression() throws EPICSyntaxException
    {
        this.elementExpressions = new ArrayList<>();
    }

    public void setCollectionName(String collectionName)
    {
        this.collectionName = collectionName;
    }

    public void addElementExpression(Expression elementExpression)
    {
        this.elementExpressions.add(elementExpression);
    }

    /**
     * Returns a String representation of this DefaultExpression.
     *
     * @return A String representation of this DefaultExpression.
     */
    @Override
    public String toString()
    {
        return "CollectionValueExpression: '" + unparse() + "'";
    }

    /**
     * Returns the evaluated value of this parseExpression.
     *
     * @param parentExecutionUnit The ExpressionEvaluator executing this parseExpression.
     * @return The result of this parseExpression.
     * @throws EPICRuntimeException
     */
    @Override
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        Object collection;

        collection = parentExecutionUnit.getVariable(collectionName);
        if (collection != null)
        {
            for (Expression elementExpression : elementExpressions)
            {
                Object elementKey;

                // Evalute the next element parseExpression to get the element key.
                elementKey = elementExpression.evaluate(parentExecutionUnit, contextObject);
                if (collection instanceof List)
                {
                    collection = ((List)collection).get((Integer)elementKey);
                }
                else if (collection instanceof Map)
                {
                    collection = ((Map)collection).get(elementKey);
                }
                else throw new EPICRuntimeException("Invalid collection type: " + collection.getClass(), lineNumber);
            }

            // At this stage the result will either be a collection or the value fetched from the collection.
            return collection;
        }
        else throw new EPICRuntimeException("Null Collection '" + collectionName + "' cannot be accessed.", lineNumber);
    }

    public static CollectionValueExpression parse(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        CollectionValueExpression newExpression;
        Token nextToken;

        newExpression = new CollectionValueExpression();
        nextToken = tokenizer.nextToken();
        if (nextToken.getType() == Token.COLLECTION)
        {
            String collectionName;

            collectionName = (String)nextToken.getValue();
            newExpression.setCollectionName(collectionName);

            nextToken = tokenizer.nextToken();
            while (nextToken.isSymbol('['))
            {
                Expression elementExpression;

                elementExpression = ExpressionParser.parseExpression(tokenizer, parentExecutionUnit);
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(']'))
                {
                    newExpression.addElementExpression(elementExpression);
                }
                else throw new EPICSyntaxException("Missing ']' in collection lookup.", tokenizer.getLineNumber());
            }

            return newExpression;
        }
        else throw new EPICSyntaxException("Invalid start of collection lookup.", tokenizer.getLineNumber());
    }
}
