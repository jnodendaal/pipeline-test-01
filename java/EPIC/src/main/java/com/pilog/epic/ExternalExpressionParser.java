package com.pilog.epic;

import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public interface ExternalExpressionParser
{
    public boolean isApplicableTo(String expression);
    public Expression parse(String expression);
}
