package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.ReflectionTools;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class NEWObjectStatement extends Statement
{
    private IDENTIFIERStatement identifierStatement;
    private final ArrayList<Expression> parameters;
    private transient Constructor cachedConstructor; // Used to cache the Constructor during execution.

    private NEWObjectStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
        parameters = new ArrayList<>();
    }

    public IDENTIFIERStatement getIdentifierStatement()
    {
        return identifierStatement;
    }

    public void setIdentifierStatement(IDENTIFIERStatement identifierStatement)
    {
        this.identifierStatement = identifierStatement;
    }

    public void addParameter(Expression parameter)
    {
        parameters.add(parameter);
    }

    public static NEWObjectStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        NEWObjectStatement newStatement;
        Token nextToken;

        // Create the new statement.
        newStatement = new NEWObjectStatement(parentExecutionUnit, tokenizer.getLineNumber());

        // Make sure we start parsing at the correct index.
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.NEW))
        {
            newStatement.setIdentifierStatement(IDENTIFIERStatement.parseStatement(tokenizer, parentExecutionUnit, true, true));
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                while (!(nextToken = tokenizer.nextToken()).isSymbol(')'))
                {
                    // Return the next token so it can be parsed as an parseExpression.
                    tokenizer.returnToken(nextToken);

                    // Parse the next token as an parseExpression and add the result as a function argument.
                    newStatement.addParameter(ExpressionParser.parseExpression(tokenizer, parentExecutionUnit));

                    // Check the next token to see if it is a comma.
                    nextToken = tokenizer.nextToken();
                    if (!nextToken.isSymbol(',')) tokenizer.returnToken(nextToken);
                }

                return newStatement;
            }
            else throw new EPICSyntaxException("No constructor arguments found.  Unexpected token: " + nextToken, tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid constructor call.", tokenizer.getLineNumber());
    }

    @Override
    public Object getReturnObject()
    {
        if (chainedStatement != null) return chainedStatement.getReturnObject();
        else return returnObject;
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Object classObject;
        ArrayList<Object> parameterValues;

        // Evaluate the procedure parameters.
        parameterValues = new ArrayList<Object>();
        for (Expression parameterExpression : parameters)
        {
            parameterValues.add(parameterExpression.evaluate(parentExecutionUnit, contextObject));
        }

        identifierStatement.execute(contextObject);
        classObject = identifierStatement.getReturnObject();
        if (classObject == null) throw new EPICRuntimeException("Null class object cannot be instantiated.", lineNumber);
        else if (!(classObject instanceof Class)) throw new EPICRuntimeException("Invalid object type found in constructor call.", lineNumber);
        else
        {
            try
            {
                Class newClass;

                newClass = (Class)classObject;
                if (cachedConstructor == null)
                {
                    cachedConstructor = getConstructor(newClass, parameterValues.toArray());
                    if (cachedConstructor == null) throw new EPICRuntimeException("No constructor found.", lineNumber);
                }

                returnObject = cachedConstructor.newInstance(getParameterValues(parameterValues));
                if (chainedStatement != null) return chainedStatement.execute(returnObject);
                else return Statement.RETURN_CODE_COMPLETED;
            }
            catch (Exception e)
            {
                throw new EPICRuntimeException("Constructor exception: " + e.getMessage() + " Parameter Types: " + getParameterTypeNames(parameterValues), e, lineNumber);
            }
        }
    }

    public Constructor getConstructor(Class cls, Object[] arguments) throws NoSuchMethodException
    {
        return ReflectionTools.getBestConstructorCandidate(arguments, cls, true);
    }

    private Object[] getParameterValues(ArrayList<Object> parameters)
    {
        Object[] parameterValues;

        parameterValues = new Object[parameters.size()];
        for (int parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex++)
        {
            Object parameter;

            parameter = parameters.get(parameterIndex);
            parameterValues[parameterIndex] = parameter;
        }

        return parameterValues;
    }

    private Class[] getParameterTypes(ArrayList<Object> parameters)
    {
        Class[] parameterTypes;

        parameterTypes = new Class[parameters.size()];
        for (int parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex++)
        {
            Object parameter;

            parameter = parameters.get(parameterIndex);
            parameterTypes[parameterIndex] = parameter.getClass();
        }

        return parameterTypes;
    }

    private List<String> getParameterTypeNames(ArrayList<Object> parameters)
    {
        ArrayList<String> typeNames;

        typeNames = new ArrayList<String>();
        for (int parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex++)
        {
            Object parameter;

            parameter = parameters.get(parameterIndex);
            typeNames.add(parameter != null ? parameter.getClass().getCanonicalName() : "null");
        }

        return typeNames;
    }
}
