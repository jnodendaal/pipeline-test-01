package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;

/**
 * This class is a simple extension of the regular expression class to act as a
 * marker for boolean type expressions and to ensure that the results of such
 * expressions are always boolean values.
 *
 * @author Bouwer du Preez
 */
public class BooleanExpression extends DefaultExpression
{
    /**
     * Creates a new binary Boolean expression.
     *
     * @param operator The expression operator.
     * @param argument1 The operand A expression.
     * @param argument2 The operand B expression.
     * @throws EPICSyntaxException
     */
    public BooleanExpression(int operator, Expression argument1, Expression argument2) throws EPICSyntaxException
    {
        super(operator, argument1, argument2);
    }

    /**
     * Create a new unary Boolean expression.
     *
     * @param operator The expression operator.
     * @param argument2
     * @throws EPICSyntaxException
     */
    public BooleanExpression(int operator, Expression argument2) throws EPICSyntaxException
    {
        super(operator, argument2);
    }

    public boolean evaluateBooleanExpression(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        Object result;

        result = evaluate(parentExecutionUnit, contextObject);
        if (result instanceof Boolean)
        {
            return ((Boolean)result);
        }
        else throw new EPICRuntimeException("Not a boolean expression. Result Type: " + result.getClass().toString(), -1);
    }
}
