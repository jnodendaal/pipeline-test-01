package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Procedure;
import com.pilog.epic.Program;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class RETURNStatement extends Statement
{
    private Expression valueExpression;

    private RETURNStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public Expression getValueExpression()
    {
        return valueExpression;
    }

    public void setValueExpression(Expression valueExpression)
    {
        this.valueExpression = valueExpression;
    }

    public static RETURNStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        RETURNStatement returnStatement;
        Token nextToken;

        returnStatement = new RETURNStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.RETURN))
        {
            nextToken = tokenizer.nextToken();
            if (!nextToken.isSymbol(';'))
            {
                Expression expression;

                tokenizer.returnToken(nextToken);
                expression = ExpressionParser.parseExpression(tokenizer, returnStatement);
                returnStatement.setValueExpression(expression);
                return returnStatement;
            }
            else
            {
                tokenizer.returnToken(nextToken);
                return returnStatement;
            }
        }
        else throw new EPICSyntaxException("Invalid start of return statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        ExecutionUnit executionUnit;

        executionUnit = parentExecutionUnit;
        while (!(executionUnit instanceof Program) && !(executionUnit instanceof Procedure))
        {
            executionUnit = executionUnit.getParentExecutionUnit();
        }

        if (valueExpression != null) executionUnit.setReturnObject(valueExpression.evaluate(parentExecutionUnit, contextObject));
        return ExecutionUnit.RETURN_CODE_RETURN;
    }
}
