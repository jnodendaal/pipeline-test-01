package com.pilog.epic.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hennie.brink@pilog.co.za
 *
 * This Annotation only servers to indicate that this method should show up in the EPIC auto complete provider.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EPICMethod
{
    String Description();

    String Name() default "";

    boolean Hidden() default false;
}
