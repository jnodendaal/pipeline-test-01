package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;

/**
 * @author Bouwer du Preez
 */
public class CONTINUEStatement extends Statement
{
    private CONTINUEStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public static CONTINUEStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        CONTINUEStatement newStatement;
        Token nextToken;

        newStatement = new CONTINUEStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.CONTINUE))
        {
            return newStatement;
        }
        else throw new EPICSyntaxException("Invalid start of continue statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        return ExecutionUnit.RETURN_CODE_CONTINUE;
    }
}
