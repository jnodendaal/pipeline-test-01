package com.pilog.epic;

import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import com.pilog.epic.expressions.Expression;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class ExpressionEvaluator
{
    private Program program;
    private Expression expression;

    public ExpressionEvaluator()
    {
    }

    public void compileExpression(String expressionString) throws EPICSyntaxException
    {
        compileExpression(expressionString, (ParserContext)null);
    }

    public void compileExpression(String expressionString, Collection<String> variableDeclarations) throws EPICSyntaxException
    {
        ParserContext parserContext;

        parserContext = new ParserContext();
        parserContext.addVariableDeclarations(variableDeclarations);
        compileExpression(expressionString, parserContext);
    }

    public void compileExpression(String expressionString, ParserContext context) throws EPICSyntaxException
    {
        LexicalTokenizer tokenizer;

        // Create a tokenizer to use when parsing the parseExpression.
        tokenizer = new LexicalTokenizer(expressionString);
        tokenizer.setCheckVariableDeclaration(false);

        // Create a dummy program to facility the execution of the parseExpression.
        program = new Program();

        // Set the parser context.
        if (context != null)
        {
            HashSet<String> variableDeclarations;
            List<ExtensionProcedure> methodImports;
            Map<String, Class> classImports;

            // Declare all of the variables that are used, so that they can be resolved during parsing.
            variableDeclarations = context.getVariableDeclarations();
            for (String variableName : variableDeclarations)
            {
                program.addVariableDeclaration(variableName);
            }

            // Add all of the class imports available in the parser context.
            classImports = context.getClassImports();
            for (String classReference : classImports.keySet())
            {
                program.addClassImport(classReference, classImports.get(classReference));
            }

            // Add all of the method imports available in the parser context.
            methodImports = context.getMethodImports();
            for (ExtensionProcedure importedProcedure : methodImports)
            {
                program.addProcedure(importedProcedure);
            }

            // Add any available external parsers to the tokenizer.
            tokenizer.addExternalExpressionParsers(context.getExternalExpressionParsers());
        }

        // Parse the parseExpression.
        expression = ExpressionParser.parseExpression(tokenizer, program);
    }

    public Set<String> getVariableNames()
    {
        return program.getVariableNames();
    }

    public Object evaluateExpression(ParserContext parserContext, String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException
    {
        compileExpression(expressionString, parserContext);
        return evaluateExpression(inputVariables, contextObject);
    }

    public Object evaluateExpression(String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException
    {
        compileExpression(expressionString);
        return evaluateExpression(inputVariables, contextObject);
    }

    public Object evaluateExpression(Map<String, Object> inputVariables, Object contextObject) throws EPICRuntimeException
    {
        if (expression != null)
        {
            if (inputVariables != null)
            {
                for (String variableName : inputVariables.keySet())
                {
                    if (!program.containsVariable(variableName)) program.declareVariable(variableName, inputVariables.get(variableName));
                    else program.setVariable(variableName, inputVariables.get(variableName));
                }
            }

            return expression.evaluate(program, contextObject);
        }
        else
        {
            return null;
        }
    }

    public boolean evaluateBooleanExpression(ParserContext parserContext, String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException
    {
        compileExpression(expressionString, parserContext);
        return evaluateBooleanExpression(inputVariables, contextObject);
    }

    public boolean evaluateBooleanExpression(String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException
    {
        compileExpression(expressionString, inputVariables != null ? inputVariables.keySet() : new ArrayList<>());
        return evaluateBooleanExpression(inputVariables, contextObject);
    }

    public boolean evaluateBooleanExpression(Map<String, Object> inputVariables, Object contextObject) throws EPICRuntimeException
    {
        Object result;

        result = evaluateExpression(inputVariables, null);
        if (result instanceof Boolean)
        {
            return (Boolean)result;
        }
        else throw new EPICRuntimeException("Not a boolean expression. Result: " + result + " Result Type: " + result.getClass().toString(), -1);
    }

    public Random getRandom()
    {
        return new Random();
    }
}
