package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Expressions are parsed by the class <b>ParseExpression</b> and creates
 * a parse tree using objects of type expression. The subclasses
 * <b>ConstantExpression</b>, <b>VariableExpression</b>, and <b>FunctionExpression</b>
 * hold specific types of indivisable elements. The class <b>BooleanExpression</b> is
 * used to point to boolean expressions. This distinction on booleans allows us to do
 * some syntax checking and statements like the IF statement can verify their expression
 * is a boolean one.
 *
 * See the class ParseExpression for the grammar and precedence rules.
 *
 * @author Bouwer du Preez
 */
public class DefaultExpression implements Expression
{
    protected int lineNumber;
    protected Expression argument1;
    protected Expression argument2;
    protected int operator;

    // Valid operator types.
    public final static int OP_ADD = 1; // Addition '+'
    public final static int OP_SUB = 2; // Subtraction '-'
    public final static int OP_MUL = 3; // Multiplication '*'
    public final static int OP_DIV = 4; // Division '/'
    public final static int OP_EXP = 5; // Exponentiation '**'
    public final static int OP_AND = 6; // Bitwise AND '&'
    public final static int OP_IOR = 7; // Bitwise inclusive OR '|'
    public final static int OP_XOR = 8; // Bitwise exclusive OR '^'
    public final static int OP_NOT = 9; // Bitwise complement '~'
    public final static int OP_EQ = 10; // Equality '=='
    public final static int OP_NE = 11; // Inequality '<>'
    public final static int OP_LT = 12; // Less than '<'
    public final static int OP_LE = 13; // Less than or equal '<='
    public final static int OP_GT = 14; // Greater than '>'
    public final static int OP_GE = 15; // Greater than or equal '>='
    public final static int OP_BAND = 16; // Boolean AND '&&'
    public final static int OP_BIOR = 17; // Boolean inclusive or '||'
    public final static int OP_BXOR = 18; // Boolean exclusive or '^^'
    public final static int OP_BNOT = 19; // Boolean negation 'NOT'
    public final static int OP_NEG = 20; // Unary minus
    public final static int OP_CAC = 21; // Collection accessor.
    public final static int OP_CIF = 22; // Conditional if operator.
    public final static int OP_COA = 23; // Coalesce operator.
    public final static int OP_MOD = 24; // Modulus operator.
    public final static int OP_INSTANCEOF = 25; // Instanceof operator.

    protected final static String operatorValues[] =
    {
        "<NULL>", "+", "-", "*", "/", "**", "&", "|", "^", "~", "==", "<>",
        "<", "<=", ">", ">=", "&&", "||", "^^", "!", "-", "[", "?", "??", "%", "instanceof"
    };

    protected final static String typeError = "Expression: cannot combine boolean term with arithmetic term.";

    /**
     * Default constructor.
     */
    protected DefaultExpression()
    {
    }

    /**
     * Creates a binary expression.
     *
     * @param operator The expression operator.
     * @param argument1 The operand A expression.
     * @param argument2 The operand B expression.
     * @throws EPICSyntaxException
     */
    protected DefaultExpression(int operator, Expression argument1, Expression argument2) throws EPICSyntaxException
    {
        this.argument1 = argument1;
        this.argument2 = argument2;
        this.operator = operator;
    }

    /**
     * Create a unary expression.
     *
     * @param operator The expression operator.
     * @param expression2 The operand expression.
     * @throws EPICSyntaxException
     */
    protected DefaultExpression(int operator, Expression expression2) throws EPICSyntaxException
    {
        this.argument2 = expression2;
        this.operator = operator;
    }

    /**
     * Returns the line number of this DefaultExpression.
     *
     * @return The line number of this DefaultExpression.
     */
    public int getLineNumber()
    {
        return lineNumber;
    }

    /**
     * Sets the line number of this DefaultExpression.
     *
     * @param lineNumber The new line number of this DefaultExpression.
     */
    public void setLineNumber(int lineNumber)
    {
        this.lineNumber = lineNumber;
    }

    /**
     * Returns a String representation of this DefaultExpression.
     *
     * @return A String representation of this DefaultExpression.
     */
    @Override
    public String toString()
    {
        StringBuffer stringBuffer;

        stringBuffer = new StringBuffer();
        stringBuffer.append("(");
        if (argument1 != null)
        {
            stringBuffer.append(argument1.toString());
        }
        stringBuffer.append(operatorValues[operator]);
        stringBuffer.append(argument2.toString());
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    /**
     * Reconstructs the DefaultExpression from the parsed tree.  Useful for diagnosing
     * parsing problems.
     *
     * @return The String reconstructed from this DefaultExpression.
     */
    @Override
    public String unparse()
    {
        StringBuffer stringBuffer;

        stringBuffer = new StringBuffer();
        stringBuffer.append("(");

        if (argument1 != null)
        {
            stringBuffer.append(argument1.unparse());
            stringBuffer.append(" " + operatorValues[operator] + " ");
        }

        if (argument2 != null)
        {
            stringBuffer.append(argument2.unparse());
        }

        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    /**
     * This method evaluates the expression in the context of the passed in
     * ExpressionEvaluator. It throws runtime errors for things like no such variable and
     * divide by zero.
     *
     * @param parentExecutionUnit The ExecutionUnit in which this expression is executing.
     * @param contextObject
     * @return The value of this expression.
     * @throws com.pilog.epic.exceptions.EPICRuntimeException
     */
    @Override
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        Object operand1;
        Object operand2;

        // Evaluate operand 1.
        operand1 = argument1 == null ? null : argument1.evaluate(parentExecutionUnit, contextObject);

        // We have to perform short-circuit processing for boolean && and || operators before evaluating operand2.
        if (operator == OP_BAND)
        {
            if ((operand1 instanceof Boolean) && (!(Boolean)operand1)) return false;
        }
        else if (operator == OP_BIOR)
        {
            if ((operand1 instanceof Boolean) && ((Boolean)operand1)) return true;
        }

        // Now evaluate operand.
        operand2 = argument2 == null ? null : argument2.evaluate(parentExecutionUnit, contextObject);

        // Now evaluate the expression based on the operator.
        switch (operator)
        {
            case OP_ADD:
            {
                return evaluateAddition(operand1, operand2);
            }
            case OP_SUB:
            {
                return evaluateSubtraction(operand1, operand2);
            }
            case OP_MUL:
            {
                return evaluateMultiplication(operand1, operand2);
            }
            case OP_DIV:
            {
                return evaluateDivision(operand1, operand2);
            }
            case OP_MOD:
            {
                return evaluateModulus(operand1, operand2);
            }
            case OP_XOR:
            {
                return evaluateBitExclusiveOr(operand1, operand2);
            }
            case OP_IOR:
            {
                return evaluateBitInclusiveOr(operand1, operand2);
            }
            case OP_AND:
            {
                return evaluateBitAnd(operand1, operand2);
            }
            case OP_EXP:
            {
                return evaluateExponentiation(operand1, operand2);
            }
            case OP_EQ:
            {
                return evaluateBooleanEquality(operand1, operand2);
            }
            case OP_NE:
            {
                return evaluateBooleanInequality(operand1, operand2);
            }
            case OP_LT:
            {
                return evaluateBooleanLessThan(operand1, operand2);
            }
            case OP_LE:
            {
                return evaluateBooleanLessThanOrEqual(operand1, operand2);
            }
            case OP_GT:
            {
                return evaluateBooleanGreaterThan(operand1, operand2);
            }
            case OP_GE:
            {
                return evaluateBooleanGreaterThanOrEqual(operand1, operand2);
            }
            case OP_BAND:
            {
                return evaluateBooleanAnd(operand1, operand2);
            }
            case OP_BIOR:
            {
                return evaluateBooleanInclusiveOr(operand1, operand2);
            }
            case OP_BXOR:
            {
                return evaluateBooleanExclusiveOr(operand1, operand2);
            }
            case OP_BNOT:
            {
                return evaluateBooleanNot(operand2);
            }
            case OP_NOT:
            {
                return evaluateBitComplement(operand2);
            }
            case OP_NEG:
            {
                return evaluateNegation(operand2);
            }
            case OP_CAC:
            {
                return evaluateCollectionAccessor(operand1, operand2);
            }
            case OP_INSTANCEOF:
            {
                return evaluateInstanceOf(operand1, operand2);
            }
            default:
            {
                throw new EPICRuntimeException("Illegal operator in expression!", lineNumber);
            }
        }
    }

    private Object evaluateAddition(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof String) || (operand2 instanceof String))
        {
            return (operand1 == null ? "null" : operand1.toString()) + (operand2 == null ? "null" : operand2.toString());
        }
        else if ((operand1 instanceof Number) && (operand2 instanceof Number))
        {
            if ((operand1 instanceof Integer) && (operand2 instanceof Integer))
            {
                return ((Number)operand1).intValue() + ((Number)operand2).intValue();
            }
            else if (((operand1 instanceof Long) || (operand1 instanceof Integer)) && ((operand2 instanceof Long) || (operand2 instanceof Integer)))
            {
                return ((Number)operand1).longValue() + ((Number)operand2).longValue();
            }
            else
            {
                return ((Number)operand1).doubleValue() + ((Number)operand2).doubleValue();
            }
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " + " + operand2, lineNumber);
        }
    }

    private Object evaluateSubtraction(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Number) && (operand2 instanceof Number))
        {
            if ((operand1 instanceof Integer) && (operand2 instanceof Integer))
            {
                return ((Number)operand1).intValue() - ((Number)operand2).intValue();
            }
            else if (((operand1 instanceof Long) || (operand1 instanceof Integer)) && ((operand2 instanceof Long) || (operand2 instanceof Integer)))
            {
                return ((Number)operand1).longValue() - ((Number)operand2).longValue();
            }
            else
            {
                return ((Number)operand1).doubleValue() - ((Number)operand2).doubleValue();
            }
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " - " + operand2, lineNumber);
        }
    }

    private Object evaluateMultiplication(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Number) && (operand2 instanceof Number))
        {
            if ((operand1 instanceof Integer) && (operand2 instanceof Integer))
            {
                return ((Number)operand1).intValue() * ((Number)operand2).intValue();
            }
            else if (((operand1 instanceof Long) || (operand1 instanceof Integer)) && ((operand2 instanceof Long) || (operand2 instanceof Integer)))
            {
                return ((Number)operand1).longValue() * ((Number)operand2).longValue();
            }
            else
            {
                return ((Number)operand1).doubleValue() * ((Number)operand2).doubleValue();
            }
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " * " + operand2, lineNumber);
        }
    }

    private Object evaluateDivision(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Number) && (operand2 instanceof Number))
        {
            if (((Number)operand2).doubleValue() == 0)
            {
                throw new EPICRuntimeException("Divide by zero: " + operand1 + " /" + operand2, lineNumber);
            }
            else if ((operand1 instanceof Integer) && (operand2 instanceof Integer))
            {
                return ((Number)operand1).intValue() / ((Number)operand2).intValue();
            }
            else if (((operand1 instanceof Long) || (operand1 instanceof Integer)) && ((operand2 instanceof Long) || (operand2 instanceof Integer)))
            {
                return ((Number)operand1).longValue() / ((Number)operand2).longValue();
            }
            else
            {
                return ((Number)operand1).doubleValue() / ((Number)operand2).doubleValue();
            }
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax: " + operand1 + " / " + operand2, lineNumber);
        }
    }

    private Object evaluateModulus(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Number) && (operand2 instanceof Number))
        {
            if (((Number)operand2).doubleValue() == 0)
            {
                throw new EPICRuntimeException("Divide by zero: " + operand1 + " /" + operand2, lineNumber);
            }
            else if ((operand1 instanceof Integer) && (operand2 instanceof Integer))
            {
                return ((Number)operand1).intValue() % ((Number)operand2).intValue();
            }
            else if (((operand1 instanceof Long) || (operand1 instanceof Integer)) && ((operand2 instanceof Long) || (operand2 instanceof Integer)))
            {
                return ((Number)operand1).longValue() % ((Number)operand2).longValue();
            }
            else
            {
                return ((Number)operand1).doubleValue() % ((Number)operand2).doubleValue();
            }
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax: " + operand1 + " % " + operand2, lineNumber);
        }
    }

    private Object evaluateBitExclusiveOr(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Number) && (operand2 instanceof Number))
        {
            return (double)(((Number)operand1).longValue() ^ ((Number)operand2).longValue());
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " ^ " + operand2, lineNumber);
        }
    }

    private Object evaluateBitInclusiveOr(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Number) && (operand2 instanceof Number))
        {
            return (double)(((Number)operand1).longValue() | ((Number)operand2).longValue());
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " | " + operand2, lineNumber);
        }
    }

    private Object evaluateBitAnd(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Number) && (operand2 instanceof Number))
        {
            return (double)(((Number)operand1).longValue() & ((Number)operand2).longValue());
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " & " + operand2, lineNumber);
        }
    }

    private Object evaluateExponentiation(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Number) && (operand2 instanceof Number))
        {
            return (Math.pow(((Number)operand1).doubleValue(), ((Number)operand2).doubleValue()));
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " ** " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanLessThan(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 == null) || (operand2 == null))
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " < " + operand2, lineNumber);
        }
        else if ((operand1 instanceof String) || (operand2 instanceof String))
        {
            return (operand1.toString().compareTo(operand2.toString()) < 0);
        }
        else if ((operand1 instanceof Number) || (operand2 instanceof Number))
        {
            return (((Number)operand1).doubleValue() < ((Number)operand2).doubleValue());
        }
        else if ((operand1 instanceof Character) || (operand2 instanceof Character))
        {
            return (((Character)operand1).compareTo((Character)operand2) < 0);
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " < " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanLessThanOrEqual(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 == null) || (operand2 == null))
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " <= " + operand2, lineNumber);
        }
        else if ((operand1 instanceof String) || (operand2 instanceof String))
        {
            return (operand1.toString().compareTo(operand2.toString()) <= 0);
        }
        else if ((operand1 instanceof Number) || (operand2 instanceof Number))
        {
            return (((Number)operand1).doubleValue() <= ((Number)operand2).doubleValue());
        }
        else if ((operand1 instanceof Character) || (operand2 instanceof Character))
        {
            return (((Character)operand1).compareTo((Character)operand2) <= 0);
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " <= " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanGreaterThan(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 == null) || (operand2 == null))
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " > " + operand2, lineNumber);
        }
        else if ((operand1 instanceof String) || (operand2 instanceof String))
        {
            return (operand1.toString().compareTo(operand2.toString()) > 0);
        }
        else if ((operand1 instanceof Number) || (operand2 instanceof Number))
        {
            return (((Number)operand1).doubleValue() > ((Number)operand2).doubleValue());
        }
        else if ((operand1 instanceof Character) || (operand2 instanceof Character))
        {
            return (((Character)operand1).compareTo((Character)operand2) > 0);
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " > " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanGreaterThanOrEqual(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 == null) || (operand2 == null))
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " >= " + operand2, lineNumber);
        }
        else if ((operand1 instanceof String) || (operand2 instanceof String))
        {
            return (operand1.toString().compareTo(operand2.toString()) >= 0);
        }
        else if ((operand1 instanceof Number) || (operand2 instanceof Number))
        {
            return (((Number)operand1).doubleValue() >= ((Number)operand2).doubleValue());
        }
        else if ((operand1 instanceof Character) || (operand2 instanceof Character))
        {
            return (((Character)operand1).compareTo((Character)operand2) >= 0);
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " >= " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanEquality(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if (operand1 == null)
        {
            return operand2 == null;
        }
        else if (operand2 == null)
        {
            return false; // By this point we know operand1 is not null.
        }
        else if ((operand1 instanceof Boolean) || (operand2 instanceof Boolean))
        {
            if (operand1 instanceof String)
            {
                return (Objects.equals(Boolean.parseBoolean((String)operand1), (Boolean)operand2));
            }
            else if (operand2 instanceof String)
            {
                return (Objects.equals((Boolean)operand1, Boolean.parseBoolean((String)operand2)));
            }
            else
            {
                return (Objects.equals(operand1, operand2));
            }
        }
        else if ((operand1 instanceof String) || (operand2 instanceof String))
        {
            return (operand1.toString().compareTo(operand2.toString()) == 0);
        }
        else if ((operand1 instanceof Number) || (operand2 instanceof Number))
        {
            return (((Number)operand1).doubleValue() == ((Number)operand2).doubleValue());
        }
        else if ((operand1 instanceof Character) || (operand2 instanceof Character))
        {
            return (Objects.equals((Character)operand1, (Character)operand2));
        }
        else if ((operand1 instanceof Enum) || (operand2 instanceof Enum))
        {
            return (Objects.equals(operand1.toString(), operand2.toString()));
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " = " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanInequality(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if (operand1 == null)
        {
            return operand2 != null;
        }
        else if (operand2 == null)
        {
            return true;
        }
        else if ((operand1 instanceof Boolean) && (operand2 instanceof Boolean))
        {
            return (!Objects.equals((Boolean)operand1, (Boolean)operand2));
        }
        else if ((operand1 instanceof String) || (operand2 instanceof String))
        {
            return (operand1.toString().compareTo(operand2.toString()) != 0);
        }
        else if ((operand1 instanceof Number) || (operand2 instanceof Number))
        {
            return (((Number)operand1).doubleValue() != ((Number)operand2).doubleValue());
        }
        else if ((operand1 instanceof Character) || (operand2 instanceof Character))
        {
            return (!Objects.equals((Character)operand1, (Character)operand2));
        }
        else if ((operand1 instanceof Enum) || (operand2 instanceof Enum))
        {
            return (((Enum)operand1) != ((Enum)operand2));
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " != " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanAnd(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Boolean) && (operand2 instanceof Boolean))
        {
            return (((Boolean)operand1) && ((Boolean)operand2));
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " && " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanInclusiveOr(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Boolean) && (operand2 instanceof Boolean))
        {
            return (((Boolean)operand1) || ((Boolean)operand2));
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " || " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanExclusiveOr(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if ((operand1 instanceof Boolean) && (operand2 instanceof Boolean))
        {
            return (((Boolean)operand1) ^ ((Boolean)operand2));
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + " ^^ " + operand2, lineNumber);
        }
    }

    private Object evaluateBooleanNot(Object operand) throws EPICRuntimeException
    {
        if (operand == null)
        {
            return true; // Null values are evaluated as false, when using the unary '!' operator.
        }
        else if (operand instanceof Boolean)
        {
            return !((Boolean)operand);
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: !" + operand, lineNumber);
        }
    }

    private Object evaluateBitComplement(Object operand) throws EPICRuntimeException
    {
        if (operand instanceof Number)
        {
            return (double)(~(((Number)operand).longValue()));
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: ~" + operand, lineNumber);
        }
    }

    private Object evaluateNegation(Object operand) throws EPICRuntimeException
    {
        if (operand instanceof Double)
        {
            return 0 - ((Number)operand).doubleValue();
        }
        else if (operand instanceof Integer)
        {
            return 0 - ((Number)operand).intValue();
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: -" + operand, lineNumber);
        }
    }

    private Object evaluateCollectionAccessor(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if (operand1 instanceof Map)
        {
            return ((Map)operand1).get(operand2);
        }
        else if (operand1 instanceof List)
        {
            if (operand2 instanceof Number)
            {
                return ((List)operand1).get(((Number)operand2).intValue());
            }
            else
            {
                throw new EPICRuntimeException("Illegal syntax near: " + operand1 + "->" + operand2, lineNumber);
            }
        }
        else
        {
            throw new EPICRuntimeException("Illegal syntax near: " + operand1 + "->" + operand2, lineNumber);
        }
    }

    private Object evaluateInstanceOf(Object operand1, Object operand2) throws EPICRuntimeException
    {
        if (operand1 == null)
        {
            return false;
        }
        else if (operand2 == null)
        {
            return false;
        }
        else if (operand2 instanceof Class)
        {
            return ((Class)operand2).isInstance(operand1);
        }
        else
        {
            throw new EPICRuntimeException("Illegal operand type: " + operand2.getClass(), lineNumber);
        }
    }
}
