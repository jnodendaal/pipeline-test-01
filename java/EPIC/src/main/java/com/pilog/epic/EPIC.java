package com.pilog.epic;

import com.pilog.epic.exceptions.EPICSyntaxException;
import java.io.UnsupportedEncodingException;

/**
 * @author Bouwer du Preez
 */
public class EPIC
{
    public static final Program compileProgram(String sourceCode, String encoding) throws EPICSyntaxException, UnsupportedEncodingException
    {
        LexicalTokenizer tokenizer;

        tokenizer = new LexicalTokenizer(sourceCode);
        return Program.parseProgram(tokenizer, new ParserContext());
    }

    public static final Program compileProgram(String sourceCode, String encoding, ParserContext context) throws EPICSyntaxException, UnsupportedEncodingException
    {
        LexicalTokenizer tokenizer;

        tokenizer = new LexicalTokenizer(sourceCode);
        tokenizer.addExternalExpressionParsers(context.getExternalExpressionParsers());
        return Program.parseProgram(tokenizer, context);
    }
}
