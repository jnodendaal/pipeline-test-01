package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.exceptions.EPICRuntimeException;

/**
 * This class implements the simplest possible expression, a constant.
 *
 * @author Bouwer du Preez
 */
public class CharacterExpression implements Expression
{
    private final char character;

    public CharacterExpression(char character)
    {
        super();
        this.character = character;
    }

    @Override
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        return character;
    }

    @Override
    public String toString()
    {
        return "" + character;
    }

    @Override
    public String unparse()
    {
        return "" + character;
    }
}
