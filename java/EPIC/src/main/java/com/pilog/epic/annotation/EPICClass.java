package com.pilog.epic.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hennie.brink@pilog.co.za
 *
 * This Annotation server to indicate that this class should show up in the EPIC Auto complete provider.
 */
@Target({ElementType.TYPE,ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
public @interface EPICClass
{
    /*
       The variable name will be used to analize auto completions.
    */
    String PreferredVariableName();

    /*
    The template that can be used to generate the text that will instantiate this variable
    */
    String VariableCreationTemplate();

    boolean onlyShowAnnotatedMethods() default false;

    boolean onlyShowDeclaredMethods() default false;

    String Description() default "";
}
