package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.Variable;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class VARStatement extends Statement
{
    private String variableName;
    private Expression variableValueExpression;

    private VARStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public void setVariable(String variableName, Expression variableValueExpression)
    {
        this.variableName = variableName;
        this.variableValueExpression = variableValueExpression;
    }

    public String getVariableName()
    {
        return variableName;
    }

    public Expression getVariableValueExpression()
    {
        return variableValueExpression;
    }

    public static VARStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        VARStatement newStatement;
        Token nextToken;

        newStatement = new VARStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.VAR))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.getType() == Token.VARIABLE)
            {
                String variableName;

                variableName = ((Variable)nextToken).getVariableName();
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol('='))
                {
                    Expression variableValueExpression;

                    variableValueExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                    newStatement.setVariable(variableName, variableValueExpression);

                    // Declare the method during parsing so that subsequent variable declarations can be resolved and checked.
                    parentExecutionUnit.addVariableDeclaration(variableName);
                    return newStatement;
                }
                else
                {
                    tokenizer.returnToken(nextToken);
                    newStatement.setVariable(variableName, null);

                    // Declare the method during parsing so that subsequent variable declarations can be resolved and checked.
                    parentExecutionUnit.addVariableDeclaration(variableName);
                    return newStatement;
                }
            }
            else throw new EPICSyntaxException("'(' character missing in variable statement.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of variable declaration.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        parentExecutionUnit.declareVariable(variableName, variableValueExpression == null ? null : variableValueExpression.evaluate(parentExecutionUnit, contextObject));
        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
