package com.pilog.epic.expressions;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * This class implements the basic set of parseExpression FUNCTIONS.  The tokenizer
 * will scan the input for one of the strings in <i>FUNCTIONS[]</i> and if
 * it finds it, it returns a token of FUNCTION with its value being that of
 * the index of the string into the array. (which are convientiently mapped
 * into named constants).
 *
 * Parsing of the arguments to to the function are left up to the parse
 * method in this class.
 *
 * @author Bouwer du Preez
 */
public class FunctionExpression extends DefaultExpression
{
    private final ArrayList<Expression> arguments;
    private final int functionOperator;

    public static final String[] FUNCTIONS =
    {
        "MAX", // 0.
        "MIN", // 1.
        "SIZE", // 2.
        "STR", // 3.
        "INDEXOF", // 4.
        "SUBSTR", // 5.
        "UPPER", // 6.
        "LOWER", // 7.
        "REPLACE", // 8.
    };

    // Valid Function parseExpression types.
    private final static int MAX = 0;
    private final static int MIN = 1;
    private final static int SIZE = 2;
    private final static int STR = 3;
    private final static int INDEXOF = 4;
    private final static int SUBSTR = 5;
    private final static int UPPER = 6;
    private final static int LOWER = 7;
    private final static int REPLACE = 8;

    /**
     * Create a unary parseExpression.
     *
     * @param operator The parseExpression operator.
     * @param expression2 The operand parseExpression.
     * @throws EPICSyntaxException
     */
    public FunctionExpression(int operator) throws EPICSyntaxException
    {
        arguments = new ArrayList<>();
        functionOperator = operator;
    }

    /**
     * Adds an argument to the list of arugments used during evaluation of this
 function parseExpression.
     *
     * @param argument The argument to add.
     */
    public void addArgument(Expression argument)
    {
        arguments.add(argument);
    }

    /**
     * Returns a String representation of this DefaultExpression.
     *
     * @return A String representation of this DefaultExpression.
     */
    @Override
    public String toString()
    {
        return "FunctionExpression: '" + unparse() + "'";
    }

    /**
     * Reconstructs the DefaultExpression from the parsed tree.  Useful for diagnosing
     * parsing problems.
     *
     * @return The String reconstructed from this DefaultExpression.
     */
    @Override
    public String unparse()
    {
        StringBuffer stringBuffer;

        stringBuffer = new StringBuffer();
        stringBuffer.append(FUNCTIONS[functionOperator].toUpperCase());
        stringBuffer.append("(");

        for (int argumentIndex = 0; argumentIndex < arguments.size(); argumentIndex++)
        {
            stringBuffer.append(arguments.get(argumentIndex).unparse());
            if (argumentIndex < arguments.size()-1)
            {
                stringBuffer.append(", ");
            }
        }

        stringBuffer.append(")");

        return stringBuffer.toString();
    }

    /**
     * Returns the evaluated value of this parseExpression.
     *
     * @param parentExecutionUnit The ExpressionEvaluator executing this parseExpression.
     * @return The result of this parseExpression.
     * @throws EPICRuntimeException
     */
    @Override
    public Object evaluate(ExecutionUnit parentExecutionUnit, Object contextObject) throws EPICRuntimeException
    {
        try
        {
            switch (functionOperator)
            {
                case MAX:
                {
                    ArrayList values;

                    values = new ArrayList();
                    for (Expression argument : arguments)
                    {
                        values.add(argument.evaluate(parentExecutionUnit, contextObject));
                    }

                    return Collections.max(values);
                }
                case MIN:
                {
                    ArrayList values;

                    values = new ArrayList();
                    for (Expression argument : arguments)
                    {
                        values.add(argument.evaluate(parentExecutionUnit, contextObject));
                    }

                    return Collections.min(values);
                }
                case SIZE:
                {
                    Object value = arguments.get(0).evaluate(parentExecutionUnit, contextObject);
                    if (value instanceof String)
                    {
                        return value != null ? ((String)value).length() : 0;
                    }
                    else if (value instanceof Collection)
                    {
                        return value != null ? ((Collection)value).size() : 0;
                    }
                    else throw new EPICRuntimeException("The function SIZE can only be applied to String types and collections.", lineNumber);
                }
                case STR:
                {
                    Object value = arguments.get(0).evaluate(parentExecutionUnit, contextObject);
                    return value != null ? value.toString() : "null";
                }
                case INDEXOF:
                {
                    String stringValue;
                    String indexValue;

                    stringValue = (String)arguments.get(0).evaluate(parentExecutionUnit, contextObject);
                    indexValue = (String)arguments.get(1).evaluate(parentExecutionUnit, contextObject);

                    return stringValue.indexOf(indexValue);
                }
                case SUBSTR:
                {
                    String stringValue;
                    int startIndex;
                    int endIndex;

                    stringValue = (String)arguments.get(0).evaluate(parentExecutionUnit, contextObject);
                    startIndex = ((Number)arguments.get(1).evaluate(parentExecutionUnit, contextObject)).intValue();
                    endIndex = ((Number)arguments.get(2).evaluate(parentExecutionUnit, contextObject)).intValue();

                    return stringValue.substring(startIndex, endIndex);
                }
                case UPPER:
                {
                    String stringValue;

                    stringValue = (String)arguments.get(0).evaluate(parentExecutionUnit, contextObject);
                    return stringValue.toUpperCase();
                }
                case LOWER:
                {
                    String stringValue;

                    stringValue = (String)arguments.get(0).evaluate(parentExecutionUnit, contextObject);
                    return stringValue.toLowerCase();
                }
                case REPLACE:
                {
                    String stringValue;
                    String oldValue;
                    String newValue;

                    stringValue = (String)arguments.get(0).evaluate(parentExecutionUnit, contextObject);
                    oldValue = (String)arguments.get(1).evaluate(parentExecutionUnit, contextObject);
                    newValue = (String)arguments.get(2).evaluate(parentExecutionUnit, contextObject);

                    return stringValue.replace(oldValue, newValue);
                }
                default:
                {
                    return "Function not implemented yet.";
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            if (e instanceof EPICRuntimeException)
            {
                throw (EPICRuntimeException)e;
            }
            else
            {
                throw new EPICRuntimeException("Arithmetic Exception.", lineNumber);
            }
        }
    }

    /**
     * Parse a function argument. This code pulls off the '(' and ')' around the
     * arguments passed to the function and parses them.
     *
     * @param operator The operator (type) of the parseExpression to parse.
     * @param tokenizer The tokenizer from which parsed tokens will be
     * extracted.
     * @return The parsed FunctionExpression.
     * @throws EPICSyntaxException
     */
    public static FunctionExpression parse(int operator, LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        Token nextToken;

        // Check whether the parseExpression starts with an opening bracket.
        nextToken = tokenizer.nextToken();
        if (nextToken.isSymbol('('))
        {
            FunctionExpression result;

            // Create a new function parseExpression.
            result = new FunctionExpression(operator);

            while (!(nextToken = tokenizer.nextToken()).isSymbol(')'))
            {
                // Return the next token so it can be parsed as an parseExpression.
                tokenizer.returnToken(nextToken);

                // Parse the next token as an parseExpression and add the result as a function argument.
                result.addArgument(ExpressionParser.parseExpression(tokenizer, parentExecutionUnit));

                // Check the next token to see if it is a comma.
                nextToken = tokenizer.nextToken();
                if (!nextToken.isSymbol(',')) tokenizer.returnToken(nextToken);
            }

            return result;
        }
        else throw new EPICSyntaxException("No function arguments found.", tokenizer.getLineNumber());
    }
}
