package com.pilog.epic;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class ParserContext
{
    private final HashSet<String> variableDeclarations;
    private final List<ExtensionProcedure> methodImports;
    private final HashMap<String, Class> classImports;
    private final List<ExternalExpressionParser> externalExpressionParsers;

    public ParserContext()
    {
        this.variableDeclarations = new HashSet<String>();
        this.methodImports = new ArrayList<ExtensionProcedure>();
        this.classImports = new HashMap<String, Class>();
        this.externalExpressionParsers = new ArrayList<>();
    }

    public void addExternalExpressionParser(ExternalExpressionParser parser)
    {
        externalExpressionParsers.add(parser);
    }

    public void addExternalExpressionParsers(List<ExternalExpressionParser> parsers)
    {
        externalExpressionParsers.addAll(parsers);
    }

    public ExternalExpressionParser getExternalExpressionEvaluator(String expression)
    {
        for (ExternalExpressionParser parser : externalExpressionParsers)
        {
            if (parser.isApplicableTo(expression))
            {
                return parser;
            }
        }

        return null;
    }

    public List<ExternalExpressionParser> getExternalExpressionParsers()
    {
        return new ArrayList<>(externalExpressionParsers);
    }

    public void clearVariableDeclarations()
    {
        variableDeclarations.clear();
    }

    public void setVariableDeclarations(Collection<String> variableNames)
    {
        variableDeclarations.clear();
        if (variableNames != null)
        {
            variableDeclarations.addAll(variableNames);
        }
    }

    public void addVariableDeclaration(String variableName)
    {
        variableDeclarations.add(variableName);
    }

    public void addVariableDeclarations(Collection<String> variableNames)
    {
        variableDeclarations.addAll(variableNames);
    }

    public HashSet<String> getVariableDeclarations()
    {
        return variableDeclarations;
    }

    public void addClassImport(String className, Class declaredClass)
    {
        classImports.put(className, declaredClass);
    }

    public HashMap<String, Class> getClassImports()
    {
        return classImports;
    }

    public void addMethodImport(String procedureName, Class classReference, String methodName)
    {
        ExtensionProcedure extensionProcedure;

        extensionProcedure = new ExtensionProcedure(procedureName, new ExtensionMethodStub(classReference, methodName));
        methodImports.add(extensionProcedure);
    }

    public void addMethodImport(String procedureName, Object contextObject, Method method)
    {
        ExtensionProcedure extensionProcedure;

        extensionProcedure = new ExtensionProcedure(procedureName, new ExtensionMethodStub(contextObject, method));
        methodImports.add(extensionProcedure);
    }

    public List<ExtensionProcedure> getMethodImports()
    {
        return methodImports;
    }
}
