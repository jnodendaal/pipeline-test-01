package com.pilog.epic;

/**
 * @author Bouwer du Preez
 */
public class Variable extends Token
{
    /**
     * Create a reference or scalar symbol table entry for this variable.
     *
     * @param variableName The name of the new variable.
     */
    public Variable(String variableName)
    {
        super(Token.VARIABLE, variableName);
    }

    /**
     * Returns this variable's name.
     *
     * @return This variable's name.
     */
    public String getVariableName()
    {
        return (String)value;
    }
}
