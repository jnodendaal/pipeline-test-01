package com.pilog.epic;

/**
 * This class is the representation of a single expression token.
 *
 * @author Bouwer du Preez
 */
public class Token
{
    protected int type; // The token's type.
    protected Object value;

    protected final static String names[] =
    {
        "symbol",
        "operator",
        "constant",
        "string",
        "character",
        "variable",
        "keyword",
        "function",
        "procedure",
        "error",
        "identifier",
        "collection",
        "ext_expression",
        "eol",
    };

    public final static int SYMBOL = 0;
    public final static int OPERATOR = 1;
    public final static int CONSTANT = 2;
    public final static int STRING = 3;
    public final static int CHARACTER = 4;
    public final static int VARIABLE = 5;
    public final static int KEYWORD = 6;
    public final static int FUNCTION = 7;
    public final static int PROCEDURE = 8;
    public final static int ERROR = 9;
    public final static int IDENTIFIER = 10;
    public final static int COLLECTION = 11;
    public final static int EXT_EXPRESSION = 12;
    public final static int EOL = 13;

    /**
     * Constructor a Token with a numeric value.
     *
     * @param type The type of the new Token to construct.
     * @param numericValue The numeric value of the new Token to construct.
     */
    public Token(int type, Object value)
    {
        this.type = type;
        this.value = value;
    }

    /**
     * Returns this token's value.
     *
     * @return This token's value.
     */
    public Object getValue()
    {
        return value;
    }

    public char getValueAsCharacter()
    {
        return (char)((Number)value).intValue();
    }

    /**
     * Returns this token's numeric code (if it has one).
     *
     * @return This token's numeric code.
     */
    public int getType()
    {
        return type;
    }

    /**
     * Returns a description of this Token.
     *
     * @return A description of this Token.
     */
    @Override
    public String toString()
    {
        if (type == Token.SYMBOL)
        {
            return ("TOKEN: Type = " + names[type] + ", Value = " + (char)((Number)getValue()).intValue());
        }
        else
        {
            return ("TOKEN: Type = " + names[type] + ", Value = " + getValue());
        }
    }

    /**
     * Returns true if the supplied character is a Symbol of this Token.
     *
     * @param character The character to compare to this Token.
     * @return True if the supplied character is a Symbol of this Token.
     */
    public final boolean isSymbol(char character)
    {
        return ((type == SYMBOL) && (((Number)value).intValue() == character));
    }

    /**
     * Compares this Token to the supplied operator code and returns the result.
     *
     * @param operator The operator code to compare to this Token.
     * @return True if this Token represents the supplied operator code.
     */
    public final boolean isOperator(int operator)
    {
        return ((type == OPERATOR) && (((Number)getValue()).intValue() == operator));
    }

    /**
     * Compares this Token to the supplied keyword code and returns the result.
     *
     * @param operator The keyword code to compare to this Token.
     * @return True if this Token represents the supplied keyword code.
     */
    public final boolean isKeyword(int keyword)
    {
        return ((type == KEYWORD) && (((Number)getValue()).intValue() == keyword));
    }

    public final boolean isKeyword()
    {
        return type == KEYWORD;
    }

    public final boolean isExternalExpression()
    {
        return type == EXT_EXPRESSION;
    }

    /**
     * Returns a boolean value indicating whether or not this token is a
     * procedure name.
     *
     * @return True if this token represents a procedure name, false otherwise.
     */
    public final boolean isProcedureName()
    {
        return (type == PROCEDURE);
    }

    public final boolean isVariable()
    {
        return (type == VARIABLE);
    }

    public final boolean isIdentifier()
    {
        return (type == IDENTIFIER);
    }

    public final boolean isCollection()
    {
        return (type == COLLECTION);
    }
}
