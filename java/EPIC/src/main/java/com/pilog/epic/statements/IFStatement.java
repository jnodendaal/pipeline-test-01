package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import java.util.ArrayList;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class IFStatement extends Statement
{
    private final ArrayList<Expression> conditions;
    private final ArrayList<ExecutionUnit> executionUnits;

    private IFStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
        conditions = new ArrayList<>();
        executionUnits = new ArrayList<ExecutionUnit>();
    }

    public void addClause(Expression condition, ExecutionUnit executionUnit)
    {
        conditions.add(condition);
        executionUnits.add(executionUnit);
    }

    public static IFStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        IFStatement newStatement;
        Token nextToken;

        newStatement = new IFStatement(parentExecutionUnit, tokenizer.getLineNumber());
        while (true)
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isKeyword(Statement.IF))
            {
                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol('('))
                {
                    Expression condition;

                    condition = ExpressionParser.parseExpression(tokenizer, newStatement);
                    nextToken = tokenizer.nextToken();
                    if (nextToken.isSymbol(')'))
                    {
                        ExecutionUnit executionUnit;

                        executionUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newStatement);
                        newStatement.addClause(condition, executionUnit);

                        // If the next token is an ELSE keyword, continue parsing, else just return the statement.
                        nextToken = tokenizer.nextToken();
                        if (nextToken.isKeyword(Statement.ELSE))
                        {
                            tokenizer.returnToken(nextToken);
                            // Do nothing, the loop will catch the ELSE.
                        }
                        else
                        {
                            // Return the token and then the finished statement.
                            tokenizer.returnToken(nextToken);
                            return newStatement;
                        }
                    }
                    else throw new EPICSyntaxException("Missing closing ')' in if statement.", tokenizer.getLineNumber());
                }
                else throw new EPICSyntaxException("'(' character missing in if statement.", tokenizer.getLineNumber());
            }
            else if (nextToken.isKeyword(Statement.ELSE))
            {
                nextToken = tokenizer.nextToken();
                if (nextToken.isKeyword(Statement.IF))
                {
                    tokenizer.returnToken(nextToken); // Return the token because so that it can be parsed as the next if clause on the following iteration of the while loop.
                }
                else
                {
                    ExecutionUnit executionUnit;

                    tokenizer.returnToken(nextToken);
                    executionUnit = ExecutionUnit.parseExecutionUnit(tokenizer, newStatement);
                    newStatement.addClause(null, executionUnit); // No condition for the last else clause.
                    return newStatement; // Parsing of the if statement in finished after the else clause.
                }
            }
            else
            {
                tokenizer.returnToken(nextToken);
                return newStatement;
            }
        }
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        for (int clauseIndex = 0; clauseIndex < conditions.size(); clauseIndex++)
        {
            Expression condition;

            condition = conditions.get(clauseIndex);
            if (condition == null)
            {
                ExecutionUnit executionUnit;

                executionUnit = executionUnits.get(clauseIndex);
                return executionUnit.execute(contextObject);
            }
            else
            {
                Object conditionResult;

                conditionResult = condition.evaluate(parentExecutionUnit, contextObject);
                if ((conditionResult != null) && (conditionResult instanceof Boolean) && ((Boolean)conditionResult))
                {
                    ExecutionUnit executionUnit;

                    executionUnit = executionUnits.get(clauseIndex);
                    return executionUnit.execute(contextObject);
                }
            }
        }

        return ExecutionUnit.RETURN_CODE_COMPLETED;
    }
}
