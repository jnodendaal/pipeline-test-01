package com.pilog.epic.statements;

import com.pilog.epic.ExecutionUnit;
import com.pilog.epic.LexicalTokenizer;
import com.pilog.epic.Token;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.epic.expressions.ExpressionParser;
import com.pilog.json.JsonObject;
import com.pilog.epic.expressions.Expression;

/**
 * @author Bouwer du Preez
 */
public class PARSEStatement extends Statement
{
    private Expression jsonStringExpression;

    private PARSEStatement(ExecutionUnit parentExecutionUnit, int lineNumber)
    {
        super(parentExecutionUnit, lineNumber);
    }

    public Expression getStringExpression()
    {
        return jsonStringExpression;
    }

    public void setStringExpression(Expression stringExpression)
    {
        this.jsonStringExpression = stringExpression;
    }

    public static PARSEStatement parseStatement(LexicalTokenizer tokenizer, ExecutionUnit parentExecutionUnit) throws EPICSyntaxException
    {
        PARSEStatement newStatement;
        Token nextToken;

        newStatement = new PARSEStatement(parentExecutionUnit, tokenizer.getLineNumber());
        nextToken = tokenizer.nextToken();
        if (nextToken.isKeyword(Statement.PARSE))
        {
            nextToken = tokenizer.nextToken();
            if (nextToken.isSymbol('('))
            {
                Expression stringExpression;

                stringExpression = ExpressionParser.parseExpression(tokenizer, newStatement);
                newStatement.setStringExpression(stringExpression);

                nextToken = tokenizer.nextToken();
                if (nextToken.isSymbol(')'))
                {
                    return newStatement;
                }
                else throw new EPICSyntaxException("Missing closing ')' in parse statement.", tokenizer.getLineNumber());
            }
            else throw new EPICSyntaxException("'(' character missing in parse statement.", tokenizer.getLineNumber());
        }
        else throw new EPICSyntaxException("Invalid start of parse statement.", tokenizer.getLineNumber());
    }

    @Override
    public int execute(Object contextObject) throws EPICRuntimeException
    {
        Object jsonString;

        jsonString = jsonStringExpression.evaluate(parentExecutionUnit, contextObject);
        if (jsonString instanceof String)
        {
            returnObject = JsonObject.readFrom((String)jsonString);
            return ExecutionUnit.RETURN_CODE_COMPLETED;
        }
        else throw new EPICRuntimeException("Cannot parse JSON from input object: " + jsonString, lineNumber);
    }
}
