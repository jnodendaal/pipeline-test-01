package com.pilog.epic;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

public class ReflectionTools
{
    private static final Map<String, Map<Integer, WeakReference<Method>>> resolvedMethodCache = Collections.synchronizedMap(new WeakHashMap<>(10));
    private static final Map<Class, Map<Integer, WeakReference<Constructor>>> resolvedConstantCache = Collections.synchronizedMap(new WeakHashMap<>(10));
    private static final Map<Constructor, WeakReference<Class[]>> constructorParameterCache = Collections.synchronizedMap(new WeakHashMap<>(10));
    private static final Map<Class, WeakReference<Constructor[]>> classConstructorCache = Collections.synchronizedMap(new WeakHashMap<>(10));

    public static Method getBestCandidate(Object[] arguments, String method, Class decl, Method[] methods, boolean requireExact)
    {
        Class[] targetParms = new Class[arguments.length];
        for (int i = 0; i != arguments.length; i++)
        {
            targetParms[i] = arguments[i] != null ? arguments[i].getClass() : null;
        }
        return getBestCandidate(targetParms, method, decl, methods, requireExact);
    }

    public static Method getBestCandidate(Class[] arguments, String method, Class decl, Method[] methods, boolean requireExact)
    {
        return getBestCandidate(arguments, method, decl, methods, requireExact, false);
    }

    public static Method getBestCandidate(Class[] requiredArgTypes, String methodName, Class declaredClass, Method[] methods, boolean requireExact, boolean classTarget)
    {
        Map<Integer, WeakReference<Method>> methodCache;
        WeakReference<Method> methodReference;
        Method bestCandidate = null;
        Integer classHash;
        int bestScore = 0;

        // Ensure that a valid selection of methods have been provided.
        if (methods.length == 0) return null;

        // Attempt to get the method from the cache of resolved methods.
        classHash = createClassSignatureHash(declaredClass, requiredArgTypes);
        methodCache = resolvedMethodCache.get(methodName); // Get the cache for the specific method name.
        if (methodCache != null)
        {
            methodReference = methodCache.get(classHash);
            if (methodReference != null)
            {
                bestCandidate = methodReference.get();
                if (bestCandidate != null) return bestCandidate;
            }
        }

        // Loop through all of the supplied methods and score each one according to its applicability.
        for (Method method : methods)
        {
            // Only evaluate the method further if the name matches.
            if (methodName.equals(method.getName()))
            {
                Class[] parameterTypes;
                int score = 0;

                // If we are looking for a static method and this one is not, skip it.
                if ((classTarget) && ((method.getModifiers() & Modifier.STATIC) == 0))
                {
                    continue;
                }

                // Get the parameter types of the method.
                parameterTypes = method.getParameterTypes();

                // If the number of parameter types does not match the number of required arguments, skip the method.
                if (parameterTypes.length != requiredArgTypes.length)
                {
                    if(parameterTypes.length > requiredArgTypes.length || !method.isVarArgs())
                    {
                        //It would be unnecessary to check for a method that has more arguments than was supplied.
                        continue;
                    }
                }
                else if (requiredArgTypes.length == 0 && parameterTypes.length == 0)
                {
                    // The method can be invoked without any parameters so it is an immediate match.
                    bestCandidate = method;
                    break;
                }

                // Loop through the arguments of the current method and score the method according to how well the argument types match with the required types.
                for (int argumentIndex = 0; argumentIndex != parameterTypes.length; argumentIndex++)
                {
                    if (requiredArgTypes[argumentIndex] == null)
                    {
                        if (!parameterTypes[argumentIndex].isPrimitive())
                        {
                            score += 5;
                        }
                        else
                        {
                            score = 0;
                            break;
                        }
                    }
                    else if (parameterTypes[argumentIndex] == requiredArgTypes[argumentIndex])
                    {
                        score += 6; // Best score because argument types match directly.
                    }
                    else if ((parameterTypes[argumentIndex].isPrimitive()) && (boxPrimitive(parameterTypes[argumentIndex]) == requiredArgTypes[argumentIndex]))
                    {
                        score += 5; // Good score because the argument type only needs to be boxed to match.
                    }
                    else if ((requiredArgTypes[argumentIndex].isPrimitive()) && (unboxPrimitive(requiredArgTypes[argumentIndex]) == parameterTypes[argumentIndex]))
                    {
                        score += 5; // Good score because the argument type only needs to be unboxed to match.
                    }
                    else if (isNumericallyCoercible(requiredArgTypes[argumentIndex], parameterTypes[argumentIndex]))
                    {
                        score += 4; // Decent score because the argument types are numerically coercible.
                    }
                    else if (boxPrimitive(parameterTypes[argumentIndex]).isAssignableFrom(boxPrimitive(requiredArgTypes[argumentIndex])) && Object.class != requiredArgTypes[argumentIndex])
                    {
                        score += 3 + scoreInterface(parameterTypes[argumentIndex], requiredArgTypes[argumentIndex]);
                    }
                    else if (!requireExact && canConvert(parameterTypes[argumentIndex], requiredArgTypes[argumentIndex]))
                    {
                        score += 1;
                        if (parameterTypes[argumentIndex].isArray() && requiredArgTypes[argumentIndex].isArray())
                        {
                            score += 1;
                        }
                        else if (parameterTypes[argumentIndex] == char.class && requiredArgTypes[argumentIndex] == String.class)
                        {
                            score += 1;
                        }
                    }
                    else if (requiredArgTypes[argumentIndex] == Object.class)
                    {
                        score += 1; // Low score, because matching on Object class is lowest priority.
                    }
                    else if(method.isVarArgs())
                    {
                        score += 1; // Low score because we would prefer specific methods over var arg ones
                    }
                    else
                    {
                        score = 0; // No match found, no score assigned.
                        break;
                    }
                }

                // If the score is better than the previous best, then flag the method as the best candidate found thus far.
                if (score != 0 && score > bestScore)
                {
                    bestCandidate = method;
                    bestScore = score;
                }
            }
        }

        if (bestCandidate != null)
        {
            if (methodCache == null)
            {
                resolvedMethodCache.put(methodName, methodCache = new WeakHashMap<Integer, WeakReference<Method>>());
            }

            methodCache.put(classHash, new WeakReference<Method>(bestCandidate));
        }

        return bestCandidate;
    }

    private static boolean canConvert(Class toType, Class convertFrom)
    {
        return false;
    }

    private static <T> T convert(Object in, Class<T> toType)
    {
        return null;
    }

    private static int scoreInterface(Class parm, Class arg)
    {
        if (parm.isInterface())
        {
            Class[] iface = arg.getInterfaces();
            if (iface != null)
            {
                for (Class c : iface)
                {
                    if (c == parm)
                    {
                        return 1;
                    }
                    else
                    {
                        if (parm.isAssignableFrom(c))
                        {
                            return scoreInterface(parm, arg.getSuperclass());
                        }
                    }
                }
            }
        }
        return 0;
    }

    private static Method getExactMatch(String methodName, Class[] arguments, Class returnType, Class cls)
    {
        for (Method meth : cls.getMethods())
        {
            if (methodName.equals(meth.getName()) && returnType == meth.getReturnType())
            {
                Class[] parameterTypes = meth.getParameterTypes();
                if (parameterTypes.length != arguments.length)
                {
                    continue;
                }

                for (int i = 0; i < parameterTypes.length; i++)
                {
                    if (parameterTypes[i] != arguments[i])
                    {
                        return null;
                    }
                }
                return meth;
            }
        }
        return null;
    }

    private static Class[] getConstructorParameterTypes(Constructor constructor)
    {
        WeakReference<Class[]> parameterReference = constructorParameterCache.get(constructor);
        Class[] parameterTypes;

        if (parameterReference != null && (parameterTypes = parameterReference.get()) != null)
        {
            return parameterTypes;
        }
        else
        {
            constructorParameterCache.put(constructor, new WeakReference<Class[]>(parameterTypes = constructor.getParameterTypes()));
            return parameterTypes;
        }
    }

    public static Constructor getBestConstructorCandidate(Object[] args, Class cls, boolean requireExact)
    {
        Class[] arguments;

        arguments = new Class[args.length];
        for (int argumentIndex = 0; argumentIndex != args.length; argumentIndex++)
        {
            if (args[argumentIndex] != null)
            {
                arguments[argumentIndex] = args[argumentIndex].getClass();
            }
        }

        return getBestConstructorCandidate(arguments, cls, requireExact);
    }

    private static Constructor getBestConstructorCandidate(Class[] arguments, Class cls, boolean requireExact)
    {
        Map<Integer, WeakReference<Constructor>> cache = resolvedConstantCache.get(cls);
        Integer hash = createClassSignatureHash(cls, arguments);
        WeakReference<Constructor> ref;
        Class[] parmTypes;
        Constructor bestCandidate = null;
        int bestScore = 0;

        if (cache != null && (ref = cache.get(hash)) != null && (bestCandidate = ref.get()) != null)
        {
            return bestCandidate;
        }

        for (Constructor construct : getConstructors(cls))
        {
            int score = 0;

            if ((parmTypes = getConstructorParameterTypes(construct)).length != arguments.length)
            {
                continue;
            }
            else if (arguments.length == 0 && parmTypes.length == 0)
            {
                return construct;
            }

            for (int i = 0; i != arguments.length; i++)
            {
                if (arguments[i] == null)
                {
                    if (!parmTypes[i].isPrimitive())
                    {
                        score += 5;
                    }
                    else
                    {
                        score = 0;
                        break;
                    }
                }
                else if (parmTypes[i] == arguments[i])
                {
                    score += 6;
                }
                else if (parmTypes[i].isPrimitive() && boxPrimitive(parmTypes[i]) == arguments[i])
                {
                    score += 5;
                }
                else if (arguments[i].isPrimitive() && unboxPrimitive(arguments[i]) == parmTypes[i])
                {
                    score += 5;
                }
                else if (isNumericallyCoercible(arguments[i], parmTypes[i]))
                {
                    score += 4;
                }
                else if (boxPrimitive(parmTypes[i]).isAssignableFrom(boxPrimitive(arguments[i])) && parmTypes[i] != Object.class)
                {
                    score += 3 + scoreInterface(parmTypes[i], arguments[i]);
                }
                else if (!requireExact && canConvert(parmTypes[i], arguments[i]))
                {
                    if (parmTypes[i].isArray() && arguments[i].isArray())
                    {
                        score += 1;
                    }
                    else if (parmTypes[i] == char.class && arguments[i] == String.class)
                    {
                        score += 1;
                    }

                    score += 1;
                }
                else if ((arguments[i] == Object.class) || (parmTypes[i] == Object.class))
                {
                    score += 1;
                }
                else
                {
                    score = 0;
                    break;
                }
            }

            // If this constructor has the best score found so far, flag it as the best candidate.
            if ((score != 0) && (score > bestScore))
            {
                bestCandidate = construct;
                bestScore = score;
            }
        }

        // Cache the resolved constructor.
        if (bestCandidate != null)
        {
            if (cache == null)
            {
                resolvedConstantCache.put(cls, cache = new WeakHashMap<Integer, WeakReference<Constructor>>());
            }
            cache.put(hash, new WeakReference<Constructor>(bestCandidate));
        }

        return bestCandidate;
    }

    public static Constructor[] getConstructors(Class cls)
    {
        WeakReference<Constructor[]> ref = classConstructorCache.get(cls);
        Constructor[] cns;

        if (ref != null && (cns = ref.get()) != null)
        {
            return cns;
        }
        else
        {
            classConstructorCache.put(cls, new WeakReference<Constructor[]>(cns = cls.getConstructors()));
            return cns;
        }
    }

    private static Class boxPrimitive(Class cls)
    {
        if (cls == int.class || cls == Integer.class)
        {
            return Integer.class;
        }
        else if (cls == int[].class || cls == Integer[].class)
        {
            return Integer[].class;
        }
        else if (cls == char.class || cls == Character.class)
        {
            return Character.class;
        }
        else if (cls == char[].class || cls == Character[].class)
        {
            return Character[].class;
        }
        else if (cls == long.class || cls == Long.class)
        {
            return Long.class;
        }
        else if (cls == long[].class || cls == Long[].class)
        {
            return Long[].class;
        }
        else if (cls == short.class || cls == Short.class)
        {
            return Short.class;
        }
        else if (cls == short[].class || cls == Short[].class)
        {
            return Short[].class;
        }
        else if (cls == double.class || cls == Double.class)
        {
            return Double.class;
        }
        else if (cls == double[].class || cls == Double[].class)
        {
            return Double[].class;
        }
        else if (cls == float.class || cls == Float.class)
        {
            return Float.class;
        }
        else if (cls == float[].class || cls == Float[].class)
        {
            return Float[].class;
        }
        else if (cls == boolean.class || cls == Boolean.class)
        {
            return Boolean.class;
        }
        else if(cls == boolean[].class || cls == Boolean[].class)
        {
            return Boolean[].class;
        }
        else if(cls == byte.class || cls == Byte.class)
        {
            return Byte.class;
        }
        else if (cls == byte[].class || cls == Byte[].class)
        {
            return Byte[].class;
        }
        else return cls;
    }

    private static Class unboxPrimitive(Class cls)
    {
        if (cls == Integer.class || cls == int.class)
        {
            return int.class;
        }
        else if (cls == Integer[].class || cls == int[].class)
        {
            return int[].class;
        }
        else if (cls == Long.class || cls == long.class)
        {
            return long.class;
        }
        else if (cls == Long[].class || cls == long[].class)
        {
            return long[].class;
        }
        else if (cls == Character.class || cls == char.class)
        {
            return char.class;
        }
        else if (cls == Character[].class || cls == char[].class)
        {
            return char[].class;
        }
        else if (cls == Short.class || cls == short.class)
        {
            return short.class;
        }
        else if (cls == Short[].class || cls == short[].class)
        {
            return short[].class;
        }
        else if (cls == Double.class || cls == double.class)
        {
            return double.class;
        }
        else if (cls == Double[].class || cls == double[].class)
        {
            return double[].class;
        }
        else if (cls == Float.class || cls == float.class)
        {
            return float.class;
        }
        else if (cls == Float[].class || cls == float[].class)
        {
            return float[].class;
        }
        else if (cls == Boolean.class || cls == boolean.class)
        {
            return boolean.class;
        }
        else if (cls == Boolean[].class || cls == boolean[].class)
        {
            return boolean[].class;
        }
        else if (cls == Byte.class || cls == byte.class)
        {
            return byte.class;
        }
        else if (cls == Byte[].class || cls == byte[].class)
        {
            return byte[].class;
        }
        else return cls;
    }

    private static int createClassSignatureHash(Class declaring, Class[] sig)
    {
        int hashValue = 0;

        for (Class cls : sig)
        {
            if (cls != null)
            {
                hashValue += cls.hashCode();
            }
        }

        return hashValue + sig.length + declaring.hashCode();
    }

    /**
     * This method simply checks whether both the target class and the parameter
     * class supplied are of a numeric type.
     *
     * @param targetClass The class to which the parameter is to be coerced.
     * @param parameterClass The parameter class that will be coerced.
     * @return true if the parameter class can be numerically coerced to the
     * specified target class.  This is true if both classes is of a numeric
     * type.
     */
    public static boolean isNumericallyCoercible(Class targetClass, Class parameterClass)
    {
        Class boxedTarget;

        // Box the target class if necessary.
        boxedTarget = targetClass.isPrimitive() ? boxPrimitive(targetClass) : targetClass;
        if ((boxedTarget != null) && (Number.class.isAssignableFrom(targetClass)))
        {
            Class boxedParameter;

            // Box the parameter class if necessary.
            boxedParameter = parameterClass.isPrimitive() ? boxPrimitive(parameterClass) : parameterClass;
            if (boxedParameter != null)
            {
                return Number.class.isAssignableFrom(boxedParameter);
            }
        }

        return false;
    }
}
