package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class EPICVersion
{
    public final static String VERSION = "204";
}
