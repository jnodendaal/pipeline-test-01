package com.pilog.xml;

import com.pilog.t8.utilities.strings.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Bouwer du Preez
 */
public class XmlElement implements Serializable
{
    private String namespacePrefix;
    private String localName;
    private String text;
    private String cData;
    private XmlElement parentElement;
    private final LinkedHashMap<String, String> attributes;
    private final List<XmlElement> elements;

    public XmlElement(String namespacePrefix, String localName)
    {
        this.namespacePrefix = namespacePrefix;
        this.localName = localName;
        this.attributes = new LinkedHashMap<>(3); // Initial capacity of three is estimated to be suitable in most cases.
        this.elements = new ArrayList<>();
    }

    public XmlElement(Element element)
    {
        this(element.getPrefix(), element.getLocalName());
        constructContent(element);
    }

    private void constructContent(Element element)
    {
        NodeList childNodes;

        childNodes = element.getChildNodes();
        for (int childIndex = 0; childIndex < childNodes.getLength(); childIndex++)
        {
            Node childNode;
            short nodeType;

            childNode = childNodes.item(childIndex);
            nodeType = childNode.getNodeType();
            switch (nodeType)
            {
                case Node.ELEMENT_NODE:
                    addElement(new XmlElement((Element)childNode));
                    break;
                case Node.ATTRIBUTE_NODE:
                    Attr attribute;

                    attribute = (Attr)childNode;
                    addAttribute(attribute.getName(), attribute.getValue());
                    break;
                case Node.TEXT_NODE:
                    String childText;

                    childText = childNode.getTextContent();
                    if (childText != null)
                    {
                        appendText(childText);
                    }
                    break;
                default:
                    // Nothing to do, we just don't handle other node types.
            }
        }
    }

    private void setParentElement(XmlElement element)
    {
        this.parentElement = element;
    }

    public String getNamespacePrefix()
    {
        return namespacePrefix;
    }

    public void setNamespacePrefix(String namespacePrefix)
    {
        this.namespacePrefix = namespacePrefix;
    }

    public String getLocalName()
    {
        return localName;
    }

    public void setLocalName(String localName)
    {
        this.localName = localName;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public void appendText(String text)
    {
        if (this.text == null)
        {
            this.text = text;
        }
        else
        {
            this.text += text;
        }
    }

    public String getCData()
    {
        return cData;
    }

    public void setCData(String cData)
    {
        this.cData = cData;
    }

    public void appendCData(String cData)
    {
        if (this.cData == null)
        {
            this.cData = cData;
        }
        else
        {
            this.cData += cData;
        }
    }

    public LinkedHashMap<String, String> getAttributes()
    {
        return new LinkedHashMap<String, String>(attributes);
    }

    public String getAttribute(String localName)
    {
        return attributes.get(localName);
    }

    public String addAttribute(String localName, String value)
    {
        return attributes.put(localName, value);
    }

    public void addElement(XmlElement newElement)
    {
        newElement.setParentElement(this);
        elements.add(newElement);
    }
    public XmlElement addElement(String localName)
    {
        XmlElement newElement;

        newElement = new XmlElement(namespacePrefix, localName);
        newElement.setParentElement(newElement);
        elements.add(newElement);
        return newElement;
    }

    public XmlElement addElement(String namespacePrefix, String localName)
    {
        XmlElement newElement;

        newElement = new XmlElement(namespacePrefix, localName);
        newElement.setParentElement(newElement);
        elements.add(newElement);
        return newElement;
    }

    public XmlElement getElement(String localName)
    {
        for (XmlElement element : elements)
        {
            if (Objects.equals(element.getLocalName(), localName))
            {
                return element;
            }
        }

        return null;
    }

    public String getElementText(String localName)
    {
        XmlElement element;

        element = getElement(localName);
        return element != null ? element.getText() : null;
    }

    public List<String> getElementTextFromPath(List<String> localNames)
    {
        List<String> textList;
        List<XmlElement> elementList;

        textList = new ArrayList<>();
        elementList = new ArrayList<>(elements);
        for (int nameIndex = 0; nameIndex < localNames.size(); nameIndex++)
        {
            List<XmlElement> nextElementList;
            String currentName;
            String nextName;

            nextElementList = new ArrayList<>();
            currentName = localNames.get(nameIndex);
            nextName = nameIndex < (localNames.size() - 1) ? localNames.get(nameIndex + 1) : null;
            for (XmlElement nextElement : elementList)
            {
                if (nextElement.getLocalName().equals(currentName))
                {
                    if (nextName != null)
                    {
                        nextElementList.addAll(nextElement.getElements(nextName));
                    }
                    else
                    {
                        textList.add(nextElement.getText());
                    }
                }
            }

            if (nextName != null)
            {
                elementList = nextElementList;
            }
        }

        return textList;
    }

    public List<XmlElement> getElements()
    {
        return new ArrayList<>(elements);
    }

    public List<XmlElement> getElements(String localName)
    {
        List<XmlElement> resultElements;

        resultElements = new ArrayList<>();
        for (XmlElement xmlElement : elements)
        {
            if (xmlElement.getLocalName().equals(localName))
            {
                resultElements.add(xmlElement);
            }
        }

        return resultElements;
    }

    Element createElement(Document xmlDocument, Node parentNode, Map<String, String> namespaces)
    {
        String namespaceUri;
        Element elementNode;

        // Create the element.
        namespaceUri = namespaces.get(namespacePrefix);
        elementNode = xmlDocument.createElementNS(namespaceUri, localName);
        elementNode.setPrefix(namespacePrefix);

        // Add the attributes of the element.
        for (String attributeName : attributes.keySet())
        {
            Attr attributeNode;

            attributeNode = xmlDocument.createAttributeNS(null, attributeName);
            attributeNode.setNodeValue(attributes.get(attributeName));
            elementNode.setAttributeNode(attributeNode);
        }

        // Add the text of the element.
        if (!Strings.isNullOrEmpty(text)) elementNode.appendChild(xmlDocument.createTextNode(text));

        // Add the CDATA of the element.
        if (!Strings.isNullOrEmpty(cData)) elementNode.appendChild(xmlDocument.createCDATASection(cData));

        // Add all of the child elements.
        for (XmlElement element : elements)
        {
            elementNode.appendChild(element.createElement(xmlDocument, parentNode, namespaces));
        }

        // Return the completed element.
        return elementNode;
    }
}
