package com.pilog.xml;

import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author Bouwer du Preez
 */
public class XmlDocument implements Serializable
{
    private XmlElement rootElement;
    private final LinkedHashMap<String, String> namespaces;

    public XmlDocument()
    {
        this.namespaces = new LinkedHashMap<String, String>();
    }

    public void addNamespace(String prefix, String uri)
    {
        this.namespaces.put(prefix, uri);
    }

    public XmlElement getRootElement()
    {
        return rootElement;
    }

    public XmlElement addRootElement(String namespacePrefix, String localName)
    {
        rootElement = new XmlElement(namespacePrefix, localName);
        return rootElement;
    }

    public String getXml(boolean indent, int indentNumber, boolean omitDeclaration) throws Exception
    {
        TransformerFactory transformerFactory;
        Transformer transformer;
        StringWriter stringWriter;

        transformerFactory = TransformerFactory.newInstance();
        if (indent) transformerFactory.setAttribute("indent-number", indentNumber);

        transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, indent ? "yes" : "no");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, omitDeclaration ? "yes" : "no");
        stringWriter = new StringWriter();

        transformer.transform(new DOMSource(createDocument()), new StreamResult(stringWriter));
        return stringWriter.toString();
    }

    public static final Document parse(String xmlString) throws Exception
    {
        DocumentBuilderFactory documentBuilderFactory;
        DocumentBuilder documentBuilder;
        InputSource inputSource;

        // Create a document builder factory and set its properties.
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setValidating(false);
        documentBuilderFactory.setSchema(null);
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);

        // Create a new document builder using the factory and use it to parse the file content.
        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(new XMLSAXErrorHandler());
        inputSource = new InputSource(new StringReader(xmlString));
        inputSource.setEncoding("UTF-8");
        return documentBuilder.parse(inputSource);
    }

    public XmlElement setContent(Element element)
    {
        rootElement = new XmlElement(element);
        return rootElement;
    }

    public Document createDocument() throws Exception
    {
        if (rootElement != null)
        {
            DocumentBuilderFactory docFactory;
            DocumentBuilder docBuilder;
            DOMImplementation domImplementation;
            Element documentElement;
            Document newDocument;
            String documentElementName;
            String documentElementNamespace;
            String documentElementNamespacePrefix;
            Map<String, String> rootAttributes;

            // Get the document element details.
            documentElementName = rootElement.getLocalName();
            documentElementNamespacePrefix = rootElement.getNamespacePrefix();
            documentElementNamespace = namespaces.get(documentElementNamespacePrefix);

            // Create the new document.
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();
            domImplementation = docBuilder.getDOMImplementation();
            newDocument = domImplementation.createDocument(documentElementNamespace, documentElementName, null);
            documentElement = newDocument.getDocumentElement();

            // Add all document namespaces.
            for (String prefix : namespaces.keySet())
            {
                String namespaceUri;

                namespaceUri = namespaces.get(prefix);
                newDocument.getDocumentElement().setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:" + prefix, namespaceUri);
            }

            // Add the attributes of the document element.
            rootAttributes = rootElement.getAttributes();
            for (String attributeName : rootAttributes.keySet())
            {
                Attr attributeNode;

                attributeNode = newDocument.createAttributeNS(null, attributeName);
                attributeNode.setNodeValue(rootAttributes.get(attributeName));
                documentElement.setAttributeNode(attributeNode);
            }

            // Append the root element children.
            for (XmlElement element : rootElement.getElements())
            {
                documentElement.appendChild(element.createElement(newDocument, documentElement, namespaces));
            }

            // Return the completed document.
            newDocument.normalizeDocument();
            return newDocument;
        }
        else
        {
            throw new Exception("No Document Element found.");
        }
    }

    private static class XMLSAXErrorHandler implements ErrorHandler
    {
        @Override
        public void warning(SAXParseException e) throws SAXException
        {
            show("Warning", e);
            throw (e);
        }

        @Override
        public void error(SAXParseException e) throws SAXException
        {
            show("Error", e);
            throw (e);
        }

        @Override
        public void fatalError(SAXParseException e) throws SAXException
        {
            show("Fatal Error", e);
            throw (e);
        }

        private void show(String type, SAXParseException e)
        {
            System.out.println(type + ": " + e.getMessage());
            System.out.println("Line " + e.getLineNumber() + " Column " + e.getColumnNumber());
            System.out.println("System ID: " + e.getSystemId());
        }
    }
}
