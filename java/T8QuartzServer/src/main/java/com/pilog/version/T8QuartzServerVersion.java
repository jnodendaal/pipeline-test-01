package com.pilog.version;

/**
 * @author Bouwer du Preez
 */
public class T8QuartzServerVersion
{
    public final static String VERSION = "32";
}
