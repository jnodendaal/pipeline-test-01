package com.pilog.t8.quartz;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.quartz.task.T8QuartzProcessTaskDefinition;
import com.pilog.t8.definition.script.T8QuartProcessParameterCreationScriptDefinition;
import com.pilog.t8.security.T8Context;
import java.util.Map;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author Gavin Boshoff
 */
public class T8DefaultQuartzProcessTaskJob implements org.quartz.Job
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultQuartzProcessTaskJob.class);

    public static JobDataMap createJobDataMap(T8ServerContext serverInternalContext, T8QuartzProcessTaskDefinition definition)
    {
        JobDataMap jobMap = new JobDataMap();
        jobMap.put("SERVER_CONTEXT", serverInternalContext);
        jobMap.put("SERVER_INTERNAL_CONTEXT", serverInternalContext);
        jobMap.put("ORGANIZATION_IDENTIFIER", definition.getOrganizationIdentifier());
        jobMap.put("PROCESS_IDENTIFIER", definition.getProcessIdentifier());
        jobMap.put("PROCESS_PARAMETER_SCRIPT", definition.getProcessParameterScript());
        jobMap.put("TASK_DEFINITION_IDENTIFIER", definition.getPublicIdentifier());

        return jobMap;
    }

    @Override
    public void execute(JobExecutionContext jobContext) throws JobExecutionException
    {
        T8ServerContext serverContext;
        T8ServerContextScript script;
        T8DataTransaction tx = null;
        T8DataSession dataSession;
        String originalThreadName;
        Object outputObject;
        T8Context context;
        String orgId;
        String taskId;
        String processId;

        context = (T8Context)jobContext.getJobDetail().getJobDataMap().get("CONTEXT");
        orgId = (String)jobContext.getJobDetail().getJobDataMap().get("ORGANIZATION_IDENTIFIER");
        taskId = (String)jobContext.getJobDetail().getJobDataMap().get("TASK_DEFINITION_IDENTIFIER");
        processId = (String)jobContext.getJobDetail().getJobDataMap().get("PROCESS_IDENTIFIER");

        // Open a data session for this thread.
        originalThreadName = Thread.currentThread().getName();
        Thread.currentThread().setName("T8QuartzJob{" + taskId + "}");
        serverContext = context.getServerContext();

        context = serverContext.getSecurityManager().createOrganizationContext(context, orgId);
        dataSession = serverContext.getDataManager().openDataSession(context);

        try
        {
            T8QuartProcessParameterCreationScriptDefinition processParameterScript;

            processParameterScript = (T8QuartProcessParameterCreationScriptDefinition)jobContext.getJobDetail().getJobDataMap().get("PROCESS_PARAMETER_SCRIPT");
            if (processParameterScript != null)
            {
                // Begin a new transaction for the current data session
                // This will be the transaction available to the script execution
                tx = dataSession.beginTransaction();

                // Execute the script.
                script = processParameterScript.getNewScriptInstance(context);
                outputObject = script.executeScript(null);
                if (outputObject instanceof Map)
                {
                    Map<String, Object> outputParameters;

                    outputParameters = (Map<String, Object>) outputObject;

                    serverContext.getProcessManager().startProcess(context, processId, outputParameters);
                } else throw new JobExecutionException("The proccess Parameter Creation Script did not return a map that could be sent to the process " + processId);

                // Commit the transaction if necessary
                if (tx.isOpen()) tx.commit();
            }
            else
            {
                serverContext.getProcessManager().startProcess(context, processId, null);
            }
        }
        catch (Exception ex)
        {
            // Rollback the transaction if necessary
            if (tx != null && tx.isOpen()) tx.rollback();
            LOGGER.log(T8Logger.Level.ERROR, "Failed to execute scheduled task " + jobContext.getJobDetail().getKey(), ex);
            throw new JobExecutionException("Failed to execute scheduled task " + jobContext.getJobDetail().getKey(), ex);
        }
        finally
        {
            serverContext.getDataManager().closeCurrentSession();
            serverContext.getSecurityManager().destroyContext();
            Thread.currentThread().setName(originalThreadName);
        }
    }
}
