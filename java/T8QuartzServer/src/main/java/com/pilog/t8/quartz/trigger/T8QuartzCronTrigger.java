package com.pilog.t8.quartz.trigger;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceDefinition;
import com.pilog.t8.definition.quartz.trigger.T8QuartzCronTriggerDefinition;
import com.pilog.t8.quartz.trigger.T8QuartzCronTriggerEnums.MissfireInstructions;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

/**
 * @author Gavin Boshoff
 */
public class T8QuartzCronTrigger extends T8QuartzTriggerAbstract
{
    private final T8QuartzCronTriggerDefinition definition;

    /**
     * This is the main constructor that should be used to create a new instance of this class. It populates the variables of this class with those defined in the definition.
     * <p/>
     * @param serverContext The server's internal context.
     * @param definition    Th definition to be used to get the default variable values for this class.
     * @param sericeDefinition
     */
    public T8QuartzCronTrigger(T8ServerContext serverContext, T8QuartzCronTriggerDefinition definition, T8QuartzServiceDefinition sericeDefinition)
    {
        super(serverContext, definition, sericeDefinition);
        this.definition = definition;
    }

    /**
     * This method is used to build a quartz compatible trigger.
     * <p/>
     * @return Quartz Trigger.
     */
    @Override
    public Trigger buildQuartzTrigger()
    {
        TriggerBuilder<CronTrigger> trig = newTrigger()
                .withIdentity(getName(), getGroup())
                .withSchedule(getCronScheduler())
                .withDescription(getDescription())
                .withPriority(getPriority())
                .usingJobData(getJobDataMap());

        if (getStartDate() == null) trig.startNow();
        else trig.startAt(getStartDate());

        if (getEndDate() != null) trig.endAt(getEndDate());

        return trig.build();
    }

    /**
     * Convenience method to create a new CronSchedulerBuilder with the user selected missfire instruction.
     * <p/>
     * @return CronScheduleBuilder
     */
    private CronScheduleBuilder getCronScheduler()
    {
        CronScheduleBuilder schBuild = cronSchedule(this.definition.getCronExpression());
        int missfireInstruction;

        missfireInstruction = getMissFireInstruction();
        if (missfireInstruction == MissfireInstructions.MISFIRE_INSTRUCTION_DO_NOTHING.ordinal())
        {
            schBuild = schBuild.withMisfireHandlingInstructionDoNothing();
        }
        else if (missfireInstruction == MissfireInstructions.MISFIRE_INSTRUCTION_FIRE_ONCE_NOW.ordinal())
        {
            schBuild = schBuild.withMisfireHandlingInstructionFireAndProceed();
        }
        else if (missfireInstruction == MissfireInstructions.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY.ordinal())
        {
            schBuild = schBuild.withMisfireHandlingInstructionIgnoreMisfires();
        }

        return schBuild;
    }
}
