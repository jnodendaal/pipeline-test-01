package com.pilog.t8.quartz.trigger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceDefinition;
import com.pilog.t8.definition.quartz.trigger.T8QuartzSimpleTriggerDefinition;
import com.pilog.t8.quartz.trigger.T8QuartzSimpleTriggerEnums.IntervalFields;
import com.pilog.t8.quartz.trigger.T8QuartzSimpleTriggerEnums.MissfireInstructions;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * @author Gavin Boshoff
 */
public class T8QuartzSimpleTrigger extends T8QuartzTriggerAbstract
{
    private int repeatAmount;
    private double interval;
    private IntervalFields intervalField;
    private final T8QuartzSimpleTriggerDefinition definition;

    /**
     * The main constructor used to create a new instance of this class from the definition and the values defined within it.
     * <p/>
     * @param serverContext The server internal context.
     * @param definition    The definition used to construct a new instance of this class.
     * @param sericeDefinition
     */
    public T8QuartzSimpleTrigger(T8ServerContext serverContext, T8QuartzSimpleTriggerDefinition definition, T8QuartzServiceDefinition sericeDefinition)
    {
        super(serverContext, definition, sericeDefinition);
        this.definition = definition;
        this.repeatAmount = this.definition.getRepeatAmount();
        this.interval = this.definition.getInterval();
        this.intervalField = this.definition.getIntervalField();
    }

    /**
     * This method will be used by other classes such as the quartz scheduler to build the quartz compatible trigger.
     * <p/>
     * @return Quartz Trigger that was generated.
     */
    @Override
    public Trigger buildQuartzTrigger()
    {
        TriggerBuilder<SimpleTrigger> trig = newTrigger()
                .withIdentity(getName(), getGroup())
                .withSchedule(getSimpleScheduler())
                .withDescription(getDescription())
                .withPriority(getPriority())
                .usingJobData(getJobDataMap());

        if (getStartDate() == null) trig.startNow();
        else trig.startAt(getStartDate());

        if (getEndDate() != null) trig.endAt(getEndDate());

        return trig.build();
    }

    /**
     * Convenience method to build up a simple scheduler based on the user selected values.
     * It uses the enums defined in this class and converts them to Quartz appropriate methods to build up the simple trigger's
     * interval fields and misfire instructions.
     * <p/>
     * @return SimpleScheduleBuilder builder.
     */
    private SimpleScheduleBuilder getSimpleScheduler()
    {
        SimpleScheduleBuilder schBuild = simpleSchedule();
        int missfireInstruction;

        switch (getIntervalField())
        {
            case Hours:
                schBuild = schBuild.withIntervalInHours((int) getInterval());
                break;
            case Minutes:
                schBuild = schBuild.withIntervalInMinutes((int) getInterval());
                break;
            case Seconds:
                schBuild = schBuild.withIntervalInSeconds((int) getInterval());
                break;
            case MilliSeconds:
                schBuild = schBuild.withIntervalInMilliseconds((long) getInterval());
                break;
        }

        schBuild = (getRepeatAmount() > 0 ? schBuild.withRepeatCount(getRepeatAmount()) : schBuild.repeatForever());

        missfireInstruction = getMissFireInstruction();
        if (missfireInstruction == MissfireInstructions.MISFIRE_INSTRUCTION_FIRE_NOW.ordinal())
        {
            schBuild = schBuild.withMisfireHandlingInstructionFireNow();
        }
        else if (missfireInstruction == MissfireInstructions.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY.ordinal())
        {
            schBuild = schBuild.withMisfireHandlingInstructionIgnoreMisfires();
        }
        else if (missfireInstruction == MissfireInstructions.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_EXISTING_COUNT.ordinal())
        {
            schBuild = schBuild.withMisfireHandlingInstructionNextWithExistingCount();
        }
        else if (missfireInstruction == MissfireInstructions.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT.ordinal())
        {
            schBuild = schBuild.withMisfireHandlingInstructionNextWithRemainingCount();
        }
        else if (missfireInstruction == MissfireInstructions.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT.ordinal())
        {
            schBuild = schBuild.withMisfireHandlingInstructionNowWithExistingCount();
        }
        else if (missfireInstruction == MissfireInstructions.MISFIRE_INSTRUCTION_SMART_POLICY.ordinal())
        {
            schBuild = schBuild.withMisfireHandlingInstructionNowWithRemainingCount();
        }

        return schBuild;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public int getRepeatAmount()
    {
        return repeatAmount;
    }

    public void setRepeatAmount(int repeatAmount)
    {
        this.repeatAmount = repeatAmount;
    }

    public double getInterval()
    {
        return interval;
    }

    public void setInterval(double interval)
    {
        this.interval = interval;
    }

    public IntervalFields getIntervalField()
    {
        return intervalField;
    }

    public void setIntervalField(IntervalFields intervalField)
    {
        this.intervalField = intervalField;
    }
    //</editor-fold>
}
