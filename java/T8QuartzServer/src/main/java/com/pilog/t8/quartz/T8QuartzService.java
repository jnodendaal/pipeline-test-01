package com.pilog.t8.quartz;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.log.T8Logger.Level;
import com.pilog.t8.service.T8Service;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceAPIHandler;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceDefinition;
import com.pilog.t8.definition.quartz.task.T8QuartzProcessTaskDefinition;
import com.pilog.t8.definition.quartz.trigger.T8QuartzTriggerDefinition;
import com.pilog.t8.definition.service.T8ServiceDefinition;
import com.pilog.t8.quartz.trigger.IT8QuartzTrigger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.Trigger;

import static org.quartz.JobBuilder.newJob;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8QuartzService implements T8Service
{
    private static final T8Logger logger = T8Log.getLogger(T8QuartzService.class);
    private final T8ServerContext serverContext;
    private T8QuartzServiceDefinition definition;
    private Scheduler scheduler;

    public T8QuartzService(T8ServerContext serverContext, T8QuartzServiceDefinition definition)
    {
        this.serverContext = serverContext;
        this.definition = definition;
    }

    @Override
    public void init() throws Exception
    {
        logger.log("Starting Scheduler");
        if (scheduler == null)
        {
            try
            {
                scheduler = StdSchedulerFactory.getDefaultScheduler();
                scheduler.startDelayed(60);

                //load configured jobs
                for (T8QuartzProcessTaskDefinition taskDef : definition.getTasks())
                {
                    if (taskDef.isActive())
                    {
                        try
                        {

                            JobDetail jobDetail = newJob(T8DefaultQuartzProcessTaskJob.class)
                                    .withDescription(taskDef.getMetaDescription())
                                    .withIdentity(taskDef.getIdentifier(), definition.getIdentifier())
                                    .usingJobData(T8DefaultQuartzProcessTaskJob.createJobDataMap(serverContext, taskDef)).build();

                            //scheduler.addJob(jobDetail, true);
                            if (taskDef.getTriggers() == null)
                            {
                                logger.log(Level.WARNING, "No Triggers registered for task " + taskDef.getPublicIdentifier());
                            }
                            else
                            {
                                Set<Trigger> jobTriggers;

                                jobTriggers = new HashSet<>();
                                for (T8QuartzTriggerDefinition t8QuartzTriggerDefinition : taskDef.getTriggers())
                                {
                                    if (t8QuartzTriggerDefinition.isActive())
                                    {
                                        try
                                        {
                                            IT8QuartzTrigger qaurtTrig = t8QuartzTriggerDefinition.instantiateTrigger(serverContext, definition);
                                            jobTriggers.add(qaurtTrig.buildQuartzTrigger());
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.log(Level.WARNING, "Failed to load trigger " + t8QuartzTriggerDefinition.getPublicIdentifier() + " for task " + taskDef.getPublicIdentifier(), ex);
                                        }
                                    }
                                }

                                if (!jobTriggers.isEmpty()) this.scheduler.scheduleJob(jobDetail, jobTriggers, false);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.log(Level.WARNING, "Failed to load task " + taskDef.getPublicIdentifier(), ex);
                        }
                    }
                }
            }
            catch (SchedulerException ex)
            {
                logger.log(Level.ERROR, "Failed to start scheduler " + getDefinition().getIdentifier(), ex);
                throw ex;
            }
        }
    }

    @Override
    public void destroy()
    {
        if (scheduler != null)
        {
            try
            {
                while(!scheduler.isStarted())
                {
                    try
                    {
                        //Wait for scheduler to start first
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException ex)
                    {
                        logger.log("Exception while waiting for scheduler to start", ex);
                    }
                }
                logger.log("Stopping Scheduler");
                scheduler.shutdown();
                scheduler = null;
            }
            catch (SchedulerException ex)
            {
                logger.log(Level.ERROR, "Failed to stop scheduler " + getDefinition().getIdentifier(), ex);
            }
        }
    }

    @Override
    public T8ServiceDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public Map<String, Object> executeOperation(T8Context context, String operationId, Map<String, Object> operationParameters)
    {
        //Strip paramter name space
        operationParameters = T8IdentifierUtilities.stripNamespace(operationParameters);

        if (T8QuartzServiceAPIHandler.OPERATION_TRIGGER_TASK.equals(operationId))
        {
            String taskIdentifier;

            taskIdentifier = (String) operationParameters.get(T8QuartzServiceAPIHandler.PARAMETER_TASK_IDENTIFIER);
            if (Strings.isNullOrEmpty(taskIdentifier))
            {
                return null;
            }
            taskIdentifier = T8IdentifierUtilities.stripNamespace(taskIdentifier, definition.getIdentifier(), false);

            for (T8QuartzProcessTaskDefinition t8QuartzProcessTaskDefinition : definition.getTasks())
            {
                if (t8QuartzProcessTaskDefinition.getIdentifier().equals(taskIdentifier))
                {
                    try
                    {
                        scheduler.triggerJob(new JobKey(t8QuartzProcessTaskDefinition.getIdentifier(), definition.getIdentifier()));
                    }
                    catch (SchedulerException ex)
                    {
                        logger.log("Failed to trigger task " + taskIdentifier + " for immediate execution.", ex);
                    }
                }
            }
        }
        else if (T8QuartzServiceAPIHandler.OPERATION_RESTART.equals(operationId))
        {
            try
            {
                destroy();
                //Get the definition again for incase it was changed
                definition = (T8QuartzServiceDefinition) serverContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), definition.getIdentifier(), null);

                try
                {
                    init();
                }
                catch (Exception ex)
                {
                    logger.log("Failed to restart the scheduler service " + definition.getIdentifier(), ex);
                }
            }
            catch (Exception ex)
            {
                logger.log("Failed to retrieve the new definition for the scheduler service " + definition.getIdentifier(), ex);
            }
        }

        return null;
    }
}
