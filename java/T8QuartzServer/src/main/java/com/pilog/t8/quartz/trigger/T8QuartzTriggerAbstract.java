package com.pilog.t8.quartz.trigger;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.quartz.service.T8QuartzServiceDefinition;
import com.pilog.t8.definition.quartz.trigger.T8QuartzTriggerDefinition;
import java.util.Date;
import org.quartz.JobDataMap;

/**
 * The main T8 Trigger class that all triggers must implement
 * @author Gavin Boshoff
 */
public abstract class T8QuartzTriggerAbstract implements IT8QuartzTrigger
{
    private Date startDate;
    private Date endDate;
    private int priority;
    private int missFireInstruction;
    private String group;
    private String name;
    private String description;
    private JobDataMap jobDataMap;

    /**
     * The main constructor that instantiates this class with the variable values defined in the definition.
     * @param serverInternalContext The server's internal context
     * @param definition The definition to be used when assigning variable values and creating linked job instances.
     * @param serviceDefinition The service definition this trigger will be registered on
     */
    public T8QuartzTriggerAbstract(T8ServerContext serverInternalContext, T8QuartzTriggerDefinition definition, T8QuartzServiceDefinition serviceDefinition)
    {
        this.startDate = definition.getStartDate();
        this.endDate = definition.getEndDate();
        this.priority = definition.getPriority();
        this.missFireInstruction = definition.getMissFireInscruction();
        this.group = serviceDefinition.getIdentifier();
        this.name = definition.getIdentifier();
        this.description = definition.getMetaDescription();
        this.jobDataMap = new JobDataMap();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    @Override
    public int getMissFireInstruction()
    {
        return missFireInstruction;
    }

    @Override
    public void setMissFireInstruction(int missFireInstruction)
    {
        this.missFireInstruction = missFireInstruction;
    }

    @Override
    public String getGroup()
    {
        return group;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public JobDataMap getJobDataMap()
    {
        return jobDataMap;
    }

    public void setJobDataMap(JobDataMap jobDataMap)
    {
        this.jobDataMap = jobDataMap;
    }
    //</editor-fold>
}
