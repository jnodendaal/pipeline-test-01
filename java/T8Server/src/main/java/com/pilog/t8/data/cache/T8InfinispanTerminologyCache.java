package com.pilog.t8.data.cache;

import com.pilog.t8.cache.T8TerminologyCache;
import com.pilog.t8.data.document.T8ConceptTerminology;
import javax.transaction.TransactionManager;
import org.infinispan.Cache;

/**
 * @author Bouwer du Preez
 */
public class T8InfinispanTerminologyCache implements T8TerminologyCache
{
    private final Cache<String, T8ConceptTerminology> cache;

    public T8InfinispanTerminologyCache(Cache<String, T8ConceptTerminology> cache) throws Exception
    {
        this.cache = cache;
    }

    @Override
    public TransactionManager getTransactionManager()
    {
        return this.cache.getAdvancedCache().getTransactionManager();
    }

    @Override
    public void remove(Object terminology)
    {
        T8ConceptTerminology conceptTerminology;

        // Update the cache.
        conceptTerminology = (T8ConceptTerminology)terminology;
        cache.remove(getCacheKey(conceptTerminology.getLanguageId(), conceptTerminology.getConceptId()));
    }

    @Override
    public Object remove(String languageId, String conceptId)
    {
        // Update the cache.
        return cache.remove(getCacheKey(languageId, conceptId));
    }

    @Override
    public void put(Object terminology)
    {
        T8ConceptTerminology conceptTerminology;

        // Update the cache.
        conceptTerminology = (T8ConceptTerminology)terminology;
        cache.put(getCacheKey(conceptTerminology.getLanguageId(), conceptTerminology.getConceptId()), conceptTerminology);
    }

    @Override
    public Object get(String languageId, String conceptId)
    {
        return cache.get(getCacheKey(languageId, conceptId));
    }

    @Override
    public boolean containsKey(String languageId, String conceptId)
    {
        return cache.containsKey(getCacheKey(languageId, conceptId));
    }

    private String getCacheKey(String languageId, String conceptId)
    {
        return conceptId + ":" + languageId;
    }
}
