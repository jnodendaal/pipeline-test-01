package com.pilog.t8.flow.node.event;

import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.definition.flow.node.event.T8StartEventDefinition;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8StartEventNode extends T8DefaultFlowNode
{
    private final T8StartEventDefinition definition;

    public T8StartEventNode(T8Context context, T8Flow parentFlow, T8StartEventDefinition definition, String nodeIid)
    {
        super(context, parentFlow, definition, nodeIid);
        this.definition = definition;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters)
    {
        Map<String, Object> outputParameters;

        // Get the flow input parameters from the state.
        outputParameters = flowState.getInputParameters();
        if (outputParameters != null)
        {
            return outputParameters;
        }
        else
        {
            return new HashMap<String, Object>();
        }
    }
}
