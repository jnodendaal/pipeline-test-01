package com.pilog.t8.definition.event;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.language.T8LanguageDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DisplayNamePersistenceHandler extends T8DefinitionManagerEventAdapter
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DisplayNamePersistenceHandler.class);

    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final T8ConfigurationManager configurationManager;
    private final T8DisplayNamePersistenceHandlerDefinition definition;
    private final List<String> definitionGroupIds;
    private final Map<String, String> languageIDMap;
    private final String entityId;
    private final String projectId;
    private boolean initialized;

    public static final String EF_DEFINITION_ID = "$DEFINITION_ID";
    public static final String EF_LANGUAGE_ID = "$LANGUAGE_ID";
    public static final String EF_TYPE_ID = "$TYPE_ID";
    public static final String EF_DATA = "$DATA";

    public static final String DISPLAY_NAME_TYPE_ID = "DISPLAY_NAME";

    public T8DisplayNamePersistenceHandler(T8Context context, T8DisplayNamePersistenceHandlerDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definitionManager = serverContext.getDefinitionManager();
        this.configurationManager = serverContext.getConfigurationManager();
        this.definition = definition;
        this.definitionGroupIds = definition.getDefinitionGroupIdentifiers();
        this.entityId = definition.getDataEntityIdentifier();
        this.languageIDMap = new HashMap<>();
        this.projectId = definition.getRootProjectId();
        createLanguageMap();
    }

    private void createLanguageMap()
    {
        try
        {
            T8SystemDefinition systemDefinition;
            List<String> languageIds;

            languageIDMap.clear();
            systemDefinition = definitionManager.getSystemDefinition(context);
            languageIds = systemDefinition.getLanguageIds();
            if (languageIds != null)
            {
                for (String languageId : languageIds)
                {
                    T8LanguageDefinition languageDefinition;

                    languageDefinition = (T8LanguageDefinition)definitionManager.getRawDefinition(context, projectId, languageId);
                    if (languageDefinition != null)
                    {
                        String languageID;

                        languageID = languageDefinition.getContentLanguageID();
                        languageIDMap.put(languageId, languageID);
                    }
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while creating language setup.", e);
        }
    }

    @Override
    public void initHandler() throws Exception
    {
        T8DataSession ds;
        T8DataTransaction tx;

        try
        {
            // Get the transaction to use and persist the data.
            LOGGER.log("Starting Display Name Persistence Handler...");
            ds = serverContext.getDataManager().getCurrentSession();
            tx = ds.instantTransaction();
            for (String groupId : definitionGroupIds)
            {
                for (T8DefinitionMetaData metaData : serverContext.getDefinitionManager().getGroupDefinitionMetaData(definition.getRootProjectId(), groupId))
                {
                    persistDisplayName(tx, metaData);
                }
            }
            initialized = true;
        }
        catch(Exception ex)
        {
            LOGGER.log("Failed to start Display Name Persistence Handler", ex);
            initialized = false;
        }
    }

    @Override
    public void definitionSaved(T8DefinitionSavedEvent event)
    {
        T8Definition savedDefinition;

        if(initialized)
        {
            savedDefinition = event.getDefinition();
            if (definitionGroupIds.contains(savedDefinition.getTypeMetaData().getGroupId()))
            {
                try
                {
                    LOGGER.log("Persisting Display Name for definition: " + savedDefinition);
                    persistDisplayName(savedDefinition.getMetaData());
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while persisting definition display name: " + savedDefinition, e);
                }
            }
        }
    }

    private void persistDisplayName(T8DefinitionMetaData definitionMetaData) throws Exception
    {
        T8DataSession ds;
        T8DataTransaction tx;


        // Get the transaction to use and persist the data.
        ds = serverContext.getDataManager().getCurrentSession();
        tx = ds.instantTransaction();
        persistDisplayName(tx, definitionMetaData);
    }

    private void persistDisplayName(T8DataTransaction tx, T8DefinitionMetaData definitionMetaData) throws Exception
    {
        for (String languageIdentifier : languageIDMap.keySet())
        {
            T8DataEntity entity;

            // Get the transaction to use and construct a new entity.
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + EF_DEFINITION_ID, definitionMetaData.getId());
            entity.setFieldValue(entityId + EF_LANGUAGE_ID, languageIDMap.get(languageIdentifier));
            entity.setFieldValue(entityId + EF_TYPE_ID, DISPLAY_NAME_TYPE_ID);
            entity.setFieldValue(entityId + EF_DATA, configurationManager.getUITranslation(context, languageIdentifier, definitionMetaData.getDisplayName()));

            if (!tx.update(entity))
            {
                tx.insert(entity);
            }
        }
    }
}
