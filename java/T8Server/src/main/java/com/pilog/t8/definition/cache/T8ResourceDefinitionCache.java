package com.pilog.t8.definition.cache;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionResource;
import com.pilog.t8.definition.T8DefinitionSerializer;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.T8L2DefinitionCache;
import com.pilog.t8.mainserver.T8ClassScanner;
import java.io.File;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8ResourceDefinitionCache implements T8L2DefinitionCache
{
    protected T8DefinitionManager definitionManager;
    protected Map<String, String> definitionMap; // Key: Definition Identifier, Value: Serialized Definition Strings.
    protected Map<String, String> typeMap; // Key: Definition Identifier, Value: Definition Type Identifier.
    protected Map<String, T8DefinitionMetaData> metaDataMap; // Key: Definition Identifier, Value: Definition Meta Data Object.
    protected Set<String> groupIdentifierSet; // Contains the identifiers of all existing groups.
    protected Set<String> storedTypes;
    protected File contextPath;
    protected T8ServerContext serverContext;
    protected T8DefinitionSerializer serializer;

    public T8ResourceDefinitionCache(T8ServerContext serverContext, T8DefinitionManager definitionManager, Collection<String> typeIds)
    {
        this.serverContext = serverContext;
        this.definitionManager = definitionManager;
        this.serializer = new T8DefinitionSerializer(definitionManager);
        this.serializer.setSerializeResourceDefinitions(true);
        this.definitionMap = Collections.synchronizedMap(new HashMap<String, String>());
        this.typeMap = Collections.synchronizedMap(new HashMap<String, String>());
        this.metaDataMap = Collections.synchronizedMap(new HashMap<String, T8DefinitionMetaData>());
        this.groupIdentifierSet = new HashSet<String>();
        this.storedTypes = new HashSet<String>();
        if (typeIds != null) storedTypes.addAll(typeIds);
    }

    @Override
    public void init()
    {
    }

    @Override
    public void destroy()
    {
        // We want to make it easier for the GC to cleanup when re-deploying
        clearCache();
        storedTypes.clear();
        contextPath = null;
    }

    @Override
    public void setContextPath(File contextPath) throws Exception
    {
        // Nothing to do for this cache type.
    }

    @Override
    public void clearCache()
    {
        definitionMap.clear();
        typeMap.clear();
        metaDataMap.clear();
        groupIdentifierSet.clear();
    }

    @Override
    public void cacheDefinitions()
    {
        loadDefinitionResources();
        cacheDefinitionMetaData();
    }

    @Override
    public boolean containsDefinition(String identifier)
    {
        return definitionMap.containsKey(identifier);
    }

    @Override
    public boolean containsDefinitionGroup(String groupIdentifier)
    {
        return groupIdentifierSet.contains(groupIdentifier);
    }

    @Override
    public boolean loadsType(T8DefinitionTypeMetaData typeMetaData)
    {
        return (storedTypes.contains(typeMetaData.getTypeId()));
    }

    @Override
    public boolean savesType(T8DefinitionTypeMetaData typeMetaData)
    {
        return false; // Resource definition cache cannot save/update any definitions.
    }

    private void loadDefinitionResources()
    {
        T8ClassScanner classScanner;
        Set<Class> classes;

        // We were keeping all the jar file references in memory, so we change that to be cleaned after the definitions have been loaded
        classScanner = new T8ClassScanner(serverContext, T8ClassScanner.SERVER_SIDE_JAR_PATH);

        // Find only the non-abstract resource classes and add them to the definition cache.
        classes = classScanner.getPackageClasses("com.pilog.t8.definition");
        for (Class resourceClass : classes)
        {
            if (T8DefinitionResource.class.isAssignableFrom(resourceClass))
            {
                if (!Modifier.isAbstract(resourceClass.getModifiers()))
                {
                    try
                    {
                        T8DefinitionResource resource;
                        List<T8Definition> resourceDefinitionList;

                        resource = (T8DefinitionResource)resourceClass.newInstance();
                        resourceDefinitionList = resource.getResourceDefinitions(serverContext, storedTypes);
                        if (resourceDefinitionList != null)
                        {
                            for (T8Definition resourceDefinition : resourceDefinitionList)
                            {
                                if (loadsType(resourceDefinition.getTypeMetaData()))
                                {
                                    if (resourceDefinition.getLevel() == T8DefinitionLevel.RESOURCE)
                                    {
                                        if (!definitionMap.containsKey(resourceDefinition.getIdentifier()))
                                        {
                                            definitionMap.put(resourceDefinition.getIdentifier(), serializer.serializeDefinition(resourceDefinition));
                                        }
                                        else throw new Exception("Duplicate Definition: " + resourceDefinition);
                                    }
                                    else throw new Exception("Definition '" + resourceDefinition + "' loaded as resource from class '" + resourceClass.getCanonicalName() + "' but meta data indicates incorrect level: " + resourceDefinition.getLevel());
                                }
                                else throw new Exception("Resource definition '" + resourceDefinition + "' returned from class '" + resourceClass.getCanonicalName() + "' but was not one of requested types: " + storedTypes);
                            }
                        }
                    }
                    catch (Throwable e)
                    {
                        T8Log.log("Throwable while loading definition resource: " + resourceClass, e);
                    }
                }
            }
        }
    }

    private void cacheDefinitionMetaData()
    {
        metaDataMap.clear();
        for (String identifier : definitionMap.keySet())
        {
            try
            {
                T8DefinitionMetaData metaData;
                T8DefinitionTypeMetaData typeMetaData;
                T8Definition loadedDefinition;

                loadedDefinition = loadDefinition(identifier);
                metaData = loadedDefinition.getMetaData();
                typeMetaData = loadedDefinition.getTypeMetaData();

                metaDataMap.put(loadedDefinition.getIdentifier(), metaData);
                typeMap.put(loadedDefinition.getIdentifier(), typeMetaData.getTypeId());
                groupIdentifierSet.add(typeMetaData.getGroupId());
            }
            catch (Exception e)
            {
                T8Log.log("Exception while loading definition '" + identifier + "'.", e);
            }
        }
    }

    @Override
    public T8Definition loadDefinition(String identifier) throws Exception
    {
        String definitionData;

        definitionData = definitionMap.get(identifier);
        if (definitionData != null)
        {
            T8Definition rawDefinition;

            rawDefinition = serializer.deserializeDefinition(definitionData);
            rawDefinition.constructDefinition(serverContext);
            return rawDefinition;
        }
        else return null;
    }

    @Override
    public void saveDefinition(T8Definition definition) throws Exception
    {
        T8DefinitionMetaData metaData;
        T8DefinitionTypeMetaData typeMetaData;

        metaData = definition.getMetaData();
        typeMetaData = definition.getTypeMetaData();

        definitionMap.put(definition.getIdentifier(), serializer.serializeDefinition(definition));
        metaDataMap.put(definition.getIdentifier(), metaData);
        typeMap.put(definition.getIdentifier(), typeMetaData.getTypeId());
        groupIdentifierSet.add(typeMetaData.getGroupId());
    }

    @Override
    public void deleteDefinition(String identifier) throws Exception
    {
        throw new Exception("Resource definition cache cannot delete definition: " + identifier);
    }

    @Override
    public long getCachedCharacterSize()
    {
        long dataSize;

        dataSize = 0;
        for (String definitionData : definitionMap.values())
        {
            dataSize += definitionData.length();
        }

        return dataSize;
    }

    @Override
    public int getCachedDefinitionCount()
    {
        return definitionMap.size();
    }

    @Override
    public Set<String> getDefinitionIdentifiers()
    {
        return new TreeSet<String>(definitionMap.keySet());
    }

    @Override
    public Set<String> getDefinitionIdentifiers(String typeIdentifier)
    {
        Set<String> identifierSet;

        identifierSet = new HashSet<String>();
        for (String identifier : typeMap.keySet())
        {
            if (typeMap.get(identifier).equals(typeIdentifier))
            {
                identifierSet.add(identifier);
            }
        }

        return identifierSet;
    }

    @Override
    public Set<String> getGroupDefinitionIdentifiers(String groupIdentifier) throws Exception
    {
        Set<String> identifierSet;

        identifierSet = new HashSet<String>();
        for (T8DefinitionTypeMetaData typeMetaData : definitionManager.getAllDefinitionTypeMetaData())
        {
            if (typeMetaData.getGroupId().equals(groupIdentifier))
            {
                for (String identifier : typeMap.keySet())
                {
                    if (typeMap.get(identifier).equals(typeMetaData.getTypeId()))
                    {
                        identifierSet.add(identifier);
                    }
                }
            }
        }

        return identifierSet;
    }

    @Override
    public Set<String> getGroupIdentifiers()
    {
        return groupIdentifierSet;
    }

    @Override
    public T8DefinitionMetaData getDefinitionMetaData(String identifier)
    {
        return metaDataMap.get(identifier);
    }

    @Override
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String typeIdentifier)
    {
        List<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<T8DefinitionMetaData>();
        for (String identifier : typeMap.keySet())
        {
            if (typeMap.get(identifier).equals(typeIdentifier))
            {
                metaDataList.add(metaDataMap.get(identifier));
            }
        }

        return metaDataList;
    }

    @Override
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String groupIdentifier) throws Exception
    {
        ArrayList<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<T8DefinitionMetaData>();
        for (T8DefinitionTypeMetaData typeMetaData : definitionManager.getAllDefinitionTypeMetaData())
        {
            if (typeMetaData.getGroupId().equals(groupIdentifier))
            {
                for (String identifier : typeMap.keySet())
                {
                    if (typeMap.get(identifier).equals(typeMetaData.getTypeId()))
                    {
                        metaDataList.add(metaDataMap.get(identifier));
                    }
                }
            }
        }

        return metaDataList;
    }

    @Override
    public List<T8DefinitionMetaData> getAllDefinitionMetaData()
    {
        ArrayList<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<T8DefinitionMetaData>(metaDataMap.values());
        return metaDataList;
    }

    @Override
    public List<String> findDefinitionsByText(String searchText)
    {
        ArrayList<String> identifiers;

        identifiers = new ArrayList<String>();
        for (String identifier : definitionMap.keySet())
        {
            String content;

            content = definitionMap.get(identifier);
            if (content.contains(searchText))
            {
                identifiers.add(identifier);
            }
        }

        return identifiers;
    }

    @Override
    public List<String> findDefinitionsByRegularExpression(String expression)
    {
        ArrayList<String> identifiers;
        Pattern pattern;

        pattern = Pattern.compile(expression);
        identifiers = new ArrayList<String>();
        for (String identifier : definitionMap.keySet())
        {
            Matcher matcher;
            String content;

            content = definitionMap.get(identifier);
            matcher = pattern.matcher(content);
            if (matcher.find())
            {
                identifiers.add(identifier);
            }
        }

        return identifiers;
    }

    @Override
    public List<String> findDefinitionsByIdentifier(String identifier)
    {
        List<String> results;
        String globalPart;
        String localPart;

        // Find all definitions containing the identifier (references).
        results = findDefinitionsByRegularExpression("[^@\\$0-9a-zA-Z_]" + Pattern.quote(identifier) + "[^0-9a-zA-Z_]");

        // Also try to find the definition itself (this will not be picked up by the previous step).
        localPart = T8IdentifierUtilities.getLocalIdentifierPart(identifier);
        globalPart = T8IdentifierUtilities.getGlobalIdentifierPart(identifier);
        if ((globalPart != null) && (containsDefinition(globalPart)) && (!results.contains(globalPart)))
        {
            try
            {
                T8Definition containingDefinition;

                // Load the definition using the global part of the identifier and then see if it contains the local definition specified.
                containingDefinition = loadDefinition(globalPart);
                if (containingDefinition.getLocalDefinition(localPart) != null)
                {
                    results.add(globalPart);
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while loading definition: " + globalPart, e);
            }
        }

        // Return the results.
        return results;
    }

    @Override
    public List<String> findDefinitionsByDatum(String datumIdentifier, T8DataType dataType, String content)
    {
        StringBuffer searchText;

        searchText = new StringBuffer();
        if (datumIdentifier != null) searchText.append(datumIdentifier.toUpperCase());
        searchText.append(" TYPE=\"" + dataType + "\">" + content + "<");
        return findDefinitionsByText(searchText.toString());
    }
}
