package com.pilog.t8.data.connection.pool;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.definition.data.connection.pool.T8BoneCPDataConnectionPoolDefinition;
import java.sql.Connection;
import java.sql.SQLException;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;

/**
 * @author Bouwer du Preez
 */
public class T8BoneCPDataConnectionPool implements T8DataConnectionPool
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8BoneCPDataConnectionPool.class);

    private final T8BoneCPDataConnectionPoolDefinition definition;
    private final boolean retryOnFailure;
    private String connectionIdentifier;
    private BoneCPConfig config;
    private BoneCP connectionPool;
    private boolean connectionAttemptFailed;

    public T8BoneCPDataConnectionPool(T8BoneCPDataConnectionPoolDefinition definition, T8DataTransaction dataAccessProvider)
    {
        this.definition = definition;
        this.retryOnFailure = definition.retryOnConnectionFailure();
        this.connectionAttemptFailed = false;
        configureConnectionPool();
    }

    private void configureConnectionPool()
    {
        // Initialize the pool.
        this.config = new BoneCPConfig();

        // BoneCP Configuration Properties
        // -----------------------------
        config.setMinConnectionsPerPartition(definition.getMinPoolSize());
        config.setMaxConnectionsPerPartition(definition.getMaxPoolSize());
        if (definition.isDebugUnclosedConnections())
        {
            LOGGER.log(T8Logger.Level.WARNING, "*********************** WARNING **********************");
            LOGGER.log(T8Logger.Level.WARNING, "Setting the Connection Watch to active will reduce the performance of the application. It can be turned off on the connection pool setup.");
            config.setCloseConnectionWatch(true);
            LOGGER.log(T8Logger.Level.WARNING, "*********************** WARNING END **********************");
        } else config.setCloseConnectionWatch(false);
        config.setPartitionCount(1);
        config.setAcquireRetryAttempts(3);
    }

    @Override
    public String getDataConnectionIdentifier()
    {
        return connectionIdentifier;
    }

    @Override
    public Connection getConnection() throws SQLException
    {
        if(connectionAttemptFailed && !retryOnFailure) //Do not attempt to create new connections if the connection pool has been configured to not allow that
        {
            throw new SQLException("This connection pool has failed at creating a connection to the database and has been configured to not retry");
        }

        try
        {
            if (connectionPool == null) connectionPool = new BoneCP(config);
            return connectionPool.getConnection();
        }
        catch(SQLException sqle)
        {

            //First check if we actually need to check the exception
            if(!retryOnFailure)
            {
                String sqlState;
                SQLException nextException;

                nextException = sqle;
                sqlState = null;

                while(nextException != null) //We need to find the sql state, which might be in one of the lower exception causes
                {
                    sqlState = nextException.getSQLState();

                    if(sqlState == null && nextException.getCause() instanceof SQLException)
                    {
                        nextException = (SQLException) nextException.getCause();
                    }
                    else nextException = null;
                }

                if(sqlState != null)
                {
                    connectionAttemptFailed = (sqlState.startsWith("08")); //Check if was a connection exception, connection exceptions will always start with 08

                    //If we are set to not retry connections, shutdown the pool and remove the reference, it wont be used again
                    if(connectionAttemptFailed)
                    {
                        destroy();
                    }
                }
            }

            throw sqle;
        }
    }

    @Override
    public void destroy()
    {
        if (connectionPool != null) connectionPool.shutdown();
        connectionPool = null;
    }

    @Override
    public void setDataConnectionIdentifier(String identifier)
    {
        connectionIdentifier = identifier;
    }

    @Override
    public void setDriverClassName(String className) throws Exception
    {
        Class.forName(className);
    }

    @Override
    public synchronized void setConnectionURL(String connectionURL)
    {
        config.setJdbcUrl(connectionURL);

        //Make sure we recreate the connection pool if this property is altered
        if(connectionPool != null)
        {
            connectionPool.shutdown();
            connectionPool = null;
        }
    }

    @Override
    public synchronized void setUsername(String username)
    {
        config.setUsername(username);

        //Make sure we recreate the connection pool if this property is altered
        if(connectionPool != null)
        {
            connectionPool.shutdown();
            connectionPool = null;
        }
    }

    @Override
    public synchronized void setPassword(String password)
    {
        config.setPassword(password);

        //Make sure we recreate the connection pool if this property is altered
        if(connectionPool != null)
        {
            connectionPool.shutdown();
            connectionPool = null;
        }
    }
}
