package com.pilog.t8.definition.cache;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionSerializer;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.T8L2DefinitionCache;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.StringUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;
import java.util.Collection;

/**
 * @author Bouwer du Preez
 */
public class T8EntityDefinitionCache implements T8L2DefinitionCache
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8EntityDefinitionCache.class);

    private final T8DefinitionManager definitionManager;
    private final Set<String> definitionIdentifiers;
    private final Map<String, String> typeMap; // Key: Definition Identifier, Value: Definition Type Identifier.
    private final Map<String, T8DefinitionMetaData> metaDataMap; // Key: Definition Identifier, Value: Definition Meta Data Object.
    private final T8Context context;
    private final String entityId;
    private final String keyFieldIdentifier;
    private final String definitionFieldIdentifier;
    private final T8ServerContext serverContext;
    private final T8DefinitionSerializer serializer;
    private final Set<String> storedTypes;

    private static final String KEY_FIELD_IDENTIFIER = "$IDENTIFIER";
    private static final String DEFINITION_FIELD_IDENTIFIER = "$DEFINITION";

    public T8EntityDefinitionCache(T8Context context, T8DefinitionManager definitionManager, String entityId, Collection<String> typeIds)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definitionManager = definitionManager;
        this.serializer = new T8DefinitionSerializer(definitionManager);
        this.entityId = entityId;
        this.keyFieldIdentifier = entityId + KEY_FIELD_IDENTIFIER;
        this.definitionFieldIdentifier = entityId + DEFINITION_FIELD_IDENTIFIER;
        this.definitionIdentifiers = Collections.synchronizedSet(new HashSet<String>());
        this.typeMap = Collections.synchronizedMap(new HashMap<String, String>());
        this.metaDataMap = Collections.synchronizedMap(new HashMap<String, T8DefinitionMetaData>());
        this.storedTypes = new HashSet<>();
        if (typeIds != null) storedTypes.addAll(typeIds);
    }

    @Override
    public void init() throws Exception
    {
    }

    @Override
    public void destroy()
    {
        // We want to make it easier for the GC by detaching references
        clearCache();
        definitionIdentifiers.clear();
        storedTypes.clear();
    }

    @Override
    public void setContextPath(File contextPath) throws Exception
    {
        // Nothing to do for this cache type.
    }

    @Override
    public void clearCache()
    {
        typeMap.clear();
        metaDataMap.clear();
    }

    @Override
    public void cacheDefinitions()
    {
        LOGGER.log("Caching definitions from entity '" + entityId + "'...");

        try
        {
            cacheDefinitionMetaData();
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while caching definitions from entity: " + entityId, e);
        }
    }

    @Override
    public boolean containsDefinition(String identifier)
    {
        return definitionIdentifiers.contains(identifier);
    }

    @Override
    public boolean containsDefinitionGroup(String groupIdentifier) throws Exception
    {
        for (String typeIdentifier : typeMap.values())
        {
            if (definitionManager.getDefinitionTypeMetaData(typeIdentifier).getGroupId().equals(groupIdentifier))
            {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean loadsType(T8DefinitionTypeMetaData typeMetaData)
    {
        return (storedTypes.contains(typeMetaData.getTypeId()));
    }

    @Override
    public boolean savesType(T8DefinitionTypeMetaData typeMetaData)
    {
        return (storedTypes.contains(typeMetaData.getTypeId()));
    }

    private void cacheDefinitionMetaData() throws Exception
    {
        T8DataEntityResults results = null;
        T8DataSession ds;

        ds = serverContext.getDataManager().getCurrentSession();

        try
        {
            metaDataMap.clear();
            results = ds.instantTransaction().scroll(entityId, null);
            while (results.next())
            {
                String definitionString;
                T8Definition loadedDefinition;
                T8DefinitionMetaData metaData;
                T8DefinitionTypeMetaData typeMetaData;

                try
                {
                    T8DataEntity nextEntity;

                    nextEntity = results.get();
                    definitionString = (String)nextEntity.getFieldValue(definitionFieldIdentifier);
                    if (!Strings.isNullOrEmpty(definitionString))
                    {
                        loadedDefinition = serializer.deserializeDefinition(definitionString);
                        metaData = loadedDefinition.getMetaData();
                        typeMetaData = loadedDefinition.getTypeMetaData();

                        definitionIdentifiers.add(loadedDefinition.getIdentifier());
                        metaDataMap.put(loadedDefinition.getIdentifier(), metaData);
                        typeMap.put(loadedDefinition.getIdentifier(), typeMetaData.getTypeId());
                    }
                    else throw new Exception("Empty definition XML found in entity: " + nextEntity);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while deserializing definition from entity: " + entityId, e);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while caching definition meta data from entity: " + entityId, e);
        }
        finally
        {
            if (results != null) results.close(); // Not necessary but efficient.
        }
    }

    @Override
    public T8Definition loadDefinition(String identifier) throws Exception
    {
        T8DataFilter filter;
        T8DataSession ds;
        List<T8DataEntity> dataEntityList;

        ds = serverContext.getDataManager().getCurrentSession();
        filter = new T8DataFilter(entityId, HashMaps.createSingular(keyFieldIdentifier, identifier));
        dataEntityList = ds.instantTransaction().select(entityId, filter);
        if (dataEntityList.size() > 0)
        {
            String definitionString;
            T8Definition rawDefinition;

            definitionString = (String)dataEntityList.get(0).getFieldValue(definitionFieldIdentifier);
            rawDefinition = serializer.deserializeDefinition(definitionString);
            return rawDefinition;
        }
        else return null;
    }

    @Override
    public void saveDefinition(T8Definition definition) throws Exception
    {
        String identifier;
        T8DefinitionTypeMetaData typeMetaData;
        T8DefinitionMetaData metaData;
        T8DataSession session;
        String definitionData;
        T8DataEntity entity;
        long startTime;
        T8DataTransaction xtx;
        T8DataTransaction tx;

        // Get the transaction to use (suspend any external transaction).
        session = serverContext.getDataManager().getCurrentSession();
        xtx = session.suspend();
        tx = session.beginTransaction();

        // Get all the required meta data.
        identifier = definition.getIdentifier();
        metaData = definition.getMetaData();
        typeMetaData = definition.getTypeMetaData();

        // Serialize the definition.
        definitionData = serializer.serializeDefinition(definition);

        // Store the serialized definition an accompanying meta data.
        startTime = System.currentTimeMillis();
        entity = new T8DataEntity((T8DataEntityDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, null, entityId, null));
        entity.setFieldValue(keyFieldIdentifier, identifier);
        entity.setFieldValue(definitionFieldIdentifier, definitionData);

        // If the entity contains any fields that match directly with one of the datum types, then add the datum value into the matching field.
        for (T8DefinitionDatumType datumType : definition.getDatumTypes())
        {
            String fieldIdentifier;

            fieldIdentifier = entityId + T8Definition.getLocalIdPrefix() + datumType.getIdentifier();
            if (entity.getDefinition().containsField(fieldIdentifier))
            {
                entity.setFieldValue(fieldIdentifier, convertDatumValueToFieldValue(definition.getDefinitionDatum(datumType.getIdentifier())));
            }
        }

        try
        {
            if (definitionIdentifiers.contains(identifier))
            {
                if (!tx.update(entity)) throw new RuntimeException("Definition identifier found in cache but not in enity source: " + identifier);
            }
            else
            {
                tx.insert(entity);
            }

            tx.commit();
            definitionIdentifiers.add(identifier);
            metaDataMap.put(definition.getIdentifier(), metaData);
            typeMap.put(definition.getIdentifier(), typeMetaData.getTypeId());
            LOGGER.log("Saved definition: " + identifier + " (" + (System.currentTimeMillis() - startTime) + " ms)");

            // Set the 'patched' flag on the meta data (when a definition is saved, it can no longer be patch since it is saved with all patch changes).
            metaData.setPatched(false);
            metaData.setPatchedContent(false);
        }
        catch (Exception e)
        {
            tx.rollback();
            throw e;
        }
        finally
        {
            // If an external session was present, resume it.
            if (xtx != null) session.resume(xtx);
        }
    }

    private Object convertDatumValueToFieldValue(Object datumValue)
    {
        if (datumValue == null)
        {
            return null;
        }
        else if (datumValue instanceof String)
        {
            return datumValue;
        }
        else if (datumValue instanceof Number)
        {
            return datumValue;
        }
        else if (datumValue instanceof List)
        {
            return StringUtilities.buildCSVString((List)datumValue);
        }
        else if (datumValue instanceof Boolean)
        {
            return (Boolean)datumValue ? "Y" : "N";
        }
        else if (datumValue instanceof Date)
        {
            return new T8Timestamp(((Date)datumValue).getTime());
        }
        else return "Unsupported Datum Data Type";
    }

    @Override
    public void deleteDefinition(String identifier) throws Exception
    {
        String typeIdentifier;

        typeIdentifier = typeMap.get(identifier);
        if (typeIdentifier != null)
        {
            T8DefinitionTypeMetaData typeMetaData;

            typeMetaData = definitionManager.getDefinitionTypeMetaData(typeIdentifier);
            if (typeMetaData != null)
            {
                T8DataSession session;
                T8DataTransaction xtx;
                T8DataTransaction tx;
                T8DataEntity entity;

                // Get the transaction to use (suspend any external transaction).
                session = serverContext.getDataManager().getCurrentSession();
                xtx = session.suspend();
                tx = session.beginTransaction();

                LOGGER.log("Deleting definition: " + identifier);
                entity = new T8DataEntity((T8DataEntityDefinition)serverContext.getDefinitionManager().getInitializedDefinition(null, null, entityId, new HashMap<>()));
                entity.setFieldValue(keyFieldIdentifier, identifier);

                try
                {
                    tx.delete(entity);
                    tx.commit();

                    definitionIdentifiers.remove(identifier);
                    typeMap.remove(identifier);
                    metaDataMap.remove(identifier);
                }
                catch (Exception e)
                {
                    tx.rollback();
                    throw e;
                }
                finally
                {
                    // If an external session was present, resume it.
                    if (xtx != null) session.resume(xtx);
                }
            }
            else throw new Exception("Definition type meta data not found: " + typeIdentifier);
        }
        else throw new Exception("Definition not found: " + identifier);
    }

    @Override
    public long getCachedCharacterSize()
    {
        return 0;
    }

    @Override
    public int getCachedDefinitionCount()
    {
        return definitionIdentifiers.size();
    }

    @Override
    public Set<String> getDefinitionIdentifiers()
    {
        return new TreeSet<>(definitionIdentifiers);
    }

    @Override
    public Set<String> getDefinitionIdentifiers(String typeIdentifier)
    {
        Set<String> identifierSet;

        identifierSet = new HashSet<>();
        for (String identifier : typeMap.keySet())
        {
            if (typeMap.get(identifier).equals(typeIdentifier))
            {
                identifierSet.add(identifier);
            }
        }

        return identifierSet;
    }

    @Override
    public Set<String> getGroupDefinitionIdentifiers(String groupIdentifier) throws Exception
    {
        Set<String> identifierSet;

        identifierSet = new HashSet<>();
        for (T8DefinitionTypeMetaData typeMetaData : definitionManager.getAllDefinitionTypeMetaData())
        {
            if (typeMetaData.getGroupId().equals(groupIdentifier))
            {
                for (String identifier : typeMap.keySet())
                {
                    if (typeMap.get(identifier).equals(typeMetaData.getTypeId()))
                    {
                        identifierSet.add(identifier);
                    }
                }
            }
        }

        return identifierSet;
    }

    @Override
    public Set<String> getGroupIdentifiers() throws Exception
    {
        Set<String> groupIdentifiers;

        groupIdentifiers = new HashSet<>();
        for (String typeIdentifier : typeMap.values())
        {
            groupIdentifiers.add(definitionManager.getDefinitionTypeMetaData(typeIdentifier).getGroupId());
        }

        return groupIdentifiers;
    }

    @Override
    public T8DefinitionMetaData getDefinitionMetaData(String identifier)
    {
        return metaDataMap.get(identifier);
    }

    @Override
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String typeIdentifier)
    {
        List<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<>();
        for (String identifier : typeMap.keySet())
        {
            if (typeMap.get(identifier).equals(typeIdentifier))
            {
                metaDataList.add(metaDataMap.get(identifier));
            }
        }

        return metaDataList;
    }

    @Override
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String groupIdentifier) throws Exception
    {
        ArrayList<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<>();
        for (T8DefinitionTypeMetaData typeMetaData : definitionManager.getAllDefinitionTypeMetaData())
        {
            if (typeMetaData.getGroupId().equals(groupIdentifier))
            {
                for (String identifier : typeMap.keySet())
                {
                    if (typeMap.get(identifier).equals(typeMetaData.getTypeId()))
                    {
                        metaDataList.add(metaDataMap.get(identifier));
                    }
                }
            }
        }

        return metaDataList;
    }

    @Override
    public List<T8DefinitionMetaData> getAllDefinitionMetaData()
    {
        ArrayList<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<>(metaDataMap.values());
        return metaDataList;
    }

    @Override
    public List<String> findDefinitionsByText(String searchText)
    {
        T8DataFilter dataFilter;
        T8DataFilterCriteria filterCriteria;
        ArrayList<String> identifiers;
        T8DataEntityResults results = null;
        T8DataSession ds;

        dataFilter = new T8DataFilter(entityId);
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, definitionFieldIdentifier, T8DataFilterCriterion.DataFilterOperator.LIKE, "%" + searchText + "%");
        dataFilter.setFilterCriteria(filterCriteria);

        identifiers = new ArrayList<>();
        ds = serverContext.getDataManager().getCurrentSession();

        try
        {
            results = ds.instantTransaction().scroll(entityId, dataFilter);
            while (results.next())
            {
                T8DataEntity nextEntity;

                nextEntity = results.get();
                identifiers.add((String)nextEntity.getFieldValue(keyFieldIdentifier));
            }

            return identifiers;
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while scrolling definition entities: " + entityId, e);
            return identifiers;
        }
        finally
        {
            if (results != null) results.close();
        }
    }

    @Override
    public List<String> findDefinitionsByRegularExpression(String expression)
    {
        T8DataFilter dataFilter;
        T8DataFilterCriteria filterCriteria;
        ArrayList<String> identifiers;
        T8DataEntityResults results = null;
        T8DataSession ds;

        dataFilter = new T8DataFilter(entityId);
        filterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, definitionFieldIdentifier, T8DataFilterCriterion.DataFilterOperator.MATCHES, expression);
        dataFilter.setFilterCriteria(filterCriteria);

        identifiers = new ArrayList<>();
        ds = serverContext.getDataManager().getCurrentSession();

        try
        {
            results = ds.instantTransaction().scroll(entityId, dataFilter);
            while (results.next())
            {
                T8DataEntity nextEntity;

                nextEntity = results.get();
                identifiers.add((String)nextEntity.getFieldValue(keyFieldIdentifier));
            }

            return identifiers;
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while scrolling definition entities: " + entityId, e);
            return identifiers;
        }
        finally
        {
            if (results != null) results.close();
        }
    }

    @Override
    public List<String> findDefinitionsByIdentifier(String identifier)
    {
        List<String> results;
        String globalPart;
        String localPart;

        // Find all definitions containing the identifier (references).
        results = findDefinitionsByRegularExpression("[^@\\$0-9a-zA-Z_]" + identifier.replace("$", "\\$") + "[^0-9a-zA-Z_]");

        // Also try to find the definition itself (this will not be picked up by the previous step).
        localPart = T8IdentifierUtilities.getLocalIdentifierPart(identifier);
        globalPart = T8IdentifierUtilities.getGlobalIdentifierPart(identifier);
        if ((globalPart != null) && (containsDefinition(globalPart)) && (!results.contains(globalPart)))
        {
            try
            {
                T8Definition containingDefinition;

                // Load the definition using the global part of the identifier and then see if it contains the local definition specified.
                containingDefinition = loadDefinition(globalPart);
                if (containingDefinition.getLocalDefinition(localPart) != null)
                {
                    results.add(globalPart);
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while loading definition: " + globalPart, e);
            }
        }

        // Return the results.
        return results;
    }

    @Override
    public List<String> findDefinitionsByDatum(String datumIdentifier, T8DataType dataType, String content)
    {
        StringBuffer searchText;

        searchText = new StringBuffer();
        if (datumIdentifier != null) searchText.append(datumIdentifier.toUpperCase());
        searchText.append(" TYPE=\"" + dataType + "\">" + content + "<");
        return findDefinitionsByText(searchText.toString());
    }
}
