package com.pilog.t8.api;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.flow.task.T8TaskEscalationTrigger;
import com.pilog.t8.flow.task.T8TaskEscalationTrigger.TriggerEvent;
import com.pilog.t8.time.T8Timestamp;
import java.util.ArrayList;
import java.util.List;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.*;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import com.pilog.t8.utilities.collections.HashMaps;

/**
 * @author Bouwer du Preez
 */
public class T8WorkflowApiDataHandler
{
    private final T8DataTransaction tx;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;

    public T8WorkflowApiDataHandler(T8DataTransaction tx, T8ServerContext serverContext, T8SessionContext sessionContext)
    {
        this.tx = tx;
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
    }
    
    public void deleteTaskEscalationTriggers(List<String> triggerIids) throws Exception
    {
        for (String triggerIid : triggerIids)
        {
            T8DataFilter triggerFilter;
            
            triggerFilter = new T8DataFilter(FLOW_TASK_ESCALATION_DE_IDENTIFIER);
            triggerFilter.addFilterCriterion(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TRIGGER_IID, triggerIid);
            
            tx.delete(FLOW_TASK_ESCALATION_DE_IDENTIFIER, triggerFilter);
        }
    }

    public void insertTaskEscalationTriggers(List<T8TaskEscalationTrigger> triggers) throws Exception
    {
        for (T8TaskEscalationTrigger trigger : triggers)
        {
            T8DataEntity triggerEntity;

            triggerEntity = tx.create(FLOW_TASK_ESCALATION_DE_IDENTIFIER, null);
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TRIGGER_IID, trigger.getTriggerIid());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_FLOW_ID, trigger.getFlowId());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TASK_ID, trigger.getTaskId());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_EVENT, trigger.getEvent().toString());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TIME_ELAPSED, trigger.getTimeElapsed());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_RECURRING, trigger.isRecurring() ? "Y" : "N");
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_PRIORITY, trigger.getPriority());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_PRIORITY_INCREASE, trigger.getPriorityIncrease());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_ESCALATION_ID, trigger.getEscalationId());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_GROUP_ID, trigger.getGroupId());
            tx.insert(triggerEntity);
        }
    }

    public void updateTaskEscalationTriggers(List<T8TaskEscalationTrigger> triggers) throws Exception
    {
        for (T8TaskEscalationTrigger trigger : triggers)
        {
            T8DataEntity triggerEntity;

            triggerEntity = tx.create(FLOW_TASK_ESCALATION_DE_IDENTIFIER, null);
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TRIGGER_IID, trigger.getTriggerIid());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_FLOW_ID, trigger.getFlowId());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TASK_ID, trigger.getTaskId());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_EVENT, trigger.getEvent().toString());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TIME_ELAPSED, trigger.getTimeElapsed());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_RECURRING, trigger.isRecurring() ? "Y" : "N");
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_PRIORITY, trigger.getPriority());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_PRIORITY_INCREASE, trigger.getPriorityIncrease());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_ESCALATION_ID, trigger.getEscalationId());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_GROUP_ID, trigger.getGroupId());
            tx.update(triggerEntity);
        }
    }

    public void saveTaskEscalationTriggers(List<T8TaskEscalationTrigger> triggers) throws Exception
    {
        for (T8TaskEscalationTrigger trigger : triggers)
        {
            T8DataEntity triggerEntity;

            triggerEntity = tx.create(FLOW_TASK_ESCALATION_DE_IDENTIFIER, null);
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TRIGGER_IID, trigger.getTriggerIid());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_FLOW_ID, trigger.getFlowId());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TASK_ID, trigger.getTaskId());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_EVENT, trigger.getEvent().toString());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TIME_ELAPSED, trigger.getTimeElapsed());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_RECURRING, trigger.isRecurring() ? "Y" : "N");
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_PRIORITY, trigger.getPriority());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_PRIORITY_INCREASE, trigger.getPriorityIncrease());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_ESCALATION_ID, trigger.getEscalationId());
            triggerEntity.setFieldValue(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_GROUP_ID, trigger.getGroupId());

            if (!tx.update(triggerEntity))
            {
                tx.insert(triggerEntity);
            }
        }
    }

    public List<T8TaskEscalationTrigger> retrieveTaskEscalationTriggers(List<String> triggerIids) throws Exception
    {
        List<T8TaskEscalationTrigger> triggers;
        List<T8DataEntity> triggerEntities;
        T8DataFilter triggerOrderFilter;

        // We want to order the triggers to ensure each one will be applied for successive applications.
        triggerOrderFilter = new T8DataFilter(FLOW_TASK_ESCALATION_DE_IDENTIFIER);
        if (triggerIids != null) triggerOrderFilter.addFilterCriteria(DataFilterConjunction.AND, FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TRIGGER_IID, triggerIids);
        triggerOrderFilter.addFieldOrdering(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_SEQUENCE, T8DataFilter.OrderMethod.ASCENDING);

        // Retrieve the task entity.
        triggers = new ArrayList<>();
        triggerEntities = tx.select(FLOW_TASK_ESCALATION_DE_IDENTIFIER, triggerOrderFilter);
        for (T8DataEntity triggerEntity : triggerEntities)
        {
            T8TaskEscalationTrigger trigger;

            trigger = new T8TaskEscalationTrigger((String)triggerEntity.getFieldValue(F_TRIGGER_IID));
            trigger.setFlowId((String)triggerEntity.getFieldValue(F_FLOW_ID));
            trigger.setTaskId((String)triggerEntity.getFieldValue(F_TASK_ID));
            trigger.setEvent(TriggerEvent.valueOf((String)triggerEntity.getFieldValue(F_EVENT)));
            trigger.setTimeElapsed((Integer)triggerEntity.getFieldValue(F_TIME_ELAPSED));
            trigger.setRecurring("Y".equalsIgnoreCase((String)triggerEntity.getFieldValue(F_RECURRING)));
            trigger.setPriority((Integer)triggerEntity.getFieldValue(F_PRIORITY));
            trigger.setPriorityIncrease((Integer)triggerEntity.getFieldValue(F_PRIORITY_INCREASE));
            trigger.setEscalationId((String)triggerEntity.getFieldValue(F_ESCALATION_ID));
            trigger.setGroupId((String)triggerEntity.getFieldValue(F_GROUP_ID));
            triggers.add(trigger);
        }

        // Return the final list of retrieved triggers.
        return triggers;
    }

    public void recalculateTaskTimespans(String taskIid, T8OperationalHoursCalculator calculator) throws Exception
    {
        T8DataEntity taskEntity;

        taskEntity = tx.retrieve(STATE_FLOW_TASK_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_DE_IDENTIFIER + F_TASK_IID, taskIid));
        if (taskEntity != null)
        {
            recalculateTaskTimespans(taskEntity, calculator);
        }
        else throw new IllegalArgumentException("Task not found: " + taskIid);
    }

    public void recalculateTaskTimespans(T8OperationalHoursCalculator calculator) throws Exception
    {
        T8DataEntityResults results = null;

        try
        {
            results = tx.scroll(STATE_FLOW_TASK_DE_IDENTIFIER, null);
            while (results.next())
            {
                T8DataEntity taskEntity;

                taskEntity = results.get();
                recalculateTaskTimespans(taskEntity, calculator);
            }
        }
        finally
        {
            if (results != null) results.close();
        }
    }

    private void recalculateTaskTimespans(T8DataEntity taskEntity, T8OperationalHoursCalculator calculator) throws Exception
    {
        T8Timestamp timeIssued;
        T8Timestamp timeClaimed;
        T8Timestamp timeCompleted;
        Long timespanIssued;
        Long timespanClaimed;
        Long timespanTotal;
        Long timespanBusinessIssued;
        Long timespanBusinessClaimed;
        Long timespanBusinessCompleted;

        timeIssued = (T8Timestamp)taskEntity.getFieldValue(F_TIME_ISSUED);
        timeClaimed = (T8Timestamp)taskEntity.getFieldValue(F_TIME_CLAIMED);
        timeCompleted = (T8Timestamp)taskEntity.getFieldValue(F_TIME_COMPLETED);
        timespanIssued = null;
        timespanClaimed = null;
        timespanTotal = null;
        timespanBusinessIssued = null;
        timespanBusinessClaimed = null;
        timespanBusinessCompleted = null;

        if (timeClaimed != null)
        {
            timespanIssued = timeClaimed.getMilliseconds() - timeIssued.getMilliseconds();
            timespanBusinessIssued = calculator != null ? calculator.calculateOperationalTimeDifference(timeIssued.getMilliseconds(), timeClaimed.getMilliseconds()) : timespanIssued;

            if (timeCompleted != null)
            {
                timespanClaimed = timeCompleted.getMilliseconds() - timeClaimed.getMilliseconds();
                timespanTotal = timespanIssued + timespanClaimed;

                if (calculator != null)
                {
                    timespanBusinessClaimed = calculator.calculateOperationalTimeDifference(timeClaimed.getMilliseconds(), timeCompleted.getMilliseconds());
                    timespanBusinessCompleted = timespanBusinessIssued + timespanBusinessClaimed;
                }
                else
                {
                    timespanBusinessClaimed = timespanClaimed;
                    timespanBusinessCompleted = timespanTotal;
                }
            }
        }

        taskEntity.setFieldValue(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_ISSUED, timespanIssued);
        taskEntity.setFieldValue(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_CLAIMED, timespanClaimed);
        taskEntity.setFieldValue(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_TOTAL, timespanTotal);
        taskEntity.setFieldValue(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_BUSINESS_ISSUED, timespanBusinessIssued);
        taskEntity.setFieldValue(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_BUSINESS_CLAIMED, timespanBusinessClaimed);
        taskEntity.setFieldValue(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_SPAN_BUSINESS_TOTAL, timespanBusinessCompleted);
        tx.update(taskEntity);
    }
}
