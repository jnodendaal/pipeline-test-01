package com.pilog.t8.notification;

import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 */
public class T8ServerNotificationManagerScriptFacade
{
    private final T8NotificationManager notificationManager;
    private final T8SessionContext sessionContext;
    private final T8Context context;

    public T8ServerNotificationManagerScriptFacade(T8Context context, T8NotificationManager notificationManager)
    {
        this.notificationManager = notificationManager;
        this.context = context;
        this.sessionContext = context.getSessionContext();
    }

    public int getTotalNotificationCount() throws Exception
    {
        T8NotificationFilter filter;

        filter = new T8NotificationFilter();
        filter.setUserIdentifier(sessionContext.getUserIdentifier());
        filter.setIncludeNewNotifications(true);
        filter.setIncludeReceivedNotifications(true);
        return notificationManager.countNotifications(context, filter);
    }

    public int getNewNotificationCount() throws Exception
    {
        T8NotificationFilter filter;

        filter = new T8NotificationFilter();
        filter.setUserIdentifier(sessionContext.getUserIdentifier());
        filter.setIncludeNewNotifications(true);
        filter.setIncludeReceivedNotifications(false);
        return notificationManager.countNotifications(context, filter);
    }

    public int getReceivedNotificationCount() throws Exception
    {
        T8NotificationFilter filter;

        filter = new T8NotificationFilter();
        filter.setUserIdentifier(sessionContext.getUserIdentifier());
        filter.setIncludeNewNotifications(false);
        filter.setIncludeReceivedNotifications(true);
        return notificationManager.countNotifications(context, filter);
    }

    public T8NotificationSummary getUserNotificationSummary(T8NotificationFilter notificationFilter) throws Exception
    {
        return notificationManager.getUserNotificationSummary(context, notificationFilter);
    }
}