package com.pilog.t8.flow.node.activity;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.procedure.T8Procedure;
import com.pilog.t8.definition.flow.node.activity.T8ProcedureActivityDefinition;
import com.pilog.t8.definition.procedure.T8ProcedureDefinition;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ProcedureActivityNode extends T8DefaultFlowNode
{
    private final T8ProcedureActivityDefinition definition;

    public T8ProcedureActivityNode(T8Context context, T8Flow parentFlow, T8ProcedureActivityDefinition definition, String nodeIid)
    {
        super(context, parentFlow, definition, nodeIid);
        this.definition = definition;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters) throws Exception
    {
        Map<String, Object> outputParameters;

        // Execute the procedure.
        outputParameters = executeProcedure(flowState, T8IdentifierUtilities.mapParameters(inputParameters, definition.getProcedureInputParameterMapping()));

        // Map the procedure output parameters to the flow parameters.
        outputParameters = T8IdentifierUtilities.mapParameters(outputParameters, definition.getProcedureOutputParameterMapping());
        return outputParameters;
    }

    protected Map<String, Object> executeProcedure(T8FlowState state, Map<String, Object> inputParameters) throws Exception
    {
        T8ProcedureDefinition procedureDefinition;

        // Start the id of the procedure to execute.
        procedureDefinition = (T8ProcedureDefinition)serverContext.getDefinitionManager().getRawDefinition(internalContext, definition.getRootProjectId(), definition.getProcedureId());
        if (procedureDefinition != null)
        {
            T8Procedure currentProcedure;
            T8Context accessContext;

            // Execute the procedure and return the results.
            accessContext = serverContext.getSecurityManager().getContext();
            currentProcedure = procedureDefinition.getNewProcedureInstance(internalContext);
            return (Map)currentProcedure.execute(accessContext, inputParameters);
        }
        else throw new Exception("Procedure not found: " + definition.getProcedureId());
    }
}
