package com.pilog.t8.flow;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.thread.T8ContextRunnable;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author Gavin Boshoff
 */
class T8QueuedFlowHandler extends Thread
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8QueuedFlowHandler.class);
    private static final int QUEUE_STARTER_TASK_COUNT = 5;

    private final T8Context internalContext;
    private final T8ServerFlowManager flowController;
    private final SynchronousQueue<String> flowQueue;
    private final ExecutorService queueProcessor;
    private final T8ServerContext serverContext;
    private boolean iterationInProgress;
    private boolean enabled;

    T8QueuedFlowHandler(T8Context context, T8ServerFlowManager flowController, boolean enabled)
    {
        super("T8ServerFlowController-QueuedFlowHandler");
        this.flowController = flowController;
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.queueProcessor = Executors.newFixedThreadPool(QUEUE_STARTER_TASK_COUNT, new NameableThreadFactory("T8ServerFlowController-QueuedFlowStarter"));
        this.flowQueue = new SynchronousQueue<>();
        this.iterationInProgress = false;
        this.enabled = false;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
        if (!enabled)
        {
            this.iterationInProgress = false;
        }
    }

    @Override
    public void run()
    {
        if (enabled)
        {
            int queuedFlowsStarted = 0;

            try
            {
                Set<String> queuedFlowIids;

                // Open a data session for this thread.
                serverContext.getSecurityManager().createContext(internalContext.getSessionContext());
                serverContext.getDataManager().openDataSession(internalContext);

                // Retrieve Queued Flows
                queuedFlowIids = retrieveQueuedFlows();

                // Initialize the threads for the current iteration
                if (!queuedFlowIids.isEmpty()) initializeCurrentIteration();

                // Send the queued flows to be started
                for (String queuedFlowIid : queuedFlowIids)
                {
                    if (enabled)
                    {
                        this.flowQueue.put(queuedFlowIid);
                        queuedFlowsStarted++;
                    }
                    else return;
                }
            }
            catch (Exception ex)
            {
                LOGGER.log("An unexpected error occurred while retrieving and submitting the queued flows: " + ex.getMessage(), ex);
            }
            finally
            {
                // Close the data session for this thread.
                serverContext.getDataManager().closeCurrentSession();
                serverContext.getSecurityManager().destroyContext();
                // Complete the current iteration. This allows the flow starter threads to complete.
                this.iterationInProgress = false;
                // Just log the number of queued flows which were started
                if (queuedFlowsStarted != 0) LOGGER.log("Started {"+queuedFlowsStarted+"} queued flows.");
            }
        }
    }

    private void initializeCurrentIteration()
    {
        if (enabled)
        {
            // This keeps the flow starter threads active
            this.iterationInProgress = true;

            // If we have successive iterations, redundant tasks may be spawned.
            // This scenario is self-correcting
            for (int i = 0; i < QUEUE_STARTER_TASK_COUNT; i++)
            {
                this.queueProcessor.submit(new QueuedFlowStarter(internalContext));
            }
        }
    }

    private Set<String> retrieveQueuedFlows() throws Exception
    {
        List<T8DataEntity> queuedFlowEntities;
        T8DataTransaction tx;

        // Create an instant transaction for the read
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Retrieve the entities for the queued flows
        // We use a simple filter, because there is no way to order to ensure the eldest queued start first
        queuedFlowEntities = tx.select(STATE_FLOW_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_DE_IDENTIFIER+"$STATUS", T8Flow.FlowStatus.QUEUED.toString()));

        // Get just the flow instance identifiers for the starter threads
        return T8DataUtilities.getEntityFieldValueSet(queuedFlowEntities, STATE_FLOW_DE_IDENTIFIER+"$FLOW_IID");
    }

    /**
     * A task to be executed in a thread. The task will run for the duration of
     * a batch of
     */
    private class QueuedFlowStarter extends T8ContextRunnable
    {
        private QueuedFlowStarter(T8Context context)
        {
            super(context, "QueuedFlowStarter-"+System.currentTimeMillis());
        }

        @Override
        public void exec()
        {
            while (enabled && iterationInProgress)
            {
                try
                {
                    String flowIid;

                    // Take the currently offered item from the queue.
                    // We poll as to prevent the thread from waiting after the
                    // current iteration has been completed.
                    // The 1 second delay has the potential of the threads being
                    // reused on the next iteration, if it follows immediately
                    // after the current has completed.
                    flowIid = flowQueue.poll(1000l, TimeUnit.MILLISECONDS);
                    if (flowIid != null)
                    {
                        // Start the flow
                        startFlow(flowIid);
                    }
                }
                catch (InterruptedException ie)
                {
                    LOGGER.log("Waiting for next queued flow has been interrupted. We will retry for another.", ie);
                }
            }
        }

        private void startFlow(String flowIid)
        {
            try
            {
                T8Flow flow;

                // Start the flow
                flowController.startQueuedFlow(context, flowIid);

                // Now we need to wait until the flow is in a waiting state.
                // This should be the first user task, or a waiting node,
                flow = flowController.getFlow(flowIid);
                while (enabled && flow.isExecuting())
                {
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException ie)
                    {
                        LOGGER.log("Waiting for executing flow was interrupted. Ignoring interruption.", ie);
                    }
                }

                LOGGER.log(()->"Queued flow {"+flowIid+"} started.");
            }
            catch (Exception ex)
            {
                LOGGER.log(()->"Failed to start the flow for {"+flowIid+"} - "+ex.getMessage(), ex);
            }
        }
    }
}