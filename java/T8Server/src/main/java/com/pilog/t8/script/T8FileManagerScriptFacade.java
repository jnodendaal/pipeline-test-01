package com.pilog.t8.script;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.file.T8FileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8TimeUnit;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8FileManagerScriptFacade
{
    private final T8FileManager fileManager;
    private final T8Context context;

    public T8FileManagerScriptFacade(T8Context context, T8FileManager fileManager)
    {
        this.fileManager = fileManager;
        this.context = context;
    }

    public T8FileDetails getFileDetails(String contextIid, String path) throws Exception
    {
        return fileManager.getFileDetails(context, contextIid, path);
    }

    public List<T8FileDetails> getFileList(String contextIid, String directoryPath) throws Exception
    {
        return fileManager.getFileList(context, contextIid, directoryPath);
    }

    public boolean fileContextExists(String contextIid) throws Exception
    {
        return fileManager.fileContextExists(context, contextIid);
    }

    public boolean fileExists(String contextIid, String filePath) throws Exception
    {
        return fileManager.fileExists(context, contextIid, filePath);
    }

    public T8FileHandler getFileHandler(String fileTypeId, String contextIid, String filePath)
    {
        return fileManager.getFileHandler(context, fileTypeId, contextIid, filePath);
    }

    public boolean openFileContext(String contextIid, String contextId, T8TimeUnit expirationTime) throws Exception
    {
        return fileManager.openFileContext(context, contextIid, contextId, expirationTime);
    }

    public boolean closeFileContext(String contextIid) throws Exception
    {
        return fileManager.closeFileContext(context, contextIid);
    }

    public boolean createDirectory(String contextIid, String filePath) throws Exception
    {
        return fileManager.createDirectory(context, contextIid, filePath);
    }

    public boolean deleteFile(String contextIid, String path) throws Exception
    {
        return fileManager.deleteFile(context, contextIid, path);
    }

    public void renameFile(String contextIid, String path, String newFilename) throws Exception
    {
        fileManager.renameFile(context, contextIid, path, newFilename);
    }

    public long getFileSize(String contextIid, String path) throws Exception
    {
        return fileManager.getFileSize(context, contextIid, path);
    }
}