package com.pilog.t8.flow;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8FlowNode.FlowNodeStatus;
import com.pilog.t8.flow.T8FlowNodeExecutionResult.NodeExecutionResultType;
import com.pilog.t8.flow.event.T8FlowNodeCompletedEvent;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.event.T8FlowCompletedEvent;
import com.pilog.t8.flow.event.T8FlowFailureEvent;
import com.pilog.t8.flow.event.T8FlowFinalizedEvent;
import com.pilog.t8.flow.event.T8FlowTaskEscalatedEvent;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.flow.task.T8FlowTask.TaskEvent;
import com.pilog.t8.flow.task.T8TaskEscalationTrigger;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.definition.flow.node.event.T8EndEventDefinition;
import com.pilog.t8.definition.flow.task.T8TaskEscalationDefinition;
import com.pilog.t8.definition.flow.task.T8TaskEscalationScriptDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.thread.T8ContextRunnable;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultFlow implements T8Flow
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultFlow.class);

    private final T8WorkFlowDefinition definition;
    private final T8Context internalContext;
    private final T8FlowController flowController;
    private final T8FlowStateHandler stateHandler;
    private final T8FlowPhaseModelHandler phaseModelHandler;
    private final T8FlowState flowState;
    private long startTime;
    private long endTime;
    private final Map<String, T8FlowNode> executingNodes;

    public T8DefaultFlow(T8Context context, T8FlowController flowController, T8FlowStateHandler stateHandler, T8WorkFlowDefinition definition, T8FlowState flowState)
    {
        this.flowState = flowState;
        this.internalContext = context;
        this.flowController = flowController;
        this.stateHandler = stateHandler;
        this.definition = definition;
        this.executingNodes = Collections.synchronizedMap(new HashMap<String, T8FlowNode>());
        this.phaseModelHandler = new T8FlowPhaseModelHandler(internalContext, definition, this.flowState);
        this.startTime = -1;
        this.endTime = -1;
    }

    @Override
    public T8FlowController getFlowController()
    {
        return flowController;
    }

    @Override
    public String getInstanceIdentifier()
    {
        return flowState != null ? flowState.getFlowIid() : null;
    }

    @Override
    public boolean isExecuting()
    {
        return !executingNodes.isEmpty();
    }

    @Override
    public synchronized T8FlowStatus getStatus()
    {
        ArrayList<T8FlowNodeStatus> nodeStatusList;
        T8FlowStatus flowStatus;

        // Create the flow status object and populate basic values.
        flowStatus = new T8FlowStatus();
        flowStatus.setFlowId(definition.getIdentifier());
        flowStatus.setFlowIid(flowState.getFlowIid());
        flowStatus.setStatus(flowState.getFlowStatus());
        flowStatus.setStartTime(startTime);
        flowStatus.setEndTime(endTime);
        flowStatus.setInitiatorUserIdentifier(flowState.getInitiatorId());
        flowStatus.setInitiatorProfileIdentifier(flowState.getInitiatorIid());
        flowStatus.setInitiatorOrganizationIdentifier(flowState.getInitiatorOrgId());

        // Add the status of each executing node to the flow status object.
        synchronized (executingNodes)
        {
            nodeStatusList = new ArrayList<T8FlowNodeStatus>();
            for (T8FlowNode node : executingNodes.values())
            {
                nodeStatusList.add(node.getStatus());
            }
            flowStatus.setExecutingNodeStatuses(nodeStatusList);
        }

        // Set the flow phase model.
        flowStatus.setPhases(phaseModelHandler.getPhaseModel());

        // Return the flow status.
        return flowStatus;
    }

    @Override
    public synchronized List<T8FlowNode> getExecutingNodes()
    {
        synchronized (executingNodes)
        {
            return new ArrayList<T8FlowNode>(executingNodes.values());
        }
    }

    @Override
    public T8WorkFlowDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public synchronized T8FlowState getState()
    {
        return flowState;
    }

    /**
     * This method executes the supplied node using the specified execution key.
     * The design is such that this method can be called at any time from any
     * thread.  This method then checks the state of execution in the flow and
     * will only executeFlow the supplied node if doing so is valid i.e. if the
     * node has not already completed its life-cycle or is not currently already
     * executing.
     *
     * Starts execution of a new instance of the specified node in a new thread.
     *
     * @param nodeIdentifier The identifier of the node to start.
     * @param nodeIid The instance identifier of the node to
     * start.
     * @param executionKey The execution key to pass to the node when its
     * execution starts.
     */
    private synchronized void executeNode(final String nodeIdentifier, final String nodeIid, final T8FlowExecutionKey executionKey) throws Exception
    {
        T8WorkFlowNodeDefinition nodeDefinition;

        // Get the node definition from the flow definition.
        nodeDefinition = definition.getNodeDefinition(nodeIdentifier);
        if (nodeDefinition != null)
        {
            // Make sure not to start two instances of the same node.
            if (!executingNodes.containsKey(nodeIid))
            {
                final T8FlowNode node;
                final T8FlowNodeState nodeState;

                // Create a new instance of the node.
                node = nodeDefinition.getNewNodeInstance(internalContext, this, nodeIid);
                nodeState = node.getState();

                // Only executeFlow the node if it has not already finished its life-cycle.
                if (!T8FlowUtilities.hasNodeCompleted(nodeState))
                {
                    // Add it to the collection of active nodes.
                    executingNodes.put(nodeIid, node);

                    // Execute the node in a new thread.
                    new Thread(new T8ContextRunnable(internalContext, "T8FlowNodeExecutionThread:" + nodeIid)
                    {
                        @Override
                        public void exec()
                        {
                            boolean flowCompleted = false;

                            // Now executeFlow the node.
                            try
                            {
                                T8FlowNodeExecutionResult nodeExecutionResult;

                                // If this is a resume action, resume the node rather than starting a new instance.
                                nodeExecutionResult = node.executeNode(executionKey);

                                // Handle node completion.
                                if (nodeExecutionResult.getResultType() == NodeExecutionResultType.COMPLETED)
                                {
                                    // If the completed node is an end node, complete the flow.
                                    if (nodeDefinition instanceof T8EndEventDefinition)
                                    {
                                        // User the output parameters of the end node as the flow output.
                                        flowCompleted = true;
                                    }
                                    else
                                    {
                                        // Start the output nodes of the completed node.
                                        startOutputNodes(nodeState);

                                        // Fire the flow event.
                                        flowController.processFlowEvent(new T8FlowNodeCompletedEvent(T8DefaultFlow.this, node, nodeState.getOutputParameters()));
                                    }
                                }
                                else if (nodeExecutionResult.getResultType() == NodeExecutionResultType.FAILED)
                                {
                                    T8FlowFailureEvent event;

                                    // Set the flow status to indicate the failure.
                                    synchronized (flowState)
                                    {
                                        flowState.setFlowStatus(FlowStatus.FAILED);
                                        persistState();
                                    }

                                    // Log the event.
                                    stateHandler.logFlowEvent(internalContext, FlowHistoryEvent.FAILED, T8DefaultFlow.this, null, nodeExecutionResult.getInformation());

                                    // Fire an event to all active listeners that a flow has failed.
                                    event = new T8FlowFailureEvent(flowState.getFlowIid());
                                    flowController.processFlowEvent(event);
                                }
                            }
                            catch (Throwable e)
                            {
                                T8FlowFailureEvent event;

                                // Log the exception.
                                LOGGER.log("Exception during execution of flow node: " + node.getDefinition(), e);

                                // Set the flow status to indicate the failure.
                                synchronized (flowState)
                                {
                                    flowState.setFlowStatus(FlowStatus.FAILED);
                                    persistState();
                                }

                                // Log the event.
                                stateHandler.logFlowEvent(internalContext, FlowHistoryEvent.FAILED, T8DefaultFlow.this, null, ExceptionUtilities.getStackTraceString(e, 1000, 1000));

                                // Fire an event to all active listeners that a flow has failed.
                                event = new T8FlowFailureEvent(flowState.getFlowIid());
                                flowController.processFlowEvent(event);
                            }
                            finally
                            {
                                // Remove the node from the collection of active nodes.
                                executingNodes.remove(nodeIid);
                            }

                            // If the flow completion flag has been set, complete the flow now.
                            if (flowCompleted)
                            {
                                completeFlow(nodeState.getOutputParameters());
                            }
                        }
                    }).start();
                }
                else LOGGER.log("Cannot start node because it has already finished execution: " + nodeState);
            }
            else LOGGER.log("Cannot start node '" + nodeIid + "' because it is already executing in flow: " + flowState);
        }
        else throw new RuntimeException("Node definition '" + nodeIdentifier + "' not found: " + definition);
    }

    protected synchronized void startOutputNodes(T8FlowNodeState nodeState) throws Exception
    {
        final Map<String, Object> outputNodeInstanceIdentifierMap;

        // Get the output node instance identifier map from the node state.
        outputNodeInstanceIdentifierMap = nodeState.getOutputNodeMap();

        // For each output identifier, spawn a new thread to start the corresponding output node in.
        for (final String outputNodeIdentifier : outputNodeInstanceIdentifierMap.keySet())
        {
            final String nodeInstanceIdentifier;

            // Get the node instance identifier.
            nodeInstanceIdentifier = (String)outputNodeInstanceIdentifierMap.get(outputNodeIdentifier);
            executeNode(outputNodeIdentifier, nodeInstanceIdentifier, null);
        }
    }

    @Override
    public synchronized boolean cancelNode(String nodeIid) throws Exception
    {
        T8FlowNode node;

        // If the node specified is still executing, cancel it.
        node = executingNodes.get(nodeIid);
        if (node != null)
        {
            node.cancelNode();
            return true;
        }
        else return false;
    }

    @Override
    public synchronized void startFlow(Map<String, Object> inputParameters) throws Exception
    {
        //Set the flow start time
        startTime = System.currentTimeMillis();

        // Do the state setup for before flow execution is started.
        synchronized (flowState)
        {
            flowState.setFlowStatus(FlowStatus.ACTIVE);
            flowState.setFlowId(definition.getIdentifier());
            flowState.setProjectId(definition.getRootProjectId());
            flowState.setInputParameters(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), inputParameters, true));
            flowState.setInitiatorId(internalContext.getAgentId());
            flowState.setInitiatorIid(internalContext.getAgentIid());
            flowState.setInitiatorOrgId(internalContext.getOrganizationId());
            flowState.setInitiatorRootOrgId(internalContext.getRootOrganizationId());
            flowState.setTimeStarted(new T8Timestamp(System.currentTimeMillis()));

            // Persist state.
            persistState();
        }

        // Create a new thread and start the first flow node in it.
        executeNode(definition.getStartNodeIdentifier(), T8IdentifierUtilities.createNewGUID(), null);
    }

    @Override
    public synchronized void startFlow() throws Exception
    {
        synchronized (flowState)
        {
            // Record the starting time and the new flow status.
            startTime = System.currentTimeMillis();
            flowState.setFlowStatus(FlowStatus.ACTIVE);
            flowState.setTimeStarted(new T8Timestamp(startTime));

            // Persist state.
            persistState();
        }

        // Create a new thread and start the first flow node in it.
        executeNode(definition.getStartNodeIdentifier(), T8IdentifierUtilities.createNewGUID(), null);
    }

    @Override
    public synchronized void executeFlow(T8FlowExecutionKey executionKey) throws Exception
    {
        synchronized (flowState)
        {
            // Only start new nodes if the flow is still active.
            if (flowState.getFlowStatus() != FlowStatus.COMPLETED)
            {
                // Set the flow status to active.
                flowState.setFlowStatus(FlowStatus.ACTIVE);

                // If we have an execution key, use it to start the specific node.
                if (executionKey != null)
                {
                    String nodeInstanceIdentifier;
                    T8FlowNodeState nodeState;

                    // Get the identifiers of the node that generated the execution key.
                    nodeInstanceIdentifier = executionKey.getNodeInstanceIdentifier();

                    // Try to find the execution state of the node to resume.
                    nodeState = flowState.getNodeState(nodeInstanceIdentifier);
                    if (nodeState != null)
                    {
                        LOGGER.log("Resuming execution of node: " + nodeState.getNodeIid() + " in flow: " + flowState.getFlowIid());
                        executeNode(nodeState.getNodeId(), nodeState.getNodeIid(), executionKey);
                    }
                    else throw new Exception("Could not find node state '" + nodeInstanceIdentifier + "' to match execution key: " + executionKey);
                }
                else // No execution key, so start all nodes that have not yet been completed.
                {
                    // Loop through the states of all nodes in the flow state and make sure that all node execution is resumed where left off.
                    for (final T8FlowNodeState nodeState : flowState.getNodeStates().values())
                    {
                        // Start each of the nodes that are active in the supplied state.
                        if ((nodeState.getNodeStatus() == FlowNodeStatus.ACTIVE) || (nodeState.getNodeStatus() == FlowNodeStatus.STARTED) || (nodeState.getNodeStatus() == FlowNodeStatus.NODE_CREATED))
                        {
                            LOGGER.log("Resuming execution of node: " + nodeState.getNodeIid() + " in flow: " + flowState.getFlowIid());
                            executeNode(nodeState.getNodeId(), nodeState.getNodeIid(), null);
                        }
                    }
                }
            }
            else
            {
                LOGGER.log("Cannot resume execution of completed flow: " + flowState);
            }
        }
    }

    @Override
    public synchronized void completeFlow(Map<String, Object> outputParameters)
    {
        T8FlowCompletedEvent event;

        // Update the flow status to reflect that it is now complete.
        flowState.setFlowStatus(FlowStatus.COMPLETED);

        // Record the end time.
        endTime = System.currentTimeMillis();

        // Persist state.
        persistState();

        // Log the event.
        stateHandler.logFlowEvent(internalContext, FlowHistoryEvent.COMPLETED, this, outputParameters, null);

        // Fire an event to all active listeners that a flow has been completed.
        event = new T8FlowCompletedEvent(getStatus(), T8IdentifierUtilities.prependNamespace(definition.getIdentifier(), T8IdentifierUtilities.prependNamespace(definition.getIdentifier(), outputParameters)));
        flowController.processFlowEvent(event);

        // Finalize the flow.
        finalizeFlow();
    }

    @Override
    public synchronized void finalizeFlow()
    {
        T8FlowFinalizedEvent event;

        // Make sure the flow has stopped.
        stopFlow();

        // Revoke all outstanding tasks.
        for (T8FlowTaskState taskState : flowState.getTaskStates())
        {
            if (taskState.getTimeCompleted() == null)
            {
                try
                {
                    flowController.revokeTask(internalContext, taskState);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while finalizing flow: " + flowState, e);
                }
            }
        }

        // Set the state.
        flowState.setFlowStatus(FlowStatus.FINALIZED);

        // Delete the state from persistence.
        stateHandler.saveFlowState(flowState);

        // Fire an event to all active listeners that a flow has been completed.
        event = new T8FlowFinalizedEvent(getStatus());
        flowController.processFlowEvent(event);
    }

    @Override
    public synchronized void stopFlow()
    {
        List<T8FlowNode> nodesToStop;

        // Instruct all currently executing nodes to to stop execution as soon as possible.
        nodesToStop = new ArrayList<T8FlowNode>(executingNodes.values()); // We create this collection to avoid a concurrent modification exception when executing nodes are stopped.
        for (T8FlowNode executingNode : nodesToStop)
        {
            try
            {
                executingNode.stopNode();
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while stopping node: " + executingNode, e);
            }
        }

        // Wait for all active nodes to stop execution.
        while (executingNodes.size() > 0)
        {
            try
            {
                Thread.sleep(50);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while waiting for flow '" + flowState.getFlowIid() + "' to stop.", e);
            }
        }
    }

    @Override
    public List<String> getInputNodeIdentifiers(String nodeId)
    {
        return definition.getParentNodeIdentifiers(nodeId);
    }

    @Override
    public List<String> getOutputNodeIdentifiers(String nodeId)
    {
        return definition.getChildNodeIdentifiers(nodeId);
    }

    @Override
    public synchronized T8FlowTaskState claimTask(T8Context context, String taskIid) throws Exception
    {
        synchronized (flowState)
        {
            T8FlowTaskState taskState;

            taskState = flowState.getTaskState(taskIid);
            if (taskState != null)
            {
                if (taskState.getClaimedByUserId() == null)
                {
                    T8SessionContext sessionContext;
                    String restrictionUserId;
                    String userId;

                    // Get the id of the user claiming the task and make sure the task is accessible to current user.
                    sessionContext = context.getSessionContext();
                    userId = sessionContext.getUserIdentifier();
                    restrictionUserId = taskState.getRestrictionUserId();
                    if ((restrictionUserId == null) || (Objects.equals(userId, restrictionUserId)))
                    {
                        String restrictionProfileId;

                        // Also check the current user's workflow profiles to make sure the task is accessible.
                        restrictionProfileId = taskState.getRestrictionProfileId();
                        if ((restrictionProfileId == null) || (sessionContext.getWorkFlowProfileIdentifiers().contains(restrictionProfileId)))
                        {
                            T8ConfigurationManager configurationManager;

                            // Set the user identifier on the task details.
                            taskState.setClaimedByUserId(userId);
                            taskState.setTimeClaimed(new T8Timestamp(System.currentTimeMillis()));

                            // Recalculate time spans.
                            configurationManager = internalContext.getServerContext().getConfigurationManager();
                            taskState.recalculateTimespanIssued(configurationManager.getOperationalHoursCalculator(context));

                            // Persist the state to reflect the changes.
                            persistState();

                            // Log the event.
                            stateHandler.logTaskEvent(context, TaskEvent.CLAIMED, taskState, null);

                            // Return the updated task state.
                            return taskState;
                        }
                        else throw new Exception("Access Violation.  Task not available to current user.");
                    }
                    else throw new Exception("Access Violation.  Task not available to current user.");
                }
                else throw new Exception("Task already claimed: " + taskIid);
            }
            else throw new Exception("Task state '" + taskIid + "' could not be found in flow: " + flowState);
        }
    }

    @Override
    public T8FlowTaskState escalateTask(T8Context context, T8TaskEscalationTrigger trigger, String taskIid) throws Exception
    {
        synchronized (flowState)
        {
            T8FlowTaskState taskState;

            // Get the flow and and the state of the specified task.
            taskState = flowState.getTaskState(taskIid);
            if (taskState != null)
            {
                T8FlowTaskEscalatedEvent event;
                String escalationId;
                int newPriority;
                int oldPriority;

                // Calculate the new priority.
                oldPriority = taskState.getPriority();
                if (trigger.getPriority() != null)
                {
                    newPriority = trigger.getPriority();
                }
                else if (trigger.getPriorityIncrease() != null)
                {
                    newPriority = oldPriority + trigger.getPriorityIncrease();
                }
                else newPriority = oldPriority;

                // Set the new priority.
                taskState.setPriority(newPriority);
                taskState.setTimeEscalated(new T8Timestamp(System.currentTimeMillis()));
                taskState.setEscalationLevel(trigger.getTimeElapsed()); // The elapsed time of the trigger is used as escalation level.

                // Execute the escalation script.
                escalationId = trigger.getEscalationId();
                if (escalationId != null)
                {
                    T8TaskEscalationDefinition escalationDefinition;

                    // Get the specified escalation definition.
                    escalationDefinition = flowController.getTaskEscalationDefinition(definition.getRootProjectId(), escalationId);
                    if (escalationDefinition != null)
                    {
                        // Make sure a script is defined for the escalation.
                        if (escalationDefinition.getScriptDefinition() != null)
                        {
                            Map<String, Object> escalationParameters;
                            T8Script script;

                            // Create the map of input parameters to use when executing escalation script.
                            escalationParameters = new HashMap<>();
                            escalationParameters.put(escalationId + T8TaskEscalationScriptDefinition.PARAMETER_TRIGGER, trigger);
                            escalationParameters.put(escalationId + T8TaskEscalationScriptDefinition.PARAMETER_TASK_STATE, taskState);

                            // Execute the escalation script.
                            script = escalationDefinition.getScriptDefinition().getNewScriptInstance(context);
                            script.executeScript(escalationParameters);
                        }
                    }
                    else throw new Exception("Specified escalation definition not found: " + escalationId);
                }

                // Persist the state to reflect the changes.
                persistState();

                // Log the event.
                stateHandler.logTaskEvent(context, TaskEvent.ESCALATED, taskState, null);

                // Fire an event to all active listeners to indicate that a task has been escalated.
                event = new T8FlowTaskEscalatedEvent(trigger, taskIid, oldPriority, newPriority);
                flowController.processFlowEvent(event);

                // Return the updated task state.
                return taskState;
            }
            else throw new Exception("Flow state did not contain expected task: " + taskIid);
        }
    }

    @Override
    public void reassignTask(String taskInstanceIdentifier, String userId, String profileId) throws Exception
    {
    }

    /**
     * Persists the current state of the flow, synchronizing on the T8FlowState
     * object until the operation has finished.
     */
    @Override
    public void persistState()
    {
        try
        {
            synchronized (flowState)
            {
                stateHandler.saveFlowState(flowState);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while persisting flow state: " + flowState, e);
        }
    }
}
