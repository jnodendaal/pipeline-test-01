package com.pilog.t8.config;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.definition.ui.laf.T8LookAndFeelDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.security.T8Context;
import java.util.Map;
import java.util.Set;

import static com.pilog.t8.definition.system.T8ConfigurationManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ConfigurationManagerOperations
{
    public static class GetLookAndFeelDefinitionOperation extends T8DefaultServerOperation
    {
        public GetLookAndFeelDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8LookAndFeelDefinition> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DefinitionManager definitionManager;
            T8SystemDefinition systemDefinition;
            T8LookAndFeelDefinition lookAndFeelDefinition;
            String lafIdentifier;

            definitionManager = serverContext.getDefinitionManager();
            systemDefinition = definitionManager.getSystemDefinition(context);
            lafIdentifier = systemDefinition.getLookAndFeelId();
            lookAndFeelDefinition = lafIdentifier != null ? (T8LookAndFeelDefinition)definitionManager.getInitializedDefinition(context, null, lafIdentifier, null) : null;

            return HashMaps.createSingular(PARAMETER_LOOK_AND_FEEL_DEFINITION, lookAndFeelDefinition);
        }
    }

    public static class GetAPIClassMapOperation extends T8DefaultServerOperation
    {
        public GetAPIClassMapOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Map<String, String>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ConfigurationManager configurationManager;

            configurationManager = serverContext.getConfigurationManager();
            return HashMaps.createSingular(PARAMETER_API_CLASS_MAP, configurationManager.getAPIClassMap(context));
        }
    }

    public static class GetUITranslationMapOperation extends T8DefaultServerOperation
    {
        public GetUITranslationMapOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Map<String, String>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ConfigurationManager configurationManager;
            String languageIdentifier;

            configurationManager = serverContext.getConfigurationManager();
            languageIdentifier = (String)operationParameters.get(PARAMETER_LANGUAGE_IDENTIFIER);

            return HashMaps.createSingular(PARAMETER_UI_TRANSLATION_MAP, configurationManager.getUITranslationMap(context, languageIdentifier));
        }
    }

    public static class RequestUITranslationsOperation extends T8DefaultServerOperation
    {
        public RequestUITranslationsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ConfigurationManager configurationManager;
            String languageIdentifier;
            Set<String> untranslatedPhrases;

            configurationManager = serverContext.getConfigurationManager();
            languageIdentifier = (String)operationParameters.get(PARAMETER_LANGUAGE_IDENTIFIER);
            untranslatedPhrases = (Set<String>)operationParameters.get(PARAMETER_UI_UNTRANSLATED_PHRASE_SET);

            configurationManager.requestUITranslations(context, languageIdentifier, untranslatedPhrases);
            return HashMaps.createSingular(PARAMETER_SUCCESS, true);
        }
    }
}
