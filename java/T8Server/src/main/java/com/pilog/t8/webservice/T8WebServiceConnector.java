package com.pilog.t8.webservice;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.remote.server.connection.T8ConnectionDefinition;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.pilog.t8.definition.remote.server.connection.T8Connection;
import com.pilog.t8.security.T8AuthenticationUtilities;
import com.pilog.t8.security.T8Context;
import java.io.OutputStreamWriter;

/**
 * @author Gavin Boshoff
 */
public abstract class T8WebServiceConnector implements T8Connection
{
    protected final T8Context context;
    protected final T8SessionContext sessionContext;
    protected final T8ServerContext serverContext;
    private final String serverUrl;

    public T8WebServiceConnector(T8Context context, T8ConnectionDefinition remoteConnectionDefinition)
    {
        this.context = context;
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.serverUrl = remoteConnectionDefinition.getURL();
    }

    public abstract String getAPIKey() throws Exception;

    public String executeRemoteOperation(String method, String executionType, String remoteOperationIdentifier, String content) throws Exception
    {
        HttpURLConnection connection = null;
        URL url;

        url = new URL(this.serverUrl + "/api/operation/" + remoteOperationIdentifier);

        try
        {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(true);
            connection.setAllowUserInteraction(false);
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("exec-type", executionType);
            connection.setRequestProperty("Content-Type", "application/json");

            // Add the authentication headers.
            T8AuthenticationUtilities.addTokenAuthentication(connection, this.getAPIKey());

            // Send request.
            try (OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream()))
            {
                writer.write(content, 0, content.length());
                writer.flush();
            }

            if (connection.getResponseCode() == 200)
            {
                // Get response
                StringBuilder response;
                InputStream inStream;
                String line;

                inStream = connection.getInputStream();
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(inStream)))
                {
                    response = new StringBuilder();
                    while ((line = reader.readLine()) != null)
                    {
                        response.append(line);
                    }
                }

                return response.toString();
            }
            else  if (connection.getResponseCode() == 500) throw new Exception("HTTP 500 : HTTP call failed due to an error on the remote server. Contact the administrator to review the issue.");
            else throw new Exception("HTTP Error Status Code:" + connection.getResponseCode() + ":" + connection.getResponseMessage() + ", URL:" + url);
        }
        finally
        {
            if (connection != null) connection.disconnect();
        }
    }
}