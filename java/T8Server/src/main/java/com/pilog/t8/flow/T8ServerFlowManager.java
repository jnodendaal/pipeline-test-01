package com.pilog.t8.flow;

import com.pilog.t8.flow.task.object.T8TaskObjectHandler;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow.FlowHistoryEvent;
import com.pilog.t8.flow.T8Flow.FlowStatus;
import com.pilog.t8.flow.event.T8FlowCompletedEvent;
import com.pilog.t8.flow.event.T8FlowEvent;
import com.pilog.t8.flow.event.T8FlowEventListener;
import com.pilog.t8.flow.event.T8FlowFinalizedEvent;
import com.pilog.t8.flow.event.T8FlowSignalReceivedEvent;
import com.pilog.t8.flow.event.T8FlowTaskClaimedEvent;
import com.pilog.t8.flow.event.T8FlowTaskCompletedEvent;
import com.pilog.t8.flow.event.T8FlowTaskPriorityChangedEvent;
import com.pilog.t8.flow.event.T8FlowTaskReassignedEvent;
import com.pilog.t8.flow.event.T8FlowTaskUnclaimedEvent;
import com.pilog.t8.flow.key.T8RetryFlowExecutionKey;
import com.pilog.t8.flow.phase.T8FlowPhase;
import com.pilog.t8.flow.phase.T8FlowPhaseProfileDetails;
import com.pilog.t8.flow.phase.T8FlowPhaseUserDetails;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.flow.task.T8FlowTask.TaskEvent;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.flow.task.T8TaskCompletionResult;
import com.pilog.t8.flow.task.T8TaskCompletionResult.T8TaskCompletionResultType;
import com.pilog.t8.flow.task.T8TaskEscalationTrigger;
import com.pilog.t8.flow.task.T8TaskValidationResult;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.definition.flow.node.activity.T8TaskActivityDefinition;
import com.pilog.t8.definition.flow.signal.T8FlowSignalDefinition;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.task.T8TaskEscalationDefinition;
import com.pilog.t8.definition.flow.task.object.T8TaskObjectDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.flow.task.T8TaskList;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8WorkflowContext;
import com.pilog.t8.security.T8Context.T8WorkflowTaskContext;
import com.pilog.t8.thread.T8ContextRunnable;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.event.EventListenerList;


/**
 * Design Notes:
 * -------------
 * In general the design of the flow engine is to provide methods that act
 * synchronously on the immediately affected state according to the contract of
 * the method.  After the state has been updated, an event is fired that will
 * result in asynchronous processing of the secondary effects of the state
 * change (such as continuation of node execution).
 *
 * Thus:  State change is applied synchronously and immediately.  The subsequent result of the state change is processed asynchronously.
 *
 * @author Bouwer du Preez
 */
public class T8ServerFlowManager implements T8FlowController, T8FlowManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerFlowManager.class);

    private static final int EVENT_QUEUE_INITIAL_DELAY = 120;
    private static final int TASK_ESCALATION_INITIAL_DELAY = 120;
    private static final int FLOW_QUEUE_INITIAL_DELAY = 120;
    private static final int FINALIZED_DELETION_INITIAL_DELAY = 120;
    private static final int FLOW_QUEUE_POLLING_INTERVAL = 15;
    private static final int EVENT_QUEUE_POLLING_INTERVAL = 5; // The interval (in seconds) at which queued flow events will be polled for execution.
    private static final int TASK_ESCALATION_INTERVAL = 60; // The interval (in seconds) at which task priority escalation will be performed.
    private static final int FINALIZED_DELETION_INTERVAL = 60; // The interval (in seconds) at which finalized flow states will be deleted.

    private final T8FlowCache flows; // Stores currently executing flows.
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final EventListenerList flowListeners;
    private final T8DefaultFlowStateHandler stateHandler;
    private final T8TaskStateHandler taskStateHandler;
    private final T8TaskValidationHandler taskValidationHandler;
    private final T8SecurityManager securityManager;
    private ScheduledExecutorService executor; // Timer used to update process details.
    private FlowEventQueueThread eventQueueThread;
    private TaskEscalationThread taskEscalationThread;
    private T8QueuedFlowHandler flowQueueThread;
    private T8FinalizedDeletionThread finalizedDeletionThread;

    public T8ServerFlowManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.securityManager = serverContext.getSecurityManager();
        this.internalContext = securityManager.createServerModuleContext(this);
        this.flowListeners = new EventListenerList();
        this.stateHandler = new T8DefaultFlowStateHandler(internalContext);
        this.taskStateHandler = new T8TaskStateHandler(internalContext, this);
        this.taskValidationHandler = new T8TaskValidationHandler(internalContext, this);
        this.flows = new T8FlowCache(internalContext, this, stateHandler, 1000);
        this.init();
    }

    public final void init()
    {
        try
        {
            T8SystemDefinition systemDefinition;
            boolean workflowEnabled;

            // Get the system definition to determine configuration.
            systemDefinition = serverContext.getDefinitionManager().getSystemDefinition(internalContext);
            workflowEnabled = systemDefinition != null && systemDefinition.isFlowManagerEnabled();

            // Start the maintenance timer.
            this.executor = Executors.newScheduledThreadPool(2, new NameableThreadFactory("T8ServerFlowController-Maintenance"));

            // Schedule the thread to take care of the flow event queue.
            this.eventQueueThread = new FlowEventQueueThread(internalContext, this, stateHandler, workflowEnabled);
            this.executor.scheduleWithFixedDelay(eventQueueThread, EVENT_QUEUE_INITIAL_DELAY, EVENT_QUEUE_POLLING_INTERVAL, TimeUnit.SECONDS);

            // Schedule the thread to take care of the queued flows.
            this.flowQueueThread = new T8QueuedFlowHandler(internalContext, this, workflowEnabled);
            this.executor.scheduleWithFixedDelay(flowQueueThread, FLOW_QUEUE_INITIAL_DELAY, FLOW_QUEUE_POLLING_INTERVAL, TimeUnit.SECONDS);

            // Schedule the thread to take care of finalized state deletion.
            this.finalizedDeletionThread = new T8FinalizedDeletionThread(internalContext, this, stateHandler, workflowEnabled);
            this.executor.scheduleWithFixedDelay(finalizedDeletionThread, FINALIZED_DELETION_INITIAL_DELAY, FINALIZED_DELETION_INTERVAL, TimeUnit.SECONDS);

            // Schedule the thread to take care of task escalation.
            this.taskEscalationThread = new TaskEscalationThread(internalContext, this, taskStateHandler, workflowEnabled);
            this.executor.scheduleWithFixedDelay(taskEscalationThread, TASK_ESCALATION_INITIAL_DELAY, TASK_ESCALATION_INTERVAL, TimeUnit.SECONDS);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing flow controller.", e);
        }
    }

    @Override
    public void start() throws Exception
    {
        // No start-up routine for this module.
    }

    @Override
    public synchronized void destroy()
    {
        List<T8Flow> flowList;

        // Terminate the timer thread and all scheduled tasks.
        try
        {
            // Shut down scheduled tasks.
            eventQueueThread.setEnabled(false);
            flowQueueThread.setEnabled(false);
            finalizedDeletionThread.setEnabled(false);
            taskEscalationThread.setEnabled(false);
            executor.shutdown();
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) throw new Exception("Flow maintenance thread executor could not by terminated.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while awaiting termination of flow maintenance thread.", e);
        }

        // Destroy flow checker thread.
        eventQueueThread.destroy();
        eventQueueThread = null;

        // Disable persistence so that no further changes to the state are saved while the controller shuts down.
        stateHandler.setEnabled(false);

        // Now setEnabled all flows.
        flowList = flows.getCachedFlows();
        for (T8Flow flow : flowList)
        {
            try
            {
                flow.stopFlow();
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while stopping flow '" + flow.getInstanceIdentifier() + "'.", e);
            }
        }
    }

    @Override
    public void addFlowEventListener(T8Context context, T8FlowEventListener listener)
    {
        flowListeners.add(T8FlowEventListener.class, listener);
    }

    @Override
    public void removeFlowEventListener(T8Context context, T8FlowEventListener listener)
    {
        flowListeners.remove(T8FlowEventListener.class, listener);
    }

    @Override
    public int getFlowCount(T8Context context)
    {
        return flows.size();
    }

    @Override
    public int getCachedFlowCount(T8Context context)
    {
        return flows.size();
    }

    @Override
    public int clearFlowCache(T8Context context)
    {
        return flows.clearNonExecutingFlows();
    }

    @Override
    public T8FlowState getFlowState(T8Context context, String flowIid)
    {
        T8Flow flow;

        flow = flows.getFlow(flowIid);
        return flow.getState();
    }

    /**
     * Returns a flow from the current set of cached flows. Package private to
     * ensure restricted access.
     *
     * @param flowIid The {@code String} flow instance identifier
     *      to be retrieved
     *
     * @return The T8Flow from the available flows
     */
    T8Flow getFlow(String flowIid)
    {
        return this.flows.getFlow(flowIid);
    }

    @Override
    public T8FlowStatus getFlowStatus(T8Context context, String flowIid, boolean includeDisplayData) throws Exception
    {
        List<T8TaskDetails> availableTasks;
        T8FlowStatus flowStatus;
        String languageId;
        T8Flow flow;

        // Get the flow status and add the available tasks (if a task filter is specified).
        flow = flows.getFlow(flowIid);
        flowStatus = flow.getStatus();

        // Determine the available tasks for the current user.
        languageId = context.getLanguageId();
        availableTasks = new ArrayList<>();
        for (T8FlowTaskState taskState : flow.getState().getTaskStates(context.getUserId(), context.getWorkflowProfileIds()))
        {
            T8FlowTaskDefinition taskDefinition;
            T8WorkFlowNodeDefinition nodeDefinition;

            taskDefinition = getTaskDefinition(taskState.getProjectId(), taskState.getTaskId());
            nodeDefinition = flow.getDefinition().getNodeDefinition(taskState.getNodeId());
            availableTasks.add(taskState.getTaskDetails(languageId, (T8TaskActivityDefinition)nodeDefinition, taskDefinition));
        }
        flowStatus.setAvailableTasks(availableTasks);

        // Add display data if required.
        if (includeDisplayData)
        {
            T8WorkFlowDefinition flowDefinition;
            T8FlowPhase phaseModel;

            flowDefinition = flow.getDefinition();
            flowStatus.setFlowDisplayName(flowDefinition.getMetaDisplayName());

            phaseModel = flowStatus.getPhases();
            if (phaseModel != null)
            {
                T8DefinitionManager definitionManager;
                T8DefinitionMetaData metaData;

                definitionManager = serverContext.getDefinitionManager();
                metaData = definitionManager.getDefinitionMetaData(null, flowStatus.getInitiatorUserIdentifier());
                if (metaData != null) flowStatus.setInitiatorUserDisplayName(metaData.getDisplayName());

                for (T8FlowPhase phase : phaseModel.getSubsequentPhases())
                {
                    for (T8FlowPhaseUserDetails userDetails : phase.getUserDetails())
                    {
                        metaData = definitionManager.getDefinitionMetaData(null, userDetails.getIdentifier());
                        if (metaData != null) userDetails.setDisplayName(metaData.getDisplayName());
                    }

                    for (T8FlowPhaseProfileDetails profileDetails : phase.getProfileDetails())
                    {
                        metaData = definitionManager.getDefinitionMetaData(null, profileDetails.getIdentifier());
                        if (metaData != null) profileDetails.setDisplayName(metaData.getDisplayName());
                    }
                }
            }
        }

        return flowStatus;
    }

    @Override
    public List<String> findActiveInputNodeInstanceIdentifiers(T8Context context, String flowIid, String nodeIid) throws Exception
    {
        T8Flow flow;

        // Get the flow and then find the active input node instance identifiers.
        flow = flows.getFlow(flowIid);
        return T8FlowUtilities.findActiveInputIdentifiers(flow, flow.getState(), flow.getState().getNodeState(nodeIid));
    }

    @Override
    public T8WorkFlowDefinition getFlowDefinition(String projectId, String flowId)
    {
        try
        {
            return (T8WorkFlowDefinition)serverContext.getDefinitionManager().getInitializedDefinition(internalContext, projectId, flowId, null);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving flow definition: " + flowId, e);
        }
    }

    @Override
    public T8FlowTaskDefinition getTaskDefinition(String projectId, String taskId)
    {
        try
        {
            return (T8FlowTaskDefinition)serverContext.getDefinitionManager().getInitializedDefinition(internalContext, projectId, taskId, null);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving task definition: " + taskId, e);
        }
    }

    @Override
    public T8FlowSignalDefinition getSignalDefinition(String projectId, String signalId)
    {
        try
        {
            return (T8FlowSignalDefinition)serverContext.getDefinitionManager().getInitializedDefinition(internalContext, projectId, signalId, null);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving signal definition: " + signalId, e);
        }
    }

    @Override
    public T8TaskEscalationDefinition getTaskEscalationDefinition(String projectId, String escalationId)
    {
        try
        {
            return (T8TaskEscalationDefinition)serverContext.getDefinitionManager().getInitializedDefinition(internalContext, null, escalationId, null);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving task escalation definition: " + escalationId, e);
        }
    }

    private T8TaskObjectHandler getTaskObjectHandler(String taskObjectId)
    {
        try
        {
            T8TaskObjectDefinition objectDefinition;

            objectDefinition = (T8TaskObjectDefinition)serverContext.getDefinitionManager().getInitializedDefinition(internalContext, null, taskObjectId, null);
            if (objectDefinition != null)
            {
                return (T8TaskObjectHandler)objectDefinition.getDataObjectHandler(serverContext.getDataManager().getCurrentSession().instantTransaction());
            }
            else throw new RuntimeException("Task Object not found: " + taskObjectId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving task object definition: " + taskObjectId, e);
        }
    }

    @Override
    public void retryExecution(T8Context context, String flowIid, String nodeIid)
    {
        final T8RetryFlowExecutionKey executionKey;

        // Create a retry execution key.
        LOGGER.log("Retrying execution of node: " + nodeIid + "  in flow: " + flowIid + "...");
        executionKey = new T8RetryFlowExecutionKey(flowIid, nodeIid);

        // Now resume execution using the key.
        execute(executionKey);
    }

    private void execute(T8FlowExecutionKey key)
    {
        // Now resume execution using the key.
        new Thread(new T8ContextRunnable(internalContext, "T8FlowExecutionKeyExecutionThread:" + key)
        {
            @Override
            public void exec()
            {
                try
                {
                    String flowIid;
                    T8Flow flow;

                    // Get the flow instance identifier from the key.
                    flowIid = key.getFlowInstanceIdentifier();
                    Thread.currentThread().setName("T8FlowExecutionThread:" + flowIid);

                    // Get the target flow and resume its execution.
                    flow = flows.getFlow(flowIid);
                    flow.executeFlow(key);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception in while resuming flow execution: " + key, e);
                }
            }
        }).start();
    }

    @Override
    public T8FlowStatus queueFlow(T8Context context, String flowId, Map<String, Object> inputParameters) throws Exception
    {
        T8WorkFlowDefinition flowDefinition;

        // Get the definition of the flow to be queued.
        flowDefinition = (T8WorkFlowDefinition)serverContext.getDefinitionManager().getRawDefinition(null, null, flowId);
        if (flowDefinition != null)
        {
            T8Context flowContext;
            T8FlowState flowState;
            String flowIid;
            T8Flow flow;

            // Create the context for the flow.
            flowIid = T8IdentifierUtilities.createNewGUID();
            flowContext = new T8WorkflowContext(context, flowDefinition.getIdentifier(), flowIid);

            // Create the new flow state.
            flowState = new T8FlowState(flowIid);
            flowState.setFlowStatus(FlowStatus.QUEUED);
            flowState.setFlowId(flowId);
            flowState.setInputParameters( T8IdentifierUtilities.stripNamespace(flowId, inputParameters, true));
            flowState.setInitiatorId(flowContext.getAgentId());
            flowState.setInitiatorIid(flowContext.getAgentIid());
            flowState.setInitiatorOrgId(context.getOrganizationId());
            flowState.setInitiatorRootOrgId(context.getRootOrganizationId());
            flowState.setTimeQueued(new T8Timestamp(System.currentTimeMillis()));

            // Create a new flow instance.
            flow = flowDefinition.getNewFlowInstance(flowContext, this, stateHandler, flowState);
            flows.addFlow(flow);

            // The flow was started successfully so log the flow event.
            stateHandler.saveFlowState(flowState);
            stateHandler.logFlowEvent(flowContext, FlowHistoryEvent.QUEUED, flow, inputParameters, null);
            return flow.getStatus();
        }
        else throw new Exception("Flow Definition not found: " + flowId);
    }

    @Override
    public T8FlowStatus startFlow(T8Context context, String flowId, Map<String, Object> inputParameters) throws Exception
    {
        T8WorkFlowDefinition flowDefinition;

        flowDefinition = (T8WorkFlowDefinition)serverContext.getDefinitionManager().getRawDefinition(context, context.getProjectId(), flowId);
        if (flowDefinition != null)
        {
            T8Context flowExecutionContext;
            T8FlowState flowState;
            T8Flow flow;

            // Create the new flow state.
            flowState = new T8FlowState(T8IdentifierUtilities.createNewGUID());

            // Create the session context for the flow.
            flowExecutionContext = new T8WorkflowContext(context, flowDefinition.getIdentifier(), flowState.getFlowIid());

            // Create a new flow instance.
            flow = flowDefinition.getNewFlowInstance(flowExecutionContext, this, stateHandler, flowState);

            // Attempt to start the flow.  This part of the error handling is
            // synchronous with the invocation of this method.  If the flow cannot
            // be started, no further logging of the failure will occur and a simple
            // exception will be thrown.  If the flow is started successfully, all
            // subsequent failures are logged properly as part of the flow state and
            // flow management procedure.
            try
            {
                // Start the flow.
                flows.addFlow(flow);
                flow.startFlow(inputParameters);
            }
            catch (Exception e)
            {
                // If anything, went wrong during the flow start, make sure to remove the flow from the active
                flows.removeFlow(flow.getInstanceIdentifier());
                throw e;
            }

            // The flow was started successfully so log the flow event.
            stateHandler.saveFlowState(flowState);
            stateHandler.logFlowEvent(flowExecutionContext, FlowHistoryEvent.STARTED, flow, inputParameters, null);
            return flow.getStatus();
        }
        else throw new Exception("Flow Definition not found: " + flowId);
    }

    /**
     * Starts a flow that was previously queued for execution.
     * @param flowIid The instance id of the flow to start.
     * @return The status of the flow once it has been started.
     * @throws Exception
     */
    T8FlowStatus startQueuedFlow(T8Context context, String flowIid) throws Exception
    {
        T8Flow flow;

        // Get the queued flow.
        flow = flows.getFlow(flowIid);
        if (flow != null)
        {
            T8FlowState flowState;

            // Get the state of the queued flow.
            flowState = flow.getState();

            // Attempt to start the flow.  This part of the error handling is
            // synchronous with the invocation of this method.  If the flow cannot
            // be started, no further logging of the failure will occur and a simple
            // exception will be thrown.  If the flow is started successfully, all
            // subsequent failures are logged properly as part of the flow state and
            // flow management procedure.
            try
            {
                // Start the flow.
                flow.startFlow();
            }
            catch (Exception e)
            {
                // If anything, went wrong during the flow start, make sure to remove the flow from the active collection.
                flows.removeFlow(flow.getInstanceIdentifier());
                throw e;
            }

            // The flow was started successfully so log the flow event.
            stateHandler.logFlowEvent(context, FlowHistoryEvent.STARTED, flow, flowState.getInputParameters(), null);
            return flow.getStatus();
        }
        else throw new Exception("Queued flow not found: " + flowIid);
    }

    @Override
    public void killFlow(T8Context context, String flowIid) throws Exception
    {
        try
        {
            T8Flow flow;

            // Get the executing flow or resume it if not found.
            flow = flows.getFlow(flowIid);

            // Finalize the flow.
            LOGGER.log("Killing flow: " + flowIid);
            flow.finalizeFlow();

            // Delete the flow state.
            stateHandler.deleteFlowState(flowIid);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while killing flow: " + flowIid, e);
        }
    }

    @Override
    public void reassignTask(T8Context context, String taskIid, String userId, String profileId) throws Exception
    {
        T8FlowTaskState taskState;

        taskState = taskStateHandler.loadTaskState(taskIid);
        if (taskState != null)
        {
            T8FlowTaskReassignedEvent event;

            // Fire an event to all active listeners that a task has been reassigned.
            event = new T8FlowTaskReassignedEvent(taskState.getTaskIid(), userId, profileId);
            processFlowEvent(event);

            // Log the event.
            stateHandler.logTaskEvent(context, TaskEvent.REASSIGNED, taskState, null);
        }
        else throw new Exception("Unabled to reassign task because it does not exist in the task list: " + taskIid);
    }

    @Override
    public void claimTask(T8Context context, String taskIid) throws Exception
    {
        String flowIid;

        flowIid = taskStateHandler.findFlowInstanceByTaskInstance(taskIid);
        if (flowIid != null)
        {
            T8FlowTaskDefinition taskDefinition;
            T8FlowTaskState taskState;
            T8FlowTaskClaimedEvent event;
            T8TaskObjectHandler handler;
            T8Flow flow;

            // Get the flow and claim the task.
            flow = flows.getFlow(flowIid);
            taskState = flow.claimTask(context, taskIid);
            taskDefinition = getTaskDefinition(taskState.getProjectId(), taskState.getTaskId());

            // Refresh the task object.
            handler = getTaskObjectHandler(taskDefinition.getTaskObjectId());
            if (handler != null) handler.refreshTaskObject(TaskEvent.CLAIMED, taskState, T8IdentifierUtilities.mapParameters(taskState.getInputParameters()));

            // Fire an event to all active listeners that a task has been claimed.
            event = new T8FlowTaskClaimedEvent(taskIid, context.getUserId());
            processFlowEvent(event);
        }
        else throw new Exception("Unabled to claim task because it does not exist in the task list: " + taskIid);
    }

    @Override
    public void unclaimTask(T8Context context, String taskIid) throws Exception
    {
        throw new RuntimeException("Un-claiming of tasks not supported yet.");
    }

    @Override
    public int getTaskCount(T8Context context, T8TaskFilter taskFilter) throws Exception
    {
        return taskStateHandler.getTaskCount(context, taskFilter);
    }

    @Override
    public T8TaskListSummary getTaskListSummary(T8Context context, T8TaskFilter taskFilter) throws Exception
    {
        return taskStateHandler.getTaskListSummary(context, taskFilter);
    }

    @Override
    public T8TaskDetails getTaskDetails(T8Context context, String taskIid) throws Exception
    {
        return taskStateHandler.getTaskDetails(context, taskIid);
    }

    @Override
    public Map<String, Object> getTaskUiData(T8Context context, String taskIid) throws Exception
    {
        return T8IdentifierUtilities.stripNamespace(taskStateHandler.getTaskDetails(context, taskIid).getTaskInputParameters());
    }

    @Override
    public T8TaskList getTaskList(T8Context context, T8TaskFilter taskFilter) throws Exception
    {
        return taskStateHandler.getTaskList(context, taskFilter);
    }

    @Override
    public void increaseTaskPriority(T8Context context, String taskIid, int amount) throws Exception
    {
        String flowIid;

        // Find the flow instance to which the specified task belongs.
        flowIid = taskStateHandler.findFlowInstanceByTaskInstance(taskIid);
        if (flowIid != null)
        {
            T8FlowTaskState taskState;
            T8FlowState flowState;
            T8Flow flow;

            // Get the flow and and the state of the specified task.
            flow = flows.getFlow(flowIid);
            flowState = flow.getState();
            taskState = flowState.getTaskState(taskIid);
            if (taskState != null)
            {
                T8FlowTaskPriorityChangedEvent event;
                int oldPriority;

                // Synchronize on the flow state as we are about to make changes to it.
                synchronized (flowState)
                {
                    // Set the new priority.
                    oldPriority = taskState.getPriority();
                    taskState.setPriority(oldPriority + amount);
                    flow.persistState();
                }

                // Log the event.
                stateHandler.logTaskEvent(context, TaskEvent.PRIORITY_UPDATED, taskState, null);

                // Fire an event to all active listeners to indicate that a task has been unclaimed.
                event = new T8FlowTaskPriorityChangedEvent(taskIid, oldPriority, oldPriority + amount);
                processFlowEvent(event);
            }
            else throw new Exception("Flow state did not contain expected task: " + taskIid);
        }
        else throw new Exception("Unabled to decrease task priority because it does not exist in the task list: " + taskIid);
    }

    @Override
    public void decreaseTaskPriority(T8Context context, String taskIid, int amount) throws Exception
    {
        String flowIid;

        // Find the flow instance to which the specified task belongs.
        flowIid = taskStateHandler.findFlowInstanceByTaskInstance(taskIid);
        if (flowIid != null)
        {
            T8FlowTaskState taskState;
            T8FlowState flowState;
            T8Flow flow;

            // Get the flow and and the state of the specified task.
            flow = flows.getFlow(flowIid);
            flowState = flow.getState();
            taskState = flowState.getTaskState(taskIid);
            if (taskState != null)
            {
                T8FlowTaskPriorityChangedEvent event;
                int oldPriority;

                // Synchronize on the flow state as we are about to make changes to it.
                synchronized (flowState)
                {
                    // Set the new priority.
                    oldPriority = taskState.getPriority();
                    taskState.setPriority(oldPriority - amount);
                    flow.persistState();
                }

                // Log the event.
                stateHandler.logTaskEvent(context, TaskEvent.PRIORITY_UPDATED, taskState, null);

                // Fire an event to all active listeners to indicate that a task has been unclaimed.
                event = new T8FlowTaskPriorityChangedEvent(taskIid, oldPriority, oldPriority - amount);
                processFlowEvent(event);
            }
            else throw new Exception("Flow state did not contain expected task: " + taskIid);
        }
        else throw new Exception("Unabled to decrease task priority because it does not exist in the task list: " + taskIid);
    }

    @Override
    public void setTaskPriority(T8Context context, String taskIid, int priority) throws Exception
    {
        String flowIid;

        // Find the flow instance to which the specified task belongs.
        flowIid = taskStateHandler.findFlowInstanceByTaskInstance(taskIid);
        if (flowIid != null)
        {
            T8FlowTaskState taskState;
            T8FlowState flowState;
            T8Flow flow;

            // Get the flow and and the state of the specified task.
            flow = flows.getFlow(flowIid);
            flowState = flow.getState();
            taskState = flowState.getTaskState(taskIid);
            if (taskState != null)
            {
                T8FlowTaskPriorityChangedEvent event;
                int oldPriority;

                // Synchronize on the flow state as we are about to make changes to it.
                synchronized (flowState)
                {
                    // Set the new priority.
                    oldPriority = taskState.getPriority();
                    taskState.setPriority(priority);
                    flow.persistState();
                }

                // Log the event.
                stateHandler.logTaskEvent(context, TaskEvent.PRIORITY_UPDATED, taskState, null);

                // Fire an event to all active listeners to indicate that a task has been unclaimed.
                event = new T8FlowTaskPriorityChangedEvent(taskIid, oldPriority, priority);
                processFlowEvent(event);
            }
            else throw new Exception("Flow state did not contain expected task: " + taskIid);
        }
        else throw new Exception("Unabled to decrease task priority because it does not exist in the task list: " + taskIid);
    }

    private T8FlowTaskState escalateTask(T8Context context, T8TaskEscalationTrigger trigger, String taskIid) throws Exception
    {
        String flowIid;

        // Find the flow instance to which the specified task belongs.
        flowIid = taskStateHandler.findFlowInstanceByTaskInstance(taskIid);
        if (flowIid != null)
        {
            T8FlowTaskState taskState;
            T8Flow flow;

            // Get the flow and and the state of the specified task.
            flow = flows.getFlow(flowIid);
            taskState = flow.escalateTask(context, trigger, taskIid);
            return taskState;
        }
        else throw new Exception("Unabled to decrease task priority because it does not exist in the task list: " + taskIid);
    }

    @Override
    public T8TaskCompletionResult completeTask(T8Context context, String taskIid, Map<String, Object> taskOutputParameters) throws Exception
    {
        String flowIid;

        // Find the flow state to which the specified task belongs.
        flowIid = taskStateHandler.findFlowInstanceByTaskInstance(taskIid);
        if (flowIid != null)
        {
            T8FlowState flowState;
            T8Flow flow;

            // Get the flow.
            flow = flows.getFlow(flowIid);
            flowState = flow.getState();

            // Synchronize on the flow state because we are making changes to it.
            synchronized (flowState)
            {
                T8FlowTaskState taskState;
                String userId;

                // Get the task state and claim the task.
                taskState = flowState.getTaskState(taskIid);
                if (taskState != null)
                {
                    String claimedByUserId;
                    T8Context taskContext;

                    // Create the task context.
                    taskContext = new T8WorkflowTaskContext(context, taskState.getFlowId(), taskState.getFlowIid(), taskState.getTaskId(), taskState.getTaskIid());

                    // Get the id of the user that claimed the task.
                    claimedByUserId = taskState.getClaimedByUserId();

                    // Get the id of the current user.
                    userId = taskContext.getUserId();

                    // Make sure the task has not been claimed by another user.
                    if ((claimedByUserId == null) || (Objects.equals(userId, claimedByUserId)))
                    {
                        String restrictionUserId;

                        // Make sure the task restriction is met.
                        restrictionUserId = taskState.getRestrictionUserId();
                        if ((restrictionUserId == null) || (Objects.equals(userId, restrictionUserId)))
                        {
                            String restrictionProfileId;

                            // Make sure the task restriction is met.
                            restrictionProfileId = taskState.getRestrictionProfileId();
                            if ((restrictionUserId == null) || (taskContext.getWorkflowProfileIds().contains(restrictionProfileId)))
                            {
                                // Make sure the task has not already been completed.
                                if (!taskState.isCompleted())
                                {
                                    T8TaskValidationResult validationResult;

                                    // Validate the task completion.
                                    validationResult = taskValidationHandler.validateTaskCompletion(context, taskState, taskOutputParameters);
                                    if (validationResult.isValid())
                                    {
                                        Map<String, Object> finalOutputParameters;
                                        Map<String, Object> validationOutputParameters;
                                        T8ConfigurationManager configurationManager;
                                        T8FlowTaskCompletedEvent event;
                                        T8Timestamp timestamp;

                                        // Get the validation output parameters.
                                        validationOutputParameters = validationResult.getOutputParameters();

                                        // Create the final task output parameter collection.
                                        finalOutputParameters = new HashMap<>();
                                        if (taskOutputParameters != null) finalOutputParameters.putAll(taskOutputParameters);
                                        if (validationOutputParameters != null) finalOutputParameters.putAll(validationOutputParameters);

                                        // If this task was not claimed (immediate completion), set the claim details.
                                        timestamp = new T8Timestamp(System.currentTimeMillis());
                                        if (claimedByUserId == null)
                                        {
                                            taskState.setTimeClaimed(timestamp);
                                            taskState.setClaimedByUserId(taskContext.getUserId());
                                        }

                                        // Set the completion details.
                                        taskState.setTimeCompleted(timestamp);
                                        taskState.setOutputParameters(T8IdentifierUtilities.stripNamespace(finalOutputParameters));

                                        // Recalculate task timespans.
                                        configurationManager = serverContext.getConfigurationManager();
                                        taskState.recalculateTimespans(configurationManager.getOperationalHoursCalculator(context));

                                        // Persiste the changes to the flow state.
                                        flow.persistState();

                                        // Log the event.
                                        stateHandler.logTaskEvent(context, TaskEvent.COMPLETED, taskState, finalOutputParameters);

                                        // Fire an event to all active listeners to indicate that a task has been unclaimed.
                                        event = new T8FlowTaskCompletedEvent(getTaskDefinition(taskState.getProjectId(), taskState.getTaskId()), taskState.getTaskIid(), finalOutputParameters);
                                        queueFlowEvent(event);

                                        // Remove the task from the task list.
                                        revokeTask(context, taskState);

                                        // Return a successful task completion result.
                                        return new T8TaskCompletionResult(taskIid, T8TaskCompletionResultType.SUCCESS, finalOutputParameters);
                                    }
                                    else
                                    {
                                        T8FlowTaskDefinition taskDefinition;
                                        T8TaskObjectHandler handler;

                                        // Refresh the task object to reflect the latest validation failure and then return the result.
                                        taskDefinition = getTaskDefinition(taskState.getProjectId(), taskState.getTaskId());
                                        handler = getTaskObjectHandler(taskDefinition.getTaskObjectId());
                                        if (handler != null) handler.refreshTaskObject(TaskEvent.VALIDATION_FAILED, taskState, validationResult.getOutputParameters());
                                        return new T8TaskCompletionResult(taskIid, T8TaskCompletionResultType.FAILURE_VALIDATION, validationResult.getOutputParameters());
                                    }
                                }
                                else return new T8TaskCompletionResult(taskIid, T8TaskCompletionResultType.FAILURE_COMPLETED, null);
                            }
                            else return new T8TaskCompletionResult(taskIid, T8TaskCompletionResultType.FAILURE_ACCESS, null);
                        }
                        else return new T8TaskCompletionResult(taskIid, T8TaskCompletionResultType.FAILURE_ACCESS, null);
                    }
                    else return new T8TaskCompletionResult(taskIid, T8TaskCompletionResultType.FAILURE_CLAIMED, null);
                }
                else throw new IllegalStateException("Unabled to complete task because its state could not be found in parent flow state: " + flow.getState());
            }
        }
        else throw new Exception("Unabled to complete task because it does not exist in the task list: " + taskIid);
    }

    @Override
    public void issueTask(T8Context context, T8FlowTaskState taskState) throws Exception
    {
        T8FlowTaskDefinition taskDefinition;

        // Refresh the task object.
        taskDefinition =  getTaskDefinition(taskState.getProjectId(), taskState.getTaskId());
        if (taskDefinition != null)
        {
            T8TaskObjectHandler handler;

            // Refresh the task object.
            handler = getTaskObjectHandler(taskDefinition.getTaskObjectId());
            if (handler != null) handler.refreshTaskObject(TaskEvent.ISSUED, taskDefinition, taskState, T8IdentifierUtilities.mapParameters(taskState.getInputParameters(), taskDefinition.getTaskObjectParameterMapping()));

            // Log the event.
            stateHandler.logTaskEvent(null, TaskEvent.ISSUED, taskState, taskState.getInputParameters());
        }
        else throw new Exception("Task definition not found for task state: " + taskState);
    }

    @Override
    public void revokeTask(T8Context context, T8FlowTaskState taskState) throws Exception
    {
        T8FlowTaskDefinition taskDefinition;
        T8TaskObjectHandler handler;

        // Refresh the task object.
        taskDefinition = getTaskDefinition(taskState.getProjectId(), taskState.getTaskId());
        handler = getTaskObjectHandler(taskDefinition.getTaskObjectId());
        if (handler != null) handler.deleteTaskObject(taskState.getTaskIid());
    }

    @Override
    public void cancelTask(T8Context context, T8FlowTaskState taskState) throws Exception
    {
        T8FlowTaskDefinition taskDefinition;
        T8TaskObjectHandler handler;
        T8Flow flow;


        // Refresh the task object.
        flow = flows.getFlow(taskState.getFlowIid());
        taskDefinition = getTaskDefinition(taskState.getProjectId(), taskState.getTaskId());
        handler = getTaskObjectHandler(taskDefinition.getTaskObjectId());
        if (handler != null) handler.deleteTaskObject(taskState.getTaskIid());

        // Log the event.
        stateHandler.logTaskEvent(null, TaskEvent.CANCELLED, taskState, null);
    }

    @Override
    public void refreshTaskList(String taskIid) throws Exception
    {
        String flowIid;

        // Find the flow state to which the specified task belongs.
        flowIid = taskStateHandler.findFlowInstanceByTaskInstance(taskIid);
        if (flowIid != null)
        {
            T8FlowTaskState taskState;
            T8Flow flow;

            // Get the flow and claim the task.
            flow = flows.getFlow(flowIid);
            taskState = flow.getState().getTaskState(taskIid);
            if (taskState != null)
            {
                // Only refresh the task list for tasks that have not been completed.
                if (taskState.getTimeCompleted() == null)
                {
                    T8FlowTaskDefinition taskDefinition;

                    // Refresh the task view.
                    taskDefinition =  getTaskDefinition(taskState.getProjectId(), taskState.getTaskId());
                    if (taskDefinition != null)
                    {
                        T8TaskObjectHandler handler;

                        // Refresh the task object.
                        handler = getTaskObjectHandler(taskDefinition.getTaskObjectId());
                        if (handler != null) handler.refreshTaskObject(TaskEvent.DATA_UPDATED, taskDefinition, taskState, T8IdentifierUtilities.mapParameters(taskState.getInputParameters(), taskDefinition.getTaskObjectParameterMapping()));
                    }
                    else throw new Exception("Task definition not found for task state: " + taskState);
                }
                else LOGGER.log("Cannot refresh requested task list because the specified task has already been completed: " + taskIid);
            }
            else throw new Exception("Task state not found in parent flow state: " + flow.getState());
        }
        else throw new Exception("Task not found: " + taskIid);
    }

    @Override
    public void refreshFlowTaskList(String flowIid) throws Exception
    {
        T8Flow flow;

        // Get the flow and claim the task.
        flow = flows.getFlow(flowIid);
        for (T8FlowTaskState taskState : flow.getState().getTaskStates())
        {
            // Only refresh the task list for tasks that have not been completed.
            if (taskState.getTimeCompleted() == null)
            {
                T8FlowTaskDefinition taskDefinition;

                // Refresh the task view.
                taskDefinition =  getTaskDefinition(taskState.getProjectId(), taskState.getTaskId());
                if (taskDefinition != null)
                {
                    T8TaskObjectHandler handler;

                    // Refresh the task object.
                    handler = getTaskObjectHandler(taskDefinition.getTaskObjectId());
                    if (handler != null) handler.refreshTaskObject(TaskEvent.DATA_UPDATED, taskDefinition, taskState, T8IdentifierUtilities.mapParameters(taskState.getInputParameters(), taskDefinition.getTaskObjectParameterMapping()));
                }
                else throw new Exception("Task definition not found for task state: " + taskState);
            }
        }
    }

    @Override
    public void recacheTaskEscalationTriggers(T8Context context) throws Exception
    {
        taskEscalationThread.reloadTriggers();
    }

    @Override
    public void refreshTaskList(T8Context context, T8TaskListRefreshType refreshType, List<String> ids) throws Exception
    {
        switch (refreshType)
        {
            case FLOW_TYPE:
                // TODO:  Retrieve all flow iid's of the specified type, iterate over each one and refresh its task list.
                throw new UnsupportedOperationException("Not implemented yet.");
            case FLOW_INSTANCE:
                for (String flowIid : ids)
                {
                    refreshFlowTaskList(flowIid);
                }
                break;
            case TASK_TYPE:
                // TODO:  Retrieve all task iid's of the specified type, iterate over each one and refresh the task list.
                throw new UnsupportedOperationException("Not implemented yet.");
            case TASK_INSTANCE:
                for (String taskIid : ids)
                {
                    refreshTaskList(taskIid);
                }
                break;
            default:
                throw new Exception("Unsupported task list refresh type: " + refreshType);
        }
    }

    @Override
    public void fireSignalEvent(T8Context context, String signalId, Map<String, Object> signalParameters)
    {
        try
        {
            T8FlowSignalDefinition signalDefinition;

            signalDefinition = (T8FlowSignalDefinition)serverContext.getDefinitionManager().getInitializedDefinition(internalContext, null, signalId, null);
            if (signalDefinition != null)
            {
                T8FlowSignalReceivedEvent event;

                event = new T8FlowSignalReceivedEvent(signalDefinition, signalParameters);
                queueFlowEvent(event);
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while firing signal event: " + signalId, e);
        }
    }

    @Override
    public void fireMessageEvent(T8Context context, String messageId, Map<String, Object> messageParameters)
    {
        // TODO:  A message differs from a signal in that it has one sender and
        // one specific receiver.  We need to to find the specified receiver
        // (flow) from either the collection of active flows, or from the
        // collection of flow definitions that are not currently active.  If
        // the flow is active, we simply fire the message on it.  If the flow is
        // not active, we create a new instance and then we fire the event on
        // it.
    }

    private void queueFlowEvent(T8FlowEvent flowEvent)
    {
        stateHandler.queueFlowEvent(this, flowEvent);
    }

    @Override
    public void processFlowEvent(T8FlowEvent flowEvent)
    {
        new Thread(new T8ContextRunnable(internalContext, flowEvent.toString())
        {
            @Override
            public void exec()
            {
                T8DataFilterCriteria waitKeyFilterCriteria;

                // Get the criteria that can be used to fetch wait keys for this event.
                waitKeyFilterCriteria = flowEvent.getWaitKeyFilterCriteria();
                if (waitKeyFilterCriteria != null)
                {
                    T8DataIterator<T8DataEntity> waitKeyIterator = null;

                    try
                    {
                        waitKeyIterator = stateHandler.getWaitingFlowIterator(waitKeyFilterCriteria);
                        while (waitKeyIterator.hasNext())
                        {
                            T8WorkFlowNodeDefinition waitingNodeDefinition;
                            T8WorkFlowDefinition waitingFlowDefinition;
                            final T8FlowExecutionKey executionKey;
                            T8DataEntity waitKeyEntity;
                            String waitingFlowIid;
                            String waitingNodeIid;
                            T8Flow flow;

                            // Get the next wait key entity and use it to determine the waiting flow and node instances.
                            waitKeyEntity = waitKeyIterator.next();
                            waitingFlowIid = (String)waitKeyEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$FLOW_IID");
                            waitingNodeIid = (String)waitKeyEntity.getFieldValue(T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + "$NODE_IID");

                            // Fetch the waiting flow and node instances.
                            flow = flows.getFlow(waitingFlowIid);
                            if (flow != null)
                            {
                                T8FlowState waitingFlowState;
                                T8FlowNodeState waitingNodeState;

                                // Get the state of the waiting flow.
                                waitingFlowState = flow.getState();
                                waitingNodeState = waitingFlowState.getNodeState(waitingNodeIid);

                                // Get the waiting flow and node definitions.
                                waitingFlowDefinition = getFlowDefinition(waitingFlowState.getProjectId(), waitingFlowState.getFlowId());
                                waitingNodeDefinition = waitingFlowDefinition.getNodeDefinition(waitingNodeState.getNodeId());

                                // Create the execution key from the entity.
                                executionKey = flowEvent.getExecutionKey(waitingNodeDefinition, waitingNodeState);
                                if (executionKey != null)
                                {
                                    // Now resume execution using the key.
                                    execute(executionKey);
                                }
                            }
                            else LOGGER.log("WARNING: Flow referenced by wait key could not be found: " + waitingFlowIid);
                        }
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while processing flow event: " + flowEvent, e);
                    }
                    finally
                    {
                        if (waitKeyIterator != null) waitKeyIterator.close();
                    }
                }

                // Dispatch the event to external listeners.
                try
                {
                    if (flowEvent instanceof T8FlowCompletedEvent)
                    {
                        for (T8FlowEventListener listener : flowListeners.getListeners(T8FlowEventListener.class))
                        {
                            listener.flowCompleted((T8FlowCompletedEvent)flowEvent);
                        }
                    }
                    else if (flowEvent instanceof T8FlowFinalizedEvent)
                    {
                        for (T8FlowEventListener listener : flowListeners.getListeners(T8FlowEventListener.class))
                        {
                            listener.flowFinalized((T8FlowFinalizedEvent)flowEvent);
                        }

                        // After the event has been processed and dispatched, the flow can be removed from the cache.
                        flows.removeFlow(((T8FlowFinalizedEvent)flowEvent).getFlowStatus().getFlowIid());
                    }
                    else if (flowEvent instanceof T8FlowTaskCompletedEvent)
                    {
                        for (T8FlowEventListener listener : flowListeners.getListeners(T8FlowEventListener.class))
                        {
                            listener.taskCompleted((T8FlowTaskCompletedEvent)flowEvent);
                        }
                    }
                    else if (flowEvent instanceof T8FlowTaskClaimedEvent)
                    {
                        for (T8FlowEventListener listener : flowListeners.getListeners(T8FlowEventListener.class))
                        {
                            listener.taskClaimed((T8FlowTaskClaimedEvent)flowEvent);
                        }
                    }
                    else if (flowEvent instanceof T8FlowTaskUnclaimedEvent)
                    {
                        for (T8FlowEventListener listener : flowListeners.getListeners(T8FlowEventListener.class))
                        {
                            listener.taskUnclaimed((T8FlowTaskUnclaimedEvent)flowEvent);
                        }
                    }
                }
                catch (Exception dispatchException)
                {
                    LOGGER.log("Exception while dispatching flow event: " + flowEvent, dispatchException);
                }
            }
        }).start();
    }

    private static class FlowEventQueueThread extends Thread
    {
        private T8ServerFlowManager flowController;
        private T8DefaultFlowStateHandler stateHandler;
        private final T8ServerContext serverContext;
        private final T8SessionContext sessionContext;
        private boolean enabled;

        private FlowEventQueueThread(T8Context context, T8ServerFlowManager flowController, T8DefaultFlowStateHandler stateHandler, boolean enabled)
        {
            super("T8FlowEventQueue");
            this.serverContext = context.getServerContext();
            this.sessionContext = context.getSessionContext();
            this.flowController = flowController;
            this.stateHandler = stateHandler;
            this.enabled = enabled;
        }

        public void setEnabled(boolean enabled)
        {
            this.enabled = enabled;
        }

        @Override
        public void destroy()
        {
            flowController = null;
            stateHandler = null;
        }

        @Override
        public void run()
        {
            if (enabled)
            {
                try
                {
                    T8FlowEvent nextEvent;
                    T8Context context;

                    // Create a data session for this execution thread.
                    context = serverContext.getSecurityManager().createContext(sessionContext);
                    serverContext.getDataManager().openDataSession(context);

                    // Retrieve the next event.
                    nextEvent = stateHandler.retrieveNextFlowEvent(flowController);
                    if (nextEvent != null)
                    {
                        // Process the retreived event.
                        flowController.processFlowEvent(nextEvent);
                    }
                }
                catch (Throwable e)
                {
                    LOGGER.log("Exception while processing flow event queue.", e);
                }
                finally
                {
                    serverContext.getDataManager().closeCurrentSession();
                    serverContext.getSecurityManager().destroyContext();
                }
            }
        }
    }

    private static class TaskEscalationThread extends T8ContextRunnable
    {
        private final T8ServerFlowManager flowController;
        private final T8TaskStateHandler stateHandler;
        private final List<T8TaskEscalationTrigger> triggers;
        private boolean enabled;

        private static final int TASK_ESCALATION_PAGE_SIZE = 20; // The maximum number of task to reprioritize every time this thread executes.

        private TaskEscalationThread(T8Context context, T8ServerFlowManager flowController, T8TaskStateHandler stateHandler, boolean enabled) throws Exception
        {
            super(context, "TaskReprioritizationThread");
            this.flowController = flowController;
            this.stateHandler = stateHandler;
            this.triggers = new ArrayList<>();
            this.enabled = enabled;
            reloadTriggers();
        }

        public void setEnabled(boolean enabled) throws Exception
        {
            this.enabled = enabled;
            reloadTriggers();
        }

        public final void reloadTriggers() throws Exception
        {
            if (enabled)
            {
                // Reload escalation triggers from the database.
                // Synchronize on triggers to prevent concurrent modification while triggers are processed.
                synchronized(triggers)
                {
                    LOGGER.log("Loading task escalation triggers...");
                    triggers.clear();
                    triggers.addAll(stateHandler.retrieveTaskEscalationTriggers());
                }
            }
        }

        @Override
        public void exec()
        {
            if (enabled)
            {
                try
                {
                    // Process all of the triggers.
                    // Synchronize on triggers to prevent concurrent modification (reload of triggers).
                    synchronized(triggers)
                    {
                        for (T8TaskEscalationTrigger trigger : triggers)
                        {
                            List<String> escalationTaskIids;

                            // Retrieve all task priorities affected by the trigger.
                            escalationTaskIids = stateHandler.retrieveEscalationTaskIids(trigger, TASK_ESCALATION_PAGE_SIZE);
                            if (!escalationTaskIids.isEmpty())
                            {
                                LOGGER.log("Escalating " + escalationTaskIids.size() + " task priorities...");
                                for (String taskIid : escalationTaskIids)
                                {
                                    try
                                    {
                                        flowController.escalateTask(context, trigger, taskIid);
                                    }
                                    catch (Exception e)
                                    {
                                        LOGGER.log("Exception while escalating task: " + taskIid + " from trigger: " + trigger, e);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Throwable e)
                {
                    LOGGER.log("Exception while performing task priority escalation.", e);
                }
            }
        }
    }
}
