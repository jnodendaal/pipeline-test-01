package com.pilog.t8.script;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8CommunicationManagerScriptFacade
{
    private final T8CommunicationManager communicationManager;
    private final T8Context context;

    public T8CommunicationManagerScriptFacade(T8Context context, T8CommunicationManager comunicationManager)
    {
        this.communicationManager = comunicationManager;
        this.context = context;
    }

    public void queueCommunication(String communicationId, Map<String, Object> communicationParameters) throws Exception
    {
        communicationManager.queueCommunication(context, communicationId, communicationParameters);
    }

    public void sendCommunication(String communicationId, Map<String, Object> communicationParameters) throws Exception
    {
        communicationManager.sendCommunication(context, communicationId, communicationParameters);
    }
}