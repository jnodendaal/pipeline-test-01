package com.pilog.t8.data.connection.direct;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.definition.data.connection.direct.T8DirectDataConnectionDefinition;

/**
 * @author Bouwer du Preez
 */
public class T8DirectDataConnection
{
    public T8DirectDataConnection(T8ServerContext serverContext, T8SessionContext sessionContext, T8DirectDataConnectionDefinition definition) throws Exception
    {
        T8DataConnectionPool connectionPool;
        T8DataManager dataManager;
        
        // Get the data manager and the required connection pool.
        dataManager = serverContext.getDataManager();
        connectionPool = dataManager.getDataConnectionPool(definition.getIdentifier());

        // If the connection pool does not exist, install it.
        if (connectionPool == null)
        {
            ComboPooledDataSource cpds = new ComboPooledDataSource();
            cpds.setDriverClass(definition.getDriverClassName()); //loads the jdbc driver            
            cpds.setJdbcUrl(definition.getConnectionString());
            cpds.setUser(definition.getUsername());                                  
            cpds.setPassword(definition.getPassword()); 
        }
        
        // Now return the connection.
        
    }
}
