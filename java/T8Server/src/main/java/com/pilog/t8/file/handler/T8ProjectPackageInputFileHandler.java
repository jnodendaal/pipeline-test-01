package com.pilog.t8.file.handler;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.file.T8FileHandler;
import com.pilog.t8.project.T8ProjectPackage;
import com.pilog.t8.security.T8Context;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectPackageInputFileHandler implements T8FileHandler
{
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8FileManager fileManager;
    private final String fileContextIid;
    private final String filePath;
    private InputStreamReader inputReader;

    public static final String FILE_HANDLER_ID = "@FILE_PROJECT_PACKAGE_INPUT";

    public T8ProjectPackageInputFileHandler(T8Context context, T8FileManager fileManager, String contextIid, String filePath)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.fileManager = fileManager;
        this.fileContextIid = contextIid;
        this.filePath = filePath;
    }

    @Override
    public void open() throws Exception
    {
        InputStream inputStream;

        inputStream = fileManager.getFileInputStream(context, fileContextIid, filePath);
        inputReader = new InputStreamReader(inputStream); // No buffered reader required - the JsonParser handles buffering internally.
    }

    @Override
    public void close() throws Exception
    {
        if (inputReader != null) inputReader.close();
        inputReader = null;
    }

    /**
     * Reads all of the project definitions from the source file and returns a package containing all of the data.
     * @return A package of the definitions read from the source file.
     * @throws Exception
     */
    public T8ProjectPackage readPackage() throws Exception
    {
        T8ProjectPackage projectPackage;

        // Read the project package and return the result.
        projectPackage = new T8ProjectPackage();
        projectPackage.read(serverContext.getDefinitionManager(), inputReader);
        return projectPackage;
    }
}
