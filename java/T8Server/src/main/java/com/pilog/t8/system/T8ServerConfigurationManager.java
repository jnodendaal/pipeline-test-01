package com.pilog.t8.system;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8UserManagerConfiguration;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.event.T8DefinitionCacheReloadedEvent;
import com.pilog.t8.definition.event.T8DefinitionManagerEventAdapter;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.ui.T8LookAndFeelManager;
import com.pilog.t8.definition.language.T8UITranslationDefinition;
import com.pilog.t8.definition.system.property.T8SystemPropertyDefinition;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalSettingsDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import com.pilog.t8.org.operational.T8OrganizationOperationalHoursCalculator;
import com.pilog.t8.security.T8Context;

/**
 * The configuration manager MUST be the first service loaded by the server.  It
 * uses a definition manager listener to update its state every time definitions
 * are reloaded.
 *
 * @author Bouwer du Preez
 */
public class T8ServerConfigurationManager implements T8ConfigurationManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerConfigurationManager.class);

    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8DefinitionManager definitionManager;
    private final Map<String, Map<String, String>> translationMaps;
    private final Map<String, Set<String>> untranslatedSets;
    private final Map<String, Set<String>> requestedPhraseSets;
    private final Map<String, String> apiClassMap;

    private Thread requestorThread; // This variable holds a reference to the current requestor thread (used to store new UI translation requests).
    private static final String UNTRANSLATED_DEFINITION_PREFIX = "@LTUI_UNTRANSLATED_";

    public T8ServerConfigurationManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.internalContext = serverContext.getSecurityManager().createServerModuleContext(T8ServerConfigurationManager.this);
        this.definitionManager = serverContext.getDefinitionManager();
        this.translationMaps = new HashMap<>();
        this.untranslatedSets = new HashMap<>();
        this.requestedPhraseSets = new HashMap<>();
        this.apiClassMap = new HashMap<>();

        // Add a listener to the definition manager so that configuration data can be refreshed when definitions are reloaded.
        serverContext.getDefinitionManager().addDefinitionManagerListener(new DefinitionRefreshListener());
    }

    @Override
    public void init()
    {
        // Add API classes identified by class scanner.
        apiClassMap.putAll(T8ApiClassScanner.getAPIClassMap(serverContext));
    }

    @Override
    public void start() throws Exception
    {
    }

    @Override
    public void destroy()
    {
        translationMaps.clear();
    }

    @Override
    public T8UserManagerConfiguration getUserManagerConfiguration()
    {
        T8SystemDefinition systemDefinition;
        T8UserManagerConfiguration config;

        systemDefinition = serverContext.getDefinitionManager().getSystemDefinition(internalContext);
        config = new T8UserManagerConfiguration();
        config.setUserDataFileContextId(systemDefinition.getUserDataFileContextId());
        return config;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <P> P getProperty(T8Context context, String propertyId)
    {
        try
        {
            T8SystemPropertyDefinition propertyDefinition;

            propertyDefinition = definitionManager.getRawDefinition(null, null, propertyId);
            if (propertyDefinition != null)
            {
                return (P)propertyDefinition.getPropertyValue();
            }
            else throw new RuntimeException("System Property '" + propertyId + "' not found.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while retrieving system property: " + propertyId, e);
            throw new RuntimeException("Exception while retrieving system property: " + propertyId, e);
        }
    }

    @Override
    public <P> P getProperty(T8Context context, String propertyId, P defaultValue)
    {
        try
        {
            return getProperty(context, propertyId);
        }
        catch (Exception ex)
        {
            LOGGER.log(T8Logger.Level.WARNING, "System property '" + propertyId + "' not found. Returning default value: " + defaultValue);
            return defaultValue;
        }
    }

    /**
     * This method returns the translated phrase using the String supplied and
     * the language identifier in the Session Context supplier, as inputs.  If
     * the phrase translation in the required language does not exist, the
     * input String is simply returned as-is but a language translation request
     * will be generated automatically.
     *
     * @param context The session context for which to fetch the
     * translated phrase.
     * @param inputString The input phrase to translate.
     * @return The translated phrase or the original input phrase if no
     * applicable translation was found.
     */
    @Override
    public String getUITranslation(T8Context context, String inputString)
    {
        return getUITranslation(context, context.getSessionContext().getLanguageIdentifier(), inputString);
    }

    /**
     * This method returns the translated phrase using the String supplied and
     * the language identifier specified, as inputs.  If the phrase translation
     * in the required language does not exist, the input String is simply
     * returned as-is but a language translation request will be generated
     * automatically.
     *
     * @param context The session context for which to fetch the
     * translated phrase.
     * @param languageIdentifier The system language identifier specifying the
     * language in which the translation is requested.
     * @param inputString The input phrase to translate.
     * @return The translated phrase or the original input phrase if no
     * applicable translation was found.
     */
    @Override
    public String getUITranslation(T8Context context, String languageIdentifier, String inputString)
    {
        // If the input string is a local or global identifier, return it without event requesting translation.
        if (inputString == null) return null;
        else if ((T8IdentifierUtilities.isPublicId(inputString)) || ((T8IdentifierUtilities.isLocalId(inputString)))) return inputString;
        else
        {
            Map<String, String> languageTranslationMap;
            String translatedString = null;

            // Get the requested language and the appropriate translation map for it.
            languageTranslationMap = translationMaps.get(languageIdentifier);

            // Now get the translated String from the translation map and if it is not available, request the translation.
            if (languageTranslationMap != null) translatedString = languageTranslationMap.get(inputString);
            if (translatedString != null)
            {
                return translatedString;
            }
            else
            {
                // Request the translation if it has not already been done.
                if (!hasUITranslationBeenRequested(languageIdentifier, inputString))
                {
                    addUITranslationRequest(languageIdentifier, inputString);
                }

                // Return the input String untranslated.
                return inputString;
            }
        }
    }

    /**
     * Returns the translation map for the specified language.  The translation
     * map contains all the translated phrases for the specified language.
     *
     * @param sessionContext The session context from which the map is
     * requested.
     * @param languageId The language for which a corresponding map will
     * be returned.
     * @return The translation map for the specified language.
     */
    @Override
    public Map<String, String> getUITranslationMap(T8Context context, String languageId)
    {
        return translationMaps.get(languageId);
    }

    /**
     * This method adds the specified input String to the new translation
     * request queue for the specific language.  IMPORTANT:  This method does
     * not check if the requested translation already exists or if it has
     * already been requested.
     *
     * @param languageIdentifier The language for which the requested will be
     * generated.
     * @param inputString The phrase to be translated.
     */
    private void addUITranslationRequest(String languageIdentifier, String inputString)
    {
        if (languageIdentifier != null)
        {
            synchronized (untranslatedSets)
            {
                Set<String> untranslatedSet;

                // If the required list is not available, create it.
                if (!untranslatedSets.containsKey(languageIdentifier))
                {
                    untranslatedSets.put(languageIdentifier, new HashSet<>());
                }

                // Add the untranslated String to the set of requests.
                untranslatedSet = untranslatedSets.get(languageIdentifier);
                untranslatedSet.add(inputString);

                // If not requestor thread is running, start a new one.
                if (requestorThread == null)
                {
                    requestorThread = new RequestorThread();
                    requestorThread.start();
                }
            }
        }
        else throw new IllegalArgumentException("Cannot request UI translation for null language identifier.");
    }

    /**
     * Checks the requested phrase collection to find out if a specific
     * translation request has already been sent to the server.
     *
     * @param languageIdentifier The identifier of the translation language.
     * @param inputString The phrase to be translated.
     * @return A boolean value indicating whether or not the specified phrase
     * has already been requested from the server in the specified translation
     * language.
     */
    private boolean hasUITranslationBeenRequested(String languageIdentifier, String inputString)
    {
        Set<String> requestedTranslationSet;

        requestedTranslationSet = requestedPhraseSets.get(languageIdentifier);
        if (requestedTranslationSet != null)
        {
            return requestedTranslationSet.contains(inputString);
        }
        else return false;
    }

    /**
     * Checks the translated phrase collection to find out if a specific
     * translation exists.
     *
     * @param languageIdentifier The identifier of the translation language.
     * @param inputString The phrase to be translated.
     * @return A boolean value indicating whether or not the specified phrase
     * has a corresponding translated value in the specified language.
     */
    private boolean translationExists(String languageIdentifier, String inputString)
    {
        Map<String, String> languageTranslationMap;

        // Get the requested language translation map.
        languageTranslationMap = translationMaps.get(languageIdentifier);

        // Now check if the input String exists in the translation map.
        return ((languageTranslationMap != null) && (languageTranslationMap.containsKey(inputString)));
    }

    /**
     * Adds all of the supplied phrases to the requested phrase collection which
     * is used to check phrases already requested from the server so that no
     * duplicate requests are processed.
     *
     * @param languageIdentifier The identifier of the translation language.
     * @param phrases The phrase to be translated.
     */
    private void addRequestedPhrases(String languageIdentifier, Set<String> phrases)
    {
        if(languageIdentifier != null)
        {
            synchronized (requestedPhraseSets)
            {
                Set<String> untranslatedSet;

                // If the required list is not available, create it.
                if (!requestedPhraseSets.containsKey(languageIdentifier))
                {
                    requestedPhraseSets.put(languageIdentifier, new HashSet<>());
                }

                // Add the untranslated String to the set of requests.
                untranslatedSet = requestedPhraseSets.get(languageIdentifier);
                untranslatedSet.addAll(phrases);
            }
        }
    }

    /**
     * This method loads all of the defined translation maps from the meta data
     * into the local cache.
     */
    private void loadTranslations()
    {
        try
        {
            List<T8Definition> translationDefinitions;

            LOGGER.log("Loading UI translation cache...");

            translationMaps.clear();
            translationDefinitions = definitionManager.getRawGroupDefinitions(internalContext, null, T8UITranslationDefinition.GROUP_IDENTIFIER);
            for (T8Definition translationDefinition : translationDefinitions)
            {
                T8UITranslationDefinition uiTranslationDefinition;
                Map<String, String> translationMap;

                uiTranslationDefinition = (T8UITranslationDefinition)translationDefinition;
                translationMap = translationMaps.get(uiTranslationDefinition.getLanguageIdentifier());
                if (translationMap == null)
                {
                    translationMap = new HashMap<>();
                    translationMaps.put(uiTranslationDefinition.getLanguageIdentifier(), translationMap);
                }

                // Add the translation map in the definition to the cached translation map.
                if (uiTranslationDefinition.getTranslationMap() != null) translationMap.putAll(uiTranslationDefinition.getTranslationMap());
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while loading UI translation definitions.", e);
        }
    }

    /**
     * Adds all of the phrases in the input phrase set as new translation
     * requests in the specified language.  If a phrase in the input set has
     * an existing translation in the required language or if a translation
     * request has already been created, it will be disregarded.
     *
     * @param context The session context from which the request has been
     * initiated.
     * @param languageIdentifier The language to be translated to.
     * @param phraseSet The input phrases to be translated.
     */
    @Override
    public void requestUITranslations(T8Context context, String languageIdentifier, Set<String> phraseSet)
    {
        if (languageIdentifier != null)
        {
            synchronized (untranslatedSets)
            {
                Set<String> untranslatedSet;

                // If the required list is not available, create it.
                if (!untranslatedSets.containsKey(languageIdentifier))
                {
                    untranslatedSets.put(languageIdentifier, new HashSet<>());
                }

                // Add the untranslated Strings to the set of requests.
                untranslatedSet = untranslatedSets.get(languageIdentifier);
                for (String phrase : phraseSet)
                {
                    // If the translation does not already exist and has not already been requested, request it.
                    if ((!translationExists(languageIdentifier, phrase)) && (!hasUITranslationBeenRequested(languageIdentifier, phrase)))
                    {
                        untranslatedSet.add(phrase);
                    }
                }

                // If not requestor thread is running, start a new one (only start it if there are actually some requests to process).
                if ((requestorThread == null) && (untranslatedSet.size() > 0))
                {
                    requestorThread = new RequestorThread();
                    requestorThread.start();
                }
            }
        }
        else throw new IllegalArgumentException("Cannot request UI translation for null language identifier.");
    }

    @Override
    public Map<String, String> getAPIClassMap(T8Context context)
    {
        return new HashMap<String, String>(apiClassMap);
    }

    @Override
    public <A extends T8Api> A getAPI(T8Context context, String apiIdentifier)
    {
        String apiClassName;

        apiClassName = apiClassMap.get(apiIdentifier);
        if (!Strings.isNullOrEmpty(apiClassName))
        {
            return T8Reflections.getFastFailInstance(apiClassName, new Class<?>[]{T8Context.class}, context);
        }
        else throw new RuntimeException("API not found: " + apiIdentifier);
    }

    @Override
    public T8LookAndFeelManager getLookAndFeelManager(T8Context context)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <V> void setUserProperty(T8Context context, String identifier, V value)
    {
        throw new UnsupportedOperationException("Setting of user properties on server side is invalid.");
    }

    @Override
    public <V> V getUserProperty(T8Context context, String identifier, V defaultValue)
    {
        throw new UnsupportedOperationException("Loading of user properties on server side is invalid.");
    }

    /**
     * This listener is used to refresh the local translation cache whenever the
     * server-side meta data is reloaded by the definition manager.
     */
    private class DefinitionRefreshListener extends T8DefinitionManagerEventAdapter
    {
        @Override
        public void definitionCacheReloaded(T8DefinitionCacheReloadedEvent event)
        {
            loadTranslations();
        }
    }

    @Override
    public T8OperationalHoursCalculator getOperationalHoursCalculator(T8Context context) throws Exception
    {
        T8OrganizationSetupDefinition setupDefinition;
        String operationalSettingsId;

        setupDefinition = T8OrganizationSetupDefinition.getOrganizationSetupDefinition(context);
        operationalSettingsId = setupDefinition.getOrganizationOperationalSettingsIdentifier();
        if (operationalSettingsId != null)
        {
            T8OrganizationOperationalSettingsDefinition settingsDefinition;

            settingsDefinition = definitionManager.getInitializedDefinition(context, null, operationalSettingsId, null);
            if (settingsDefinition != null)
            {
                return T8OrganizationOperationalHoursCalculator.getInstance(settingsDefinition);
            }
            else throw new RuntimeException("Operational settings definition not found: " + operationalSettingsId);
        }
        else return null;
    }

    /**
     * This thread is used to save new translation requests as meta data
     * definitions to the server.  The thread has a built-in delay before
     * actually performing its function on order to allow for rapid sequential
     * requests to be batched before saving them to meta.
     */
    private class RequestorThread extends Thread
    {
        private RequestorThread()
        {
            this.setDaemon(true);
        }

        @Override
        public void run()
        {
            try
            {
                // Wait for 60 seconds.  This allows time for a batch of UI requests to be accumulated before storing the requests as definitions.
                Thread.sleep(60000);
            }
            catch (InterruptedException e)
            {
                LOGGER.log("Exception while waiting for new UI Requests to accumulate.", e);
            }

            // Send the requests to the server.
            synchronized (untranslatedSets)
            {
                for (String languageIdentifier : untranslatedSets.keySet())
                {
                    Set<String> untranslatedSet;
                    String definitionId;

                    // Get the untranslated set and generate a definition identifier for it.
                    untranslatedSet = untranslatedSets.get(languageIdentifier);
                    definitionId = UNTRANSLATED_DEFINITION_PREFIX + languageIdentifier.substring(languageIdentifier.indexOf('_') + 1);

                    try
                    {
                        T8UITranslationDefinition translationDefinition;
                        Map<String, String> newTranslationMap;

                        // Load the translation definition or if it does not exists, create a new one.
                        translationDefinition = (T8UITranslationDefinition)serverContext.getDefinitionManager().getRawDefinition(null, null, definitionId);
                        if (translationDefinition == null)
                        {
                            translationDefinition = new T8UITranslationDefinition(definitionId);
                            translationDefinition.setLanguageIdentifier(languageIdentifier);
                        }

                        // Update the definition with the untranslated Set.
                        newTranslationMap = translationDefinition.getTranslationMap();
                        if (newTranslationMap == null) newTranslationMap = new HashMap<>();

                        // Add all the newly requested phrases to the map.
                        for (String phrase : untranslatedSet)
                        {
                            if (!Strings.isNullOrEmpty(phrase))
                            {
                                newTranslationMap.put(phrase, null);
                            }
                        }

                        // Add the new map to the definition.
                        translationDefinition.setTranslationMap(newTranslationMap);

                        // Save the definition (with the updated changes).
                        LOGGER.log("Saving newly requested UI Translations to definition: " + definitionId);
                        serverContext.getDefinitionManager().saveDefinition(internalContext, translationDefinition, null);
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while storing newly requested UI Translations for language: " + languageIdentifier, e);
                    }

                    // Because we don't want the phrases to be re-added to the untranslated set, we store them for checking in the requested phrase collection.
                    addRequestedPhrases(languageIdentifier, untranslatedSet);
                }

                // Remove the untranslated phrases so they are not processed again later.
                untranslatedSets.clear();
            }

            // Finally clear the requestor thread reference, so that subsequent requests will generate a new thread.
            requestorThread = null;
            LOGGER.log("UI Translations requestor thread ends.");
        }
    }
}
