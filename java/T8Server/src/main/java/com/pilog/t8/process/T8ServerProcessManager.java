package com.pilog.t8.process;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.exception.T8UserException;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.process.T8Process.T8ProcessHistoryEvent;
import com.pilog.t8.process.T8Process.T8ProcessStatus;
import com.pilog.t8.definition.notification.T8ProcessNotificationDefinition;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.process.event.T8ProcessCompletedEvent;
import com.pilog.t8.process.event.T8ProcessFailedEvent;
import com.pilog.t8.process.event.T8ProcessManagerListener;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.thread.T8ContextRunnable;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public class T8ServerProcessManager implements T8ProcessManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerProcessManager.class);

    private final Map<String, T8ProcessDetails> executingProcessDetails; // Stores details of all processes in the registry (not yet finalized).
    private final Map<String, T8Process> executingProcesses; // Stores currently executing processes.
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final EventListenerList listeners;
    private final T8ProcessManagerDataHandler dataHandler;
    private ScheduledExecutorService executor; // Timer used to update process details.
    private ProcessMaintenanceThread maintenanceThread;
    private boolean managerEnabled;

    private final long PROCESS_MAINTENANCE_INTERVAL = 1000; // The interval in milliseconds at which the process maintenance task is run.

    public T8ServerProcessManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.internalContext = serverContext.getSecurityManager().createServerModuleContext(T8ServerProcessManager.this);
        this.listeners = new EventListenerList();
        this.executingProcessDetails = Collections.synchronizedMap(new HashMap<String, T8ProcessDetails>());
        this.executingProcesses = Collections.synchronizedMap(new HashMap<String, T8Process>());
        this.dataHandler = new T8ProcessManagerDataHandler(internalContext);
    }

    @Override
    public void init()
    {
        try
        {
            T8SystemDefinition systemDefinition;

            // Get the system definition to determine configuration.
            systemDefinition = serverContext.getDefinitionManager().getSystemDefinition(internalContext);
            managerEnabled = systemDefinition != null && systemDefinition.isProcessManagerEnabled();

            // Start the maintenance timer.
            executor = Executors.newSingleThreadScheduledExecutor(new NameableThreadFactory("T8ServerProcessManager-Maintenance"));
            maintenanceThread = new ProcessMaintenanceThread(internalContext, dataHandler, executingProcesses, executingProcessDetails, managerEnabled);
            executor.scheduleWithFixedDelay(maintenanceThread, PROCESS_MAINTENANCE_INTERVAL, PROCESS_MAINTENANCE_INTERVAL, TimeUnit.MILLISECONDS);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing process manager.", e);
        }
    }

    @Override
    public void start() throws Exception
    {
    }

    @Override
    public void destroy()
    {
        ArrayList<T8Process> processList;

        // Terminate the timer thread and all scheduled tasks.
        try
        {
            maintenanceThread.setEnabled(false);
            executor.shutdown();
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) throw new Exception("Process maintenance thread executor could not by terminated.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while awaiting termination of process maintenance thread.", e);
        }

        // Stop all processes.
        processList = new ArrayList<T8Process>(executingProcesses.values());
        for (T8Process process : processList)
        {
            try
            {
                // Stop the process.
                process.stop();
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while stopping process '" + process.getDefinition() + "'.", e);
            }
        }
    }

    @Override
    public void addEventListener(T8ProcessManagerListener listener)
    {
        listeners.add(T8ProcessManagerListener.class, listener);
    }

    @Override
    public void removeEventListener(T8ProcessManagerListener listener)
    {
        listeners.remove(T8ProcessManagerListener.class, listener);
    }

    private void fireProcessCompletedEvent(T8ProcessDetails processDetails, Map<String, Object> outputParameters)
    {
        T8ProcessCompletedEvent event;

        event = new T8ProcessCompletedEvent(this, processDetails, outputParameters);
        for (T8ProcessManagerListener listener : listeners.getListeners(T8ProcessManagerListener.class))
        {
            try
            {
                listener.processCompleted(event);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception during propagation of process completion event: " + event, e);
            }
        }
    }

    private void fireProcessFailedEvent(T8ProcessDetails processDetails)
    {
        T8ProcessFailedEvent event;

        event = new T8ProcessFailedEvent(this, processDetails);
        for (T8ProcessManagerListener listener : listeners.getListeners(T8ProcessManagerListener.class))
        {
            try
            {
                listener.processFailed(event);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception during propagation of process completion event: " + event, e);
            }
        }
    }

    @Override
    public T8ProcessDetails getProcessDetails(T8Context context, String processIid) throws Exception
    {
        T8ProcessDetails details;

        // First try to find the state among the actively executing processes.
        details = executingProcessDetails.get(processIid);
        if (details != null)
        {
            return details;
        }
        else // No actively executing, so use the state to construct the details.
        {
            List<T8ProcessState> states;

            states = dataHandler.retrieveStates(ArrayLists.newArrayList(processIid));
            return states.size() > 0 ? states.get(0).getProcessDetails(serverContext) : null;
        }
    }

    @Override
    public T8ProcessSummary getProcessSummary(T8Context context) throws Exception
    {
        T8SessionContext sessionContext;
        T8ProcessFilter filter;

        sessionContext = context.getSessionContext();

        filter = new T8ProcessFilter();
        filter.addUserId(sessionContext.getUserIdentifier());
        filter.addProfileId(sessionContext.getUserProfileIdentifier());
        return dataHandler.retrieveProcessSummary(context, filter);
    }

    @Override
    public int countProcesses(T8Context context, T8ProcessFilter processFilter, String searchString) throws Exception
    {
        T8SessionContext sessionContext;
        T8ProcessFilter checkedFilter;

        // Create a new filter and make sure to add the user id and profile id.
        sessionContext = context.getSessionContext();
        checkedFilter = processFilter != null ? processFilter.copy() : new T8ProcessFilter();
        checkedFilter.addUserId(sessionContext.getUserIdentifier());
        checkedFilter.addProfileId(sessionContext.getUserProfileIdentifier());

        // Use the filter to count the states matching input criteria.
        return dataHandler.countStates(context, checkedFilter, searchString);
    }

    @Override
    public List<T8ProcessDetails> searchProcesses(T8Context context, T8ProcessFilter processFilter, String searchString, int pageOffset, int pageSize) throws Exception
    {
        List<T8ProcessDetails> detailsList;
        List<T8ProcessState> stateList;
        T8ProcessFilter checkedFilter;
        T8SessionContext sessionContext;

        // Create a new filter and make sure to add the user id and profile id.
        sessionContext = context.getSessionContext();
        checkedFilter = processFilter != null ? processFilter.copy() : new T8ProcessFilter();
        checkedFilter.addUserId(sessionContext.getUserIdentifier());
        checkedFilter.addProfileId(sessionContext.getUserProfileIdentifier());

        // Use the filter to retrieve the states matching input criteria.
        detailsList = new ArrayList<>();
        stateList = dataHandler.searchStates(context, checkedFilter, searchString, pageOffset, pageSize);
        for (T8ProcessState state : stateList)
        {
            T8ProcessDetails details;

            // First try to find the state among the actively executing processes.
            details = executingProcessDetails.get(state.getIid());
            if (details != null)
            {
                detailsList.add(details);
            }
            else // No actively executing, so use the state to construct the details.
            {
                detailsList.add(state.getProcessDetails(serverContext));
            }
        }

        // Return the final list of details.
        return detailsList;
    }

    @Override
    public T8ProcessDetails startProcess(T8Context context, String processId, Map<String, Object> inputParameters) throws Exception
    {
        T8ProcessDefinition processDefinition;

        processDefinition = (T8ProcessDefinition)serverContext.getDefinitionManager().getRawDefinition(null, null, processId);
        if (processDefinition != null)
        {
            return startProcess(context, processDefinition, inputParameters);
        }
        else throw new Exception("Process Definition not found: " + processId);
    }

    @Override
    public T8ProcessDetails startProcess(final T8Context context, T8ProcessDefinition processDefinition, Map<String, Object> inputParameters) throws Exception
    {
        final Map<String, Object> processInputParameters;
        final T8ProcessDetails processDetails;
        final T8Process process;
        T8ProcessState processState;
        String processIid;

        // Create a new process instance.
        processIid = T8IdentifierUtilities.createNewGUID();
        process = processDefinition.getNewProcessInstance(context, processIid);
        processDetails = process.getDetails();
        processInputParameters = T8IdentifierUtilities.stripNamespace(processDefinition.getNamespace(), inputParameters, true);
        executingProcessDetails.put(processIid, processDetails);
        executingProcesses.put(process.getIid(), process);

        // Execute the process in a separate thread.
        new Thread(new T8ContextRunnable(context, processDefinition.getIdentifier()+"-"+processIid)
        {
            @Override
            public void exec()
            {
                try
                {
                    Map<String, Object> outputParameters;
                    T8ProcessDetails completionDetails;

                    // Start the process.
                    outputParameters = process.execute(processInputParameters);

                    // Get the updated process details after process completion.
                    completionDetails = process.getDetails();

                    // This will tell the server to remove the process from the idle list after a set amount of time.
                    completionDetails.setFinalizeOnCompletion(process.getDefinition().isFinalizeOnCompletionEnabled());

                    // Remove the process from the active list.
                    executingProcesses.remove(completionDetails.getProcessIid());

                    // Remove the process details from the active collection and move it to the idle collection.
                    executingProcessDetails.remove(completionDetails.getProcessIid());

                    // Save the process state.
                    dataHandler.updateState(process.getState());

                    // Log the process event.
                    dataHandler.logProcessEvent(context, T8ProcessHistoryEvent.COMPLETED, process.getState());

                    // Fire the process event to all listeners.
                    fireProcessCompletedEvent(completionDetails, T8IdentifierUtilities.prependNamespace(processDefinition.getNamespace(), outputParameters));

                    // If the process is configured to be finalized on completion, do it now.
                    if (processDefinition.isFinalizeOnCompletionEnabled())
                    {
                        finalizeProcess(context, process.getIid());
                    }
                }
                catch (Exception ex)
                {
                    T8ProcessDetails failureDetails;

                    // Get the updated process details after process failure.
                    try
                    {
                        failureDetails = process.getDetails();
                    }
                    catch (Exception exInner)
                    {
                        LOGGER.log("Exception while fetching process details after failure: " + process.getIid(), exInner);
                        failureDetails = processDetails;
                    }

                    // Check if the stack contains an exception that is user friendly, and return that message.
                    failureDetails.setFailureMessage(findUserExceptionMessage(ex));

                    // Remove the process from the active list.
                    executingProcesses.remove(failureDetails.getProcessIid());

                    // Remove the process details from the active collection and move it to the idle collection.
                    executingProcessDetails.remove(failureDetails.getProcessIid());

                    try
                    {
                        T8ProcessState failureState;

                        // Log the exception.
                        LOGGER.log("Exception while starting new process: " + process.getDefinition(), ex);

                        // Save the process failure state.
                        failureState = process.getState();
                        failureState.setStatus(T8ProcessStatus.FAILED);
                        dataHandler.updateState(failureState);

                        // If anything, went wrong during the process execution, log the event.
                        dataHandler.logProcessEvent(context, T8ProcessHistoryEvent.FAILED, failureState);
                    }
                    catch (Exception exInner)
                    {
                        LOGGER.log("Exception while trying to log process failure: " + process.getDefinition(), exInner);
                    }

                    // Fire the process event to all listeners.
                    fireProcessFailedEvent(failureDetails);
                }
            }
        }).start();

        // Log the event.
        processState = process.getState();
        processState.setParameters(inputParameters);
        dataHandler.insertState(processState);
        dataHandler.logProcessEvent(context, T8ProcessHistoryEvent.STARTED, processState);

        // Send a notification if required.
        if (processDefinition.isSendNotification())
        {
            T8NotificationManager notificationManager;

            notificationManager = serverContext.getNotificationManager();
            notificationManager.sendNotification(context, T8ProcessNotificationDefinition.IDENTIFIER, HashMaps.createSingular(T8ProcessNotificationDefinition.PARAMETER_PROCESS_IID, processIid));
        }

        // Return the process details.
        return processDetails;
    }

    @Override
    public T8ProcessDetails stopProcess(T8Context context, String processIid) throws Exception
    {
        T8Process process;

        process = executingProcesses.get(processIid);
        if (process != null)
        {
            T8ProcessState processState;

            // Stop the process and get the details.
            process.stop();
            processState = process.getState();

            // Save the process state.
            dataHandler.updateState(process.getState());

            // Log the process event to history.
            dataHandler.logProcessEvent(context, T8ProcessHistoryEvent.STOPPED, processState);
            return processState.getProcessDetails(serverContext);
        }
        else throw new Exception("Process not found: " + processIid);
    }

    @Override
    public T8ProcessDetails cancelProcess(T8Context context, String processIid) throws Exception
    {
        T8Process process;

        process = executingProcesses.get(processIid);
        if (process != null)
        {
            T8ProcessState processState;

            // Cancel the process and get the details.
            process.cancel();
            processState = process.getState();

            // Save the process state.
            dataHandler.updateState(process.getState());

            // Log the process event to history.
            dataHandler.logProcessEvent(context, T8ProcessHistoryEvent.CANCELLED, processState);
            return processState.getProcessDetails(serverContext);
        }
        else throw new Exception("Process not found: " + processIid);
    }

    @Override
    public T8ProcessDetails pauseProcess(T8Context context, String processIid) throws Exception
    {
        T8Process process;

        process = executingProcesses.get(processIid);
        if (process != null)
        {
            T8ProcessState processState;

            // Pause the process and get the details.
            process.pause();
            processState = process.getState();

            // Save the process state.
            dataHandler.updateState(process.getState());

            // Log the process event to history.
            dataHandler.logProcessEvent(context, T8ProcessHistoryEvent.PAUSED, processState);
            return processState.getProcessDetails(serverContext);
        }
        else throw new Exception("Process not found: " + processIid);
    }

    @Override
    public T8ProcessDetails resumeProcess(T8Context context, String processIid) throws Exception
    {
        T8Process process;

        process = executingProcesses.get(processIid);
        if (process != null)
        {
            T8ProcessState processState;

            // Resume the process and get the details.
            process.resume();
            processState = process.getState();

            // Save the process state.
            dataHandler.updateState(process.getState());

            // Log the process event to history.
            dataHandler.logProcessEvent(context, T8ProcessHistoryEvent.RESUMED, processState);
            return processState.getProcessDetails(serverContext);
        }
        else throw new Exception("Process not found: " + processIid);
    }

    @Override
    public T8ProcessDetails finalizeProcess(T8Context context, String processIid) throws Exception
    {
        T8Process process;

        process = executingProcesses.get(processIid);
        if (process != null) // Process is still running.
        {
            T8ProcessDetails processDetails;
            int waitCount;

            // Wait for the process execution to end.
            waitCount = 0;
            process.stop();
            while (process.getStatus() == T8ProcessStatus.IN_PROGRESS)
            {
                // Wait one second for the process to stop.
                Thread.sleep(1000);
                waitCount++;

                // If the wait count exceeds the expected value, log it.
                if (waitCount > 10)
                {
                    LOGGER.log("Execution thread in process '" + process.getDefinition() + "', Instance '" + processIid + "' still active " + waitCount + " seconds following instruction to stop.  Status: " + process.getStatus());
                }

                // If the wait count exceeds a preset limit, throw an exception.
                throw new Exception("Process failed to stop during finalization: '" + process.getDefinition() + "', Instance '" + processIid + "'");
            }

            // Get the final process details.
            processDetails = process.getDetails();

            // Log the process event to history.
            dataHandler.logProcessEvent(context, T8ProcessHistoryEvent.FINALIZED, process.getState());

            // Delete the process state.
            dataHandler.deleteState(process.getIid());

            // Remove the process details.
            executingProcessDetails.remove(processDetails.getProcessIid());

            // Return the process details.
            return processDetails;
        }
        else
        {
            T8ProcessState processState;

            // Get the final process state.
            processState = dataHandler.retrieveState(processIid);
            if (processState != null)
            {
                // Log the process event to history.
                dataHandler.logProcessEvent(context, T8ProcessHistoryEvent.FINALIZED, processState);

                // Delete the process state.
                dataHandler.deleteState(processState.getIid());

                // Return the process details.
                return processState.getProcessDetails(serverContext);
            }
            else throw new Exception("Process not found: " + processIid);
        }
    }

    private String findUserExceptionMessage(Throwable ex)
    {
        String exceptionMessage = null;
        //First traverse the stack, so that we can start checking from the deepest exception
        if(ex.getCause() != null) exceptionMessage = findUserExceptionMessage(ex.getCause());

        if(exceptionMessage == null && ex instanceof T8UserException) return ((T8UserException) ex).getUserMessage();

        return exceptionMessage;
    }

    /**
     * This class is a task that is scheduled using a timer object and is used
     * to run regular maintenance checks on the currently executing processes
     * such as updating the list of active process details.
     */
    private static class ProcessMaintenanceThread extends T8ContextRunnable
    {
        private final Map<String, T8Process> executingProcesses;
        private final Map<String, T8ProcessDetails> executingProcessDetails;
        private final T8ProcessManagerDataHandler dataHandler;
        private boolean enabled;

        private ProcessMaintenanceThread(T8Context context, T8ProcessManagerDataHandler dataHandler, Map<String, T8Process> executingProcesses, Map<String, T8ProcessDetails> executingProcessDetails, boolean enabled)
        {
            super(context, ProcessMaintenanceThread.class.getName());
            this.dataHandler = dataHandler;
            this.executingProcesses = executingProcesses;
            this.executingProcessDetails = executingProcessDetails;
            this.enabled = enabled;
        }

        public void setEnabled(boolean enabled)
        {
            this.enabled = enabled;
        }

        @Override
        public void exec()
        {
            if (enabled)
            {
                try
                {
                    // Synchronize on collection to prevent concurrent modification.
                    synchronized (executingProcesses)
                    {
                        // Iterate over all executing processes and update their locally cached, as well as persisted states.
                        for (T8Process process : executingProcesses.values())
                        {
                            try
                            {
                                T8ProcessDetails newProcessDetails;

                                // Get the latest process details and update the active details list.
                                newProcessDetails = process.getDetails();
                                executingProcessDetails.put(process.getIid(), newProcessDetails);

                                // Persist the latest state of the process.
                                dataHandler.updateState(process.getState());
                            }
                            catch (Exception e)
                            {
                                LOGGER.log("Exception while checking executing process details: " + process.getIid(), e);
                            }
                        }
                    }
                }
                catch (Throwable e)
                {
                    LOGGER.log("Throwable during process check.", e);
                }
            }
        }
    }
}
