package com.pilog.t8.api;

import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.data.object.filter.T8ObjectFilter;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.Map;
import java.util.List;
import com.pilog.t8.utilities.collections.HashMaps;

import static com.pilog.t8.definition.api.T8DataObjectApiResource.*;

/**
 *
 * @author Pieter Strydom
 */
public class T8DataObjectApiOperations
{
    public static class ApiRetrieve extends T8DefaultServerOperation
    {
        public ApiRetrieve(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, T8DataObject> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataObjectApi objApi;
            T8DataObject dataObject;
            String dataObjectId;
            String dataObjectIid;
            Boolean includeState;

            // Get the nessacary values from the paramater map.
            dataObjectId = (String)operationParameters.get(PARAMETER_OBJECT_ID);
            dataObjectIid = (String)operationParameters.get(PARAMETER_OBJECT_IID);
            includeState = (Boolean)operationParameters.get(PARAMETER_INCLUDE_STATE);

            // Initialize the api and retrieve the data object.
            objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
            dataObject = objApi.retrieve(dataObjectId, dataObjectIid, includeState != null && includeState);

            return HashMaps.createSingular(PARAMETER_OBJECT, dataObject);
        }
    }

    public static class ApiSearch extends T8DefaultServerOperation
    {
        public ApiSearch(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataObject>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8DataObject> dataObjects;
            T8DataObjectApi objApi;
            T8ObjectFilter filter;
            String dataObjectId;
            String searchExpression;
            Integer pageOffset;
            Integer pageSize;
            Boolean includeState;

            // Get the nessacary values from the paramater map.
            dataObjectId = (String)operationParameters.get(PARAMETER_OBJECT_ID);
            filter = (T8ObjectFilter)operationParameters.get(PARAMETER_OBJECT_FILTER);
            searchExpression = (String)operationParameters.get(PARAMETER_SEARCH_EXPRESSION);
            pageOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);
            includeState = (Boolean)operationParameters.get(PARAMETER_INCLUDE_STATE);

            // Initialize the api and retrieve the data object.
            objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
            dataObjects = objApi.search(dataObjectId, filter, searchExpression, pageOffset, pageSize, includeState != null && includeState);
            return HashMaps.createSingular(PARAMETER_OBJECTS, dataObjects);
        }
    }

    public static class ApiSearchGroup extends T8DefaultServerOperation
    {
        public ApiSearchGroup(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataObject>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8DataObject> dataObjects;
            T8DataObjectApi objApi;
            T8ObjectFilter filter;
            String groupId;
            String searchExpression;
            Integer pageOffset;
            Integer pageSize;
            Boolean includeState;

            // Get the nessacary values from the paramater map.
            groupId = (String)operationParameters.get(PARAMETER_GROUP_ID);
            filter = (T8ObjectFilter)operationParameters.get(PARAMETER_OBJECT_FILTER);
            searchExpression = (String)operationParameters.get(PARAMETER_SEARCH_EXPRESSION);
            pageOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);
            includeState = (Boolean)operationParameters.get(PARAMETER_INCLUDE_STATE);

            // Initialize the api and retrieve the data object.
            objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
            dataObjects = objApi.searchGroup(groupId, filter, searchExpression, pageOffset, pageSize, includeState != null && includeState);
            return HashMaps.createSingular(PARAMETER_OBJECTS, dataObjects);
        }
    }
}
