package com.pilog.t8.api;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilterExpressionParser;
import com.pilog.t8.data.filter.expression.T8SearchExpressionParseException;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.data.object.T8DataObjectGroupHandler;
import com.pilog.t8.data.object.T8DataObjectHandler;
import com.pilog.t8.data.object.T8DataObjectState;
import com.pilog.t8.data.object.T8DataObjectStateGraph;
import com.pilog.t8.data.object.T8DataObjectStateTransition;
import com.pilog.t8.data.object.filter.T8ObjectFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.data.object.T8DataObjectGroupDefinition;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateGraphDefinition;
import com.pilog.t8.functionality.T8DataObjectStateDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectApi implements T8Api, T8PerformanceStatisticsProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataObjectApi.class);

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8DataObjectApiDataHandler dataHandler;
    private final Map<String, T8DataObjectStateGraph> stateGraphCache; // Stored using data object id as key and state graph as value.
    private final Map<String, T8DataObjectDefinition> dataObjectDefinitionCache;
    private final Map<String, T8DataObjectHandler> objectHandlers;
    private final Map<String, T8DataObjectGroupHandler> objectGroupHandlers;
    private T8PerformanceStatistics stats;

    public static final String API_IDENTIFIER = "@API_DATA_OBJECT";

    public T8DataObjectApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.stats = tx.getPerformanceStatistics();
        this.dataHandler = new T8DataObjectApiDataHandler(tx);
        this.dataObjectDefinitionCache = new HashMap<>();
        this.objectHandlers = new HashMap<>();
        this.objectGroupHandlers = new HashMap<>();
        this.stateGraphCache = new HashMap<>();
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    private T8DataObjectDefinition getDataObjectDefinition(String objectId) throws Exception
    {
        T8DataObjectDefinition objectDefinition;

        objectDefinition = dataObjectDefinitionCache.get(objectId);
        if (objectDefinition == null)
        {
            T8DefinitionManager definitionManager;

            definitionManager = serverContext.getDefinitionManager();
            objectDefinition = (T8DataObjectDefinition)definitionManager.getInitializedDefinition(context, context.getProjectId(), objectId, null);
            if (objectDefinition != null)
            {
                dataObjectDefinitionCache.put(objectId, objectDefinition);
            }

            return objectDefinition;
        }
        else return objectDefinition;
    }

    private T8DataObjectStateGraph getStateGraph(String dataObjectId) throws Exception
    {
        T8DataObjectStateGraph stateGraph;

        // Check for the state graph in the cache.
        stateGraph = stateGraphCache.get(dataObjectId);
        if (stateGraph == null)
        {
            List<T8DataObjectStateGraphDefinition> stateGraphDefinitions;
            T8DefinitionManager definitionManager;

            // Recache all state graphs.
            definitionManager = serverContext.getDefinitionManager();
            stateGraphDefinitions = (List)definitionManager.getInitializedGroupDefinitions(context, context.getProjectId(), T8DataObjectStateGraphDefinition.GROUP_IDENTIFIER, null);
            for (T8DataObjectStateGraphDefinition stateGraphDefinition : stateGraphDefinitions)
            {
                stateGraph = stateGraphDefinition.getNewStateGraphInstance(context);
                for (String objectId : stateGraph.getDataObjectIds())
                {
                    stateGraphCache.put(objectId, stateGraph);
                }
            }

            return stateGraphCache.get(dataObjectId);
        }
        else return stateGraph;
    }

    public T8DataObjectHandler getObjectHandler(String objectId) throws Exception
    {
        if (objectHandlers.containsKey(objectId))
        {
            return objectHandlers.get(objectId);
        }
        else
        {
            T8DataObjectDefinition objectDefinition;

            // Cache the new object handler.
            objectDefinition = serverContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), objectId, null);
            if (objectDefinition != null)
            {
                T8DataObjectHandler objectHandler;

                objectHandler = objectDefinition.getDataObjectHandler(tx);
                objectHandlers.put(objectId, objectHandler);
                return objectHandler;
            }
            else return null;
        }
    }

    public T8DataObjectGroupHandler getObjectGroupHandler(String groupId) throws Exception
    {
        if (objectGroupHandlers.containsKey(groupId))
        {
            return objectGroupHandlers.get(groupId);
        }
        else
        {
            T8DataObjectGroupDefinition groupDefinition;

            // Cache the new object handler.
            groupDefinition = serverContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), groupId, null);
            if (groupDefinition != null)
            {
                T8DataObjectGroupHandler groupHandler;

                groupHandler = groupDefinition.getDataObjectGroupHandler(tx);
                objectGroupHandlers.put(groupId, groupHandler);
                return groupHandler;
            }
            else return null;
        }
    }

    public List<String> parseSearchTerms(String searchExpression) throws T8SearchExpressionParseException
    {
        T8DataFilterExpressionParser parser;

        parser = new T8DataFilterExpressionParser(null, null);
        return parser.parseExpressionSearchTerms(searchExpression);
    }

    public T8DataObject retrieve(String objectId, String objectIid, boolean includeState) throws Exception
    {
        T8DataObjectHandler objectHandler;

        objectHandler = getObjectHandler(objectId);
        if (objectHandler != null)
        {
            T8DataObject object;

            object = objectHandler.retrieve(objectIid);
            if (object != null)
            {
                if (includeState)
                {
                    object.setState(retrieveState(objectIid));
                    return object;
                }
                else return object;
            }
            else return null;
        }
        else throw new Exception("Data object not found: " + objectId);
    }

    public <O extends T8DataObject> List<O> search(String objectId, T8ObjectFilter filter, String searchExpression, int pageOffset, int pageSize, boolean includeState) throws Exception
    {
        T8DataObjectHandler objectHandler;

        objectHandler = getObjectHandler(objectId);
        if (objectHandler != null)
        {
            List<O> objects;

            objects = objectHandler.search(filter, searchExpression, pageOffset, pageSize);
            if (includeState)
            {
                for (O object : objects)
                {
                    object.setState(retrieveState(object.getIid()));
                }

                return objects;
            }
            else return objects;
        }
        else throw new Exception("Data object not found: " + objectId);
    }

    public <O extends T8DataObject> List<O> searchGroup(String groupId, T8ObjectFilter filter, String searchExpression, int pageOffset, int pageSize, boolean includeState) throws Exception
    {
        T8DataObjectGroupHandler groupHandler;

        groupHandler = getObjectGroupHandler(groupId);
        if (groupHandler != null)
        {
            List<O> objects;

            objects = groupHandler.search(filter, searchExpression, pageOffset, pageSize);
            if (includeState)
            {
                for (O object : objects)
                {
                    object.setState(retrieveState(object.getIid()));
                }

                return objects;
            }
            else return objects;
        }
        else throw new Exception("Data object group not found: " + groupId);
    }

    public T8DataObjectState retrieveState(String dataObjectIid) throws Exception
    {
        T8DataObjectStateDetails stateDetails;

        stateDetails = dataHandler.retrieveObjectStateDetails(dataObjectIid);
        if (stateDetails != null)
        {
            T8DataObjectDefinition objectDefinition;
            String dataObjectId;

            dataObjectId = stateDetails.getDataObjectId();
            objectDefinition = getDataObjectDefinition(dataObjectId);
            if (objectDefinition != null)
            {
                T8DataObjectStateGraph stateGraph;
                T8DataObjectState currentState;

                // Get the applicable state graph.
                stateGraph = getStateGraph(objectDefinition.getIdentifier());

                // Find the current state using the retrieve concept id.
                currentState = stateGraph.getStateByConceptId(stateDetails.getStateConceptId());
                return (currentState != null) ? currentState.copy() : null;
            }
            else throw new RuntimeException("Data Object definition not found: " + dataObjectId);
        }
        else return null;
    }

    public void saveState(String dataObjectId, String dataObjectIid, String publicStateId) throws Exception
    {
        T8DataObjectStateGraph stateGraph;

        // Get the applicable state graph.
        stateGraph = getStateGraph(dataObjectId);
        if (stateGraph != null)
        {
            T8DataObjectStateDetails currentStateDetails;
            String stateId;

            stateId = T8IdentifierUtilities.stripNamespace(publicStateId, stateGraph.getId(), true);
            currentStateDetails = dataHandler.retrieveObjectStateDetails(dataObjectIid);
            if (currentStateDetails != null)
            {
                T8DataObjectState currentState;

                // Get the current state from the state graph.
                currentState = stateGraph.getStateByConceptId(currentStateDetails.getStateConceptId());
                if (currentState != null)
                {
                    // Only switch the state if the new state is actually different from the current state.
                    if (!Objects.equals(currentState.getId(), stateId))
                    {
                        T8DataObjectState nextState;

                        // Find the next state.
                        nextState = stateGraph.getNextState(null, currentState.getId(), stateId);
                        if (nextState != null)
                        {
                            String nextStateConceptId;

                            // Get the Concept ID of the next state.
                            nextStateConceptId = nextState.getConceptId();

                            // Set the object state.
                            dataHandler.saveObjectState(dataObjectId, dataObjectIid, nextStateConceptId);

                            // Log the state change event.
                            dataHandler.logObjectStateEvent(dataObjectIid, null, currentState.getConceptId(), nextStateConceptId);
                        }
                        else throw new RuntimeException("New state '" + stateId + "' of object '" + dataObjectId + "' could not be found in state graph: " + stateGraph);
                    }
                }
                else throw new RuntimeException("Curent state '" + currentStateDetails.getStateConceptId() + "' of object '" + dataObjectId + "' could not be found in state graph: " + stateGraph);
            }
            else // The object does not have any state yet, so we can set it now.
            {
                T8DataObjectState nextState;

                // Find the next state.
                nextState = stateGraph.getNextState(null, null, stateId);
                if (nextState != null)
                {
                    String nextStateConceptId;

                    // Get the Concept ID of the next state.
                    nextStateConceptId = nextState.getConceptId();

                    // Set the object state.
                    dataHandler.saveObjectState(dataObjectId, dataObjectIid, nextStateConceptId);

                    // Log the state change event.
                    dataHandler.logObjectStateEvent(dataObjectIid, null, null, nextStateConceptId);
                }
                else throw new Exception("New state '" + stateId + "' of object '" + dataObjectId + "' could not be found in state graph: " + stateGraph);
            }
        }
        else throw new RuntimeException("No state graph found for data object '" + dataObjectId + "'");
    }

    public void saveInitialState(String objectId, String objectIid) throws Exception
    {
        String functionalityId;

        // Get the functionality context of this operation.
        functionalityId = context.getFunctionalityId();
        if (functionalityId != null)
        {
            T8DataObjectStateGraph stateGraph;

            // Get the state graph corresponding to the data object.
            stateGraph = getStateGraph(objectId);
            if (stateGraph != null)
            {
                T8DataObjectState initialState;

                // Get the initial state of the object, according to the current functionality from which it has been created.
                initialState = stateGraph.getStateCreatedBy(functionalityId);
                if (initialState != null)
                {
                    // Save the initial state of the object.
                    saveState(objectId, objectIid, stateGraph.getId() + initialState.getId());
                }
                else throw new RuntimeException("Cannot set initial state of data object " + objectId + " as no state matches functionality of current context: " + functionalityId);
            }
            else throw new RuntimeException("Cannot set initial state of data object " + objectId + " as not state graph was found.");
        }
        else throw new RuntimeException("Cannot set initial state of object " + objectId + ":" + objectIid + " from outside of a functionality.  Current context: " + context);
    }

    public void handleFunctionalityCompletion(String dataObjectId, String dataObjectIid, String functionalityId, Map<String, Object> functionalityOutputParameters) throws Exception
    {
        T8DataObjectState currentState;
        T8ExpressionEvaluator evaluator;

        evaluator = serverContext.getExpressionEvaluator(context);
        currentState = retrieveState(dataObjectIid);
        if (currentState != null)
        {
            for (T8DataObjectStateTransition transition : currentState.getTransitions())
            {
                if (functionalityId.equals(transition.getFunctionalityId()))
                {
                    String transitionStateId;
                    String graphId;

                    graphId = currentState.getGraphId();
                    transitionStateId = transition.getStateId();
                    if (!Objects.equals(transitionStateId, currentState.getId())) // Only proceed if the transition leads to a new state.
                    {
                        String conditionExpression;

                        conditionExpression = transition.getConditionExpression();
                        if ((conditionExpression == null) || evaluator.evaluateBooleanExpression(conditionExpression, functionalityOutputParameters, null))
                        {
                            saveState(dataObjectId, dataObjectIid, graphId + transitionStateId);
                            return;
                        }
                    }
                }
            }
        }
    }
}
