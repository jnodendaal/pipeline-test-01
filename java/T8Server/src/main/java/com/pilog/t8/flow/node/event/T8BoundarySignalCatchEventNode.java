package com.pilog.t8.flow.node.event;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.T8FlowSignalStartEventNode;
import com.pilog.t8.flow.event.T8FlowSignalReceivedEvent;
import com.pilog.t8.definition.flow.node.event.T8BoundarySignalCatchEventDefinition;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.flow.state.T8FlowStateParameterMap;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8BoundarySignalCatchEventNode extends T8DefaultFlowNode implements T8FlowSignalStartEventNode
{
    private final T8BoundarySignalCatchEventDefinition definition;
    private T8FlowSignalReceivedEvent startEvent;
    private String attachedToNodeInstanceIdentifier;

    public T8BoundarySignalCatchEventNode(T8Context context, T8Flow parentFlow, T8BoundarySignalCatchEventDefinition definition, String nodeIid)
    {
        super(context, parentFlow, definition, nodeIid);
        this.definition = definition;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters) throws Exception
    {
        Map<String, Object> outputParameters;
        Map<String, Object> signalParameters;

        // Get the signal parameters from the start event.
        signalParameters = startEvent.getSignalParameters();
        synchronized (flowState)
        {
            T8FlowNodeState attachedNodeState;

            // Get the node state to which this event is attached.
            attachedNodeState = flowState.getNodeState(attachedToNodeInstanceIdentifier);

            // Cancel the attached node instance.
            statusMessage = "Waiting for cancellation of node: " + attachedNodeState;
            if (!parentFlow.cancelNode(attachedNodeState.getNodeIid()))
            {
                // This is an important check.  An instance of this node type should
                // not even be instantiated if the activity to which it is attached
                // is not active but because all nodes executeFlow on separate threads,
                // we need to ensure that the attached activity is definitely active
                // when the cancel instruction is issued or else we may end up with
                // two parallel threads of execution.
                //
                // So if the attached activity finished execution between the time
                // that this node was instantiated and the time the cancel
                // instruction was issued, we simply stop this node's execution
                // since it will no longer have a function to perform and it should
                // not spawn any outgoing sequence flows.
                stop = true;
                T8Log.log("Activity '" + attachedNodeState + "' to which boundary event '" + definition + "' is attached is not active.  Stopping execution of event path.");
            }
            statusMessage = null;

            // Get the input parameters of the attached node state and use them as the base output parameters for the signal event.
            outputParameters = new HashMap<String, Object>();
            for (T8FlowStateParameterMap attachedInputParameters : attachedNodeState.getInputParameters().values())
            {
                if (attachedInputParameters != null) outputParameters.putAll(attachedInputParameters);
            }

            // Map the signal parameters to the flow parameters.
            outputParameters.putAll(T8IdentifierUtilities.mapParameters(signalParameters, definition.getParameterMapping()));

            // Map the signal parameters to the flow parameters.
            return outputParameters;
        }
    }

    protected synchronized HashMap<String, Object> getActivityInputParameters() throws Exception
    {
        String attachedToNodeIdentifier;

        // Get the nodeId of the activity to which this node is attached.
        attachedToNodeIdentifier = definition.getAttachedToNodeIdentifier();
        if (attachedToNodeIdentifier != null)
        {
            Map<String, T8FlowStateParameterMap> inputDataSetMap;
            HashMap<String, Object> inputData;
            List<String> activityInputNodeIdentifiers;

            // Get the activity input node identifiers and the input data from the activity node's state.
            inputData = new HashMap<String, Object>();
            activityInputNodeIdentifiers = parentFlow.getInputNodeIdentifiers(attachedToNodeIdentifier);
            inputDataSetMap = flowState.getNodeState(attachedToNodeIdentifier).getInputParameters();

            // Loop through the input set and merge all input sets into a final input parameter map.
            for (String inputNodeIdentifier : activityInputNodeIdentifiers)
            {
                HashMap<String, Object> inputDataSet;

                inputDataSet = inputDataSetMap.get(inputNodeIdentifier);
                if (inputDataSet != null) inputData.putAll(inputDataSet);
            }

            // Return the complete input data collection.
            return inputData;
        }
        else throw new Exception("No activity specified to which boundary node is attached: " + definition);
    }

    @Override
    public void setStartEvent(T8FlowSignalReceivedEvent event)
    {
        startEvent = event;
    }

    @Override
    public void setAttachedToNodeInstanceIdentifier(String nodeInstanceIdentifier)
    {
        attachedToNodeInstanceIdentifier = nodeInstanceIdentifier;
    }
}
