package com.pilog.t8.flow;

import com.pilog.t8.T8FlowManager;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.task.T8TaskCompletionResult;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.flow.task.T8TaskList;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8FlowOperations
{
    public static class FindNodeActiveInputIdentifiersOperation extends T8DefaultServerOperation
    {
        public FindNodeActiveInputIdentifiersOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String flowInstanceIdentifier;
            String nodeInstanceIdentifier;

            flowManager = serverContext.getFlowManager();
            flowInstanceIdentifier = (String)operationParameters.get(PARAMETER_FLOW_IID);
            nodeInstanceIdentifier = (String)operationParameters.get(PARAMETER_NODE_IID);
            return HashMaps.newHashMap(PARAMETER_NODE_IIDS, flowManager.findActiveInputNodeInstanceIdentifiers(context, flowInstanceIdentifier, nodeInstanceIdentifier));
        }
    }

    public static class RetryExecutionOperation extends T8DefaultServerOperation
    {
        public RetryExecutionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String flowInstanceIdentifier;
            String nodeInstanceIdentifier;

            flowManager = serverContext.getFlowManager();
            flowInstanceIdentifier = (String)operationParameters.get(PARAMETER_FLOW_IID);
            nodeInstanceIdentifier = (String)operationParameters.get(PARAMETER_NODE_IID);
            flowManager.retryExecution(context, flowInstanceIdentifier, nodeInstanceIdentifier);
            return null;
        }
    }

    public static class GetFlowCountOperation extends T8DefaultServerOperation
    {
        public GetFlowCountOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;

            flowManager = serverContext.getFlowManager();
            return HashMaps.newHashMap(PARAMETER_FLOW_COUNT, flowManager.getFlowCount(context));
        }
    }

    public static class GetCachedFlowCountOperation extends T8DefaultServerOperation
    {
        public GetCachedFlowCountOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;

            flowManager = serverContext.getFlowManager();
            return HashMaps.newHashMap(PARAMETER_FLOW_COUNT, flowManager.getCachedFlowCount(context));
        }
    }

    public static class ClearFlowCacheOperation extends T8DefaultServerOperation
    {
        public ClearFlowCacheOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;

            flowManager = serverContext.getFlowManager();
            return HashMaps.newHashMap(PARAMETER_FLOW_COUNT, flowManager.clearFlowCache(context));
        }
    }

    public static class GetTaskCountOperation extends T8DefaultServerOperation
    {
        public GetTaskCountOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Integer> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            T8TaskFilter taskFilter;

            flowManager = serverContext.getFlowManager();
            taskFilter = (T8TaskFilter) operationParameters.get(T8FlowManagerResource.PARAMETER_TASK_FILTER);
            return HashMaps.createSingular(PARAMETER_TASK_COUNT, flowManager.getTaskCount(context, taskFilter));
        }
    }

    public static class GetTaskListSummaryOperation extends T8DefaultServerOperation
    {
        public GetTaskListSummaryOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8TaskListSummary> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            T8TaskFilter taskFilter;

            flowManager = serverContext.getFlowManager();
            taskFilter = (T8TaskFilter) operationParameters.get(T8FlowManagerResource.PARAMETER_TASK_FILTER);
            return HashMaps.createSingular(PARAMETER_TASK_LIST_SUMMARY, flowManager.getTaskListSummary(context, taskFilter));
        }
    }

    public static class GetTaskListOperation extends T8DefaultServerOperation
    {
        public GetTaskListOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8TaskList> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            T8TaskFilter taskFilter;

            taskFilter = (T8TaskFilter)operationParameters.get(PARAMETER_TASK_FILTER);

            flowManager = serverContext.getFlowManager();
            return HashMaps.createSingular(PARAMETER_TASK_LIST, flowManager.getTaskList(context, taskFilter));
        }
    }

    public static class ReassignTaskOperation extends T8DefaultServerOperation
    {
        public ReassignTaskOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String taskInstanceIdentifier;
            String userID;
            String profileID;

            taskInstanceIdentifier = (String)operationParameters.get(PARAMETER_TASK_IID);
            userID = (String)operationParameters.get(PARAMETER_USER_ID);
            profileID = (String)operationParameters.get(PARAMETER_PROFILE_ID);

            flowManager = serverContext.getFlowManager();
            flowManager.reassignTask(context, taskInstanceIdentifier, userID, profileID);

            return null;
        }
    }

    public static class SetTaskPriorityOperation extends T8DefaultServerOperation
    {
        public SetTaskPriorityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String taskInstanceIdentifier;
            Integer priority;

            taskInstanceIdentifier = (String) operationParameters.get(PARAMETER_TASK_IID);
            priority = (Integer) operationParameters.get(PARAMETER_TASK_PRIORITY);

            flowManager = serverContext.getFlowManager();
            flowManager.setTaskPriority(context, taskInstanceIdentifier, priority);

            return null;
        }
    }

    public static class DecreaseTaskPriorityOperation extends T8DefaultServerOperation
    {
        public DecreaseTaskPriorityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String taskInstanceIdentifier;
            Integer priority;

            taskInstanceIdentifier = (String) operationParameters.get(PARAMETER_TASK_IID);
            priority = (Integer) operationParameters.get(PARAMETER_TASK_PRIORITY_AMOUNT);

            flowManager = serverContext.getFlowManager();
            flowManager.decreaseTaskPriority(context, taskInstanceIdentifier, priority);

            return null;
        }
    }

    public static class IncreaseTaskPriorityOperation extends T8DefaultServerOperation
    {
        public IncreaseTaskPriorityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String taskInstanceIdentifier;
            Integer priority;

            taskInstanceIdentifier = (String) operationParameters.get(PARAMETER_TASK_IID);
            priority = (Integer) operationParameters.get(PARAMETER_TASK_PRIORITY_AMOUNT);

            flowManager = serverContext.getFlowManager();
            flowManager.increaseTaskPriority(context, taskInstanceIdentifier, priority);

            return null;
        }
    }

    public static class ClaimTaskOperation extends T8DefaultServerOperation
    {
        public ClaimTaskOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String taskInstanceIdentifier;

            taskInstanceIdentifier = (String)operationParameters.get(PARAMETER_TASK_IID);

            flowManager = serverContext.getFlowManager();
            flowManager.claimTask(context, taskInstanceIdentifier);
            return null;
        }
    }

    public static class UnclaimTaskOperation extends T8DefaultServerOperation
    {
        public UnclaimTaskOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String taskInstanceIdentifier;

            taskInstanceIdentifier = (String)operationParameters.get(PARAMETER_TASK_IID);

            flowManager = serverContext.getFlowManager();
            flowManager.unclaimTask(context, taskInstanceIdentifier);
            return null;
        }
    }

    public static class CompleteTaskOperation extends T8DefaultServerOperation
    {
        public CompleteTaskOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Map<String, Object> outputParameters;
            T8TaskCompletionResult result;
            T8FlowManager flowManager;
            String taskIid;

            taskIid = (String)operationParameters.get(PARAMETER_TASK_IID);
            outputParameters = (Map<String, Object>)operationParameters.get(PARAMETER_OUTPUT_PARAMETERS);

            flowManager = serverContext.getFlowManager();
            result = flowManager.completeTask(context, taskIid, outputParameters);
            return HashMaps.createSingular(PARAMETER_TASK_COMPLETION_RESULT, result);
        }
    }

    public static class GetFlowStatusOperation extends T8DefaultServerOperation
    {
        public GetFlowStatusOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8FlowStatus> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String flowInstanceIdentifier;
            Boolean includeDisplayData;

            flowInstanceIdentifier = (String)operationParameters.get(PARAMETER_FLOW_IID);
            includeDisplayData = (Boolean)operationParameters.get(PARAMETER_INCLUDE_DISPLAY_DATA);

            if (includeDisplayData == null) includeDisplayData = false;

            flowManager = serverContext.getFlowManager();
            return HashMaps.createSingular(PARAMETER_FLOW_STATUS, flowManager.getFlowStatus(context, flowInstanceIdentifier, includeDisplayData));
        }
    }

    public static class GetFlowStateOperation extends T8DefaultServerOperation
    {
        public GetFlowStateOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8FlowState> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String flowInstanceIdentifier;

            flowInstanceIdentifier = (String)operationParameters.get(PARAMETER_FLOW_IID);

            flowManager = serverContext.getFlowManager();
            return HashMaps.createSingular(PARAMETER_FLOW_STATE, flowManager.getFlowState(context, flowInstanceIdentifier));
        }
    }

    public static class QueueFlowOperation extends T8DefaultServerOperation
    {
        public QueueFlowOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8FlowStatus> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Map<String, Object> flowInputParameters;
            T8FlowManager flowManager;
            T8FlowStatus flowStatus;
            String flowIdentifier;

            flowIdentifier = (String)operationParameters.get(PARAMETER_FLOW_ID);
            flowInputParameters = (Map<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETERS);

            flowManager = serverContext.getFlowManager();
            flowStatus = flowManager.queueFlow(context, flowIdentifier, flowInputParameters);
            return HashMaps.createSingular(PARAMETER_FLOW_STATUS, flowStatus);
        }
    }

    public static class StartNewFlowOperation extends T8DefaultServerOperation
    {
        public StartNewFlowOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8FlowStatus> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Map<String, Object> inputParameters;
            T8FlowManager flowManager;
            String flowId;

            flowId = (String)operationParameters.get(PARAMETER_FLOW_ID);
            inputParameters = (Map<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETERS);

            flowManager = serverContext.getFlowManager();
            return HashMaps.createSingular(PARAMETER_FLOW_STATUS, flowManager.startFlow(context, flowId, inputParameters));
        }
    }

    public static class KillFlowOperation extends T8DefaultServerOperation
    {
        public KillFlowOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String flowInstanceIdentifier;

            flowInstanceIdentifier = (String)operationParameters.get(PARAMETER_FLOW_IID);

            flowManager = serverContext.getFlowManager();
            flowManager.killFlow(context, flowInstanceIdentifier);
            return null;
        }
    }

    public static class SignalOperation extends T8DefaultServerOperation
    {
        public SignalOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String signalIdentifier;
            Map<String, Object> signalParameters;

            signalIdentifier = (String)operationParameters.get(PARAMETER_SIGNAL_ID);
            signalParameters = (Map<String, Object>)operationParameters.get(PARAMETER_SIGNAL_PARAMETERS);

            flowManager = serverContext.getFlowManager();
            flowManager.fireSignalEvent(context, signalIdentifier, signalParameters);
            return null;
        }
    }

    public static class GetTaskDetailsOperation extends T8DefaultServerOperation
    {
        public GetTaskDetailsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8TaskDetails> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String taskInstanceIdentifier;

            taskInstanceIdentifier = (String)operationParameters.get(PARAMETER_TASK_IID);

            flowManager = serverContext.getFlowManager();
            return HashMaps.createSingular(PARAMETER_TASK_DETAIL, flowManager.getTaskDetails(context, taskInstanceIdentifier));
        }
    }

    public static class GetTaskUiDataOperation extends T8DefaultServerOperation
    {
        public GetTaskUiDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String taskIid;

            taskIid = (String)operationParameters.get(PARAMETER_TASK_IID);

            flowManager = serverContext.getFlowManager();
            return HashMaps.createSingular(PARAMETER_TASK_UI_DATA, flowManager.getTaskUiData(context, taskIid));
        }
    }

    public static class RefreshTaskListOperation extends T8DefaultServerOperation
    {
        public RefreshTaskListOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;
            String refreshType;
            List<String> idList;

            refreshType = (String)operationParameters.get(PARAMETER_REFRESH_TYPE);
            idList = (List<String>)operationParameters.get(PARAMETER_ID_LIST);

            flowManager = serverContext.getFlowManager();
            flowManager.refreshTaskList(context, T8FlowManager.T8TaskListRefreshType.valueOf(refreshType), idList);
            return null;
        }
    }

    public static class RecacheTaskEscalationTriggersOperation extends T8DefaultServerOperation
    {
        public RecacheTaskEscalationTriggersOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FlowManager flowManager;

            flowManager = serverContext.getFlowManager();
            flowManager.recacheTaskEscalationTriggers(context);
            return null;
        }
    }
}
