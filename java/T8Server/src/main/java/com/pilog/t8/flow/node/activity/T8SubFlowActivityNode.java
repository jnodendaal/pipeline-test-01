package com.pilog.t8.flow.node.activity;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.definition.flow.node.activity.T8SubFlowActivityDefinition;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.T8FlowReference;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SubFlowActivityNode extends T8DefaultFlowNode
{
    private final T8SubFlowActivityDefinition definition;
    private final boolean subFlowCompleted;
    private String subFlowInstanceIdentifier;
    private final Map<String, Object> subFlowOutputParameters;

    public T8SubFlowActivityNode(T8Context context, T8Flow parentFlow, T8SubFlowActivityDefinition definition, String instanceIdentifier)
    {
        super(context, parentFlow, definition, instanceIdentifier);
        this.definition = definition;
        this.subFlowOutputParameters = null;
        this.subFlowInstanceIdentifier = null;
        this.subFlowCompleted = false;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters) throws Exception
    {
        Map<String, Object> outputParameters;

        // Execute the sub-flow.
        outputParameters = executeSubFlow(flowState, T8IdentifierUtilities.mapParameters(inputParameters, definition.getSubFlowInputParameterMapping()));

        // Map the sub-flow output parameters to the flow parameters.
        outputParameters = T8IdentifierUtilities.mapParameters(outputParameters, definition.getSubFlowOutputParameterMapping());
        return outputParameters;
    }

    protected Map<String, Object> executeSubFlow(T8FlowState state, Map<String, Object> inputParameters) throws Exception
    {
        T8FlowReference subFlowReference;
        Map<String, Object> outputParameters;
        T8FlowStatus subFlowStatus;
        String subFlowIdentifier;

        // Start the sub-flow and get it's instance nodeId.
        subFlowIdentifier = definition.getSubFlowIdentifier();
        subFlowStatus = parentFlow.getFlowController().startFlow(internalContext, subFlowIdentifier, inputParameters);
        subFlowInstanceIdentifier = subFlowStatus.getFlowIid();
        //nodeState.setExecutionParameter(T8SubFlowActivityDefinition.NODE_STATE_SUB_FLOW_INSTANCE_IDENTIFIER, subFlowInstanceIdentifier);

        // Create the sub-flow instance definition and add it to the state.
        subFlowReference = new T8FlowReference(subFlowInstanceIdentifier);
        subFlowReference.setInitiatorNodeIdentifier(definition.getIdentifier());
        subFlowReference.setInitiatorUserIdentifier(null);
        state.addSubFlowReference(subFlowReference);

        // Update the sub-flow instance definition to indicate that the sub-flow has started.
        subFlowReference.setFlowStatus(T8Flow.FlowStatus.ACTIVE);

        // Persist the flow state.
        //persistState();

        // Wait for the sub-flow to complete.
        outputParameters = waitForSubFlowCompletion();

        // Update the sub-flow instance definition to indicate that the sub-flow has completed.
        subFlowReference.setFlowStatus(T8Flow.FlowStatus.COMPLETED);

        // Return the output from the sub-flow.
        return outputParameters;
    }

    protected synchronized Map<String, Object> waitForSubFlowCompletion() throws Exception
    {
        // Wait until the required input data set becomes available.
        while ((!stop) && (!subFlowCompleted))
        {
            statusMessage = "Waiting for sub-flow completion.";
            wait(); // The sub-flow output data is not available yet, so wait some more and then try again.
        }

        // Return the task output data.
        return subFlowOutputParameters;
    }

//    @Override
//    public void processFlowEvent(T8FlowEvent event)
//    {
//        // Always invoke the super method to ensure default event processing is handled.
//        super.processFlowEvent(event);
//
//        // Check the event type and only process events that are
//        if (event instanceof T8FlowCompletedEvent)
//        {
//            T8FlowCompletedEvent flowCompletedEvent;
//
//            // Only react to resources required by this node's parent flow.
//            flowCompletedEvent = (T8FlowCompletedEvent)event;
//            if (flowCompletedEvent.getFlow().getInstanceIdentifier().equals(subFlowInstanceIdentifier))
//            {
//                synchronized (T8SubFlowActivityNode.this)
//                {
//                    subFlowOutputParameters = flowCompletedEvent.getOutputParameters();
//                    subFlowCompleted = true;
//                    T8SubFlowActivityNode.this.notifyAll();
//                }
//            }
//        }
//    }
}
