package com.pilog.t8.functionality;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectStateDetails
{
    private String dataObjectId;
    private String dataObjectIid;
    private String stateConceptId;

    public T8DataObjectStateDetails(String dataObjectId, String dataObjectIid, String stateConceptId)
    {
        this.dataObjectId = dataObjectId;
        this.dataObjectIid = dataObjectIid;
        this.stateConceptId = stateConceptId;
    }

    public String getDataObjectId()
    {
        return dataObjectId;
    }

    public void setDataObjectId(String dataObejctId)
    {
        this.dataObjectId = dataObejctId;
    }

    public String getDataObjectIid()
    {
        return dataObjectIid;
    }

    public void setDataObjectIid(String dataObjectIid)
    {
        this.dataObjectIid = dataObjectIid;
    }

    public String getStateConceptId()
    {
        return stateConceptId;
    }

    public void setStateConceptId(String stateConceptId)
    {
        this.stateConceptId = stateConceptId;
    }
}
