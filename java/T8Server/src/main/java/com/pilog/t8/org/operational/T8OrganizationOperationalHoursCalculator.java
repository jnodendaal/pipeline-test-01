package com.pilog.t8.org.operational;

// We use specific static imports to keep things clean and to allow for optimization
import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.SATURDAY;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.SUNDAY;
import static java.util.Calendar.YEAR;
import static org.joda.time.DateTimeConstants.HOURS_PER_DAY;
import static org.joda.time.DateTimeConstants.MINUTES_PER_HOUR;
import static org.joda.time.DateTimeConstants.MINUTES_PER_DAY;
import static org.joda.time.DateTimeConstants.MILLIS_PER_HOUR;
import static org.joda.time.DateTimeConstants.MILLIS_PER_MINUTE;
import static org.joda.time.DateTimeConstants.MILLIS_PER_SECOND;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalIntervalDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalNonWorkDayDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalSettingsDefinition;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author Gavin Boshoff
 */
public class T8OrganizationOperationalHoursCalculator implements T8OperationalHoursCalculator
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8OrganizationOperationalHoursCalculator.class);

    /**
     * Gets the applicable version of the required operational hours calculator.
     * The instance is determined by the supplied definition, as well as the
     * version of the definition. If there is a change in the version, the
     * original (if any) will be discarded and the new version created.<br/>
     * <br/>
     * Each operational hours calculator is uniquely identified by the
     * identifier of the definition. If for some reason a duplicate identifier
     * exists, the calculator will be continuously replaced in the event that
     * the version is different.
     *
     * @param definition The {@code T8OrganizationOperationalSettingsDefinition}
     *      defining the constraints and properties of the calculator to be
     *      returned
     *
     * @return The {@code T8OrganizationOperationalHoursCalculator} instance as
     *      defined by the supplied definition
     */
    public static T8OrganizationOperationalHoursCalculator getInstance(T8OrganizationOperationalSettingsDefinition definition)
    {
        T8OrganizationOperationalHoursCalculator calculator;

        synchronized (T8OrganizationOperationalHoursCalculatorHolder.INSTANCE_CALCULATORS)
        {
            calculator = T8OrganizationOperationalHoursCalculatorHolder.INSTANCE_CALCULATORS.get(definition.getIdentifier());
        }

        return verifyCalculator(definition, calculator);
    }

    private static T8OrganizationOperationalHoursCalculator verifyCalculator(T8OrganizationOperationalSettingsDefinition definition, T8OrganizationOperationalHoursCalculator calculator)
    {
        // Even though we make use of synchronization, we don't use double checks,
        // simply because it does not have a big enough influence on the current
        // singleton's structure

        if (calculator == null)
        {
            synchronized (T8OrganizationOperationalHoursCalculatorHolder.INSTANCE_CALCULATORS)
            {
                T8OrganizationOperationalHoursCalculatorHolder.INSTANCE_CALCULATORS.put(definition.getIdentifier(), new T8OrganizationOperationalHoursCalculator(definition));
            }
        }
        // This simply caters for the definition being modified during the system lifecycle
        // We will not cater for an older version being imported during runtime
        else if (calculator.getRevision() < definition.getRevision())
        {
            // The existing calculator will be replaced
            synchronized (T8OrganizationOperationalHoursCalculatorHolder.INSTANCE_CALCULATORS)
            {
                T8OrganizationOperationalHoursCalculatorHolder.INSTANCE_CALCULATORS.put(definition.getIdentifier(), new T8OrganizationOperationalHoursCalculator(definition));
            }
        } else return calculator;

        return getInstance(definition);
    }

    /**
     * Holder class as defined by the standardized singleton pattern.
     */
    private static class T8OrganizationOperationalHoursCalculatorHolder
    {
        private T8OrganizationOperationalHoursCalculatorHolder() {}
        private static final Map<String, T8OrganizationOperationalHoursCalculator> INSTANCE_CALCULATORS = new HashMap<>();
    }

    private final T8OrganizationOperationalSettingsDefinition definition;
    private final OperationalIntervals operationalIntervals;
    private final HashSet<NonWorkDay> nonWorkDays;
    private final Long calculatorRevision;

    /**
     * Instantiates and initializes the
     * {@code T8OrganizationOperationalHoursCalculator} as defined by the
     * supplied definition.
     *
     * @param definition The {@code T8OrganizationOperationalSettingsDefinition}
     *      defining the constraints and properties of the calculator to be
     *      initialized
     */
    private T8OrganizationOperationalHoursCalculator(T8OrganizationOperationalSettingsDefinition definition)
    {
        this.operationalIntervals = new OperationalIntervals();
        this.calculatorRevision = definition.getRevision();
        this.nonWorkDays = new HashSet<>();
        this.definition = definition;
        initialize();

        LOGGER.log("Calculator Version : " + this.calculatorRevision);
        LOGGER.log("" + this.operationalIntervals);
    }

    /**
     * The version of the calculator as defined by the version of the
     * originating definition.
     *
     * @return The {@code Long} version of the calculator
     */
    private Long getRevision()
    {
        return this.calculatorRevision;
    }

    private void initialize()
    {
        Calendar workingCalendar;
        NonWorkDay nonWorkDay;
        int currentYear;

        // Add the intervals
        this.definition.getOperationalIntervalDefinitions().forEach(dayDefinition -> operationalIntervals.addOperationalInterval(dayDefinition));
        // Fill empty weekday's with inactive intervals
        this.operationalIntervals.fillIntervals();

        // Create a new calendar instance
        workingCalendar = Calendar.getInstance();
        currentYear = workingCalendar.get(YEAR);
        // Add the public holidays to be taken into account later
        for (T8OrganizationOperationalNonWorkDayDefinition nonWorkDayDefinition : this.definition.getNonWorkDayDefinitions())
        {
            workingCalendar.setTimeInMillis(nonWorkDayDefinition.getNonWorkingDayDateMillis());

            // If the date is repeated, there may be multiple instances
            if (nonWorkDayDefinition.isRepeatYearly())
            {
                for (int year = workingCalendar.get(YEAR); year <= currentYear; year++)
                {
                    // Set the year on the working calendar. First iteration will not make a difference
                    workingCalendar.set(YEAR, year);

                    nonWorkDay = new NonWorkDay(workingCalendar, this.operationalIntervals);
                    // We don't want to keep non-observed non work days in memory
                    if (nonWorkDay.isObserved()) this.nonWorkDays.add(nonWorkDay);
                }
            }
            else
            {
                nonWorkDay = new NonWorkDay(workingCalendar, this.operationalIntervals);
                // We don't want to keep non-observed non work days in memory
                if (nonWorkDay.isObserved()) this.nonWorkDays.add(nonWorkDay);
            }
        }
    }

    /**
     * Creates a well-defined display {@code String} for the specified
     * millisecond duration.
     *
     * @param millisecondDuration The {@code long} millisecond value which
     *      defines the duration for which the string needs to be built
     *
     * @return The display {@code String} representation for the specified
     *      duration
     */
    @Override
    public String prettyPrintDuration(long millisecondDuration)
    {
        StringBuilder prettyPrintBuilder;
        boolean leadingValueAdded;
        long millisCopy;
        long minutes;
        long seconds;
        long hours;
        long days;

        millisCopy = millisecondDuration;

        // Calculate the Days
        days = TimeUnit.MILLISECONDS.toDays(millisCopy);
        millisCopy -= TimeUnit.DAYS.toMillis(days);

        // Calculate the Hours
        hours = TimeUnit.MILLISECONDS.toHours(millisCopy);
        millisCopy -= TimeUnit.HOURS.toMillis(hours);

        // Calculate the Minutes
        minutes = TimeUnit.MILLISECONDS.toMinutes(millisCopy);
        millisCopy -= TimeUnit.MINUTES.toMillis(minutes);

        // Calculate the Seconds
        seconds = TimeUnit.MILLISECONDS.toSeconds(millisCopy);

        prettyPrintBuilder = new StringBuilder();
        leadingValueAdded = false;

        // Append the days if required
        if (days > 0)
        {
            prettyPrintBuilder.append(days).append("d ");
            leadingValueAdded = true;
        }

        // Append the hours if required
        if (hours > 0 || leadingValueAdded)
        {
            prettyPrintBuilder.append(hours).append("h ");
            leadingValueAdded = true;
        }

        // Append the minutes if required
        if (minutes > 0 || leadingValueAdded)
        {
            prettyPrintBuilder.append(minutes).append("m ");
        }

        // Append the seconds, regardless if it is 0
        prettyPrintBuilder.append(seconds).append("s");

        return prettyPrintBuilder.toString();
    }

    @Override
    public long calculateOperationalEndTime(long startTime, long duration)
    {
        if (startTime < 0) throw new IllegalArgumentException("The Start Time specified has to be a positive.");
        if (duration < 0) throw new IllegalArgumentException("The duration specified has to be a positive.");

        CalculatorInstant startInstant;
        boolean startIntervalFound;

        // We start with a start instant, since we want to shift into the future
        startInstant = new CalculatorStartInstant(startTime);
        startIntervalFound = false;

        // Here we want to get the intervals within which the start and end times fall
        for (OperationalInterval operationalInterval : this.operationalIntervals)
        {
            startIntervalFound = startInstant.findInInterval(operationalInterval);
            if (startIntervalFound) break;
        }

        if (!startIntervalFound) throw new RuntimeException("Failure to find the required interval. Start {"+startTime+"} - {"+startIntervalFound+"}");

        // We get the instant into the past, to a point where it will be larger than the duration
        while ((startInstant.accumulated() + startInstant.currentInterval().intervalMillisDuration()) <= duration)
        {
            startInstant.shift();
        }

        // Now we need to do the final calculation to ensure we get the almost-perfect timestamp
        if (startInstant.accumulated() < duration)
        {
            long missingDifference;

            missingDifference = startInstant.currentInterval().intervalMillisDuration() // The maximum possible difference still missing
                    - (duration - startInstant.accumulated()); // The missing difference

            startInstant.currentTimeCal().add(MILLISECOND, (int)missingDifference); // Subtract the missing difference for an earlier time
        }

        return startInstant.currentTime();
    }

    @Override
    public long calculateOperationalStartTime(long endTime, long duration)
    {
        if (endTime < 0) throw new IllegalArgumentException("The End Time specified has to be a positive.");
        if (duration < 0) throw new IllegalArgumentException("The duration specified has to be a positive.");

        CalculatorInstant endInstant;
        boolean endIntervalFound;
        boolean noShift;

        // We start with an end instant, since we want to shift into the past
        endInstant = new CalculatorEndInstant(endTime);
        endIntervalFound = false;
        noShift = true;

        // Here we want to get the intervals within which the start and end times fall
        for (OperationalInterval operationalInterval : this.operationalIntervals)
        {
            endIntervalFound = endInstant.findInInterval(operationalInterval);
            if (endIntervalFound) break;
        }

        if (!endIntervalFound) throw new RuntimeException("Failure to find the required interval. End {"+endTime+"} - {"+endIntervalFound+"}");

        // We get the instant into the past, to a point where it will be larger than the duration
        while ((endInstant.accumulated() + endInstant.currentInterval().intervalMillisDuration()) <= duration)
        {
            endInstant.shift();
            noShift = false;
        }

        // If we didn't shift, we are working with a single interval
        if (noShift) return endTime-duration;

        // Now we need to do the final calculation to ensure we get the almost-perfect timestamp
        if (endInstant.accumulated() < duration)
        {
            long missingDifference;

            missingDifference = endInstant.currentInterval().intervalMillisDuration() // The maximum possible difference still missing
                    - (duration - endInstant.accumulated()); // The missing difference

            endInstant.currentTimeCal().add(MILLISECOND, (0-(int)missingDifference)); // Subtract the missing difference for an earlier time
        }

        return endInstant.currentTime();
    }

    /**
     * This method calculates the absolute time between two dates, this does not
     * take into consideration business hours/days.<br/>
     * <br/>
     * The time returned is a millisecond long of the amount of time between
     * the two given dates.
     *
     * @param startTime The start time as a {@code long} of milliseconds
     * @param endTime The end time as a {@code long} of milliseconds
     *
     * @return The amount of time between the two times as a millisecond value
     */
    @Override
    public long calculateTimeDifference(long startTime, long endTime)
    {
        if (startTime < 0) throw new IllegalArgumentException("The Start Time specified has to be a positive.");
        if (endTime < 0) throw new IllegalArgumentException("The End Time specified has to be a positive.");
        if (endTime < startTime) throw new IllegalArgumentException("The End Time needs to be equal to or after the start time.");

        return (endTime - startTime);
    }

    /**
     * Calculates the operational time difference between the start and the end
     * time. This takes into consideration the operational setup which has been
     * done by only calculating the working hours which fall between the start
     * and end time.<br/>
     * <br/>
     * This also takes into consideration any public holidays which have been
     * specified and which fall within the period between the start and end
     * times.<br/>
     * <br/>
     * Operations which occur completely outside of working hours will return
     * a {@code 0 (zero)} elapsed time, since no operational time was spent on
     * the task.
     *
     * @param startTime The {@code long} millisecond, since the Epoch, time
     *      which defines the exact instant at which the process was started
     * @param endTime The {@code long} millisecond, since the Epoch, time which
     *      defines the exact instant at which the process was completed
     *
     * @return The {@code long} millisecond time period which defines the
     *      operational time which has elapsed since the start time, up to the
     *      end time
     */
    @Override
    public long calculateOperationalTimeDifference(long startTime, long endTime)
    {
        if (startTime < 0) throw new IllegalArgumentException("The Start Time specified has to be a positive.");
        if (endTime < 0) throw new IllegalArgumentException("The End Time specified has to be a positive.");
        if (endTime < startTime) throw new IllegalArgumentException("The End Time needs to be equal to or after the start time.");

        CalculatorInstant startInstant;
        CalculatorInstant endInstant;
        boolean startIntervalFound;
        boolean endIntervalFound;
        long totalDifference;

        startInstant = new CalculatorStartInstant(startTime);
        endInstant = new CalculatorEndInstant(endTime);
        startIntervalFound = false;
        endIntervalFound = false;

        // Here we want to get the intervals within which the start and end times fall
        for (OperationalInterval operationalInterval : this.operationalIntervals)
        {
            if (!startIntervalFound)
            {
                startIntervalFound = startInstant.findInInterval(operationalInterval);
            }

            if (!endIntervalFound)
            {
                endIntervalFound = endInstant.findInInterval(operationalInterval);
            }

            if (startIntervalFound && endIntervalFound) break;
        }

        if (!startIntervalFound || !endIntervalFound) throw new RuntimeException("Failure to find the intervals. Start {"+startTime+"} - {"+startIntervalFound+"} - End {"+endTime+"} - {"+endIntervalFound+"}");

        // If the start instant is now after the end, it is because they
        // probably fell in between 2 intervals, or marginally with one end inside
        switch (startInstant.after(endInstant))
        {
            case CalculatorInstant.F_NOT_AFTER:
                break;
            case CalculatorInstant.T_WITH_CONTAINMENT:
                return calculateMarginalContainment(startInstant, endInstant);
            case CalculatorInstant.T_NO_CONTAINMENT:
                return 0l;
        }

        // If these are already in the same interval, they were either shifted to
        // it, or they fell within it from the start.
        if (startInstant.isSameDayInterval(endInstant))
        {
            totalDifference = startInstant.getDifference(endInstant);
        }
        else
        {
            while (!startInstant.isSameDayInterval(endInstant))
            {
                if (startInstant.shouldShift(endInstant)) startInstant.shift();
                if (endInstant.shouldShift(startInstant)) endInstant.shift();
            }

            totalDifference = startInstant.getDifference(endInstant);
        }

        // Here we are going to have to check if any public holidays fell within the working period
        int nonWorkDayContainmentLevel;

        for (NonWorkDay nonWorkDay : this.nonWorkDays)
        {
            nonWorkDayContainmentLevel = nonWorkDay.findContainmentLevel(startTime, endTime);
            switch (nonWorkDayContainmentLevel) // Order of cases from most to least likely
            {
                case NonWorkDay.NOT_CONTAINED:
                    break;
                case NonWorkDay.FULLY_CONTAINED:
                    totalDifference -= nonWorkDay.getFullyContainedSubtractionDuration();
                    break;
                case NonWorkDay.BOTH_INSIDE:
                    totalDifference = 0l;
                    break;
                case NonWorkDay.END_INSIDE:
                    totalDifference -= nonWorkDay.getEndInsideSubtractionDuration(endTime);
                    break;
                case NonWorkDay.START_INSIDE:
                    totalDifference -= nonWorkDay.getStartInsideSubtractionDuration(startTime);
                    break;
            }
        }

        return totalDifference;
    }

    @Override
    public boolean isOperationalInstant(long instant)
    {
        if (instant < 0) throw new IllegalArgumentException("The Instant to check has to be a positive value.");
        Calendar instantCalendar;

        instantCalendar = Calendar.getInstance();
        instantCalendar.setTimeInMillis(instant);

        for (OperationalInterval operationalInterval : this.operationalIntervals)
        {
            if (operationalInterval.findInInterval(instantCalendar) == OperationalInterval.IN_CURRENT)
            {
                return !this.nonWorkDays.stream().anyMatch(nonWorkDay -> (nonWorkDay.findContainmentLevel(0L, instant) == NonWorkDay.END_INSIDE));
            }
        }

        return false;
    }

    private long calculateMarginalContainment(CalculatorInstant startInstant, CalculatorInstant endInstant)
    {
        long marginalContainmentDifference;

        // We get the absolute difference between the 2
        marginalContainmentDifference = (endInstant.originalTime() - startInstant.originalTime());
        // Now we want to subtract the non-contained time between the 2
        marginalContainmentDifference -= (startInstant.currentTime() - endInstant.currentTime());

        return marginalContainmentDifference;
    }

    /**
     * A container class which contains redundant information relating to a
     * specific public holiday. The information is considered redundant since it
     * is initialized with pre-calculated values and details to prevent
     * additional processing at a later stage.
     */
    private class NonWorkDay
    {
        /** The Public Holiday falls completely outside of the given interval. */
        private static final int NOT_CONTAINED = -1;
        /** The Public Holiday falls completely inside of the given interval. */
        private static final int FULLY_CONTAINED = 0;
        /** The Public Holiday contains the start time of the given interval. */
        private static final int START_INSIDE = 1;
        /** The Public Holiday contains both the start and end times of the given interval. */
        private static final int BOTH_INSIDE = 2;
        /** The Public Holiday contains the end time of the given interval. */
        private static final int END_INSIDE = 3;

        private final List<OperationalInterval> containedIntervals;
        private final Calendar originCalendar;
        private final boolean observed;

        private Calendar holidayStartTime;
        private Calendar holidayEndTime;

        private long holidayStartTimeMillis;
        private long holidayEndTimeMillis;

        private NonWorkDay(Calendar holidayCalendar, OperationalIntervals operationalIntervals)
        {
            this.containedIntervals = new ArrayList<>();
            this.originCalendar = holidayCalendar;
            this.observed = initialize(operationalIntervals);
        }

        private boolean initialize(OperationalIntervals operationalIntervals)
        {
            // We set the start time for the holiday to midnight of that day
            {
                this.holidayStartTime = (Calendar)this.originCalendar.clone();
                this.holidayStartTime.set(HOUR_OF_DAY, 0);
                this.holidayStartTime.set(MINUTE, 0);
                this.holidayStartTime.set(SECOND, 0);
                this.holidayStartTime.set(MILLISECOND, 0);
                this.holidayStartTimeMillis = this.holidayStartTime.getTimeInMillis();
            }

            // We set the end time for the holiday to 1 millisecond away from midnight of the following day
            {
                this.holidayEndTime = (Calendar)this.originCalendar.clone();
                this.holidayEndTime.set(HOUR_OF_DAY, 23);
                this.holidayEndTime.set(MINUTE, 59);
                this.holidayEndTime.set(SECOND, 59);
                this.holidayEndTime.set(MILLISECOND, 999);
                this.holidayEndTimeMillis = this.holidayEndTime.getTimeInMillis();
            }

            // Now we need to check for operational intervals which fall within
            // the public holiday
            int holidayDayOfWeek;

            holidayDayOfWeek = this.originCalendar.get(DAY_OF_WEEK);
            for (OperationalInterval operationalInterval : operationalIntervals)
            {
                if (operationalInterval.isActive() && operationalInterval.dayOfWeek() == holidayDayOfWeek)
                {
                    this.containedIntervals.add(operationalInterval);
                }
            }

            // We want to be able to ignore non work days which do not affect the operational hours
            return !this.containedIntervals.isEmpty();
        }

        private boolean isObserved()
        {
            return this.observed;
        }

        /**
         * The containment level is defined by how the public holiday relates to
         * the specified start and end times. In general it is expected that the
         * public holiday will fall completely between the start and end times
         * or not at all. There is however accommodation made for when the
         * start and/or end time falls within the public holiday.<br/>
         * <br/>
         * Potential Values:<br/>
         * <blockquote>
         *      {@link #FULLY_CONTAINED}<br/>
         *      {@link #NOT_CONTAINED}<br/>
         *      {@link #BOTH_INSIDE}<br/>
         *      {@link #END_INSIDE}<br/>
         *      {@link #START_INSIDE}<br/>
         * </blockquote>
         *
         * @param startTime The {@code long} millisecond start time defining the
         *      instant at which the operation initialized
         * @param endTime The {@code long} millisecond end time defining the
         *      instant at which the operation completed
         *
         * @return The {@code int} level of containment
         */
        private int findContainmentLevel(long startTime, long endTime)
        {
            if (startTime < this.holidayStartTimeMillis)
            {
                if (endTime >= this.holidayEndTimeMillis)
                {
                    return FULLY_CONTAINED;
                }
                else if (endTime > this.holidayStartTimeMillis && endTime < this.holidayEndTimeMillis)
                {
                    return END_INSIDE;
                }
            }
            else if (startTime > this.holidayStartTimeMillis && startTime <= this.holidayEndTimeMillis)
            {
                if (endTime > this.holidayEndTimeMillis)
                {
                    return START_INSIDE;
                }
                else if (endTime <= this.holidayEndTimeMillis)
                {
                    return BOTH_INSIDE;
                }
            }

            return NOT_CONTAINED;
        }

        private long getFullyContainedSubtractionDuration()
        {
            long operationalDurations;

            operationalDurations = 0l;
            for (OperationalInterval containedInterval : this.containedIntervals)
            {
                operationalDurations += containedInterval.intervalMillisDuration();
            }

            return operationalDurations;
        }

        /**
         * This method will be called rarely. Therefore some of the calculations
         * may seem redundant, but obsolete in principal.
         *
         * @param startTime The {@code long} milliseconds defining the original
         *      start time for the calculation
         *
         * @return The {@code long} number of milliseconds to be subtracted from
         *      the total calculated time difference, if any
         */
        private long getStartInsideSubtractionDuration(long startTime)
        {
            Calendar startTimeCal;

            startTimeCal = Calendar.getInstance();
            startTimeCal.setTimeInMillis(startTime);

            for (OperationalInterval containedInterval : this.containedIntervals)
            {
                switch (containedInterval.findInInterval(startTimeCal))
                {
                    case OperationalInterval.IN_CURRENT:
                        long subtractionDuration;

                        subtractionDuration = (1000l - startTimeCal.get(MILLISECOND));
                        startTimeCal.add(MILLISECOND, (1000 - startTimeCal.get(MILLISECOND)));
                        subtractionDuration += ((60l - startTimeCal.get(SECOND)) * MILLIS_PER_SECOND);
                        startTimeCal.add(SECOND, (60 - startTimeCal.get(SECOND)));
                        subtractionDuration += ((long)(containedInterval.endMinutes() - startTimeCal.get(MINUTE)) * MILLIS_PER_MINUTE);
                        startTimeCal.add(MINUTE, (containedInterval.endMinutes() - startTimeCal.get(MINUTE)));
                        subtractionDuration += ((long)(containedInterval.endHour() - startTimeCal.get(HOUR_OF_DAY)) * MILLIS_PER_HOUR);

                        return subtractionDuration;
                    case OperationalInterval.AFTER_PREVIOUS:
                        return containedInterval.intervalMillisDuration();
                    case OperationalInterval.BEFORE_NEXT:
                        if (this.containedIntervals.contains(containedInterval.nextActive()))
                        {
                            return containedInterval.nextActive().intervalMillisDuration();
                        } else return 0l;
                    case OperationalInterval.CANNOT_DETERMINE:
                        break;
                }
            }

            throw new RuntimeException("Something went wrong here...");
        }

        /**
         * This method will be called rarely. Therefore some of the calculations
         * may seem redundant, but obsolete in principal.
         *
         * @param endTime The {@code long} milliseconds defining the original
         *      end time for the calculation
         *
         * @return The {@code long} number of milliseconds to be subtracted from
         *      the total calculated time difference, if any
         */
        private long getEndInsideSubtractionDuration(long endTime)
        {
            Calendar endTimeCal;

            endTimeCal = Calendar.getInstance();
            endTimeCal.setTimeInMillis(endTime);

            for (OperationalInterval containedInterval : this.containedIntervals)
            {
                switch (containedInterval.findInInterval(endTimeCal))
                {
                    case OperationalInterval.IN_CURRENT:
                        long subtractionDuration;

                        subtractionDuration = endTimeCal.get(MILLISECOND);
                        subtractionDuration += ((long)endTimeCal.get(SECOND) * MILLIS_PER_SECOND);
                        subtractionDuration += ((long)(endTimeCal.get(MINUTE) - containedInterval.startMinutes()) * MILLIS_PER_MINUTE);
                        subtractionDuration += ((long)(endTimeCal.get(HOUR_OF_DAY) - containedInterval.startHour()) * MILLIS_PER_HOUR);

                        return subtractionDuration;
                    case OperationalInterval.AFTER_PREVIOUS:
                        if (this.containedIntervals.contains(containedInterval.previousActive()))
                        {
                            return containedInterval.previousActive().intervalMillisDuration();
                        } else return 0l;
                    case OperationalInterval.BEFORE_NEXT:
                        return containedInterval.intervalMillisDuration();
                    case OperationalInterval.CANNOT_DETERMINE:
                        break;
                }
            }

            throw new RuntimeException("Something went wrong here...");
        }

        @Override
        public int hashCode()
        {
            int hash = 3;

            hash = 79 * hash + (int) (this.holidayStartTimeMillis ^ (this.holidayStartTimeMillis >>> 32));
            hash = 79 * hash + (int) (this.holidayEndTimeMillis ^ (this.holidayEndTimeMillis >>> 32));

            return hash;
        }

        @Override
        @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
        public boolean equals(Object obj)
        {
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;

            final NonWorkDay other = (NonWorkDay) obj;
            if (this.holidayStartTimeMillis != other.holidayStartTimeMillis) return false;
            return this.holidayEndTimeMillis == other.holidayEndTimeMillis;
        }
    }

    private class OperationalIntervals implements Iterable<OperationalInterval>
    {
        private final List<OperationalInterval> intervals = new ArrayList<>();

        private OperationalIntervals() {}

        /**
         * This method is costly, since it does a re-sort on every addition, but
         * this action only occurs during the initial setup and therefore has no
         * influence on the runtime calculation performance.
         *
         * @param intervalDefinition The
         *      {@code T8OrganizationalIntervalDefinition} which specifies a
         *      single operation interval as a day of the week, start time and
         *      end time
         */
        private void addOperationalInterval(T8OrganizationOperationalIntervalDefinition intervalDefinition)
        {
            // Add the new interval
            this.intervals.add(new OperationalInterval(intervalDefinition));

            // Re-sort the intervals
            Collections.sort(this.intervals);

            // Rebuild the links between the intervals
            rebuildLinks();
        }

        /**
         * We make sure the the intervals are linked in as a roundabout to
         * prevent any additional calculation thereof later on.
         */
        private void rebuildLinks()
        {
            OperationalInterval interval;

            // We need to fix up the filler intervals first
            for (int intervalIdx = 0; intervalIdx < this.intervals.size(); intervalIdx++)
            {
                interval = this.intervals.get(intervalIdx);
                if (!interval.isActive())
                {
                    if (intervalIdx == 0) // First item
                    {
                        interval.setPrevious(this.intervals.get(this.intervals.size()-1));
                        interval.setNext(this.intervals.get(1));
                    }
                    else if (intervalIdx == (this.intervals.size()-1))
                    {
                        interval.setPrevious(this.intervals.get(this.intervals.size()-2));
                        interval.setNext(this.intervals.get(0));
                    }
                    else
                    {
                        interval.setPrevious(this.intervals.get(intervalIdx-1));
                        interval.setNext(this.intervals.get(intervalIdx+1));
                    }
                }
            }

            // We skip the first and last items in this loop as to avoid unnecessary checks
            for (int intervalIdx = 1; intervalIdx < this.intervals.size()-1;)
            {
                interval = this.intervals.get(intervalIdx);
                interval.setPrevious(this.intervals.get(intervalIdx-1));
                interval.setNext(this.intervals.get(++intervalIdx));
            }

            // First interval in the list
            interval = this.intervals.get(0);
            interval.setPrevious(this.intervals.get(this.intervals.size()-1));
            interval.setNext((this.intervals.size() > 1) ? this.intervals.get(1) : interval);

            // Last interval in the list
            interval = this.intervals.get(this.intervals.size()-1);
            interval.setPrevious((this.intervals.size() > 1) ? this.intervals.get(this.intervals.size()-2) : interval);
            interval.setNext(this.intervals.get(0));
        }

        /**
         * In the event that the operational intervals do not cover all the
         * weekdays, we fill in the blanks with inactive intervals to ensure we
         * can more easily find the correct intervals for specific times later
         * on.
         */
        private void fillIntervals()
        {
            boolean fillerAdded;
            int currentDow;
            int nextDow;

            fillerAdded = true;
            while (fillerAdded)
            {
                fillerAdded = false;
                for (OperationalInterval interval : this)
                {
                    currentDow = interval.dayOfWeek();
                    nextDow = interval.next().dayOfWeek();

                    // Next interval either needs to be on the same day, or the next to ensure there are no gaps
                    if (currentDow == SATURDAY)
                    {
                        if (nextDow != SATURDAY && nextDow != SUNDAY)
                        {
                            addFillerInterval(SUNDAY);
                            fillerAdded = true;
                        }
                    }
                    else if (nextDow != currentDow && nextDow != (currentDow+1))
                    {
                        addFillerInterval(interval.dayOfWeek()+1);
                        fillerAdded = true;
                    }

                    if (fillerAdded) break; // We need to break to prevent ConcurrentModificationException
                }
            }
        }

        private void addFillerInterval(int calDayOfWeek)
        {
            T8OrganizationOperationalIntervalDefinition intervalDefinition;

            intervalDefinition = new T8OrganizationOperationalIntervalDefinition("$"+T8IdentifierUtilities.createNewGUID());
            intervalDefinition.setActive(false);
            intervalDefinition.setOperatingDay(T8OrganizationOperationalSettingsDefinition.OperatingDay.fromCalendarDayOfWeek(calDayOfWeek));

            addOperationalInterval(intervalDefinition);
        }

        @Override
        public Iterator<OperationalInterval> iterator()
        {
            return this.intervals.iterator();
        }

        @Override
        public String toString()
        {
            StringBuilder toStringBuilder;

            toStringBuilder = new StringBuilder("OperationalIntervals{");
            for (OperationalInterval interval : this)
            {
                toStringBuilder.append("\n\n").append(interval).append(",");
            }
            toStringBuilder.deleteCharAt(toStringBuilder.lastIndexOf(","));
            toStringBuilder.append("}");

            return toStringBuilder.toString();
        }
    }

    /**
     * Defines a single operational interval with some redundant information.
     * All redundant information assists with calculations at a later point and
     * therefore increases performance at the cost of minimal memory.
     */
    private class OperationalInterval implements Comparable<OperationalInterval>
    {
        private static final int CANNOT_DETERMINE = -1;
        private static final int IN_CURRENT = 0;
        private static final int BEFORE_NEXT = 1;
        private static final int AFTER_PREVIOUS = 2;
        private static final int INACTIVE_INT = 3;

        private final long intervalMillisDuration;
        private final int intervalMinuteDuration;
        private final int startMinutes;
        private final int endMinutes;
        private final int dayOfWeek;
        private final int startHour;
        private final int endHour;

        private final boolean active;

        private OperationalInterval previousActive;
        private OperationalInterval nextActive;
        private OperationalInterval previous;
        private OperationalInterval next;

        private int currentToPreviousMinuteDifference;
        private int currentToNextMinuteDifference;

        private OperationalInterval(T8OrganizationOperationalIntervalDefinition definition)
        {
            this.dayOfWeek = definition.getOperatingDay().getCalendarDayOfWeek();
            this.startHour = definition.getOperatingHoursStart();
            this.startMinutes = definition.getOperatingMinutesStart();
            this.endHour = definition.getOperatingHoursEnd();
            this.endMinutes = definition.getOperatingMinutesEnd();
            this.intervalMinuteDuration = getMinuteDifference();
            this.intervalMillisDuration = (((long)this.intervalMinuteDuration) * MILLIS_PER_MINUTE);
            if (this.intervalMillisDuration <= 0) throw new IllegalStateException("The interval has been specified with incorrect start and end time parameters. Day {"+definition.getOperatingDay()+"} - Start {"+definition.getOperatingHoursStart()+"} - End {"+definition.getOperatingHoursEnd()+"}");

            this.active = definition.isActive();

            this.previous = null;
            this.next = null;

            this.currentToPreviousMinuteDifference = 0;
            this.currentToNextMinuteDifference = 0;
        }

        /**
         * The minute difference is calculated as the hour difference between
         * the end and start hours and multiplied by the number of minutes per
         * hour. The number of minutes after the end hour is then added and the
         * number of minutes after the start hour is subtracted. This gives the
         * total number of minutes defining the interval.
         *
         * @return The {@code int} number of minutes defining the interval
         */
        private int getMinuteDifference()
        {
            int minutes;

            minutes = ((this.endHour - this.startHour) * MINUTES_PER_HOUR);
            minutes += this.endMinutes;
            minutes -= this.startMinutes;

            return minutes;
        }

        @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
        private void setPrevious(OperationalInterval previous)
        {
            this.currentToPreviousMinuteDifference = 0;
            this.previousActive = previous;
            this.previous = previous;

            if (this.previous != null)
            {
                // We want to keep a reference of the previous active interval
                while (!this.previousActive.active)
                {
                    this.previousActive = this.previousActive.previous;
                }

                // If the current interval isn't active, we don't care about setting these values
                if (!this.active) return;

                if (this.dayOfWeek == this.previousActive.dayOfWeek)
                {
                    this.currentToPreviousMinuteDifference = ((0-(this.startHour - this.previousActive.endHour)) * MINUTES_PER_HOUR);
                    this.currentToPreviousMinuteDifference -= this.startMinutes; // Added as a negative
                    this.currentToPreviousMinuteDifference += this.previousActive.endMinutes; // Subtracted as a negative
                }
                else
                {
                    Calendar differenceCalculator;

                    differenceCalculator = Calendar.getInstance();
                    differenceCalculator.set(DAY_OF_WEEK, this.dayOfWeek);

                    // We take everything between midnight and the start hour as the difference
                    this.currentToPreviousMinuteDifference = ((0-this.startHour) * MINUTES_PER_HOUR);
                    this.currentToPreviousMinuteDifference -= this.startMinutes; // Added as a negative
                    differenceCalculator.add(DAY_OF_WEEK, -1);

                    while (differenceCalculator.get(DAY_OF_WEEK) != this.previousActive.dayOfWeek)
                    {
                        this.currentToPreviousMinuteDifference -= MINUTES_PER_DAY; // Added as a negative
                        differenceCalculator.add(DAY_OF_WEEK, -1);
                    }

                    this.currentToPreviousMinuteDifference -= ((HOURS_PER_DAY - this.previousActive.endHour) * MINUTES_PER_HOUR); // Added as a negative
                    this.currentToPreviousMinuteDifference += this.previousActive.endMinutes; // Subtracted as a negative
                }
            } else throw new NullPointerException("Previous interval cannot be null");
        }

        @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
        private void setNext(OperationalInterval next)
        {
            this.currentToNextMinuteDifference = 0;
            this.nextActive = next;
            this.next = next;

            if (this.next != null)
            {
                // We want to keep a reference of the next active interval
                while (!this.nextActive.active)
                {
                    this.nextActive = this.nextActive.next;
                }

                // If the current interval isn't active, we don't care about setting these values
                if (!this.active) return;

                if (this.dayOfWeek == this.nextActive.dayOfWeek)
                {
                    this.currentToNextMinuteDifference = ((this.nextActive.startHour - this.endHour) * MINUTES_PER_HOUR);
                    this.currentToNextMinuteDifference += this.nextActive.startMinutes;
                    this.currentToNextMinuteDifference -= this.endMinutes;
                }
                else
                {
                    Calendar differenceCalculator;

                    differenceCalculator = Calendar.getInstance();
                    differenceCalculator.set(DAY_OF_WEEK, this.dayOfWeek);

                    // We take everything between midnight and the start hour as the difference
                    this.currentToNextMinuteDifference = ((HOURS_PER_DAY - this.endHour) * MINUTES_PER_HOUR);
                    this.currentToNextMinuteDifference -= this.endMinutes;
                    differenceCalculator.add(DAY_OF_WEEK, 1);

                    while (differenceCalculator.get(DAY_OF_WEEK) != this.nextActive.dayOfWeek)
                    {
                        this.currentToNextMinuteDifference += MINUTES_PER_DAY;
                        differenceCalculator.add(DAY_OF_WEEK, 1);
                    }

                    this.currentToNextMinuteDifference += (this.nextActive.startHour * MINUTES_PER_HOUR);
                    this.currentToNextMinuteDifference += this.nextActive.startMinutes;
                }
            } else throw new NullPointerException("Next interval cannot be null");
        }

        private boolean isActive()
        {
            return this.active;
        }

        /**
         * Returns the number of minutes between the current interval and the
         * previous <b>active</b> interval. Since this is a backwards view, the
         * value is expected to be negative.
         *
         * @return The {@code int} number of minutes between the current and
         *      previously active interval
         */
        private int currentToPreviousMinuteDifference()
        {
            return this.currentToPreviousMinuteDifference;
        }

        /**
         * Returns the number of minutes between the current interval and the
         * next <b>active</b> interval. This is a forward view and the values
         * is expected to be positive.
         *
         * @return The {@code int} number of minutes between the current and
         *      next active interval
         */
        private int currentToNextMinuteDifference()
        {
            return this.currentToNextMinuteDifference;
        }

        private long intervalMillisDuration()
        {
            return this.intervalMillisDuration;
        }

        private int intervalMinuteDuration()
        {
            return this.intervalMinuteDuration;
        }

        private int endHour()
        {
            return this.endHour;
        }

        private int endMinutes()
        {
            return this.endMinutes;
        }

        private int startHour()
        {
            return this.startHour;
        }

        private int startMinutes()
        {
            return this.startMinutes;
        }

        private int dayOfWeek()
        {
            return this.dayOfWeek;
        }

        private OperationalInterval previousActive()
        {
            return this.previousActive;
        }

        private OperationalInterval nextActive()
        {
            return this.nextActive;
        }

        private OperationalInterval next()
        {
            return this.next;
        }

        /**
         * Finds exactly where the specified date time value falls within the
         * current interval. Both the leading and trailing segments of the
         * interval are checked as to determine whether or not the date time
         * value falls between the current and the next/previous intervals.<br/>
         * <br/>
         * Potential Values:<br/>
         * <blockquote>
         *      {@link #IN_CURRENT}<br/>
         *      {@link #AFTER_PREVIOUS}<br/>
         *      {@link #BEFORE_NEXT}<br/>
         *      {@link #CANNOT_DETERMINE}
         * </blockquote><br/>
         * <br/>
         * The ordering of checks done in this method is important. In the wrong
         * order, the incorrect results may be found, or no results at all.
         *
         * @param dateTime The {@code Calendar} defining the date time value for
         *      which the interval needs to be determined
         *
         * @return The {@code int} value defining where the date time value
         *      falls in relation to the interval
         */
        @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
        private int findInInterval(Calendar dateTime)
        {
            int instantMinuteOfHour;
            int instantDayOfWeek;
            int instantHourOfDay;

            instantMinuteOfHour = dateTime.get(MINUTE);
            if ((instantDayOfWeek = dateTime.get(DAY_OF_WEEK)) == this.dayOfWeek)
            {
                if (!this.active)
                {
                    return INACTIVE_INT;
                }
                else
                {
                    // We know the current instance is active, so we first check if the time falls within it
                    if ((instantHourOfDay = dateTime.get(HOUR_OF_DAY)) >= this.startHour)
                    {
                        // If we are working with the starting hour, we need to check the minutes
                        if (instantHourOfDay == this.startHour)
                        {
                            if (instantMinuteOfHour >= this.startMinutes)
                            {
                                return IN_CURRENT;
                            } else return AFTER_PREVIOUS;
                        }
                        // If the end hour is greater than the instant
                        else if (instantHourOfDay < this.endHour)
                        {
                            return IN_CURRENT;
                        }
                        else if (instantHourOfDay == this.endHour)
                        {
                            // We cater for the extreme case (e.g. end hour = 14 && end time = 14:00:00.001 -> does not fall within the interval, but end time = 14:00:00.000 does)
                            if (dateTime.get(MILLISECOND) > 0 || dateTime.get(SECOND) > 0 || dateTime.get(MINUTE) > this.endMinutes)
                            {
                                return BEFORE_NEXT;
                            } else return IN_CURRENT;
                        }
                        else if (instantDayOfWeek != this.next.dayOfWeek) // > this.endHour
                        {
                            return BEFORE_NEXT;
                        } // CANNOT_DETERMINE
                    }
                    // If it's the current DOW, and the hour is before the interval start
                    else if (instantDayOfWeek == this.dayOfWeek && instantHourOfDay < this.startHour)
                    {
                        return AFTER_PREVIOUS;
                    }
                    // Now we check if the time falls between previous and current intervals
                    else if (instantDayOfWeek == this.previous.dayOfWeek)
                    {
                        if (instantHourOfDay > this.previous.endHour && instantMinuteOfHour > this.previous.endMinutes)
                        {
                            return AFTER_PREVIOUS;
                        } // CANNOT_DETERMINE
                    }
                    // Now we check if the time falls between next and current intervals
                    else if (instantDayOfWeek == this.next.dayOfWeek)
                    {
                        if (instantHourOfDay < this.next.startHour)
                        {
                            return BEFORE_NEXT;
                        }
                        else if (instantHourOfDay == this.next.startHour && instantMinuteOfHour < this.next.startMinutes)
                        {
                            return BEFORE_NEXT;
                        } // CANNOT_DETERMINE
                    }
                }
            }

            return CANNOT_DETERMINE;
        }

        private String toShortString()
        {
            StringBuilder toStringBuilder;

            toStringBuilder = new StringBuilder("[");
            toStringBuilder.append("DayOfWeek:").append(this.dayOfWeek);
            toStringBuilder.append(",Active:").append(this.active);
            toStringBuilder.append(",StartHour:").append(this.startHour);
            toStringBuilder.append(",EndHour:").append(this.endHour);
            toStringBuilder.append("]");

            return toStringBuilder.toString();
        }

        @Override
        public String toString()
        {
            StringBuilder toStringBuilder;

            toStringBuilder = new StringBuilder("OperationalInterval {");
            toStringBuilder.append("\nDayOfWeek:").append(this.dayOfWeek);
            toStringBuilder.append(",\nActive:").append(this.active);
            toStringBuilder.append(",\nStartHour:").append(this.startHour);
            toStringBuilder.append(",\nEndHour:").append(this.endHour);
            toStringBuilder.append(",\nIntervalDuration:").append(this.intervalMillisDuration);
            toStringBuilder.append(",\nPrevious:").append((this.previous != null ? this.previous.toShortString() : "null")); // Endless loop if we use the regular toString method
            toStringBuilder.append(",\nCurrentToPreviousMinute:").append(this.currentToPreviousMinuteDifference);
            toStringBuilder.append(",\nNext:").append((this.next != null ? this.next.toShortString() : "null"));
            toStringBuilder.append(",\nCurrentToNextMinute:").append(this.currentToNextMinuteDifference);
            toStringBuilder.append("\n}");

            return toStringBuilder.toString();
        }

        @Override
        @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
        public int compareTo(OperationalInterval other)
        {
            if (this.dayOfWeek < other.dayOfWeek) return -1;
            if (this.dayOfWeek > other.dayOfWeek) return 1;
            // Here the day of week is equal
            if (this.endHour < other.startHour) return -1;
            if (this.startHour > other.endHour) return 1;
            // Here we have reached some form of overlap. This is not allowed
            throw new IllegalStateException("Overlapping or contiguous intervals found. Intervals: 1 - {"+this+"} 2 - {"+other+"}");
        }
    }

    private abstract class CalculatorInstant
    {
        private static final int F_NOT_AFTER = 0;
        private static final int T_NO_CONTAINMENT = 1;
        private static final int T_WITH_CONTAINMENT = 2;

        protected final Calendar currentTime;
        protected final long originalTime;

        protected OperationalInterval currentInterval;
        protected long accumulated;

        private CalculatorInstant(long originalTime)
        {
            this.originalTime = originalTime;
            this.currentTime = Calendar.getInstance();
            this.currentTime.setTimeInMillis(originalTime);
            this.accumulated = 0l;
        }

        protected abstract boolean findInInterval(OperationalInterval operationalInterval);

        protected abstract boolean shouldShift(CalculatorInstant other);

        protected abstract void shift();

        protected boolean isSameDayInterval(CalculatorInstant other)
        {
            if (this.currentInterval == other.currentInterval)
            {
                if (this.currentTime.get(DAY_OF_YEAR) == other.currentTime.get(DAY_OF_YEAR)) // More likely to be different than the year
                {
                    if (this.currentTime.get(YEAR) == other.currentTime.get(YEAR))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private long accumulated()
        {
            return this.accumulated;
        }

        private OperationalInterval currentInterval()
        {
            return this.currentInterval;
        }

        private long originalTime()
        {
            return this.originalTime;
        }

        private long currentTime()
        {
            return this.currentTime.getTimeInMillis();
        }

        private Calendar currentTimeCal()
        {
            return this.currentTime;
        }

        private long getDifference(CalculatorInstant other)
        {
            if (this.currentInterval == other.currentInterval)
            {
                return ((this.accumulated + other.accumulated) + (other.currentTime.getTimeInMillis() - this.currentTime.getTimeInMillis()));
            } else throw new IllegalStateException("Cannot request the difference when intervals are not the same.");
        }

        private int after(CalculatorInstant other)
        {
            // It should always be the case that the 2 will still follow on one another
            if (this.currentInterval == other.currentInterval.nextActive())
            {
                long absoluteDifference;

                // Calculate the absolute difference between the 2
                absoluteDifference = (other.originalTime - this.originalTime);
                if ((this.currentTime.getTimeInMillis() - other.currentTime.getTimeInMillis()) > absoluteDifference)
                {
                    // The gap between the instances is greater than the actual difference
                    return T_NO_CONTAINMENT;
                } else return T_WITH_CONTAINMENT; // Either one or both fall within an interval and therefore there is a marginal time to calculate
            }

            return F_NOT_AFTER;
        }

        @Override
        public String toString()
        {
            StringBuilder toStringBuilder;

            toStringBuilder = new StringBuilder("CalculatorInstant {");
            toStringBuilder.append("\nOriginalTime:").append(originalTime);
            toStringBuilder.append(",\nAccumulated:").append(this.accumulated);
            toStringBuilder.append(",\nCurrentTime:").append(this.currentTime);
            toStringBuilder.append(",\nCurrentTimeDate:").append(this.currentTime.getTime().toString());
            toStringBuilder.append(",\nCurrentInterval:").append(this.currentInterval);
            toStringBuilder.append("\n}");

            return toStringBuilder.toString();
        }
    }

    private class CalculatorStartInstant extends CalculatorInstant
    {
        private CalculatorStartInstant(long originalTime)
        {
            super(originalTime);
        }

        @Override
        protected boolean findInInterval(OperationalInterval operationalInterval)
        {
            switch (operationalInterval.findInInterval(currentTime))
            {
                case OperationalInterval.IN_CURRENT:
                    this.currentInterval = operationalInterval;
                    fill();
                    setToCurrent();
                    return true;
                case OperationalInterval.BEFORE_NEXT:
                case OperationalInterval.INACTIVE_INT:
                    this.currentInterval = operationalInterval.nextActive();
                    setToCurrent();
                    return true;
                case OperationalInterval.AFTER_PREVIOUS:
                    this.currentInterval = operationalInterval;
                    setToCurrent();
                    return true;
            }

            return false;
        }

        @Override
        protected boolean shouldShift(CalculatorInstant other)
        {
            return true;
        }

        @Override
        protected void shift()
        {
            this.accumulated += this.currentInterval.intervalMillisDuration();

            this.currentTime.add(MINUTE, this.currentInterval.intervalMinuteDuration());
            this.currentTime.add(MINUTE, this.currentInterval.currentToNextMinuteDifference());

            this.currentInterval = this.currentInterval.nextActive();
        }

        private void fill()
        {
            this.accumulated -= ((long)(this.currentTime.get(HOUR_OF_DAY) - this.currentInterval.startHour()) * MILLIS_PER_HOUR);
            this.accumulated -= ((long)(this.currentTime.get(MINUTE) - this.currentInterval.startMinutes()) * MILLIS_PER_MINUTE);
            this.accumulated -= ((long)this.currentTime.get(SECOND) * MILLIS_PER_SECOND);
            this.accumulated -= this.currentTime.get(MILLISECOND);
        }

        private void setToCurrent()
        {
            while (this.currentTime.get(DAY_OF_WEEK) != this.currentInterval.dayOfWeek())
            {
                this.currentTime.add(DAY_OF_WEEK, 1);
            }
            this.currentTime.set(HOUR_OF_DAY, this.currentInterval.startHour());
            this.currentTime.set(MINUTE, this.currentInterval.startMinutes());
            this.currentTime.set(SECOND, 0);
            this.currentTime.set(MILLISECOND, 0);
        }
    }

    private class CalculatorEndInstant extends CalculatorInstant
    {
        private CalculatorEndInstant(long originalTime)
        {
            super(originalTime);
        }

        @Override
        protected boolean findInInterval(OperationalInterval operationalInterval)
        {
            switch (operationalInterval.findInInterval(currentTime))
            {
                case OperationalInterval.IN_CURRENT:
                    this.currentInterval = operationalInterval;
                    fill();
                    return true;
                case OperationalInterval.BEFORE_NEXT:
                    this.currentInterval = operationalInterval;
                    setToCurrent();
                    return true;
                case OperationalInterval.AFTER_PREVIOUS:
                case OperationalInterval.INACTIVE_INT:
                    this.currentInterval = operationalInterval.previousActive();
                    setToCurrent();
                    return true;
            }

            return false;
        }

        @Override
        protected boolean shouldShift(CalculatorInstant other)
        {
            return (this.currentInterval.previousActive() != other.currentInterval && !isSameDayInterval(other));
        }

        @Override
        protected void shift()
        {
            this.accumulated += this.currentInterval.intervalMillisDuration();

            this.currentTime.add(MINUTE, (0-this.currentInterval.intervalMinuteDuration()));
            this.currentTime.add(MINUTE, this.currentInterval.currentToPreviousMinuteDifference());

            this.currentInterval = this.currentInterval.previousActive();
        }

        private void fill()
        {
            this.accumulated -= (1000l - this.currentTime.get(MILLISECOND));
            this.currentTime.add(MILLISECOND, (1000 - this.currentTime.get(MILLISECOND)));
            this.accumulated -= ((60l - this.currentTime.get(SECOND)) * MILLIS_PER_SECOND);
            this.currentTime.add(SECOND, (60 - this.currentTime.get(SECOND)));
            this.accumulated -= ((long)(this.currentInterval.endMinutes() - this.currentTime.get(MINUTE)) * MILLIS_PER_MINUTE);
            this.currentTime.add(MINUTE, (this.currentInterval.endMinutes() - this.currentTime.get(MINUTE)));
            this.accumulated -= ((long)(this.currentInterval.endHour() - this.currentTime.get(HOUR_OF_DAY)) * MILLIS_PER_HOUR);
            this.currentTime.add(HOUR_OF_DAY, (this.currentInterval.endHour() - this.currentTime.get(HOUR_OF_DAY)));
        }

        private void setToCurrent()
        {
            while (this.currentTime.get(DAY_OF_WEEK) != this.currentInterval.dayOfWeek())
            {
                this.currentTime.add(DAY_OF_WEEK, -1);
            }
            this.currentTime.set(HOUR_OF_DAY, this.currentInterval.endHour());
            this.currentTime.set(MINUTE, this.currentInterval.endMinutes());
            this.currentTime.set(SECOND, 0);
            this.currentTime.set(MILLISECOND, 0);
        }
    }
}