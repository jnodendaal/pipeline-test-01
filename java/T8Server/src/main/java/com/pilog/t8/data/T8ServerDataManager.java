package com.pilog.t8.data;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.cache.T8DataRecordCache;
import com.pilog.t8.cache.T8DataRequirementCache;
import com.pilog.t8.cache.T8DataRequirementInstanceCache;
import com.pilog.t8.cache.T8TerminologyCache;
import com.pilog.t8.data.entity.T8DataEntityValidationError;
import com.pilog.t8.data.entity.T8DataEntityValidationReport;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.data.cache.T8InfinispanDataRequirementCache;
import com.pilog.t8.data.cache.T8InfinispanTerminologyCache;
import com.pilog.t8.definition.data.cache.T8DataCacheDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.key.T8DataKeyGeneratorDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.script.validation.entity.T8EntityValidationScriptDefinition;
import com.pilog.t8.definition.script.validation.entity.T8ServerEntityValidationScriptDefinition;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.transaction.LockingMode;
import org.infinispan.transaction.TransactionMode;
import org.infinispan.util.concurrent.IsolationLevel;
import com.pilog.t8.data.cache.T8InfinispanDataRecordCache;
import com.pilog.t8.data.cache.T8InfinispanDataRequirementInstanceCache;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.security.T8Context;
import org.infinispan.Cache;

/**
 * Cache notes:
 * - Locking Mode:
 *      - If two overlapping transactions update the same keys, optimistic caches will fail to prepare one of the
 *      transactions and instead they will throw a WriteSkewException.
 *      - If the write skew check is disabled, the last write will always win.  This means conditional operations
 *      are not reliable, e.g. two overlapping transactions trying to increment a counter with replace(key, counter, counter + 1)
 *      could both succeed (incrementing the counter by 1 instead of 2).
 *      - In a pessimistic cache, one of the overlapping transactions would block until the other was finished. But this requires
 *      all transactions to lock their keys in the same order, otherwise the write/lock operation will time out with a generic TimeoutException.
 *      If deadlock detection is enabled in the configuration, the application will instead get a DeadlockDetectedException after
 *      a (usually much shorter) spin duration.
 * @author Bouwer du Preez
 */
public class T8ServerDataManager implements T8DataManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerDataManager.class);

    private final T8Context internalContext;
    private final Map<String, T8DataSession> openSessions;
    private final Map<String, T8DataConnectionPool> openConnectionPools;
    private final Map<String, T8DataKeyGenerator> dataKeyGenerators;
    private final Map<String, T8DataCache> dataCaches;
    private final Map<Thread, T8DataSession> threadSessions;
    private ScheduledExecutorService executor; // Timer used to run maintenance jobs.
    private SessionMaintenanceThread maintenanceThread;
    private DefaultCacheManager cacheManager;
    private int terminologyCacheSize;
    private int dataRequirementInstanceCacheSize;
    private int dataRequirementCacheSize;
    private int dataRecordCacheSize;

    private static final int TERMINOLOGY_CACHE_SIZE = 100_000;
    private static final int DATA_REQUIREMENT_INSTANCE_CACHE_SIZE = 1_000;
    private static final int DATA_REQUIREMENT_CACHE_SIZE = 1_000;
    private static final int DATA_RECORD_CACHE_SIZE = 1_000;
    private static final long MAINTENANCE_INTERVAL = 5*60*1000; // The interval in milliseconds at which the maintenance task is run.
    private static final long SESSION_EXPIRATION_TIME = 5*60*1000; // The number of miliseconds for which a session will be maintained after the last recorded activity.
    private static final long INSTANT_TRANSACTION_EXPIRATION_TIME = 5*60*1000; // The number of miliseconds for which an instant transaction will be maintained after the last recorded activity.

    public T8ServerDataManager(T8ServerContext serverContext)
    {
        this.internalContext = serverContext.getSecurityManager().createServerModuleContext(T8ServerDataManager.this);
        this.openSessions = Collections.synchronizedMap(new HashMap<String, T8DataSession>());
        this.openConnectionPools = Collections.synchronizedMap(new HashMap<String, T8DataConnectionPool>());
        this.dataKeyGenerators = Collections.synchronizedMap(new HashMap<String, T8DataKeyGenerator>());
        this.dataCaches = Collections.synchronizedMap(new HashMap<String, T8DataCache>());
        this.threadSessions = Collections.synchronizedMap(new HashMap<Thread, T8DataSession>());
        this.terminologyCacheSize = TERMINOLOGY_CACHE_SIZE;
        this.dataRequirementInstanceCacheSize = DATA_REQUIREMENT_INSTANCE_CACHE_SIZE;
        this.dataRequirementCacheSize = DATA_REQUIREMENT_CACHE_SIZE;
        this.dataRecordCacheSize = DATA_RECORD_CACHE_SIZE;
    }

    @Override
    public void init() throws Exception
    {
        GlobalConfiguration globalConfiguration;

        // Construct a local cache manager.
        globalConfiguration = new GlobalConfigurationBuilder().globalJmxStatistics().allowDuplicateDomains(true).build();
        cacheManager = new DefaultCacheManager(globalConfiguration);

        // Start the maintenance timer.
        executor = Executors.newSingleThreadScheduledExecutor(new NameableThreadFactory("T8ServerDataManager-Maintenance"));
        maintenanceThread = new SessionMaintenanceThread(internalContext, this, openSessions, SESSION_EXPIRATION_TIME, INSTANT_TRANSACTION_EXPIRATION_TIME);
        executor.scheduleWithFixedDelay(maintenanceThread, MAINTENANCE_INTERVAL, MAINTENANCE_INTERVAL, TimeUnit.MILLISECONDS);
    }

    @Override
    public void start() throws Exception
    {
        List<T8Definition> cacheDefinitions;
        T8SystemDefinition systemDefinition;
        T8DefinitionManager definitionManager;

        // Set the cache configuration.
        definitionManager = internalContext.getServerContext().getDefinitionManager();
        systemDefinition = definitionManager.getSystemDefinition(internalContext);
        terminologyCacheSize = systemDefinition.getTerminologyCacheSize();
        dataRequirementCacheSize = systemDefinition.getDataRequirementCacheSize();

        // Initialize custom cache types.
        cacheDefinitions = definitionManager.getInitializedGroupDefinitions(internalContext, null, T8DataCacheDefinition.GROUP_IDENTIFIER, null);
        for (T8Definition definition : cacheDefinitions)
        {
            T8DataCacheDefinition cacheDefinition;
            T8DataCache dataCache;

            cacheDefinition = (T8DataCacheDefinition)definition;
            dataCache = cacheDefinition.createNewDataCacheInstance(internalContext);
            dataCaches.put(definition.getIdentifier(), dataCache);

            // Initialize the data cache if needed.
            if (!cacheDefinition.isLazyInitialization())
            {
                try
                {
                    dataCache.init();
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while initializing data cache: " + cacheDefinition, e);
                }
            }
        }
    }

    @Override
    public void destroy()
    {
        // Terminate the maintenance thread and all scheduled tasks.
        try
        {
            executor.shutdown();
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) throw new Exception("Data maintenance thread executor could not by terminated.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while awaiting termination of data maintenance thread.", e);
        }

        // Destroy maintenance thread.
        maintenanceThread.destroy();
        maintenanceThread = null;

        // Close all open sessions.
        for (T8DataSession session : openSessions.values())
        {
            session.closeSession();
        }

        // Clear closed sessions collection.
        openSessions.clear();

        // Close all open connection pools.
        for (T8DataConnectionPool connectionPool : openConnectionPools.values())
        {
            connectionPool.destroy();
        }

        // Clear closed connection pools collection.
        openConnectionPools.clear();

        // Clear all data caches.
        for (T8DataCache dataCache : dataCaches.values())
        {
            dataCache.destroy();
        }

        // Clear data caches collection.
        dataCaches.clear();

        // Destroy all data key generators.
        for (T8DataKeyGenerator dataKeyGenerator : dataKeyGenerators.values())
        {
            dataKeyGenerator.destroy();
        }

        // Clear the data key generators collection.
        dataKeyGenerators.clear();

        // Clear caches.
        cacheManager.stop();
    }

    @Override
    public void reset() throws Exception
    {
        // Close all open sessions.
        for (T8DataSession session : openSessions.values())
        {
            session.closeSession();
        }

        // Clear closed sessions collection.
        openSessions.clear();

        // Close all open connection pools.
        for (T8DataConnectionPool connectionPool : openConnectionPools.values())
        {
            connectionPool.destroy();
        }

        // Clear closed connection pools collection.
        openConnectionPools.clear();

        // Clear all data caches.
        for (T8DataCache dataCache : dataCaches.values())
        {
            dataCache.destroy();
        }

        // Clear data caches collection.
        dataCaches.clear();

        // Destroy all data key generators.
        for (T8DataKeyGenerator dataKeyGenerator : dataKeyGenerators.values())
        {
            dataKeyGenerator.destroy();
        }

        // Clear the data key generators collection.
        dataKeyGenerators.clear();
    }

    @Override
    public List<T8DataSessionDetails> getDataSessionDetails(T8Context context)
    {
        List<T8DataSessionDetails> sessionDetailsList;
        List<T8DataSession> dataSessions;

        dataSessions = new ArrayList<>(openSessions.values());
        sessionDetailsList = new ArrayList<>();
        for (T8DataSession dataSession : dataSessions)
        {
            sessionDetailsList.add(dataSession.getDetails());
        }

        return sessionDetailsList;
    }

    @Override
    public void killDataSession(String dataSessionId)
    {
        if(openSessions.containsKey(dataSessionId))
        {
            openSessions.get(dataSessionId).closeSession();
            openSessions.remove(dataSessionId);
        }
    }

    @Override
    public T8DataSession openDataSession(T8Context context)
    {
        if (context != null)
        {
            T8DataSession existingSession;
            Thread currentThread;

            currentThread = Thread.currentThread();
            existingSession = threadSessions.get(currentThread);
            if (existingSession == null)
            {
                try
                {
                    T8ServerDataSession newDataSession;

                    // Create the new data session and add it to the collection of open sessions.
                    newDataSession = new T8ServerDataSession(context, T8IdentifierUtilities.createNewGUID());
                    openSessions.put(newDataSession.getId(), newDataSession);
                    threadSessions.put(currentThread, newDataSession);
                    return newDataSession;
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while creating data session for thread: " + currentThread, e);
                }
            }
            else throw new RuntimeException("Attempt to create a new data session from a thread to which an existing session is already associated.  Existing Session: " + existingSession);
        }
        else throw new RuntimeException("Null access context cannot be used to obtain the current data session.");
    }

    @Override
    public T8DataSession getCurrentSession()
    {
        T8DataSession existingSession;
        Thread currentThread;

        currentThread = Thread.currentThread();
        existingSession = threadSessions.get(currentThread);
        if (existingSession == null)
        {
            // The use of this method has changed, and as such the creation of a new session is no longer permitted.
            // In order to allow backwards compatibility until all incorrect uses of this method have been resolved,
            // the implementation remains but a warning will be printed.
            LOGGER.log("WARNING: Attempt to get current data session within a context where none exists.  Thread: " + currentThread);
            // The next line is a hack to allow printing of invoking stack.
            new Exception("WARNING: Attempt to get current data session within a context where none exists.  Thread: " + currentThread).printStackTrace();

            try
            {
                T8ServerDataSession newDataSession;

                // Create the new data session and add it to the collection of open sessions.
                newDataSession = new T8ServerDataSession(internalContext, T8IdentifierUtilities.createNewGUID());
                openSessions.put(newDataSession.getId(), newDataSession);
                threadSessions.put(currentThread, newDataSession);
                return newDataSession;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while creating data session for thread: " + currentThread, e);
            }
        }
        else return existingSession;
    }

    @Override
    public void closeCurrentSession()
    {
        T8DataSession sessionToClose;
        Thread currentThread;

        currentThread = Thread.currentThread();
        sessionToClose = threadSessions.remove(currentThread);
        if (sessionToClose != null)
        {
            // Close the data session.
            if (sessionToClose.isActive()) LOGGER.log("WARNING: Closing data session while still active: " + sessionToClose);
            closeSession(sessionToClose.getId());
        }
        else throw new RuntimeException("No session to close for invoking thread: " + currentThread);
    }

    @Override
    public Map<String, Integer> getCacheSizes()
    {
        Map<String, Integer> sizeMap;

        sizeMap = new HashMap<>();
        for (String cacheName : cacheManager.getCacheNames())
        {
            sizeMap.put(cacheName, cacheManager.getCache(cacheName).size());
        }

        return sizeMap;
    }

    @Override
    public void clearCache(String cacheName)
    {
        Cache cache;

        cache = cacheManager.getCache(cacheName);
        if (cache != null) cache.clear();
        else throw new IllegalArgumentException("Cache not found: " + cacheName);
    }

    @Override
    public T8TerminologyCache getTerminologyCache(String organizationID)
    {
        try
        {
            String cacheName;

            // Create the cache name.
            cacheName = "TERMINOLOGY:" + organizationID;

            // Make sure not to create the cache if it already exists.
            if (!cacheManager.cacheExists(cacheName))
            {
                // Configure terminology cache.
                cacheManager.defineConfiguration(cacheName, new ConfigurationBuilder()
                    .clustering()
                        .cacheMode(CacheMode.LOCAL)
                    .eviction()
                        .strategy(EvictionStrategy.REMOVE)
                        .size(terminologyCacheSize)
                    .transaction()
                        .transactionMode(TransactionMode.TRANSACTIONAL)
                        .autoCommit(true) // Setting this to true is important as it will allow any mutation outside of a transaction to work as auto-commit.
                        .lockingMode(LockingMode.OPTIMISTIC) // See cache notes in class javadoc.
                        .locking()
                            .isolationLevel(IsolationLevel.READ_COMMITTED)
                            .useLockStriping(false)
                            .concurrencyLevel(10000)
                    .build());
            }

            // Get the cache and return it.
            return new T8InfinispanTerminologyCache(cacheManager.getCache(cacheName));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while creating terminology cache for organization: " + organizationID, e);
        }
    }

    @Override
    public T8DataRecordCache getDataRecordCache(String orgId)
    {
        try
        {
            String cacheName;

            // Create the cache name.
            cacheName = "DATA_RECORD:" + orgId;

            // Make sure not to create the cache if it already exists.
            if (!cacheManager.cacheExists(cacheName))
            {
                // Configure terminology cache.
                cacheManager.defineConfiguration(cacheName, new ConfigurationBuilder()
                    .clustering()
                        .cacheMode(CacheMode.LOCAL)
                    .eviction()
                        .strategy(EvictionStrategy.REMOVE)
                        .size(dataRecordCacheSize)
                    .transaction()
                        .transactionMode(TransactionMode.TRANSACTIONAL)
                        .autoCommit(true) // Setting this to true is important as it will allow any mutation outside of a transaction to work as auto-commit.
                        .lockingMode(LockingMode.OPTIMISTIC) // See lock mode description in class notes.
                        .locking()
                            .isolationLevel(IsolationLevel.READ_COMMITTED)
                            .useLockStriping(false)
                            .concurrencyLevel(10000)
                    .build());
            }
            // Get the cache and return it.
            return new T8InfinispanDataRecordCache(cacheManager.getCache(cacheName));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while creating data record cache for organization: " + orgId, e);
        }
    }

    @Override
    public T8DataRequirementInstanceCache getDataRequirementInstanceCache(String orgId)
    {
        try
        {
            String cacheName;

            // Create the cache name.
            cacheName = "DATA_REQUIREMENT_INSTANCE:" + orgId;

            // Make sure not to create the cache if it already exists.
            if (!cacheManager.cacheExists(cacheName))
            {
                // Configure terminology cache.
                cacheManager.defineConfiguration(cacheName, new ConfigurationBuilder()
                    .clustering()
                        .cacheMode(CacheMode.LOCAL)
                    .eviction()
                        .strategy(EvictionStrategy.REMOVE)
                        .size(dataRequirementInstanceCacheSize)
                    .transaction()
                        .transactionMode(TransactionMode.TRANSACTIONAL)
                        .autoCommit(true) // Setting this to true is important as it will allow any mutation outside of a transaction to work as auto-commit.
                        .lockingMode(LockingMode.OPTIMISTIC) // See lock mode description in class notes.
                        .locking()
                            .isolationLevel(IsolationLevel.READ_COMMITTED)
                            .useLockStriping(false)
                            .concurrencyLevel(10000)
                    .build());
            }
            // Get the cache and return it.
            return new T8InfinispanDataRequirementInstanceCache(cacheManager.getCache(cacheName));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while creating data requirement instance cache for organization: " + orgId, e);
        }
    }

    @Override
    public T8DataRequirementCache getDataRequirementCache(String orgId)
    {
        try
        {
            String cacheName;

            // Create the cache name.
            cacheName = "DATA_REQUIREMENT:" + orgId;

            // Make sure not to create the cache if it already exists.
            if (!cacheManager.cacheExists(cacheName))
            {
                // Configure terminology cache.
                cacheManager.defineConfiguration(cacheName, new ConfigurationBuilder()
                    .clustering()
                        .cacheMode(CacheMode.LOCAL)
                    .eviction()
                        .strategy(EvictionStrategy.REMOVE)
                        .size(dataRequirementCacheSize)
                    .transaction()
                        .transactionMode(TransactionMode.TRANSACTIONAL)
                        .autoCommit(true) // Setting this to true is important as it will allow any mutation outside of a transaction to work as auto-commit.
                        .lockingMode(LockingMode.OPTIMISTIC) // See lock mode description in class notes.
                        .locking()
                            .isolationLevel(IsolationLevel.READ_COMMITTED)
                            .useLockStriping(false)
                            .concurrencyLevel(10000)
                    .build());
            }
            // Get the cache and return it.
            return new T8InfinispanDataRequirementCache(cacheManager.getCache(cacheName));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while creating data requirement cache for organization: " + orgId, e);
        }
    }

    @Override
    public List<T8DataEntityValidationReport> validateDataEntities(T8Context context, List<T8DataEntity> entityList, T8ServerEntityValidationScriptDefinition scriptDefinition) throws Exception
    {
        if (entityList == null)
        {
            throw new Exception("No entities supplied to validate.");
        }
        else if (scriptDefinition == null)
        {
            throw new Exception("No Script definition supplied to use for validation.");
        }
        else
        {
            List<T8DataEntityValidationReport> validationReportList;
            T8ServerContextScript script;

            // Prepare the validation script.
            script = scriptDefinition.getNewScriptInstance(internalContext);
            script.prepareScript();

            // Execute the preapared script for each of the entities.
            validationReportList = new ArrayList<>();
            for (T8DataEntity dataEntity : entityList)
            {
                Map<String, Object> scriptOutput;
                List<T8DataEntityValidationError> validationErrors;
                boolean validationSuccess;

                scriptOutput = script.executePreparedScript(dataEntity.getFieldValues());
                validationSuccess = (Boolean)scriptOutput.get(scriptDefinition.getNamespace() + T8EntityValidationScriptDefinition.PARAMETER_VALIDATION_SUCCESS);
                validationErrors = (List<T8DataEntityValidationError>)scriptOutput.get(scriptDefinition.getNamespace() + T8EntityValidationScriptDefinition.PARAMETER_VALIDATION_ERROR_LIST);

                validationReportList.add(new T8DataEntityValidationReport(dataEntity.getGUID(), validationSuccess, validationErrors));
            }

            script.finalizeScript();
            return validationReportList;
        }
    }

    @Override
    public synchronized T8DataConnectionPool getDataConnectionPool(String dataConnectionIdentifier)
    {
        return openConnectionPools.get(dataConnectionIdentifier);
    }

    @Override
    public synchronized void installDataConnectionPool(T8DataConnectionPool connectionPool)
    {
        if (!openConnectionPools.containsKey(connectionPool.getDataConnectionIdentifier()))
        {
            LOGGER.log("Installing new data connection pool: " + connectionPool.getDataConnectionIdentifier());
            openConnectionPools.put(connectionPool.getDataConnectionIdentifier(), connectionPool);
        }
        else throw new RuntimeException("Attempt to install a connection pool that already exists: " + connectionPool.getDataConnectionIdentifier());
    }

    private void closeSession(String sessionIdentifier)
    {
        T8DataSession sessionToClose;

        sessionToClose = openSessions.remove(sessionIdentifier);
        if (sessionToClose != null)
        {
            sessionToClose.closeSession();
            threadSessions.remove(sessionToClose.getParentThread());
        }
    }

    @Override
    public synchronized String generateKey(String keyId) throws Exception
    {
        T8DataKeyGenerator generator;

        generator = dataKeyGenerators.get(keyId);
        if (generator == null)
        {
            T8DataKeyGeneratorDefinition definition;
            T8DefinitionManager definitionManager;

            definitionManager = internalContext.getServerContext().getDefinitionManager();
            definition = (T8DataKeyGeneratorDefinition)definitionManager.getInitializedDefinition(null, null, keyId, null);
            if (definition != null)
            {
                // Create a new key generator from the definition.
                generator = definition.createNewDataKeyGenerator(internalContext);
                generator.init();
                dataKeyGenerators.put(keyId, generator);
            }
            else throw new Exception("Key Generator Definition not found: " + keyId);
        }

        // Return the next generated key.
        return generator.generateKey();
    }

    /**
     * This method attempts to find the requested data in one of the available
     * data caches.  If no cache provides the requested data type, null is returned.
     * If an applicable cache is present but the requested data
     * is not available in the cache, null is returned.
     * @param dataId The type identifier of the data to fetch.
     * @param parameters The additional parameters that define the characteristics of the data required.
     * @return The data object fetched from an applicable data cache.
     * @throws Exception
     */
    @Override
    public Object getCachedData(String dataId, Map<String, Object> parameters) throws Exception
    {
        for (T8DataCache dataCache : dataCaches.values())
        {
            if (dataCache.cachesData(dataId))
            {
                // If the cache has not been initialized (lazy initialization) then do it now.
                if (!dataCache.isInitialized())
                {
                    dataCache.init();
                }

                // Now return the data fetched from the cache.
                return dataCache.getCachedData(dataId, parameters);
            }
        }

        // No applicable cache found.
        return null;
    }

    /**
     * This method attempts to find the data cache responsible for caching the
     * requested data and then requests the cache to refresh is cached content.
     * If no cache provides the requested data type, an exception
     * will be thrown.
     * @param dataIdentifier The type identifier of the data to fetch.
     * @param parameters The additional parameters that define the
     * characteristics of the data required.
     * @return The data object fetched from the data cache.
     * @throws Exception
     */
    @Override
    public Object recacheData(String dataIdentifier, Map<String, Object> parameters) throws Exception
    {
        for (T8DataCache dataCache : dataCaches.values())
        {
            if (dataCache.cachesData(dataIdentifier))
            {
                // If the cache has not been initialized (lazy initialization) then do it now.
                if (!dataCache.isInitialized())
                {
                    dataCache.init();

                    // Now return the data fetched from the cache.
                    return dataCache.getCachedData(dataIdentifier, parameters);
                }
                else
                {
                    // Now return the data fetched from the cache.
                    return dataCache.recacheData(dataIdentifier, parameters);
                }
            }
        }

        // This point should not be reached during successful execution.
        throw new Exception("Data Cache for required data type not found: " + dataIdentifier);
    }

    @Override
    public boolean isEntityAccessible(T8Context context, String entityId)
    {
        T8DataEntityDefinition dataEntityDefinition;
        T8DataSourceDefinition dataSourceDefinition;
        T8DataSource dataSource;

        try
        {
            T8DefinitionManager definitionManager;

            // Get anew data source instance to use for the test.
            definitionManager = internalContext.getServerContext().getDefinitionManager();
            dataEntityDefinition = (T8DataEntityDefinition)definitionManager.getRawDefinition(context, context.getProjectId(), entityId);
            dataSourceDefinition = (T8DataSourceDefinition)definitionManager.getRawDefinition(context, context.getProjectId(), dataEntityDefinition.getDataSourceIdentifier());
            dataSource = dataSourceDefinition.getNewDataSourceInstance(getCurrentSession().instantTransaction());

            // We open and close the data source to check if it is accessible.
            dataSource.open();
            dataSource.close();
        }
        catch (Throwable ex)
        {
            LOGGER.log("Failed to determine if entity is Accessible", ex);
            return false;
        }
        return true;
    }

    /**
     * This class is a task that is scheduled using a timer object and is used
     * to run regular checks on the currently open sessions in order to close
     * and discard sessions that have expired.  This checker thread also asks
     * open sessions to check the status of any instant transactions in use and
     * to close those that have expired.
     */
    private static class SessionMaintenanceThread extends Thread
    {
        private Map<String, T8DataSession> openSessions;
        private T8ServerDataManager dataManager;
        private T8Context context;
        private final long sessionExpirationTime;
        private final long instantTransactionExpirationTime;

        private SessionMaintenanceThread(T8Context context, T8ServerDataManager dataManager, Map<String, T8DataSession> openSessions, long sessionExpirationTime, long instantTransactionExpirationTime)
        {
            this.dataManager = dataManager;
            this.context = context;
            this.openSessions = openSessions;
            this.sessionExpirationTime = sessionExpirationTime;
            this.instantTransactionExpirationTime = instantTransactionExpirationTime;
        }

        @Override
        public void destroy()
        {
            dataManager = null;
            context = null;
            openSessions = null;
        }

        @Override
        public void run()
        {
            try
            {
                ArrayList<String> expiredSessions;
                List<T8DataSession> sessionsToCheck;
                long currentTime;

                currentTime = System.currentTimeMillis();
                expiredSessions = new ArrayList<>();

                // Create a copy of the active open session collection so that we can iterate over it without causing a concurrent modification exception.
                synchronized (openSessions)
                {
                    sessionsToCheck = new ArrayList<>(openSessions.values());
                }

                // Check the active data sessions collection and remove any sessions that have been idle for longer than the expiration time.
                for (T8DataSession sessionToCheck : sessionsToCheck)
                {
                    synchronized (sessionToCheck)
                    {
                        long lastActivityTime;

                        lastActivityTime = sessionToCheck.getLastActivityTime();
                        if ((currentTime - lastActivityTime) > sessionExpirationTime)
                        {
                            // Make sure that the data session is not currently active.
                            if (!sessionToCheck.isActive())
                            {
                                expiredSessions.add(sessionToCheck.getId());
                            }
                        }
                    }
                }

                // If there are any expired sessions, close them.
                if (expiredSessions.size() > 0)
                {
                    for (String sessionIdentifier : expiredSessions)
                    {
                        try
                        {
                            LOGGER.log("Finalizing expired data session: " + sessionIdentifier);
                            dataManager.closeSession(sessionIdentifier);
                        }
                        catch (Exception e)
                        {
                            LOGGER.log("Exception while finalizing data session from expiration checker thread: " + sessionIdentifier, e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while checking for expired data sessions.", e);
            }
        }
    }
}
