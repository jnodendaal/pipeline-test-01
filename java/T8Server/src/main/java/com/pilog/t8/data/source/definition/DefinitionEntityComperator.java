/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pilog.t8.data.source.definition;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilter.OrderMethod;
import java.util.Comparator;
import java.util.LinkedHashMap;

/**
 *
 * @author hennie.brink@pilog.co.za
 */
public class DefinitionEntityComperator implements Comparator<T8DataEntity>
{
    private LinkedHashMap<String, OrderMethod> fieldOrdering;
    private boolean hasFieldOrdering;

    public DefinitionEntityComperator(
            LinkedHashMap<String, OrderMethod> fieldOrdering)
    {
        this.fieldOrdering = fieldOrdering;
        this.hasFieldOrdering = true;
    }

    /**
     * This Constructor will load the field ordering defined in the data filter. If the data filter has no field ordering then all fields will be regarded as the same
     * and will not be ordered.
     * <p/>
     * @param dataFilter
     */
    public DefinitionEntityComperator(T8DataFilter dataFilter)
    {
        this.fieldOrdering = dataFilter.getFieldOrdering();
        this.hasFieldOrdering = dataFilter.hasFieldOrdering();
    }

    @Override
    public int compare(T8DataEntity o1, T8DataEntity o2)
    {
        if (!hasFieldOrdering)
        {
            return 0;
        }

        if (o1 == null && o2 != null)
        {
            return -1;
        }

        if (o1 != null && o2 == null)
        {
            return 1;
        }

        //We will now loop through the field orderdings in reverse, with the last field having the lowest score and the first one the highest
        int entity1Score = 0;
        int entity2Score = 0;

        int totalFields = fieldOrdering.size() + 1;

        for (String fieldIdentifier : fieldOrdering.keySet())
        {
            int score = totalFields--;
            OrderMethod orderMethod = fieldOrdering.get(fieldIdentifier);

            Object entity1Object = o1.getFieldValue(fieldIdentifier);
            Object entity2Object = o2.getFieldValue(fieldIdentifier);

            if (entity1Object == null && entity2Object != null)
            {
                if (orderMethod == OrderMethod.ASCENDING)
                {
                    entity2Score += score;
                }
                else
                {
                    entity1Score += score;
                }
                continue;
            }
            if (entity1Object != null && entity2Object == null)
            {
                if (orderMethod == OrderMethod.ASCENDING)
                {
                    entity1Score += score;
                }
                else
                {
                    entity2Score += score;
                }
                continue;
            }

            if (entity1Object instanceof String && entity2Object instanceof String)
            {
                int stringCompare = ((String) entity1Object).toUpperCase().compareTo(((String) entity2Object).toUpperCase());
                if (stringCompare == 0)
                {
                    entity1Score += score;
                    entity2Score += score;
                }
                else
                {
                    if (orderMethod == OrderMethod.ASCENDING)
                    {
                        if (stringCompare > 0)
                        {
                            entity1Score += score;
                        }
                        else
                        {
                            entity2Score += score;
                        }
                    }
                    else
                    {
                        if (stringCompare > 0)
                        {
                            entity2Score += score;
                        }
                        else
                        {
                            entity1Score += score;
                        }
                    }
                }
                continue;
            }

            if (entity1Object instanceof Number && entity2Object instanceof Number)
            {
                Number e1Number = (Number)entity1Object;
                Number e2Number = (Number)entity2Object;
                if (orderMethod == OrderMethod.ASCENDING)
                {
                    if (e1Number.floatValue() > e2Number.floatValue())
                    {
                        entity1Score += score;
                    }
                    else
                    {
                        entity2Score += score;
                    }
                }
                else
                {
                    if (e1Number.floatValue() > e2Number.floatValue())
                    {
                        entity2Score += score;
                    }
                    else
                    {
                        entity1Score += score;
                    }
                }
                continue;
            }
        }

        if(entity1Score == entity2Score)
            return 0;

        if(entity1Score > entity2Score)
            return 1;

        if(entity1Score < entity2Score)
            return -1;

        //Could not be determined, just return 0 to say they are equal
        return 0;
    }
}
