package com.pilog.t8.data.source.sql;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8SQLBasedDataSource;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.source.T8CommonStatementHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.cte.T8CTEDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLDataSourceDefinition;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.utilities.strings.ParameterizedString;
import com.pilog.t8.utilities.strings.StringUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8SQLDataSource implements T8SQLBasedDataSource
{
    protected final T8DataTransaction tx;
    protected final T8ServerContext serverContext;
    protected final T8Context context;
    protected final T8SQLDataSourceDefinition definition;
    protected T8DataConnection connection;
    protected final List<T8DataEntityResults> openEntityResults;
    protected final String projectId;
    protected Map<String, String> sqlParameterMap;
    protected Map<String, Map<String, String>> cteSQLParameterMap;
    protected Map<String, T8CTEDefinition> cteDefinitions;
    protected T8PerformanceStatistics stats;
    protected int queryTimeout; // The number of seconds allowed for queries on this data source to execute before being terminated.

    public T8SQLDataSource(T8SQLDataSourceDefinition definition, T8DataTransaction tx)
    {
        this.definition = definition;
        this.tx = tx;
        this.openEntityResults = new ArrayList<T8DataEntityResults>();
        this.context = tx.getContext();
        this.serverContext = context.getServerContext();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
        this.cteDefinitions = new LinkedHashMap<String, T8CTEDefinition>();
        this.queryTimeout = definition.getQueryTimeout();
        this.projectId = definition.getRootProjectId();
    }

    @Override
    public T8DataSourceDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    @Override
    public void open() throws Exception
    {
        T8DefinitionManager definitionManager;

        // Open the connection.
        connection = tx.getDataConnection(definition.getConnectionIdentifier());
        definitionManager = serverContext.getDefinitionManager();

        // Get all of the required CTE Definitions.
        if (definition.getCTEDefinitionIdentifiers() != null)
        {
            for (String cteId : definition.getCTEDefinitionIdentifiers())
            {
                T8CTEDefinition cteDefinition;

                cteDefinition = (T8CTEDefinition)definitionManager.getInitializedDefinition(context, projectId, cteId, null);
                if (cteDefinition != null)
                {
                    cteDefinitions.put(cteId, cteDefinition);
                }
                else T8Log.log("WARNING: CTE Required by data source " + definition + " not found: " + cteId);
            }
        }

        // Evaluate the SQL Parameter expressions (if any are defined).
        cacheSQLParameters();
    }

    @Override
    public void close() throws Exception
    {
        // Close any open entity results.
        for (T8DataEntityResults results : openEntityResults)
        {
            results.close();
        }
    }

    @Override
    public void commit() throws Exception
    {
        if (connection != null) connection.commit();
    }

    @Override
    public void rollback() throws Exception
    {
        if (connection != null) connection.rollback();
    }

    @Override
    public void setParameters(Map<String, Object> parameters)
    {
    }

    @Override
    public List<T8DataSourceFieldDefinition> retrieveFieldDefinitions() throws Exception
    {
        ParameterizedString queryString;

        // Create the query string.
        queryString = getQueryString(null, null, true, 0, 1); // We don't have to supply an entity definition because we are not filtering or ordering the data according to fields in the entity.
        queryString.append(" WHERE 1 <> 1 ");
        return T8CommonStatementHandler.getQueryFieldDefinitions(context, connection, queryString);
    }

    @Override
    public int count(T8DataFilter filter) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = tx.getDataEntityDefinition(filter.getEntityIdentifier());
        queryString = getCountQueryString(entityDefinition, filter, true);

        return T8CommonStatementHandler.executeCountQuery(context, connection, queryString, definition, entityDefinition, queryTimeout);
    }

    @Override
    public boolean exists(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return count(new T8DataFilter(entityIdentifier, keyMap)) > 0;
    }

    @Override
    public void insert(T8DataEntity tdo) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void insert(List<T8DataEntity> tdo) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> keyValues) throws Exception
    {
        ParameterizedString queryString;
        ArrayList<T8DataEntity> selectedObjects;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);
        queryString = getQueryString(entityDefinition, new T8DataFilter(entityIdentifier, keyValues), true, 0, 1);
        selectedObjects = T8CommonStatementHandler.executeQuery(context, connection, queryString, definition, entityDefinition, 0, 1, queryTimeout);
        if (selectedObjects.size() > 0)
        {
            return selectedObjects.get(0);
        }
        else return null;
    }

    @Override
    public boolean update(T8DataEntity tdo) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean update(List<T8DataEntity> tdo) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int update(String entityIdentifier, Map<String, Object> updatedValues, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean delete(T8DataEntity tdo) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int delete(List<T8DataEntity> tdo) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int delete(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return select(entityIdentifier, keyMap != null ? new T8DataFilter(entityIdentifier, keyMap) : null);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        return select(entityIdentifier, filter, 0, 1000000);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap, int offset, int pageSize) throws Exception
    {
        return select(entityIdentifier, keyMap != null ? new T8DataFilter(entityIdentifier, keyMap) : null, offset, pageSize);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        // Create the query string.
        queryString = getQueryString(entityDefinition, filter, true, pageOffset, pageSize);

        // Retrieve the results.
        return T8CommonStatementHandler.executeQuery(context, connection, queryString, definition, entityDefinition, pageOffset, pageSize, queryTimeout);
    }

    @Override
    public T8DataEntityResults scroll(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;
        T8DataEntityResults entityResults;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        // Create the query string.
        queryString = getQueryString(entityDefinition, filter, true, 0, -1);

        // Retrieve the results.
        entityResults = T8CommonStatementHandler.getEntityResults(context, connection, queryString, definition, entityDefinition);
        openEntityResults.add(entityResults);
        return entityResults;
    }

    @Override
    public ParameterizedString getWithClause(T8DataEntityDefinition entityDefinition, T8DataFilter filter, int pageOffset, int pageSize)
    {
        StringBuilder withClause;
        String sqlString;
        String withString;
        String filterString;
        int withCount;

        // Get the SQL and WITH strings from the definition.
        sqlString = definition.getSQLSelectString();
        withString = definition.getSQLWithString();

        // Get the filter string.
        if (filter != null)
        {
            StringBuffer filterWhereClause;

            filterWhereClause = filter.getWhereClause(tx, null);
            filterString = filterWhereClause != null ? filterWhereClause.toString() : "";
        }
        else filterString = "";

        // Create a new string builder that will append all of the with clauses.
        withCount = 0;
        withClause = new StringBuilder("WITH ");
        for (T8CTEDefinition cteDefinition : cteDefinitions.values())
        {
            String cteString;

            // Replace any with string found, we will use our own.
            cteString = cteDefinition.getSQLCTEString(filter, pageOffset, pageSize);
            if (!Strings.isNullOrEmpty(cteString))
            {
                String cteName;

                // Replace any with string found, we will use our own.
                cteString = cteString.replaceAll("\\b(WITH)|(with)\\b", "");
                cteString = StringUtilities.replaceStringParameters(cteString, cteSQLParameterMap.get(cteDefinition.getIdentifier()));
                cteName = cteDefinition.getName();
                if (!Strings.isNullOrEmpty(cteName))
                {
                    // Only append the WITH if it is used.
                    if ((sqlString.contains(cteName)) || (withString.contains(cteName)) || (filterString.contains(cteName)))
                    {
                        // Add all included CTE's first.
                        for (T8CTEDefinition includedCTEDefinition : cteDefinition.getIncludedCTEDefinitions())
                        {
                            String includedCTEString;

                            // Replace any with string found, we will use our own.
                            includedCTEString = includedCTEDefinition.getSQLCTEString(filter, pageOffset, pageSize);
                            if (!Strings.isNullOrEmpty(includedCTEString))
                            {
                                // Replace any with string found, we will use our own.
                                includedCTEString = includedCTEString.replaceAll("\\b(WITH)|(with)\\b", "");
                                includedCTEString = StringUtilities.replaceStringParameters(includedCTEString, cteSQLParameterMap.get(includedCTEDefinition.getIdentifier()));

                                // If a previous CTE has been appended, add a comma to seperate them.
                                if (withCount > 0) withClause.append(", ");

                                // Append the CTE.
                                withClause.append(includedCTEString);
                                withCount++;
                            }
                        }

                        // If a previous CTE has been appended, add a comma to seperate them.
                        if (withCount > 0) withClause.append(", ");

                        // Append the CTE.
                        withClause.append(cteString);
                        withCount++;
                    }
                }
                else throw new RuntimeException("No Name defined for CTE: " + cteDefinition);
            }
        }

        // We add the definition with last as it may be dependant on other with clauses.
        if (!Strings.isNullOrEmpty(withString))
        {
            String cteString;

            // If a previous CTE has been appended, add a comma to seperate them.
            if (withCount > 0) withClause.append(", ");

            // Replace any with string found, we will use our own.
            cteString = withString.replaceAll("\\b(WITH)|(with)\\b", "");
            withClause.append(cteString);
            withCount++;
        }

        if (withCount > 0)
        {
            return new ParameterizedString(StringUtilities.replaceStringParameters(withClause.toString(), sqlParameterMap));
        }
        else return null;
    }

    @Override
    public ParameterizedString getSelectClause(T8DataEntityDefinition entityDefinition, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        return new ParameterizedString(StringUtilities.replaceStringParameters(definition.getSQLSelectString(), sqlParameterMap));
    }

    @Override
    public ParameterizedString getQueryString(T8DataEntityDefinition entityDefinition, T8DataFilter filter, boolean includeWithClause, int pageOffset, int pageSize) throws Exception
    {
        ParameterizedString queryString;
        ParameterizedString withClause;

        // If a WITH clause is defined, prepend it to the query if required.
        withClause = getWithClause(entityDefinition, filter, pageOffset, pageSize);
        if ((withClause != null) && (!withClause.isEmpty()) && includeWithClause)
        {
            queryString = new ParameterizedString("-- ");
            if (entityDefinition != null) queryString.append(entityDefinition.getIdentifier());
            queryString.append("\n");
            queryString.append(withClause);
            queryString.append(" SELECT * FROM (");
        }
        else
        {
            queryString = new ParameterizedString("-- ");
            if (entityDefinition != null) queryString.append(entityDefinition.getIdentifier());
            queryString.append("\nSELECT * FROM (");
        }

        // Append the SQL Selection string from the definition (with SQL parameters replaced).
        queryString.append(getSelectClause(entityDefinition, filter, pageOffset, pageSize));
        queryString.append(") selectResults ");

        // Append filter strings.
        if (filter != null)
        {
            // Add the WHERE clause if available.
            if (filter.hasFilterCriteria())
            {
                queryString.append(" ");
                queryString.append(filter.getWhereClause(tx, "selectResults"));
                queryString.setParameterValues(filter.getWhereClauseParameters(tx));
            }

            // Add the ORDER BY clause if available.
            if (filter.hasFieldOrdering())
            {
                queryString.append(" ");
                queryString.append(filter.getOrderByClause(connection, entityDefinition, definition));
            }
        }

        return queryString;
    }

    @Override
    public ParameterizedString getCountQueryString(T8DataEntityDefinition entityDefinition, T8DataFilter filter, boolean includeWithClause) throws Exception
    {
        ParameterizedString queryString;
        ParameterizedString withClause;

        // Check if we have any with expressions to add before the select clause and then Create the query string.
        withClause = getWithClause(entityDefinition, filter, 0, -1);
        if ((withClause != null) && (!withClause.isEmpty()) && includeWithClause)
        {
            queryString = new ParameterizedString();
            queryString.append(withClause);
            queryString.append(" SELECT COUNT(*) AS RECORD_COUNT FROM (");
        }
        else
        {
            queryString = new ParameterizedString("SELECT COUNT(*) AS RECORD_COUNT FROM (");
        }

        // Append the SQL Selection string from the definition (with SQL parameters replaced).
        queryString.append(getSelectClause(entityDefinition, filter, 0, -1));
        queryString.append(") recordCount ");

        // Append filter strings.
        if ((filter != null) && (filter.hasFilterCriteria()))
        {
            queryString.append(filter.getWhereClause(tx, "recordCount"));
            queryString.setParameterValues(filter.getWhereClauseParameters(tx));
        }

        return queryString;
    }

    private void cacheSQLParameters() throws Exception
    {
        // Create new caching maps.
        sqlParameterMap = new HashMap<String, String>();
        cteSQLParameterMap = new HashMap<String, Map<String, String>>();

        // Evaluate the parameters used by this data source's internal query string.
        sqlParameterMap = evaluateExpressions(definition.getSQLParameterExpressionMap());

        // Evaluate the parameters used by each of the CTE's used by this source.
        for (T8CTEDefinition cteDefinition : cteDefinitions.values())
        {
            // Evaluate the root CTE parameters.
            cteSQLParameterMap.put(cteDefinition.getIdentifier(), evaluateExpressions(cteDefinition.getSQLParameterExpressionMap()));

            // Evaluate the dependant CTE parameters.
            for (T8CTEDefinition dependantDefinition : cteDefinition.getIncludedCTEDefinitions())
            {
                cteSQLParameterMap.put(dependantDefinition.getIdentifier(), evaluateExpressions(dependantDefinition.getSQLParameterExpressionMap()));
            }
        }
    }

    private Map<String, String> evaluateExpressions(Map<String, String> expressionMap) throws Exception
    {
        Map<String, String> resultMap;

        resultMap = new HashMap<String, String>();
        if ((expressionMap != null) && (expressionMap.size() > 0))
        {
            T8ServerExpressionEvaluator expressionEvaluator;
            HashMap<String, Object> expressionParameters;

            // Create a map of expression parameters.
            expressionParameters = new HashMap<String, Object>();
            expressionParameters.put("dbAdaptor", connection.getDatabaseAdaptor());
            expressionParameters.put("session", context);

            // Construct a new expression evaluator to use.
            expressionEvaluator = new T8ServerExpressionEvaluator(context);
            for (String parameterId : expressionMap.keySet())
            {
                String expression;
                Object result;

                expression = expressionMap.get(parameterId);
                result = expressionEvaluator.evaluateExpression(expression, expressionParameters, null);
                if (result instanceof String)
                {
                    resultMap.put(parameterId, (String)result);
                }
                else throw new RuntimeException("SQL Parameter Expression return invalid object type '" + (result != null ? result.getClass().getCanonicalName() : null) + "': " + expression);
            }
        }

        return resultMap;
    }
}
