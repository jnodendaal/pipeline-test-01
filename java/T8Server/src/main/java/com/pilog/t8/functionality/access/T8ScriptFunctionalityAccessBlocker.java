package com.pilog.t8.functionality.access;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.definition.functionality.access.T8FunctionalityAccessBlockerScriptDefinition;
import com.pilog.t8.definition.functionality.access.T8ScriptFunctionalityAccessBlockerDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ScriptFunctionalityAccessBlocker implements T8FunctionalityAccessBlocker
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8ScriptFunctionalityAccessBlockerDefinition definition;
    private final T8FunctionalityAccessBlockerScriptDefinition scriptDefinition;

    public T8ScriptFunctionalityAccessBlocker(T8Context context, T8ScriptFunctionalityAccessBlockerDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.definition = definition;
        this.scriptDefinition = definition.getScriptDefinition();
    }

    @Override
    public List<T8FunctionalityBlock> getFunctionalityBlocks(T8Context context, String functionalityId, List<T8DataObject> dataObjects)
    {
        if (scriptDefinition != null)
        {
            try
            {
                Map<String, Object> inputParameters;
                Map<String, Object> outputParameters;
                T8FunctionalityAccessBlockerScript script;

                // Create the script instance.
                script = (T8FunctionalityAccessBlockerScript)scriptDefinition.getNewScriptInstance(context);

                // Create the input parameter map for the script.
                inputParameters = new HashMap<String, Object>();
                inputParameters.put(T8FunctionalityAccessBlockerScriptDefinition.PARAMETER_FUNCTIONALITY_IDENTIFIER, functionalityId);
                inputParameters.put(T8FunctionalityAccessBlockerScriptDefinition.PARAMETER_DATA_OBJECTS, dataObjects);

                // Execute the script and get the output parameters.
                outputParameters = script.executeScript(inputParameters);
                if (outputParameters != null)
                {
                    List<T8FunctionalityBlock> functionalityBlocks;

                    // Make sure never to return a null list.
                    functionalityBlocks = (List<T8FunctionalityBlock>)outputParameters.get(definition.getNamespace() + T8FunctionalityAccessBlockerScriptDefinition.PARAMETER_FUNCTIONALITY_BLOCK_LIST);
                    return functionalityBlocks != null ? functionalityBlocks : new ArrayList<T8FunctionalityBlock>();
                }
                else return new ArrayList<T8FunctionalityBlock>();
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while generating functionality access blocks from script: " + definition, e);
            }
        }
        else return new ArrayList<T8FunctionalityBlock>();
    }
}
