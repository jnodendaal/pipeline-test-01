package com.pilog.t8.notification;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.notification.T8SystemNotificationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8SessionDetails;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8SystemNotificationGenerator implements T8NotificationGenerator
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8SystemNotificationGenerator.class);

    private final T8SystemNotificationDefinition definition;
    private final T8ServerContext serverContext;

    public T8SystemNotificationGenerator(T8Context context, T8SystemNotificationDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.definition = definition;
    }

    @Override
    public List<T8Notification> generateNotifications(T8Context context, Map<String, ? extends Object> inputParameters) throws Exception
    {
        List<String> toUserIds;
        List<T8Notification> notifications;
        T8Notification notification;

        notifications = new ArrayList<>();
        toUserIds = (List<String>)inputParameters.get(T8SystemNotificationDefinition.PARAMETER_USER_IDENTIFIER_LIST);
        if (CollectionUtilities.isNullOrEmpty(toUserIds))
        {
            toUserIds = getUserSubset(context, (String)inputParameters.get(T8SystemNotificationDefinition.PARAMETER_USER_SUBSET));
        }

        if (toUserIds.isEmpty()) LOGGER.log("No receiving users found for System Notification : " + this.definition.getIdentifier() + " - using parameters : " + inputParameters);

        for (String toUserId : toUserIds)
        {
            notification = new T8Notification(this.definition.getRootProjectId(), this.definition.getIdentifier(), T8IdentifierUtilities.createNewGUID());
            notification.setSenderDetail(context.getSessionContext());
            notification.setUserIdentifier(toUserId);
            notification.setParameters(inputParameters);
            notifications.add(notification);
        }

        return notifications;
    }

    private List<String> getUserSubset(T8Context context, String userSubsetParameter) throws Exception
    {
        T8SystemNotificationDefinition.UserSubset userSubset;
        List<String> subsetUserIdentifiers;
        List<T8SessionDetails> sessions;

        userSubset = T8SystemNotificationDefinition.UserSubset.valueOf(userSubsetParameter);
        subsetUserIdentifiers = new ArrayList<>();
        switch (userSubset)
        {
            case ACTIVE_SESSIONS:
                sessions = this.serverContext.getSecurityManager().getAllSessionDetails(context);
                for (T8SessionDetails session : sessions) subsetUserIdentifiers.add(session.getUserId());
                break;
            default: throw new IllegalArgumentException("The specified user subset : " + userSubsetParameter + " could not be processed.");
        }

        return subsetUserIdentifiers;
    }
}