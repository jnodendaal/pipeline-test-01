package com.pilog.t8.process.report;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.process.T8Process;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.process.T8ProcessState;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.process.report.T8ReportGenerationProcessDefinition;
import com.pilog.t8.definition.report.T8ReportDefinition;
import com.pilog.t8.report.T8ReportGenerationResult;
import com.pilog.t8.report.T8ReportGenerator;
import com.pilog.t8.operation.progressreport.T8DefaultProgressReport;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

import static com.pilog.t8.definition.report.T8ReportManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ReportGenerationProcess implements T8Process
{
    private final T8ReportGenerationProcessDefinition definition;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final String iid;
    private final String initiatorId;
    private final String initiatorDisplayName;
    private final String initiatorProfileId;
    private String reportIid;
    private T8ReportGenerator generator;
    private T8ReportGenerationResult result;
    private T8ProcessStatus status;
    private long startTime;
    private long endTime;

    public T8ReportGenerationProcess(T8Context context, T8ReportGenerationProcessDefinition definition, String iid)
    {
        this.context = context;
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.definition = definition;
        this.iid = iid;
        this.initiatorId = sessionContext.getUserIdentifier();
        this.initiatorDisplayName = sessionContext.getUserName() + " " + sessionContext.getUserSurname();
        this.initiatorProfileId = sessionContext.getUserProfileIdentifier();
        this.reportIid = null;
        this.status = T8ProcessStatus.CREATED;
        this.startTime = -1;
        this.endTime = -1;
    }

    @Override
    public String getIid()
    {
        return iid;
    }

    @Override
    public T8ProcessDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public T8ProcessState getState()
    {
        T8ProcessState state;

        state = new T8ProcessState(definition.getRootProjectId(), definition.getIdentifier(), iid);
        state.setStartTime(new T8Timestamp(startTime));
        state.setEndTime(new T8Timestamp(endTime));
        state.setName(definition.getProcessDisplayName());
        state.setDescription(definition.getProcessDescription());
        state.setRestrictionUserId(null);
        state.setRestrictionUserProfileId(null);
        state.setStatus(status);
        state.setProgress(generator != null ? generator.getProgress() : -1);

        if (sessionContext.isSystemSession())
        {
            state.setInitiatorId(sessionContext.getSystemAgentIdentifier());
            state.setInitiatorInstanceIdentifier(sessionContext.getSystemAgentInstanceIdentifier());
        }
        else
        {
            state.setInitiatorId(sessionContext.getUserIdentifier());
            state.setInitiatorInstanceIdentifier(null);
        }

        return state;
    }

    @Override
    public T8ProcessStatus getStatus()
    {
        return status;
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> inputParameters) throws Exception
    {
        try
        {
            T8DefinitionManager definitionManager;
            T8ReportDefinition reportDefinition;
            Map<String, Object> reportParameters;
            String fileContextId;
            String directory;
            String projectId;
            String reportId;

            // Get the parameters for report generation.
            projectId = (String)inputParameters.get(PARAMETER_PROJECT_ID);
            reportIid = (String)inputParameters.get(PARAMETER_REPORT_IID);
            reportId = (String)inputParameters.get(PARAMETER_REPORT_ID);
            fileContextId = (String)inputParameters.get(PARAMETER_FILE_CONTEXT_ID);
            directory = (String)inputParameters.get(PARAMETER_DIRECTORY);
            reportParameters = (Map<String, Object>)inputParameters.get(PARAMETER_REPORT_PARAMETERS);

            // Load the report definition, get the generator and use it to generate the report.
            definitionManager = serverContext.getDefinitionManager();
            reportDefinition = definitionManager.getInitializedDefinition(context, projectId, reportId, null);
            if (reportDefinition != null)
            {
                // Generate the report.
                startTime = System.currentTimeMillis();
                status = T8ProcessStatus.IN_PROGRESS;
                generator = reportDefinition.getReportGenerator(context);
                result = generator.generateReport(reportIid, reportParameters, fileContextId, directory);
                endTime = System.currentTimeMillis();
                return HashMaps.newHashMap(PARAMETER_REPORT_IID, reportIid, PARAMETER_FILE_CONTEXT_ID, result.getFileContextId(), PARAMETER_FILE_PATH, result.getFilepath());
            }
            else throw new Exception("Report not found: " + reportId);
        }
        catch (Throwable throwable)
        {
            throw new Exception(throwable);
        }
    }

    @Override
    public void stop() throws Exception
    {
        if (generator != null)
        {
            generator.cancel();
        }
    }

    @Override
    public void cancel() throws Exception
    {
        if (generator != null)
        {
            generator.cancel();
        }
    }

    @Override
    public double getProgress() throws Exception
    {
        if (generator != null)
        {
            return generator.getProgress();
        }
        else return -1;
    }

    @Override
    public T8ProcessDetails getDetails() throws Exception
    {
        T8ProcessDetails processDetails;

        processDetails = new T8ProcessDetails(iid);
        processDetails.setStartTime(startTime);
        processDetails.setEndTime(endTime);
        processDetails.setProgress(getProgress());
        processDetails.setInitiatorId(initiatorId);
        processDetails.setInitiatorName(initiatorDisplayName);
        processDetails.setProcessStatus(status);
        processDetails.setProcessDescription(definition.getProcessDescription());
        processDetails.setName(definition.getProcessDisplayName());
        processDetails.setProcessId(definition.getIdentifier());
        processDetails.setProcessIid(iid);
        processDetails.setExecuting(status == T8ProcessStatus.IN_PROGRESS);
        processDetails.setVisibilityRestrictionUserIds(definition.getUserVisibilityResctrictions());
        processDetails.setVisibilityRestrictionProfileIds(definition.getUserProfileVisibilityResctrictions());
        processDetails.setVisibilityRestrictionWorkflowProfileIds(definition.getWorkFlowProfileVisibilityResctrictions());
        processDetails.setProgressReport(new T8DefaultProgressReport());
        return processDetails;
    }

    @Override
    public void pause() throws Exception
    {
        throw new UnsupportedOperationException("Report generation cannot be paused."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void resume() throws Exception
    {
        throw new UnsupportedOperationException("Report generation cannot be paused or resumed."); //To change body of generated methods, choose Tools | Templates.
    }
}
