package com.pilog.t8.data.key;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.utilities.collections.HashMaps;

import static com.pilog.t8.definition.data.T8DataManagerResource.*;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DataKeyStatePersistenceHandler
{
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8DataManager dataManager;
    private final String dataKeyIdentifier;
    private T8DataEntityDefinition stateEntityDefinition;

    public T8DataKeyStatePersistenceHandler(T8Context context, String dataKeyId)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.dataManager = serverContext.getDataManager();
        this.dataKeyIdentifier = dataKeyId;
        setEntityDefinitions();
    }

    private void setEntityDefinitions()
    {
        try
        {
            // Get the data key state entity definition.
            this.stateEntityDefinition = (T8DataEntityDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, null, DATA_KEY_STATE_DE_IDENTIFIER, null);
            if (stateEntityDefinition == null) throw new Exception("Entity definition not found: " + DATA_KEY_STATE_DE_IDENTIFIER);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while setting Data Key State Persistence Handler entity definitions.", e);
        }
    }

    private T8DataEntity createStateEntity(int seedValue)
    {
        T8DataEntity entity;

        // Create a new entity.
        entity = stateEntityDefinition.getNewDataEntityInstance();
        entity.setFieldValue(DATA_KEY_STATE_DE_IDENTIFIER + "$DATA_KEY_IDENTIFIER", dataKeyIdentifier);
        entity.setFieldValue(DATA_KEY_STATE_DE_IDENTIFIER + "$SEED_VALUE", seedValue);
        return entity;
    }

    public Integer retrieveSeedValue(int defaultSeedValue) throws Exception
    {
        T8DataEntity seedEntity;
        T8DataTransaction tx;

        // Get the transaction to use.
        tx = dataManager.getCurrentSession().instantTransaction();

        // Retrieve the entity using the key identifier.
        seedEntity = tx.retrieve(DATA_KEY_STATE_DE_IDENTIFIER, HashMaps.newHashMap(DATA_KEY_STATE_DE_IDENTIFIER + "$DATA_KEY_IDENTIFIER", dataKeyIdentifier));
        if (seedEntity != null)
        {
            Integer seedValue;

            // Get the seed value from the retrieved entity.  If the value is null (which it shouldn't be) update the entity with the default seed value.
            seedValue = (Integer)seedEntity.getFieldValue("$SEED_VALUE");
            if (seedValue != null)
            {
                // Return the retrieved seed value.
                return seedValue;
            }
            else
            {
                // Entity contained null seed value, so update it with the default.
                seedEntity.setFieldValue(DATA_KEY_STATE_DE_IDENTIFIER + "$DATA_KEY_IDENTIFIER", defaultSeedValue);
                tx.update(seedEntity);
                return defaultSeedValue;
            }
        }
        else
        {
            // Entity did not exist, so insert it using the default seed value.
            tx.insert(createStateEntity(defaultSeedValue));
            return defaultSeedValue;
        }
    }

    public void saveSeedValue(int seedValue) throws Exception
    {
        T8DataTransaction tx;

        // Get the transaction to use.
        tx = dataManager.getCurrentSession().instantTransaction();
        tx.update(createStateEntity(seedValue));
    }
}
