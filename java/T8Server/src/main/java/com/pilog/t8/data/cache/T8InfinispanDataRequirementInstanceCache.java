package com.pilog.t8.data.cache;

import com.pilog.t8.cache.T8DataRequirementInstanceCache;
import com.pilog.t8.cache.T8DataRequirementInstanceCache.DataRequirementInstanceData;
import javax.transaction.TransactionManager;
import org.infinispan.Cache;

/**
 * @author Bouwer du Preez
 */
public class T8InfinispanDataRequirementInstanceCache implements T8DataRequirementInstanceCache
{
    private final Cache<String, DataRequirementInstanceData> cache;

    public T8InfinispanDataRequirementInstanceCache(Cache<String, DataRequirementInstanceData> cache) throws Exception
    {
        this.cache = cache;
    }

    @Override
    public TransactionManager getTransactionManager()
    {
        return this.cache.getAdvancedCache().getTransactionManager();
    }

    @Override
    public void remove(DataRequirementInstanceData data)
    {
        cache.remove(data.getDrInstanceId());
    }

    @Override
    public DataRequirementInstanceData remove(String drInstanceId)
    {
        // Update the cache.
        return cache.remove(drInstanceId);
    }

    @Override
    public void put(DataRequirementInstanceData data)
    {
        cache.put(data.getDrInstanceId(), data);
    }

    @Override
    public DataRequirementInstanceData get(String drInstanceId)
    {
        return cache.get(drInstanceId);
    }

    @Override
    public boolean containsKey(String drInstanceId)
    {
        return cache.containsKey(drInstanceId);
    }
}
