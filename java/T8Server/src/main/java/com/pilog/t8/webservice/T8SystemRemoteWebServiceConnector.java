package com.pilog.t8.webservice;

import com.pilog.t8.definition.remote.server.connection.T8SystemRemoteServerConnectionDefinition;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 */
public class T8SystemRemoteWebServiceConnector extends T8WebServiceConnector
{
    private final T8SystemRemoteServerConnectionDefinition definition;

    public T8SystemRemoteWebServiceConnector(T8Context context, T8SystemRemoteServerConnectionDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    public String getAPIKey()
    {
        return this.definition.getRemoteUserDefinition().getApiKey();
    }
}