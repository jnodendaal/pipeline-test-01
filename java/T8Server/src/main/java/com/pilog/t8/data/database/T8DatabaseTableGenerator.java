package com.pilog.t8.data.database;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.utilities.jdbc.JDBCUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 */
public class T8DatabaseTableGenerator
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DatabaseTableGenerator.class);
    private static final String CUSTOM_TABLE_IDENTIFIER_PREFIX = "@DS_GENERATED_";

    private final T8DataTransaction dataAccessProvider;

    public enum TableGenerationType
    {
        /** Regardless if table changes exist, table is dropped and re-created. */
        DROP_EXISTING,
        /** Creates the table if it does not exist, otherwise just adds any missing columns. */
        ALTER_EXISTING,
        /** Outputs to the default log the differences between the existing table and that which is expected. */
        LOG_DIFFERENCES_ONLY
    };

    public T8DatabaseTableGenerator(T8Context context)
    {
        this.dataAccessProvider = context.getServerContext().getDataManager().getCurrentSession().instantTransaction();
    }

    public T8DatabaseTableGenerator(T8DataTransaction tx)
    {
        this.dataAccessProvider = tx;
    }

    /**
     * This method can generate a table in the database according to the
     * specifications by the data source definition. A table for which definition
     * was created, but the actual table is missing can be created during runtime
     * to ensure that the database integrity is adhered to. However, the reverse
     * of this process is not true. Anything which exists as part of the table
     * in the database which does not exist in the data source definition is
     * regarded as irrelevant and ignored by the current process. The
     * {@code TableGenerationType} specifies the actions performed and are as follow:<br>
     * <br>
     * 1. <b>DROP_EXISTING</b> - If the table already exists in the database, it
     * will be dropped and recreated from scratch, with all the required
     * columns and their attributes, including a primary key if required<br>
     * 2. <b>ALTER_EXISTING</b> - The table specified by the data source definition
     * will be either created if it does not yet exist, or it will be amended
     * to comply with all of the specifications of the data source definition.
     * Attributes added include at the least columns which are missing for
     * the existing database table, specifications of whether or not a field
     * is required or not, and the primary key for the table<br>
     * 3. <b>LOG_DIFFERENCES_ONLY</b> - If the table structure should not be
     * consolidated, but only the differences verified between the data
     * source definition and the database table. This will only show if
     * the database table is missing, or if the database table does not
     * contain all of the attributes of the columns specifications in the
     * data source definition
     *
     * @param dataSourceDefinition The {@code T8TableDataSourceDefinition} which
     * specifies all of the relevant definition data for the data source
     * being handled
     * @param generationType The {@code TableGenerationType} which specifies the
     * type of generation process to be followed
     *
     * @throws Exception If there are any errors which causes the entire table
     * generation process to fail
     */
    public void generateTable(T8TableDataSourceDefinition dataSourceDefinition, TableGenerationType generationType) throws Exception
    {
        T8DataConnection dataConnection;

        //Get a connection to the database in use
        dataConnection = this.dataAccessProvider.getDataConnection(dataSourceDefinition.getConnectionIdentifier());

        //If only logging the differences in the system is required then log and return
        if (TableGenerationType.LOG_DIFFERENCES_ONLY.equals(generationType))
        {
            logDifferences(dataSourceDefinition, dataConnection);
            return;
        }

        //Drop the table if it exists and is specified as such
        if (TableGenerationType.DROP_EXISTING.equals(generationType)) dropTable(dataSourceDefinition.getTableName(), dataConnection);

        createUpdateTable(dataSourceDefinition, dataConnection);
    }

    /**
     * Creates a new table where all the columns are of type string, with the specified precision and nullable
     *
     * This method creates column information and calls generateTable(String tableName, Set<Column> columns, TableGenerationType generationType)
     *
     * @param tableName The table name for the new table
     * @param columnNames The column names
     * @param precision The precision for the columns
     * @param generationType the TableGenerationType
     * @throws Exception
     */
    public void generateTable(String tableName, Set<String> columnNames, int precision, TableGenerationType generationType) throws Exception
    {
        Set<Column> columns = new LinkedHashSet<>();
        for (String columnName : columnNames)
        {
            Column column = new Column();
            column.setColumnName(columnName);
            column.setDataType(T8DataType.STRING);
            column.setPrimaryKey(false);
            column.setNullable(true);
            column.setPrecision(precision);
            columns.add(column);
        }
        generateTable(tableName, columns, generationType);
    }

    /**
     * Retrieves table and column information from result set meta data.
     *
     * This method does not retrieve primary key information.
     *
     * This method uses the last valid table name of the last column for use as table name
     *
     * This method creates column information and calls generateTable(String tableName, Set<Column> columns, TableGenerationType generationType)
     *
     * @param metaData The result set meta data to inspect
     * @param optionalTableName An optional table name to use if the table name should not be retrieved from column
     * @param generationType The TableGenerateType
     * @throws Exception
     */
    public void generateTable(T8DataConnection connection, ResultSetMetaData metaData, String optionalTableName, TableGenerationType generationType) throws Exception
    {
        String tableName = "";
        Set<Column> columns = new LinkedHashSet<>();
        for (int i = 1; i <= metaData.getColumnCount(); i++)
        {
            if (!Strings.isNullOrEmpty(metaData.getTableName(i)))
            {
                tableName = metaData.getTableName(i);
            }

            Column newColumn = new Column();
            newColumn.setColumnName(metaData.getColumnName(i));
            newColumn.setNullable((metaData.isNullable(i) == ResultSetMetaData.columnNullable));
            newColumn.setPrecision(metaData.getPrecision(i));
            newColumn.setScale(metaData.getScale(i));
            newColumn.setDataType(connection.getDatabaseAdaptor().getDataType(metaData.getColumnType(i), newColumn.getPrecision()));
            columns.add(newColumn);
        }
        tableName = !Strings.isNullOrEmpty(optionalTableName) ? optionalTableName : tableName;
        generateTable(tableName, columns, generationType);
    }

    /**
     * Generates a table by inspecting the table information of the provided table name and database meta data.
     * This method creates column information and calls generateTable(String tableName, Set<Column> columns, TableGenerationType generationType)
     * @param databaseMetaData The database meta data to use
     * @param catalog The catalog of the table (Optional/Nullable)
     * @param schema The schema of the table (Optional/Nullable)
     * @param tableName The table name from which existing column information will be retrieved.
     * @param newTableName The table name for the new table (Optional, if not provided tableName will be used.)
     * @param generationType The TableGenerationType
     * @throws Exception
     */
    public void generateTable(T8DataConnection connection, DatabaseMetaData databaseMetaData, String catalog, String schema, String tableName, String newTableName, TableGenerationType generationType) throws Exception
    {
        Set<Column> columns = new LinkedHashSet<>();
        ResultSet metaDataColumnInformationResultSet = databaseMetaData.getColumns(catalog, schema, tableName, "*");
        ResultSet primaryKeyResultSet = databaseMetaData.getPrimaryKeys(catalog, schema, tableName);
        while (metaDataColumnInformationResultSet.next())
        {
            Column newColumn = new Column();
            newColumn.setColumnName(metaDataColumnInformationResultSet.getString("COLUMN_NAME"));
            newColumn.setNullable((metaDataColumnInformationResultSet.getInt("NULLABLE") == DatabaseMetaData.columnNullable));
            newColumn.setPrecision(metaDataColumnInformationResultSet.getInt("COLUMN_SIZE"));
            newColumn.setDataType(connection.getDatabaseAdaptor().getDataType(metaDataColumnInformationResultSet.getInt("DATA_TYPE"), newColumn.getPrecision()));
            newColumn.setScale(metaDataColumnInformationResultSet.getInt("DECIMAL_DIGITS"));
            columns.add(newColumn);
        }
        while (primaryKeyResultSet.next())
        {
            for (Column column : columns)
            {
                if (column.getColumnName() != null && column.getColumnName().equals(primaryKeyResultSet.getString("COLUMN_NAME")))
                {
                    column.setPrimaryKey(true);
                }
            }
        }
        metaDataColumnInformationResultSet.close();
        primaryKeyResultSet.close();
        tableName = !Strings.isNullOrEmpty(newTableName) ? newTableName : tableName;
        generateTable(tableName, columns, generationType);
    }

    /**
     * Creates a new table with the column information provided.
     * This method just creates a new T8TableDataSourceDefinition with the provided information and calls createUpdateTable(T8TableDataSourceDefinition dataSourceDefinition, T8DataConnection dataConnection)
     * @param tableName The name of the new table to be created
     * @param columns The column information for the table columns
     * @param generationType The TabelGenerationType
     * @throws Exception
     */
    public void generateTable(String tableName, Set<Column> columns, TableGenerationType generationType) throws Exception
    {
        T8DataConnection dataConnection;

        T8TableDataSourceDefinition dataSourceDefinition = new T8TableDataSourceDefinition(CUSTOM_TABLE_IDENTIFIER_PREFIX + tableName);
        dataSourceDefinition.setTableName(tableName);
        for (Column column : columns)
        {
            T8DataSourceFieldDefinition fieldDefinition = new T8DataSourceFieldDefinition(column.getColumnName());
            fieldDefinition.setMetaDisplayName(column.getColumnName());
            fieldDefinition.setDataType(column.getDataType());
            fieldDefinition.setKey(column.isPrimaryKey());
            fieldDefinition.setRequired(column.isNullable());
            fieldDefinition.setSourceIdentifier(column.getColumnName());
            dataSourceDefinition.addFieldDefinition(fieldDefinition);
        }

        //Get a connection to the database in use
        dataConnection = this.dataAccessProvider.getDataConnection(dataSourceDefinition.getConnectionIdentifier());

        //If only logging the differences in the system is required then log and return
        if (TableGenerationType.LOG_DIFFERENCES_ONLY.equals(generationType))
        {
            logDifferences(dataSourceDefinition, dataConnection);
            return;
        }

        //Drop the table if it exists and is specified as such
        if (TableGenerationType.DROP_EXISTING.equals(generationType)) dropTable(dataSourceDefinition.getTableName(), dataConnection);

        createUpdateTable(dataSourceDefinition, dataConnection);
    }

    /**
     * Performs all of the required operations to create a new database table
     * according to the specifications in the data source definition. If the
     * table already exists, the required actions to add any missing columns
     * the the table is performed.
     */
    private void createUpdateTable(T8TableDataSourceDefinition dataSourceDefinition, T8DataConnection dataConnection) throws Exception
    {
        T8DatabaseAdaptor databaseAdaptor;
        String tableName;

        databaseAdaptor = dataConnection.getDatabaseAdaptor();
        tableName = dataSourceDefinition.getTableName();

        if (tableExists(tableName, dataConnection, databaseAdaptor))
        {
            setMissingColumns(tableName, dataConnection, databaseAdaptor, dataSourceDefinition.getFieldDefinitions());
        }
        else createTable(dataSourceDefinition, databaseAdaptor, dataConnection);
    }

    /**
     * Checks whether or not the specified table exists in the database. The
     * database checked is the one referred to by the data connection.
     */
    private boolean tableExists(String tableName, T8DataConnection dataConnection, T8DatabaseAdaptor databaseAdaptor) throws SQLException
    {
        PreparedStatement pstmtSelectTable = null;
        String tableSelectQuery;

        try
        {
            tableSelectQuery = databaseAdaptor.getTableSelectionQuery().concat(" WHERE TABLE_NAME = ?");
            pstmtSelectTable = dataConnection.prepareStatement(tableSelectQuery);
            pstmtSelectTable.setString(1, tableName);

            if (pstmtSelectTable.executeQuery().next()) return true;
        }
        finally
        {
            JDBCUtilities.close(pstmtSelectTable);
        }

        return false;
    }

    /**
     * Creates the table as specified by the data source definition. The table
     * created will include all of the available attributes which can be applied
     * to the creation.
     */
    private void createTable(T8TableDataSourceDefinition dataSourceDefinition, T8DatabaseAdaptor databaseAdaptor, T8DataConnection dataConnection) throws SQLException
    {
        PreparedStatement pstmtCreateTable = null;
        String tableCreationDML = null;

        try
        {
            tableCreationDML = databaseAdaptor.getTableCreationStatement(dataSourceDefinition);
            pstmtCreateTable = dataConnection.prepareStatement(tableCreationDML);

            pstmtCreateTable.execute();
        }
        catch(SQLException ex)
        {
            LOGGER.log("Creating table: " + dataSourceDefinition.getTableName());
            LOGGER.log("DML: " + tableCreationDML);
            throw ex;
        }
        finally
        {
            JDBCUtilities.close(pstmtCreateTable);
        }
    }

    /**
     * Drops the table defined by the {@code T8TableDataSourceDefinition}.
     *
     * @param dataSourceDefinition The {@code T8TableDataSourceDefinition} that defines the table that should be dropped.
     * @throws Exception
     */
    public void dropTable(T8TableDataSourceDefinition dataSourceDefinition) throws Exception
    {
        T8DataConnection dataConnection;

        //Get a connection to the database in use
        dataConnection = this.dataAccessProvider.getDataConnection(dataSourceDefinition.getConnectionIdentifier());

        dropTable(dataSourceDefinition.getTableName(), dataConnection);
    }

    /**
     * The database table referred to the data source definition will be dropped
     * if it already exists. However, if it does not exist, a warning message
     * will be printed to the log, but the {@code Exception} ignored
     */
    private void dropTable(String tableName, T8DataConnection dataConnection)
    {
        PreparedStatement pstmtDropTable = null;

        try
        {
            pstmtDropTable = dataConnection.prepareStatement("DROP TABLE " + dataConnection.getDatabaseAdaptor().escapeMetaString(tableName));
            pstmtDropTable.execute();
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Database table " + tableName + " cannot be dropped due to : " + sqle.getMessage());
        }
        finally
        {
            JDBCUtilities.close(pstmtDropTable);
        }
    }

    /**
     * Compares the columns from the database table to the columns specified in
     * the data source definition. If any columns exist in the data source
     * definition, these columns will be added to the database table. The reverse
     * is not true. Existing columns in the database table will not be added to
     * the data source definition
     */
    private void setMissingColumns(String tableName, T8DataConnection dataConnection, T8DatabaseAdaptor databaseAdaptor, List<T8DataSourceFieldDefinition> dataFieldDefinitions) throws Exception
    {
        List<String> tableColumns;
        String sourceColumn;

        tableColumns = getTableColumnList(tableName, dataConnection);

        for (T8DataFieldDefinition dataFieldDefinition : dataFieldDefinitions)
        {
            sourceColumn = dataFieldDefinition.getSourceIdentifier();
            if (sourceColumn != null)
            {
                //If column already exists move to the next
                if (tableColumns.contains(sourceColumn))
                {
                    if (dataFieldDefinition.isKey()) databaseAdaptor.addColumnToPrimaryKey(tableName, sourceColumn, dataConnection);
                    continue;
                }

                addTableColumn(tableName, dataFieldDefinition, dataConnection, databaseAdaptor);
            }
            else throw new Exception("Null source column found in data source field definition: " + dataFieldDefinition);
        }
    }

    /**
     * Retrieves a {@code List} of all of the existing columns for the specified table
     */
    private List<String> getTableColumnList(String tableName, T8DataConnection dataConnection) throws SQLException
    {
        PreparedStatement pstmtSelectColumns = null;
        ResultSetMetaData rsMetaData;
        List<String> existingColumns;

        try
        {
            pstmtSelectColumns = dataConnection.prepareStatement("SELECT * FROM " + dataConnection.getDatabaseAdaptor().escapeMetaString(tableName) + " WHERE 1 = 0");
            rsMetaData = pstmtSelectColumns.executeQuery().getMetaData();

            existingColumns = new ArrayList<>();
            for (int columnIndex = 1; columnIndex <= rsMetaData.getColumnCount(); columnIndex++)
            {
                existingColumns.add(rsMetaData.getColumnLabel(columnIndex));
            }
        }
        finally
        {
            JDBCUtilities.close(pstmtSelectColumns);
        }

        return existingColumns;
    }

    /**
     * Adds a column to the specified table in the database. Any attributes that
     * the column may carry, such as being a required field, or part of the
     * primary key, will be carried over to the database as best possible
     */
    private void addTableColumn(String tableName, T8DataFieldDefinition dataFieldDefinition, T8DataConnection dataConnection, T8DatabaseAdaptor databaseAdaptor) throws SQLException
    {
        PreparedStatement pstmtAlterTable = null;
        String addColumnStatement;

        try
        {
            LOGGER.log("Adding column: " + dataFieldDefinition.getSourceIdentifier() + " to table: " + tableName);
            addColumnStatement = databaseAdaptor.getAddTableColumnStatement(tableName, dataFieldDefinition);
            pstmtAlterTable = dataConnection.prepareStatement(addColumnStatement);
            pstmtAlterTable.execute();

            if (dataFieldDefinition.isKey()) databaseAdaptor.addColumnToPrimaryKey(tableName, dataFieldDefinition.getSourceIdentifier(), dataConnection);
        }
        finally
        {
            JDBCUtilities.close(pstmtAlterTable);
        }
    }

    /**
     * Logs the differences between the data source definition and the database,
     * but not the other way around. In short, anything extra in the database
     * is irrelevant, but anything contained in the data definition which is not
     * part of the database will be logged.
     *
     * @param dataSourceDefinition The data source definition which specifies
     * the relevant definition data to compare to the database
     * @param dataConnection The current active {@code Connection} to the
     * database
     */
    private void logDifferences(T8TableDataSourceDefinition dataSourceDefinition, T8DataConnection dataConnection)
    {
        List<String> tableColumns;
        String columnName;
        String tableName;

        try
        {
            tableName = dataSourceDefinition.getTableName();
            if (!tableExists(tableName, dataConnection, dataConnection.getDatabaseAdaptor()))
            {
                LOGGER.log("Table " + tableName + " does NOT exist");
                return;
            }

            tableColumns = getTableColumnList(tableName, dataConnection);
            for (T8DataFieldDefinition dataFieldDefinition : dataSourceDefinition.getFieldDefinitions())
            {
                columnName = dataFieldDefinition.getSourceIdentifier();

                if (!tableColumns.contains(columnName)) LOGGER.log("Table " + tableName + " is MISSING column " + columnName);
            }

            dataConnection.getDatabaseAdaptor().logPrimaryKeyDifferences(tableName, dataSourceDefinition.getFieldDefinitions(), dataConnection);
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Logging the differences for table " + dataSourceDefinition.getTableName() + " could not be completed", sqle);
        }
    }

    public static class Column
    {
        private String columnName;
        private T8DataType dataType;
        private int Precision;
        private int Scale;
        private boolean nullable;
        private boolean primaryKey;

        @Override
        public int hashCode()
        {
            int hash = 5;
            hash = 89 * hash + (this.getColumnName() != null ? this.getColumnName().hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;

            final Column other = (Column)obj;
            return !((this.getColumnName() == null) ? (other.getColumnName() != null) : !this.columnName.equals(other.columnName));
        }

        /**
         * @return the columnName
         */
        public String getColumnName()
        {
            return columnName;
        }

        /**
         * @param columnName the columnName to set
         */
        public void setColumnName(String columnName)
        {
            this.columnName = columnName;
        }

        /**
         * @return the SQLColumnType
         */
        public T8DataType getDataType()
        {
            return dataType;
        }

        /**
         * @param SQLColumnType the SQLColumnType to set
         */
        public void setDataType(T8DataType dataType)
        {
            this.dataType = dataType;
        }

        /**
         * @return the Precision
         */
        public int getPrecision()
        {
            return Precision;
        }

        /**
         * @param Precision the Precision to set
         */
        public void setPrecision(int Precision)
        {
            this.Precision = Precision;
        }

        /**
         * @return the Scale
         */
        public int getScale()
        {
            return Scale;
        }

        /**
         * @param Scale the Scale to set
         */
        public void setScale(int Scale)
        {
            this.Scale = Scale;
        }

        /**
         * @return the nullable
         */
        public boolean isNullable()
        {
            return nullable;
        }

        /**
         * @param nullable the nullable to set
         */
        public void setNullable(boolean nullable)
        {
            this.nullable = nullable;
        }

        /**
         * @return the primaryKey
         */
        public boolean isPrimaryKey()
        {
            return primaryKey;
        }

        /**
         * @param primaryKey the primaryKey to set
         */
        public void setPrimaryKey(boolean primaryKey)
        {
            this.primaryKey = primaryKey;
        }
    }
}
