package com.pilog.t8.data;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.data.T8DataManagerResource;
import com.pilog.t8.definition.data.connection.direct.T8DirectDataConnectionDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.script.validation.entity.T8ServerEntityValidationScriptDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import com.pilog.t8.utilities.strings.ParameterizedString;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.data.T8DataManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DataManagerOperations
{
    public static class GetCacheSizesOperation extends T8DefaultServerOperation
    {
        public GetCacheSizesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Map<String, Integer> sizeMap;

            sizeMap = serverContext.getDataManager().getCacheSizes();
            return HashMaps.newHashMap(PARAMETER_CACHE_SIZE_MAP, sizeMap);
        }
    }

    public static class ClearCacheOperation extends T8DefaultServerOperation
    {
        public ClearCacheOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String cacheId;

            cacheId = (String)operationParameters.get(T8DataManagerResource.PARAMETER_CACHE_ID);
            serverContext.getDataManager().clearCache(cacheId);
            return null;
        }
    }

    public static class GetDataSessionDetailsOperation extends T8DefaultServerOperation
    {
        public GetDataSessionDetailsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8DataSessionDetails> sessionDetailsList;

            sessionDetailsList = serverContext.getDataManager().getDataSessionDetails(context);
            return HashMaps.newHashMap(PARAMETER_DATA_SESSION_DETAILS_LIST, sessionDetailsList);
        }
    }

    public static class KillDataSessionOperation extends T8DefaultServerOperation
    {
        public KillDataSessionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String sessionIdentifier;

            sessionIdentifier = (String)operationParameters.get(T8DataManagerResource.PARAMETER_DATA_SESSION_IDENTIFIER);
            if(Strings.isNullOrEmpty(sessionIdentifier)) throw new Exception("No session Identifier Provided. A session identifier must be provided using the parameter " + T8DataManagerResource.PARAMETER_DATA_SESSION_IDENTIFIER);
            serverContext.getDataManager().killDataSession(sessionIdentifier);
            return null;
        }
    }

    public static class RetrieveDataSourceFieldDefinitionsOperation extends T8DefaultServerOperation
    {
        public RetrieveDataSourceFieldDefinitionsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataSource dataSource;
            String dataSourceId;
            String projectId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            dataSourceId = (String)operationParameters.get(PARAMETER_DATA_SOURCE_IDENTIFIER);
            dataSource = getDataSource(projectId, dataSourceId);
            if (dataSource != null)
            {
                try
                {
                    dataSource.open();
                    return HashMaps.newHashMap(PARAMETER_FIELD_DEFINITION_LIST, dataSource.retrieveFieldDefinitions());
                }
                finally
                {
                    dataSource.close();
                }
            }
            else
            {
                T8Log.log("Data Source not found: " + dataSourceId);
                return null;
            }
        }

        private T8DataSource getDataSource(String projectId, String dataSourceId) throws Exception
        {
            T8DataSource dataSource;
            T8DataSourceDefinition dataSourceDefinition;
            T8DefinitionManager definitionManager;
            T8DataManager dataManager;

            definitionManager = serverContext.getDefinitionManager();
            dataManager = serverContext.getDataManager();
            dataSourceDefinition = (T8DataSourceDefinition)definitionManager.getInitializedDefinition(context, projectId, dataSourceId, null);
            if (dataSourceDefinition != null)
            {
                dataSource = dataSourceDefinition.getNewDataSourceInstance(dataManager.getCurrentSession().instantTransaction());
                if (dataSource != null)
                {
                    return dataSource;
                }
                else throw new Exception("Data source could not be initialized: " + dataSourceId);
            }
            else throw new Exception("Data source not found: " + dataSourceId);
        }
    }

    public static class RetrieveDataEntityOperation extends T8DefaultServerOperation
    {
        public RetrieveDataEntityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String entityIdentifier;
            Map<String, Object> fieldValueMap;
            T8DataSession dataSession;

            entityIdentifier = (String)operationParameters.get(PARAMETER_DATA_ENTITY_IDENTIFIER);
            fieldValueMap = (Map<String, Object>)operationParameters.get(PARAMETER_FIELD_VALUE_MAP);
            dataSession = serverContext.getDataManager().getCurrentSession();
            return HashMaps.newHashMap(PARAMETER_DATA_ENTITY, dataSession.instantTransaction().retrieve(entityIdentifier, fieldValueMap));
        }
    }

    public static class InsertDataEntityOperation extends T8DefaultServerOperation
    {
        public InsertDataEntityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataEntity dataEntity;

            dataEntity = (T8DataEntity)operationParameters.get(PARAMETER_DATA_ENTITY);
            tx.insert(dataEntity);
            return null;
        }
    }

    public static class UpdateDataEntityOperation extends T8DefaultServerOperation
    {
        public UpdateDataEntityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataEntity dataEntity;

            dataEntity = (T8DataEntity)operationParameters.get(PARAMETER_DATA_ENTITY);
            if (tx.update(dataEntity))
            {
                return HashMaps.newHashMap(PARAMETER_SUCCESS, true);
            }
            else
            {
                return HashMaps.newHashMap(PARAMETER_SUCCESS, false);
            }
        }
    }

    public static class CreateDataEntityOperation extends T8DefaultServerOperation
    {
        public CreateDataEntityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String entityIdentifier;
            Map<String, Object> fieldValueMap;

            entityIdentifier = (String)operationParameters.get(PARAMETER_DATA_ENTITY_IDENTIFIER);
            fieldValueMap = (Map)operationParameters.get(PARAMETER_FIELD_VALUE_MAP);

            return HashMaps.createSingular(PARAMETER_DATA_ENTITY, this.tx.create(entityIdentifier, fieldValueMap));
        }
    }

    public static class DeleteDataEntityOperation extends T8DefaultServerOperation
    {
        public DeleteDataEntityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataEntity dataEntity;

            dataEntity = (T8DataEntity)operationParameters.get(PARAMETER_DATA_ENTITY);
            if (tx.delete(dataEntity))
            {
                return HashMaps.newHashMap(PARAMETER_SUCCESS, true);
            }
            else
            {
                return HashMaps.newHashMap(PARAMETER_SUCCESS, false);
            }
        }
    }

    public static class SelectDataEntitiesOperation extends T8DefaultServerOperation
    {
        public SelectDataEntitiesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, List<T8DataEntity>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String dataEntityIdentifier;
            T8DataFilter filter;
            Integer startOffset;
            Integer pageSize;

            dataEntityIdentifier = (String)operationParameters.get(PARAMETER_DATA_ENTITY_IDENTIFIER);
            filter = (T8DataFilter)operationParameters.get(PARAMETER_DATA_FILTER);
            startOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);

            if ((startOffset != null) && (pageSize != null))
            {
                return HashMaps.createSingular(PARAMETER_DATA_ENTITY_LIST, this.tx.select(dataEntityIdentifier, filter, startOffset, pageSize));
            } else return HashMaps.createSingular(PARAMETER_DATA_ENTITY_LIST, this.tx.select(dataEntityIdentifier, filter));
        }
    }

    public static class CountDataEntitiesOperation extends T8DefaultServerOperation
    {
        public CountDataEntitiesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Integer> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String dataEntityIdentifier;
            T8DataFilter filter;
            T8DataSession dataSession;

            dataEntityIdentifier = (String)operationParameters.get(PARAMETER_DATA_ENTITY_IDENTIFIER);
            filter = (T8DataFilter)operationParameters.get(PARAMETER_DATA_FILTER);

            dataSession = serverContext.getDataManager().getCurrentSession();
            return HashMaps.createSingular(PARAMETER_DATA_ENTITY_COUNT, dataSession.instantTransaction().count(dataEntityIdentifier, filter));
        }
    }

    public static class ValidateDataEntitiesOperation extends T8DefaultServerOperation
    {
        public ValidateDataEntitiesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ServerEntityValidationScriptDefinition scriptDefinition;
            List<T8DataEntity> entityList;

            entityList = (List<T8DataEntity>)operationParameters.get(PARAMETER_DATA_ENTITY_LIST);
            scriptDefinition = (T8ServerEntityValidationScriptDefinition)operationParameters.get(PARAMETER_DATA_ENTITY_VALIDATION_SCRIPT_DEFINITION);
            return HashMaps.newHashMap(PARAMETER_DATA_ENTITY_VALIDATION_REPORT_LIST, serverContext.getDataManager().validateDataEntities(context, entityList, scriptDefinition));
        }
    }

    public static class GetSQLQueryString extends T8DefaultServerOperation
    {
        public GetSQLQueryString(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DataSourceDefinition dataSourceDefinition;
            T8DataSource dataSource;
            String projectId;
            String dataSourceId;
            String queryString = null;
            ParameterizedString parameterizedString;
            T8SQLBasedDataSource propertyPivotDataSource;
            T8DataEntityDefinition tempEntityDefinition;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            dataSourceId = (String)operationParameters.get(PARAMETER_DATA_SOURCE_IDENTIFIER);

            dataSourceDefinition = (T8DataSourceDefinition) serverContext.getDefinitionManager().getInitializedDefinition(context, projectId, dataSourceId, operationParameters);
            dataSource = dataSourceDefinition.getNewDataSourceInstance(serverContext.getDataManager().getCurrentSession().instantTransaction());

            propertyPivotDataSource = (T8SQLBasedDataSource) dataSource;
            tempEntityDefinition = T8DataUtilities.createEntityDefinition("TEMP", dataSourceDefinition);

            if(dataSource instanceof T8SQLBasedDataSource)
            {
                propertyPivotDataSource.open();
                parameterizedString = propertyPivotDataSource.getQueryString(tempEntityDefinition, null, true, 0, -1);
                propertyPivotDataSource.close();

                queryString = parameterizedString.getStringValue().toString();
                for (Object parameter : parameterizedString.getParameterValues())
                {
                    queryString = queryString.replaceFirst("\\?", "'" + parameter + "'");
                }
            }

            return HashMaps.newHashMap(PARAMETER_SQL_QUERY_STRING, queryString);
        }
    }

    public static class TestDirectConnectionConnectionOperation extends T8DefaultServerOperation
    {
        public TestDirectConnectionConnectionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DirectDataConnectionDefinition connectionDefinition;
            String connectionId;
            String projectId;

            projectId = (String)operationParameters.get(T8DataManagerResource.PARAMETER_PROJECT_ID);
            connectionId = (String)operationParameters.get(T8DataManagerResource.PARAMETER_CONNECTION_IDENTIFIER);
            connectionDefinition = (T8DirectDataConnectionDefinition) serverContext.getDefinitionManager().getInitializedDefinition(context, projectId, connectionId, null);

            try
            {
                T8DataConnection newConnectionInstance;
                T8DataConnectionPool dataConnectionPool;

                dataConnectionPool = serverContext.getDataManager().getDataConnectionPool(connectionDefinition.getIdentifier());

                //If the connection pool already exists then just reset the connection pool settings
                if(dataConnectionPool != null)
                {
                    dataConnectionPool.setConnectionURL(connectionDefinition.getConnectionString());
                    dataConnectionPool.setUsername(connectionDefinition.getUsername());
                    dataConnectionPool.setPassword(connectionDefinition.getPassword());
                }

                newConnectionInstance = connectionDefinition.createNewConnectionInstance(serverContext.getDataManager().getCurrentSession().instantTransaction());

                newConnectionInstance.createStatement().executeQuery("SELECT 1").next();
                newConnectionInstance.close();
            }
            catch (Exception e)
            {
                T8Log.log("Failed to test data connection", e);
                return HashMaps.newHashMap(PARAMETER_SUCCESS, false, PARAMETER_FAILURE_MESSAGE, ExceptionUtilities.getRootCauseMessage(e));
            }

            return HashMaps.newHashMap(PARAMETER_SUCCESS, true);
        }
    }
}
