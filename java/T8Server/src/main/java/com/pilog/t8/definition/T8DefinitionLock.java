package com.pilog.t8.definition;

import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.T8DefinitionLockDetails;
import com.pilog.t8.T8SessionContext;
import java.io.Serializable;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionLock implements Serializable
{
    private T8DefinitionHandle definitionHandle;
    private String userIdentifier;
    private String userProfileIdentifier;
    private String keyIdentifier;
    private long lockTime;
    
    public T8DefinitionLock(T8DefinitionHandle definitionHandle, String keyIdentifier, long lockTime, T8SessionContext sessionContext)
    {
        this.definitionHandle = definitionHandle;
        this.userIdentifier = sessionContext != null ? sessionContext.getUserIdentifier() : null;
        this.userProfileIdentifier = sessionContext != null ? sessionContext.getUserProfileIdentifier() : null;
        this.keyIdentifier = keyIdentifier;
        this.lockTime = lockTime;
    }
    
    public T8DefinitionHandle getDefinitionHandle()
    {
        return definitionHandle;
    }

    public String getDefinitionIdentifier()
    {
        return definitionHandle.getDefinitionIdentifier();
    }

    public String getKeyIdentifier()
    {
        return keyIdentifier;
    }

    public long getLockTime()
    {
        return lockTime;
    }

    public String getProjectIdentifier()
    {
        return definitionHandle.getProjectIdentifier();
    }

    public String getUserIdentifier()
    {
        return userIdentifier;
    }

    public String getUserProfileIdentifier()
    {
        return userProfileIdentifier;
    }

    public void setDefinitionHandle(T8DefinitionHandle definitionHandle)
    {
        this.definitionHandle = definitionHandle;
    }

    public void setUserIdentifier(String userIdentifier)
    {
        this.userIdentifier = userIdentifier;
    }

    public void setUserProfileIdentifier(String userProfileIdentifier)
    {
        this.userProfileIdentifier = userProfileIdentifier;
    }

    public void setKeyIdentifier(String keyIdentifier)
    {
        this.keyIdentifier = keyIdentifier;
    }

    public void setLockTime(long lockTime)
    {
        this.lockTime = lockTime;
    }
    
    public T8DefinitionLockDetails getDetails()
    {
        return new T8DefinitionLockDetails(definitionHandle.getDefinitionIdentifier(), definitionHandle.getProjectIdentifier(), userIdentifier, userProfileIdentifier, true);
    }
}
