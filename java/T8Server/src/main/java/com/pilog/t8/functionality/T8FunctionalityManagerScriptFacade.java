package com.pilog.t8.functionality;

import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityManagerScriptFacade
{
    private final T8FunctionalityManager functionalityManager;
    private final T8Context context;

    public T8FunctionalityManagerScriptFacade(T8Context context, T8FunctionalityManager functionalityManager)
    {
        this.functionalityManager = functionalityManager;
        this.context = context;
    }

    public T8FunctionalityAccessHandle accessFunctionality(String functionalityId, Map<String, Object> inputParameters) throws Exception
    {
        return functionalityManager.accessFunctionality(context, functionalityId, inputParameters);
    }
}
