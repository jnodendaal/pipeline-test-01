package com.pilog.t8.system.monitor;

import com.pilog.t8.data.T8DataSessionDetails;
import com.pilog.t8.server.monitor.T8ServerMonitorReport;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.system.monitor.T8ServerMonitorAPIHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8ServerMonitorOperations
{
    public static class GetT8ServerMonitorReport extends T8DefaultServerOperation
    {
        public GetT8ServerMonitorReport(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8DataSessionDetails> dataSessionDetails;
            T8ServerMonitorReport report;
            Runtime javaRuntime;
            int totalActiveDataTransactions;

            // Create the new report object.
            report = new T8ServerMonitorReport();
            report.setReportTime(System.currentTimeMillis());

            // Get the java runtime values.
            javaRuntime = Runtime.getRuntime();
            report.setJavaFreeMemory(javaRuntime.freeMemory());
            report.setJavaMaxMemory(javaRuntime.maxMemory());
            report.setJavaTotalMemory(javaRuntime.totalMemory());

            // Get the session details.
            report.setTotalActiveSessions(serverContext.getSecurityManager().getAllSessionDetails(context).size());

            // Get the data session details.
            dataSessionDetails = serverContext.getDataManager().getDataSessionDetails(context);
            totalActiveDataTransactions = 0;
            totalActiveDataTransactions = dataSessionDetails.stream().map((sessionDetails) -> sessionDetails.getActiveTransactionCount()).reduce(totalActiveDataTransactions, Integer::sum);
            report.setTotalActiveDataSessions(dataSessionDetails.size());
            report.setTotalActiveDataTransactions(totalActiveDataTransactions);

            // Get other system information.
            report.setTotalQueuedCommunications(serverContext.getCommunicationManager().getQueuedMessageCount(null));
            report.setTotalRunningOperations(serverContext.getServerOperationStatusReports(context).size() - 1); //We do not want to include this operation as running

            // Get the flow information.
            report.setTotalCachedFlows(serverContext.getFlowManager().getCachedFlowCount(context));

            // Return the report as output.
            return HashMaps.create(T8ServerMonitorAPIHandler.PARAMETER_REPORT, report);
        }
    }
}