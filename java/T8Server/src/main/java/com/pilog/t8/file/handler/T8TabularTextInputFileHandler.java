package com.pilog.t8.file.handler;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.file.T8FileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.Maps;
import com.pilog.t8.utilities.files.text.TabularTextInputFile;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TabularTextInputFileHandler implements T8FileHandler
{
    private final T8Context context;
    private final T8FileManager fileManager;
    private TabularTextInputFile textInputFile;
    private final String fileContextIid;
    private final String filePath;
    private boolean trimStrings;
    private boolean convertEmptyStringsToNull;
    private String encoding;
    private String separator;

    public static final String FILE_HANDLER_ID = "@FILE_TABULAR_TEXT_INPUT";

    public T8TabularTextInputFileHandler(T8Context context, T8FileManager fileManager, String contextIid, String filePath)
    {
        this.context = context;
        this.fileManager = fileManager;
        this.fileContextIid = contextIid;
        this.filePath = filePath;
        this.trimStrings = true;
        this.convertEmptyStringsToNull = true;
        this.encoding = "UTF-8";
        this.separator = "\t";
    }

    public boolean isTrimStrings()
    {
        return trimStrings;
    }

    public void setTrimStrings(boolean trimStrings)
    {
        this.trimStrings = trimStrings;
    }

    public boolean isConvertEmptyStringsToNull()
    {
        return convertEmptyStringsToNull;
    }

    public void setConvertEmptyStringsToNull(boolean convertEmptyStringsToNull)
    {
        this.convertEmptyStringsToNull = convertEmptyStringsToNull;
    }

    public String getEncoding()
    {
        return encoding;
    }

    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    public String getSeparator()
    {
        return separator;
    }

    public void setSeparator(String separator)
    {
        this.separator = separator;
    }

    public boolean isOpen()
    {
        return (textInputFile != null);
    }

    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIid, filePath);
    }

    @Override
    public void open() throws Exception
    {
        InputStream inputStream;

        inputStream = fileManager.getFileInputStream(context, fileContextIid, filePath);
        textInputFile = new TabularTextInputFile(encoding, separator);
        textInputFile.openFile(inputStream);
    }

    @Override
    public void close() throws Exception
    {
        if (textInputFile != null) textInputFile.closeFile();
        textInputFile = null;
    }

    public int getRowCount() throws Exception
    {
        InputStream inputStream;

        inputStream = fileManager.getFileInputStream(context, fileContextIid, filePath);
        return textInputFile.countRows(inputStream);
    }

    public Map<String, Object> readRow() throws Exception
    {
        HashMap<String, Object> dataRow;

        // Read the next data row from the input file.
        dataRow = textInputFile.readDataRow();
        if (dataRow != null)
        {
            // Do some trimming if required.
            if (trimStrings) Maps.trimStringValues(dataRow, convertEmptyStringsToNull);
            else if (convertEmptyStringsToNull) Maps.setEmptyStringValuesToNull(dataRow);

            // Return the data row.
            return dataRow;
        }
        else return null; // We have to return null if the row read from the file is null, to allow the invoker of this method to handle the end of file event.
    }
}
