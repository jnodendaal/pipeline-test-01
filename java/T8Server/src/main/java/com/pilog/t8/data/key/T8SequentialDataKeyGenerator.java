package com.pilog.t8.data.key;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataKeyGenerator;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.data.key.T8SequentialDataKeyGeneratorDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8SequentialDataKeyGenerator implements T8DataKeyGenerator
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8SequentialDataKeyGenerator.class.getName());

    private final T8ServerContext serverContext;
    private final T8SequentialDataKeyGeneratorDefinition definition;
    private final Queue<Key> keyQueue = new LinkedBlockingQueue<Key>(); // Stores all prefeched keys.
    private final T8DataKeyStatePersistenceHandler stateHandler;
    private Key currentKey; // Stores the last key provided by this generator on client request.
    private int batchSize; // The number of keys that will be generated at a time.

    public T8SequentialDataKeyGenerator(T8Context context, T8SequentialDataKeyGeneratorDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.definition = definition;
        this.batchSize = definition.getFetchSize();
        this.stateHandler = new T8DataKeyStatePersistenceHandler(context, definition.getIdentifier());
    }

    @Override
    public void init() throws Exception
    {
        // Make sure we have a valid batch size.
        if (batchSize < 1) batchSize = 1;

        // Initialize the current key
        currentKey = new Key(-1);

        // Generate the first batch of keys.
        generateKeyBatch();
    }

    @Override
    public void destroy()
    {
        try
        {
            // Save the next seed to be used when this generator is restarted.
            stateHandler.saveSeedValue(currentKey.getSeed() + 1);
        }
        catch (Exception ex)
        {
            LOGGER.log("CRITICAL:  Failed to save key sate for " + definition.getPublicIdentifier() + ".  Seed value to be saved: " + currentKey.getSeed() + 1, ex);
        }
    }

    /**
     * This method generates a new sequential key. The keys are retrieved in
     * batches determined by the size set in the definition, when a batch runs
     * out of keys the next batch will be retrieved, inserted into the queue and
     * the state of saved to last key retrieved.  Keys are retrieved from a
     * blocking queue and are thread safe.
     * @return The next available key in the queue.
     * @throws Exception Throws and exception when there are no more keys
     * available to be given, or when the length of the generated value exceeds
     * the maximum length specified in the definition.
     */
    @Override
    public String generateKey() throws Exception
    {
        Key nextKey;

        // Get the next key from the queue.  If we found no keys in the queue, generate a new batch.
        nextKey = keyQueue.poll();
        if (nextKey == null)
        {
            // Generate a new key batch.
            generateKeyBatch();
            nextKey = keyQueue.poll();
        }

        // If we found a key, use it.
        if (nextKey != null)
        {
            // Save the current key.
            synchronized (currentKey)
            {
                currentKey = nextKey;
            }

            // Return the generated string value of the next key.
            return nextKey.getGeneratedValue();
        }
        else throw new Exception("The maximum allowed keys for " + definition.getPublicIdentifier() + " have been reached. Increase the maximum amount in the definition to get more keys.");
    }

    /**
     * Generates the next batch of keys but fetching the current
     */
    private void generateKeyBatch() throws Exception
    {
        // Only allow one thread to update the new keys.
        synchronized (keyQueue)
        {
            List<Key> generatedKeys;
            int seedValue;
            int maxSeed;

            // Get the next seed value from the state.
            seedValue = stateHandler.retrieveSeedValue(definition.getFirstValue());

            // Determine the maximum seed value to be generated for this batch.
            maxSeed = (seedValue + batchSize);
            if (definition.isLastValueEnabled() && maxSeed > definition.getLastValue()) maxSeed = definition.getLastValue();

            // Add the next set of keys to the queue.
            generatedKeys = new ArrayList<Key>();
            while (seedValue <= maxSeed)
            {
                generatedKeys.add(new Key(seedValue++));
            }

            // Save the seed value so that it is available for the next batch generation.
            stateHandler.saveSeedValue(seedValue);

            // Add all of the generated keys to the queue.
            keyQueue.addAll(generatedKeys);
        }
    }

    private class Key
    {
        private String generatedValue;
        private Integer seed;

        public Key(Integer seed)
        {
            this.seed = seed;
        }

        /**
         * The seed will be used to determine the value of the generated key.
         * <p/>
         * @return the seed
         */
        public Integer getSeed()
        {
            return seed;
        }

        /**
         * @param seed The seed that will be used to determine the value of the generated key
         */
        public void setSeed(Integer seed)
        {
            this.seed = seed;
        }

        /**
         * The generated value is determined in the following ways:
         * 1. If there is a prefix in the definition, that prefix will be prepended to the generated seed.
         * 2. If there is a suffix in the definition, that suffix will be appended to the generated seed after padding has been applied.
         * 3. If there a key length specified in the definition, the generated seed will be padded based on the padding character and the padding direction. If
         * the padding direction was set as before, padding will be applied after the prefix and before the seed, if the padding direction was set as after, then
         * padding will be applied after the seed but before the suffix.
         * <p/>
         * @return the generatedValue The generated value with all the rules applied.
         * <p/>
         * @exception throws and exception if the generated value exceeds the maximum length allowed.
         */
        public String getGeneratedValue() throws Exception
        {
            //Generate the key String on demand
            if (generatedValue == null)
            {
                //Initialize the StringBuilder with the seed length, because it will be that in the minumum scenario
                StringBuilder newKey = new StringBuilder(seed.toString().length());

                if (!Strings.isNullOrEmpty(definition.getPrefix()))
                {
                    newKey.append(definition.getPrefix());
                }

                //check if we should pad before the value
                if (definition.getPaddingDirection() == T8SequentialDataKeyGeneratorDefinition.PaddingDirection.BEFORE)
                {
                    newKey = addPadding(newKey, seed);
                }

                //add the value
                newKey.append(seed.toString());

                //Check if we shoud pad after
                if (definition.getPaddingDirection() == T8SequentialDataKeyGeneratorDefinition.PaddingDirection.AFTER)
                {
                    newKey = addPadding(newKey, seed);
                }

                if (!Strings.isNullOrEmpty(definition.getSuffix()))
                {
                    newKey.append(definition.getSuffix());
                }

                generatedValue = newKey.toString();

                if (generatedValue.length() > definition.getLength())
                {
                    throw new Exception("Key length exceeded. The key " + generatedValue + " exceeds the maximum allowed limit of " + definition.getLength() + ". Increase the maximum allowed length or reduce the size of the prefix/suffix.");
                }
            }
            return generatedValue;
        }

        private StringBuilder addPadding(StringBuilder stringBuilder,
                                         Integer value) throws Exception
        {
            int padAmount = getPaddingAmount(value);
            String padChar = definition.getPaddingCharacter();
            //Check that the padding char has a value, if not default to 0
            if (Strings.isNullOrEmpty(padChar))
            {
                padChar = "0";
            }
            //Lets check that the padding character is indeed one in size, if not, make it so
            if (padChar.length() > 1)
            {
                padChar = padChar.substring(1, padChar.length() - 1);
            }
            //If this padding amount is less than zero then this string will exceed the maximum length amount as it is
            if (padAmount < 0)
            {
                throw new Exception("Key length exceeded. The key value " + value + " allong with prefix and suffix will exceed the maximum allowed limit of " + definition.getLength() + ". Increase the maximum allowed length or reduce the size of the prefix/suffix.");
            }
            for (int i = 1; i <= padAmount; i++)
            {
                stringBuilder.append(padChar);
            }
            return stringBuilder;
        }

        private int getPaddingAmount(Integer value)
        {
            if (!definition.isLengthLimited())
            {
                return 0;
            }

            int prefixLength = 0;
            int suffixLenth = 0;
            int valueLength = value.toString().length();
            if (!Strings.isNullOrEmpty(definition.getPrefix()))
            {
                prefixLength = definition.getPrefix().length();
            }

            if (!Strings.isNullOrEmpty(definition.getSuffix()))
            {
                suffixLenth = definition.getSuffix().length();
            }

            return definition.getLength() - prefixLength - suffixLenth - valueLength;
        }
    }
}
