package com.pilog.t8.file;

import com.pilog.t8.time.T8TimeUnit;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

import static com.pilog.t8.definition.file.T8FileManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8FileManagerOperations
{
    public static class OpenFileContextOperation extends T8DefaultServerOperation
    {
        public OpenFileContextOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;
            String fileContextID;
            T8TimeUnit expirationTime;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            fileContextID = (String)operationParameters.get(PARAMETER_CONTEXT_ID);
            expirationTime = (T8TimeUnit)operationParameters.get(PARAMETER_EXPIRATION_TIME);
            return HashMaps.newHashMap(PARAMETER_SUCCESS, serverContext.getFileManager().openFileContext(context, fileContextInstanceID, fileContextID, expirationTime));
        }
    }

    public static class CloseFileContextOperation extends T8DefaultServerOperation
    {
        public CloseFileContextOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            return HashMaps.newHashMap(PARAMETER_SUCCESS, serverContext.getFileManager().closeFileContext(context, fileContextInstanceID));
        }
    }

    public static class CreateDirectoryOperation extends T8DefaultServerOperation
    {
        public CreateDirectoryOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;
            String filePath;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            return HashMaps.newHashMap(PARAMETER_SUCCESS, serverContext.getFileManager().createDirectory(context, fileContextInstanceID, filePath));
        }
    }

    public static class DeleteFileOperation extends T8DefaultServerOperation
    {
        public DeleteFileOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;
            String filePath;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            return HashMaps.newHashMap(PARAMETER_SUCCESS, serverContext.getFileManager().deleteFile(context, fileContextInstanceID, filePath));
        }
    }

    public static class UploadFileDataOperation extends T8DefaultServerOperation
    {
        public UploadFileDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;
            String filePath;
            byte[] bytes;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            bytes = (byte[])operationParameters.get(PARAMETER_BINARY_DATA);
            return HashMaps.newHashMap(PARAMETER_BYTE_SIZE, serverContext.getFileManager().uploadFileData(context, fileContextInstanceID, filePath, bytes));
        }
    }

    public static class DownloadFileDataOperation extends T8DefaultServerOperation
    {
        public DownloadFileDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;
            String filePath;
            long fileOffset;
            int byteSize;
            byte[] bytes;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            fileOffset = (Long)operationParameters.get(PARAMETER_FILE_OFFSET);
            byteSize = (Integer)operationParameters.get(PARAMETER_BYTE_SIZE);
            bytes = (byte[])operationParameters.get(PARAMETER_BINARY_DATA);
            return HashMaps.newHashMap(PARAMETER_BINARY_DATA, serverContext.getFileManager().downloadFileData(context, fileContextInstanceID, filePath, fileOffset, byteSize));
        }
    }

    public static class GetFileDetailsOperation extends T8DefaultServerOperation
    {
        public GetFileDetailsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String contextIid;
            String filePath;

            contextIid = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            return HashMaps.newHashMap(PARAMETER_FILE_DETAILS, serverContext.getFileManager().getFileDetails(context, contextIid, filePath));
        }
    }

    public static class GetFileListOperation extends T8DefaultServerOperation
    {
        public GetFileListOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;
            String filePath;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            return HashMaps.newHashMap(PARAMETER_FILE_DETAILS_LIST, serverContext.getFileManager().getFileList(context, fileContextInstanceID, filePath));
        }
    }

    public static class CheckFileContextExistenceOperation extends T8DefaultServerOperation
    {
        public CheckFileContextExistenceOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            return HashMaps.newHashMap(PARAMETER_SUCCESS, serverContext.getFileManager().fileContextExists(context, fileContextInstanceID));
        }
    }

    public static class CheckFileExistenceOperation extends T8DefaultServerOperation
    {
        public CheckFileExistenceOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;
            String filePath;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            return HashMaps.newHashMap(PARAMETER_SUCCESS, serverContext.getFileManager().fileExists(context, fileContextInstanceID, filePath));
        }
    }

    public static class GetFileSizeOperation extends T8DefaultServerOperation
    {
        public GetFileSizeOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String fileContextInstanceID;
            String filePath;

            fileContextInstanceID = (String)operationParameters.get(PARAMETER_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            return HashMaps.newHashMap(PARAMETER_BYTE_SIZE, serverContext.getFileManager().getFileSize(context, fileContextInstanceID, filePath));
        }
    }
}
