package com.pilog.t8.data.connection.pool;

import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.definition.data.connection.pool.T8H2DataConnectionPoolDefinition;
import java.sql.Connection;
import java.sql.SQLException;
import org.h2.jdbcx.JdbcConnectionPool;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8H2DataConnectionPool implements T8DataConnectionPool
{
    private T8H2DataConnectionPoolDefinition definition;
    private String connectionIdentifier;
    private JdbcConnectionPool cp;
    private String connectionURL;
    private String username;
    private String password;

    public T8H2DataConnectionPool(T8H2DataConnectionPoolDefinition definition, T8DataTransaction dataAccessProvider)
    {
        this.definition = definition;
    }

    @Override
    public String getDataConnectionIdentifier()
    {
        return connectionIdentifier;
    }

    @Override
    public Connection getConnection() throws SQLException
    {
        // Initialize the connection pool if it has not already been done.
        if (cp == null)
        {
            cp = JdbcConnectionPool.create(connectionURL, username, password);
            cp.setMaxConnections(definition.getMaxPoolSize());
        }

        // Return a new connection.
        return cp.getConnection();
    }

    @Override
    public void destroy()
    {
        cp.dispose();
    }

    @Override
    public void setDataConnectionIdentifier(String identifier)
    {
        connectionIdentifier = identifier;
    }

    @Override
    public void setDriverClassName(String className) throws Exception
    {
        // No driver name required.
    }

    @Override
    public void setConnectionURL(String connectionURL)
    {
        this.connectionURL = connectionURL;
    }

    @Override
    public void setUsername(String username)
    {
       this.username = username;
    }

    @Override
    public void setPassword(String password)
    {
        this.password = password;
    }
}
