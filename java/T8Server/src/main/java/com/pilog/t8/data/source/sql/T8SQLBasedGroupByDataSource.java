package com.pilog.t8.data.source.sql;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8SQLBasedDataSource;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.groupby.T8GroupByDataFilter;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.source.T8CommonStatementHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataConnectionBasedSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLAggregateFieldDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLBasedGroupByDataSourceDefinition;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author hennie.brink@pilog.co.za
 */
public class T8SQLBasedGroupByDataSource implements T8SQLBasedDataSource
{
    protected final T8SQLBasedGroupByDataSourceDefinition definition;
    protected final T8DataTransaction tx;
    protected T8SQLBasedDataSource originatingSQLBasedDataSource;
    protected T8DataSourceDefinition originatingSQLDataSourceDefinition;
    protected T8DataConnection connection;
    protected List<T8DataEntityResults> openEntityResults;
    protected T8Context context;
    protected T8PerformanceStatistics stats;
    protected Collection<T8SQLAggregateFieldDefinition> aggregateFieldDefinitions;

    public T8SQLBasedGroupByDataSource(T8SQLBasedGroupByDataSourceDefinition definition, T8DataTransaction tx) throws Exception
    {
        this.definition = definition;
        this.tx = tx;
        this.originatingSQLDataSourceDefinition = tx.getDataSourceDefinition(definition.getOriginatingDataSourceIdentifier());
        this.originatingSQLBasedDataSource = (T8SQLBasedDataSource) originatingSQLDataSourceDefinition.getNewDataSourceInstance(tx);
        if (this.originatingSQLBasedDataSource == null)
        {
            throw new IllegalArgumentException("Failed to create datasource instance using identifier " + definition.getOriginatingDataSourceIdentifier());
        }
        this.context = tx.getContext();
        this.openEntityResults = new ArrayList<T8DataEntityResults>();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.

        // Get the Aggregate Function Fields.
        aggregateFieldDefinitions = new ArrayList<T8SQLAggregateFieldDefinition>();
        for (T8DataSourceFieldDefinition dataSourceFieldDefinition : definition.getFieldDefinitions())
        {
            if (dataSourceFieldDefinition instanceof T8SQLAggregateFieldDefinition)
            {
                aggregateFieldDefinitions.add((T8SQLAggregateFieldDefinition) dataSourceFieldDefinition);
            }
        }
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null)
        {
            throw new IllegalArgumentException("Cannot set performance statistics as null.");
        }
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    /**
     * This method will return a select query based on the specified group by
     * fields identifiers of definition. If no field identifiers were found then
     * a normal select will be performed. The {@code T8Filter} passed through
     * will be intercepted and all the aggregated column values will be added to
     * a having clause.
     * <p/>
     * @param entityDefinition The entityDefinition
     * @param filter The data filter
     * <p/>
     * @return ParameterizedString containing the query and parameter values
     * <p/>
     * @throws Exception
     */
    @Override
    public ParameterizedString getQueryString(T8DataEntityDefinition entityDefinition, T8DataFilter filter, boolean includeWithClause, int pageOffset, int pageSize) throws Exception
    {
        ParameterizedString queryString;
        StringBuffer groupByFields;
        StringBuffer aggregateFields;
        ParameterizedString havingClause = null;
        T8GroupByDataFilter groupByFilter;
        ParameterizedString withClause;

        groupByFields = new StringBuffer(0);
        aggregateFields = new StringBuffer(0);

        //Make a copy of the incoming filter so that we know that we will always behave correctly if it was not of the correct type
        groupByFilter = null;
        if (filter instanceof T8GroupByDataFilter)
        {
            groupByFilter = (T8GroupByDataFilter) filter;
        } else
        {
            if (filter instanceof T8DataFilter)
            {
                groupByFilter = new T8GroupByDataFilter(filter.getEntityIdentifier(), filter.getFilterCriteria());
            }
        }

        // Check if we have any with expressions to add before the select clause and then Create the query string.
        withClause = getWithClause(entityDefinition, filter, pageOffset, pageSize);
        if ((withClause != null) && (!withClause.isEmpty()) && includeWithClause)
        {
            queryString = new ParameterizedString();
            queryString.append(withClause);
            queryString.append(" SELECT ");
        }
        else
        {
            queryString = new ParameterizedString("SELECT ");
        }

        // Intercept the filter to remove the having clause filters for the aggregate functions.
        if (groupByFilter != null && groupByFilter instanceof T8GroupByDataFilter)
        {
            havingClause = getHavingFilterString(removeAggregates(groupByFilter.copy(), false));
        }

        // Adds the group By fields to the select clause.
        Iterator<String> fieldIdentifierIterator = definition.getGroupByFieldIdentifiers().iterator();

        while (fieldIdentifierIterator.hasNext())
        {
            String fieldIdentifier = definition.getPublicIdentifier() + fieldIdentifierIterator.next();
            String sourceIdentifier = definition.mapFieldToSourceIdentifier(fieldIdentifier);
            groupByFields.append(sourceIdentifier);
            if (fieldIdentifierIterator.hasNext())
            {
                groupByFields.append(", ");
            }
        }

        queryString.append(groupByFields);

        // Adds all the aggregated fields to the select clause.
        for (T8DataSourceFieldDefinition t8DataSourceFieldDefinition : definition.getFieldDefinitions())
        {
            if (t8DataSourceFieldDefinition instanceof T8SQLAggregateFieldDefinition)
            {
                aggregateFields.append(", ");
                aggregateFields.append(((T8SQLAggregateFieldDefinition) t8DataSourceFieldDefinition).getAggregateFunctionString());
                aggregateFields.append(" AS ");
                aggregateFields.append(definition.mapFieldToSourceIdentifier(definition.getPublicIdentifier() + t8DataSourceFieldDefinition.getIdentifier()));
            }
        }

        queryString.append(aggregateFields);
        queryString.append(" FROM (");
        if (groupByFilter != null)
        {
            queryString.append(originatingSQLBasedDataSource.getQueryString(entityDefinition, groupByFilter.getInnerFilter(), false, pageOffset, pageSize));
        } else
        {
            queryString.append(originatingSQLBasedDataSource.getQueryString(entityDefinition, null, false, pageOffset, pageSize));
        }

        queryString.append(") groupByResults ");
        if (groupByFilter != null)
        {
            T8DataFilter whereFilter = removeAggregates(groupByFilter.copy(), true);
            if (whereFilter != null)
            {
                StringBuffer whereClause = whereFilter.getWhereClause(tx, "groupByResults");
                if (whereClause != null)
                {
                    queryString.append(whereClause, whereFilter.getWhereClauseParameters(tx));
                }
            }
        }

        // Append Group By Fields.
        if (groupByFields.length() > 1)
        {
            queryString.append(" GROUP BY ");
            queryString.append(groupByFields);
        }

        // Add the having clause.
        if (havingClause != null)
        {
            queryString.append(" HAVING ");
            queryString.append(havingClause);
        }

        return queryString;
    }

    private ParameterizedString getHavingFilterString(T8DataFilter havingFilter) throws Exception
    {
        //if there are no aggregate fields, return
        if (aggregateFieldDefinitions.isEmpty() || havingFilter == null || !havingFilter.hasFilterCriteria() || !havingFilter.getFilterCriteria().hasCriteria())
        {
            return null;
        }

        String where = havingFilter.getFilterCriteria().getWhereClause(tx, "groupByResults").toString();
        //We now have a having clause with the values for the Alias field, we need the aggregate functions in having so lets replace them
        for (T8SQLAggregateFieldDefinition t8SQLAggregateFieldDefinition : aggregateFieldDefinitions)
        {
            where = where.replaceAll(definition.mapFieldToSourceIdentifier(definition.getPublicIdentifier() + t8SQLAggregateFieldDefinition.getIdentifier()), t8SQLAggregateFieldDefinition.getAggregateFunctionString());
        }

        return new ParameterizedString(where, havingFilter.getWhereClauseParameters(tx));
    }

    private T8DataFilter removeAggregates(T8DataFilter filter, boolean removeAggregates)
    {
        for (T8DataFilterCriterion t8DataFilterCriterion : filter.getFilterCriterionList())
        {
            boolean isAggregateFieldB = false;
            for (T8SQLAggregateFieldDefinition dataSourceFieldDefinition : aggregateFieldDefinitions)
            {
                if (t8DataFilterCriterion.getFieldIdentifier().equals(filter.getEntityIdentifier() + dataSourceFieldDefinition.getIdentifier()))
                {
                    isAggregateFieldB = true;
                    if (removeAggregates)
                    {
                        filter.removeFilterClause(t8DataFilterCriterion);
                    }
                    break;
                }
            }
            if (!isAggregateFieldB && !removeAggregates)
            {
                filter.removeFilterClause(t8DataFilterCriterion);
            }
        }
        return filter;
    }

    @Override
    public ParameterizedString getCountQueryString(T8DataEntityDefinition entityDefinition, T8DataFilter filter, boolean includeWithClause) throws Exception
    {
        ParameterizedString queryString;
        ParameterizedString withClause;

        // Check if we have any with expressions to add before the select clause and then Create the query string.
        withClause = getWithClause(entityDefinition, filter, 0, -1);
        if ((withClause != null) && (!withClause.isEmpty()))
        {
            queryString = new ParameterizedString();
            queryString.append(withClause);
            queryString.append(" SELECT COUNT(*) AS RECORD_COUNT FROM ( ");
        }
        else
        {
            queryString = new ParameterizedString("SELECT COUNT(*) AS RECORD_COUNT FROM ( ");
        }

        queryString.append(getQueryString(entityDefinition, filter, false, 0, -1));
        queryString.append(") recordCount ");
        return queryString;
    }

    @Override
    public T8DataSourceDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void open() throws Exception
    {
        connection = tx.getDataConnection(((T8DataConnectionBasedSourceDefinition) originatingSQLDataSourceDefinition).getDataConnectionIdentifier());
        originatingSQLBasedDataSource.open();
    }

    @Override
    public void close() throws Exception
    {
        // Close any open entity results.
        for (T8DataEntityResults results : openEntityResults)
        {
            results.close();
        }
        originatingSQLBasedDataSource.close();
    }

    @Override
    public void commit() throws Exception
    {
        if (connection != null)
        {
            connection.commit();
        }
        originatingSQLBasedDataSource.commit();
    }

    @Override
    public void rollback() throws Exception
    {
        if (connection != null)
        {
            connection.rollback();
        }
        originatingSQLBasedDataSource.rollback();
    }

    @Override
    public void setParameters(Map<String, Object> parameters)
    {
    }

    @Override
    public List<T8DataSourceFieldDefinition> retrieveFieldDefinitions() throws Exception
    {
        List<T8DataSourceFieldDefinition> fieldDefinitions = new ArrayList<T8DataSourceFieldDefinition>();
        for (T8DataSourceFieldDefinition t8DataSourceFieldDefinition : definition.getFieldDefinitions())
        {
            if (t8DataSourceFieldDefinition instanceof T8SQLAggregateFieldDefinition)
            {
                fieldDefinitions.add(t8DataSourceFieldDefinition);
            }
        }
        for (T8DataSourceFieldDefinition t8DataSourceFieldDefinition : originatingSQLDataSourceDefinition.getFieldDefinitions())
        {
            for (String groubyFieldIdentifier : definition.getGroupByFieldIdentifiers())
            {
                if (t8DataSourceFieldDefinition.getIdentifier().equals(groubyFieldIdentifier))
                {
                    fieldDefinitions.add(t8DataSourceFieldDefinition);
                }
            }
        }
        return fieldDefinitions;
    }

    @Override
    public int count(T8DataFilter filter) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = tx.getDataEntityDefinition(filter.getEntityIdentifier());
        queryString = getCountQueryString(entityDefinition, filter, true);

        return T8CommonStatementHandler.executeCountQuery(context, connection, queryString, definition, entityDefinition, 0);
    }

    @Override
    public boolean exists(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return count(new T8DataFilter(entityIdentifier, keyMap)) > 0;
    }

    @Override
    public void insert(T8DataEntity dataEntity) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insert(List<T8DataEntity> dataEntities) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(T8DataEntity dataEntity) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(List<T8DataEntity> dataEntities) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(String entityIdentifier, Map<String, Object> updatedValues, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean delete(T8DataEntity dataEntity) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T8DataEntity retrieve(String entityIdentifier,
            Map<String, Object> keyMap) throws Exception
    {
        List<T8DataEntity> selectedObjects = select(entityIdentifier, keyMap);
        if (selectedObjects.size() > 0)
        {
            return selectedObjects.get(0);
        } else
        {
            return null;
        }
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier,
            Map<String, Object> keyMap) throws Exception
    {
        return select(entityIdentifier, keyMap != null
                ? new T8DataFilter(entityIdentifier, keyMap)
                : null);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier,
            T8DataFilter filter) throws Exception
    {
        return select(entityIdentifier, filter, 0, 50000);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier,
            Map<String, Object> keyMap, int startOffset,
            int pageSize) throws Exception
    {
        return select(entityIdentifier, keyMap != null
                ? new T8DataFilter(entityIdentifier, keyMap)
                : null, startOffset, pageSize);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        // Create the query string.
        queryString = getSelectQueryString(entityIdentifier, filter, pageOffset, pageSize);

        // Retrieve the results.
        return T8CommonStatementHandler.executeQuery(context, connection, queryString, definition, entityDefinition, pageOffset, pageSize, 0);
    }

    @Override
    public T8DataEntityResults scroll(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;
        T8DataEntityResults entityResults;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        // Create the query string.
        queryString = getSelectQueryString(entityIdentifier, filter, 0, -1);

        // Retrieve the results.
        entityResults = T8CommonStatementHandler.getEntityResults(context, connection, queryString, definition, entityDefinition);
        openEntityResults.add(entityResults);
        return entityResults;
    }

    @Override
    public ParameterizedString getWithClause(T8DataEntityDefinition entityDefinition, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        return originatingSQLBasedDataSource.getWithClause(entityDefinition, filter, pageOffset, pageSize);
    }

    @Override
    public int delete(String entityId, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private ParameterizedString getSelectQueryString(String entityId, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityId);

        // Create the query string.
        queryString = getQueryString(entityDefinition, filter, true, pageOffset, pageSize);

        //We can only add the order by clause on a normal select query, because the select and count use the same method we need to add it after the fact.
        StringBuffer orderByClause = null;
        if (filter != null)
        {
            orderByClause = filter.getOrderByClause(connection, entityDefinition, definition);
        }
        if (orderByClause != null)
        {
            queryString.append(" ");
            queryString.append(orderByClause);
        }

        return queryString;
    }

    @Override
    public int delete(List<T8DataEntity> dataEntities) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ParameterizedString getSelectClause(T8DataEntityDefinition entityDefinition, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
