package com.pilog.t8.process;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.process.T8Process.T8ProcessHistoryEvent;
import com.pilog.t8.process.T8Process.T8ProcessStatus;
import com.pilog.t8.state.T8ParameterPeristenceHandler;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterExpressionParser;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.process.T8ProcessManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessManagerDataHandler
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8DefinitionManager definitionManager;
    private boolean persistenceEnabled; // Flag that can be set to false to disable persistence of flow states.
    private final T8ParameterPeristenceHandler stateParameterPersistenceHandler;
    private final T8ParameterPeristenceHandler historyParameterPersistenceHandler;

    public T8ProcessManagerDataHandler(T8Context internalContext)
    {
        this.serverContext = internalContext.getServerContext();
        this.internalContext = internalContext;
        this.definitionManager = serverContext.getDefinitionManager();
        this.persistenceEnabled = true;
        this.stateParameterPersistenceHandler = new T8ParameterPeristenceHandler(PROCESS_STATE_PAR_DE_IDENTIFIER);
        this.historyParameterPersistenceHandler = new T8ParameterPeristenceHandler(PROCESS_HISTORY_PAR_DE_IDENTIFIER);
    }

    public void setEnabled(boolean enabled)
    {
        persistenceEnabled = enabled;
    }

    public void updateState(T8ProcessState state) throws Exception
    {
        if (persistenceEnabled)
        {
            T8DataSession session;
            T8DataTransaction xtx;
            T8DataTransaction tx;

            // Get the transaction to use (suspend any external transaction).
            session = serverContext.getDataManager().getCurrentSession();
            xtx = session.suspend();
            tx = session.beginTransaction();

            try
            {
                T8DataEntity entity;

                // Create a new state entity.
                entity = createStateEntity(tx, state);

                // Update the entity.
                tx.update(entity);

                // Commit the transaction.
                tx.commit();
            }
            catch (Exception e)
            {
                tx.rollback();
                throw e;
            }
            finally
            {
                // If an external session was present, resume it.
                if (xtx != null) session.resume(xtx);
            }
        }
    }

    public void insertState(T8ProcessState state) throws Exception
    {
        if (persistenceEnabled)
        {
            T8DataSession session;
            T8DataTransaction xtx;
            T8DataTransaction tx;

            // Get the transaction to use (suspend any external transaction).
            session = serverContext.getDataManager().getCurrentSession();
            xtx = session.suspend();
            tx = session.beginTransaction();

            try
            {
                T8DataEntity entity;

                // Create a new state entity.
                entity = createStateEntity(tx, state);

                // Insert the entity.
                tx.insert(entity);

                // Insert the state parameters.
                stateParameterPersistenceHandler.insertParameters(tx, HashMaps.newHashMap(PROCESS_STATE_PAR_DE_IDENTIFIER + F_PROCESS_IID, state.getIid()), null, T8IdentifierUtilities.stripNamespace(state.getParameters()));

                // Commit the transaction.
                tx.commit();
            }
            catch (Exception e)
            {
                tx.rollback();
                throw e;
            }
            finally
            {
                // If an external session was present, resume it.
                if (xtx != null) session.resume(xtx);
            }
        }
    }

    public void deleteState(String processIid) throws Exception
    {
        if (persistenceEnabled)
        {
            T8DataTransaction tx;

            // Get the transaction to use.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

            // Delete process state parameters.
            tx.delete(PROCESS_STATE_PAR_DE_IDENTIFIER, new T8DataFilter(PROCESS_STATE_PAR_DE_IDENTIFIER, HashMaps.newHashMap(PROCESS_STATE_PAR_DE_IDENTIFIER + F_PROCESS_IID, processIid)));

            // Delete process state.
            tx.delete(PROCESS_STATE_DE_IDENTIFIER, new T8DataFilter(PROCESS_STATE_DE_IDENTIFIER, HashMaps.newHashMap(PROCESS_STATE_DE_IDENTIFIER + F_PROCESS_IID, processIid)));
        }
    }

    public int countStates(T8Context context, T8ProcessFilter processFilter, String searchString) throws Exception
    {
        T8DataFilterExpressionParser parser;
        T8DataTransaction tx;
        T8DataFilter filter;

        // Parse the search expression and create a filter from it.
        parser = new T8DataFilterExpressionParser(PROCESS_STATE_DE_IDENTIFIER, ArrayLists.newArrayList(PROCESS_STATE_DE_IDENTIFIER + F_PROCESS_DISPLAY_NAME));
        parser.setWhitespaceConjunction(T8DataFilterClause.DataFilterConjunction.AND);
        filter = parser.parseExpression(searchString, false);
        filter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, processFilter.getDataFilter(context, PROCESS_STATE_DE_IDENTIFIER));

        // Count process states using the created filter.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        return tx.count(PROCESS_STATE_DE_IDENTIFIER, filter);
    }

    public List<T8ProcessState> searchStates(T8Context context, T8ProcessFilter processFilter, String searchString, int pageOffset, int pageSize) throws Exception
    {
        T8DataFilterExpressionParser parser;
        T8DataFilter filter;

        // Parse the search expression and create a filter from it.
        parser = new T8DataFilterExpressionParser(PROCESS_STATE_DE_IDENTIFIER, ArrayLists.newArrayList(PROCESS_STATE_DE_IDENTIFIER + F_PROCESS_DISPLAY_NAME));
        parser.setWhitespaceConjunction(T8DataFilterClause.DataFilterConjunction.AND);
        filter = parser.parseExpression(searchString, false);
        filter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, processFilter.getDataFilter(context, PROCESS_STATE_DE_IDENTIFIER));

        // Retrieve process states using the created filter.
        return retrieveStates(filter, pageOffset, pageSize);
    }

    public T8ProcessSummary retrieveProcessSummary(T8Context context, T8ProcessFilter filter) throws Exception
    {
        List<T8DataEntity> entityList;
        T8ProcessSummary summary;
        T8DataSession dataSession;
        T8DataTransaction tx;
        String filterEntityId;
        String entityId;

        // Get the identifiers for the entities to use.
        entityId = PROCESS_STATE_SUMMARY_DE_IDENTIFIER;
        filterEntityId = PROCESS_STATE_DE_IDENTIFIER;

        // Retrieve the summary data.
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.instantTransaction();
        entityList = tx.select(entityId, filter.getDataFilter(internalContext, filterEntityId));

        // Create a new summary object and add all of the retrieved entries to it.
        summary = new T8ProcessSummary();
        for (T8DataEntity entity : entityList)
        {
            T8ProcessTypeSummary processType;
            int entityCount;

            // Get the process type summary, creating it if it doesn't exist
            processType = getProcessTypeSummary(summary, entity, entityId);

            // Increment the total count for the process type.
            entityCount = (Integer)entity.getFieldValue(entityId + F_PROCESS_COUNT);
            processType.setTotalCount(processType.getTotalCount() + entityCount);
        }

        return summary;
    }

    private T8ProcessTypeSummary getProcessTypeSummary(T8ProcessSummary summary, T8DataEntity processSummaryEntity, String entityId) throws Exception
    {
        T8ProcessTypeSummary processTypeSummary;
        String projectId;
        String processId;

        processId = (String)processSummaryEntity.getFieldValue(entityId + F_PROCESS_ID);
        projectId = (String)processSummaryEntity.getFieldValue(entityId + F_PROJECT_ID);
        processTypeSummary = summary.getProcessTypeSummary(processId);
        if (processTypeSummary == null)
        {
            T8ProcessDefinition processDefinition;

            processDefinition = (T8ProcessDefinition)definitionManager.getRawDefinition(internalContext, projectId, processId);
            if (processDefinition != null)
            {
                processTypeSummary = new T8ProcessTypeSummary(processId);
                processTypeSummary.setDisplayName(processDefinition.getProcessDisplayName());
                processTypeSummary.setDescription(processDefinition.getProcessDescription());
                summary.putProcessTypeSummary(processTypeSummary);
            }
        }

        return processTypeSummary;
    }

    public T8ProcessState retrieveState(String processIid) throws Exception
    {
        List<T8ProcessState> states;
        T8ProcessFilter filter;

        filter = new T8ProcessFilter();
        filter.addProcessIid(processIid);
        states = retrieveStates(filter.getDataFilter(internalContext, PROCESS_STATE_DE_IDENTIFIER), 0, -1);
        return states.size() > 0 ? states.get(0) : null;
    }

    public List<T8ProcessState> retrieveStates(List<String> processIids) throws Exception
    {
        T8ProcessFilter filter;

        filter = new T8ProcessFilter();
        filter.setProcessIids(processIids);
        return retrieveStates(filter.getDataFilter(internalContext, PROCESS_STATE_DE_IDENTIFIER), 0, -1);
    }

    public List<T8ProcessState> retrieveStates(T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        List<T8DataEntity> processStateEntities;
        List<T8ProcessState> processStates;
        T8DataTransaction tx;

        // Retrieve the process state entities.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        processStateEntities = tx.select(PROCESS_STATE_DE_IDENTIFIER, filter, 0, pageSize > -1 ? pageSize : -1);

        // Create the complete states from each entity retrieved.
        processStates = new ArrayList<>(processStateEntities.size());
        for (T8DataEntity processStateEntity : processStateEntities)
        {
            T8ProcessState processState;

            // Construct the new process state.
            processState = new T8ProcessState((String)processStateEntity.getFieldValue(F_PROJECT_ID), (String)processStateEntity.getFieldValue(F_PROCESS_ID), (String)processStateEntity.getFieldValue(F_PROCESS_IID));
            processState.setName((String)processStateEntity.getFieldValue(F_PROCESS_DISPLAY_NAME));
            processState.setDescription((String)processStateEntity.getFieldValue(F_PROCESS_DESCRIPTION));
            processState.setStatus(T8ProcessStatus.valueOf((String)processStateEntity.getFieldValue(F_STATUS)));
            processState.setProgress((Double)processStateEntity.getFieldValue(F_PROGRESS));
            processState.setInitiatorId((String)processStateEntity.getFieldValue(F_INITIATOR_ID));
            processState.setInitiatorInstanceIdentifier((String)processStateEntity.getFieldValue(F_INITIATOR_IID));
            processState.setRestrictionUserId((String)processStateEntity.getFieldValue(F_RESTRICTION_USER_ID));
            processState.setRestrictionUserProfileId((String)processStateEntity.getFieldValue(F_RESTRICTION_USER_PROFILE_ID));
            processState.setStartTime((T8Timestamp)processStateEntity.getFieldValue(F_START_TIME));
            processState.setEndTime((T8Timestamp)processStateEntity.getFieldValue(F_END_TIME));

            // Add the process state parameters.
            processState.setParameters(stateParameterPersistenceHandler.retrieveParameters(tx, HashMaps.newHashMap(PROCESS_STATE_PAR_DE_IDENTIFIER + F_PROCESS_IID, processState.getIid())));
            processStates.add(processState);
        }

        // Return the retrieved process states.
        return processStates;
    }

    public void logProcessEvent(T8Context context, T8ProcessHistoryEvent event, T8ProcessState state) throws Exception
    {
        T8SessionContext sessionContext;
        Map<String, Object> keyFields;
        T8DataTransaction tx;
        T8DataEntity entity;
        String eventIid;

        // Create a new proces history entity.
        sessionContext = context.getSessionContext();
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        eventIid = T8IdentifierUtilities.createNewGUID();
        entity = tx.create(PROCESS_HISTORY_DE_IDENTIFIER, null);
        entity.setFieldValue(PROCESS_HISTORY_DE_IDENTIFIER + F_EVENT_IID, eventIid);
        entity.setFieldValue(PROCESS_HISTORY_DE_IDENTIFIER + F_PROJECT_ID, state.getProjectId());
        entity.setFieldValue(PROCESS_HISTORY_DE_IDENTIFIER + F_PROCESS_ID, state.getId());
        entity.setFieldValue(PROCESS_HISTORY_DE_IDENTIFIER + F_PROCESS_IID, state.getIid());
        entity.setFieldValue(PROCESS_HISTORY_DE_IDENTIFIER + F_USER_ID, sessionContext.getUserIdentifier());
        entity.setFieldValue(PROCESS_HISTORY_DE_IDENTIFIER + F_USER_PROFILE_ID, sessionContext.getUserProfileIdentifier());
        entity.setFieldValue(PROCESS_HISTORY_DE_IDENTIFIER + F_EVENT, event.toString());
        entity.setFieldValue(PROCESS_HISTORY_DE_IDENTIFIER + F_TIME, new java.sql.Timestamp(System.currentTimeMillis()));
        entity.setFieldValue(PROCESS_HISTORY_DE_IDENTIFIER + F_EVENT_PARAMETERS, createParameterString(state.getId(), state.getParameters())); // We always strip the process namespace, all other parameter types will maintain their namespaces.

        // Insert the entity.
        tx.insert(entity);

        // Insert the Process History parameters.
        keyFields = new HashMap<>();
        keyFields.put(PROCESS_HISTORY_PAR_DE_IDENTIFIER + F_EVENT_IID, eventIid);
        keyFields.put(PROCESS_HISTORY_PAR_DE_IDENTIFIER + F_PROCESS_IID, state.getIid());
        historyParameterPersistenceHandler.insertParameters(tx, keyFields, null, T8IdentifierUtilities.stripNamespace(state.getParameters()));
    }

    private T8DataEntity createStateEntity(T8DataTransaction tx, T8ProcessState state) throws Exception
    {
        T8DataEntity entity;

        // Create a new entity.
        entity = tx.create(PROCESS_STATE_DE_IDENTIFIER, null);
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_PROJECT_ID, state.getProjectId());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_PROCESS_ID, state.getId());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_PROCESS_IID, state.getIid());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_PROCESS_DISPLAY_NAME, state.getName());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_PROCESS_DESCRIPTION, state.getDescription());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_STATUS, state.getStatus().toString());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_PROGRESS, state.getProgress());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_INITIATOR_ID, state.getInitiatorId());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_INITIATOR_IID, state.getInitiatorIid());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_RESTRICTION_USER_ID, state.getRestrictionUserId());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_RESTRICTION_USER_PROFILE_ID, state.getRestrictionUserProfileId());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_START_TIME, state.getStartTime());
        entity.setFieldValue(PROCESS_STATE_DE_IDENTIFIER + F_END_TIME, state.getEndTime());
        return entity;
    }

    private String createParameterString(String namespace, Map<String, Object> parameters)
    {
        if (parameters == null)
        {
            return null;
        }
        else if (parameters.isEmpty())
        {
            return null;
        }
        else
        {
            StringBuffer parameterString;
            Map<String, Object> namespaceStrippedParameters;

            // Strip the flow namespace from the parameters.
            namespaceStrippedParameters = T8IdentifierUtilities.stripNamespace(namespace, parameters, false);

            // Add all of the parameters to the string.
            parameterString = new StringBuffer();
            for (String parameterKey : namespaceStrippedParameters.keySet())
            {
                Object parameterValue;

                // Append the prefix and parameter key.
                parameterString.append(" ["); // Yes, there is a space before the bracket.
                parameterString.append(parameterKey);
                parameterString.append(":");

                // Append the parameter value if any.
                parameterValue = namespaceStrippedParameters.get(parameterKey);
                if (parameterValue != null) parameterString.append(parameterValue.toString());

                // Append the suffix.
                parameterString.append("]");
            }

            return parameterString.toString();
        }
    }
}
