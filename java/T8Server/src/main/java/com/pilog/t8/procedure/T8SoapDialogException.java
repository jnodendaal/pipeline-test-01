package com.pilog.t8.procedure;

import com.pilog.xml.XmlDocument;

/**
 * @author Bouwer du Preez
 */
public class T8SoapDialogException extends Exception
{
    private final XmlDocument soapFault;

    public T8SoapDialogException(XmlDocument soapFault)
    {
        this.soapFault = soapFault;
    }

    public T8SoapDialogException(String message, Throwable cause)
    {
        super(message, cause);
        this.soapFault = null;
    }

    public XmlDocument getSoapFault()
    {
        return soapFault;
    }
}
