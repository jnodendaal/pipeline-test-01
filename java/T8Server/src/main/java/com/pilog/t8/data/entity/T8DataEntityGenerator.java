package com.pilog.t8.data.entity;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.definition.T8DefinitionUtilities;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityFieldDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityRelationshipDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * The basic generation process followed by this definition are as follows:
 *
 * - Take any definition as input.  For the definition, create an entity
 * definition with field definitions for each of the input definition datums.
 * - For each of the input definition datums with a data type of Map or List,
 * create a related entity containing the map or list data.
 * - For each of the input definition datums with a sub-definition data type
 * create a related entity containing the sub-definition and repeat the steps
 * listed here recursively until the entire descendent definition tree has been
 * mapped out as an entity tree.
 *
 * @author Bouwer du Preez
 */
public class T8DataEntityGenerator
{
    public static final String IDENTIFIER_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "IDENTIFIER";
    public static final String DISPLAY_NAME_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "DISPLAY_NAME";
    public static final String DESCRIPTION_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "DESCRIPTION";
    public static final String DEFINITION_PROJECT_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "DEFINITION_PROJECT";
    public static final String CREATED_BY_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "CREATED_BY";
    public static final String CREATED_AT_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "CREATED_AT";
    public static final String UPDATED_BY_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "UPDATED_BY";
    public static final String UPDATED_AT_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "UPDATED_AT";

    private Map<String, String> datumTableNameMap;

    private T8DefinitionManager definitionManager;
    private Map<String, T8DataEntityDefinition> entityDefinitions;
    private Map<String, T8TableDataSourceDefinition> dataSourceDefinitions;

    public T8DataEntityGenerator(T8DefinitionManager definitionManager)
    {
        this.datumTableNameMap = new HashMap<String, String>();
        this.definitionManager = definitionManager;
        this.entityDefinitions = new HashMap<String, T8DataEntityDefinition>();
        this.dataSourceDefinitions = new HashMap<String, T8TableDataSourceDefinition>();
    }

    public void setDatumTableNameMap(Map<String, String> tableNameMap)
    {
        if (tableNameMap != null) datumTableNameMap.putAll(tableNameMap);
    }

    public List<T8DataEntityDefinition> getGeneratedEntityDefinitions()
    {
        return new ArrayList<T8DataEntityDefinition>(entityDefinitions.values());
    }

    public List<T8TableDataSourceDefinition> getGeneratedDataSourceDefinitions()
    {
        return new ArrayList<T8TableDataSourceDefinition>(dataSourceDefinitions.values());
    }

    public void reset()
    {
        entityDefinitions.clear();
        dataSourceDefinitions.clear();
    }

    public T8DataEntity generateEntity(String entityIdentifier, T8Definition definition)
    {
        T8DataEntityDefinition entityDefinition;
        T8DataEntity dataEntity;

        entityDefinition = entityDefinitions.get(entityIdentifier);
        dataEntity = entityDefinition.getNewDataEntityInstance();
        for (T8DataFieldDefinition fieldDefinition : entityDefinition.getFieldDefinitions())
        {
            Object fieldValue;

            fieldValue = definition.getDefinitionDatum(fieldDefinition.getSourceIdentifier());
            dataEntity.setFieldValue(fieldDefinition.getPublicIdentifier(), fieldValue);
        }

        return dataEntity;
    }

    public T8DataEntityDefinition generateEntityDefinition(String parentEntityIdentifier, String entityIdentifier, List<T8DefinitionDatumType> datumTypes)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8DataEntityFieldDefinition> fieldDefinitions;
        T8DataEntityDefinition parentEntityDefinition;

        // Get the parent entity definition (if applicable).
        parentEntityDefinition = entityDefinitions.get(parentEntityIdentifier);

        // Generate the new entity and add it to the collection of generated entities.
        entityDefinition = new T8DataEntityDefinition(entityIdentifier);
        entityDefinitions.put(entityDefinition.getIdentifier(), entityDefinition);

        // Generate the field definitions for the new entity.
        fieldDefinitions = createDatumEntityFieldDefinitions(datumTypes);
        if (parentEntityDefinition != null)
        {
            String parentFieldIdentifier;

            parentFieldIdentifier = T8Definition.getLocalIdPrefix() + getDefaultEntityTableName(parentEntityDefinition);
            fieldDefinitions.add(0, new T8DataEntityFieldDefinition(parentFieldIdentifier, parentFieldIdentifier.substring(1), true, true, T8DataType.DEFINITION_IDENTIFIER));
        }
        entityDefinition.setFieldDefinitions(fieldDefinitions);

        // Generate all related entities.
        generateRelatedEntityDefinitions(entityDefinition, datumTypes);

        // Generate a data source for each of the generated entities.
        for (T8DataEntityDefinition generatedEntityDefinition : entityDefinitions.values())
        {
            T8TableDataSourceDefinition dataSourceDefinition;

            dataSourceDefinition = generateTableDataSourceDefinition(generatedEntityDefinition);
            dataSourceDefinitions.put(dataSourceDefinition.getIdentifier(), dataSourceDefinition);
        }

        return entityDefinition;
    }

    private void generateRelatedEntityDefinitions(T8DataEntityDefinition contextEntityDefinition, List<T8DefinitionDatumType> datumTypes)
    {
        for (T8DefinitionDatumType datumType : datumTypes)
        {
            T8DataType dataType;

            dataType = datumType.getDataType();
            if (isRelationDataType(dataType))
            {
                if ((dataType.equals(T8DataType.MAP)) || (dataType.equals(T8DataType.DEFINITION_IDENTIFIER_MAP)))
                {
                    T8DataEntityDefinition relatedEntityDefinition;
                    T8DataEntityRelationshipDefinition relationshipDefinition;
                    String relatedEntityIdentifier;

                    relatedEntityIdentifier = contextEntityDefinition.getIdentifier() + "_" + datumType.getIdentifier();
                    relatedEntityDefinition = createMapEntityDefinition(relatedEntityIdentifier, contextEntityDefinition);
                    entityDefinitions.put(relatedEntityDefinition.getIdentifier(), relatedEntityDefinition);

                    relationshipDefinition = new T8DataEntityRelationshipDefinition(T8Definition.getLocalIdPrefix() + datumType.getIdentifier());
                    relationshipDefinition.setContextEntityIdentifier(contextEntityDefinition.getIdentifier());
                    relationshipDefinition.setRelatedEntityIdentifier(relatedEntityIdentifier);
                }
                else if ((dataType.equals(T8DataType.LIST)) || (dataType.equals(T8DataType.DEFINITION_IDENTIFIER_LIST)))
                {
                    T8DataEntityDefinition relatedEntityDefinition;
                    T8DataEntityRelationshipDefinition relationshipDefinition;
                    String relatedEntityIdentifier;

                    relatedEntityIdentifier = contextEntityDefinition.getIdentifier() + "_" + datumType.getIdentifier();
                    relatedEntityDefinition = createListEntityDefinition(relatedEntityIdentifier, contextEntityDefinition);
                    entityDefinitions.put(relatedEntityDefinition.getIdentifier(), relatedEntityDefinition);

                    relationshipDefinition = new T8DataEntityRelationshipDefinition(T8Definition.getLocalIdPrefix() + datumType.getIdentifier());
                    relationshipDefinition.setContextEntityIdentifier(contextEntityDefinition.getIdentifier());
                    relationshipDefinition.setRelatedEntityIdentifier(relatedEntityIdentifier);
                }
                else if ((dataType.equals(T8DataType.DEFINITION)) || (dataType.equals(T8DataType.DEFINITION_LIST)))
                {
                    String datumGroupIdentifier;

                    /**
                     * TODO:
                     * 1. Get the group identifier for the datum type
                     * 2. For each definition type in the group, generate a
                     * entity definition.
                     */
                }
            }
        }
    }

    private String getDatumTableName(T8Definition contextDefinition, String datumIdentifier)
    {
        if (datumTableNameMap.containsKey(datumIdentifier))
        {
            return datumTableNameMap.get(datumIdentifier);
        }
        else return datumIdentifier;
    }

    public static List<T8DataEntityFieldDefinition> createDatumEntityFieldDefinitions(List<T8DefinitionDatumType> datumTypes)
    {
        ArrayList<T8DataEntityFieldDefinition> fieldDefinitions;

        fieldDefinitions = new ArrayList<T8DataEntityFieldDefinition>();

        fieldDefinitions.add(new T8DataEntityFieldDefinition(IDENTIFIER_FIELD_IDENTIFIER, "IDENTIFIER", true, true, T8DataType.DEFINITION_IDENTIFIER));
        fieldDefinitions.add(new T8DataEntityFieldDefinition(DISPLAY_NAME_FIELD_IDENTIFIER, "DISPLAY_NAME", false, false, T8DataType.STRING));
        fieldDefinitions.add(new T8DataEntityFieldDefinition(DESCRIPTION_FIELD_IDENTIFIER, "DESCRIPTION", false, false, T8DataType.STRING));
        fieldDefinitions.add(new T8DataEntityFieldDefinition(DEFINITION_PROJECT_FIELD_IDENTIFIER, "DEFINITION_PROJECT", false, false, T8DataType.DEFINITION_IDENTIFIER));
        fieldDefinitions.add(new T8DataEntityFieldDefinition(CREATED_AT_FIELD_IDENTIFIER, "CREATED_AT", true, true, T8DataType.DATE_TIME));
        fieldDefinitions.add(new T8DataEntityFieldDefinition(CREATED_BY_FIELD_IDENTIFIER, "CREATED_BY", false, false, T8DataType.DEFINITION_IDENTIFIER));
        fieldDefinitions.add(new T8DataEntityFieldDefinition(UPDATED_AT_FIELD_IDENTIFIER, "UPDATED_AT", false, false, T8DataType.DATE_TIME));
        fieldDefinitions.add(new T8DataEntityFieldDefinition(UPDATED_BY_FIELD_IDENTIFIER, "UPDATED_BY", false, false, T8DataType.DEFINITION_IDENTIFIER));
        for (T8DefinitionDatumType datumType : datumTypes)
        {
            T8DataType dataType;

            dataType = datumType.getDataType();
            if (!isRelationDataType(dataType))
            {
                fieldDefinitions.add(new T8DataEntityFieldDefinition(T8Definition.getLocalIdPrefix() + datumType.getIdentifier(), datumType.getIdentifier(), false, false, datumType.getDataType()));
            }
        }

        return fieldDefinitions;
    }

    public static List<T8DataSourceFieldDefinition> createDatumDataSourceFieldDefinitions(List<T8DefinitionDatumType> datumTypes)
    {
        ArrayList<T8DataSourceFieldDefinition> fieldDefinitions;

        fieldDefinitions = new ArrayList<T8DataSourceFieldDefinition>();

        fieldDefinitions.add(new T8DataSourceFieldDefinition(IDENTIFIER_FIELD_IDENTIFIER, "IDENTIFIER", true, true, T8DataType.DEFINITION_IDENTIFIER));
        fieldDefinitions.add(new T8DataSourceFieldDefinition(DISPLAY_NAME_FIELD_IDENTIFIER, "DISPLAY_NAME", false, false, T8DataType.STRING));
        fieldDefinitions.add(new T8DataSourceFieldDefinition(DESCRIPTION_FIELD_IDENTIFIER, "DESCRIPTION", false, false, T8DataType.STRING));
        fieldDefinitions.add(new T8DataSourceFieldDefinition(DEFINITION_PROJECT_FIELD_IDENTIFIER, "DEFINITION_PROJECT", false, false, T8DataType.DEFINITION_IDENTIFIER));
        fieldDefinitions.add(new T8DataSourceFieldDefinition(CREATED_AT_FIELD_IDENTIFIER, "CREATED_AT", true, true, T8DataType.DATE_TIME));
        fieldDefinitions.add(new T8DataSourceFieldDefinition(CREATED_BY_FIELD_IDENTIFIER, "CREATED_BY", false, false, T8DataType.DEFINITION_IDENTIFIER));
        fieldDefinitions.add(new T8DataSourceFieldDefinition(UPDATED_AT_FIELD_IDENTIFIER, "UPDATED_AT", false, false, T8DataType.DATE_TIME));
        fieldDefinitions.add(new T8DataSourceFieldDefinition(UPDATED_BY_FIELD_IDENTIFIER, "UPDATED_BY", false, false, T8DataType.DEFINITION_IDENTIFIER));

        for (T8DefinitionDatumType datumType : datumTypes)
        {
            T8DataType dataType;

            dataType = datumType.getDataType();
            if (!isRelationDataType(dataType))
            {
                fieldDefinitions.add(new T8DataSourceFieldDefinition(T8Definition.getLocalIdPrefix() + datumType.getIdentifier(), datumType.getIdentifier(), false, false, datumType.getDataType()));
            }
        }

        return fieldDefinitions;
    }

    public static T8DataEntityDefinition createMapEntityDefinition(String entityIdentifier, T8DataEntityDefinition parentEntityDefinition)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8DataEntityFieldDefinition> fieldDefinitions;
        String keyIdentifier;

        keyIdentifier = T8Definition.getLocalIdPrefix() + getDefaultEntityTableName(parentEntityDefinition);

        entityDefinition = new T8DataEntityDefinition(entityIdentifier);
        entityDefinition.setMetaDescription("Map Entity");
        entityDefinition.setMetaDisplayName("Map Entity");

        fieldDefinitions = new ArrayList<T8DataEntityFieldDefinition>();
        fieldDefinitions.add(new T8DataEntityFieldDefinition(keyIdentifier, null, true, true, T8DataType.DEFINITION_IDENTIFIER));
        fieldDefinitions.add(new T8DataEntityFieldDefinition("$KEY", null, true, true, T8DataType.STRING));
        fieldDefinitions.add(new T8DataEntityFieldDefinition("$KEY_DATA_TYPE", null, false, true, T8DataType.STRING));
        fieldDefinitions.add(new T8DataEntityFieldDefinition("$VALUE", null, false, false, T8DataType.STRING));
        fieldDefinitions.add(new T8DataEntityFieldDefinition("$VALUE_DATA_TYPE", null, false, true, T8DataType.STRING));
        entityDefinition.setFieldDefinitions(fieldDefinitions);

        return entityDefinition;
    }

    public static T8DataEntityDefinition createListEntityDefinition(String entityIdentifier, T8DataEntityDefinition parentEntityDefinition)
    {
        T8DataEntityDefinition entityDefinition;
        List<T8DataEntityFieldDefinition> fieldDefinitions;
        String keyIdentifier;

        keyIdentifier = T8Definition.getLocalIdPrefix() + getDefaultEntityTableName(parentEntityDefinition);

        entityDefinition = new T8DataEntityDefinition(entityIdentifier);
        entityDefinition.setMetaDescription("List Entity");
        entityDefinition.setMetaDisplayName("List Entity");

        fieldDefinitions = new ArrayList<T8DataEntityFieldDefinition>();
        fieldDefinitions.add(new T8DataEntityFieldDefinition(keyIdentifier, null, true, true, T8DataType.DEFINITION_IDENTIFIER));
        fieldDefinitions.add(new T8DataEntityFieldDefinition("$ELEMENT_INDEX", null, true, true, T8DataType.STRING));
        fieldDefinitions.add(new T8DataEntityFieldDefinition("$ELEMENT_DATA_TYPE", null, false, true, T8DataType.STRING));
        fieldDefinitions.add(new T8DataEntityFieldDefinition("$ELEMENT_VALUE", null, false, false, T8DataType.STRING));
        entityDefinition.setFieldDefinitions(fieldDefinitions);

        return entityDefinition;
    }

    private static boolean isRelationDataType(T8DataType dataType)
    {
        return ((dataType.equals(T8DataType.DEFINITION)) || (dataType.equals(T8DataType.DEFINITION_LIST)));
    }

    private static T8TableDataSourceDefinition generateTableDataSourceDefinition(T8DataEntityDefinition entityDefinition)
    {
        T8TableDataSourceDefinition dataSourceDefinition;
        List<T8DataSourceFieldDefinition> dataSourceFieldDefinitions;

        dataSourceDefinition = new T8TableDataSourceDefinition(getDefaultDataSourceIdentifier(entityDefinition));
        dataSourceDefinition.setMetaDescription("Data Source: " + entityDefinition.getMetaDescription());
        dataSourceDefinition.setMetaDisplayName("Data Source: " + entityDefinition.getMetaDisplayName());
        dataSourceDefinition.setConnectionIdentifier("@DATA_CONNECTION_MDQM_DEV_OPS");
        dataSourceDefinition.setTableName(getDefaultEntityTableName(entityDefinition));

        dataSourceFieldDefinitions = new ArrayList<T8DataSourceFieldDefinition>();
        for (T8DataFieldDefinition entityFieldDefinition : entityDefinition.getFieldDefinitions())
        {
            T8DataSourceFieldDefinition dataSourceFieldDefinition;
            String sourceIdentifier;

            sourceIdentifier = entityFieldDefinition.getIdentifier().substring(1); // Remove the '$' prefix from the entity field identifier.
            if (sourceIdentifier.length() > 30) sourceIdentifier = T8IdentifierUtilities.stripVowels(sourceIdentifier, true);

            dataSourceFieldDefinition = new T8DataSourceFieldDefinition(entityFieldDefinition.getSourceIdentifier());
            dataSourceFieldDefinition.setKey(entityFieldDefinition.isKey());
            dataSourceFieldDefinition.setRequired(entityFieldDefinition.isRequired());
            dataSourceFieldDefinition.setSourceIdentifier(sourceIdentifier);
            dataSourceFieldDefinition.setDataType(entityFieldDefinition.getDataType());
            dataSourceFieldDefinitions.add(dataSourceFieldDefinition);
        }

        dataSourceDefinition.setFieldDefinitions(dataSourceFieldDefinitions);
        return dataSourceDefinition;
    }

    private static String getDefaultDataSourceIdentifier(T8DataEntityDefinition sourceEntityDefinition)
    {
        String dataSourceIdentifier;
        String identifierPrefix;

        dataSourceIdentifier = sourceEntityDefinition.getIdentifier(); // Get the entity identifier.
        identifierPrefix = sourceEntityDefinition.getTypeMetaData().getIdPrefix(); // Get the entity type prefix.
        dataSourceIdentifier = dataSourceIdentifier.substring(1); // Remove the global identifier prefix from the entity identifier.
        if (identifierPrefix != null) dataSourceIdentifier = dataSourceIdentifier.substring(identifierPrefix.length()); // Remove the entity prefix from the entity identifier.
        dataSourceIdentifier = T8DefinitionUtilities.getDefinitionTypeMetaData(T8TableDataSourceDefinition.class).getIdPrefix() + dataSourceIdentifier; // Prepend the data source type prefix.
        dataSourceIdentifier = T8Definition.getPublicIdPrefix() + dataSourceIdentifier; // Prepend the global identifier prefix.
        return dataSourceIdentifier;
    }

    private static String getDefaultEntityTableName(T8DataEntityDefinition sourceEntityDefinition)
    {
        String tableName;
        String identifierPrefix;

        tableName = sourceEntityDefinition.getIdentifier(); // Get the entity identifier.
        identifierPrefix = sourceEntityDefinition.getTypeMetaData().getIdPrefix(); // Get the entity type prefix.
        tableName = tableName.substring(1); // Remove the global identifier prefix from the entity identifier.
        if (identifierPrefix != null) tableName = tableName.substring(identifierPrefix.length()); // Remove the entity prefix from the entity identifier.
        tableName = T8IdentifierUtilities.stripVowels(tableName, true);
        return tableName;
    }
}
