package com.pilog.t8.flow.node.activity;

import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.definition.flow.node.activity.T8ParameterAssignmentActivityDefinition;
import com.pilog.t8.definition.flow.node.activity.T8ParameterAssignmentExpressionDefinition;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowParameterAssignmentActivityNode extends T8DefaultFlowNode
{
    private final T8ParameterAssignmentActivityDefinition definition;

    public T8FlowParameterAssignmentActivityNode(T8Context contex, T8Flow parentFlow, T8ParameterAssignmentActivityDefinition definition, String instanceIdentifier)
    {
        super(contex, parentFlow, definition, instanceIdentifier);
        this.definition = definition;
    }

    @Override
    public T8ParameterAssignmentActivityDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters) throws Exception
    {
        // Execute the script.
        return executeExpressions(internalContext, inputParameters);
    }

    private Map<String, Object> executeExpressions(T8Context context, Map<String, Object> inputParameters) throws Exception
    {
        List<T8ParameterAssignmentExpressionDefinition> expressionDefinitions;
        T8ServerExpressionEvaluator expressionEvaluator;
        Map<String, Object> expressionParameters;

        // Create a map of expression parameters.
        expressionParameters = inputParameters;
        if (expressionParameters == null) expressionParameters = new HashMap<String, Object>();

        // Create the expression evaluator.
        expressionEvaluator = new T8ServerExpressionEvaluator(context);

        // Evaluate all value expressions in sequence.
        expressionDefinitions = definition.getParameterExpressionDefinitions();
        if (expressionDefinitions != null)
        {
            for (T8ParameterAssignmentExpressionDefinition expressionDefinition : expressionDefinitions)
            {
                String parameterIdentifier;
                String expression;
                Object result;

                parameterIdentifier = expressionDefinition.getParameterIdentifier();
                expression = expressionDefinition.getValueExpression();
                result = expressionEvaluator.evaluateExpression(expression, expressionParameters, null);
                expressionParameters.put(parameterIdentifier, result);
            }

            return expressionParameters;
        }
        else return null;
    }
}
