package com.pilog.t8.flow.node.activity;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.definition.flow.node.activity.T8OperationActivityDefinition;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ServerOperationActivityNode extends T8DefaultFlowNode
{
    private final T8OperationActivityDefinition definition;

    public T8ServerOperationActivityNode(T8Context context, T8Flow parentFlow, T8OperationActivityDefinition definition, String nodeIid)
    {
        super(context, parentFlow, definition, nodeIid);
        this.definition = definition;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters) throws Exception
    {
        Map<String, Object> outputParameters;

        // Execute the operation.
        outputParameters = executeServerOperation(flowState, T8IdentifierUtilities.mapParameters(inputParameters, definition.getOperationInputParameterMapping()));

        // Map the operation output parameters to the flow parameters.
        outputParameters = T8IdentifierUtilities.mapParameters(outputParameters, definition.getOperationOutputParameterMapping());
        return outputParameters;
    }

    protected Map<String, Object> executeServerOperation(T8FlowState state, Map<String, Object> inputParameters) throws Exception
    {
        String operationId;

        // Start the sub-flow and get it's instance nodeId.
        operationId = definition.getOperationIdentifier();
        if (operationId != null)
        {
            return serverContext.executeSynchronousOperation(internalContext, operationId, inputParameters);
        }
        else throw new Exception("No Server Operation Identifier found.");
    }
}
