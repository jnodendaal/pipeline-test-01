package com.pilog.t8.notification;

import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.definition.notification.T8NotificationManagerResource;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import java.util.Map;

/**
 * @author Hennie Brink
 */
@SuppressWarnings("PublicInnerClass")
public class T8NotificationOperations
{
    public static class SendNotificationOperation extends T8DefaultServerOperation
    {
        public SendNotificationOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Map<String, ? extends Object> notificationParameters;
            String notificationIdentifier;

            notificationIdentifier = (String)operationParameters.get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_IDENTIFIER);
            notificationParameters = (Map<String, ? extends Object>)operationParameters.get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_PARAMETERS);
            serverContext.getNotificationManager().sendNotification(context, notificationIdentifier, notificationParameters);

            return null;
        }
    }

    public static class GetUserNotificationSummaryOperation extends T8DefaultServerOperation
    {
        public GetUserNotificationSummaryOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8NotificationSummary> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8NotificationSummary notificationSummary;

            notificationSummary = serverContext.getNotificationManager().getUserNotificationSummary(context, (T8NotificationFilter)operationParameters.get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_FILTER));

            return HashMaps.createSingular(T8NotificationManagerResource.PARAMETER_NOTIFICATION_SUMMARY, notificationSummary);
        }
    }

    public static class GetUserNotificationsOperation extends T8DefaultServerOperation
    {
        public GetUserNotificationsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, List<T8Notification>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8NotificationManager notificationManager;
            T8NotificationFilter notificationFilter;

            notificationFilter = (T8NotificationFilter)operationParameters.get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_FILTER);

            notificationManager = serverContext.getNotificationManager();
            return HashMaps.createSingular(T8NotificationManagerResource.PARAMETER_NOTIFICATION_LIST, notificationManager.getUserNotifications(context, notificationFilter));
        }
    }

    public static class CloseAllNotificationsOperation extends T8DefaultServerOperation
    {

        public CloseAllNotificationsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, ?> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            serverContext.getNotificationManager().closeAllNotifications(context);
            return null;
        }
    }

    public static class CloseNotificationOperation extends T8DefaultServerOperation
    {

        public CloseNotificationOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, ?> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String notificationIID;

            notificationIID = (String)operationParameters.get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_IID);
            serverContext.getNotificationManager().closeNotification(context, notificationIID);

            return null;
        }
    }

    public static class MarkNotificationAsOperation extends T8DefaultServerOperation
    {
        public MarkNotificationAsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, ? extends Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8Notification.NotificationStatus status;
            String notificationIID;

            notificationIID = (String)operationParameters.get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_IID);
            status = (T8Notification.NotificationStatus)operationParameters.get(T8NotificationManagerResource.PARAMETER_NOTIFICATION_STATUS);

            this.serverContext.getNotificationManager().markWithStatus(context, notificationIID, status);

            return null;
        }
    }
}
