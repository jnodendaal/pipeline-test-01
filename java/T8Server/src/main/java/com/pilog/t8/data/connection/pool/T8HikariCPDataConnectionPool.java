/**
 * Created on 30 Mar 2016, 7:17:00 AM
 */
package com.pilog.t8.data.connection.pool;

import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.definition.data.connection.pool.T8HikariCPDataConnectionPoolDefinition;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Gavin Boshoff
 */
public class T8HikariCPDataConnectionPool implements T8DataConnectionPool
{
    private final T8HikariCPDataConnectionPoolDefinition definition;
    private final boolean retryOnFailure;

    private HikariDataSource connectionPool;
    private boolean connectionAttemptFailed;
    private String connectionIdentifier;
    private HikariConfig config;

    public T8HikariCPDataConnectionPool(T8HikariCPDataConnectionPoolDefinition definition, T8DataTransaction dataAccessProvider)
    {
        this.definition = definition;
        this.retryOnFailure = definition.retryOnConnectionFailure();
        this.connectionAttemptFailed = false;
        configureConnectionPool();
    }

    private void configureConnectionPool()
    {
        this.config = new HikariConfig();

        this.config.setMinimumIdle(this.definition.getMinPoolSize());
        this.config.setMaximumPoolSize(this.definition.getMaxPoolSize());
    }

    @Override
    public String getDataConnectionIdentifier()
    {
        return this.connectionIdentifier;
    }

    @Override
    public Connection getConnection() throws SQLException
    {
        // Do not attempt to create new connections if the connection pool has been configured to not allow that
        if (this.connectionAttemptFailed && !retryOnFailure)
        {
            throw new SQLException("This connection pool has failed at creating a connection to the database and has been configured to not retry");
        }

        try
        {
            if (this.connectionPool == null) this.connectionPool = new HikariDataSource(this.config);
            return this.connectionPool.getConnection();
        }
        catch (SQLException sqle)
        {
            this.connectionAttemptFailed = true;

            if (!this.retryOnFailure) destroy();
            throw sqle;
        }
    }

    @Override
    public void destroy()
    {
        if (this.connectionPool != null) this.connectionPool.close();
        this.connectionPool = null;
    }

    @Override
    public void setDataConnectionIdentifier(String identifier)
    {
        this.connectionIdentifier = identifier;
    }

    @Override
    public void setDriverClassName(String className) throws Exception
    {
        // HikariCP will attempt to resolve a driver through the DriverManager
        // based solely on the jdbcUrl, but for some older drivers the
        // driverClassName must also be specified. Omit this property unless you
        // get an obvious error message indicating that the driver was not found.
        Class.forName(className);
    }

    @Override
    public void setConnectionURL(String connectionURL)
    {
        this.config.setJdbcUrl(connectionURL);
        this.destroy();
    }

    @Override
    public void setUsername(String username)
    {
        this.config.setUsername(username);
        this.destroy();
    }

    @Override
    public void setPassword(String password)
    {
        this.config.setPassword(password);
        this.destroy();
    }

}