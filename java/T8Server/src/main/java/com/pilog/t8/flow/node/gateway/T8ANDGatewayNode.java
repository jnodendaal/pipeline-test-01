package com.pilog.t8.flow.node.gateway;

import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.definition.flow.node.gateway.T8ANDGatewayDefinition;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ANDGatewayNode extends T8DefaultFlowNode
{
    private final T8ANDGatewayDefinition definition;

    public T8ANDGatewayNode(T8Context context, T8Flow parentFlow, T8ANDGatewayDefinition definition, String instanceIdentifier)
    {
        super(context, parentFlow, definition, instanceIdentifier);
        this.definition = definition;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters)
    {
        // AND Gateways do not perform any function, so just return the input parameters;
        return inputParameters;
    }
}
