package com.pilog.t8.data.database.adaptor;

import static com.pilog.t8.data.database.adaptor.T8DatabaseAdaptorTools.*;

import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.database.T8TablePrimaryKey;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.database.T8DatabaseAdaptorDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.utilities.jdbc.JDBCUtilities;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8H2DatabaseAdaptor implements T8DatabaseAdaptor
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8H2DatabaseAdaptor.class);

    private static final char META_ESCAPE_CHAR = '"';

    private static final Map<T8DataType, H2DataType> dataTypeMapping;
    private static final Map<Integer, T8DataType> sqlTypeMap;

    private enum H2DataType
    {
        BLOB("BLOB"),
        CLOB("CLOB"),
        GUID("UUID"),
        CHAR("CHAR"),
        DATE("DATE"),
        TIMESTAMP("TIMESTAMP"),
        DECIMAL("DECIMAL"),
        INTEGER("SMALLINT"),
        BIG_INTEGER("BIGINT"),
        DOUBLE("DOUBLE"),
        VARCHAR("VARCHAR(4000)");

        private final String sqlTypeString;

        H2DataType(String sqlTypeString)
        {
            this.sqlTypeString = sqlTypeString;
        }

        private String getSQLTypeString()
        {
            return sqlTypeString;
        }
    };

    static
    {
        dataTypeMapping = new HashMap<>();
        dataTypeMapping.put(T8DataType.GUID, H2DataType.GUID);
        dataTypeMapping.put(T8DataType.BOOLEAN, H2DataType.CHAR);
        dataTypeMapping.put(T8DataType.BYTE_ARRAY, H2DataType.BLOB);
        dataTypeMapping.put(T8DataType.DATE, H2DataType.DATE);
        dataTypeMapping.put(T8DataType.DATE_TIME, H2DataType.TIMESTAMP);
        dataTypeMapping.put(T8DataType.DISPLAY_STRING, H2DataType.VARCHAR);
        dataTypeMapping.put(T8DataType.DOUBLE, H2DataType.DOUBLE);
        dataTypeMapping.put(T8DataType.FLOAT, H2DataType.DECIMAL);
        dataTypeMapping.put(T8DataType.DEFINITION_IDENTIFIER, H2DataType.VARCHAR);
        dataTypeMapping.put(T8DataType.IDENTIFIER_STRING, H2DataType.VARCHAR);
        dataTypeMapping.put(T8DataType.INTEGER, H2DataType.INTEGER);
        dataTypeMapping.put(T8DataType.STRING, H2DataType.VARCHAR);
        dataTypeMapping.put(T8DataType.LONG_STRING, H2DataType.CLOB);
        dataTypeMapping.put(T8DataType.LONG, H2DataType.BIG_INTEGER);

        sqlTypeMap = new HashMap<>();
        sqlTypeMap.put(Types.ARRAY, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.BIGINT, T8DataType.LONG);
        sqlTypeMap.put(Types.BINARY, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.BIT, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.BLOB, T8DataType.BYTE_ARRAY);
        sqlTypeMap.put(Types.BOOLEAN, T8DataType.BOOLEAN);
        sqlTypeMap.put(Types.CHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.CLOB, T8DataType.LONG_STRING);
        sqlTypeMap.put(Types.DATALINK, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.DATE, T8DataType.DATE);
        sqlTypeMap.put(Types.DECIMAL, T8DataType.DOUBLE);
        sqlTypeMap.put(Types.DISTINCT, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.DOUBLE, T8DataType.DOUBLE);
        sqlTypeMap.put(Types.FLOAT, T8DataType.FLOAT);
        sqlTypeMap.put(Types.INTEGER, T8DataType.INTEGER);
        sqlTypeMap.put(Types.JAVA_OBJECT, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.LONGNVARCHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.LONGVARBINARY, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.LONGVARCHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.NCHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.NCLOB, T8DataType.STRING);
        sqlTypeMap.put(Types.NULL, T8DataType.UNDEFINED);
        sqlTypeMap.put(Types.NUMERIC, T8DataType.DOUBLE);
        sqlTypeMap.put(Types.NVARCHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.OTHER, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.REAL, T8DataType.DOUBLE);
        sqlTypeMap.put(Types.REF, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.ROWID, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.SMALLINT, T8DataType.INTEGER);
        sqlTypeMap.put(Types.SQLXML, T8DataType.STRING);
        sqlTypeMap.put(Types.STRUCT, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.TIME, T8DataType.TIME);
        sqlTypeMap.put(Types.TIMESTAMP, T8DataType.TIMESTAMP);
        sqlTypeMap.put(Types.TINYINT, T8DataType.INTEGER);
        sqlTypeMap.put(Types.VARBINARY, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.VARCHAR, T8DataType.STRING);
    }

    public T8H2DatabaseAdaptor(T8DatabaseAdaptorDefinition definition)
    {
    }

    @Override
    public T8DataType getDataType(int sqlType, int width)
    {
        return sqlTypeMap.get(sqlType);
    }

    @Override
    public String getSQLParameterizedColumnContains(String columnName)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSQLStringConcatenationOperator()
    {
        return "||";
    }

    @Override
    public String getSQLTagReplaceByIdentifier(String tagIdentifier, String replacementTag)
    {
        return "TAGS = REGEXP_REPLACE(TAGS, ' \\[" + tagIdentifier + ":[^].]+\\]', '" + replacementTag + "')";
    }

    @Override
    public String getSequenceNextValueQuery(T8DataConnection dataConnection, String sequenceName)
    {
        return "SELECT " + sequenceName + ".NEXTVAL AS SEQUENCE FROM DUAL";
    }

    @Override
    public String convertHexStringToSQLGUIDString(String inputString)
    {
        // No conversion required for Oracle GUID's because they are the same format as used internally by T8.
        return inputString;
    }

    @Override
    public String getSQLColumnHexToGUID(String columnString)
    {
        return columnString;
    }

    @Override
    public boolean requiresSqlLikeEscape(String inputString)
    {
        if (inputString == null) return false;
        else return (inputString.contains("%"));
    }

    @Override
    public String getSQLEscapedLikeOperand(String inputString, char escapeCharacter)
    {
        String result;
        String escapeString;

        escapeString = "" + escapeCharacter;
        result = inputString.replace("%", escapeString + "%");
        return result;
    }

    @Override
    public String getSQLParameterizedHexToGUID()
    {
        return "?";
    }

    @Override
    public String getSQLColumnMillisecondsToTimestamp(String columnName)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getSQLColumnTimestampToMilliseconds(String columnName)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSQLColumnTimestampAddMilliseconds(String timestampColumnName, String millisecondExpression)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSQLParameterizedMillisecondsToTimestamp()
    {
        return "?";
    }

    @Override
    public String getSQLCreateNewGUID()
    {
        return "RANDOM_UUID()";
    }

    @Override
    public String getSQLHexToGUID(String expressionString)
    {
        return "(" + expressionString + ")";
    }

    @Override
    public String getRegularExpressionLike(String columnName, String expression)
    {
        return "REGEXP_LIKE(" + columnName + ", '" + expression + "')";
    }

    @Override
    public String getTableSelectionQuery()
    {
        return "SELECT TABLE_NAME FROM USER_TABLES";
    }

    @Override
    public String getTableCreationStatement(T8TableDataSourceDefinition dataSourceDefinition)
    {
        List<T8DataSourceFieldDefinition> dataFieldDefinitions;
        StringBuilder createTable;
        String tableName;

        dataFieldDefinitions = dataSourceDefinition.getFieldDefinitions();

        if (dataFieldDefinitions.isEmpty())
        {
            throw new RuntimeException("Table cannot be created without any columns specified");
        }

        createTable = new StringBuilder("CREATE TABLE ");
        tableName = dataSourceDefinition.getTableName();

        createTable.append(escapeMetaString(tableName));
        createTable.append(" (");
        createTable.append(getCreateColumns(dataFieldDefinitions));
        createTable.append(getCreateTablePrimConstraint(tableName, dataFieldDefinitions, this));
        createTable.append(")");

        return createTable.toString();
    }

    /**
     * Builds up the section of the create table statement which contains the
     * columns for the statement according to the list of data field definitions
     * passed to the method. The types for the columns are set according to the
     * {@code OracleDataType} mapping object.
     */
    private StringBuilder getCreateColumns(List<T8DataSourceFieldDefinition> dataFieldDefinitions)
    {
        Iterator<T8DataSourceFieldDefinition> fieldIterator;
        T8DataFieldDefinition dataFieldDefinition;
        StringBuilder columns;

        columns = new StringBuilder();
        fieldIterator = dataFieldDefinitions.listIterator();
        while (fieldIterator.hasNext())
        {
            T8DataType fieldDataType;

            dataFieldDefinition = fieldIterator.next();
            fieldDataType = dataFieldDefinition.getDataType();
            if (dataTypeMapping.containsKey(fieldDataType))
            {
                columns.append(escapeMetaString(dataFieldDefinition.getSourceIdentifier()));
                columns.append(" ");
                columns.append(dataTypeMapping.get(dataFieldDefinition.getDataType()).getSQLTypeString());
                if (dataFieldDefinition.isRequired()) columns.append(" NOT NULL ");
                if (fieldIterator.hasNext()) columns.append(", ");
            }
        }

        return columns;
    }

    @Override
    public String getAddTableColumnStatement(String tableName, T8DataFieldDefinition dataFieldDefinition)
    {
        if (tableName == null || tableName.isEmpty() || dataFieldDefinition == null)
        {
            throw new NullPointerException("Table and Column name needs to have an actual value");
        }

        StringBuilder alterTable;

        alterTable = new StringBuilder("ALTER TABLE ");
        alterTable.append(escapeMetaString(tableName));
        alterTable.append(" ADD ");
        alterTable.append(escapeMetaString(dataFieldDefinition.getSourceIdentifier()));
        alterTable.append(" ");
        alterTable.append(dataTypeMapping.get(dataFieldDefinition.getDataType()).getSQLTypeString());
        if (dataFieldDefinition.isRequired()) alterTable.append(" NOT NULL ");

        return alterTable.toString();
    }

    @Override
    public boolean addColumnToPrimaryKey(String tableName, String columnName, T8DataConnection dataConnection)
    {
        if (tableName == null || tableName.isEmpty() || columnName == null || columnName.isEmpty())
        {
            throw new NullPointerException("Invalid values supplied - Table: " + tableName + ", Column: " + columnName);
        }

        T8TablePrimaryKey primaryKey;

        primaryKey = getTablePrimaryKey(tableName, dataConnection);
        if (primaryKey == null)
        {
            LOGGER.log("Failed to retrieve the primary key data for table " + tableName + ", adding column " + columnName + " to primary key has failed");
            return false;
        }

        try
        {
            if (!primaryKey.hasPrimaryKey()) createNewPrimaryKey(tableName, dataConnection, columnName);
            else if (!primaryKey.getPrimaryKeyColumns().contains(columnName)) return alterPrimaryKey(primaryKey, columnName, dataConnection);
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Failed to modify the table " + tableName + " primary key", sqle);
            return false;
        }

        return true;
    }

    /**
     * Retrieves an object which represents the primary key on the specified table.
     * If no primary key exists on the table, the object is still returned and
     * the {@code hasPrimaryKey()} method will read false
     *
     * @return The primary key object containing the primary key data if one
     * exists. An empty primary key object if no primary key exists.
     * {@code null} is returned if an error occurs trying to retrieve the
     * primary key data
     */
    private T8TablePrimaryKey getTablePrimaryKey(String tableName, T8DataConnection dataConnection)
    {
        PreparedStatement pstmtSelectPrimKey = null;
        ResultSet rsSelectPrimKey = null;
        T8TablePrimaryKey primaryKey;

        try
        {
            pstmtSelectPrimKey = dataConnection.prepareStatement("SELECT a.CONSTRAINT_NAME, A.COLUMN_NAME FROM ALL_CONS_COLUMNS a "
                    + "INNER JOIN ALL_CONSTRAINTS c ON a.CONSTRAINT_NAME = c.CONSTRAINT_NAME AND a.OWNER = c.OWNER "
                    + "WHERE c.TABLE_NAME = ? AND c.CONSTRAINT_TYPE = 'P'");
            pstmtSelectPrimKey.setString(1, tableName);
            rsSelectPrimKey = pstmtSelectPrimKey.executeQuery();

            primaryKey = new T8TablePrimaryKey(tableName);
            while (rsSelectPrimKey.next())
            {
                primaryKey.setPrimaryKeyIdentifier(rsSelectPrimKey.getString("CONSTRAINT_NAME"));
                primaryKey.addPrimaryKeyColumn(rsSelectPrimKey.getString("COLUMN_NAME"));
            }
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Failed to retrieve the primary key data " + sqle.getMessage());
            return null;
        }
        finally
        {
            JDBCUtilities.close(rsSelectPrimKey, pstmtSelectPrimKey);
        }

        return primaryKey;
    }

    /**
     * Alters the primary key for a specific table. Since altering a primary key
     * requires the primary key to be dropped, this method acts the same as
     * dropping the primary key and creating a new primary key, with the additional
     * column appended to all of the original primary key columns.<br>
     * <br>
     * If the creation of the new primary key fails, an attempt will be made to
     * rebuild the original primary key.
     *
     * @return {@code true} if the primary key is altered successfully.
     * {@code false} if original primary key was rebuilt.
     *
     * @throws SQLException If altering the primary key has failed, as well as
     * rebuilding the original primary key.
     */
    private boolean alterPrimaryKey(T8TablePrimaryKey existingKey, String columnToAdd, T8DataConnection dataConnection) throws SQLException
    {
        List<String> keyColumns;

        dropPrimaryKey(existingKey, dataConnection);
        dropPrimaryKeyIndex(existingKey, dataConnection);

        try
        {
            keyColumns = new ArrayList<>(existingKey.getPrimaryKeyColumns());
            keyColumns.add(columnToAdd);
            createNewPrimaryKey(existingKey.getTableName(), dataConnection, (String[])keyColumns.toArray());
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Failed to create new primary key. Attempting to rebuild old primary key", sqle);
            createNewPrimaryKey(existingKey.getTableName(), dataConnection, (String[])existingKey.getPrimaryKeyColumns().toArray());
            return false;
        }

        return true;
    }

    /**
     * Drops the index on the table where the primary key was. This is used due
     * to the fact that oracle creates an index for every primary key which has
     * been created, but the index is not dropped when the primary key is. The
     * index created is created with the same name as the primary key, therefore
     * we can use the dropped primary key name to drop the index.<br>
     * <br>
     * <b>Note: </b>Any Exception raised during this method is assumed that it
     * is due to the index not existing. Since that is the end result we are
     * looking for, the exception is disregarded.
     *
     * @param primaryKey The {@code T8TablePrimaryKey} object which should refer to
     * a primary key which has already been dropped.
     * @param dataConnection The current active {@code Connection} to the database
     */
    private void dropPrimaryKeyIndex(T8TablePrimaryKey primaryKey, T8DataConnection dataConnection)
    {
        PreparedStatement pstmtDropKey = null;

        try
        {
            pstmtDropKey = dataConnection.prepareStatement("ALTER TABLE " + escapeMetaString(primaryKey.getTableName()) + " DROP INDEX " + escapeMetaString(primaryKey.getPrimKeyIdentifier()));
            pstmtDropKey.execute();
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Oracle Primary key index not dropped : " + sqle.getMessage());
        }
        finally
        {
            JDBCUtilities.close(pstmtDropKey);
        }
    }

    @Override
    public void logPrimaryKeyDifferences(String tableName, List<T8DataSourceFieldDefinition> dataFieldDefinitions, T8DataConnection dataConnection)
    {
        T8TablePrimaryKey primaryKey;
        List<String> primaryKeyColumns;

        primaryKey = getTablePrimaryKey(tableName, dataConnection);

        if (!primaryKey.hasPrimaryKey())
        {
            for (T8DataFieldDefinition dataFieldDefinition : dataFieldDefinitions)
            {
                if (dataFieldDefinition.isKey())
                {
                    LOGGER.log("Table " + tableName + " is missing it's primary key containing column " + dataFieldDefinition.getSourceIdentifier());
                }
            }

            return;
        }

        primaryKeyColumns = primaryKey.getPrimaryKeyColumns();
        for (T8DataFieldDefinition dataFieldDefinition : dataFieldDefinitions)
        {
            if (dataFieldDefinition.isKey())
            {
                if (!dataFieldDefinition.isRequired())
                {
                    LOGGER.log("Table " + tableName + " has primary key column " + dataFieldDefinition.getSourceIdentifier() + " which is NOT required");
                }

                if (!primaryKeyColumns.contains(dataFieldDefinition.getSourceIdentifier()))
                {
                    LOGGER.log("Table " + tableName + " if missing primary key column " + dataFieldDefinition.getSourceIdentifier());
                }
            }
        }
    }

    @Override
    public char getMetaEscapeCharacterStart()
    {
        return META_ESCAPE_CHAR;
    }

    @Override
    public char getMetaEscapeCharacterEnd()
    {
        return META_ESCAPE_CHAR;
    }

    @Override
    public String escapeMetaString(String metaString)
    {
        return T8DatabaseAdaptorTools.escapeMetaString(this, metaString);
    }

    @Override
    public String getSqlCurrentDateTimeFunction()
    {
        throw new UnsupportedOperationException("Unsupported Operation. Review the H2 documentation to correct.");
    }
}
