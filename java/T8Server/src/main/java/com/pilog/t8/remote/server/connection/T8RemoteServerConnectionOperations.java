package com.pilog.t8.remote.server.connection;

import static com.pilog.t8.definition.remote.server.connection.T8RemoteServerConnectionAPIHandler.*;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.remote.server.connection.T8ConnectionDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * @author Hennie Brink
 */
public class T8RemoteServerConnectionOperations
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8RemoteServerConnectionOperations.class);

    public static class TestRemoteConnectionOperation extends T8DefaultServerOperation
    {
        public TestRemoteConnectionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ConnectionDefinition remoteConnDefinition;
            String remoteConnectionId;
            String projectId;

            projectId = (String) operationParameters.get(PARAMETER_PROJECT_ID);
            remoteConnectionId = (String) operationParameters.get(PARAMETER_REMOTE_CONNECTION_IDENTIFIER);
            remoteConnDefinition = (T8ConnectionDefinition) operationParameters.get(PARAMETER_REMOTE_CONNECTION_DEFINITION);
            if(remoteConnDefinition == null)
            {
                remoteConnDefinition = (T8ConnectionDefinition) serverContext.getDefinitionManager().getRawDefinition(context, projectId, remoteConnectionId);
                remoteConnectionId = remoteConnDefinition.getIdentifier();
            }

            try
            {
                HttpURLConnection uRLConnection;

                uRLConnection = (HttpURLConnection) new URL(remoteConnDefinition.getURL()).openConnection();

                if(uRLConnection.getResponseCode() == 200)
                {
                    uRLConnection.disconnect();
                    return HashMaps.createSingular(PARAMETER_SUCCESS, true);
                }
                else
                {
                    uRLConnection.disconnect();
                    return HashMaps.createTypeSafeMap(new String[]{PARAMETER_SUCCESS, PARAMETER_FAILURE_MESSAGE}, new Object[]{false, "Server returned invalid response code " + uRLConnection.getResponseCode()});
                }
            }
            catch(IOException ex)
            {
                LOGGER.log("Failed to test connection to remote host using connection details " + remoteConnectionId, ex);
                return HashMaps.createTypeSafeMap(new String[]{PARAMETER_SUCCESS, PARAMETER_FAILURE_MESSAGE}, new Object[]{false, ex.getClass().getSimpleName() + ": " + ex.getMessage()});
            }
        }
    }
}
