package com.pilog.t8.script;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionManagerScriptFacade
{
    private final T8DefinitionManager definitionManager;
    private final T8Context context;
    private final String projectId;

    public T8DefinitionManagerScriptFacade(T8Context context, T8DefinitionManager definitionManager)
    {
        this.definitionManager = definitionManager;
        this.context = context;
        this.projectId = context.getProjectId();
    }

    public T8Definition getRawDefinition(String definitionId) throws Exception
    {
        return this.definitionManager.getRawDefinition(this.context, projectId, definitionId);
    }

    public T8DataType getDataType(String stringRepresentation)
    {
        return definitionManager.createDataType(stringRepresentation);
    }
}
