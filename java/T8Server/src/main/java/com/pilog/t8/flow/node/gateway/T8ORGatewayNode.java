package com.pilog.t8.flow.node.gateway;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.definition.flow.node.gateway.T8FlowForkConditionDefinition;
import com.pilog.t8.definition.flow.node.gateway.T8ORGatewayDefinition;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ORGatewayNode extends T8DefaultFlowNode
{
    private final T8ORGatewayDefinition definition;
    private final Set<String> applicableOutputNodeIds;

    public T8ORGatewayNode(T8Context context, T8Flow parentFlow, T8ORGatewayDefinition definition, String instanceIdentifier)
    {
        super(context, parentFlow, definition, instanceIdentifier);
        this.definition = definition;
        this.applicableOutputNodeIds = new HashSet<String>();
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters)
    {
        Map<String, Object> expressionInputParameters;

        // Strip the flow namespace from the expression input parameters.
        expressionInputParameters = T8IdentifierUtilities.stripNamespace(definition.getNamespace(), inputParameters, false);

        // Evaluate gateway conditions.
        evaluateGatewayConditions(expressionInputParameters);

        // Return the input parameters as output, since we didn't change any of them.
        return inputParameters;
    }

    @Override
    protected boolean isOutputNodeApplicable(String outputNodeIdentifier)
    {
        return applicableOutputNodeIds.contains(outputNodeIdentifier);
    }

    protected void evaluateGatewayConditions(Map<String, Object> expressionInputParameters)
    {
        ArrayList<T8FlowForkConditionDefinition> conditionDefinitions;
        ExpressionEvaluator expressionEvaluator;

        // Create a new expression evaluator to handle the condition checking.
        expressionEvaluator = new ExpressionEvaluator();

        // Clear the set of applicable identifiers before it is regenerated.
        applicableOutputNodeIds.clear();

        // Find the correct fork definition and evaluate it.
        conditionDefinitions = definition.getForkConditionDefinitions();
        for (T8FlowForkConditionDefinition conditionDefinition : conditionDefinitions)
        {
            String expression;

            // Get the fork condition expression.
            expression = conditionDefinition.getConditionExpression();

            // Evaluate the expression and return the result.
            try
            {
                if (expressionEvaluator.evaluateBooleanExpression(expression, expressionInputParameters, null))
                {
                    applicableOutputNodeIds.add(conditionDefinition.getNodeIdentifier());
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while evaluating fork condition '" + expression + "' using parameters: " + expressionInputParameters, e);
            }
        }
    }
}
