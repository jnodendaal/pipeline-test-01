package com.pilog.t8.flow;

import com.pilog.t8.flow.T8FlowNode.FlowNodeStatus;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8FlowUtilities
{
    public static T8FlowNodeState getSynchronizableNodeState(T8Flow parentFlow, T8FlowState flowState, T8FlowNodeState nodeState, List<T8FlowNodeState> nodeStateOptions)
    {
        for (T8FlowNodeState stateOption : nodeStateOptions)
        {
            if (checkStateSynchronization(parentFlow, flowState, nodeState, stateOption))
            {
                return stateOption;
            }
        }

        return null;
    }

    public static boolean checkStateSynchronization(T8Flow parentFlow, T8FlowState flowState, T8FlowNodeState node1State, T8FlowNodeState node2State)
    {
        // Check that both states are active.
        if ((!hasNodeCompleted(node1State)) && (!hasNodeCompleted(node2State)))
        {
            List<String> node1UpStreamNodeIdentifiers;

            node1UpStreamNodeIdentifiers = getUpStreamNodeStateIdentifiers(parentFlow, flowState, node1State, null);
            if (!node1UpStreamNodeIdentifiers.contains(node2State.getNodeIid()))
            {
                List<String> node2UpStreamNodeIdentifiers;

                node2UpStreamNodeIdentifiers = getUpStreamNodeStateIdentifiers(parentFlow, flowState, node2State, null);
                if (!node2UpStreamNodeIdentifiers.contains(node1State.getNodeIid()))
                {
                    // There are some more accurate tests to be done here, because
                    // the two nodes are not necessarily synchronizable at this
                    // stage of the algorithm.
                    return true; // TODO:  Implement better synchronizability checks in this block.
                }
                else
                {
                    // If the first node state is one of the up-stream node states of node 2 then the two cannot be synchronized because they were executed in the same thread.
                    return false;
                }
            }
            else
            {
                // If the second node state is one of the up-stream node states of node 1 then the two cannot be synchronized because they were executed in the same thread.
                return false;
            }
        }
        else
        {
            // Both nodes states need to be active in order for synchronization to be possible.
            return false;
        }
    }

    /**
     * Returns all node state definitions that have the specified node instance
     * as an output.
     *
     * @param flowState The flow state from which the node states will be
     * retrieved.
     * @param nodeInstanceIdentifier The node instance for which parent states
     * will be fetched.
     * @return A list of all node states that have the specified instance as an
     * output.
     */
    public static List<T8FlowNodeState> getParentNodeStates(T8FlowState flowState, String nodeInstanceIdentifier)
    {
        List<T8FlowNodeState> nodeStates;
        List<T8FlowNodeState> parentNodeStates;

        parentNodeStates = new ArrayList<T8FlowNodeState>();
        nodeStates = flowState.getNodeStateList();
        for (T8FlowNodeState nodeState : nodeStates)
        {
            if (nodeState.getOutputNodeMap().containsValue(nodeInstanceIdentifier))
            {
                parentNodeStates.add(nodeState);
            }
        }

        return parentNodeStates;
    }

    /**
     * Returns the node state of the specified type that has the specified node
     * instance as output.
     *
     * @param flowState The flow state from which the node states will be
     * fetched.
     * @param nodeInstanceIdentifier The node instance for which the parent
     * state will be found.
     * @param parentNodeIdentifier The type of parent node state to search for.
     * @return The node state of the specified type that has the specified
     * node instance as output.
     */
    public static T8FlowNodeState getParentNodeState(T8FlowState flowState, String nodeInstanceIdentifier, String parentNodeIdentifier)
    {
        List<T8FlowNodeState> nodeStates;

        nodeStates = flowState.getNodeStateList();
        for (T8FlowNodeState nodeState : nodeStates)
        {
            if (nodeState.getNodeId().equals(parentNodeIdentifier))
            {
                if (nodeState.getOutputNodeMap().containsValue(nodeInstanceIdentifier))
                {
                    return nodeState;
                }
            }
        }

        return null;
    }

    public static T8FlowNodeState getNodeTypeState(List<T8FlowNodeState> nodeStates, String nodeIdentifier)
    {
        for (T8FlowNodeState nodeState : nodeStates)
        {
            if (nodeState.getNodeId().equals(nodeIdentifier)) return nodeState;
        }

        return null;
    }

    public static List<T8FlowNodeState> getInitializedInputNodeStates(T8FlowState flowState, T8FlowNodeState nodeState)
    {
        List<T8FlowNodeState> inputStates;

        inputStates = new ArrayList<T8FlowNodeState>();
        for (T8FlowNodeState inputNodeState : flowState.getNodeStateList())
        {
            if (isNodeStateInitialized(inputNodeState))
            {
                Map<String, Object> outputInstanceIdentifierMap;

                // Check if the target node was an output of this input node state.
                outputInstanceIdentifierMap = inputNodeState.getOutputNodeMap();
                if ((outputInstanceIdentifierMap != null) && (outputInstanceIdentifierMap.containsValue(nodeState.getNodeIid())))
                {
                    inputStates.add(inputNodeState);
                }
            }
        }

        return inputStates;
    }

    /**
     * Returns the list of node states that have the specified target node state
     * as an output.
     * @param flowState The flow state to search.
     * @param nodeState The target node state for which to find input node
     * states.
     * @return The list of node states that have the specified target node state
     * as an output.
     */
    public static List<T8FlowNodeState> getInputNodeStates(T8FlowState flowState, T8FlowNodeState nodeState)
    {
        List<T8FlowNodeState> inputStates;

        inputStates = new ArrayList<T8FlowNodeState>();
        for (T8FlowNodeState inputNodeState : flowState.getNodeStateList())
        {
            Map<String, Object> outputInstanceIdentifierMap;

            // Check if the target node was an output of this input node state.
            outputInstanceIdentifierMap = inputNodeState.getOutputNodeMap();
            if ((outputInstanceIdentifierMap != null) && (outputInstanceIdentifierMap.containsValue(nodeState.getNodeIid())))
            {
                inputStates.add(inputNodeState);
            }
        }

        return inputStates;
    }

    /**
     * Returns the list of node state identifiers that have the specified target
     * node state as an output.
     * @param flowState The flow state to search.
     * @param nodeState The target node state for which to find input node
     * states.
     * @return The list of node state identifiers that have the specified target
     * node state as an output.
     */
    public static List<String> getInputNodeStateIdentifiers(T8FlowState flowState, T8FlowNodeState nodeState)
    {
        List<String> inputStateIdentifiers;

        inputStateIdentifiers = new ArrayList<String>();
        for (T8FlowNodeState inputNodeState : flowState.getNodeStateList())
        {
            Map<String, Object> outputInstanceIdentifierMap;

            // Check if the target node was an output of this input node state.
            outputInstanceIdentifierMap = inputNodeState.getOutputNodeMap();
            if ((outputInstanceIdentifierMap != null) && (outputInstanceIdentifierMap.containsValue(nodeState.getNodeIid())))
            {
                inputStateIdentifiers.add(inputNodeState.getNodeIid());
            }
        }

        return inputStateIdentifiers;
    }

    /**
     * Returns the list of node identifiers of states that have the specified
     * target node state as an output.
     * @param flowState The flow state to search.
     * @param nodeState The target node state for which to find input node
     * states.
     * @return The list of node identifiers of states that have the specified
     * target node state as an output.
     */
    public static List<String> getInputNodeIdentifiers(T8FlowState flowState, T8FlowNodeState nodeState)
    {
        List<String> inputStateIdentifiers;

        inputStateIdentifiers = new ArrayList<String>();
        for (T8FlowNodeState inputNodeState : flowState.getNodeStateList())
        {
            Map<String, Object> outputInstanceIdentifierMap;

            // Check if the target node was an output of this input node state.
            outputInstanceIdentifierMap = inputNodeState.getOutputNodeMap();
            if ((outputInstanceIdentifierMap != null) && (outputInstanceIdentifierMap.containsValue(nodeState.getNodeIid())))
            {
                inputStateIdentifiers.add(inputNodeState.getNodeId());
            }
        }

        return inputStateIdentifiers;
    }

    /**
     * Searches up-stream from the specified node state for the first fork node
     * state that has been activated, following only the input trail of
     * completed node states.
     * @param parentFlow The flow within which to search.
     * @param flowState The flow state within which to search.
     * @param nodeState The node state where the search starts.
     * @return The first fork node state that has been activated, up-stream from
     * the specific start node.
     */
    public static T8FlowNodeState findActiveUpStreamForkNodeState(T8Flow parentFlow, T8FlowState flowState, T8FlowNodeState nodeState)
    {
        LinkedList<T8FlowNodeState> stateQueue;
        Set<String> checkedInstances;

        checkedInstances = new HashSet<String>();
        stateQueue = new LinkedList<T8FlowNodeState>();
        stateQueue.add(nodeState);
        while (stateQueue.size() > 0)
        {
            T8FlowNodeState nextState;
            List<T8FlowNodeState> inputStates;

            nextState = stateQueue.removeFirst();
            inputStates = getInitializedInputNodeStates(flowState, nextState);
            for (T8FlowNodeState inputState : inputStates)
            {
                String nodeInstanceIdentifier;
                String nodeIdentifier;
                List<String> outputIdentifiers;

                // Get the node identifiers.
                nodeInstanceIdentifier = inputState.getNodeIid();
                nodeIdentifier = inputState.getNodeId();
                outputIdentifiers = parentFlow.getOutputNodeIdentifiers(nodeIdentifier);

                // Check if this input node state has more than one output (is a fork).
                if (outputIdentifiers.size() > 1)
                {
                    // We now know the input node is a fork, now check if it is active or has completed.
                    if (hasNodeStarted(inputState))
                    {
                        // Ok so this is a fork node and it has activated, so we can return the state.
                        return inputState;
                    }
                }

                // Add the input node state to the queue so that its input nodes can also be processed.
                if (!checkedInstances.contains(nodeInstanceIdentifier))
                {
                    checkedInstances.add(nodeInstanceIdentifier); // We do this to make sure that the same instance is not added twice in the event of a loop in the flow.
                    stateQueue.add(inputState);
                }
            }
        }

        // No active up-stream fork found.
        return null;
    }

    /**
     * Returns all of the up-stream node states in the execution path that led
     * to the specified target node state.
     * @param parentFlow The flow within which to search.
     * @param flowState The flow state within which to search.
     * @param nodeState The node state where the search starts.
     * @param excludedNodeInstanceIdentifiers Node states to exclude when traversing the up-stream
     * path.
     * @return The list of up-stream node states.
     */
    public static List<T8FlowNodeState> getUpStreamNodeStates(T8Flow parentFlow, T8FlowState flowState, T8FlowNodeState nodeState, List<String> excludedNodeInstanceIdentifiers)
    {
        List<T8FlowNodeState> nodeStates;
        LinkedList<T8FlowNodeState> stateQueue;

        nodeStates = new ArrayList<T8FlowNodeState>();
        stateQueue = new LinkedList<T8FlowNodeState>();
        stateQueue.add(nodeState);
        while (stateQueue.size() > 0)
        {
            T8FlowNodeState nextState;
            List<T8FlowNodeState> inputStates;

            nextState = stateQueue.removeFirst();
            inputStates = getInputNodeStates(flowState, nextState);
            for (T8FlowNodeState inputState : inputStates)
            {
                if (!nodeStates.contains(inputState))
                {
                    // Make sure that we don't need to exclude this input.
                    if ((excludedNodeInstanceIdentifiers == null) || (!excludedNodeInstanceIdentifiers.contains(inputState.getNodeIid())))
                    {
                        nodeStates.add(inputState);
                        stateQueue.add(inputState);
                    }
                }
            }
        }

        return nodeStates;
    }

    /**
     * Returns all of the up-stream node state identifiers in the path that led
     * to the specified target node state.
     * @param parentFlow The flow within which to search.
     * @param flowState The flow state within which to search.
     * @param nodeState The node state where the search starts.
     * @param excludedNodes Node states to exclude when traversing the up-stream
     * path.
     * @return The list of up-stream node state identifiers.
     */
    public static List<String> getUpStreamNodeStateIdentifiers(T8Flow parentFlow, T8FlowState flowState, T8FlowNodeState nodeState, List<String> excludedNodes)
    {
        List<String> stateIdentifiers;
        LinkedList<T8FlowNodeState> stateQueue;

        stateIdentifiers = new ArrayList<String>();
        stateQueue = new LinkedList<T8FlowNodeState>();
        stateQueue.add(nodeState);
        while (stateQueue.size() > 0)
        {
            T8FlowNodeState nextState;
            List<T8FlowNodeState> inputStates;

            nextState = stateQueue.removeFirst();
            inputStates = getInputNodeStates(flowState, nextState);
            for (T8FlowNodeState inputState : inputStates)
            {
                String stateIdentifier;

                stateIdentifier = inputState.getNodeIid();
                if (!stateIdentifiers.contains(stateIdentifier))
                {
                    // Make sure that we don't need to exclude this input.
                    if ((excludedNodes == null) || (!excludedNodes.contains(stateIdentifier)))
                    {
                        stateIdentifiers.add(stateIdentifier);
                        stateQueue.add(inputState);
                    }
                }
            }
        }

        return stateIdentifiers;
    }

    /**
     * Returns a list of the node identifiers of all nodes that are reachable
     * down-stream from the specified node state (excluding paths where one
     * of the nodes in the excluded list is found).  This method uses the
     * flow definition to determine down-stream nodes.
     * @param parentFlow The parent flow context.
     * @param flowState The flow state to search.
     * @param nodeState The node state from where the down-stream search will
     * be performed.
     * @param excludedNodes The list of nodes to be excluded from the search.
     * Their down-stream paths will also be excluded.
     * @return A list of all the node identifier in the down-stream path from
     * the specified node.
     */
    public static List<String> getDownStreamNodeIdentifiers(T8Flow parentFlow, T8FlowState flowState, T8FlowNodeState nodeState, List<String> excludedNodes)
    {
        List<String> nodeIdentifiers;
        LinkedList<String> identifierQueue;

        nodeIdentifiers = new ArrayList<String>();
        identifierQueue = new LinkedList<String>();
        identifierQueue.add(nodeState.getNodeId());
        while (identifierQueue.size() > 0)
        {
            String nextIdentifier;
            List<String> outputIdentifiers;

            nextIdentifier = identifierQueue.removeFirst();
            outputIdentifiers = parentFlow.getOutputNodeIdentifiers(nextIdentifier);
            for (String outputIdentifier : outputIdentifiers)
            {
                if (!nodeIdentifiers.contains(outputIdentifier))
                {
                    // Make sure that we don't need to exclude this output.
                    if ((excludedNodes == null) || (!excludedNodes.contains(outputIdentifier)))
                    {
                        nodeIdentifiers.add(outputIdentifier);
                        identifierQueue.add(outputIdentifier);
                    }
                }
            }
        }

        return nodeIdentifiers;
    }

    /**
     * Returns a list of the node identifiers of all nodes that are reachable
     * via active down-stream paths from the specified node state
     * (excluding paths where one of the nodes in the excluded list is found).
     * This method uses the flow state to determine down-stream nodes i.e. only
     * node states already in the flow state will be taken into account.  If the
     * boolean flag is set to true, once the end of a downstream path is reached
     * and the node is not an end-node, the rest of the inactive path will also
     * be determined and added to the result list.
     * @param parentFlow The parent flow context.
     * @param flowState The flow state to search.
     * @param nodeState The node state from where the down-stream search will
     * be performed.
     * @param excludedNodes The list of nodes to be excluded from the search.
     * Their down-stream paths will also be excluded.
     * @param includeInactiveTrailingPaths
     * @return A list of all the node identifier in the active down-stream path
     * from the specified node.
     */
    public static List<String> getActiveStateDownStreamNodeIdentifiers(T8Flow parentFlow, T8FlowState flowState, T8FlowNodeState nodeState, List<String> excludedNodes, boolean includeInactiveTrailingPaths)
    {
        List<String> nodeIdentifiers;
        LinkedList<T8FlowNodeState> nodeStateQueue;

        nodeIdentifiers = new ArrayList<String>();
        nodeStateQueue = new LinkedList<T8FlowNodeState>();
        nodeStateQueue.add(nodeState);
        while (nodeStateQueue.size() > 0)
        {
            T8FlowNodeState nextState;
            Map<String, Object> outputNodeMap;

            nextState = nodeStateQueue.removeFirst();
            outputNodeMap = nextState.getOutputNodeMap();
            if ((outputNodeMap != null) && (outputNodeMap.size() > 0))
            {
                for (String outputNodeIdentifier : outputNodeMap.keySet())
                {
                    // Make sure not to add the identifier to the list twice (loops).
                    if (!nodeIdentifiers.contains(outputNodeIdentifier))
                    {
                        // Make sure that we don't need to exclude this output.
                        if ((excludedNodes == null) || (!excludedNodes.contains(outputNodeIdentifier)))
                        {
                            String outputNodeInstanceIdentifier;

                            // Get the output node instance identifier.
                            outputNodeInstanceIdentifier = (String)outputNodeMap.get(outputNodeIdentifier);
                            if (outputNodeInstanceIdentifier != null)
                            {
                                nodeIdentifiers.add(outputNodeIdentifier);
                                nodeStateQueue.add(flowState.getNodeState(outputNodeInstanceIdentifier));
                            }
                        }
                    }
                }
            }
            else // This part will be reached once a node is found that has not been activated or the end of the flow (end-node) is reached.
            {
                if (includeInactiveTrailingPaths)
                {
                    nodeIdentifiers.addAll(getDownStreamNodeIdentifiers(parentFlow, flowState, nodeState, excludedNodes));
                }
            }
        }

        return nodeIdentifiers;
    }

    /**
     * This method is critical to successful flow processing.  It is used to
     * determine the active input identifiers for any specified node state
     * within an executing flow state.  The active input identifiers returned
     * from this method are then used by various parts of the flow to determine
     * what input data the target node must wait for before it can activate.
     * @param parentFlow The parent flow context.
     * @param flowState The state of the parent flow context.
     * @param nodeState The node state for which to determine the active input
     * identifiers.
     * @return A list of node identifiers designating the input paths to the
     * specified node state that will have to be waited for before the specified
     * node can activate itself.
     */
    public static List<String> findActiveInputIdentifiers(T8Flow parentFlow, T8FlowState flowState, T8FlowNodeState nodeState)
    {
        T8FlowNodeState activeForkState;
        List<String> inputIdentifiers;
        List<String> activeInputStateIdentifiers;
        List<T8FlowNodeState> upStreamNodeStates;

        // First find the active up-stream node states
        upStreamNodeStates = getUpStreamNodeStates(parentFlow, flowState, nodeState, null);

        // Create a list to hold the active input identifiers.
        activeInputStateIdentifiers = getInputNodeIdentifiers(flowState, nodeState);
        inputIdentifiers = parentFlow.getInputNodeIdentifiers(nodeState.getNodeId());
        if (inputIdentifiers.size() > 1)
        {
            // Find the first up-stream fork node state.
            activeForkState = findActiveUpStreamForkNodeState(parentFlow, flowState, nodeState);
            if (activeForkState != null)
            {
                // Continue to check up-stream fork nodes until none remain.
                while (activeForkState != null)
                {
                    List<T8FlowNodeState> forkOutputNodeStates;

                    // Find all output states of the fork node, that are active.
                    // We are gonna try to find one active output path that leads us to the target node i.e. a path we need to wait for.
                    forkOutputNodeStates = getOutputNodeStates(flowState, activeForkState);
                    for (T8FlowNodeState forkOutputNodeState : forkOutputNodeStates)
                    {
                        // If the output state is part of the up-stream from our target, then we don't need to take it into account because we know already where this path led.
                        if (!upStreamNodeStates.contains(forkOutputNodeState))
                        {
                            // If the active output state of the fork is equal to the target node, we add the fork itself as an active input identifier.
                            if (forkOutputNodeState == nodeState)
                            {
                                activeInputStateIdentifiers.add(activeForkState.getNodeId());
                            }
                            else if (inputIdentifiers.contains(forkOutputNodeState.getNodeId()))
                            {
                                // The active fork output state is one of the target node's inputs.
                                activeInputStateIdentifiers.add(forkOutputNodeState.getNodeId());
                            }
                            else // Now we need to check all output paths from the active fork to see if any of them lead us to the target node.
                            {
                                List<String> downStreamNodeIdentifiers;

                                // For each of the active fork output states, we now get all down-stream node identifiers.
                                downStreamNodeIdentifiers = getActiveStateDownStreamNodeIdentifiers(parentFlow, flowState, forkOutputNodeState, ArrayLists.newArrayList(nodeState.getNodeId()), true);

                                // Now loop through all of the possible input identifiers of our target node and if one of the inputs is part of the down-stream collection it is regarded as active.
                                for (String inputIdentifier : inputIdentifiers)
                                {
                                    // If the input is in the down-stream collection, it means it was reachable via one of the active fork outputs.
                                    if (downStreamNodeIdentifiers.contains(inputIdentifier))
                                    {
                                        // Make sure not to add the identifier twice to the result collection.
                                        if (!activeInputStateIdentifiers.contains(inputIdentifier))
                                        {
                                            activeInputStateIdentifiers.add(inputIdentifier);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Get the next active up-stream fork state.
                    activeForkState = findActiveUpStreamForkNodeState(parentFlow, flowState, activeForkState);
                }

                // Return the list of active identifiers.
                return activeInputStateIdentifiers;
            }
            else
            {
                List<T8FlowNodeState> activeInputNodeStates;

                activeInputNodeStates = getInitializedInputNodeStates(flowState, nodeState);
                for (T8FlowNodeState activeInputNodeState : activeInputNodeStates)
                {
                    activeInputStateIdentifiers.add(activeInputNodeState.getNodeId());
                }

                // No active up-stream fork was found so there are no active inputs.
                return activeInputStateIdentifiers;
            }
        }
        else if ((inputIdentifiers.size() == 1) && (isNodeStateInitialized(nodeState)))
        {
            // There is only one input identifier and it is active, so add it to the result collection and return the collection.
            activeInputStateIdentifiers.addAll(inputIdentifiers);
            return activeInputStateIdentifiers;
        }
        else
        {
            // Return an empty collection because no input identifiers are active.
            return activeInputStateIdentifiers;
        }
    }

    /**
     * This method determines which of the output paths from the specified node
     * state have been started (not necessarily activated).
     * @param flowState The flow state context.
     * @param nodeState The node state for which to determine the started output
     * node states.
     * @return The output node states from the specified target state, that have
     * been started.
     */
    public static final List<T8FlowNodeState> getOutputNodeStates(T8FlowState flowState, T8FlowNodeState nodeState)
    {
        List<T8FlowNodeState> activeOutputStates;
        Map<String, Object> outputInstanceIdentifierMap;

        activeOutputStates = new ArrayList<T8FlowNodeState>();
        outputInstanceIdentifierMap = nodeState.getOutputNodeMap();
        if (outputInstanceIdentifierMap != null)
        {
            for (Object outputInstanceIdentifier : outputInstanceIdentifierMap.values())
            {
                T8FlowNodeState outputState;

                outputState = flowState.getNodeState((String)outputInstanceIdentifier);
                if (outputState != null)
                {
                    activeOutputStates.add(outputState);
                }
            }
        }

        return activeOutputStates;
    }

    public static boolean isNodeStateInitialized(T8FlowNodeState nodeState)
    {
        if (nodeState != null)
        {
            FlowNodeStatus nodeStatus;

            nodeStatus = nodeState.getNodeStatus();
            if (nodeStatus == FlowNodeStatus.NODE_CREATED) return true;
            else if (nodeStatus == FlowNodeStatus.STARTED) return true;
            else if (nodeStatus == FlowNodeStatus.ACTIVE) return true;
            else if (nodeStatus == FlowNodeStatus.COMPLETED) return true;
            else if (nodeStatus == FlowNodeStatus.FAILED) return true;
            else return false;
        }
        else return false;
    }

    public static boolean isNodeActiveOrStarted(T8FlowNodeState nodeState)
    {
        if (nodeState != null)
        {
            FlowNodeStatus nodeStatus;

            nodeStatus = nodeState.getNodeStatus();
            if (nodeStatus == FlowNodeStatus.ACTIVE) return true;
            else if (nodeStatus == FlowNodeStatus.STARTED) return true;
            else return false;
        }
        else return false;
    }

    public static boolean hasNodeCompleted(T8FlowNodeState nodeState)
    {
        if (nodeState != null)
        {
            FlowNodeStatus nodeStatus;

            nodeStatus = nodeState.getNodeStatus();
            return (nodeStatus == FlowNodeStatus.COMPLETED);
        }
        else return false;
    }

    /**
     * Returns Boolean true if the supplied node state has started i.e. its
     * state is either started, active, completed or failed.
     * @param nodeState The state to evaluate.
     * @return Boolean true if the supplied node state has started.
     */
    public static boolean hasNodeStarted(T8FlowNodeState nodeState)
    {
        if (nodeState != null)
        {
            FlowNodeStatus nodeStatus;

            nodeStatus = nodeState.getNodeStatus();
            if (nodeStatus == FlowNodeStatus.ACTIVE) return true;
            else if (nodeStatus == FlowNodeStatus.STARTED) return true;
            else if (nodeStatus == FlowNodeStatus.COMPLETED) return true;
            else if (nodeStatus == FlowNodeStatus.FAILED) return true;
            else return false;
        }
        else return false;
    }

    public static boolean hasFailed(T8FlowNodeState nodeState)
    {
        if (nodeState != null)
        {
            FlowNodeStatus nodeStatus;

            nodeStatus = nodeState.getNodeStatus();
            return (nodeStatus == FlowNodeStatus.FAILED);
        }
        else return false;
    }
}
