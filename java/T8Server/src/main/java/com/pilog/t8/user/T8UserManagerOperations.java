package com.pilog.t8.user;

import com.pilog.t8.T8UserManager;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import java.util.Map;

import static com.pilog.t8.definition.user.T8UserManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8UserManagerOperations
{
    public static class ApiChangeUserProfilePicture extends T8DefaultServerOperation
    {
        public ApiChangeUserProfilePicture(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8UserManager userManager;
            String fileContextIid;
            String filePath;

            fileContextIid = (String)operationParameters.get(PARAMETER_FILE_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);

            userManager = serverContext.getUserManager();
            userManager.updateUserProfilePicture(context, fileContextIid, filePath);
            return null;
        }
    }
}
