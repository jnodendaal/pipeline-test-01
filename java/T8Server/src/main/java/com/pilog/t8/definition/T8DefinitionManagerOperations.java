package com.pilog.t8.definition;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.definition.filter.T8DefinitionFilter;
import com.pilog.t8.project.T8ProjectImportReport;
import com.pilog.t8.system.T8SystemDetails;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.T8DefinitionManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionManagerOperations
{
    public static class CreateDataTypeOperation extends T8DefaultServerOperation
    {
        public CreateDataTypeOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String dataTypeString;

            dataTypeString = (String)operationParameters.get(PARAMETER_DATA_TYPE_STRING);
            return HashMaps.newHashMap(PARAMETER_DATA_TYPE, serverContext.getDefinitionManager().createDataType(dataTypeString));
        }
    }

    public static class GetSystemDetailsOperation extends T8DefaultServerOperation
    {
        public GetSystemDetailsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8SystemDetails systemDetails;

            systemDetails = serverContext.getDefinitionManager().getSystemDetails();
            return HashMaps.newHashMap(PARAMETER_SYSTEM_DETAILS, systemDetails);
        }
    }

    public static class InitializeDefinitionOperation extends T8DefaultServerOperation
    {
        public InitializeDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8Definition definitionToInitialize;

            definitionToInitialize = (T8Definition)operationParameters.get(PARAMETER_DEFINITION);
            serverContext.getDefinitionManager().initializeDefinition(context, definitionToInitialize, operationParameters);
            return HashMaps.newHashMap(PARAMETER_DEFINITION, definitionToInitialize);
        }
    }

    public static class GetInitializedDefinitionOperation extends T8DefaultServerOperation
    {
        public GetInitializedDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                Map<String, Object> inputParameters;
                T8DefinitionManager definitionManager;
                T8Definition initializedDefinition;
                String definitionId;
                String projectId;

                projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
                definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
                inputParameters = (Map<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETERS);

                definitionManager = serverContext.getDefinitionManager();
                initializedDefinition = definitionManager.getInitializedDefinition(context, projectId, definitionId, inputParameters);
                return HashMaps.newHashMap(PARAMETER_DEFINITION, initializedDefinition);
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class GetDefinitionTypeIdentifiersOperation extends T8DefaultServerOperation
    {
        public GetDefinitionTypeIdentifiersOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String groupIdentifier;

            groupIdentifier = (String)operationParameters.get(PARAMETER_DEFINITION_GROUP_ID);
            if (groupIdentifier == null)
            {
                return HashMaps.newHashMap(PARAMETER_DEFINITION_TYPE_ID_LIST, serverContext.getDefinitionManager().getDefinitionTypeIdentifiers());
            }
            else
            {
                return HashMaps.newHashMap(PARAMETER_DEFINITION_TYPE_ID_LIST, serverContext.getDefinitionManager().getDefinitionTypeIdentifiers(groupIdentifier));
            }
        }
    }

    public static class GetDefinitionIdentifiersOperation extends T8DefaultServerOperation
    {
        public GetDefinitionIdentifiersOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String projectId;
            String typeId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            typeId = (String)operationParameters.get(PARAMETER_DEFINITION_TYPE_ID);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_ID_LIST, serverContext.getDefinitionManager().getDefinitionIdentifiers(projectId, typeId));
        }
    }

    public static class GetGroupDefinitionIdentifiersOperation extends T8DefaultServerOperation
    {
        public GetGroupDefinitionIdentifiersOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String projectId;
            String groupId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            groupId = (String)operationParameters.get(PARAMETER_DEFINITION_GROUP_ID);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_ID_LIST, serverContext.getDefinitionManager().getGroupDefinitionIdentifiers(projectId, groupId));
        }
    }

    public static class GetDefinitionGroupIdentifiersOperation extends T8DefaultServerOperation
    {
        public GetDefinitionGroupIdentifiersOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            return HashMaps.newHashMap(PARAMETER_DEFINITION_GROUP_ID_LIST, serverContext.getDefinitionManager().getDefinitionGroupIdentifiers());
        }
    }

    public static class GetDefinitionCompositionOperation extends T8DefaultServerOperation
    {
        public GetDefinitionCompositionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Boolean includeDescendants;
            String definitionId;
            String projectId;

            // Get the operation parameters.
            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
            includeDescendants = (Boolean)operationParameters.get(PARAMETER_INCLUDE_DESCENDANTS);

            // Set some default values.
            if (includeDescendants == null) includeDescendants = false;

            // Execute the operation and return the results.
            return HashMaps.newHashMap(PARAMETER_DEFINITION_COMPOSITION, serverContext.getDefinitionManager().getDefinitionComposition(context, projectId, definitionId, includeDescendants));
        }
    }

    public static class GetDefinitionMetaDataOperation extends T8DefaultServerOperation
    {
        public GetDefinitionMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8DefinitionHandle> definitionHandleList;
            T8DefinitionHandle definitionHandle;
            String definitionId;
            String projectId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
            definitionHandleList = (List<T8DefinitionHandle>)operationParameters.get(PARAMETER_DEFINITION_HANDLE_LIST);
            definitionHandle = (T8DefinitionHandle)operationParameters.get(PARAMETER_DEFINITION_HANDLE);

            if (definitionHandleList != null)
            {
                return HashMaps.newHashMap(PARAMETER_DEFINITION_META_DATA_LIST, serverContext.getDefinitionManager().getDefinitionMetaData(definitionHandleList));
            }
            else if (definitionHandle != null)
            {
                return HashMaps.newHashMap(PARAMETER_DEFINITION_META_DATA, serverContext.getDefinitionManager().getDefinitionMetaData(definitionHandle));
            }
            else return HashMaps.newHashMap(PARAMETER_DEFINITION_META_DATA, serverContext.getDefinitionManager().getDefinitionMetaData(projectId, definitionId));
        }
    }

    public static class GetTypeDefinitionMetaDataOperation extends T8DefaultServerOperation
    {
        public GetTypeDefinitionMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String projectId;
            String typeId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            typeId = (String)operationParameters.get(PARAMETER_DEFINITION_TYPE_ID);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_META_DATA_LIST, serverContext.getDefinitionManager().getTypeDefinitionMetaData(projectId, typeId));
        }
    }

    public static class FinalizeDefinitionOperation extends T8DefaultServerOperation
    {
        public FinalizeDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DefinitionHandle definitionHandle;
            String comment;

            definitionHandle = (T8DefinitionHandle)operationParameters.get(PARAMETER_DEFINITION_HANDLE);
            comment = (String)operationParameters.get(PARAMETER_COMMENT);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_META_DATA, serverContext.getDefinitionManager().finalizeDefinition(context, definitionHandle, comment));
        }
    }

    public static class GetProjectDefinitionMetaDataOperation extends T8DefaultServerOperation
    {
        public GetProjectDefinitionMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String projectId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_META_DATA_LIST, serverContext.getDefinitionManager().getProjectDefinitionMetaData(projectId));
        }
    }

    public static class GetGroupDefinitionMetaDataOperation extends T8DefaultServerOperation
    {
        public GetGroupDefinitionMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String projectId;
            String groupId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            groupId = (String)operationParameters.get(PARAMETER_DEFINITION_GROUP_ID);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_META_DATA_LIST, serverContext.getDefinitionManager().getGroupDefinitionMetaData(projectId, groupId));
        }
    }

    public static class GetDefinitionTypeMetaDataOperation extends T8DefaultServerOperation
    {
        public GetDefinitionTypeMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String typeIdentifier;

            typeIdentifier = (String)operationParameters.get(PARAMETER_DEFINITION_TYPE_ID);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_TYPE_META_DATA, serverContext.getDefinitionManager().getDefinitionTypeMetaData(typeIdentifier));
        }
    }

    public static class GetGroupDefinitionTypeMetaDataOperation extends T8DefaultServerOperation
    {
        public GetGroupDefinitionTypeMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String groupIdentifier;

            groupIdentifier = (String)operationParameters.get(PARAMETER_DEFINITION_GROUP_ID);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_TYPE_META_DATA_LIST, serverContext.getDefinitionManager().getGroupDefinitionTypeMetaData(groupIdentifier));
        }
    }

    public static class GetAllDefinitionGroupMetaDataOperation extends T8DefaultServerOperation
    {
        public GetAllDefinitionGroupMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            return HashMaps.newHashMap(PARAMETER_DEFINITION_GROUP_META_DATA, serverContext.getDefinitionManager().getAllDefinitionGroupMetaData());
        }
    }

    public static class GetAllDefinitionTypeMetaDataOperation extends T8DefaultServerOperation
    {
        public GetAllDefinitionTypeMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            return HashMaps.newHashMap(PARAMETER_DEFINITION_TYPE_META_DATA_LIST, serverContext.getDefinitionManager().getAllDefinitionTypeMetaData());
        }
    }

    public static class ReloadDefinitionsOperation extends T8DefaultServerOperation
    {
        public ReloadDefinitionsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            serverContext.getDefinitionManager().loadDefinitionData(context);
            return HashMaps.newHashMap(PARAMETER_SUCCESS, true);
        }
    }

    public static class MoveDefinitionToProjectOperation extends T8DefaultServerOperation
    {
        public MoveDefinitionToProjectOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                String definitionId;
                String newProjectId;
                String projectId;

                projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
                definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
                newProjectId = (String)operationParameters.get(PARAMETER_NEW_PROJECT_ID);
                serverContext.getDefinitionManager().moveDefinitionToProject(context, projectId, definitionId, newProjectId);
                return HashMaps.newHashMap(PARAMETER_SUCCESS, true);
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class CopyDefinitionToProjectOperation extends T8DefaultServerOperation
    {
        public CopyDefinitionToProjectOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                String definitionId;
                String newProjectId;
                String projectId;

                projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
                definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
                newProjectId = (String)operationParameters.get(PARAMETER_NEW_PROJECT_ID);
                serverContext.getDefinitionManager().copyDefinitionToProject(context, projectId, definitionId, newProjectId);
                return HashMaps.newHashMap(PARAMETER_SUCCESS, true);
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class RenameDefinitionOperation extends T8DefaultServerOperation
    {
        public RenameDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                List<T8DefinitionHandle> affectedDefinitions;
                T8Definition definitionToRename;
                String newIdentifier;

                definitionToRename = (T8Definition)operationParameters.get(PARAMETER_DEFINITION);
                newIdentifier = (String)operationParameters.get(PARAMETER_NEW_DEFINITION_ID);
                affectedDefinitions = serverContext.getDefinitionManager().renameDefinition(context, definitionToRename, newIdentifier);
                return HashMaps.newHashMap(PARAMETER_DEFINITION, definitionToRename, PARAMETER_DEFINITION_HANDLE_LIST, affectedDefinitions);
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class TransferDefinitionReferencesOperation extends T8DefaultServerOperation
    {
        public TransferDefinitionReferencesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                String oldIdentifier;
                String newIdentifier;

                oldIdentifier = (String)operationParameters.get(PARAMETER_OLD_DEFINITION_ID);
                newIdentifier = (String)operationParameters.get(PARAMETER_NEW_DEFINITION_ID);
                serverContext.getDefinitionManager().transferDefinitionReferences(context, oldIdentifier, newIdentifier);
                return HashMaps.newHashMap(PARAMETER_SUCCESS, true);
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class CopyDefinitionOperation extends T8DefaultServerOperation
    {
        public CopyDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                T8Definition definitionToCopy;
                String definitionId;
                String newDefinitionId;
                String projectId;

                definitionToCopy = (T8Definition)operationParameters.get(PARAMETER_DEFINITION);
                projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
                definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
                newDefinitionId = (String)operationParameters.get(PARAMETER_NEW_DEFINITION_ID);

                if (definitionToCopy != null)
                {
                    return HashMaps.newHashMap(PARAMETER_DEFINITION, serverContext.getDefinitionManager().copyDefinition(context, definitionToCopy, newDefinitionId));
                }
                else
                {
                    return HashMaps.newHashMap(PARAMETER_DEFINITION, serverContext.getDefinitionManager().copyDefinition(context, projectId, definitionId, newDefinitionId));
                }
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class CreateNewDefinitionOperation extends T8DefaultServerOperation
    {
        public CreateNewDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                List<Object> parameterList;
                String typeIdentifier;
                String identifier;

                typeIdentifier = (String)operationParameters.get(PARAMETER_DEFINITION_TYPE_ID);
                identifier = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
                parameterList = (List<Object>)operationParameters.get(PARAMETER_DEFINITION_CONSTRUCTOR_PARAMETER_LIST);

                if (parameterList == null)
                {
                    return HashMaps.newHashMap(PARAMETER_DEFINITION, serverContext.getDefinitionManager().createNewDefinition(identifier, typeIdentifier));
                }
                else
                {
                    return HashMaps.newHashMap(PARAMETER_DEFINITION, serverContext.getDefinitionManager().createNewDefinition(identifier, typeIdentifier, parameterList));
                }
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class GetResolvedDefinitionOperation extends T8DefaultServerOperation
    {
        public GetResolvedDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                Map<String, Object> inputParameters;
                String typeIdentifier;

                typeIdentifier = (String)operationParameters.get(PARAMETER_DEFINITION_TYPE_ID);
                inputParameters = (Map<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETERS);
                return HashMaps.newHashMap(PARAMETER_DEFINITION, serverContext.getDefinitionManager().getResolvedDefinition(context, typeIdentifier, inputParameters));
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class GetRawDefinitionOperation extends T8DefaultServerOperation
    {
        public GetRawDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                T8DefinitionHandle definitionHandle;
                String definitionId;
                String projectId;

                projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
                definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
                definitionHandle = (T8DefinitionHandle)operationParameters.get(PARAMETER_DEFINITION_HANDLE);
                if (definitionHandle == null)
                {
                    return HashMaps.newHashMap(PARAMETER_DEFINITION, serverContext.getDefinitionManager().getRawDefinition(context, projectId, definitionId));
                }
                else
                {
                    return HashMaps.newHashMap(PARAMETER_DEFINITION, serverContext.getDefinitionManager().getRawDefinition(context, definitionHandle));
                }
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class LockDefinitionOperation extends T8DefaultServerOperation
    {
        public LockDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                T8DefinitionHandle definitionHandle;
                String keyIdentifier;

                definitionHandle = (T8DefinitionHandle)operationParameters.get(PARAMETER_DEFINITION_HANDLE);
                keyIdentifier = (String)operationParameters.get(PARAMETER_KEY_ID);
                return HashMaps.newHashMap(PARAMETER_DEFINITION, serverContext.getDefinitionManager().lockDefinition(context, definitionHandle, keyIdentifier));
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class UnlockDefinitionOperation extends T8DefaultServerOperation
    {
        public UnlockDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DefinitionHandle definitionHandle;
            T8Definition definitionToUnlock;
            String keyIdentifier;

            definitionHandle = (T8DefinitionHandle)operationParameters.get(PARAMETER_DEFINITION_HANDLE);
            definitionToUnlock = (T8Definition)operationParameters.get(PARAMETER_DEFINITION);
            keyIdentifier = (String)operationParameters.get(PARAMETER_KEY_ID);
            if (definitionToUnlock != null)
            {
                T8Definition unlockDefinition = serverContext.getDefinitionManager().unlockDefinition(context, definitionToUnlock, keyIdentifier);
                return HashMaps.createTypeSafeMap(new String[]{PARAMETER_SUCCESS, PARAMETER_DEFINITION}, new Object[]{true, unlockDefinition});
            }
            else
            {
                T8Definition unlockDefinition = serverContext.getDefinitionManager().unlockDefinition(context, definitionHandle, keyIdentifier);
                return HashMaps.createTypeSafeMap(new String[]{PARAMETER_SUCCESS, PARAMETER_DEFINITION}, new Object[]{true, unlockDefinition});
            }
        }
    }

    public static class GetDefinitionLockDetailsOperation extends T8DefaultServerOperation
    {
        public GetDefinitionLockDetailsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                T8DefinitionHandle definitionHandle;

                definitionHandle = (T8DefinitionHandle)operationParameters.get(PARAMETER_DEFINITION_HANDLE);
                return HashMaps.newHashMap(PARAMETER_DEFINITION_LOCK_DETAILS, serverContext.getDefinitionManager().getDefinitionLockDetails(context, definitionHandle));
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class GetRawGroupDefinitionsOperation extends T8DefaultServerOperation
    {
        public GetRawGroupDefinitionsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String projectId;
            String groupId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            groupId = (String)operationParameters.get(PARAMETER_DEFINITION_GROUP_ID);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_LIST, serverContext.getDefinitionManager().getRawGroupDefinitions(context, projectId, groupId));
        }
    }

    public static class GetInitializedGroupDefinitionsOperation extends T8DefaultServerOperation
    {
        public GetInitializedGroupDefinitionsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Map<String, Object> inputParameters;
            String projectId;
            String groupId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            groupId = (String)operationParameters.get(PARAMETER_DEFINITION_GROUP_ID);
            inputParameters = (Map<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETERS);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_LIST, serverContext.getDefinitionManager().getInitializedGroupDefinitions(context, projectId, groupId, inputParameters));
        }
    }

    public static class RemoveDefinitionReferencesOperation extends T8DefaultServerOperation
    {
        public RemoveDefinitionReferencesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String definitionIdentifier;
            String keyIdentifier;

            definitionIdentifier = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
            keyIdentifier = (String)operationParameters.get(PARAMETER_KEY_ID);
            serverContext.getDefinitionManager().removeDefinitionReferences(context, definitionIdentifier, keyIdentifier);
            return HashMaps.createSingular(PARAMETER_SUCCESS, true);
        }
    }

    public static class DeleteDefinitionOperation extends T8DefaultServerOperation
    {
        public DeleteDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Boolean> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                T8DefinitionHandle definitionHandle;
                String definitionId;
                Boolean safeDeletion;
                String projectId;

                safeDeletion = (Boolean)operationParameters.get(PARAMETER_SAFE_DELETION);
                projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
                definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
                definitionHandle = (T8DefinitionHandle)operationParameters.get(PARAMETER_DEFINITION_HANDLE);
                if (definitionHandle == null)
                {
                    serverContext.getDefinitionManager().deleteDefinition(context, projectId, definitionId, safeDeletion == null || safeDeletion, null);
                    return HashMaps.createSingular(PARAMETER_SUCCESS, true);
                }
                else
                {
                    serverContext.getDefinitionManager().deleteDefinition(context, definitionHandle, safeDeletion == null || safeDeletion, null);
                    return HashMaps.createSingular(PARAMETER_SUCCESS, true);
                }
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class SaveDefinitionOperation extends T8DefaultServerOperation
    {
        public SaveDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                T8DefinitionMetaData metaData;
                T8Definition definitionToSave;

                definitionToSave = (T8Definition)operationParameters.get(PARAMETER_DEFINITION);
                metaData = serverContext.getDefinitionManager().saveDefinition(context, definitionToSave, null);
                return HashMaps.newHashMap(PARAMETER_DEFINITION_META_DATA, metaData);
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class CheckIdentifierAvailabilityOperation extends T8DefaultServerOperation
    {
        public CheckIdentifierAvailabilityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                String definitionId;
                String projectId;

                projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
                definitionId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
                return HashMaps.newHashMap(PARAMETER_SUCCESS, serverContext.getDefinitionManager().checkIdentifierAvailability(context, projectId, definitionId));
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class FindDefinitionsByTextOperation extends T8DefaultServerOperation
    {
        public FindDefinitionsByTextOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String text;

            text = (String)operationParameters.get(PARAMETER_TEXT);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_HANDLE_LIST, serverContext.getDefinitionManager().findDefinitionsByText(context, text));
        }
    }

    public static class FindDefinitionsByRegularExpressionOperation extends T8DefaultServerOperation
    {
        public FindDefinitionsByRegularExpressionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String expression;

            expression = (String)operationParameters.get(PARAMETER_REGULAR_EXPRESSION);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_HANDLE_LIST, serverContext.getDefinitionManager().findDefinitionsByRegularExpression(context, expression));
        }
    }

    public static class FindDefinitionsByIdentifierOperation extends T8DefaultServerOperation
    {
        public FindDefinitionsByIdentifierOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            Boolean includeReferencesOnly;
            String identifier;

            identifier = (String)operationParameters.get(PARAMETER_DEFINITION_ID);
            includeReferencesOnly = (Boolean)operationParameters.get(PARAMETER_INCLUDE_REFERENCES_ONLY);
            if (includeReferencesOnly == null) includeReferencesOnly = false;

            return HashMaps.createSingular(PARAMETER_DEFINITION_HANDLE_LIST, serverContext.getDefinitionManager().findDefinitionsByIdentifier(context, identifier, includeReferencesOnly));
        }
    }

    public static class GetAllDefinitionHandlesOperation extends T8DefaultServerOperation
    {
        public GetAllDefinitionHandlesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            if ((operationParameters != null) && (operationParameters.size() > 0))
            {
                T8DefinitionFilter definitionFilter;

                definitionFilter = (T8DefinitionFilter)operationParameters.get(PARAMETER_DEFINITION_FILTER);
                return HashMaps.newHashMap(PARAMETER_DEFINITION_HANDLE_LIST, serverContext.getDefinitionManager().getAllDefinitionHandles(context, definitionFilter));
            }
            else throw new Exception("No operation parameters found.");
        }
    }

    public static class GetAllDefinitionMetaDataOperation extends T8DefaultServerOperation
    {
        public GetAllDefinitionMetaDataOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DefinitionFilter definitionFilter;

            definitionFilter = (T8DefinitionFilter)operationParameters.get(PARAMETER_DEFINITION_FILTER);
            return HashMaps.newHashMap(PARAMETER_DEFINITION_META_DATA_LIST, serverContext.getDefinitionManager().getAllDefinitionMetaData(context, definitionFilter));
        }
    }

    public static class ImportDefinitionOperation extends T8DefaultServerOperation
    {
        public ImportDefinitionOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8Definition definitionToImport;

            definitionToImport = (T8Definition)operationParameters.get(PARAMETER_DEFINITION);
            serverContext.getDefinitionManager().importDefinition(context, definitionToImport);
            return HashMaps.newHashMap(PARAMETER_SUCCESS, true);
        }
    }

    public static class ExportProjectsOperation extends T8DefaultServerOperation
    {
        public ExportProjectsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<String> projectIds;
            String fileContextIid;
            String filePath;

            projectIds = (List<String>)operationParameters.get(PARAMETER_PROJECT_IDS);
            fileContextIid = (String)operationParameters.get(PARAMETER_FILE_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            serverContext.getDefinitionManager().exportProjects(context, fileContextIid, filePath, projectIds);
            return null;
        }
    }

    public static class ImportProjectsOperation extends T8DefaultServerOperation
    {
        public ImportProjectsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProjectImportReport report;
            List<String> projectIds;
            String fileContextIid;
            String filePath;

            projectIds = (List<String>)operationParameters.get(PARAMETER_PROJECT_IDS);
            fileContextIid = (String)operationParameters.get(PARAMETER_FILE_CONTEXT_IID);
            filePath = (String)operationParameters.get(PARAMETER_FILE_PATH);
            report = serverContext.getDefinitionManager().importProjects(context, fileContextIid, filePath, projectIds);
            return HashMaps.newHashMap(PARAMETER_PROJECT_IMPORT_REPORT, report);
        }
    }

    public static class DeleteProjectsOperation extends T8DefaultServerOperation
    {
        public DeleteProjectsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<String> projectIds;

            projectIds = (List<String>)operationParameters.get(PARAMETER_PROJECT_IDS);
            serverContext.getDefinitionManager().deleteProjects(context, projectIds);
            return null;
        }
    }
}
