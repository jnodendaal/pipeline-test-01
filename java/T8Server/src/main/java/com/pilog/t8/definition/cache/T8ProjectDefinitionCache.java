package com.pilog.t8.definition.cache;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.files.FileUtilities;
import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectDefinitionCache extends T8FileDefinitionCache
{
    private T8ProjectDefinition projectDefinition;
    private String projectId;
    private File rootPath;
    private boolean initialized;

    public T8ProjectDefinitionCache(T8Context context, T8DefinitionManager definitionManager, File rootPath, T8ProjectDefinition projectDefinition, Set<String> typeIds) throws Exception
    {
        super(context, definitionManager, T8ProjectDefinitionCache.getProjectPath(rootPath, projectDefinition.getIdentifier()), projectDefinition.getIdentifier(), typeIds);
        this.projectDefinition = projectDefinition;
        this.rootPath = rootPath;
        this.initialized = false;
        projectRenamed(projectDefinition.getIdentifier());
    }

    public void projectDefinitionSaved(T8ProjectDefinition projectDefinition)
    {
        this.projectDefinition = projectDefinition;
    }

    /**
     * If this method is invoked before the cache has been initialized, the
     * local variable will simply be set.  If invoked after cache initialization
     * this method will rename the project folder.
     * @param newProjectId
     * @throws java.lang.Exception
     */
    public void projectRenamed(String newProjectId) throws Exception
    {
        if (!Objects.equals(this.projectId, newProjectId))
        {
            if (initialized)
            {
                File currentProjectPath;
                File newProjectPath;

                // Get the current and new project paths.
                T8Log.log("Changing project definition cache identifier from '" + projectId + "' to '" + newProjectId + "'...");
                currentProjectPath = getProjectPath(rootPath, projectId);
                newProjectPath = getProjectPath(rootPath, newProjectId);

                // Update the local project id.
                this.projectId = newProjectId;

                // Rename the project folder.
                T8Log.log("Renaming project folder '" + currentProjectPath + "' to '" + newProjectPath + "'...");
                currentProjectPath.renameTo(newProjectPath);

                // Make sure the old project folder has been deleted.
                if (currentProjectPath.exists()) currentProjectPath.delete();

                // Reset the context path on definition caches.
                super.setContextPath(newProjectPath);
            }
            else
            {
                this.projectId = newProjectId;
            }
        }
    }

    public void delete()
    {
        // Delete the folder.
        FileUtilities.deleteDirectory(getProjectPath(rootPath, projectId));

        // Clear the cache.
        clearCache();
    }

    public void backup() throws Exception
    {
        File backupPath;

        // Delete the old back folder.
        backupPath = getBackupPath(rootPath, projectId);
        FileUtilities.deleteDirectory(backupPath);

        // Copy the current folder to the backup location.
        FileUtilities.copy(getProjectPath(rootPath, projectId), backupPath);
    }

    public void restore() throws Exception
    {
        File backupPath;

        // Get the backup location and make sure that it exists.
        backupPath = getBackupPath(rootPath, projectId);
        if (backupPath.exists())
        {
            // Clear the cache.
            clearCache();

            // Delete the current folder.
            delete();

            // Copy the backup folder to the current location.
            FileUtilities.copy(backupPath, getProjectPath(rootPath, projectId));

            // Cache the new definition data.
            cacheDefinitions();
        }
        else throw new IllegalStateException("No backup found at: " + backupPath);
    }

    public String getProjectId()
    {
        return projectId;
    }

    public List<String> getRequiredProjectIds()
    {
        return projectDefinition.getRequiredProjectIds();
    }

    @Override
    public void init()
    {
        // Now do the normal super initialization method.
        super.init();

        // Set the initialized flag.
        initialized = true;
    }

    private static File getProjectPath(File rootPath, String projectId)
    {
        String projectFolderName;

        // Create the context path for all project definitions.
        projectFolderName = projectId.substring(1).toLowerCase();
        return new File(rootPath.getAbsolutePath() + "/" + projectFolderName);
    }

    private static File getBackupPath(File rootPath, String projectId)
    {
        String projectFolderName;

        // Create the context path for all project definitions.
        projectFolderName = projectId.substring(1).toLowerCase();
        return new File(rootPath.getAbsolutePath() + "/backups/" + projectFolderName);
    }
}
