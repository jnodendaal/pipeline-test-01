package com.pilog.t8.functionality;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.api.T8DataObjectApi;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.data.object.T8DataObjectState;
import com.pilog.t8.data.object.T8DataObjectStateGraph;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.event.T8FlowEventAdapter;
import com.pilog.t8.flow.event.T8FlowFinalizedEvent;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.system.event.T8OperationCompletedEvent;
import com.pilog.t8.system.event.T8ServerEventListener;
import com.pilog.t8.translation.T8TranslatableString;
import com.pilog.t8.ui.functionality.T8TargetObjectValidationResult;
import com.pilog.t8.definition.data.object.T8DataObjectDefinition;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateGraphDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.functionality.access.T8FunctionalityAccessPolicyDefinition;
import com.pilog.t8.definition.functionality.group.T8FunctionalityGroupDefinition;
import com.pilog.t8.definition.script.T8DataObjectValidationScriptDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.functionality.access.T8FunctionalityAccessPolicy;
import com.pilog.t8.functionality.access.T8FunctionalityBlock;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.event.T8SessionEventListener;
import com.pilog.t8.security.event.T8SessionLogoutEvent;
import com.pilog.t8.utilities.strings.Strings;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.data.entity.T8FunctionalityDataObjectDefinition;
import com.pilog.t8.definition.functionality.T8WorkflowFunctionalityDefinition;
import com.pilog.t8.definition.security.T8DataAccessDefinition;
import com.pilog.t8.flow.event.T8FlowCompletedEvent;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle.T8FunctionalityAccessType;
import com.pilog.t8.functionality.access.T8FunctionalityAccessRights;
import com.pilog.t8.functionality.access.T8FunctionalityDataObjectAccessRights;
import com.pilog.t8.functionality.state.T8FunctionalityDataObjectState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.util.ArrayList;

/**
 * @author Bouwer du Preez
 */
public class T8ServerFunctionalityManager implements T8FunctionalityManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerFunctionalityManager.class);

    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8FunctionalityStateHandler functionalityStateHandler;
    private final T8DefinitionManager definitionManager;
    private final T8ConfigurationManager configurationManager;
    private final Map<String, T8DataObjectStateGraph> stateGraphs;
    private final Map<String, T8DataObjectDefinition> dataObjectDefinitionCache;
    private final Map<String, T8FunctionalityDefinition> functionalityDefinitionCache;
    private final T8FunctionalityBlockingManager blockingManager;
    private final SessionLogoutListener sessionListener;
    private final WorkFlowListener flowListener;
    private final OperationListener operationListener;

    private T8FunctionalityAccessPolicy accessPolicy;

    public T8ServerFunctionalityManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.internalContext = serverContext.getSecurityManager().createServerModuleContext(T8ServerFunctionalityManager.this);
        this.definitionManager = serverContext.getDefinitionManager();
        this.configurationManager = serverContext.getConfigurationManager();
        this.functionalityStateHandler = new T8FunctionalityStateHandler(internalContext);
        this.stateGraphs = new HashMap<>();
        this.dataObjectDefinitionCache = new HashMap<>();
        this.functionalityDefinitionCache = new HashMap<>();
        this.blockingManager = new T8FunctionalityBlockingManager(internalContext);
        this.sessionListener = new SessionLogoutListener();
        this.flowListener = new WorkFlowListener();
        this.operationListener = new OperationListener();
    }

    @Override
    public void init()
    {
        try
        {
            // Load the access policy.
            reloadAccessPolicy(internalContext);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing functionality manager.", e);
        }
    }

    @Override
    public void start() throws Exception
    {
        try
        {
            // Add a server listener so that we can kill functionalities linked to an operation when it ends.
            serverContext.addServerEventListener(operationListener);

            // Add a session listener so that we can kill functionalities linked to a session when it is logged out.
            serverContext.getSecurityManager().addSessionEventListener(sessionListener);

            // Add a workflow listener so that we can kill functionalities linked to a workflow when it ends.
            serverContext.getFlowManager().addFlowEventListener(internalContext, flowListener);

            // Initialize the functionality state handler.
            functionalityStateHandler.init();

            // Initialize the blocking manager.
            blockingManager.init();

            // Delete residual functionality states.
            LOGGER.log("Deleting residual functionality states...");
            deleteResidualFunctionalityStates();
        }
        catch(SQLException sqle)
        {
            LOGGER.log("Failed to start Functionality Manager", sqle);
        }
    }

    @Override
    public void destroy()
    {
        serverContext.removeServerEventListener(operationListener);
        serverContext.getSecurityManager().removeSessionEventListener(sessionListener);
        serverContext.getFlowManager().removeFlowEventListener(internalContext, flowListener);
    }

    /**
     * Deletes all functionality states that are residual states from an
     * abnormal system shutdown (such as an Application Server crash).
     * @param context
     */
    private void deleteResidualFunctionalityStates() throws Exception
    {
        T8DataIterator<String> iidIterator;

        iidIterator = functionalityStateHandler.getResidualFunctionalityIidIterator();
        try
        {
            while (iidIterator.hasNext())
            {
                String functionalityIid;
                T8DataTransaction tx;

                // Get the next functionality instance identifier.
                functionalityIid = iidIterator.next();

                // Get an instant transaction for this operation.
                tx = serverContext.getDataManager().getCurrentSession().instantTransaction();;

                // Remove all functionlity blocks spawned by this instance.
                blockingManager.removeFunctionalityBlocks(tx, functionalityIid);

                // Remove the functionality state.
                functionalityStateHandler.deleteFunctionalityState(tx, functionalityIid);
            }
        }
        finally
        {
            iidIterator.close();
        }
    }

    @Override
    public void reloadAccessPolicy(T8Context context)
    {
        T8SystemDefinition systemDefinition;

        systemDefinition = definitionManager.getSystemDefinition(internalContext);
        if (systemDefinition != null)
        {
            try
            {
                String accessPolicyId;

                accessPolicyId = systemDefinition.getFunctionalityAccessPolicyId();
                if (accessPolicyId != null)
                {
                    T8FunctionalityAccessPolicyDefinition accessPolicyDefinition;

                    accessPolicyDefinition = (T8FunctionalityAccessPolicyDefinition)definitionManager.getInitializedDefinition(internalContext, internalContext.getProjectId(), accessPolicyId, null);
                    if (accessPolicyDefinition != null)
                    {
                        accessPolicy = accessPolicyDefinition.createNewAccessPolicyInstance(internalContext);
                    }
                    else throw new Exception("Functionality access policy not found: " + accessPolicyId);
                }
                else
                {
                    LOGGER.log("WARNING:  No functionality access Policy set!");
                    accessPolicy = new T8FunctionalityAccessPolicy(internalContext, null);
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while initializing functionality manager.", e);
            }
        }
        else throw new RuntimeException("System definition not found.");
    }

    @Override
    public void clearCache(T8Context context)
    {
        LOGGER.log("Functionality Cache cleared by " + context.getSessionContext().getUserIdentifier());
        this.dataObjectDefinitionCache.clear();
        this.functionalityDefinitionCache.clear();
        this.stateGraphs.clear();
    }

    private String translate(String inputString)
    {
        return configurationManager.getUITranslation(internalContext, inputString);
    }

    public T8FunctionalityState getFunctionalityState(T8DataTransaction tx, String functionalityIid) throws Exception
    {
        return functionalityStateHandler.retrieveFunctionalityState(tx, functionalityIid);
    }

    @Override
    public T8FunctionalityAccessRights getFunctionalityAccessRights(T8Context context, String functionalityIid)
    {
        T8FunctionalityState state;
        T8DataTransaction tx;

        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        state = functionalityStateHandler.retrieveFunctionalityState(tx, functionalityIid);
        if (state != null)
        {
            T8FunctionalityDefinition definition;

            definition = getFunctionalityDefinition(state.getFunctionalityId());
            if (definition != null)
            {
                T8DataAccessDefinition dataAccessDefinition;
                T8FunctionalityAccessRights rights;

                // Set the default rights of the functionality using the details of the definition and state.
                rights = new T8FunctionalityAccessRights();
                rights.setProjectId(definition.getRootProjectId());
                rights.setFunctionalityId(state.getFunctionalityId());
                rights.setFunctionalityIid(state.getFunctionalityIid());
                rights.setOperationIds(definition.getOperationIds());

                // Set specific data access if available.
                dataAccessDefinition = definition.getDataAccessDefinition();
                if (dataAccessDefinition != null)
                {
                    rights.setDataAccess(dataAccessDefinition.getDataAccess());
                }

                // Set specific object rights.
                for (T8FunctionalityDataObjectState objectState : state.getDataObjectStates())
                {
                    rights.addObjectAccessRights(new T8FunctionalityDataObjectAccessRights(objectState.getDataObjectId(), objectState.getDataObjectIid()));
                }

                return rights;
            }
            else throw new RuntimeException("Functionality found in state data but no supporting definition found: " + state.getFunctionalityId());
        }
        else return null;
    }

    @Override
    public T8FunctionalityAccessRights getWorkflowAccessRights(T8Context context, String flowIid)
    {
        List<T8FunctionalityState> states;
        T8DataTransaction tx;

        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        states = functionalityStateHandler.retrieveWorkflowFunctionalityStates(tx, flowIid);
        if (states.size() > 0)
        {
            if (states.size() == 1)
            {
                T8FunctionalityDefinition definition;
                T8FunctionalityState state;

                state = states.get(0);
                definition = getFunctionalityDefinition(state.getFunctionalityId());
                if (definition != null)
                {
                    T8FunctionalityAccessRights rights;

                    rights = new T8FunctionalityAccessRights();
                    rights.setProjectId(definition.getRootProjectId());
                    rights.setFunctionalityId(state.getFunctionalityId());
                    rights.setFunctionalityIid(state.getFunctionalityIid());
                    rights.setOperationIds(definition.getOperationIds());

                    for (T8FunctionalityDataObjectState objectState : state.getDataObjectStates())
                    {
                        rights.addObjectAccessRights(new T8FunctionalityDataObjectAccessRights(objectState.getDataObjectId(), objectState.getDataObjectIid()));
                    }

                    return rights;
                }
                else throw new RuntimeException("Functionality found in state data but no supporting definition found: " + state.getFunctionalityId());
            }
            else throw new RuntimeException("Access to workflow '" + flowIid + "' is ambiguous.  More than one functionality is currently linked to this workflow: " + states);
        }
        else return null;
    }

    @Override
    public T8FunctionalityAccessHandle accessFunctionality(T8Context context, String functionalityId, Map<String, Object> parameters) throws Exception
    {
        T8FunctionalityDefinition functionalityDefinition;

        functionalityDefinition = (T8FunctionalityDefinition)definitionManager.getInitializedDefinition(context, null, functionalityId, null);
        if (functionalityDefinition != null)
        {
            List<T8DataObject> targetDataObjects;
            T8DataTransaction tx;
            T8DataObjectApi objApi;

            // Get the transaction to use for this operaiton.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
            objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);

            // Retrieve the target objects (if any).
            targetDataObjects = new ArrayList<>();
            if (parameters != null)
            {
                for (T8FunctionalityDataObjectDefinition objectDefinition : functionalityDefinition.getDataObjects())
                {
                    String inputParameterId;

                    inputParameterId = objectDefinition.getFunctionalityInputParameterId();
                    if (inputParameterId != null)
                    {
                        String dataObjectIid;

                        dataObjectIid = (String)parameters.get(functionalityId + inputParameterId);
                        if (dataObjectIid != null)
                        {
                            T8DataObject object;
                            String dataObjectId;

                            dataObjectId = objectDefinition.getDataObjectId();
                            object = objApi.retrieve(dataObjectId, dataObjectIid, false);
                            if (object != null) targetDataObjects.add(object);
                        }
                    }
                }
            }

            // Synchronize on blocking manager while checking for existing
            // blocks and adding new blocks so that no other thread can
            // interfere with this transaction.
            synchronized (blockingManager)
            {
                T8FunctionalityBlock existingBlock;

                // First check that the functionality we want to access is not currently blocked.
                existingBlock = blockingManager.getFunctionalityBlock(context, functionalityId, targetDataObjects);
                if (existingBlock == null) // Ok no blocks found, so proceed.
                {
                    T8TargetObjectValidationResult validationResult;
                    T8FunctionalityInstance instance;
                    String functionalityIid;

                    // Construct the instance.
                    instance = functionalityDefinition.getNewFunctionalityInstance(context);
                    functionalityIid = instance.getFunctionalityIid();

                    // Validate the target object.
                    validationResult = instance.validateTargetObjects(parameters);
                    if (validationResult.isValid())
                    {
                        T8FunctionalityAccessHandle accessHandle;
                        List<T8FunctionalityBlock> newBlockList;

                        // Add the functionality blocks applicable.
                        newBlockList = accessPolicy.getFunctionalityBlocks(context, functionalityId, targetDataObjects);
                        blockingManager.addFunctionalityBlocks(functionalityIid, newBlockList);

                        // Execute the functionality.
                        try
                        {
                            accessHandle = instance.access(parameters);
                        }
                        catch (Exception e)
                        {
                            // If the functionality fails, we have to remove the blocks we put in place.
                            blockingManager.removeFunctionalityBlocks(tx, functionalityIid);
                            throw e;
                        }

                        // Persist the functionality state.
                        functionalityStateHandler.insertFunctionalityState(tx, instance.getFunctionalityState());

                        // Log the functionality event.
                        functionalityStateHandler.logFunctionalityEvent(tx, FunctionalityHistoryEvent.ACCESSED, instance.getFunctionalityState());

                        // Return the access handle.
                        return accessHandle;
                    }
                    else
                    {
                        // Return the applicable functionality denied handle.
                        return new T8DeniedFunctionalityAccessHandle(functionalityDefinition.getNewFunctionalityHandle(context), T8FunctionalityAccessType.DENIED_VALIDATION, validationResult.getMessage());
                    }
                }
                else
                {
                    T8TranslatableString message;

                    // Return the applicable functionality denied handle.
                    message = existingBlock.getMessage();
                    return new T8DeniedFunctionalityAccessHandle(functionalityDefinition.getNewFunctionalityHandle(context), T8FunctionalityAccessType.DENIED_BLOCKED, message.translateString(configurationManager, context));
                }
            }
        }
        else throw new Exception("Functionality Definition not found: " + functionalityId);
    }

    @Override
    public void accessDataObject(T8DataTransaction tx, String functionalityIid, String dataObjectId, String dataObjectIid) throws Exception
    {
        T8FunctionalityState functionalityState;

        // Get the functionality state.
        functionalityState = functionalityStateHandler.retrieveFunctionalityState(tx, functionalityIid);
        if (functionalityState != null)
        {
            T8FunctionalityDefinition functionalityDefinition;
            String functionalityId;

            // Get the functionality definition using its identifier.
            functionalityId = functionalityState.getFunctionalityId();
            functionalityDefinition = getFunctionalityDefinition(functionalityId);
            if (functionalityDefinition != null)
            {
                List<String> accessibleObjectIds;

                // Make sure the functionality allows access to the object.
                accessibleObjectIds = functionalityDefinition.getObjectIds();
                if (accessibleObjectIds.contains(dataObjectId))
                {
                    if (!functionalityState.containsDataObjectState(dataObjectIid))
                    {
                        T8FunctionalityDataObjectState object;

                        object = new T8FunctionalityDataObjectState(functionalityIid, dataObjectId, dataObjectIid);
                        object.setFunctionalityIid(functionalityIid);
                        functionalityStateHandler.insertFunctionalityDataObjectState(tx, object);
                    }
                    else throw new Exception("Functionality already has access to data object: " + dataObjectId);
                }
                else throw new Exception("Functionality does not allow for access to data objects: " + dataObjectId);
            }
            else throw new Exception("Functionality definition not found: " + functionalityId);
        }
        else throw new Exception("Functionality cannot be released, because it is not being accessed: " + functionalityIid);
    }

    @Override
    public void releaseFunctionality(T8Context context, String functionalityIid) throws Exception
    {
        T8FunctionalityState functionalityState;
        T8DataTransaction tx;

        // Get the functionality state.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        functionalityState = functionalityStateHandler.retrieveFunctionalityState(tx, functionalityIid);
        if (functionalityState != null)
        {
            T8FunctionalityDefinition functionalityDefinition;
            String functionalityId;

            // Get the functionality definition using its identifier.
            functionalityId = functionalityState.getFunctionalityId();
            functionalityDefinition = getFunctionalityDefinition(functionalityId);
            if (functionalityDefinition != null)
            {
                T8FunctionalityInstance instance;

                // Create an instance of the functionality.
                instance = functionalityDefinition.getNewFunctionalityInstance(context, functionalityState);
                if (instance.release())
                {
                    // Remove all functionlity blocks spawned by this instance.
                    blockingManager.removeFunctionalityBlocks(tx, functionalityIid);

                    // Remove the functionality state.
                    functionalityStateHandler.deleteFunctionalityState(tx, functionalityIid);

                    // Log the functionality event.
                    functionalityStateHandler.logFunctionalityEvent(tx, FunctionalityHistoryEvent.RELEASED, instance.getFunctionalityState());
                }
                else throw new Exception("Cannot release functionality: " + functionalityDefinition + " (" + functionalityIid + ")");
            }
            else throw new Exception("Functionality definition not found: " + functionalityId);
        }
        else throw new Exception("Functionality cannot be released, because it is not being accessed: " + functionalityIid);
    }

    @Override
    public T8FunctionalityGroupHandle getFunctionalityGroupHandle(T8Context context, String functionalityGroupId, String dataObjectId, String dataObjectIid) throws Exception
    {
        T8FunctionalityGroupHandle groupHandle;
        T8DataObjectApi objApi;
        T8DataObject targetObject;
        T8DataTransaction tx;

        // Get the group handle.
        groupHandle = getFunctionalityGroupHandle(context, functionalityGroupId);

        // Get the dataObject;
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
        targetObject = ((dataObjectId != null) && (dataObjectIid != null)) ? objApi.retrieve(dataObjectId, dataObjectIid, true) : null;

        // Remove all functionalities that are not applicable to the specified data object.
        // Also set the input parameters of applicable functionalities to reference this target object.
        groupHandle.setTargetDataObject(targetObject);

        // Disable all functionalities that are not applicable to the current object state.
        if (targetObject != null)
        {
            T8DataObjectState currentState;

            currentState = targetObject.getState();
            if (currentState != null)
            {
                // Enable only functionalities applicable to this state.
                groupHandle.setFunctionalityEnabled(currentState.getFunctionalityIdSet(), true, true, false, translate("The selected object is not in the correct state for this operation."));
            }
            else
            {
                // Enabled only independent functionalities.
                groupHandle.setFunctionalityEnabled(null, true, true, false, translate("The selected object is not in the correct state for this operation."));
            }
        }

        // Disable all functionalities currently blocked by executing instances.
        for (T8FunctionalityHandle handle : groupHandle.getAllFunctionalities())
        {
            T8TargetObjectValidationResult validationResult;
            T8FunctionalityBlock block;
            String functionalityId;

            // Check for functionality blocks.
            functionalityId = handle.getId();
            block = blockingManager.getFunctionalityBlock(context, functionalityId, targetObject != null ? ArrayLists.newArrayList(targetObject) : new ArrayList<>());
            if (block != null)
            {
                T8TranslatableString message;

                handle.setEnabled(false);
                message = block.getMessage();
                if (message != null)
                {
                    handle.setDisabledMessage(message.translateString(configurationManager, context));
                }
            }

            // Perform data object validations.
            validationResult = validateTargetObject(context, handle.getId(), targetObject);
            if (!validationResult.isValid())
            {
                handle.setEnabled(false);
                handle.setDisabledMessage(validationResult.getMessage());
            }
        }

        // Return the group handle.
        return groupHandle;
    }

    @Override
    public T8FunctionalityHandle getFunctionalityHandle(T8Context context, String functionalityId, String dataObjectId, String dataObjectIid) throws Exception
    {
        T8FunctionalityDefinition functionalityDefinition;

        functionalityDefinition = (T8FunctionalityDefinition)definitionManager.getInitializedDefinition(context, null, functionalityId, null);
        if (functionalityDefinition != null)
        {
            return functionalityDefinition.getNewFunctionalityHandle(context);
        }
        else throw new Exception("Functionality Definition not found: " + functionalityId);
    }

    private T8DataObjectStateGraph getStateGraph(String stateGraphId) throws Exception
    {
        T8DataObjectStateGraph stateGraph;

        stateGraph = stateGraphs.get(stateGraphId);
        if (stateGraph == null)
        {
            T8DataObjectStateGraphDefinition stateGraphDefinition;

            stateGraphDefinition = (T8DataObjectStateGraphDefinition)definitionManager.getInitializedDefinition(internalContext, internalContext.getProjectId(), stateGraphId, null);
            if (stateGraphDefinition != null)
            {
                stateGraph = stateGraphDefinition.getNewStateGraphInstance(internalContext);
                stateGraphs.put(stateGraph.getId(), stateGraph);
                return stateGraph;
            }
            else throw new RuntimeException("State Graph not found: " + stateGraphId);
        }
        else return stateGraph;
    }

    private T8FunctionalityDefinition getFunctionalityDefinition(String functionalityId)
    {
        try
        {
            T8FunctionalityDefinition functionalityDefinition;

            functionalityDefinition = functionalityDefinitionCache.get(functionalityId);
            if (functionalityDefinition == null)
            {
                functionalityDefinition = (T8FunctionalityDefinition)definitionManager.getInitializedDefinition(internalContext, internalContext.getProjectId(), functionalityId, null);
                if (functionalityDefinition != null)
                {
                    functionalityDefinitionCache.put(functionalityId, functionalityDefinition);
                }

                return functionalityDefinition;
            }
            else return functionalityDefinition;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getStateConceptId(T8Context context, String stateId) throws Exception
    {
        String stateGraphId;

        stateGraphId = T8IdentifierUtilities.getGlobalIdentifierPart(stateId);
        if (!Strings.isNullOrEmpty(stateGraphId))
        {
            T8DataObjectStateGraph stateGraph;

            stateGraph = getStateGraph(stateGraphId);
            if (stateGraph != null)
            {
                T8DataObjectState state;

                state = stateGraph.getState(stateId);
                if (state != null)
                {
                    return state.getConceptId();
                }
                else throw new RuntimeException("State not found: " + stateId);
            }
            else throw new RuntimeException("State Graph not found: " + stateGraphId);
        }
        else throw new IllegalArgumentException("Input state identifier does not have namespace prefixed: " + stateId);
    }

    private T8FunctionalityGroupHandle getFunctionalityGroupHandle(T8Context context, String functionalityGroupId) throws Exception
    {
        T8FunctionalityGroupDefinition functionalityGroupDefinition;

        functionalityGroupDefinition = (T8FunctionalityGroupDefinition)definitionManager.getInitializedDefinition(context, context.getProjectId(), functionalityGroupId, null);
        if (functionalityGroupDefinition != null)
        {
            return functionalityGroupDefinition.getNewFunctionalityGroupHandle(context, null);
        }
        else throw new RuntimeException("Functionality group not found: " + functionalityGroupId);
    }

    public T8TargetObjectValidationResult validateTargetObject(T8Context context, String functionalityId, T8DataObject targetObject) throws Exception
    {
        T8FunctionalityDefinition functionalityDefinition;
        T8DataObjectValidationScriptDefinition scriptDefinition;

        functionalityDefinition = getFunctionalityDefinition(functionalityId);
        scriptDefinition = functionalityDefinition.getTargetObjectValidationScriptDefinition();

        if (targetObject == null)
        {
            List<String> requiredObjectIds;

            requiredObjectIds = functionalityDefinition.getRequiredObjectIds();
            if ((requiredObjectIds != null) && (!requiredObjectIds.isEmpty()))
            {
                String objectNotFoundMessage;

                objectNotFoundMessage = functionalityDefinition.getTargetObjectNotFoundMessage();
                if (!Strings.isNullOrEmpty(objectNotFoundMessage))
                {
                    return new T8TargetObjectValidationResult(false, objectNotFoundMessage);
                }
                else return new T8TargetObjectValidationResult(false, "Required Target Object not Found.");
            }
            else return new T8TargetObjectValidationResult(true, null);
        }
        else
        {
            if (scriptDefinition != null)
            {
                try
                {
                    Map<String, Object> validationInputParameters;
                    Map<String, Object> validationOutputParameters;
                    String validationErrorMessage;
                    T8Script validationScript;
                    Boolean valid;

                    validationScript = scriptDefinition.getNewScriptInstance(context);

                    validationInputParameters = new HashMap<>();
                    validationInputParameters.put(scriptDefinition.getNamespace() + T8DataObjectValidationScriptDefinition.PARAMETER_DATA_OBJECT, targetObject);

                    validationOutputParameters = validationScript.executeScript(validationInputParameters);
                    valid = (Boolean)validationOutputParameters.get(scriptDefinition.getNamespace() + T8DataObjectValidationScriptDefinition.PARAMETER_VALID);
                    validationErrorMessage = (String)validationOutputParameters.get(scriptDefinition.getNamespace() + T8DataObjectValidationScriptDefinition.PARAMETER_VALIDATION_ERROR_MESSAGE);
                    if ((valid != null) && (!valid))
                    {
                        return new T8TargetObjectValidationResult(false, validationErrorMessage);
                    }
                    else return new T8TargetObjectValidationResult(true, null);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while executing target object validation script: " + scriptDefinition + " in functionality: " + functionalityDefinition, e);
                    return new T8TargetObjectValidationResult(false, "Unexpected Error Prevent Functionality Invocation.");
                }
            }
            else return new T8TargetObjectValidationResult(true, null);
        }
    }

    private void releaseOperationFunctionalities(String operationIid) throws Exception
    {
        synchronized (blockingManager)
        {
            List<String> functionalityIidList;

            functionalityIidList = functionalityStateHandler.getOperationFunctionalityIidList(operationIid);
            for (String functionalityIid : functionalityIidList)
            {
                releaseFunctionality(internalContext, functionalityIid);
            }
        }
    }

    private void releaseSessionFunctionalities(String sessionId) throws Exception
    {
        synchronized (blockingManager)
        {
            List<String> functionalityIidList;

            functionalityIidList = functionalityStateHandler.getSessionFunctionalityIidList(sessionId);
            for (String functionalityIid : functionalityIidList)
            {
                releaseFunctionality(internalContext, functionalityIid);
            }
        }
    }

    private void completeWorkFlowFunctionality(String flowIid, Map<String, Object> flowOutputParameters)
    {
        List<T8FunctionalityState> functionalityStates;
        T8DataObjectApi objApi;
        T8DataTransaction tx;

        // Get the transaction and object api to handle the functionality completion.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);

        // Retrieve the state of the completed functionalities.
        functionalityStates = functionalityStateHandler.retrieveWorkflowFunctionalityStates(tx, flowIid);
        for (T8FunctionalityState state : functionalityStates)
        {
            T8WorkflowFunctionalityDefinition functionalityDefinition;
            Map<String, Object> functionalityOutputParameters;
            String functionalityId;

            // Get the functionality output parameters.
            functionalityId = state.getFunctionalityId();
            functionalityDefinition = (T8WorkflowFunctionalityDefinition)getFunctionalityDefinition(state.getFunctionalityId());
            functionalityOutputParameters = T8IdentifierUtilities.mapParameters(flowOutputParameters, functionalityDefinition.getOutputParameterMapping());
            functionalityOutputParameters = T8IdentifierUtilities.prependNamespace(functionalityId, functionalityOutputParameters);

            // Handle the completion of the functionality.
            for (T8FunctionalityDataObjectState object : state.getDataObjectStates())
            {
                try
                {
                    objApi.handleFunctionalityCompletion(object.getDataObjectId(), object.getDataObjectIid(), functionalityId, functionalityOutputParameters);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while handling objects after completion of workflow: " + flowIid + " functionality: " + state, e);
                }
            }
        }
    }

    private void releaseWorkFlowFunctionalities(String flowIid) throws Exception
    {
        synchronized (blockingManager)
        {
            List<T8FunctionalityState> functionalityStates;
            T8DataTransaction tx;

            // Get the transaction and object api to handle the functionality completion.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

            // Retrieve the state of the completed functionalities.
            functionalityStates = functionalityStateHandler.retrieveWorkflowFunctionalityStates(tx, flowIid);
            for (T8FunctionalityState state : functionalityStates)
            {
                releaseFunctionality(internalContext, state.getFunctionalityIid());
            }
        }
    }

    private class OperationListener implements T8ServerEventListener
    {
        @Override
        public void operationCompleted(T8OperationCompletedEvent event)
        {
            try
            {
                releaseOperationFunctionalities(event.getOperationInstanceIdentifier());
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while releasing functionalities linked to operation: " + event.getOperationInstanceIdentifier(), e);
            }
        }
    }

    private class SessionLogoutListener implements T8SessionEventListener
    {
        private SessionLogoutListener()
        {
        }

        @Override
        public void sessionLoggedOut(T8SessionLogoutEvent event)
        {
            try
            {
                releaseSessionFunctionalities(event.getSessionIdentifier());
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while releasing functionalities linked to session: " + event.getSessionIdentifier(), e);
            }
        }
    }

    private class WorkFlowListener extends T8FlowEventAdapter
    {
        @Override
        public void flowCompleted(T8FlowCompletedEvent event)
        {
            completeWorkFlowFunctionality(event.getFlowStatus().getFlowIid(), event.getOutputParameters());
        }

        @Override
        public void flowFinalized(T8FlowFinalizedEvent event)
        {
            try
            {
                releaseWorkFlowFunctionalities(event.getFlowStatus().getFlowIid());
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while releasing functionalities after finalization of workflow: " + event.getFlowStatus().getFlowIid(), e);
            }
        }
    }
}
