package com.pilog.t8.process;

import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.security.T8Context;
import java.util.Map;

import static com.pilog.t8.definition.process.T8ProcessManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessOperations
{
    public static class GetProcessDetailsOperation extends T8DefaultServerOperation
    {
        public GetProcessDetailsOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;
            String processIid;

            processIid = (String)operationParameters.get(PARAMETER_PROCESS_IID);
            processManager = serverContext.getProcessManager();
            return HashMaps.createSingular(PARAMETER_PROCESS_DETAILS, processManager.getProcessDetails(context, processIid));
        }
    }

    public static class GetProcessSummaryOperation extends T8DefaultServerOperation
    {
        public GetProcessSummaryOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;

            processManager = serverContext.getProcessManager();
            return HashMaps.createSingular(PARAMETER_PROCESS_SUMMARY, processManager.getProcessSummary(context));
        }
    }

    public static class CountProcessesOperation extends T8DefaultServerOperation
    {
        public CountProcessesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Integer> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;
            T8ProcessFilter processFilter;
            String searchExpression;

            processFilter = (T8ProcessFilter)operationParameters.get(PARAMETER_PROCESS_FILTER);
            searchExpression = (String)operationParameters.get(PARAMETER_SEARCH_EXPRESSION);

            processManager = serverContext.getProcessManager();
            return HashMaps.createSingular(PARAMETER_PROCESS_COUNT, processManager.countProcesses(context, processFilter, searchExpression));
        }
    }

    public static class SearchProcessesOperation extends T8DefaultServerOperation
    {
        public SearchProcessesOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;
            T8ProcessFilter processFilter;
            String searchExpression;
            Integer pageOffset;
            Integer pageSize;

            processFilter = (T8ProcessFilter)operationParameters.get(PARAMETER_PROCESS_FILTER);
            searchExpression = (String)operationParameters.get(PARAMETER_SEARCH_EXPRESSION);
            pageOffset = (Integer)operationParameters.get(PARAMETER_PAGE_OFFSET);
            pageSize = (Integer)operationParameters.get(PARAMETER_PAGE_SIZE);

            if (pageOffset == null) pageOffset = 0;
            if (pageSize == null) pageSize = -1;

            processManager = serverContext.getProcessManager();
            return HashMaps.createSingular(PARAMETER_PROCESS_LIST, processManager.searchProcesses(context, processFilter, searchExpression, pageOffset, pageSize));
        }
    }

    public static class StartProcessOperation extends T8DefaultServerOperation
    {
        public StartProcessOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8ProcessDetails> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;
            String processIdentifier;
            T8ProcessDefinition processDefinition;
            Map<String, Object> inputParameters;

            processIdentifier = (String)operationParameters.get(PARAMETER_PROCESS_ID);
            processDefinition = (T8ProcessDefinition)operationParameters.get(PARAMETER_PROCESS_DEFINITION);
            inputParameters = (Map<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETERS);

            processManager = serverContext.getProcessManager();
            if (processDefinition == null) return HashMaps.createSingular(PARAMETER_PROCESS_DETAILS, processManager.startProcess(context, processIdentifier, inputParameters));
            else return HashMaps.createSingular(PARAMETER_PROCESS_DETAILS, processManager.startProcess(context, processDefinition, inputParameters));

        }
    }

    public static class StopProcessOperation extends T8DefaultServerOperation
    {
        public StopProcessOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8ProcessDetails> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;
            String processInstanceIdentifier;

            processInstanceIdentifier = (String)operationParameters.get(PARAMETER_PROCESS_IID);
            processManager = serverContext.getProcessManager();
            return HashMaps.createSingular(PARAMETER_PROCESS_DETAILS, processManager.stopProcess(context, processInstanceIdentifier));
        }
    }

    public static class CancelProcessOperation extends T8DefaultServerOperation
    {
        public CancelProcessOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8ProcessDetails> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;
            String processInstanceIdentifier;

            processInstanceIdentifier = (String)operationParameters.get(PARAMETER_PROCESS_IID);
            processManager = serverContext.getProcessManager();
            return HashMaps.createSingular(PARAMETER_PROCESS_DETAILS, processManager.cancelProcess(context, processInstanceIdentifier));
        }
    }

    public static class PauseProcessOperation extends T8DefaultServerOperation
    {
        public PauseProcessOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8ProcessDetails> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;
            String processInstanceIdentifier;

            processInstanceIdentifier = (String)operationParameters.get(PARAMETER_PROCESS_IID);
            processManager = serverContext.getProcessManager();
            return HashMaps.createSingular(PARAMETER_PROCESS_DETAILS, processManager.pauseProcess(context, processInstanceIdentifier));
        }
    }

    public static class ResumeProcessOperation extends T8DefaultServerOperation
    {
        public ResumeProcessOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8ProcessDetails> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;
            String processInstanceIdentifier;

            processInstanceIdentifier = (String)operationParameters.get(PARAMETER_PROCESS_IID);
            processManager = serverContext.getProcessManager();
            return HashMaps.createSingular(PARAMETER_PROCESS_DETAILS, processManager.resumeProcess(context, processInstanceIdentifier));
        }
    }

    public static class FinalizeProcessOperation extends T8DefaultServerOperation
    {
        public FinalizeProcessOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, T8ProcessDetails> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcessManager processManager;
            String processInstanceIdentifier;

            processInstanceIdentifier = (String)operationParameters.get(PARAMETER_PROCESS_IID);
            processManager = serverContext.getProcessManager();
            return HashMaps.createSingular(PARAMETER_PROCESS_DETAILS, processManager.finalizeProcess(context, processInstanceIdentifier));
        }
    }
}
