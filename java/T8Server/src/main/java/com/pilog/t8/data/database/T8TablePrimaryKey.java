package com.pilog.t8.data.database;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic Object which holds the details for a specific primary key in the database.
 * These details can be used to modify the primary key manually, since this is
 * only a base object and does not do anything more than store the details for
 * further use.
 *
 * @author gavin.boshoff
 */
public class T8TablePrimaryKey
{
    private final String tableName;
    private String primaryKeyIdentifier = null;
    private List<String> primaryKeyColumns = null;

    public T8TablePrimaryKey(String tableName)
    {
        this.tableName = tableName;
        this.primaryKeyColumns = new ArrayList<String>();
    }

    public void setPrimaryKeyIdentifier(String primKeyName)
    {
        this.primaryKeyIdentifier = primKeyName;
    }

    public String getPrimKeyIdentifier()
    {
        return this.primaryKeyIdentifier;
    }

    public boolean hasPrimaryKey()
    {
        return (this.primaryKeyIdentifier != null && !this.primaryKeyColumns.isEmpty());
    }

    public void addPrimaryKeyColumn(String primKeyColumn)
    {
        this.primaryKeyColumns.add(primKeyColumn);
    }

    public String getTableName()
    {
        return this.tableName;
    }

    public List<String> getPrimaryKeyColumns()
    {
        return this.primaryKeyColumns;
    }
}
