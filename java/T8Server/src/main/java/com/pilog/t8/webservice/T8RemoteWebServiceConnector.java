package com.pilog.t8.webservice;

import com.pilog.t8.definition.remote.server.connection.T8DefaultRemoteServerConnectionDefinition;
import com.pilog.t8.definition.remote.server.connection.T8RemoteConnectionUser;
import com.pilog.t8.security.T8Context;

/**
 * @author Gavin Boshoff
 */
public class T8RemoteWebServiceConnector extends T8WebServiceConnector
{
    private final T8DefaultRemoteServerConnectionDefinition definition;

    public T8RemoteWebServiceConnector(T8Context context, T8DefaultRemoteServerConnectionDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    public String getAPIKey() throws Exception
    {
        String apiKey;

        if (this.sessionContext.isSystemSession())
        {
            if (this.definition.getDefaultRemoteUserDefinition() != null)
            {
                apiKey = this.definition.getDefaultRemoteUserDefinition().getApiKey();
            } else throw new Exception("The default system user for the remote connection on the operation " + this.definition.getIdentifier() + " is invalid.");
        }
        else
        {
            try
            {
                T8RemoteConnectionUser remoteConnectionUser;

                remoteConnectionUser = this.serverContext.getSecurityManager().getRemoteConnectionUser(this.context, this.definition.getIdentifier());
                if (remoteConnectionUser != null) apiKey = remoteConnectionUser.getApiKey();
                else throw new Exception("No authentication credentials found for this remote connection.");
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to get the api key for the remote server connection: " + this.definition.getIdentifier(), ex);
            }
        }

        return apiKey;
    }
}
