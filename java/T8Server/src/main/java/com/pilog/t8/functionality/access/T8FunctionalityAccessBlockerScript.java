package com.pilog.t8.functionality.access;

import com.pilog.epic.ParserContext;
import com.pilog.t8.translation.T8TranslatableString;
import com.pilog.t8.definition.functionality.access.T8FunctionalityAccessBlockerScriptDefinition;
import com.pilog.t8.script.T8DefaultServerContextScript;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityAccessBlockerScript extends T8DefaultServerContextScript
{
    private final T8FunctionalityAccessBlockerScriptDefinition definition;

    public T8FunctionalityAccessBlockerScript(T8Context context, T8FunctionalityAccessBlockerScriptDefinition definition)
    {
        super(context, definition);
        this.definition = definition;
    }

    @Override
    protected void addProgramImports(ParserContext parserContext) throws Exception
    {
        // Make sure the default imports are performed.
        super.addProgramImports(parserContext);

        // Method imports.
        parserContext.addMethodImport("createBlock", this, T8FunctionalityAccessBlockerScript.class.getDeclaredMethod("createBlock", String.class, String.class, String.class, String.class, String.class, String.class, String.class, Object[].class));
    }

    public T8FunctionalityBlock createBlock(String orgID, String userID, String profileID, String functionalityIdentifier, String dataObjectIdentifier, String dataObjectKey, String message, Object... messageParameters)
    {
        return new T8FunctionalityBlock(sessionContext.getRootOrganizationIdentifier(), orgID, userID, profileID, functionalityIdentifier, dataObjectIdentifier, dataObjectKey, new T8TranslatableString(message, messageParameters));
    }
}
