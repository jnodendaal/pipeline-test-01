package com.pilog.t8.flow;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.flow.state.T8FlowStateUpdates;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.flow.state.data.T8TaskStateDataSet;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskList;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.flow.task.T8TaskTypeSummary;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.node.activity.T8TaskActivityDefinition;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.flow.task.T8TaskEscalationTrigger;
import com.pilog.t8.flow.task.T8TaskEscalationTrigger.TriggerEvent;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalSettingsDefinition;
import com.pilog.t8.org.operational.T8OrganizationOperationalHoursCalculator;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import java.util.ArrayList;
import java.util.List;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8TaskStateHandler
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8ServerFlowManager controller;
    private final T8DefinitionManager definitionManager;
    private T8DataEntityDefinition taskStateEntityDefinition;
    private T8DataEntityDefinition taskStateInputParameterEntityDefinition;
    private T8DataEntityDefinition taskStateOutputParameterEntityDefinition;

    public T8TaskStateHandler(T8Context context, T8ServerFlowManager controller)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.controller = controller;
        this.definitionManager = serverContext.getDefinitionManager();
        fetchDefinitions();
    }

    private void fetchDefinitions()
    {
        try
        {
            // STATE_FLOW_TASK.
            taskStateEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER, null);
            if (taskStateEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER);

            // STATE_FLOW_TASK_INPUT_PAR.
            taskStateInputParameterEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER, null);
            if (taskStateInputParameterEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER);

            // STATE_FLOW_TASK_OUTPUT_PAR.
            taskStateOutputParameterEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER, null);
            if (taskStateOutputParameterEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching entity definitions required for flow persistence.", e);
        }
    }

    public T8TaskDetails getTaskDetails(T8Context context, String taskIid) throws Exception
    {
        T8FlowTaskState taskState;

        taskState = loadTaskState(taskIid);
        if (taskState != null)
        {
            T8WorkFlowDefinition flowDefinition;
            String projectId;
            String flowId;
            String taskId;
            String nodeId;

            projectId = taskState.getProjectId();
            flowId = taskState.getFlowId();
            flowDefinition = controller.getFlowDefinition(projectId, flowId);
            nodeId = taskState.getNodeId();
            taskId = taskState.getTaskId();
            return taskState.getTaskDetails(context.getLanguageId(), (T8TaskActivityDefinition)flowDefinition.getNodeDefinition(nodeId), controller.getTaskDefinition(projectId, taskId));
        }
        else return null;
    }

    public String findFlowInstanceByTaskInstance(String taskIid) throws Exception
    {
        T8DataTransaction tx;
        T8DataEntity taskEntity;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Retrieve the task entity.
        taskEntity = tx.retrieve(STATE_FLOW_TASK_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_DE_IDENTIFIER + "$TASK_IID", taskIid));
        if (taskEntity != null)
        {
            return (String)taskEntity.getFieldValue(STATE_FLOW_TASK_DE_IDENTIFIER + "$FLOW_IID");
        }
        else return null;
    }

    public T8FlowTaskState loadTaskState(String taskIid) throws Exception
    {
        T8DataTransaction tx;
        T8DataEntity taskEntity;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // STATE_FLOW_TASK.
        taskEntity = tx.retrieve(STATE_FLOW_TASK_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_DE_IDENTIFIER + "$TASK_IID", taskIid));
        if (taskEntity != null)
        {
            T8TaskStateDataSet dataSet;
            T8FlowTaskState taskState;

            // Create a new data set to hold the retrieved data.
            dataSet = new T8TaskStateDataSet(taskEntity);

            // STATE_FLOW_TASK_INPUT_PAR.
            dataSet.addInputParameterData(tx.select(STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER + "$TASK_IID", taskIid)));

            // STATE_FLOW_TASK_OUTPUT_PAR.
            dataSet.addOutputParameterData(tx.select(STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER + "$TASK_IID", taskIid)));

            // Construct the task state.
            taskState = new T8FlowTaskState(null, dataSet);
            taskState.statePersisted();
            return taskState;
        }
        else return null;
    }

    public void saveTaskState(T8FlowTaskState taskState) throws Exception
    {
        synchronized (taskState)
        {
            T8DataSession session;
            T8DataTransaction xtx;
            T8DataTransaction tx;

            // Get the transaction to use (suspend any external transaction).
            session = serverContext.getDataManager().getCurrentSession();
            xtx = session.suspend();
            tx = session.beginTransaction();

            // Save the flow state.
            try
            {
                T8FlowStateUpdates updates;

                // Create an object to store all updates.
                updates = new T8FlowStateUpdates();
                updates.setTaskStateEntityDefinition(taskStateEntityDefinition);
                updates.setTaskStateInputParameterEntityDefinition(taskStateInputParameterEntityDefinition);
                updates.setTaskStateOutputParameterEntityDefinition(taskStateOutputParameterEntityDefinition);

                // Get all flow state updates.
                taskState.addUpdates(updates);

                // Persist inserts.
                for (T8DataEntity entity : updates.getInserts())
                {
                    tx.insert(entity);
                }

                // Persist updates.
                for (T8DataEntity entity : updates.getUpdates())
                {
                    tx.update(entity);
                }

                // Persist deletions.
                for (T8DataEntity entity : updates.getDeletions())
                {
                    tx.delete(entity);
                }

                // Commit the transaction.
                tx.commit();

                // Now that all updates have been saved, reset the state flags.
                taskState.statePersisted();
            }
            catch (Exception e)
            {
                tx.rollback();
                throw e;
            }
            finally
            {
                // If an external session was present, resume it.
                if (xtx != null) session.resume(xtx);
            }
        }
    }

    public T8FlowTaskState cancelTask(String taskIid)
    {
        // Nothing to do here yet.
        return null;
    }

    public void issueTask(String taskIid)
    {
        // Nothing to do here yet.
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public T8TaskList getTaskList(T8Context context, T8TaskFilter taskFilter) throws Exception
    {
        if (CollectionUtilities.hasContent(context.getSessionContext().getWorkFlowProfileIdentifiers()))
        {
            List<T8DataEntity> entityList;
            T8DataSession dataSession;
            T8DataTransaction tx;
            String entityId;
            T8TaskList taskList;

            // Get the values for the search criteria.
            if (taskFilter == null)
            {
                //If the filter is null, we assume there is a need to retrieve the
                //entire task summary
                taskFilter = new T8TaskFilter();
                taskFilter.setIncludeClaimedTasks(true);
                taskFilter.setIncludeUnclaimedTasks(true);
            }

            // Retrieve the entity. We only want to use the join if we need to.
            entityId = T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER;
            dataSession = serverContext.getDataManager().getCurrentSession();
            tx = dataSession.instantTransaction();

            // Select the first entities that matches the filter criteria.
            taskList = new T8TaskList();
            entityList = tx.select(entityId, taskFilter.getDataFilter(context, entityId), taskFilter.getPageOffset(), taskFilter.getPageSize());
            for (T8DataEntity taskEntity : entityList)
            {
                String taskIid;

                taskIid = (String)taskEntity.getFieldValue(entityId + "$TASK_IID");
                taskList.addTaskDetails(getTaskDetails(context, taskIid));
            }

            // Return the complete task list.
            return taskList;
        }
        else return new T8TaskList();
    }

    public int getTaskCount(T8Context context, T8TaskFilter taskFilter) throws Exception
    {
        T8DataSession dataSession;
        T8DataTransaction tx;
        String entityId;

        // Make sure we have a valid task filter to use.
        if (taskFilter == null)
        {
            // If the filter is null, we assume there is a need to retrieve the entire task summary.
            taskFilter = new T8TaskFilter();
            taskFilter.setUserIdentifier(context.getSessionContext().getUserIdentifier());
            taskFilter.setIncludeClaimedTasks(true);
            taskFilter.setIncludeUnclaimedTasks(true);
        }

        // Retrieve the task count using the specified filter and return the result.
        entityId = T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER;
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.instantTransaction();
        return tx.count(entityId, taskFilter.getDataFilter(context, entityId));
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public T8TaskListSummary getTaskListSummary(T8Context context, T8TaskFilter taskFilter) throws Exception
    {
        String filterEntityId;
        List<T8DataEntity> entityList;
        T8TaskListSummary summary;
        T8DataSession dataSession;
        T8DataTransaction tx;
        String entityIdentifier;

        if (taskFilter == null)
        {
            //If the filter is null, we assume there is a need to retrieve the
            //entire task summary
            taskFilter = new T8TaskFilter();
            taskFilter.setIncludeClaimedTasks(true);
            taskFilter.setIncludeUnclaimedTasks(true);
        }

        // Get the transaction for retrieval
        entityIdentifier = Strings.isNullOrEmpty(taskFilter.getDescriptivePropertySearchString()) ? T8FlowManagerResource.STATE_FLOW_TASK_SUMMARY_DE_IDENTIFIER : T8FlowManagerResource.STATE_FLOW_TASK_DETAIL_SUMMARY_DE_IDENTIFIER;
        filterEntityId = T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER;
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.instantTransaction();

        // Get the results from the database
        entityList = tx.select(entityIdentifier, taskFilter.getDataFilter(context, filterEntityId));

        // Process the results for the summary
        summary = new T8TaskListSummary();
        for (T8DataEntity taskSummaryEntity : entityList)
        {
            T8TaskTypeSummary taskTypeSummary;
            int taskCount;

            // Get the task type summary, creating it if it doesn't exist
            taskTypeSummary = getTaskTypeSummary(summary, taskSummaryEntity, entityIdentifier);

            // Increment the total count for the task type.
            taskCount = (Integer)taskSummaryEntity.getFieldValue(entityIdentifier+F_TASK_COUNT);
            taskTypeSummary.setTotalCount(taskTypeSummary.getTotalCount() + taskCount);

            // If the task is claimed increment the claimed count for the task type.
            if (taskSummaryEntity.getFieldValue(entityIdentifier+F_CLAIMED_BY_USER_ID) != null)
            {
                taskTypeSummary.setClaimedCount(taskTypeSummary.getClaimedCount() + taskCount);
            }
        }

        return summary;
    }

    private T8TaskTypeSummary getTaskTypeSummary(T8TaskListSummary summary, T8DataEntity taskSummaryEntity, String entityId)
    {
        T8TaskTypeSummary taskTypeSummary;
        String projectId;
        String taskId;

        projectId = (String)taskSummaryEntity.getFieldValue(entityId + F_PROJECT_ID);
        taskId = (String)taskSummaryEntity.getFieldValue(entityId + F_TASK_ID);
        taskTypeSummary = summary.getTaskTypeSummary(taskId);
        if (taskTypeSummary == null)
        {
            T8FlowTaskDefinition taskDefinition;

            taskDefinition = controller.getTaskDefinition(projectId, taskId);
            if (taskDefinition != null)
            {
                taskTypeSummary = new T8TaskTypeSummary(taskId);
                taskTypeSummary.setDisplayName(taskDefinition.getName());
                taskTypeSummary.setDescription(taskDefinition.getDescription());

                summary.putTaskTypeSummary(taskTypeSummary);
            }
            else throw new RuntimeException("Task Definition not found: " + taskId);
        }

        return taskTypeSummary;
    }

    public List<T8TaskEscalationTrigger> retrieveTaskEscalationTriggers() throws Exception
    {
        List<T8TaskEscalationTrigger> triggers;
        List<T8DataEntity> triggerEntities;
        T8DataFilter triggerOrderFilter;
        T8DataTransaction tx;

        // Get the transaction to use.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // It is extremely important to ensure that trigger are ordered from highest to lowest TIME_ELAPSED values, to allow proper escalation sequence.
        triggerOrderFilter = new T8DataFilter(FLOW_TASK_ESCALATION_DE_IDENTIFIER);
        triggerOrderFilter.addFieldOrdering(FLOW_TASK_ESCALATION_DE_IDENTIFIER + F_TIME_ELAPSED, T8DataFilter.OrderMethod.DESCENDING);

        // Retrieve the task entity.
        triggers = new ArrayList<>();
        triggerEntities = tx.select(FLOW_TASK_ESCALATION_DE_IDENTIFIER, triggerOrderFilter);
        for (T8DataEntity triggerEntity : triggerEntities)
        {
            T8TaskEscalationTrigger trigger;

            trigger = new T8TaskEscalationTrigger((String)triggerEntity.getFieldValue(F_TRIGGER_IID));
            trigger.setFlowId((String)triggerEntity.getFieldValue(F_FLOW_ID));
            trigger.setTaskId((String)triggerEntity.getFieldValue(F_TASK_ID));
            trigger.setGroupId((String)triggerEntity.getFieldValue(F_GROUP_ID));
            trigger.setEvent(TriggerEvent.valueOf((String)triggerEntity.getFieldValue(F_EVENT)));
            trigger.setTimeElapsed((Integer)triggerEntity.getFieldValue(F_TIME_ELAPSED));
            trigger.setRecurring("Y".equalsIgnoreCase((String)triggerEntity.getFieldValue(F_RECURRING)));
            trigger.setPriority((Integer)triggerEntity.getFieldValue(F_PRIORITY));
            trigger.setPriorityIncrease((Integer)triggerEntity.getFieldValue(F_PRIORITY_INCREASE));
            trigger.setEscalationId((String)triggerEntity.getFieldValue(F_ESCALATION_ID));
            triggers.add(trigger);
        }

        // Return the final list of retrieved triggers.
        return triggers;
    }

    public List<String> retrieveEscalationTaskIids(T8TaskEscalationTrigger trigger, int pageSize) throws Exception
    {
        T8OperationalHoursCalculator hourCalculator;
        long currentMillis;

        // Get the current millis.
        currentMillis = System.currentTimeMillis();

        // Get an operational hours calculator to use for business hour calculations.  If the current time falls outside operational hours, no escalation required.
        hourCalculator = getOperationalHoursCalculator();
        if ((hourCalculator == null) || (hourCalculator.isOperationalInstant(currentMillis)))
        {
            List<String> escalationTaskIids;
            List<T8DataEntity> taskEntities;
            T8DataFilter triggerFilter;
            TriggerEvent event;
            Integer timeElapsed;
            boolean recurring;
            String flowId;
            String taskId;
            String groupId;
            T8DataTransaction tx;

            // Get the transaction to use.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

            // Get the trigger details.
            flowId = trigger.getFlowId();
            taskId = trigger.getTaskId();
            groupId = trigger.getGroupId();
            event = trigger.getEvent();
            timeElapsed = trigger.getTimeElapsed();
            recurring = trigger.isRecurring();

            // Create a list to hold all iids of tasks to be escalated.
            escalationTaskIids = new ArrayList<>();

            // Add the event time filters.
            if (event == TriggerEvent.GROUP_DURATION)
            {
                T8Timestamp filterTimestamp;
                long filterMillis;

                // Calculate the current time minus the specified elapsed period to determine the cut-off time for this trigger.
                filterMillis = currentMillis - (timeElapsed * 60 * 1000); // Business hours are not used here as the calculation is more complex than the other cases.

                // Add the flow and task id's if specified.
                triggerFilter = new T8DataFilter(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER);
                triggerFilter.addFilterCriterion(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_FLOW_ID, flowId);
                triggerFilter.addFilterCriterion(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_GROUP_ID, groupId);

                // Add a filter to exclude all tasks already completed.
                triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_TIME_COMPLETED, DataFilterOperator.IS_NULL, null, false);

                // Create filter criteria where the adjusted issue time (original issued time minus time already elapsed for the group) occurred prior to the current time minus the specified elapsed period.
                filterTimestamp = new T8Timestamp(filterMillis);
                triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_ADJUSTED_TIME_ISSUED, DataFilterOperator.IS_NOT_NULL, null, false);
                triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_ADJUSTED_TIME_ISSUED, DataFilterOperator.LESS_THAN, filterTimestamp, false);

                // Add the recurring checks.
                if (recurring)
                {
                    T8DataFilterCriteria recurringCriteria;

                    // Add filter criteria to include only records where the last repioritization time is null or less than the filter time.
                    recurringCriteria = new T8DataFilterCriteria();
                    recurringCriteria.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_ADJUSTED_TIME_ESCALATED, DataFilterOperator.LESS_THAN, filterTimestamp, false);
                    recurringCriteria.addFilterCriterion(DataFilterConjunction.OR, STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_ADJUSTED_TIME_ESCALATED, DataFilterOperator.IS_NULL, null, false);
                    triggerFilter.addFilterCriteria(DataFilterConjunction.AND, recurringCriteria);

                    // Add ordering on the time, so that we continue to work from the oldest entries to the latest.
                    triggerFilter.addFieldOrdering(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_TIME_ESCALATED, T8DataFilter.OrderMethod.ASCENDING);
                }
                else
                {
                    T8DataFilterCriteria escalationCriteria;

                    // Add a filter criterion to exclude all entries that have been reprioritized already (not a recurring trigger).
                    escalationCriteria = new T8DataFilterCriteria();
                    escalationCriteria.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_ESCALATION_LEVEL, DataFilterOperator.IS_NULL, null, false);
                    escalationCriteria.addFilterCriterion(DataFilterConjunction.OR, STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_ESCALATION_LEVEL, DataFilterOperator.LESS_THAN, timeElapsed, false);
                    triggerFilter.addFilterCriteria(DataFilterConjunction.AND, escalationCriteria);

                    // Add ordering on the time, so that we continue to work from the oldest entries to the latest.
                    triggerFilter.addFieldOrdering(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_TIME_ISSUED, T8DataFilter.OrderMethod.ASCENDING);
                }
            }
            else if ((event == TriggerEvent.ISSUED) || (event == TriggerEvent.ISSUED_OR_CLAIMED))
            {
                T8Timestamp filterTimestamp;
                long filterMillis;

                // Calculate the current time minus the specified elapsed period to determine the cut-off time for this trigger.
                if (hourCalculator != null) filterMillis = hourCalculator.calculateOperationalStartTime(currentMillis, (timeElapsed * 60 * 1000));
                else filterMillis = currentMillis - (timeElapsed * 60 * 1000);

                // Add the flow and task id's if specified.
                triggerFilter = new T8DataFilter(STATE_FLOW_TASK_DE_IDENTIFIER);
                if (flowId != null) triggerFilter.addFilterCriterion(STATE_FLOW_TASK_DE_IDENTIFIER + F_FLOW_ID, flowId);
                if (taskId != null) triggerFilter.addFilterCriterion(STATE_FLOW_TASK_DE_IDENTIFIER + F_TASK_ID, taskId);

                // Add a filter to exclude all tasks already completed.
                triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_COMPLETED, DataFilterOperator.IS_NULL, null, false);

                // Create filter criteria where the issued time occurred prior to the current time minus the specified elapsed period and the claimed time is null.
                filterTimestamp = new T8Timestamp(filterMillis);
                triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ISSUED, DataFilterOperator.IS_NOT_NULL, null, false);
                triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ISSUED, DataFilterOperator.LESS_THAN, filterTimestamp, false);
                if (event == TriggerEvent.ISSUED) triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_CLAIMED, DataFilterOperator.IS_NULL, null, false);

                // Add the recurring checks.
                if (recurring)
                {
                    T8DataFilterCriteria recurringCriteria;

                    // Add filter criteria to include only records where the last escalation time is null or less than the filter time.
                    recurringCriteria = new T8DataFilterCriteria();
                    recurringCriteria.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ESCALATED, DataFilterOperator.LESS_THAN, filterTimestamp, false);
                    recurringCriteria.addFilterCriterion(DataFilterConjunction.OR, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ESCALATED, DataFilterOperator.IS_NULL, null, false);
                    triggerFilter.addFilterCriteria(DataFilterConjunction.AND, recurringCriteria);

                    // Add ordering on the time, so that we continue to work from the oldest entries to the latest.
                    triggerFilter.addFieldOrdering(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ESCALATED, T8DataFilter.OrderMethod.ASCENDING);
                }
                else
                {
                    T8DataFilterCriteria escalationCriteria;

                    // Add a filter criterion to exclude all entries that have been escalation already (not a recurring trigger).
                    escalationCriteria = new T8DataFilterCriteria();
                    escalationCriteria.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_ESCALATION_LEVEL, DataFilterOperator.IS_NULL, null, false);
                    escalationCriteria.addFilterCriterion(DataFilterConjunction.OR, STATE_FLOW_TASK_DE_IDENTIFIER + F_ESCALATION_LEVEL, DataFilterOperator.LESS_THAN, timeElapsed, false);
                    triggerFilter.addFilterCriteria(DataFilterConjunction.AND, escalationCriteria);

                    // Add ordering on the time, so that we continue to work from the oldest entries to the latest.
                    triggerFilter.addFieldOrdering(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ISSUED, T8DataFilter.OrderMethod.ASCENDING);
                }
            }
            else if (event == TriggerEvent.CLAIMED)
            {
                T8Timestamp filterTimestamp;
                long filterMillis;

                // Calculate the current time minus the specified elapsed period to determine the cut-off time for this trigger.
                if (hourCalculator != null) filterMillis = hourCalculator.calculateOperationalStartTime(currentMillis, (timeElapsed * 60 * 1000));
                else filterMillis = currentMillis - (timeElapsed * 60 * 1000);

                // Add the flow and task id's if specified.
                triggerFilter = new T8DataFilter(STATE_FLOW_TASK_DE_IDENTIFIER);
                if (flowId != null) triggerFilter.addFilterCriterion(STATE_FLOW_TASK_DE_IDENTIFIER + F_FLOW_ID, flowId);
                if (taskId != null) triggerFilter.addFilterCriterion(STATE_FLOW_TASK_DE_IDENTIFIER + F_TASK_ID, taskId);

                // Add a filter to exclude all tasks already completed.
                triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_COMPLETED, DataFilterOperator.IS_NULL, null, false);

                // Create filter criteria where the claimed time occurred prior to the current time minus the specified elapsed period.
                filterTimestamp = new T8Timestamp(filterMillis);
                triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_CLAIMED, DataFilterOperator.IS_NOT_NULL, null, false);
                triggerFilter.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_CLAIMED, DataFilterOperator.LESS_THAN, filterTimestamp, false);

                // Add the recurring checks.
                if (recurring)
                {
                    T8DataFilterCriteria recurringCriteria;

                    // Add filter criteria to include only records where the last repioritization time is null or less than the filter time.
                    recurringCriteria = new T8DataFilterCriteria();
                    recurringCriteria.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ESCALATED, DataFilterOperator.LESS_THAN, filterTimestamp, false);
                    recurringCriteria.addFilterCriterion(DataFilterConjunction.OR, STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ESCALATED, DataFilterOperator.IS_NULL, null, false);
                    triggerFilter.addFilterCriteria(DataFilterConjunction.AND, recurringCriteria);

                    // Add ordering on the time, so that we continue to work from the oldest entries to the latest.
                    triggerFilter.addFieldOrdering(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_ESCALATED, T8DataFilter.OrderMethod.ASCENDING);
                }
                else
                {
                    T8DataFilterCriteria escalationCriteria;

                    // Add a filter criterion to exclude all entries that have been reprioritized already (not a recurring trigger).
                    escalationCriteria = new T8DataFilterCriteria();
                    escalationCriteria.addFilterCriterion(DataFilterConjunction.AND, STATE_FLOW_TASK_DE_IDENTIFIER + F_ESCALATION_LEVEL, DataFilterOperator.IS_NULL, null, false);
                    escalationCriteria.addFilterCriterion(DataFilterConjunction.OR, STATE_FLOW_TASK_DE_IDENTIFIER + F_ESCALATION_LEVEL, DataFilterOperator.LESS_THAN, timeElapsed, false);
                    triggerFilter.addFilterCriteria(DataFilterConjunction.AND, escalationCriteria);

                    // Add ordering on the time, so that we continue to work from the oldest entries to the latest.
                    triggerFilter.addFieldOrdering(STATE_FLOW_TASK_DE_IDENTIFIER + F_TIME_CLAIMED, T8DataFilter.OrderMethod.ASCENDING);
                }
            }
            else throw new Exception("Insupported trigger event: " + trigger.getEvent());

            // Now use the constructed filter to retrieve the tasks to be escalated based on the trigger type.
            if (event == TriggerEvent.GROUP_DURATION)
            {
                // Select the entities.
                taskEntities = tx.select(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER, triggerFilter, 0, pageSize);
                for (T8DataEntity taskEntity : taskEntities)
                {
                    T8Timestamp timeIssued;
                    Long groupTimespan;
                    String taskIid;

                    // Get the details of the task.
                    taskIid = (String)taskEntity.getFieldValue(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_TASK_IID);
                    groupTimespan = (Long)taskEntity.getFieldValue(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_GROUP_TIME_SPAN);
                    timeIssued = (T8Timestamp)taskEntity.getFieldValue(STATE_FLOW_TASK_ESCALATION_GROUP_DE_IDENTIFIER + F_TIME_ISSUED);

                    // Because the retrieval of this task is based on actual time and does not consider business hours, perform a check first to make sure that the task must be escalated.
                    if (isTaskDueForEscalation(hourCalculator, groupTimespan, timeIssued.getMilliseconds(), currentMillis, (timeElapsed * 60L * 1000L)))
                    {
                        escalationTaskIids.add(taskIid);
                    }
                }

                // Return the final map of task priorties.
                return escalationTaskIids;
            }
            else // TriggerEvent.ISSUED) || (event == TriggerEvent.CLAIMED) || (event == TriggerEvent.ISSUED_OR_CLAIMED)
            {
                // Select the entities.
                taskEntities = tx.select(STATE_FLOW_TASK_DE_IDENTIFIER, triggerFilter, 0, pageSize);
                for (T8DataEntity taskEntity : taskEntities)
                {
                    String taskIid;

                    // Get the details of the task.
                    taskIid = (String)taskEntity.getFieldValue(STATE_FLOW_TASK_DE_IDENTIFIER + F_TASK_IID);
                    escalationTaskIids.add(taskIid);
                }

                // Return the final map of task priorties.
                return escalationTaskIids;
            }
        }
        else return new ArrayList<>(); // Current time is not in operational hours, so no escalation applicable.
    }

    private boolean isTaskDueForEscalation(T8OperationalHoursCalculator calculator, Long groupTimespan, Long uncompletedIssuedTime, Long currentTime, Long elapsedTimeTrigger)
    {
        if (calculator != null)
        {
            Long totalGroupTimespan;
            Long uncompletedTimespan;

            // Calculate the business hour time that has elapsed for the currently uncompleted task.
            uncompletedTimespan = calculator.calculateOperationalTimeDifference(uncompletedIssuedTime, currentTime);

            // Calculate the total number of business hour time elapsed for the group.
            totalGroupTimespan = groupTimespan + uncompletedTimespan;

            // If the total group time span is longer than the trigger time span, return true.
            return totalGroupTimespan >= elapsedTimeTrigger;
        }
        else // Use normal time.
        {
            // If the time span of the currently uncompleted task plus the time span of the completed tasks in the group is longer than the trigger time, return true.
            return (currentTime - uncompletedIssuedTime + groupTimespan) >= elapsedTimeTrigger;
        }
    }

    private T8OperationalHoursCalculator getOperationalHoursCalculator() throws Exception
    {
        T8OrganizationSetupDefinition setupDefinition;
        String operationalSettingsId;

        setupDefinition = T8OrganizationSetupDefinition.getOrganizationSetupDefinition(internalContext);
        operationalSettingsId = setupDefinition.getOrganizationOperationalSettingsIdentifier();
        if (operationalSettingsId != null)
        {
            T8OrganizationOperationalSettingsDefinition settingsDefinition;

            settingsDefinition = definitionManager.getInitializedDefinition(internalContext, null, operationalSettingsId, null);
            if (settingsDefinition != null)
            {
                return T8OrganizationOperationalHoursCalculator.getInstance(settingsDefinition);
            }
            else throw new RuntimeException("Operational settings definition not found: " + operationalSettingsId);
        }
        else return null;
    }
}
