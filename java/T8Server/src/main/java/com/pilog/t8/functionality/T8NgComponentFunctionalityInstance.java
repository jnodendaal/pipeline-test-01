package com.pilog.t8.functionality;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.functionality.T8NgComponentFunctionalityDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.ng.T8NgComponentDefinition;
import com.pilog.t8.definition.ng.T8NgComponentInitializationScriptDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8NgComponentFunctionalityInstance extends T8DefaultFunctionalityInstance implements T8FunctionalityInstance
{
    private final T8NgComponentFunctionalityDefinition definition;
    private final String componentId;
    private Map<String, Object> inputParameters;

    public T8NgComponentFunctionalityInstance(T8Context context, T8NgComponentFunctionalityDefinition definition)
    {
        super(context, definition, T8IdentifierUtilities.createNewGUID());
        this.definition = definition;
        this.componentId = definition.getComponentId();
    }

    public T8NgComponentFunctionalityInstance(T8Context context, T8NgComponentFunctionalityDefinition definition, T8FunctionalityState state)
    {
        super(context, definition, state.getFunctionalityIid());
        this.definition = definition;
        this.componentId = definition.getComponentId();
    }

    @Override
    public String getFunctionalityId()
    {
        return definition.getIdentifier();
    }

    @Override
    public String getFunctionalityIid()
    {
        return iid;
    }

    @Override
    public T8FunctionalityDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void initialize()
    {
    }

    @Override
    public boolean release()
    {
        return true;
    }

    @Override
    public T8FunctionalityAccessHandle access(Map<String, Object> parameters) throws Exception
    {
        T8NgComponentFunctionalityAccessHandle accessHandle;
        T8NgComponentDefinition componentDefinition;
        Map<String, Object> scriptOutputParameters;
        Map<String, Object> componentParameters;
        Map<String, Object> componentInputParameters;

        // Store the input parameters.
        this.timeAccessed = new T8Timestamp(System.currentTimeMillis());
        this.inputParameters = parameters;

        // Get the component definition.
        componentDefinition = (T8NgComponentDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), componentId, null);

        // Get the input parameters.
        componentInputParameters = getComponentInputParameters();

        // Get the component parameters from the script output.
        scriptOutputParameters = componentDefinition.executeComponentInitializationScript(context, componentInputParameters);
        componentParameters = (Map<String, Object>)scriptOutputParameters.get(T8NgComponentInitializationScriptDefinition.PARAMTER_COMPONENT_PARAMETERS);

        // Return the execution handle.
        accessHandle = new T8NgComponentFunctionalityAccessHandle(getFunctionalityId(), iid, componentDefinition, componentParameters, getDisplayName(), getDescription());
        accessHandle.setIconUri(definition.getIconUri());
        return accessHandle;
    }

    @Override
    public boolean stop()
    {
        return true;
    }

    @Override
    public T8FunctionalityState getFunctionalityState()
    {
        T8FunctionalityState state;

        // Create the new state.
        state = super.getFunctionalityState(inputParameters);

        // Add the session id so that this functionality can be removed when the session expires.
        state.setSessionId(sessionContext.getSessionIdentifier());

        // Return the state.
        return state;
    }

    private Map<String, Object> getComponentInputParameters() throws Exception
    {
        Map<String, Object> componentInputParameters;

        componentInputParameters = T8IdentifierUtilities.mapParameters(inputParameters, definition.getInputParameterMapping());
        componentInputParameters.putAll(evaluateComponentParameterExpressions());
        return componentInputParameters;
    }

    private Map<String, Object> evaluateComponentParameterExpressions() throws Exception
    {
        Map<String, String> expressionMap;
        Map<String, Object> resultMap;

        resultMap = new HashMap<String, Object>();
        expressionMap = definition.getInputParameterExpressionMap();
        if ((expressionMap != null) && (expressionMap.size() > 0))
        {
            HashMap<String, Object> expressionParameters;

            // Create a map of expression parameters.
            expressionParameters = new HashMap<String, Object>();

            // Construct a new expression evaluator to use.
            for (String parameterId : expressionMap.keySet())
            {
                String expression;
                Object result;

                expression = expressionMap.get(parameterId);
                result = expressionEvaluator.evaluateExpression(expression, expressionParameters, null);
                resultMap.put(parameterId, result);
            }
        }

        return resultMap;
    }
}
