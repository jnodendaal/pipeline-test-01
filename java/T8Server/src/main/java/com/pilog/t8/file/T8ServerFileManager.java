package com.pilog.t8.file;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.commons.io.RandomAccessStream;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Day;
import com.pilog.t8.time.T8Hour;
import com.pilog.t8.time.T8Millisecond;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.time.T8TimeUnit;
import com.pilog.t8.definition.file.T8FileContextDefinition;
import com.pilog.t8.file.context.T8TemporaryFileContext;
import com.pilog.t8.file.handler.T8ExcelInputFileHandler;
import com.pilog.t8.file.handler.T8ExcelOutputFileHandler;
import com.pilog.t8.file.handler.T8TabularTextInputFileHandler;
import com.pilog.t8.file.handler.T8TabularTextOutputFileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.strings.StringUtilities;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Bouwer du Preez
 */
public class T8ServerFileManager implements T8FileManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerFileManager.class);

    private final T8ServerContext serverContext;
    private final File rootDirectory;
    private final Map<String, T8FileContextHandle> fileContexts; // Key: Context Instance ID.
    private ScheduledExecutorService executor; // Timer used to run maintenance jobs.
    private FileMaintenanceThread maintenanceThread;

    private final long MAINTENANCE_INTERVAL = new T8Minute(1).getMilliseconds(); // The interval in milliseconds at which the maintenance task is run.
    private final long DEFAULT_CONTEXT_EXPIRATION_TIME = new T8Hour(1).getMilliseconds(); // The number of miliseconds for which a file context will be maintained after the last recorded activity.
    private final long MAXIMUM_CONTEXT_EXPIRATION_TIME = new T8Day(1).getMilliseconds(); // The number of miliseconds for which a file context will be maintained after the last recorded activity.

    public T8ServerFileManager(T8ServerContext serverContext, File rootDirectory)
    {
        this.serverContext = serverContext;
        this.rootDirectory = rootDirectory;
        this.fileContexts = Collections.synchronizedMap(new HashMap<String, T8FileContextHandle>());
    }

    @Override
    public void init()
    {
        // Start the maintenance timer.
        executor = Executors.newSingleThreadScheduledExecutor(new NameableThreadFactory("T8ServerFileManager-Maintenance"));
        maintenanceThread = new FileMaintenanceThread(this, fileContexts);
        executor.scheduleWithFixedDelay(maintenanceThread, MAINTENANCE_INTERVAL, MAINTENANCE_INTERVAL, TimeUnit.MILLISECONDS);
    }

    @Override
    public void start() throws Exception
    {
        // No start-up routine for this module.
    }

    @Override
    public void destroy()
    {
        Set<String> openContexts;

        // Terminate the maintenance thread and all scheduled tasks.
        try
        {
            executor.shutdown();
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) throw new Exception("File Manager maintenance thread executor could not by terminated.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while awaiting termination of file manager maintenance thread.", e);
        }

        // Destroy maintenance thread.
        maintenanceThread.destroy();
        maintenanceThread = null;

        // Delete all open file contexts.
        openContexts = new HashSet<String>(fileContexts.keySet());
        for (String contextIid : openContexts)
        {
            try
            {
                closeFileContext(null, contextIid);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while deleting file context during file manager shutdown: " + contextIid, e);
            }
        }

        // Clear the open context collection.
        fileContexts.clear();
    }

    private T8FileContext getFileContext(String contextIid, boolean updateLastActivityTime) throws Exception
    {
        T8FileContextHandle fileContextHandle;

        fileContextHandle = fileContexts.get(contextIid);
        if (fileContextHandle != null)
        {
            if (updateLastActivityTime) fileContextHandle.setLastActivityTime(System.currentTimeMillis());
            return fileContextHandle.getFileContext();
        }
        else throw new Exception("File context not found: " + contextIid);
    }

    @Override
    public T8FileHandler getFileHandler(T8Context context, String fileTypeId, String contextIid, String filePath)
    {
        // The intention is for this section to become reflective so that new file handler types can be added dynically.
        switch (fileTypeId)
        {
            case T8ExcelInputFileHandler.FILE_HANDLER_ID:
                return new T8ExcelInputFileHandler(context, this, contextIid, filePath);
            case T8ExcelOutputFileHandler.FILE_HANDLER_ID:
                return new T8ExcelOutputFileHandler(context, this, contextIid, filePath);
            case T8TabularTextInputFileHandler.FILE_HANDLER_ID:
                return new T8TabularTextInputFileHandler(context, this, contextIid, filePath);
            case T8TabularTextOutputFileHandler.FILE_HANDLER_ID:
                return new T8TabularTextOutputFileHandler(context, this, contextIid, filePath);
            default:
                throw new IllegalArgumentException("No applicable file handler found for file type id: " + fileTypeId);
        }
    }

    @Override
    public File getFile(T8Context context, String contextIid, String filePath) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.getFile(filePath);
    }

    @Override
    public boolean fileExists(T8Context context, String contextIid, String filePath) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.fileExists(filePath);
    }

    @Override
    public boolean fileContextExists(T8Context context, String contextIid) throws Exception
    {
        return fileContexts.containsKey(contextIid);
    }

    @Override
    public InputStream getFileInputStream(T8Context context, String contextIid, String filePath) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.getFileInputStream(filePath);
    }

    @Override
    public OutputStream getFileOutputStream(T8Context context, String contextIid, String filePath, boolean append) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.getFileOutputStream(filePath, append);
    }

    @Override
    public RandomAccessStream getFileRandomAccessStream(T8Context context, String contextIid, String filePath) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.getFileRandomAccessStream(filePath);
    }

    @Override
    public boolean openFileContext(T8Context context, String contextIid, String contextId, T8TimeUnit expirationTime) throws Exception
    {
        if (fileContexts.containsKey(contextIid))
        {
            T8FileContextHandle fileContextHandle;

            fileContextHandle = fileContexts.get(contextIid);
            if (!Objects.equals(fileContextHandle.getFileContext().getId(), contextId))
            {
                throw new Exception("Requested File Context Instance '" + contextIid + "' found but is not of same Context type as requested: " + contextId);
            }
            else return true;
        }
        else
        {
            T8TimeUnit checkedExpirationTime;

            // Make sure the expiration time requested is valid.
            if ((expirationTime != null) && (expirationTime.getMilliseconds() > 0))
            {
                if(expirationTime.getMilliseconds() <= MAXIMUM_CONTEXT_EXPIRATION_TIME)
                {
                    checkedExpirationTime = expirationTime;
                } else throw new Exception("File Context expiration time exceeds maximum limit of " + StringUtilities.buildTimeString(MAXIMUM_CONTEXT_EXPIRATION_TIME, "undeterminable", "hours", "minutes", "seconds"));
            }
            else checkedExpirationTime = new T8Millisecond(DEFAULT_CONTEXT_EXPIRATION_TIME);

            // If not specific context ID is specified, we create a temporary file context.
            if (contextId == null)
            {
                T8FileContext fileContext;

                fileContext = new T8TemporaryFileContext(rootDirectory, contextIid);
                fileContext.open();
                fileContexts.put(contextIid, new T8FileContextHandle(fileContext, checkedExpirationTime));
                return true;
            }
            else
            {
                T8FileContextDefinition fileContextDefinition;

                fileContextDefinition = (T8FileContextDefinition)serverContext.getDefinitionManager().getInitializedDefinition(null, null, contextId, null);
                if (fileContextDefinition != null)
                {
                    T8FileContext fileContext;

                    fileContext = fileContextDefinition.createNewFileContextInstance(context, this, contextIid);
                    fileContext.open();
                    fileContexts.put(contextIid, new T8FileContextHandle(fileContext, checkedExpirationTime));
                    return true;
                }
                else throw new Exception("File Context Definition not found: " + contextId);
            }
        }
    }

    @Override
    public boolean closeFileContext(T8Context context, String contextIid) throws Exception
    {
        T8FileContextHandle fileContextHandle;

        fileContextHandle = fileContexts.remove(contextIid);
        if (fileContextHandle != null)
        {
            return fileContextHandle.getFileContext().close();
        }
        else throw new Exception("File context not found: " + contextIid);
    }

    @Override
    public boolean createDirectory(T8Context context, String contextIid, String filePath) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.createDirectory(filePath);
    }

    @Override
    public void renameFile(T8Context context, String contextIid, String filePath, String newFilename) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        fileContext.renameFile(filePath, newFilename);
    }

    @Override
    public boolean deleteFile(T8Context context, String contextIid, String filePath) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.deleteFile(filePath);
    }

    @Override
    public long getFileSize(T8Context context, String contextIid, String filePath) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.getFileSize(filePath);
    }

    @Override
    public String getMd5Checksum(T8Context context, String contextIid, String path) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.getMD5Checksum(path);
    }

    @Override
    public T8FileDetails getFileDetails(T8Context context, String contextIid, String path) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.getFileDetails(path);
    }

    @Override
    public List<T8FileDetails> getFileList(T8Context context, String contextIid, String directoryPath) throws Exception
    {
        T8FileContext fileContext;

        fileContext = getFileContext(contextIid, true);
        return fileContext.getFileList(directoryPath);
    }

    @Override
    public long uploadFileData(T8Context context, String contextIid, String filePath, byte[] fileData) throws Exception
    {
        OutputStream outputStream = null;
        T8FileContext fileContext;

        // Get the applicable file context.
        fileContext = getFileContext(contextIid, true);

        try
        {
            outputStream = fileContext.getFileOutputStream(filePath, true);
            outputStream.write(fileData);
        }
        finally
        {
            if (outputStream != null) outputStream.close();
        }

        return fileContext.getFileSize(filePath);
    }

    @Override
    public byte[] downloadFileData(T8Context context, String contextIid, String filePath, long offset, int byteSize) throws Exception
    {
        InputStream inputStream = null;
        T8FileContext fileContext;

        // Get the applicable file context.
        fileContext = getFileContext(contextIid, true);

        try
        {
            byte[] buffer;
            int bytesRead;

            inputStream = fileContext.getFileInputStream(filePath);
            buffer = new byte[byteSize];
            inputStream.skip(offset);
            bytesRead = inputStream.read(buffer, 0, byteSize);
            if (bytesRead < byteSize)
            {
                return (bytesRead < 1) ? new byte[0] : Arrays.copyOf(buffer, bytesRead);
            }
            else
            {
                return buffer;
            }
        }
        finally
        {
            if (inputStream != null) inputStream.close();
        }
    }

    @Override
    public T8FileDetails uploadFile(T8Context context, String string, String string1, String string2) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getUploadOperationBytesUploaded(T8Context context, String string) throws RuntimeException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getUploadOperationFileSize(T8Context context, String string) throws RuntimeException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getUploadOperationProgress(T8Context context, String string) throws RuntimeException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long downloadFile(T8Context context, String string, String string1, String string2) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getDownloadOperationBytesDownloaded(T8Context context, String string) throws RuntimeException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getDownloadOperationFileSize(T8Context context, String string) throws RuntimeException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getDownloadOperationProgress(T8Context context, String string) throws RuntimeException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * This is a maintenance routine that is run periodically to do cleanups of
     * expired file context and other maintenance tasks.
     */
    private static class FileMaintenanceThread extends Thread
    {
        private T8FileManager fileManager;
        private Map<String, T8FileContextHandle> contextHandles;

        private FileMaintenanceThread(T8FileManager fileManager, Map<String, T8FileContextHandle> contextHandles)
        {
            this.fileManager = fileManager;
            this.contextHandles = contextHandles;
        }

        @Override
        public void destroy()
        {
            fileManager = null;
            contextHandles = null;
        }

        @Override
        public void run()
        {
            try
            {
                ArrayList<String> expiredContexts;
                List<T8FileContextHandle> handlesToCheck;
                long currentTime;

                currentTime = System.currentTimeMillis();
                expiredContexts = new ArrayList<>();

                // Make a copy of the active collection so that we can iterate through it.
                synchronized (contextHandles)
                {
                    handlesToCheck = new ArrayList<>(contextHandles.values());
                }

                // Check the active data sessions collection and remove any sessions that have been idle for longer than the expiration time.
                for (T8FileContextHandle contextHandle : handlesToCheck)
                {
                    long lastActivityTime;

                    lastActivityTime = contextHandle.getLastActivityTime();
                    if ((currentTime - lastActivityTime) > contextHandle.getExpirationTime().getMilliseconds())
                    {
                        expiredContexts.add(contextHandle.getContextInstanceID());
                    }
                }

                // If there are any expired contexts, delete them.
                if (expiredContexts.size() > 0)
                {
                    for (String contextInstanceIdentifier : expiredContexts)
                    {
                        try
                        {
                            LOGGER.log("Removing expired file context: " + contextInstanceIdentifier);
                            fileManager.closeFileContext(null, contextInstanceIdentifier);
                            contextHandles.remove(contextInstanceIdentifier);
                        }
                        catch (Exception e)
                        {
                            LOGGER.log("Exception while deleting file context '" + contextInstanceIdentifier + "' from maintenance thread: " + contextInstanceIdentifier, e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception executing file manager maintenance thread.", e);
            }
        }
    }
}
