package com.pilog.t8.functionality;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.definition.flow.task.user.T8WorkFlowUserTaskDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.functionality.T8WorkFlowClientFunctionalityDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Timestamp;
import java.util.Map;
import javax.swing.Icon;

/**
 * @author Bouwer du Preez
 */
public class T8WorkFlowClientFunctionalityInstance extends T8DefaultFunctionalityInstance implements T8FunctionalityInstance
{
    private final T8WorkFlowClientFunctionalityDefinition definition;
    private Map<String, Object> inputParameters;

    public T8WorkFlowClientFunctionalityInstance(T8Context context, T8WorkFlowClientFunctionalityDefinition definition)
    {
        super(context, definition, T8IdentifierUtilities.createNewGUID());
        this.definition = definition;
    }

    public T8WorkFlowClientFunctionalityInstance(T8Context context, T8WorkFlowClientFunctionalityDefinition definition, T8FunctionalityState state)
    {
        super(context, definition, state.getFunctionalityIid());
        this.definition = definition;
    }

    @Override
    public T8FunctionalityDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void initialize()
    {
    }

    @Override
    public boolean release()
    {
        return true;
    }

    @Override
    public T8FunctionalityAccessHandle access(Map<String, Object> parameters) throws Exception
    {
        String functionalityId;
        String flowIid;
        String taskIid;
        T8FlowManager flowManager;
        T8DefinitionManager definitionManager;
        T8TaskDetails taskDetails;
        T8WorkFlowUserTaskDefinition taskDefinition;

        // Store the input parameters.
        this.timeAccessed = new T8Timestamp(System.currentTimeMillis());
        this.inputParameters = parameters;

        functionalityId = definition.getIdentifier();
        flowIid = (String)parameters.get(functionalityId + "$P_FLOW_IID");
        taskIid = (String)parameters.get(functionalityId + "$P_TASK_IID");

        definitionManager = serverContext.getDefinitionManager();
        flowManager = serverContext.getFlowManager();
        taskDetails = flowManager.getTaskDetails(context, taskIid);
        taskDefinition = (T8WorkFlowUserTaskDefinition)definitionManager.getInitializedDefinition(context, taskDetails.getProjectId(), taskDetails.getTaskId(), null);
        if (taskDefinition != null)
        {
            String executionDisplayName;
            String executionDescription;
            Icon executionIcon;

            // Return the execution handle.
            executionDisplayName = taskDefinition.getName();
            executionDescription = taskDefinition.getDescription();
            executionIcon = getIcon();
            return new T8WorkFlowClientFunctionalityAccessHandle(getFunctionalityId(), iid, flowIid, taskIid, executionDisplayName, executionDescription, executionIcon);
        }
        else throw new Exception("Task definition " + taskDetails.getTaskId() + " not found in project " + taskDetails.getProjectId());
    }

    @Override
    public boolean stop()
    {
        return true;
    }

    @Override
    public T8FunctionalityState getFunctionalityState()
    {
        T8FunctionalityState state;

        // Create the new state.
        state = super.getFunctionalityState(inputParameters);

        // Add the session identifier so that this functionality can be removed when the session expires.
        state.setSessionId(sessionContext.getSessionIdentifier());

        // Return the state.
        return state;
    }
}
