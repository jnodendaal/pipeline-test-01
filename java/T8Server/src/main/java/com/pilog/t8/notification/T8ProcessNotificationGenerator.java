package com.pilog.t8.notification;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.definition.notification.T8ProcessNotificationDefinition;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.CollectionUtilities;
import com.pilog.t8.utilities.collections.T8RemovePredicate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8ProcessNotificationGenerator implements T8NotificationGenerator
{
    private final T8ProcessNotificationDefinition definition;
    private final T8ServerContext serverContext;

    public T8ProcessNotificationGenerator(T8Context context, T8ProcessNotificationDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.definition = definition;
    }

    @Override
    public List<T8Notification> generateNotifications(T8Context context, Map<String, ? extends Object> inputParameters) throws Exception
    {
        Map<String, Object> notificationParameters;
        List<T8Notification> notifications;
        T8ProcessDetails processDetail;
        List<String> recipientUserList;
        T8Notification notification;
        String processInstanceIdentifier;
        String notificationId;

        // Get the process details.
        processInstanceIdentifier = (String)inputParameters.get(T8ProcessNotificationDefinition.PARAMETER_PROCESS_IID);
        processDetail = this.serverContext.getProcessManager().getProcessDetails(context, processInstanceIdentifier);
        notificationId = this.definition.getIdentifier();

        // Create the process notification parameter map.
        notificationParameters = new HashMap<String, Object>();
        notificationParameters.put(T8ProcessNotificationDefinition.PARAMETER_PROCESS_IID, processInstanceIdentifier);
        notificationParameters.put(T8ProcessNotificationDefinition.PARAMETER_PROCESS_ID, processDetail.getProcessId());
        notificationParameters.put(T8ProcessNotificationDefinition.PARAMETER_PROCESS_DISPLAY_NAME, processDetail.getName());
        notificationParameters.put(T8ProcessNotificationDefinition.PARAMETER_PROCESS_DESCRIPTION, processDetail.getProcessDescription());
        notificationParameters.put(T8ProcessNotificationDefinition.PARAMETER_PROCESS_INITIATOR_ID, processDetail.getInitiatorId());
        notificationParameters.put(T8ProcessNotificationDefinition.PARAMETER_PROCESS_INITIATOR_DISPLAY_NAME, processDetail.getInitiatorName());

        // Create the list of recipient users.
        recipientUserList = getRecipientUserIDList(context, processDetail.getVisibilityRestrictionUserIds(), processDetail.getVisibilityRestrictionProfileIds(), processDetail.getVisibilityRestrictionWorkflowProfileIds());

        // Create a notification for each recipient.
        notifications = new ArrayList<>(recipientUserList.size());
        for (String userIdentifier : recipientUserList)
        {
            notification = new T8Notification(notificationId, definition.getRootProjectId(), T8IdentifierUtilities.createNewGUID());
            notification.setSenderDetail(context.getSessionContext());
            notification.setUserIdentifier(userIdentifier);
            notification.setParameters(notificationParameters);
            notifications.add(notification);
        }

        // Return the list of newly generated notifications.
        return notifications;
    }

    private List<String> getRecipientUserIDList(T8Context context, List<String> userVisibility, List<String> userProfileVisibility, List<String> workflowProfileVisibility) throws Exception
    {
        List<T8UserDefinition> userDefinitions;

        // Fetch all user definitions.  TODO (BDP):  This list of user definitions must be cached in future but refreshing of the cache must be handled by notification manager.
        userDefinitions = serverContext.getDefinitionManager().getRawGroupDefinitions(context, context.getProjectId(), T8UserDefinition.GROUP_IDENTIFIER);

        // If no restrictions have been added to the process visibility, add all users to the list.
        if (CollectionUtilities.isNullOrEmpty(userVisibility) && CollectionUtilities.isNullOrEmpty(userProfileVisibility) && CollectionUtilities.isNullOrEmpty(workflowProfileVisibility))
        {
            return buildUserIdentifierList(userDefinitions);
        }
        else
        {
            userDefinitions.removeIf(new RemoveNonReceiversPredicate(userVisibility, userProfileVisibility, workflowProfileVisibility));
            return buildUserIdentifierList(userDefinitions);
        }
    }

    /**
     * Extracts and returns the {@code String} user identifiers from the list of
     * {@code T8UserDefinition} objects.
     *
     * @param userDefinitions The list of definitions from which the identifiers
     *      should be extracted
     *
     * @return The {@code List} of identifiers from the processed list of
     *      definitions
     */
    private List<String> buildUserIdentifierList(List<T8UserDefinition> userDefinitions)
    {
        List<String> userIdentifiers;

        userIdentifiers = new ArrayList<>(userDefinitions.size());
        for (T8UserDefinition userDefinition : userDefinitions)
        {
            userIdentifiers.add(userDefinition.getIdentifier());
        }

        return userIdentifiers;
    }

    /**
     * Predicate which will be used to determine whether or not a user should
     * receive the notification. The rules are:<br/>
     * 1. If the user identifier is in the set of restricted user ID's, they
     *      have to receive the notification.<br/>
     * 2. If the user is linked to one of the restricted user profile ID's, they
     *      have to receive the notification.<br/>
     * 3. If the user is linked to one of the restricted workflow profile ID's,
     *      they have to receive the notification.<br/>
     * <br/>
     * If a user does not apply to any of these rules, they are removed from the
     * set of users which will receive the notification.
     */
    private class RemoveNonReceiversPredicate extends T8RemovePredicate<T8UserDefinition>
    {
        private final List<String> workflowProfileVisibility;
        private final List<String> userProfileVisibility;
        private final List<String> userVisibility;

        private RemoveNonReceiversPredicate(List<String> userVisibility, List<String> userProfileVisibility, List<String> workflowProfileVisibility)
        {
            this.userVisibility = userVisibility;
            this.userProfileVisibility = userProfileVisibility;
            this.workflowProfileVisibility = workflowProfileVisibility;
        }

        @Override
        public boolean remove(T8UserDefinition userDefinition)
        {
            //If the specific user is in the user visibility list, they should receive the notification
            if (!CollectionUtilities.isNullOrEmpty(this.userVisibility))
            {
                if (this.userVisibility.contains(userDefinition.getIdentifier())) return false;
            }

            //If the user has one of the visible profiles linked, they should receive the notification
            if (!CollectionUtilities.isNullOrEmpty(this.userProfileVisibility))
            {
                if (CollectionUtilities.countIntersection(userDefinition.getProfileIdentifiers(), this.userProfileVisibility) != 0)
                {
                    return false;
                }
            }

            //If the user has one of the visible workflow profiles linked, they should receive the notification
            if (!CollectionUtilities.isNullOrEmpty(this.workflowProfileVisibility))
            {
                if (CollectionUtilities.countIntersection(userDefinition.getWorkFlowProfileIdentifiers(), this.workflowProfileVisibility) != 0)
                {
                    return false;
                }
            }

            //All scenarios where the user should receive the notification should
            //now have been processed, so they are removed and will not receive it
            return true;
        }
    }
}