package com.pilog.t8.file;

import com.pilog.t8.time.T8TimeUnit;

/**
 * @author Bouwer du Preez
 */
public class T8FileContextHandle
{
    private T8FileContext fileContext;
    private long creationTime;
    private long lastActivityTime;
    private T8TimeUnit expirationTime;
    
    public T8FileContextHandle(T8FileContext fileContext, T8TimeUnit expirationTime)
    {
        this.fileContext = fileContext;
        this.expirationTime = expirationTime;
        this.creationTime = System.currentTimeMillis();
        this.lastActivityTime = creationTime; // The last activity time is alwasy defaulted the creation time when the object is constructed.
    }

    public String getContextInstanceID()
    {
        return fileContext.getIid();
    }
    
    public T8FileContext getFileContext()
    {
        return fileContext;
    }
    
    public long getCreationTime()
    {
        return creationTime;
    }

    public void setCreationTime(long creationTime)
    {
        this.creationTime = creationTime;
    }

    public long getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(long lastActivityTime)
    {
        this.lastActivityTime = lastActivityTime;
    }

    public T8TimeUnit getExpirationTime()
    {
        return expirationTime;
    }

    public void setExpirationTime(T8TimeUnit expirationTime)
    {
        this.expirationTime = expirationTime;
    }
}
