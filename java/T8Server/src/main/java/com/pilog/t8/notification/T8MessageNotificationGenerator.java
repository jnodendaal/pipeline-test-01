package com.pilog.t8.notification;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.notification.T8MessageNotificationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8MessageNotificationGenerator implements T8NotificationGenerator
{
    private final T8MessageNotificationDefinition definition;

    public T8MessageNotificationGenerator(T8MessageNotificationDefinition definition)
    {
        this.definition = definition;
    }

    @Override
    public List<T8Notification> generateNotifications(T8Context context, Map<String, ? extends Object> inputParameters) throws Exception
    {
        List<T8Notification> notifications;

        // Create a list to hold all of the newly generated notifications.
        notifications = new ArrayList<>();
        if (inputParameters != null)
        {
            List<String> userIdentifiers;
            String notificationId;
            String projectId;

            // Get the input parameters that will be used to generate the notifications.
            projectId = definition.getRootProjectId();
            notificationId = definition.getIdentifier();
            userIdentifiers = (List<String>)inputParameters.get(T8MessageNotificationDefinition.PARAMETER_USER_IDENTIFIER_LIST);

            // If we have a valid list of recipient users, continue with notification generation.
            if (userIdentifiers != null)
            {
                // Generate a unique notification for each of the recipient users.
                for (String userIdentifier : userIdentifiers)
                {
                    T8Notification notification;

                    // Create the new notification and configure it.
                    notification = new T8Notification(projectId, notificationId, T8IdentifierUtilities.createNewGUID());
                    notification.setSenderDetail(context.getSessionContext());
                    notification.setUserIdentifier(userIdentifier);
                    notification.setParameters(inputParameters);
                    notifications.add(notification);
                }
            }
        }

        // Return the generated list of notification.
        return notifications;
    }
}
