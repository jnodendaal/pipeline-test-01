package com.pilog.t8.functionality;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FunctionalityManager.FunctionalityHistoryEvent;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.entity.T8DataEntityFieldIterator;
import com.pilog.t8.definition.functionality.T8FunctionalityManagerResource;
import com.pilog.t8.functionality.state.T8FunctionalityDataObjectState;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.state.T8StateDataSet;
import com.pilog.t8.state.T8StateUpdates;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.state.T8ParameterPeristenceHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

import static com.pilog.t8.definition.functionality.T8FunctionalityManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityStateHandler
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8DefinitionManager definitionManager;
    private final T8ParameterPeristenceHandler historyParameterPersistenceHandler;
    private boolean persistenceEnabled; // Flag that can be set to false to disable persistence of functionality states.

    public T8FunctionalityStateHandler(T8Context context)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.definitionManager = serverContext.getDefinitionManager();
        this.persistenceEnabled = true;
        this.historyParameterPersistenceHandler = new T8ParameterPeristenceHandler(HISTORY_FUNC_INPUT_PAR_DE_IDENTIFIER);
    }

    public void init() throws Exception
    {
    }

    public void setEnabled(boolean enabled)
    {
        persistenceEnabled = enabled;
    }

    public T8FunctionalityState retrieveFunctionalityState(T8DataTransaction tx, String functionalityIid)
    {
        try
        {
            T8DataEntity functionalityEntity;
            String entityId;

            // Select all entities linked to the session.
            entityId = T8FunctionalityManagerResource.STATE_FUNC_DE_IDENTIFIER;
            functionalityEntity = tx.retrieve(entityId, HashMaps.newHashMap(entityId + F_FUNCTIONALITY_IID, functionalityIid));
            if (functionalityEntity != null)
            {
                T8StateDataSet dataSet;

                dataSet = new T8StateDataSet();
                dataSet.addData(tx.select(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER, HashMaps.newHashMap(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityIid)));
                dataSet.addData(tx.select(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER, HashMaps.newHashMap(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityIid)));
                return new T8FunctionalityState(functionalityEntity, dataSet);
            }
            else return null;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public void insertFunctionalityState(T8DataTransaction tx, T8FunctionalityState state) throws Exception
    {
        T8StateUpdates stateUpdates;

        // Create an object to store all updates.
        stateUpdates = new T8StateUpdates(ArrayLists.typeSafeList
        (
            STATE_FUNC_DE_IDENTIFIER,
            STATE_FUNC_INPUT_PAR_DE_IDENTIFIER,
            STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER
        ));

        // Get the state updates.
        state.addUpdates(tx, stateUpdates);

        // Persist inserts.
        for (T8DataEntity entity : stateUpdates.getInserts())
        {
            tx.insert(entity);
        }

        // Persist updates.
        for (T8DataEntity entity : stateUpdates.getUpdates())
        {
            tx.update(entity);
        }

        // Persist deletions.
        for (T8DataEntity entity : stateUpdates.getDeletions())
        {
            tx.delete(entity);
        }
    }

    public void insertFunctionalityDataObjectState(T8DataTransaction tx, T8FunctionalityDataObjectState object) throws Exception
    {
        if (persistenceEnabled)
        {
            // Insert the object entity.
            tx.insert(object.createEntity(tx));
        }
    }

    public void deleteFunctionalityState(T8DataTransaction tx, String functionalityIid) throws Exception
    {
        if (persistenceEnabled)
        {
            // Delete the objects belonging to the functionality state.
            tx.delete(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER, new T8DataFilter(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER, HashMaps.newHashMap(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityIid)));

            // Delete the input parameters.
            tx.delete(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER, new T8DataFilter(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER, HashMaps.newHashMap(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityIid)));

            // Delete the functionality state itself.
            tx.delete(STATE_FUNC_DE_IDENTIFIER, new T8DataFilter(STATE_FUNC_DE_IDENTIFIER, HashMaps.newHashMap(STATE_FUNC_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityIid)));
        }
    }

    public void logFunctionalityEvent(T8DataTransaction tx, FunctionalityHistoryEvent event, T8FunctionalityState functionalityState) throws Exception
    {
        T8SessionContext sessionContext;
        Map<String, Object> keyFields;
        T8DataEntity entity;
        String entityId;
        String eventIid;
        String initiatorId;
        String initiatorIid;
        String initiatorOrgId;

        // Determine the initiator.
        sessionContext = tx.getContext().getSessionContext();
        if (sessionContext.isSystemSession())
        {
            initiatorId = sessionContext.getSystemAgentIdentifier();
            initiatorIid = sessionContext.getSystemAgentInstanceIdentifier();
            initiatorOrgId = sessionContext.getOrganizationIdentifier();
        }
        else
        {
            initiatorId = sessionContext.getUserIdentifier();
            initiatorIid = null;
            initiatorOrgId = sessionContext.getOrganizationIdentifier();
        }

        // Create a new task history entity.
        eventIid = T8IdentifierUtilities.createNewGUID();
        entityId = HISTORY_FUNC_DE_IDENTIFIER;
        entity = tx.create(entityId, null);
        entity.setFieldValue(entityId + F_EVENT_IID, eventIid);
        entity.setFieldValue(entityId + F_FUNCTIONALITY_ID, functionalityState.getFunctionalityId());
        entity.setFieldValue(entityId + F_FUNCTIONALITY_IID, functionalityState.getFunctionalityIid());
        entity.setFieldValue(entityId + "$EVENT", event.toString());
        entity.setFieldValue(entityId + "$TIME", new java.sql.Timestamp(System.currentTimeMillis()));
        entity.setFieldValue(entityId + "$INITIATOR_ID", initiatorId);
        entity.setFieldValue(entityId + "$INITIATOR_IID", initiatorIid);
        entity.setFieldValue(entityId + "$INITIATOR_ORG_ID", initiatorOrgId);

        // Insert the entity.
        tx.insert(entity);

        // Insert the Process History parameters.
        keyFields = new HashMap<>();
        keyFields.put(HISTORY_FUNC_INPUT_PAR_DE_IDENTIFIER + F_EVENT_IID, eventIid);
        keyFields.put(HISTORY_FUNC_INPUT_PAR_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityState.getFunctionalityIid());
        historyParameterPersistenceHandler.insertParameters(tx, keyFields, null, T8IdentifierUtilities.stripNamespace(functionalityState.getInputParameters()));
    }

    public T8DataIterator<String> getResidualFunctionalityIidIterator() throws Exception
    {
        T8DataTransaction tx;
        T8DataEntityResults entityResults;
        T8DataSession dataSession;
        String entityIdentifier;
        T8DataFilter filter;
        T8DataFilterCriteria filterCriteria;

        // Create the new filter object.
        entityIdentifier = T8FunctionalityManagerResource.STATE_FUNC_DE_IDENTIFIER;
        filterCriteria = new T8DataFilterCriteria();
        filter = new T8DataFilter(entityIdentifier, filterCriteria);

        // Add the filter criteria (if the functionality is not linked to a flow, it is residual).
        filterCriteria.addFilterClause(T8DataFilterClause.DataFilterConjunction.AND, new T8DataFilterCriterion(entityIdentifier + "$FLOW_IID", T8DataFilterCriterion.DataFilterOperator.IS_NULL, null));

        // Select all entities linked to the session.
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.instantTransaction();
        entityResults = tx.scroll(entityIdentifier, filter);
        return new T8DataEntityFieldIterator(tx, entityResults, "$FUNCTIONALITY_IID");
    }

    public List<String> getSessionFunctionalityIidList(String sessionIdentifier) throws Exception
    {
        T8DataTransaction tx;
        T8DataSession dataSession;
        String entityIdentifier;
        List<T8DataEntity> entityList;
        List<String> idList;

        // Select all entities linked to the session.
        entityIdentifier = T8FunctionalityManagerResource.STATE_FUNC_DE_IDENTIFIER;
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.instantTransaction();
        entityList = tx.select(entityIdentifier, new T8DataFilter(entityIdentifier, HashMaps.newHashMap(entityIdentifier + "$SESSION_ID", sessionIdentifier)));

        // Get the IID's of all the functionalities linked to the session.
        idList = new ArrayList<String>();
        for (T8DataEntity entity : entityList)
        {
            idList.add((String)entity.getFieldValue(entityIdentifier + "$FUNCTIONALITY_IID"));
        }

        // Return the final list of all functionalities linked to the session.
        return idList;
    }

    public List<T8FunctionalityState> retrieveWorkflowFunctionalityStates(T8DataTransaction tx, String flowIid)
    {
        try
        {
            List<T8FunctionalityState> functionalityStates;
            List<T8DataEntity> entityList;
            String entityId;

            // Select all entities linked to the session.
            entityId = T8FunctionalityManagerResource.STATE_FUNC_DE_IDENTIFIER;
            entityList = tx.select(entityId, new T8DataFilter(entityId, HashMaps.newHashMap(entityId + F_FLOW_IID, flowIid)));

            // Create the complete states from each entity retrieved.
            functionalityStates = new ArrayList<>(entityList.size());
            for (T8DataEntity functionalityStateEntity : entityList)
            {
                T8StateDataSet dataSet;
                String functionalityIid;

                functionalityIid = (String)functionalityStateEntity.getFieldValue(F_FUNCTIONALITY_IID);

                dataSet = new T8StateDataSet();
                dataSet.addData(tx.select(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER, HashMaps.newHashMap(STATE_FUNC_INPUT_PAR_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityIid)));
                dataSet.addData(tx.select(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER, HashMaps.newHashMap(STATE_FUNC_DATA_OBJECT_DE_IDENTIFIER + F_FUNCTIONALITY_IID, functionalityIid)));
                functionalityStates.add(new T8FunctionalityState(functionalityStateEntity, dataSet));
            }

            // Return the retrieved notifications.
            return functionalityStates;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<String> getOperationFunctionalityIidList(String operationIid) throws Exception
    {
        T8DataTransaction tx;
        T8DataSession dataSession;
        String entityIdentifier;
        List<T8DataEntity> entityList;
        List<String> idList;

        // Select all entities linked to the session.
        entityIdentifier = T8FunctionalityManagerResource.STATE_FUNC_DE_IDENTIFIER;
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.instantTransaction();
        entityList = tx.select(entityIdentifier, new T8DataFilter(entityIdentifier, HashMaps.newHashMap(entityIdentifier + "$OPERATION_IID", operationIid)));

        // Get the IID's of all the functionalities linked to the session.
        idList = new ArrayList<String>();
        for (T8DataEntity entity : entityList)
        {
            idList.add((String)entity.getFieldValue(entityIdentifier + "$FUNCTIONALITY_IID"));
        }

        // Return the final list of all functionalities linked to the session.
        return idList;
    }
}
