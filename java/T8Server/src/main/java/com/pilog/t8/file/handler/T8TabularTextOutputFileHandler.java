package com.pilog.t8.file.handler;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.file.T8FileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.files.text.TabularTextOutputFile;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TabularTextOutputFileHandler implements T8FileHandler
{
    private final T8Context context;
    private final T8FileManager fileManager;
    private TabularTextOutputFile textOutputFile;
    private final String fileContextIid;
    private final String filePath;
    private String encoding;
    private String separator;

    public static final String FILE_HANDLER_ID = "@FILE_TABULAR_TEXT_OUTPUT";

    public T8TabularTextOutputFileHandler(T8Context context, T8FileManager fileManager, String contextIid, String filePath)
    {
        this.context = context;
        this.fileManager = fileManager;
        this.fileContextIid = contextIid;
        this.filePath = filePath;
        this.encoding = "UTF-8";
        this.separator = "\t";
    }

    public String getEncoding()
    {
        return encoding;
    }

    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    public String getSeparator()
    {
        return separator;
    }

    public void setSeparator(String separator)
    {
        this.separator = separator;
    }

    public boolean isOpen()
    {
        return (textOutputFile != null);
    }

    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIid, filePath);
    }

    @Override
    public void open() throws Exception
    {
        OutputStream outputStream;

        outputStream = fileManager.getFileOutputStream(context, fileContextIid, filePath, false);
        textOutputFile = new TabularTextOutputFile(encoding, separator);
        textOutputFile.openFile(outputStream);
    }

    @Override
    public void close() throws Exception
    {
        if (textOutputFile != null) textOutputFile.closeFile();
        textOutputFile = null;
    }

    public void writeRow(Map<String, Object> rowData) throws Exception
    {
        // Write the data row to the output file.
        textOutputFile.writeRow(rowData);
    }

    public void writeRow(List<Object> rowData) throws Exception
    {
        // Write the data row to the output file.
        textOutputFile.writeRow(rowData);
    }
}
