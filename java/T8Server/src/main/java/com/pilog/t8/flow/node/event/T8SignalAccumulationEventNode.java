package com.pilog.t8.flow.node.event;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.key.T8SignalExecutionKey;
import com.pilog.t8.definition.flow.node.event.T8SignalAccumulationEventDefinition;
import com.pilog.t8.definition.flow.node.event.T8SignalAccumulationScriptDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SignalAccumulationEventNode extends T8DefaultFlowNode
{
    private final T8SignalAccumulationEventDefinition definition;

    public enum T8SignalAccumulationEventNodeExecutionStep {WAITING_FOR_SIGNAL};

    public T8SignalAccumulationEventNode(T8Context context, T8Flow parentFlow, T8SignalAccumulationEventDefinition definition, String nodeIid)
    {
        super(context, parentFlow, definition, nodeIid);
        this.definition = definition;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters) throws Exception
    {
        if (!isExecutionStepComplete(T8SignalAccumulationEventNodeExecutionStep.WAITING_FOR_SIGNAL.toString()))
        {
            if (executionKey instanceof T8SignalExecutionKey)
            {
                Map<String, Object> outputParameters;
                Map<String, Object> signalParameters;

                // Get the signal parameters from the execution key.
                signalParameters = ((T8SignalExecutionKey)executionKey).getSignalParameters();

                // Execute the accumulation script and complete the step if accumulation is complete.
                outputParameters = executeAccumulationScript(inputParameters, signalParameters);
                if (outputParameters != null)
                {
                    // Clear the wait key (if any was set).
                    nodeState.clearWaitKeys();

                    // Complete the execution step.
                    completeExecutionStep(T8SignalAccumulationEventNodeExecutionStep.WAITING_FOR_SIGNAL.toString());
                    return outputParameters;
                }
                else
                {
                    // Accumulation not yet complete, so wait for another signal.
                    return null;
                }
            }
            else
            {
                String keyParameter;

                // Get the signal key parameter from the flow parameters.
                keyParameter = (String)inputParameters.get(definition.getKeyParameterId());
                if (keyParameter != null)
                {
                    // Synchronize on flowState when a single state transition transaction must occur.
                    synchronized (flowState)
                    {
                        // The signal has not been received yet, so set a new wait key.
                        nodeState.addSignalCatchWaitKey(definition.getSignalIdentifier(), keyParameter);
                        parentFlow.persistState();
                        return null;
                    }
                }
                else throw new Exception("The workflow parameter '" + definition.getKeyParameterId() + "' to use as key for signal '" + definition.getSignalIdentifier() + "' was not found.  Flow parameters: " + inputParameters);
            }
        }
        else throw new Exception("Cannot wait for signal when it has already been caught.");
    }

    /**
     * Executes the signal accumulation script.  This script receives the flow
     * and signal parameters as input and must accumulate signal data into the
     * flow parameter state.  This method returns a null value as long as the
     * accumulation completion condition has not yet been met.  This method
     * returns a valid non-null map of flow parameters when accumulation has
     * been completed.
     * @param serverContext The server context in which this script is executed.
     * @param inputFlowParameters The flow parameters received as input for
     * script execution.
     * @param signalParameters The signal parameters received as input for
     * script execution.
     * @return The output flow parameters with all accumulated signal data, if
     * accumulation has been completed or null otherwise.
     * @throws Exception
     */
    private Map<String, Object> executeAccumulationScript(Map<String, Object> inputFlowParameters, Map<String, Object> signalParameters) throws Exception
    {
        T8ServerContextScriptDefinition scriptDefinition;
        Map<String, Object> inputParameters;
        Map<String, Object> flowParameters;
        Object outputObject;
        T8Script script;

        // Create a map of flow input parameters (add parameters from the node state that have already been output by previous executions of this accumulation script).
        flowParameters = new HashMap<String, Object>();
        if (inputFlowParameters != null) flowParameters.putAll(inputFlowParameters);
        if (nodeState.getOutputParameters() != null) flowParameters.putAll(nodeState.getOutputParameters()); // This will overwrite input flow parameters that have already been updated by previous executions.

        // Create the script input parameter map.
        inputParameters = new HashMap<String, Object>();
        inputParameters.put(T8SignalAccumulationScriptDefinition.PARAMETER_FLOW_PARAMETERS, flowParameters);
        inputParameters.put(T8SignalAccumulationScriptDefinition.PARAMETER_SIGNAL_PARAMETERS, signalParameters);

        // Execute the script.
        scriptDefinition = definition.getAccumulationScriptDefinition();
        script = scriptDefinition.getNewScriptInstance(internalContext);
        outputObject = script.executeScript(inputParameters);

        // Process the script output.
        if (outputObject == null)
        {
            // No result means accumulation not yet complete.
            return null;
        }
        else if (outputObject instanceof Map)
        {
            Map<String, Object> resultMap;
            Map<String, Object> outputFlowParameters;
            Boolean completeFlag;

            // Get the result from the script output.
            resultMap = T8IdentifierUtilities.stripNamespace(definition.getNamespace(), (Map<String, Object>)outputObject, false);
            outputFlowParameters = (Map<String, Object>)resultMap.get(T8SignalAccumulationScriptDefinition.PARAMETER_FLOW_PARAMETERS);
            completeFlag = (Boolean)resultMap.get(T8SignalAccumulationScriptDefinition.PARAMETER_COMPLETE);

            // Save the output flow parameters to the state.
            if (outputFlowParameters != null)
            {
                nodeState.addOutputParameters(outputFlowParameters);
                parentFlow.persistState();
            }

            // Return the completion flag.
            return ((completeFlag != null) && (completeFlag)) ? outputFlowParameters : null;
        }
        else throw new RuntimeException("Script activity '" + definition + "' did not return a valida Map object as output.");
    }
}
