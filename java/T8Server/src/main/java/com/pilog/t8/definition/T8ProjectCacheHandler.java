package com.pilog.t8.definition;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.cache.T8ProjectDefinitionCache;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The main definition manager will always act on the inherited hierarchy of
 * definitions i.e. it does not take project specific changes into account.
 * This handle provides access methods that do allow the use of project
 * identifiers so that changes and retrievals can be performed on specific
 * project definition caches instead of the inherited set of definitions.
 *
 * @author Bouwer du Preez
 */
public class T8ProjectCacheHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ProjectCacheHandler.class);

    private final List<T8ProjectDefinitionCache> projectCaches;
    private final T8DefinitionManager definitionManager;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final Set<String> typeIds;
    private boolean initialized;

    public T8ProjectCacheHandler(T8Context context, T8DefinitionManager definitionManager, Set<String> typeIds)
    {
        this.definitionManager = definitionManager;
        this.serverContext = context.getServerContext();
        this.context = context;
        this.projectCaches = new ArrayList<>();
        this.typeIds = new HashSet<>(typeIds);
        this.initialized = false;
    }

    public void init()
    {
        for (T8ProjectDefinitionCache definitionCache : projectCaches)
        {
            LOGGER.log("Initializing Project Definition Cache: " + definitionCache.getProjectId());
            definitionCache.init();
        }

        initialized = true;
    }

    public void destroy()
    {
        for (T8ProjectDefinitionCache definitionCache : projectCaches)
        {
            definitionCache.destroy();
        }
    }

    public void cacheDefinitions() throws Exception
    {
        for (T8ProjectDefinitionCache definitionCache : projectCaches)
        {
            LOGGER.log("Caching Project Definitions: " + definitionCache.getProjectId());
            definitionCache.cacheDefinitions();
        }
    }

    public void addCache(T8ProjectDefinition projectDefinition) throws Exception
    {
        T8ProjectDefinitionCache definitionCache;

        // Initialize the definition cache.
        definitionCache = new T8ProjectDefinitionCache(context, definitionManager, serverContext.getContextPath(), projectDefinition, typeIds);
        addCache(definitionCache);

        // If this cache has been initialized, also initialize the new definition cache that has been added.
        if (initialized)
        {
            definitionCache.init();
        }
    }

    private void addCache(T8ProjectDefinitionCache cache)
    {
        this.projectCaches.add(cache);
    }

    public void projectDefinitionSaved(T8ProjectDefinition projectDefinition)
    {
        T8ProjectDefinitionCache definitionCache;

        definitionCache = getProjectDefinitionCache(projectDefinition.getIdentifier());
        definitionCache.projectDefinitionSaved(projectDefinition);
    }

    public void removeAllCaches()
    {
        this.projectCaches.clear();
    }

    public boolean containsProjectDefinitionCache(String projectId)
    {
        return getProjectDefinitionCache(projectId) != null;
    }

    private T8ProjectDefinitionCache getProjectDefinitionCache(String projectId)
    {
        for (T8ProjectDefinitionCache definitionCache : projectCaches)
        {
            if (definitionCache.getProjectId().equals(projectId))
            {
                return definitionCache;
            }
        }

        return null;
    }

    public void projectRenamed(String oldProjectId, String newProjectId) throws Exception
    {
        T8ProjectDefinitionCache projectDefinitionCache;

        projectDefinitionCache = getProjectDefinitionCache(oldProjectId);
        if (projectDefinitionCache != null)
        {
            projectDefinitionCache.projectRenamed(newProjectId);
        }
        else throw new Exception("Project definition cache to reset not found: " + oldProjectId);
    }

    public T8Definition loadDefinition(String projectId, String definitionId) throws Exception
    {
        T8ProjectDefinitionCache projectCache;

        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            T8Definition definition;

            definition = projectCache.loadDefinition(definitionId);
            if (definition != null)
            {
                return definition;
            }
            else if (!T8IdentifierUtilities.isPrivateId(definitionId))
            {
                for (String requiredProjectId : projectCache.getRequiredProjectIds())
                {
                    T8ProjectDefinitionCache requiredProjectCache;

                    requiredProjectCache = getProjectDefinitionCache(requiredProjectId);
                    if (requiredProjectCache != null)
                    {
                        definition = requiredProjectCache.loadDefinition(definitionId);
                        if (definition != null) return definition;
                    }
                }

                return null;
            }
            else return null;
        }
        else return null;
    }

    /**
     * Loads the specified definition from the first project cache containing a matching id.
     * This method should only be used for public definitions.  If this method is called using a
     * private definition id, an IllegalArgumentException is thrown.
     * @param definitionId The id of the definition to load.
     * @return The definition requested or null if not found.
     * @throws Exception
     */
    public T8Definition loadDefinition(String definitionId) throws Exception
    {
        if (!T8IdentifierUtilities.isPrivateId(definitionId))
        {
            for (T8ProjectDefinitionCache projectCache : projectCaches)
            {
                T8Definition definition;

                definition = projectCache.loadDefinition(definitionId);
                if (definition != null)
                {
                    return definition;
                }
            }

            return null;
        }
        else throw new IllegalArgumentException("Cannot load private definition without specifying project: " + definitionId);
    }

    public void saveDefinition(T8Definition definition) throws Exception
    {
        T8ProjectDefinitionCache projectCache;
        String projectId;

        projectId = definition.getProjectIdentifier();
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            projectCache.saveDefinition(definition);
        }
        else throw new Exception("No applicable definition cache found for definition '" + definition + "' with project identifier: " + projectId);
    }

    public void deleteDefinition(String projectId, String definitionId) throws Exception
    {
        T8ProjectDefinitionCache projectCache;

        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            projectCache.deleteDefinition(definitionId);
        }
        else throw new IllegalArgumentException("Project not found: " + projectId);
    }

    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String projectId, String typeId, boolean includePublicDefinitions, boolean includePrivateDefinitions, boolean includeRequiredPublicDefinitions) throws Exception
    {
        List<T8DefinitionMetaData> metaList;
        T8ProjectDefinitionCache projectCache;

        // Add project-specific meta data.
        metaList = new ArrayList<>();
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            for (T8DefinitionMetaData metaData : projectCache.getTypeDefinitionMetaData(typeId))
            {
                if ((metaData.isPublic()) && (includePublicDefinitions))
                {
                    metaList.add(metaData);
                }
                else if (includePrivateDefinitions)
                {
                    metaList.add(metaData);
                }
            }

            if (includeRequiredPublicDefinitions)
            {
                for (String requiredProjectId : projectCache.getRequiredProjectIds())
                {
                    metaList.addAll(getTypeDefinitionMetaData(requiredProjectId, typeId, true, false, false));
                }
            }
        }

        return metaList;
    }

    public Set<String> getTypeDefinitionIds(String projectId, String typeId, boolean includePublicDefinitions, boolean includePrivateDefinitions, boolean includeRequiredPublicDefinitions) throws Exception
    {
        T8ProjectDefinitionCache projectCache;
        Set<String> ids;

        // Add project-specific meta data.
        ids = new HashSet<>();
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            for (T8DefinitionMetaData metaData : projectCache.getTypeDefinitionMetaData(typeId))
            {
                if ((metaData.isPublic()) && (includePublicDefinitions))
                {
                    ids.add(metaData.getId());
                }
                else if (includePrivateDefinitions)
                {
                    ids.add(metaData.getId());
                }
            }

            if (includeRequiredPublicDefinitions)
            {
                for (String requiredProjectId : projectCache.getRequiredProjectIds())
                {
                    ids.addAll(T8ProjectCacheHandler.this.getTypeDefinitionIds(requiredProjectId, typeId, true, false, false));
                }
            }
        }

        return ids;
    }

    /**
     * Returns definition meta data for all public definitions of the specific type across all projects.
     * @param typeId The id of the definition type for which to return meta data.
     * @return Definition meta data for all public definitions of the specific type across all projects.
     * @throws Exception
     */
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String typeId) throws Exception
    {
        List<T8DefinitionMetaData> metaDataList;

        // Add project-specific meta data.
        metaDataList = new ArrayList<>();
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            for (T8DefinitionMetaData metaData : projectDefinitionCache.getTypeDefinitionMetaData(typeId))
            {
                if (metaData.isPublic())
                {
                    metaDataList.add(metaData);
                }
            }
        }

        return metaDataList;
    }

    public T8DefinitionMetaData getDefinitionMetaData(String projectId, String definitionId) throws Exception
    {
        T8DefinitionMetaData metaData;

        // First try to find the meta data in the specified project.
        if (projectId != null)
        {
            T8ProjectDefinitionCache projectCache;

            projectCache = getProjectDefinitionCache(projectId);
            metaData = projectCache.getDefinitionMetaData(definitionId);
            if (metaData != null) return metaData;
        }

        // Loop through the project definition caches in descending order, because caches at higher indices override caches at lower indices.
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            metaData = projectDefinitionCache.getDefinitionMetaData(definitionId);
            if (metaData != null)
            {
                return metaData;
            }
        }

        return null;
    }

    public List<T8DefinitionMetaData> getProjectDefinitionMetaData(String projectId) throws Exception
    {
        T8ProjectDefinitionCache projectCache;

        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            return projectCache.getAllDefinitionMetaData();
        }
        else return new ArrayList<>();
    }

    public List<String> getGroupDefinitionIds(String groupId) throws Exception
    {
        Set<String> ids;

        // Add identifiers from project caches.
        ids = new HashSet<>();
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            for (T8DefinitionMetaData definitionMeta : projectDefinitionCache.getGroupDefinitionMetaData(groupId))
            {
                if (definitionMeta.isPublic()) ids.add(definitionMeta.getId());
            }
        }

        return new ArrayList<>(ids);
    }

    public List<String> getTypeDefinitionIds(String typeId) throws Exception
    {
        Set<String> ids;

        // Add identifiers from project caches.
        ids = new HashSet<>();
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            for (T8DefinitionMetaData definitionMeta : projectDefinitionCache.getTypeDefinitionMetaData(typeId))
            {
                if (definitionMeta.isPublic()) ids.add(definitionMeta.getId());
            }
        }

        return new ArrayList<>(ids);
    }

    public List<String> getGroupIdentifiers() throws Exception
    {
        Set<String> groupIds;

        // Add groups from project caches.
        groupIds = new HashSet<>();
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            groupIds.addAll(projectDefinitionCache.getGroupIdentifiers());
        }

        return new ArrayList<>(groupIds);
    }

    public boolean containsDefinition(String projectId, String definitionId) throws Exception
    {
        T8ProjectDefinitionCache projectCache;

        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            return projectCache.containsDefinition(definitionId);
        }
        else return false;
    }

    public boolean containsDefinition(String definitionId) throws Exception
    {
        if (definitionId == null) return false;
        else
        {
            // Check project caches.
            for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
            {
                if (projectDefinitionCache.containsDefinition(definitionId)) return true;
            }

            return false;
        }
    }

    public int getCachedDefinitionCount()
    {
        int count;

        count = 0;

        // Add project count.
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            count += projectDefinitionCache.getCachedDefinitionCount();
        }

        return count;
    }

    public long getCachedCharacterSize()
    {
        long size;

        size = 0;

        // Add project size.
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            size += projectDefinitionCache.getCachedCharacterSize();
        }

        return size;
    }

    public List<T8DefinitionHandle> findDefinitionsByText(T8Context context, String searchText) throws Exception
    {
        List<T8DefinitionHandle> handles;

        // Create a list to hold all definition handles that we find.
        handles = new ArrayList<>();

        // Add identifiers from project caches.
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            String projectId;

            projectId = projectDefinitionCache.getProjectId();
            for (String definitionId : projectDefinitionCache.findDefinitionsByText(searchText))
            {
                handles.add(new T8DefinitionHandle(T8DefinitionLevel.PROJECT, definitionId, projectId));
            }
        }

        // Return the complete list of handles.
        return handles;
    }

    public List<T8DefinitionHandle> findDefinitionsByIdentifier(T8Context context, String searchId, boolean includeReferencesOnly) throws Exception
    {
        List<T8DefinitionHandle> handles;

        // Create a list to hold all definition handles that we find.
        handles = new ArrayList<>();

        // Add identifiers from project caches.
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            String projectId;

            projectId = projectDefinitionCache.getProjectId();
            for (String definitionId : projectDefinitionCache.findDefinitionsByIdentifier(searchId))
            {
                if ((!includeReferencesOnly) || (!definitionId.equals(searchId)))
                {
                    handles.add(new T8DefinitionHandle(T8DefinitionLevel.PROJECT, definitionId, projectId));
                }
            }
        }

        return handles;
    }

    public List<T8DefinitionHandle> findDefinitionsByRegularExpression(T8Context context, String expression) throws Exception
    {
        List<T8DefinitionHandle> handles;

        handles = new ArrayList<T8DefinitionHandle>();
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            List<String> identifiers;
            String projectIdentifier;

            projectIdentifier = projectDefinitionCache.getProjectId();
            identifiers = projectDefinitionCache.findDefinitionsByRegularExpression(expression);
            for (String identifier : identifiers)
            {
                handles.add(new T8DefinitionHandle(T8DefinitionLevel.PROJECT, identifier, projectIdentifier));
            }
        }

        return handles;
    }

    public List<T8DefinitionHandle> findDefinitionsUsing(T8Context context, String id) throws Exception
    {
        List<T8DefinitionHandle> handles;

        handles = new ArrayList<T8DefinitionHandle>();
        for (T8ProjectDefinitionCache projectCache : projectCaches)
        {
            List<String> usedByDefinitionIds;
            String projectId;

            projectId = projectCache.getProjectId();
            usedByDefinitionIds = projectCache.findDefinitionsByIdentifier(id);
            for (String usedByDefinitionId : usedByDefinitionIds)
            {
                if (!usedByDefinitionId.equals(id))
                {
                    handles.add(new T8DefinitionHandle(T8DefinitionLevel.PROJECT, usedByDefinitionId, projectId));
                }
            }
        }

        return handles;
    }

    public List<T8DefinitionHandle> findDefinitionsUsing(T8Context context, String projectId, String id) throws Exception
    {
        T8ProjectDefinitionCache projectCache;
        List<T8DefinitionHandle> handles;

        handles = new ArrayList<T8DefinitionHandle>();
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            List<String> usedByDefinitionIds;

            usedByDefinitionIds = projectCache.findDefinitionsByIdentifier(id);
            for (String usedByDefinitionId : usedByDefinitionIds)
            {
                if (!usedByDefinitionId.equals(id))
                {
                    handles.add(new T8DefinitionHandle(T8DefinitionLevel.PROJECT, usedByDefinitionId, projectId));
                }
            }
        }

        return handles;
    }

    public List<T8DefinitionHandle> findDefinitionsByIdentifier(T8Context context, String identifier) throws Exception
    {
        List<T8DefinitionHandle> handles;

        handles = new ArrayList<T8DefinitionHandle>();
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            List<String> definitionIds;
            String projectId;

            projectId = projectDefinitionCache.getProjectId();
            definitionIds = projectDefinitionCache.findDefinitionsByIdentifier(identifier);
            for (String definitionId : definitionIds)
            {
                handles.add(new T8DefinitionHandle(T8DefinitionLevel.PROJECT, definitionId, projectId));
            }
        }

        return handles;
    }

    public List<T8DefinitionHandle> findDefinitionsByDatum(T8Context context, String datumIdentifier, T8DataType dataType, String content) throws Exception
    {
        List<T8DefinitionHandle> handles;

        handles = new ArrayList<T8DefinitionHandle>();
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            List<String> identifiers;
            String projectId;

            projectId = projectDefinitionCache.getProjectId();
            identifiers = projectDefinitionCache.findDefinitionsByDatum(datumIdentifier, dataType, content);
            for (String identifier : identifiers)
            {
                handles.add(new T8DefinitionHandle(T8DefinitionLevel.PROJECT, identifier, projectId));
            }
        }

        return handles;
    }

    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String projectId, String groupId, boolean includePublicDefinitions, boolean includePrivateDefinitions, boolean includeRequiredPublicDefinitions) throws Exception
    {
        List<T8DefinitionMetaData> metaList;
        T8ProjectDefinitionCache projectCache;

        // Create the collection.
        metaList = new ArrayList<>();

        // Add all other project-specific definition meta data.
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            for (T8DefinitionMetaData metaData : projectCache.getGroupDefinitionMetaData(groupId))
            {
                if ((metaData.isPublic()) && (includePublicDefinitions))
                {
                    metaList.add(metaData);
                }
                else if (includePrivateDefinitions)
                {
                    metaList.add(metaData);
                }
            }

            if (includeRequiredPublicDefinitions)
            {
                for (String requiredProjectId : projectCache.getRequiredProjectIds())
                {
                    metaList.addAll(getGroupDefinitionMetaData(requiredProjectId, groupId, true, false, false));
                }
            }
        }

        return metaList;
    }

    public Set<String> getGroupDefinitionIds(String projectId, String groupId, boolean includePublicDefinitions, boolean includePrivateDefinitions, boolean includeRequiredPublicDefinitions) throws Exception
    {
        T8ProjectDefinitionCache projectCache;
        Set<String> ids;

        // Create the collection.
        ids = new HashSet<>();

        // Add all other project-specific definition meta data.
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            for (T8DefinitionMetaData metaData : projectCache.getGroupDefinitionMetaData(groupId))
            {
                if ((metaData.isPublic()) && (includePublicDefinitions))
                {
                    ids.add(metaData.getId());
                }
                else if (includePrivateDefinitions)
                {
                    ids.add(metaData.getId());
                }
            }

            if (includeRequiredPublicDefinitions)
            {
                for (String requiredProjectId : projectCache.getRequiredProjectIds())
                {
                    ids.addAll(T8ProjectCacheHandler.this.getGroupDefinitionIds(requiredProjectId, groupId, true, false, false));
                }
            }
        }

        return ids;
    }

    /**
     * Returns definition meta data for all public definitions of the specified group across all projects.
     * @param groupId The id of the group for which to return definition meta data.
     * @return Definition meta data for all public definitions of the specified group across all projects.
     * @throws Exception
     */
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String groupId) throws Exception
    {
        Map<String, T8DefinitionMetaData> metaDataMap;

        // Create the collection.
        metaDataMap = new HashMap<>();

        // Add all other project-specific definition meta data.
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            for (T8DefinitionMetaData metaData : projectDefinitionCache.getGroupDefinitionMetaData(groupId))
            {
                if (metaData.isPublic())
                {
                    metaDataMap.put(metaData.getId(), metaData);
                }
            }
        }

        return new ArrayList<>(metaDataMap.values());
    }

    public List<T8DefinitionMetaData> getAllDefinitionMetaData() throws Exception
    {
        Map<String, T8DefinitionMetaData> metaDataMap;

        metaDataMap = new HashMap<>();

        // Add Project definition meta data.
        for (T8ProjectDefinitionCache projectDefinitionCache : projectCaches)
        {
            for (T8DefinitionMetaData metaData : projectDefinitionCache.getAllDefinitionMetaData())
            {
                metaDataMap.put(metaData.getId(), metaData);
            }
        }

        return new ArrayList<>(metaDataMap.values());
    }

    public void backupProject(String projectId) throws Exception
    {
        T8ProjectDefinitionCache projectCache;

        // Add all other project-specific definition meta data.
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            projectCache.backup();
        }
        else throw new Exception("Project not found: " + projectId);
    }

    public void restoreProject(String projectId) throws Exception
    {
        T8ProjectDefinitionCache projectCache;

        // Add all other project-specific definition meta data.
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            projectCache.restore();
        }
        else throw new Exception("Project not found: " + projectId);
    }

    public void deleteProject(String projectId) throws Exception
    {
        T8ProjectDefinitionCache projectCache;

        // Add all other project-specific definition meta data.
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            // Delete the contents of the project cache.
            projectCache.delete();

            // Destroy the project cache.
            projectCache.destroy();

            // Remove the destroyed project cache from the list of active caches.
            projectCaches.remove(projectCache);
        }
        else throw new Exception("Project not found: " + projectId);
    }

    public void clearProject(String projectId) throws Exception
    {
        T8ProjectDefinitionCache projectCache;

        // Add all other project-specific definition meta data.
        projectCache = getProjectDefinitionCache(projectId);
        if (projectCache != null)
        {
            // Delete the contents of the project cache.
            projectCache.delete();
        }
        else throw new Exception("Project not found: " + projectId);
    }
}
