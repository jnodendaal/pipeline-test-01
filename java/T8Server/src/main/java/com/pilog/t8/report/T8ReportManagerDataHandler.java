package com.pilog.t8.report;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.filter.T8DataFilterExpressionParser;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.report.T8ReportDefinition;
import com.pilog.t8.report.T8ReportState.Status;
import com.pilog.t8.state.T8ParameterPeristenceHandler;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import static com.pilog.t8.definition.report.T8ReportManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ReportManagerDataHandler
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8DefinitionManager definitionManager;
    private final T8ParameterPeristenceHandler historyParameterPersistenceHandler;
    private boolean persistenceEnabled; // Flag that can be set to false to disable persistence of flow states.

    public T8ReportManagerDataHandler(T8Context internalContext)
    {
        this.serverContext = internalContext.getServerContext();
        this.internalContext = internalContext;
        this.definitionManager = serverContext.getDefinitionManager();
        this.persistenceEnabled = true;
        this.historyParameterPersistenceHandler = new T8ParameterPeristenceHandler(HISTORY_REPORT_PAR_DE_IDENTIFIER);
    }

    public void setEnabled(boolean enabled)
    {
        persistenceEnabled = enabled;
    }

    public void updateState(T8DataTransaction tx, T8ReportState state) throws Exception
    {
        if (persistenceEnabled)
        {
            T8DataEntity entity;

            // Create a new state entity.
            entity = createStateEntity(tx, state);

            // Update the entity.
            tx.update(entity);
        }
    }

    public void insertState(T8DataTransaction tx, T8ReportState state) throws Exception
    {
        if (persistenceEnabled)
        {
            T8DataEntity entity;

            // Create a new state entity.
            entity = createStateEntity(tx, state);

            // Insert the entity.
            tx.insert(entity);
        }
    }

    public boolean deleteState(T8DataTransaction tx, String reportIid) throws Exception
    {
        if (persistenceEnabled)
        {
            int deletionCount;

            // Delete report state parameters.
            deletionCount = tx.delete(STATE_REPORT_DE_IDENTIFIER, new T8DataFilter(STATE_REPORT_DE_IDENTIFIER, HashMaps.newHashMap(STATE_REPORT_DE_IDENTIFIER + F_REPORT_IID, reportIid)));
            return deletionCount > 0;
        }
        else return false;
    }

    public T8ReportSummary retrieveReportSummary(T8Context context, T8ReportFilter filter) throws Exception
    {
        List<T8DataEntity> entityList;
        T8ReportSummary summary;
        T8DataSession dataSession;
        T8DataTransaction tx;
        String filterEntityId;
        String entityId;

        // Get the identifiers for the entities to use.
        entityId = STATE_REPORT_SUMMARY_DE_IDENTIFIER;
        filterEntityId = STATE_REPORT_DE_IDENTIFIER;

        // Retrieve the summary data.
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.instantTransaction();
        entityList = tx.select(entityId, filter.getDataFilter(context, filterEntityId));

        // Create a new summary object and add all of the retrieved entries to it.
        summary = new T8ReportSummary();
        for (T8DataEntity entity : entityList)
        {
            T8ReportTypeSummary reportType;
            int entityCount;

            // Get the report type summary, creating it if it doesn't exist
            reportType = getReportTypeSummary(summary, entity, entityId);

            // Increment the total count for the report type.
            entityCount = (Integer)entity.getFieldValue(entityId + F_REPORT_COUNT);
            reportType.setTotalCount(reportType.getTotalCount() + entityCount);
        }

        return summary;
    }

    private T8ReportTypeSummary getReportTypeSummary(T8ReportSummary summary, T8DataEntity reportSummaryEntity, String entityId) throws Exception
    {
        T8ReportTypeSummary reportTypeSummary;
        String projectId;
        String reportId;

        reportId = (String)reportSummaryEntity.getFieldValue(entityId + F_REPORT_ID);
        projectId = (String)reportSummaryEntity.getFieldValue(entityId + F_PROJECT_ID);
        reportTypeSummary = summary.getTypeSummary(reportId);
        if (reportTypeSummary == null)
        {
            T8ReportDefinition reportDefinition;

            reportDefinition = (T8ReportDefinition)definitionManager.getRawDefinition(internalContext, projectId, reportId);
            if (reportDefinition != null)
            {
                reportTypeSummary = new T8ReportTypeSummary(reportId);
                reportTypeSummary.setDisplayName(reportDefinition.getTypeName());
                reportTypeSummary.setDescription(reportDefinition.getTypeDescription());
                summary.putTypeSummary(reportTypeSummary);
            }
        }

        return reportTypeSummary;
    }

    public List<T8ReportState> retrieveReportStates(T8DataTransaction tx, T8ReportFilter reportFilter, String searchExpression, int pageOffset, int pageSize) throws Exception
    {
        T8DataFilterExpressionParser parser;
        T8DataFilter filter;

        // Parse the search expression and create a filter from it.
        parser = new T8DataFilterExpressionParser(STATE_REPORT_DE_IDENTIFIER, ArrayLists.newArrayList(STATE_REPORT_DE_IDENTIFIER + F_NAME));
        parser.setWhitespaceConjunction(T8DataFilterClause.DataFilterConjunction.AND);
        filter = parser.parseExpression(searchExpression, false);
        filter.addFilterCriteria(T8DataFilterClause.DataFilterConjunction.AND, reportFilter.getDataFilter(tx.getContext(), STATE_REPORT_DE_IDENTIFIER));

        // Retrieve report states using the created filter.
        return retrieveStates(tx, filter, pageOffset, pageSize);
    }

    private List<T8ReportState> retrieveStates(T8DataTransaction tx, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        List<T8DataEntity> reportStateEntities;
        List<T8ReportState> reportStates;

        // Retrieve the report state entities.
        reportStateEntities = tx.select(STATE_REPORT_DE_IDENTIFIER, filter, pageOffset, pageSize);

        // Create the complete states from each entity retrieved.
        reportStates = new ArrayList<>(reportStateEntities.size());
        for (T8DataEntity reportStateEntity : reportStateEntities)
        {
            T8ReportState state;

            // Construct the new report state.
            state = constructReportState(reportStateEntity);
            state.setStatus(Status.COMPLETED);
            reportStates.add(state);
        }

        // Return the retrieved report states.
        return reportStates;
    }

    public int countReportStates(T8DataTransaction tx, T8ReportFilter reportFilter) throws Exception
    {
        T8DataFilter filter;

        // Create the data filter to be used for report state retrieval.
        filter = new T8DataFilter(STATE_REPORT_DE_IDENTIFIER);
        filter.addFilterCriterion(STATE_REPORT_DE_IDENTIFIER + F_USER_ID, DataFilterOperator.EQUAL, reportFilter.getUserId());

        // Retrieve the report state entities.
        return tx.count(STATE_REPORT_DE_IDENTIFIER, filter);
    }

    public T8ReportState retrieveReportState(T8DataTransaction tx, String reportIid) throws Exception
    {
        T8DataEntity reportEntity;

        // Retrieve the report state entity.
        reportEntity = tx.retrieve(STATE_REPORT_DE_IDENTIFIER, HashMaps.newHashMap(STATE_REPORT_DE_IDENTIFIER + F_REPORT_IID, reportIid));
        if (reportEntity != null)
        {
            T8ReportState state;

            // Construct the new report state.
            state = constructReportState(reportEntity);
            state.setStatus(Status.COMPLETED);
            return state;
        }
        else return null;
    }

    public List<String> retrieveExpiredReportIids(T8DataTransaction tx, int pageSize) throws Exception
    {
        List<T8DataEntity> reportStateEntities;
        List<String> reportIids;
        T8DataFilter filter;

        // Create the data filter to be used for report state retrieval.
        filter = new T8DataFilter(STATE_REPORT_DE_IDENTIFIER);
        filter.addFilterCriterion(STATE_REPORT_DE_IDENTIFIER + F_EXPIRATION_TIME, DataFilterOperator.LESS_THAN, new T8Timestamp(System.currentTimeMillis()));

        // Retrieve the report state entities.
        reportStateEntities = tx.select(STATE_REPORT_DE_IDENTIFIER, filter, 0, pageSize);

        // Create the complete states from each entity retrieved.
        reportIids = new ArrayList<>(reportStateEntities.size());
        for (T8DataEntity reportStateEntity : reportStateEntities)
        {
            reportIids.add((String)reportStateEntity.getFieldValue(F_REPORT_IID));
        }

        // Return the retrieved report states.
        return reportIids;
    }

    public void insertHistoryEntry(T8DataTransaction tx, T8ReportState state) throws Exception
    {
        if (persistenceEnabled)
        {
            Map<String, Object> keyFields;
            T8DataEntity entity;

            // Create a new state entity.
            entity = createHistoryEntity(tx, state);

            // Insert the entity.
            tx.insert(entity);

            // Insert the Process History parameters.
            keyFields = new HashMap<>();
            keyFields.put(HISTORY_REPORT_PAR_DE_IDENTIFIER + F_REPORT_IID, state.getIid());
            historyParameterPersistenceHandler.insertParameters(tx, keyFields, null, T8IdentifierUtilities.stripNamespace(state.getParameters()));
        }
    }

    public void updateHistoryEntry(T8DataTransaction tx, T8ReportState state) throws Exception
    {
        if (persistenceEnabled)
        {
            T8DataEntity entity;

            // Create a new state entity.
            entity = createHistoryEntity(tx, state);

            // Insert the entity.
            tx.update(entity);
        }
    }

    private T8ReportState constructReportState(T8DataEntity reportEntity)
    {
        T8ReportState reportState;

        // Construct the new report state.
        reportState = new T8ReportState((String)reportEntity.getFieldValue(F_PROJECT_ID), (String)reportEntity.getFieldValue(F_REPORT_ID), (String)reportEntity.getFieldValue(F_REPORT_IID));
        reportState.setName((String)reportEntity.getFieldValue(F_NAME));
        reportState.setDescription((String)reportEntity.getFieldValue(F_DESCRIPTION));
        reportState.setUserId((String)reportEntity.getFieldValue(F_USER_ID));
        reportState.setInitiatorId((String)reportEntity.getFieldValue(F_INITIATOR_ID));
        reportState.setInitiatorIid((String)reportEntity.getFieldValue(F_INITIATOR_IID));
        reportState.setStartTime((T8Timestamp)reportEntity.getFieldValue(F_GENERATION_START_TIME));
        reportState.setEndTime((T8Timestamp)reportEntity.getFieldValue(F_GENERATION_END_TIME));
        reportState.setFileContextId((String)reportEntity.getFieldValue(F_FILE_CONTEXT_ID));
        reportState.setFilePath((String)reportEntity.getFieldValue(F_FILE_PATH));
        return reportState;
    }

    private T8DataEntity createStateEntity(T8DataTransaction tx, T8ReportState state) throws Exception
    {
        T8DataEntity entity;

        // Create a new entity.
        entity = tx.create(STATE_REPORT_DE_IDENTIFIER, null);
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_REPORT_IID, state.getIid());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_REPORT_ID, state.getId());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_PROJECT_ID, state.getProjectId());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_NAME, state.getName());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_DESCRIPTION, state.getDescription());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_USER_ID, state.getUserId());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_INITIATOR_ID, state.getInitiatorId());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_INITIATOR_IID, state.getInitiatorIid());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_GENERATION_START_TIME, state.getStartTime());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_GENERATION_END_TIME, state.getEndTime());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_EXPIRATION_TIME, state.getExpirationTime());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_FILE_CONTEXT_ID, state.getFileContextId());
        entity.setFieldValue(STATE_REPORT_DE_IDENTIFIER + F_FILE_PATH, state.getFilePath());
        return entity;
    }

    private T8DataEntity createHistoryEntity(T8DataTransaction tx, T8ReportState state) throws Exception
    {
        T8DataEntity entity;

        // Create a new entity.
        entity = tx.create(HISTORY_REPORT_DE_IDENTIFIER, null);
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_REPORT_IID, state.getIid());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_REPORT_ID, state.getId());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_PROJECT_ID, state.getProjectId());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_NAME, state.getName());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_DESCRIPTION, state.getDescription());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_STATUS, state.getStatus().toString());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_USER_ID, state.getUserId());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_INITIATOR_ID, state.getInitiatorId());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_INITIATOR_IID, state.getInitiatorIid());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_GENERATION_START_TIME, state.getStartTime());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_GENERATION_END_TIME, state.getEndTime());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_EXPIRATION_TIME, state.getExpirationTime());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_FILE_CONTEXT_ID, state.getFileContextId());
        entity.setFieldValue(HISTORY_REPORT_DE_IDENTIFIER + F_FILE_PATH, state.getFilePath());
        return entity;
    }
}
