package com.pilog.t8.file.context;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.file.T8FileContext;
import com.pilog.t8.definition.file.context.T8ServerFileContextDefinition;
import com.pilog.t8.security.T8Context;
import java.io.File;

/**
 * @author Bouwer du Preez
 */
public class T8ServerFileContext extends T8LocalFileContext implements T8FileContext
{
    private final T8ServerFileContextDefinition definition;
    protected T8Context context;

    public T8ServerFileContext(T8Context context, T8FileManager fileManager, T8ServerFileContextDefinition definition, String fileContextIid)
    {
        super(new File(definition.getFilePath()), fileContextIid, definition.getIdentifier());
        this.definition = definition;
        this.context = context;
    }
}
