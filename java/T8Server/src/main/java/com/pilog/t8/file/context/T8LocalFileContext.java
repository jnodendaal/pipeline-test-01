package com.pilog.t8.file.context;

import com.google.common.base.Strings;
import com.pilog.t8.file.T8FileContext;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.commons.io.RandomAccessStream;
import com.pilog.t8.utilities.files.FileUtilities;
import com.pilog.t8.utilities.files.RandomAccessFileStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author Bouwer du Preez
 */
public abstract class T8LocalFileContext implements T8FileContext
{
    protected String contextIid;
    protected String contextId;
    protected File rootDirectory;

    public T8LocalFileContext(File rootDirectory, String contextIid, String contextId)
    {
        this.contextIid = contextIid;
        this.contextId = contextId;
        this.rootDirectory = rootDirectory;
    }

    @Override
    public String getId()
    {
        return contextId;
    }

    @Override
    public String getIid()
    {
        return contextIid;
    }

    protected String getContextPath()
    {
        return rootDirectory.getAbsolutePath();
    }

    protected File getContextFile()
    {
        return new File(getContextPath());
    }

    protected String getContextRelativePath(String filePath)
    {
        String contextPath;

        contextPath = getContextPath();
        return filePath.substring(contextPath.length());
    }

    @Override
    public File getFile(String filePath) throws Exception
    {
        if (filePath != null)
        {
            if ((!filePath.startsWith("/")) && (!filePath.startsWith("\\")))
            {
                filePath = "/" + filePath;
            }

            return new File(getContextPath() + filePath);
        }
        else return getContextFile();
    }

    @Override
    public boolean fileExists(String filePath) throws Exception
    {
        if (Strings.isNullOrEmpty(filePath)) throw new IllegalArgumentException("Empty file path not allowed as argument.");
        else
        {
            File contextFile;

            contextFile = getFile(filePath);
            return contextFile.exists();
        }
    }

    @Override
    public boolean open() throws Exception
    {
        File contextFile;

        contextFile = getContextFile();
        return (contextFile.mkdirs());
    }

    @Override
    public boolean close() throws Exception
    {
        // Nothing to do here, this method is overridden by other sub-types.
        return true;
    }

    @Override
    public boolean createDirectory(String filePath) throws Exception
    {
        File newFolder;

        newFolder = getFile(filePath);
        return newFolder.mkdirs();
    }

    @Override
    public void renameFile(String filePath, String newFilename) throws Exception, FileNotFoundException
    {
        File targetFile;

        targetFile = getFile(filePath);
        if (targetFile.exists())
        {
            if (filePath != null)
            {
                String newFilePath;
                File newFile;

                newFilePath = filePath.substring(filePath.length() - targetFile.getName().length());
                newFilePath += newFilename;
                newFile = getFile(newFilePath);
                targetFile.renameTo(newFile);
            }
            else
            {
                targetFile.renameTo(getFile(newFilename));
            }
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public boolean deleteFile(String filePath) throws Exception, FileNotFoundException
    {
        File targetFile;

        targetFile = getFile(filePath);
        if (targetFile.exists())
        {
            if (targetFile.isDirectory())
            {
                return FileUtilities.deleteDirectory(targetFile);
            }
            else
            {
                return targetFile.delete();
            }
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public long getFileSize(String filePath) throws Exception, FileNotFoundException
    {
        File targetFile;

        targetFile = getFile(filePath);
        if (targetFile.exists())
        {
            if (!targetFile.isDirectory())
            {
                return targetFile.length();
            }
            else
            {
                return -1;
            }
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public String getMD5Checksum(String filePath) throws Exception, FileNotFoundException
    {
        File targetFile;

        targetFile = getFile(filePath);
        if (targetFile.exists())
        {
            if (!targetFile.isDirectory())
            {
                // Use Apache commons to compute the checksum.
                return DigestUtils.md5Hex(new FileInputStream(filePath));
            }
            else
            {
                throw new RuntimeException("Cannot compute MD5 Checksum for specified path: " + filePath);
            }
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public T8FileDetails getFileDetails(String filePath) throws Exception, FileNotFoundException
    {
        File file;

        file = getFile(filePath);
        if (file.exists())
        {
            String mediaType;
            long fileSize;
            boolean isDirectory;

            isDirectory = file.isDirectory();
            fileSize = file.length();
            filePath = getContextRelativePath(file.getPath());
            mediaType = Files.probeContentType(file.toPath());

            return new T8FileDetails(contextIid, contextId, filePath, fileSize, isDirectory, null, mediaType);
        }
        else throw new FileNotFoundException("File '" + filePath + "' not found in: " + this);
    }

    @Override
    public List<T8FileDetails> getFileList(String directoryPath) throws Exception, FileNotFoundException
    {
        File directory;

        directory = getFile(directoryPath);
        if (directory.isDirectory())
        {
            ArrayList<T8FileDetails> fileDetails;
            File[] fileList;

            fileDetails = new ArrayList<T8FileDetails>();
            fileList = directory.listFiles();
            for (File file : fileList)
            {
                String filePath;
                String mediaType;
                long fileSize;
                boolean isDirectory;

                isDirectory = file.isDirectory();
                fileSize = file.length();
                filePath = getContextRelativePath(file.getPath());
                mediaType = Files.probeContentType(file.toPath());

                fileDetails.add(new T8FileDetails(contextIid, contextId, filePath, fileSize, isDirectory, null, mediaType));
            }

            return fileDetails;
        }
        else throw new FileNotFoundException("Directory '" + directoryPath + "' not found in: " + this);
    }

    @Override
    public InputStream getFileInputStream(String filePath) throws Exception, FileNotFoundException
    {
        File inputFile;

        inputFile = getFile(filePath);
        if ((inputFile.exists()) && (inputFile.isFile()))
        {
            InputStream inputStream;

            inputStream = new BufferedInputStream(new FileInputStream(inputFile));
            return inputStream;
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public OutputStream getFileOutputStream(String filePath, boolean append) throws Exception
    {
        OutputStream outputStream;
        File outputFile;

        outputFile = getFile(filePath);

        // If the file directory does not exist, then create it
        if (!outputFile.exists())
        {
            Path pathToFile;

            // We want to ensure that any directory structure that doesn't exist is created
            pathToFile = Paths.get(outputFile.getAbsolutePath());
            Files.createDirectories(pathToFile.getParent());
        }

        outputStream = new BufferedOutputStream(new FileOutputStream(outputFile, append));
        return outputStream;
    }

    @Override
    public RandomAccessStream getFileRandomAccessStream(String filePath) throws Exception, FileNotFoundException
    {
        File inputFile;

        inputFile = getFile(filePath);
        if (inputFile.exists() || inputFile.createNewFile())
        {
            RandomAccessStream fileStream;

            fileStream = new RandomAccessFileStream(inputFile);
            return fileStream;
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public String toString()
    {
        StringBuffer buffer;

        buffer = new StringBuffer();
        buffer.append("[FileContext:");
        buffer.append(contextId);
        buffer.append(":");
        buffer.append(contextIid);
        buffer.append("]");
        return buffer.toString();
    }
}
