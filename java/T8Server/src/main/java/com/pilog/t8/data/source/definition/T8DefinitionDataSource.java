package com.pilog.t8.data.source.definition;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.datatype.T8DtBoolean;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtDate;
import com.pilog.t8.datatype.T8DtDouble;
import com.pilog.t8.datatype.T8DtFloat;
import com.pilog.t8.datatype.T8DtInteger;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtLong;
import com.pilog.t8.datatype.T8DtTime;
import com.pilog.t8.datatype.T8DtTimestamp;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionDatumType;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.entity.T8DataEntityGenerator;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.definition.T8DefinitionDataSourceDefinition;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.joda.time.DateTime;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionDataSource implements T8DataSource
{
    private final T8Context context;
    private final T8DefinitionDataSourceDefinition definition;
    private final T8DataTransaction dataAccessProvider;
    private final T8DefinitionManager definitionManager;
    protected T8PerformanceStatistics stats;
    private final String projectId;

    public static final String IDENTIFIER_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "IDENTIFIER";
    public static final String DISPLAY_NAME_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "DISPLAY_NAME";
    public static final String DESCRIPTION_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "DESCRIPTION";
    public static final String DEFINITION_PROJECT_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "DEFINITION_PROJECT";
    public static final String CREATED_BY_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "CREATED_BY";
    public static final String CREATED_AT_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "CREATED_AT";
    public static final String UPDATED_BY_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "UPDATED_BY";
    public static final String UPDATED_AT_FIELD_IDENTIFIER = T8Definition.getLocalIdPrefix() + "UPDATED_AT";

    public T8DefinitionDataSource(T8DefinitionDataSourceDefinition definition, T8DataTransaction tx)
    {
        this.definition = definition;
        this.context = tx.getContext();
        this.dataAccessProvider = tx;
        this.definitionManager = tx.getDataSession().getDefinitionManager();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
        this.projectId = definition.getRootProjectId();
    }

    @Override
    public T8DefinitionDataSourceDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    @Override
    public void open() throws Exception
    {
    }

    @Override
    public void close() throws Exception
    {
    }

    @Override
    public void commit() throws Exception
    {
    }

    @Override
    public void rollback() throws Exception
    {
    }

    @Override
    public void setParameters(Map<String, Object> parameters)
    {
    }

    @Override
    public List<T8DataSourceFieldDefinition> retrieveFieldDefinitions() throws Exception
    {
        List<T8Definition> definitions;

        definitions = definitionManager.getInitializedGroupDefinitions(context, definition.getRootProjectId(), definition.getDefinitionGroupIdentifier(), null);
        if (definitions.size() > 0)
        {
            if(Strings.isNullOrEmpty(definition.getDefinitionTypeIdentifier())) return T8DataEntityGenerator.createDatumDataSourceFieldDefinitions(definitions.get(0).getDatumTypes());

            for (T8Definition t8Definition : definitions)
            {
                if(t8Definition.getMetaData().getTypeId().equals(definition.getDefinitionTypeIdentifier())) return T8DataEntityGenerator.createDatumDataSourceFieldDefinitions(t8Definition.getDatumTypes());
            }
        }

        return null;
    }

    @Override
    public int count(T8DataFilter filter) throws Exception
    {
        if(filter == null)
        {
            if(Strings.isNullOrEmpty(definition.getDefinitionTypeIdentifier()))
            {
                return definitionManager.getRawGroupDefinitions(context, definition.getRootProjectId(), definition.getDefinitionGroupIdentifier()).size();
            }
            else
            {
                return definitionManager.getTypeDefinitionMetaData(definition.getRootProjectId(), definition.getDefinitionTypeIdentifier()).size();
            }
        }
        else return select(filter.getEntityIdentifier(), filter).size();
    }

    @Override
    public boolean exists(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return count(new T8DataFilter(entityIdentifier, keyMap)) > 0;
    }

    @Override
    public void insert(T8DataEntity dataEntity) throws Exception
    {
    }

    @Override
    public void insert(List<T8DataEntity> dataEntity) throws Exception
    {
    }

    @Override
    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> keyValues) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        T8Definition sourceDefinition;
        String identifier;

        identifier = (String)keyValues.get(entityIdentifier + IDENTIFIER_FIELD_IDENTIFIER);
        entityDefinition = dataAccessProvider.getDataEntityDefinition(entityIdentifier);
        sourceDefinition = definitionManager.getInitializedDefinition(context, projectId, identifier, null);
        if (sourceDefinition != null)
        {
            return createEntity(entityDefinition, sourceDefinition);
        }
        else return null;
    }

    @Override
    public boolean update(T8DataEntity dataEntity) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        T8Definition sourceDefinition;
        String identifier;

        identifier = (String)dataEntity.getFieldValue(dataEntity.getIdentifier() + IDENTIFIER_FIELD_IDENTIFIER);
        entityDefinition = dataEntity.getDefinition();
        sourceDefinition = definitionManager.getInitializedDefinition(context, projectId, identifier, null);

        for (T8DataFieldDefinition fieldDefinition : entityDefinition.getFieldDefinitions())
        {
            //Make sure we only update datums
            if(!fieldDefinition.getIdentifier().equals(IDENTIFIER_FIELD_IDENTIFIER)
                    && !fieldDefinition.getIdentifier().equals(DISPLAY_NAME_FIELD_IDENTIFIER)
                    && !fieldDefinition.getIdentifier().equals(DESCRIPTION_FIELD_IDENTIFIER)
                    && !fieldDefinition.getIdentifier().equals(CREATED_AT_FIELD_IDENTIFIER)
                    && !fieldDefinition.getIdentifier().equals(CREATED_BY_FIELD_IDENTIFIER)
                    && !fieldDefinition.getIdentifier().equals(UPDATED_AT_FIELD_IDENTIFIER)
                    && !fieldDefinition.getIdentifier().equals(UPDATED_BY_FIELD_IDENTIFIER)
                    && !fieldDefinition.getIdentifier().equals(DEFINITION_PROJECT_FIELD_IDENTIFIER))
            {
               String datumIdentifier;
               Object fieldDatum;

               datumIdentifier = fieldDefinition.getSourceIdentifier();
               datumIdentifier = definition.mapFieldToSourceIdentifier(datumIdentifier);
               fieldDatum = dataEntity.getFieldValue(fieldDefinition.getPublicIdentifier());
               if (fieldDatum != null)
               {
                   //Check that the object is not an empty string, if it is then parse the value as null
                   if(fieldDatum instanceof String && Strings.isNullOrEmpty((String)fieldDatum))
                   {
                       fieldDatum = null;
                   }
                   else
                   {
                       T8DataType fieldDataType;

                       fieldDataType = fieldDefinition.getDataType();
                        //Ensure we cast the object to the correct type
                        switch(fieldDataType.getDataTypeIdentifier())
                        {
                            case T8DtBoolean.IDENTIFIER:
                            {
                                if(fieldDatum instanceof String)
                                 fieldDatum = Boolean.valueOf((String)fieldDatum);
                                break;
                            }
                            case T8DtDate.IDENTIFIER:
                            {
                                if(fieldDatum instanceof String)
                                 fieldDatum = DateTime.parse((String)fieldDatum);
                                break;
                            }
                            case  T8DtDouble.IDENTIFIER:
                            {
                                if(fieldDatum instanceof String)
                                     fieldDatum = Double.valueOf((String)fieldDatum);
                                break;
                            }
                            case T8DtFloat.IDENTIFIER:
                            {
                                if(fieldDatum instanceof String)
                                     fieldDatum = Float.valueOf((String)fieldDatum);
                                break;
                            }
                            case T8DtInteger.IDENTIFIER:
                            {
                                if(fieldDatum instanceof String)
                                     fieldDatum = Integer.valueOf((String)fieldDatum);
                                break;
                            }
                            case T8DtLong.IDENTIFIER:
                            {
                                if(fieldDatum instanceof String)
                                     fieldDatum = Long.valueOf((String)fieldDatum);
                                break;
                            }
                            case T8DtList.IDENTIFIER:
                            {
                                 List datumList;
                                 String datumListString;

                                 datumList = new ArrayList();
                                 datumListString = (String) fieldDatum;
                                 for (StringTokenizer stringTokenizer = new StringTokenizer(datumListString, "|"); stringTokenizer.hasMoreTokens();)
                                 {
                                     String token = stringTokenizer.nextToken();
                                     datumList.add(token.replaceAll("\\[\\]", ""));
                                 }
                                 fieldDatum = datumList;
                                 break;
                            }
                            case T8DtTime.IDENTIFIER:
                            {
                                if(fieldDatum instanceof String)
                                 fieldDatum = DateTime.parse((String)fieldDatum);
                                break;
                            }
                            case T8DtTimestamp.IDENTIFIER:
                            {
                                if(fieldDatum instanceof String)
                                 fieldDatum = DateTime.parse((String)fieldDatum);
                                break;
                            }
                            default:
                            {
                                //Do not modify the field value
                            }
                        }
                   }
               }

               sourceDefinition.setDefinitionDatum(datumIdentifier, fieldDatum);
            }
        }
        definitionManager.saveDefinition(context, sourceDefinition, null);
        return true;
    }

    @Override
    public boolean update(List<T8DataEntity> dataEntity) throws Exception
    {
        for (T8DataEntity t8DataEntity : dataEntity)
        {
            if(!update(t8DataEntity)) return false;
        }
        return true;
    }

    @Override
    public int update(String entityIdentifier, Map<String, Object> updatedValues, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean delete(T8DataEntity dataEntity) throws Exception
    {
        return false;
    }

    @Override
    public int delete(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return select(entityIdentifier, keyMap != null ? new T8DataFilter(entityIdentifier, keyMap) : null);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        ArrayList<T8DataEntity> entities;
        List<T8Definition> sourceDefinitions;

        entities = new ArrayList<>();
        entityDefinition = dataAccessProvider.getDataEntityDefinition(entityIdentifier);
        sourceDefinitions = definitionManager.getInitializedGroupDefinitions(context, definition.getRootProjectId(), definition.getDefinitionGroupIdentifier(), null);
        for (T8Definition sourceDefinition : sourceDefinitions)
        {
            if(Strings.isNullOrEmpty(definition.getDefinitionTypeIdentifier()) || sourceDefinition.getTypeMetaData().getTypeId().equals(definition.getDefinitionTypeIdentifier()))
            {
                T8DataEntity definitionEntity;

                definitionEntity = createEntity(entityDefinition, sourceDefinition);
                if ((filter == null) || (filter.includesEntity(definitionEntity)))
                {
                    entities.add(definitionEntity);
                }
            }
        }

        if (filter != null)
        {
            Collections.sort(entities, new DefinitionEntityComperator(filter));
        }

        return entities;
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap, int offset, int pageSize) throws Exception
    {
        return select(entityIdentifier, keyMap != null ? new T8DataFilter(entityIdentifier, keyMap) : null, offset, pageSize);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter, int offset, int pageSize) throws Exception
    {
        List<T8DataEntity> entities;
        int entityCount;

        entities = select(entityIdentifier, filter);
        entityCount = entities.size();

        if (entityCount > offset)
        {
            int endIndex;

            endIndex = offset + pageSize;
            if (endIndex > entityCount) endIndex = entityCount;
            return new ArrayList<>(entities.subList(offset, endIndex));
        }
        else return new ArrayList<>();
    }

    @Override
    public T8DataEntityResults scroll(String entityIdentifier, T8DataFilter tdf) throws Exception
    {
        return new T8ScrollableDefinitionResults(entityIdentifier, select(entityIdentifier, tdf));
    }

    private T8DataEntity createEntity(T8DataEntityDefinition entityDefinition, T8Definition sourceDefinition) throws Exception
    {
        T8DefinitionDatumType fieldDataType;
        T8DataEntity entity;
        String entityIdentifier;

        entityIdentifier = entityDefinition.getIdentifier();
        entity = entityDefinition.getNewDataEntityInstance();
        for (T8DataFieldDefinition fieldDefinition : entityDefinition.getFieldDefinitions())
        {
            String datumIdentifier;
            Object fieldDatum;

            datumIdentifier = fieldDefinition.getSourceIdentifier();
            datumIdentifier = definition.mapFieldToSourceIdentifier(datumIdentifier);
            fieldDatum = sourceDefinition.getDefinitionDatum(datumIdentifier);

            if (fieldDatum != null)
            {
                fieldDataType = sourceDefinition.getDatumType(datumIdentifier);

                if ((fieldDataType.equals(T8DataType.TIMESTAMP)) || ((fieldDataType.equals(T8DataType.DATE_TIME))))
                {
                    // DateTime and Timestamp values are stored as Long values in definitions
                    fieldDatum = T8Timestamp.fromTime((Long)fieldDatum);
                }
                else if (fieldDatum instanceof List)
                {
                    List datumList;

                    datumList = (List)fieldDatum;
                    if (datumList.size() > 0)
                    {
                        StringBuffer value;
                        Iterator listIterator;

                        listIterator = datumList.iterator();
                        value = new StringBuffer();
                        while (listIterator.hasNext())
                        {
                            Object listValue;

                            listValue = listIterator.next();
                            value.append("[");
                            if (listValue != null) value.append(listValue.toString());
                            value.append("]");
                            if (listIterator.hasNext()) value.append("|");
                        }

                        fieldDatum = value.toString();
                    }
                    else fieldDatum = null;
                }
            }

            entity.setFieldValue(fieldDefinition.getPublicIdentifier(), fieldDatum);
        }

        if(entity.containsField(entityIdentifier + IDENTIFIER_FIELD_IDENTIFIER)) entity.setFieldValue(entityIdentifier + IDENTIFIER_FIELD_IDENTIFIER, sourceDefinition.getIdentifier());
        if(entity.containsField(entityIdentifier + DISPLAY_NAME_FIELD_IDENTIFIER)) entity.setFieldValue(entityIdentifier + DISPLAY_NAME_FIELD_IDENTIFIER, sourceDefinition.getMetaDisplayName());
        if(entity.containsField(entityIdentifier + DESCRIPTION_FIELD_IDENTIFIER)) entity.setFieldValue(entityIdentifier + DESCRIPTION_FIELD_IDENTIFIER, sourceDefinition.getMetaDescription());
        if(entity.containsField(entityIdentifier + DEFINITION_PROJECT_FIELD_IDENTIFIER)) entity.setFieldValue(entityIdentifier + DEFINITION_PROJECT_FIELD_IDENTIFIER, sourceDefinition.getProjectIdentifier());
        if(entity.containsField(entityIdentifier + CREATED_AT_FIELD_IDENTIFIER)) entity.setFieldValue(entityIdentifier + CREATED_AT_FIELD_IDENTIFIER, T8Timestamp.fromTime(sourceDefinition.getCreatedTime()));
        if(entity.containsField(entityIdentifier + CREATED_BY_FIELD_IDENTIFIER)) entity.setFieldValue(entityIdentifier + CREATED_BY_FIELD_IDENTIFIER, sourceDefinition.getCreatedUserIdentifier());
        if(entity.containsField(entityIdentifier + UPDATED_AT_FIELD_IDENTIFIER)) entity.setFieldValue(entityIdentifier + UPDATED_AT_FIELD_IDENTIFIER, T8Timestamp.fromTime(sourceDefinition.getUpdatedTime()));
        if(entity.containsField(entityIdentifier + UPDATED_BY_FIELD_IDENTIFIER)) entity.setFieldValue(entityIdentifier + UPDATED_BY_FIELD_IDENTIFIER, sourceDefinition.getUpdatedUserIdentifier());
        return entity;
    }

    @Override
    public int delete(List<T8DataEntity> dataEntities) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
