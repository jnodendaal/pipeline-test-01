package com.pilog.t8.data.source;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataEntityResults.StreamType;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.time.T8Date;
import com.pilog.t8.time.T8Time;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.utilities.codecs.HexCodec;
import com.pilog.t8.utilities.collections.SSortedArrayList;
import com.pilog.t8.utilities.sql.BatchParameterizedString;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.pilog.t8.security.T8Context;
import java.sql.Date;
import java.sql.Time;

/**
 * @author Bouwer du Preez
 */
public class T8CommonStatementHandler
{
    private static final T8Logger LOGGER = com.pilog.t8.T8Log.getLogger(T8CommonStatementHandler.class.getName());
    private static final int MAXIMUM_FETCH_SIZE = 1001;

    /**
     * Executes the specified query and returns the resultant count.
     * @param context The session within which this operation is executed.
     * @param connection The connection to use for the count operation.
     * @param queryString The query string to use for the count operation.
     * @param dataSourceDefinition Definition of the data source on which the count operation is being executed.
     * @param entityDefinition Definition of the entity to use for the count operation.
     * @param queryTimeout Sets the number of seconds the driver will wait for a Statement object
     * to execute to the given number of seconds. By default there is no limit on the amount of time
     * allowed for a running statement to complete. Zero means there is no limit.
     * @return The resultant count.
     * @throws SQLException
     * @throws Exception
     */
    public static final int executeCountQuery(T8Context context, Connection connection, ParameterizedString queryString, T8DataSourceDefinition dataSourceDefinition, T8DataEntityDefinition entityDefinition, int queryTimeout) throws SQLException, Exception
    {
        PreparedStatement prepStatement = null;
        ResultSet resultSet = null;

        try
        {
            // Create the prepared statement and set all of its paramters.
            prepStatement = createPreparedStatement(connection, queryString);
            prepStatement.setQueryTimeout(queryTimeout);

            // Execute the statement and get the results.
            resultSet = prepStatement.executeQuery();

            // Return the count if available;
            if (resultSet.next())
            {
                return resultSet.getInt(1);
            }
            else return -1;
        }
        catch (Exception e)
        {
            LOGGER.log("Context: " + context + " Data Source: " + dataSourceDefinition + " Exception while executing query: " + queryString, e);
            throw e;
        }
        finally
        {
            closeJDBCObjects(resultSet, prepStatement);
        }
    }

    private static PreparedStatement createPreparedStatement(Connection connection, ParameterizedString queryString) throws Exception
    {
        PreparedStatement prepStatement;
        List<Object> parameters;

        // Create the prepared statement and set all of its paramters.
        prepStatement = connection.prepareStatement(queryString.getStringValue().toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        parameters = queryString.getParameterValues();
        for (int parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex++)
        {
            setPreparedStatementObject(prepStatement, parameterIndex + 1, parameters.get(parameterIndex));
        }

        // Return prepared statement.
        return prepStatement;
    }

    /**
     * Executes the specified query string and returns the entities retrieved.
     * @param context The session within which this operation is executed.
     * @param connection The connection on which the retrieval will be performed.
     * @param queryString The query string to execute in order to retrieve the entities.
     * @param dataSourceDefinition Definition of the data source used to retrieve the entities.
     * @param entityDefinition Definition of the entities to be retrieved.
     * @param offset The offset of the data page to retrieve.  The offset is zero based.
     * @param pageSize The size of the data page to retrieve.  This value will determine the number of entities in the resultant list.
     * @param queryTimeout Sets the number of seconds the driver will wait for a Statement object
     * to execute to the given number of seconds. By default there is no limit on the amount of time
     * allowed for a running statement to complete. Zero means there is no limit.
     * @return The data entities fetched from the database.
     * @throws SQLException
     * @throws Exception
     */
    public static final ArrayList<T8DataEntity> executeQuery(T8Context context, Connection connection, ParameterizedString queryString, T8DataSourceDefinition dataSourceDefinition, T8DataEntityDefinition entityDefinition, int offset, int pageSize, int queryTimeout) throws SQLException, Exception
    {
        PreparedStatement prepStatement = null;
        ResultSet resultSet = null;
        int effectivePageSize;

        // Convert negative page size value.
        effectivePageSize = pageSize < 0 ? Integer.MAX_VALUE - 1 : pageSize;

        try
        {
            // Create the prepared statement and set all of its paramters.
            prepStatement = createPreparedStatement(connection, queryString);
            prepStatement.setFetchSize((effectivePageSize < MAXIMUM_FETCH_SIZE) && (effectivePageSize >= 0) ? effectivePageSize: MAXIMUM_FETCH_SIZE); // Make sure we don't fetch more data than we need to.
            prepStatement.setQueryTimeout(queryTimeout);

            // Execute the statement and get the results.
            resultSet = prepStatement.executeQuery();

            // Create a collection of data objects from the result set and return it.
            return T8CommonStatementHandler.getResultSetDataEntities(dataSourceDefinition, entityDefinition, resultSet, offset, effectivePageSize);
        }
        catch (Exception e)
        {
            LOGGER.log("Context: " + context + " Data Source: " + dataSourceDefinition + " Exception while executing query: " + queryString, e);
            throw e;
        }
        finally
        {
            closeJDBCObjects(resultSet, prepStatement);
        }
    }

    public static List<T8DataEntity> executePostOrderingQuery(T8Context context, Connection connection, ParameterizedString queryString, T8DataSourceDefinition dataSourceDefinition, T8DataEntityDefinition entityDefinition, PostOrderingComparator orderingComparator, int offset, int pageSize) throws SQLException, Exception
    {
        // GBO - Side-note: The post ordering method used here is not the simplest,
        // which does mean some performance advatage, however, it is not the most
        // complex either, which does result in some performance loss. The ideal
        // would be to compare the next result with existing entity results
        // before it is converted to a data entity. This ideal however might not
        // be possible due to type conversion done during the switch to a data entity.

        SSortedArrayList<T8DataEntity> sortedEntityList;
        PreparedStatement prepStatement = null;
        ResultSet resultSet = null;
        T8DataEntity nextRow;

        try
        {
            // Create the prepared statement and set all of its parameters
            // We can't set the fetch size, since we need to work through the entire result set
            prepStatement = createPreparedStatement(connection, queryString);

            // Execute the statement and get the results
            resultSet = prepStatement.executeQuery();

            // Now we complete the ordering by keeping a sorted list with a no-grow policy
            sortedEntityList = new SSortedArrayList<>(orderingComparator, (offset+pageSize));
            while ((nextRow = getNextResultSetDataEntity(dataSourceDefinition, entityDefinition, resultSet, null)) != null)
            {
                sortedEntityList.add(nextRow);
            }

            // Now we need to ensure we only return the actual page requested
            if (sortedEntityList.size() < offset) return new ArrayList<>();
            else if (sortedEntityList.size() < (offset+pageSize))
            {
                return new ArrayList<>(sortedEntityList.subList(offset, sortedEntityList.size()));
            } else return new ArrayList<>(sortedEntityList.subList(offset, offset+pageSize));
        }
        catch (Exception ex)
        {
            LOGGER.log(() -> "Context: " + context + " Data Source: " + dataSourceDefinition + " Exception while executing query: " + queryString, ex);
            throw ex;
        }
        finally
        {
            closeJDBCObjects(resultSet, prepStatement);
        }
    }

    public static final T8DataEntityResults getEntityResults(T8Context context, Connection connection, ParameterizedString queryString, T8DataSourceDefinition dataSourceDefinition, T8DataEntityDefinition entityDefinition) throws SQLException, Exception
    {
        PreparedStatement prepStatement = null;
        ResultSet resultSet = null;

        try
        {
            List<Object> parameters;

            // Create the prepared statement and set all of its paramters.
            prepStatement = connection.prepareStatement(queryString.getStringValue().toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            parameters = queryString.getParameterValues();
            for (int parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex++)
            {
                setPreparedStatementObject(prepStatement, parameterIndex +1 , parameters.get(parameterIndex));
            }

            // Execute the statement and get the results.
            return new T8ScrollableEntityResults(dataSourceDefinition, entityDefinition, prepStatement, prepStatement.executeQuery());
        }
        catch (Exception e)
        {
            closeJDBCObjects(resultSet, prepStatement);
            LOGGER.log("Context: " + context + " Data Source: " + dataSourceDefinition + " Exception while executing statement: " + queryString, e);
            throw e;
        }
    }

    public static final int executeUpdate(T8Context context, Connection connection, ParameterizedString statementString, T8DataSourceDefinition dataSourceDefinition) throws SQLException, Exception
    {
        PreparedStatement prepStatement = null;

        try
        {
            List<Object> insertParameters;

            // Create the prepared statement and set all of its paramters.
            insertParameters = statementString.getParameterValues();
            prepStatement = connection.prepareStatement(statementString.getStringValue().toString());
            for (int parameterIndex = 0; parameterIndex < insertParameters.size(); parameterIndex++)
            {
                setPreparedStatementObject(prepStatement, parameterIndex + 1, insertParameters.get(parameterIndex));
            }

            // Execute the statement.
            return prepStatement.executeUpdate();
        }
        catch (Exception e)
        {
            LOGGER.log("Context: " + context + " Data Source: " + dataSourceDefinition + " Exception while executing statement: " + statementString, e);
            throw e;
        }
        finally
        {
            // Close JDBC objects.
            closeJDBCObjects(prepStatement);
        }
    }

    public static final int[] executeUpdate(T8Context context, Connection connection, BatchParameterizedString statementString, T8DataSourceDefinition dataSourceDefinition) throws SQLException, Exception
    {
        //if this statement does not have batch parameters just do a normal update
        if (!statementString.hasBatchParameters())
        {
            return new int[]{executeUpdate(context, connection, (ParameterizedString)statementString, dataSourceDefinition)};
        }

        PreparedStatement prepStatement = null;

        try
        {
            prepStatement = connection.prepareStatement(statementString.getStringValue().toString());
            for (List<Object> insertParameters : statementString.getBatchParameters())
            {
                for (int parameterIndex = 0; parameterIndex < insertParameters.size(); parameterIndex++)
                {
                    setPreparedStatementObject(prepStatement, parameterIndex + 1, insertParameters.get(parameterIndex));
                }
                prepStatement.addBatch();
            }

            // Execute the statement.
            return prepStatement.executeBatch();
        }
        catch (Exception e)
        {
            LOGGER.log("Context: " + context + " Data Source: " + dataSourceDefinition + " Exception while executing statement: " + statementString, e);
            throw e;
        }
        finally
        {
            // Close JDBC objects.
            closeJDBCObjects(prepStatement);
        }
    }

    private static void closeJDBCObjects(AutoCloseable... jdbcObjects) throws SQLException
    {
        for (Object jdbcObject : jdbcObjects)
        {
            if (jdbcObject != null)
            {
                try{((AutoCloseable)jdbcObject).close();}catch(Exception ex){LOGGER.log(ex);}//failed to close
            }
        }
    }

    public static final ArrayList<T8DataSourceFieldDefinition> getQueryFieldDefinitions(T8Context context, T8DataConnection connection, ParameterizedString queryString) throws SQLException
    {
        PreparedStatement prepStatement = null;
        ResultSet resultSet = null;

        try
        {
            List<Object> parameters;

            // Create the prepared statement and set all of its paramters.
            prepStatement = connection.prepareStatement(queryString.getStringValue().toString());
            parameters = queryString.getParameterValues();
            for (int parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex++)
            {
                setPreparedStatementObject(prepStatement, parameterIndex + 1, parameters.get(parameterIndex));
            }

            // Execute the statement and get the results.
            resultSet = prepStatement.executeQuery();

            // Create a collection of data objects from the result set and return it.
            return T8CommonStatementHandler.getResultSetFieldDefinitions(connection, resultSet);
        }
        catch (SQLException e)
        {
            LOGGER.log("Context: " + context + " Exception while executing statement: " + queryString, e);
            throw e;
        }
        finally
        {
            closeJDBCObjects(resultSet, prepStatement);
        }
    }

    private static ArrayList<T8DataSourceFieldDefinition> getResultSetFieldDefinitions(T8DataConnection connection, ResultSet rSet) throws SQLException
    {
        ArrayList<T8DataSourceFieldDefinition> returnList;
        T8DatabaseAdaptor dbAdaptor;
        ResultSetMetaData rSetMeta;
        List<String> primaryKeyList;

        // Get the database adaptor.
        dbAdaptor = connection.getDatabaseAdaptor();

        // Get the Result Set's meta data.
        rSetMeta = rSet.getMetaData();

        // Pack the retrieved data rows into the ArrayList<Map>.
        primaryKeyList = null;
        returnList = new ArrayList<T8DataSourceFieldDefinition>();
        for (int columnIndex = 0; columnIndex < rSetMeta.getColumnCount(); columnIndex++)
        {
            T8DataSourceFieldDefinition fieldDefinition;
            T8DataType fieldDataType;
            String fieldIdentifier;
            String sourceIdentifier;
            String tableName;
            String columnName;
            int columnType;
            int width;
            int scale;
            int precision;
            boolean nullable;

            // Get the data value from the ResultSet.
            columnName = rSetMeta.getColumnName(columnIndex + 1);
            sourceIdentifier = columnName.toUpperCase();
            fieldIdentifier = T8Definition.getLocalIdPrefix() + sourceIdentifier;
            columnType = rSetMeta.getColumnType(columnIndex + 1);
            nullable = (rSetMeta.isNullable(columnIndex + 1) == ResultSetMetaData.columnNullable);
            width = rSetMeta.getColumnDisplaySize(columnIndex + 1);
            scale = rSetMeta.getScale(columnIndex + 1);
            precision = rSetMeta.getPrecision(columnIndex + 1);
            tableName = rSetMeta.getTableName(columnIndex + 1);
            if (primaryKeyList == null) primaryKeyList = getPrimaryKeys(connection, tableName);

            fieldDataType = dbAdaptor.getDataType(columnType, width);
            fieldDefinition = new T8DataSourceFieldDefinition(fieldIdentifier);
            fieldDefinition.setSourceIdentifier(sourceIdentifier);
            fieldDefinition.setDataType(fieldDataType);
            fieldDefinition.setKey(primaryKeyList != null && primaryKeyList.contains(columnName));
            fieldDefinition.setRequired(!nullable);
            returnList.add(fieldDefinition);
        }

        return returnList;
    }

    private static List<String> getPrimaryKeys(Connection connection, String tableName) throws SQLException
    {
        ResultSet results = null;
        DatabaseMetaData dbMeta;
        List<String> primaryKeyList;

        dbMeta = connection.getMetaData();
        primaryKeyList = new ArrayList<String>();

        try
        {
            results = dbMeta.getPrimaryKeys(null, null, tableName);
            while (results.next())
            {
                primaryKeyList.add(results.getString("COLUMN_NAME"));
            }

            System.out.println("Primary keys (" + tableName + "): " + primaryKeyList);
            return primaryKeyList;
        }
        finally
        {
            if (results != null) results.close();
        }
    }

    /**
     * Builds a list of {@code T8DataEntity} objects representing the results
     * retrieved from the database for the executed query.<br/>
     * <br/>
     * Unless an error occurs, at the very least an empty list will be returned
     * to the calling method.
     *
     * @param dataSourceDefinition The {@code T8DataSourceDefinition} which
     *      defines the connection and associated database details for the
     *      executing query
     * @param entityDefinition The {@code T8EntityDefinition} which defines the
     *      details of the entities to be returned
     * @param rSet The {@code ResultSet} containing the actual results retrieved
     *      from the database
     * @param pageOffset The {@code int} offset at which the data retrieved
     *      should be processed from
     * @param pageSize The {@code int} number of rows to extract from the
     *      supplied result set, starting from the specified offset.  A negative
     *      value or zero will result in an empty output list.
     *
     * @return The {@code ArrayList} of data entities representing the result
     *      set retrieved from the data source
     *
     * @throws SQLException If there is any error during the processing of the
     *      result set
     * @throws Exception If any other error occurs during the conversion of data
     *      from the result set to the set of data entities
     */
    public static ArrayList<T8DataEntity> getResultSetDataEntities(T8DataSourceDefinition dataSourceDefinition, T8DataEntityDefinition entityDefinition, ResultSet rSet, int pageOffset, int pageSize) throws SQLException, Exception
    {
        ArrayList<T8DataEntity> returnList;
        T8DataEntity nextEntity;

        // Move the cursor to the start of the page to retrieve.  The underlying
        // result set is not zero-based, but starts at one.  The offset
        // parameter of this method is zero-based and therefore we need to make
        // the proper adjustments by moving the cursor on the resultset to the
        // index before the one we require so that the first call to .next()
        // will return the requested first row of the page.
        if (pageOffset > 0) rSet.absolute(pageOffset);

        // Pack the retrieved data rows into the ArrayList<Map>.
        returnList = new ArrayList<>();
        while ((returnList.size() < pageSize) && ((nextEntity = getNextResultSetDataEntity(dataSourceDefinition, entityDefinition, rSet)) != null))
        {
            returnList.add(nextEntity);
        }

        return returnList;
    }

    public static T8DataEntity getNextResultSetDataEntity(T8DataSourceDefinition dataSourceDefinition, T8DataEntityDefinition entityDefinition, ResultSet rSet) throws SQLException, Exception
    {
        return getNextResultSetDataEntity(dataSourceDefinition, entityDefinition, rSet, null);
    }

    public static T8DataEntity getNextResultSetDataEntity(T8DataSourceDefinition dataSourceDefinition, T8DataEntityDefinition entityDefinition, ResultSet rSet, Map<String, StreamType> entityFieldStreamTypes) throws SQLException, Exception
    {
        ResultSetMetaData rSetMeta;

        // Get the Result Set's meta data.
        rSetMeta = rSet.getMetaData();

        // Pack the retrieved data rows into the ArrayList<Map>.
        if (rSet.next())
        {
            T8DataEntity newEntity;

            newEntity = new T8DataEntity(entityDefinition);
            for (int columnIndex = 0; columnIndex < rSetMeta.getColumnCount(); columnIndex++)
            {
                T8DataSourceFieldDefinition sourceFieldDefinition;
                T8DataType fieldDataType;
                String sourceIdentifier;
                String dataSourceFieldIdentifier;
                String entityFieldIdentifier;

                // Get the identifier of the source, the data source field as well as the entity field to which the data source is mapped.
                sourceIdentifier = rSetMeta.getColumnName(columnIndex + 1).toUpperCase();
                dataSourceFieldIdentifier = dataSourceDefinition.mapSourceToFieldIdentifier(sourceIdentifier);
                entityFieldIdentifier = entityDefinition.mapSourceToFieldIdentifier(dataSourceFieldIdentifier);

                // Get the source field definition and data type.
                sourceFieldDefinition = dataSourceDefinition.getFieldDefinition(dataSourceFieldIdentifier);
                fieldDataType = sourceFieldDefinition != null ? sourceFieldDefinition.getDataType() : null;

                // Only fetch the value from the result set if we have a valid mapped entity field identifier.
                if (entityFieldIdentifier != null)
                {
                    Object dataValue;
                    int columnType;

                    // Get the data type from the ResultSet.
                    columnType = rSetMeta.getColumnType(columnIndex + 1);

                    // Do some special conversion for time types.
                    if (fieldDataType.equals(T8DataType.DATE))
                    {
                        Date date;

                        // We always use getTimeStamp to ensure that no data is lost in the conversion.
                        date = rSet.getDate(columnIndex + 1);
                        dataValue = date != null ? new T8Date(date.getTime()) : null;
                    }
                    else if (fieldDataType.equals(T8DataType.TIME))
                    {
                        Time time;

                        // We always use getTimeStamp to ensure that no data is lost in the conversion.
                        time = rSet.getTime(columnIndex + 1);
                        dataValue = time != null ? new T8Time(time.getTime()) : null;
                    }
                    else if ((fieldDataType.equals(T8DataType.TIMESTAMP)) || ((fieldDataType.equals(T8DataType.DATE_TIME))))
                    {
                        Timestamp timestamp;

                        // We always use getTimeStamp to ensure that no data is lost in the conversion.
                        timestamp = rSet.getTimestamp(columnIndex + 1);
                        dataValue = timestamp != null? new T8Timestamp(timestamp.getTime()) : null;
                    }
                    else if ((columnType == java.sql.Types.BLOB) || (columnType == java.sql.Types.LONGVARBINARY))
                    {
                        java.sql.Blob blob;

                        blob = (java.sql.Blob)rSet.getBlob(columnIndex + 1);
                        if ((entityFieldStreamTypes != null) && (entityFieldStreamTypes.containsKey(entityFieldIdentifier)))
                        {
                            if (entityFieldStreamTypes.get(entityFieldIdentifier) ==  StreamType.INPUT)
                            {
                                //Place the stream in a buffered stream to reduce network IO
                                dataValue = new BufferedInputStream(blob.getBinaryStream(), 1024 * 1024);
                            }
                            else
                            {
                                //Place the stream in a buffered stream to reduce network IO
                                dataValue = new BufferedOutputStream(blob.setBinaryStream(1), 1024 * 1024);
                            }
                        }
                        else dataValue = blob != null ? blob.getBytes(1, (int)blob.length()) : null;
                    }
                    else if ((columnType == java.sql.Types.CLOB) || (columnType == java.sql.Types.LONGVARCHAR))
                    {
                        java.sql.Clob clob;

                        clob = (java.sql.Clob)rSet.getClob(columnIndex + 1);
                        if ((entityFieldStreamTypes != null) && (entityFieldStreamTypes.containsKey(entityFieldIdentifier)))
                        {
                            if (entityFieldStreamTypes.get(entityFieldIdentifier) ==  StreamType.INPUT)
                            {
                                // Place the stream in a buffered stream to reduce network IO.
                                dataValue = new BufferedReader(clob.getCharacterStream(), 1024 * 1024);
                            }
                            else
                            {
                                // Place the stream in a buffered stream to reduce network IO.
                                dataValue = new BufferedWriter(clob.setCharacterStream(1), 1024 * 1024);
                            }
                        }
                        else dataValue = clob != null ? clob.getSubString(1, (int) clob.length()) : null;
                    }
                    else if (columnType == java.sql.Types.CHAR) // Convert GUID char arrays to String representations.
                    {
                        dataValue = rSet.getString(columnIndex + 1);
                        if ((dataValue != null) && (((String)dataValue).length() == 36)) // Length of 36: 16 byte GUID * 2 = 32 hex characters + 4 dash characters (MSSQL).
                        {
                            dataValue = ((String)dataValue).replaceAll("-", "");
                        }
                    }
                    else
                    {
                        dataValue = rSet.getObject(columnIndex + 1);
                    }

                    // Do some conversion of certain types of values depending on the field data type.
                    if (dataValue instanceof byte[]) // Convert GUID byte arrays to String representations.
                    {
                        byte[] byteArray;

                        byteArray = (byte[])dataValue;
                        if ((byteArray != null) && (byteArray.length == 16))
                        {
                            dataValue = HexCodec.convertBytesToHexString(byteArray);
                        }
                    }
                    else if (dataValue instanceof BigDecimal)
                    {
                        if (fieldDataType.equals(T8DataType.LONG))
                        {
                            dataValue = ((BigDecimal)dataValue).longValue();
                        }
                        else if (fieldDataType.equals(T8DataType.INTEGER))
                        {
                            dataValue = ((BigDecimal)dataValue).intValue();
                        }
                        else if (fieldDataType.equals(T8DataType.FLOAT))
                        {
                            dataValue = ((BigDecimal)dataValue).floatValue();
                        }
                        else if (fieldDataType.equals(T8DataType.DOUBLE))
                        {
                            dataValue = ((BigDecimal)dataValue).doubleValue();
                        }
                        else if (fieldDataType.equals(T8DataType.BIG_INTEGER))
                        {
                            dataValue = ((BigDecimal)dataValue).toBigInteger();
                        }
                    }

                    // Pack the data value into the data row.
                    newEntity.setFieldValue(entityFieldIdentifier, dataValue);
                }
            }

            return newEntity;
        }
        else return null;
    }

    public static void setPreparedStatementObject(PreparedStatement preparedStatement, int parameterIndex, Object parameterObject) throws SQLException
    {
        if (parameterObject instanceof T8Time)
        {
            preparedStatement.setTime(parameterIndex, new java.sql.Time(((T8Time)parameterObject).getMilliseconds()));
        }
        else if (parameterObject instanceof T8Timestamp)
        {
            preparedStatement.setTimestamp(parameterIndex, new java.sql.Timestamp(((T8Timestamp)parameterObject).getMilliseconds()));
        }
        else if (parameterObject instanceof T8Date)
        {
            preparedStatement.setDate(parameterIndex, new java.sql.Date(((T8Date)parameterObject).getMilliseconds()));
        }
        else if (DataPersistenceUtilities.isClobValue(parameterObject))
        {
            preparedStatement.setCharacterStream(parameterIndex, new StringReader((String)parameterObject));
        }
        else
        {
            preparedStatement.setObject(parameterIndex, parameterObject);
        }
    }
}
