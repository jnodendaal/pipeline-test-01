package com.pilog.t8.notification;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.notification.T8EPICNotificationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EPICNotificationGenerator implements T8NotificationGenerator
{
    private final T8ServerContext serverContext;
    private final T8EPICNotificationDefinition definition;
    private T8ServerContextScript generatorScript;

    public T8EPICNotificationGenerator(T8Context context, T8EPICNotificationDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.definition = definition;
    }

    @Override
    public List<T8Notification> generateNotifications(T8Context context, Map<String, ? extends Object> inputParameters) throws Exception
    {
        Map<String, ? extends Object> outputParameters;

        // If we've not yet created a generator script, to it now.
        if (generatorScript == null)
        {
            generatorScript = definition.getNewScriptInstance(context);
        }

        // Execute the script to generate the notifications.
        outputParameters = generatorScript.executeScript(inputParameters);
        if (outputParameters != null)
        {
            return (List<T8Notification>)outputParameters.get(definition.getNamespace() + T8EPICNotificationDefinition.PARAMETER_NOTIFICATION_LIST);
        }
        else throw new RuntimeException("Notification generator script return null output: " + definition);
    }
}
