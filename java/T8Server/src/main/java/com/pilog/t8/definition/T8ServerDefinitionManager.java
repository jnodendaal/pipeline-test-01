package com.pilog.t8.definition;

import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.datatype.T8DtString.T8StringType;
import com.pilog.t8.datatype.T8DtUndefined;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8Definition.T8DefinitionStatus;
import com.pilog.t8.definition.T8DefinitionComment.CommentType;
import com.pilog.t8.definition.cache.T8FileDefinitionCache;
import com.pilog.t8.definition.cache.T8ResourceDefinitionCache;
import com.pilog.t8.definition.cache.T8SystemDefinitionCache;
import com.pilog.t8.definition.event.T8DefinitionCacheReloadedEvent;
import com.pilog.t8.definition.event.T8DefinitionDeletedEvent;
import com.pilog.t8.definition.event.T8DefinitionEventHandler;
import com.pilog.t8.definition.event.T8DefinitionImportedEvent;
import com.pilog.t8.definition.event.T8DefinitionManagerListener;
import com.pilog.t8.definition.event.T8DefinitionRenamedEvent;
import com.pilog.t8.definition.event.T8DefinitionSavedEvent;
import com.pilog.t8.definition.filter.T8DefinitionFilter;
import com.pilog.t8.definition.patch.T8DefinitionPatch;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.project.T8ProjectImportReport;
import com.pilog.t8.project.T8ProjectImportReport.T8ProjectImportResult;
import com.pilog.t8.project.T8ProjectPackage;
import com.pilog.t8.system.T8SystemDetails;
import com.pilog.t8.definition.data.type.T8DataTypeDefinition;
import com.pilog.t8.definition.event.T8DefinitionEventHandlerDefinition;
import com.pilog.t8.definition.patch.T8PatchDefinition;
import com.pilog.t8.definition.project.T8ProjectDefinition;
import com.pilog.t8.definition.resolver.T8DefinitionResolverDefinition;
import com.pilog.t8.definition.system.T8SetupDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.definition.user.T8AdministratorUserDefinition;
import com.pilog.t8.file.handler.T8ProjectPackageInputFileHandler;
import com.pilog.t8.file.handler.T8ProjectPackageOutputFileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import com.pilog.t8.utilities.serialization.SerializationUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public class T8ServerDefinitionManager implements T8DefinitionManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerDefinitionManager.class);

    private final T8ServerContext serverContext;
    private T8Context internalContext;
    private T8ConfigurationManager configurationManager;
    private final T8DefinitionSerializer serializer;
    private final T8DefinitionTypeCache definitionTypeCache;
    private final T8DataTypeCache dataTypeCache;
    private final List<T8DefinitionResolver> definitionResolvers;
    private T8SetupDefinition setupDefinition;
    private T8SystemDefinition systemDefinition;
    private final Map<String, T8ProjectDefinition> projectDefinitions;
    private EventListenerList eventListeners;
    private T8FileDefinitionCache rootLevelDefinitionCache;
    private T8SystemDefinitionCache systemLevelDefinitionCache;
    private T8ProjectCacheHandler projectLevelDefinitionCache;
    private T8ResourceDefinitionCache resourceLevelDefinitionCache;
    private final Map<String, T8DefinitionLock> definitionLocks;
    private final List<T8DefinitionPatch> definitionPatches;

    private static final String DEFAULT_ADMINISTRATOR_IDENTIFIER = "@UA_SYSTEM";
    private static final String DEFAULT_ADMINISTRATOR_USER_PROFILE_IDENTIFIER = "@UP_ADMINISTRATOR";

    public T8ServerDefinitionManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.definitionTypeCache = new T8DefaultDefinitionTypeCache(serverContext);
        this.dataTypeCache = new T8DataTypeCache(serverContext, this);
        this.definitionResolvers = new ArrayList<>();
        this.projectDefinitions = new LinkedHashMap<>();
        this.definitionLocks = new HashMap<>();
        this.definitionPatches = new ArrayList<>();
        this.serializer = new T8DefinitionSerializer(this);
    }

    @Override
    public void init()
    {
        // Cache data types.
        this.dataTypeCache.cacheDataTypes();

        // Cache definition types.
        this.definitionTypeCache.cacheDefinitionTypes();

        // Create the session context to be used by the definition manager.
        this.internalContext = serverContext.getSecurityManager().createServerModuleContext(T8ServerDefinitionManager.this);
    }

    @Override
    public void start() throws Exception
    {
        // Fetch the configuration manager.
        configurationManager = serverContext.getConfigurationManager();

        // Initialize all definition event handlers.
        for (String definitionId : getGroupDefinitionIdentifiers(null, T8DefinitionEventHandlerDefinition.GROUP_IDENTIFIER))
        {
            T8DefinitionEventHandlerDefinition eventHandlerDefinition;
            T8DefinitionEventHandler eventHandler;

            eventHandlerDefinition = (T8DefinitionEventHandlerDefinition)getRawDefinition(internalContext, null, definitionId);
            eventHandler = eventHandlerDefinition.getNewEventHandlerInstance(internalContext);
            eventHandler.initHandler();
            addDefinitionManagerListener(eventHandler);
        }
    }

    @Override
    public void destroy()
    {
        destroyDefinitionResolvers();
        destroySystemLevelDefinitionCache();
        projectLevelDefinitionCache.destroy();
        definitionTypeCache.clearCache();
    }

    @Override
    public void addDefinitionManagerListener(T8DefinitionManagerListener listener)
    {
        if (eventListeners == null) eventListeners = new EventListenerList();
        eventListeners.add(T8DefinitionManagerListener.class, listener);
    }

    @Override
    public void removeDefinitionManagerListener(T8DefinitionManagerListener listener)
    {
        if (eventListeners != null) eventListeners.remove(T8DefinitionManagerListener.class, listener);
    }

    @Override
    public List<T8DefinitionPatch> getDefinitionPatches()
    {
        return new ArrayList<>(definitionPatches);
    }

    private void fireDefinitionCacheReloadedEvent()
    {
        if (eventListeners != null)
        {
            T8DefinitionCacheReloadedEvent event;

            event = new T8DefinitionCacheReloadedEvent(this);
            for (T8DefinitionManagerListener listener : eventListeners.getListeners(T8DefinitionManagerListener.class))
            {
                listener.definitionCacheReloaded(event);
            }
        }
    }

    private void fireDefinitionSavedEvent(T8Context context, T8Definition savedDefinition)
    {
        if (eventListeners != null)
        {
            T8DefinitionSavedEvent event;

            event = new T8DefinitionSavedEvent(context, savedDefinition);
            for (T8DefinitionManagerListener listener : eventListeners.getListeners(T8DefinitionManagerListener.class))
            {
                listener.definitionSaved(event);
            }
        }
    }

    private void fireDefinitionDeletedEvent(T8Context context, T8Definition deletedDefinition)
    {
        if (eventListeners != null)
        {
            T8DefinitionDeletedEvent event;

            event = new T8DefinitionDeletedEvent(context, deletedDefinition);
            for (T8DefinitionManagerListener listener : eventListeners.getListeners(T8DefinitionManagerListener.class))
            {
                listener.definitionDeleted(event);
            }
        }
    }

    private void fireDefinitionRenamedEvent(T8Context context, T8Definition renamedDefinition, String oldIdentifier, List<T8DefinitionHandle> affectedDefinitions)
    {
        if (eventListeners != null)
        {
            T8DefinitionRenamedEvent event;

            event = new T8DefinitionRenamedEvent(context, renamedDefinition, oldIdentifier, affectedDefinitions);
            for (T8DefinitionManagerListener listener : eventListeners.getListeners(T8DefinitionManagerListener.class))
            {
                listener.definitionRenamed(event);
            }
        }
    }

    private void fireDefinitionImportedEvent(T8Context context, T8Definition importedDefinition)
    {
        if (eventListeners != null)
        {
            T8DefinitionImportedEvent event;

            event = new T8DefinitionImportedEvent(context, importedDefinition);
            for (T8DefinitionManagerListener listener : eventListeners.getListeners(T8DefinitionManagerListener.class))
            {
                listener.definitionImported(event);
            }
        }
    }

    @Override
    public void loadDefinitionData(T8Context context)
    {
        try
        {
            // Destroy existing definition resolvers.
            destroyDefinitionResolvers();

            // Destroy existing root-level definition caches.
            destroyRootLevelDefinitionCache();

            // Destroy existing resource-level definition caches.
            destroyResourceLevelDefinitionCache();

            // Destroy existing system-level definition caches.
            destroySystemLevelDefinitionCache();

            // Destroy existing project-level definition caches.
            destroyProjectLevelDefinitionCache();

            // Load definition patches.
            cacheDefinitionPatches();

            // Load the setup definition.
            loadSetupDefinition();

            // Load the system definitions first.
            initializeRootLevelDefinitionCache();

            // Set the active system definition
            systemDefinition = (T8SystemDefinition)rootLevelDefinitionCache.loadDefinition(setupDefinition.getStartupSystemIdentifier());

            // Load the project definitions.
            cacheProjectDefinitions();

            // Initialize the resource-level definition caches.
            initializeResourceLevelDefinitionCache();

            // Initialize the project-level definition caches.
            initializeProjectLevelDefinitionCaches();

            // Initialize the system-level definition caches.
            initializeSystemLevelDefinitionCache();

            // Initialize definition resolvers.
            initializeDefinitionResolvers();

            // Check that at least one administrator user definition is registered.
            checkAdministratorUser();

            // Cache data types.
            dataTypeCache.cacheDataTypes(getRawGroupDefinitions(internalContext, null, T8DataTypeDefinition.GROUP_IDENTIFIER));

            // Store all data type definitions as resources.
            for (T8DataTypeDefinition dataTypeDefinition : dataTypeCache.getDataTypeDefinitions())
            {
                resourceLevelDefinitionCache.saveDefinition(dataTypeDefinition);
            }

            // Fire an event to let all listeners know that the cache has been reloaded.
            fireDefinitionCacheReloadedEvent();
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while loading definitions.", e);
        }
    }

    private void loadSetupDefinition() throws Exception
    {
        File contextPath;

        contextPath = serverContext.getContextPath();
        if (!contextPath.exists()) throw new RuntimeException("Context meta-data path not found: " + contextPath.getAbsolutePath());
        else
        {
            File storagePath;

            storagePath = new File(contextPath.getAbsolutePath() + "/system/@T8_SETUP.t8d");
            if (storagePath.exists())
            {
                String definitionString;

                definitionString = T8DefinitionFileHandler.loadDefinition(storagePath);
                setupDefinition = (T8SetupDefinition)serializer.deserializeDefinition(definitionString);
            }
            else
            {
                LOGGER.log("Setup Definition not found - creating default and setting system to: @T8_SYSTEM");
                setupDefinition = new T8SetupDefinition("@T8_SETUP");
                saveSetupDefinition(null, setupDefinition);
            }
        }
    }

    public String getSystemIdentifier()
    {
        return systemDefinition.getIdentifier();
    }

    @Override
    public T8SystemDetails getSystemDetails()
    {
        T8SystemDetails systemDetails;

        systemDetails = new T8SystemDetails(systemDefinition.getIdentifier());
        systemDetails.setDisplayName(systemDefinition.getSystemDisplayName());
        systemDetails.setProductDeveloperIconIdentifier(systemDefinition.getProductDeveloperIconId());
        systemDetails.setProductClientIconIdentifier(systemDefinition.getProductClientIconId());
        systemDetails.setProductConfigurationIconIdentifier(systemDefinition.getProductConfigurationIconId());
        systemDetails.setProductSplashIconIdentifier(systemDefinition.getProductSplashIconId());
        systemDetails.setExternalBinding(systemDefinition.getExternalBinding());
        return systemDetails;
    }

    @Override
    public T8SystemDefinition getSystemDefinition(T8Context context)
    {
        return systemDefinition;
    }

    @Override
    public T8SetupDefinition getSetupDefinition(T8Context context)
    {
        return setupDefinition;
    }

    @Override
    public void saveSetupDefinition(T8Context context, T8SetupDefinition setupDefinition) throws Exception
    {
        File contextPath;

        contextPath = serverContext.getContextPath();
        if (!contextPath.exists()) throw new RuntimeException("Context meta-data path not found: " + contextPath.getAbsolutePath());
        else
        {
            File storagePath;

            storagePath = new File(contextPath.getAbsolutePath() + "/system");
            T8DefinitionFileHandler.saveDefinition(storagePath, "@T8_SETUP", serializer.serializeDefinition(setupDefinition));
            this.setupDefinition = setupDefinition;
        }
    }

    /**
     * Caches all project definition required by the current system.
     * @throws Exception
     */
    private void cacheProjectDefinitions() throws Exception
    {
        for (String projectId : rootLevelDefinitionCache.getGroupDefinitionIdentifiers(T8ProjectDefinition.GROUP_IDENTIFIER))
        {
            try
            {
                T8ProjectDefinition projectDefinition;

                projectDefinition = (T8ProjectDefinition)getRawDefinition(null, null, projectId);
                if (projectDefinition != null)
                {
                    projectDefinitions.put(projectId, projectDefinition);
                }
                else throw new RuntimeException("Project definition required by system not found: " + projectId);
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while caching project definition: " + projectId, e);
            }
        }
    }

    private void cacheDefinitionPatches() throws Exception
    {
        File contextPath;

        contextPath = serverContext.getContextPath();
        if (!contextPath.exists()) throw new RuntimeException("Context meta-data path not found: " + contextPath.getAbsolutePath());
        else
        {
            File storagePath;

            storagePath = new File(contextPath.getAbsolutePath() + T8PatchDefinition.STORAGE_PATH);
            if (storagePath.exists())
            {
                try
                {
                    HashMap<String, String> loadedDefinitions;

                    loadedDefinitions = T8DefinitionFileHandler.loadDefinitions(storagePath);
                    if (loadedDefinitions != null)
                    {
                        // Add the loaded patch definitions to the cache.
                        for (String identifier : loadedDefinitions.keySet())
                        {
                            T8PatchDefinition patchDefinition;
                            String patchDefinitionString;

                            LOGGER.log("Loading definition patch '" + identifier + "'...");
                            patchDefinitionString = (String)loadedDefinitions.get(identifier);
                            patchDefinition = (T8PatchDefinition)serializer.deserializeDefinition(patchDefinitionString);
                            if (patchDefinition.isEnabled())
                            {
                                definitionPatches.add(patchDefinition.getNewPatchInstance());
                            }
                            else LOGGER.log("Patch disabled: " + identifier);
                        }
                    }
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while loading definitions from path: " + storagePath, e);
                }
            }
        }
    }

    private void checkAdministratorUser() throws Exception
    {
        if (getTypeDefinitionMetaData(null, T8AdministratorUserDefinition.TYPE_IDENTIFIER).isEmpty())
        {
            T8AdministratorUserDefinition defaultUserDefinition;

            LOGGER.log("Creating default administrator user '" + DEFAULT_ADMINISTRATOR_IDENTIFIER + "'...");
            defaultUserDefinition = new T8AdministratorUserDefinition(DEFAULT_ADMINISTRATOR_IDENTIFIER);
            defaultUserDefinition.setProfileIdentifiers(ArrayLists.newArrayList(DEFAULT_ADMINISTRATOR_USER_PROFILE_IDENTIFIER));
            defaultUserDefinition.setMetaDisplayName("Default Administrator");
            defaultUserDefinition.setMetaDisplayName("A Default Adminsitrator user definition created when no other administrator definitions are available.");
            defaultUserDefinition.setUsername("admin");
            defaultUserDefinition.setSurname("Administrator");
            defaultUserDefinition.setResetPasswordFlag(true);
            defaultUserDefinition.setEmailAddress("default.administrator@pilog.co.za");
            defaultUserDefinition.setActive(true);
            defaultUserDefinition.setName("Default");
            systemLevelDefinitionCache.saveDefinition(defaultUserDefinition);
        }
    }

    private void destroyDefinitionResolvers()
    {
        for (T8DefinitionResolver definitionResolver : definitionResolvers)
        {
            definitionResolver.destroy();
        }
    }

    private void initializeDefinitionResolvers()
    {
        try
        {
            List<T8Definition> resolverDefinitions;

            // Clear the existing collection.
            definitionResolvers.clear();

            // Load all resolver definitions and create an instance of each.
            resolverDefinitions = this.getRawGroupDefinitions(internalContext, null, T8DefinitionResolverDefinition.GROUP_IDENTIFIER);
            for (T8Definition resolverDefinition : resolverDefinitions)
            {
                try
                {
                    T8DefinitionResolver definitionResolver;

                    definitionResolver = ((T8DefinitionResolverDefinition)resolverDefinition).getNewDefinitionResolverInstance(internalContext);
                    definitionResolver.init();
                    definitionResolvers.add(definitionResolver);
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while initializing definition resolver: " + resolverDefinition, e);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing definition resolvers.", e);
        }
    }

    private void initializeProjectLevelDefinitionCaches() throws Exception
    {
        Set<String> projectIds;

        // Clear the existing collection.
        projectLevelDefinitionCache = new T8ProjectCacheHandler(internalContext, this, getProjectLevelDefinitionTypeIds());

        // Create a project definition cache for all projects available.
        projectIds = rootLevelDefinitionCache.getGroupDefinitionIdentifiers(T8ProjectDefinition.GROUP_IDENTIFIER);
        for (String projectId : projectIds)
        {
            T8ProjectDefinition projectDefinition;

            projectDefinition = projectDefinitions.get(projectId);
            if (projectDefinition != null)
            {
                projectLevelDefinitionCache.addCache(projectDefinition);
            }
            else throw new Exception("Project specified in system configuration not found: " + projectId);
        }

        // Initialize the project level cache.
        projectLevelDefinitionCache.init();

        // Load project level definitions.
        projectLevelDefinitionCache.cacheDefinitions();

        // We have to reset the data manager so that subsequent connection, source, entity and connection pool definitions can override the already loaded ones.
        serverContext.getDataManager().reset();
    }

    private Set<String> getProjectLevelDefinitionTypeIds()
    {
        Set<String> identifierSet;

        identifierSet = new HashSet<>();
        for (T8DefinitionTypeMetaData typeMetaData : definitionTypeCache.getDefinitionTypeMetaData())
        {
            if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.PROJECT)
            {
                identifierSet.add(typeMetaData.getTypeId());
            }
        }

        return identifierSet;
    }

    private void destroyProjectLevelDefinitionCache()
    {
        if (projectLevelDefinitionCache != null) projectLevelDefinitionCache.destroy();
    }

    private void destroySystemLevelDefinitionCache()
    {
        if (systemLevelDefinitionCache != null) systemLevelDefinitionCache.destroy();
    }

    private void initializeSystemLevelDefinitionCache() throws Exception
    {
        if (systemDefinition != null)
        {
            // Initialize the definition cache.
            systemLevelDefinitionCache = new T8SystemDefinitionCache(internalContext, this, serverContext.getContextPath());
            systemLevelDefinitionCache.setSystemIdentifier(systemDefinition.getIdentifier());
            systemLevelDefinitionCache.init();

            // Instruct the system definition cache to cache its content definitions.
            LOGGER.log("Caching System Definitions: " + systemLevelDefinitionCache.getSystemIdentifier());
            systemLevelDefinitionCache.cacheDefinitions();
        }
        else throw new Exception("No system definition set.");
    }

    private void destroyResourceLevelDefinitionCache()
    {
        if (resourceLevelDefinitionCache != null) resourceLevelDefinitionCache.destroy();
    }

    private void initializeResourceLevelDefinitionCache() throws Exception
    {
        if (setupDefinition != null)
        {
            // Initialize the definition cache.
            resourceLevelDefinitionCache = new T8ResourceDefinitionCache(serverContext, this, definitionTypeCache.getTypeIdentifiers());
            resourceLevelDefinitionCache.init();

            // Instruct the resource definition cache to cache its content definitions.
            LOGGER.log("Caching Resource Definitions...");
            resourceLevelDefinitionCache.cacheDefinitions();
        }
        else throw new Exception("No Setup definition found.");
    }

    private void destroyRootLevelDefinitionCache()
    {
        if (rootLevelDefinitionCache != null) rootLevelDefinitionCache.destroy();
    }

    private void initializeRootLevelDefinitionCache() throws Exception
    {
        if (setupDefinition != null)
        {
            Set<String> rootLevelDefinitionTypeIdentifiers;

            // Get the root type identifiers to be cached.
            rootLevelDefinitionTypeIdentifiers = getRootLevelDefinitionTypeIdentifiers();
            rootLevelDefinitionTypeIdentifiers.remove(T8SetupDefinition.TYPE_IDENTIFIER); // We have to remove this definition, since it will be loaded and maintained independently.

            // Initialize the definition cache.
            rootLevelDefinitionCache = new T8FileDefinitionCache(internalContext, this, serverContext.getContextPath(), "@DCACHE_ROOT", rootLevelDefinitionTypeIdentifiers);
            rootLevelDefinitionCache.init();

            // Instruct the resource definition cache to cache its content definitions.
            LOGGER.log("Caching Root Definitions...");
            rootLevelDefinitionCache.cacheDefinitions();
        }
        else throw new Exception("No Setup definition found.");
    }

    private Set<String> getRootLevelDefinitionTypeIdentifiers()
    {
        Set<String> identifierSet;

        identifierSet = new HashSet<>();
        for (T8DefinitionTypeMetaData typeMetaData : definitionTypeCache.getDefinitionTypeMetaData())
        {
            if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.ROOT)
            {
                identifierSet.add(typeMetaData.getTypeId());
            }
        }

        return identifierSet;
    }

    @Override
    public boolean moveDefinitionToProject(T8Context context, String projectId, String definitionId, String newProjectId) throws Exception
    {
        T8Definition definition;

        definition = getRawDefinition(context, projectId, definitionId);
        if (definition != null)
        {
            // Only do the move if the new project identifier differs from the current one.
            if (!Objects.equals(definition.getProjectIdentifier(), newProjectId))
            {
                T8DefinitionHandle oldHandle;
                String oldProjectId;

                oldProjectId = definition.getProjectIdentifier();
                oldHandle = definition.getHandle();
                definition.setProjectIdentifier(newProjectId);
                checkUpdatePermission(context, definition);

                try
                {
                    deleteDefinitionUnsafe(oldHandle);
                    saveDefinition(context, definition, null);
                    return true;
                }
                catch (Exception e)
                {
                    definition.setProjectIdentifier(oldProjectId);
                    saveDefinition(context, definition, null);
                    throw e;
                }
            }
            else return false;
        }
        else throw new Exception("Definition not found: " + definitionId);
    }

    @Override
    public boolean copyDefinitionToProject(T8Context context, String projectId, String definitionId, String newProjectId) throws Exception
    {
        T8Definition definition;

        definition = getRawDefinition(context, projectId, definitionId);
        if (definition != null)
        {
            // Only do the copy if the new project identifier differs from the current one.
            if (!Objects.equals(definition.getProjectIdentifier(), newProjectId))
            {
                definition.setProjectIdentifier(newProjectId);
                saveDefinition(context, definition, null);
                return true;
            }
            else return false;
        }
        else throw new Exception("Definition not found: " + definitionId);
    }

    @Override
    public void transferDefinitionReferences(T8Context context, String oldIdentifier, String newIdentifier) throws Exception
    {
        // First check that valid identifiers were supplied.
        if (!T8IdentifierUtilities.isPublicId(oldIdentifier)) throw new Exception("Cannot transfer references from non-glabal old identifier: " + oldIdentifier);
        else if (!T8IdentifierUtilities.isPublicId(newIdentifier)) throw new Exception("Cannot transfer references to non-glabal new identifier: " + newIdentifier);
        else
        {
            List<T8DefinitionHandle> usedByHandles;

            // Find all definitions that use the old identifier.
            usedByHandles = findDefinitionsUsing(context, null, oldIdentifier);

            // Check the updated permissions using the old identifier.
            checkIdentifierChangePermissions(context, usedByHandles, oldIdentifier);

            // Propagate the new identifier to all the definitions that use the old identifier.
            propagateIdentifierChange(context, usedByHandles, oldIdentifier, newIdentifier);
        }
    }

    @Override
    public List<T8DefinitionHandle> renameDefinition(T8Context context, T8Definition definition, String newDefinitionId) throws Exception
    {
        if (!T8DefinitionUtilities.isDefinitionDatumMutable(definition)) throw new Exception("Cannot rename immutable definition '" + definition.getIdentifier() + "' in context of definition '" + definition.getParentDefinition() + "'.");
        else if (definition.getLevel() == T8DefinitionLevel.RESOURCE) throw new Exception("Cannot rename resource definition '" + definition.getIdentifier() + "' in context of definition '" + definition.getParentDefinition() + "'.");
        else
        {
            List<T8DefinitionHandle> usedByHandles;
            String oldDefinitionId;
            String projectId;

            // Store the old identifier.
            projectId = definition.getRootProjectId();
            oldDefinitionId = definition.getPublicIdentifier();

            // Find all definitions that use the old identifier.
            usedByHandles = findDefinitionsUsing(context, projectId, oldDefinitionId);

            // If the identifier is public and used by other definitions, make sure we are not renaming it to a private identifier.
            if (!usedByHandles.isEmpty())
            {
                if ((T8IdentifierUtilities.isPublicId(definition.getIdentifier())))
                {
                    if ((T8IdentifierUtilities.isPrivateId(newDefinitionId)))
                    {
                        if (projectId != null)
                        {
                            int externalProjectUsages;

                            externalProjectUsages = 0;
                            for (T8DefinitionHandle handle : usedByHandles)
                            {
                                if (!projectId.equals(handle.getProjectIdentifier()))
                                {
                                    externalProjectUsages++;
                                }
                            }

                            // If we found usages outside of the current scope, throw an exception preventing the rename.
                            if (externalProjectUsages > 0)
                            {
                                throw new Exception("Cannot rename public definition " + definition.getIdentifier() + " to private id " + newDefinitionId + " because it used by " + externalProjectUsages + " other definitions outside of project " + projectId + ".");
                            }
                        }
                        else throw new Exception("Cannot rename public definition " + definition.getIdentifier() + " to private id " + newDefinitionId + " because it does not belong to any project.");
                    }
                }
            }

            // Check the updated permissions using the old identifier.
            checkIdentifierChangePermissions(context, usedByHandles, oldDefinitionId);

            // If the renamed definition is a project definition, we need to perform some special case steps.
            if (definition instanceof T8ProjectDefinition)
            {
                T8ProjectDefinition projectDefinition;

                // Rename the cached project definition.
                projectDefinition = projectDefinitions.remove(oldDefinitionId);
                ((T8Definition)projectDefinition).setIdentifier(newDefinitionId, true);
                projectDefinitions.put(newDefinitionId, projectDefinition);

                // Reset the project definition cache identifier.
                projectLevelDefinitionCache.projectRenamed(oldDefinitionId, newDefinitionId);

                // Set the new identifier on all the used-by handles.
                for (T8DefinitionHandle handle : usedByHandles)
                {
                    if (oldDefinitionId.equals(handle.getProjectIdentifier()))
                    {
                        handle.setProjectIdentifier(newDefinitionId);
                    }
                }
            }

            // If the renamed definition is a system definition, we need to perform some special case steps.
            if (definition instanceof T8SystemDefinition)
            {
                resetSystemDefinitionCache(oldDefinitionId, newDefinitionId);
            }

            // If the definition to rename is a public definition then rename it and save the renamed version before deleting the old version.
            if ((T8IdentifierUtilities.isPublicId(definition.getIdentifier())) || (T8IdentifierUtilities.isPrivateId(definition.getIdentifier())))
            {
                T8DefinitionLock definitionLock;
                T8DefinitionHandle oldHandle;

                // Get the definition lock (if any) so that we can update it with the new identifier.
                definitionLock = definitionLocks.remove(definition.getHandle().getHandleIdentifier());

                // Set the new identifier on the definition and save the definition.
                oldHandle = definition.getHandle();
                definition.setIdentifier(newDefinitionId, true);
                saveDefinition(context, definition, null);

                // Propagate the new identifier to all the definitions that use the old identifier.
                propagateIdentifierChange(context, usedByHandles, oldDefinitionId, newDefinitionId);

                // The rename event is only useful thus far for global identifiers being changed
                fireDefinitionRenamedEvent(context, definition, oldDefinitionId, usedByHandles);

                // Delete the old copy of the definition.
                deleteDefinitionUnsafe(oldHandle);

                // If we found a lock, update it now.
                if (definitionLock != null)
                {
                    T8DefinitionHandle newHandle;

                    newHandle = definition.getHandle();
                    definitionLock.setDefinitionHandle(newHandle);
                    definitionLocks.put(newHandle.getHandleIdentifier(), definitionLock);
                }
            }
            else // It's a local definition so rename it and save the root.
            {
                T8Definition rootDefinition;
                String replacementId;

                // Set the new identifier on the definition and save the root definition.
                rootDefinition = definition.getRootDefinition();
                definition.setIdentifier(newDefinitionId, true);

                // Only save the definition and propagate changes if other definitions were affected.
                if (usedByHandles.size() > 1)
                {
                    // Save the definition before propagating changes.
                    saveDefinition(context, rootDefinition, null);

                    // Propagate the new identifier to all the definitions that use the old identifier.
                    // If the new identifier is a local identifier, then the namespace must be prepended when propagating the identifier change to other namespaces.
                    replacementId = T8IdentifierUtilities.isLocalId(newDefinitionId) ? rootDefinition.getIdentifier() + newDefinitionId : newDefinitionId;
                    propagateIdentifierChange(context, usedByHandles, oldDefinitionId, replacementId);
                }
            }

            // Return the list of definitions affected by this operation.
            return usedByHandles;
        }
    }

    private void resetSystemDefinitionCache(String oldIdentifier, String newIdentifier) throws Exception
    {
        if (systemLevelDefinitionCache.getSystemIdentifier().equals(oldIdentifier))
        {
            systemLevelDefinitionCache.setSystemIdentifier(newIdentifier);
        }
    }

    /**
     * Finds all of the definitions that contain or reference the specified
     * definition identifier.  The identifier must be global.
     * @param context The session for which this search is carried out.
     * @param definitionId The global identifier to search for.
     * @return A list of definition handles that
     * @throws Exception
     */
    private List<T8DefinitionHandle> findDefinitionsUsing(T8Context context, String projectId, String definitionId) throws Exception
    {
        if (T8IdentifierUtilities.isPrivateId(definitionId))
        {
            return projectLevelDefinitionCache.findDefinitionsUsing(context, projectId, definitionId);
        }
        else
        {
            List<T8DefinitionHandle> usedByHandles;
            List<String> usedByIds;

            // Create a new list to hold the definitions handles found.
            usedByHandles = new ArrayList<>();

            // Check the setup definition for the identifier.
            if (setupDefinition.containsIdentifier(definitionId))
            {
                usedByHandles.add(new T8DefinitionHandle(T8DefinitionLevel.ROOT, setupDefinition.getIdentifier(), null));
            }

            // Check the other root-level definitions.
            usedByIds = rootLevelDefinitionCache.findDefinitionsByIdentifier(definitionId);
            for (String usedByIdentifier : usedByIds)
            {
                usedByHandles.add(new T8DefinitionHandle(T8DefinitionLevel.ROOT, usedByIdentifier, null));
            }

            // Check the system-level definitions.
            usedByIds = systemLevelDefinitionCache.findDefinitionsByIdentifier(definitionId);
            for (String usedByIdentifier : usedByIds)
            {
                usedByHandles.add(new T8DefinitionHandle(T8DefinitionLevel.SYSTEM, usedByIdentifier, null));
            }

            // Check the project-level definitions.
            usedByHandles.addAll(projectLevelDefinitionCache.findDefinitionsUsing(context, definitionId));

            // Return the list of handles found.
            return usedByHandles;
        }
    }

    private void checkIdentifierChangePermissions(T8Context context, List<T8DefinitionHandle> usedByHandles, String oldDefinitionId) throws Exception
    {
        String userIdentifier;
        String globalIdentifier;
        String localIdentifier;

        // Check each one of the 'used-by' definitions to make sure that the current user can update them.
        userIdentifier = context.getSessionContext().getUserIdentifier();
        globalIdentifier = T8IdentifierUtilities.getGlobalIdentifierPart(oldDefinitionId);
        localIdentifier = T8IdentifierUtilities.getLocalIdentifierPart(oldDefinitionId);

        // Check each of the handles to make sure that the definitions can be updated/refactored.
        for (T8DefinitionHandle usedByHandle : usedByHandles)
        {
            T8DefinitionMetaData usedByDefinitionMeta;
            boolean locked;

            // Check the locked state of the 'used-by' definition.
            locked = isDefinitionLocked(usedByHandle);
            if (usedByHandle.getDefinitionIdentifier().equals(globalIdentifier))
            {
                if ((localIdentifier != null) && (!locked))
                {
                    // If we are renaming a local definition, make sure its root is locked.
                    throw new Exception("Definition '" + usedByHandle + "' contains '" + oldDefinitionId + "' to be renamed bu is not currently locked and cannot be updated.");
                }
            }
            else if (locked)
            {
                // If we are renaming a global definition, make sure its root is locked.
                throw new Exception("Definition '" + usedByHandle + "' references '" + oldDefinitionId + "' but is currently locked and cannot be changed.");
            }

            // Get the definition meta data of the 'used-by' definition.
            usedByDefinitionMeta = getDefinitionMetaData(usedByHandle);
            if (usedByDefinitionMeta != null)
            {
                if (usedByHandle.getDefinitionLevel() == T8DefinitionLevel.ROOT)
                {
                    // TODO:  Check system persmissions.
                }
                else if (usedByDefinitionMeta.getDefinitionLevel() == T8DefinitionLevel.RESOURCE)
                {
                    throw new Exception("Definition '" + usedByHandle + "' references '" + oldDefinitionId + "' but is of type 'resource' and cannot be changed.");
                }
                else if (usedByHandle.getDefinitionLevel() == T8DefinitionLevel.SYSTEM)
                {
                    // TODO:  Check system persmissions.
                }
                else if (usedByHandle.getDefinitionLevel() == T8DefinitionLevel.PROJECT)
                {
                    T8ProjectDefinition projectDefinition;
                    String projectId;

                    projectId = usedByDefinitionMeta.getProjectId();
                    if (projectDefinitions.containsKey(projectId))
                    {
                        projectDefinition = projectDefinitions.get(projectId);
                        if (!projectDefinition.getDeveloperIdentifiers().contains(userIdentifier)) throw new Exception("Definition '" + usedByHandle + "' references '" + oldDefinitionId + "' but the current user '" + userIdentifier + "' does not have permission to change this definition.");
                    }
                    else throw new Exception("Definition '" + usedByHandle + "' is PROJECT level but does not specify a valid project identifier: " + projectId);
                }
                else throw new Exception("Definition handle '" + usedByHandle + "' reports invalid definition level: " + usedByHandle.getDefinitionLevel());
            }
            else throw new Exception("Invalid definition handle.  Definition not found: " + usedByHandle);
        }
    }

    private void propagateIdentifierChange(T8Context context, List<T8DefinitionHandle> usedByHandles, String oldIdentifier, String newIdentifier) throws Exception
    {
        // Propagate the new identifier to all the definitions that use the old identifier.
        for (T8DefinitionHandle usedByHandle : usedByHandles)
        {
            T8Definition usedByDefinition;

            // Load the definition that uses the identifier to be changed.
            usedByDefinition = getRawDefinition(context, usedByHandle);

            // Set the new identifier on the definition that uses the old identifier.
            if (usedByDefinition.getIdentifier().equals(oldIdentifier)) usedByDefinition.setIdentifier(newIdentifier, true);
            else usedByDefinition.changeReferenceId(oldIdentifier, newIdentifier, false);

            // Save the updated definition.
            saveDefinition(context, usedByDefinition, null);
        }
    }

    @Override
    public T8Definition copyDefinition(T8Context context, T8Definition definition, String newIdentifier) throws Exception
    {
        T8Definition copiedDefinition;

        copiedDefinition = (T8Definition)SerializationUtilities.copyObjectBySerialization(definition);
        copiedDefinition.setIdentifier(newIdentifier, false);
        return copiedDefinition;
    }

    @Override
    public T8Definition copyDefinition(T8Context context, String projectId, String definitionId, String newDefinitionId) throws Exception
    {
        T8Definition copiedDefinition;

        copiedDefinition = getRawDefinition(context, projectId, definitionId);
        copiedDefinition.setIdentifier(newDefinitionId, false);
        return copiedDefinition;
    }

    @Override
    public T8Definition initializeDefinition(T8Context context, T8Definition definition, Map<String, Object> inputParameters) throws Exception
    {
        // Loop through all datum values and translate the display String type.
        for (T8DefinitionDatumType datum : definition.getDatumTypes())
        {
            T8DataType datumDataType;

            datumDataType = datum.getDataType();
            if (datumDataType.isType(T8DtString.IDENTIFIER))
            {
                T8DtString stringDataType;

                stringDataType = (T8DtString)datumDataType;
                if (stringDataType.getType() == T8StringType.TRANSLATED_STRING)
                {
                    String datumIdentifier;
                    String value;

                    datumIdentifier = datum.getIdentifier();
                    value = (String)definition.getDefinitionDatum(datumIdentifier);
                    definition.setDefinitionDatum(datumIdentifier, configurationManager.getUITranslation(context, value), false);
                }
            }
        }

        // Initialize the definition.
        definition.initializeDefinition(context, inputParameters, null);

        // Initialize all of the sub-definitions.
        for (T8Definition subDefinition : definition.getSubDefinitions())
        {
            initializeDefinition(context, subDefinition, inputParameters);
        }

        return definition;
    }

    @Override
    public T8Definition getResolvedDefinition(T8Context context, String typeIdentifier, Map<String, Object> inputParameters) throws Exception
    {
        // Find a resolver capably of resolving the requested type and then fetch the resolved definition.
        for (T8DefinitionResolver definitionResolver : definitionResolvers)
        {
            if (definitionResolver.resolvesType(typeIdentifier))
            {
                return definitionResolver.getResolvedDefinition(context, typeIdentifier, inputParameters);
            }
        }

        // This point should not be reached if the requested type is valid.
        throw new Exception("Definition type could not be resolved: " + typeIdentifier);
    }

    @Override
    public <D extends T8Definition> D getInitializedDefinition(T8Context context, String projectId, String definitionId, Map<String, Object> inputParameters) throws Exception
    {
        D definition;

        definition = getRawDefinition(context, projectId, definitionId);
        if (definition != null) initializeDefinition(context, definition, inputParameters);
        return definition;
    }

    @Override
    public <D extends T8Definition> D getRawDefinition(T8Context context, String projectId, String definitionId) throws Exception
    {
        T8Definition rawDefinition;

        // Load the definition from the most applicable cache.
        if ((rootLevelDefinitionCache != null) && (rootLevelDefinitionCache.containsDefinition(definitionId)))
        {
            // Fetch the definition from the root level cache.
            rawDefinition = rootLevelDefinitionCache.loadDefinition(definitionId);
        }
        else if ((resourceLevelDefinitionCache != null) && (resourceLevelDefinitionCache.containsDefinition(definitionId)))
        {
            // Fetch the definition from the resource level cache.
            rawDefinition = resourceLevelDefinitionCache.loadDefinition(definitionId);
        }
        else if ((systemLevelDefinitionCache != null) && (systemLevelDefinitionCache.containsDefinition(definitionId)))
        {
            // Fetch the definition from the system level cache.
            rawDefinition = systemLevelDefinitionCache.loadDefinition(definitionId);
        }
        else if (projectId != null)
        {
            // Fetch the definition from the project level cache using a specific project context.
            rawDefinition = projectLevelDefinitionCache.loadDefinition(projectId, definitionId);
        }
        else
        {
            // Fetch the definition from the project level cache.
            rawDefinition = projectLevelDefinitionCache.loadDefinition(definitionId);
        }

        // All definitions are locked by default when loaded from the cache.
        if (rawDefinition != null) rawDefinition.setLocked(true);
        return (D)rawDefinition;
    }

    @Override
    public T8Definition getRawDefinition(T8Context context, T8DefinitionHandle definitionHandle) throws Exception
    {
        T8Definition rawDefinition = null;
        T8DefinitionLevel level;
        String projectId;
        String definitionId;

        // Get the details of the definition to be loaded.
        definitionId = definitionHandle.getDefinitionIdentifier();
        projectId = definitionHandle.getProjectIdentifier();
        level = definitionHandle.getDefinitionLevel();

        // If the definition handle refers to the setup definition, return it.
        if (setupDefinition.getIdentifier().equals(definitionId)) return setupDefinition;

        // Find the definition in the applicable cache according to its storage level.
        if ((level == T8DefinitionLevel.ROOT) && (rootLevelDefinitionCache.containsDefinition(definitionId)))
        {
            // Check the root definition cache for the definition.
            rawDefinition = rootLevelDefinitionCache.loadDefinition(definitionId);
        }
        else if ((level == T8DefinitionLevel.SYSTEM) && (systemLevelDefinitionCache.containsDefinition(definitionId)))
        {
            // Check the system definition cache for the definition.
            rawDefinition = systemLevelDefinitionCache.loadDefinition(definitionId);
        }
        else if (level == T8DefinitionLevel.PROJECT)
        {
            // Fetch the required definition from the project level cache.
            rawDefinition = projectLevelDefinitionCache.loadDefinition(projectId, definitionId);
        }
        else if (level == T8DefinitionLevel.RESOURCE)
        {
            // Check the resource definition cache for the definition.
            rawDefinition = resourceLevelDefinitionCache.loadDefinition(definitionId);
        }

        // Configure the retrieve definition before returning it.
        if (rawDefinition != null)
        {
            // All definitions are locked by default when loaded from the cache.
            rawDefinition.setLocked(true);
        }

        // Return the result.
        return rawDefinition;
    }

    private boolean isDefinitionLocked(T8DefinitionHandle definitionHandle)
    {
        return definitionLocks.containsKey(definitionHandle.getHandleIdentifier());
    }

    private boolean isDefinitionLockedWithKey(T8DefinitionHandle definitionHandle, String key)
    {
        T8DefinitionLock lock;

        lock = definitionLocks.get(definitionHandle.getHandleIdentifier());
        if (lock != null)
        {
            return lock.getKeyIdentifier().equals(key);
        }
        else return false;
    }

    @Override
    public T8Definition lockDefinition(T8Context context, T8DefinitionHandle definitionHandle, String keyIdentifier) throws Exception
    {
        String identifier;

        // Check the validity of the supplied hanle.
        if ((definitionHandle.getDefinitionLevel() == T8DefinitionLevel.RESOURCE) && (Strings.isNullOrEmpty(definitionHandle.getProjectIdentifier()))) throw new IllegalArgumentException("Cannot lock RESOURCE level definition: " + definitionHandle);
        if ((definitionHandle.getDefinitionLevel() == T8DefinitionLevel.PROJECT) && (Strings.isNullOrEmpty(definitionHandle.getProjectIdentifier()))) throw new IllegalArgumentException("Definition handle set as PROJECT level but no project identifier specified: " + definitionHandle);

        // Now get the handle identifier to lock.
        identifier = definitionHandle.getHandleIdentifier();
        if (!definitionLocks.containsKey(identifier))
        {
            T8Definition rawDefinition;

            rawDefinition = getRawDefinition(context, definitionHandle);
            if (rawDefinition != null)
            {
                checkUpdatePermission(context, rawDefinition);

                if (keyIdentifier != null)
                {
                    // Set the lock.
                    definitionLocks.put(identifier, new T8DefinitionLock(definitionHandle, keyIdentifier, System.currentTimeMillis(), context.getSessionContext()));

                    // Set the locked flag on the definition to false (it's locked for all other users not this one).
                    rawDefinition.setLocked(false);

                    // Return the locked definition.
                    return rawDefinition;
                }
                else throw new Exception("No key specified for definition lock: " + definitionHandle);

            }
            else throw new Exception("Definition not found: " + definitionHandle);
        }
        else throw new Exception("Definition already locked: " + definitionHandle);
    }

    @Override
    public T8Definition unlockDefinition(T8Context context, T8DefinitionHandle definitionHandle, String keyId) throws Exception
    {
        synchronized (definitionLocks)
        {
            if (definitionHandle != null)
            {
                String definitionId;

                definitionId = definitionHandle.getHandleIdentifier();
                if (definitionLocks.containsKey(definitionId))
                {
                    if (keyId != null)
                    {
                        T8DefinitionLock lock;

                        lock = definitionLocks.get(definitionId);
                        if (!lock.getKeyIdentifier().equals(keyId))
                        {
                            // Allow the project owner to unlock a definition even if he does not hold the lock.
                            if (context != null)
                            {
                                if (definitionHandle.getDefinitionLevel() == T8DefinitionLevel.ROOT)
                                {
                                    // TODO:  We need to add System owner specification to the System definition so that they can be checked here.
                                    // Only System owners are allowed to unlock root definitions.
                                    definitionLocks.remove(definitionId);
                                }
                                else if (definitionHandle.getDefinitionLevel() == T8DefinitionLevel.SYSTEM)
                                {
                                    // TODO:  We need to add System owner specification to the System definition so that they can be checked here.
                                    // Only System owners are allowed to unlock system definitions.
                                    definitionLocks.remove(definitionId);
                                }
                                else
                                {
                                    String projectId;

                                    projectId = definitionHandle.getProjectIdentifier();
                                    if (projectId != null)
                                    {
                                        T8ProjectDefinition projectDefinition;

                                        projectDefinition = projectDefinitions.get(projectId);
                                        if (projectDefinition != null)
                                        {
                                            if (projectDefinition.getOwnerIdentifiers().contains(context.getSessionContext().getUserIdentifier()))
                                            {
                                                definitionLocks.remove(definitionId);
                                            }
                                            else throw new Exception("Only project owners can unlock a definition with a lock held by another user: " + definitionHandle);
                                        }
                                        else throw new Exception("Referenced Project definition '" + projectId + "' not found during definition unlock: " + definitionHandle);
                                    }
                                    else throw new Exception("Invalid key identifier supplied for definition unlock: " + definitionHandle);
                                }
                            }
                            else throw new Exception("Invalid key identifier supplied for definition unlock: " + definitionHandle);
                        }
                        else
                        {
                            definitionLocks.remove(definitionId);
                        }
                    }
                    else throw new Exception("No key specified for definition unlock: " + definitionHandle);
                }
                else throw new Exception("Definition not locked: " + definitionHandle);

                // If we got this far then the definition go unlocked and we can return it with the new lock status.
                return getRawDefinition(context, definitionHandle);
            }
            else throw new Exception("No definition specified for locking operation.");
        }
    }

    @Override
    public T8Definition unlockDefinition(T8Context context, T8Definition definition, String keyId) throws Exception
    {
        if (definition != null)
        {
            synchronized (definitionLocks)
            {
                T8DefinitionHandle definitionHandle;
                String identifier;

                definitionHandle = definition.getHandle();
                identifier = definitionHandle.getHandleIdentifier();
                if (definitionLocks.containsKey(identifier))
                {
                    if (keyId != null)
                    {
                        T8DefinitionLock lock;

                        // Check the lock key.
                        lock = definitionLocks.get(identifier);
                        if (!lock.getKeyIdentifier().equals(keyId)) throw new Exception("Invalid key identifier supplied for definition unlock: " + definitionHandle);
                        else
                        {
                            // Save the definition and remove the lock.
                            saveDefinition(context, definition, null);
                            definitionLocks.remove(identifier);
                        }
                    }
                    else throw new Exception("No key specified for definition unlock: " + definitionHandle);
                }
                else throw new Exception("Definition not locked: " + definitionHandle);

                //If we got this far then the definition go unlocked and we can return it with the new lock status
                return getRawDefinition(context, definitionHandle);
            }
        }
        else throw new Exception("Null definition found.");
    }

    @Override
    public T8DefinitionLockDetails getDefinitionLockDetails(T8Context context, T8DefinitionHandle definitionHandle) throws Exception
    {
        String identifier;

        identifier = definitionHandle.getHandleIdentifier();
        if (definitionLocks.containsKey(identifier))
        {
            return definitionLocks.get(identifier).getDetails();
        }
        else return new T8DefinitionLockDetails(definitionHandle.getDefinitionIdentifier(), definitionHandle.getProjectIdentifier(), null, null, false);
    }

    @Override
    public T8DefinitionMetaData finalizeDefinition(T8Context context, T8DefinitionHandle definitionHandle, String comment) throws Exception
    {
        T8Definition definition;

        definition = getRawDefinition(context, definitionHandle);
        if (definition != null)
        {
            List<T8DefinitionValidationError> validationErrors;

            // Validate the definition to ensure no problems exist before it is finalized.
            validationErrors = definition.validateDefinition(serverContext, context.getSessionContext(), T8Definition.DefinitionValidationType.GLOBAL);
            if ((validationErrors != null) && (!validationErrors.isEmpty()))
            {
                // TODO:  Upgrade this exception to a proper type that will contain the validation error list.
                throw new Exception("Definition cannot be finalized until all validation errors have been resolved: " + definitionHandle);
            }
            else
            {
                // Check the definition update permissions.
                checkUpdatePermission(context, definition);

                // Update the meta data of the definition.
                definition.setFinalizedTime(System.currentTimeMillis());
                definition.setFinalizedUserIdentifier(context.getSessionContext().getUserIdentifier());
                definition.setDefinitionStatus(T8DefinitionStatus.FINALIZED);
                definition.addComment(new T8DefinitionComment(T8Timestamp.now(), CommentType.FINALIZATION, context.getSessionContext().getUserIdentifier(), comment));

                // Check for system reserved definition types and save them separately.  All other types are saved to the project-specific caches.
                return saveDefinitionInternal(context, definition);
            }
        }
        else throw new Exception("Definition not found: " + definitionHandle);
    }

    @Override
    public T8DefinitionMetaData saveDefinition(T8Context context, T8Definition definition, String keyIdentifier) throws Exception
    {
        // Check the definition update permissions.
        checkUpdatePermission(context, definition);

        // Update the meta data of the definition.
        definition.incrementRevision();
        definition.setUpdatedTime(System.currentTimeMillis());
        definition.setUpdatedUserIdentifier(context != null ? context.getSessionContext().getUserIdentifier() : "SYSTEM");
        if (definition.getCreatedTime() == null) definition.setCreatedTime(definition.getUpdatedTime());
        if (definition.getCreatedUserIdentifier() == null) definition.setCreatedUserIdentifier(definition.getUpdatedUserIdentifier());
        definition.setDefinitionStatus(T8DefinitionStatus.UPDATED);

        // Check for system reserved definition types and save them separately.  All other types are saved to the project-specific caches.
        return saveDefinitionInternal(context, definition);
    }

    /**
     * Saves a definition to the appropriate storage without any validation or checking.
     * This method must only be used internally after use-case dependent validation has been done.
     * @param context The context within which the definition is saved.
     * @param definition The definition to be saved.
     * @param keyIdentifier The key identifier used for access to a locked definition.
     * @return The updated meta data of the saved definition.
     * @throws Exception
     */
    private T8DefinitionMetaData saveDefinitionInternal(T8Context context, T8Definition definition) throws Exception
    {
        T8DefinitionTypeMetaData typeMetaData;

        // Get the type meta data of the definition.
        typeMetaData = definition.getTypeMetaData();

        // Check for system reserved definition types and save them separately.  All other types are saved to the project-specific caches.
        if (typeMetaData.getTypeId().equals(T8SetupDefinition.TYPE_IDENTIFIER))
        {
            saveSetupDefinition(context, (T8SetupDefinition)definition);
            fireDefinitionSavedEvent(context, definition);
            return definition.getMetaData();
        }
        else if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.ROOT)
        {
            // Save the root definition.
            if (rootLevelDefinitionCache.savesType(typeMetaData))
            {
                // Now save the definition.
                rootLevelDefinitionCache.saveDefinition(definition);

                // If we are saving a project definition, we need to administer the appropriate updates specifically.
                if (definition instanceof T8ProjectDefinition)
                {
                    T8ProjectDefinition projectDefinition;

                    projectDefinition = (T8ProjectDefinition)definition;
                    if (projectDefinitions.containsKey(projectDefinition.getIdentifier()))
                    {
                        projectLevelDefinitionCache.projectDefinitionSaved(projectDefinition);
                        projectDefinitions.put(projectDefinition.getIdentifier(), projectDefinition);
                    }
                    else
                    {
                        projectLevelDefinitionCache.addCache(projectDefinition);
                        projectDefinitions.put(projectDefinition.getIdentifier(), projectDefinition);
                    }
                }

                // Fire event to indicate the new definition changes have been saved.
                fireDefinitionSavedEvent(context, definition);
                return definition.getMetaData();
            }

            // This point should never be reached.
            throw new Exception("No applicable definition cache found for system definition '" + definition + "' with type identifier: " + typeMetaData.getTypeId());
        }
        else if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.SYSTEM)
        {
            // Save the system definition.
            if (systemLevelDefinitionCache.savesType(typeMetaData))
            {
                // Now save the definition.
                systemLevelDefinitionCache.saveDefinition(definition);
                fireDefinitionSavedEvent(context, definition);
                return definition.getMetaData();
            }

            // This point should never be reached.
            throw new Exception("No applicable definition cache found for system definition '" + definition + "' with type identifier: " + typeMetaData.getTypeId());
        }
        else
        {
            projectLevelDefinitionCache.saveDefinition(definition);
            fireDefinitionSavedEvent(context, definition);
            return definition.getMetaData();
        }
    }

    private void checkUpdatePermission(T8Context context, T8Definition definition) throws Exception
    {
        T8DefinitionTypeMetaData typeMetaData;
        T8ProjectDefinition projectDefinition;
        T8SessionContext sessionContext;
        String projectId;
        String userId;

        // Get the type meta data of the definition.
        typeMetaData = definition.getTypeMetaData();

        // Check the definition to be saved to ensure that it is valid.
        sessionContext = context.getSessionContext();
        userId = sessionContext.getUserIdentifier();
        projectId = definition.getProjectIdentifier();
        projectDefinition = projectDefinitions.get(projectId);
        if (!definition.isGlobalDefinition()) throw new Exception("Cannot save a non-global definition: " + definition);
        if (definition.getLevel() == T8DefinitionLevel.RESOURCE) throw new Exception("Cannot save resource definition: " + definition);
        if (sessionContext.isSystemSession())
        {
            // System is always allowed to update definitions.
        }
        else if (typeMetaData.getTypeId().equals(T8SystemDefinition.TYPE_IDENTIFIER))
        {
            // No checks for System definition currently.
        }
        else if (typeMetaData.getTypeId().equals(T8ProjectDefinition.TYPE_IDENTIFIER))
        {
            T8ProjectDefinition newProjectDefinition;
            T8ProjectDefinition oldProjectDefinition;
            List<String> oldOwnerIdentifiers;
            List<String> newOwnerIdentifiers;

            // Get the new project definition to save and the the old one that already exists (if any).
            newProjectDefinition = (T8ProjectDefinition)definition;
            oldProjectDefinition = projectDefinitions.get(definition.getIdentifier());
            oldOwnerIdentifiers = oldProjectDefinition != null ? oldProjectDefinition.getOwnerIdentifiers() : null;
            newOwnerIdentifiers = newProjectDefinition.getOwnerIdentifiers();

            // If the old definition was found (the one to be updated) we need to check that only one of the owners of the project can update it.
            if (oldOwnerIdentifiers != null)
            {
                if (!oldOwnerIdentifiers.contains(userId)) throw new Exception("Current user '" + userId + "' does not have permission to update definition: " + definition);
            }

            // Check that the new project has owners linked to it.
            if ((newOwnerIdentifiers == null) || (newOwnerIdentifiers.isEmpty())) throw new Exception("New Project '" + definition + "' has no owners linked to it.");
            else if (!newOwnerIdentifiers.contains(userId)) throw new Exception("New Project '" + definition + "' does not have the current user '" + userId + "' linked as an owner.");
        }
        else if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.ROOT)
        {
            // Root definitions do not need to have a project identifier.
        }
        else if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.SYSTEM)
        {
            // System definitions do not need to have a project identifier.
        }
        else
        {
            if (projectId == null) throw new Exception("No Project set for definition: " + definition);
            if (projectDefinition == null) throw new Exception("Project definition not found: " + projectId);
            if (!projectDefinition.getDeveloperIdentifiers().contains(userId)) throw new Exception("Current user '" + userId + "' does not have permission to update definition: " + definition);
        }
    }

    @Override
    public void removeDefinitionReferences(T8Context context, String idToBeRemoved, String keyId) throws Exception
    {
        List<T8DefinitionHandle> referrerHandles;
        LinkedList<T8Definition> lockedDefinitions;

        // Get the handles of all definitions containing the identifier to be removed.
        referrerHandles = findDefinitionsByIdentifier(context, idToBeRemoved, true);

        // Update all referrer definitions.
        lockedDefinitions = new LinkedList<>();
        try
        {
            // First attempt to lock all of the affected definitions to ensure that we can perform the entire transaction.
            for (T8DefinitionHandle referrerHandle : referrerHandles)
            {
                T8Definition lockedDefinition;

                lockedDefinition = lockDefinition(context, referrerHandle, keyId);
                lockedDefinitions.add(lockedDefinition);
            }

            // Now loop through all of the locked definitions, updating each one.
            while (!lockedDefinitions.isEmpty())
            {
                T8Definition lockedDefinition;

                lockedDefinition = lockedDefinitions.removeFirst();
                lockedDefinition.removeReferenceId(idToBeRemoved, true);
                unlockDefinition(context, lockedDefinition, keyId);
            }
        }
        catch (Exception e)
        {
            // If the transaction fails, unlock all affected definitions.
            for (T8Definition lockedDefinition : lockedDefinitions)
            {
                unlockDefinition(context, lockedDefinition.getHandle(), keyId);
            }

            // Throw the exception.
            throw e;
        }
    }

    @Override
    public void deleteDefinition(T8Context context, String projectId, String definitionId, boolean safeDeletion, String keyId) throws Exception
    {
        T8Definition definitionToDelete;

        definitionToDelete = getRawDefinition(context, projectId, definitionId);
        if (definitionToDelete != null)
        {
            // Check the definition update permission.
            checkUpdatePermission(context, definitionToDelete);

            // If it is 'safe' deletion, we need to check for other definitions that use the one to be deleted.
            if (safeDeletion)
            {
                List<T8DefinitionHandle> usedByDefinitionHandles;

                // If the definition to be deleted does not override any other definition then we have to do further checks for usage.  Otherwise we can just delete it.
                usedByDefinitionHandles = findDefinitionsUsing(context, projectId, definitionId);
                if (usedByDefinitionHandles.isEmpty())
                {
                    deleteDefinitionUnsafe(definitionToDelete.getHandle());
                    fireDefinitionDeletedEvent(context, definitionToDelete);
                }
                else throw new Exception("The definition '" + definitionId + "' cannot be deleted because it is used by " + usedByDefinitionHandles.size() + " other definitions.");
            }
            else
            {
                deleteDefinitionUnsafe(definitionToDelete.getHandle());
                fireDefinitionDeletedEvent(context, definitionToDelete);
            }
        }
        else throw new Exception("Definition not found for deletion: " + definitionId);
    }

    private void deleteDefinitionUnsafe(T8DefinitionHandle definitionHandle) throws Exception
    {
        definitionLocks.remove(definitionHandle.getHandleIdentifier());
        switch (definitionHandle.getDefinitionLevel())
        {
            case ROOT:
                rootLevelDefinitionCache.deleteDefinition(definitionHandle.getDefinitionIdentifier());
                return;
            case SYSTEM:
                systemLevelDefinitionCache.deleteDefinition(definitionHandle.getDefinitionIdentifier());
                return;
            case PROJECT:
                projectLevelDefinitionCache.deleteDefinition(definitionHandle.getProjectIdentifier(), definitionHandle.getDefinitionIdentifier());
                return;
        }
    }

    @Override
    public int getCachedDefinitionTypeCount(T8Context context)
    {
        return definitionTypeCache.getCachedDefinitionTypeCount();
    }

    @Override
    public int getCachedDefinitionCount(T8Context context)
    {
        int count;

        count = 0;

        // Add project count.
        count += projectLevelDefinitionCache.getCachedDefinitionCount();

        // Add system count.
        count += systemLevelDefinitionCache.getCachedDefinitionCount();

        // Add resource count.
        count += resourceLevelDefinitionCache.getCachedDefinitionCount();

        return count;
    }

    @Override
    public long getCachedDataSize(T8Context context)
    {
        long size;

        size = 0;

        // Add project size.
        size += projectLevelDefinitionCache.getCachedCharacterSize();

        // Add system size.
        size += systemLevelDefinitionCache.getCachedCharacterSize();

        // Add resource size.
        size += resourceLevelDefinitionCache.getCachedCharacterSize();

        return size;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends T8Definition> ArrayList<T> getRawGroupDefinitions(T8Context context, String projectId, String groupId) throws Exception
    {
        ArrayList<T> definitionList;
        List<T8DefinitionMetaData> metaDataList;

        definitionList = new ArrayList<>();
        metaDataList = getGroupDefinitionMetaData(projectId, groupId);
        for (T8DefinitionMetaData metaData : metaDataList)
        {
            T8Definition rawDefinition;

            rawDefinition = getRawDefinition(context, projectId, metaData.getId());
            if (rawDefinition != null)
            {
                definitionList.add((T)rawDefinition);
            }
            else throw new RuntimeException("Definition meta data found in cache, but definition could not be found: " + metaData);
        }

        return definitionList;
    }

    @Override
    public <T extends T8Definition> List<T> getInitializedGroupDefinitions(T8Context context, String projectId, String groupId, Map<String, Object> inputParameters) throws Exception
    {
        ArrayList<T> definitionList;
        List<T8DefinitionMetaData> metaDataList;

        definitionList = new ArrayList<>();
        metaDataList = getGroupDefinitionMetaData(projectId, groupId);
        for (T8DefinitionMetaData metaData : metaDataList)
        {
            T8Definition definition;

            definition = getRawDefinition(context, null, metaData.getId());
            if (definition != null)
            {
                initializeDefinition(context, definition, inputParameters);
                definitionList.add((T)definition);
            }
            else throw new Exception("Definition registered to group '" + groupId + "' could not be loaded: " + metaData.getId());
        }

        return definitionList;
    }

    @Override
    public T8Definition createNewDefinition(String identifier, String typeIdentifier) throws Exception
    {
        T8DefinitionTypeMetaData typeMetaData;

        typeMetaData = definitionTypeCache.getDefinitionTypeMetaData(typeIdentifier);
        if (typeMetaData != null)
        {
            try
            {
                T8Definition newDefinition;
                String className;

                className = typeMetaData.getClassName();
                newDefinition = (T8Definition)Class.forName(className).getConstructor(String.class).newInstance(identifier);
                newDefinition.constructDefinition(serverContext);
                return newDefinition;
            }
            catch (Throwable e)
            {
                throw new Exception("Exception while creating definition of type: " + typeMetaData.getTypeId(), e);
            }
        }
        else throw new Exception("Unresolvable Definition Type: " + typeIdentifier);
    }

    @Override
    public T8Definition createNewDefinition(String identifier, String typeIdentifier, List parameters) throws Exception
    {
        T8DefinitionTypeMetaData typeMetaData;
        Class[] classArray;
        Object[] objectArray;

        classArray = new Class[parameters.size() + 1];
        classArray[0] = String.class; // The first parameter of a definition constructor must always be the new definition's identifier.
        objectArray = new Object[parameters.size() + 1];
        objectArray[0] = identifier; // The first parameter of a definition constructor must always be the new definition's identifier.

        for (int parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex++)
        {
            Object parameter;

            parameter = parameters.get(parameterIndex);
            if (parameter != null)
            {
                classArray[parameterIndex + 1] = parameter.getClass();
                objectArray[parameterIndex + 1] = parameter;
            }
            else
            {
                classArray[parameterIndex + 1] = Object.class;
                objectArray[parameterIndex + 1] = null;
            }
        }

        typeMetaData = definitionTypeCache.getDefinitionTypeMetaData(typeIdentifier);
        if (typeMetaData != null)
        {
            T8Definition newDefinition;
            String className;

            className = typeMetaData.getClassName();
            newDefinition = (T8Definition)Class.forName(className).getConstructor(classArray).newInstance(objectArray);
            newDefinition.constructDefinition(serverContext);
            return newDefinition;
        }
        else throw new Exception("Unresolvable Definition Type: " + typeIdentifier);
    }

    @Override
    public boolean checkIdentifierAvailability(T8Context context, String projectId, String definitionId) throws Exception
    {
        if (definitionId == null) return false;
        else if (T8IdentifierUtilities.isPrivateId(definitionId))
        {
            // Check the specified project cache.
            return (!projectLevelDefinitionCache.containsDefinition(projectId, definitionId));
        }
        else
        {
            // Check project caches.
            if (projectLevelDefinitionCache.containsDefinition(definitionId)) return false;

            // Check system definition cache.
            if (systemLevelDefinitionCache.containsDefinition(definitionId)) return false;

            // Check resource definition cache.
            if (resourceLevelDefinitionCache.containsDefinition(definitionId)) return false;

            // Check root definition cache.
            if (rootLevelDefinitionCache.containsDefinition(definitionId)) return false;

            return true;
        }
    }

    // TODO: GBO - Suggested change
    // Profiling tests indicate that this is on average at least 45% faster
    // Also, the average CPU usage is about 2.5-4 times higher, which is
    // acceptable based on the fact that multiple threads are in use.
    //        // We want to limit the number of threads, so we process over caches, rather than over definitions
    //        // An ArrayList is best for parallel streams
    //        identifiers = ArrayLists.appendAll(ArrayLists.typeSafeList(systemLevelDefinitionCache, resourceLevelDefinitionCache, rootLevelDefinitionCache), projectDefinitionCaches)
    //                .parallelStream()   // We want to use a prallel stream
    //                .flatMap(projectDefinitionCache -> projectDefinitionCache.findDefinitionsByText(searchText).stream())  // Create streams of all definitions found
    //                .parallel() // We want to continue working with a prallel stream. flatMap resulted in sequential stream
    //                .collect(Collectors.toCollection(HashSet::new));  // Collect to HashSet to prevent duplicates (Using distinct in the stream defeats the purpose of parallel processing)
    //        return new ArrayList<>(identifiers);
    @Override
    public List<T8DefinitionHandle> findDefinitionsByText(T8Context context, String searchText) throws Exception
    {
        List<T8DefinitionHandle> handles;

        // Create a list to hold all definition handles that we find.
        handles = new ArrayList<>();

        // Add identifiers from project caches.
        handles.addAll(projectLevelDefinitionCache.findDefinitionsByText(context, searchText));

        // Add identifiers from system caches.
        for (String definitionId : systemLevelDefinitionCache.findDefinitionsByText(searchText))
        {
            handles.add(new T8DefinitionHandle(T8DefinitionLevel.SYSTEM, definitionId, null));
        }

        // Add identifiers from resource caches.
        for (String definitionId : resourceLevelDefinitionCache.findDefinitionsByText(searchText))
        {
            handles.add(new T8DefinitionHandle(T8DefinitionLevel.RESOURCE, definitionId, null));
        }

        // Add identifiers from root caches.
        for (String definitionId : rootLevelDefinitionCache.findDefinitionsByText(searchText))
        {
            handles.add(new T8DefinitionHandle(T8DefinitionLevel.ROOT, definitionId, null));
        }

        // Return the complete list of handles.
        return handles;
    }

    @Override
    public List<T8DefinitionHandle> findDefinitionsByRegularExpression(T8Context context, String expression) throws Exception
    {
        List<T8DefinitionHandle> handles;

        // Create a list to hold all definition handles that we find.
        handles = new ArrayList<>();

        // Add identifiers from project caches.
        handles.addAll(projectLevelDefinitionCache.findDefinitionsByRegularExpression(context, expression));

        // Add identifiers from system caches.
        for (String definitionIdentifier : systemLevelDefinitionCache.findDefinitionsByRegularExpression(expression))
        {
            handles.add(new T8DefinitionHandle(T8DefinitionLevel.SYSTEM, definitionIdentifier, null));
        }

        // Add identifiers from resource caches.
        for (String definitionIdentifier : resourceLevelDefinitionCache.findDefinitionsByRegularExpression(expression))
        {
            handles.add(new T8DefinitionHandle(T8DefinitionLevel.RESOURCE, definitionIdentifier, null));
        }

        // Add identifiers from root caches.
        for (String definitionIdentifier : rootLevelDefinitionCache.findDefinitionsByRegularExpression(expression))
        {
            handles.add(new T8DefinitionHandle(T8DefinitionLevel.ROOT, definitionIdentifier, null));
        }

        // Return the complete list of handles.
        return handles;
    }

    @Override
    public List<T8DefinitionHandle> findDefinitionsByIdentifier(T8Context context, String searchIdentifier, boolean includeReferencesOnly) throws Exception
    {
        List<T8DefinitionHandle> handles;

        // Create a list to hold all definition handles that we find.
        handles = new ArrayList<>();

        // Add identifiers from project caches.
        handles.addAll(projectLevelDefinitionCache.findDefinitionsByIdentifier(context, searchIdentifier, includeReferencesOnly));

        // Add identifiers from system caches.
        for (String definitionIdentifier : systemLevelDefinitionCache.findDefinitionsByIdentifier(searchIdentifier))
        {
            if ((!includeReferencesOnly) || (!definitionIdentifier.equals(searchIdentifier)))
            {
                handles.add(new T8DefinitionHandle(T8DefinitionLevel.SYSTEM, definitionIdentifier, null));
            }
        }

        // Add identifiers from resource caches.
        for (String definitionIdentifier : resourceLevelDefinitionCache.findDefinitionsByIdentifier(searchIdentifier))
        {
            if ((!includeReferencesOnly) || (!definitionIdentifier.equals(searchIdentifier)))
            {
                handles.add(new T8DefinitionHandle(T8DefinitionLevel.RESOURCE, definitionIdentifier, null));
            }
        }

        // Add identifiers from root caches.
        for (String definitionIdentifier : rootLevelDefinitionCache.findDefinitionsByIdentifier(searchIdentifier))
        {
            if ((!includeReferencesOnly) || (!definitionIdentifier.equals(searchIdentifier)))
            {
                handles.add(new T8DefinitionHandle(T8DefinitionLevel.ROOT, definitionIdentifier, null));
            }
        }

        // Return the complete list of handles.
        return handles;
    }

    @Override
    public List<String> getDefinitionIdentifiers(String projectId, String typeId) throws Exception
    {
        Set<String> ids;

        ids = new HashSet<>();

        // If a project is specified, add project private definition data first.
        if (projectId != null)
        {
            ids.addAll(projectLevelDefinitionCache.getTypeDefinitionIds(projectId, typeId, true, true, true));
        }
        else
        {
            ids.addAll(projectLevelDefinitionCache.getTypeDefinitionIds(typeId));
        }

        // Add identifiers from system caches.
        ids.addAll(systemLevelDefinitionCache.getDefinitionIdentifiers(typeId));

        // Add identifiers from resource caches.
        ids.addAll(resourceLevelDefinitionCache.getDefinitionIdentifiers(typeId));

        // Add identifiers from root caches.
        ids.addAll(rootLevelDefinitionCache.getDefinitionIdentifiers(typeId));

        return new ArrayList<>(ids);
    }

    @Override
    public List<String> getGroupDefinitionIdentifiers(String projectId, String groupId) throws Exception
    {
        Set<String> ids;

        ids = new HashSet<>();

        // If a project is specified, add project private definition data first.
        if (projectId != null)
        {
            ids.addAll(projectLevelDefinitionCache.getGroupDefinitionIds(projectId, groupId, true, true, true));
        }
        else
        {
            ids.addAll(projectLevelDefinitionCache.getGroupDefinitionIds(groupId));
        }

        // Add identifiers from system caches.
        ids.addAll(systemLevelDefinitionCache.getGroupDefinitionIdentifiers(groupId));

        // Add identifiers from resource caches.
        ids.addAll(resourceLevelDefinitionCache.getGroupDefinitionIdentifiers(groupId));

        // Add identifiers from root caches.
        ids.addAll(rootLevelDefinitionCache.getGroupDefinitionIdentifiers(groupId));

        return new ArrayList<>(ids);
    }

    @Override
    public List<String> getDefinitionGroupIdentifiers() throws Exception
    {
        Set<String> groupIdentifiers;

        groupIdentifiers = new HashSet<>();
        groupIdentifiers.add(T8SystemDefinition.GROUP_IDENTIFIER);
        groupIdentifiers.add(T8ProjectDefinition.GROUP_IDENTIFIER);

        // Add groups from project caches.
        groupIdentifiers.addAll(projectLevelDefinitionCache.getGroupIdentifiers());

        // Add groups from system caches.
        groupIdentifiers.addAll(systemLevelDefinitionCache.getGroupIdentifiers());

        // Add groups from resource caches.
        groupIdentifiers.addAll(resourceLevelDefinitionCache.getGroupIdentifiers());

        return new ArrayList<>(groupIdentifiers);
    }

    @Override
    public List<String> getDefinitionTypeIdentifiers()
    {
        return definitionTypeCache.getTypeIdentifiers();
    }

    @Override
    public List<String> getDefinitionTypeIdentifiers(String groupIdentifier)
    {
        return definitionTypeCache.getTypeIdentifiers(groupIdentifier);
    }

    /**
     * Returns the composition break-down of the specified definition.  The composition is
     * a hierarchical structure of definition meta data, showing all of the composite definitions
     * that make up or is used by the specified root definition, via direct or indirect reference.
     * @param context The context of the user requesting the composition.
     * @param definitionId The id of the definition for which the composition will be requested.
     * @return The composition break-down of the specified definition.
     * @throws Exception
     */
    @Override
    public T8DefinitionComposition getDefinitionComposition(T8Context context, String projectId, String definitionId, boolean includeDescendants) throws Exception
    {
        T8Definition rootDefinition;

        // Get the root definition for which a break-down composition will be created.
        rootDefinition = getRawDefinition(context, projectId, definitionId);
        if (rootDefinition != null)
        {
            LinkedList<T8DefinitionComposition> definitionQueue;
            T8DefinitionComposition rootComposition;

            // Create the root composition and add it to the processing queue.
            rootComposition = new T8DefinitionComposition(rootDefinition.getTypeMetaData(), rootDefinition.getMetaData());
            definitionQueue = new LinkedList<T8DefinitionComposition>();
            definitionQueue.add(rootComposition);
            while (!definitionQueue.isEmpty())
            {
                T8DefinitionComposition nextComposition;
                T8Definition nextDefinition;

                // Get the next composition from the queue for processing.
                nextComposition = definitionQueue.removeFirst();
                nextDefinition = getRawDefinition(context, nextComposition.getProjectId(), nextComposition.getDefinitionId());
                if (nextDefinition != null)
                {
                    Set<String> referenceIds;
                    List<String> lineIds;

                    // Get the LINE id's of the next composition (the ids from the composition to the root).
                    lineIds = nextComposition.getLineIds();
                    referenceIds = nextDefinition.getReferenceIds(false, true, true, true);
                    for (String referenceId : referenceIds)
                    {
                        // Make sure that we are not running into a cyclic reference.
                        if (!lineIds.contains(referenceId))
                        {
                            T8Definition referenceDefinition;

                            // Retrieve the definition referred to.
                            referenceDefinition = getRawDefinition(context, nextDefinition.getRootProjectId(), referenceId);
                            if (referenceDefinition != null)
                            {
                                T8DefinitionComposition referenceComposition;

                                // Create a composition for the reference and add the composition to the queue for processing.
                                referenceComposition = new T8DefinitionComposition(referenceDefinition.getTypeMetaData(), referenceDefinition.getMetaData());
                                nextComposition.addReference(referenceComposition);

                                // Add the reference to the processing queue if descendants are to be included.
                                if (includeDescendants) definitionQueue.add(referenceComposition);
                            }
                        }
                    }
                }
            }

            // Return the root composition as result.
            return rootComposition;
        }
        else throw new Exception("Definition not found.");
    }

    @Override
    public T8DefinitionMetaData getDefinitionMetaData(String projectId, String definitionId) throws Exception
    {
        T8DefinitionMetaData metaData;

        // Check the root-level cache for the required definition.
        metaData = rootLevelDefinitionCache.getDefinitionMetaData(definitionId);
        if (metaData != null) return metaData;

        // Check the resource-level cache for the required definition.
        metaData = resourceLevelDefinitionCache.getDefinitionMetaData(definitionId);
        if (metaData != null) return metaData;

        // Check the system-level cache for the required definition.
        metaData = systemLevelDefinitionCache.getDefinitionMetaData(definitionId);
        if (metaData != null) return metaData;

        // Finally, attempt to fetch the specified meta data from the project definition cache.
        metaData = projectLevelDefinitionCache.getDefinitionMetaData(projectId, definitionId);
        return metaData;
    }

    @Override
    public T8DefinitionMetaData getDefinitionMetaData(T8DefinitionHandle definitionHandle) throws Exception
    {
        T8DefinitionLevel level;
        String definitionId;

        // Get the details of the definition to be loaded.
        definitionId = definitionHandle.getDefinitionIdentifier();
        level = definitionHandle.getDefinitionLevel();

        // If the definition handle refers to the setup definition, return it.
        if (setupDefinition.getIdentifier().equals(definitionId)) return setupDefinition.getMetaData();

        // Find the definition in the applicable cache according to its storage level.
        if ((level == T8DefinitionLevel.ROOT) && (rootLevelDefinitionCache.containsDefinition(definitionId)))
        {
            return rootLevelDefinitionCache.getDefinitionMetaData(definitionId);
        }
        else if ((level == T8DefinitionLevel.SYSTEM) && (systemLevelDefinitionCache.containsDefinition(definitionId)))
        {
            return systemLevelDefinitionCache.getDefinitionMetaData(definitionId);
        }
        else if (level == T8DefinitionLevel.PROJECT)
        {
            return projectLevelDefinitionCache.getDefinitionMetaData(definitionHandle.getProjectIdentifier(), definitionId);
        }
        else if (level == T8DefinitionLevel.RESOURCE)
        {
            if (resourceLevelDefinitionCache.containsDefinition(definitionId))
            {
                return resourceLevelDefinitionCache.getDefinitionMetaData(definitionId);
            }
        }

        // Definition not found.
        return null;
    }

    @Override
    public List<T8DefinitionMetaData> getDefinitionMetaData(List<T8DefinitionHandle> handles) throws Exception
    {
        List<T8DefinitionMetaData> metaDataList;

        // Fetch the meta data for each handle.
        metaDataList = new ArrayList<>();
        for (T8DefinitionHandle handle : handles)
        {
            T8DefinitionMetaData metaData;

            metaData = getDefinitionMetaData(handle);
            if (metaData != null)
            {
                metaDataList.add(metaData);
            }
        }

        // Return the completed list.
        return metaDataList;
    }

    @Override
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String projectId, String typeId) throws Exception
    {
        List<T8DefinitionMetaData> metaList;

        // If a project is specified, add project private definition data first.
        metaList = new ArrayList<>();
        if (projectId != null)
        {
            metaList.addAll(projectLevelDefinitionCache.getTypeDefinitionMetaData(projectId, typeId, false, true, true));
        }
        else
        {
            metaList.addAll(projectLevelDefinitionCache.getTypeDefinitionMetaData(typeId));
        }

        // Add system-specific meta data.
        metaList.addAll(systemLevelDefinitionCache.getTypeDefinitionMetaData(typeId));

        // Add resource-level meta data.
        metaList.addAll(resourceLevelDefinitionCache.getTypeDefinitionMetaData(typeId));

        // Add root-level meta data.
        metaList.addAll(rootLevelDefinitionCache.getTypeDefinitionMetaData(typeId));
        return metaList;
    }

    @Override
    public List<T8DefinitionMetaData> getProjectDefinitionMetaData(String projectId) throws Exception
    {
        return projectLevelDefinitionCache.getProjectDefinitionMetaData(projectId);
    }

    @Override
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String projectId, String groupId) throws Exception
    {
        List<T8DefinitionMetaData> metaData;

        // Create the collection.
        metaData = new ArrayList<>();

        // If a project is specified, add project private definition data first.
        if (projectId != null)
        {
            metaData.addAll(projectLevelDefinitionCache.getGroupDefinitionMetaData(projectId, groupId, true, true, true));
        }
        else
        {
            metaData.addAll(projectLevelDefinitionCache.getGroupDefinitionMetaData(groupId));
        }

        // Add all other system-level definition meta data.
        metaData.addAll(systemLevelDefinitionCache.getGroupDefinitionMetaData(groupId));

        // Add all other resource-level definition meta data.
        metaData.addAll(resourceLevelDefinitionCache.getGroupDefinitionMetaData(groupId));

        // Add all other root-level definition meta data.
        metaData.addAll(rootLevelDefinitionCache.getGroupDefinitionMetaData(groupId));

        return metaData;
    }

    @Override
    public T8DefinitionTypeMetaData getDefinitionTypeMetaData(String typeId)
    {
        return definitionTypeCache.getDefinitionTypeMetaData(typeId);
    }

    @Override
    public List<T8DefinitionTypeMetaData> getGroupDefinitionTypeMetaData(String groupId)
    {
        return definitionTypeCache.getGroupDefinitionTypeMetaData(groupId);
    }

    @Override
    public List<T8DefinitionGroupMetaData> getAllDefinitionGroupMetaData()
    {
        return definitionTypeCache.getDefinitionGroupMetaData();
    }

    @Override
    public List<T8DefinitionTypeMetaData> getAllDefinitionTypeMetaData()
    {
        return definitionTypeCache.getDefinitionTypeMetaData();
    }

    @Override
    public List<T8DefinitionMetaData> getAllDefinitionMetaData(T8Context context, T8DefinitionFilter definitionFilter) throws Exception
    {
        if (definitionFilter != null) LOGGER.log(T8Logger.Level.WARNING, "Definition Filter will be ignored. Filter: " + definitionFilter);
        Map<String, T8DefinitionMetaData> metaDataMap;

        metaDataMap = new HashMap<>();

        // Add Project definition meta data.
        for (T8DefinitionMetaData metaData : projectLevelDefinitionCache.getAllDefinitionMetaData())
        {
            metaDataMap.put(metaData.getId(), metaData);
        }

        // Add System-level definition meta data.
        for (T8DefinitionMetaData metaData : systemLevelDefinitionCache.getAllDefinitionMetaData())
        {
            metaDataMap.put(metaData.getId(), metaData);
        }

        // Add Resource-level definition meta data.
        resourceLevelDefinitionCache.getAllDefinitionMetaData().stream().forEach((metaData) ->
        {
            metaDataMap.put(metaData.getId(), metaData);
        });

        // Add Root-level definition meta data.
        rootLevelDefinitionCache.getAllDefinitionMetaData().stream().forEach((metaData) ->
        {
            metaDataMap.put(metaData.getId(), metaData);
        });

        return new ArrayList<>(metaDataMap.values());
    }

    @Override
    public List<T8DefinitionHandle> getAllDefinitionHandles(T8Context context, T8DefinitionFilter definitionFilter) throws Exception
    {
        List<T8DefinitionHandle> handles;

        handles = new ArrayList<>();
        if (definitionFilter == null)
        {
            // TODO : Implement addition of all definition handles from all project and system caches.
            return handles;
        }
        else
        {
            // TODO:  Implement use of filter to identify definitions for which to return handles.
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    @Override
    public void importDefinition(T8Context context, T8Definition definition) throws Exception
    {
        T8DefinitionTypeMetaData typeMetaData;

        // Check for system reserved definition types and save them separately.  All other types are saved to the project-specific caches.
        typeMetaData = definition.getTypeMetaData();
        if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.ROOT)
        {
            // Make sure the root definition cache saves this type and then save it.
            if (rootLevelDefinitionCache.savesType(typeMetaData))
            {
                rootLevelDefinitionCache.saveDefinition(definition);
                fireDefinitionImportedEvent(context, definition);
            }
            else throw new Exception("No applicable definition cache found for root definition type: " + typeMetaData.getTypeId());
        }
        else if (typeMetaData.getDefinitionLevel() == T8DefinitionLevel.SYSTEM)
        {
            // Make sure the system definition cache saves this type and then save it.
            if (systemLevelDefinitionCache.savesType(typeMetaData))
            {
                systemLevelDefinitionCache.saveDefinition(definition);
                fireDefinitionImportedEvent(context, definition);
            }
            else throw new Exception("No applicable definition cache found for system definition type: " + typeMetaData.getTypeId());
        }
        else
        {
            projectLevelDefinitionCache.saveDefinition(definition);
        }
    }

    @Override
    public boolean deleteDefinition(T8Context context, T8DefinitionHandle definitionHandle, boolean safeDeletion, String keyIdentifier) throws Exception
    {
        T8Definition definitionToDelete;

        definitionToDelete = getRawDefinition(context, definitionHandle);
        if (definitionToDelete != null)
        {
            String definitionId;
            String projectId;

            // Check the definition update permission.
            projectId = definitionHandle.getProjectIdentifier();
            definitionId = definitionHandle.getDefinitionIdentifier();
            checkUpdatePermission(context, definitionToDelete);

            // If it is 'safe' deletion, we need to check for other definitions that use the one to be deleted.
            if (safeDeletion)
            {
                List<T8DefinitionHandle> usedByDefinitionHandles;

                // If the definition to be deleted does not override any other definition then we have to do further checks for usage.  Otherwise we can just delete it.
                usedByDefinitionHandles = findDefinitionsUsing(context, projectId, definitionId);
                usedByDefinitionHandles.remove(definitionHandle); // Remove the handle of the definition we are deleting.
                if (usedByDefinitionHandles.isEmpty())
                {
                    deleteDefinitionUnsafe(definitionHandle);
                    fireDefinitionDeletedEvent(context, definitionToDelete);
                    return true;
                }
                else throw new Exception("The definition '" + definitionId + "' cannot be deleted because it is used by " + usedByDefinitionHandles.size() + " other definitions.");
            }
            else
            {
                deleteDefinitionUnsafe(definitionHandle);
                fireDefinitionDeletedEvent(context, definitionToDelete);
                return true;
            }
        }
        else throw new Exception("Definition not found for deletion: " + definitionHandle);
    }

    @Override
    public T8DataType createDataType(String dataTypeString)
    {
        return dataTypeCache.createDataType(dataTypeString);
    }

    @Override
    public T8DataType getObjectDataType(Object object)
    {
        if (object != null)
        {
            if (object instanceof ArrayList)
            {
                ArrayList list;

                list = (ArrayList)object;
                if (list.size() > 0)
                {
                    T8DataType elementDataType;

                    // Determine the element data type.
                    elementDataType = null;
                    for (Object element : list)
                    {
                        if (element != null)
                        {
                            elementDataType = getObjectDataType(element);
                            break;
                        }
                    }
                    if (elementDataType == null) elementDataType = T8DataType.UNDEFINED;

                    // Create the list data type.
                    return new T8DtList(elementDataType);
                }
                else return new T8DtList(new T8DtUndefined(this));
            }
            else if (object instanceof HashMap)
            {
                HashMap map;

                map = (HashMap)object;
                if (map.size() > 0)
                {
                    T8DataType valueDataType;
                    T8DataType keyDataType;

                    // Determine the key data type.
                    keyDataType = null;
                    for (Object key : map.keySet())
                    {
                        if (key != null)
                        {
                            keyDataType = getObjectDataType(key);
                            break;
                        }
                    }
                    if (keyDataType == null) keyDataType = T8DataType.UNDEFINED;

                    // Determine the value data type.
                    valueDataType = null;
                    for (Object value : map.values())
                    {
                        if (value != null)
                        {
                            valueDataType = getObjectDataType(value);
                            break;
                        }
                    }
                    if (valueDataType == null) valueDataType = T8DataType.UNDEFINED;

                    // Create the map data type.
                    return new T8DtMap(keyDataType, valueDataType);
                }
                else return new T8DtMap(new T8DtUndefined(this), new T8DtUndefined(this));
            }
            else
            {
                T8DataType commonDataType;
                String objectClassName;

                objectClassName = object.getClass().getCanonicalName();
                commonDataType = dataTypeCache.getCommonDataTypeByObjectClass(objectClassName);
                if (commonDataType != null) return commonDataType;
                else // No commong type found, so create the type.
                {
                    return dataTypeCache.createNewDataTypeForObject(objectClassName);
                }
            }
        }
        else return T8DataType.UNDEFINED;
    }

    /**
     * Deletes the specified projects and all of the definitions they contains if the current session has the required access rights.
     * @param context
     * @param projectIds The ids of the projects to delete.
     * @throws Exception
     */
    @Override
    public void deleteProjects(T8Context context, List<String> projectIds) throws Exception
    {
        // First check permissions for all projects to be deleted.
        for (String projectId : projectIds)
        {
            T8ProjectDefinition projectDefinition;

            projectDefinition = projectDefinitions.get(projectId);
            if (projectDefinition != null)
            {
                checkUpdatePermission(context, projectDefinition);
            }
            else throw new Exception("Project not found: " + projectId);
        }

        // Now delete all projects.
        for (String projectId : projectIds)
        {
            T8ProjectDefinition projectDefinition;

            // Get the project definition.
            projectDefinition = projectDefinitions.get(projectId);

            // Backup the existing project.
            projectLevelDefinitionCache.backupProject(projectId);

            // Delete the existing project.
            projectLevelDefinitionCache.deleteProject(projectId);

            // Delete the project definition.
            deleteDefinitionUnsafe(projectDefinition.getHandle());
            projectDefinitions.remove(projectId);
        }
    }

    /**
     * Imports the definition file located at the specified location in the specified file context.
     * @param context
     * @param fileContextIid The instance id of the file context where the file to import is located.
     * @param filePath The path within the file context where the file to import is located.
     * @throws Exception
     */
    @Override
    public T8ProjectImportReport importProjects(T8Context context, String fileContextIid, String filePath, List<String> projectIds) throws Exception
    {
        T8ProjectPackage projectPackage;
        T8ProjectPackageInputFileHandler fileHandler;
        T8FileManager fileManager;

        // Get the applicable file handler.
        fileManager = serverContext.getFileManager();
        fileHandler = new T8ProjectPackageInputFileHandler(context, fileManager, fileContextIid, filePath);

        // Import the project data.
        try
        {
            T8ProjectImportReport report;

            // Open the file handler and read the definition data from the target file.
            fileHandler.open();
            projectPackage = fileHandler.readPackage();

            // Import each of the specified projects.
            report = new T8ProjectImportReport();
            for (String projectId : projectIds)
            {
                T8ProjectDefinition projectDefinition;

                // Get the project definition from the pacage.
                projectDefinition = projectPackage.getProjectDefinition(projectId);
                if (projectDefinition != null)
                {
                    try
                    {
                        List<T8Definition> contentDefinitions;

                        // If the project to be imported already exists, clear it.
                        if (projectLevelDefinitionCache.containsProjectDefinitionCache(projectId))
                        {
                            // Backup the existing project.
                            projectLevelDefinitionCache.backupProject(projectId);

                            // Delete the existing project.
                            projectLevelDefinitionCache.clearProject(projectId);
                        }

                        // Save the project definition (creating a new project cache if required).
                        saveDefinition(context, projectDefinition, null);

                        // Save each one of the content definitions individually.
                        contentDefinitions = projectPackage.getContentDefinitions(projectId);
                        for (T8Definition contentDefinition : contentDefinitions)
                        {
                            saveDefinition(context, contentDefinition, null);
                        }

                        // Add the successful result to the report.
                        report.addResults(new T8ProjectImportResult(projectId, true, "Imported " + contentDefinitions.size() + " Definitions"));
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while importing project " + projectDefinition + " from: " + filePath, e);
                        report.addResults(new T8ProjectImportResult(projectId, false, ExceptionUtilities.getRootCauseMessage(e)));
                    }
                }
            }

            // Return the report containing results for all projects imported.
            return report;
        }
        finally
        {
            fileHandler.close();
        }
    }

    /**
     * Exports the specified projects to the specified file context.  If no file context is specified
     * @param context
     * @param fileContextIid
     * @param filePath
     * @param projectIds
     * @throws Exception
     */
    @Override
    public void exportProjects(T8Context context, String fileContextIid, String filePath, List<String> projectIds) throws Exception
    {
        T8ProjectPackageOutputFileHandler fileHandler = null;
        T8ProjectPackage projectPackage;
        T8FileManager fileManager;

        // Load the definitions to the exported.
        projectPackage = new T8ProjectPackage();
        for (String projectId : projectIds)
        {
            T8ProjectDefinition projectDefinition;

            // Get the definition of the project to export.
            projectDefinition = projectDefinitions.get(projectId);
            if (projectDefinition != null)
            {
                List<T8DefinitionMetaData> contentMetaData;

                projectPackage.addProject(projectDefinition);
                contentMetaData = projectLevelDefinitionCache.getProjectDefinitionMetaData(projectId);
                for (T8DefinitionMetaData metaData : contentMetaData)
                {
                    T8Definition contentDefinition;

                    contentDefinition = projectLevelDefinitionCache.loadDefinition(projectId, metaData.getId());
                    projectPackage.addContentDefinition(contentDefinition);
                }
            }
            else throw new Exception("Project not found: " + projectId);
        }

        // Get the applicable file handler and write the projects to the output file.
        try
        {
            fileManager = serverContext.getFileManager();
            fileHandler = new T8ProjectPackageOutputFileHandler(context, fileManager, fileContextIid, filePath);
            fileHandler.open();
            fileHandler.writePackage(projectPackage);
        }
        finally
        {
            if (fileHandler != null) fileHandler.close();
        }
    }
}
