package com.pilog.t8.script;

import com.pilog.epic.EPIC;
import com.pilog.epic.ParserContext;
import com.pilog.epic.Program;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.tag.T8DataTagFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.exception.T8RaisedException;
import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.operation.progressreport.T8DefaultCategorizedProgressReport;
import com.pilog.t8.operation.progressreport.T8DefaultProgressReport;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.procedure.T8Procedure;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.time.T8Date;
import com.pilog.t8.time.T8Time;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyAbbreviation;
import com.pilog.t8.data.ontology.T8OntologyCode;
import com.pilog.t8.data.ontology.T8OntologyConcept;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8OntologyDefinition;
import com.pilog.t8.data.ontology.T8OntologyTerm;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.data.org.T8OntologyLink;
import com.pilog.t8.definition.exception.T8ExceptionDefinition;
import com.pilog.t8.definition.procedure.T8ProcedureDefinition;
import com.pilog.t8.definition.script.T8ScriptClassImport;
import com.pilog.t8.definition.script.T8ScriptDefinition;
import com.pilog.t8.definition.script.T8ScriptMethodImport;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.functionality.T8FunctionalityAccessHandle;
import com.pilog.t8.functionality.T8FunctionalityManagerScriptFacade;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8UserDetails;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.utilities.strings.Strings;
import java.lang.reflect.Method;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.time.T8TimeUnit;
import com.pilog.t8.data.document.path.DocPathEvaluatorFactory;
import com.pilog.t8.data.document.path.EpicDocPathParser;
import com.pilog.t8.process.T8ServerProcessManagerScriptFacade;
import com.pilog.t8.time.T8Day;
import com.pilog.t8.time.T8Hour;
import com.pilog.t8.time.T8Millisecond;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.time.T8Second;
import com.pilog.t8.time.T8Year;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultServerContextScript implements T8ServerContextScript
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultServerContextScript.class);

    protected final T8ServerContext serverContext;
    protected final T8SessionContext sessionContext;
    private final T8Context context;
    private final T8ScriptDefinition scriptDefinition;
    private Program program;
    private double progress;
    private T8ProgressReport progressReport;
    private final Object progressLock = new Object();
    private final T8ScriptExecutionHandle executionHandle;
    private final Map<String, T8Procedure> procedureCache;
    private final DocPathEvaluatorFactory docPathFactory;
    private DocPathExpressionEvaluator docPathExpressionEvaluator;
    private volatile T8Procedure currentProcedure; // Stores a reference to the currently executing procedure (if any).
    private double currentProcedureExecutionWeight; // Stores the weight of the currently executing procedure's progress relative to this script's overall progress.

    public T8DefaultServerContextScript(T8Context context, T8ServerContextScriptDefinition definition)
    {
        this.context = context;
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.scriptDefinition = definition;
        this.executionHandle = new T8ScriptExecutionHandle();
        this.procedureCache = new HashMap<String, T8Procedure>();
        this.docPathFactory = DocPathEvaluatorFactory.getFactory(context);
        this.docPathFactory.setLanguage(sessionContext.getContentLanguageIdentifier());
        this.progress = 0.0;
    }

    protected T8ServerContext getServerContext()
    {
        return this.serverContext;
    }

    protected T8Context getAccessContext()
    {
        return this.context;
    }

    protected T8ScriptExecutionHandle getExecutionHandle()
    {
        return this.executionHandle;
    }

    protected Program getProgram()
    {
        return this.program;
    }

    protected DocPathEvaluatorFactory getDocPathEvaluatorFactory()
    {
        return this.docPathFactory;
    }

    @Override
    public double getProgress()
    {
        synchronized(progressLock)
        {
            // If a procedure is currently executing, take account of its progress.
            if (currentProcedure != null)
            {
                return progress + (currentProcedure.getProgress() * currentProcedureExecutionWeight);
            }
            else return progress;
        }
    }

    @Override
    public T8ProgressReport getProgressReport()
    {
        return progressReport;
    }

    @Override
    public void prepareScript() throws Exception
    {
        ParserContext parserContext;

        // Create the parser context.
        parserContext = new ParserContext();
        parserContext.addExternalExpressionParser(new EpicDocPathParser(docPathFactory));

        // Add all standard server-side imports to the EPIC program.
        addProgramImports(parserContext);

        // Get the script namespace and compiled the script into an EPIC program.
        program = EPIC.compileProgram(scriptDefinition.getScript(), "UTF-8", parserContext);
    }

    @Override
    public void finalizeScript() throws Exception
    {
    }

    @Override
    public Map<String, Object> executePreparedScript(Map<String, ? extends Object> inputParameters) throws Exception
    {
        Map<String, Object> scriptInputParameters;
        Object scriptOutputObject;
        T8DataTransaction tx;
        T8DataSession dataSession;

        // Immediately reset the progress.
        progress = 0.0;

        // Get the script namespace and script parameters.
        scriptInputParameters = T8IdentifierUtilities.stripNamespace(scriptDefinition.getNamespace(), inputParameters, false);
        if (scriptInputParameters == null)
        {
            scriptInputParameters = new HashMap<>();
        }

        // Get the script transaction to use.
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.getTransaction();
        if (tx == null)
        {
            // If this script requires a transaction but no open transaction exists, the invoking thread is either
            // not using a transaction or is using an instant transaction and in that case so should this script.
            if (program.isVariableDeclared("tx")) tx = dataSession.instantTransaction();
        }

        // Add the default script parameters.
        scriptInputParameters.put("tx", tx);
        scriptInputParameters.put("ds", dataSession);
        scriptInputParameters.put("ctx", context);
        scriptInputParameters.put("session", sessionContext);
        scriptInputParameters.put("server", serverContext);
        scriptInputParameters.put("processManager", new T8ServerProcessManagerScriptFacade(context, serverContext.getProcessManager()));
        scriptInputParameters.put("communicationManager", new T8CommunicationManagerScriptFacade(context, serverContext.getCommunicationManager()));
        scriptInputParameters.put("functionalityManager", new T8FunctionalityManagerScriptFacade(context, serverContext.getFunctionalityManager()));
        scriptInputParameters.put("flowManager", new T8FlowManagerScriptFacade(context, serverContext.getFlowManager()));
        scriptInputParameters.put("definitionManager", new T8DefinitionManagerScriptFacade(context, serverContext.getDefinitionManager()));
        scriptInputParameters.put("fileManager", new T8FileManagerScriptFacade(context, serverContext.getFileManager()));
        scriptInputParameters.put("executionHandle", executionHandle);

        // Execute the program.
        try
        {
            program.execute(scriptInputParameters);
        }
        catch (EPICRuntimeException e)
        {
            // Log the exception.
            LOGGER.log("Exception while executing script '" + scriptDefinition + "' using input parameters: " + inputParameters, e);
            throw e;
        }

        // Get the EPIC program output and convert the output parameters to the correct namespace.
        scriptOutputObject = program.getReturnObject();
        if ((scriptOutputObject == null) || (scriptOutputObject instanceof Map))
        {
            Map<String, Object> scriptOutputParameters;

            // Convert all output parameters to script output parameters.
            scriptOutputParameters = (Map<String, Object>)scriptOutputObject;
            scriptOutputParameters = T8IdentifierUtilities.prependNamespace(scriptDefinition.getNamespace(), scriptOutputParameters, false);
            return scriptOutputParameters;
        }
        else throw new Exception("Script '" + scriptDefinition + "' did not return a Map result.  Return type: " + scriptOutputObject.getClass());
    }

    @Override
    public Map<String, Object> executeScript(Map<String, ? extends Object> inputParameters) throws Exception
    {
        Map<String, Object> scriptOutputParameters;

        // Start a new data transaciton.
        prepareScript();

        // Run the prepared script and do some cleanup.
        try
        {
            // Run the prepared script.
            scriptOutputParameters = executePreparedScript(inputParameters);

            // Return the script output parameters.
            return scriptOutputParameters;
        }
        finally
        {
            // Finalize the script.
            finalizeScript();
        }
    }

    protected void addProgramImports(ParserContext parserContext) throws Exception
    {
        List<T8ScriptClassImport> classImports;
        List<T8ScriptMethodImport> methodImports;

        // Class imports.
        parserContext.addClassImport(T8IdentifierUtilities.class.getSimpleName(), T8IdentifierUtilities.class);
        parserContext.addClassImport(T8DataUtilities.class.getSimpleName(), T8DataUtilities.class);
        parserContext.addClassImport(T8DataFilter.class.getSimpleName(), T8DataFilter.class);
        parserContext.addClassImport(T8DataFilterCriteria.class.getSimpleName(), T8DataFilterCriteria.class);
        parserContext.addClassImport(T8DataFilterCriterion.class.getSimpleName(), T8DataFilterCriterion.class);
        parserContext.addClassImport(T8DataFilterCriterion.DataFilterConjunction.class.getSimpleName(), T8DataFilterCriterion.DataFilterConjunction.class);
        parserContext.addClassImport(T8DataFilterCriterion.DataFilterOperator.class.getSimpleName(), T8DataFilterCriterion.DataFilterOperator.class);
        parserContext.addClassImport(T8DataTagFilter.class.getSimpleName(), T8DataTagFilter.class);
        parserContext.addClassImport(T8OntologyConcept.class.getSimpleName(), T8OntologyConcept.class);
        parserContext.addClassImport(T8OntologyTerm.class.getSimpleName(), T8OntologyTerm.class);
        parserContext.addClassImport(T8OntologyAbbreviation.class.getSimpleName(), T8OntologyAbbreviation.class);
        parserContext.addClassImport(T8OntologyDefinition.class.getSimpleName(), T8OntologyDefinition.class);
        parserContext.addClassImport(T8OntologyCode.class.getSimpleName(), T8OntologyCode.class);
        parserContext.addClassImport(T8OntologyLink.class.getSimpleName(), T8OntologyLink.class);
        parserContext.addClassImport(T8OntologyConcept.class.getSimpleName(), T8OntologyConcept.class);
        parserContext.addClassImport(T8OntologyTerm.class.getSimpleName(), T8OntologyTerm.class);
        parserContext.addClassImport(T8OntologyAbbreviation.class.getSimpleName(), T8OntologyAbbreviation.class);
        parserContext.addClassImport(T8OntologyDefinition.class.getSimpleName(), T8OntologyDefinition.class);
        parserContext.addClassImport(T8OntologyCode.class.getSimpleName(), T8OntologyCode.class);
        parserContext.addClassImport(T8OntologyConceptType.class.getSimpleName(), T8OntologyConceptType.class);
        parserContext.addClassImport(T8PrimaryOntologyDataType.class.getSimpleName(), T8PrimaryOntologyDataType.class);
        parserContext.addClassImport(RequirementType.class.getSimpleName(), RequirementType.class);
        parserContext.addClassImport(T8Timestamp.class.getSimpleName(), T8Timestamp.class);
        parserContext.addClassImport(T8Time.class.getSimpleName(), T8Time.class);
        parserContext.addClassImport(T8Date.class.getSimpleName(), T8Date.class);
        parserContext.addClassImport(Strings.class.getSimpleName(), Strings.class);

        // Add all class imports that are specific to this script type.
        classImports = scriptDefinition.getScriptClassImports();
        if (classImports != null)
        {
            for (T8ScriptClassImport classImport : classImports)
            {
                parserContext.addClassImport(classImport.getReference(), classImport.getClassToImport());
            }
        }

        // Method imports.
        parserContext.addMethodImport("accessFunctionality", this, T8DefaultServerContextScript.class.getDeclaredMethod("accessFunctionality", String.class, Map.class));
        parserContext.addMethodImport("docPath", this, T8DefaultServerContextScript.class.getDeclaredMethod("docPath", Object.class, String.class));
        parserContext.addMethodImport("getFlowTaskListSummary", this, T8DefaultServerContextScript.class.getDeclaredMethod("getFlowTaskListSummary"));
        parserContext.addMethodImport("getFlowTaskListSummary", this, T8DefaultServerContextScript.class.getDeclaredMethod("getFlowTaskListSummary", T8TaskFilter.class));
        parserContext.addMethodImport("getSystemProperty", this, T8DefaultServerContextScript.class.getDeclaredMethod("getSystemProperty", String.class));
        parserContext.addMethodImport("getSystemDisplayName", this, T8DefaultServerContextScript.class.getDeclaredMethod("getSystemDisplayName"));
        parserContext.addMethodImport("getUserDetails", this, T8DefaultServerContextScript.class.getDeclaredMethod("getUserDetails", String.class));
        parserContext.addMethodImport("startFlow", this, T8DefaultServerContextScript.class.getDeclaredMethod("startFlow", String.class, Map.class));
        parserContext.addMethodImport("signalFlow", this, T8DefaultServerContextScript.class.getDeclaredMethod("signalFlow", String.class, Map.class));
        parserContext.addMethodImport("createNewGUID", this, T8DefaultServerContextScript.class.getDeclaredMethod("createNewGUID"));
        parserContext.addMethodImport("getTimeUnit", this, T8DefaultServerContextScript.class.getDeclaredMethod("getTimeUnit", String.class, Integer.class));
        parserContext.addMethodImport("getTime", this, T8DefaultServerContextScript.class.getDeclaredMethod("getTime"));
        parserContext.addMethodImport("execProc", this, T8DefaultServerContextScript.class.getDeclaredMethod("executeProcedure", String.class, Map.class, String.class, Double.class));
        parserContext.addMethodImport("execSynchServerOp", this, T8DefaultServerContextScript.class.getDeclaredMethod("executeSynchronousServerOperation", String.class, Map.class));
        parserContext.addMethodImport("execAsynchServerOp", this, T8DefaultServerContextScript.class.getDeclaredMethod("executeAsynchronousServerOperation", String.class, Map.class));
        parserContext.addMethodImport("execSynchServerProc", this, T8DefaultServerContextScript.class.getDeclaredMethod("executeSynchronousServerProcess", String.class, Map.class));
        parserContext.addMethodImport("execAsynchServerProc", this, T8DefaultServerContextScript.class.getDeclaredMethod("executeAsynchronousServerProcess", String.class, Map.class));
        parserContext.addMethodImport("translate", this, T8DefaultServerContextScript.class.getDeclaredMethod("translate", String.class, Object[].class));
        parserContext.addMethodImport("translateTo", this, T8DefaultServerContextScript.class.getDeclaredMethod("translate", String.class, String.class, Object[].class));
        parserContext.addMethodImport("generateKey", this, T8DefaultServerContextScript.class.getDeclaredMethod("generateKey", String.class));
        parserContext.addMethodImport("sendCommunication", this, T8DefaultServerContextScript.class.getDeclaredMethod("sendCommunication", String.class, Map.class));
        parserContext.addMethodImport("sendNotification", this, T8DefaultServerContextScript.class.getDeclaredMethod("sendNotification", String.class, Map.class));
        parserContext.addMethodImport("getAPI", this, T8DefaultServerContextScript.class.getDeclaredMethod("getAPI", String.class));
        parserContext.addMethodImport("isUserRegistered", this, T8DefaultServerContextScript.class.getDeclaredMethod("isUserRegistered", String.class));
        parserContext.addMethodImport("setProgress", this, T8DefaultServerContextScript.class.getDeclaredMethod("setProgress", Double.class));
        parserContext.addMethodImport("setProgressReport", this, T8DefaultServerContextScript.class.getDeclaredMethod("setProgressReport", String.class, String.class, Double.class));
        parserContext.addMethodImport("setCategorizedProgressReport", this, T8DefaultServerContextScript.class.getDeclaredMethod("setCategorizedProgressReport", String.class, String.class, Double.class, String.class, String.class, Double.class));
        parserContext.addMethodImport("raiseException", this, T8DefaultServerContextScript.class.getDeclaredMethod("raiseException", String.class, String.class, Map.class));
        parserContext.addMethodImport("requestDataAccess", this, T8DefaultServerContextScript.class.getDeclaredMethod("requestDataAccess", String.class, String.class));

        // Add all method imports that are specific to this script type.
        methodImports = scriptDefinition.getScriptMethodImports();
        if (methodImports != null)
        {
            for (T8ScriptMethodImport methodImport : methodImports)
            {
                parserContext.addMethodImport(methodImport.getProcedureName(), methodImport.getContextObject(), methodImport.getMethod());
            }
        }
    }

    @Override
    public void addMethodImport(String procedureName, Object contextObject, Method method)
    {
        program.addImport(procedureName, contextObject, method);
    }

    public T8FunctionalityAccessHandle accessFunctionality(String functionalityId, Map<String, Object> parameters) throws Exception
    {
        T8FunctionalityManager functionalityManager;

        functionalityManager = serverContext.getFunctionalityManager();
        return functionalityManager.accessFunctionality(context, functionalityId, parameters);
    }

    /**
     * This method requests the specified access rights to records based on a specific Data Requirement Instance.
     * The aim of this method in future, is to validate the rights of the current session before simple granting access.
     * Currently this method always grants the requested access.
     * @param drInstanceId The Data Requirement Instance for which data access is requested.
     * @param dataAccessId The id of the access requested.
     */
    public void requestDataAccess(String drInstanceId, String dataAccessId)
    {
        if (context != null)
        {
            context.setDataAccessId(drInstanceId, dataAccessId);
        }
    }

    public T8TimeUnit getTimeUnit(String unitType, Integer number)
    {
        switch (unitType)
        {
            case "MILLISECOND":
                return new T8Millisecond(number);
            case "SECOND":
                return new T8Second(number);
            case "MINUTE":
                return new T8Minute(number);
            case "HOUR":
                return new T8Hour(number);
            case "DAY":
                return new T8Day(number);
            case "YEAR":
                return new T8Year(number);
            default:
                throw new IllegalArgumentException("Unsupported T8TimeUnit type: " + unitType);
        }
    }

    public long getTime()
    {
        return System.currentTimeMillis();
    }

    public Object docPath(Object documentObject, String expression)
    {
        try
        {
            // Fetch the DocPath expression evaluator if we don't have one yet.
            if (docPathExpressionEvaluator == null)
            {
                docPathExpressionEvaluator = docPathFactory.getEvaluator();
            }

            // Evaluate the expression.
            return docPathExpressionEvaluator.evaluateExpression(documentObject, expression);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while evaluating DocPath " + expression + " using input: " + documentObject, e);
        }
    }

    public void setProgress(Double progress)
    {
        this.progress = progress;
    }

    public void setProgressReport(String title, String message, Double progress)
    {
        this.progress = progress;
        synchronized(progressLock)
        {
            this.progressReport = new T8DefaultProgressReport(title, message, progress);
        }
    }

    public void setCategorizedProgressReport(String progressTitle, String message, Double progress, String categoryTitle, String categoryMessage, Double categoryProgress)
    {
        this.progress = progress;
        synchronized(progressLock)
        {
            this.progressReport = new T8DefaultCategorizedProgressReport(progressTitle, message, progress, categoryTitle, categoryMessage, categoryProgress);
        }
    }

    public String getSystemDisplayName()
    {
        return serverContext.getDefinitionManager().getSystemDefinition(context).getSystemDisplayName();
    }

    public T8UserDetails getUserDetails(String userIdentifier) throws Exception
    {
        return serverContext.getSecurityManager().getUserDetails(context, userIdentifier);
    }

    public String translate(String inputString, Object... args)
    {
        return String.format(serverContext.getConfigurationManager().getUITranslation(context, inputString), args);
    }

    public String translate(String inputString, String languageIdentifier, Object... args)
    {
        return String.format(serverContext.getConfigurationManager().getUITranslation(context, languageIdentifier, inputString), args);
    }

    public Boolean isUserRegistered(String userIdentifier) throws Exception
    {
        return serverContext.getSecurityManager().isUserRegistered(context, userIdentifier);
    }

    public T8Api getAPI(String apiIdentifier) throws Exception
    {
        return serverContext.getConfigurationManager().getAPI(context, apiIdentifier);
    }

    public String generateKey(String keyIdentifier) throws Exception
    {
        return serverContext.getDataManager().generateKey(keyIdentifier);
    }

    public Object getSystemProperty(String propertyIdentifier)
    {
        return serverContext.getConfigurationManager().getProperty(context, propertyIdentifier);
    }

    public T8FlowStatus startFlow(String flowIdentifier, Map<String, Object> inputParameters) throws Exception
    {
        return serverContext.getFlowManager().startFlow(context, flowIdentifier, inputParameters);
    }

    public void signalFlow(String signalIdentifier, Map<String, Object> signalParameters) throws Exception
    {
        serverContext.getFlowManager().fireSignalEvent(context, signalIdentifier, signalParameters);
    }

    public Map<String, ? extends Object> executeProcedure(String procedureId, Map<String, Object> procedureParameters, String executionMessage, Double executionWeight) throws Exception
    {
        try
        {
            // First get the procedure from the cache or create a new instance.
            currentProcedure = procedureCache.get(procedureId);
            if (currentProcedure == null)
            {
                T8ProcedureDefinition procedureDefinition;

                procedureDefinition = serverContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), procedureId, procedureParameters);
                if (procedureDefinition != null)
                {
                    currentProcedure = procedureDefinition.getNewProcedureInstance(context);
                    procedureCache.put(procedureId, currentProcedure);
                }
                else throw new RuntimeException("Procedure not found: " + procedureId);
            }

            // Execute the procedure and return the results.
            currentProcedureExecutionWeight = executionWeight;
            return currentProcedure.execute(context, procedureParameters);
        }
        finally
        {
            synchronized(progressLock)
            {
                // Reset the currently executing procedure reference.
                currentProcedure = null;

                // Update the script's progress.
                progress += (100.00 * executionWeight);
                if (progress > 100.00) progress = 100.00;
            }
        }
    }

    public Map<String, Object> executeSynchronousServerOperation(String operationId, Map<String, Object> operationParameters) throws Exception
    {
        Map<String, Object> inputParameters;
        Map<String, Object> outputParameters;

        inputParameters = T8IdentifierUtilities.stripNamespace(operationId, operationParameters, true);
        outputParameters = serverContext.executeSynchronousOperation(context, operationId, inputParameters);
        return T8IdentifierUtilities.prependNamespace(operationId, outputParameters);
    }

    public T8ServerOperationStatusReport executeAsynchronousServerOperation(String operationId, Map<String, Object> operationParameters) throws Exception
    {
        Map<String, Object> inputParameters;

        inputParameters = T8IdentifierUtilities.stripNamespace(operationId, operationParameters, true);
        return serverContext.executeAsynchronousOperation(context, operationId, inputParameters);
    }

    public Map<String, Object> executeSynchronousServerProcess(String processId, Map<String, Object> processParameters) throws Exception
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public T8ProcessDetails executeAsynchronousServerProcess(String processId, Map<String, Object> processParameters) throws Exception
    {
        return serverContext.getProcessManager().startProcess(context, processId, processParameters);
    }

    public T8TaskListSummary getFlowTaskListSummary() throws Exception
    {
        return getFlowTaskListSummary(null);
    }

    public T8TaskListSummary getFlowTaskListSummary(T8TaskFilter taskFilter) throws Exception
    {
        return serverContext.getFlowManager().getTaskListSummary(context, taskFilter);
    }

    public String createNewGUID()
    {
        return T8IdentifierUtilities.createNewGUID();
    }

    public void sendCommunication(String communicationId, Map<String, Object> communicationParameters) throws Exception
    {
        // In order to maintain backwards compatibility, the communications are queued and not directly sent.
        // More specific methods are available via the Communication Manager Script facade.
        serverContext.getCommunicationManager().queueCommunication(context, communicationId, communicationParameters);
    }

    public void sendNotification(String notificationId, Map<String, ? extends Object> notificationParameters) throws Exception
    {
        serverContext.getNotificationManager().sendNotification(context, notificationId, notificationParameters);
    }

    public void raiseException(String exceptionId, String technicalMessage, Map<String, Object> exceptionParameters) throws Exception
    {
        T8ExceptionDefinition definition;
        T8ExpressionEvaluator evaluator;
        Map<String, Object> strippedParameters;

        definition = (T8ExceptionDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), exceptionId, null);
        evaluator = serverContext.getExpressionEvaluator(context);
        strippedParameters = T8IdentifierUtilities.stripNamespace(exceptionId, exceptionParameters, true);

        if(definition != null)
        {
            T8RaisedException exception;
            String exceptionMessage;

            exceptionMessage = (String) evaluator.evaluateExpression(definition.getMessageExpession(), strippedParameters, null);
            exception = new T8RaisedException(definition.getCode(), exceptionMessage, technicalMessage);

            throw exception;
        } else throw new RuntimeException("Invalid exception identifier " + exceptionId);
    }

    @Override
    public T8ScriptExecutionHandle getScriptExecutionHandle()
    {
        return executionHandle;
    }
}
