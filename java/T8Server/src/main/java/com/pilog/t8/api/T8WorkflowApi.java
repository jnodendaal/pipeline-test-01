package com.pilog.t8.api;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.flow.task.T8TaskEscalationTrigger;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalSettingsDefinition;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import com.pilog.t8.org.operational.T8OrganizationOperationalHoursCalculator;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8WorkflowApi implements T8Api, T8PerformanceStatisticsProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8WorkflowApi.class);

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8WorkflowApiDataHandler dataHandler;
    private T8PerformanceStatistics stats;

    public static final String API_IDENTIFIER = "@API_WORKFLOW";

    public T8WorkflowApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.stats = tx.getPerformanceStatistics();
        this.dataHandler = new T8WorkflowApiDataHandler(tx, serverContext, sessionContext);
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    public void deleteEscalationTriggers(List<String> triggerIids) throws Exception
    {
        dataHandler.deleteTaskEscalationTriggers(triggerIids);
    }

    public void insertEscalationTriggers(List<T8TaskEscalationTrigger> triggers) throws Exception
    {
        dataHandler.insertTaskEscalationTriggers(triggers);
    }

    public void updateEscalationTriggers(List<T8TaskEscalationTrigger> triggers) throws Exception
    {
        dataHandler.updateTaskEscalationTriggers(triggers);
    }

    public void saveEscalationTriggers(List<T8TaskEscalationTrigger> triggers) throws Exception
    {
        dataHandler.saveTaskEscalationTriggers(triggers);
    }

    public List<T8TaskEscalationTrigger> retrieveEscalationTriggers(List<String> triggerIids) throws Exception
    {
        return dataHandler.retrieveTaskEscalationTriggers(triggerIids);
    }

    public void recalculateTaskTimespans(String taskIid) throws Exception
    {
        dataHandler.recalculateTaskTimespans(taskIid, getOperationalHoursCalculator());
    }

    public void recalculateTaskTimespans() throws Exception
    {
        dataHandler.recalculateTaskTimespans(getOperationalHoursCalculator());
    }

    private T8OperationalHoursCalculator getOperationalHoursCalculator() throws Exception
    {
        T8OrganizationSetupDefinition setupDefinition;
        T8DefinitionManager definitionManager;
        String operationalSettingsId;

        definitionManager = serverContext.getDefinitionManager();
        setupDefinition = T8OrganizationSetupDefinition.getOrganizationSetupDefinition(context);
        operationalSettingsId = setupDefinition.getOrganizationOperationalSettingsIdentifier();
        if (operationalSettingsId != null)
        {
            T8OrganizationOperationalSettingsDefinition settingsDefinition;

            settingsDefinition = definitionManager.getInitializedDefinition(context, null, operationalSettingsId, null);
            if (settingsDefinition != null)
            {
                return T8OrganizationOperationalHoursCalculator.getInstance(settingsDefinition);
            }
            else throw new RuntimeException("Operational settings definition not found: " + operationalSettingsId);
        }
        else return null;
    }
}
