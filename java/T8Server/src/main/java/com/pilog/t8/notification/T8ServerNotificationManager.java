package com.pilog.t8.notification;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.event.T8DefinitionCacheReloadedEvent;
import com.pilog.t8.definition.event.T8DefinitionManagerEventAdapter;
import com.pilog.t8.definition.event.T8DefinitionManagerListener;
import com.pilog.t8.definition.event.T8DefinitionSavedEvent;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.notification.T8Notification.NotificationHistoryEvent;
import com.pilog.t8.notification.T8Notification.NotificationStatus;
import com.pilog.t8.definition.notification.T8NotificationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8ServerNotificationManager implements T8NotificationManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerNotificationManager.class);

    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8DefinitionManager definitionManager;
    private final Set<String> newNotificationUserIDSet;
    private final Map<String, T8NotificationGenerator> notificationGeneratorCache;
    private final T8NotificationManagerDataHandler persistenceHandler;
    private T8DefinitionManagerListener definitionManagerListener;

    public T8ServerNotificationManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.internalContext = serverContext.getSecurityManager().createServerModuleContext(T8ServerNotificationManager.this);
        this.persistenceHandler = new T8NotificationManagerDataHandler(internalContext);
        this.definitionManager = serverContext.getDefinitionManager();
        this.newNotificationUserIDSet = Collections.synchronizedSet(new HashSet<String>(100));
        this.notificationGeneratorCache = Collections.synchronizedMap(new HashMap<String, T8NotificationGenerator>(10));
    }

    @Override
    public void init()
    {
        //Add the monitoring thread when it actually has a purpose
    }

    @Override
    public void start() throws Exception
    {
        definitionManagerListener = new DefinitionManagerListener();
        definitionManager.addDefinitionManagerListener(definitionManagerListener);
    }

    @Override
    public void destroy()
    {
        definitionManager.removeDefinitionManagerListener(definitionManagerListener);
    }

    @Override
    public void sendNotification(T8Context context, String notificationId, Map<String, ? extends Object> inputParameters) throws Exception
    {
        T8NotificationGenerator generator;
        List<T8Notification> notifications;

        // Generate the specified notifications.
        generator = getNotificationGenerator(notificationId);
        notifications = generator.generateNotifications(context, T8IdentifierUtilities.stripNamespace(notificationId, inputParameters, false));
        if (notifications != null)
        {
            long sentTime;

            // Persist each one of the notifications.
            sentTime = System.currentTimeMillis();
            for (T8Notification notification : notifications)
            {
                // Set some default data on the notification.
                notification.setSenderDetail(context.getSessionContext());
                notification.setCreationTime(sentTime);
                notification.setStatus(NotificationStatus.SENT);

                // Add the recipient user identifier to the pending set.
                newNotificationUserIDSet.add(notification.getUserIdentifier());

                // Persist the generated notifcation state.
                persistenceHandler.saveNotificationState(notification);

                // Persist the notification event.
                persistenceHandler.logNotificationEvent(context, NotificationHistoryEvent.SENT, notification);
            }
        }
    }

    private T8NotificationGenerator getNotificationGenerator(String notificationId) throws Exception
    {
        T8NotificationGenerator generator;

        generator = notificationGeneratorCache.get(notificationId);
        if (generator != null)
        {
            return generator;
        }
        else
        {
            T8NotificationDefinition notificationDefinition;

            notificationDefinition = (T8NotificationDefinition)definitionManager.getInitializedDefinition(internalContext, null, notificationId, null);
            if (notificationDefinition != null)
            {
                generator = notificationDefinition.getNotificationGeneratorInstance(internalContext);
                notificationGeneratorCache.put(notificationId, generator);
                return generator;
            }
            else throw new RuntimeException("Notification definition not found: " + notificationId);
        }
    }

    @Override
    public void closeAllNotifications(T8Context context) throws Exception
    {
        this.persistenceHandler.closeAllNotifications(context, context.getSessionContext().getUserIdentifier());
    }

    @Override
    public void closeNotification(T8Context context, String notificationIid) throws Exception
    {
        this.persistenceHandler.closeNotification(context, context.getSessionContext().getUserIdentifier(), notificationIid);
    }

    @Override
    public void markWithStatus(T8Context context, String notificationIid, T8Notification.NotificationStatus status) throws Exception
    {
        this.persistenceHandler.updateNotificationStatus(context, notificationIid, status);
    }

    @Override
    public T8NotificationSummary getUserNotificationSummary(T8Context context, T8NotificationFilter filter) throws Exception
    {
        if (filter == null)
        {
            filter = new T8NotificationFilter();
            filter.setUserIdentifier(context.getSessionContext().getUserIdentifier());
            filter.setIncludeReceivedNotifications(true);
            filter.setPageOffset(5000);
        }
        else if (filter.getUserIdentifier() == null)
        {
            filter.setUserIdentifier(context.getSessionContext().getUserIdentifier());
        }

        return this.persistenceHandler.getNotificationSummary(context, filter);
    }

    @Override
    public List<T8Notification> getUserNotifications(T8Context context, T8NotificationFilter filter) throws Exception
    {
        String userId;

        // Get the session user identifier.
        userId = context.getSessionContext().getUserIdentifier();

        // If no more new notifications remain for this user, remove the identifier from the pending list.
        if (persistenceHandler.getNewNotificationCount(context, userId) == 0)
        {
            newNotificationUserIDSet.remove(userId);
        }

        // Make sure we have a valid filter to use.
        if (filter == null)
        {
            filter = new T8NotificationFilter();
            filter.setIncludeReceivedNotifications(true);
            filter.setUserIdentifier(userId);
        }
        else
        {
            filter.setUserIdentifier(userId);
        }

        // Retrieve the notifications.
        return persistenceHandler.retrieveNotifications(context, filter);
    }

    @Override
    public boolean hasNewNotifications(T8Context context)
    {
        String userId;

        // Check for the session user in the set of pending notification user identifiers.
        userId = context.getSessionContext().getUserIdentifier();
        return newNotificationUserIDSet.contains(userId);
    }

    @Override
    public int countNotifications(T8Context context, T8NotificationFilter filter) throws Exception
    {
        String userIdentifier;

        // Get the session user identifier.
        userIdentifier = context.getSessionContext().getUserIdentifier();

        // Make sure we have a valid filter to use.
        if (filter == null)
        {
            filter = new T8NotificationFilter();
            filter.setIncludeReceivedNotifications(true);
            filter.setUserIdentifier(userIdentifier);
        }
        else
        {
            filter.setUserIdentifier(userIdentifier);
        }

        // Retrieve the count and return the result.
        return persistenceHandler.retrieveNotificationCount(context, filter);
    }

    /**
     * This listener clears cached notification generators when their
     * definitions are updated on the server.
     */
    private class DefinitionManagerListener extends T8DefinitionManagerEventAdapter
    {
        @Override
        public void definitionCacheReloaded(T8DefinitionCacheReloadedEvent tdcre)
        {
            notificationGeneratorCache.clear();
        }

        @Override
        public void definitionSaved(T8DefinitionSavedEvent event)
        {
            if (event.getDefinition() instanceof T8NotificationDefinition)
            {
                notificationGeneratorCache.remove(event.getDefinition().getIdentifier());
            }
        }
    }
}
