package com.pilog.t8.operation.script;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.operation.script.T8ScriptServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ServerScriptOperation implements T8ServerOperation
{
    private final T8ScriptServerOperationDefinition definition;
    private final String operationIid;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private T8ServerContextScript script;

    public T8ServerScriptOperation(T8Context context, T8ScriptServerOperationDefinition definition, String operationIid)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
        this.operationIid = operationIid;
    }

    @Override
    public void init()
    {
    }

    @Override
    public double getProgress(T8Context context)
    {
        return script != null ? script.getProgress() : -1;
    }

    @Override
    public T8ProgressReport getProgressReport(T8Context context)
    {
        return script != null ? script.getProgressReport() : null;
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
    {
        script = definition.getNewScriptInstance(context);
        return script.executeScript(operationParameters);
    }

    @Override
    public void stop(T8Context context)
    {
        script.getScriptExecutionHandle().setStopped(true);
    }

    @Override
    public void cancel(T8Context context)
    {
        script.getScriptExecutionHandle().setCancelled(true);
    }

    @Override
    public void pause(T8Context context)
    {
        script.getScriptExecutionHandle().setPaused(true);
    }

    @Override
    public void resume(T8Context context)
    {
        script.getScriptExecutionHandle().setPaused(false);
    }
}
