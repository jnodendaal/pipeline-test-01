package com.pilog.t8.process;

import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ServerProcessManagerScriptFacade
{
    private final T8ProcessManager processManager;
    private final T8Context context;

    public T8ServerProcessManagerScriptFacade(T8Context context, T8ProcessManager processManager)
    {
        this.processManager = processManager;
        this.context = context;
    }

    public T8ProcessSummary getProcessSummary() throws Exception
    {
        return processManager.getProcessSummary(context);
    }

    public T8ProcessDetails getProcessDetails(String processIid) throws Exception
    {
        return processManager.getProcessDetails(context, processIid);
    }

    public int countProcesses(T8ProcessFilter processFilter, String searchExpression) throws Exception
    {
        return processManager.countProcesses(context, processFilter, searchExpression);
    }

    public List<T8ProcessDetails> searchProcesses(T8ProcessFilter processFilter, String searchExpression, int pageOffset, int pageSize) throws Exception
    {
        return processManager.searchProcesses(context, processFilter, searchExpression, pageOffset, pageSize);
    }
}
