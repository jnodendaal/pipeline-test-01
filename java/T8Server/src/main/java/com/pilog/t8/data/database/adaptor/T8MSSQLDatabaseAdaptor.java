package com.pilog.t8.data.database.adaptor;

import static com.pilog.t8.data.database.adaptor.T8DatabaseAdaptorTools.*;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.database.T8TablePrimaryKey;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.database.T8DatabaseAdaptorDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.utilities.jdbc.JDBCUtilities;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class T8MSSQLDatabaseAdaptor implements T8DatabaseAdaptor
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8MSSQLDatabaseAdaptor.class);

    private static final char START_ESCAPE_CHAR = '[';
    private static final char END_ESCAPE_CHAR = ']';

    private static final Map<Integer, T8DataType> sqlTypeMap;

    private enum MssqlDataType
    {
        BIT("BIT"),
        DATETIME("DATETIME"),
        FLOAT("FLOAT"),
        IMAGE("IMAGE"),
        INTEGER("INTEGER"),
        NUMERIC("NUMERIC(10,0)"),
        VARCHAR("VARCHAR(4000)"),
        NVARCHAR("NVARCHAR(4000)"),
        NVARCHAR_MAX("NVARCHAR(MAX)"),
        BIGINT("BIGINT"),
        UNIQUEIDENTIFIER("UNIQUEIDENTIFIER"),
        TIMESTAMP("TIMESTAMP");

        private final String sqlTypeString;

        private MssqlDataType(String sqlTypeString)
        {
            this.sqlTypeString = sqlTypeString;
        }

        private String getSqlTypeString()
        {
            return this.sqlTypeString;
        }
    };

    static
    {
        sqlTypeMap = new HashMap<>();
        sqlTypeMap.put(Types.ARRAY, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.BIGINT, T8DataType.LONG);
        sqlTypeMap.put(Types.BINARY, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.BIT, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.BLOB, T8DataType.BYTE_ARRAY);
        sqlTypeMap.put(Types.BOOLEAN, T8DataType.BOOLEAN);
        sqlTypeMap.put(Types.CHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.CLOB, T8DataType.LONG_STRING);
        sqlTypeMap.put(Types.DATALINK, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.DATE, T8DataType.DATE);
        sqlTypeMap.put(Types.DECIMAL, T8DataType.DOUBLE);
        sqlTypeMap.put(Types.DISTINCT, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.DOUBLE, T8DataType.DOUBLE);
        sqlTypeMap.put(Types.FLOAT, T8DataType.FLOAT);
        sqlTypeMap.put(Types.INTEGER, T8DataType.INTEGER);
        sqlTypeMap.put(Types.JAVA_OBJECT, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.LONGNVARCHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.LONGVARBINARY, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.LONGVARCHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.NCHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.NCLOB, T8DataType.STRING);
        sqlTypeMap.put(Types.NULL, T8DataType.UNDEFINED);
        sqlTypeMap.put(Types.NUMERIC, T8DataType.DOUBLE);
        sqlTypeMap.put(Types.NVARCHAR, T8DataType.STRING);
        sqlTypeMap.put(Types.OTHER, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.REAL, T8DataType.DOUBLE);
        sqlTypeMap.put(Types.REF, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.ROWID, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.SMALLINT, T8DataType.INTEGER);
        sqlTypeMap.put(Types.SQLXML, T8DataType.STRING);
        sqlTypeMap.put(Types.STRUCT, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.TIME, T8DataType.TIME);
        sqlTypeMap.put(Types.TIMESTAMP, T8DataType.TIMESTAMP);
        sqlTypeMap.put(Types.TINYINT, T8DataType.INTEGER);
        sqlTypeMap.put(Types.VARBINARY, T8DataType.CUSTOM_OBJECT);
        sqlTypeMap.put(Types.VARCHAR, T8DataType.STRING);
    }

    public T8MSSQLDatabaseAdaptor(T8DatabaseAdaptorDefinition definition)
    {
    }

    @Override
    public T8DataType getDataType(int sqlType, int width)
    {
        return sqlTypeMap.get(sqlType);
    }

    @Override
    public String getSQLStringConcatenationOperator()
    {
        return "+";
    }

    @Override
    public String getSQLParameterizedColumnContains(String columnName)
    {
        return "CONTAINS(" + columnName + ", ?)";
    }

    @Override
    public String getTableSelectionQuery()
    {
        return "SELECT a.TABLE_NAME FROM (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE') a";
    }

    @Override
    public String getTableCreationStatement(T8TableDataSourceDefinition dataSourceDefinition)
    {
        List<T8DataSourceFieldDefinition> dataFieldDefinitions;
        StringBuilder createTable;
        String tableName;

        dataFieldDefinitions = dataSourceDefinition.getFieldDefinitions();

        if (dataFieldDefinitions.isEmpty())
        {
            throw new RuntimeException("Table cannot be created without any columns specified");
        }

        createTable = new StringBuilder("CREATE TABLE ");
        tableName = dataSourceDefinition.getTableName();

        createTable.append(escapeMetaString(tableName));
        createTable.append(" (");
        createTable.append(getCreateColumns(dataFieldDefinitions));
        createTable.append(getCreateTablePrimConstraint(tableName, dataFieldDefinitions, this));
        createTable.append(")");

        return createTable.toString();
    }

    private MssqlDataType getMssqlDataType(T8DataType dataType)
    {
        if (dataType.isType(T8DataType.STRING))
        {
            T8DtString stringType;

            stringType = (T8DtString)dataType;
            switch (stringType.getType())
            {
                case DEFAULT: return MssqlDataType.NVARCHAR;
                case EPIC_EXPRESSION: return MssqlDataType.NVARCHAR_MAX;
                case EPIC_SCRIPT: return MssqlDataType.NVARCHAR_MAX;
                case JAVA_SCRIPT: return MssqlDataType.NVARCHAR_MAX;
                case JSON: return MssqlDataType.NVARCHAR_MAX;
                case CSS: return MssqlDataType.NVARCHAR_MAX;
                case XML: return MssqlDataType.NVARCHAR_MAX;
                case TRANSLATED_STRING: return MssqlDataType.NVARCHAR;
                case PASSWORD: return MssqlDataType.NVARCHAR;
                case COLOR_HEX: return MssqlDataType.NVARCHAR;
                case GUID: return MssqlDataType.UNIQUEIDENTIFIER;
                case DEFINITION_IDENTIFIER: return MssqlDataType.NVARCHAR;
                case DEFINITION_TYPE_IDENTIFIER: return MssqlDataType.NVARCHAR;
                case DEFINITION_GROUP_IDENTIFIER: return MssqlDataType.NVARCHAR;
                case DEFINITION_INSTANCE_IDENTIFIER: return MssqlDataType.NVARCHAR;
                case LONG_STRING: return MssqlDataType.NVARCHAR_MAX;
                default: return null;
            }
        }
        else if (dataType.isType(T8DataType.BOOLEAN)) return MssqlDataType.BIT;
        else if (dataType.isType(T8DataType.BYTE_ARRAY)) return MssqlDataType.IMAGE;
        else if (dataType.isType(T8DataType.DATE)) return MssqlDataType.DATETIME;
        else if (dataType.isType(T8DataType.DATE_TIME)) return MssqlDataType.DATETIME;
        else if (dataType.isType(T8DataType.DISPLAY_STRING)) return MssqlDataType.NVARCHAR;
        else if (dataType.isType(T8DataType.IDENTIFIER_STRING)) return MssqlDataType.NVARCHAR;
        else if (dataType.isType(T8DataType.DOUBLE)) return MssqlDataType.NUMERIC;
        else if (dataType.isType(T8DataType.FLOAT)) return MssqlDataType.FLOAT;
        else if (dataType.isType(T8DataType.LONG)) return MssqlDataType.BIGINT;
        else return null;
    }

    private StringBuilder getCreateColumns(List<T8DataSourceFieldDefinition> fieldDefinitions)
    {
        Iterator<T8DataSourceFieldDefinition> fieldIterator;
        T8DataFieldDefinition fieldDefinition;
        StringBuilder columns;

        columns = new StringBuilder();
        fieldIterator = fieldDefinitions.listIterator();
        while (fieldIterator.hasNext())
        {
            MssqlDataType mssqlDataType;

            fieldDefinition = fieldIterator.next();
            mssqlDataType = getMssqlDataType(fieldDefinition.getDataType());
            if (mssqlDataType != null)
            {
                columns.append(escapeMetaString(fieldDefinition.getSourceIdentifier()));
                columns.append(" ");
                columns.append(mssqlDataType.getSqlTypeString());
                if (fieldDefinition.isRequired()) columns.append(" NOT NULL ");
                if (fieldIterator.hasNext()) columns.append(", ");
            }
            else throw new RuntimeException("No MSSQL data type equivalent found for T8DataType: " + fieldDefinition.getDataType());
        }

        return columns;
    }

    @Override
    public String getAddTableColumnStatement(String tableName, T8DataFieldDefinition fieldDefinition)
    {
        if (tableName == null || tableName.isEmpty() || fieldDefinition == null)
        {
            throw new NullPointerException("Table and Column name needs to have an actual value");
        }
        else
        {
            MssqlDataType mssqlDataType;

            mssqlDataType = getMssqlDataType(fieldDefinition.getDataType());
            if (mssqlDataType != null)
            {
                StringBuilder alterTable;

                alterTable = new StringBuilder("ALTER TABLE ");
                alterTable.append(escapeMetaString(tableName));
                alterTable.append(" ADD ");
                alterTable.append(escapeMetaString(fieldDefinition.getSourceIdentifier()));
                alterTable.append(" ");
                alterTable.append(mssqlDataType.getSqlTypeString());
                if (fieldDefinition.isRequired()) alterTable.append(" NOT NULL ");

                return alterTable.toString();
            }
            else throw new RuntimeException("No MSSQL data type equivalent found for T8DataType: " + fieldDefinition.getDataType());
        }
    }

    @Override
    public boolean addColumnToPrimaryKey(String tableName, String columnName, T8DataConnection dataConnection)
    {
        if (tableName == null || tableName.isEmpty() || columnName == null || columnName.isEmpty())
            throw new NullPointerException("Invalid values supplied Table: " + tableName + ", Column: " + columnName);

        T8TablePrimaryKey primaryKey;

        primaryKey = getTablePrimaryKey(tableName, dataConnection);
        if (primaryKey == null)
        {
            LOGGER.log("Failed to retrieve the primary key data for table " + tableName + ", adding column " + columnName + " to primary key has failed");
            return false;
        }

        try
        {
            if (primaryKey.getPrimKeyIdentifier() == null)
                createNewPrimaryKey(tableName, dataConnection, columnName);
            else if (!primaryKey.getPrimaryKeyColumns().contains(columnName)) return alterPrimaryKey(primaryKey, columnName, dataConnection);
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Failed to modify the table " + tableName + " primary key", sqle);
            return false;
        }

        return true;
    }

    /**
     * Retrieves an object which represents the primary key on the specified table.
     * If no primary key exists on the table, the object is still returned and
     * the {@code hasPrimaryKey()} method will read false
     *
     * @return The primary key object containing the primary key data if one
     * exists. An empty primary key object if no primary key exists.
     * {@code null} is returned if an error occurs trying to retrieve the
     * primary key data
     */
    private T8TablePrimaryKey getTablePrimaryKey(String tableName, T8DataConnection dataConnection)
    {
        PreparedStatement pstmtSelectPrimKey = null;
        ResultSet rsSelectPrimKey = null;
        T8TablePrimaryKey primaryKey;

        try
        {
            pstmtSelectPrimKey = dataConnection.prepareStatement("SELECT CONSTRAINT_NAME, COLUMN_NAME "
                    + "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE "
                    + "WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1 "
                    + "AND table_name = ? ORDER BY ORDINAL_POSITION");
            pstmtSelectPrimKey.setString(1, tableName);
            rsSelectPrimKey = pstmtSelectPrimKey.executeQuery();

            primaryKey = new T8TablePrimaryKey(tableName);
            while (rsSelectPrimKey.next())
            {
                primaryKey.setPrimaryKeyIdentifier(rsSelectPrimKey.getString("CONSTRAINT_NAME"));
                primaryKey.addPrimaryKeyColumn(rsSelectPrimKey.getString("COLUMN_NAME"));
            }
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Failed to retrieve the primary key data " + sqle.getMessage());
            return null;
        }
        finally
        {
            JDBCUtilities.close(rsSelectPrimKey, pstmtSelectPrimKey);
        }

        return primaryKey;
    }

    /**
     * Alters the primary key for a specific table. Since altering a primary key
     * requires the primary key to be dropped, this method acts the same as
     * dropping the primary key and creating a new primary key, with the additional
     * column appended to all of the original primary key columns.<br>
     * <br>
     * If the creation of the new primary key fails, an attempt will be made to
     * rebuild the original primary key.
     *
     * @return {@code true} if the primary key is altered successfully.
     * {@code false} if original primary key was rebuilt.
     *
     * @throws SQLException If altering the primary key has failed, as well as
     * rebuilding the original primary key.
     */
    private boolean alterPrimaryKey(T8TablePrimaryKey existingKey, String columnToAdd, T8DataConnection dataConnection) throws SQLException
    {
        List<String> keyColumns;

        dropPrimaryKey(existingKey, dataConnection);

        try
        {
            keyColumns = new ArrayList<>(existingKey.getPrimaryKeyColumns());
            keyColumns.add(columnToAdd);
            createNewPrimaryKey(existingKey.getTableName(), dataConnection, keyColumns.toArray(new String[keyColumns.size()]));
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Failed to create new primary key. Attempting to rebuild old primary key", sqle);
            createNewPrimaryKey(existingKey.getTableName(), dataConnection, existingKey.getPrimaryKeyColumns().toArray(new String[existingKey.getPrimaryKeyColumns().size()]));
            return false;
        }

        return true;
    }

    @Override
    public void logPrimaryKeyDifferences(String tableName, List<T8DataSourceFieldDefinition> dataFieldDefinitions, T8DataConnection dataConnection)
    {
        T8TablePrimaryKey primaryKey;
        List<String> primaryKeyColumns;

        primaryKey = getTablePrimaryKey(tableName, dataConnection);

        if (!primaryKey.hasPrimaryKey())
        {
            for (T8DataFieldDefinition dataFieldDefinition : dataFieldDefinitions)
            {
                if (dataFieldDefinition.isKey())
                    LOGGER.log("Table " + tableName + " is missing it's primary key containing column " + dataFieldDefinition.getSourceIdentifier());
            }
            return;
        }

        primaryKeyColumns = primaryKey.getPrimaryKeyColumns();
        for (T8DataFieldDefinition dataFieldDefinition : dataFieldDefinitions)
        {
            if (dataFieldDefinition.isKey())
            {
                if (!dataFieldDefinition.isRequired())
                    LOGGER.log("Table " + tableName + " has primary key column " + dataFieldDefinition.getSourceIdentifier() + " which is NOT required");
                if (!primaryKeyColumns.contains(dataFieldDefinition.getSourceIdentifier()))
                    LOGGER.log("Table " + tableName + " is missing primary key column " + dataFieldDefinition.getSourceIdentifier());
            }
        }
    }

    @Override
    public String getRegularExpressionLike(String columnName, String expression)
    {
        return "(1 = dbo.RegExMatch(" + columnName + ", '" + expression + "'))";
    }

    @Override
    public String getSequenceNextValueQuery(T8DataConnection dataConnection, String sequenceName)
    {
        try
        {
            if (dataConnection.getMetaData().getDatabaseMajorVersion() > 10)
            {
                return "SELECT NEXT VALUE FOR ".concat(sequenceName);
            }
        }
        catch (SQLException sqle)
        {
            LOGGER.log("Failed to determine Database Version", sqle);
        }
        throw new UnsupportedOperationException("Database does not support sequences.");
    }

    @Override
    public String getSQLTagReplaceByIdentifier(String tagIdentifier, String replacementTag)
    {
        return "TAGS = dbo.RegExReplace(TAGS, '\\[" + tagIdentifier + ":[^].]+\\]', '" + replacementTag + "')";
    }

    @Override
    public String getSQLParameterizedHexToGUID()
    {
        return "CONVERT(UNIQUEIDENTIFIER, ?)";
    }

    @Override
    public String getSQLColumnMillisecondsToTimestamp(String columnName)
    {
        return "CONVERT(DATETIME, DATEADD(MILLISECOND, CONVERT(BIGINT, " + columnName + ") % 1000, DATEADD(SECOND, CONVERT(BIGINT, " + columnName + ") / 1000, '19700101')))";
    }

    @Override
    public String getSQLColumnTimestampToMilliseconds(String columnName)
    {
        return "CAST(DATEDIFF(ss, '19700101', " + columnName + ") AS BIGINT) * 1000 + CAST(DATEPART(ms, " + columnName + ") AS BIGINT)";
    }

    @Override
    public String getSQLColumnTimestampAddMilliseconds(String timestampColumnName, String millisecondExpression)
    {
        return "DATEADD(ms, " + millisecondExpression + ", " + timestampColumnName + ")";
    }

    @Override
    public String getSQLParameterizedMillisecondsToTimestamp()
    {
        // Please note:  Because the DATEADD funciton in MSSQL can only take
        // and int value as second parameter, and the actual millisecond value
        // is most likely too large to fit in an int, this method discards the
        // millisconds part of the timestamp parameter by dividing it by 1000.
        // A more accurate method would be:  DATEADD(MILLISECOND, ? % 1000, DATEADD(SECOND, ? / 1000, '19700101')) but this method needs two parameters to be set.
        return "CONVERT(DATETIME, DATEADD(SECOND, ? / 1000, '19700101'))";
    }

    @Override
    public String getSQLCreateNewGUID()
    {
        return "NEWID()";
    }

    @Override
    public String getSQLHexToGUID(String guid)
    {
        StringBuilder guidString;

        guidString = new StringBuilder(guid);
        guidString.insert(8, '-');
        guidString.insert(13, '-');
        guidString.insert(18, '-');
        guidString.insert(23, '-');
        return "CONVERT(UNIQUEIDENTIFIER, '" + guidString + "')";
    }

    @Override
    public String convertHexStringToSQLGUIDString(String inputString)
    {
        if (inputString != null)
        {
            if(!inputString.matches("[a-zA-Z0-9]{32}"))
                throw new RuntimeException("The input String " + inputString + " is not a valid GUID");
            StringBuffer guidString;

            // Add the dashes to the string.
            guidString = new StringBuffer(inputString);
            guidString.insert(8, "-");
            guidString.insert(13, "-");
            guidString.insert(18, "-");
            guidString.insert(23, "-");
            return guidString.toString();
        } else return null;
    }

    @Override
    public String getSQLColumnHexToGUID(String columnString)
    {
        return " STUFF(STUFF(STUFF(STUFF(" + columnString + ", 9, 0, '-'), 14, 0, '-'), 19, 0, '-'), 24, 0, '-') ";
    }

    @Override
    public boolean requiresSqlLikeEscape(String inputString)
    {
        if (inputString == null) return false;
        else if (inputString.contains("%")) return true;
        else if (inputString.contains("[")) return true;
        else return (inputString.contains("]"));
    }

    @Override
    public String getSQLEscapedLikeOperand(String inputString, char escapeCharacter)
    {
        String result;
        String escapeString;

        escapeString = "" + escapeCharacter;
        result = inputString.replace("%", escapeString + "%");
        result = result.replace("[", escapeString + "[");
        result = result.replace("]", escapeString + "]");
        return result;
    }

    @Override
    public char getMetaEscapeCharacterStart()
    {
        return START_ESCAPE_CHAR;
    }

    @Override
    public char getMetaEscapeCharacterEnd()
    {
        return END_ESCAPE_CHAR;
    }

    @Override
    public String escapeMetaString(String metaString)
    {
        return T8DatabaseAdaptorTools.escapeMetaString(this, metaString);
    }

    @Override
    public String getSqlCurrentDateTimeFunction()
    {
        return "GETDATE()";
    }
}
