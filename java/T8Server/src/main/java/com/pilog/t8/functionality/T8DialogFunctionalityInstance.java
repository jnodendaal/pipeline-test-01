package com.pilog.t8.functionality;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.functionality.T8DialogFunctionalityDefinition;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DialogFunctionalityInstance extends T8DefaultFunctionalityInstance implements T8FunctionalityInstance
{
    private final T8DialogFunctionalityDefinition definition;
    private final String dialogId;
    private Map<String, Object> inputParameters;

    public T8DialogFunctionalityInstance(T8Context context, T8DialogFunctionalityDefinition definition)
    {
        super(context, definition, T8IdentifierUtilities.createNewGUID());
        this.definition = definition;
        this.dialogId = definition.getDialogIdentifier();
    }

    public T8DialogFunctionalityInstance(T8Context context, T8DialogFunctionalityDefinition definition, T8FunctionalityState state)
    {
        super(context, definition, state.getFunctionalityIid());
        this.definition = definition;
        this.dialogId = definition.getDialogIdentifier();
    }

    @Override
    public T8FunctionalityDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void initialize()
    {
    }

    @Override
    public boolean release()
    {
        return true;
    }

    @Override
    public T8FunctionalityAccessHandle access(Map<String, Object> parameters) throws Exception
    {
        // Store the input parameters.
        this.timeAccessed = new T8Timestamp(System.currentTimeMillis());
        this.inputParameters = parameters;

        // Return the execution handle.
        return new T8DialogFunctionalityAccessHandle(getFunctionalityId(), iid, dialogId, getDialogInputParameters(parameters), getDisplayName(), getDescription(), getIcon());
    }

    @Override
    public boolean stop()
    {
        return true;
    }

    @Override
    public T8FunctionalityState getFunctionalityState()
    {
        T8FunctionalityState state;

        // Create the new state.
        state = super.getFunctionalityState(inputParameters);

        // Add the session id so that this functionality can be removed when the session expires.
        state.setSessionId(sessionContext.getSessionIdentifier());

        // Return the state.
        return state;
    }

    private Map<String, Object> getDialogInputParameters(Map<String, Object> functionalityInputParameters) throws Exception
    {
        Map<String, Object> dialogInputParameters;

        dialogInputParameters = T8IdentifierUtilities.stripNamespace(id, functionalityInputParameters, true);
        dialogInputParameters = T8IdentifierUtilities.mapParameters(dialogInputParameters, definition.getInputParameterMapping());
        dialogInputParameters.putAll(evaluateExpressions());
        return dialogInputParameters;
    }

    private Map<String, Object> evaluateExpressions() throws Exception
    {
        Map<String, String> expressionMap;
        Map<String, Object> resultMap;

        resultMap = new HashMap<String, Object>();
        expressionMap = definition.getInputParameterExpressionMap();
        if ((expressionMap != null) && (expressionMap.size() > 0))
        {
            HashMap<String, Object> expressionParameters;

            // Create a map of expression parameters.
            expressionParameters = new HashMap<String, Object>();

            // Construct a new expression evaluator to use.
            for (String parameterIdentifier : expressionMap.keySet())
            {
                String expression;
                Object result;

                expression = expressionMap.get(parameterIdentifier);
                result = expressionEvaluator.evaluateExpression(expression, expressionParameters, null);
                resultMap.put(parameterIdentifier, result);
            }
        }

        return resultMap;
    }
}
