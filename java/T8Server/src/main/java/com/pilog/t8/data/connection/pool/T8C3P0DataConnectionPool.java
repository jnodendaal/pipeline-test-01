package com.pilog.t8.data.connection.pool;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.pilog.t8.data.T8DataConnectionPool;
import com.pilog.t8.definition.data.connection.pool.T8C3P0DataConnectionPoolDefinition;
import java.sql.Connection;
import java.sql.SQLException;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8C3P0DataConnectionPool implements T8DataConnectionPool
{
    private T8C3P0DataConnectionPoolDefinition definition;
    private String connectionIdentifier;
    private ComboPooledDataSource cpds;

    public T8C3P0DataConnectionPool(T8C3P0DataConnectionPoolDefinition definition, T8DataTransaction dataAccessProvider)
    {
        this.definition = definition;
        configureConnectionPool();
    }

    private void configureConnectionPool()
    {
        // Initialize the pool.
        this.cpds = new ComboPooledDataSource();

        // C3P0 Configuration Properties
        // -----------------------------
        this.cpds.setMinPoolSize(definition.getMinPoolSize());

        // Maximum number of Connections a pool will maintain at any given time.  Default = 15.
        this.cpds.setMaxPoolSize(definition.getMaxPoolSize());

        // Seconds a Connection can remain pooled but unused before being discarded. Zero means idle connections never expire.
        this.cpds.setMaxIdleTime(600); // 10 minutes.

        // If this is a number greater than 0, c3p0 will test all idle, pooled but unchecked-out connections, every this number of seconds.
        this.cpds.setIdleConnectionTestPeriod(300); // 5 minutes.
    }

    @Override
    public String getDataConnectionIdentifier()
    {
        return connectionIdentifier;
    }

    @Override
    public Connection getConnection() throws SQLException
    {
        return cpds.getConnection();
    }

    @Override
    public void destroy()
    {
        cpds.hardReset();
        cpds.close();
    }

    @Override
    public void setDataConnectionIdentifier(String identifier)
    {
        connectionIdentifier = identifier;
    }

    @Override
    public void setDriverClassName(String className) throws Exception
    {
        cpds.setDriverClass(className);
    }

    @Override
    public void setConnectionURL(String connectionURL)
    {
        cpds.setJdbcUrl(connectionURL);
    }

    @Override
    public void setUsername(String username)
    {
        cpds.setUser(username);
    }

    @Override
    public void setPassword(String password)
    {
        cpds.setPassword(password);
    }
}
