package com.pilog.t8.definition;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.datatype.T8DtBigDecimal;
import com.pilog.t8.datatype.T8DtBoolean;
import com.pilog.t8.datatype.T8DtByteArray;
import com.pilog.t8.datatype.T8DtCustomObject;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.datatype.T8DtDate;
import com.pilog.t8.datatype.T8DtDefinition;
import com.pilog.t8.datatype.T8DtDouble;
import com.pilog.t8.datatype.T8DtFloat;
import com.pilog.t8.datatype.T8DtImage;
import com.pilog.t8.datatype.T8DtInteger;
import com.pilog.t8.datatype.T8DtList;
import com.pilog.t8.datatype.T8DtLong;
import com.pilog.t8.datatype.T8DtMap;
import com.pilog.t8.datatype.T8DtUndefined;
import com.pilog.t8.datatype.T8DtString;
import com.pilog.t8.datatype.T8DtTime;
import com.pilog.t8.datatype.T8DtTimestamp;
import com.pilog.t8.datatype.T8DtUserDetails;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.data.type.T8DataTypeDefinition;
import com.pilog.t8.mainserver.T8ClassScanner;
import com.pilog.t8.utilities.reflection.T8Reflections;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.w3c.dom.Element;

/**
 * @author Bouwer du Preez
 */
public class T8DataTypeCache
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DataTypeCache.class);

    private final T8ServerContext serverContext;
    private final T8DefinitionManager definitionManager;
    private final Map<String, String> dataTypeClasses; // Key: Data Type Identifier, Value: Data Type Class Name.
    private final Map<String, T8DataType> commonDataTypeClasses; // Key: Data Type Class Name, Value: Data Type.
    private final Map<String, T8DataType> dataTypeCache; // Key: Definition Type Identifier, Value: Definition Type.

    public T8DataTypeCache(T8ServerContext serverContext, T8DefinitionManager definitionManager)
    {
        this.serverContext = serverContext;
        this.definitionManager = definitionManager;
        this.dataTypeClasses = Collections.synchronizedMap(new HashMap<String, String>());
        this.commonDataTypeClasses = new HashMap<>();
        this.dataTypeCache = new HashMap<>();
        cacheStandardDataTypes();
    }

    public void cacheDataTypes(List<T8Definition> dataTypeDefinitions)
    {
        LOGGER.log("Caching data types from definitions...");
        for (T8Definition definition : dataTypeDefinitions)
        {
            String dataTypeIdentifier;

            dataTypeIdentifier = definition.getIdentifier();
            if (!dataTypeClasses.containsKey(dataTypeIdentifier))
            {
                T8DataTypeDefinition dataTypeDefinition;

                dataTypeDefinition = (T8DataTypeDefinition)definition;
                dataTypeClasses.put(dataTypeIdentifier, dataTypeDefinition.getClassName());
            }
        }
    }

    public void cacheDataTypes()
    {
        T8ClassScanner classScanner;
        Set<Class> classes;

        // We were keeping multiple copies of the jar file references in memory. We only need it during this scan
        classScanner = new T8ClassScanner(serverContext, T8ClassScanner.SERVER_SIDE_JAR_PATH);

        // Find only the non-abstract classes and add all of them to the type cache.
        LOGGER.log("Caching data types from classes....");
        classes = classScanner.getPackageClasses("com.pilog.t8.datatype");
        for (Class dataTypeClass : classes)
        {
            if (T8DataType.class.isAssignableFrom(dataTypeClass))
            {
                if (!Modifier.isAbstract(dataTypeClass.getModifiers()))
                {
                    try
                    {
                        Constructor constructor;
                        T8DataType dataType;
                        String dataTypeId;

                        constructor = dataTypeClass.getConstructor(T8DefinitionManager.class);
                        dataType = (T8DataType)constructor.newInstance(definitionManager);
                        dataTypeId = dataType.getDataTypeIdentifier();

                        LOGGER.log(dataTypeId + " cached.");
                        dataTypeClasses.put(dataTypeId, dataTypeClass.getCanonicalName());
                        dataTypeCache.put(dataTypeId, dataType);
                        commonDataTypeClasses.put(dataType.getDataTypeClassName(), dataType);
                    }
                    catch (Exception e)
                    {
                        T8Log.log("Exception while loading data type: " + dataTypeClass, e);
                    }
                }
            }
        }
    }

    public void clearCache()
    {
        dataTypeClasses.clear();
        dataTypeCache.clear();
    }

    public List<T8DataTypeDefinition> getDataTypeDefinitions()
    {
        List<T8DataTypeDefinition> definitions;

        definitions = new ArrayList<>();
        for (String dataTypeId : dataTypeClasses.keySet())
        {
            T8DataTypeDefinition definition;

            definition = new T8DataTypeDefinition(dataTypeId);
            definition.setMetaDisplayName("Data Type");
            definition.setMetaDescription("The data type of a data value in the T8 system.");
            definition.setClassName(dataTypeClasses.get(dataTypeId));
            definitions.add(definition);
        }

        return definitions;
    }

    private void cacheStandardDataTypes()
    {
        dataTypeCache.put("BOOLEAN", T8DataType.BOOLEAN);
        commonDataTypeClasses.put(T8DataType.BOOLEAN.getDataTypeClassName(), T8DataType.BOOLEAN);
        dataTypeClasses.put(T8DataType.BOOLEAN.getDataTypeIdentifier(), T8DtBoolean.class.getCanonicalName());

        dataTypeCache.put("BYTE_ARRAY", T8DataType.BYTE_ARRAY);
        commonDataTypeClasses.put(T8DataType.BYTE_ARRAY.getDataTypeClassName(), T8DataType.BYTE_ARRAY);
        dataTypeClasses.put(T8DataType.BYTE_ARRAY.getDataTypeIdentifier(), T8DtByteArray.class.getCanonicalName());

        dataTypeCache.put("COLOR_HEX", T8DataType.COLOR_HEX);
        commonDataTypeClasses.put(T8DataType.COLOR_HEX.getDataTypeClassName(), T8DataType.COLOR_HEX);

        dataTypeCache.put("CUSTOM_OBJECT", T8DataType.CUSTOM_OBJECT);
        commonDataTypeClasses.put(T8DataType.CUSTOM_OBJECT.getDataTypeClassName(), T8DataType.CUSTOM_OBJECT);
        dataTypeClasses.put(T8DataType.CUSTOM_OBJECT.getDataTypeIdentifier(), T8DtCustomObject.class.getCanonicalName());

        dataTypeCache.put("USER_DETAILS", T8DataType.USER_DETAILS);
        commonDataTypeClasses.put(T8DataType.USER_DETAILS.getDataTypeClassName(), T8DataType.USER_DETAILS);
        dataTypeClasses.put(T8DataType.USER_DETAILS.getDataTypeIdentifier(), T8DtUserDetails.class.getCanonicalName());

        dataTypeCache.put("DATE", T8DataType.DATE);
        commonDataTypeClasses.put(T8DataType.DATE.getDataTypeClassName(), T8DataType.DATE);
        dataTypeClasses.put(T8DataType.DATE.getDataTypeIdentifier(), T8DtDate.class.getCanonicalName());

        dataTypeCache.put("DEFINITION", T8DataType.DEFINITION);
        commonDataTypeClasses.put(T8DataType.DEFINITION.getDataTypeClassName(), T8DataType.DEFINITION);
        dataTypeClasses.put(T8DataType.DEFINITION.getDataTypeIdentifier(), T8DtDefinition.class.getCanonicalName());

        dataTypeCache.put("DEFINITION_GROUP_IDENTIFIER", T8DataType.DEFINITION_GROUP_IDENTIFIER);
        commonDataTypeClasses.put(T8DataType.DEFINITION_GROUP_IDENTIFIER.getDataTypeClassName(), T8DataType.DEFINITION_GROUP_IDENTIFIER);

        dataTypeCache.put("DEFINITION_GROUP_IDENTIFIER_LIST", T8DataType.DEFINITION_GROUP_IDENTIFIER_LIST);
        commonDataTypeClasses.put(T8DataType.DEFINITION_GROUP_IDENTIFIER_LIST.getDataTypeClassName(), T8DataType.DEFINITION_GROUP_IDENTIFIER_LIST);

        dataTypeCache.put("DEFINITION_IDENTIFIER", T8DataType.DEFINITION_IDENTIFIER);
        commonDataTypeClasses.put(T8DataType.DEFINITION_IDENTIFIER.getDataTypeClassName(), T8DataType.DEFINITION_IDENTIFIER);

        dataTypeCache.put("DEFINITION_IDENTIFIER_EXPRESSION_MAP", T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP);
        commonDataTypeClasses.put(T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP.getDataTypeClassName(), T8DataType.DEFINITION_IDENTIFIER_EXPRESSION_MAP);

        dataTypeCache.put("DEFINITION_IDENTIFIER_STRING_MAP", T8DataType.DEFINITION_IDENTIFIER_STRING_MAP);
        commonDataTypeClasses.put(T8DataType.DEFINITION_IDENTIFIER_STRING_MAP.getDataTypeClassName(), T8DataType.DEFINITION_IDENTIFIER_STRING_MAP);

        dataTypeCache.put("DEFINITION_IDENTIFIER_KEY_MAP", T8DataType.DEFINITION_IDENTIFIER_KEY_MAP);
        commonDataTypeClasses.put(T8DataType.DEFINITION_IDENTIFIER_KEY_MAP.getDataTypeClassName(), T8DataType.DEFINITION_IDENTIFIER_KEY_MAP);

        dataTypeCache.put("DEFINITION_IDENTIFIER_LIST", T8DataType.DEFINITION_IDENTIFIER_LIST);
        commonDataTypeClasses.put(T8DataType.DEFINITION_IDENTIFIER_LIST.getDataTypeClassName(), T8DataType.DEFINITION_IDENTIFIER_LIST);

        dataTypeCache.put("DEFINITION_IDENTIFIER_MAP", T8DataType.DEFINITION_IDENTIFIER_MAP);
        commonDataTypeClasses.put(T8DataType.DEFINITION_IDENTIFIER_MAP.getDataTypeClassName(), T8DataType.DEFINITION_IDENTIFIER_MAP);

        dataTypeCache.put("DEFINITION_IDENTIFIER_VALUE_MAP", T8DataType.DEFINITION_IDENTIFIER_VALUE_MAP);
        commonDataTypeClasses.put(T8DataType.DEFINITION_IDENTIFIER_VALUE_MAP.getDataTypeClassName(), T8DataType.DEFINITION_IDENTIFIER_VALUE_MAP);

        dataTypeCache.put("DEFINITION_INSTANCE_IDENTIFIER", T8DataType.DEFINITION_INSTANCE_IDENTIFIER);
        commonDataTypeClasses.put(T8DataType.DEFINITION_INSTANCE_IDENTIFIER.getDataTypeClassName(), T8DataType.DEFINITION_INSTANCE_IDENTIFIER);

        dataTypeCache.put("DEFINITION_LIST", T8DataType.DEFINITION_LIST);
        commonDataTypeClasses.put(T8DataType.DEFINITION_LIST.getDataTypeClassName(), T8DataType.DEFINITION_LIST);

        dataTypeCache.put("DEFINITION_TYPE_IDENTIFIER", T8DataType.DEFINITION_TYPE_IDENTIFIER);
        commonDataTypeClasses.put(T8DataType.DEFINITION_TYPE_IDENTIFIER.getDataTypeClassName(), T8DataType.DEFINITION_TYPE_IDENTIFIER);

        dataTypeCache.put("DISPLAY_STRING", T8DataType.DISPLAY_STRING);
        commonDataTypeClasses.put(T8DataType.DISPLAY_STRING.getDataTypeClassName(), T8DataType.DISPLAY_STRING);

        dataTypeCache.put("BIG_DECIMAL", T8DataType.BIG_DECIMAL);
        commonDataTypeClasses.put(T8DataType.BIG_DECIMAL.getDataTypeClassName(), T8DataType.BIG_DECIMAL);
        dataTypeClasses.put(T8DataType.BIG_DECIMAL.getDataTypeIdentifier(), T8DtBigDecimal.class.getCanonicalName());

        dataTypeCache.put("DOUBLE", T8DataType.DOUBLE);
        commonDataTypeClasses.put(T8DataType.DOUBLE.getDataTypeClassName(), T8DataType.DOUBLE);
        dataTypeClasses.put(T8DataType.DOUBLE.getDataTypeIdentifier(), T8DtDouble.class.getCanonicalName());

        dataTypeCache.put("EPIC_EXPRESSION", T8DataType.EPIC_EXPRESSION);
        commonDataTypeClasses.put(T8DataType.EPIC_EXPRESSION.getDataTypeClassName(), T8DataType.EPIC_EXPRESSION);

        dataTypeCache.put("EPIC_EXPRESSION_DISPLAY_STRING_MAP", T8DataType.EPIC_EXPRESSION_DISPLAY_STRING_MAP);
        commonDataTypeClasses.put(T8DataType.EPIC_EXPRESSION_DISPLAY_STRING_MAP.getDataTypeClassName(), T8DataType.EPIC_EXPRESSION_DISPLAY_STRING_MAP);

        dataTypeCache.put("EPIC_EXPRESSION_LIST", T8DataType.EPIC_EXPRESSION_LIST);
        commonDataTypeClasses.put(T8DataType.EPIC_EXPRESSION_LIST.getDataTypeClassName(), T8DataType.EPIC_EXPRESSION_LIST);

        dataTypeCache.put("EPIC_EXPRESSION_VALUE_MAP", T8DataType.EPIC_EXPRESSION_VALUE_MAP);
        commonDataTypeClasses.put(T8DataType.EPIC_EXPRESSION_VALUE_MAP.getDataTypeClassName(), T8DataType.EPIC_EXPRESSION_VALUE_MAP);

        dataTypeCache.put("EPIC_SCRIPT", T8DataType.EPIC_SCRIPT);
        commonDataTypeClasses.put(T8DataType.EPIC_SCRIPT.getDataTypeClassName(), T8DataType.EPIC_SCRIPT);

        dataTypeCache.put("FLOAT", T8DataType.FLOAT);
        commonDataTypeClasses.put(T8DataType.FLOAT.getDataTypeClassName(), T8DataType.FLOAT);
        dataTypeClasses.put(T8DataType.FLOAT.getDataTypeIdentifier(), T8DtFloat.class.getCanonicalName());

        dataTypeCache.put("GUID", T8DataType.GUID);
        commonDataTypeClasses.put(T8DataType.GUID.getDataTypeClassName(), T8DataType.GUID);

        dataTypeCache.put("IDENTIFIER_STRING", T8DataType.IDENTIFIER_STRING);
        commonDataTypeClasses.put(T8DataType.IDENTIFIER_STRING.getDataTypeClassName(), T8DataType.IDENTIFIER_STRING);

        dataTypeCache.put("IMAGE", T8DataType.IMAGE);
        commonDataTypeClasses.put(T8DataType.IMAGE.getDataTypeClassName(), T8DataType.IMAGE);
        dataTypeClasses.put(T8DataType.IMAGE.getDataTypeIdentifier(), T8DtImage.class.getCanonicalName());

        dataTypeCache.put("BIG_INTEGER", T8DataType.BIG_INTEGER);
        commonDataTypeClasses.put(T8DataType.BIG_INTEGER.getDataTypeClassName(), T8DataType.BIG_INTEGER);
        dataTypeClasses.put(T8DataType.BIG_INTEGER.getDataTypeIdentifier(), T8DtInteger.class.getCanonicalName());

        dataTypeCache.put("INTEGER", T8DataType.INTEGER);
        commonDataTypeClasses.put(T8DataType.INTEGER.getDataTypeClassName(), T8DataType.INTEGER);
        dataTypeClasses.put(T8DataType.INTEGER.getDataTypeIdentifier(), T8DtInteger.class.getCanonicalName());

        dataTypeCache.put("LIST", T8DataType.LIST);
        commonDataTypeClasses.put(T8DataType.LIST.getDataTypeClassName(), T8DataType.LIST);
        dataTypeClasses.put(T8DataType.LIST.getDataTypeIdentifier(), T8DtList.class.getCanonicalName());

        dataTypeCache.put("LONG", T8DataType.LONG);
        commonDataTypeClasses.put(T8DataType.LONG.getDataTypeClassName(), T8DataType.LONG);
        dataTypeClasses.put(T8DataType.LONG.getDataTypeIdentifier(), T8DtLong.class.getCanonicalName());

        dataTypeCache.put("LONG_STRING", T8DataType.LONG_STRING);
        commonDataTypeClasses.put(T8DataType.LONG_STRING.getDataTypeClassName(), T8DataType.LONG_STRING);

        dataTypeCache.put("MAP", T8DataType.MAP);
        commonDataTypeClasses.put(T8DataType.MAP.getDataTypeClassName(), T8DataType.MAP);
        dataTypeClasses.put(T8DataType.MAP.getDataTypeIdentifier(),T8DtMap.class.getCanonicalName());

        dataTypeCache.put("NULL", T8DataType.UNDEFINED);
        commonDataTypeClasses.put(T8DataType.UNDEFINED.getDataTypeClassName(), T8DataType.UNDEFINED);
        dataTypeClasses.put(T8DataType.UNDEFINED.getDataTypeIdentifier(), T8DtUndefined.class.getCanonicalName());

        dataTypeCache.put("PASSWORD", T8DataType.PASSWORD_HASH);
        commonDataTypeClasses.put(T8DataType.PASSWORD_HASH.getDataTypeClassName(), T8DataType.PASSWORD_HASH);

        dataTypeCache.put("STRING", T8DataType.STRING);
        commonDataTypeClasses.put(T8DataType.STRING.getDataTypeClassName(), T8DataType.STRING);
        dataTypeClasses.put(T8DataType.STRING.getDataTypeIdentifier(), T8DtString.class.getCanonicalName());

        dataTypeCache.put("TIME", T8DataType.TIME);
        commonDataTypeClasses.put(T8DataType.TIME.getDataTypeClassName(), T8DataType.TIME);
        dataTypeClasses.put(T8DataType.TIME.getDataTypeIdentifier(), T8DtTime.class.getCanonicalName());

        dataTypeCache.put("TIMESTAMP", T8DataType.TIMESTAMP);
        commonDataTypeClasses.put(T8DataType.TIMESTAMP.getDataTypeClassName(), T8DataType.TIMESTAMP);
        dataTypeClasses.put(T8DataType.TIMESTAMP.getDataTypeIdentifier(), T8DtTimestamp.class.getCanonicalName());

        dataTypeCache.put("DATE_TIME", T8DataType.DATE_TIME);
        commonDataTypeClasses.put(T8DataType.DATE_TIME.getDataTypeClassName(), T8DataType.DATE_TIME);
    }

    public T8DataType getLegacyDataType(String identifier)
    {
        return dataTypeCache.get(identifier);
    }

    public T8DataType createNewDataType(Element xmlElement)
    {
        String identifier;
        String className;

        identifier = "@" + xmlElement.getTagName();
        className = dataTypeClasses.get(identifier);
        if (className != null)
        {
            try
            {
                return T8Reflections.getFastFailInstance(className, new Class<?>[]{T8DefinitionManager.class, Element.class}, this.definitionManager, xmlElement);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while creating new data type '" + identifier + "' from class: " + className, e);
            }
        }
        else throw new RuntimeException("No class found to create data type: " + identifier);
    }

    public T8DataType createDataType(String dataTypeString)
    {
        T8DataType cachedDataType;

        // Check for a cached type and if one is found use it, otherwise create a new type.
        cachedDataType = dataTypeCache.get(dataTypeString);
        if (cachedDataType != null) return cachedDataType;
        else
        {
            if (dataTypeString != null)
            {
                String identifier;
                String className;
                int endIndex;

                // First find the index where the Identifier part of the data type stops.
                endIndex = dataTypeString.indexOf('<');
                if (endIndex == -1) endIndex = dataTypeString.indexOf('(');

                // Parse the identifier.
                if (endIndex == -1) // No nested types.
                {
                    identifier = dataTypeString;
                }
                else if (endIndex > 1) // A valid identifier must be at least 2 characters, which means the bracket must be at least at index 2.
                {
                    identifier = dataTypeString.substring(0, endIndex);
                }
                else throw new IllegalArgumentException("Invalid Data Type String: " + dataTypeString);

                // Create the type and return it.
                className = dataTypeClasses.get(identifier);
                if (className != null)
                {
                    try
                    {
                        T8DataType newDataType;

                        // Construct the new data type.
                        newDataType = T8Reflections.getFastFailInstance(className, new Class<?>[]{T8DefinitionManager.class, String.class}, this.definitionManager, dataTypeString);

                        // Cache the new datat type.
                        dataTypeCache.put(dataTypeString, newDataType);
                        return newDataType;
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException("Exception while creating new data type '" + identifier + "' from class: " + className, e);
                    }
                }
                else throw new RuntimeException("No class found to create data type: " + identifier);
            }
            else return T8DataType.UNDEFINED;
        }
    }

    public T8DataType getCommonDataTypeByObjectClass(String objectClassName)
    {
        return commonDataTypeClasses.get(objectClassName);
    }

    public T8DataType createNewDataTypeForObject(String objectClassName)
    {
        throw new RuntimeException("No Data Type found matching object Class: " + objectClassName);
    }
}