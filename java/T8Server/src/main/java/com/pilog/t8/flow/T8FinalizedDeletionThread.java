package com.pilog.t8.flow;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.thread.T8ContextRunnable;
import java.util.List;
import java.util.Set;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
class T8FinalizedDeletionThread extends T8ContextRunnable
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8FinalizedDeletionThread.class);

    private final T8ServerFlowManager flowController;
    private final T8DefaultFlowStateHandler stateHandler;
    private boolean enabled;

    public T8FinalizedDeletionThread(T8Context context, T8ServerFlowManager flowController, T8DefaultFlowStateHandler stateHandler, boolean enabled)
    {
        super(context, T8FinalizedDeletionThread.class.getName());
        this.flowController = flowController;
        this.stateHandler = stateHandler;
        this.enabled = enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    private Set<String> retrieveFinalizedFlowIids() throws Exception
    {
        List<T8DataEntity> queuedFlowEntities;
        T8DataFilter dataFilter;
        T8DataTransaction tx;

        // Use an instant transaction for the read.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Retrieve the entities for the finalized flows using FIFO order.
        dataFilter = new T8DataFilter(STATE_FLOW_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_DE_IDENTIFIER + F_STATUS, T8Flow.FlowStatus.FINALIZED.toString()));
        dataFilter.addFieldOrdering(STATE_FLOW_DE_IDENTIFIER + F_TIME_STARTED, T8DataFilter.OrderMethod.ASCENDING);
        queuedFlowEntities = tx.select(STATE_FLOW_DE_IDENTIFIER, dataFilter);

        // Get just the flow instance identifiers for the starter threads
        return T8DataUtilities.getEntityFieldValueSet(queuedFlowEntities, STATE_FLOW_DE_IDENTIFIER + F_FLOW_IID);
    }

    @Override
    public void exec()
    {
        if (enabled)
        {
            try
            {
                Set<String> finalizedFlowIids;
                T8DataTransaction tx;

                // Deletion of a flow state involves many tables so we use an instant transaction to reduce the transaction length.
                // All flows marked as 'FINALIZED' are considered to be deleted and therefore we do not need to ensure absolute atomicity.
                // Performance of the deletion and the reduction of locks on the flow tables is considered paramount.
                tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

                // Retrieve a batch of finalized flow iids.
                finalizedFlowIids = retrieveFinalizedFlowIids();
                while (!finalizedFlowIids.isEmpty())
                {
                    for (String flowIid : finalizedFlowIids)
                    {
                        stateHandler.deleteFlowState(tx, flowIid);
                    }

                    // Retrieve the next batch of finalized flow iids.
                    finalizedFlowIids = retrieveFinalizedFlowIids();
                }
            }
            catch (Throwable e)
            {
                LOGGER.log("Exception while performing task priority escalation.", e);
            }
        }
    }
}