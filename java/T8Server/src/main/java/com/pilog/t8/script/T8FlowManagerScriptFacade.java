package com.pilog.t8.script;

import com.pilog.t8.T8FlowManager;
import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.flow.task.T8TaskCompletionResult;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.flow.task.T8TaskList;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowManagerScriptFacade
{
    private final T8FlowManager flowManager;
    private final T8Context context;

    public T8FlowManagerScriptFacade(T8Context context, T8FlowManager flowManager)
    {
        this.flowManager = flowManager;
        this.context = context;
    }

    public int getFlowCount()
    {
        return flowManager.getFlowCount(context);
    }

    public int getCachedFlowCount()
    {
        return flowManager.getCachedFlowCount(context);
    }

    public T8TaskListSummary getTaskListSummary(T8TaskFilter taskFilter) throws Exception
    {
        return flowManager.getTaskListSummary(context, taskFilter);
    }

    public T8TaskList getTaskList(T8TaskFilter taskFilter) throws Exception
    {
        return flowManager.getTaskList(context, taskFilter);
    }

    public void claimTask(String taskIid) throws Exception
    {
        flowManager.claimTask(context, taskIid);
    }

    public void unclaimTask(String taskIid) throws Exception
    {
        flowManager.unclaimTask(context, taskIid);
    }

    public T8TaskCompletionResult completeTask(String taskIid, Map<String, Object> outputParameters) throws Exception
    {
        return flowManager.completeTask(context, taskIid, outputParameters);
    }

    public T8FlowStatus queueFlow(String flowId, Map<String, Object> inputParameters) throws Exception
    {
        return flowManager.queueFlow(context, flowId, inputParameters);
    }

    public T8FlowStatus startNewFlow(String flowId, Map<String, Object> inputParameters) throws Exception
    {
        return flowManager.startFlow(context, flowId, inputParameters);
    }

    public T8TaskDetails getTaskDetails(String taskIid) throws Exception
    {
        return flowManager.getTaskDetails(context, taskIid);
    }

    public void setTaskPriority(String taskIid, int priority) throws Exception
    {
        flowManager.setTaskPriority(context, taskIid, priority);
    }

    public void decreaseTaskPriority(String taskIid, int amount) throws Exception
    {
        flowManager.decreaseTaskPriority(context, taskIid, amount);
    }

    public void increaseTaskPriority(String taskIid, int amount) throws Exception
    {
        flowManager.increaseTaskPriority(context, taskIid, amount);
    }

    public void reassignTask(String taskIid, String userIdentifier, String profileIdentifier) throws Exception
    {
        flowManager.reassignTask(context, taskIid, userIdentifier, profileIdentifier);
    }
}