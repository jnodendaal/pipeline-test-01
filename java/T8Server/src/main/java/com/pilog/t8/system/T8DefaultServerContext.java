package com.pilog.t8.system;

import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8ConfigurationManager;
import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8FlowManager;
import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8NotificationManager;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8ReportManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8Server;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.T8ServiceManager;
import com.pilog.t8.T8UserManager;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.system.event.T8ServerEventListener;
import com.pilog.t8.definition.operation.T8ServerOperationDefinition;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultServerContext implements T8ServerContext
{
    private final T8Server server;

    public T8DefaultServerContext(T8Server mainServer)
    {
        this.server = mainServer;
    }

    @Override
    public T8ServiceManager getServiceManager()
    {
        return server.getServiceManager();
    }

    @Override
    public T8ProcessManager getProcessManager()
    {
        return server.getProcessManager();
    }

    @Override
    public T8FlowManager getFlowManager()
    {
        return server.getFlowManager();
    }

    @Override
    public T8FunctionalityManager getFunctionalityManager()
    {
        return server.getFunctionalityManager();
    }

    @Override
    public T8DefinitionManager getDefinitionManager()
    {
        return server.getDefinitionManager();
    }

    @Override
    public T8SecurityManager getSecurityManager()
    {
        return server.getSecurityManager();
    }

    @Override
    public T8DataManager getDataManager()
    {
        return server.getDataManager();
    }

    @Override
    public T8FileManager getFileManager()
    {
        return server.getFileManager();
    }

    @Override
    public T8ConfigurationManager getConfigurationManager()
    {
        return server.getConfigurationManager();
    }

    @Override
    public T8CommunicationManager getCommunicationManager()
    {
        return server.getCommunicationManager();
    }

    @Override
    public T8NotificationManager getNotificationManager()
    {
        return server.getNotificationManager();
    }

    @Override
    public T8UserManager getUserManager()
    {
        return server.getUserManager();
    }

    @Override
    public T8ReportManager getReportManager()
    {
        return server.getReportManager();
    }

    @Override
    public File getContextPath()
    {
        return new File(server.getContextPath());
    }

    @Override
    public T8ServerOperationDefinition getOperationDefinition(T8Context context, String operationId)
    {
        return server.getOperationDefinition(context, operationId);
    }

    @Override
    public File getRealPath(String relativePath)
    {
        return new File(server.getRealPath(relativePath));
    }

    @Override
    public void addServerEventListener(T8ServerEventListener listener)
    {
        server.addServerEventListener(listener);
    }

    @Override
    public void removeServerEventListener(T8ServerEventListener listener)
    {
        server.removeServerEventListener(listener);
    }

    @Override
    public List<T8ServerOperationDefinition> getOperationDefinitions()
    {
        return server.getOperationDefinitions();
    }

    @Override
    public T8ServerOperationStatusReport getOperationStatus(T8Context context, String operationInstanceIdentifier) throws Exception
    {
        return server.getOperationStatus(context, operationInstanceIdentifier);
    }

    @Override
    public T8ServerOperationStatusReport stopOperation(T8Context context, String operationInstanceIdentifier) throws Exception
    {
        return server.stopOperation(context, operationInstanceIdentifier);
    }

    @Override
    public T8ServerOperationStatusReport cancelOperation(T8Context context, String operationInstanceIdentifier) throws Exception
    {
        return server.cancelOperation(context, operationInstanceIdentifier);
    }

    @Override
    public T8ServerOperationStatusReport pauseOperation(T8Context context, String operationInstanceIdentifier) throws Exception
    {
        return server.pauseOperation(context, operationInstanceIdentifier);
    }

    @Override
    public T8ServerOperationStatusReport resumeOperation(T8Context context, String operationInstanceIdentifier) throws Exception
    {
        return server.resumeOperation(context, operationInstanceIdentifier);
    }

    @Override
    public T8ServerOperationStatusReport executeAsynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        return server.executeAsynchronousOperation(context, operationId, operationParameters);
    }

    @Override
    public Map<String, Object> executeSynchronousOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        return server.executeSynchronousOperation(context, operationId, operationParameters);
    }

    @Override
    public T8ExpressionEvaluator getExpressionEvaluator(T8Context context)
    {
        return new T8ServerExpressionEvaluator(context);
    }

    @Override
    public List<T8ServerOperationStatusReport> getServerOperationStatusReports(T8Context context)
    {
        return server.getServerOperationStatusReports(context);
    }

    @Override
    public String queueOperation(T8Context context, String operationId, Map<String, Object> operationParameters) throws Exception
    {
        return server.queueOperation(context, operationId, operationParameters);
    }
}
