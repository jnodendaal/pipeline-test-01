package com.pilog.t8.data;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.datastructures.stacks.FixedSizeStack;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.date.Dates;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ServerDataSession implements T8DataSession
{
    private final String id;
    private final String sesionId;
    private final T8Context context;
    private final T8DefinitionManager definitionManager;
    private final T8DataManager dataManager;
    private final String parentThreadName; // This is set when the session is created and can never again be changed.
    private Thread parentThread;
    private T8ServerDataTransaction instantTransaction;
    private T8PerformanceStatistics stats;
    private final Map<String, T8DataTransaction> transactions;
    private final Map<Thread, T8DataTransaction> threadTransactions;
    private final FixedSizeStack transactionLogs;
    private final long creationTime;
    private long lastActivityTime;

    public T8ServerDataSession(T8Context context, String id)
    {
        this.context = context;
        this.id = id;
        this.sesionId = this.context.getSessionContext().getSessionIdentifier();
        this.definitionManager = context.getServerContext().getDefinitionManager();
        this.dataManager = context.getServerContext().getDataManager();
        this.transactions = Collections.synchronizedMap(new HashMap<String, T8DataTransaction>());
        this.threadTransactions = Collections.synchronizedMap(new HashMap<Thread, T8DataTransaction>());
        this.creationTime = System.currentTimeMillis();
        this.transactionLogs = new FixedSizeStack(10);
        this.parentThread = Thread.currentThread();
        this.parentThreadName = parentThread.getName();
        this.stats = this.context.getSessionContext().getPerformanceStatistics();
        updateLastActivityTime();
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public Thread getParentThread()
    {
        return parentThread;
    }

    @Override
    public T8Context getContext()
    {
        return context;
    }

    @Override
    public T8DataManager getDataManager()
    {
        return dataManager;
    }

    @Override
    public T8DefinitionManager getDefinitionManager()
    {
        return definitionManager;
    }

    @Override
    public boolean isActive()
    {
        return transactions.size() > 0 || (instantTransaction != null && instantTransaction.isActive());
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    @Override
    public T8DataSessionDetails getDetails()
    {
        T8DataSessionDetails sessionDetails;

        sessionDetails = new T8DataSessionDetails(id);
        sessionDetails.setCreationTime(creationTime);
        sessionDetails.setLastActivityTime(lastActivityTime);
        sessionDetails.setParentSessionIdentifier(sesionId);
        sessionDetails.setActiveTransactionCount(transactions.size());
        sessionDetails.setParentThreadName(parentThreadName);
        if (context != null)
        {
            T8SessionContext sessionContext;

            sessionContext = context.getSessionContext();
            if (sessionContext.isSystemSession())
            {
                sessionDetails.setUserIdentifier(sessionContext.getSystemAgentIdentifier());
            }
            else
            {
                sessionDetails.setUserIdentifier(sessionContext.getUserIdentifier());
                sessionDetails.setUserProfileIdentifier(sessionContext.getUserProfileIdentifier());
            }
        }

        return sessionDetails;
    }

    void closeTransaction(T8DataTransaction transaction)
    {
        updateLastActivityTime();
        transactions.remove(transaction.getIdentifier());
        threadTransactions.remove(transaction.getParentThread());

        // If the transaction being closed is the instant transaction, reset the reference to null.
        if (transaction == instantTransaction) instantTransaction = null;
    }

    @Override
    public synchronized T8ServerDataTransaction instantTransaction()
    {
        try
        {
            if (instantTransaction == null)
            {
                // Create a new instant transaction.
                instantTransaction = new T8ServerDataTransaction(context, this, T8IdentifierUtilities.createNewGUID(), true);
                return instantTransaction;
            }
            else // Check the existing transaction and return it.
            {
                if (!instantTransaction.checkConnections())
                {
                    try
                    {
                        // Try to close the existing instant transaction to release any acquired resources.
                        instantTransaction.close();
                    }
                    catch (Exception e)
                    {
                        T8Log.log("Exception while closing instant transaction before recreation.", e);
                    }

                    // Create a new instant transaction to replace the broken existing one.
                    instantTransaction = new T8ServerDataTransaction(context, this, T8IdentifierUtilities.createNewGUID(), true);
                }

                // Return the instant transaction.
                return instantTransaction;
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while creating instant transaction in data session: " + this, e);
        }
    }

    @Override
    public boolean hasInstantTransaction()
    {
        return instantTransaction != null;
    }

    @Override
    public synchronized void refreshInstantTransaction()
    {
        // We just close the instant transaction - it will be recreated when
        // it is needed.
        closeInstantTransaction();
    }

    private synchronized void closeInstantTransaction()
    {
        if (instantTransaction != null)
        {
            instantTransaction.close();
            instantTransaction = null;
        }
    }

    @Override
    public synchronized T8DataTransaction getTransaction()
    {
        Thread currentThread;

        updateLastActivityTime();
        currentThread = Thread.currentThread();
        return threadTransactions.get(currentThread);
    }

    @Override
    public synchronized T8DataTransaction suspend()
    {
        T8DataTransaction txToSuspend;
        Thread currentThread;

        updateLastActivityTime();
        currentThread = Thread.currentThread();
        txToSuspend = threadTransactions.remove(currentThread);
        if (txToSuspend != null)
        {
            txToSuspend.suspend();
        }

        return txToSuspend;
    }

    @Override
    public synchronized void resume(T8DataTransaction tx)
    {
        Thread currentThread;

        updateLastActivityTime();
        currentThread = Thread.currentThread();
        threadTransactions.put(currentThread, tx);
        tx.resume();
    }

    @Override
    public synchronized T8DataTransaction beginTransaction()
    {
        T8DataTransaction existingTransaction;
        Thread currentThread;

        // Get the current thread and the existing transaction associated with it (if any).
        currentThread = Thread.currentThread();
        existingTransaction = threadTransactions.get(currentThread);
        if (existingTransaction == null)
        {
            try
            {
                T8DataTransaction newTransaction;

                // Create a new transaction.
                updateLastActivityTime();
                newTransaction = new T8ServerDataTransaction(context, this, T8IdentifierUtilities.createNewGUID(), false);
                transactions.put(newTransaction.getIdentifier(), newTransaction);
                threadTransactions.put(currentThread, newTransaction);
                transactionLogs.push(newTransaction.getLog());

                // Log a warning if the number of transactions seems irregular.
                if (transactions.size() > 50)
                {
                    T8Log.log("WARNING:  Irregular number of open transactions detected in data session '" + this + "': " + transactions.size());
                }

                // Return the newly created transaction.
                return newTransaction;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while creating transaction in data session: " + this, e);
            }
        }
        else throw new RuntimeException("Nested transactions not supported.  Thread '" + currentThread + "' is already associated with transaction: " + existingTransaction);
    }

    synchronized void commitSession()
    {
        List<T8DataTransaction> transactionList;

        updateLastActivityTime();

        // Commit all open data transactions.
        transactionList = new ArrayList<T8DataTransaction>(transactions.values());
        for (T8DataTransaction transaction : transactionList)
        {
            try
            {
                transaction.commit();
            }
            catch (Exception e)
            {
                T8Log.log("Exception while committing transaction:" + transaction, e);
            }
        }
    }

    synchronized void rollbackSession()
    {
        List<T8DataTransaction> transactionList;

        updateLastActivityTime();

        // Commit all open data transactions.
        transactionList = new ArrayList<T8DataTransaction>(transactions.values());
        for (T8DataTransaction transaction : transactionList)
        {
            try
            {
                transaction.rollback();
            }
            catch (Exception e)
            {
                T8Log.log("Exception while committing transaction:" + transaction, e);
            }
        }
    }

    @Override
    public void closeSession()
    {
        List<T8DataTransaction> transactionList;

        // Commit all open data transactions.
        // At this point, no transaction should still be open.  Transactions still present in this collection have not been properly committed/rolled-back and will be forcibly closed.
        transactionList = new ArrayList<T8DataTransaction>(transactions.values());
        for (T8DataTransaction transaction : transactionList)
        {
            try
            {
                // All open transactions are rolled-back when the session is closed.
                T8Log.log("WARNING:  '" + transaction +"' still active while parent session '" + this + "' is being closed.");
                transaction.rollback();
            }
            catch (Exception e)
            {
                T8Log.log("Exception while committing transaction:" + transaction, e);
            }
        }

        // Close the instant transaction.
        closeInstantTransaction();

        // Remove reference to parent thread.
        parentThread = null;
    }

    void updateLastActivityTime()
    {
        lastActivityTime = System.currentTimeMillis();
    }

    @Override
    public long getLastActivityTime()
    {
        return lastActivityTime;
    }

    @Override
    public String toString()
    {
        StringBuilder builder;

        builder = new StringBuilder();
        builder.append("T8ServerDataSession{");
        builder.append("identifier=");
        builder.append(id);
        builder.append(", parentSessionIdentifier=");
        builder.append(sesionId);
        builder.append(", parentThread=");
        builder.append(parentThreadName);
        builder.append(", creationTime=");
        builder.append(Dates.STD_SYS_TS_FORMATTER.format(creationTime));
        builder.append(", lastActivityTime=");
        builder.append(Dates.STD_SYS_TS_FORMATTER.format(lastActivityTime));
        builder.append('}');
        return builder.toString();
    }
}
