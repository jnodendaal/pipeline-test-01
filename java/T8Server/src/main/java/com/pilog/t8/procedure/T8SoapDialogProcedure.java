package com.pilog.t8.procedure;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.connection.T8SoapConnection;
import com.pilog.t8.definition.procedure.T8SoapDialogProcedureDefinition;
import com.pilog.t8.definition.remote.server.connection.T8SoapConnectionDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.xml.XmlDocument;
import java.util.Iterator;
import java.util.Map;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Bouwer du Preez
 */
public class T8SoapDialogProcedure implements T8Procedure
{
    private final T8SoapDialogProcedureDefinition definition;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private T8ServerContextScript script;
    private T8SoapConnection connection;

    public T8SoapDialogProcedure(T8Context context, T8SoapDialogProcedureDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
        createConnection();
    }

    private void createConnection()
    {
        String connectionId;

        connectionId = definition.getConnectionId();
        if (connectionId != null)
        {
            try
            {
                T8SoapConnectionDefinition connectionDefinition;

                connectionDefinition = (T8SoapConnectionDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, definition.getRootProjectId(), connectionId, null);
                if (connectionDefinition != null)
                {
                    this.connection = (T8SoapConnection)connectionDefinition.getNewConnectionInstance(context);
                }
                else throw new RuntimeException("Connection specified in procedure " + definition + " not found: " + connectionId);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while configuring SOAP connection in procedure: " + definition, e);
            }
        }
        else throw new RuntimeException("No connection specified for procedure: " + definition);
    }

    @Override
    public void init()
    {
    }

    @Override
    public double getProgress()
    {
        return script != null ? script.getProgress() : -1;
    }

    @Override
    public T8ProgressReport getProgressReport()
    {
        return script != null ? script.getProgressReport() : null;
    }

    @Override
    public Map<String, Object> execute(T8Context accessContext, Map<String, Object> operationParameters) throws Exception
    {
        // Instantiate the script if we haven't done so yet.
        if (script == null)
        {
            script = definition.getNewScriptInstance(context);
            script.prepareScript();
            script.addMethodImport("sendRequest", this, T8SoapDialogProcedure.class.getDeclaredMethod("sendRequest", XmlDocument.class));
        }

        // Execute the prepared script and return the results.
        return script.executePreparedScript(operationParameters);
    }

    @Override
    public void stop()
    {
        script.getScriptExecutionHandle().setStopped(true);
    }

    @Override
    public void cancel()
    {
        script.getScriptExecutionHandle().setCancelled(true);
    }

    @Override
    public void pause()
    {
        script.getScriptExecutionHandle().setPaused(true);
    }

    @Override
    public void resume()
    {
        script.getScriptExecutionHandle().setPaused(false);
    }

    public XmlDocument sendRequest(XmlDocument document) throws T8SoapDialogException
    {
        try
        {
            MessageFactory messageFactory;
            Document requestContentDocument;
            SOAPMessage requestMessage;
            SOAPMessage responseMessage;
            SOAPPart requestSoapPart;
            SOAPEnvelope requestSoapEnvelope;
            SOAPHeader requestHeader;
            SOAPBody requestBody;

            // Create the request envelope.
            messageFactory = MessageFactory.newInstance();
            requestMessage = messageFactory.createMessage();
            requestSoapPart = requestMessage.getSOAPPart();
            requestSoapEnvelope = requestSoapPart.getEnvelope();
            requestHeader = requestSoapEnvelope.getHeader();
            requestBody = requestSoapEnvelope.getBody();

            // Set an empty soap action (required for some services).
            requestMessage.getMimeHeaders().addHeader("SOAPAction", "\"\""); // May be specified in the definition in future.

            // Add the content to the body of the envelope.
            requestContentDocument = document.createDocument();
            requestBody.addDocument(requestContentDocument);
            requestMessage.saveChanges();

            // Send the request and wait for the response.
            responseMessage = connection.call(requestMessage);
            if (responseMessage != null)
            {
                SOAPBody responseBody;
                SOAPFault soapFault;

                responseBody = responseMessage.getSOAPBody();
                soapFault = responseBody.getFault();
                if (soapFault != null)
                {
                    XmlDocument faultDocument;

                    // Create an XmlDocument from the SOAPFault and throw an exception so the invoking script can handle it.
                    faultDocument = new XmlDocument();
                    faultDocument.setContent(soapFault);
                    throw new T8SoapDialogException(faultDocument);
                }
                else // No SOAP fault so the request succeeded.
                {
                    Iterator<Element> elementIterator;

                    // Get the first element in the response body.  SOAP 1.2 allows for more than one body entry but this method does not implement such a use case.
                    elementIterator = responseBody.getChildElements();
                    if (elementIterator.hasNext())
                    {
                        XmlDocument responseDocument;

                        // Create an XmlDocument from the body element and return it so that incoking script can process the result.
                        responseDocument = new XmlDocument();
                        responseDocument.setContent(elementIterator.next());
                        return responseDocument;
                    }
                    else return null; // No body entries found, so we return null to indicate an empty response.
                }
            }
            else return null; // Return a null when no response message was received.
        }
        catch (T8SoapDialogException e)
        {
            throw e;
        }
        catch (Throwable e)
        {
            throw new T8SoapDialogException("An unhandled exception occurred during SOAP dialog " + definition + " to connection: " + definition.getConnectionId(), e);
        }
    }
}
