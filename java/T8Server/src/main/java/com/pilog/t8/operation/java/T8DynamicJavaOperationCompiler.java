package com.pilog.t8.operation.java;

import com.pilog.t8.system.T8OperationCompilationResult;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.compiler.CompilationPackage;
import com.pilog.t8.compiler.CompilationPackageLoader;
import com.pilog.t8.compiler.CompilationReport.CompilationError;
import com.pilog.t8.compiler.CompilationUnit;
import com.pilog.t8.compiler.InMemoryCompiler;
import com.pilog.t8.compiler.InMemoryCompiler.CompilerException;
import com.pilog.t8.definition.operation.java.T8DynamicJavaOperationDefinition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.mainserver.T8ClassScanner;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.cache.LRUCache;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DynamicJavaOperationCompiler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DynamicJavaOperationCompiler.class);
    private static final LRUCache<String, Class> classCache = new LRUCache<>(500);
    private final T8ServerContext serverContext;
    private final T8Context context;
    private static String classpath;

    public T8DynamicJavaOperationCompiler(T8Context context)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
    }

    private String getClassPath()
    {
        if (classpath == null)
        {
            StringBuilder newPath;
            T8ClassScanner classScanner;

            // Construct the class path.
            classScanner = new T8ClassScanner(serverContext, T8ClassScanner.SERVER_SIDE_JAR_PATH);
            newPath = new StringBuilder();
            for (File jarFile : classScanner.getJarFiles())
            {
                newPath.append(jarFile.getAbsolutePath());
                newPath.append(File.pathSeparator);
            }

            classpath = newPath.toString();
            return classpath;
        }
        else return classpath;
    }

    public T8ServerOperation loadOperation(T8DynamicJavaOperationDefinition definition, String operationIid) throws Exception
    {
        String className;
        Class compiledClass;

        // Get the name of the class to load and check the cache.
        className = definition.getClassName();
        compiledClass = classCache.get(className);
        if (compiledClass != null)
        {
            // Instantiate the operation.
            return (T8ServerOperation)compiledClass.getConstructor(T8Context.class, String.class).newInstance(context, operationIid);
        }
        else
        {
            Map<String, byte[]> bytecode;

            // Get the bytecode of the class to load.
            bytecode = definition.getByteCode();
            if (bytecode != null)
            {
                Map<String, Class<?>> classes;
                CompilationPackageLoader loader;

                // Load the compiled class.
                loader = new CompilationPackageLoader();
                classes = loader.loadAsMap(bytecode);
                compiledClass = classes.get(className);

                // Instantiate the operation.
                return (T8ServerOperation)compiledClass.getConstructor(T8Context.class, String.class).newInstance(context, operationIid);
            }
            else throw new Exception("Operation not compiled: " + definition);
        }
    }

    public T8OperationCompilationResult compileOperation(T8DynamicJavaOperationDefinition definition)
    {
        try
        {
            Map<String, byte[]> byteCode;
            InMemoryCompiler compiler;
            CompilationPackage pkg;
            String sourceCode;
            String className;

            // Get the class name and source code.
            className = definition.getClassName();
            sourceCode = definition.getSourceCode();

            // Clear the cached version of this class.
            classCache.remove(className);

            // Log the start of the compilation.
            LOGGER.log("Compiling operation " + definition + " using class name " + className + "...");

            // Compile the source code.
            compiler = new InMemoryCompiler();
            pkg = compiler.singleCompile(className, sourceCode, getClassPath());

            // Add the compiled byte code units to the definition.
            byteCode = new HashMap<>();
            for (CompilationUnit unit : pkg.getUnits())
            {
                byteCode.put(unit.getName(), unit.getBytecode());
            }
            definition.setByteCode(byteCode);
            LOGGER.log("Compilation of operation " + definition + " complete.");
            return new T8OperationCompilationResult(true);
        }
        catch (CompilerException e)
        {
            T8OperationCompilationResult result;

            result = new T8OperationCompilationResult(false);
            result.setOptions(e.getReport().getOptions());
            for (CompilationError error : e.getReport().getErrors())
            {
                result.addError(error.getSourceObject(), error.getLineNumber(), error.getColumnNumber(), error.getMessage());
            }

            return result;
        }
        catch (Exception e)
        {
            LOGGER.log("Exception during compilation of operation: " + definition, e);
            return new T8OperationCompilationResult(false, e.getMessage());
        }
    }
}
