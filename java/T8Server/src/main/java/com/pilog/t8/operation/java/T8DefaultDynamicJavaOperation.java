package com.pilog.t8.operation.java;

import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDynamicJavaOperation extends T8DefaultServerOperation
{
    public T8DefaultDynamicJavaOperation(T8Context context, String operationIid)
    {
        super(context, null, operationIid);
    }

    @Override
    public Map<String, ? extends Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
    {
        return null;
    }
}
