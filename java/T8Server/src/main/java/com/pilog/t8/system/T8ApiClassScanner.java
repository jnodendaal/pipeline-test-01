package com.pilog.t8.system;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.mainserver.T8ClassScanner;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import com.pilog.t8.api.T8Api;
import java.lang.reflect.Modifier;

/**
 * @author Bouwer du Preez
 */
public class T8ApiClassScanner
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ApiClassScanner.class);

    private T8ApiClassScanner()
    {
    }

    public static Map<String, String> getAPIClassMap(T8ServerContext serverContext)
    {
        T8ClassScanner classScanner;
        HashMap<String, String> apiClassMap;
        Set<Class> classes;

        classScanner = new T8ClassScanner(serverContext, T8ClassScanner.SERVER_SIDE_JAR_PATH);
        classes = classScanner.getPackageClasses("com.pilog.t8.api");
        apiClassMap = new HashMap<>();
        for (Class apiClass : classes)
        {
            if (T8Api.class.isAssignableFrom(apiClass))
            {
                if (!Modifier.isAbstract(apiClass.getModifiers()))
                {
                    try
                    {
                        String apiId;
                        String className;

                        apiId = (String)apiClass.getField("API_IDENTIFIER").get(null);
                        className = apiClass.getCanonicalName();
                        apiClassMap.put(apiId, className);
                        LOGGER.log("Including API " + apiId + " in class: " + className);
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while checking API Identifier in class: " + apiClass.getCanonicalName(), e);
                        apiClassMap.put(apiClass.getCanonicalName(), "Not Found");
                    }
                }
            }
        }

        return apiClassMap;
    }
}
