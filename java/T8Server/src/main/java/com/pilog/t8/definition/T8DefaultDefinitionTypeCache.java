package com.pilog.t8.definition;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.mainserver.T8ClassScanner;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultDefinitionTypeCache implements T8DefinitionTypeCache
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultDefinitionTypeCache.class);

    private final Map<String, String> typeMap; // Key: Definition Identifier, Value: Definition Type Identifier.
    private final Map<String, T8DefinitionTypeMetaData> typeMetaDataMap; // Key: Definition Type Identifier, Value: Definition Type Meta Data Object.
    private final Map<String, T8DefinitionGroupMetaData> groupMetaDataMap; // Key: Definition Group Identifier, Value: Definition Group Meta Data Object.
    private final T8ServerContext serverContext;

    public T8DefaultDefinitionTypeCache(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.typeMap = Collections.synchronizedMap(new HashMap<String, String>());
        this.typeMetaDataMap = new HashMap<>();
        this.groupMetaDataMap = new HashMap<>();
    }

    @Override
    public void clearCache()
    {
        typeMap.clear();
        typeMetaDataMap.clear();
        groupMetaDataMap.clear();
    }

    @Override
    public void cacheDefinitionTypes()
    {
        LOGGER.log("Caching definition types...");
        loadDefinitionTypes();
    }

    private void loadDefinitionTypes()
    {
        T8ClassScanner classScanner;
        Set<Class> classes;

        // We were keeping multiple copies of the jar file references in memory. We only want it available during this scan
        classScanner = new T8ClassScanner(serverContext, T8ClassScanner.SERVER_SIDE_JAR_PATH);

        // Find only the non-abstract classes and add all of them to the type cache.
        classes = classScanner.getPackageClasses("com.pilog.t8.definition");
        for (Class definitionClass : classes)
        {
            if (T8Definition.class.isAssignableFrom(definitionClass))
            {
                if (!Modifier.isAbstract(definitionClass.getModifiers()))
                {
                    try
                    {
                        T8DefinitionTypeMetaData typeMetaData;
                        String typeId;

                        typeMetaData = T8DefinitionUtilities.getDefinitionTypeMetaData(definitionClass);
                        typeId = typeMetaData.getTypeId();
                        if (!typeMetaDataMap.containsKey(typeId))
                        {
                            String groupId;

                            groupId = typeMetaData.getGroupId();
                            typeMetaDataMap.put(typeId, typeMetaData);
                            groupMetaDataMap.put(groupId, T8DefinitionUtilities.getDefinitionGroupMetaData(definitionClass));
                        }
                        else
                        {
                            throw new Exception("Duplicate Definition Type Identifier '" + typeMetaData.getTypeId() + "' found in class: " + typeMetaData.getClassName() + ".  Type also defined in class: " + typeMetaDataMap.get(typeId).getClassName());
                        }
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while loading definition type: " + definitionClass, e);
                    }
                }
                else
                {
                    T8DefinitionGroupMetaData groupMetaData;

                    groupMetaData = T8DefinitionUtilities.getDefinitionGroupMetaData(definitionClass);
                    if (groupMetaData != null)
                    {
                        groupMetaDataMap.put(groupMetaData.getGroupId(), groupMetaData);
                    }
                }
            }
        }
    }

    @Override
    public int getCachedDefinitionTypeCount()
    {
        return typeMetaDataMap.size();
    }

    @Override
    public List<String> getTypeIdentifiers()
    {
        return new ArrayList<>(typeMetaDataMap.keySet());
    }

    @Override
    public List<String> getTypeIdentifiers(String groupIdentifier)
    {
        ArrayList<String> identifierList;

        identifierList = new ArrayList<>();
        for (T8DefinitionTypeMetaData typeMetaData : typeMetaDataMap.values())
        {
            if (typeMetaData.getGroupId().equals(groupIdentifier))
            {
                identifierList.add(typeMetaData.getTypeId());
            }
        }

        return identifierList;
    }

    @Override
    public List<String> getGroupIdentifiers()
    {
        return new ArrayList<>(groupMetaDataMap.keySet());
    }

    @Override
    public List<T8DefinitionTypeMetaData> getDefinitionTypeMetaData()
    {
        return new ArrayList<>(typeMetaDataMap.values());
    }

    @Override
    public List<T8DefinitionGroupMetaData> getDefinitionGroupMetaData()
    {
        return new ArrayList<>(groupMetaDataMap.values());
    }

    @Override
    public T8DefinitionTypeMetaData getDefinitionTypeMetaData(String typeId)
    {
        return typeMetaDataMap.get(typeId);
    }

    @Override
    public List<T8DefinitionTypeMetaData> getGroupDefinitionTypeMetaData(String groupId)
    {
        ArrayList<T8DefinitionTypeMetaData> metaDataList;

        metaDataList = new ArrayList<>();
        for (T8DefinitionTypeMetaData typeMetaData : typeMetaDataMap.values())
        {
            if (typeMetaData.getGroupId().equals(groupId))
            {
                metaDataList.add(typeMetaData);
            }
        }

        return metaDataList;
    }
}
