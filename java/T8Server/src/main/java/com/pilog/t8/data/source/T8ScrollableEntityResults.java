package com.pilog.t8.data.source;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResultListener;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.utilities.jdbc.JDBCUtilities;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ScrollableEntityResults implements T8DataEntityResults
{
    private final T8DataSourceDefinition sourceDefinition;
    private final T8DataEntityDefinition entityDefinition;
    private T8DataEntity nextEntity;
    private final Statement statement;
    private final ResultSet resultSet;
    private final Map<String, StreamType> streamTypes;
    private final List<T8DataEntityResultListener> listeners;

    public T8ScrollableEntityResults(T8DataSourceDefinition sourceDefinition, T8DataEntityDefinition entityDefinition, Statement statement, ResultSet resultSet)
    {
        this.sourceDefinition = sourceDefinition;
        this.entityDefinition = entityDefinition;
        this.statement = statement;
        this.resultSet = resultSet;
        this.streamTypes = new HashMap<>();
        this.listeners = new ArrayList<>();
    }

    @Override
    public void setStreamType(String fieldIdentifier, StreamType streamType)
    {
        this.streamTypes.put(fieldIdentifier, streamType);
    }

    @Override
    public void close()
    {
        // Close the result set and statement from which this results object retrieves.
        JDBCUtilities.close(resultSet, statement);

        // Fire the closed event.
        for (T8DataEntityResultListener listener : listeners)
        {
            listener.entityResultsClosed();
        }

        // Clear the listeners as they will no longer be used.
        listeners.clear();
    }

    @Override
    public boolean next() throws Exception
    {
        // Fetch the next entity from the resultSet.
        nextEntity = T8CommonStatementHandler.getNextResultSetDataEntity(sourceDefinition, entityDefinition, resultSet, streamTypes);

        // Fire the retrieved event.
        for (T8DataEntityResultListener listener : listeners)
        {
            listener.entityRetrieved();
        }

        // Return the entity.
        return nextEntity != null;
    }

    @Override
    public T8DataEntity get()
    {
        return nextEntity;
    }

    @Override
    public String getEntityIdentifier()
    {
        return entityDefinition.getIdentifier();
    }

    @Override
    public void addListener(T8DataEntityResultListener listener)
    {
        listeners.add(listener);
    }

    @Override
    public void removeListener(T8DataEntityResultListener listener)
    {
        listeners.remove(listener);
    }
}
