package com.pilog.t8.flow.node.event;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.definition.flow.node.event.T8EndEventDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EndEventNode extends T8DefaultFlowNode
{
    private final T8EndEventDefinition definition;

    public T8EndEventNode(T8Context context, T8Flow parentFlow, T8EndEventDefinition definition, String nodeIid)
    {
        super(context, parentFlow, definition, nodeIid);
        this.definition = definition;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, final Map<String, Object> inputParameters) throws Exception
    {
        // Execute the script.
        return runScript(inputParameters);
    }

    private Map<String, Object> runScript(Map<String, Object> inputParameters) throws Exception
    {
        T8ServerContextScriptDefinition scriptDefinition;
        Object outputObject;
        T8Script script;

        // Execute the script.
        scriptDefinition = definition.getScriptDefinition();
        script = scriptDefinition.getNewScriptInstance(internalContext);
        outputObject = script.executeScript(inputParameters);

        // Process the script output.
        if (outputObject == null)
        {
            // We have to return a valid map to incdicate successful execution completion.
            return new HashMap<String, Object>();
        }
        else if (outputObject instanceof Map)
        {
            return T8IdentifierUtilities.stripNamespace(definition.getNamespace(), (Map<String, Object>)outputObject, false);
        }
        else throw new RuntimeException("Script activity '" + definition + "' did not return a valida Map object as output.");
    }
}
