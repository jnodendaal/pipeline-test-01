package com.pilog.t8.service;

import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.security.T8Context;
import java.util.Map;

import static com.pilog.t8.definition.service.T8ServiceManagerAPIHandler.*;

/**
 * @author Bouwer du Preez
 */
public class T8ServiceManagerOperations
{
    public static class ExecuteServiceOperationOperation extends T8DefaultServerOperation
    {
        public ExecuteServiceOperationOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Map<String, Object>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            String serviceIdentifier;
            String operationIdentifier;
            Map<String, Object> inputParameterMap;
            Map<String, Object> outputParameterMap;

            serviceIdentifier = (String)operationParameters.get(PARAMETER_SERVICE_IDENTIFIER);
            operationIdentifier = (String)operationParameters.get(PARAMETER_OPERATION_IDENTIFIER);
            inputParameterMap = (Map<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETER_MAP);
            outputParameterMap = serverContext.getServiceManager().executeServiceOperation(context, serviceIdentifier, operationIdentifier, inputParameterMap);

            return HashMaps.createSingular(PARAMETER_OUTPUT_PARAMETER_MAP, outputParameterMap);
        }
    }
}
