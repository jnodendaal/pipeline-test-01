package com.pilog.t8.report;

import com.pilog.t8.T8ReportManager;
import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

import static com.pilog.t8.definition.report.T8ReportManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ReportManagerOperations
{
    public static class ApiGenerateReportOperation extends T8DefaultServerOperation
    {
        public ApiGenerateReportOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ReportManager reportManager;
            Map<String, Object> reportParameters;
            String reportId;

            reportManager = serverContext.getReportManager();
            reportId = (String)operationParameters.get(PARAMETER_REPORT_ID);
            reportParameters = (Map<String, Object>)operationParameters.get(PARAMETER_REPORT_PARAMETERS);
            return HashMaps.newHashMap(PARAMETER_PROCESS_IID, reportManager.generateReport(context, reportId, reportParameters));
        }
    }

    public static class ApiDeleteReportOperation extends T8DefaultServerOperation
    {
        public ApiDeleteReportOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ReportManager reportManager;
            String reportIid;

            reportManager = serverContext.getReportManager();
            reportIid = (String)operationParameters.get(PARAMETER_REPORT_IID);
            return HashMaps.newHashMap(PARAMETER_SUCCESS, reportManager.deleteReport(context, reportIid));
        }
    }
}
