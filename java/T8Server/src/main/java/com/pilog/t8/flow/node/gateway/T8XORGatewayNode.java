package com.pilog.t8.flow.node.gateway;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.definition.flow.node.gateway.T8FlowForkConditionDefinition;
import com.pilog.t8.definition.flow.node.gateway.T8XORGatewayDefinition;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8XORGatewayNode extends T8DefaultFlowNode
{
    private final T8XORGatewayDefinition definition;
    private String exclusiveOutputNodeIdentifier;

    public T8XORGatewayNode(T8Context context, T8Flow parentFlow, T8XORGatewayDefinition definition, String instanceIdentifier)
    {
        super(context, parentFlow, definition, instanceIdentifier);
        this.definition = definition;
        this.exclusiveOutputNodeIdentifier = null;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters)
    {
        Map<String, Object> expressionInputParameters;

        // Strip the flow namespace from the expression input parameters.
        expressionInputParameters = T8IdentifierUtilities.stripNamespace(definition.getNamespace(), inputParameters, false);

        // Evaluate gateway conditions.
        evaluateGatewayConditions(expressionInputParameters);

        // Return the input parameters as output, since we didn't change any of them.
        return inputParameters;
    }

    @Override
    protected boolean isOutputNodeApplicable(String outputNodeIdentifier)
    {
        // If the exclusive output is null, no condition evaluated true.
        if (exclusiveOutputNodeIdentifier != null)
        {
            return exclusiveOutputNodeIdentifier.equals(outputNodeIdentifier);
        }
        else return false;
    }

    protected void evaluateGatewayConditions(Map<String, Object> expressionInputParameters)
    {
        ArrayList<T8FlowForkConditionDefinition> conditionDefinitions;
        ExpressionEvaluator expressionEvaluator;

        // Create a new expression evaluator to handle the condition checking.
        expressionEvaluator = new ExpressionEvaluator();

        // Find the correct fork definition and evaluate it.
        conditionDefinitions = definition.getForkConditionDefinitions();
        for (T8FlowForkConditionDefinition conditionDefinition : conditionDefinitions)
        {
            String expression;

            // Get the fork condition expression.
            expression = conditionDefinition.getConditionExpression();

            // Evaluate the expression and return the result.
            try
            {
                if (expressionEvaluator.evaluateBooleanExpression(expression, expressionInputParameters, null))
                {
                    exclusiveOutputNodeIdentifier = conditionDefinition.getNodeIdentifier();
                    return;
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while evaluating fork condition '" + expression + "' using parameters: " + expressionInputParameters, e);
            }
        }

        // If this point was reached, none of the condition expressions evaluated to true.
        exclusiveOutputNodeIdentifier = null;
    }
}
