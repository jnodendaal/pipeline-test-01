package com.pilog.t8.flow;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.flow.T8Flow.FlowStatus;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8WorkflowContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowCache
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8FlowCache.class);

    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8FlowController controller;
    private final T8FlowStateHandler stateHandler;
    private final Map<String, T8Flow> flows;

    public T8FlowCache(T8Context context, T8FlowController controller, T8FlowStateHandler stateHandler, final int maximumEntries)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.controller = controller;
        this.stateHandler = stateHandler;
        this.flows = Collections.synchronizedMap(new LinkedHashMap<String, T8Flow>(maximumEntries, .75F, true)
        {
            @Override
            protected boolean removeEldestEntry(Map.Entry<String, T8Flow> eldest)
            {
                return size() > maximumEntries;
            }
        });
    }

    public void addFlow(T8Flow flow)
    {
        flows.put(flow.getInstanceIdentifier(), flow);
    }

    public void removeFlow(String flowIid)
    {
        flows.remove(flowIid);
    }

    public T8Flow getFlow(String flowIid)
    {
        T8Flow flow;

        flow = flows.get(flowIid);
        if (flow != null)
        {
            LOGGER.log("Flow fetched from cache: " + flowIid);
            return flow;
        }
        else
        {
            T8FlowState flowState;

            LOGGER.log("Reinstating flow state: " + flowIid);
            try
            {
                flowState = stateHandler.loadFlowState(flowIid);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while loading flow state from persistence: " + flowIid, e);
            }

            // Now use the loaded flow state to reinstantiate the flow instance.
            if (flowState != null)
            {
                if (flowState.getFlowStatus() != FlowStatus.FINALIZED)
                {
                    T8WorkFlowDefinition flowDefinition;

                    // Instantiate the flow from its definition and state.
                    flowDefinition = getFlowDefinition(flowState.getFlowId());
                    if (flowDefinition != null)
                    {
                        try
                        {
                            T8Context executionContext;
                            String orgId;

                            // Create the execution context.
                            orgId = flowState.getInitiatorOrgId();
                            executionContext = new T8WorkflowContext(internalContext, flowState.getFlowId(), flowState.getFlowIid());
                            executionContext.setOrganizationId(orgId);

                            // Start the new flow.
                            flow = flowDefinition.getNewFlowInstance(executionContext, controller, stateHandler, flowState);
                            flows.put(flowState.getFlowIid(), flow); // Is the instance identifier from the flow state and not the newly created flow.
                            return flow;
                        }
                        catch (Exception e)
                        {
                            throw new RuntimeException("Exception while instantiating new flow instance from state: " + flowState, e);
                        }
                    }
                    else throw new RuntimeException("Cannot resume flow because the flow definition could not be found: " + flowState.getFlowId());
                }
                else throw new RuntimeException("Flow Instance marked as finalized: " + flowIid);
            }
            else throw new RuntimeException("Flow Instance not found: " + flowIid);
        }
    }

    public List<T8Flow> getCachedFlows()
    {
        return new ArrayList<>(flows.values());
    }

    public int clearNonExecutingFlows()
    {
        int cleardFlowCount;

        cleardFlowCount = 0;
        for (T8Flow cachedFlow : getCachedFlows())
        {
            synchronized (cachedFlow)
            {
                if (!cachedFlow.isExecuting())
                {
                    removeFlow(cachedFlow.getInstanceIdentifier());
                    cleardFlowCount++;
                }
            }
        }

        return cleardFlowCount;
    }

    private T8WorkFlowDefinition getFlowDefinition(String flowId)
    {
        try
        {
            return (T8WorkFlowDefinition)serverContext.getDefinitionManager().getInitializedDefinition(internalContext, null, flowId, null);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving flow definition: " + flowId, e);
        }
    }

    public int size()
    {
        return flows.size();
    }

    public boolean isEmpty()
    {
        return flows.isEmpty();
    }
}
