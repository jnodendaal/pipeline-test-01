package com.pilog.t8.flow.task.script;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8FlowController;
import com.pilog.t8.flow.task.T8FlowTask;
import com.pilog.t8.flow.task.T8FlowTaskStatus;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.ui.T8ComponentController;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.task.script.T8FlowScriptTaskDefinition;
import com.pilog.t8.definition.script.T8ServerContextScriptDefinition;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8FlowScriptTask implements T8FlowTask
{
    private T8FlowScriptTaskDefinition definition;
    private T8TaskDetails taskDetails;
    private TaskStatus status;
    private String statusMessage;

    public T8FlowScriptTask(T8FlowScriptTaskDefinition definition, T8TaskDetails taskDetails)
    {
        this.definition = definition;
        this.taskDetails = taskDetails;
        this.status = TaskStatus.NOT_STARTED;
    }

    @Override
    public T8FlowTaskDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public T8TaskDetails getTaskDetails()
    {
        return taskDetails;
    }

    @Override
    public void stopTask()
    {
    }

    @Override
    public void cancelTask()
    {
    }

    @Override
    public T8FlowTaskStatus getStatus()
    {
        return new T8FlowTaskStatus(taskDetails.getTaskId(), taskDetails.getTaskIid(), status, -1, statusMessage);
    }

    @Override
    public Map<String, Object> doTask(T8ComponentController controller, Map<String, Object> inputParameters) throws Exception
    {
        throw new UnsupportedOperationException("Script Task cannot be executed from within a client context.");
    }

    @Override
    public Map<String, Object> doTask(T8Context context, T8FlowController flowController, Map<String, Object> inputParameters) throws Exception
    {
        return runScript(context, inputParameters);
    }

    @Override
    public Map<String, Object> resumeTask(T8Context context, T8FlowController flowController, Map<String, Object> inputParameters) throws Exception
    {
        return runScript(context, inputParameters);
    }

    private Map<String, Object> runScript(T8Context context, Map<String, Object> inputParameters) throws Exception
    {
        T8ServerContextScriptDefinition scriptDefinition;
        T8Script script;
        Object outputObject;

        // Set the status to indicate that the task is active.
        status = TaskStatus.ACTIVE;

        // Execute the script.
        statusMessage = "Executing script.";
        scriptDefinition = definition.getScriptDefinition();
        script = scriptDefinition.getNewScriptInstance(context);
        outputObject = script.executeScript(inputParameters);
        statusMessage = "Script Execution completed.";

        // Set the status to indicate the completion of the task.
        status = TaskStatus.COMPLETED;

        if (outputObject == null) return null;
        else if (outputObject instanceof Map)
        {
            Map<String, Object> outputParameters;

            outputParameters = (Map<String, Object>)outputObject;
            return T8IdentifierUtilities.prependNamespace(definition.getIdentifier(), outputParameters);
        }
        else throw new Exception("Script '' returned non-Map result: " + outputObject);
    }
}
