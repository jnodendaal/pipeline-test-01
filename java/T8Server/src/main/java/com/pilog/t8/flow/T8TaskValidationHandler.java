package com.pilog.t8.flow;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.flow.task.T8TaskValidationResult;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.task.T8FlowTaskValidationScriptDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8WorkflowTaskContext;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Bouwer du Preez
 */
public class T8TaskValidationHandler
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8ServerFlowManager controller;
    private final T8DefinitionManager definitionManager;
    private final T8SecurityManager securityManager;

    public T8TaskValidationHandler(T8Context context, T8ServerFlowManager controller)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.controller = controller;
        this.definitionManager = serverContext.getDefinitionManager();
        this.securityManager = serverContext.getSecurityManager();
    }

    public T8TaskValidationResult validateTaskCompletion(T8Context context, T8FlowTaskState taskState, Map<String, Object> requestOutputParameters) throws Exception
    {
        T8FlowTaskDefinition taskDefinition;

        // Get the specified task definition.
        taskDefinition = (T8FlowTaskDefinition)definitionManager.getInitializedDefinition(internalContext, null, taskState.getTaskId(), null);
        if (taskDefinition != null)
        {
            return validateTaskCompletion(context, taskState, taskDefinition, requestOutputParameters);
        }
        else throw new Exception("Task Definition not found: " + taskState);
    }

    public T8TaskValidationResult validateTaskCompletion(T8Context context, T8FlowTaskState taskState, T8FlowTaskDefinition taskDefinition, Map<String, Object> requestOutputParameters) throws Exception
    {
        T8FlowTaskValidationScriptDefinition scriptDefinition;

        // Get the field creation script definition from the view definition.
        scriptDefinition = taskDefinition.getValidationScriptDefinition();
        if (scriptDefinition != null)
        {
            Map<String, Object> scriptInputParameters;
            T8ServerContextScript script;
            T8Context validationContext;
            Object outputObject;

            // Create the validation context.
            validationContext = new T8WorkflowTaskContext(internalContext, taskState.getFlowId(), taskState.getFlowIid(), taskState.getTaskId(), taskState.getTaskIid());
            validationContext.setProjectId(taskDefinition.getRootProjectId());
            validationContext.setDataAccessId(taskDefinition.getDataAccessId());

            // Execute the script and use the output to
            script = scriptDefinition.getNewScriptInstance(validationContext);

            // Create the script input parameter collection.
            scriptInputParameters = new HashMap<String, Object>();
            scriptInputParameters.putAll(taskState.getInputParameters());
            if (requestOutputParameters != null) scriptInputParameters.putAll(requestOutputParameters);

            // Process the script output if not null.
            outputObject = script.executeScript(scriptInputParameters);
            if (outputObject instanceof Map)
            {
                Map<String, Object> scriptOutputParameters;
                Boolean valid;

                // Check the flag indicating validation success and return the result accordingly.
                scriptOutputParameters = (Map)outputObject;
                valid = (Boolean)scriptOutputParameters.get(taskDefinition.getNamespace() + T8FlowTaskValidationScriptDefinition.PARAMETER_VALID);
                return new T8TaskValidationResult(taskState.getTaskIid(), ((valid != null) && (valid)), scriptOutputParameters);
            }
            else throw new RuntimeException("Task Validation Script '" + taskDefinition + "' did not return a valid Map object as output.");
        }
        else return new T8TaskValidationResult(taskState.getTaskIid(), true, requestOutputParameters);
    }
}
