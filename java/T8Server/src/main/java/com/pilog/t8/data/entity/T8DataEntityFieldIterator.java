package com.pilog.t8.data.entity;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataIterator;
import java.util.NoSuchElementException;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8DataEntityFieldIterator<E> implements T8DataIterator<E>
{
    private final T8DataTransaction tx;
    private final T8DataEntityResults entityResults;
    private T8DataEntity nextEntity;
    private T8DataEntity currentEntity;
    private final String fieldIdentifier;
    
    public T8DataEntityFieldIterator(T8DataTransaction tx, T8DataEntityResults entityResults, String fieldIdentifier) throws Exception
    {
        this.tx = tx;
        this.entityResults = entityResults;
        this.fieldIdentifier = fieldIdentifier;
        this.nextEntity = fetchNextEntity();
        this.currentEntity = null;
    }
    
    @Override
    public T8DataTransaction getDataAccessProvider()
    {
        return tx;
    }

    @Override
    public void close()
    {
        entityResults.close();
    }

    @Override
    public boolean hasNext()
    {
        return nextEntity != null;
    }

    @Override
    public E next()
    {
        if (hasNext())
        {
            try
            {
                currentEntity = nextEntity;
                nextEntity = fetchNextEntity();
                return (E)currentEntity.getFieldValue(fieldIdentifier);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while fetching next Data Entity.", e);
            }
        }
        else throw new NoSuchElementException("Cannot fetch next element from exhauseted iterator.");
    }

    @Override
    public void remove()
    {
        if (currentEntity != null)
        {
            try
            {
                tx.delete(currentEntity);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while removing data entity.", e);
            }
        }
        else throw new RuntimeException("Cannot remove from iterator without current element.");
    }
    
    private T8DataEntity fetchNextEntity() throws Exception
    {
        if (entityResults.next())
        {
            return entityResults.get();
        }
        else return null;
    }
}
