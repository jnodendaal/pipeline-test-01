package com.pilog.t8.functionality;

import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow.FlowStatus;
import com.pilog.t8.flow.T8FlowStatus;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.functionality.T8WorkflowFunctionalityDefinition;
import com.pilog.t8.definition.ng.T8NgComponentDefinition;
import com.pilog.t8.definition.ng.T8NgComponentInitializationScriptDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8WorkflowFunctionalityInstance extends T8DefaultFunctionalityInstance implements T8FunctionalityInstance
{
    private final T8WorkflowFunctionalityDefinition definition;
    private final String flowId;
    private String flowIid;
    private Map<String, Object> inputParameters;

    public T8WorkflowFunctionalityInstance(T8Context context, T8WorkflowFunctionalityDefinition definition)
    {
        super(context, definition, T8IdentifierUtilities.createNewGUID());
        this.definition = definition;
        this.flowId = definition.getFlowId();
    }

    public T8WorkflowFunctionalityInstance(T8Context context, T8WorkflowFunctionalityDefinition definition, T8FunctionalityState state)
    {
        super(context, definition, state.getFunctionalityIid());
        this.definition = definition;
        this.flowId = definition.getFlowId();
        this.flowIid = state.getFlowIid();
    }

    @Override
    public T8FunctionalityDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void initialize()
    {
    }

    @Override
    public boolean release()
    {
        try
        {
            T8FlowState flowState;

            flowState = serverContext.getFlowManager().getFlowState(context, flowIid);
            if ((flowState == null) || (flowState.getFlowStatus() == FlowStatus.FINALIZED))
            {
                return true;
            }
            else return (flowState.getFlowStatus() == FlowStatus.COMPLETED);
        }
        catch (Exception e)
        {
            T8Log.log("Exception while checking status of flow: " + flowIid + " as functionality: " + iid, e);
            return false;
        }
    }

    @Override
    public T8FunctionalityAccessHandle access(Map<String, Object> parameters) throws Exception
    {
        Map<String, Object> workflowInputParameters;
        Map<String, Object> strippedInputParameters;
        T8WorkflowFunctionalityAccessHandle handle;
        Boolean createViewComponent;
        T8FlowStatus flowStatus;
        String componentId;
        Boolean queueFlow;
        String projectId;

        // Store the input parameters.
        this.timeAccessed = new T8Timestamp(System.currentTimeMillis());
        this.inputParameters = parameters;

        // Create the workflow input parameter map.
        strippedInputParameters = T8IdentifierUtilities.stripNamespace(id, parameters, true);
        workflowInputParameters = T8IdentifierUtilities.mapParameters(strippedInputParameters, definition.getInputParameterMapping());

        // Evaluate parameter expressions.
        workflowInputParameters.putAll(evaluateFlowInputParameters());

        // Check the parameter value that specified whether or not a view component must be created.
        queueFlow = null;
        createViewComponent = null;
        if (strippedInputParameters != null)
        {
            queueFlow = (Boolean)strippedInputParameters.get(T8WorkflowFunctionalityDefinition.PARAMETER_QUEUE_FLOW);
            createViewComponent = (Boolean)strippedInputParameters.get(T8WorkflowFunctionalityDefinition.PARAMETER_CREATE_VIEW_COMPONENT);
        }

        // Set default values for parameters not received.
        if (queueFlow == null) queueFlow = false;
        if (createViewComponent == null) createViewComponent = true;

        // Queue/Start the flow.
        if (queueFlow)
        {
            flowStatus = serverContext.getFlowManager().queueFlow(context, flowId, workflowInputParameters);
        }
        else
        {
            flowStatus = serverContext.getFlowManager().startFlow(context, flowId, workflowInputParameters);
        }

        // Create the access handle to the flow.
        flowIid = flowStatus.getFlowIid();
        handle = new T8WorkflowFunctionalityAccessHandle(definition.getIdentifier(), iid, flowStatus.getFlowIid(), null, getDisplayName(), getDescription(), getIcon(), createViewComponent);
        handle.setFlowIid(flowIid);
        handle.setTaskIid(null);
        handle.setIconUri(definition.getIconUri());

        // Set the component details (if a component is specified).
        componentId = definition.getComponentId();
        if (componentId != null)
        {
            T8NgComponentDefinition componentDefinition;

            projectId = definition.getRootProjectId();
            componentDefinition = (T8NgComponentDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, projectId, componentId, null);
            if (componentDefinition != null)
            {
                Map<String, Object> initializationParameters;
                Map<String, Object> componentParameters;

                // Add the flow iid that has now been determined, to the stripped parameter collection.
                strippedInputParameters.put(T8WorkflowFunctionalityDefinition.PARAMETER_FLOW_IID, flowIid);

                // Create the component input parameter map.
                initializationParameters = T8IdentifierUtilities.mapParameters(strippedInputParameters, definition.getComponentParameterMapping());

                // Evaluate parameter expressions.
                initializationParameters.putAll(evaluateComponentInputParameters(strippedInputParameters));

                // Get the component parameters from the script output.
                initializationParameters = componentDefinition.executeComponentInitializationScript(context, initializationParameters);
                componentParameters = (Map<String, Object>)initializationParameters.get(T8NgComponentInitializationScriptDefinition.PARAMTER_COMPONENT_PARAMETERS);

                // Return the component details on the handle.
                handle.setComponentUri(componentDefinition.getComponentUri());
                handle.setComponentParameters(componentParameters);
            }
            else throw new Exception("Workflow component definition not found: " + componentId + " as specified in definition: " + definition);
        }

        // Return the final access handle.
        return handle;
    }

    @Override
    public boolean stop()
    {
        return true;
    }

    @Override
    public T8FunctionalityState getFunctionalityState()
    {
        T8FunctionalityState state;

        // Create the new state.
        state = super.getFunctionalityState(inputParameters);

        // Add the flow instance id so that this functionality can be removed when the flow ends.
        state.setFlowIid(flowIid);

        // Return the state.
        return state;
    }

    private Map<String, Object> evaluateFlowInputParameters() throws Exception
    {
        Map<String, String> expressionMap;
        Map<String, Object> resultMap;

        resultMap = new HashMap<String, Object>();
        expressionMap = definition.getInputParameterExpressionMap();
        if ((expressionMap != null) && (expressionMap.size() > 0))
        {
            HashMap<String, Object> expressionParameters;

            // Create a map of expression parameters.
            expressionParameters = new HashMap<String, Object>();

            // Construct a new expression evaluator to use.
            for (String parameterId : expressionMap.keySet())
            {
                String expression;
                Object result;

                expression = expressionMap.get(parameterId);
                result = expressionEvaluator.evaluateExpression(expression, expressionParameters, null);
                resultMap.put(parameterId, result);
            }
        }

        return resultMap;
    }

    private Map<String, Object> evaluateComponentInputParameters(Map<String, Object> strippedInputParameters) throws Exception
    {
        Map<String, String> expressionMap;
        Map<String, Object> resultMap;

        resultMap = new HashMap<String, Object>();
        expressionMap = definition.getComponentParameterExpressionMap();
        if ((expressionMap != null) && (expressionMap.size() > 0))
        {
            HashMap<String, Object> expressionParameters;

            // Create a map of expression parameters.
            expressionParameters = new HashMap<String, Object>(strippedInputParameters);

            // Construct a new expression evaluator to use.
            for (String parameterId : expressionMap.keySet())
            {
                String expression;
                Object result;

                expression = expressionMap.get(parameterId);
                result = expressionEvaluator.evaluateExpression(expression, expressionParameters, null);
                resultMap.put(parameterId, result);
            }
        }

        return resultMap;
    }
}
