package com.pilog.t8.file.handler;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.file.T8FileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.Maps;
import com.pilog.t8.utilities.excel.ExcelInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelInputFileHandler implements T8FileHandler
{
    private final T8Context context;
    private final T8FileManager fileManager;
    private final String fileContextIid;
    private final String filePath;
    private final boolean trimStrings;
    private final boolean convertEmptyStringsToNull;
    private ExcelInputStream excelInputStream;

    public static final String FILE_HANDLER_ID = "@FILE_EXCEL_INPUT";

    public T8ExcelInputFileHandler(T8Context context, T8FileManager fileManager, String contextIid, String filePath)
    {
        this.context = context;
        this.fileManager = fileManager;
        this.fileContextIid = contextIid;
        this.filePath = filePath;
        this.trimStrings = true;
        this.convertEmptyStringsToNull = true;
    }

    public boolean isOpen()
    {
        return (excelInputStream != null);
    }

    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIid, filePath);
    }

    @Override
    public void open() throws Exception
    {
        InputStream inputStream;

        inputStream = fileManager.getFileInputStream(context, fileContextIid, filePath);
        excelInputStream = new ExcelInputStream();
        excelInputStream.openFile(inputStream);
    }

    @Override
    public void close() throws Exception
    {
        if (excelInputStream != null) excelInputStream.closeFile();
        excelInputStream = null;
    }

    public int getRowCount(String sheetName) throws Exception
    {
        return excelInputStream.getSheetRowCount(sheetName);
    }

    public List<String> getSheetNames() throws Exception
    {
        return excelInputStream.getSheetNames();
    }

    public Map<String, Object> readRow(String sheetName) throws Exception
    {
        if (sheetName != null)
        {
            HashMap<String, Object> dataRow;

            // Read the next data row from the input file.
            dataRow = excelInputStream.readRow(sheetName);
            if (dataRow != null)
            {
                // Do some trimming if required.
                if (trimStrings) Maps.trimStringValues(dataRow, convertEmptyStringsToNull);
                else if (convertEmptyStringsToNull) Maps.setEmptyStringValuesToNull(dataRow);

                return dataRow; // Return the data row.
            }
            return null; // We have to return null if the row read from the file is null, to allow the invoker of this method to handle the end of file event.
        }
        else throw new IllegalArgumentException("Invalid sheet id: " + sheetName);
    }
}
