package com.pilog.t8.definition;

import com.pilog.t8.T8Log;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.utilities.files.FileUtilities;
import com.pilog.t8.utilities.strings.Strings;
import java.io.File;
import java.util.HashMap;

/**
 * @author Bouwer du Preez
 */
public class T8DefinitionFileHandler
{
    private static final T8Logger logger = T8Log.getLogger(T8DefinitionFileHandler.class.getName());

    public static final HashMap<String, String> loadDefinitions(File directory) throws Exception
    {
        HashMap<String, String> definitions;
        File[] fileList;

        definitions = new HashMap<>();
        fileList = directory.listFiles();
        for (File file : fileList)
        {
            try
            {
                if (file.isDirectory())
                {
                    logger.log(T8Logger.Level.WARNING, ()->"Directory found in projects directory. Skipping project : " + file.getPath());
                    continue;
                }

                String definitionData;
                String definitionIdentifier;

                definitionData = loadDefinition(file);
                if(Strings.isNullOrEmpty(definitionData)) throw new Exception("Definition Data is Empty for file " + file.getPath());
                definitionIdentifier = file.getName();
                definitionIdentifier = definitionIdentifier.substring(0, definitionIdentifier.indexOf('.'));
                definitions.put(definitionIdentifier, definitionData);
            }
            catch (Exception e)
            {
                logger.log("Exception while loading definition file: " + file, e);
            }
        }

        return definitions;
    }

    public static final String loadDefinition(File filePath) throws Exception
    {
        return FileUtilities.readTextFile(filePath, "UTF-8");
    }

    public static final boolean saveDefinition(File directory, String identifier, String definitionData) throws Exception
    {
        String fileName;

        fileName = directory.getCanonicalPath() + "/" + identifier + ".t8d";
        FileUtilities.writeTextFile(new File(fileName), "UTF-8", definitionData);
        return true;
    }

    public static final boolean deleteDefinition(File directory, String identifier) throws Exception
    {
        File definitionFile;

        definitionFile = new File(directory.getCanonicalPath() + "/" + identifier + ".t8d");
        return definitionFile.delete();
    }
}
