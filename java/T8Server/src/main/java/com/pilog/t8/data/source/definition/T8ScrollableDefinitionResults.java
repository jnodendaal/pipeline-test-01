package com.pilog.t8.data.source.definition;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResultListener;
import com.pilog.t8.data.T8DataEntityResults;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Hennie Brink
 */
public class T8ScrollableDefinitionResults implements T8DataEntityResults
{
    private final List<T8DataEntityResultListener> listeners;
    private final String entityIdentifier;
    private final Iterator<T8DataEntity> entities;
    private T8DataEntity nextDataEntity;

    public T8ScrollableDefinitionResults(String entityIdentifier, List<T8DataEntity> entities)
    {
        this.entityIdentifier = entityIdentifier;

        // Make sure we use a linked list here as it has the quickest scrolling performance
        this.entities = new LinkedList<>(entities).iterator();
        this.listeners = new ArrayList<>();
    }

    @Override
    public String getEntityIdentifier()
    {
        return entityIdentifier;
    }

    @Override
    public void setStreamType(String fieldIdentifier, StreamType streamType)
    {
    }

    @Override
    public boolean next() throws Exception
    {
        // We have to do this because the called might call get multiple times per iteration,
        // so we are only allowed to move foward in this method.
        if (entities.hasNext())
        {
            // Get the actual next entity
            nextDataEntity = entities.next();

            // Fire the retrieved event
            this.listeners.forEach(T8DataEntityResultListener::entityRetrieved);

            return true;
        } else return false;
    }

    @Override
    public T8DataEntity get()
    {
        return nextDataEntity;
    }

    @Override
    public void close()
    {
        // Fire the closed event
        this.listeners.forEach(T8DataEntityResultListener::entityResultsClosed);

        // Clear the listeners as they will no longer be used
        this.listeners.clear();
    }

    @Override
    public void addListener(T8DataEntityResultListener listener)
    {
        this.listeners.add(listener);
    }

    @Override
    public void removeListener(T8DataEntityResultListener listener)
    {
        this.listeners.remove(listener);
    }
}
