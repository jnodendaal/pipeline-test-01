package com.pilog.t8.operation.java;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.definition.operation.java.T8DynamicJavaOperationDefinition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8DynamicJavaOperation implements T8ServerOperation
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DynamicJavaOperation.class);

    private final T8DynamicJavaOperationDefinition definition;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final String operationIid;
    private T8ServerOperation compiledOperation;

    public T8DynamicJavaOperation(T8Context context, T8DynamicJavaOperationDefinition definition, String operationIid)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
        this.operationIid = operationIid;
    }

    @Override
    public void init()
    {
        try
        {
            T8DynamicJavaOperationCompiler compiler;

            compiler = new T8DynamicJavaOperationCompiler(context);
            compiledOperation = compiler.loadOperation(definition, operationIid);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while initializing operation: " + definition, e);
        }
    }

    @Override
    public double getProgress(T8Context context)
    {
        return compiledOperation.getProgress(context);
    }

    @Override
    public T8ProgressReport getProgressReport(T8Context context)
    {
        return compiledOperation.getProgressReport(context);
    }

    @Override
    public Map<String, ? extends Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
    {
        return compiledOperation.execute(operationParameters, executionType);
    }

    @Override
    public void stop(T8Context context)
    {
        compiledOperation.stop(context);
    }

    @Override
    public void cancel(T8Context context)
    {
        compiledOperation.cancel(context);
    }

    @Override
    public void pause(T8Context context)
    {
        compiledOperation.pause(context);
    }

    @Override
    public void resume(T8Context context)
    {
        compiledOperation.resume(context);
    }
}
