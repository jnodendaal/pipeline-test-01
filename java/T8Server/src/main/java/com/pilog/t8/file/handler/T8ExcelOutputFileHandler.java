package com.pilog.t8.file.handler;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.file.T8FileHandler;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.excel.ExcelOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ExcelOutputFileHandler implements T8FileHandler
{
    private final T8Context context;
    private final T8FileManager fileManager;
    private final String fileContextIid;
    private final String filePath;
    private ExcelOutputStream excelOutputStream;

    public static final String FILE_HANDLER_ID = "@FILE_EXCEL_OUTPUT";

    public T8ExcelOutputFileHandler(T8Context context, T8FileManager fileManager, String contextIid, String filePath)
    {
        this.context = context;
        this.fileManager = fileManager;
        this.fileContextIid = contextIid;
        this.filePath = filePath;
    }

    public boolean isOpen()
    {
        return (excelOutputStream != null);
    }

    public boolean isFileAvailable() throws Exception
    {
        return filePath == null ? false : fileManager.fileExists(context, fileContextIid, filePath);
    }

    @Override
    public void open() throws Exception
    {
        OutputStream outputStream;

        outputStream = fileManager.getFileOutputStream(context, fileContextIid, filePath, false);
        excelOutputStream = new ExcelOutputStream();
        excelOutputStream.openFile(outputStream);
    }

    @Override
    public void close() throws Exception
    {
        if (excelOutputStream != null) excelOutputStream.closeFile();
        excelOutputStream = null;
    }

    public void flush(String sheetName) throws Exception
    {
        excelOutputStream.flush(sheetName);
    }

    public void createSheet(String sheetName, List<String> columnNames) throws Exception
    {
        excelOutputStream.createSheet(sheetName, columnNames);
    }

    public void createSheet(String sheetName, Map<String, String> columnNameFormatPatternMap) throws Exception
    {
        List<String> columnNames;
        List<String> formatPatterns;

        columnNames = new ArrayList<>();
        formatPatterns = new ArrayList<>();
        for (String columnName : columnNameFormatPatternMap.keySet())
        {
            columnNames.add(columnName);
            formatPatterns.add(columnNameFormatPatternMap.get(columnName));
        }

        excelOutputStream.createSheet(sheetName, columnNames);
        excelOutputStream.setCellFormatPatterns(sheetName, formatPatterns);
    }

    public void writeSheet(String sheetName) throws Exception
    {
        excelOutputStream.writeSheet(sheetName);
    }

    public void addSheet(String sheetName, List<String> columnNames) throws Exception
    {
        excelOutputStream.writeSheet(sheetName, columnNames);
    }

    public void addSheet(String sheetName, Map<String, String> columnNameFormatPatternMap) throws Exception
    {
        List<String> columnNames;
        List<String> formatPatterns;

        columnNames = new ArrayList<>();
        formatPatterns = new ArrayList<>();
        for (String columnName : columnNameFormatPatternMap.keySet())
        {
            columnNames.add(columnName);
            formatPatterns.add(columnNameFormatPatternMap.get(columnName));
        }

        excelOutputStream.writeSheet(sheetName, columnNames);
        excelOutputStream.setCellFormatPatterns(sheetName, formatPatterns);
    }

    public void writeRow(String sheetName, Map<String, Object> rowData) throws Exception
    {
        if (sheetName != null)
        {
            // Write the row of data.
            excelOutputStream.writeRow(sheetName, rowData);
        }
        else throw new Exception("Could not resolve Excel output sheet name for sheet identifier: " + sheetName);
    }

    public void writeRow(String sheetName, List<Object> rowData) throws Exception
    {
        if (sheetName != null)
        {
            // Write the row of data.
            excelOutputStream.writeRow(sheetName, rowData);
        }
        else throw new Exception("Could not resolve Excel output sheet name for sheet identifier: " + sheetName);
    }

    public void setColumnWidth(String sheetName, String columnName, int width)
    {
        excelOutputStream.setColumnWidth(sheetName, columnName, width);
    }

    public void setColumnWidth(String sheetName, int width)
    {
        excelOutputStream.setColumnWidth(sheetName, width);
    }

    public void autoSizeColumn(String sheetName, String columnName)
    {
        excelOutputStream.autoSizeColumn(sheetName, columnName);
    }

    public void autoSizeColumns(String sheetName)
    {
        excelOutputStream.autoSizeColumns(sheetName);
    }

    public void setColumnColor(String sheetName, String columnName, int red, int green, int blue)
    {
        excelOutputStream.setColumnColor(sheetName, columnName, red, green, blue);
    }

    public void clearColumnStyle(String sheetName, String columnName)
    {
        excelOutputStream.clearColumnStyle(sheetName, columnName);
    }

    public void setDefaultColumnFormatPattern(String sheetName, String columnName , String formatPattern)
    {
        excelOutputStream.setDefaultColumnFormatPattern(formatPattern, sheetName, columnName);
    }

    public void setDefaultColumnFormatPattern(String sheetName, List<String> sheetColumnNames , String formatPattern)
    {
        for ( String columnName :  sheetColumnNames)
        {
            excelOutputStream.setDefaultColumnFormatPattern(formatPattern, sheetName, columnName);
        }
    }
}
