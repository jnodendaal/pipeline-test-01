package com.pilog.t8.procedure;

import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.procedure.T8ProcedureDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.security.T8Context;
import java.util.Map;

import static com.pilog.t8.definition.procedure.T8ProcedureResources.*;

/**
 * @author Bouwer du Preez
 */
public class T8ProcedureOperations
{
    public static class ExecuteProcedureOperation extends T8DefaultServerOperation
    {
        public ExecuteProcedureOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationInstanceIdentifier)
        {
            super(context, definition, operationInstanceIdentifier);
        }

        @Override
        public Map<String, Map<String, ? extends Object>> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8ProcedureDefinition procedureDefinition;
            Map<String, Object> inputParameters;
            T8Procedure procedure;
            String procedureId;
            String projectId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            procedureId = (String)operationParameters.get(PARAMETER_PROCEDURE_IDENTIFIER);
            procedureDefinition = (T8ProcedureDefinition)operationParameters.get(PARAMETER_PROCEDURE_DEFINITION);
            inputParameters = (Map<String, Object>)operationParameters.get(PARAMETER_INPUT_PARAMETERS);

            if (procedureDefinition == null)
            {
                procedureDefinition = serverContext.getDefinitionManager().getInitializedDefinition(context, projectId, procedureId, inputParameters);
                if (procedureDefinition == null) throw new Exception("Procedure not found: " + procedureId);
            }

            // Execute the procedure and return the results.
            procedure = procedureDefinition.getNewProcedureInstance(context);
            return HashMaps.createSingular(PARAMETER_OUTPUT_PARAMETERS, procedure.execute(context, inputParameters));
        }
    }
}
