package com.pilog.t8.data.source.table;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8RefreshableDataSource;
import com.pilog.t8.definition.data.source.table.T8RefreshableTableDataSourceDefinition;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.database.T8SqlScriptExecutor;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.script.T8ServerExpressionEvaluator;

/**
 * @author Bouwer du Preez
 */
public class T8RefreshableTableDataSource extends T8TableDataSource implements T8RefreshableDataSource
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8RefreshableTableDataSource.class);
    private final T8RefreshableTableDataSourceDefinition definition;

    public T8RefreshableTableDataSource(T8RefreshableTableDataSourceDefinition definition, T8DataTransaction tx)
    {
        super(definition, tx);
        this.definition = definition;
    }

    @Override
    public void refresh(Map<String, Object> refreshParameters) throws Exception
    {
        Map<String, Object> sqlParameterMap;
        T8SqlScriptExecutor scriptExecutor;
        String refreshScript;

        // Get the parameters to be used by the script.
        sqlParameterMap = getScriptParameters(refreshParameters);

        // Run the SQL Refresh script.
        refreshScript = definition.getSqlRefreshScript();
        scriptExecutor = new T8SqlScriptExecutor(connection, false, true);
        scriptExecutor.executeScript(refreshScript, sqlParameterMap);
    }

    private Map<String, Object> getScriptParameters(Map<String, Object> refreshParameters) throws Exception
    {
        Map<String, String> expressionMap;
        Map<String, Object> resultMap;

        // Create a new map and add all definined refresh parameters to it.
        resultMap = new HashMap<>();
        for (T8DataParameterDefinition parameterDefinition : definition.getRefreshParameterDefinitions())
        {
            String parameterId;

            parameterId = parameterDefinition.getPublicIdentifier();
            resultMap.put(T8IdentifierUtilities.stripNamespace(parameterId), refreshParameters != null ? refreshParameters.get(parameterId) : null);
        }

        // Evaluate all of the additional parameters expressions.
        expressionMap = definition.getSQLParameterExpressionMap();
        if ((expressionMap != null) && (expressionMap.size() > 0))
        {
            T8ServerExpressionEvaluator expressionEvaluator;
            HashMap<String, Object> expressionParameters;

            // Create a map of expression parameters.
            expressionParameters = new HashMap<>();
            expressionParameters.put("dbAdaptor", connection.getDatabaseAdaptor());
            expressionParameters.putAll(resultMap);

            // Construct a new expression evaluator to use.
            expressionEvaluator = new T8ServerExpressionEvaluator(tx.getContext());
            for (String parameterIdentifier : expressionMap.keySet())
            {
                String expression;
                Object result;

                expression = expressionMap.get(parameterIdentifier);
                result = expressionEvaluator.evaluateExpression(expression, expressionParameters, null);
                if (result instanceof String)
                {
                    resultMap.put(parameterIdentifier, (String) result);
                }
                else
                {
                    throw new RuntimeException("SQL Parameter Expression return invalid object type '" + (result != null ? result.getClass().getCanonicalName() : null) + "': " + expression);
                }
            }
        }

        // Return the final map of script parameters.
        return resultMap;
    }
}
