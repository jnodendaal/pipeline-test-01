package com.pilog.t8.notification;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.notification.T8Notification.NotificationHistoryEvent;
import com.pilog.t8.notification.state.T8NotificationState;
import com.pilog.t8.state.T8StateDataSet;
import com.pilog.t8.data.T8HistoryParameterPersistenceHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.notification.T8NotificationDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.pilog.t8.definition.notification.T8NotificationManagerResource;
import com.pilog.t8.state.T8StateUpdates;
import com.pilog.t8.utilities.collections.ArrayLists;

import static com.pilog.t8.definition.notification.T8NotificationManagerResource.*;
import static com.pilog.t8.data.filter.T8DataFilterCriterion.*;

/**
 * @author Bouwer du Preez
 */
public class T8NotificationManagerDataHandler
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8DefinitionManager definitionManager;

    private static final T8Logger LOGGER = T8Log.getLogger(T8NotificationManagerDataHandler.class);

    public T8NotificationManagerDataHandler(T8Context context)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.definitionManager = serverContext.getDefinitionManager();
    }

    public T8NotificationSummary getNotificationSummary(T8Context context, T8NotificationFilter filter) throws Exception
    {
        T8NotificationSummary summary;
        List<T8DataEntity> entityList;
        String filterEntityId;
        String entityId;
        T8DataSession dataSession;
        T8DataTransaction tx;

        if (filter == null)
        {
            filter = new T8NotificationFilter();
            filter.setIncludeNewNotifications(true);
            filter.setIncludeReceivedNotifications(true);
        }

        // Get the identifiers for the entities to use.
        entityId = STATE_NOTIFICATION_SUMMARY_DE_IDENTIFIER;
        filterEntityId = STATE_NOTIFICATION_DE_IDENTIFIER;

        // Retrieve the summary data.
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.instantTransaction();
        entityList = tx.select(entityId, filter.getDataFilter(context, filterEntityId));

        // Create a new summary object and add all of the retrieved entries to it.
        summary = new T8NotificationSummary();
        for (T8DataEntity entity : entityList)
        {
            T8NotificationTypeSummary notificationTypeSummary;
            int entityCount;

            // Get the notification type summary, creating it if it doesn't exist
            notificationTypeSummary = getNotificationTypeSummary(summary, entity, entityId);

            // Increment the total count for the notification type.
            entityCount = (Integer)entity.getFieldValue(entityId + F_NOTIFICATION_COUNT);
            notificationTypeSummary.setTotalCount(notificationTypeSummary.getTotalCount() + entityCount);

            // If the task is claimed increment the claimed count for the task type.
            if ("SENT".equals(entity.getFieldValue(entityId + F_STATUS)))
            {
                notificationTypeSummary.setNewCount(notificationTypeSummary.getNewCount() + entityCount);
            }
        }

        return summary;
    }

    private T8NotificationTypeSummary getNotificationTypeSummary(T8NotificationSummary summary, T8DataEntity notificationSummaryEntity, String entityId) throws Exception
    {
        T8NotificationTypeSummary notificationTypeSummary;
        String notificationId;

        notificationId = (String)notificationSummaryEntity.getFieldValue(entityId + F_NOTIFICATION_ID);
        notificationTypeSummary = summary.getNotificationTypeSummary(notificationId);
        if (notificationTypeSummary == null)
        {
            T8NotificationDefinition notificationDefinition;
            String notificationDisplayName;

            notificationDefinition = (T8NotificationDefinition)definitionManager.getRawDefinition(internalContext, null, notificationId);
            notificationDisplayName = notificationDefinition != null ? notificationDefinition.getNotificationDisplayName() : T8IdentifierUtilities.readableIdentifier(notificationId.replace("@NOTIF_", ""));

            notificationTypeSummary = new T8NotificationTypeSummary(notificationId);
            notificationTypeSummary.setDisplayName(notificationDisplayName);
            notificationTypeSummary.setDescription(null);

            summary.putNotificationTypeSummary(notificationTypeSummary);
        }

        return notificationTypeSummary;
    }

    public int retrieveNotificationCount(T8Context context, T8NotificationFilter filter) throws Exception
    {
        T8DataTransaction tx;
        T8DataFilter dataFilter;

        // Retrieve the notification state entities.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        dataFilter = (filter == null) ? null : filter.getDataFilter(context, STATE_NOTIFICATION_DE_IDENTIFIER);
        return tx.count(STATE_NOTIFICATION_DE_IDENTIFIER, dataFilter);
    }

    public List<T8Notification> retrieveNotifications(T8Context context, T8NotificationFilter filter) throws Exception
    {
        List<T8DataEntity> notificationStateEntities;
        List<T8Notification> notifications;
        T8StateDataSet dataSet;
        T8DataTransaction tx;
        T8DataFilter dataFilter;

        // Retrieve the notification state entities.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        dataFilter = (filter == null) ? null : filter.getDataFilter(context, STATE_NOTIFICATION_DE_IDENTIFIER);
        notificationStateEntities = tx.select(STATE_NOTIFICATION_DE_IDENTIFIER, dataFilter, (filter != null) ? filter.getPageOffset() : 0, (filter != null) ? filter.getPageSize() : 100);

        // Create the complete states from each entity retrieved.
        notifications = new ArrayList<>(notificationStateEntities.size());
        for (T8DataEntity notificationStateEntity : notificationStateEntities)
        {
            dataSet = new T8StateDataSet();
            dataSet.addData(tx.select(STATE_NOTIFICATION_PAR_DE_IDENTIFIER, HashMaps.createTypeSafeMap(new String[]{STATE_NOTIFICATION_PAR_DE_IDENTIFIER + "$NOTIFICATION_IID"}, new String[]{(String)notificationStateEntity.getFieldValue(STATE_NOTIFICATION_DE_IDENTIFIER + "$NOTIFICATION_IID")})));
            notifications.add(new T8NotificationState(notificationStateEntity, dataSet).getNotification());
        }

        // Return the retrieved notifications.
        return notifications;
    }

    @SuppressWarnings("unchecked")
    public void logNotificationEvent(T8Context context, NotificationHistoryEvent event, T8Notification notification) throws Exception
    {
        Map<String, Object> notificationParameters;
        T8DataEntityDefinition entityDefinition;
        T8DataTransaction tx;
        T8DataEntity entity;
        String eventIID;

        // Get the transaction and entity definition.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        entityDefinition = tx.getDataEntityDefinition(HISTORY_NOTIFICATION_DE_IDENTIFIER);
        if (entityDefinition == null) throw new IllegalStateException("Notification History entity definition not found: " + HISTORY_NOTIFICATION_DE_IDENTIFIER);

        // Cast the notification parameters so it fits in with the rest of the system
        notificationParameters = (Map<String, Object>)notification.getParameters();

        // Create a new proces history entity.
        eventIID = T8IdentifierUtilities.createNewGUID();
        entity = entityDefinition.getNewDataEntityInstance();
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$EVENT_IID", eventIID);
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$NOTIFICATION_IID", notification.getInstanceIdentifier());
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$NOTIFICATION_ID", notification.getIdentifier());
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$USER_ID", notification.getUserIdentifier());
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$PRIORITY", 0);
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$SENDER_ID", notification.getSenderIdentifier());
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$SENDER_IID", notification.getSenderInstanceIdentifier());
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$SENDER_ORG_ID", notification.getSenderOrganizationIdentifier());
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$EVENT", event.toString());
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$TIME", new java.sql.Timestamp(System.currentTimeMillis()));
        entity.setFieldValue(HISTORY_NOTIFICATION_DE_IDENTIFIER + "$EVENT_PARAMETERS", createParameterString(notification.getIdentifier(), notificationParameters)); // We always strip the process namespace, all other parameter types will maintain their namespaces.

        // Insert the entity.
        tx.insert(entity);

        // Insert the Communication History parameters.
        T8HistoryParameterPersistenceHandler.insertParameters(tx, HISTORY_NOTIFICATION_PAR_DE_IDENTIFIER, eventIID, T8IdentifierUtilities.stripNamespace(notificationParameters));
    }

    /**
     * Retrieves all of the new notifications for the specified user identifier
     * and returns the count for the number of entities which were retrieved.
     *
     * @param context The {@code T8SessionContext} which defines the
     *      details for the current session
     * @param userIdentifier The {@code String} user identifier for which the
     *      notification count should be retrieved
     *
     * @return The {@code int} number of new notifications for the specified
     *      user
     *
     * @throws Exception If there is any error during the retrieval of the
     *      notifications
     */
    public int getNewNotificationCount(T8Context context, String userIdentifier) throws Exception
    {
        T8NotificationFilter filter;
        T8DataTransaction tx;

        filter = new T8NotificationFilter();
        filter.setUserIdentifier(userIdentifier);
        filter.setIncludeReceivedNotifications(false);

        //Get an instant transaction to retrieve the count with
        tx = this.serverContext.getDataManager().getCurrentSession().instantTransaction();

        return tx.count(STATE_NOTIFICATION_DE_IDENTIFIER, filter.getDataFilter(context, STATE_NOTIFICATION_DE_IDENTIFIER));
    }

    /**
     * Closes the notifications by deleting the state objects referring to them.
     * As a precaution, the user identifier is also used as part of the filter.
     * To avoid having to go through the entire process of creating a the entire
     * state mapping structure, the statements are directly run, and a rollback
     * method used in the event that an unauthorized user attempts to close a
     * specific notification.
     *
     * @param context The {@code T8SessionContext} identifying the
     *      current session
     * @param userId The {@code String} user identifier to be used as
     *      part of the fail-safe filter
     * @param notificationIid The {@code String} notification
     *      instance identifier of the notification instance to be closed
     *
     * @throws Exception If there is an error during the execution of the
     *      deletion process
     */
    public void closeNotification(T8Context context, String userId, String notificationIid) throws Exception
    {
        int notificationsRemoved;
        int propertiesRemoved;
        T8DataSession session;
        T8DataTransaction xtx;
        T8DataTransaction tx;

        // Get the transaction to use (suspend any external transaction).
        session = serverContext.getDataManager().getCurrentSession();
        xtx = session.suspend();
        tx = session.beginTransaction();

        try
        {
            //Delete the records from STATE_NOTIFICATION_PROP
            propertiesRemoved = tx.delete(STATE_NOTIFICATION_PAR_DE_IDENTIFIER, new T8DataFilter(STATE_NOTIFICATION_PAR_DE_IDENTIFIER, HashMaps.createTypeSafeMap(new String[]{STATE_NOTIFICATION_PAR_DE_IDENTIFIER + "$NOTIFICATION_IID"}, new Object[]{notificationIid})));

            //Delete the records from STATE_NOTIFICATION
            notificationsRemoved = tx.delete(STATE_NOTIFICATION_DE_IDENTIFIER, new T8DataFilter(STATE_NOTIFICATION_DE_IDENTIFIER, HashMaps.createTypeSafeMap(new String[]{STATE_NOTIFICATION_DE_IDENTIFIER + "$NOTIFICATION_IID", STATE_NOTIFICATION_DE_IDENTIFIER + "$USER_ID"}, new Object[]{notificationIid, userId})));

            //Since the full filter can only be applied to the STATE_NOTIFICATION table
            //we need to rollback the transaction if any properties were deleted which
            //should not have been.
            if (notificationsRemoved == 0 && propertiesRemoved != 0) tx.rollback();
            else tx.commit();
        }
        catch (Exception ex)
        {
            tx.rollback();
            throw ex;
        }
        finally
        {
            // If an external session was present, resume it.
            if (xtx != null) session.resume(xtx);
        }
    }

    public void closeAllNotifications(T8Context context, String userId) throws Exception
    {
        T8DataSession session;
        T8DataTransaction xtx;
        T8DataTransaction tx;

        // Get the transaction to use (suspend any external transaction).
        session = serverContext.getDataManager().getCurrentSession();
        xtx = session.suspend();
        tx = session.beginTransaction();

        try
        {
            T8DataFilter notificationFilter;
            T8DataFilter parameterFilter;

            // Create the notification filter.
            notificationFilter = new T8DataFilter(STATE_NOTIFICATION_DE_IDENTIFIER);
            notificationFilter.addFilterCriterion
            (
                DataFilterConjunction.AND,
                STATE_NOTIFICATION_DE_IDENTIFIER + "$USER_ID",
                DataFilterOperator.EQUAL,
                userId,
                false
            );

            // Create the parameter filter.
            parameterFilter = new T8DataFilter(STATE_NOTIFICATION_PAR_DE_IDENTIFIER);
            parameterFilter.addSubFilterCriterion
            (
                DataFilterConjunction.AND,
                STATE_NOTIFICATION_PAR_DE_IDENTIFIER + "$NOTIFICATION_IID",
                DataFilterOperator.IN,
                notificationFilter,
                HashMaps.newHashMap(STATE_NOTIFICATION_PAR_DE_IDENTIFIER + "$NOTIFICATION_IID", STATE_NOTIFICATION_DE_IDENTIFIER + "$NOTIFICATION_IID")
            );

            // Delete the notification parameters.
            tx.delete(STATE_NOTIFICATION_PAR_DE_IDENTIFIER, parameterFilter);

            // Delete the notifications.
            tx.delete(STATE_NOTIFICATION_DE_IDENTIFIER, notificationFilter);
            tx.commit();
        }
        catch (Exception ex)
        {
            tx.rollback();
            throw ex;
        }
        finally
        {
            // If an external session was present, resume it.
            if (xtx != null) session.resume(xtx);
        }
    }

    void updateNotificationStatus(T8Context context, String notificationInstanceIdentifier, T8Notification.NotificationStatus status) throws Exception
    {
        T8DataTransaction tx;
        int updateCount;

        //Get the transaction to use
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        updateCount = tx.update(STATE_NOTIFICATION_DE_IDENTIFIER, HashMaps.createSingular(STATE_NOTIFICATION_DE_IDENTIFIER+"$STATUS", status.toString()), new T8DataFilter(STATE_NOTIFICATION_DE_IDENTIFIER, HashMaps.createSingular(STATE_NOTIFICATION_DE_IDENTIFIER + "$NOTIFICATION_IID", notificationInstanceIdentifier)));
        if (updateCount == 0) LOGGER.log(T8Logger.Level.ERROR, "Failed to update the status for notification IID : " + notificationInstanceIdentifier + " - to : " + status);
    }

    public void saveNotificationState(T8Notification notification) throws Exception
    {
        T8NotificationState notificationState;
        T8DataSession session;
        T8DataTransaction xtx;
        T8DataTransaction tx;

        // Get the transaction to use (suspend any external transaction).
        session = serverContext.getDataManager().getCurrentSession();
        xtx = session.suspend();
        tx = session.beginTransaction();

        // Create the notification state.
        notificationState = new T8NotificationState(notification);

        // Persist the notification state.
        try
        {
            T8StateUpdates stateUpdates;

            // Create an object to store all updates.
            stateUpdates = new T8StateUpdates(ArrayLists.typeSafeList
            (T8NotificationManagerResource.STATE_NOTIFICATION_DE_IDENTIFIER,
                T8NotificationManagerResource.STATE_NOTIFICATION_PAR_DE_IDENTIFIER
            ));

            // Get the notification updates.
            notificationState.addUpdates(tx, stateUpdates);

            // Persist inserts.
            for (T8DataEntity entity : stateUpdates.getInserts())
            {
                tx.insert(entity);
            }

            // Persist updates.
            for (T8DataEntity entity : stateUpdates.getUpdates())
            {
                tx.update(entity);
            }

            // Persist deletions.
            for (T8DataEntity entity : stateUpdates.getDeletions())
            {
                tx.delete(entity);
            }

            // Commit the transaction.
            tx.commit();
        }
        catch (Exception e)
        {
            tx.rollback();
            throw new RuntimeException("Exception while saving notification state: " + notificationState, e);
        }
        finally
        {
            // If an external session was present, resume it.
            if (xtx != null) session.resume(xtx);
        }
    }

    private String createParameterString(String namespace, Map<String, Object> parameters)
    {
        if (parameters == null || parameters.isEmpty())
        {
            return null;
        }
        else
        {
            Map<String, Object> namespaceStrippedParameters;
            StringBuilder parameterString;

            // Strip the flow namespace from the parameters.
            namespaceStrippedParameters = T8IdentifierUtilities.stripNamespace(namespace, parameters, false);

            // Add all of the parameters to the string.
            parameterString = new StringBuilder();
            namespaceStrippedParameters.keySet().stream().map((parameterKey) ->
            {
                Object parameterValue;
                // Append the prefix and parameter key.
                parameterString.append(" ["); // Yes, there is a space before the bracket.
                parameterString.append(parameterKey);
                parameterString.append(":");
                // Append the parameter value if any.
                parameterValue = namespaceStrippedParameters.get(parameterKey);
                return parameterValue;
            }).map((parameterValue) ->
            {
                if (parameterValue != null) parameterString.append(parameterValue.toString());
                return parameterValue;
            }).forEach((_item) ->
            {
                // Append the suffix.
                parameterString.append("]");
            });

            return parameterString.toString();
        }
    }
}
