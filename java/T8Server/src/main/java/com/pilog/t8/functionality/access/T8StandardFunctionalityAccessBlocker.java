package com.pilog.t8.functionality.access;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.translation.T8TranslatableString;
import com.pilog.t8.definition.functionality.access.T8StandardFunctionalityAccessBlockerDefinition;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8StandardFunctionalityAccessBlocker implements T8FunctionalityAccessBlocker
{
    private final String blockingFunctionalityId;
    private final List<String> blockedFunctionalityIds;
    private final boolean blockOnDataObject;
    private final boolean blockOnUser;
    private final boolean blockOnUserProfile;
    private final String blockMessage;

    public T8StandardFunctionalityAccessBlocker(T8Context context, T8StandardFunctionalityAccessBlockerDefinition definition)
    {
        this.blockingFunctionalityId = definition.getBlockingFunctionalityIdentifier();
        this.blockedFunctionalityIds = definition.getBlockedFunctionalityIdentifiers();
        this.blockOnDataObject = definition.isBlockOnDataObject();
        this.blockOnUser = definition.isBlockOnUser();
        this.blockOnUserProfile = definition.isBlockOnUserProfile();
        this.blockMessage = definition.getBlockMessage();
    }

    @Override
    public List<T8FunctionalityBlock> getFunctionalityBlocks(T8Context context, String functionalityId, List<T8DataObject> dataObjects)
    {
        ArrayList<T8FunctionalityBlock> blocks;
        T8SessionContext sessionContext;

        sessionContext = context.getSessionContext();
        blocks = new ArrayList<T8FunctionalityBlock>();
        if (functionalityId.equals(blockingFunctionalityId))
        {
            if (blockedFunctionalityIds != null)
            {
                for (String blockedFunctionalityId : blockedFunctionalityIds)
                {
                    String rootOrgId;
                    String userId;
                    String profileId;

                    // Get the user blocking identifiers.
                    rootOrgId = sessionContext.getRootOrganizationIdentifier();
                    userId = blockOnUser ? sessionContext.getUserIdentifier() : null;
                    profileId = blockOnUserProfile ? sessionContext.getUserProfileIdentifier() : null;

                    // Get the object blocking identifiers.
                    if ((blockOnDataObject) && (dataObjects != null))
                    {
                        for (T8DataObject dataObject : dataObjects)
                        {
                            String dataObjectId;
                            String dataObjectIid;

                            dataObjectId = dataObject.getId();
                            dataObjectIid = dataObject.getKey();

                            // Create the new block and add it to the result list.
                            blocks.add(new T8FunctionalityBlock(rootOrgId, null, userId, profileId, blockedFunctionalityId, dataObjectId, dataObjectIid, new T8TranslatableString(blockMessage)));
                        }
                    }
                    else
                    {
                        // Create the new block and add it to the result list.
                        blocks.add(new T8FunctionalityBlock(rootOrgId, null, userId, profileId, blockedFunctionalityId, null, null, new T8TranslatableString(blockMessage)));
                    }
                }
            }
        }

        return blocks;
    }
}
