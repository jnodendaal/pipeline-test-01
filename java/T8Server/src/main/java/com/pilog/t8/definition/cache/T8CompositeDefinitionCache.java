package com.pilog.t8.definition.cache;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionMetaData;
import com.pilog.t8.definition.T8DefinitionTypeMetaData;
import com.pilog.t8.definition.T8L2DefinitionCache;
import com.pilog.t8.security.T8Context;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public abstract class T8CompositeDefinitionCache implements T8L2DefinitionCache
{
    protected T8Context context;
    protected T8ServerContext serverContext;
    protected T8SessionContext sessionContext;
    protected List<T8L2DefinitionCache> definitionCaches;
    protected T8DefinitionManager definitionManager;

    public T8CompositeDefinitionCache(T8Context context, T8DefinitionManager definitionManager)
    {
        this.context = context;
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.definitionManager = definitionManager;
        this.definitionCaches = new ArrayList<T8L2DefinitionCache>();
    }

    @Override
    public void init() throws Exception
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            definitionCache.init();
        }
    }

    @Override
    public void destroy()
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            definitionCache.destroy();
        }
    }

    @Override
    public void setContextPath(File contextPath) throws Exception
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            definitionCache.setContextPath(contextPath);
        }
    }

    public void addDefinitionCache(T8L2DefinitionCache definitionCache)
    {
        this.definitionCaches.add(definitionCache);
    }

    public void addDefinitionCaches(List<T8L2DefinitionCache> definitionCaches)
    {
        this.definitionCaches.addAll(definitionCaches);
    }

    @Override
    public void clearCache()
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            definitionCache.clearCache();
        }
    }

    @Override
    public void cacheDefinitions() throws Exception
    {
        // Instruct all definition caches to cache their respective definitions.
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            try
            {
                // Reload the definition cache.
                definitionCache.clearCache();
                definitionCache.cacheDefinitions();
            }
            catch (Exception e)
            {
                T8Log.log("Exception in definition cache: " + definitionCache, e);
                throw e;
            }
        }
    }

    @Override
    public boolean containsDefinition(String identifier) throws Exception
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (definitionCache.containsDefinition(identifier)) return true;
        }

        return false;
    }

    @Override
    public boolean containsDefinitionGroup(String groupIdentifier) throws Exception
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (definitionCache.containsDefinitionGroup(groupIdentifier)) return true;
        }

        return false;
    }

    @Override
    public boolean loadsType(T8DefinitionTypeMetaData typeMetaData)
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (definitionCache.loadsType(typeMetaData)) return true;
        }

        return false;
    }

    @Override
    public boolean savesType(T8DefinitionTypeMetaData typeMetaData)
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (definitionCache.savesType(typeMetaData)) return true;
        }

        return false;
    }

    @Override
    public T8Definition loadDefinition(String identifier) throws Exception
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (definitionCache.containsDefinition(identifier))
            {
                return definitionCache.loadDefinition(identifier);
            }
        }

        return null;
    }

    @Override
    public void saveDefinition(T8Definition definition) throws Exception
    {
        T8DefinitionTypeMetaData typeMetaData;

        // Get the type meta data of the definition.
        typeMetaData = definition.getTypeMetaData();

        // Now find the definition cache that is responsible for storing the definition and save it.
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (definitionCache.savesType(typeMetaData))
            {
                definitionCache.saveDefinition(definition);
                return;
            }
        }

        throw new Exception("No applicable definition cache found for storing type: " + typeMetaData.getTypeId());
    }

    @Override
    public void deleteDefinition(String identifier) throws Exception
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (definitionCache.containsDefinition(identifier))
            {
                definitionCache.deleteDefinition(identifier);
                return;
            }
        }

        throw new Exception("No applicable definition cache found for deleting definition: " + identifier);
    }

    @Override
    public long getCachedCharacterSize()
    {
        long size;

        size = 0;
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            size += definitionCache.getCachedCharacterSize();
        }

        return size;
    }

    @Override
    public int getCachedDefinitionCount()
    {
        int count;

        count = 0;
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            count += definitionCache.getCachedDefinitionCount();
        }

        return count;
    }

    @Override
    public Set<String> getDefinitionIdentifiers() throws Exception
    {
        Set<String> identifiers;

        identifiers = new HashSet<String>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            identifiers.addAll(definitionCache.getDefinitionIdentifiers());
        }

        return identifiers;
    }

    @Override
    public Set<String> getDefinitionIdentifiers(String typeIdentifier) throws Exception
    {
        Set<String> identifiers;

        identifiers = new HashSet<String>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (typeIdentifier == null)
            {
                identifiers.addAll(definitionCache.getDefinitionIdentifiers());
            }
            else
            {
                identifiers.addAll(definitionCache.getDefinitionIdentifiers(typeIdentifier));
            }
        }

        return identifiers;
    }

    @Override
    public Set<String> getGroupDefinitionIdentifiers(String groupIdentifier) throws Exception
    {
        Set<String> identifiers;

        identifiers = new HashSet<String>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (groupIdentifier == null)
            {
                identifiers.addAll(definitionCache.getDefinitionIdentifiers());
            }
            else
            {
                identifiers.addAll(definitionCache.getGroupDefinitionIdentifiers(groupIdentifier));
            }
        }

        return identifiers;
    }

    @Override
    public Set<String> getGroupIdentifiers() throws Exception
    {
        Set<String> identifiers;

        identifiers = new HashSet<String>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            identifiers.addAll(definitionCache.getGroupIdentifiers());
        }

        return identifiers;
    }

    @Override
    public T8DefinitionMetaData getDefinitionMetaData(String identifier) throws Exception
    {
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (definitionCache.containsDefinition(identifier))
            {
                return definitionCache.getDefinitionMetaData(identifier);
            }
        }

        return null;
    }

    @Override
    public List<T8DefinitionMetaData> getAllDefinitionMetaData() throws Exception
    {
        List<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<T8DefinitionMetaData>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            metaDataList.addAll(definitionCache.getAllDefinitionMetaData());
        }

        return metaDataList;
    }

    @Override
    public List<T8DefinitionMetaData> getTypeDefinitionMetaData(String typeIdentifier) throws Exception
    {
        List<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<T8DefinitionMetaData>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            metaDataList.addAll(definitionCache.getTypeDefinitionMetaData(typeIdentifier));
        }

        return metaDataList;
    }

    @Override
    public List<T8DefinitionMetaData> getGroupDefinitionMetaData(String groupIdentifier) throws Exception
    {
        List<T8DefinitionMetaData> metaDataList;

        metaDataList = new ArrayList<T8DefinitionMetaData>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            if (definitionCache.containsDefinitionGroup(groupIdentifier))
            {
                metaDataList.addAll(definitionCache.getGroupDefinitionMetaData(groupIdentifier));
            }
        }

        return metaDataList;
    }

    @Override
    public List<String> findDefinitionsByText(String searchText)
    {
        List<String> definitionIdentifiers;

        definitionIdentifiers = new ArrayList<String>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            List<String> foundIdentifiers;

            foundIdentifiers = definitionCache.findDefinitionsByText(searchText);
            if (foundIdentifiers != null) definitionIdentifiers.addAll(foundIdentifiers);
        }

        return definitionIdentifiers;
    }

    @Override
    public List<String> findDefinitionsByRegularExpression(String expression) throws Exception
    {
        List<String> definitionIdentifiers;

        definitionIdentifiers = new ArrayList<String>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            List<String> foundIdentifiers;

            foundIdentifiers = definitionCache.findDefinitionsByRegularExpression(expression);
            if (foundIdentifiers != null) definitionIdentifiers.addAll(foundIdentifiers);
        }

        return definitionIdentifiers;
    }

    @Override
    public List<String> findDefinitionsByIdentifier(String identifier) throws Exception
    {
        List<String> definitionIdentifiers;

        definitionIdentifiers = new ArrayList<String>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            List<String> foundIdentifiers;

            foundIdentifiers = definitionCache.findDefinitionsByIdentifier(identifier);
            if (foundIdentifiers != null) definitionIdentifiers.addAll(foundIdentifiers);
        }

        return definitionIdentifiers;
    }

    @Override
    public List<String> findDefinitionsByDatum(String datumIdentifier, T8DataType dataType, String content) throws Exception
    {
        List<String> definitionIdentifiers;

        definitionIdentifiers = new ArrayList<String>();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            List<String> foundIdentifiers;

            foundIdentifiers = definitionCache.findDefinitionsByDatum(datumIdentifier, dataType, content);
            if (foundIdentifiers != null) definitionIdentifiers.addAll(foundIdentifiers);
        }

        return definitionIdentifiers;
    }
}
