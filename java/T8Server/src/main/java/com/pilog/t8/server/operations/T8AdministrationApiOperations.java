package com.pilog.t8.server.operations;

import static com.pilog.t8.definition.system.T8AdministrationResource.*;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8DefinitionHandle;
import com.pilog.t8.definition.validation.T8DefinitionValidationError;
import com.pilog.t8.operation.progressreport.T8DefaultCategorizedProgressReport;
import com.pilog.t8.operation.progressreport.T8DefaultProgressReport;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8DynamicJavaOperationDefinition;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.definition.system.T8AdministrationResource.ValidationType;
import com.pilog.t8.operation.java.T8DynamicJavaOperationCompiler;
import com.pilog.t8.system.T8OperationCompilationResult;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8AdministrationApiOperations
{
    public static class ApiSystemValidationOperation extends T8DefaultServerOperation
    {
        private volatile T8DefaultCategorizedProgressReport progressReport;

        public ApiSystemValidationOperation(T8ServerContext serverContext, T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8DefinitionValidationError> allValidationErrors;
            T8DefinitionManager definitionManager;
            List<String> groupIds;
            StringBuilder report;
            ValidationType validationType;
            double groupsCompleted;
            double groupsCount;
            double definitionCount;
            double definitionsCompleted;

            validationType = (ValidationType) operationParameters.get(PARAMETER_VALIDATION_TYPE);
            definitionManager = serverContext.getDefinitionManager();
            groupIds = definitionManager.getDefinitionGroupIdentifiers();

            report = new StringBuilder();
            report.append("<html>");
            report.append("<h1>System Validation Report</h1>");
            report.append("</br>");

            report.append("<h2>Information</h2>");
            report.append("<table border=\"0\">");
            report.append("<tr><td><b>Definition Count: </b></td><td>").append(definitionManager.getCachedDefinitionCount(context)).append("</td></tr>");
            report.append("<tr><td><b>Definition Group Count: </b></td><td>").append(groupIds.size()).append("</td></tr>");
            report.append("<tr><td><b>Cached Characters: </b></td><td>" + "" + "</td></tr>");
            report.append("<tr><td><b>Cached Bytes: </b></td><td>").append(definitionManager.getCachedDataSize(context)).append("</td></tr>");
            report.append("</table>");
            report.append("</br>");
            report.append("<h2>Validation Errors</h2>");

            allValidationErrors = new ArrayList<>();
            groupsCount = groupIds.size();
            groupsCompleted = 0;
            for (String groupId : groupIds)
            {
                List<T8Definition> groupDefinitions;

                groupsCompleted++;
                groupDefinitions = definitionManager.getRawGroupDefinitions(context, null, groupId);
                definitionCount = groupDefinitions.size();
                definitionsCompleted = 0;
                for (T8Definition globalDefinition : groupDefinitions)
                {
                    List<T8DefinitionValidationError> validationErrors;

                    if(validationType == null || validationType == ValidationType.DEFINITION_VALIDATION)
                    {
                        validationErrors = globalDefinition.validateDefinition(serverContext, null, T8Definition.DefinitionValidationType.GLOBAL);
                    }
                    else
                    {
                        validationErrors = new ArrayList<>();

                        if(globalDefinition.getTypeMetaData().getDefinitionLevel() == T8Definition.T8DefinitionLevel.PROJECT)
                        {
                            List<T8DefinitionHandle> usages;

                            usages = definitionManager.findDefinitionsByIdentifier(context, globalDefinition.getIdentifier(), true);
                            if (usages.isEmpty())
                            {
                                validationErrors.add(new T8DefinitionValidationError(globalDefinition.getProjectIdentifier(), globalDefinition.getIdentifier(), "", "Definition has no usages", T8DefinitionValidationError.ErrorType.WARNING));
                            }
                        }
                    }
                    if ((validationErrors != null) && (validationErrors.size() > 0))
                    {
                        // If this is the first time errors are found, add the table header.
                        if (allValidationErrors.isEmpty())
                        {
                            report.append("<table border=\"1\">");
                            report.append("<tr><td><b>Error Type</b></td><td><b>Project</b></td><td><b>Definition</b></td><td><b>Datum</b></td><td><b>Message</b></td></tr>");
                        }

                        // Add the validation errors to the global list and then to the report.
                        allValidationErrors.addAll(validationErrors);
                        for (T8DefinitionValidationError error : validationErrors)
                        {
                            report.append("<tr><td>" + error.getErrorType() + "</td><td>" + error.getProjectIdentifier() + "</td><td>" + error.getDefinitionIdentifier() + "</td><td>" + error.getDatumIdentifier() + "</td><td>" + error.getMessage() + "</td></tr>");
                        }
                    }

                    definitionsCompleted++;
                    progressReport = new T8DefaultCategorizedProgressReport("Processing Group " + groupId, (groupsCompleted / groupsCount) * 100.00, "Processing Definition " + globalDefinition.getIdentifier(), (definitionsCompleted / definitionCount) * 100.00);

                    if (stopFlag) break;
                }

                if (stopFlag) break;
            }

            // Close the table tag if errors were found, else just log a message to indicate that everything is ok.
            if (allValidationErrors.size() > 0)
            {
                report.append("</table>");
            }
            else
            {
                report.append("No validation errors found.");
            }

            // Close the html tag.
            report.append("</html>");

            return HashMaps.createSingular(PARAMETER_DEFINITION_VALIDATION_REPORT, report.toString());
        }

        @Override
        public double getProgress(T8Context tsc)
        {
            return progressReport == null ? 0 : progressReport.getProgress();
        }

        @Override
        public T8ProgressReport getProgressReport(T8Context context)
        {
            return progressReport;
        }
    }

    public static class ApiDefinitionValidationOperation extends T8DefaultServerOperation
    {
        private volatile T8DefaultProgressReport progressReport;

        public ApiDefinitionValidationOperation(T8ServerContext serverContext, T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            List<T8DefinitionValidationError> validationErrors;
            T8DefinitionManager definitionManager;
            List<String> definitionIds;
            T8Definition rawDefinition;
            boolean noValidationErrors;
            StringBuilder reportBuilder;
            double definitionCount;
            double definitionsCompleted;

            definitionIds = (List<String>) operationParameters.get(PARAMETER_DEFINITION_IDS);
            definitionManager = serverContext.getDefinitionManager();
            noValidationErrors = true;
            definitionCount = definitionIds.size();

            reportBuilder = new StringBuilder();
            reportBuilder.append("<html>");
            reportBuilder.append("<h1>Definition Validation Report</h1>");
            reportBuilder.append("</br>");
            reportBuilder.append("<h2>Information</h2>");
            reportBuilder.append("<table border=\"0\">");
            reportBuilder.append("<tr><td><b>Definition Count: </b></td><td>").append(definitionIds.size()).append("</td></tr>");
            reportBuilder.append("</table>");
            reportBuilder.append("</br>");
            reportBuilder.append("<h2>Validation Errors</h2>");

            // Add the report table
            reportBuilder.append("<table border=\"1\">");
            reportBuilder.append("<tr><td><b>Error Type</b></td><td><b>Project</b></td><td><b>Definition</b></td><td><b>Datum</b></td><td><b>Message</b></td></tr>");

            // Validate each of the specified definitions.
            definitionsCompleted = 0;
            for (String definitionId : definitionIds)
            {
                rawDefinition = definitionManager.getRawDefinition(context, null, definitionId);
                validationErrors = rawDefinition.validateDefinition(context.getServerContext(), context.getSessionContext(), T8Definition.DefinitionValidationType.GLOBAL);
                if (validationErrors != null)
                {
                    for (T8DefinitionValidationError validationError : validationErrors)
                    {
                        noValidationErrors = false;
                        reportBuilder.append("<tr><td>").append(validationError.getErrorType()).append("</td>");
                        reportBuilder.append("<td>").append(validationError.getProjectIdentifier()).append("</td>");
                        reportBuilder.append("<td>").append(validationError.getDefinitionIdentifier()).append("</td>");
                        reportBuilder.append("<td>").append(validationError.getDatumIdentifier()).append("</td>");
                        reportBuilder.append("<td>").append(validationError.getMessage()).append("</td></tr>");
                    }
                }

                // Set the progress of the operation.
                definitionsCompleted++;
                progressReport = new T8DefaultProgressReport("Validating Definition: " + definitionId, (definitionsCompleted / definitionCount) * 100.00);
                if (stopFlag) break;
            }

            reportBuilder.append("</table>");

            // Close the html tag.
            reportBuilder.append("</html>");

            // Return the results of the validation.
            if (noValidationErrors) return HashMaps.createSingular(PARAMETER_DEFINITION_VALIDATION_REPORT, null);
            else return HashMaps.createSingular(PARAMETER_DEFINITION_VALIDATION_REPORT, reportBuilder.toString());
        }

        @Override
        public double getProgress(T8Context tsc)
        {
            return progressReport == null ? 0 : progressReport.getProgress();
        }

        @Override
        public T8ProgressReport getProgressReport(T8Context context)
        {
            return progressReport;
        }
    }

    public static class ApiCompileOperation extends T8DefaultServerOperation
    {
        public ApiCompileOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, String> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8DynamicJavaOperationDefinition operationDefinition;
            T8DefinitionManager definitionManager;
            String projectId;
            String operationId;

            projectId = (String)operationParameters.get(PARAMETER_PROJECT_ID);
            operationId = (String)operationParameters.get(PARAMETER_DEFINITION_ID);

            definitionManager = serverContext.getDefinitionManager();
            operationDefinition = (T8DynamicJavaOperationDefinition)definitionManager.getRawDefinition(context, projectId, operationId);
            if (operationDefinition != null)
            {
                T8DynamicJavaOperationCompiler compiler;
                T8OperationCompilationResult result;

                // Compile the operation.
                compiler = new T8DynamicJavaOperationCompiler(context);
                result = compiler.compileOperation(operationDefinition);
                if (result.isSuccess())
                {
                    definitionManager.saveDefinition(context, operationDefinition, null);
                }
                else
                {
                    operationDefinition.setByteCode(null);
                    definitionManager.saveDefinition(context, operationDefinition, null);
                }

                // Return the results of the compilation.
                return HashMaps.newHashMap(PARAMETER_SUCCESS, result.isSuccess(), PARAMETER_COMPILATION_RESULT, result);
            }
            else throw new Exception("Definition not found: " + operationId);
        }
    }
}