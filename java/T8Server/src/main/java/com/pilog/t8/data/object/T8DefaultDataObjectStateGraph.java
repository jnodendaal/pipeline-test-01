package com.pilog.t8.data.object;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateDefinition;
import com.pilog.t8.definition.data.object.state.T8DataObjectStateGraphDefinition;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Bouwer
 */
public class T8DefaultDataObjectStateGraph implements T8DataObjectStateGraph
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8DataObjectStateGraphDefinition definition;
    private final Map<String, T8DataObjectState> states;

    public T8DefaultDataObjectStateGraph(T8ServerContext serverContext, T8SessionContext sessionContext, T8DataObjectStateGraphDefinition definition)
    {
        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
        this.states = new HashMap<String, T8DataObjectState>();
        constructStates();
    }

    private void constructStates()
    {
        try
        {
            for (T8DataObjectStateDefinition stateDefinition : definition.getStateDefinitions())
            {
                T8DataObjectState state;

                state = stateDefinition.getNewStateInstance(serverContext, sessionContext, definition.getIdentifier());
                states.put(state.getId(), state);
            }
        }
        catch (Exception e)
        {
            T8Log.log("Exception while constructing states in state graph: " + definition, e);
        }
    }

    @Override
    public String getId()
    {
        return definition.getIdentifier();
    }

    @Override
    public List<String> getDataObjectIds()
    {
        return definition.getDataObjectIds();
    }

    @Override
    public T8DataObjectState getState(String stateId)
    {
        return states.get(stateId);
    }

    @Override
    public T8DataObjectState getStateCreatedBy(String functionalityId)
    {
        for (T8DataObjectState state : states.values())
        {
            if (state.isCreatedBy(functionalityId))
            {
                return state;
            }
        }

        return null;
    }

    @Override
    public T8DataObjectState getNextState(String functionalityId, String fromStateId, String toStateId)
    {
        // TODO:  Actually check that the from- and to-states are connected with the specified functionality.
        return states.get(toStateId);
    }

    @Override
    public T8DataObjectState getStateByConceptId(String conceptId)
    {
        for (T8DataObjectState state : states.values())
        {
            if (Objects.equals(state.getConceptId(), conceptId))
            {
                return state;
            }
        }

        return null;
    }

    @Override
    public String toString()
    {
        return "T8DefaultDataObjectStateGraph{" + "definition=" + definition + '}';
    }
}
