package com.pilog.t8.connection;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.remote.server.connection.T8Connection;
import com.pilog.t8.definition.remote.server.connection.T8SoapConnectionDefinition;
import com.pilog.t8.security.T8AuthenticationUtilities;
import com.pilog.t8.utilities.strings.Strings;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

/**
 * This class implements a point-to-point connection that a client can use for sending messages
 * directly to a remote party represented by a URL.
 *
 * A <code>T8SoapConnection</code> object follows the request/response paradigm.  That is, messages
 * are sent using the method <code>call</code>, which sends the message and then waits until it gets a reply.
 *
 * @author Bouwer du Preez
 */
public class T8SoapConnection implements T8Connection
{
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8SoapConnectionDefinition definition;
    private final SOAPConnection connection;
    private final String url;
    private final String username;
    private final String password;

    public T8SoapConnection(T8ServerContext serverContext, T8SessionContext sessionContext, T8SoapConnectionDefinition definition) throws Exception
    {
        SOAPConnectionFactory connectionFactory;

        this.serverContext = serverContext;
        this.sessionContext = sessionContext;
        this.definition = definition;
        this.url = definition.getURL();
        this.username = definition.getAuthenticationUsername();
        this.password = definition.getAuthenticationPassword();

        // Get the connection and message factories and create a new connection instance.
        connectionFactory = SOAPConnectionFactory.newInstance();
        this.connection = connectionFactory.createConnection();
    }

    /**
     * Sends the given message to the specified endpoint and blocks until
     * it has returned the response.
     *
     * @param request the <code>SOAPMessage</code> object to be sent
     * @return the <code>SOAPMessage</code> object that is the response to the
     *         message that was sent
     * @throws SOAPException if there is a SOAP error
     */
    public SOAPMessage call(SOAPMessage request) throws SOAPException
    {
        // If no authentication details are specified, send the request.
        if (Strings.isNullOrEmpty(username))
        {
            return connection.call(request, url);
        }
        else
        {
            // Authentication details are specified, so add them to the request.
            T8AuthenticationUtilities.addBasicAuthentication(request, username, password);
            return connection.call(request, url);
        }
    }

    /**
     * Gets a message from a specific endpoint and blocks until it receives,
     *
     * @return the <code>SOAPMessage</code> object that is the response to the
     *                  get message request
     * @throws SOAPException if there is a SOAP error
     */
    public SOAPMessage get() throws SOAPException
    {
        return connection.get(url);
    }
}
