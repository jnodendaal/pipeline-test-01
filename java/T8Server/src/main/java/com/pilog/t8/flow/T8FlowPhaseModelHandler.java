package com.pilog.t8.flow;

import com.pilog.t8.flow.phase.T8FlowPhase;
import com.pilog.t8.flow.phase.T8FlowPhaseNodeDetails;
import com.pilog.t8.flow.phase.T8FlowPhaseProfileDetails;
import com.pilog.t8.flow.phase.T8FlowPhaseUserDetails;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.definition.flow.T8WorkFlowDefinition;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.definition.flow.phase.T8FlowPhaseDefinition;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8FlowPhaseModelHandler
{
    private final T8Context context;
    private final T8WorkFlowDefinition definition;
    private final T8FlowState flowState;
    private T8FlowPhase phaseModel;

    public T8FlowPhaseModelHandler(T8Context context, T8WorkFlowDefinition definition, T8FlowState flowState)
    {
        this.context = context;
        this.definition = definition;
        this.flowState = flowState;
        this.phaseModel = createPhaseModel();
    }

    public T8FlowPhase getPhaseModel()
    {
        if (phaseModel != null)
        {
            LinkedList<T8FlowPhase> phaseQueue;

            // Get all phases.
            phaseQueue = new LinkedList<T8FlowPhase>();
            phaseQueue.add(phaseModel);
            while (phaseQueue.size() > 0)
            {
                T8FlowPhase phase;
                boolean activeFound;
                boolean completedFound;

                // Get the next phase from the queue.
                phase = phaseQueue.removeFirst();

                // Loop through all nodes for this state and try to find one what is active.
                activeFound = false;
                completedFound = false;
                for (T8FlowPhaseNodeDetails nodeDetails : phase.getNodeDetails())
                {
                    // Get the node states for this identifier type.
                    for (T8FlowNodeState nodeState : flowState.getNodeStatesByType(nodeDetails.getNodeId()))
                    {
                        T8FlowNode.FlowNodeStatus nodeStatus;
                        T8FlowTaskState taskState;

                        // Add task state details if present.
                        taskState = nodeState.getTaskState();
                        if (taskState != null)
                        {
                            String userId;
                            String profileId;

                            userId = taskState.getRestrictionUserId();
                            profileId = taskState.getRestrictionProfileId();

                            if (userId != null)
                            {
                                phase.addUserDetails(new T8FlowPhaseUserDetails(userId, null));
                            }
                            else if (profileId != null)
                            {
                                phase.addProfileDetails(new T8FlowPhaseProfileDetails(profileId, null));
                            }
                        }

                        // Get the status from the node state.
                        nodeStatus = nodeState.getNodeStatus();
                        if (nodeStatus == T8FlowNode.FlowNodeStatus.ACTIVE)
                        {
                            phase.setSubsequentStatus(T8FlowPhase.T8FlowPhaseStatus.NOT_STARTED);
                            phase.setStatus(T8FlowPhase.T8FlowPhaseStatus.IN_PROGRESS);
                            activeFound = true;
                            break;
                        }
                        else if (nodeStatus == T8FlowNode.FlowNodeStatus.COMPLETED)
                        {
                            completedFound = true;
                        }
                    }

                    // If the phase has been set as active, then we can break the loop.
                    if (activeFound) break;
                }

                // If an active node state for this phase was found, we don't need to do anything else.
                if ((!activeFound) && (completedFound))
                {
                    phase.setStatus(T8FlowPhase.T8FlowPhaseStatus.COMPLETED);
                    phaseQueue.addAll(phase.getNextPhases());
                }
            }

            // Return the phase model.
            return phaseModel;
        }
        else return null;
    }

    private T8FlowPhase createPhaseModel()
    {
        Map<String, T8FlowPhase> phases;
        LinkedList<String> nodeIdQueue;
        Set<String> completedNodeIdSet;
        T8FlowPhase startPhase;

        // Create a map of phases used by the flow.
        try
        {
            phases = new HashMap<String, T8FlowPhase>();
            for (T8FlowPhaseDefinition phaseDefinition : definition.getPhaseDefinitions())
            {
                phases.put(phaseDefinition.getIdentifier(), phaseDefinition.createNewPhaseInstance(context));
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while creating flow phases from definition.", e);
        }

        // Iterate over the flow nodes from start to end and for each node add
        // its phase as a subsequent phase of the previous node's phase (if it
        // has not already been added).
        startPhase = null;
        nodeIdQueue = new LinkedList<String>();
        completedNodeIdSet = new HashSet<String>();
        nodeIdQueue.add(definition.getStartNodeIdentifier());
        while (!nodeIdQueue.isEmpty())
        {
            T8WorkFlowNodeDefinition nodeDefinition;
            List<String> outputNodeIdList;
            T8FlowPhase nodePhase;
            String nodeId;

            // Get the phase details.
            nodeId = nodeIdQueue.removeFirst();
            nodeDefinition = definition.getNodeDefinition(nodeId);
            outputNodeIdList = definition.getChildNodeIdentifiers(nodeId);
            nodePhase = phases.get(nodeDefinition.getPhaseIdentifier());
            if (nodePhase != null) nodePhase.addNodeDetails(new T8FlowPhaseNodeDetails(nodeId, nodeDefinition.getNodeType(), nodeDefinition.getDisplayName()));
            if (startPhase == null) startPhase = nodePhase; // Set the start phase if has not already been set.

            // Loop through the output nodes of this node and add the output node phases.
            for (String outputNodeID : outputNodeIdList)
            {
                // If this output node has not been processed yet, do it now.
                if (!completedNodeIdSet.contains(outputNodeID))
                {
                    T8WorkFlowNodeDefinition outputNodeDefinition;
                    T8FlowPhase outputPhase;
                    String outputPhaseIdentifier;

                    // Get the output phase details.
                    outputNodeDefinition = definition.getNodeDefinition(outputNodeID);
                    outputPhaseIdentifier = outputNodeDefinition.getPhaseIdentifier();
                    outputPhase = phases.get(outputPhaseIdentifier);

                    // Link the output phase as a 'next-phase' of the node phase.
                    if (outputPhase != null)
                    {
                        // We can only add this output phase to a previous phase, if there was one.
                        if (nodePhase != null)
                        {
                            // Only link the output phase if it has not already been linked to the model.
                            if (nodePhase.canAddNextPhase(outputPhase))
                            {
                                nodePhase.addNextPhase(outputPhase);
                            }
                        }
                    }

                    // Add the output node ID to the queue so that it can be processed.
                    nodeIdQueue.add(outputNodeID);
                }
            }

            // Add the node ID to the set of compelted ID's to prevent it from being processed again.
            completedNodeIdSet.add(nodeId);
        }

        // Return the starting phase.
        return startPhase;
    }
}
