package com.pilog.t8.flow;

import com.pilog.json.JsonObject;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataIterator;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow.FlowHistoryEvent;
import com.pilog.t8.flow.event.T8FlowEvent;
import com.pilog.t8.flow.state.T8FlowNodeStateWaitKey;
import com.pilog.t8.flow.task.T8FlowTask.TaskEvent;
import com.pilog.t8.data.T8HistoryParameterPersistenceHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.state.data.T8FlowStateDataSet;
import com.pilog.t8.flow.state.T8FlowStateUpdates;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8DefaultFlowStateHandler implements T8FlowStateHandler
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8DefaultFlowStateHandler.class);

    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8DefinitionManager definitionManager;
    private T8DataEntityDefinition flowStateWaitKeyEntityDefinition;
    private T8DataEntityDefinition flowStateEntityDefinition;
    private T8DataEntityDefinition flowStateInputParameterEntityDefinition;
    private T8DataEntityDefinition nodeStateEntityDefinition;
    private T8DataEntityDefinition nodeStateInputParameterEntityDefinition;
    private T8DataEntityDefinition nodeStateOutputParameterEntityDefinition;
    private T8DataEntityDefinition nodeStateExecParameterEntityDefinition;
    private T8DataEntityDefinition nodeStateOutputNodeEntityDefinition;
    private T8DataEntityDefinition nodeStateDataObjectEntityDefinition;
    private T8DataEntityDefinition nodeStateWaitKeyEntityDefinition;
    private T8DataEntityDefinition taskStateEntityDefinition;
    private T8DataEntityDefinition taskStateInputParameterEntityDefinition;
    private T8DataEntityDefinition taskStateOutputParameterEntityDefinition;
    private boolean persistenceEnabled; // Flag that can be set to false to disable persistence of flow states.

    public T8DefaultFlowStateHandler(T8Context context)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.definitionManager = serverContext.getDefinitionManager();
        this.persistenceEnabled = true;
        fetchDefinitions();
    }

    private void fetchDefinitions()
    {
        try
        {
            // STATE_FLOW.
            flowStateWaitKeyEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER, null);
            if (flowStateWaitKeyEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER);

            // STATE_FLOW.
            flowStateEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER, null);
            if (flowStateEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_DE_IDENTIFIER);

            // STATE_FLOW_INPUT_PAR.
            flowStateInputParameterEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_INPUT_PAR_DE_IDENTIFIER, null);
            if (flowStateInputParameterEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_INPUT_PAR_DE_IDENTIFIER);

            // STATE_FLOW_NODE.
            nodeStateEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER, null);
            if (nodeStateEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_NODE_DE_IDENTIFIER);

            // STATE_FLOW_NODE_INPUT_PAR.
            nodeStateInputParameterEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER, null);
            if (nodeStateInputParameterEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER);

            // STATE_FLOW_NODE_OUTPUT_PAR.
            nodeStateOutputParameterEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER, null);
            if (nodeStateOutputParameterEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER);

            // STATE_FLOW_NODE_EXEC_PAR.
            nodeStateExecParameterEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER, null);
            if (nodeStateExecParameterEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER);

            // STATE_FLOW_NODE_OUTPUT_NODE.
            nodeStateOutputNodeEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER, null);
            if (nodeStateOutputNodeEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER);

            // STATE_FLOW_DATA_OBJECT.
            nodeStateDataObjectEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER, null);
            if (nodeStateDataObjectEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER);

            // STATE_FLOW_WAIT_KEY.
            nodeStateWaitKeyEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER, null);
            if (nodeStateWaitKeyEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_WAIT_KEY_DE_IDENTIFIER);

            // STATE_FLOW_TASK.
            taskStateEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER, null);
            if (taskStateEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_TASK_DE_IDENTIFIER);

            // STATE_FLOW_TASK_INPUT_PAR.
            taskStateInputParameterEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER, null);
            if (taskStateInputParameterEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER);

            // STATE_FLOW_TASK_OUTPUT_PAR.
            taskStateOutputParameterEntityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(internalContext, null, T8FlowManagerResource.STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER, null);
            if (taskStateOutputParameterEntityDefinition == null) throw new Exception("Entity definition not found: " + T8FlowManagerResource.STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching entity definitions required for flow persistence.", e);
        }
    }

    public void setEnabled(boolean enabled)
    {
        persistenceEnabled = enabled;
    }

    @Override
    public T8FlowState loadFlowState(String flowInstanceId)
    {
        T8DataSession session;
        T8DataTransaction iTx;

        // Get the transaction to use (suspend any external transaction).
        session = serverContext.getDataManager().getCurrentSession();
        iTx = session.instantTransaction();

        // Retrieve the flow state.
        try
        {
            T8FlowStateDataSet dataSet;
            T8FlowState flowState;
            long retrievalStartTime;
            long retrievalEndTime;

            // Create a new data set to hold the retrieved data.
            dataSet = new T8FlowStateDataSet();
            retrievalStartTime = System.currentTimeMillis();

            // STATE_FLOW.
            dataSet.setFlowEntity(iTx.retrieve(STATE_FLOW_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_INPUT_PAR.
            dataSet.addInputParameterData(iTx.select(STATE_FLOW_INPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_INPUT_PAR_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_NODE.
            dataSet.addNodeData(iTx.select(STATE_FLOW_NODE_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_NODE_EXEC_PAR.
            dataSet.addNodeExecutionParameterData(iTx.select(STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_NODE_INPUT_PAR.
            dataSet.addNodeInputParameterData(iTx.select(STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_NODE_OUTPUT_PAR.
            dataSet.addNodeOutputParameterData(iTx.select(STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_NODE_OUTPUT_NODE.
            dataSet.addNodeOutputNodeData(iTx.select(STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_DATA_OBJECT.
            dataSet.addDataObjectData(iTx.select(STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_WAIT_KEY.
            dataSet.addWaitKeyData(iTx.select(STATE_FLOW_WAIT_KEY_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_TASK.
            dataSet.addTaskData(iTx.select(STATE_FLOW_TASK_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // STATE_FLOW_TASK_INPUT_PAR.
            dataSet.addTaskInputParameterData(iTx.select(STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER + F_FLOW_IID, flowInstanceId)));

            // Commit the transaction.
            retrievalEndTime = System.currentTimeMillis();
            LOGGER.log(dataSet.getEntityCount() + " rows of Flow '" + flowInstanceId + "' state data retrieved in: " + (System.currentTimeMillis() - retrievalStartTime) + "ms");

            // Construct the flow state and make sure its flags have been reset.
            if (dataSet.getFlowEntity() != null)
            {
                long constructionStartTime;

                constructionStartTime = System.currentTimeMillis();
                flowState = new T8FlowState(flowInstanceId, dataSet);
                flowState.statePersisted();
                flowState.setRetrievedTime(retrievalStartTime);
                flowState.setRetrievalTime(retrievalEndTime - retrievalStartTime);
                flowState.setConstructedTime(constructionStartTime);
                flowState.setConstructionTime(System.currentTimeMillis() - constructionStartTime);
                LOGGER.log(flowState.getSummary());
                return flowState;
            }
            else return null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while loading flow state: " + flowInstanceId, e);
        }
    }

    @Override
    public List<T8FlowState> loadActiveFlowStates()
    {
        try
        {
            T8DataTransaction tx;
            List<T8FlowState> flowStates;
            T8DataIterator<T8DataEntity> iterator;

            // Get the transaction to use.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

            // STATE_FLOW.
            flowStates = new ArrayList<>();
            iterator = tx.iterate(STATE_FLOW_DE_IDENTIFIER, null);
            while (iterator.hasNext())
            {
                T8FlowState flowState;
                T8DataEntity nextEntity;

                nextEntity = iterator.next();
                flowState = loadFlowState((String)nextEntity.getFieldValue(STATE_FLOW_DE_IDENTIFIER + F_FLOW_IID));
                flowStates.add(flowState);
            }

            // Construct the flow state and return it.
            iterator.close();
            return flowStates;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while loading active flow states.", e);
        }
    }

    @Override
    public void saveFlowState(T8FlowState flowState)
    {
        if (persistenceEnabled)
        {
            synchronized (flowState)
            {
                T8FlowStateUpdates updates;
                Retryable retryable;

                // Create an object to store all updates.
                updates = new T8FlowStateUpdates();
                updates.setFlowStateEntityDefinition(flowStateEntityDefinition);
                updates.setFlowStateInputParameterEntityDefinition(flowStateInputParameterEntityDefinition);
                updates.setNodeStateEntityDefinition(nodeStateEntityDefinition);
                updates.setNodeStateInputParameterEntityDefinition(nodeStateInputParameterEntityDefinition);
                updates.setNodeStateOutputParameterEntityDefinition(nodeStateOutputParameterEntityDefinition);
                updates.setNodeStateExecutionParameterEntityDefinition(nodeStateExecParameterEntityDefinition);
                updates.setNodeStateOutputNodeEntityDefinition(nodeStateOutputNodeEntityDefinition);
                updates.setNodeStateWaitKeyEntityDefinition(nodeStateWaitKeyEntityDefinition);
                updates.setDataObjectStateEntityDefinition(nodeStateDataObjectEntityDefinition);
                updates.setTaskStateEntityDefinition(taskStateEntityDefinition);
                updates.setTaskStateInputParameterEntityDefinition(taskStateInputParameterEntityDefinition);
                updates.setTaskStateOutputParameterEntityDefinition(taskStateOutputParameterEntityDefinition);

                // Get all flow state updates.
                flowState.addUpdates(updates);

                // Create a new rerunnable logic.
                retryable = new Retryable(internalContext, "saveFlowState")
                {
                    @Override
                    public void execute(T8DataTransaction tx) throws Exception
                    {
                        // Persist inserts.
                        for (T8DataEntity entity : updates.getInserts())
                        {
                            tx.insert(entity);
                        }

                        // Persist updates.
                        for (T8DataEntity entity : updates.getUpdates())
                        {
                            tx.update(entity);
                        }

                        // Persist deletions.
                        for (T8DataEntity entity : updates.getDeletions())
                        {
                            tx.delete(entity);
                        }
                    }
                };

                // Execute the rerunnable logic.
                try
                {
                    retryable.execute(3, 1000);

                    // Now that all updates have been saved, reset the state flags.
                    flowState.statePersisted();
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while saving flow state: " , e);
                }
            }
        }
    }

    @Override
    public void deleteFlowState(T8FlowState flowState)
    {
        if (persistenceEnabled)
        {
            Retryable retryable;
            String flowIid;

            // Get the instance identifier of the flow to delete.
            flowIid = flowState.getFlowIid();

            // Create a new retryable logic.
            retryable = new Retryable(internalContext, "deleteFlowState")
            {
                @Override
                public void execute(T8DataTransaction tx) throws Exception
                {
                    deleteFlowState(tx, flowIid);
                }
            };

            // Execute the retryable logic.
            try
            {
                retryable.execute(3, 1000);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while deleting flow state: " + flowState , e);
            }
        }
    }

    public void deleteFlowState(String flowIid) throws Exception
    {
        if (persistenceEnabled)
        {
            Retryable retryable;

            // Create a new retryable logic.
            retryable = new Retryable(internalContext, "deleteFlowState")
            {
                @Override
                public void execute(T8DataTransaction tx) throws Exception
                {
                    deleteFlowState(tx, flowIid);
                }
            };

            // Execute the retryable logic.
            try
            {
                retryable.execute(3, 1000);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while deleting flow state: " + flowIid , e);
            }
        }
    }

    public void deleteFlowState(T8DataTransaction tx, String flowIid) throws Exception
    {
        if (persistenceEnabled)
        {
            // STATE_FLOW_TASK_OUTPUT_PAR.
            tx.delete(STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_OUTPUT_PAR_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_TASK_INPUT_PAR.
            tx.delete(STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_INPUT_PAR_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_TASK.
            tx.delete(STATE_FLOW_TASK_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_TASK_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_TASK_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_DATA_OBJECT.
            tx.delete(STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_DATA_OBJECT_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_NODE_OUTPUT_NODE.
            tx.delete(STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_OUTPUT_NODE_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_NODE_EXEC_PAR.
            tx.delete(STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_EXEC_PAR_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_NODE_INPUT_PAR.
            tx.delete(STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_NODE_OUTPUT_PAR.
            tx.delete(STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_OUTPUT_PAR_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_WAIT_KEY.
            tx.delete(STATE_FLOW_WAIT_KEY_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_WAIT_KEY_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_NODE.
            tx.delete(STATE_FLOW_NODE_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_NODE_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_NODE_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW_INPUT_PAR.
            tx.delete(STATE_FLOW_INPUT_PAR_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_INPUT_PAR_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_INPUT_PAR_DE_IDENTIFIER + F_FLOW_IID, flowIid)));

            // STATE_FLOW.
            tx.delete(STATE_FLOW_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_DE_IDENTIFIER + F_FLOW_IID, flowIid)));
        }
    }

    @Override
    public void addWaitKey(T8FlowNodeStateWaitKey waitKey)
    {
        if (persistenceEnabled)
        {
            T8DataTransaction tx;
            T8DataEntity keyEntity;

            // Get the transaction to use.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

            try
            {
                // Save the wait key.
                keyEntity = waitKey.createEntity(flowStateWaitKeyEntityDefinition);
                if (!tx.update(keyEntity)) // TODO:  Investigate ways to remove this existence check i.e. ways that the flow will never need to add a wait key twice.
                {
                    tx.insert(keyEntity);
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while adding flow wait key: " + waitKey, e);
            }
        }
    }

    @Override
    public void deleteNodeWaitKeys(String nodeIid)
    {
        if (persistenceEnabled)
        {
            try
            {
                T8DataTransaction tx;

                // Get the transaction to use.
                tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
                tx.delete(STATE_FLOW_WAIT_KEY_DE_IDENTIFIER, new T8DataFilter(STATE_FLOW_WAIT_KEY_DE_IDENTIFIER, HashMaps.createSingular(STATE_FLOW_WAIT_KEY_DE_IDENTIFIER + F_NODE_IID, nodeIid)));
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while deleting node wait keys: " + nodeIid, e);
            }
        }
    }

    @Override
    public T8DataIterator<T8DataEntity> getWaitingFlowIterator(T8DataFilterCriteria filterCriteria)
    {
        try
        {
            T8DataTransaction tx;
            T8DataSession dataSession;
            String entityId;

            // Select all wait key entities that are applicable to the flow event.
            entityId = STATE_FLOW_WAIT_KEY_DE_IDENTIFIER;
            dataSession = serverContext.getDataManager().getCurrentSession();
            tx = dataSession.instantTransaction();
            return tx.iterate(entityId, new T8DataFilter(entityId, filterCriteria));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching waiting flow iterator.", e);
        }
    }

    @Override
    public void logFlowEvent(T8Context eventContext, FlowHistoryEvent event, T8Flow flow, Map<String, Object> eventParameters, String eventInformation)
    {
        T8DataTransaction tx;
        T8DataEntity entity;
        String entityId;
        String eventIid;

        // Get the transaction.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Create a new task history entity.
        try
        {
            T8FlowState flowState;

            flowState = flow.getState();
            eventIid = T8IdentifierUtilities.createNewGUID();
            entity = tx.create(T8FlowManagerResource.FLOW_HISTORY_DE_IDENTIFIER, null);
            entityId = T8FlowManagerResource.FLOW_HISTORY_DE_IDENTIFIER;
            entity.setFieldValue(entityId + F_EVENT_IID, eventIid);
            entity.setFieldValue(entityId + F_FLOW_IID, flow.getInstanceIdentifier());
            entity.setFieldValue(entityId + F_FLOW_ID, flow.getDefinition().getIdentifier());
            entity.setFieldValue(entityId + F_PROJECT_ID, flow.getDefinition().getRootProjectId());
            entity.setFieldValue(entityId + F_INITIATOR_ID, flowState.getInitiatorId());
            entity.setFieldValue(entityId + F_INITIATOR_IID, flowState.getInitiatorIid());
            entity.setFieldValue(entityId + F_INITIATOR_ORG_ID, flowState.getInitiatorOrgId());
            entity.setFieldValue(entityId + F_EVENT, event.toString());
            entity.setFieldValue(entityId + F_TIME, new java.sql.Timestamp(System.currentTimeMillis()));
            entity.setFieldValue(entityId + F_EVENT_PARAMETERS, createParameterString(flow.getDefinition().getIdentifier(), eventParameters)); // We always strip the flow namespace, all other parameter types will maintain their namespaces.
            entity.setFieldValue(entityId + F_INFORMATION, eventInformation);

            // Insert the entity.
            tx.insert(entity);

            // Insert the Flow History parameters.
            T8HistoryParameterPersistenceHandler.insertParameters(tx, T8FlowManagerResource.FLOW_HISTORY_PAR_DE_IDENTIFIER, eventIid, T8IdentifierUtilities.stripNamespace(eventParameters));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while logging flow event.", e);
        }
    }

    @Override
    public void logTaskEvent(T8Context eventContext, TaskEvent event, T8FlowTaskState taskState, Map<String, Object> eventParameters)
    {
        Map<String, Object> flowParameters;
        T8SessionContext sessionContext;
        T8DataTransaction tx;
        T8DataEntity entity;
        String entityId;
        String userId;
        String userProfileId;
        String eventIid;

        flowParameters = new HashMap<>();
        sessionContext = eventContext.getSessionContext();

        // Determine the user identifier;
        switch (event)
        {
            case CLAIMED:
            case UNCLAIMED:
            case REASSIGNED:
            case PRIORITY_UPDATED:
            case COMPLETED:
                userId = sessionContext.getUserIdentifier();
                userProfileId = sessionContext.getUserProfileIdentifier();
                break;
            case ISSUED:
                userId = taskState.getRestrictionUserId();
                userProfileId = null;
                break;
            case CANCELLED:
            default:
                userId = null;
                userProfileId = null;
        }

        // Get the transaction.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Create a new task history entity.
        try
        {
            eventIid = T8IdentifierUtilities.createNewGUID();
            entity = tx.create(T8FlowManagerResource.TASK_HISTORY_DE_IDENTIFIER, null);
            entityId = T8FlowManagerResource.TASK_HISTORY_DE_IDENTIFIER;
            entity.setFieldValue(entityId + F_EVENT_IID, eventIid);
            entity.setFieldValue(entityId + F_FLOW_ID, taskState.getFlowId());
            entity.setFieldValue(entityId + F_FLOW_IID, taskState.getFlowIid());
            entity.setFieldValue(entityId + F_NODE_ID, taskState.getNodeId());
            entity.setFieldValue(entityId + F_NODE_IID, taskState.getNodeIid());
            entity.setFieldValue(entityId + F_TASK_ID, taskState.getTaskId());
            entity.setFieldValue(entityId + F_TASK_IID, taskState.getTaskIid());
            entity.setFieldValue(entityId + F_PROJECT_ID, taskState.getProjectId());
            entity.setFieldValue(entityId + F_USER_ID, userId);
            entity.setFieldValue(entityId + F_USER_PROFILE_ID, userProfileId);
            entity.setFieldValue(entityId + F_FLOW_PROFILE_ID, taskState.getRestrictionProfileId());
            entity.setFieldValue(entityId + F_EVENT, event.toString());
            entity.setFieldValue(entityId + F_TIME, new java.sql.Timestamp(System.currentTimeMillis()));
            entity.setFieldValue(entityId + F_EVENT_PARAMETERS, createParameterString(taskState.getTaskId(), eventParameters)); // We always strip the task namespace, all other parameter types will maintain their namespaces.
            entity.setFieldValue(entityId + F_FLOW_PARAMETERS, createParameterString(taskState.getFlowId(), flowParameters)); // We always strip the flow namespace, all other parameter types will maintain their namespaces.

            // Insert the history log entity.
            tx.insert(entity);
            T8HistoryParameterPersistenceHandler.insertParameters(tx, T8FlowManagerResource.TASK_HISTORY_PAR_DE_IDENTIFIER, eventIid, T8IdentifierUtilities.stripNamespace(eventParameters));

            // Create the history summary entity.
            entityId = T8FlowManagerResource.TASK_HISTORY_SUMMARY_DE_IDENTIFIER;
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + F_FLOW_ID, taskState.getFlowId());
            entity.setFieldValue(entityId + F_FLOW_IID, taskState.getFlowIid());
            entity.setFieldValue(entityId + F_NODE_ID, taskState.getNodeId());
            entity.setFieldValue(entityId + F_NODE_IID, taskState.getNodeIid());
            entity.setFieldValue(entityId + F_TASK_ID, taskState.getTaskId());
            entity.setFieldValue(entityId + F_TASK_IID, taskState.getTaskIid());
            entity.setFieldValue(entityId + F_PROJECT_ID, taskState.getProjectId());
            entity.setFieldValue(entityId + F_USER_ID, taskState.getRestrictionUserId() != null ? taskState.getRestrictionUserId() : taskState.getClaimedByUserId());
            entity.setFieldValue(entityId + F_FLOW_PROFILE_ID, taskState.getRestrictionProfileId());
            entity.setFieldValue(entityId + F_GROUP_ID, taskState.getGroupId());
            entity.setFieldValue(entityId + F_GROUP_ITERATION, taskState.getGroupIteration());
            entity.setFieldValue(entityId + F_ESCALATION_LEVEL, taskState.getEscalationLevel());
            entity.setFieldValue(entityId + F_ISSUED_TIME, taskState.getTimeIssued());
            entity.setFieldValue(entityId + F_ISSUED_TIME_SPAN, taskState.getTimespanIssued());
            entity.setFieldValue(entityId + F_ISSUED_BUSINESS_TIME_SPAN, taskState.getTimespanBusinessIssued());
            entity.setFieldValue(entityId + F_CLAIMED_TIME, taskState.getTimeClaimed());
            entity.setFieldValue(entityId + F_CLAIMED_TIME_SPAN, taskState.getTimespanClaimed());
            entity.setFieldValue(entityId + F_CLAIMED_BUSINESS_TIME_SPAN, taskState.getTimespanBusinessClaimed());
            entity.setFieldValue(entityId + F_COMPLETED_TIME, taskState.getTimeCompleted());
            entity.setFieldValue(entityId + F_TOTAL_TIME_SPAN, taskState.getTimespanTotal());
            entity.setFieldValue(entityId + F_TOTAL_BUSINESS_TIME_SPAN, taskState.getTimespanBusinessTotal());

            // Update the task history summary.
            if (!tx.update(entity))
            {
                tx.insert(entity);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while logging task event.", e);
        }
    }

    private String createParameterString(String namespace, Map<String, Object> parameters)
    {
        if (parameters == null || parameters.isEmpty())
        {
            return null;
        }
        else
        {
            StringBuffer parameterString;
            Map<String, Object> namespaceStrippedParameters;

            // Strip the flow namespace from the parameters.
            namespaceStrippedParameters = T8IdentifierUtilities.stripNamespace(namespace, parameters, false);

            // Add all of the parameters to the string.
            parameterString = new StringBuffer();
            for (String parameterKey : namespaceStrippedParameters.keySet())
            {
                Object parameterValue;

                // Append the prefix and parameter key.
                parameterString.append(" ["); // Yes, there is a space before the bracket.
                parameterString.append(parameterKey);
                parameterString.append(":");

                // Append the parameter value if any.
                parameterValue = namespaceStrippedParameters.get(parameterKey);
                if (parameterValue != null) parameterString.append(parameterValue.toString());

                // Append the suffix.
                parameterString.append("]");
            }

            return parameterString.toString();
        }
    }

    public void queueFlowEvent(T8FlowController controller, T8FlowEvent flowEvent)
    {
        T8DataTransaction tx;

        // Get the transaction.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Create the event entity and persist it.
        try
        {
            T8DataEntity entity;
            String entityId;

            entityId = T8FlowManagerResource.STATE_FLOW_EVENT_QUEUE_DE_IDENTIFIER;
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + F_EVENT_IID, flowEvent.getEventIid());
            entity.setFieldValue(entityId + F_TYPE, flowEvent.getEventTypeId());
            entity.setFieldValue(entityId + F_TIME, new java.sql.Timestamp(flowEvent.getEventTimeInMillis()));
            entity.setFieldValue(entityId + F_DATA, flowEvent.serialize(controller).toString());

            // Insert the entity.
            tx.insert(entity);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while logging flow event.", e);
        }
    }

    public void unqueueFlowEvent(T8FlowController controller, T8FlowEvent flowEvent)
    {
        T8DataTransaction tx;

        // Get the transaction.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Create the event entity and persist it.
        try
        {
            T8DataEntity entity;
            String entityId;

            entityId = T8FlowManagerResource.STATE_FLOW_EVENT_QUEUE_DE_IDENTIFIER;
            entity = tx.create(entityId, null);
            entity.setFieldValue(entityId + F_EVENT_IID, flowEvent.getEventIid());

            // Delete the entity.
            tx.delete(entity);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while logging flow event.", e);
        }
    }

    public T8FlowEvent retrieveNextFlowEvent(T8FlowController controller)
    {
        T8DataTransaction tx;
        String entityId;

        // Get the transaction.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Create the event entity and persist it.
        try
        {
            List<T8DataEntity> entities;
            T8DataFilter filter;

            // Create the filter to retrieve the entity with the lowest time i.e. forming a fifo queuing system.
            entityId = T8FlowManagerResource.STATE_FLOW_EVENT_QUEUE_DE_IDENTIFIER;
            filter = new T8DataFilter(entityId);
            filter.addFieldOrdering(entityId + F_TIME, T8DataFilter.OrderMethod.ASCENDING);

            // Select the first entity from the ordered result set.
            entities = tx.select(entityId, filter, 0, 1);
            if (entities.size() > 0)
            {
                T8DataEntity entity;
                T8FlowEvent nextEvent;
                String data;

                //  Get the JSON data from the entity.
                entity = entities.get(0);
                data = (String)entity.getFieldValue(entityId + F_DATA);

                // Now attempt to deserialize the event and then remove it from the queue.
                nextEvent = T8FlowEvent.deserialize(controller, JsonObject.readFrom(data));
                tx.delete(entity);
                return nextEvent;
            }
            else return null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while logging flow event.", e);
        }
    }

    private abstract static class Retryable
    {
        private final T8DataSession session;
        private final String executionName;

        private static final T8Logger LOGGER = T8Log.getLogger(Retryable.class);

        public Retryable(T8Context context, String executionName)
        {
            this.session = context.getServerContext().getDataManager().getCurrentSession();
            this.executionName = executionName;
        }

        protected abstract void execute(T8DataTransaction tx) throws Exception;

        public void execute(int maxRetryCount, int retryWaitMillis) throws Exception
        {
            T8DataTransaction xtx;

            // Suspend any external transaction.
            xtx = session.suspend();

            try
            {
                boolean retry;
                int retryCount;

                retry = true;
                retryCount = 0;
                while (retry)
                {
                    T8DataTransaction tx;

                    // Begin a new transaction just for this iteration.
                    tx = session.beginTransaction();
                    try
                    {
                        execute(tx);
                        tx.commit();
                        retry = false;
                    }
                    catch (Exception e)
                    {
                        // Rollback any changes.
                        tx.rollback();
                        LOGGER.log("Exception during execution " + retryCount + " of: " + executionName , e);

                        // If the current retry count is still less than the maximum allowable number, log the failure wait a while and then retry.
                        if (retryCount < maxRetryCount)
                        {
                            // Wait for a second and then retry.
                            LOGGER.log("Retrying '" + executionName + "' in " + retryWaitMillis + " ms...");
                            Thread.sleep(retryWaitMillis);
                            retryCount++;
                            retry = true;
                        }
                        else
                        {
                            // Retry count has been reach so simply throw the exception.
                            LOGGER.log("Maximum retry count " + maxRetryCount + " of '" + executionName + " reached.  Throwing exception...");
                            throw e;
                        }
                    }
                }
            }
            finally
            {
                // If an external session was suspended, resume it.
                if (xtx != null) session.resume(xtx);
            }
        }
    }
}
