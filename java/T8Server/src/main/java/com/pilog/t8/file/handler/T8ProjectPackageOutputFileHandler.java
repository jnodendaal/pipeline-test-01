package com.pilog.t8.file.handler;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.file.T8FileHandler;
import com.pilog.t8.project.T8ProjectPackage;
import com.pilog.t8.security.T8Context;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * @author Bouwer du Preez
 */
public class T8ProjectPackageOutputFileHandler implements T8FileHandler
{
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8FileManager fileManager;
    private final String fileContextIid;
    private final String filePath;
    private OutputStreamWriter outputWriter;

    public static final String FILE_HANDLER_ID = "@FILE_PROJECT_PACKAGE_OUTPUT";

    public T8ProjectPackageOutputFileHandler(T8Context context, T8FileManager fileManager, String contextIid, String filePath)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.fileManager = fileManager;
        this.fileContextIid = contextIid;
        this.filePath = filePath;
    }

    @Override
    public void open() throws Exception
    {
        OutputStream outputStream;

        outputStream = fileManager.getFileOutputStream(context, fileContextIid, filePath, false);
        outputWriter = new OutputStreamWriter(outputStream); // No buffered reader required - the JsonParser handles buffering internally.
    }

    @Override
    public void close() throws Exception
    {
        if (outputWriter != null) outputWriter.close();
        outputWriter = null;
    }

    public void writePackage(T8ProjectPackage projectPackage) throws Exception
    {
        projectPackage.write(serverContext.getDefinitionManager(), outputWriter);
    }
}
