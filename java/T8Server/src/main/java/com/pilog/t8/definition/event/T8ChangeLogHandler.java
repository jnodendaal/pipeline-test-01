package com.pilog.t8.definition.event;

import static com.pilog.t8.definition.event.T8ChangeLogHandlerAPIHandler.*;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8Definition.T8DefinitionLevel;
import com.pilog.t8.definition.T8DefinitionSerializer;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ChangeLogHandler extends T8DefinitionManagerEventAdapter
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ChangeLogHandler.class);

    private static final String EVENT_SAVED = "SAVED";
    private static final String EVENT_DELETED = "DELETED";
    private static final String EVENT_IMPORTED = "IMPORTED";
    private static final String EVENT_RENAMED = "RENAMED";

    private final T8ChangeLogHandlerDefinition definition;
    private final T8DefinitionManager definitionManager;
    private final T8DefinitionSerializer serializer;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final String entityIdentifier;

    public T8ChangeLogHandler(T8Context context, T8ChangeLogHandlerDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definitionManager = serverContext.getDefinitionManager();
        this.definition = definition;
        this.entityIdentifier = definition.getDataEntityIdentifier();
        this.serializer = new T8DefinitionSerializer(definitionManager);
    }

    @Override
    public void definitionSaved(T8DefinitionSavedEvent event)
    {
        T8Definition savedDefinition;

        savedDefinition = event.getDefinition();
        // The user definition is a special case, but we need a better way to handle the check
        if (savedDefinition.getLevel() == T8DefinitionLevel.PROJECT || savedDefinition instanceof T8UserDefinition)
        {
            try
            {
                T8DataTransaction tx;
                T8DataSession session;

                // Get the transaction to use and persist the data.
                session = serverContext.getDataManager().getCurrentSession();
                // If this is happening in the context of a transaction, we want to use that transaction
                tx = session.getTransaction();
                // Otherwise we use an instant transaction
                if (tx == null) tx = session.instantTransaction();

                persistDefinitionChange(tx, event.getContext(), savedDefinition, serializer.serializeDefinition(savedDefinition), EVENT_SAVED);
            }
            catch (Exception ex)
            {
                LOGGER.log("Exception while logging definition change.", ex);
            }
        }
    }

    @Override
    public void definitionDeleted(T8DefinitionDeletedEvent event)
    {
        T8Definition deletedDefinition;

        deletedDefinition = event.getOldDefinition();
        // The user definition is a special case, but we need a better way to handle the check
        if (deletedDefinition.getLevel() == T8DefinitionLevel.PROJECT || deletedDefinition instanceof T8UserDefinition)
        {
            try
            {
                T8DataTransaction tx;
                T8DataSession session;

                // Get the transaction to use and persist the data.
                session = serverContext.getDataManager().getCurrentSession();
                // If this is happening in the context of a transaction, we want to use that transaction
                tx = session.getTransaction();
                // Otherwise we use an instant transaction
                if (tx == null) tx = session.instantTransaction();

                persistDefinitionChange(tx, event.getContext(), deletedDefinition, null, EVENT_DELETED);
            }
            catch (Exception ex)
            {
                LOGGER.log("Exception while logging definition deletion event.", ex);
            }
        }
    }

    @Override
    public void definitionRenamed(T8DefinitionRenamedEvent event)
    {
        T8Definition renamedDefinition;

        renamedDefinition = event.getDefinition();
        // The user definition is a special case, but we need a better way to handle the check
        if (renamedDefinition.getLevel() == T8DefinitionLevel.PROJECT || renamedDefinition instanceof T8UserDefinition)
        {
            try
            {
                T8DataTransaction tx;
                T8DataSession session;

                // Get the transaction to use and persist the data.
                session = serverContext.getDataManager().getCurrentSession();
                // If this is happening in the context of a transaction, we want to use that transaction
                tx = session.getTransaction();
                // Otherwise we use an instant transaction
                if (tx == null) tx = session.instantTransaction();

                persistDefinitionChange(tx, event.getContext(), renamedDefinition, event.getOldIdentifier(), EVENT_RENAMED);
            }
            catch (Exception ex)
            {
                LOGGER.log("Exception while logging definition rename event.", ex);
            }
        }
    }

    @Override
    public void definitionImported(T8DefinitionImportedEvent event)
    {
        T8Definition importedDefinition;

        importedDefinition = event.getDefinition();
        // The user definition is a special case, but we need a better way to handle the check
        if (importedDefinition.getLevel() == T8DefinitionLevel.PROJECT || importedDefinition instanceof T8UserDefinition)
        {
            try
            {
                T8DataTransaction tx;
                T8DataSession session;

                // Get the transaction to use and persist the data.
                session = serverContext.getDataManager().getCurrentSession();
                // If this is happening in the context of a transaction, we want to use that transaction
                tx = session.getTransaction();
                // Otherwise we use an instant transaction
                if (tx == null) tx = session.instantTransaction();

                persistDefinitionChange(tx, event.getContext(), importedDefinition, serializer.serializeDefinition(importedDefinition), EVENT_IMPORTED);
            }
            catch (Exception ex)
            {
                LOGGER.log("Exception while logging definition import event.", ex);
            }
        }
    }

    private void persistDefinitionChange(T8DataTransaction tx, T8Context eventContext, T8Definition definition, String eventData, String changeType) throws Exception
    {
        T8SessionContext sessionContext;
        T8DataEntity entity;
        String agentId;

        // Get the id of the agent that performed the update.
        sessionContext = eventContext.getSessionContext();
        agentId = sessionContext.getUserIdentifier();
        if (agentId == null) agentId = sessionContext.getSystemAgentIdentifier();

        // Get the transaction to use and construct a new entity.
        entity = tx.create(entityIdentifier, null);
        entity.setFieldValue(entityIdentifier + EF_EVENT_IID, T8IdentifierUtilities.createNewGUID());
        entity.setFieldValue(entityIdentifier + EF_EVENT, changeType);
        entity.setFieldValue(entityIdentifier + EF_DEFINITION_ID, definition.getIdentifier());
        entity.setFieldValue(entityIdentifier + EF_TIME, new java.sql.Timestamp(System.currentTimeMillis()));
        entity.setFieldValue(entityIdentifier + EF_USER_ID, agentId);
        entity.setFieldValue(entityIdentifier + EF_DATA, eventData);

        // Log the event.
        tx.insert(entity);
    }
}
