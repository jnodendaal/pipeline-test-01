package com.pilog.t8.data;

import com.pilog.t8.definition.T8IdentifierUtilities;
import java.util.List;
import java.util.Map;
import com.pilog.t8.time.T8Timestamp;

/**
 * @author Bouwer du Preez
 */
public class T8HistoryParameterPersistenceHandler
{
    private static final String EF_EVENT_IID = "$EVENT_IID";
    private static final String EF_PARAMETER_KEY = "$PARAMETER_KEY";
    private static final String EF_PARAMETER_ID = "$PARAMETER_ID";
    private static final String EF_PARAMETER_IID = "$PARAMETER_IID";
    private static final String EF_PARENT_PARAMETER_IID = "$PARENT_PARAMETER_IID";
    private static final String EF_ID = "$ID";
    private static final String EF_SEQUENCE = "$SEQUENCE";
    private static final String EF_VALUE = "$VALUE";
    private static final String EF_TYPE = "$TYPE";

    public static final void insertParameters(T8DataTransaction tx, String entityIdentifier, String eventIID, Map<String, Object> parameters) throws Exception
    {
        if (parameters != null)
        {
            for (String parameterId : parameters.keySet())
            {
                T8DataEntity parameterEntity;
                Object parameterValue;

                // Create the parameter entity.
                parameterValue = parameters.get(parameterId);
                parameterEntity = tx.create(entityIdentifier, null);
                parameterEntity.setFieldValue(entityIdentifier + EF_EVENT_IID, eventIID);
                parameterEntity.setFieldValue(entityIdentifier + EF_PARAMETER_ID, T8IdentifierUtilities.createNewGUID());
                parameterEntity.setFieldValue(entityIdentifier + EF_ID, parameterId);
                parameterEntity.setFieldValue(entityIdentifier + EF_VALUE, parameterValue != null ? parameterValue.toString() : null);

                // Insert the entity.
                tx.insert(parameterEntity);
            }
        }
    }

    public static final void insertParametersExpanded(T8DataTransaction tx, String entityIdentifier, String eventIID, Map<String, Object> parameters) throws Exception
    {
        insertParametersExpanded(tx, entityIdentifier, eventIID, null, parameters);
    }

    private static void insertParametersExpanded(T8DataTransaction tx, String entityIdentifier, String eventIID, String parentParameterIID, Map<String, ? extends Object> parameters) throws Exception
    {
        if (parameters != null)
        {
            int sequence;

            sequence = 0;
            for (String parameterIdentifier : parameters.keySet())
            {
                Object parameterValue;

                parameterValue = parameters.get(parameterIdentifier);
                if (parameterValue != null)
                {
                    T8DataEntity parameterEntity;
                    String parameterIID;

                    // Create the parameter entity.
                    parameterIID = T8IdentifierUtilities.createNewGUID();
                    parameterEntity = tx.create(entityIdentifier, null);
                    parameterEntity.setFieldValue(entityIdentifier + EF_EVENT_IID, eventIID);
                    parameterEntity.setFieldValue(entityIdentifier + EF_PARAMETER_IID, parameterIID);
                    parameterEntity.setFieldValue(entityIdentifier + EF_PARENT_PARAMETER_IID, parentParameterIID);
                    parameterEntity.setFieldValue(entityIdentifier + EF_TYPE, getParameterType(parameterValue));
                    parameterEntity.setFieldValue(entityIdentifier + EF_PARAMETER_KEY, parameterIdentifier);
                    parameterEntity.setFieldValue(entityIdentifier + EF_SEQUENCE, sequence++);
                    parameterEntity.setFieldValue(entityIdentifier + EF_VALUE, getParameterValue(parameterValue));

                    // Insert the entity.
                    tx.insert(parameterEntity);

                    // If the parameter value is a collection, insert its contents.
                    if (parameterValue instanceof Map)
                    {
                        insertParametersExpanded(tx, entityIdentifier, eventIID, parameterIID, (Map<String, ? extends Object>)parameterValue);
                    }
                    else if (parameterValue instanceof List)
                    {
                        insertParametersExpanded(tx, entityIdentifier, eventIID, parameterIID, (List<? extends Object>)parameterValue);
                    }
                }
            }
        }
    }

    private static final void insertParametersExpanded(T8DataTransaction tx, String entityIdentifier, String eventIID, String parentParameterIID, List<? extends Object> parameters) throws Exception
    {
        if (parameters != null)
        {
            for (int index = 0; index < parameters.size(); index++)
            {
                T8DataEntity parameterEntity;
                Object parameterValue;
                String parameterIID;

                // Create the parameter entity.
                parameterValue = parameters.get(index);
                parameterIID = T8IdentifierUtilities.createNewGUID();
                parameterEntity = tx.create(entityIdentifier, null);
                parameterEntity.setFieldValue(entityIdentifier + EF_EVENT_IID, eventIID);
                parameterEntity.setFieldValue(entityIdentifier + EF_PARAMETER_IID, parameterIID);
                parameterEntity.setFieldValue(entityIdentifier + EF_PARENT_PARAMETER_IID, parentParameterIID);
                parameterEntity.setFieldValue(entityIdentifier + EF_TYPE, getParameterType(parameterValue));
                parameterEntity.setFieldValue(entityIdentifier + EF_PARAMETER_KEY, null);
                parameterEntity.setFieldValue(entityIdentifier + EF_SEQUENCE, index);
                parameterEntity.setFieldValue(entityIdentifier + EF_VALUE, getParameterValue(parameterValue));

                // Insert the entity.
                tx.insert(parameterEntity);

                // If the parameter value is a collection, insert its contents.
                if (parameterValue instanceof Map)
                {
                    insertParametersExpanded(tx, entityIdentifier, eventIID, parameterIID, (Map<String, ? extends Object>)parameterValue);
                }
                else if (parameterValue instanceof List)
                {
                    insertParametersExpanded(tx, entityIdentifier, eventIID, parameterIID, (List<? extends Object>)parameterValue);
                }
            }
        }
    }

    public static String getParameterValue(Object value)
    {
        if (value == null)
        {
            return null;
        }
        else if (value instanceof Map)
        {
            return null;
        }
        else if (value instanceof List)
        {
            return null;
        }
        else if (value instanceof T8Timestamp)
        {
            return String.valueOf(((T8Timestamp)value).getMilliseconds());
        }
        else
        {
            return value.toString();
        }
    }

    public static String getParameterType(Object parameterValue)
    {
        if (parameterValue == null)
        {
            return "NULL";
        }
        else if (parameterValue instanceof Map)
        {
            return "MAP";
        }
        else if (parameterValue instanceof List)
        {
            return "LIST";
        }
        else if (parameterValue instanceof Integer)
        {
            return "INTEGER";
        }
        else if (parameterValue instanceof Long)
        {
            return "LONG";
        }
        else if (parameterValue instanceof Double)
        {
            return "DOUBLE";
        }
        else if (parameterValue instanceof Float)
        {
            return "FLOAT";
        }
        else if (parameterValue instanceof Boolean)
        {
            return "BOOLEAN";
        }
        else if (parameterValue instanceof String)
        {
            return "STRING";
        }
        else if (parameterValue instanceof T8Timestamp)
        {
            return "TIMESTAMP";
        }
        else throw new RuntimeException("Invalid parameter type: " + parameterValue.getClass());
    }
}
