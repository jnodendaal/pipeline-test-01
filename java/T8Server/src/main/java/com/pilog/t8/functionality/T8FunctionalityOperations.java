package com.pilog.t8.functionality;

import com.pilog.t8.T8FunctionalityManager;
import com.pilog.t8.T8ServerOperation.T8ServerOperationExecutionType;
import com.pilog.t8.definition.operation.T8DefaultServerOperation;
import com.pilog.t8.definition.operation.java.T8JavaServerOperationDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.Map;

import static com.pilog.t8.definition.functionality.T8FunctionalityManagerResource.*;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8FunctionalityOperations
{
    public static class GetFunctionalityGroupHandleOperation extends T8DefaultServerOperation
    {
        public GetFunctionalityGroupHandleOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FunctionalityManager functionalityManager;
            String functionalityGroupId;
            String dataObjectIid;
            String dataObjectId;

            functionalityGroupId = (String)operationParameters.get(PARAMETER_FUNCTIONALITY_GROUP_ID);
            dataObjectIid = (String)operationParameters.get(PARAMETER_DATA_OBJECT_IID);
            dataObjectId = (String)operationParameters.get(PARAMETER_DATA_OBJECT_ID);

            functionalityManager = serverContext.getFunctionalityManager();
            return HashMaps.newHashMap(PARAMETER_FUNCTIONALITY_GROUP_HANDLE, functionalityManager.getFunctionalityGroupHandle(context, functionalityGroupId, dataObjectId, dataObjectIid));
        }
    }

    public static class GetFunctionalityHandleOperation extends T8DefaultServerOperation
    {
        public GetFunctionalityHandleOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FunctionalityManager functionalityManager;
            String functionalityId;
            String dataObjectIid;
            String dataObjectId;

            functionalityId = (String)operationParameters.get(PARAMETER_FUNCTIONALITY_ID);
            dataObjectIid = (String)operationParameters.get(PARAMETER_DATA_OBJECT_IID);
            dataObjectId = (String)operationParameters.get(PARAMETER_DATA_OBJECT_ID);

            functionalityManager = serverContext.getFunctionalityManager();
            return HashMaps.newHashMap(PARAMETER_FUNCTIONALITY_HANDLE, functionalityManager.getFunctionalityHandle(context, functionalityId, dataObjectId, dataObjectIid));
        }
    }

    public static class AccessFunctionalityOperation extends T8DefaultServerOperation
    {
        public AccessFunctionalityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FunctionalityManager functionalityManager;
            Map<String, Object> parameters;
            String functionalityId;

            functionalityId = (String)operationParameters.get(PARAMETER_FUNCTIONALITY_ID);
            parameters = (Map<String, Object>)operationParameters.get(PARAMETER_FUNCTIONALITY_PARAMETERS);

            functionalityManager = serverContext.getFunctionalityManager();
            return HashMaps.newHashMap(PARAMETER_FUNCTIONALITY_EXECUTION_HANDLE, functionalityManager.accessFunctionality(context, functionalityId, parameters));
        }
    }

    public static class ReleaseFunctionalityOperation extends T8DefaultServerOperation
    {
        public ReleaseFunctionalityOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            T8FunctionalityManager functionalityManager;
            String functionalityInstanceIdentifier;

            functionalityInstanceIdentifier = (String)operationParameters.get(PARAMETER_FUNCTIONALITY_IID);

            functionalityManager = serverContext.getFunctionalityManager();
            functionalityManager.releaseFunctionality(context, functionalityInstanceIdentifier);
            return null;
        }
    }

    public static class ClearFunctionalityCacheOperation extends T8DefaultServerOperation
    {
        public ClearFunctionalityCacheOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            serverContext.getFunctionalityManager().clearCache(context);
            return null;
        }
    }

    public static class ReloadFunctionalityAccessPolicyOperation extends T8DefaultServerOperation
    {
        public ReloadFunctionalityAccessPolicyOperation(T8Context context, T8JavaServerOperationDefinition definition, String operationIid)
        {
            super(context, definition, operationIid);
        }

        @Override
        public Map<String, Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
        {
            serverContext.getFunctionalityManager().reloadAccessPolicy(context);
            return null;
        }
    }
}
