package com.pilog.t8.flow.node.activity;


import com.pilog.t8.T8CommunicationManager;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.flow.T8FlowNodeStatus;
import com.pilog.t8.flow.task.T8FlowTask;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.definition.flow.node.activity.T8TaskParameterAssignmentDefinition;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition.TaskExecutionType;
import com.pilog.t8.definition.flow.node.activity.T8TaskActivityDefinition;
import com.pilog.t8.definition.org.T8OrganizationSetupDefinition;
import com.pilog.t8.flow.key.T8TaskClaimExecutionKey;
import com.pilog.t8.flow.key.T8TaskCompletionExecutionKey;
import com.pilog.t8.flow.key.T8TaskReassignedExecutionKey;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.flow.node.activity.T8TaskActivityCommunicationDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalSettingsDefinition;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.org.operational.T8OperationalHoursCalculator;
import com.pilog.t8.org.operational.T8OrganizationOperationalHoursCalculator;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.strings.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskActivityNode extends T8DefaultFlowNode
{
    private final T8TaskActivityDefinition definition;
    private T8FlowTask internalTask;

    public enum T8TaskActivityNodeExecutionStep
    {
        TASK_CREATION,
        TASK_ISSUING,
        WAITING_FOR_TASK_CLAIM,
        WAITING_FOR_TASK_COMPLETION
    };

    public T8TaskActivityNode(T8Context context, T8Flow parentFlow, T8TaskActivityDefinition definition, String instanceId)
    {
        super(context, parentFlow, definition, instanceId);
        this.definition = definition;
        this.internalTask = null;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters) throws Exception
    {
        T8DefinitionManager definitionManager;
        Map<String, Object> taskInputParameters;
        T8FlowTaskDefinition taskDefinition;
        String taskId;

        // Load the task definition.
        definitionManager = serverContext.getDefinitionManager();
        taskId = definition.getTaskIdentifier();
        taskDefinition = (T8FlowTaskDefinition)definitionManager.getRawDefinition(null, definition.getRootProjectId(), taskId);
        if (taskDefinition == null) throw new RuntimeException("Task not found: " + taskId);

        // Compile all task input parameters.
        taskInputParameters = T8IdentifierUtilities.mapParameters(inputParameters, definition.getTaskInputParameterMapping());
        taskInputParameters.putAll(getTaskInputParameterAssignments(inputParameters));

        // Execute the task according to the type required.
        if (taskDefinition.getExecutionType() == TaskExecutionType.EXTERNAL)
        {
            T8FlowTaskState taskState = null;

            // Create the task details.
            if (!isExecutionStepComplete(T8TaskActivityNodeExecutionStep.TASK_CREATION.toString()))
            {
                // Synchronize when altering flow state.
                synchronized (flowState)
                {
                    // Create the task state.
                    taskState = createNewTaskState(taskDefinition, inputParameters, taskInputParameters);

                    // Complete the execution step.
                    completeExecutionStep(T8TaskActivityNodeExecutionStep.TASK_CREATION.toString());
                }
            }

            // If this is the first (initial issuing of the task, complete the execution step).
            if (!isExecutionStepComplete(T8TaskActivityNodeExecutionStep.TASK_ISSUING.toString()))
            {
                // Get the task state.
                if (taskState == null) taskState = nodeState.getTaskState();

                // Synchronize when altering flow state.
                synchronized (flowState)
                {
                    try
                    {
                        // Set the time of issuing.
                        taskState.setTimeIssued(new T8Timestamp(System.currentTimeMillis()));

                        // Issue the task.
                        flowController.issueTask(internalContext, taskState);

                        // Complete the execution step.
                        completeExecutionStep(T8TaskActivityNodeExecutionStep.TASK_ISSUING.toString());
                    }
                    catch (Exception e)
                    {
                        // If an exception occurs, reset the state.
                        taskState.setTimeIssued(null);
                        flowController.revokeTask(internalContext, taskState);

                        // Rethrow the exception.
                        throw e;
                    }
                }

                // Set the node communication (if one is specified).
                sendIssuedCommunications(inputParameters);
            }

            // Wait for the task to be claimed or completed.
            if (!isExecutionStepComplete(T8TaskActivityNodeExecutionStep.WAITING_FOR_TASK_CLAIM.toString()))
            {
                // Get the task state.
                if (taskState == null) taskState = nodeState.getTaskState();

                // If we have the claim key, use it or else wait for it.
                if (executionKey instanceof T8TaskClaimExecutionKey)
                {
                    // Synchronize when altering flow state.
                    synchronized (flowState)
                    {
                        T8OperationalHoursCalculator timeCalculator;

                        // Clear the wait key (if any was set).
                        nodeState.clearWaitKeys();

                        // Compute and determine times and timespans applicable.
                        timeCalculator = getOperationalHoursCalculator();
                        taskState.recalculateTimespanIssued(timeCalculator);

                        // Complete the execution step.
                        completeExecutionStep(T8TaskActivityNodeExecutionStep.WAITING_FOR_TASK_CLAIM.toString());
                    }
                }
                else if (executionKey instanceof T8TaskReassignedExecutionKey)
                {
                    // Synchronize when altering flow state.
                    synchronized (flowState)
                    {
                        // Set the user nodeId on the task details.
                        taskState.setClaimedByUserId(null);
                        taskState.setTimeClaimed(null);
                        taskState.setRestrictionUserId(((T8TaskReassignedExecutionKey)executionKey).getUserIdentifier());
                        taskState.setRestrictionProfileId(((T8TaskReassignedExecutionKey)executionKey).getProfileIdentifier());

                        // Remove execution steps that have to be redone.
                        completedExecutionSteps.remove(T8TaskActivityNodeExecutionStep.TASK_ISSUING.toString());
                        completedExecutionSteps.remove(T8TaskActivityNodeExecutionStep.WAITING_FOR_TASK_CLAIM.toString());

                        // Complete the execution step.
                        completeExecutionStep(T8TaskActivityNodeExecutionStep.TASK_ISSUING.toString());
                    }

                    return null;
                }
                else if (executionKey instanceof T8TaskCompletionExecutionKey)
                {
                    // This scenario will occur when a task is completed without first being claimed.
                    // There are no steps to perform here, we simply want to skip ahead to the completion step.
                }
                else
                {
                    // The task has not been claimed yet, so add new wait keys to wait for either a task claim or the completion of the task.
                    nodeState.addTaskClaimWaitKey(taskState.getTaskId(), taskState.getTaskIid());
                    nodeState.addTaskCompletionWaitKey(taskState.getTaskId(), taskState.getTaskIid());
                    parentFlow.persistState();
                    return null;
                }
            }

            // Wait for the task to be completed.
            if (!isExecutionStepComplete(T8TaskActivityNodeExecutionStep.WAITING_FOR_TASK_COMPLETION.toString()))
            {
                // Get the task state.
                if (taskState == null) taskState = nodeState.getTaskState();

                // If we have the completion key, use it or else wait for it.
                if (executionKey instanceof T8TaskCompletionExecutionKey)
                {
                    Map<String, Object> outputParameters;
                    String userIdOutputParameterId;
                    String profileIdOutputParameterId;

                    // Synchronize when altering flow state.
                    synchronized (flowState)
                    {
                        // Clear the wait key (if any was set).
                        nodeState.clearWaitKeys();

                        // Get the User output parameter identifiers.
                        userIdOutputParameterId = definition.getUserIdOutputParameterId();
                        profileIdOutputParameterId = definition.getProfileIdOutputParameterId();

                        // Get the claimed task details from the execution key.
                        outputParameters = ((T8TaskCompletionExecutionKey)executionKey).getTaskOutputParameters();
                        outputParameters = T8IdentifierUtilities.mapParameters(outputParameters, definition.getTaskOutputParameterMapping());
                        if (userIdOutputParameterId != null) outputParameters.put(userIdOutputParameterId, taskState.getClaimedByUserId());
                        if (profileIdOutputParameterId != null) outputParameters.put(profileIdOutputParameterId, taskState.getRestrictionProfileId());

                        // Complete the execution step.
                        completeExecutionStep(T8TaskActivityNodeExecutionStep.WAITING_FOR_TASK_COMPLETION.toString());
                    }

                    // Do output parameter assignments.
                    if (outputParameters == null) outputParameters = new HashMap<String, Object>();
                    outputParameters.putAll(getFlowParameterAssignments(outputParameters));
                    return outputParameters;
                }
                else if (executionKey instanceof T8TaskReassignedExecutionKey)
                {
                    // Synchronize when altering flow state.
                    synchronized (flowState)
                    {
                        // Clear the wait key (if any was set).
                        nodeState.clearWaitKeys();

                        // Set the user nodeId on the task details.
                        taskState.setClaimedByUserId(null);
                        taskState.setTimeClaimed(null);
                        taskState.setRestrictionUserId(((T8TaskReassignedExecutionKey)executionKey).getUserIdentifier());
                        taskState.setRestrictionProfileId(((T8TaskReassignedExecutionKey)executionKey).getProfileIdentifier());

                        // Remove execution steps that have to be redone.
                        completedExecutionSteps.remove(T8TaskActivityNodeExecutionStep.TASK_ISSUING.toString());
                        completedExecutionSteps.remove(T8TaskActivityNodeExecutionStep.WAITING_FOR_TASK_CLAIM.toString());

                        // Reset the task claim wait key and complete the execution step.
                        nodeState.addTaskClaimWaitKey(taskState.getTaskId(), taskState.getTaskIid());
                        completeExecutionStep(T8TaskActivityNodeExecutionStep.TASK_ISSUING.toString());
                    }

                    return null;
                }
                else
                {
                    // The task has not been completed yet, so set a new wait key.
                    nodeState.addTaskCompletionWaitKey(taskState.getTaskId(), taskState.getTaskIid());
                    parentFlow.persistState();
                    return null;
                }
            }

            // This point should never be reached.
            throw new Exception("Execution of an already completed task.");
        }
        else // Internal execution.
        {
            Map<String, Object> outputParameters;
            T8FlowTaskState taskState;
            T8TaskDetails taskDetails;

            // Create the task state.
            synchronized (flowState)
            {
                // Get the task state from the node or create a new task state if one has not yet been created.
                taskState = nodeState.getTaskState();
                if (taskState == null)
                {
                    taskState = createNewTaskState(taskDefinition, inputParameters, taskInputParameters);
                }
            }

            // Create a new instance of the task and executeFlow it internally.
            taskDetails = taskState.getTaskDetails(internalContext.getLanguageId(), definition, taskDefinition);
            internalTask = taskDefinition.getNewTaskInstance(taskDetails);
            outputParameters = internalTask.doTask(internalContext, parentFlow.getFlowController(), taskInputParameters);
            outputParameters = T8IdentifierUtilities.mapParameters(outputParameters, definition.getTaskOutputParameterMapping());

            // Do output parameter assignments.
            if (outputParameters == null) outputParameters = new HashMap<String, Object>();
            outputParameters.putAll(getFlowParameterAssignments(outputParameters));
            return outputParameters;
        }
    }

    private T8FlowTaskState createNewTaskState(T8FlowTaskDefinition taskDefinition, Map<String, Object> flowParameters, Map<String, Object> taskInputParameters)
    {
        String priorityParameterId;
        T8FlowTaskState taskState;
        String groupId;

        // Create the new task state.
        taskState = new T8FlowTaskState(nodeState, taskDefinition.getIdentifier(), T8IdentifierUtilities.createNewGUID());
        taskState.setGroupId(definition.getTaskGroupId());
        taskState.setRestrictionUserId((String)flowParameters.get(definition.getUserRestrictionParameterId()));
        taskState.setRestrictionProfileId(definition.getProfileIdentifier());
        taskState.setInputParameters(T8IdentifierUtilities.stripNamespace(taskDefinition.getIdentifier(), taskInputParameters, true));

        // Set the group iteration of the task.
        calculateTaskGroupIteration(taskState);

        // Set the task state priority and escalated status.
        groupId = definition.getTaskGroupId();
        priorityParameterId = definition.getTaskPriorityParameterId();
        if (groupId != null)
        {
            List<T8FlowTaskState> groupTaskStates;
            Integer maxEscalationLevel;
            T8Timestamp latest;
            int priority;

            // Get all tasks in the group.
            groupTaskStates = flowState.getTaskStatesByGroupIteration(groupId, taskState.getGroupIteration());

            // Find the latest task (according to time completed) and use its priority as input for this task.
            latest = null;
            maxEscalationLevel = null;
            priority = taskDefinition.getDefaultPriority() != null ? taskDefinition.getDefaultPriority() : T8TaskDetails.PriorityLevel.NORMAL.getPriority();
            for (T8FlowTaskState groupTaskState : groupTaskStates)
            {
                T8Timestamp timeCompleted;
                Integer escalationLevel;

                timeCompleted = groupTaskState.getTimeCompleted();
                if (timeCompleted != null)
                {
                    if ((latest == null) || (latest.compareTo(timeCompleted) < 0))
                    {
                        latest = timeCompleted;
                        priority = groupTaskState.getPriority();
                    }
                }

                escalationLevel = groupTaskState.getEscalationLevel();
                if (escalationLevel != null)
                {
                    if ((maxEscalationLevel == null) || (maxEscalationLevel.compareTo(escalationLevel) < 0))
                    {
                        maxEscalationLevel = escalationLevel;
                    }
                }
            }

            // Set the priority and elapsed escalation time of the new task state to that of the latest preceding task in the same group.
            taskState.setPriority(priority);
            taskState.setEscalationLevel(maxEscalationLevel);
        }
        else if (priorityParameterId == null)
        {
            taskState.setPriority(taskDefinition.getDefaultPriority());
        }
        else
        {
            Integer priorityValue;

            priorityValue = (Integer)flowParameters.get(priorityParameterId);
            if (priorityValue != null) taskState.setPriority(priorityValue);
            else throw new RuntimeException("Task Priority parameter '" + priorityParameterId + "' does not contain a valid integer value for task: " + definition);
        }

        // Add the task state to the node state.
        nodeState.setTaskState(taskState);
        return taskState;
    }

    /**
     * This method uses the supplied input parameters to evaluate task input
     * parameter expressions defined for this task activity.  The output of this
     * method is a map containing parameters to be used as input for the task
     * that is executed by this activity.
     */
    private Map<String, Object> getTaskInputParameterAssignments(Map<String, Object> inputParameters) throws Exception
    {
        List<T8TaskParameterAssignmentDefinition> parameterAssignmentDefinitions;
        Map<String, Object> assignedParameters;
        T8ServerExpressionEvaluator expressionEvaluator;

        expressionEvaluator = new T8ServerExpressionEvaluator(internalContext);

        assignedParameters = new HashMap<String, Object>();
        parameterAssignmentDefinitions = definition.getTaskInputParameterAssignmentDefinitions();
        for (T8TaskParameterAssignmentDefinition assignmentDefinition : parameterAssignmentDefinitions)
        {
            String expression;
            String parameterIdentifier;

            parameterIdentifier = assignmentDefinition.getParameterIdentifier();
            expression = assignmentDefinition.getValueExpression();
            if ((expression != null) && (expression.trim().length() > 0))
            {
                assignedParameters.put(parameterIdentifier, expressionEvaluator.evaluateExpression(expression, inputParameters, null));
            }
            else assignedParameters.put(parameterIdentifier, null);
        }

        return assignedParameters;
    }

    /**
     * This method used the supplied input parameters to evaluate task input
     * parameter expressions defined for this task activity.  The output of this
     * method is a map containing parameters to be used as input for the task
     * that is executed by this activity.
     */
    private Map<String, Object> getFlowParameterAssignments(Map<String, Object> outputParameters) throws Exception
    {
        Map<String, String> parameterAssignmentMap;
        Map<String, Object> assignedParameters;
        T8ServerExpressionEvaluator expressionEvaluator;

        expressionEvaluator = new T8ServerExpressionEvaluator(internalContext);

        assignedParameters = new HashMap<String, Object>();
        parameterAssignmentMap = definition.getFlowParameterAssignmentMap();
        if (parameterAssignmentMap != null)
        {
            for (String parameterIdentifier : parameterAssignmentMap.keySet())
            {
                String expression;

                expression = parameterAssignmentMap.get(parameterIdentifier);
                if ((expression != null) && (expression.trim().length() > 0))
                {
                    assignedParameters.put(parameterIdentifier, expressionEvaluator.evaluateExpression(expression, outputParameters, null));
                }
                else assignedParameters.put(parameterIdentifier, null);
            }
        }

        return assignedParameters;
    }

    private List<String> getTaskPropertyLanguageIDList() throws Exception
    {
        T8OrganizationSetupDefinition setupDefinition;
        String organizationID;

        organizationID = nodeState.getFlowState().getInitiatorOrgId();
        setupDefinition = (T8OrganizationSetupDefinition)serverContext.getDefinitionManager().getResolvedDefinition(null, T8OrganizationSetupDefinition.TYPE_IDENTIFIER, HashMaps.<String, Object>create(T8OrganizationSetupDefinition.RESOLUTION_PARAMETER_ORG_ID, organizationID));
        if (setupDefinition != null)
        {
            List<String> languageIDList;

            languageIDList = new ArrayList<String>();
            languageIDList.add(setupDefinition.getDefaultContentLanguageIdentifier());
            return languageIDList;
        }
        else throw new RuntimeException("No setup definition found for organization: " + organizationID);
    }

    @Override
    public void stopNode() throws Exception
    {
        // Set the stop flag.
        stop = true;

        // If an internal task is executing, stop it.
        if (internalTask != null)
        {
            synchronized (internalTask)
            {
                internalTask.stopTask();
            }
        }

        // Invoke the super method to make sure default behaviour is maintained.
        super.stopNode();
    }

    @Override
    public void cancelNode() throws Exception
    {
        T8FlowTaskState taskState;

        // Set the cancellation flag.
        cancel = true;

        // Get the task instance nodeId from the state.
        taskState = nodeState.getTaskState();
        if (taskState != null)
        {
            // Immediately instruct the controller to cancel the task so that it cannot be completed/claimed/unclaimed during the internal cancellation process.
            parentFlow.getFlowController().cancelTask(internalContext, taskState);
        }

        // If an internal task is executing, cancel it.
        if (internalTask != null)
        {
            synchronized (internalTask)
            {
                internalTask.cancelTask();
            }
        }

        // Invoke the super method to make sure default behaviour is maintained.
        super.cancelNode();
    }

    @Override
    public T8FlowNodeStatus getStatus()
    {
        T8FlowNodeStatus nodeStatus;

        nodeStatus = new T8FlowNodeStatus(definition.getIdentifier(), nodeState.getNodeStatus(), -1, statusMessage);
        if (internalTask != null) nodeStatus.setTaskStatus(internalTask.getStatus());
        return nodeStatus;
    }

    private void sendIssuedCommunications(Map<String, Object> nodeInputParameters)
    {
        for (T8TaskActivityCommunicationDefinition communicationDefinition : definition.getTaskIssuedCommunicationDefinitions())
        {
            String communicationIdentifier;

            // Fetch the referenced communication definition (optional).
            communicationIdentifier = communicationDefinition.getCommunicationIdentifier();
            if (communicationIdentifier != null)
            {
                try
                {
                    T8CommunicationManager communicationManager;
                    Map<String, Object> communicationParameters;
                    Map<String, String> parameterMapping;
                    Map<String, String> parameterExpressionMapping;

                    // Get the parameter mapping to use and send the communication.
                    parameterMapping = communicationDefinition.getCommunicationParameterMapping();
                    communicationParameters = parameterMapping != null ? T8IdentifierUtilities.mapParameters(nodeInputParameters, parameterMapping) : new HashMap<>();

                    // Next evaluate any parameter assignment expressions.
                    parameterExpressionMapping = communicationDefinition.getCommunicationParameterExpressionMapping();
                    if (parameterExpressionMapping != null)
                    {
                        T8ServerExpressionEvaluator expressionEvaluator;

                        expressionEvaluator = new T8ServerExpressionEvaluator(internalContext);
                        for (String parameterIdentifier : parameterExpressionMapping.keySet())
                        {
                            String expression;

                            expression = parameterExpressionMapping.get(parameterIdentifier);
                            if (!Strings.isNullOrEmpty(expression))
                            {
                                communicationParameters.put(parameterIdentifier, expressionEvaluator.evaluateExpression(expression, nodeInputParameters, null));
                            }
                            else
                            {
                                communicationParameters.put(parameterIdentifier, null);
                            }
                        }
                    }

                    // Now that all of the communication input parameters have been assembled, send the communication.
                    communicationManager = serverContext.getCommunicationManager();
                    communicationManager.sendCommunication(internalContext, communicationIdentifier, communicationParameters);
                }
                catch (Exception e)
                {
                    T8Log.log("Exception while sending task communication: " + definition, e);
                }
            }
        }
    }

    private void calculateTaskGroupIteration(T8FlowTaskState taskState)
    {
        String groupId;

        // Get the group id.
        groupId = taskState.getGroupId();
        if (groupId != null)
        {
            int maxIteration;

            // Find the max group iteration of all task states belonging to the group.
            maxIteration = 0;
            for (T8FlowTaskState groupTaskState : flowState.getTaskStatesByGroup(groupId))
            {
                // Process all task states except the input state.
                if (groupTaskState != taskState)
                {
                    int iteration;

                    iteration = groupTaskState.getGroupIteration();
                    if (iteration > maxIteration) maxIteration = iteration;
                }
            }

            // If this node is marked as a reset of the task group, increment the group iteration to indicate a new cycle.
            if ((definition.isTaskGroupReset()) || (maxIteration == 0)) // If the max iteration is 0, also increment it because a group must always start at iteration 1, even if not marked.
            {
                maxIteration++;
            }

            // Set the new task group iteration on the state.
            taskState.setGroupIteration(maxIteration);
        }
        else taskState.setGroupIteration(0);
    }

    private T8OperationalHoursCalculator getOperationalHoursCalculator() throws Exception
    {
        T8OrganizationSetupDefinition setupDefinition;
        T8DefinitionManager definitionManager;
        String operationalSettingsId;

        definitionManager = serverContext.getDefinitionManager();
        setupDefinition = T8OrganizationSetupDefinition.getOrganizationSetupDefinition(internalContext);
        operationalSettingsId = setupDefinition.getOrganizationOperationalSettingsIdentifier();
        if (operationalSettingsId != null)
        {
            T8OrganizationOperationalSettingsDefinition settingsDefinition;

            settingsDefinition = definitionManager.getInitializedDefinition(internalContext, definition.getRootProjectId(), operationalSettingsId, null);
            if (settingsDefinition != null)
            {
                return T8OrganizationOperationalHoursCalculator.getInstance(settingsDefinition);
            }
            else throw new RuntimeException("Operational settings definition not found: " + operationalSettingsId);
        }
        else return null;
    }
}
