package com.pilog.t8.data.cache;

import com.pilog.t8.cache.T8DataRequirementCache;
import com.pilog.t8.data.document.datarequirement.DataRequirement;
import javax.transaction.TransactionManager;
import org.infinispan.Cache;

/**
 * @author Bouwer du Preez
 */
public class T8InfinispanDataRequirementCache implements T8DataRequirementCache
{
    private final Cache<String, DataRequirement> cache;

    public T8InfinispanDataRequirementCache(Cache<String, DataRequirement> cache) throws Exception
    {
        this.cache = cache;
    }

    @Override
    public TransactionManager getTransactionManager()
    {
        return this.cache.getAdvancedCache().getTransactionManager();
    }

    @Override
    public void remove(Object dataRequirement)
    {
        DataRequirement dr;

        // Update the cache.
        dr = (DataRequirement)dataRequirement;
        cache.remove(dr.getConceptID());
    }

    @Override
    public DataRequirement remove(String drID)
    {
        // Update the cache.
        return cache.remove(drID);
    }

    @Override
    public void put(Object dataRequirement)
    {
        DataRequirement dr;

        // Update the cache.  Use a defensive copy to ensure cached data always remains immutable.
        dr = (DataRequirement)dataRequirement;
        cache.put(dr.getConceptID(), dr.copy());
    }

    @Override
    public DataRequirement get(String conceptID)
    {
        DataRequirement dataRequirement;

        // Use a defensive copy to ensure cached data always remains immutable.
        dataRequirement = cache.get(conceptID);
        return dataRequirement != null ? dataRequirement.copy() : null;
    }

    @Override
    public boolean containsKey(String conceptID)
    {
        return cache.containsKey(conceptID);
    }
}
