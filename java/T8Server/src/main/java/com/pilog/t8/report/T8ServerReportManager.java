package com.pilog.t8.report;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8ReportManager;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.report.T8ReportDefinition;
import com.pilog.t8.definition.system.T8SystemDefinition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.thread.T8ContextRunnable;
import com.pilog.t8.utilities.threads.NameableThreadFactory;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.process.event.T8ProcessCompletedEvent;
import com.pilog.t8.process.event.T8ProcessFailedEvent;
import com.pilog.t8.process.event.T8ProcessManagerListener;
import com.pilog.t8.report.T8ReportState.Status;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.pilog.t8.definition.report.T8ReportManagerResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8ServerReportManager implements T8ReportManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerReportManager.class);
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8ReportManagerDataHandler dataHandler;
    private final ReportGenerationProcessListener processListener;
    private final Map<String, T8ReportState> generatingReports;
    private final T8SecurityManager securityManager;
    private ScheduledExecutorService executor; // Timer used to update report details.
    private ReportMaintenanceThread maintenanceThread;
    private boolean managerEnabled;

    private final long PROCESS_MAINTENANCE_INTERVAL = 1000; // The interval in milliseconds at which the report maintenance task is run.

    public T8ServerReportManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.securityManager = serverContext.getSecurityManager();
        this.internalContext = securityManager.createServerModuleContext(T8ServerReportManager.this);
        this.dataHandler = new T8ReportManagerDataHandler(internalContext);
        this.processListener = new ReportGenerationProcessListener();
        this.generatingReports = new HashMap<>();
    }

    @Override
    public void init()
    {
        try
        {
            T8SystemDefinition systemDefinition;

            // Get the system definition to determine configuration.
            systemDefinition = serverContext.getDefinitionManager().getSystemDefinition(internalContext);
            managerEnabled = systemDefinition != null && systemDefinition.isReportManagerEnabled();

            // Start the maintenance timer.
            executor = Executors.newSingleThreadScheduledExecutor(new NameableThreadFactory("T8ServerReportManager-Maintenance"));
            maintenanceThread = new ReportMaintenanceThread(internalContext, this, dataHandler, managerEnabled);
            executor.scheduleWithFixedDelay(maintenanceThread, PROCESS_MAINTENANCE_INTERVAL, PROCESS_MAINTENANCE_INTERVAL, TimeUnit.MILLISECONDS);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while initializing report manager.", e);
        }
    }

    @Override
    public void start() throws Exception
    {
        T8ProcessManager processManager;

        // Add the process listener that will respond to report generation process lifecycle events.
        processManager = serverContext.getProcessManager();
        processManager.addEventListener(processListener);
    }

    @Override
    public void destroy()
    {
        // Terminate the timer thread and all scheduled tasks.
        try
        {
            T8ProcessManager processManager;

            // Remove the process listener.
            processManager = serverContext.getProcessManager();
            processManager.removeEventListener(processListener);

            // Shutdown the maintenance thread.
            maintenanceThread.setEnabled(false);
            executor.shutdown();
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) throw new Exception("Report maintenance thread executor could not by terminated.");
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while awaiting termination of report maintenance thread.", e);
        }
    }

    @Override
    public T8ReportDetails getReportDetails(T8Context context, String reportIid) throws Exception
    {
        T8DataTransaction tx;
        T8ReportState state;

        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        state = dataHandler.retrieveReportState(tx, reportIid);
        return createReportDetails(state);
    }

    @Override
    public T8ReportSummary getReportSummary(T8Context context) throws Exception
    {
        T8ReportFilter filter;

        filter = new T8ReportFilter();
        filter.setUserId(context.getSessionContext().getUserIdentifier());
        return dataHandler.retrieveReportSummary(context, filter);
    }

    @Override
    public int countReports(T8Context context, T8ReportFilter reportFilter, String searchExpression) throws Exception
    {
        T8ReportFilter checkedFilter;
        T8DataTransaction tx;

        // Create a verified filter that specifies the current user as criterion.
        checkedFilter = reportFilter != null ? reportFilter.copy() : new T8ReportFilter();
        checkedFilter.setUserId(context.getSessionContext().getUserIdentifier());

        // Retrieve the report states using the filter.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        return dataHandler.countReportStates(tx, checkedFilter);
    }

    @Override
    public List<T8ReportDetails> searchReports(T8Context context, T8ReportFilter reportFilter, String searchExpression, int pageOffset, int pageSize) throws Exception
    {
        T8ReportFilter checkedFilter;
        List<T8ReportState> states;
        List<T8ReportDetails> reportList;
        T8DataTransaction tx;

        // Create a verified filter that specifies the current user as criterion.
        checkedFilter = reportFilter != null ? reportFilter.copy() : new T8ReportFilter();
        checkedFilter.setUserId(context.getSessionContext().getUserIdentifier());

        // Retrieve the report states using the filter.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        states = dataHandler.retrieveReportStates(tx, checkedFilter, searchExpression, pageOffset, pageSize);

        // Create the report list from the retrieve states.
        reportList = new ArrayList<>();
        for (T8ReportState state : states)
        {
            reportList.add(createReportDetails(state));
        }

        return reportList;
    }

    @Override
    public boolean deleteReport(T8Context context, String reportIid) throws Exception
    {
        T8DataTransaction tx;

        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
        return dataHandler.deleteState(tx, reportIid);
    }

    private T8ReportDetails createReportDetails(T8ReportState state)
    {
        if (state != null)
        {
            T8ReportDetails details;

            details = new T8ReportDetails(state.getProjectId(), state.getId(), state.getIid());
            details.setName(state.getName());
            details.setDescription(state.getDescription());
            details.setInitiatorId(state.getInitiatorId());
            details.setInitiatorIid(state.getInitiatorIid());
            details.setUserId(state.getUserId());
            details.setStartTime(state.getStartTime());
            details.setEndTime(state.getEndTime());
            details.setExpirationTime(state.getExpirationTime());
            details.setFileContextId(state.getFileContextId());
            details.setFilePath(state.getFilePath());

            try
            {
                T8ReportDefinition reportDefinition;

                reportDefinition = serverContext.getDefinitionManager().getInitializedDefinition(internalContext, state.getProjectId(), state.getId(), null);
                if (reportDefinition != null)
                {
                    details.setThumbnailImageId(reportDefinition.getThumbnailImageId());
                }
            }
            catch (Exception e)
            {
                LOGGER.log("Exception while retrieving report definition: " + state.getId(), e);
            }

            return details;
        }
        else return null;
    }

    @Override
    public String generateReport(T8Context context, String reportId, Map<String, Object> inputParameters) throws Exception
    {
        T8ReportDefinition reportDefinition;

        // Retrieve the definition of the report to be generated.
        reportDefinition = serverContext.getDefinitionManager().getInitializedDefinition(context, context.getProjectId(), reportId, inputParameters);
        if (reportDefinition != null)
        {
            T8DefinitionManager definitionManager;
            T8SystemDefinition systemDefinition;
            String fileContextId;

            // Get the system definition to find the configured file context id for reports.
            definitionManager = serverContext.getDefinitionManager();
            systemDefinition = definitionManager.getSystemDefinition(context);
            fileContextId = systemDefinition.getReportFileContextId();
            if (fileContextId != null)
            {
                T8ReportDescription reportDescription;
                T8ProcessManager processManager;
                T8ProcessDetails processDetails;
                T8SessionContext sessionContext;
                T8ReportGenerator generator;
                T8ReportState reportState;
                T8DataTransaction tx;
                String reportIid;

                // Get the details of the report to be generated.
                reportIid = T8IdentifierUtilities.createNewGUID();
                generator = reportDefinition.getReportGenerator(context);
                reportDescription = generator.generateReportDescription(reportIid, inputParameters);
                sessionContext = context.getSessionContext();

                // Create the preliminary state of the new report.
                reportState = new T8ReportState(context.getProjectId(), reportId, reportIid);
                reportState.setInitiatorId(sessionContext.isSystemSession() ? sessionContext.getSystemAgentIdentifier() : sessionContext.getUserIdentifier());
                reportState.setInitiatorIid(sessionContext.isSystemSession() ? sessionContext.getSystemAgentInstanceIdentifier() : null);
                reportState.setUserId(sessionContext.getUserIdentifier());
                reportState.setStartTime(new T8Timestamp(System.currentTimeMillis()));
                reportState.setName(reportDescription.getName());
                reportState.setDescription(reportDescription.getDescription());
                reportState.setStatus(Status.STARTED);
                reportState.setParameters(inputParameters);

                // Start the process that will generate the report.
                synchronized (generatingReports) // We have synchronize on the colletion to ensure that it is not check by an asynchronous process failure listener while still being updated.
                {
                    Map<String, Object> processParameters;
                    String directory;

                    // Store the output report in 'n folder that is named according to the report iid in order to prevent filename clashes.
                    directory = reportIid;

                    processParameters = new HashMap<>();
                    processParameters.put(PROCESS_GENERATE_REPORT + PARAMETER_PROJECT_ID, context.getProjectId());
                    processParameters.put(PROCESS_GENERATE_REPORT + PARAMETER_REPORT_IID, reportIid);
                    processParameters.put(PROCESS_GENERATE_REPORT + PARAMETER_REPORT_ID, reportId);
                    processParameters.put(PROCESS_GENERATE_REPORT + PARAMETER_FILE_CONTEXT_ID, fileContextId);
                    processParameters.put(PROCESS_GENERATE_REPORT + PARAMETER_DIRECTORY, directory);
                    processParameters.put(PROCESS_GENERATE_REPORT + PARAMETER_REPORT_PARAMETERS, inputParameters);
                    processManager = serverContext.getProcessManager();
                    processDetails = processManager.startProcess(context, PROCESS_GENERATE_REPORT, processParameters);

                    // Add the state of the report to the collection of reorts being generated.
                    generatingReports.put(processDetails.getProcessIid(), reportState);
                }

                // Insert the history entry.
                tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
                dataHandler.insertHistoryEntry(tx, reportState);

                // Return the iid of the process handling the report generation.
                return processDetails.getProcessIid();
            }
            else throw new Exception("No report file context is defined in system configuration.");
        }
        else throw new Exception("Report not found: " + reportId);
    }

    private void handleReportGenerationCompletion(T8ReportState reportState, String fileContextId, String filepath)
    {
        try
        {
            T8DataTransaction tx;

            // Update the state with details of the completion.
            reportState.setEndTime(new T8Timestamp(System.currentTimeMillis()));
            reportState.setStatus(Status.COMPLETED);
            reportState.setFileContextId(fileContextId);
            reportState.setFilePath(filepath);

            // Issue the report by adding it to the state table.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
            dataHandler.insertState(tx, reportState);

            // Log the history event.
            dataHandler.updateHistoryEntry(tx, reportState);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while handling report generation completion: " + reportState, e);
        }
    }

    private void handleReportGenerationFailure(T8ReportState reportState)
    {
        try
        {
            T8DataTransaction tx;

            // Update the state with details of the completion.
            reportState.setEndTime(new T8Timestamp(System.currentTimeMillis()));
            reportState.setStatus(Status.FAILED);

            // Log the history event.
            tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
            dataHandler.updateHistoryEntry(tx, reportState);
        }
        catch (Exception e)
        {
            LOGGER.log("Exception while handling report generation completion: " + reportState, e);
        }
    }

    private class ReportGenerationProcessListener implements T8ProcessManagerListener
    {
        @Override
        public void processCompleted(T8ProcessCompletedEvent event)
        {
            T8ProcessDetails processDetails;

            processDetails = event.getProcessDetails();
            if (processDetails.getProcessId().equals(PROCESS_GENERATE_REPORT))
            {
                new Thread(new T8ContextRunnable(internalContext, ReportGenerationProcessListener.class.getName())
                {
                    @Override
                    public void exec()
                    {
                        // Synchronize on the collection to prevent updates while it is checked and vice versa.
                        synchronized (generatingReports)
                        {
                            T8ReportState reportState;
                            String processIid;

                            processIid = processDetails.getProcessIid();
                            reportState = generatingReports.remove(processIid);
                            if (reportState != null)
                            {
                                Map<String, Object> outputParameters;
                                String fileContextId;
                                String filepath;

                                outputParameters = event.getOutputParameters();
                                fileContextId = (String)outputParameters.get(PROCESS_GENERATE_REPORT + PARAMETER_FILE_CONTEXT_ID);
                                filepath = (String)outputParameters.get(PROCESS_GENERATE_REPORT + PARAMETER_FILE_PATH);
                                handleReportGenerationCompletion(reportState, fileContextId, filepath);
                            }
                            else LOGGER.log("WARNING: Report generation process " + processIid + " completed but now corresponding report was found.");
                        }
                    }
                }).start();
            }
        }

        @Override
        public void processFailed(T8ProcessFailedEvent event)
        {
            T8ProcessDetails processDetails;

            processDetails = event.getProcessDetails();
            if (processDetails.getProcessId().equals(PROCESS_GENERATE_REPORT))
            {
                new Thread(new T8ContextRunnable(internalContext, ReportGenerationProcessListener.class.getName())
                {
                    @Override
                    public void exec()
                    {
                        // Synchronize on the collection to prevent updates while it is checked and vice versa.
                        synchronized (generatingReports)
                        {
                            T8ReportState reportState;
                            String processIid;

                            processIid = processDetails.getProcessIid();
                            reportState = generatingReports.remove(processIid);
                            if (reportState != null)
                            {
                                handleReportGenerationFailure(reportState);
                            }
                            else LOGGER.log("WARNING: Report generation process " + processIid + " failed but now corresponding report was found.");
                        }
                    }
                }).start();
            }
        }
    }

    private static class ReportMaintenanceThread extends T8ContextRunnable
    {
        private final T8ServerReportManager reportManager;
        private final T8ReportManagerDataHandler stateHandler;
        private boolean enabled;

        private ReportMaintenanceThread(T8Context context, T8ServerReportManager reportManager, T8ReportManagerDataHandler stateHandler, boolean enabled) throws Exception
        {
            super(context, "TaskReprioritizationThread");
            this.reportManager = reportManager;
            this.stateHandler = stateHandler;
            this.enabled = enabled;
        }

        public void setEnabled(boolean enabled) throws Exception
        {
            this.enabled = enabled;
        }

        @Override
        public void exec()
        {
            if (enabled)
            {
                try
                {
                    T8DataTransaction tx;
                    List<String> expiredReportIids;

                    // Retrieve iids of all reports that have expired.
                    tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
                    expiredReportIids = stateHandler.retrieveExpiredReportIids(tx, 50);
                    for (String expiredReportIid : expiredReportIids)
                    {
                        try
                        {
                            reportManager.deleteReport(context, expiredReportIid);
                        }
                        catch (Exception e)
                        {
                            LOGGER.log("Exception while deleting report: " + expiredReportIid, e);
                        }
                    }
                }
                catch (Throwable e)
                {
                    LOGGER.log("Exception during deletion of expired reports.", e);
                }
            }
        }
    }
}
