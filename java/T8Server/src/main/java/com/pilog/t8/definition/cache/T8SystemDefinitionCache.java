package com.pilog.t8.definition.cache;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.T8L2DefinitionCache;
import com.pilog.t8.definition.language.T8LanguageDefinition;
import com.pilog.t8.definition.language.T8UITranslationDefinition;
import com.pilog.t8.definition.org.operational.T8OrganizationOperationalSettingsDefinition;
import com.pilog.t8.definition.user.T8AdministratorUserDefinition;
import com.pilog.t8.definition.user.T8ClientUserDefinition;
import com.pilog.t8.definition.user.T8DeveloperUserDefinition;
import static com.pilog.t8.definition.user.T8UserResourceDefinitions.USER_CLIENT_ENTITY_IDENTIFIER;
import com.pilog.t8.definition.user.property.T8UserPropertyDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.ArrayLists;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bouwer du Preez
 */
public class T8SystemDefinitionCache extends T8CompositeDefinitionCache
{
    private String systemId;
    private File contextPath;
    private boolean initialized;

    public T8SystemDefinitionCache(T8Context context, T8DefinitionManager definitionManager, File contextPath)
    {
        super(context, definitionManager);
        this.contextPath = contextPath;
        this.initialized = false;
    }

    /**
     * If this method is invoked before the cache has been initialized, the
     * local variable will simply be set.  If invoked after cache initialization
     * this method will rename the system folder.
     * @param identifier
     * @throws java.lang.Exception
     */
    public void setSystemIdentifier(String identifier) throws Exception
    {
        if (initialized)
        {
            File currentContextPath;
            File newContextPath;

            // Change the system identifier.
            T8Log.log("Changing system definition cache identifier from '" + systemId + "' to '" + identifier + "'...");
            currentContextPath = getSystemContextPath();
            this.systemId = identifier;
            newContextPath = getSystemContextPath();

            // Rename the system folder.
            T8Log.log("Renaming system folder '" + currentContextPath + "' to '" + newContextPath + "'...");
            currentContextPath.renameTo(newContextPath);

            // Make sure the old system folder has been deleted.
            if (currentContextPath.exists()) currentContextPath.delete();

            // Reset the context path on definition caches.
            resetContextPath();
        }
        else
        {
            this.systemId = identifier;
        }
    }

    public String getSystemIdentifier()
    {
        return systemId;
    }

    @Override
    public void init() throws Exception
    {
        T8FileDefinitionCache fileCache;
        T8EntityDefinitionCache entityCache;
        File systemContextPath;
        String systemFolderName;
        Set<String> typeIds;

        // Create the context path for all system definitions.
        systemFolderName = systemId.substring(1).toLowerCase();
        systemContextPath = new File(contextPath.getAbsolutePath() + "/" + systemFolderName);

        // Add a file cache for system definitions.
        typeIds = new HashSet<>();
        typeIds.add(T8LanguageDefinition.TYPE_IDENTIFIER);
        typeIds.add(T8OrganizationOperationalSettingsDefinition.TYPE_IDENTIFIER);
        typeIds.add(T8DeveloperUserDefinition.TYPE_IDENTIFIER);
        typeIds.add(T8AdministratorUserDefinition.TYPE_IDENTIFIER);
        typeIds.add(T8UserPropertyDefinition.TYPE_IDENTIFIER);
        typeIds.add(T8UITranslationDefinition.TYPE_IDENTIFIER);
        fileCache = new T8FileDefinitionCache(context, definitionManager, systemContextPath, systemId, typeIds);
        addDefinitionCache(fileCache);
        fileCache.init();

        // Add an entity cache for system users.
        entityCache = new T8EntityDefinitionCache(context, definitionManager, USER_CLIENT_ENTITY_IDENTIFIER, ArrayLists.newArrayList(T8ClientUserDefinition.TYPE_IDENTIFIER));
        addDefinitionCache(entityCache);
        entityCache.init();

        // Now do the normal super initialization method.
        super.init();

        // Set the initialized flag.
        initialized = true;
    }

    @Override
    public void setContextPath(File contextPath) throws Exception
    {
        // Update the local context path variable.
        this.contextPath = contextPath;
        resetContextPath();
    }

    @Override
    public T8Definition loadDefinition(String identifier) throws Exception
    {
        T8Definition loadedDefinition;

        // Make sure the loaded definition's system identifier is set.
        loadedDefinition = super.loadDefinition(identifier);
        return loadedDefinition;
    }

    @Override
    public void saveDefinition(T8Definition definition) throws Exception
    {
        // Make sure the definition has no project identifier.
        definition.setProjectIdentifier(null);

        // Now save the definition.
        super.saveDefinition(definition);
    }

    public File getSystemContextPath()
    {
        String systemFolderName;

        // Create the context path for all system definitions.
        systemFolderName = systemId.substring(1).toLowerCase();
        return new File(contextPath.getAbsolutePath() + "/" + systemFolderName);
    }

    public void resetContextPath() throws Exception
    {
        File systemContextPath;

        // Set the context path of all the constituent definition caches.
        systemContextPath = getSystemContextPath();
        for (T8L2DefinitionCache definitionCache : definitionCaches)
        {
            definitionCache.setContextPath(systemContextPath);
        }
    }
}
