package com.pilog.t8.functionality;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.functionality.T8TaskFunctionalityDefinition;
import com.pilog.t8.definition.functionality.T8WorkflowFunctionalityDefinition;
import com.pilog.t8.definition.ng.T8NgComponentDefinition;
import com.pilog.t8.definition.ng.T8NgComponentInitializationScriptDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8TaskFunctionalityInstance extends T8DefaultFunctionalityInstance implements T8FunctionalityInstance
{
    private final T8TaskFunctionalityDefinition definition;
    private final String componentId;
    private String flowIid;
    private String taskIid;
    private Map<String, Object> inputParameters;

    public T8TaskFunctionalityInstance(T8Context context, T8TaskFunctionalityDefinition definition)
    {
        super(context, definition, T8IdentifierUtilities.createNewGUID());
        this.definition = definition;
        this.componentId = definition.getComponentId();
    }

    public T8TaskFunctionalityInstance(T8Context context, T8TaskFunctionalityDefinition definition, T8FunctionalityState state)
    {
        super(context, definition, state.getFunctionalityIid());
        this.definition = definition;
        this.componentId = definition.getComponentId();
        this.flowIid = state.getFlowIid();
    }

    @Override
    public T8FunctionalityDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void initialize()
    {
    }

    @Override
    public boolean release()
    {
        return true;
    }

    @Override
    public T8FunctionalityAccessHandle access(Map<String, Object> parameters) throws Exception
    {
        T8TaskFunctionalityAccessHandle handle;
        String projectId;

        // Store the input parameters.
        this.timeAccessed = new T8Timestamp(System.currentTimeMillis());
        this.inputParameters = parameters;
        this.taskIid = parameters != null ? (String)parameters.get(id + "$P_TASK_IID") : null;

        // Create the access handle to the flow.
        handle = new T8TaskFunctionalityAccessHandle(definition.getIdentifier(), iid, getDisplayName(), getDescription(), getIcon());
        handle.setFlowIid(flowIid);
        handle.setTaskIid(taskIid);
        handle.setIconUri(definition.getIconUri());

        // Set the component details (if a component is specified).
        if (componentId != null)
        {
            T8NgComponentDefinition componentDefinition;

            projectId = definition.getRootProjectId();
            componentDefinition = (T8NgComponentDefinition)serverContext.getDefinitionManager().getInitializedDefinition(context, projectId, componentId, null);
            if (componentDefinition != null)
            {
                Map<String, Object> initializationParameters;
                Map<String, Object> componentParameters;

                // Get the component parameters.
                initializationParameters = getComponentInputParameters(parameters);

                // Get the component parameters from the script output.
                initializationParameters = componentDefinition.executeComponentInitializationScript(context, initializationParameters);
                componentParameters = (Map<String, Object>)initializationParameters.get(T8NgComponentInitializationScriptDefinition.PARAMTER_COMPONENT_PARAMETERS);

                // Return the component details on the handle.
                handle.setComponentUri(componentDefinition.getComponentUri());
                handle.setComponentParameters(componentParameters);
            }
            else throw new Exception("Task component definition not found: " + componentId + " as specified in definition: " + definition);
        }

        // Return the final access handle.
        return handle;
    }

    @Override
    public boolean stop()
    {
        return true;
    }

    @Override
    public T8FunctionalityState getFunctionalityState()
    {
        T8FunctionalityState state;

        // Create the new state.
        state = super.getFunctionalityState(inputParameters);

        // Add the flow instance id so that this functionality can be removed when the flow ends.
        state.setFlowIid(flowIid);

        // Return the state.
        return state;
    }

    private Map<String, Object> getComponentInputParameters(Map<String, Object> functionalityParameters) throws Exception
    {
        Map<String, Object> componentParameters;

        componentParameters = T8IdentifierUtilities.stripNamespace(id, functionalityParameters, true);
        componentParameters = T8IdentifierUtilities.mapParameters(componentParameters, definition.getComponentParameterMapping());
        componentParameters.putAll(evaluateComponentParameterExpressions());
        return componentParameters;
    }

    private Map<String, Object> evaluateComponentParameterExpressions() throws Exception
    {
        Map<String, String> expressionMap;
        Map<String, Object> resultMap;

        resultMap = new HashMap<String, Object>();
        expressionMap = definition.getComponentParameterExpressionMap();
        if ((expressionMap != null) && (expressionMap.size() > 0))
        {
            HashMap<String, Object> expressionParameters;

            // Create a map of expression parameters.
            expressionParameters = new HashMap<String, Object>();
            expressionParameters.put(T8WorkflowFunctionalityDefinition.PARAMETER_FLOW_IID, flowIid);

            // Construct a new expression evaluator to use.
            for (String parameterId : expressionMap.keySet())
            {
                String expression;
                Object result;

                expression = expressionMap.get(parameterId);
                result = expressionEvaluator.evaluateExpression(expression, expressionParameters, null);
                resultMap.put(parameterId, result);
            }
        }

        return resultMap;
    }
}
