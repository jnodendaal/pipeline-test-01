package com.pilog.t8.operation.entity;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.operation.entity.T8EntityServerOperationDefinition;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Pieter Strydom
 */
public class T8ServerEntityOperation implements T8ServerOperation
{
    protected final T8ServerContext serverContext;
    protected final T8Context context;
    protected final T8EntityServerOperationDefinition definition;
    protected T8DataTransaction tx;

    public T8ServerEntityOperation(T8Context context, T8EntityServerOperationDefinition definition, String operationInstanceIdentifier)
    {
        this.serverContext = context.getServerContext();
        this.definition = definition;
        this.context = context;
    }

    @Override
    public void init()
    {
        this.tx = serverContext.getDataManager().getCurrentSession().instantTransaction();
    }

    @Override
    public double getProgress(T8Context context)
    {
        return -1;
    }

    @Override
    public T8ProgressReport getProgressReport(T8Context context)
    {
        return null;
    }

    @Override
    public Map<String, ? extends Object> execute(Map<String, Object> operationParameters, T8ServerOperationExecutionType executionType) throws Exception
    {
        T8DataFilter filter;
        Integer pageSize;
        Integer pageOffset;
        String dataEntityId;
        List<T8DataEntity> entityList;
        HashMap<String,List> returnMap;

        // Get the nessacary data from the input paramater map.
        filter = (T8DataFilter)operationParameters.get("$P_DATA_FILTER");
        pageSize = (Integer)operationParameters.get("$P_PAGE_SIZE");
        pageOffset = (Integer)operationParameters.get("$P_PAGE_OFFSET");

        // Get the entity identifier, that will be used to retrieve from.
        dataEntityId = this.definition.getDataEntityIdentifier();

        // Retrieve the data.
        if(pageSize != null && pageOffset != null)
        {
            entityList = tx.select(dataEntityId, filter, pageOffset, pageSize);
        }
        else
        {
            entityList = tx.select(dataEntityId, filter);
        }

        // Create the return map.
        returnMap = new HashMap<>();
        returnMap.put("$P_ENTITY_LIST",entityList);

        return returnMap;
    }

    @Override
    public void stop(T8Context context)
    {
    }

    @Override
    public void cancel(T8Context context)
    {
    }

    @Override
    public void pause(T8Context context)
    {
    }

    @Override
    public void resume(T8Context context)
    {
    }
}
