package com.pilog.t8.data.source.sequence;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataSource;
import com.pilog.t8.data.type.T8DataType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.source.T8CommonStatementHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.sequence.T8SequenceDataSourceDefinition;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8SequenceDataSource implements T8DataSource
{
    protected final T8DataTransaction tx;
    protected final T8SequenceDataSourceDefinition definition;
    protected final T8Context context;
    protected T8DataConnection connection;
    protected T8PerformanceStatistics stats;

    public T8SequenceDataSource(T8SequenceDataSourceDefinition definition, T8DataTransaction tx)
    {
        this.definition = definition;
        this.tx = tx;
        this.context = tx.getContext();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.
    }

    @Override
    public T8DataSourceDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    @Override
    public void open() throws Exception
    {
        connection = tx.getDataConnection(definition.getConnectionIdentifier());
    }

    @Override
    public void close() throws Exception
    {
    }

    @Override
    public void commit() throws Exception
    {
        if (connection != null) connection.commit();
    }

    @Override
    public void rollback() throws Exception
    {
        if (connection != null) connection.rollback();
    }

    @Override
    public void setParameters(Map<String, Object> parameters)
    {
    }

    @Override
    public List<T8DataSourceFieldDefinition> retrieveFieldDefinitions() throws Exception
    {
        ArrayList<T8DataSourceFieldDefinition> definitionList;

        // The field definitions are constants and do not need to be retrieved.
        definitionList = new ArrayList<T8DataSourceFieldDefinition>();
        definitionList.add(new T8DataSourceFieldDefinition(T8SequenceDataSourceDefinition.SEQUENCE_FIELD_IDENTIFIER, "SEQUENCE", false, true, T8DataType.INTEGER));
        return definitionList;
    }

    @Override
    public int count(T8DataFilter filter) throws Exception
    {
        throw new Exception("Cannot perform count operation on Sequence Data Source.");
    }

    @Override
    public void insert(T8DataEntity tdo) throws Exception
    {
        throw new Exception("Cannot perform insert operation on Sequence Data Source.");
    }

    @Override
    public void insert(List<T8DataEntity> tdo) throws Exception
    {
        throw new Exception("Cannot perform insert operation on Sequence Data Source.");
    }

    @Override
    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> keyValues) throws Exception
    {
        ParameterizedString queryString;
        ArrayList<T8DataEntity> selectedObjects;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);
        queryString = new ParameterizedString(connection.getDatabaseAdaptor().getSequenceNextValueQuery(this.connection, definition.getSequenceName()));
        selectedObjects = T8CommonStatementHandler.executeQuery(context, connection, queryString, definition, entityDefinition, 0, 1, 0);
        if (selectedObjects.size() > 0)
        {
            return selectedObjects.get(0);
        }
        else return null;
    }

    @Override
    public boolean update(T8DataEntity tdo) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean update(List<T8DataEntity> tdo) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int update(String entityIdentifier, Map<String, Object> updatedValues, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean delete(T8DataEntity tdo) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        return select(entityIdentifier, filter, 0, 50000);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap, int offset, int pageSize) throws Exception
    {
        return select(entityIdentifier, keyMap != null ? new T8DataFilter(entityIdentifier, keyMap) : null, offset, pageSize);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter, int offset, int pageSize) throws Exception
    {
        throw new UnsupportedOperationException("Cannot perform operation on Sequence Data Source.");
    }

    @Override
    public T8DataEntityResults scroll(String string, T8DataFilter tdf) throws Exception
    {
        throw new UnsupportedOperationException("Cannot perform operation on Sequence Data Source.");
    }

    @Override
    public int delete(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("Cannot perform delete operation on Sequence Data Source.");
    }

    @Override
    public boolean exists(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int delete(List<T8DataEntity> dataEntities) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
