package com.pilog.t8.api;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ProcessManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilterExpressionParser;
import com.pilog.t8.data.filter.expression.T8SearchExpressionParseException;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.process.T8ProcessFilter;
import com.pilog.t8.process.T8ProcessSummary;
import com.pilog.t8.security.T8Context;
import java.util.List;

/**
 * @author Bouwer du Preez
 */
public class T8ProcessApi implements T8Api, T8PerformanceStatisticsProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ProcessApi.class);

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8ProcessManager processManager;
    private T8PerformanceStatistics stats;

    public static final String API_IDENTIFIER = "@API_PROCESS";

    public T8ProcessApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.processManager = serverContext.getProcessManager();
        this.stats = tx.getPerformanceStatistics();
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    public List<String> parseSearchTerms(String searchExpression) throws T8SearchExpressionParseException
    {
        T8DataFilterExpressionParser parser;

        parser = new T8DataFilterExpressionParser(null, null);
        return parser.parseExpressionSearchTerms(searchExpression);
    }

    public T8ProcessSummary getProcessSummary() throws Exception
    {
        return processManager.getProcessSummary(context);
    }

    public T8ProcessDetails getProcessDetails(String processIid) throws Exception
    {
        return processManager.getProcessDetails(context, processIid);
    }

    public int countProcesses(T8ProcessFilter processFilter, String searchExpression) throws Exception
    {
        return processManager.countProcesses(context, processFilter, searchExpression);
    }

    public List<T8ProcessDetails> searchProcesses(T8ProcessFilter processFilter, String searchExpression, int pageOffset, int pageSize) throws Exception
    {
        return processManager.searchProcesses(context, processFilter, searchExpression, pageOffset, pageSize);
    }
}
