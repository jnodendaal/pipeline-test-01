package com.pilog.t8.operation.script;

import com.pilog.t8.definition.operation.script.T8SoapServiceOperationDefinition;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8SoapServiceOperation extends T8ServerScriptOperation
{
    private final T8SoapServiceOperationDefinition definition;

    public T8SoapServiceOperation(T8Context context, T8SoapServiceOperationDefinition definition, String operationIid)
    {
        super(context, definition, operationIid);
        this.definition = definition;
    }
}
