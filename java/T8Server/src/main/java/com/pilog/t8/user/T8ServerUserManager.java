package com.pilog.t8.user;

import com.pilog.t8.T8FileManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8UserManager;
import com.pilog.t8.T8UserManagerConfiguration;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Minute;
import com.pilog.t8.user.event.T8UserManagerListener;
import com.pilog.t8.user.event.T8UserProfilePictureUpdatedEvent;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

/**
 * @author Bouwer du Preez
 */
public class T8ServerUserManager implements T8UserManager
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerUserManager.class);

    private final T8ServerContext serverContext;
    private final List<T8UserManagerListener> listeners;
    private T8UserManagerConfiguration configuration;

    public static final int PROFILE_PICTURE_WIDTH = 800;
    public static final int PROFILE_PICTURE_HEIGHT = 800;
    public static final int PROFILE_THUMBNAIL_WIDTH = 70;
    public static final int PROFILE_THUMBNAIL_HEIGHT = 70;
    public static final String PROFILE_PICTURE_FILENAME = "profile_picture.jpg";
    public static final String PROFILE_THUMBNAIL_FILENAME = "profile_thumbnail.jpg";

    public T8ServerUserManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.listeners = new ArrayList<>();
    }

    @Override
    public void init()
    {
        this.configuration = serverContext.getConfigurationManager().getUserManagerConfiguration();
    }

    @Override
    public void start() throws Exception
    {
        // No start-up routine for this module.
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public void addUserManagerListener(T8UserManagerListener listener)
    {
        this.listeners.add(listener);
    }

    @Override
    public void removeUserManagerListener(T8UserManagerListener listener)
    {
        this.listeners.remove(listener);
    }

    @Override
    public void updateUserProfilePicture(T8Context context, String fileContextIid, String filePath) throws Exception
    {
        String userDataFileContextId;

        userDataFileContextId = configuration.getUserDataFileContextId();
        if (userDataFileContextId != null)
        {
            InputStream inputStream = null;
            OutputStream pictureOutputStream = null;
            OutputStream thumbnailOutputStream = null;
            T8FileManager fileManager;
            String outputContextIid;
            String outputFolderPath;
            String outputFolderUrl;

            // Get the file manager to use for file access.
            fileManager = serverContext.getFileManager();
            outputContextIid = T8IdentifierUtilities.createNewGUID();
            outputFolderPath = getUserProfilePicturePath(context.getSessionContext().getUserIdentifier());
            outputFolderUrl = "/" + userDataFileContextId + outputFolderPath;

            try
            {
                Image profileImage;
                Image thumbnailImage;
                BufferedImage inputImage;
                BufferedImage profilePicture;
                BufferedImage profileThumbnail;

                // Read the new input image from the specified file context.
                inputStream = fileManager.getFileInputStream(context, fileContextIid, filePath);
                inputImage = ImageIO.read(inputStream);

                // Scale the image to the correct size for the profile picture.
                profileImage = inputImage.getScaledInstance(PROFILE_PICTURE_WIDTH, PROFILE_PICTURE_HEIGHT, Image.SCALE_SMOOTH);
                profilePicture = new BufferedImage(PROFILE_PICTURE_WIDTH, PROFILE_PICTURE_HEIGHT, BufferedImage.TYPE_INT_RGB);
                profilePicture.getGraphics().drawImage(profileImage, 0, 0, null);

                // Scale the image to the correct size for the profile thumbnail.
                thumbnailImage = inputImage.getScaledInstance(PROFILE_THUMBNAIL_WIDTH, PROFILE_THUMBNAIL_HEIGHT, Image.SCALE_SMOOTH);
                profileThumbnail = new BufferedImage(PROFILE_THUMBNAIL_WIDTH, PROFILE_THUMBNAIL_HEIGHT, BufferedImage.TYPE_INT_RGB);
                profileThumbnail.getGraphics().drawImage(thumbnailImage, 0, 0, null);

                // Open the output file context.
                fileManager.openFileContext(context, outputContextIid, userDataFileContextId, new T8Minute(1));
                fileManager.createDirectory(context, fileContextIid, outputFolderPath);

                // Write the picture image.
                pictureOutputStream = fileManager.getFileOutputStream(context, outputContextIid, outputFolderPath + "/" + PROFILE_PICTURE_FILENAME, false);
                ImageIO.write(profilePicture, "jpeg", pictureOutputStream);
                pictureOutputStream.close();

                // Write the thumbnail image.
                thumbnailOutputStream = fileManager.getFileOutputStream(context, outputContextIid, outputFolderPath + "/" + PROFILE_THUMBNAIL_FILENAME, false);
                ImageIO.write(profileThumbnail, "jpeg", thumbnailOutputStream);
                thumbnailOutputStream.close();

                // Fire the event to notify observers of the update.
                fireUserProfilePictureUpdatedEvent(outputFolderUrl + "/" + PROFILE_PICTURE_FILENAME, outputFolderUrl + "/" + PROFILE_THUMBNAIL_FILENAME);
            }
            finally
            {
                if (inputStream != null) inputStream.close();
                if (pictureOutputStream != null) pictureOutputStream.close();
                if (thumbnailOutputStream != null) thumbnailOutputStream.close();
                if (fileManager.fileContextExists(context, outputContextIid)) fileManager.closeFileContext(context, outputContextIid);
            }
        }
        else throw new Exception("No user data file context is configured.");
    }

    private String getUserProfilePicturePath(String userId)
    {
        return "/" + userId;
    }

    private void fireUserProfilePictureUpdatedEvent(String pictureUrl, String thumbnailUrl)
    {
        T8UserProfilePictureUpdatedEvent event;

        event = new T8UserProfilePictureUpdatedEvent(pictureUrl, thumbnailUrl);
        for (T8UserManagerListener listener : listeners)
        {
            listener.userProfilePictureUpdated(event);
        }
    }
}
