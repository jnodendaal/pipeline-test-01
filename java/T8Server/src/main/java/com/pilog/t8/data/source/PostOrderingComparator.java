/**
 * Created on 15 Dec 2015, 8:36:40 AM
 */
package com.pilog.t8.data.source;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author Gavin Boshoff
 */
public class PostOrderingComparator implements Comparator<T8DataEntity>
{
    private static final T8Logger logger = T8Log.getLogger(PostOrderingComparator.class);

    public static final byte ASCENDING = 0;
    public static final byte DESCENDING = 1;

    public static PostOrderingComparator build(T8DataFilter dataFilter, T8DataEntityDefinition entityDefinition, T8DataSourceDefinition dataSourceDefinition, boolean nullFirst)
    {
        if (dataFilter == null || !dataFilter.hasFieldOrdering()) return null;
        PostOrderingComparator postOrderingComparator;
        String entityFieldIdentifier;
        String fieldSourceIdentifier;

        postOrderingComparator = new PostOrderingComparator(nullFirst);
        for (Map.Entry<String, T8DataFilter.OrderMethod> entry : dataFilter.getFieldOrdering().entrySet())
        {
            entityFieldIdentifier = entry.getKey();

            fieldSourceIdentifier = dataSourceDefinition.mapFieldToSourceIdentifier(entityDefinition.mapFieldToSourceIdentifier(entityFieldIdentifier));
            if (entry.getValue() == T8DataFilter.OrderMethod.ASCENDING)
            {
                postOrderingComparator.addOrderingField(entityFieldIdentifier, fieldSourceIdentifier, ASCENDING);
            } else postOrderingComparator.addOrderingField(entityFieldIdentifier, fieldSourceIdentifier, DESCENDING);
        }

        return postOrderingComparator;
    }

    private final List<OrderingField> orderingFields;
    private final boolean nullFirst;

    private PostOrderingComparator(boolean nullFirst)
    {
        this.orderingFields = new ArrayList<>();
        this.nullFirst = nullFirst;
    }

    private void addOrderingField(String entityFieldIdentifier, String fieldSourceIdentifier, byte orderMethod)
    {
        this.orderingFields.add(new OrderingField(entityFieldIdentifier, fieldSourceIdentifier, orderMethod));
    }

    @Override
    public String toString()
    {
        StringBuilder toStringBuilder;

        toStringBuilder = new StringBuilder("PostOrderingAttributes{");
        toStringBuilder.append("orderingFields=").append(this.orderingFields);
        toStringBuilder.append('}');

        return toStringBuilder.toString();
    }

    @Override
    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public int compare(T8DataEntity o1, T8DataEntity o2)
    {
        int result;

        for (OrderingField orderingField : orderingFields)
        {
            result = compareValues(o1.getFieldValue(orderingField.entityFieldIdentifier), o2.getFieldValue(orderingField.entityFieldIdentifier), orderingField.orderMethod);
            if (result != 0) return result;
        }

        return 0;
    }

    private int compareValues(Object fieldValue1, Object fieldValue2, byte orderMethod)
    {
        if (fieldValue1 == null)
        {
            if (fieldValue2 == null) return 0;
            else if (this.nullFirst) return -1;
            else return 1;
        }
        else if (fieldValue2 == null)
        {
            if (this.nullFirst) return 1;
            else return -1;
        }
        else if (fieldValue1 instanceof Comparable)
        {
            int result;

            result = ((Comparable)fieldValue1).compareTo(fieldValue2);
            if (orderMethod == DESCENDING) return (result * -1);
            else return result;
        }
        else // We have a non-comparable value. We assume fieldValue1 is greater
        {
            logger.log(() -> "Non-comparable value type found. Type: " + fieldValue1.getClass());
            if (fieldValue1.equals(fieldValue2)) return 0;
            else if (orderMethod == DESCENDING) return -1;
            else return 1;
        }
    }

    private class OrderingField
    {
        private final String fieldSourceIdentifier; // This is when ordering can be done before the data entity is built
        private final String entityFieldIdentifier;
        private final byte orderMethod;

        private OrderingField(String entityFieldIdentifier, String fieldSourceIdentifier, byte orderMethod)
        {
            this.entityFieldIdentifier = entityFieldIdentifier;
            this.fieldSourceIdentifier = fieldSourceIdentifier;
            this.orderMethod = orderMethod;
        }

        @Override
        public String toString()
        {
            StringBuilder toStringBuilder;

            toStringBuilder = new StringBuilder("OrderingField{");
            toStringBuilder.append("entityFieldIdentifier=").append(this.entityFieldIdentifier);
            toStringBuilder.append(",fieldSourceIdentifier=").append(this.fieldSourceIdentifier);
            toStringBuilder.append(",orderMethod=").append(this.orderMethod);
            toStringBuilder.append('}');

            return toStringBuilder.toString();
        }
    }
}