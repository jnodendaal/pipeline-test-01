package com.pilog.t8.data;

import com.pilog.t8.api.T8Api;
import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8SecurityManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.cache.T8DataRecordCache;
import com.pilog.t8.cache.T8DataRequirementCache;
import com.pilog.t8.cache.T8DataRequirementInstanceCache;
import com.pilog.t8.cache.T8TerminologyCache;
import com.pilog.t8.data.T8DataTransactionLogEntry.TransactionOperationType;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.data.entity.T8DataEntityIterator;
import com.pilog.t8.definition.data.connection.T8DataConnectionDefinition;
import com.pilog.t8.definition.data.connection.pool.T8DataConnectionPoolDefinition;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.model.T8DataModelDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.cache.LRUCache;
import com.pilog.t8.utilities.date.Dates;
import com.pilog.t8.utilities.reflection.T8Reflections;
import com.pilog.t8.utilities.strings.Strings;
import java.sql.SQLException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.InvalidTransactionException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.TransactionManager;
import javax.transaction.Transaction;

import static com.pilog.t8.utilities.date.Dates.STD_SYS_TS_FORMATTER;

/**
 * @author Bouwer du Preez
 */
public class T8ServerDataTransaction implements T8DataTransaction
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8ServerDataTransaction.class);

    private final String identifier;
    private final T8Context context;
    private final T8SecurityManager securityManager;
    private final T8DefinitionManager definitionManager;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8ServerDataSession dataSession;
    private final Map<String, ? super T8Api> apiCache;
    private final Map<String, T8DataSource> dataSourceCache;
    private final Map<String, T8DataConnection> dataConnectionCache;
    private final Map<String, T8DataEntityDefinition> temporaryEntityDefinitions;
    private final Map<String, T8DataSourceDefinition> temporaryDataSourceDefinitions;
    private final LRUCache<String, T8DataEntityDefinition> entityDefinitionCache;
    private final LRUCache<String, T8DataSourceDefinition> dataSourceDefinitionCache;
    private final Thread parentThread;
    private final T8DataTransactionLog log;
    private final boolean autoCommit;
    private Map<String, String> apiClassMap;
    private Transaction suspendedCacheTransaction;
    private final TransactionManager cacheTransactionManager;
    private T8TerminologyCache terminologyCache;
    private final T8DataRecordCache dataRecordCache;
    private final T8DataRequirementInstanceCache dataRequirementInstanceCache;
    private T8DataRequirementCache dataRequirementCache;
    private T8DataModel transactionDataModel;
    private T8PerformanceStatistics stats;
    private FinalizationEvent finalizationEvent;
    private boolean open; // Used as a flag that is set to false when this transaction is finalized so that no further operations are caried out on it.
    private long lastActivityTime;
    private final long creationTime;
    private long finalizationTime;

    private enum FinalizationEvent {COMMIT, ROLLBACK};

    private static final int CACHE_TRANSACTION_TIMEOUT = 100_000*60; // The timeout for cache transactions (in seconds).  This value must be large enough to ensure that timeouts do not happen.  Transaction timeout is handled externally from the cache transactions.

    public T8ServerDataTransaction(T8Context context, T8ServerDataSession dataSession, String id, boolean autoCommit) throws Exception
    {
        this.securityManager = context.getServerContext().getSecurityManager();
        this.context = context;
        this.dataSession = dataSession;
        this.identifier = id;
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.definitionManager = serverContext.getDefinitionManager();
        this.dataSourceCache = Collections.synchronizedMap(new HashMap<String, T8DataSource>());
        this.dataConnectionCache = Collections.synchronizedMap(new HashMap<String, T8DataConnection>());
        this.temporaryEntityDefinitions = Collections.synchronizedMap(new HashMap<String, T8DataEntityDefinition>());
        this.temporaryDataSourceDefinitions = Collections.synchronizedMap(new HashMap<String, T8DataSourceDefinition>());
        this.entityDefinitionCache = new LRUCache<>(5);
        this.dataSourceDefinitionCache = new LRUCache<>(5);
        this.apiCache = new HashMap<>();
        this.stats = dataSession.getPerformanceStatistics();
        this.transactionDataModel = null;
        this.log = new T8DataTransactionLog(id, 50);
        this.open = true;
        this.creationTime = System.currentTimeMillis();
        this.parentThread = Thread.currentThread();
        this.terminologyCache = dataSession.getDataManager().getTerminologyCache(sessionContext.getRootOrganizationIdentifier());
        this.dataRecordCache = dataSession.getDataManager().getDataRecordCache(sessionContext.getRootOrganizationIdentifier());
        this.dataRequirementInstanceCache = dataSession.getDataManager().getDataRequirementInstanceCache(sessionContext.getRootOrganizationIdentifier());
        this.dataRequirementCache = dataSession.getDataManager().getDataRequirementCache(sessionContext.getRootOrganizationIdentifier());
        this.cacheTransactionManager = (TransactionManager)terminologyCache.getTransactionManager();
        this.cacheTransactionManager.setTransactionTimeout(CACHE_TRANSACTION_TIMEOUT);
        this.autoCommit = autoCommit;
        if (!autoCommit) this.cacheTransactionManager.begin();
        updateLastActivityTime();
    }

    @Override
    public Thread getParentThread()
    {
        return parentThread;
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public T8DataSession getDataSession()
    {
        return dataSession;
    }

    @Override
    public T8Context getContext()
    {
        return context;
    }

    @Override
    public T8Context accessContext(T8Context context)
    {
        return securityManager.accessContext(context);
    }

    @Override
    public boolean isOpen()
    {
        return open;
    }

    @Override
    public boolean isActive()
    {
        return log.getOpenEntryCount() > 0;
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;

        // Set the statistics on the data sources.
        for (T8DataSource dataSource : dataSourceCache.values())
        {
            dataSource.setPerformanceStatistics(statistics);
        }
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    @Override
    public T8DataTransactionLog getLog()
    {
        return log;
    }

    @Override
    public void suspend()
    {
        try
        {
            suspendedCacheTransaction = cacheTransactionManager.suspend();
        }
        catch (SystemException e)
        {
            throw new RuntimeException("Exception while suspending cache transaction.", e);
        }
    }

    @Override
    public void resume()
    {
        try
        {
            if (suspendedCacheTransaction != null)
            {
                cacheTransactionManager.resume(suspendedCacheTransaction);
                suspendedCacheTransaction = null;
            }
        }
        catch (IllegalStateException | InvalidTransactionException | SystemException e)
        {
            throw new RuntimeException("Exception while suspending cache transaction.", e);
        }
    }

    @Override
    public synchronized T8TerminologyCache getTerminologyCache()
    {
        updateLastActivityTime();
        return terminologyCache;
    }

    @Override
    public synchronized T8DataRecordCache getDataRecordCache()
    {
        updateLastActivityTime();
        return dataRecordCache;
    }

    @Override
    public synchronized T8DataRequirementInstanceCache getDataRequirementInstanceCache()
    {
        updateLastActivityTime();
        return dataRequirementInstanceCache;
    }

    @Override
    public synchronized T8DataRequirementCache getDataRequirementCache()
    {
        updateLastActivityTime();
        return dataRequirementCache;
    }

    public boolean checkConnections()
    {
        try
        {
            for (T8DataConnection connection : dataConnectionCache.values())
            {
                if (connection.isClosed()) return false;
            }

            return true;
        }
        catch (SQLException e)
        {
            LOGGER.log("Exception while checking status of connection in instant transaction.", e);
            return false;
        }
    }

    private T8DataModel getDataModel() throws Exception
    {
        if (transactionDataModel == null)
        {
            T8DataModelDefinition dataModelDefinition;

            dataModelDefinition = (T8DataModelDefinition)definitionManager.getResolvedDefinition(context, T8DataModelDefinition.TYPE_IDENTIFIER, null);
            transactionDataModel = dataModelDefinition.createNewDataModelInstance(this);
        }

        return transactionDataModel;
    }

    @Override
    public synchronized T8DataConnection getDataConnection(String connectionId)
    {
        if (open)
        {
            // If the requested connection is not cached, try to create it.
            updateLastActivityTime();
            if (!dataConnectionCache.containsKey(connectionId))
            {
                try
                {
                    T8DataConnectionDefinition connectionDefinition;

                    connectionDefinition = (T8DataConnectionDefinition)definitionManager.getInitializedDefinition(context, null, connectionId, null);
                    if (connectionDefinition != null)
                    {
                        T8DataConnection connection;

                        connection = connectionDefinition.createNewConnectionInstance(this);
                        connection.setAutoCommit(autoCommit);
                        dataConnectionCache.put(connectionId, connection);
                        return connection;
                    }
                    else throw new RuntimeException("Data Connection Definition not found: " + connectionId);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Exception while fetching data connection definition: " + connectionId, e);
                }
            }
            else // Return the cached connection.
            {
                // Return the connection from the cache.
                return dataConnectionCache.get(connectionId);
            }
        }
        else throw createTransactionFinalizedException();
    }

    private synchronized T8DataSource getDataSource(String dataSourceId) throws Exception
    {
        if (open)
        {
            String projectId;
            String cacheKey;

            // Get the project id for the current context and use it to create the cache key.
            projectId = getContext().getProjectId();
            cacheKey = projectId + ":" + dataSourceId;
            if (dataSourceCache.containsKey(cacheKey))
            {
                return dataSourceCache.get(cacheKey);
            }
            else
            {
                T8DataSource dataSource;
                T8DataSourceDefinition dataSourceDefinition;

                dataSourceDefinition = getDataSourceDefinition(dataSourceId);
                if (dataSourceDefinition != null)
                {
                    dataSource = dataSourceDefinition.getNewDataSourceInstance(this);
                    if (dataSource != null)
                    {
                        dataSource.open();
                        dataSource.setPerformanceStatistics(stats);
                        dataSourceCache.put(cacheKey, dataSource);
                        return dataSource;
                    }
                    else throw new Exception("Data source could not be initialized: " + dataSourceId);
                }
                else throw new Exception("Data source not found: " + dataSourceId);
            }
        }
        else throw createTransactionFinalizedException();
    }

    @Override
    public void refreshDataSource(String dataSourceId, Map<String, Object> refreshParameters) throws Exception
    {
        T8DataSource dataSource;

        dataSource = getDataSource(dataSourceId);
        if (dataSource != null)
        {
            if (dataSource instanceof T8RefreshableDataSource)
            {
                ((T8RefreshableDataSource)dataSource).refresh(refreshParameters);
            }
            else throw new Exception("Data Source is not refreshable: " + dataSourceId);
        }
        else throw new Exception("Data Source not found: " + dataSourceId);
    }

    @Override
    public T8DataConnectionPoolDefinition getDataConnectionPoolDefinition(String connectionPoolId)
    {
        try
        {
            T8DataConnectionPoolDefinition dataConnectionPoolDefinition;

            // Get the data connection pool definition.
            dataConnectionPoolDefinition = (T8DataConnectionPoolDefinition)definitionManager.getInitializedDefinition(context, null, connectionPoolId, null);
            if (dataConnectionPoolDefinition != null)
            {
                return dataConnectionPoolDefinition;
            }
            else throw new Exception("Data Connection Pool definition not found: " + connectionPoolId);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while fetching data connection pool definition: " + connectionPoolId, e);
        }
    }

    @Override
    public T8DataSourceDefinition getDataSourceDefinition(String dataSourceId)
    {
        T8DataSourceDefinition dataSourceDefinition;
        String projectId;
        String cacheKey;

        // Get the project id for the current context and use it to create the cache key.
        projectId = getContext().getProjectId();
        cacheKey = projectId + ":" + dataSourceId;

        // Check the temporary storage first for the requested data source definition.
        dataSourceDefinition = temporaryDataSourceDefinitions.get(dataSourceId);
        if (dataSourceDefinition != null) return dataSourceDefinition;

        // Get the data source definition.
        dataSourceDefinition = dataSourceDefinitionCache.get(cacheKey);
        if (dataSourceDefinition == null)
        {
            try
            {
                dataSourceDefinition = (T8DataSourceDefinition)definitionManager.getInitializedDefinition(context, projectId, dataSourceId, null);
                if (dataSourceDefinition != null)
                {
                    dataSourceDefinitionCache.put(cacheKey, dataSourceDefinition);
                    return dataSourceDefinition;
                }
                else throw new Exception("Data Source definition not found: " + dataSourceId);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while fetching data source definition: " + dataSourceId, e);
            }
        }
        else return dataSourceDefinition;
    }

    @Override
    public void setDataSourceDefinition(T8DataSourceDefinition dataSourceDefinition)
    {
        if (dataSourceDefinition == null) throw new IllegalArgumentException("Cannot set null data source definition on transaction.");
        temporaryDataSourceDefinitions.put(dataSourceDefinition.getIdentifier(), dataSourceDefinition);
    }

    @Override
    public T8DataEntityDefinition getDataEntityDefinition(String entityId)
    {
        T8DataEntityDefinition entityDefinition;
        String projectId;
        String cacheKey;

        // Get the project id for the current context and use it to create the cache key.
        projectId = getContext().getProjectId();
        cacheKey = projectId + ":" + entityId;

        // Check the temporary storage first for the requested data entity definition.
        entityDefinition = temporaryEntityDefinitions.get(entityId);
        if (entityDefinition != null) return entityDefinition;

        // Get the entity definition.
        entityDefinition = entityDefinitionCache.get(cacheKey);
        if (entityDefinition == null)
        {
            try
            {
                entityDefinition = (T8DataEntityDefinition)definitionManager.getInitializedDefinition(context, projectId, entityId, null);
                if (entityDefinition != null)
                {
                    entityDefinitionCache.put(cacheKey, entityDefinition);
                    return entityDefinition;
                }
                else throw new Exception("Entity definition not found: " + entityId);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Exception while fetching data entity definition: " + entityId, e);
            }
        }
        else return entityDefinition;
    }

    @Override
    public void setDataEntityDefinition(T8DataEntityDefinition entityDefinition)
    {
        if (entityDefinition == null) throw new IllegalArgumentException("Cannot set null entity definition on transaction.");
        temporaryEntityDefinitions.put(entityDefinition.getIdentifier(), entityDefinition);
    }

    @Override
    public <A extends T8Api> A getApi(String apiId)
    {
        // We synchronize on the API cache so that it can not be altered while we are potentially making changes to it.
        synchronized (apiCache)
        {
            A api;

            // First try to fetch the API from the cache.
            if ((api = (A)apiCache.get(apiId)) != null)
            {
                return api;
            }
            else // The API could not be found in the cache, so create a new instance.
            {
                // First make sure we have the api class map.
                if (apiClassMap == null)
                {
                    apiClassMap = serverContext.getConfigurationManager().getAPIClassMap(context);
                }

                try
                {
                    String apiClassName;

                    apiClassName = apiClassMap.get(apiId);
                    if (!Strings.isNullOrEmpty(apiClassName))
                    {
                        api = T8Reflections.getFastFailInstance(apiClassName, new Class<?>[]{T8DataTransaction.class}, this);
                        apiCache.put(apiId, api);
                        return api;
                    }
                    else throw new Exception("API not found: " + apiId);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Could not initialize API: " + apiId, e);
                }
            }
        }
    }

    @Override
    public T8DataEntity create(String entityId, Map<String, Object> fieldValues) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        T8DataEntity newEntity;

        // Get the entity definition.
        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityId);
        if (entityDefinition != null)
        {
            // Create a new entity from the definition.
            newEntity = entityDefinition.getNewDataEntityInstance();
            if (fieldValues != null) newEntity.setFieldValues(fieldValues);
            return newEntity;
        }
        else throw new Exception("Entity definition not found: " + entityId);
    }

    @Override
    public int count(String entityId, T8DataFilter dataFilter) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityId);
        if (entityDefinition != null)
        {
            T8DataSource dataSource;
            int count;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());
            if (dataFilter != null)
            {
                String operationIdentifier;

                // Do the count.
                operationIdentifier = T8IdentifierUtilities.createNewGUID();
                log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), dataFilter.toString(), TransactionOperationType.COUNT);
                count = dataSource.count(dataFilter);
                log.logOperationEnd(operationIdentifier);
                return count;
            }
            else
            {
                String operationIdentifier;

                // Do the count.
                operationIdentifier = T8IdentifierUtilities.createNewGUID();
                log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), null, TransactionOperationType.COUNT);
                count = dataSource.count(new T8DataFilter(entityId));
                log.logOperationEnd(operationIdentifier);
                return count;
            }
        }
        else throw new Exception("Entity definition not found: " + entityId);
    }

    @Override
    public boolean exists(String entityId, Map<String, Object> keyMap) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityId);
        if (entityDefinition != null)
        {
            T8DataSource dataSource;

            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());
            return dataSource.exists(entityId, keyMap);
        }
        else throw new Exception("Entity definition not found: " + entityId);
    }

    @Override
    public void insert(T8DataEntity entity) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = entity.getDefinition();
        if (entityDefinition != null)
        {
            String operationId;
            T8DataSource dataSource;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the insert.
            operationId = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationId, entityDefinition.getIdentifier(), null, TransactionOperationType.INSERT);
            dataSource.insert(entity);
            log.logOperationEnd(operationId);
        }
        else throw new Exception("Entity to insert does not contain a definition!");
    }

    @Override
    public void insert(List<T8DataEntity> entities) throws Exception
    {
        // Only proceed if the entity list if not empty.
        updateLastActivityTime();
        if (!entities.isEmpty())
        {
            T8DataEntityDefinition entityDefinition;

            entityDefinition = entities.get(0).getDefinition();
            if (entityDefinition != null)
            {
                String operationId;
                T8DataSource dataSource;

                // Get the data source.
                dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

                // Do the insert.
                operationId = T8IdentifierUtilities.createNewGUID();
                log.logOperationStart(operationId, entityDefinition.getIdentifier(), null, TransactionOperationType.INSERT);
                dataSource.insert(entities);
                log.logOperationEnd(operationId);
            }
            else throw new Exception("Entity to insert does not contain a definition!");
        }
    }

    @Override
    public boolean update(T8DataEntity entity) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = entity.getDefinition();
        if (entityDefinition != null)
        {
            String operationIdentifier;
            T8DataSource dataSource;
            boolean result;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the update.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), null, TransactionOperationType.UPDATE);
            result = dataSource.update(entity);
            log.logOperationEnd(operationIdentifier);
            return result;
        }
        else throw new Exception("Entity to update does not contain a definition!");
    }

    @Override
    public boolean update(List<T8DataEntity> entities) throws Exception
    {
        updateLastActivityTime();
        if (!entities.isEmpty())
        {
            T8DataEntityDefinition entityDefinition;

            entityDefinition = entities.get(0).getDefinition();
            if (entityDefinition != null)
            {
                String operationIdentifier;
                T8DataSource dataSource;
                boolean result;

                // Get the data source.
                dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

                // Do the update.
                operationIdentifier = T8IdentifierUtilities.createNewGUID();
                log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), null, TransactionOperationType.UPDATE);
                result = dataSource.update(entities);
                log.logOperationEnd(operationIdentifier);
                return result;
            }
            else throw new Exception("Entity to update does not contain a definition!");
        }
        else return true;
    }

    @Override
    public int update(String entityIdentifier, Map<String, Object> updatedValues, T8DataFilter dataFilter) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityIdentifier);
        if (entityDefinition != null)
        {
            String operationIdentifier;
            T8DataSource dataSource;
            int updateCount;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the update.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), dataFilter != null ? dataFilter.toString() : null, TransactionOperationType.UPDATE);
            updateCount = dataSource.update(entityIdentifier, updatedValues, dataFilter);
            log.logOperationEnd(operationIdentifier);
            return updateCount;
        }
        else throw new Exception("Entity definition not found: " + entityIdentifier);
    }

    @Override
    public boolean delete(T8DataEntity entity) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = entity.getDefinition();
        if (entityDefinition != null)
        {
            String operationIdentifier;
            T8DataSource dataSource;
            boolean result;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the deletion.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), null, TransactionOperationType.DELETE);
            result = dataSource.delete(entity);
            log.logOperationEnd(operationIdentifier);
            return result;
        }
        else throw new Exception("Entity to delete does not contain a definition!");
    }

    @Override
    public int delete(List<T8DataEntity> entities) throws Exception
    {
        updateLastActivityTime();
        if (!entities.isEmpty())
        {
            T8DataEntityDefinition entityDefinition;

            entityDefinition = entities.get(0).getDefinition();
            if (entityDefinition != null)
            {
                String operationIdentifier;
                T8DataSource dataSource;
                int result;

                // Get the data source.
                dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

                // Do the update.
                operationIdentifier = T8IdentifierUtilities.createNewGUID();
                log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), null, TransactionOperationType.DELETE);
                result = dataSource.delete(entities);
                log.logOperationEnd(operationIdentifier);
                return result;
            }
            else throw new Exception("Entity to delete does not contain a definition!");
        }
        else return 0;
    }

    @Override
    public int delete(String entityIdentifier, T8DataFilter dataFilter) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityIdentifier);
        if (entityDefinition != null)
        {
            String operationIdentifier;
            T8DataSource dataSource;
            int deletionCount;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the deletion.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), dataFilter != null ? dataFilter.toString() : null, TransactionOperationType.DELETE);
            deletionCount = dataSource.delete(entityIdentifier, dataFilter);
            log.logOperationEnd(operationIdentifier);
            return deletionCount;
        }
        else throw new Exception("Entity definition not found: " + entityIdentifier);
    }

    @Override
    public int deleteCascade(T8DataEntity dataEntity) throws Exception
    {
        T8DataModel dataModel;

        updateLastActivityTime();
        dataModel = getDataModel();
        if (dataModel != null)
        {
            // Use the data model to delete all dependent entities first and then the entity we start with.
            return dataModel.deleteCascade(this, dataEntity);
        }
        else // No data model so we just delete normally.
        {
            if (delete(dataEntity)) return 1;
            else return 0;
        }
    }

    @Override
    public int deleteCascade(String entityIdentifier, T8DataFilter dataFilter) throws Exception
    {
        T8DataEntityDefinition entityDefinition;
        T8DataEntityResults entityResults;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityIdentifier);
        if (entityDefinition != null)
        {
            int deletionCount;

            deletionCount = 0;
            entityResults = scroll(entityIdentifier, dataFilter);
            while (entityResults.next())
            {
                deletionCount += deleteCascade(entityResults.get());
            }

            entityResults.close();
            return deletionCount;
        }
        else throw new Exception("Entity definition not found: " + entityIdentifier);
    }

    @Override
    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> map) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityIdentifier);
        if (entityDefinition != null)
        {
            String operationIdentifier;
            T8DataSource dataSource;
            T8DataEntity retrievedEntity;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the retrieval.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), null, TransactionOperationType.RETRIEVE);
            retrievedEntity = dataSource.retrieve(entityIdentifier, map);
            log.logOperationEnd(operationIdentifier);
            return retrievedEntity;
        }
        else throw new Exception("Entity definition not found: " + entityIdentifier);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> map) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityIdentifier);
        if (entityDefinition != null)
        {
            String operationIdentifier;
            T8DataSource dataSource;
            List<T8DataEntity> entityList;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the selection.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), null, TransactionOperationType.SELECT);
            entityList = dataSource.select(entityIdentifier, map);
            log.logOperationEnd(operationIdentifier);
            return entityList;
        }
        else throw new Exception("Entity definition not found: " + entityIdentifier);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter dataFilter) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityIdentifier);
        if (entityDefinition != null)
        {
            String operationIdentifier;
            T8DataSource dataSource;
            List<T8DataEntity> entityList;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the selection.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), dataFilter != null ? dataFilter.toString() : null, TransactionOperationType.SELECT);
            entityList = dataSource.select(entityIdentifier, dataFilter);
            log.logOperationEnd(operationIdentifier);
            return entityList;
        }
        else throw new Exception("Entity definition not found: " + entityIdentifier);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> map, int pageOffset, int pageSize) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityIdentifier);
        if (entityDefinition != null)
        {
            String operationIdentifier;
            T8DataSource dataSource;
            List<T8DataEntity> entityList;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the selection.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), null, TransactionOperationType.SELECT);
            entityList = dataSource.select(entityIdentifier, map, pageOffset, pageSize);
            log.logOperationEnd(operationIdentifier);
            return entityList;
        }
        else throw new Exception("Entity definition not found: " + entityIdentifier);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter dataFilter, int pageOffset, int pageSize) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityIdentifier);
        if (entityDefinition != null)
        {
            String operationIdentifier;
            T8DataSource dataSource;
            List<T8DataEntity> entityList;

            // Get the data source.
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());

            // Do the selection.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), dataFilter != null ? dataFilter.toString() : null, TransactionOperationType.SELECT);
            entityList = dataSource.select(entityIdentifier, dataFilter, pageOffset, pageSize);
            log.logOperationEnd(operationIdentifier);
            return entityList;
        }
        else throw new Exception("Entity definition not found: " + entityIdentifier);
    }

    @Override
    public T8DataEntityResults scroll(String entityIdentifier, T8DataFilter dataFilter) throws Exception
    {
        T8DataEntityDefinition entityDefinition;

        updateLastActivityTime();
        entityDefinition = getDataEntityDefinition(entityIdentifier);
        if (entityDefinition != null)
        {
            T8DataEntityResults results;
            String operationIdentifier;
            T8DataSource dataSource;

            // Get the data source.
            operationIdentifier = T8IdentifierUtilities.createNewGUID();
            dataSource = getDataSource(entityDefinition.getDataSourceIdentifier());
            log.logOperationStart(operationIdentifier, entityDefinition.getIdentifier(), dataFilter != null ? dataFilter.toString() : null, TransactionOperationType.SCROLL);
            results = dataSource.scroll(entityIdentifier, dataFilter);
            results.addListener(new EntityResultListener(this, operationIdentifier));
            return results;
        }
        else throw new Exception("Entity definition not found: " + entityIdentifier);
    }

    @Override
    public T8DataIterator<T8DataEntity> iterate(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        T8DataEntityResults entityResults;

        updateLastActivityTime();
        entityResults = scroll(entityIdentifier, filter);
        return new T8DataEntityIterator(this, entityResults);
    }

    private void updateLastActivityTime()
    {
        // We have to update the last activity time on the session as well.
        dataSession.updateLastActivityTime();

        // Update the activity time for the transaction.
        lastActivityTime = System.currentTimeMillis();
    }

    @Override
    public long getLastActivityTime()
    {
        return lastActivityTime;
    }

    @Override
    public synchronized void rollback()
    {
        if (open)
        {
            if (!autoCommit)
            {
                // Set the finalization event.
                finalizationEvent = FinalizationEvent.ROLLBACK;

                // Update the last activity time.
                updateLastActivityTime();

                // Roll-back all open data sources.
                for (T8DataSource dataSource : dataSourceCache.values())
                {
                    try
                    {
                        dataSource.rollback();
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while rolling back data source:" + dataSource, e);
                    }
                }

                // Set all connections to auto-commit because the existing transaction was ended after the roll-back.
                for (T8DataConnection dataConnection : dataConnectionCache.values())
                {
                    try
                    {
                        dataConnection.rollback();
                        dataConnection.setAutoCommit(true);
                        dataConnection.close();
                    }
                    catch (SQLException e)
                    {
                        LOGGER.log("Exception while rolling back data connection: " + dataConnection.getDefinition(), e);
                    }
                }

                // Rollback cache changes.
                try
                {
                    cacheTransactionManager.rollback();
                }
                catch (IllegalStateException | SecurityException | SystemException e)
                {
                    LOGGER.log("Exception while rolling back cache changes.", e);
                }

                // Close the transaction and release the resources.
                close();
            }
            else throw new RuntimeException("Cannot rollback on an auto-commit transaction.");
        }
        else throw createTransactionFinalizedException();
    }

    @Override
    public synchronized void commit()
    {
        if (open)
        {
            if (!autoCommit)
            {
                // Set the finalization event.
                finalizationEvent = FinalizationEvent.COMMIT;

                // Update the last activity time.
                updateLastActivityTime();

                // Commit all open data sources.
                for (T8DataSource dataSource : dataSourceCache.values())
                {
                    try
                    {
                        dataSource.commit();
                    }
                    catch (Exception e)
                    {
                        LOGGER.log("Exception while committing data source: " + dataSource, e);
                    }
                }

                // Set all connections to auto-commit.
                for (T8DataConnection dataConnection : dataConnectionCache.values())
                {
                    try
                    {
                        dataConnection.commit();
                    }
                    catch (SQLException e)
                    {
                        LOGGER.log("Exception while closing data connection: " + dataConnection.getDefinition(), e);
                    }
                }

                // Commit cache changes.
                try
                {
                    cacheTransactionManager.commit();
                }
                catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | RollbackException | SystemException e)
                {
                    LOGGER.log("Exception while committing cache changes.", e);
                }

                // Close the transaction and release the resources.
                close();
            }
            else throw new RuntimeException("Cannot commit on an auto-commit transaction.");
        }
        else throw createTransactionFinalizedException();
    }

    /**
     * Closes the data transaction. Closing of transactions is handled
     * internally and therefore this method is not exposed further than the
     * {@code protected} scope to allow for sub-types to override the
     * implementation.
     */
    protected synchronized void close()
    {
        if (this.open) // Should never be false when called correctly
        {
            // Commit all open data sources.
            for (T8DataSource dataSource : dataSourceCache.values())
            {
                try
                {
                    dataSource.close();
                }
                catch (Exception e)
                {
                    LOGGER.log("Exception while committing data source:" + dataSource, e);
                }
            }

            // Set all connections to auto-commit.
            for (T8DataConnection dataConnection : dataConnectionCache.values())
            {
                try
                {
                    dataConnection.close();
                }
                catch (SQLException e)
                {
                    LOGGER.log("Exception while closing data connection:" + dataConnection.getDefinition(), e);
                }
            }

            // Clear the caches.
            terminologyCache = null;
            dataRequirementCache = null;

            // Clear the cache of open sources and connections.
            dataSourceCache.clear();
            dataConnectionCache.clear();
            dataSession.closeTransaction(this);
            open = false;
            finalizationTime = System.currentTimeMillis();
        }
        else throw createTransactionFinalizedException();
    }

    private RuntimeException createTransactionFinalizedException()
    {
        return new RuntimeException("Attempt to use finalized transaction: " + identifier + ".  Finalization '" + finalizationEvent + "' occurred at: " + STD_SYS_TS_FORMATTER.format(finalizationTime));
    }

    @Override
    public String toString()
    {
        StringBuilder buffer;

        buffer = new StringBuilder();
        buffer.append("T8ServerDataTransaction{identifier=");
        buffer.append(identifier);
        buffer.append(", parentThread=");
        buffer.append(parentThread);
        buffer.append(", autoCommit=");
        buffer.append(autoCommit);
        buffer.append(", active=");
        buffer.append(open);
        buffer.append(", finalizationEvent=");
        buffer.append(finalizationEvent);
        buffer.append(", finalizationTime=");
        buffer.append(Dates.STD_SYS_TS_FORMATTER.format(finalizationTime));
        buffer.append(", lastActivityTime=");
        buffer.append(Dates.STD_SYS_TS_FORMATTER.format(lastActivityTime));
        buffer.append(", creationTime=");
        buffer.append(Dates.STD_SYS_TS_FORMATTER.format(creationTime));
        buffer.append('}');
        return buffer.toString();
    }

    private static class EntityResultListener implements T8DataEntityResultListener
    {
        private final T8ServerDataTransaction tx;
        private final String operationID;

        private EntityResultListener(T8ServerDataTransaction tx, String operationID)
        {
            this.tx = tx;
            this.operationID = operationID;
        }

        @Override
        public void entityRetrieved()
        {
            tx.updateLastActivityTime();
        }

        @Override
        public void entityResultsClosed()
        {
            tx.log.logOperationEnd(operationID);
        }
    }
}
