package com.pilog.t8.file.context;

import com.pilog.t8.file.T8FileContext;
import com.pilog.t8.utilities.files.FileUtilities;
import java.io.File;

/**
 * @author Bouwer du Preez
 */
public class T8TemporaryFileContext extends T8LocalFileContext implements T8FileContext
{
    public T8TemporaryFileContext(File rootDirectory, String contextInstanceID)
    {
        super(rootDirectory, contextInstanceID, null);
    }

    @Override
    protected String getContextPath()
    {
        return rootDirectory.getAbsolutePath() + "/" + contextIid;
    }

    @Override
    public boolean close() throws Exception
    {
        File contextFile;

        contextFile = getContextFile();
        if (contextFile.exists())
        {
            return FileUtilities.deleteDirectory(contextFile);
        }
        else throw new Exception("File context not found: " + contextIid);
    }    
}
