package com.pilog.t8.flow.task.object;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.filter.T8DataFilterExpressionParser;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.data.object.T8DataObjectHandler;
import com.pilog.t8.data.object.filter.T8ObjectFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.state.T8FlowTaskState;
import com.pilog.t8.flow.task.T8FlowTask.TaskEvent;
import com.pilog.t8.flow.task.T8TaskDataObject;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.flow.task.T8FlowTaskDefinition;
import com.pilog.t8.definition.flow.task.object.T8TaskObjectConstructorScriptDefinition;
import com.pilog.t8.definition.flow.task.object.T8TaskObjectDefinition;
import com.pilog.t8.utilities.collections.HashMaps;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import static com.pilog.t8.definition.flow.task.object.T8TaskObjectResource.*;
import com.pilog.t8.security.T8Context.T8ProjectContext;

/**
 * @author Bouwer du Preez
 */
public class T8TaskObjectHandler implements T8DataObjectHandler
{
    private final T8TaskObjectDefinition definition;
    private final T8ServerContext serverContext;
    private final T8Context context;
    private final T8DataTransaction tx;
    private final String objectId;
    private final String entityId;
    private final T8DefinitionManager definitionManager;
    private final Map<String, String> entityFieldMapping;
    private final List<String> languageIds;
    private final T8ServerContextScript script;

    public T8TaskObjectHandler(T8TaskObjectDefinition definition, T8DataTransaction tx) throws Exception
    {
        this.definition = definition;
        this.tx = tx;
        this.context = new T8ProjectContext(tx.getContext(), definition.getRootProjectId());
        this.serverContext = context.getServerContext();
        this.definitionManager = serverContext.getDefinitionManager();
        this.objectId = definition.getIdentifier();
        this.languageIds = definition.getLanguageIds();
        this.entityFieldMapping = definition.getDataEntityFieldMapping();
        this.entityId = definition.getDataEntityId();
        this.script = definition.getConstructorScript() != null ? definition.getConstructorScript().getNewScriptInstance(context) : null;

        // Do some initialization checks to ensure settings are valid.
        if (script == null) throw new Exception("No constructor script defined for task object: " + objectId);
        if ((languageIds == null) || (languageIds.isEmpty())) throw new Exception("No language ids specified for task obejct: " + objectId);
    }

    public void refreshTaskObject(TaskEvent taskEvent, T8FlowTaskState taskState, Map<String, Object> validationOutputParameters) throws Exception
    {
        T8FlowTaskDefinition taskDefinition;

        // Get the specified task definition.
        taskDefinition = (T8FlowTaskDefinition)definitionManager.getInitializedDefinition(context, null, taskState.getTaskId(), null);
        if (taskDefinition != null)
        {
            Map<String, Object> taskParameters;

            taskParameters = new HashMap<>();
            taskParameters.putAll(taskState.getInputParameters());
            if (validationOutputParameters != null) taskParameters.putAll(validationOutputParameters);
            refreshTaskObject(taskEvent, taskDefinition, taskState, T8IdentifierUtilities.mapParameters(taskParameters, taskDefinition.getTaskObjectParameterMapping()));
        }
        else throw new Exception("Task Definition not found: " + taskState);
    }

    public void refreshTaskObject(TaskEvent taskEvent, T8FlowTaskDefinition taskDefinition, T8FlowTaskState taskState, Map<String, Object> taskListParameters) throws Exception
    {
        for (String languageId : languageIds)
        {
            T8DataObject newObject;

            newObject = create(taskEvent, taskDefinition, taskState, taskListParameters, languageId);
            save(newObject);
        }
    }

    public void deleteTaskObject(String taskIid) throws Exception
    {
        // Create the entity and update/insert new values.
        tx.delete(entityId, new T8DataFilter(entityId, HashMaps.createSingular(entityId + F_TASK_IID, taskIid)));
    }

    public void deleteFlowTaskObjects(String flowIid) throws Exception
    {
        // Delete all entities with the flow iid as key.
        tx.delete(entityId, new T8DataFilter(entityId, HashMaps.createSingular(entityId + F_FLOW_IID, flowIid)));
    }

    @Override
    public String getDataObjectId()
    {
        return objectId;
    }

    private T8DataObject create(TaskEvent taskEvent, T8FlowTaskDefinition taskDefinition, T8FlowTaskState taskState, Map<String, Object> taskListParameters, String languageId)
    {
        Map<String, Object> inputParameters;

        // Create the script input parameter collection.
        inputParameters = new HashMap<String, Object>(taskListParameters);
        inputParameters.put(T8TaskObjectConstructorScriptDefinition.PARAMETER_TASK_EVENT_ID, taskEvent.toString());
        inputParameters.put(T8TaskObjectConstructorScriptDefinition.PARAMETER_LANGUAGE_ID, languageId);
        inputParameters.put(T8TaskObjectConstructorScriptDefinition.PARAMETER_TASK_STATE, taskState);
        inputParameters.put(T8TaskObjectConstructorScriptDefinition.PARAMETER_TASK_DEFINITION, taskDefinition);

        // Process the script output if not null.
        try
        {
            Map<String, Object> outputParameters;
            T8DataObject newObject;

            // Execute the script.
            outputParameters = script.executeScript(inputParameters);
            outputParameters = T8IdentifierUtilities.stripNamespace(outputParameters);
            outputParameters.put(F_FLOW_IID, taskState.getFlowIid());
            outputParameters.put(F_FLOW_ID, taskState.getFlowId());
            outputParameters.put(F_NODE_IID, taskState.getNodeIid());
            outputParameters.put(F_NODE_ID, taskState.getNodeId());
            outputParameters.put(F_TASK_IID, taskState.getTaskIid());
            outputParameters.put(F_TASK_ID, taskState.getTaskId());
            outputParameters.put(F_ORG_ID, taskState.getInitiatorRootOrgId());
            outputParameters.put(F_LANGUAGE_ID, languageId);
            outputParameters.put(F_TASK_NAME, taskDefinition.getName());
            outputParameters.put(F_TASK_DESCRIPTION, taskDefinition.getDescription());
            outputParameters.put(F_RESTRICTION_USER_ID, taskState.getRestrictionUserId());
            outputParameters.put(F_RESTRICTION_PROFILE_ID, taskState.getRestrictionProfileId());
            outputParameters.put(F_CLAIMED_BY_USER_ID, taskState.getClaimedByUserId());
            outputParameters.put(F_PRIORITY, taskState.getPriority());
            outputParameters.put(F_INITIATOR_ID, taskState.getInitiatorId());
            outputParameters.put(F_INITIATOR_IID, taskState.getInitiatorIid());
            outputParameters.put(F_INITIATOR_ORG_ID, taskState.getInitiatorOrgId());
            outputParameters.put(F_INITIATOR_ROOT_ORG_ID, taskState.getInitiatorRootOrgId());
            outputParameters.put(F_TIME_ISSUED, taskState.getTimeIssued());
            outputParameters.put(F_TIME_CLAIMED, taskState.getTimeClaimed());
            outputParameters.put(F_TIME_COMPLETED, taskState.getTimeCompleted());
            outputParameters.put(F_TIME_ESCALATED, taskState.getTimeEscalated());
            outputParameters.put(F_TIME_SPAN_ISSUED, taskState.getTimespanIssued());
            outputParameters.put(F_TIME_SPAN_CLAIMED, taskState.getTimespanClaimed());
            outputParameters.put(F_TIME_SPAN_TOTAL, taskState.getTimespanTotal());
            outputParameters.put(F_TIME_SPAN_BUSINESS_ISSUED, taskState.getTimespanBusinessIssued());
            outputParameters.put(F_TIME_SPAN_BUSINESS_CLAIMED, taskState.getTimespanBusinessClaimed());
            outputParameters.put(F_TIME_SPAN_BUSINESS_TOTAL, taskState.getTimespanBusinessTotal());
            outputParameters.put(F_GROUP_ID, taskState.getGroupId());
            outputParameters.put(F_GROUP_ITERATION, taskState.getGroupIteration());
            outputParameters.put(F_ESCALATION_LEVEL, taskState.getEscalationLevel());
            outputParameters.put(F_FUNCTIONALITY_ID, taskDefinition.getFunctionalityId());

            // Create the new object from the output field values.
            newObject = new T8TaskDataObject(definition, null);
            newObject.setFieldValues(outputParameters);
            return newObject;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing new data object: " + objectId, e);
        }
    }

    @Override
    public T8DataObject retrieve(String objectIid) throws Exception
    {
        T8DataEntity entity;

        // Retrieve the entity from the view using the specified record id and the current transaction language.
        entity = tx.retrieve(entityId, HashMaps.newHashMap(entityId + F_TASK_IID, objectIid, entityId + F_LANGUAGE_ID, context.getLanguageId()));
        if (entity != null)
        {
            // Create an object from the entity and return the result.
            return mapObject(entity);
        }
        else return null;
    }

    @Override
    public List<T8DataObject> search(T8ObjectFilter objectFilter, String searchExpression, int pageOffset, int pageSize) throws Exception
    {
        T8DataFilterExpressionParser parser;
        List<T8DataEntity> entities;
        List<T8DataObject> objects;
        T8DataFilter dataFilter;

        // Parse the search expression and create a filter from it.
        parser = new T8DataFilterExpressionParser(entityId, ArrayLists.newArrayList(entityId + F_SEARCH_TERMS));
        parser.setWhitespaceConjunction(T8DataFilterClause.DataFilterConjunction.AND);
        dataFilter = parser.parseExpression(searchExpression, false);
        dataFilter.addFilterCriterion(entityId + F_LANGUAGE_ID, DataFilterOperator.EQUAL, context.getLanguageId());

        // Select entities from the view using the search filter.
        objects = new ArrayList<>();
        entities = tx.select(entityId, dataFilter, pageOffset, pageSize);
        for (T8DataEntity entity : entities)
        {
            // Map each entity to a new data object and add it to the result.
            objects.add(mapObject(entity));
        }

        // Return the final list of objects, found using the search filter.
        return objects;
    }

    public T8DataObject mapObject(T8DataEntity entity)
    {
        T8DataObject object;

        object = new T8TaskDataObject(definition, null);
        object.setFieldValues(T8IdentifierUtilities.mapParameters(entity.getFieldValues(), T8IdentifierUtilities.createReverseIdentifierMap(entityFieldMapping)));
        return object;
    }

    private void insert(T8DataObject dataObject) throws Exception
    {
        Map<String, Object> fieldValues;
        T8DataEntity objectEntity;

        // Create the object entity.
        fieldValues = T8IdentifierUtilities.stripNamespace(dataObject.getFieldValues());
        fieldValues.put("$ID", dataObject.getId());
        objectEntity = tx.create(entityId, T8IdentifierUtilities.mapParameters(fieldValues, entityFieldMapping));
        objectEntity.setFieldValue(entityId + F_ROW_ID, T8IdentifierUtilities.createNewGUID());
        objectEntity.setFieldValue(entityId + F_PROJECT_ID, definition.getRootProjectId());

        // Insert the entity if it exists, else insert.
        tx.insert(objectEntity);
    }

    private void save(T8DataObject dataObject) throws Exception
    {
        Map<String, Object> fieldValues;
        T8DataEntity objectEntity;

        // Create the object entity.
        fieldValues = T8IdentifierUtilities.stripNamespace(dataObject.getFieldValues());
        fieldValues.put("$ID", dataObject.getId());
        objectEntity = tx.create(entityId, T8IdentifierUtilities.mapParameters(fieldValues, entityFieldMapping));
        objectEntity.setFieldValue(entityId + F_ROW_ID, T8IdentifierUtilities.createNewGUID());
        objectEntity.setFieldValue(entityId + F_PROJECT_ID, definition.getRootProjectId());

        // Update the entity if it exists, else insert.
        if (!tx.update(objectEntity))
        {
            tx.insert(objectEntity);
        }
    }
}
