package com.pilog.t8.flow;

import com.pilog.t8.T8FlowManager;
import com.pilog.t8.flow.task.T8TaskDetails;
import com.pilog.t8.flow.task.T8TaskFilter;
import com.pilog.t8.flow.task.T8TaskList;
import com.pilog.t8.flow.task.T8TaskListSummary;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ServerFlowManagerScriptFacade
{
    private final T8FlowManager flowManager;
    private final T8Context context;

    public T8ServerFlowManagerScriptFacade(T8Context context, T8FlowManager flowManager)
    {
        this.context = context;
        this.flowManager = flowManager;
    }

    public int getTaskCount(T8TaskFilter taskFilter) throws Exception
    {
        return flowManager.getTaskCount(context, taskFilter);
    }

    public T8TaskListSummary getTaskListSummary(T8TaskFilter taskFilter) throws Exception
    {
        return flowManager.getTaskListSummary(context, taskFilter);
    }

    public T8TaskList getTaskList(T8TaskFilter taskFilter) throws Exception
    {
        return flowManager.getTaskList(context, taskFilter);
    }

    public T8TaskDetails getTaskDetails(String taskInstanceIdentifier) throws Exception
    {
        return flowManager.getTaskDetails(context, taskInstanceIdentifier);
    }
}
