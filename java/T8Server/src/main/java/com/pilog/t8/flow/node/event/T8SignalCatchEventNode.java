package com.pilog.t8.flow.node.event;

import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow;
import com.pilog.t8.flow.T8FlowExecutionKey;
import com.pilog.t8.definition.flow.node.event.T8SignalCatchEventDefinition;
import com.pilog.t8.flow.key.T8SignalExecutionKey;
import com.pilog.t8.flow.T8DefaultFlowNode;
import com.pilog.t8.security.T8Context;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8SignalCatchEventNode extends T8DefaultFlowNode
{
    private final T8SignalCatchEventDefinition definition;

    public enum T8SignalCatchEventNodeExecutionStep {WAITING_FOR_SIGNAL};

    public T8SignalCatchEventNode(T8Context context, T8Flow parentFlow, T8SignalCatchEventDefinition definition, String nodeIid)
    {
        super(context, parentFlow, definition, nodeIid);
        this.definition = definition;
    }

    @Override
    public Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters) throws Exception
    {
        if (!isExecutionStepComplete(T8SignalCatchEventNodeExecutionStep.WAITING_FOR_SIGNAL.toString()))
        {
            if (executionKey instanceof T8SignalExecutionKey)
            {
                // Synchronize on flowState when a single state transition transaction must occur.
                synchronized (flowState)
                {
                    Map<String, Object> signalParameters;

                    // Clear the wait key (if any was set).
                    nodeState.clearWaitKeys();

                    // Get the signal parameters from the execution key.
                    signalParameters = ((T8SignalExecutionKey)executionKey).getSignalParameters();
                    signalParameters = T8IdentifierUtilities.mapParameters(signalParameters, definition.getParameterMapping());

                    // Complete the execution step.
                    completeExecutionStep(T8SignalCatchEventNodeExecutionStep.WAITING_FOR_SIGNAL.toString());
                    return signalParameters != null ? signalParameters : new HashMap<String, Object>();
                }
            }
            else
            {
                String keyParameterId;
                String keyParameter;

                // Get the signal key parameter from the flow parameters.
                keyParameterId = definition.getKeyParameterId();
                if (T8SignalCatchEventDefinition.FLOW_IID_PARAMETER_ID.equals(keyParameterId)) keyParameter = flowState.getFlowIid();
                else if (T8SignalCatchEventDefinition.FLOW_ID_PARAMETER_ID.equals(keyParameterId)) keyParameter = flowState.getFlowId();
                else keyParameter = (String)inputParameters.get(definition.getKeyParameterId());
                if (keyParameter != null)
                {
                    // Synchronize on flowState when a single state transition transaction must occur.
                    synchronized (flowState)
                    {
                        // The signal has not been received yet, so set a new wait key.
                        nodeState.addSignalCatchWaitKey(definition.getSignalIdentifier(), keyParameter);
                        parentFlow.persistState();
                        return null;
                    }
                }
                else throw new Exception("The workflow parameter '" + definition.getKeyParameterId() + "' to use as key for signal '" + definition.getSignalIdentifier() + "' was not found.  Flow parameters: " + inputParameters);
            }
        }
        else throw new Exception("Cannot wait for signal when it has already been caught.");
    }
}
