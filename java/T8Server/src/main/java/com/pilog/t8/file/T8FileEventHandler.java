package com.pilog.t8.file;

import java.io.File;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public interface T8FileEventHandler
{
    public enum FileEventType {REGISTERED, CREATED, DELETED, MODIFIED};

    public void handleFileEvent(Map<String, Object> contextParameters, Map<String, Object> inputParameters, File file, FileEventType eventType);
}
