package com.pilog.t8.script;

import com.pilog.epic.ExpressionEvaluator;
import com.pilog.epic.ParserContext;
import com.pilog.epic.exceptions.EPICRuntimeException;
import com.pilog.epic.exceptions.EPICSyntaxException;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.org.T8OrganizationStructure;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.data.document.path.DocPathExpressionEvaluator;
import com.pilog.t8.data.document.requirementtype.RequirementType;
import com.pilog.t8.data.ontology.T8OntologyConceptType;
import com.pilog.t8.data.ontology.T8OntologyStructure;
import com.pilog.t8.data.ontology.T8PrimaryOntologyDataType;
import com.pilog.t8.flow.T8ServerFlowManagerScriptFacade;
import com.pilog.t8.notification.T8ServerNotificationManagerScriptFacade;
import com.pilog.t8.process.T8ServerProcessManagerScriptFacade;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import com.pilog.t8.api.T8Api;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.document.path.DocPathEvaluatorFactory;
import com.pilog.t8.data.document.path.EpicDocPathParser;
import com.pilog.t8.data.ontology.T8OntologyStructureProvider;
import com.pilog.t8.data.org.T8OrganizationStructureProvider;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8ServerExpressionEvaluator extends ExpressionEvaluator implements T8ExpressionEvaluator
{
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final ParserContext parserContext;
    private final DocPathEvaluatorFactory docPathFactory;
    private final Map<String, T8Api> apiCache;

    public T8ServerExpressionEvaluator(T8Context context)
    {
        this.context = context;
        this.serverContext = context.getServerContext();
        this.parserContext = new ParserContext();
        this.apiCache = new HashMap<>();
        this.docPathFactory = DocPathEvaluatorFactory.getFactory(context);
        addImports();
    }

    private void addImports()
    {
        try
        {
            parserContext.addClassImport(T8OntologyConceptType.class.getSimpleName(), T8OntologyConceptType.class);
            parserContext.addClassImport(T8PrimaryOntologyDataType.class.getSimpleName(), T8PrimaryOntologyDataType.class);
            parserContext.addClassImport(RequirementType.class.getSimpleName(), RequirementType.class);

            parserContext.addMethodImport("translate", this, this.getClass().getDeclaredMethod("translate", String.class, Object[].class));
            parserContext.addMethodImport("createNewGUID", this, this.getClass().getDeclaredMethod("createNewGUID"));
            parserContext.addMethodImport("getTime", this, this.getClass().getDeclaredMethod("getTime"));
            parserContext.addMethodImport("generateKey", this, this.getClass().getDeclaredMethod("generateKey", String.class));
            parserContext.addMethodImport("getAPI", this, this.getClass().getDeclaredMethod("getApi", String.class));
            parserContext.addMethodImport("docPath", this, this.getClass().getDeclaredMethod("docPath", Object.class, String.class));
            parserContext.addMethodImport("getOrganizationStructure", this, this.getClass().getDeclaredMethod("getOrganizationStructure", new Class[0]));
            parserContext.addMethodImport("getOntologyStructure", this, this.getClass().getDeclaredMethod("getOntologyStructure", new Class[0]));

            parserContext.addExternalExpressionParser(new EpicDocPathParser(docPathFactory));
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while adding server expression evaluator imports.", e);
        }
    }

    /**
     * Sets the language to use when resolving expressions involving terminology.
     * @param languageId The concept id of the language to set.
     */
    @Override
    public void setLanguage(String languageId)
    {
        docPathFactory.setLanguage(languageId);
    }

    public T8OrganizationStructure getOrganizationStructure()
    {
        T8OrganizationStructureProvider provider;

        // We use a String literal for the API identifier, rather than the proper constant in order to eliminate the dependency on T8DocumentClient project.
        provider = (T8OrganizationStructureProvider)getApi("@API_ORGANIZATION");
        return provider.getOrganizationStructure();
    }

    public T8OntologyStructure getOntologyStructure()
    {
        T8OntologyStructureProvider provder;

        // We use a String literal for the API identifier, rather than the proper constant in order to eliminate the dependency on T8DocumentClient project.
        provder = (T8OntologyStructureProvider)getApi("@API_ONTOLOGY");
        return provder.getOntologyStructure();
    }

    public Object docPath(Object documentObject, String expression)
    {
        DocPathExpressionEvaluator expressionEvaluator;

        expressionEvaluator = docPathFactory.getEvaluator();
        return expressionEvaluator.evaluateExpression(documentObject, expression);
    }

    public T8Api getApi(String apiId)
    {
        T8Api api;

        api = apiCache.get(apiId);
        if (api != null)
        {
            return api;
        }
        else
        {
            T8DataTransaction tx;
            T8DataSession ds;

            ds =  serverContext.getDataManager().getCurrentSession();
            tx = ds.getTransaction();
            if (tx == null) tx = ds.instantTransaction();

            api = tx.getApi(apiId);
            apiCache.put(apiId, api);
            return api;
        }
    }

    public long getTime()
    {
        return System.currentTimeMillis();
    }

    public String generateKey(String keyId) throws Exception
    {
        return context.getServerContext().getDataManager().generateKey(keyId);
    }

    public void addClassImport(String className, Class declaredClass)
    {
        parserContext.addClassImport(className, declaredClass);
    }

    public void addMethodImport(String procedureName, Class classReference, String methodName)
    {
        parserContext.addMethodImport(procedureName, classReference, methodName);
    }

    public void addMethodImport(String procedureName, Object contextObject, Method method)
    {
        parserContext.addMethodImport(procedureName, contextObject, method);
    }

    public String translate(String inputString, Object... varargs)
    {
        return String.format(serverContext.getConfigurationManager().getUITranslation(context, inputString), varargs);
    }

    public String createNewGUID()
    {
        return T8IdentifierUtilities.createNewGUID();
    }

    @Override
    public void compileExpression(String expressionString) throws EPICSyntaxException
    {
        compileExpression(expressionString, parserContext);
    }

    @Override
    public Object evaluateExpression(Map<String, Object> inputVariables, Object contextObject) throws EPICRuntimeException
    {
        Map<String, Object> expressionParameters;

        // Add some default expression parameters.
        expressionParameters = new HashMap<>();
        expressionParameters.put("session", context.getSessionContext());
        expressionParameters.put("processManager", new T8ServerProcessManagerScriptFacade(context, serverContext.getProcessManager()));
        expressionParameters.put("flowManager", new T8ServerFlowManagerScriptFacade(context, serverContext.getFlowManager()));
        expressionParameters.put("notificationManager", new T8ServerNotificationManagerScriptFacade(context, serverContext.getNotificationManager()));
        if (inputVariables != null) expressionParameters.putAll(inputVariables);

        // Set the input parameters on the parser context and evaluate the expression.
        parserContext.setVariableDeclarations(expressionParameters.keySet());
        return super.evaluateExpression(expressionParameters, contextObject);
    }

    @Override
    public Object evaluateExpression(String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException
    {
        Map<String, Object> expressionParameters;

        // Add some default expression parameters.
        expressionParameters = new HashMap<>();
        expressionParameters.put("session", context.getSessionContext());
        expressionParameters.put("processManager", new T8ServerProcessManagerScriptFacade(context, serverContext.getProcessManager()));
        expressionParameters.put("flowManager", new T8ServerFlowManagerScriptFacade(context, serverContext.getFlowManager()));
        expressionParameters.put("notificationManager", new T8ServerNotificationManagerScriptFacade(context, serverContext.getNotificationManager()));
        if (inputVariables != null) expressionParameters.putAll(inputVariables);

        // Set the input parameters on the parser context and evaluate the expression.
        parserContext.setVariableDeclarations(expressionParameters.keySet());
        return evaluateExpression(parserContext, expressionString, expressionParameters, contextObject);
    }

    @Override
    public boolean evaluateBooleanExpression(Map<String, Object> inputVariables, Object contextObject) throws EPICRuntimeException
    {
        Map<String, Object> expressionParameters;

        // Add some default expression parameters.
        expressionParameters = new HashMap<String, Object>();
        expressionParameters.put("session", context.getSessionContext());
        expressionParameters.put("processManager", new T8ServerProcessManagerScriptFacade(context, serverContext.getProcessManager()));
        expressionParameters.put("flowManager", new T8ServerFlowManagerScriptFacade(context, serverContext.getFlowManager()));
        if (inputVariables != null) expressionParameters.putAll(inputVariables);

        // Set the input parameters on the parser context and evaluate the expression.
        parserContext.setVariableDeclarations(expressionParameters.keySet());
        return super.evaluateBooleanExpression(expressionParameters, contextObject);
    }

    @Override
    public boolean evaluateBooleanExpression(String expressionString, Map<String, Object> inputVariables, Object contextObject) throws EPICSyntaxException, EPICRuntimeException
    {
        Map<String, Object> expressionParameters;

        // Add some default expression parameters.
        expressionParameters = new HashMap<String, Object>();
        expressionParameters.put("session", context.getSessionContext());
        expressionParameters.put("processManager", new T8ServerProcessManagerScriptFacade(context, serverContext.getProcessManager()));
        expressionParameters.put("flowManager", new T8ServerFlowManagerScriptFacade(context, serverContext.getFlowManager()));
        if (inputVariables != null) expressionParameters.putAll(inputVariables);

        // Set the input parameters on the parser context and evaluate the expression.
        parserContext.setVariableDeclarations(expressionParameters.keySet());
        return evaluateBooleanExpression(parserContext, expressionString, expressionParameters, contextObject);
    }
}
