package com.pilog.t8.data.cache;

import com.pilog.t8.cache.T8DataRecordCache;
import javax.transaction.TransactionManager;
import org.infinispan.Cache;

/**
 * @author Bouwer du Preez
 */
public class T8InfinispanDataRecordCache implements T8DataRecordCache
{
    private final Cache<String, RecordData> cache;

    public T8InfinispanDataRecordCache(Cache<String, RecordData> cache) throws Exception
    {
        this.cache = cache;
    }

    @Override
    public TransactionManager getTransactionManager()
    {
        return this.cache.getAdvancedCache().getTransactionManager();
    }

    @Override
    public void remove(RecordData data)
    {
        cache.remove(data.getRecordId());
    }

    @Override
    public RecordData remove(String recordId)
    {
        // Update the cache.
        return cache.remove(recordId);
    }

    @Override
    public void put(RecordData data)
    {
        cache.put(data.getRecordId(), data);
    }

    @Override
    public RecordData get(String recordId)
    {
        return cache.get(recordId);
    }

    @Override
    public boolean containsKey(String recordId)
    {
        return cache.containsKey(recordId);
    }
}
