package com.pilog.t8.data.object;

import com.pilog.t8.api.T8DataObjectApi;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Bouwer du Preez
 */
public class T8ServerDataObjectProvider implements T8DataObjectProvider
{
    private final T8DataObjectApi objApi;

    public T8ServerDataObjectProvider(T8DataTransaction tx)
    {
        this.objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
    }

    @Override
    public T8DataObject getDataObject(String dataObjectId, String dataObjectIid, boolean includeState)
    {
        try
        {
            return objApi.retrieve(dataObjectId, dataObjectIid, includeState);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data object: " + dataObjectIid, e);
        }
    }

    @Override
    public T8DataObjectState getDataObjectState(String dataObjectIid)
    {
        try
        {
            return objApi.retrieveState(dataObjectIid);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while retrieving data object state: " + dataObjectIid, e);
        }
    }
}
