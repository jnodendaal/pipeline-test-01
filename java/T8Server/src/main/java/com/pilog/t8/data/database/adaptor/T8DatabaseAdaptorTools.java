package com.pilog.t8.data.database.adaptor;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.data.database.T8TablePrimaryKey;
import com.pilog.t8.definition.data.T8DataFieldDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.utilities.jdbc.JDBCUtilities;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * This class is an abstract class only available to the current package. It
 * contains methods which are reused between at least 2 or more database adaptors.
 * Main purpose of the class is to avoid duplicated methods between multiple
 * database adaptors.
 *
 * @author gavin.boshoff
 */
abstract class T8DatabaseAdaptorTools
{
    private T8DatabaseAdaptorTools()
    {
    }

    /**
     * Builds and executes the statement to create a new primary key on a table,
     * using the specified table name and the list of columns as the columns for
     * the primary key.
     *
     * @throws SQLException If there is an error during the creation of the new
     *      primary key on the specific table
     */
    protected static void createNewPrimaryKey(String tableName, T8DataConnection dataConnection, String... columns) throws SQLException
    {
        PreparedStatement pstmtCreatePrimKey = null;
        T8DatabaseAdaptor dbAdaptor;
        StringBuilder createPrimKey;

        try
        {
            dbAdaptor = dataConnection.getDatabaseAdaptor();

            createPrimKey = new StringBuilder("ALTER TABLE ").append(escapeMetaString(dataConnection.getDatabaseAdaptor(), tableName));
            createPrimKey.append(" ADD CONSTRAINT ");
            createPrimKey.append(escapeMetaString(dbAdaptor, "PK_".concat(tableName)));
            createPrimKey.append(" PRIMARY KEY (");
            for(String column : columns)
            {
                createPrimKey.append(escapeMetaString(dbAdaptor, column));
                createPrimKey.append(",");
            }
            createPrimKey.deleteCharAt(createPrimKey.lastIndexOf(",")).append(")");

            pstmtCreatePrimKey = dataConnection.prepareStatement(createPrimKey.toString());
            pstmtCreatePrimKey.execute();
        }
        finally
        {
            JDBCUtilities.close(pstmtCreatePrimKey);
        }
    }

    /**
     * Simply drops a primary key in the table, for which the details of the
     * key are specified by the {@code T8TablePrimaryKey} object.
     *
     * @throws SQLException If there is an error dropping the specified primary
     *      key in the database
     */
    protected static void dropPrimaryKey(T8TablePrimaryKey existingKey, T8DataConnection dataConnection) throws SQLException
    {
        PreparedStatement pstmtDropKey = null;
        T8DatabaseAdaptor dbAdaptor;

        try
        {
            dbAdaptor = dataConnection.getDatabaseAdaptor();

            pstmtDropKey = dataConnection.prepareStatement("ALTER TABLE " + escapeMetaString(dbAdaptor, existingKey.getTableName()) + " DROP CONSTRAINT " + escapeMetaString(dbAdaptor, existingKey.getPrimKeyIdentifier()));
            pstmtDropKey.execute();
        }
        finally
        {
            JDBCUtilities.close(pstmtDropKey);
        }
    }

    /**
     * Builds the section of the create table statement which adds a primary key
     * to the table as specified by all of the columns in the list of data field
     * definitions which are specified as keys on the table to which they belong
     */
    protected static StringBuilder getCreateTablePrimConstraint(String tableName, List<T8DataSourceFieldDefinition> dataFieldDefinitions, T8DatabaseAdaptor dbAdaptor)
    {
        StringBuilder primKeyConstraint;

        primKeyConstraint = new StringBuilder();
        for (T8DataFieldDefinition dataFieldDefinition : dataFieldDefinitions)
        {
            if (dataFieldDefinition.isKey())
            {
                primKeyConstraint.append(escapeMetaString(dbAdaptor, dataFieldDefinition.getSourceIdentifier()));
                primKeyConstraint.append(", ");
            }
        }

        if (primKeyConstraint.length() > 0)
        {
            primKeyConstraint.deleteCharAt(primKeyConstraint.lastIndexOf(","));
            primKeyConstraint.insert(0, ", CONSTRAINT " + escapeMetaString(dbAdaptor, "PK_" + tableName) + " PRIMARY KEY (");
            primKeyConstraint.append(")");
        }

        return primKeyConstraint;
    }

    /**
     * Escapes the specified meta {@code String} using the
     * {@code T8DatabaseAdaptor} and its methods
     * {@link T8DatabaseAdaptor#getMetaEscapeCharacterStart()}
     * and {@link T8DatabaseAdaptor#getMetaEscapeCharacterEnd()}.
     *
     * @param dbAdaptor The {@code T8DatabaseAdaptor} invoking this method
     * @param metaString The {@code Meta String} to be escaped
     *
     * @return The escaped {@code Meta String}
     */
    protected static String escapeMetaString(T8DatabaseAdaptor dbAdaptor, String metaString)
    {
        StringBuilder escapedStringBuilder;

        escapedStringBuilder = new StringBuilder();
        escapedStringBuilder.append(dbAdaptor.getMetaEscapeCharacterStart());
        escapedStringBuilder.append(metaString);
        escapedStringBuilder.append(dbAdaptor.getMetaEscapeCharacterEnd());

        return escapedStringBuilder.toString();
    }
}
