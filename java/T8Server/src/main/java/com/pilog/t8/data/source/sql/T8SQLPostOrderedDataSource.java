package com.pilog.t8.data.source.sql;

import com.pilog.t8.T8Log;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.data.source.PostOrderingComparator;
import com.pilog.t8.data.source.T8CommonStatementHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.sql.T8SQLPostOrderedDataSourceDefinition;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.util.List;
import java.util.Map;
import com.pilog.t8.data.T8DataTransaction;

/**
 * @author Gavin Boshoff
 */
public class T8SQLPostOrderedDataSource extends T8SQLDataSource
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8SQLPostOrderedDataSource.class);

    private final T8SQLPostOrderedDataSourceDefinition postOrderedDSDefinition;

    public T8SQLPostOrderedDataSource(T8SQLPostOrderedDataSourceDefinition definition, T8DataTransaction dataAccessProvider)
    {
        super(definition, dataAccessProvider);
        this.postOrderedDSDefinition = definition;
    }

    @Override
    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> keyValues) throws Exception
    {
        LOGGER.log(T8Logger.Level.WARNING, "Using a non-ordering method on a ordering specific data source. Consider revising the usage class.");
        List<T8DataEntity> orderedResults;
        T8DataFilter dataFilter;
        int pageSize = 10;
        int offset = 0;

        dataFilter = new T8DataFilter(entityIdentifier, keyValues);
        orderedResults = select(entityIdentifier, dataFilter, offset, pageSize);

        if (!orderedResults.isEmpty()) return orderedResults.get(offset);
        else return null;
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        PostOrderingComparator postOrderingComparator;
        T8DataEntityDefinition entityDefinition;
        ParameterizedString queryString;

        // Get the entity definition.
        entityDefinition = this.tx.getDataEntityDefinition(entityIdentifier);

        // Create the query string.
        queryString = getQueryString(entityDefinition, filter, true, pageOffset, pageSize);

        // Create the post ordering comparator
        postOrderingComparator = PostOrderingComparator.build(filter, entityDefinition, this.definition, this.postOrderedDSDefinition.isNullsFirst());

        // Retrieve the results.
        return T8CommonStatementHandler.executePostOrderingQuery(this.context, this.connection, queryString, this.definition, entityDefinition, postOrderingComparator, pageOffset, pageSize);
    }

    @Override
    public T8DataEntityResults scroll(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        throw new UnsupportedOperationException("scroll not supported as it cannot be post-ordered. Use select instead.");
    }

    @Override
    public ParameterizedString getQueryString(T8DataEntityDefinition entityDefinition, T8DataFilter filter, boolean includeWithClause, int pageOffset, int pageSize) throws Exception
    {
        ParameterizedString queryString;
        ParameterizedString withClause;

        // If a WITH clause is defined, prepend it to the query if required.
        withClause = getWithClause(entityDefinition, filter, pageOffset, pageSize);
        if ((withClause != null) && (!withClause.isEmpty()) && includeWithClause)
        {
            queryString = new ParameterizedString("-- ");
            if (entityDefinition != null) queryString.append(entityDefinition.getIdentifier());
            queryString.append("\n");
            queryString.append(withClause);
            queryString.append(" SELECT * FROM (");
        }
        else
        {
            queryString = new ParameterizedString("-- ");
            if (entityDefinition != null) queryString.append(entityDefinition.getIdentifier());
            queryString.append("\nSELECT * FROM (");
        }

        // Append the SQL Selection string from the definition (with SQL parameters replaced).
        queryString.append(getSelectClause(entityDefinition, filter, pageOffset, pageSize));
        queryString.append(") selectResults ");

        // Append filter strings.
        if (filter != null)
        {
            // Add the WHERE clause if available.
            if (filter.hasFilterCriteria())
            {
                queryString.append(" ");
                queryString.append(filter.getWhereClause(tx, "selectResults"));
                queryString.setParameterValues(filter.getWhereClauseParameters(tx));
            }

            // We don't add the ORDER by clause. It will be handled on the results retrieved
        }

        return queryString;
    }
}