package com.pilog.t8.functionality;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerOperation.T8ServerOperationStatus;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.functionality.T8OperationFunctionalityDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.time.T8Timestamp;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8OperationFunctionalityInstance extends T8DefaultFunctionalityInstance implements T8FunctionalityInstance
{
    private final T8OperationFunctionalityDefinition definition;
    private final String operationId;
    private String operationIid;
    private Map<String, Object> inputParameters;

    public T8OperationFunctionalityInstance(T8Context context, T8OperationFunctionalityDefinition definition)
    {
        super(context, definition, T8IdentifierUtilities.createNewGUID());
        this.definition = definition;
        this.operationId = definition.getOperationIdentifier();
    }

    public T8OperationFunctionalityInstance(T8Context context, T8OperationFunctionalityDefinition definition, T8FunctionalityState state)
    {
        super(context, definition, state.getFunctionalityIid());
        this.definition = definition;
        this.operationId = definition.getOperationIdentifier();
        this.operationIid = state.getOperationIid();
    }

    @Override
    public T8FunctionalityDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void initialize()
    {
    }

    @Override
    public boolean release()
    {
        try
        {
            T8ServerOperationStatusReport statusReport;

            // Execute the operation and return the results.
            statusReport = serverContext.getOperationStatus(context, operationIid);
            return statusReport == null || statusReport.getOperationStatus() != T8ServerOperationStatus.IN_PROGRESS;
        }
        catch (Exception e)
        {
            T8Log.log("Exception while checking status of operation: " + operationIid + " as functionality: " + iid, e);
            return false;
        }
    }

    @Override
    public T8FunctionalityAccessHandle access(Map<String, Object> parameters) throws Exception
    {
        Map<String, Object> operationParameters;
        T8ServerOperationStatusReport statusReport;
        T8Context accessContext;

        // Store the input parameters.
        this.timeAccessed = new T8Timestamp(System.currentTimeMillis());
        this.inputParameters = parameters;

        // Create the new access context.
        accessContext = new T8Context(context);
        accessContext.setFunctionalityId(definition.getIdentifier());
        accessContext.setFunctionalityIid(iid);

        // Create the operation input parameter map.
        operationParameters = T8IdentifierUtilities.stripNamespace(id, parameters, true);
        operationParameters = T8IdentifierUtilities.mapParameters(operationParameters, definition.getInputParameterMapping());

        // Execute the operation and return the results.
        statusReport = serverContext.executeAsynchronousOperation(context, operationId, operationParameters);
        operationIid = statusReport.getOperationInstanceIdentifier();
        return new T8OperationFunctionalityAccessHandle(definition.getIdentifier(), iid, getDisplayName(), getDescription(), getIcon(), statusReport);
    }

    @Override
    public boolean stop()
    {
        return true;
    }

    @Override
    public T8FunctionalityState getFunctionalityState()
    {
        T8FunctionalityState state;

        // Create the new state.
        state = super.getFunctionalityState(inputParameters);

        // Add the operation instance id so that this functionality can be removed when the operation ends.
        state.setOperationIid(operationIid);

        // Return the state.
        return state;
    }
}
