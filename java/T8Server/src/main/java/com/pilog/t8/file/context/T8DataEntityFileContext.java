package com.pilog.t8.file.context;

import com.pilog.t8.T8DataManager;
import com.pilog.t8.T8FileManager;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.file.T8FileContext;
import com.pilog.t8.file.T8FileDetails;
import com.pilog.t8.commons.io.RandomAccessStream;
import com.pilog.t8.data.source.T8ScrollableEntityResults;
import com.pilog.t8.definition.file.context.T8DataEntityFileContextDefinition;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.files.RandomAccessFileStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author bouwer
 */
public class T8DataEntityFileContext implements T8FileContext
{
    private final T8DataEntityFileContextDefinition definition;
    private final T8DataManager dataManager;
    private final T8Context context;
    private final String entityId;
    private final String fileDataFieldId;
    private final String fileNameFieldId;
    private final String fileIdFieldId;
    private final String contextIid;
    private final String contextId;
    private T8ScrollableEntityResults inputStreamResults;
    private T8ScrollableEntityResults outputStreamResults;

    public T8DataEntityFileContext(T8Context context, T8FileManager fileManager, T8DataEntityFileContextDefinition definition, String contextIid)
    {
        this.contextIid = contextIid;
        this.definition = definition;
        this.contextId = definition.getIdentifier();
        this.context = context;
        this.dataManager = context.getServerContext().getDataManager();
        this.entityId = definition.getEntityId();
        this.fileDataFieldId = definition.getFileDataFieldId();
        this.fileNameFieldId = definition.getFileNameFieldId();
        this.fileIdFieldId = definition.getFileIdFieldId();
    }

    @Override
    public String getId()
    {
        return contextId;
    }

    @Override
    public String getIid()
    {
        return contextIid;
    }

    @Override
    public File getFile(String filePath) throws Exception
    {
        return new File(filePath);
    }

    @Override
    public boolean fileExists(String filePath) throws Exception
    {
        T8DataTransaction tx;
        T8DataFilter filter;
        List<T8DataEntity> entityResult;

        filter = new T8DataFilter(entityId);
        filter.addFilterCriterion(fileNameFieldId, filePath);

        tx = dataManager.getCurrentSession().instantTransaction();
        entityResult = tx.select(entityId, filter);

        if (entityResult.size() > 0)
        {
            return true;
        }
        else return false;
    }

    @Override
    public boolean open() throws Exception
    {
        return true;
    }

    @Override
    public boolean close() throws Exception
    {
        if (inputStreamResults != null && outputStreamResults != null)
        {
            inputStreamResults.close();
            outputStreamResults.close();

            return true;
        }
        else if (inputStreamResults != null)
        {
            inputStreamResults.close();

            return true;
        }
        else if (outputStreamResults != null)
        {
            outputStreamResults.close();

            return true;
        }
        else return false;
    }

    @Override
    public boolean createDirectory(String filePath) throws Exception
    {
        File newFolder;

        newFolder = getFile(filePath);
        return newFolder.mkdirs();
    }

    @Override
    public void renameFile(String filePath, String newFilename) throws Exception, FileNotFoundException
    {
        T8DataTransaction tx;
        T8DataFilter filter;
        List<T8DataEntity> entityResult;

        tx = dataManager.getCurrentSession().instantTransaction();
        filter = new T8DataFilter(entityId);
        filter.addFilterCriterion(fileNameFieldId, filePath);

        entityResult = tx.select(entityId, filter);

        //Check if the file exist before trying to rename.
        if (entityResult.size() > 0)
        {
            for(T8DataEntity entity : entityResult)
            {
               entity.setFieldValue(fileNameFieldId, newFilename);

               tx.update(entity);
            }
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public boolean deleteFile(String filePath) throws Exception, FileNotFoundException
    {
        T8DataTransaction tx;
        T8DataFilter filter;
        Boolean fileExist;
        int deletionCount;

        fileExist = fileExists(filePath);
        if (fileExist)
        {
            tx = dataManager.getCurrentSession().instantTransaction();
            filter = new T8DataFilter(entityId);
            filter.addFilterCriterion(fileNameFieldId, filePath);

            deletionCount = tx.delete(entityId, filter);

            if (deletionCount > 0)
            {
                return true;
            }
            else return false;
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public long getFileSize(String filePath) throws Exception, FileNotFoundException
    {
        return -1;
    }

    @Override
    public String getMD5Checksum(String filePath) throws Exception, FileNotFoundException
    {
        File targetFile;

        targetFile = getFile(filePath);
        if (targetFile.exists())
        {
            if (!targetFile.isDirectory())
            {
                // Use Apache commons to compute the checksum.
                return DigestUtils.md5Hex(getFileInputStream(filePath));
            }
            else
            {
                throw new RuntimeException("Cannot compute MD5 Checksum for specified path: " + filePath);
            }
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public T8FileDetails getFileDetails(String filePath) throws Exception, FileNotFoundException
    {
        Boolean fileExist;

        fileExist = fileExists(filePath);
        if (fileExist)
        {
            long fileSize;

            fileSize = -1;

            return new T8FileDetails(contextIid, contextId, filePath, fileSize, false, null, null);
        }
        else throw new FileNotFoundException("File '" + filePath + "' not found in: " + this);
    }

    @Override
    public List<T8FileDetails> getFileList(String directoryPath) throws Exception, FileNotFoundException
    {
        File directory;

        directory = getFile(directoryPath);
        if (directory.isDirectory())
        {
            ArrayList<T8FileDetails> fileDetails;
            File[] fileList;

            fileDetails = new ArrayList<T8FileDetails>();
            fileList = directory.listFiles();
            for (File file : fileList)
            {
                String filePath;
                long fileSize;
                boolean isDirectory;

                isDirectory = file.isDirectory();
                fileSize = -1;
                filePath = directory.getAbsolutePath();

                fileDetails.add(new T8FileDetails(contextIid, contextId, filePath, fileSize, isDirectory, null, null));
            }

            return fileDetails;
        }
        else throw new FileNotFoundException("Directory '" + directoryPath + "' not found in: " + this);
    }

    @Override
    public InputStream getFileInputStream(String filePath) throws Exception, FileNotFoundException
    {
        InputStream inputStream = null;
        Object streamObject;

        try
        {
            T8DataTransaction tx;
            T8DataFilter filter;

            filter = new T8DataFilter(entityId);
            filter.addFilterCriterion(fileNameFieldId, filePath);

            tx = dataManager.getCurrentSession().instantTransaction();
            inputStreamResults = (T8ScrollableEntityResults)tx.scroll(entityId, filter);
            inputStreamResults.setStreamType(fileDataFieldId, T8DataEntityResults.StreamType.INPUT);

            if (inputStreamResults.next())
            {
                T8DataEntity fileEntity;

                fileEntity = inputStreamResults.get();
                streamObject = fileEntity.getFieldValue(fileDataFieldId);

                if (streamObject instanceof byte[])
                {
                    inputStream = new ByteArrayInputStream((byte[]) streamObject);
                }
                else
                {
                    inputStream = (InputStream) streamObject;
                }
            }
        }
        finally
        {
            if (inputStreamResults != null) inputStreamResults.close();
        }
        return inputStream;
    }

    @Override
    public OutputStream getFileOutputStream(String filePath, boolean append) throws Exception
    {
        OutputStream outputStream = null;

        try
        {
            T8DataTransaction tx;
            T8DataFilter filter;

            filter = new T8DataFilter(entityId);
            filter.addFilterCriterion(fileNameFieldId, filePath);

            tx = dataManager.getCurrentSession().instantTransaction();
            outputStreamResults = (T8ScrollableEntityResults)tx.scroll(entityId, filter);
            outputStreamResults.setStreamType(fileDataFieldId, T8DataEntityResults.StreamType.OUTPUT);

            if (outputStreamResults.next())
            {
                T8DataEntity fileEntity;

                fileEntity = outputStreamResults.get();
                outputStream = (OutputStream)fileEntity.getFieldValue(fileDataFieldId);
            }
        }
        finally
        {
            if (outputStreamResults != null) outputStreamResults.close();
        }
        return outputStream;
    }

    @Override
    public RandomAccessStream getFileRandomAccessStream(String filePath) throws Exception, FileNotFoundException
    {
        File inputFile;

        inputFile = getFile(filePath);
        if (inputFile.exists() || inputFile.createNewFile())
        {
            RandomAccessStream fileStream;

            fileStream = new RandomAccessFileStream(inputFile);
            return fileStream;
        }
        else throw new FileNotFoundException("Filepath '" + filePath + "' not found in: " + this);
    }

    @Override
    public String toString()
    {
        StringBuffer buffer;

        buffer = new StringBuffer();
        buffer.append("[FileContext:");
        buffer.append(contextId);
        buffer.append(":");
        buffer.append(contextIid);
        buffer.append("]");
        return buffer.toString();
    }
}
