package com.pilog.t8.flow.task.object;

import com.pilog.t8.api.T8DataObjectApi;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterExpressionParser;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.data.object.T8DataObjectGroupHandler;
import com.pilog.t8.data.object.filter.T8ObjectFilter;
import com.pilog.t8.definition.flow.task.object.T8TaskObjectGroupDefinition;
import com.pilog.t8.utilities.collections.ArrayLists;
import com.pilog.t8.security.T8Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import static com.pilog.t8.definition.flow.task.object.T8TaskObjectResource.*;

/**
 * @author Bouwer du Preez
 */
public class T8TaskObjectGroupHandler implements T8DataObjectGroupHandler
{
    private final T8Context context;
    private final T8DataTransaction tx;
    private final T8DataObjectApi objApi;
    private final String entityId;
    private final List<String> objectIds;
    private final Map<String, String> objectToEntityFieldMapping;


    public T8TaskObjectGroupHandler(T8TaskObjectGroupDefinition definition, T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.objApi = tx.getApi(T8DataObjectApi.API_IDENTIFIER);
        this.entityId = DE_OBJECT_FLOW_TASK;
        this.objectIds = definition.getObjectIds();
        this.objectToEntityFieldMapping = createDefaultFieldMapping(); // May be specified in definition later.
    }

    private Map<String, String> createDefaultFieldMapping()
    {
        Map<String, String> fieldMapping;

        fieldMapping = new HashMap<>();
        fieldMapping.put(F_RESTRICTION_USER_ID, entityId + F_RESTRICTION_USER_ID);
        fieldMapping.put(F_TASK_IID, entityId + F_TASK_IID);
        fieldMapping.put(F_TASK_ID, entityId + F_TASK_ID);
        fieldMapping.put(F_FLOW_IID, entityId + F_FLOW_IID);
        fieldMapping.put(F_FLOW_ID, entityId + F_FLOW_ID);
        fieldMapping.put(F_NODE_IID, entityId + F_NODE_IID);
        fieldMapping.put(F_NODE_ID, entityId + F_NODE_ID);
        fieldMapping.put(F_ORG_ID, entityId + F_ORG_ID);
        fieldMapping.put(F_LANGUAGE_ID, entityId + F_LANGUAGE_ID);
        fieldMapping.put(F_TASK_NAME, entityId + F_TASK_NAME);
        fieldMapping.put(F_TASK_DESCRIPTION, entityId + F_TASK_DESCRIPTION);
        fieldMapping.put(F_SEARCH_TERMS, entityId + F_SEARCH_TERMS);
        fieldMapping.put(F_GROUP_ID, entityId + F_GROUP_ID);
        fieldMapping.put(F_RESTRICTION_USER_ID, entityId + F_RESTRICTION_USER_ID);
        fieldMapping.put(F_RESTRICTION_PROFILE_ID, entityId + F_RESTRICTION_PROFILE_ID);
        fieldMapping.put(F_INITIATOR_ID, entityId + F_INITIATOR_ID);
        fieldMapping.put(F_INITIATOR_IID, entityId + F_INITIATOR_IID);
        fieldMapping.put(F_INITIATOR_ORG_ID, entityId + F_INITIATOR_ORG_ID);
        fieldMapping.put(F_INITIATOR_ROOT_ORG_ID, entityId + F_INITIATOR_ROOT_ORG_ID);
        fieldMapping.put(F_CLAIMED_BY_USER_ID, entityId + F_CLAIMED_BY_USER_ID);
        fieldMapping.put(F_PRIORITY, entityId + F_PRIORITY);
        fieldMapping.put(F_TIME_ISSUED, entityId + F_TIME_ISSUED);
        fieldMapping.put(F_TIME_CLAIMED, entityId + F_TIME_CLAIMED);
        fieldMapping.put(F_TIME_COMPLETED, entityId + F_TIME_COMPLETED);
        fieldMapping.put(F_TIME_ESCALATED, entityId + F_TIME_ESCALATED);
        return fieldMapping;
    }

    @Override
    public List<T8DataObject> search(T8ObjectFilter objectFilter, String searchExpression, int pageOffset, int pageSize) throws Exception
    {
        T8DataFilterExpressionParser parser;
        List<T8DataEntity> entities;
        List<T8DataObject> objects;
        T8DataFilter dataFilter;

        // Parse the search expression and create a filter from it.
        parser = new T8DataFilterExpressionParser(entityId, ArrayLists.newArrayList(entityId + F_SEARCH_TERMS));
        parser.setWhitespaceConjunction(T8DataFilterClause.DataFilterConjunction.AND);
        dataFilter = parser.parseExpression(searchExpression, false);
        dataFilter.addFilterCriterion(entityId + F_OBJECT_ID, T8DataFilterCriterion.DataFilterOperator.IN, objectIds);
        dataFilter.addFilterCriterion(entityId + F_LANGUAGE_ID, T8DataFilterCriterion.DataFilterOperator.EQUAL, context.getLanguageId());

        // Add the input object filter if any criteria are present.
        if (objectFilter != null)
        {
            T8DataFilter additionalFilter;

            additionalFilter = objectFilter.getDataFilter(entityId, objectToEntityFieldMapping);
            dataFilter.setFieldOrdering(additionalFilter.getFieldOrdering());
            dataFilter.addFilterCriteria(DataFilterConjunction.AND, additionalFilter);
        }

        // Select entities from the view using the search filter.
        objects = new ArrayList<>();
        entities = tx.select(entityId, dataFilter, pageOffset, pageSize);
        for (T8DataEntity entity : entities)
        {
            T8TaskObjectHandler objectHandler;
            String objectId;

            objectId = (String)entity.getFieldValue(F_OBJECT_ID);
            objectHandler = (T8TaskObjectHandler)objApi.getObjectHandler(objectId);
            objects.add(objectHandler.mapObject(entity));
        }

        // Return the final list of objects, found using the search filter.
        return objects;
    }
}
