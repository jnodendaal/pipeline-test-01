package com.pilog.t8.data.database;

import com.pilog.t8.data.source.T8CommonStatementHandler;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bouwer du Preez
 */
public class T8SqlScriptExecutor
{
    private static final String DEFAULT_DELIMITER = ";";
    private static final String DELIMITER_LINE_REGEX = "(?i)DELIMITER.+";
    private static final String DELIMITER_LINE_SPLIT_REGEX = "(?i)DELIMITER";
    private final Connection connection;
    private final boolean stopOnError;
    private PrintWriter logWriter;
    private PrintWriter errorLogWriter;
    private String delimiter = DEFAULT_DELIMITER;
    private boolean fullLineDelimiter;
    private int lineNumber;

    public T8SqlScriptExecutor(Connection connection, boolean autoCommit, boolean stopOnError)
    {
        this.connection = connection;
        this.logWriter = new PrintWriter(System.out);
        this.errorLogWriter = new PrintWriter(System.err);
        this.stopOnError = stopOnError;
        this.fullLineDelimiter = false;
    }

    public void setDelimiter(String delimiter, boolean fullLineDelimiter)
    {
        println("Setting delimiter: " + delimiter);
        this.delimiter = delimiter;
        this.fullLineDelimiter = fullLineDelimiter;
    }

    public void setLogWriter(PrintWriter logWriter)
    {
        this.logWriter = logWriter;
    }

    public void setErrorLogWriter(PrintWriter errorLogWriter)
    {
        this.errorLogWriter = errorLogWriter;
    }

    public void executeScript(String script, Map<String, Object> parameters) throws IOException, SQLException
    {
        executeScript(connection, new StringReader(script), parameters);
    }

    public void executeScript(Reader reader, Map<String, Object> parameters) throws IOException, SQLException
    {
        executeScript(connection, reader, parameters);
    }

    public int getLineNumber()
    {
        return lineNumber;
    }

    private void executeScript(Connection connection, Reader reader, Map<String, Object> parameters) throws IOException, SQLException
    {
        StringBuffer command = null;

        try
        {
            LineNumberReader lineReader;
            String line = null;

            lineReader = new LineNumberReader(reader);
            while ((line = lineReader.readLine()) != null)
            {
                String trimmedLine;

                // Keep track of the current line number.
                lineNumber = lineReader.getLineNumber();

                // Create a new buffer to hold the next command if required.
                if (command == null)
                {
                    command = new StringBuffer();
                }

                // Trim the next line and handle it according to the type of text it contains.
                trimmedLine = line.trim();
                if (trimmedLine.startsWith("--"))
                {
                    println(trimmedLine);
                }
                else if (trimmedLine.startsWith("//"))
                {
                    println(trimmedLine);
                }
                else if (trimmedLine.length() < 1)
                {
                    // No statement - do nothing.
                }
                else if (trimmedLine.endsWith(getDelimiter()))
                {
                    Pattern pattern;
                    Matcher matcher;

                    pattern = Pattern.compile(DELIMITER_LINE_REGEX);
                    matcher = pattern.matcher(trimmedLine);
                    if (matcher.matches())
                    {
                        setDelimiter(trimmedLine.split(DELIMITER_LINE_SPLIT_REGEX)[1].trim(), fullLineDelimiter);

                        line = lineReader.readLine();
                        if (line == null)
                        {
                            break;
                        }

                        trimmedLine = line.trim();
                    }

                    // Append the String up to the next delimiter to the command buffer.
                    command.append(line.substring(0, line.lastIndexOf(getDelimiter())));
                    command.append(" ");

                    // Create the next statement and execute it.
                    try
                    {
                        String commandString;

                        // Get the command String from the buffer.
                        commandString = command.toString();
                        if (commandString.toUpperCase().startsWith("EXEC"))
                        {
                            // The next command is an invocation of a Stored Procedure, so use a callable statement to execute it.
                            commandString = commandString.replace("execute", "call");
                            commandString = commandString.replace("EXECUTE", "call");
                            commandString = commandString.replace("exec", "call");
                            commandString = commandString.replace("EXEC", "call");
                            commandString = "{" + commandString + "}";

                            // Execute a callable statement.
                            executeCallableStatement(connection, parseParameterizedString(commandString, parameters));
                        }
                        else
                        {
                            // Execute a normal statement.
                            executeStatement(connection, parseParameterizedString(command.toString(), parameters));
                        }

                        // Log the command that was executed.
                        println(command);
                    }
                    catch (SQLException e)
                    {
                        e.fillInStackTrace();
                        printlnError("Error executing: " + command);
                        printlnError(e);
                        if (stopOnError) throw e;
                    }

                    // Clear the command variable.
                    command = null;
                }
                else
                {
                    Pattern pattern;
                    Matcher matcher;

                    pattern = Pattern.compile(DELIMITER_LINE_REGEX);
                    matcher = pattern.matcher(trimmedLine);
                    if (matcher.matches())
                    {
                        setDelimiter(trimmedLine.split(DELIMITER_LINE_SPLIT_REGEX)[1].trim(), fullLineDelimiter);
                        line = lineReader.readLine();
                        if (line == null)
                        {
                            break;
                        }

                        trimmedLine = line.trim();
                    }

                    command.append(line);
                    command.append(" ");
                }
            }
        }
        catch (SQLException e)
        {
            e.fillInStackTrace();
            printlnError("Error executing: " + command);
            printlnError(e);
            throw e;
        }
        catch (IOException e)
        {
            e.fillInStackTrace();
            printlnError("Error executing: " + command);
            printlnError(e);
            throw e;
        }
        finally
        {
            flush();
        }
    }

    private ParameterizedString parseParameterizedString(String statement, Map<String, Object> parameters)
    {
        if (parameters == null)
        {
            return new ParameterizedString(statement);
        }
        else
        {
            List<Object> statementParameters;

            statementParameters = new ArrayList<>();
            for (String parameterId : parameters.keySet())
            {
                Object parameterValue;
                String searchString;
                String searchPattern;

                // Get the parameter value.
                parameterValue = parameters.get(parameterId);

                // First, do all regular string replacements.
                statement = statement.replace("<<-" + parameterId + "->>", "" + parameterValue);

                // Next, do SQL parameter replacement.
                searchString = "<<-?:" + parameterId + "->>";
                searchPattern = Pattern.quote(searchString);
                while (statement.contains(searchString))
                {
                    statement = statement.replaceFirst(searchPattern, "?");
                    statementParameters.add(parameterValue);
                }
            }

            // Return the statement with the parsed parameters in the correct sequence.
            return new ParameterizedString(statement, statementParameters);
        }
    }

    private void executeCallableStatement(Connection connection, ParameterizedString statementString) throws SQLException
    {
        PreparedStatement prepStatement = null;

        try
        {
            List<Object> insertParameters;
            boolean hasResults;

            // Create the prepared statement and set all of its paramters.
            insertParameters = statementString.getParameterValues();
            prepStatement = connection.prepareCall(statementString.getStringValue().toString());
            for (int parameterIndex = 0; parameterIndex < insertParameters.size(); parameterIndex++)
            {
                T8CommonStatementHandler.setPreparedStatementObject(prepStatement, parameterIndex + 1, insertParameters.get(parameterIndex));
            }

            // Execute the statement and process results if available.
            hasResults = prepStatement.execute();
            if (hasResults)
            {
                processResultSet(prepStatement);
            }
        }
        finally
        {
            // Close JDBC objects.
            if (prepStatement != null) prepStatement.close();
        }
    }

    private void executeStatement(Connection connection, ParameterizedString statementString) throws SQLException
    {
        PreparedStatement prepStatement = null;

        try
        {
            List<Object> insertParameters;
            boolean hasResults;

            // Create the prepared statement and set all of its paramters.
            insertParameters = statementString.getParameterValues();
            prepStatement = connection.prepareStatement(statementString.getStringValue().toString());
            for (int parameterIndex = 0; parameterIndex < insertParameters.size(); parameterIndex++)
            {
                T8CommonStatementHandler.setPreparedStatementObject(prepStatement, parameterIndex + 1, insertParameters.get(parameterIndex));
            }

            // Execute the statement and process results if available.
            hasResults = prepStatement.execute();
            if (hasResults)
            {
                processResultSet(prepStatement);
            }
        }
        finally
        {
            // Close JDBC objects.
            if (prepStatement != null) prepStatement.close();
        }
    }

    private void processResultSet(Statement statement) throws SQLException
    {
        ResultSet resultSet = null;

        try
        {
            // Get any results that might be available from the statement.
            resultSet = statement.getResultSet();
            if (resultSet != null)
            {
                ResultSetMetaData resultSetMetaData;
                int columnCount;

                resultSetMetaData = resultSet.getMetaData();
                columnCount = resultSetMetaData.getColumnCount();
                for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                {
                    String name = resultSetMetaData.getColumnLabel(columnIndex + 1);
                    print(name + "\t");
                }

                println("");
                while (resultSet.next())
                {
                    for (int i = 1; i <= columnCount; i++)
                    {
                        print(resultSet.getString(i) + "\t");
                    }

                    println("");
                }
            }
        }
        finally
        {
            if (resultSet != null) resultSet.close();
        }
    }

    private String getDelimiter()
    {
        return delimiter;
    }

    private void print(Object o)
    {
        if (logWriter != null)
        {
            logWriter.print(o);
        }
    }

    private void println(Object o)
    {
        if (logWriter != null)
        {
            logWriter.println(o);
        }
    }

    private void printlnError(Object o)
    {
        if (errorLogWriter != null)
        {
            errorLogWriter.println(o);
        }
    }

    private void flush()
    {
        if (logWriter != null)
        {
            logWriter.flush();
        }

        if (errorLogWriter != null)
        {
            errorLogWriter.flush();
        }
    }
}
