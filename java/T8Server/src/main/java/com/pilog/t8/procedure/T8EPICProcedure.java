package com.pilog.t8.procedure;

import com.pilog.t8.T8ServerContext;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.procedure.T8EPICProcedureDefinition;
import com.pilog.t8.security.T8Context;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8EPICProcedure implements T8Procedure
{
    private final T8EPICProcedureDefinition definition;
    private T8ServerContextScript script;
    private final T8ServerContext serverContext;
    private final T8Context context;

    public T8EPICProcedure(T8Context context, T8EPICProcedureDefinition definition)
    {
        this.serverContext = context.getServerContext();
        this.context = context;
        this.definition = definition;
    }

    @Override
    public void init()
    {
    }

    @Override
    public double getProgress()
    {
        return script != null ? script.getProgress() : -1;
    }

    @Override
    public T8ProgressReport getProgressReport()
    {
        return script != null ? script.getProgressReport() : null;
    }

    @Override
    public Map<String, Object> execute(T8Context accessContext, Map<String, Object> operationParameters) throws Exception
    {
        // Instantiate the script if we haven't done so yet.
        if (script == null)
        {
            script = definition.getNewScriptInstance(context);
            script.prepareScript();
        }

        // Execute the prepared script and return the results.
        return script.executePreparedScript(operationParameters);
    }

    @Override
    public void stop()
    {
        script.getScriptExecutionHandle().setStopped(true);
    }

    @Override
    public void cancel()
    {
        script.getScriptExecutionHandle().setCancelled(true);
    }

    @Override
    public void pause()
    {
        script.getScriptExecutionHandle().setPaused(true);
    }

    @Override
    public void resume()
    {
        script.getScriptExecutionHandle().setPaused(false);
    }
}
