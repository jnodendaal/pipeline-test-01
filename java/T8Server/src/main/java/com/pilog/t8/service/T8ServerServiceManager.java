package com.pilog.t8.service;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServiceManager;
import com.pilog.t8.definition.T8Definition;
import com.pilog.t8.definition.service.T8ServiceDefinition;
import com.pilog.t8.security.T8Context;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ServerServiceManager implements T8ServiceManager
{
    private final Map<String, T8Service> services; // Stores currently active services.
    private final T8ServerContext serverContext;

    public T8ServerServiceManager(T8ServerContext serverContext)
    {
        this.serverContext = serverContext;
        this.services = Collections.synchronizedMap(new HashMap<String, T8Service>());
    }

    @Override
    public void init() throws Exception
    {
        List<T8Definition> serviceDefinitions;

        serviceDefinitions = serverContext.getDefinitionManager().getInitializedGroupDefinitions(null, null, T8ServiceDefinition.GROUP_IDENTIFIER, null);
        for (T8Definition serviceDefinition : serviceDefinitions)
        {
            try
            {
                T8Service service;

                service = ((T8ServiceDefinition)serviceDefinition).getNewServiceInstance(serverContext);
                if (service != null)
                {
                    services.put(serviceDefinition.getIdentifier(), service);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while initializing T8Service: " + serviceDefinition, e);
            }
        }
    }

    @Override
    public void start() throws Exception
    {
        for (T8Service service : services.values())
        {
            service.init();
        }
    }

    @Override
    public void destroy()
    {
        // Destroy each service.
        for (T8Service service : services.values())
        {
            service.destroy();
        }

        // Clear the services cache.
        services.clear();
    }

    @Override
    public T8Service getService(T8Context context, String serviceIdentifier)
    {
        return services.get(serviceIdentifier);
    }

    @Override
    public Map<String, Object> executeServiceOperation(T8Context context, String serviceIdentifier, String serviceOperationIdentifier, Map<String, Object> operationParameters) throws Exception
    {
        T8Service service;

        service = getService(context, serviceIdentifier);
        if (service != null)
        {
            return service.executeOperation(context, serviceOperationIdentifier, operationParameters);
        }
        else throw new Exception("Service not found: " + serviceIdentifier);
    }
}
