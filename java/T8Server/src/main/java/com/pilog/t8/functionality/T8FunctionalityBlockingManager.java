package com.pilog.t8.functionality;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.data.filter.T8DataFilterClause.DataFilterConjunction;
import com.pilog.t8.data.filter.T8DataFilterCriteria;
import com.pilog.t8.data.filter.T8DataFilterCriterion;
import com.pilog.t8.data.filter.T8DataFilterCriterion.DataFilterOperator;
import com.pilog.t8.data.object.T8DataObject;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.translation.T8TranslatableString;
import com.pilog.t8.definition.functionality.T8FunctionalityManagerResource;
import com.pilog.t8.functionality.access.T8FunctionalityBlock;
import com.pilog.t8.utilities.collections.HashMaps;
import java.util.List;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer
 */
public class T8FunctionalityBlockingManager
{
    private final T8ServerContext serverContext;
    private final T8Context internalContext;
    private final T8DefinitionManager definitionManager;

    public T8FunctionalityBlockingManager(T8Context context)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.definitionManager = serverContext.getDefinitionManager();
    }

    public void init() throws Exception
    {
    }

    public synchronized void clearBlocks() throws Exception
    {
        T8DataTransaction tx;
        T8DataSession dataSession;
        String entityIdentifier;

        // Retrieve the entity.
        entityIdentifier = T8FunctionalityManagerResource.STATE_FUNC_BLOCK_DE_IDENTIFIER;
        dataSession = serverContext.getDataManager().getCurrentSession();
        tx = dataSession.instantTransaction();
        tx.delete(entityIdentifier, (T8DataFilter)null);
    }

    public synchronized void removeFunctionalityBlocks(T8DataTransaction tx, String blockingFunctionalityIid) throws Exception
    {
        String entityId;

        // Retrieve the entity.
        entityId = T8FunctionalityManagerResource.STATE_FUNC_BLOCK_DE_IDENTIFIER;
        tx.delete(entityId, new T8DataFilter(entityId, HashMaps.newHashMap(entityId + "$BLOCKING_FUNCTIONALITY_IID", blockingFunctionalityIid)));
    }

    public synchronized void addFunctionalityBlocks(String blockingFunctionalityIID, List<T8FunctionalityBlock> blockList) throws Exception
    {
        T8DataTransaction xtx;
        T8DataTransaction tx;
        T8DataSession dataSession;

        // Get the transaction to use (suspend any external transaction because we want this operation to run in its own transaction unaffected by external influences).
        dataSession = serverContext.getDataManager().getCurrentSession();
        xtx = dataSession.suspend();
        tx = dataSession.beginTransaction();

        try
        {
            for (T8FunctionalityBlock block : blockList)
            {
                T8DataEntity entity;
                String entityId;

                // Create a new block entity.
                entityId = T8FunctionalityManagerResource.STATE_FUNC_BLOCK_DE_IDENTIFIER;
                entity = tx.create(entityId, null);
                entity.setFieldValue(entityId + "$BLOCK_IID", T8IdentifierUtilities.createNewGUID());
                entity.setFieldValue(entityId + "$BLOCKING_FUNCTIONALITY_IID", blockingFunctionalityIID);
                entity.setFieldValue(entityId + "$BLOCKED_FUNCTIONALITY_ID", block.getFunctionalityIdentifier());
                entity.setFieldValue(entityId + "$BLOCKED_ROOT_ORG_ID", block.getRootOrgID());
                entity.setFieldValue(entityId + "$BLOCKED_DATA_OBJECT_ID", block.getDataObjectIdentifier());
                entity.setFieldValue(entityId + "$BLOCKED_DATA_OBJECT_IID", block.getDataObjectKey());
                entity.setFieldValue(entityId + "$BLOCKED_USER_ID", block.getUserIdentifier());
                entity.setFieldValue(entityId + "$BLOCKED_PROFILE_ID", block.getProfileIdentifier());
                entity.setFieldValue(entityId + "$BLOCKED_MESSAGE", block.getMessage().getStringValue().toString());

                // Insert the entity.
                tx.insert(entity);
            }

            // Commit the transaction.
            tx.commit();
        }
        catch (Exception e)
        {
            tx.rollback();
            throw e;
        }
        finally
        {
            // If an external session was present, resume it.
            if (xtx != null) dataSession.resume(xtx);
        }
    }

    public synchronized T8FunctionalityBlock getFunctionalityBlock(T8Context context, String blockedFunctionalityId, List<T8DataObject> dataObjects) throws Exception
    {
        T8SessionContext sessionContext;
        T8DataSession dataSession;
        T8DataTransaction tx;
        List<T8DataEntity> entityList;
        String entityId;
        T8DataFilter filter;
        T8DataFilterCriteria filterCriteria;
        T8DataFilterCriteria userFilterCriteria;
        T8DataFilterCriteria profileFilterCriteria;
        T8DataFilterCriteria objectFilterCriteria;

        // Retrieve the entity.
        sessionContext = context.getSessionContext();
        entityId = T8FunctionalityManagerResource.STATE_FUNC_BLOCK_DE_IDENTIFIER;
        dataSession = serverContext.getDataManager().getCurrentSession(); // Very important to use the internal session and not the user's.
        tx = dataSession.instantTransaction();

        // Create the new filter object.
        filterCriteria = new T8DataFilterCriteria();

        // Add the root organization ID.
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_ROOT_ORG_ID", DataFilterOperator.EQUAL, sessionContext.getRootOrganizationIdentifier()));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_FUNCTIONALITY_ID", DataFilterOperator.EQUAL, blockedFunctionalityId));

        // Create the user filter criteria.
        userFilterCriteria = new T8DataFilterCriteria();
        userFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_USER_ID", DataFilterOperator.EQUAL, sessionContext.getUserIdentifier()));
        userFilterCriteria.addFilterClause(DataFilterConjunction.OR, new T8DataFilterCriterion(entityId + "$BLOCKED_USER_ID", DataFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, userFilterCriteria);

        // Create the user filter criteria.
        profileFilterCriteria = new T8DataFilterCriteria();
        profileFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_PROFILE_ID", DataFilterOperator.EQUAL, sessionContext.getUserProfileIdentifier()));
        profileFilterCriteria.addFilterClause(DataFilterConjunction.OR, new T8DataFilterCriterion(entityId + "$BLOCKED_PROFILE_ID", DataFilterOperator.IS_NULL, null));
        filterCriteria.addFilterClause(DataFilterConjunction.AND, profileFilterCriteria);

        // If the data object is not null, add its details to the filter.
        objectFilterCriteria = new T8DataFilterCriteria();
        filterCriteria.addFilterClause(DataFilterConjunction.AND, objectFilterCriteria);
        if (!dataObjects.isEmpty())
        {
            T8DataFilterCriteria noObjectFilterCriteria;

            for (T8DataObject dataObject : dataObjects)
            {
                T8DataFilterCriteria specificObjectFilterCriteria;

                specificObjectFilterCriteria = new T8DataFilterCriteria();
                specificObjectFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_DATA_OBJECT_ID", DataFilterOperator.EQUAL, dataObject.getId()));
                specificObjectFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_DATA_OBJECT_IID", DataFilterOperator.EQUAL, dataObject.getKey()));
                objectFilterCriteria.addFilterClause(DataFilterConjunction.OR, specificObjectFilterCriteria);
            }

            noObjectFilterCriteria = new T8DataFilterCriteria();
            noObjectFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_DATA_OBJECT_ID", DataFilterOperator.IS_NULL, null));
            noObjectFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_DATA_OBJECT_IID", DataFilterOperator.IS_NULL, null));
            objectFilterCriteria.addFilterClause(DataFilterConjunction.OR, noObjectFilterCriteria);
        }
        else
        {
            T8DataFilterCriteria noObjectFilterCriteria;

            noObjectFilterCriteria = new T8DataFilterCriteria();
            noObjectFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_DATA_OBJECT_ID", DataFilterOperator.IS_NULL, null));
            noObjectFilterCriteria.addFilterClause(DataFilterConjunction.AND, new T8DataFilterCriterion(entityId + "$BLOCKED_DATA_OBJECT_IID", DataFilterOperator.IS_NULL, null));
            objectFilterCriteria.addFilterClause(DataFilterConjunction.AND, noObjectFilterCriteria);
        }

        // Select the first entity that matches the filter criteria.
        filter = new T8DataFilter(entityId, filterCriteria);
        entityList = tx.select(entityId, filter, 0, 1);
        if (entityList.size() > 0)
        {
            T8DataEntity entity;
            String functionalityID;
            String dataObjectID;
            String dataObjectIID;
            String rootOrgID;
            String orgID;
            String userID;
            String profileID;
            String message;

            entity = entityList.get(0);
            functionalityID = (String)entity.getFieldValue(entityId + "$BLOCKED_FUNCTIONALITY_ID");
            dataObjectID = (String)entity.getFieldValue(entityId + "$BLOCKED_DATA_OBJECT_ID");
            dataObjectIID = (String)entity.getFieldValue(entityId + "$BLOCKED_DATA_OBJECT_IID");
            rootOrgID = (String)entity.getFieldValue(entityId + "$BLOCKED_ROOT_ORG_ID");
            orgID = (String)entity.getFieldValue(entityId + "$BLOCKED_ORG_ID");
            userID = (String)entity.getFieldValue(entityId + "$BLOCKED_USER_ID");
            profileID = (String)entity.getFieldValue(entityId + "$BLOCKED_PROFILE_ID");
            message = (String)entity.getFieldValue(entityId + "$BLOCKED_MESSAGE");

            return new T8FunctionalityBlock(rootOrgID, orgID, userID, profileID, functionalityID, dataObjectID, dataObjectIID, new T8TranslatableString(message));
        }
        else return null;
    }
}
