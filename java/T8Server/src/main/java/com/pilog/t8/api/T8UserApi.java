package com.pilog.t8.api;

import com.pilog.t8.T8DefinitionManager;
import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.user.T8UserDefinition;
import com.pilog.t8.log.T8Logger;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatisticsProvider;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8UserApi implements T8Api, T8PerformanceStatisticsProvider
{
    private static final T8Logger LOGGER = T8Log.getLogger(T8UserApi.class);

    private final T8DataTransaction tx;
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private T8PerformanceStatistics stats;

    public static final String API_IDENTIFIER = "@API_USER";

    public T8UserApi(T8DataTransaction tx) throws Exception
    {
        this.tx = tx;
        this.context = tx.getContext();
        this.sessionContext = context.getSessionContext();
        this.serverContext = context.getServerContext();
        this.stats = tx.getPerformanceStatistics();
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    public T8ServerContext getServerContext()
    {
        return serverContext;
    }

    public T8SessionContext getSessionContext()
    {
        return sessionContext;
    }

    /**
     * Returns the display name for the specified user, as per system configuration.
     * If the user does not exist or no configuration for the display name is available, the input user id is returned.
     * @param userId The id of the user for which to return the display name.
     * @return The display name of the specified user.
     */
    public String getUserDisplayName(String userId)
    {
        try
        {
            T8DefinitionManager definitionManager;
            T8UserDefinition userDefinition;

            definitionManager = serverContext.getDefinitionManager();
            userDefinition = definitionManager.getRawDefinition(context, null, userId);
            return userDefinition != null ? userDefinition.getMetaDisplayName() : userId;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
