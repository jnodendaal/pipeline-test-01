package com.pilog.t8.functionality;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.data.entity.T8FunctionalityDataObjectDefinition;
import com.pilog.t8.script.T8ExpressionEvaluator;
import com.pilog.t8.script.T8Script;
import com.pilog.t8.ui.functionality.T8TargetObjectValidationResult;
import com.pilog.t8.ui.functionality.event.T8FunctionalityInstanceEndedEvent;
import com.pilog.t8.ui.functionality.event.T8FunctionalityInstanceListener;
import com.pilog.t8.definition.functionality.T8FunctionalityDefinition;
import com.pilog.t8.definition.script.T8DataObjectValidationScriptDefinition;
import com.pilog.t8.functionality.state.T8FunctionalityState;
import com.pilog.t8.functionality.state.T8FunctionalityDataObjectState;
import com.pilog.t8.script.T8ServerExpressionEvaluator;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.security.T8Context.T8FunctionalityContext;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.utilities.strings.Strings;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.event.EventListenerList;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultFunctionalityInstance implements T8FunctionalityInstance
{
    protected final String id;
    protected final String iid;
    protected final String description;
    protected final String displayNameExpression;
    protected final Icon icon;
    protected final String objectNotFoundMessage;
    protected final String confirmationMessage;
    protected final EventListenerList listeners;
    protected final T8DataObjectValidationScriptDefinition objectValidationScriptDefinition;
    protected final T8DataObjectValidationScriptDefinition secondaryValidationScriptDefinition;
    protected final T8FunctionalityDefinition functionalityDefinition;
    protected final T8Context context;
    protected final T8ServerContext serverContext;
    protected final T8SessionContext sessionContext;
    protected T8Script targetObjectValidationScript;
    protected T8Script validationScript;
    protected T8ExpressionEvaluator expressionEvaluator;
    protected T8Timestamp timeAccessed;

    public T8DefaultFunctionalityInstance(T8Context context, T8FunctionalityDefinition functionalityDefinition, String iid)
    {
        this.functionalityDefinition = functionalityDefinition;
        this.context = new T8FunctionalityContext(context, functionalityDefinition.getRootProjectId(), functionalityDefinition.getIdentifier(), iid);
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.id = functionalityDefinition.getIdentifier();
        this.iid = iid;
        this.displayNameExpression = functionalityDefinition.getDisplayNameExpression();
        this.description = functionalityDefinition.getFunctionalityDescription();
        this.icon = functionalityDefinition.getIcon();
        this.objectNotFoundMessage = functionalityDefinition.getTargetObjectNotFoundMessage();
        this.confirmationMessage = functionalityDefinition.getConfirmationMessage();
        this.objectValidationScriptDefinition = functionalityDefinition.getTargetObjectValidationScriptDefinition();
        this.secondaryValidationScriptDefinition = functionalityDefinition.getValidationScriptDefinition();
        this.listeners = new EventListenerList();
        initializeHandle();
    }

    private void initializeHandle()
    {
        try
        {
            // Create an expression evaluator.
            expressionEvaluator = new T8ServerExpressionEvaluator(context);

            // Create the primary validation script.
            if ((objectValidationScriptDefinition != null) && (objectValidationScriptDefinition.containsScript()))
            {
                targetObjectValidationScript = objectValidationScriptDefinition.getNewScriptInstance(context);
            }
            else targetObjectValidationScript = null;

            // Create the secondary validation script.
            if ((secondaryValidationScriptDefinition != null) && (secondaryValidationScriptDefinition.containsScript()))
            {
                validationScript = secondaryValidationScriptDefinition.getNewScriptInstance(context);
            }
            else validationScript = null;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Exception while constructing functionality instance: " + id, e);
        }
    }

    @Override
    public String getFunctionalityId()
    {
        return id;
    }

    @Override
    public String getFunctionalityIid()
    {
        return iid;
    }

    protected String getDisplayName()
    {
        if (!Strings.isNullOrEmpty(displayNameExpression))
        {
            try
            {
                return (String)expressionEvaluator.evaluateExpression(displayNameExpression, null, null);
            }
            catch (Exception ex)
            {
                T8Log.log("Failed to evaluate functionality '" + id + "' display name expression: " + displayNameExpression, ex);
                return "Error";
            }
        }
        else return id;
    }

    protected String getDescription()
    {
        return description;
    }

    protected Icon getIcon()
    {
        return icon;
    }

    @Override
    public T8Timestamp getTimeAccessed()
    {
        return timeAccessed;
    }

    @Override
    public T8TargetObjectValidationResult validateTargetObjects(Map<String, Object> functionalityParameters)
    {
        // Perform primary validation.
        if (targetObjectValidationScript != null)
        {
            try
            {
                Map<String, Object> validationOutputParameters;
                String validationErrorMessage;
                Boolean valid;

                validationOutputParameters = targetObjectValidationScript.executeScript(functionalityParameters);
                valid = (Boolean)validationOutputParameters.get(id + T8DataObjectValidationScriptDefinition.PARAMETER_VALID);
                validationErrorMessage = (String)validationOutputParameters.get(id + T8DataObjectValidationScriptDefinition.PARAMETER_VALIDATION_ERROR_MESSAGE);
                if ((valid != null) && (!valid))
                {
                    return new T8TargetObjectValidationResult(false, validationErrorMessage);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while executing target object validation script: " + objectValidationScriptDefinition, e);
                return new T8TargetObjectValidationResult(false, "Unexpected Error Prevent Functionality Invocation.");
            }
        }

        // Perform secondary validation.
        if (validationScript != null)
        {
            try
            {
                Map<String, Object> validationOutputParameters;
                String validationErrorMessage;
                Boolean valid;

                validationOutputParameters = validationScript.executeScript(functionalityParameters);
                valid = (Boolean)validationOutputParameters.get(id + T8DataObjectValidationScriptDefinition.PARAMETER_VALID);
                validationErrorMessage = (String)validationOutputParameters.get(id + T8DataObjectValidationScriptDefinition.PARAMETER_VALIDATION_ERROR_MESSAGE);
                if ((valid != null) && (!valid))
                {
                    return new T8TargetObjectValidationResult(false, validationErrorMessage);
                }
            }
            catch (Exception e)
            {
                T8Log.log("Exception while executing secondary validation script: " + secondaryValidationScriptDefinition, e);
                return new T8TargetObjectValidationResult(false, "Unexpected Error Prevent Functionality Invocation.");
            }
        }

        // No validation failure, so return success flag.
        return new T8TargetObjectValidationResult(true, null);
    }

    @Override
    public void addFunctionalityInstanceListener(T8FunctionalityInstanceListener listener)
    {
        listeners.add(T8FunctionalityInstanceListener.class, listener);
    }

    @Override
    public void removeFunctionalityInstanceListener(T8FunctionalityInstanceListener listener)
    {
        listeners.remove(T8FunctionalityInstanceListener.class, listener);
    }

    protected T8FunctionalityState getFunctionalityState(Map<String, Object> inputParameters)
    {
        T8FunctionalityState state;

        // Create the new state.
        state = new T8FunctionalityState(iid, id);
        state.setInputParameters(T8IdentifierUtilities.stripNamespace(inputParameters));
        state.setTimeAccessed(timeAccessed);

        // Add data object details if a target object is available.
        if (inputParameters != null)
        {
            for (T8FunctionalityDataObjectDefinition objectDefinition : functionalityDefinition.getDataObjects())
            {
                String inputParameterId;

                inputParameterId = objectDefinition.getFunctionalityInputParameterId();
                if (inputParameterId != null)
                {
                    String objectIid;

                    objectIid = (String)inputParameters.get(id + inputParameterId);
                    if (objectIid != null)
                    {
                        state.addDataObjectState(new T8FunctionalityDataObjectState(iid, objectDefinition.getDataObjectId(), objectIid));
                    }
                }
            }
        }

        // Add initiator details.
        if (sessionContext.isSystemSession())
        {
            state.setInitiatorId(sessionContext.getSystemAgentIdentifier());
            state.setInitiatorIid(sessionContext.getSystemAgentInstanceIdentifier());
        }
        else
        {
            state.setInitiatorId(sessionContext.getUserIdentifier());
            state.setInitiatorIid(null);
        }

        // Return the state.
        return state;
    }

    protected void fireFunctionalityEndedEvent()
    {
        T8FunctionalityInstanceEndedEvent event;

        event = new T8FunctionalityInstanceEndedEvent(this);
        for (T8FunctionalityInstanceListener listener : listeners.getListeners(T8FunctionalityInstanceListener.class))
        {
            listener.functionalityEnded(event);
        }
    }
}
