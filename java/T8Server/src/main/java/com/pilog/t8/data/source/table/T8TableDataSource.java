package com.pilog.t8.data.source.table;

import com.pilog.t8.T8SessionContext;
import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataEntityResults;
import com.pilog.t8.data.T8SQLBasedDataSource;
import com.pilog.t8.data.filter.T8DataFilter;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.performance.T8DefaultPerformanceStatistics;
import com.pilog.t8.performance.T8PerformanceStatistics;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.source.DataPersistenceUtilities;
import com.pilog.t8.data.source.T8CommonStatementHandler;
import com.pilog.t8.definition.data.entity.T8DataEntityDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.definition.data.source.table.T8TableDataSourceDefinition;
import com.pilog.t8.utilities.sql.BatchParameterizedString;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.security.T8Context;

/**
 * @author Bouwer du Preez
 */
public class T8TableDataSource implements T8SQLBasedDataSource
{
    protected T8Context context;
    protected T8DataTransaction tx;
    protected final T8TableDataSourceDefinition definition;
    protected T8DataConnection connection;
    protected final List<T8DataEntityResults> openEntityResults;
    protected T8PerformanceStatistics stats;

    public static final String UPDATED_BY_ID_FIELD_IDENTIFIER = "$UPDATED_BY_ID";
    public static final String UPDATED_BY_IID_FIELD_IDENTIFIER = "$UPDATED_BY_IID";
    public static final String UPDATED_AT_FIELD_IDENTIFIER = "$UPDATED_AT";
    public static final String INSERTED_BY_ID_FIELD_IDENTIFIER = "$INSERTED_BY_ID";
    public static final String INSERTED_BY_IID_FIELD_IDENTIFIER = "$INSERTED_BY_IID";
    public static final String INSERTED_AT_FIELD_IDENTIFIER = "$INSERTED_AT";

    public T8TableDataSource(T8TableDataSourceDefinition definition, T8DataTransaction tx)
    {
        this.definition = definition;
        this.tx = tx;
        this.openEntityResults = new ArrayList<T8DataEntityResults>();
        this.context = tx.getContext();
        this.stats = new T8DefaultPerformanceStatistics(this.getClass().getSimpleName());
        this.stats.setEnabled(false); // The default behaviour is alwasy false.  Other clients of the API can set this variable to their own instance of stats.

        // Do a validity check.
        if (context == null) throw new RuntimeException("Null session context obtained from data session.");
    }

    @Override
    public T8TableDataSourceDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public void setPerformanceStatisticsEnabled(boolean enabled)
    {
        stats.setEnabled(enabled);
    }

    @Override
    public void setPerformanceStatistics(T8PerformanceStatistics statistics)
    {
        if (statistics == null) throw new IllegalArgumentException("Cannot set performance statistics as null.");
        this.stats = statistics;
    }

    @Override
    public T8PerformanceStatistics getPerformanceStatistics()
    {
        return stats;
    }

    @Override
    public void open() throws Exception
    {
        connection = tx.getDataConnection(definition.getConnectionIdentifier());
    }

    @Override
    public void close() throws Exception
    {
        // Close any open entity results.
        for (T8DataEntityResults results : openEntityResults)
        {
            results.close();
        }
    }

    @Override
    public void commit() throws Exception
    {
        if (connection != null) connection.commit();
    }

    @Override
    public void rollback() throws Exception
    {
        if (connection != null) connection.rollback();
    }

    @Override
    public void setParameters(Map<String, Object> parameters)
    {
    }

    @Override
    public List<T8DataSourceFieldDefinition> retrieveFieldDefinitions() throws Exception
    {
        ParameterizedString queryString;

        queryString = new ParameterizedString("SELECT * FROM ");
        queryString.append(definition.getTableName());
        queryString.append(" WHERE 1=0");

        return T8CommonStatementHandler.getQueryFieldDefinitions(context, connection, queryString);
    }

    @Override
    public int count(T8DataFilter filter) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = tx.getDataEntityDefinition(filter.getEntityIdentifier());
        queryString = getCountQueryString(entityDefinition, filter, true);
        return T8CommonStatementHandler.executeCountQuery(context, connection, queryString, definition, entityDefinition, 0);
    }

    @Override
    public boolean exists(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return count(new T8DataFilter(entityIdentifier, keyMap)) > 0;
    }

    @Override
    public void insert(T8DataEntity dataEntity) throws Exception
    {
        T8SessionContext sessionContext;
        ParameterizedString insertString;
        T8DataEntityDefinition entityDefinition;
        Map<String, Object> dataSourceValues;
        Map<String, Object> sourceValues;
        String insertedByIDSourceIdentifier;
        String insertedByIIDSourceIdentifier;
        String insertedAtSourceIdentifier;
        String insertedByID;
        String insertedByIID;
        T8Timestamp insertedAt;

        // Get auditing values.
        sessionContext = context.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            insertedByID = sessionContext.getSystemAgentIdentifier();
            insertedByIID = sessionContext.getSystemAgentInstanceIdentifier();
            insertedAt = new T8Timestamp(System.currentTimeMillis());
        }
        else
        {
            insertedByID = sessionContext.getUserIdentifier();
            insertedByIID = null;
            insertedAt = new T8Timestamp(System.currentTimeMillis());
        }

        // Get the entity definition and from it map all of the required field value.
        entityDefinition = dataEntity.getDefinition();
        dataSourceValues = T8IdentifierUtilities.mapParameters(dataEntity.getFieldValues(), entityDefinition.getFieldToSourceIdentifierMapping());
        sourceValues = T8IdentifierUtilities.mapParameters(dataSourceValues, definition.getFieldToSourceIdentifierMapping());

        // Add auditing data.
        insertedByIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + INSERTED_BY_ID_FIELD_IDENTIFIER);
        if (insertedByIDSourceIdentifier != null) dataSourceValues.put(insertedByIDSourceIdentifier, insertedByID); // Add to data source value map.
        insertedByIDSourceIdentifier = definition.mapFieldToSourceIdentifier(insertedByIDSourceIdentifier);
        if (insertedByIDSourceIdentifier != null) sourceValues.put(insertedByIDSourceIdentifier, insertedByID); // Add to source value map.
        insertedByIIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + INSERTED_BY_IID_FIELD_IDENTIFIER);
        if (insertedByIIDSourceIdentifier != null) dataSourceValues.put(insertedByIIDSourceIdentifier, insertedByIID); // Add to data source value map.
        insertedByIIDSourceIdentifier = definition.mapFieldToSourceIdentifier(insertedByIIDSourceIdentifier);
        if (insertedByIIDSourceIdentifier != null) sourceValues.put(insertedByIIDSourceIdentifier, insertedByIID); // Add to source value map.
        insertedAtSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + INSERTED_AT_FIELD_IDENTIFIER);
        if (insertedAtSourceIdentifier != null) dataSourceValues.put(insertedAtSourceIdentifier, insertedAt); // Add to data source value map.
        insertedAtSourceIdentifier = definition.mapFieldToSourceIdentifier(insertedAtSourceIdentifier);
        if (insertedAtSourceIdentifier != null) sourceValues.put(insertedAtSourceIdentifier, insertedAt); // Add to source value map.

        // Construct the statement String.
        insertString = new ParameterizedString("-- ");
        insertString.append(entityDefinition.getIdentifier());
        insertString.append("\nINSERT INTO ");
        insertString.append(definition.getTableName());
        insertString.append(" (");
        insertString.append(DataPersistenceUtilities.buildColumnNameCSV(connection, new ArrayList<String>(sourceValues.keySet())));
        insertString.append(") VALUES (");
        insertString.append(DataPersistenceUtilities.buildParameterCSV(connection, definition, dataSourceValues));
        insertString.append(")");


        T8CommonStatementHandler.executeUpdate(context, connection, insertString, definition);
    }

    @Override
    public void insert(List<T8DataEntity> dataEntities) throws Exception
    {
        BatchParameterizedString insertString = null;
        T8DataEntityDefinition entityDefinition;
        Map<String, Object> dataSourceValues;
        Map<String, Object> sourceValues;
        String insertedByIDSourceIdentifier;
        String insertedByIIDSourceIdentifier;
        String insertedAtSourceIdentifier;
        T8SessionContext sessionContext;
        String insertedByID;
        String insertedByIID;
        T8Timestamp insertedAt;

        // Get auditing values.
        sessionContext = context.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            insertedByID = sessionContext.getSystemAgentIdentifier();
            insertedByIID = sessionContext.getSystemAgentInstanceIdentifier();
            insertedAt = new T8Timestamp(System.currentTimeMillis());
        }
        else
        {
            insertedByID = sessionContext.getUserIdentifier();
            insertedByIID = null;
            insertedAt = new T8Timestamp(System.currentTimeMillis());
        }

        for (T8DataEntity dataEntity : dataEntities)
        {
            // Get the entity definition and from it map all of the required field value.
            entityDefinition = dataEntity.getDefinition();
            dataSourceValues = T8IdentifierUtilities.mapParameters(dataEntity.getFieldValues(), entityDefinition.getFieldToSourceIdentifierMapping());
            sourceValues = T8IdentifierUtilities.mapParameters(dataSourceValues, definition.getFieldToSourceIdentifierMapping());

            // Add auditing data.
            insertedByIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + INSERTED_BY_ID_FIELD_IDENTIFIER);
            if (insertedByIDSourceIdentifier != null) dataSourceValues.put(insertedByIDSourceIdentifier, insertedByID); // Add to data source value map.
            insertedByIDSourceIdentifier = definition.mapFieldToSourceIdentifier(insertedByIDSourceIdentifier);
            if (insertedByIDSourceIdentifier != null) sourceValues.put(insertedByIDSourceIdentifier, insertedByID); // Add to source value map.
            insertedByIIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + INSERTED_BY_IID_FIELD_IDENTIFIER);
            if (insertedByIIDSourceIdentifier != null) dataSourceValues.put(insertedByIIDSourceIdentifier, insertedByIID); // Add to data source value map.
            insertedByIIDSourceIdentifier = definition.mapFieldToSourceIdentifier(insertedByIIDSourceIdentifier);
            if (insertedByIIDSourceIdentifier != null) sourceValues.put(insertedByIIDSourceIdentifier, insertedByIID); // Add to source value map.
            insertedAtSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + INSERTED_AT_FIELD_IDENTIFIER);
            if (insertedAtSourceIdentifier != null) dataSourceValues.put(insertedAtSourceIdentifier, insertedAt); // Add to data source value map.
            insertedAtSourceIdentifier = definition.mapFieldToSourceIdentifier(insertedAtSourceIdentifier);
            if (insertedAtSourceIdentifier != null) sourceValues.put(insertedAtSourceIdentifier, insertedAt); // Add to source value map.

            // Construct the statement String.
            if(insertString == null)
            {
                insertString = new BatchParameterizedString("-- ");
                insertString.append(entityDefinition.getIdentifier());
                insertString.append("\nINSERT INTO ");
                insertString.append(definition.getTableName());
                insertString.append(" (");
                insertString.append(DataPersistenceUtilities.buildColumnNameCSV(connection, new ArrayList<String>(sourceValues.keySet())));
                insertString.append(") VALUES (");
                insertString.append(DataPersistenceUtilities.buildParameterCSV(connection, definition, dataSourceValues));
                insertString.append(")");

            }
            else
            {
                //check for GUID's
                DataPersistenceUtilities.convertGuidValues(connection, definition, dataSourceValues);
                insertString.addBatch(dataSourceValues.values());
            }
        }

        T8CommonStatementHandler.executeUpdate(context, connection, insertString, definition);
    }

    @Override
    public T8DataEntity retrieve(String entityIdentifier, Map<String, Object> keyValues) throws Exception
    {
        ParameterizedString queryString;
        ArrayList<T8DataEntity> selectedObjects;
        Map<String, Object> keySourceValues;
        T8DataEntityDefinition entityDefinition;

        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);
        keySourceValues = T8IdentifierUtilities.mapParameters(keyValues, entityDefinition.getFieldToSourceIdentifierMapping(), definition.getFieldToSourceIdentifierMapping());

        queryString = new ParameterizedString("-- ");
        queryString.append(entityDefinition.getIdentifier());
        queryString.append("\nSELECT * FROM ");
        queryString.append(definition.getTableName());
        queryString.append(" ");
        queryString.append(DataPersistenceUtilities.buildWhereClause(connection, definition, definition.getSourceToFieldIdentifierMapping(), keySourceValues));


        selectedObjects = T8CommonStatementHandler.executeQuery(context, connection, queryString, definition, entityDefinition, 0, 1, 0);
        if (selectedObjects.size() > 0)
        {
            return selectedObjects.get(0);
        }
        else return null;
    }

    @Override
    public boolean update(T8DataEntity dataEntity) throws Exception
    {
        ParameterizedString updateString;
        ParameterizedString whereClause;
        T8DataEntityDefinition entityDefinition;
        Map<String, Object> nonKeySourceValues;
        Map<String, Object> keySourceValues;
        String updatedByIDSourceIdentifier;
        String updatedByIIDSourceIdentifier;
        String updatedAtSourceIdentifier;
        T8SessionContext sessionContext;
        String updatedByID;
        String updatedByIID;
        T8Timestamp updatedAt;

        // Get auditing values.
        sessionContext = context.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            updatedByID = sessionContext.getSystemAgentIdentifier();
            updatedByIID = sessionContext.getSystemAgentInstanceIdentifier();
            updatedAt = new T8Timestamp(System.currentTimeMillis());
        }
        else
        {
            updatedByID = sessionContext.getUserIdentifier();
            updatedByIID = null;
            updatedAt = new T8Timestamp(System.currentTimeMillis());
        }

        // Get the entity definition and from it map all of the required field value.
        entityDefinition = dataEntity.getDefinition();
        nonKeySourceValues = T8IdentifierUtilities.mapParameters(dataEntity.getNonKeyFieldValues(), entityDefinition.getFieldToSourceIdentifierMapping(), definition.getFieldToSourceIdentifierMapping());
        keySourceValues = T8IdentifierUtilities.mapParameters(dataEntity.getKeyFieldValues(), entityDefinition.getFieldToSourceIdentifierMapping(), definition.getFieldToSourceIdentifierMapping());

        // Add auditing data.
        updatedByIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + UPDATED_BY_ID_FIELD_IDENTIFIER);
        updatedByIDSourceIdentifier = definition.mapFieldToSourceIdentifier(updatedByIDSourceIdentifier);
        updatedByIIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + UPDATED_BY_IID_FIELD_IDENTIFIER);
        updatedByIIDSourceIdentifier = definition.mapFieldToSourceIdentifier(updatedByIIDSourceIdentifier);
        updatedAtSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + UPDATED_AT_FIELD_IDENTIFIER);
        updatedAtSourceIdentifier = definition.mapFieldToSourceIdentifier(updatedAtSourceIdentifier);
        if (updatedByIDSourceIdentifier != null) nonKeySourceValues.put(updatedByIDSourceIdentifier, updatedByID);
        if (updatedByIIDSourceIdentifier != null) nonKeySourceValues.put(updatedByIIDSourceIdentifier, updatedByIID);
        if (updatedAtSourceIdentifier != null) nonKeySourceValues.put(updatedAtSourceIdentifier, updatedAt);

        // Construct a WHERE clause.
        whereClause = DataPersistenceUtilities.buildWhereClause(connection, definition, definition.getSourceToFieldIdentifierMapping(), keySourceValues);

        // Construct the updated String.
        updateString = new ParameterizedString("-- ");
        updateString.append(entityDefinition.getIdentifier());
        updateString.append("\nUPDATE ");
        updateString.append(definition.getTableName());
        updateString.append(" ");
        updateString.append(DataPersistenceUtilities.buildUpdateSetClause(connection, definition, definition.getSourceToFieldIdentifierMapping(), nonKeySourceValues));
        updateString.append(" ");
        updateString.append(whereClause);

        // Execute the update and if any records were affected, return true.
        return T8CommonStatementHandler.executeUpdate(context, connection, updateString, definition) > 0;
    }

    @Override
    public boolean update(List<T8DataEntity> dataEntities) throws Exception
    {
        BatchParameterizedString updateString = null;
        T8DataEntityDefinition entityDefinition;
        Map<String, Object> dataSourceKeyValues;
        Map<String, Object> dataSourceNonKeyValues;
        String updatedByIDSourceIdentifier;
        String updatedByIIDSourceIdentifier;
        String updatedAtSourceIdentifier;
        T8SessionContext sessionContext;
        String updatedByID;
        String updatedByIID;
        T8Timestamp updatedAt;
        Set<String> populatedFields = null;

        // Get auditing values.
        sessionContext = context.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            updatedByID = sessionContext.getSystemAgentIdentifier();
            updatedByIID = sessionContext.getSystemAgentInstanceIdentifier();
            updatedAt = new T8Timestamp(System.currentTimeMillis());
        }
        else
        {
            updatedByID = sessionContext.getUserIdentifier();
            updatedByIID = null;
            updatedAt = new T8Timestamp(System.currentTimeMillis());
        }

        // Update all entities in the list.
        for (T8DataEntity dataEntity : dataEntities)
        {
            // Get the entity definition and from it map all of the required field value.
            entityDefinition = dataEntity.getDefinition();
            dataSourceNonKeyValues = new TreeMap(T8IdentifierUtilities.mapParameters(dataEntity.getNonKeyFieldValues(), entityDefinition.getFieldToSourceIdentifierMapping()));
            dataSourceKeyValues = new TreeMap(T8IdentifierUtilities.mapParameters(dataEntity.getKeyFieldValues(), entityDefinition.getFieldToSourceIdentifierMapping()));

            //Sort the maps by key so we always insert into the correct location

            // Add auditing data.
            updatedByIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + UPDATED_BY_ID_FIELD_IDENTIFIER);
            if (updatedByIDSourceIdentifier != null) dataSourceNonKeyValues.put(updatedByIDSourceIdentifier, updatedByID);
            updatedByIIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + UPDATED_BY_IID_FIELD_IDENTIFIER);
            if (updatedByIIDSourceIdentifier != null) dataSourceNonKeyValues.put(updatedByIIDSourceIdentifier, updatedByIID);
            updatedAtSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + UPDATED_AT_FIELD_IDENTIFIER);
            if (updatedAtSourceIdentifier != null) dataSourceNonKeyValues.put(updatedAtSourceIdentifier, updatedAt);

            if(updateString == null)
            {
                Map<String, Object> nonKeySourceValues;
                Map<String, Object> keySourceValues;

                nonKeySourceValues = T8IdentifierUtilities.mapParameters(dataSourceNonKeyValues, definition.getFieldToSourceIdentifierMapping());
                keySourceValues = T8IdentifierUtilities.mapParameters(dataSourceKeyValues, definition.getFieldToSourceIdentifierMapping());

                // Construct the updated String.
                updateString = new BatchParameterizedString("-- ");
                updateString.append(entityDefinition.getIdentifier());
                updateString.append("\nUPDATE ");
                updateString.append(definition.getTableName());
                updateString.append(" ");
                updateString.append(DataPersistenceUtilities.buildUpdateSetClause(connection, definition, definition.getSourceToFieldIdentifierMapping(), nonKeySourceValues));
                updateString.append(" ");
                // Construct a WHERE clause.
                updateString.append(DataPersistenceUtilities.buildWhereClause(connection, definition, definition.getSourceToFieldIdentifierMapping(), keySourceValues));

                populatedFields = dataSourceNonKeyValues.keySet();
            }
            else
            {
                //We can only handle batch updates if the field sizes of the entities are the same, ie they must all contain updates to the same fields
                if(!populatedFields.containsAll(dataSourceNonKeyValues.keySet())) throw new RuntimeException("Batch updates must all contain updates to the exact same fields for all entities");

                Map<String, Object> updateValues;

                //We need to populate a new map so that the values are in the correct order
                updateValues = new LinkedHashMap<>(dataSourceNonKeyValues);
                updateValues.putAll(dataSourceKeyValues);

                //check for GUID's
                DataPersistenceUtilities.convertGuidValues(connection, definition, updateValues);
                updateString.addBatch(updateValues.values());
            }
        }

        T8CommonStatementHandler.executeUpdate(context, connection, updateString, definition);
        return true;
    }

    @Override
    public int update(String entityIdentifier, Map<String, Object> updatedValues, T8DataFilter filter) throws Exception
    {
        ParameterizedString updateString;
        T8DataEntityDefinition entityDefinition;
        Map<String, Object> updatedSourceValues;
        String updatedByIDSourceIdentifier;
        String updatedByIIDSourceIdentifier;
        String updatedAtSourceIdentifier;
        T8SessionContext sessionContext;
        String updatedByID;
        String updatedByIID;
        T8Timestamp updatedAt;

        // Get auditing values.
        sessionContext = context.getSessionContext();
        if (sessionContext.isSystemSession())
        {
            updatedByID = sessionContext.getSystemAgentIdentifier();
            updatedByIID = sessionContext.getSystemAgentInstanceIdentifier();
            updatedAt = new T8Timestamp(System.currentTimeMillis());
        }
        else
        {
            updatedByID = sessionContext.getUserIdentifier();
            updatedByIID = null;
            updatedAt = new T8Timestamp(System.currentTimeMillis());
        }

        // Get the entity definition and from it map all of the required field values.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);
        updatedSourceValues = T8IdentifierUtilities.mapParameters(updatedValues, entityDefinition.getFieldToSourceIdentifierMapping(), definition.getFieldToSourceIdentifierMapping());

        // Add auditing data.
        updatedByIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + UPDATED_BY_ID_FIELD_IDENTIFIER);
        updatedByIDSourceIdentifier = definition.mapFieldToSourceIdentifier(updatedByIDSourceIdentifier);
        updatedByIIDSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + UPDATED_BY_IID_FIELD_IDENTIFIER);
        updatedByIIDSourceIdentifier = definition.mapFieldToSourceIdentifier(updatedByIIDSourceIdentifier);
        updatedAtSourceIdentifier = entityDefinition.mapFieldToSourceIdentifier(entityDefinition.getIdentifier() + UPDATED_AT_FIELD_IDENTIFIER);
        updatedAtSourceIdentifier = definition.mapFieldToSourceIdentifier(updatedAtSourceIdentifier);
        if (updatedByIDSourceIdentifier != null) updatedSourceValues.put(updatedByIDSourceIdentifier, updatedByID);
        if (updatedByIIDSourceIdentifier != null) updatedSourceValues.put(updatedByIIDSourceIdentifier, updatedByIID);
        if (updatedAtSourceIdentifier != null) updatedSourceValues.put(updatedAtSourceIdentifier, updatedAt);

        // Construct the updated String.
        updateString = new ParameterizedString("-- ");
        updateString.append(entityDefinition.getIdentifier());
        updateString.append("\nUPDATE ");
        updateString.append(definition.getTableName());
        updateString.append(" ");
        updateString.append(DataPersistenceUtilities.buildUpdateSetClause(connection, definition, definition.getSourceToFieldIdentifierMapping(), updatedSourceValues));

        // Append filter strings.
        if (filter != null)
        {
            // Add the WHERE clause if available.
            if (filter.hasFilterCriteria())
            {
                updateString.append(" ");
                updateString.append(filter.getWhereClause(tx, definition.getTableName()));
                updateString.addParameterValues(filter.getWhereClauseParameters(tx));
            }
        }

        // Execute the update and return the number of affected rows.
        return T8CommonStatementHandler.executeUpdate(context, connection, updateString, definition);
    }

    @Override
    public boolean delete(T8DataEntity dataEntity) throws Exception
    {
        ParameterizedString deleteString;
        T8DataEntityDefinition entityDefinition;
        Map<String, Object> keySourceValues;

        entityDefinition = dataEntity.getDefinition();
        keySourceValues = T8IdentifierUtilities.mapParameters(dataEntity.getKeyFieldValues(), entityDefinition.getFieldToSourceIdentifierMapping(), definition.getFieldToSourceIdentifierMapping());

        deleteString = new ParameterizedString("-- ");
        deleteString.append(entityDefinition.getIdentifier());
        deleteString.append("\nDELETE FROM ");
        deleteString.append(definition.getTableName());
        deleteString.append(" ");
        deleteString.append(DataPersistenceUtilities.buildWhereClause(connection, definition, definition.getSourceToFieldIdentifierMapping(), keySourceValues));

        return T8CommonStatementHandler.executeUpdate(context, connection, deleteString, definition) > 0;
    }

    @Override
    public int delete(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        ParameterizedString statementString;
        T8DataEntityDefinition entityDefinition;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        // Create the query string.
        statementString = new ParameterizedString("-- ");
        statementString.append(entityDefinition.getIdentifier());
        statementString.append("\nDELETE FROM ");
        statementString.append(definition.getTableName());

        // Append filter strings.
        if (filter != null)
        {
            // Add the WHERE clause if available.
            if (filter.hasFilterCriteria())
            {
                statementString.append(" ");
                statementString.append(filter.getWhereClause(tx, definition.getTableName()));
                statementString.setParameterValues(filter.getWhereClauseParameters(tx));
            }

            // Add the ORDER BY clause if available.
            if (filter.hasFieldOrdering())
            {
                statementString.append(" ");
                statementString.append(filter.getOrderByClause(connection, entityDefinition, definition));
            }
        }

        // Execute the statement.
        return T8CommonStatementHandler.executeUpdate(context, connection, statementString, definition);
    }

    @Override
    public int delete(List<T8DataEntity> dataEntities) throws Exception
    {
        int result;

        // This method of deletion can be optimizes to use a batch statement.
        result = 0;
        for (T8DataEntity dataEntity : dataEntities)
        {
            if (delete(dataEntity)) result++;
        }

        return result;
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap) throws Exception
    {
        return select(entityIdentifier, keyMap != null ? new T8DataFilter(entityIdentifier, keyMap) : null);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        return select(entityIdentifier, filter, 0, 50000);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, Map<String, Object> keyMap, int offset, int pageSize) throws Exception
    {
        return select(entityIdentifier, keyMap != null ? new T8DataFilter(entityIdentifier, keyMap) : null, offset, pageSize);
    }

    @Override
    public List<T8DataEntity> select(String entityIdentifier, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        // Create the query string.
        queryString = getQueryString(entityDefinition, filter, true, pageOffset, pageSize);

        // Retrieve the results.
        return T8CommonStatementHandler.executeQuery(context, connection, queryString, definition, entityDefinition, pageOffset, pageSize, 0);
    }

    @Override
    public T8DataEntityResults scroll(String entityIdentifier, T8DataFilter filter) throws Exception
    {
        ParameterizedString queryString;
        T8DataEntityDefinition entityDefinition;
        T8DataEntityResults entityResults;

        // Get the entity definition.
        entityDefinition = tx.getDataEntityDefinition(entityIdentifier);

        // Create the query string.
        queryString = getQueryString(entityDefinition, filter, true, 0, -1);

        // Retrieve the results.
        entityResults = T8CommonStatementHandler.getEntityResults(context, connection, queryString, definition, entityDefinition);
        openEntityResults.add(entityResults);
        return entityResults;
    }

    @Override
    public ParameterizedString getQueryString(T8DataEntityDefinition entityDefinition, T8DataFilter filter, boolean includeWithClause, int pageOffset, int pageSize) throws Exception
    {
        ParameterizedString queryString;

        // Create the query string.
        queryString = new ParameterizedString("-- ");
        if(entityDefinition != null)queryString.append(entityDefinition.getIdentifier());
        queryString.append("\nSELECT * FROM ");
        queryString.append(definition.getTableName());

        // Append filter strings.
        if (filter != null)
        {
            // Add the WHERE clause if available.
            if (filter.hasFilterCriteria())
            {
                queryString.append(" ");
                queryString.append(filter.getWhereClause(tx, definition.getTableName()));
                queryString.setParameterValues(filter.getWhereClauseParameters(tx));
            }

            // Add the ORDER BY clause if available.
            if (filter.hasFieldOrdering())
            {
                queryString.append(" ");
                queryString.append(filter.getOrderByClause(connection, entityDefinition, definition));
            }
        }

        return queryString;
    }

    @Override
    public ParameterizedString getCountQueryString(T8DataEntityDefinition entityDefinition, T8DataFilter filter, boolean includeWithClause) throws Exception
    {
        ParameterizedString queryString;

        // Create the query string.

        queryString = new ParameterizedString("-- ");
        if(entityDefinition != null)queryString.append(entityDefinition.getIdentifier());
        queryString.append("\nSELECT COUNT(*) AS RECORD_COUNT FROM ");
        queryString.append(definition.getTableName());
        queryString.append(" ");

        // Append filter strings.
        if ((filter != null) && (filter.hasFilterCriteria()))
        {
            queryString.append(filter.getWhereClause(tx, definition.getTableName()));
            queryString.setParameterValues(filter.getWhereClauseParameters(tx));
        }

        return queryString;
    }

    @Override
    public ParameterizedString getWithClause(T8DataEntityDefinition entityDefinition, T8DataFilter filter, int pageOffset, int pageSize)
    {
        return null;
    }

    @Override
    public ParameterizedString getSelectClause(T8DataEntityDefinition entityDefinition, T8DataFilter filter, int pageOffset, int pageSize) throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
