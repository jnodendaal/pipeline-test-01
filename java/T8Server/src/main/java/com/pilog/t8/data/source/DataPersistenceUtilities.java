package com.pilog.t8.data.source;

import com.pilog.t8.data.T8DataConnection;
import com.pilog.t8.data.T8DataUtilities;
import com.pilog.t8.data.database.T8DatabaseAdaptor;
import com.pilog.t8.definition.data.source.T8DataSourceDefinition;
import com.pilog.t8.definition.data.source.T8DataSourceFieldDefinition;
import com.pilog.t8.utilities.strings.ParameterizedString;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author Bouwer du Preez
 */
public class DataPersistenceUtilities
{
    public static String getNewGUID()
    {
        UUID id;

        id = UUID.randomUUID();
        return id.toString().replace("-", "").toUpperCase();
    }

    public static final Map<String, Object> getClobValues(Map<String, Object> values)
    {
        HashMap<String, Object> clobValues;

        clobValues = new HashMap<String, Object>();
        for (String key : values.keySet())
        {
            Object value;

            value = values.get(key);
            if ((value != null) && (value instanceof String) && (((String)value).length() > 4000))
            {
                clobValues.put(key, value);
            }
        }

        return clobValues;
    }

    public static final boolean isClobValue(Object value)
    {
        return ((value != null) && (value instanceof String) && (((String)value).length() > 4000));

    }

    public static final StringBuffer buildColumnNameCSV(T8DataConnection dataConnection, List<String> columnNameList)
    {
        StringBuffer csvString;
        T8DatabaseAdaptor dbAdaptor;

        // Create the new CSV String and add all variable followed by a comma to it.
        dbAdaptor = dataConnection.getDatabaseAdaptor();
        csvString = new StringBuffer();
        for (int variableIndex = 0; variableIndex < columnNameList.size() - 1; variableIndex++)
        {
            String columnName;

            columnName = columnNameList.get(variableIndex);
            csvString.append(dbAdaptor.escapeMetaString(columnName));
            csvString.append(", ");
        }

        // Add the last variable without a comma.
        if (columnNameList.size() > 0) csvString.append(columnNameList.get(columnNameList.size() - 1));

        return csvString;
    }

    public static final ParameterizedString buildParameterCSV(T8DataConnection dataConnection, T8DataSourceDefinition sourceDefinition, Map<String, Object> dataValues)
    {
        ParameterizedString parameterString;
        Iterator<String> fieldIterator;
        T8DatabaseAdaptor dbAdaptor;

        dbAdaptor = dataConnection.getDatabaseAdaptor();
        parameterString = new ParameterizedString();
        fieldIterator = dataValues.keySet().iterator();
        while (fieldIterator.hasNext())
        {
            T8DataSourceFieldDefinition fieldDefinition;
            String fieldIdentifier;
            Object value;

            fieldIdentifier = fieldIterator.next();
            value = dataValues.get(fieldIdentifier);
            fieldDefinition = sourceDefinition.getFieldDefinition(fieldIdentifier);
            if (fieldDefinition == null)
            {
                throw new IllegalArgumentException("Data Source Field not found: " + fieldIdentifier);
            }
            else if (T8DataUtilities.isGUIDDataType(fieldDefinition.getDataType()))
            {
                if (value != null) value = dbAdaptor.convertHexStringToSQLGUIDString(value.toString());
                parameterString.append(dbAdaptor.getSQLParameterizedHexToGUID(), value);
            }
            else
            {
                parameterString.append("?", value);
            }

            // Append a comma if more fields are to be added.
            if (fieldIterator.hasNext()) parameterString.append(", ");
        }

        return parameterString;
    }

    public static final void convertGuidValues(T8DataConnection dataConnection, T8DataSourceDefinition sourceDefinition, Map<String, Object> dataValues)
    {
        Iterator<String> fieldIterator;
        T8DatabaseAdaptor dbAdaptor;

        dbAdaptor = dataConnection.getDatabaseAdaptor();
        fieldIterator = dataValues.keySet().iterator();
        while (fieldIterator.hasNext())
        {
            T8DataSourceFieldDefinition fieldDefinition;
            String fieldIdentifier;
            Object value;

            fieldIdentifier = fieldIterator.next();
            value = dataValues.get(fieldIdentifier);
            fieldDefinition = sourceDefinition.getFieldDefinition(fieldIdentifier);
            if (T8DataUtilities.isGUIDDataType(fieldDefinition.getDataType()))
            {
                if (value != null) dataValues.put(fieldIdentifier,  dbAdaptor.convertHexStringToSQLGUIDString(value.toString()));
            }
        }
    }

    public static final ParameterizedString buildWhereClause(T8DataConnection dataConnection, T8DataSourceDefinition sourceDefinition, Map<String, String> columnToFieldIdentifierMap, Map<String, Object> dataValues)
    {
        ParameterizedString whereClause;
        Iterator<String> columnIterator;
        T8DatabaseAdaptor dbAdaptor;

        whereClause = new ParameterizedString();
        whereClause.append("WHERE ");

        dbAdaptor = dataConnection.getDatabaseAdaptor();
        columnIterator = dataValues.keySet().iterator();
        while (columnIterator.hasNext())
        {
            T8DataSourceFieldDefinition fieldDefinition;
            String fieldIdentifier;
            String columnName;
            Object columnValue;

            columnName = (String)columnIterator.next();
            columnValue = dataValues.get(columnName);
            fieldIdentifier = columnToFieldIdentifierMap.get(columnName);
            fieldDefinition = sourceDefinition.getFieldDefinition(fieldIdentifier);

            whereClause.append(dbAdaptor.escapeMetaString(columnName));
            whereClause.append("=");
            if (T8DataUtilities.isGUIDDataType(fieldDefinition.getDataType()))
            {
                if (columnValue != null) columnValue = dbAdaptor.convertHexStringToSQLGUIDString(columnValue.toString());
                whereClause.append(dbAdaptor.getSQLParameterizedHexToGUID(), columnValue);
            }
            else
            {
                whereClause.append("?", columnValue);
            }

            if (columnIterator.hasNext())
            {
                whereClause.append(" AND ");
            }
        }

        return whereClause;
    }

    public static final ParameterizedString buildUpdateSetClause(T8DataConnection dataConnection, T8DataSourceDefinition sourceDefinition, Map<String, String> columnToFieldIdentifierMap, Map<String, Object> dataValues)
    {
        ParameterizedString setClause;
        Iterator<String> columnIterator;
        T8DatabaseAdaptor dbAdaptor;

        setClause = new ParameterizedString();
        setClause.append("SET ");

        dbAdaptor = dataConnection.getDatabaseAdaptor();
        columnIterator = dataValues.keySet().iterator();
        while (columnIterator.hasNext())
        {
            T8DataSourceFieldDefinition fieldDefinition;
            String fieldIdentifier;
            String columnName;
            Object columnValue;

            columnName = (String)columnIterator.next();
            columnValue = dataValues.get(columnName);
            fieldIdentifier = columnToFieldIdentifierMap.get(columnName);
            fieldDefinition = sourceDefinition.getFieldDefinition(fieldIdentifier);
            if (fieldDefinition == null)
            {
                throw new RuntimeException("Field not found in source definition: " + fieldIdentifier);
            }
            else
            {
                setClause.append(dbAdaptor.escapeMetaString(columnName));
                setClause.append("=");
                if (T8DataUtilities.isGUIDDataType(fieldDefinition.getDataType()))
                {
                    if (columnValue != null) columnValue = dbAdaptor.convertHexStringToSQLGUIDString(columnValue.toString());
                    setClause.append(dbAdaptor.getSQLParameterizedHexToGUID(), columnValue);
                }
                else
                {
                    setClause.append("?", columnValue);
                }

                if (columnIterator.hasNext())
                {
                    setClause.append(", ");
                }
            }
        }

        return setClause;
    }
}
