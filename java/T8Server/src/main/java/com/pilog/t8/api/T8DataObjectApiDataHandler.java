package com.pilog.t8.api;

import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.definition.functionality.T8FunctionalityManagerResource;
import com.pilog.t8.functionality.T8DataObjectStateDetails;
import com.pilog.t8.security.T8Context;
import com.pilog.t8.utilities.collections.HashMaps;

/**
 * @author Bouwer du Preez
 */
public class T8DataObjectApiDataHandler
{
    private final T8DataTransaction tx;

    public T8DataObjectApiDataHandler(T8DataTransaction tx)
    {
        this.tx = tx;
    }

    /**
     * Returns the Concept ID of the current state of the specified object as
     * persisted.
     * @param dataObjectIid The key of the data object for which to retrieve a state
     * ID.
     * @return The Concept ID of the current state of the specified object.
     * Returns null if the object does not exist in the persistence data source.
     * @throws Exception
     */
    public T8DataObjectStateDetails retrieveObjectStateDetails(String dataObjectIid) throws Exception
    {
        T8DataEntity entity;
        String entityId;

        // Get the entity id and transaction to use for this operation.
        entityId = T8FunctionalityManagerResource.STATE_DATA_OBJECT_DE_IDENTIFIER;

        // Retrieve the entity.
        entity = tx.retrieve(entityId, HashMaps.newHashMap(entityId + "$DATA_OBJECT_IID", dataObjectIid));
        if (entity != null)
        {
            String dataObjectId;
            String stateConceptId;

            dataObjectId = (String)entity.getFieldValue(entityId + "$DATA_OBJECT_ID");
            stateConceptId = (String)entity.getFieldValue(entityId + "$STATE_ID");
            return new T8DataObjectStateDetails(dataObjectId, dataObjectIid, stateConceptId);
        }
        else return null;
    }

    public void saveObjectState(String dataObjectId, String dataObjectIid, String stateConceptId) throws Exception
    {
        T8DataEntity entity;
        String entityId;

        // Create a new proces history entity.
        entityId = T8FunctionalityManagerResource.STATE_DATA_OBJECT_DE_IDENTIFIER;
        entity = tx.create(entityId, null);
        entity.setFieldValue(entityId + "$DATA_OBJECT_IID", dataObjectIid);
        entity.setFieldValue(entityId + "$DATA_OBJECT_ID", dataObjectId);
        entity.setFieldValue(entityId + "$STATE_ID", stateConceptId);

        // Insert the entity.
        if (!tx.update(entity))
        {
            tx.insert(entity);
        }
    }

    public void logObjectStateEvent(String objectKey, String functionalityId, String stateFromConceptId, String stateToConceptId) throws Exception
    {
        T8Context context;
        T8DataEntity entity;
        String entityId;
        String eventIID;
        String agentID;
        String agentIID;

        // Determine the agent responsible for the state transfer.
        context = tx.getContext();
        if (context.isSystem())
        {
            agentID = context.getSystemAgentId();
            agentIID = context.getSystemAgentIid();
        }
        else
        {
            agentID = context.getUserId();
            agentIID = null;
        }

        // Create a new proces history entity.
        entityId = T8FunctionalityManagerResource.HISTORY_DATA_OBJECT_STATE_DE_IDENTIFIER;
        eventIID = T8IdentifierUtilities.createNewGUID();
        entity = tx.create(entityId, null);
        entity.setFieldValue(entityId + "$EVENT_IID", eventIID);
        entity.setFieldValue(entityId + "$DATA_OBJECT_IID", objectKey);
        entity.setFieldValue(entityId + "$STATE_FROM_ID", stateFromConceptId);
        entity.setFieldValue(entityId + "$STATE_TO_ID", stateToConceptId);
        entity.setFieldValue(entityId + "$TIME", new java.sql.Timestamp(System.currentTimeMillis()));
        entity.setFieldValue(entityId + "$FUNCTIONALITY_ID", functionalityId);
        entity.setFieldValue(entityId + "$AGENT_ID", agentID);
        entity.setFieldValue(entityId + "$AGENT_IID", agentIID);

        // Insert the entity.
        tx.insert(entity);
    }
}
