package com.pilog.t8.process.operation;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.T8ServerOperation.T8ServerOperationStatus;
import com.pilog.t8.T8ServerOperationNotificationHandle;
import com.pilog.t8.T8ServerOperationStatusReport;
import com.pilog.t8.T8SessionContext;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.operation.progressreport.T8ProgressReport;
import com.pilog.t8.process.T8Process;
import com.pilog.t8.process.T8ProcessDetails;
import com.pilog.t8.script.T8ServerContextScript;
import com.pilog.t8.definition.process.T8ProcessDefinition;
import com.pilog.t8.definition.process.T8ProcessFailureScriptDefinition;
import com.pilog.t8.process.T8ProcessState;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.definition.process.operation.T8ServerOperationProcessDefinition;
import com.pilog.t8.script.T8DefaultServerContextScript;
import com.pilog.t8.security.T8Context;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bouwer du Preez
 */
public class T8ServerOperationProcess implements T8Process
{
    private final T8Context context;
    private final T8ServerContext serverContext;
    private final T8SessionContext sessionContext;
    private final T8ServerOperationProcessDefinition definition;
    private volatile T8ServerOperationStatusReport lastOperationStatusReport;
    private final String instanceIdentifier;
    private final String initiatorIdentifier;
    private final String initiatorDisplayName;
    private final String initiatorProfileIdentifier;
    private Map<String, Object> processResult;
    private Throwable processThrowable;
    private T8ProcessStatus status;
    private String operationIid;
    private volatile T8ProgressReport latestProgressReport;
    private volatile double latestProgress;
    private long startTime;
    private long endTime;

    public T8ServerOperationProcess(T8Context context, T8ServerOperationProcessDefinition definition, String instanceIdentifier)
    {
        this.context = context;
        this.serverContext = context.getServerContext();
        this.sessionContext = context.getSessionContext();
        this.definition = definition;
        this.instanceIdentifier = instanceIdentifier;
        this.initiatorIdentifier = sessionContext.getUserIdentifier();
        this.initiatorDisplayName = sessionContext.getUserName() + " " + sessionContext.getUserSurname();
        this.initiatorProfileIdentifier = sessionContext.getUserProfileIdentifier();
        this.operationIid = null;
        this.status = T8ProcessStatus.CREATED;
        this.latestProgress = 0;
        this.startTime = -1;
        this.endTime = -1;
    }

    @Override
    public String getIid()
    {
        return instanceIdentifier;
    }

    @Override
    public T8ProcessDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public T8ProcessState getState()
    {
        T8ProcessState state;

        state = new T8ProcessState(definition.getRootProjectId(), definition.getIdentifier(), instanceIdentifier);
        state.setStartTime(new T8Timestamp(startTime));
        state.setEndTime(new T8Timestamp(endTime));
        state.setName(definition.getProcessDisplayName());
        state.setDescription(definition.getProcessDescription());
        state.setRestrictionUserId(null);
        state.setRestrictionUserProfileId(null);
        state.setStatus(status);
        state.setProgress(latestProgress);

        if (sessionContext.isSystemSession())
        {
            state.setInitiatorId(sessionContext.getSystemAgentIdentifier());
            state.setInitiatorInstanceIdentifier(sessionContext.getSystemAgentInstanceIdentifier());
        }
        else
        {
            state.setInitiatorId(sessionContext.getUserIdentifier());
            state.setInitiatorInstanceIdentifier(null);
        }

        return state;
    }

    @Override
    public T8ProcessStatus getStatus()
    {
        return status;
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> inputParameters) throws Exception
    {
        try
        {
            T8ServerOperationNotificationHandle operationNotificationHandle;
            T8Context accessContext;

            // Create the new access context.
            accessContext = new T8Context(context);
            accessContext.setProcessId(definition.getIdentifier());
            accessContext.setProcessIid(instanceIdentifier);

            startTime = System.currentTimeMillis();
            status = T8ProcessStatus.IN_PROGRESS;
            lastOperationStatusReport = serverContext.executeAsynchronousOperation(accessContext, definition.getOperationIdentifier(), T8IdentifierUtilities.mapParameters(inputParameters, definition.getInputParameterMapping()));
            operationIid = lastOperationStatusReport.getOperationInstanceIdentifier();
            operationNotificationHandle = lastOperationStatusReport.getNotificationHandle();

            while (true)
            {
                T8ServerOperationStatus operationStatus;
                T8ServerOperationStatusReport statusReport;

                statusReport = serverContext.getOperationStatus(context, operationIid);
                if (statusReport != null) lastOperationStatusReport = statusReport;

                latestProgress = lastOperationStatusReport.getOperationProgress();
                latestProgressReport = lastOperationStatusReport.getProgressReportObject();
                operationStatus = lastOperationStatusReport.getOperationStatus();
                if (operationStatus == T8ServerOperationStatus.CANCELLED)
                {
                    status = T8ProcessStatus.CANCELLED;
                    break;
                }
                else if (operationStatus == T8ServerOperationStatus.COMPLETED)
                {
                    status = T8ProcessStatus.COMPLETED;
                    break;
                }
                else if (operationStatus == T8ServerOperationStatus.FAILED)
                {
                    status = T8ProcessStatus.FAILED;
                    break;
                }
                else if (operationStatus == T8ServerOperationStatus.STOPPED)
                {
                    status = T8ProcessStatus.STOPPED;
                    break;
                }
                else if (operationStatus == T8ServerOperationStatus.PAUSED)
                {
                    status = T8ProcessStatus.PAUSED;
                    synchronized (operationNotificationHandle)
                    {
                        operationNotificationHandle.wait();
                    }
                }
                else if (operationStatus == T8ServerOperationStatus.IN_PROGRESS)
                {
                    status = T8ProcessStatus.IN_PROGRESS;
                    synchronized (operationNotificationHandle)
                    {
                        operationNotificationHandle.wait();
                    }
                }
            }

            // Record the end-time.
            endTime = System.currentTimeMillis();

            // Return the result of the operation.
            if (lastOperationStatusReport.getOperationThrowable() != null)
            {
                processThrowable = lastOperationStatusReport.getOperationThrowable();
                throw new InvocationTargetException(lastOperationStatusReport.getOperationThrowable(), "Exception while executing process: " + instanceIdentifier + " (" + definition + ")");
            }
            else
            {
                processResult = T8IdentifierUtilities.mapParameters(lastOperationStatusReport.getOperationResult(), definition.getOutputParameterMapping());
                return processResult;
            }
        }
        catch(Throwable throwable)
        {
            //if this process has failure script set, execute that now.
            try
            {
                T8ProcessFailureScriptDefinition failureScriptDefinition = definition.getFailureScriptDefinition();
                if(failureScriptDefinition != null)
                {
                    T8ServerContextScript script;
                    Map<String, Object> scriptParameters;

                    scriptParameters = new HashMap<String,Object>(2);
                    scriptParameters.put(T8ProcessFailureScriptDefinition.PARAMETER_THROWABLE, throwable);
                    scriptParameters.put(T8ProcessFailureScriptDefinition.PARAMETER_INPUT_PARAMETERS, inputParameters);
                    scriptParameters.put(T8ProcessFailureScriptDefinition.PARAMETER_PROCESS_DEFINITION, definition);

                    script = new T8DefaultServerContextScript(context, failureScriptDefinition);
                    script.executeScript(scriptParameters);
                }
            }
            catch(Exception ex)
            {
                T8Log.log("Failed to execute failure script for process " + definition.getPublicIdentifier(), ex);
            }
            //rethrow
            throw new Exception(throwable);
        }
    }

    @Override
    public void stop() throws Exception
    {
        if (operationIid != null)
        {
            lastOperationStatusReport = serverContext.stopOperation(context, operationIid);
        }
    }

    @Override
    public void cancel() throws Exception
    {
        if (operationIid != null)
        {
            lastOperationStatusReport = serverContext.cancelOperation(context, operationIid);
        }
    }

    @Override
    public void pause() throws Exception
    {
        if (operationIid != null)
        {
            lastOperationStatusReport = serverContext.pauseOperation(context, operationIid);
        }
    }

    @Override
    public void resume() throws Exception
    {
        if (operationIid != null)
        {
            lastOperationStatusReport = serverContext.resumeOperation(context, operationIid);
        }
    }

    @Override
    public double getProgress() throws Exception
    {
        if (operationIid != null)
        {
            T8ServerOperationStatusReport statusReport;

            statusReport = serverContext.getOperationStatus(context, operationIid);
            if (statusReport != null)
            {
                latestProgress = statusReport.getOperationProgress();
                latestProgressReport = statusReport.getProgressReportObject();
                return latestProgress;
            }
            else return latestProgress;
        }
        else return 0;
    }

    @Override
    public T8ProcessDetails getDetails() throws Exception
    {
        T8ProcessDetails processDetails;

        processDetails = new T8ProcessDetails(instanceIdentifier);
        processDetails.setStartTime(startTime);
        processDetails.setEndTime(endTime);
        processDetails.setProgress(getProgress());
        processDetails.setInitiatorId(initiatorIdentifier);
        processDetails.setInitiatorName(initiatorDisplayName);
        processDetails.setProcessStatus(status);
        processDetails.setProcessDescription(definition.getProcessDescription());
        processDetails.setName(definition.getProcessDisplayName());
        processDetails.setProcessId(definition.getIdentifier());
        processDetails.setProcessIid(instanceIdentifier);
        processDetails.setExecuting(status == T8ProcessStatus.IN_PROGRESS);
        processDetails.setVisibilityRestrictionUserIds(definition.getUserVisibilityResctrictions());
        processDetails.setVisibilityRestrictionProfileIds(definition.getUserProfileVisibilityResctrictions());
        processDetails.setVisibilityRestrictionWorkflowProfileIds(definition.getWorkFlowProfileVisibilityResctrictions());
        if (operationIid != null)
        {
            T8ServerOperationStatusReport statusReport;

            statusReport = serverContext.getOperationStatus(context, operationIid);
            if (statusReport != null)
            {
                latestProgressReport = statusReport.getProgressReportObject();
            }
        }
        processDetails.setProgressReport(latestProgressReport);
        return processDetails;
    }
}
