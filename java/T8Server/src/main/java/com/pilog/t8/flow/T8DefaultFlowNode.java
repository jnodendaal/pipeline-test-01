package com.pilog.t8.flow;

import com.pilog.t8.T8Log;
import com.pilog.t8.T8ServerContext;
import com.pilog.t8.data.T8DataEntity;
import com.pilog.t8.data.T8DataSession;
import com.pilog.t8.data.T8DataTransaction;
import com.pilog.t8.definition.T8IdentifierUtilities;
import com.pilog.t8.flow.T8Flow.FlowDataObjectHistoryEvent;
import com.pilog.t8.flow.T8FlowNode.FlowNodeStatus;
import com.pilog.t8.flow.T8FlowNodeExecutionResult.NodeExecutionResultType;
import com.pilog.t8.flow.state.T8FlowDataObjectState;
import com.pilog.t8.definition.flow.T8WorkFlowNodeDefinition;
import com.pilog.t8.flow.state.T8FlowNodeState;
import com.pilog.t8.flow.state.T8FlowStateParameterMap;
import com.pilog.t8.flow.state.T8FlowState;
import com.pilog.t8.flow.state.T8FlowStateNodeInputParameterMap;
import com.pilog.t8.time.T8Timestamp;
import com.pilog.t8.data.T8HistoryParameterPersistenceHandler;
import com.pilog.t8.definition.data.T8DataParameterDefinition;
import com.pilog.t8.definition.flow.T8FlowDataObjectDefinition;
import com.pilog.t8.definition.flow.T8FlowManagerResource;
import com.pilog.t8.utilities.strings.Strings;
import com.pilog.t8.utilities.exceptions.ExceptionUtilities;
import com.pilog.t8.security.T8Context;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pilog.t8.definition.flow.T8FlowManagerResource.STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER;

/**
 * @author Bouwer du Preez
 */
public abstract class T8DefaultFlowNode implements T8FlowNode
{
    protected final T8WorkFlowNodeDefinition definition;
    protected final T8ServerContext serverContext;
    protected final T8Context internalContext;
    protected final T8FlowController flowController;
    protected final T8FlowState flowState;
    protected final T8FlowNodeState nodeState;
    protected final T8Flow parentFlow;
    protected boolean stop;
    protected boolean cancel;
    protected String statusMessage;
    protected String flowId;
    protected String flowIid;
    protected String nodeId;
    protected String nodeIid;
    protected List<String> outputNodeIds;
    protected List<String> inputNodeIds;
    protected Thread executionThread;
    protected List<String> completedExecutionSteps;

    public enum T8DefaultFlowNodeExecutionStep
    {
        START,
        WAITING_FOR_INPUT_DATA,
        ACTIVATION,
        EXECUTION,
        OUTPUT,
        FINALIZATION
    };

    public T8DefaultFlowNode(T8Context context, T8Flow parentFlow, T8WorkFlowNodeDefinition definition, String nodeIid)
    {
        this.serverContext = context.getServerContext();
        this.internalContext = context;
        this.parentFlow = parentFlow;
        this.flowController = parentFlow.getFlowController();
        this.flowId = parentFlow.getDefinition().getIdentifier();
        this.flowIid = parentFlow.getInstanceIdentifier();
        this.nodeId = definition.getIdentifier();
        this.nodeIid = nodeIid;
        this.flowState = parentFlow.getState();
        this.definition = definition;
        this.stop = false;
        this.cancel = false;

        // Get the output node identifiers.
        outputNodeIds = parentFlow.getOutputNodeIdentifiers(definition.getIdentifier());

        // Get the input node identifiers.
        inputNodeIds = parentFlow.getInputNodeIdentifiers(definition.getIdentifier());

        // Create the state for this node if it does not exist.
        synchronized (flowState)
        {
            T8FlowNodeState existingNodeState;

            existingNodeState = flowState.getNodeState(nodeIid);
            if (existingNodeState != null)
            {
                nodeState = existingNodeState;
            }
            else
            {
                nodeState = new T8FlowNodeState(flowState, nodeIid, definition.getIdentifier(), definition.getNodeType(), FlowNodeStatus.NODE_CREATED);
                flowState.addNodeState(nodeState);
            }
        }

        // Read the list of completed execution steps for this node from the node state.
        completedExecutionSteps = nodeState.getCompletedExecutionSteps();
        if (completedExecutionSteps == null) completedExecutionSteps = new ArrayList<String>();
    }

    @Override
    public T8WorkFlowNodeDefinition getDefinition()
    {
        return definition;
    }

    @Override
    public T8FlowNodeState getState()
    {
        return nodeState;
    }

    @Override
    public String getInstanceIdentifier()
    {
        return nodeIid;
    }

    @Override
    public T8FlowNodeExecutionResult executeNode(T8FlowExecutionKey executionKey) throws Exception
    {
        Map<String, Object> inputParameters = null;
        Map<String, Object> outputParameters = null;
        T8DataTransaction tx = null;
        T8DataSession ds;

        // Get the data session for this node's execution.
        ds = serverContext.getDataManager().getCurrentSession();

        // Execute the node steps.
        try
        {
            // Start the node execution.
            if (!isExecutionStepComplete(T8DefaultFlowNodeExecutionStep.START.toString()))
            {
                // Return if the node has been stopped.
                if (stop) return new T8FlowNodeExecutionResult(NodeExecutionResultType.STOPPED, null);

                // Set the first status message.
                statusMessage = "Executing node.";

                // Set the execution thread.
                this.executionThread = Thread.currentThread();

                // Synchronize when altering flow state.
                synchronized (flowState)
                {
                    // Set the node status to indicate that it has been started.
                    nodeState.setNodeStatus(FlowNodeStatus.STARTED);

                    // Complete the execution step.
                    completeExecutionStep(T8DefaultFlowNodeExecutionStep.START.toString());
                }
            }

            // Wait for node input data.
            if (!isExecutionStepComplete(T8DefaultFlowNodeExecutionStep.WAITING_FOR_INPUT_DATA.toString()))
            {
                // Return if the node has been stopped.
                if (stop) return new T8FlowNodeExecutionResult(NodeExecutionResultType.STOPPED, null);

                // Synchronize when altering flow state.
                synchronized (flowState)
                {
                    // Now wait for all required input data to arrive.
                    inputParameters = getInputParametersFromState(true);

                    // If null was returned, we know all input data was not yet available.
                    if (inputParameters == null)
                    {
                        // A new wait key has been added, so persist the flow state.
                        parentFlow.persistState();
                        return new T8FlowNodeExecutionResult(NodeExecutionResultType.WAITING, null);
                    }
                    else
                    {
                        // Clear the wait key (if any was set).
                        nodeState.clearWaitKeys();

                        // Update the node state with the execution parameters.
                        nodeState.addExecutionParameters(inputParameters);

                        // Complete the execution step.
                        completeExecutionStep(T8DefaultFlowNodeExecutionStep.WAITING_FOR_INPUT_DATA.toString());
                    }
                }
            }

            // Activate the node.
            if (!isExecutionStepComplete(T8DefaultFlowNodeExecutionStep.ACTIVATION.toString()))
            {
                List<String> allocatedObjectIids;

                // Return if the node has been stopped.
                if (stop) return new T8FlowNodeExecutionResult(NodeExecutionResultType.STOPPED, null);

                // If we do not have the input parameters, get them.
                if (inputParameters == null) inputParameters = getInputParametersFromState(false);

                // Synchronize when altering flow state.
                synchronized (flowState)
                {
                    // Set the node status to indicate that it has been started.
                    nodeState.setNodeStatus(FlowNodeStatus.ACTIVE);

                    // Set the activation time.
                    nodeState.setTimeActivated(new T8Timestamp(System.currentTimeMillis()));

                    // Now allocate the data objects used by this node.
                    allocatedObjectIids = allocateDataObjects();

                    // Complete the execution step.
                    completeExecutionStep(T8DefaultFlowNodeExecutionStep.ACTIVATION.toString());
                }

                // Log data object event.
                logDataObjectEvent(allocatedObjectIids, FlowDataObjectHistoryEvent.ALLOCATED);

                // Log the node event.
                logNodeEvent(NodeHistoryEvent.STARTED, inputParameters, null);
            }

            // Execute the node function.
            if (!isExecutionStepComplete(T8DefaultFlowNodeExecutionStep.EXECUTION.toString()))
            {
                Map<String, Object> executionOutputParameters;

                // Return if the node has been stopped.
                if (stop) return new T8FlowNodeExecutionResult(NodeExecutionResultType.STOPPED, null);

                // If we do not have the input parameters, get them.
                if (inputParameters == null) inputParameters = getInputParametersFromState(false);

                // Execute the node content and get the output parameters.
                tx = ds.beginTransaction();
                executionOutputParameters = executeNode(executionKey, inputParameters);
                if ((tx != null) && (tx.isOpen())) tx.commit();

                // Synchronize when altering flow state.
                synchronized (flowState)
                {
                    // If null was returned, we know execution was not completed.
                    if (executionOutputParameters == null) return new T8FlowNodeExecutionResult(NodeExecutionResultType.WAITING, null);

                    // Create the output parameter map.
                    outputParameters = new HashMap<String, Object>();
                    outputParameters.putAll(inputParameters);
                    outputParameters.putAll(executionOutputParameters);

                    // Update the state to reflect the output parameters.
                    nodeState.addOutputParameters(outputParameters);

                    // Complete the execution step.
                    completeExecutionStep(T8DefaultFlowNodeExecutionStep.EXECUTION.toString());
                }
            }

            // Propagate the node output parameters to all output node states.
            if (!isExecutionStepComplete(T8DefaultFlowNodeExecutionStep.OUTPUT.toString()))
            {
                // Return if the node has been stopped.
                if (stop) return new T8FlowNodeExecutionResult(NodeExecutionResultType.STOPPED, null);

                // If we do not have the output parameters, get them.
                if (outputParameters == null) outputParameters = getOutputParametersFromState();

                // Synchronize when altering flow state.
                synchronized (flowState)
                {
                    // Send the output data to all output node states.
                    propagateOutputData(outputNodeIds, outputParameters);

                    // Complete the execution step.
                    completeExecutionStep(T8DefaultFlowNodeExecutionStep.OUTPUT.toString());
                }
            }

            // Finalize the execution of this node.
            if (!isExecutionStepComplete(T8DefaultFlowNodeExecutionStep.FINALIZATION.toString()))
            {
                // If we do not have the output parameters, get them.
                if (outputParameters == null) outputParameters = getOutputParametersFromState();

                // Synchronize when altering flow state.
                synchronized (flowState)
                {
                    // Update the node state to reflect that it has been completed.
                    nodeState.setNodeStatus(FlowNodeStatus.COMPLETED);

                    // Complete the execution step.
                    completeExecutionStep(T8DefaultFlowNodeExecutionStep.FINALIZATION.toString());
                }

                // Log the node event.
                logNodeEvent(NodeHistoryEvent.COMPLETED, outputParameters, null);

                // Return the completed result.
                return new T8FlowNodeExecutionResult(NodeExecutionResultType.COMPLETED, null);
            }

            // This point should never be reached.
            throw new Exception("Attempt to execute an already completed node: " + nodeState);
        }
        catch (Exception e)
        {
            String exceptionInformation;

            // Create a descriptive string from the exception.
            exceptionInformation = ExceptionUtilities.getStackTraceString(e, 1000, 1000);

            // Log the exception.
            T8Log.log("Exception while executing node '" + definition.getIdentifier() + "' in flow '" + parentFlow.getInstanceIdentifier() + "': ", e);

            // Rollback the transaction.
            if ((tx != null) && (tx.isOpen())) tx.rollback();

            // Update the node state to reflect that it has failed.
            synchronized (flowState)
            {
                nodeState.setNodeStatus(FlowNodeStatus.FAILED);
                nodeState.setInformation(exceptionInformation);
                parentFlow.persistState();
                statusMessage = "Failed due to exception: " + e.getMessage();
            }

            // Log the node event.
            logNodeEvent(NodeHistoryEvent.FAILED, null, exceptionInformation);

            // At this point, the node will have a status of FAILED and it is
            // up to the administrator to resolve and retry the execution of
            // the node.
            return new T8FlowNodeExecutionResult(NodeExecutionResultType.FAILED, exceptionInformation);
        }
    }

    /**
     * Executes a node using the supplied input parameters.  It is important
     * the sub-classes must return a valid Map on successful execution.  If null
     * is returned, the default behavior is to assume that execution could not
     * be completed and that that node is in a waiting state.
     * @param executionKey
     * @param inputParameters The input parameters to use when executing the
     * node.
     * @return The output parameters of the node.  This has to be a valid non-
     * null Map on successful completion of execution.
     * @throws Exception
     */
    protected abstract Map<String, Object> executeNode(T8FlowExecutionKey executionKey, Map<String, Object> inputParameters) throws Exception;

    protected boolean isExecutionStepComplete(String executionStepIdentifier)
    {
        return completedExecutionSteps.contains(executionStepIdentifier);
    }

    protected void completeExecutionStep(String executionStepId) throws Exception
    {
        if (!completedExecutionSteps.contains(executionStepId))
        {
            completedExecutionSteps.add(executionStepId);
            nodeState.setCompletedExecutionSteps(completedExecutionSteps);
            parentFlow.persistState();
        }
        else throw new RuntimeException("Cannot complete an already completed execution step: " + executionStepId);
    }

    protected List<String> allocateDataObjects()
    {
        Map<String, Object> executionParameters;
        List<String> allocatedObjectIids;

        allocatedObjectIids = new ArrayList<>();
        executionParameters = nodeState.getExecutionParameters();
        for (T8FlowDataObjectDefinition dataObjectDefinition : parentFlow.getDefinition().getFlowDataObjectDefinitions())
        {
            String objectIid;

            objectIid = (String)executionParameters.get(dataObjectDefinition.getKeyParameterIdentifier());
            if (!Strings.isNullOrEmpty(objectIid))
            {
                if (!flowState.containsDataObjectState(objectIid))
                {
                    T8FlowDataObjectState dataObjectState;
                    String objectIdentifier;

                    objectIdentifier = dataObjectDefinition.getDataObjectIdentifier();
                    dataObjectState = new T8FlowDataObjectState(flowState, objectIdentifier, objectIid);
                    flowState.addDataObjectState(dataObjectState);
                    allocatedObjectIids.add(objectIid);
                }
            }
        }

        return allocatedObjectIids;
    }

    protected void propagateOutputData(List<String> outputNodeIdentifiers, Map<String, Object> outputParameters)
    {
        Map<String, String> outputNodeInstanceIdentifierMap;

        // Get the output node instance nodeId map from the node state.
        outputNodeInstanceIdentifierMap = new HashMap<String, String>();

        // Send the output data to all output node states.
        for (String outputNodeIdentifier : outputNodeIdentifiers)
        {
            if (isOutputNodeApplicable(outputNodeIdentifier))
            {
                List<T8FlowNodeState> outputNodeStates;
                T8FlowNodeState outputNodeState;
                T8WorkFlowNodeDefinition outputNodeDefinition;

                // Get the output node definition.
                outputNodeDefinition = parentFlow.getDefinition().getNodeDefinition(outputNodeIdentifier);

                // Add the output data to the output node state.  If no synchronizable output node state was found, create a new state.
                synchronized (flowState)
                {
                    T8FlowStateNodeInputParameterMap newInputParameters;

                    // Find all output state of the correct type and check if this node can be synchronized with one of them.
                    outputNodeStates = flowState.getNodeStatesByType(outputNodeIdentifier);
                    outputNodeState = T8FlowUtilities.getSynchronizableNodeState(parentFlow, flowState, nodeState, outputNodeStates);
                    if (outputNodeState == null)
                    {
                        outputNodeState = new T8FlowNodeState(flowState, T8IdentifierUtilities.createNewGUID(), outputNodeIdentifier, outputNodeDefinition.getNodeType(), FlowNodeStatus.STATE_CREATED);
                        flowState.addNodeState(outputNodeState);
                    }

                    // Add the output data set to the output node's input data set.
                    outputNodeInstanceIdentifierMap.put(outputNodeIdentifier, outputNodeState.getNodeIid());
                    newInputParameters = new T8FlowStateNodeInputParameterMap(outputNodeState, null, nodeId, null, STATE_FLOW_NODE_INPUT_PAR_DE_IDENTIFIER, null);
                    newInputParameters.putAll(T8IdentifierUtilities.stripNamespace(definition.getNamespace(), outputParameters, false));
                    outputNodeState.addInputParameters(newInputParameters);
                }
            }
        }

        // Set the output node nodeId map on the node state.
        nodeState.addOutputNodeMappings(outputNodeInstanceIdentifierMap);
    }

    /**
     * This method's purpose is to allow gateway nodes that extend this node
     * to override the default behavior where all output nodes are started.
     * @param outputNodeIdentifier Identifier of the output node to check.
     * @return A boolean value indicating whether or not the specified output
     * node must be started.
     */
    protected boolean isOutputNodeApplicable(String outputNodeIdentifier)
    {
        return true;
    }

    /**
     * Compiles the input data required by this node before it can activate.
     * If all required input data is not available, this method will add an
     * execution key to the thread manager and will return null.
     * @param waitForCompleteSet
     * @return All of the input data required by this node to activate or null
     * if all of the input data is not available.
     * @throws Exception
     */
    protected synchronized HashMap<String, Object> getInputParametersFromState(boolean waitForCompleteSet) throws Exception
    {
        Map<String, T8FlowStateParameterMap> inputDataSetMap;
        HashMap<String, Object> inputData;
        List<String> activeInputNodeIdentifiers;

        // Loop through the input set and merge all input sets into a final input parameter map.
        synchronized (flowState)
        {
            // First create an empty map of all parameters defined by the flow.
            inputData = new HashMap<String, Object>();
            for (T8DataParameterDefinition parameterDefinition : parentFlow.getDefinition().getFlowParameterDefinitions())
            {
                inputData.put(parameterDefinition.getIdentifier(), null);
            }

            // Get the input data set from the node state.
            inputDataSetMap = nodeState.getInputParameters();
            activeInputNodeIdentifiers = T8FlowUtilities.findActiveInputIdentifiers(parentFlow, flowState, nodeState);
        }

        // Loop through all of the active input node identifiers and get the input data from each input node type.
        statusMessage = "Compiling input data collection.";
        for (String activeInputNodeIdentifier : activeInputNodeIdentifiers)
        {
            if (!inputDataSetMap.containsKey(activeInputNodeIdentifier))
            {
                if ((!stop) && (waitForCompleteSet))
                {
                    // Wait until the required input data set becomes available.
                    statusMessage = "Waiting for input data from input node: " + activeInputNodeIdentifier;

                    // All of the input data is not yet available so wait some more and then get the updated input data set.
                    nodeState.addNodeCompletionWaitKey(activeInputNodeIdentifier);

                    // Return null to indicate the input data is not evailable.
                    return null;
                }
            }
            else
            {
                HashMap<String, Object> inputDataSet;

                inputDataSet = inputDataSetMap.get(activeInputNodeIdentifier);
                if (inputDataSet != null) inputData.putAll(inputDataSet);
            }
        }

        // Convert the input parameters from the state collections to default and return the result.
        return (HashMap<String, Object>)convertStateObject(inputData);
    }

    /**
     * This method takes an input object, and inspects it for collection types
     * Map and List.  If such collections are found, they are copied into new
     * collections LinkedHashMap and ArrayList (effectively discarding the old
     * collection types).  This is done, so that state collections can be used
     * and altered during node execution without affecting the underlying
     * structure of the state.  All objects not of type Map or List are simply
     * added to the output collection and are not altered.
     *
     * @param stateObject The state object to convert.
     * @return The converted state object.
     */
    private Object convertStateObject(Object stateObject)
    {
        if (stateObject instanceof Map)
        {
            LinkedHashMap outputMap;
            Map inputMap;

            inputMap = (Map)stateObject;
            outputMap = new LinkedHashMap();
            for (Object key : inputMap.keySet())
            {
                outputMap.put(key, convertStateObject(inputMap.get(key)));
            }

            return outputMap;
        }
        else if (stateObject instanceof List)
        {
            ArrayList outputList;
            List inputList;

            inputList = (List)stateObject;
            outputList = new ArrayList();
            for (int index = 0; index < inputList.size(); index++)
            {
                outputList.add(convertStateObject(inputList.get(index)));
            }

            return outputList;
        }
        else return stateObject;
    }

    protected synchronized HashMap<String, Object> getOutputParametersFromState() throws Exception
    {
        synchronized (flowState)
        {
            return nodeState.getOutputParameters();
        }
    }

    @Override
    public void stopNode() throws Exception
    {
        // Set the stop flag.
        stop = true;

        // Notify all threads that might currently be waiting for a change in state.
        synchronized (this)
        {
            notifyAll();
        }

        // Wait for the node execution to end.
        waitForExecutionThreadToEnd();
    }

    @Override
    public void cancelNode() throws Exception
    {
        // Set the stop and cancel flags.
        stop = true;
        cancel = true;

        // Notify all threads that might currently be waiting for a change in state.
        synchronized (this)
        {
            notifyAll();
        }

        // Wait for the node execution to end.
        waitForExecutionThreadToEnd();
    }

    private void waitForExecutionThreadToEnd() throws Exception
    {
        int waitCount;

        // Wait for the node execution to end.
        waitCount = 0;
        while ((executionThread.isAlive()) && (executionThread != Thread.currentThread()))
        {
            // Wait one second for the thread to die.
            executionThread.join(1000);
            waitCount++;

            // If the wait count exceeds the expected value, log it.
            if (waitCount > 10)
            {
                T8Log.log("Execution thread in node '" + definition + "', flow '" + parentFlow.getInstanceIdentifier() + "' still alive " + waitCount + " seconds following instruction to finalize.  Status: " + statusMessage);
            }
        }
    }

    @Override
    public T8FlowNodeStatus getStatus()
    {
        return new T8FlowNodeStatus(definition.getIdentifier(), nodeState.getNodeStatus(), -1, statusMessage);
    }

    private void logNodeEvent(NodeHistoryEvent event, Map<String, Object> eventParameters, String eventInformation) throws Exception
    {
        T8DataTransaction tx;
        T8DataEntity entity;
        String entityIdentifier;
        String eventIID;

        // Get the transaction.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        // Create a new node history entity.
        eventIID = T8IdentifierUtilities.createNewGUID();
        entity = tx.create(T8FlowManagerResource.NODE_HISTORY_DE_IDENTIFIER, null);
        entityIdentifier = T8FlowManagerResource.NODE_HISTORY_DE_IDENTIFIER;
        entity.setFieldValue(entityIdentifier + "$EVENT_IID", eventIID);
        entity.setFieldValue(entityIdentifier + "$FLOW_ID", flowId);
        entity.setFieldValue(entityIdentifier + "$FLOW_IID", flowIid);
        entity.setFieldValue(entityIdentifier + "$NODE_ID", nodeId);
        entity.setFieldValue(entityIdentifier + "$NODE_IID", nodeIid);
        entity.setFieldValue(entityIdentifier + "$EVENT", event.toString());
        entity.setFieldValue(entityIdentifier + "$TIME", new java.sql.Timestamp(System.currentTimeMillis()));
        entity.setFieldValue(entityIdentifier + "$INFORMATION", eventInformation);

        // Insert the entity.
        tx.insert(entity);

        // Insert the Node History parameters.
        T8HistoryParameterPersistenceHandler.insertParametersExpanded(tx, T8FlowManagerResource.NODE_HISTORY_PAR_DE_IDENTIFIER, eventIID, T8IdentifierUtilities.stripNamespace(eventParameters));
    }

    private void logDataObjectEvent(List<String> objectIids, FlowDataObjectHistoryEvent event) throws Exception
    {
        T8DataTransaction tx;
        String entityId;

        // Get the transaction.
        tx = serverContext.getDataManager().getCurrentSession().instantTransaction();

        for (String objectIid : objectIids)
        {
            T8FlowDataObjectState objectState;
            T8DataEntity entity;
            String eventIid;

            // Create a new data object history entity.
            objectState = flowState.getDataObjectState(objectIid);
            eventIid = T8IdentifierUtilities.createNewGUID();
            entity = tx.create(T8FlowManagerResource.FLOW_DATA_OBJECT_HISTORY_DE_IDENTIFIER, null);
            entityId = T8FlowManagerResource.FLOW_DATA_OBJECT_HISTORY_DE_IDENTIFIER;
            entity.setFieldValue(entityId + "$EVENT_IID", eventIid);
            entity.setFieldValue(entityId + "$FLOW_ID", flowId);
            entity.setFieldValue(entityId + "$FLOW_IID", flowIid);
            entity.setFieldValue(entityId + "$DATA_OBJECT_ID", objectState.getDataObjectId());
            entity.setFieldValue(entityId + "$DATA_OBJECT_IID", objectState.getDataObjectIid());
            entity.setFieldValue(entityId + "$EVENT", event.toString());
            entity.setFieldValue(entityId + "$TIME", new java.sql.Timestamp(System.currentTimeMillis()));

            // Insert the entity.
            tx.insert(entity);
        }
    }
}
